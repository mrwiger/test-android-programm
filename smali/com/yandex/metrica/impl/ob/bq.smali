.class Lcom/yandex/metrica/impl/ob/bq;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/yandex/metrica/impl/ob/dv;)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 50
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 51
    const-string v1, "sputi"

    iget-wide v2, p0, Lcom/yandex/metrica/impl/ob/dv;->g:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 52
    const-string v1, "spudi"

    iget v2, p0, Lcom/yandex/metrica/impl/ob/dv;->h:F

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 53
    const-string v1, "sbs"

    iget v2, p0, Lcom/yandex/metrica/impl/ob/dv;->i:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 54
    const-string v1, "mbs"

    iget v2, p0, Lcom/yandex/metrica/impl/ob/dv;->j:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 55
    const-string v1, "maff"

    iget-wide v2, p0, Lcom/yandex/metrica/impl/ob/dv;->k:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 56
    const-string v1, "mrtsl"

    iget v2, p0, Lcom/yandex/metrica/impl/ob/dv;->l:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 57
    const-string v1, "ce"

    iget-boolean v2, p0, Lcom/yandex/metrica/impl/ob/dv;->m:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 59
    return-object v0
.end method


# virtual methods
.method a(Landroid/os/Bundle;Ljava/lang/String;ZLcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/dq;)V
    .locals 6

    .prologue
    .line 27
    const-string v0, "package_name"

    invoke-virtual {p1, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    const-string v0, "clte"

    invoke-virtual {p1, v0, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 29
    if-eqz p4, :cond_0

    .line 30
    const-string v0, "flcc"

    .line 31
    invoke-static {p4}, Lcom/yandex/metrica/impl/ob/bq;->a(Lcom/yandex/metrica/impl/ob/dv;)Landroid/os/Bundle;

    move-result-object v1

    .line 30
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 33
    :cond_0
    if-eqz p5, :cond_1

    .line 34
    const-string v0, "blcc"

    .line 1041
    invoke-static {p5}, Lcom/yandex/metrica/impl/ob/bq;->a(Lcom/yandex/metrica/impl/ob/dv;)Landroid/os/Bundle;

    move-result-object v1

    .line 1042
    const-string v2, "cd"

    iget-wide v4, p5, Lcom/yandex/metrica/impl/ob/dq;->c:J

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1043
    const-string v2, "ci"

    iget-wide v4, p5, Lcom/yandex/metrica/impl/ob/dq;->d:J

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 34
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 37
    :cond_1
    return-void
.end method
