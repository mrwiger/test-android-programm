.class public Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;
.super Landroid/support/v4/app/Fragment;
.source "CalendarScheduleFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/mobile/calendar/CalendarScheduleFragment$PagerAdapter;
    }
.end annotation


# instance fields
.field private adapter:Lru/cn/tv/mobile/calendar/CalendarScheduleFragment$PagerAdapter;

.field private currentChannelId:J

.field private currentDate:Ljava/util/Calendar;

.field private listener:Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;

.field private noSchedule:Landroid/view/View;

.field private progress:Landroid/view/View;

.field private tabs:Landroid/support/design/widget/TabLayout;

.field private viewModel:Lru/cn/tv/mobile/calendar/CalendarViewModel;

.field private viewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;)Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->listener:Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;)J
    .locals 2
    .param p0, "x0"    # Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;

    .prologue
    .line 27
    iget-wide v0, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->currentChannelId:J

    return-wide v0
.end method

.method private initialSelection(Ljava/util/List;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/Calendar;",
            ">;)I"
        }
    .end annotation

    .prologue
    .local p1, "dates":Ljava/util/List;, "Ljava/util/List<Ljava/util/Calendar;>;"
    const/4 v7, 0x5

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 139
    const/4 v1, 0x0

    .line 140
    .local v1, "position":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 141
    .local v0, "c":Ljava/util/Calendar;
    iget-object v3, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->currentDate:Ljava/util/Calendar;

    invoke-virtual {v3, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->currentDate:Ljava/util/Calendar;

    .line 142
    invoke-virtual {v3, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->currentDate:Ljava/util/Calendar;

    .line 143
    invoke-virtual {v3, v7}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v3, v4, :cond_1

    .line 150
    .end local v0    # "c":Ljava/util/Calendar;
    :cond_0
    return v1

    .line 147
    .restart local v0    # "c":Ljava/util/Calendar;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 148
    goto :goto_0
.end method

.method private resetState()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 123
    invoke-virtual {p0}, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->tabs:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v0, v4}, Landroid/support/design/widget/TabLayout;->setVisibility(I)V

    .line 125
    iget-object v0, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v4}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 127
    iget-wide v0, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->currentChannelId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 128
    iget-object v0, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->noSchedule:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    iget-object v0, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->progress:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 132
    iget-object v0, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->noSchedule:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 133
    iget-object v0, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->viewModel:Lru/cn/tv/mobile/calendar/CalendarViewModel;

    iget-wide v2, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->currentChannelId:J

    invoke-virtual {v0, v2, v3}, Lru/cn/tv/mobile/calendar/CalendarViewModel;->setChannelId(J)V

    goto :goto_0
.end method

.method private setDates(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/Calendar;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "dates":Ljava/util/List;, "Ljava/util/List<Ljava/util/Calendar;>;"
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 105
    iget-object v1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->progress:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 107
    iget-object v1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->adapter:Lru/cn/tv/mobile/calendar/CalendarScheduleFragment$PagerAdapter;

    invoke-virtual {v1, p1}, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment$PagerAdapter;->setDates(Ljava/util/List;)V

    .line 108
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 109
    iget-object v1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->noSchedule:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 110
    iget-object v1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 111
    iget-object v1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->tabs:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v1, v2}, Landroid/support/design/widget/TabLayout;->setVisibility(I)V

    .line 120
    :goto_0
    return-void

    .line 113
    :cond_0
    iget-object v1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->noSchedule:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 114
    iget-object v1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v3}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 115
    iget-object v1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->tabs:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v1, v3}, Landroid/support/design/widget/TabLayout;->setVisibility(I)V

    .line 117
    invoke-direct {p0, p1}, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->initialSelection(Ljava/util/List;)I

    move-result v0

    .line 118
    .local v0, "position":I
    iget-object v1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$CalendarScheduleFragment(Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->setDates(Ljava/util/List;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 45
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 47
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v0

    const-class v1, Lru/cn/tv/mobile/calendar/CalendarViewModel;

    invoke-static {p0, v0, v1}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lru/cn/tv/mobile/calendar/CalendarViewModel;

    iput-object v0, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->viewModel:Lru/cn/tv/mobile/calendar/CalendarViewModel;

    .line 48
    iget-object v0, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->viewModel:Lru/cn/tv/mobile/calendar/CalendarViewModel;

    .line 49
    invoke-virtual {v0}, Lru/cn/tv/mobile/calendar/CalendarViewModel;->dates()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment$$Lambda$0;-><init>(Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;)V

    .line 50
    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 51
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 56
    const v0, 0x7f0c0097

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 61
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 62
    const v1, 0x7f0901c4

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/design/widget/TabLayout;

    iput-object v1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->tabs:Landroid/support/design/widget/TabLayout;

    .line 63
    const v1, 0x7f09018a

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager;

    iput-object v1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 64
    const v1, 0x7f090175

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->progress:Landroid/view/View;

    .line 66
    const v1, 0x7f09014b

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->noSchedule:Landroid/view/View;

    .line 67
    new-instance v1, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment$PagerAdapter;

    invoke-virtual {p0}, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment$PagerAdapter;-><init>(Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;Landroid/support/v4/app/FragmentManager;)V

    iput-object v1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->adapter:Lru/cn/tv/mobile/calendar/CalendarScheduleFragment$PagerAdapter;

    .line 68
    iget-object v1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->viewPager:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->adapter:Lru/cn/tv/mobile/calendar/CalendarScheduleFragment$PagerAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 69
    iget-object v1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->tabs:Landroid/support/design/widget/TabLayout;

    iget-object v2, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v2}, Landroid/support/design/widget/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 71
    invoke-virtual {p0}, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lru/cn/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 72
    iget-object v1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->tabs:Landroid/support/design/widget/TabLayout;

    invoke-virtual {p0}, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f060099

    invoke-static {v2, v3}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/design/widget/TabLayout;->setBackgroundColor(I)V

    .line 75
    :cond_0
    const v1, 0x7f0900e8

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 76
    .local v0, "goToAir":Landroid/view/View;
    new-instance v1, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment$1;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment$1;-><init>(Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    invoke-direct {p0}, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->resetState()V

    .line 86
    return-void
.end method

.method public setChannelId(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 89
    iget-wide v0, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->currentChannelId:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    .line 90
    iput-wide p1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->currentChannelId:J

    .line 91
    invoke-static {}, Lru/cn/utils/Utils;->getCalendar()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->currentDate:Ljava/util/Calendar;

    .line 92
    invoke-direct {p0}, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->resetState()V

    .line 94
    :cond_0
    return-void
.end method

.method public setListener(Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;)V
    .locals 0
    .param p1, "l"    # Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;

    .prologue
    .line 101
    iput-object p1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->listener:Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;

    .line 102
    return-void
.end method
