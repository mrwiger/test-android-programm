.class public Lcom/google/obf/hq;
.super Lcom/google/obf/gy;
.source "IMASDK"

# interfaces
.implements Lcom/google/ads/interactivemedia/v3/api/StreamManager;


# instance fields
.field private final g:Ljava/lang/String;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcom/google/ads/interactivemedia/v3/api/AdProgressInfo;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/obf/hj;Lcom/google/obf/hl;Lcom/google/ads/interactivemedia/v3/api/StreamDisplayContainer;Lcom/google/ads/interactivemedia/v3/api/player/ContentProgressProvider;Ljava/lang/String;Lcom/google/obf/ht;Lcom/google/obf/hc;Lcom/google/obf/gi;Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/ads/interactivemedia/v3/api/AdError;
        }
    .end annotation

    .prologue
    .line 6
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    move/from16 v8, p12

    invoke-direct/range {v1 .. v8}, Lcom/google/obf/gy;-><init>(Ljava/lang/String;Lcom/google/obf/hj;Lcom/google/obf/hl;Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;Lcom/google/obf/gi;Landroid/content/Context;Z)V

    .line 7
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/obf/hq;->h:Ljava/util/List;

    .line 8
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/obf/hq;->g:Ljava/lang/String;

    .line 9
    if-eqz p7, :cond_0

    .line 10
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/obf/hq;->c:Lcom/google/obf/ht;

    .line 13
    :goto_0
    iget-object v1, p0, Lcom/google/obf/hq;->c:Lcom/google/obf/ht;

    invoke-virtual {p0, v1}, Lcom/google/obf/hq;->addAdErrorListener(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent$AdErrorListener;)V

    .line 14
    iget-object v1, p0, Lcom/google/obf/hq;->c:Lcom/google/obf/ht;

    invoke-virtual {p2, v1, p1}, Lcom/google/obf/hj;->a(Lcom/google/obf/ht;Ljava/lang/String;)V

    .line 15
    return-void

    .line 11
    :cond_0
    new-instance v1, Lcom/google/obf/hs;

    move-object v2, p1

    move-object v3, p3

    move-object v4, p2

    move-object v5, p0

    move-object v6, p4

    move-object v7, p6

    move-object/from16 v8, p10

    invoke-direct/range {v1 .. v8}, Lcom/google/obf/hs;-><init>(Ljava/lang/String;Lcom/google/obf/hl;Lcom/google/obf/hj;Lcom/google/obf/hq;Lcom/google/ads/interactivemedia/v3/api/StreamDisplayContainer;Ljava/lang/String;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/obf/hq;->c:Lcom/google/obf/ht;

    .line 12
    iget-object v1, p0, Lcom/google/obf/hq;->c:Lcom/google/obf/ht;

    check-cast v1, Lcom/google/obf/hs;

    invoke-virtual {v1}, Lcom/google/obf/hs;->f()V

    goto :goto_0
.end method

.method constructor <init>(Ljava/lang/String;Lcom/google/obf/hj;Lcom/google/obf/hl;Lcom/google/ads/interactivemedia/v3/api/StreamRequest;Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/ads/interactivemedia/v3/api/AdError;
        }
    .end annotation

    .prologue
    .line 1
    invoke-interface/range {p4 .. p4}, Lcom/google/ads/interactivemedia/v3/api/StreamRequest;->getStreamDisplayContainer()Lcom/google/ads/interactivemedia/v3/api/StreamDisplayContainer;

    move-result-object v4

    .line 2
    invoke-interface/range {p4 .. p4}, Lcom/google/ads/interactivemedia/v3/api/StreamRequest;->getStreamDisplayContainer()Lcom/google/ads/interactivemedia/v3/api/StreamDisplayContainer;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/StreamDisplayContainer;->getVideoStreamPlayer()Lcom/google/ads/interactivemedia/v3/api/player/VideoStreamPlayer;

    move-result-object v5

    .line 3
    invoke-interface/range {p4 .. p4}, Lcom/google/ads/interactivemedia/v3/api/StreamRequest;->getManifestSuffix()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    move/from16 v12, p7

    .line 4
    invoke-direct/range {v0 .. v12}, Lcom/google/obf/hq;-><init>(Ljava/lang/String;Lcom/google/obf/hj;Lcom/google/obf/hl;Lcom/google/ads/interactivemedia/v3/api/StreamDisplayContainer;Lcom/google/ads/interactivemedia/v3/api/player/ContentProgressProvider;Ljava/lang/String;Lcom/google/obf/ht;Lcom/google/obf/hc;Lcom/google/obf/gi;Landroid/content/Context;Ljava/lang/String;Z)V

    .line 5
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 67
    invoke-super {p0, p1, p2, p3}, Lcom/google/obf/gy;->a(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;ILjava/lang/String;)V

    return-void
.end method

.method public bridge synthetic a(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 66
    invoke-super {p0, p1, p2, p3}, Lcom/google/obf/gy;->a(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V

    return-void
.end method

.method public a(Lcom/google/obf/hj$c;)V
    .locals 2

    .prologue
    .line 16
    sget-object v0, Lcom/google/obf/hq$1;->a:[I

    iget-object v1, p1, Lcom/google/obf/hj$c;->a:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 26
    :goto_0
    invoke-super {p0, p1}, Lcom/google/obf/gy;->a(Lcom/google/obf/hj$c;)V

    .line 27
    return-void

    .line 17
    :pswitch_0
    iget-object v0, p0, Lcom/google/obf/hq;->c:Lcom/google/obf/ht;

    invoke-interface {v0}, Lcom/google/obf/ht;->b()V

    goto :goto_0

    .line 19
    :pswitch_1
    iget-object v0, p0, Lcom/google/obf/hq;->c:Lcom/google/obf/ht;

    invoke-interface {v0}, Lcom/google/obf/ht;->c()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/hq;->i:Lcom/google/ads/interactivemedia/v3/api/AdProgressInfo;

    goto :goto_0

    .line 22
    :pswitch_2
    iget-object v0, p1, Lcom/google/obf/hj$c;->d:Ljava/util/List;

    iput-object v0, p0, Lcom/google/obf/hq;->h:Ljava/util/List;

    goto :goto_0

    .line 24
    :pswitch_3
    iget-object v0, p1, Lcom/google/obf/hj$c;->e:Lcom/google/ads/interactivemedia/v3/api/AdProgressInfo;

    iput-object v0, p0, Lcom/google/obf/hq;->i:Lcom/google/ads/interactivemedia/v3/api/AdProgressInfo;

    goto :goto_0

    .line 16
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 65
    invoke-super {p0, p1}, Lcom/google/obf/gy;->a(Ljava/util/Map;)V

    return-void
.end method

.method public bridge synthetic addAdErrorListener(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent$AdErrorListener;)V
    .locals 0

    .prologue
    .line 71
    invoke-super {p0, p1}, Lcom/google/obf/gy;->addAdErrorListener(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent$AdErrorListener;)V

    return-void
.end method

.method public bridge synthetic addAdEventListener(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventListener;)V
    .locals 0

    .prologue
    .line 69
    invoke-super {p0, p1}, Lcom/google/obf/gy;->addAdEventListener(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventListener;)V

    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/google/obf/hi$c;->contentComplete:Lcom/google/obf/hi$c;

    invoke-virtual {p0, v0}, Lcom/google/obf/hq;->a(Lcom/google/obf/hi$c;)V

    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/hq;->f:Z

    .line 63
    invoke-virtual {p0}, Lcom/google/obf/hq;->a()V

    .line 64
    return-void
.end method

.method public bridge synthetic getAdProgress()Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;
    .locals 1

    .prologue
    .line 73
    invoke-super {p0}, Lcom/google/obf/gy;->getAdProgress()Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getCurrentAd()Lcom/google/ads/interactivemedia/v3/api/Ad;
    .locals 1

    .prologue
    .line 72
    invoke-super {p0}, Lcom/google/obf/gy;->getCurrentAd()Lcom/google/ads/interactivemedia/v3/api/Ad;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic init(Lcom/google/ads/interactivemedia/v3/api/AdsRenderingSettings;)V
    .locals 0

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/google/obf/gy;->init(Lcom/google/ads/interactivemedia/v3/api/AdsRenderingSettings;)V

    return-void
.end method

.method public isCustomPlaybackUsed()Z
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic removeAdErrorListener(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent$AdErrorListener;)V
    .locals 0

    .prologue
    .line 70
    invoke-super {p0, p1}, Lcom/google/obf/gy;->removeAdErrorListener(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent$AdErrorListener;)V

    return-void
.end method

.method public bridge synthetic removeAdEventListener(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventListener;)V
    .locals 0

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/google/obf/gy;->removeAdEventListener(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventListener;)V

    return-void
.end method
