.class Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;
.super Ljava/lang/Object;
.source "RuntimeCheckOnConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltoothpick/configuration/RuntimeCheckOnConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Pair"
.end annotation


# instance fields
.field public final clazz:Ljava/lang/Class;

.field public final name:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 0
    .param p1, "clazz"    # Ljava/lang/Class;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;->clazz:Ljava/lang/Class;

    .line 80
    iput-object p2, p0, Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;->name:Ljava/lang/String;

    .line 81
    return-void
.end method

.method static synthetic access$000(Ljava/util/Collection;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/Collection;

    .prologue
    .line 74
    invoke-static {p0}, Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;->getClassList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private equal(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "a"    # Ljava/lang/Object;
    .param p2, "b"    # Ljava/lang/Object;

    .prologue
    .line 98
    if-eq p1, p2, :cond_0

    if-eqz p1, :cond_1

    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getClassList(Ljava/util/Collection;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    .local p0, "pairCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 103
    .local v0, "classList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Class;>;"
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;

    .line 104
    .local v1, "pair":Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;
    iget-object v3, v1, Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;->clazz:Ljava/lang/Class;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 106
    .end local v1    # "pair":Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;
    :cond_0
    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 85
    instance-of v2, p1, Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;

    if-nez v2, :cond_1

    .line 89
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 88
    check-cast v0, Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;

    .line 89
    .local v0, "p":Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;
    iget-object v2, v0, Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;->clazz:Ljava/lang/Class;

    iget-object v3, p0, Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;->clazz:Ljava/lang/Class;

    invoke-direct {p0, v2, v3}, Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;->name:Ljava/lang/String;

    iget-object v3, p0, Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;->name:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 94
    iget-object v0, p0, Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;->clazz:Ljava/lang/Class;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v2, p0, Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;->name:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    xor-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;->clazz:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method
