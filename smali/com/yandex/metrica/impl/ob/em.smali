.class Lcom/yandex/metrica/impl/ob/em;
.super Lcom/yandex/metrica/impl/ob/dt;
.source "SourceFile"


# instance fields
.field private a:Lcom/yandex/metrica/impl/ob/ct;

.field private b:Lcom/yandex/metrica/impl/ob/du;

.field private c:Lcom/yandex/metrica/impl/utils/p;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/ds;)V
    .locals 3

    .prologue
    .line 35
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/cq;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/cq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/cq;->f()Lcom/yandex/metrica/impl/ob/ct;

    move-result-object v0

    new-instance v1, Lcom/yandex/metrica/impl/ob/du;

    invoke-direct {v1, p1}, Lcom/yandex/metrica/impl/ob/du;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/yandex/metrica/impl/utils/p;

    invoke-direct {v2}, Lcom/yandex/metrica/impl/utils/p;-><init>()V

    invoke-direct {p0, p2, v0, v1, v2}, Lcom/yandex/metrica/impl/ob/em;-><init>(Lcom/yandex/metrica/impl/ob/ds;Lcom/yandex/metrica/impl/ob/ct;Lcom/yandex/metrica/impl/ob/du;Lcom/yandex/metrica/impl/utils/p;)V

    .line 37
    return-void
.end method

.method constructor <init>(Lcom/yandex/metrica/impl/ob/ds;Lcom/yandex/metrica/impl/ob/ct;Lcom/yandex/metrica/impl/ob/du;Lcom/yandex/metrica/impl/utils/p;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/dt;-><init>(Lcom/yandex/metrica/impl/ob/ds;)V

    .line 58
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/em;->a:Lcom/yandex/metrica/impl/ob/ct;

    .line 59
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/em;->b:Lcom/yandex/metrica/impl/ob/du;

    .line 60
    iput-object p4, p0, Lcom/yandex/metrica/impl/ob/em;->c:Lcom/yandex/metrica/impl/utils/p;

    .line 61
    return-void
.end method


# virtual methods
.method public a(Landroid/location/Location;Lcom/yandex/metrica/impl/ob/dv;)V
    .locals 4

    .prologue
    .line 42
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 43
    new-instance v0, Lcom/yandex/metrica/impl/ob/eh;

    invoke-virtual {p2}, Lcom/yandex/metrica/impl/ob/dv;->b()Lcom/yandex/metrica/impl/ob/dv$a;

    move-result-object v1

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/em;->c:Lcom/yandex/metrica/impl/utils/p;

    .line 44
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/utils/p;->a()J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/yandex/metrica/impl/ob/eh;-><init>(Lcom/yandex/metrica/impl/ob/dv$a;JLandroid/location/Location;)V

    .line 46
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/em;->b:Lcom/yandex/metrica/impl/ob/du;

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/ob/du;->a(Lcom/yandex/metrica/impl/ob/eh;)Ljava/lang/String;

    move-result-object v0

    .line 47
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 50
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/em;->a:Lcom/yandex/metrica/impl/ob/ct;

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, v0}, Lcom/yandex/metrica/impl/ob/ct;->a(JLjava/lang/String;)V

    .line 53
    :cond_0
    return-void
.end method
