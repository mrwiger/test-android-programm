.class public final Lru/cn/ad/PlaceLoader;
.super Ljava/lang/Object;
.source "PlaceLoader.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private final factory:Lru/cn/ad/AdAdapter$Factory;

.field private isLoading:Z

.field private listener:Lru/cn/ad/AdapterLoader$Listener;

.field private loadStepListener:Lru/cn/ad/AdapterLoader$LoadStepListener;

.field private loader:Lru/cn/ad/AdapterLoader;

.field private final placeId:Ljava/lang/String;

.field private task:Landroid/os/AsyncTask;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lru/cn/ad/AdAdapter$Factory;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "placeId"    # Ljava/lang/String;
    .param p3, "factory"    # Lru/cn/ad/AdAdapter$Factory;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lru/cn/ad/PlaceLoader;->context:Landroid/content/Context;

    .line 25
    iput-object p2, p0, Lru/cn/ad/PlaceLoader;->placeId:Ljava/lang/String;

    .line 26
    iput-object p3, p0, Lru/cn/ad/PlaceLoader;->factory:Lru/cn/ad/AdAdapter$Factory;

    .line 27
    return-void
.end method

.method static synthetic access$000(Lru/cn/ad/PlaceLoader;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/PlaceLoader;

    .prologue
    .line 8
    iget-object v0, p0, Lru/cn/ad/PlaceLoader;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/ad/PlaceLoader;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/PlaceLoader;

    .prologue
    .line 8
    iget-object v0, p0, Lru/cn/ad/PlaceLoader;->placeId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lru/cn/ad/PlaceLoader;Z)Z
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/PlaceLoader;
    .param p1, "x1"    # Z

    .prologue
    .line 8
    iput-boolean p1, p0, Lru/cn/ad/PlaceLoader;->isLoading:Z

    return p1
.end method

.method static synthetic access$300(Lru/cn/ad/PlaceLoader;)Lru/cn/ad/AdapterLoader$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/PlaceLoader;

    .prologue
    .line 8
    iget-object v0, p0, Lru/cn/ad/PlaceLoader;->listener:Lru/cn/ad/AdapterLoader$Listener;

    return-object v0
.end method

.method static synthetic access$400(Lru/cn/ad/PlaceLoader;Lru/cn/api/money_miner/replies/AdPlace;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/PlaceLoader;
    .param p1, "x1"    # Lru/cn/api/money_miner/replies/AdPlace;

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lru/cn/ad/PlaceLoader;->loadAdapters(Lru/cn/api/money_miner/replies/AdPlace;)V

    return-void
.end method

.method private loadAdapters(Lru/cn/api/money_miner/replies/AdPlace;)V
    .locals 4
    .param p1, "place"    # Lru/cn/api/money_miner/replies/AdPlace;

    .prologue
    .line 83
    new-instance v0, Lru/cn/ad/AdapterLoader;

    iget-object v1, p0, Lru/cn/ad/PlaceLoader;->placeId:Ljava/lang/String;

    iget-object v2, p1, Lru/cn/api/money_miner/replies/AdPlace;->networks:Ljava/util/List;

    iget-object v3, p0, Lru/cn/ad/PlaceLoader;->factory:Lru/cn/ad/AdAdapter$Factory;

    invoke-direct {v0, v1, v2, v3}, Lru/cn/ad/AdapterLoader;-><init>(Ljava/lang/String;Ljava/util/List;Lru/cn/ad/AdAdapter$Factory;)V

    iput-object v0, p0, Lru/cn/ad/PlaceLoader;->loader:Lru/cn/ad/AdapterLoader;

    .line 84
    iget-object v0, p0, Lru/cn/ad/PlaceLoader;->loader:Lru/cn/ad/AdapterLoader;

    iget-object v1, p0, Lru/cn/ad/PlaceLoader;->listener:Lru/cn/ad/AdapterLoader$Listener;

    invoke-virtual {v0, v1}, Lru/cn/ad/AdapterLoader;->setListener(Lru/cn/ad/AdapterLoader$Listener;)V

    .line 85
    iget-object v0, p0, Lru/cn/ad/PlaceLoader;->loader:Lru/cn/ad/AdapterLoader;

    iget-object v1, p0, Lru/cn/ad/PlaceLoader;->loadStepListener:Lru/cn/ad/AdapterLoader$LoadStepListener;

    invoke-virtual {v0, v1}, Lru/cn/ad/AdapterLoader;->setLoadStepListener(Lru/cn/ad/AdapterLoader$LoadStepListener;)V

    .line 86
    iget-object v0, p0, Lru/cn/ad/PlaceLoader;->loader:Lru/cn/ad/AdapterLoader;

    invoke-virtual {v0}, Lru/cn/ad/AdapterLoader;->load()V

    .line 87
    return-void
.end method


# virtual methods
.method public load()V
    .locals 2

    .prologue
    .line 30
    invoke-virtual {p0}, Lru/cn/ad/PlaceLoader;->stop()V

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/ad/PlaceLoader;->isLoading:Z

    .line 33
    new-instance v0, Lru/cn/ad/PlaceLoader$1;

    invoke-direct {v0, p0}, Lru/cn/ad/PlaceLoader$1;-><init>(Lru/cn/ad/PlaceLoader;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 55
    invoke-virtual {v0, v1}, Lru/cn/ad/PlaceLoader$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/PlaceLoader;->task:Landroid/os/AsyncTask;

    .line 56
    return-void
.end method

.method public setListener(Lru/cn/ad/AdapterLoader$Listener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/ad/AdapterLoader$Listener;

    .prologue
    .line 59
    iput-object p1, p0, Lru/cn/ad/PlaceLoader;->listener:Lru/cn/ad/AdapterLoader$Listener;

    .line 60
    return-void
.end method

.method public setLoadStepListener(Lru/cn/ad/AdapterLoader$LoadStepListener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/ad/AdapterLoader$LoadStepListener;

    .prologue
    .line 63
    iput-object p1, p0, Lru/cn/ad/PlaceLoader;->loadStepListener:Lru/cn/ad/AdapterLoader$LoadStepListener;

    .line 64
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lru/cn/ad/PlaceLoader;->task:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lru/cn/ad/PlaceLoader;->task:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 75
    :cond_0
    iget-object v0, p0, Lru/cn/ad/PlaceLoader;->loader:Lru/cn/ad/AdapterLoader;

    if-eqz v0, :cond_1

    .line 76
    iget-object v0, p0, Lru/cn/ad/PlaceLoader;->loader:Lru/cn/ad/AdapterLoader;

    invoke-virtual {v0}, Lru/cn/ad/AdapterLoader;->stop()V

    .line 79
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/ad/PlaceLoader;->isLoading:Z

    .line 80
    return-void
.end method
