.class public interface abstract Lru/cn/draggableview/DraggableView$DraggableViewListener;
.super Ljava/lang/Object;
.source "DraggableView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/draggableview/DraggableView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DraggableViewListener"
.end annotation


# virtual methods
.method public abstract closed()V
.end method

.method public abstract dragViewClicked()Z
.end method

.method public abstract dragViewDoubleClicked()Z
.end method

.method public abstract maximized()V
.end method

.method public abstract minimized()V
.end method

.method public abstract offsetChanged(FF)V
.end method
