.class Lru/cn/tv/mobile/live/LiveNowFragment$3;
.super Ljava/lang/Object;
.source "LiveNowFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/mobile/live/LiveNowFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/mobile/live/LiveNowFragment;


# direct methods
.method constructor <init>(Lru/cn/tv/mobile/live/LiveNowFragment;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/mobile/live/LiveNowFragment;

    .prologue
    .line 160
    iput-object p1, p0, Lru/cn/tv/mobile/live/LiveNowFragment$3;->this$0:Lru/cn/tv/mobile/live/LiveNowFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 164
    iget-object v4, p0, Lru/cn/tv/mobile/live/LiveNowFragment$3;->this$0:Lru/cn/tv/mobile/live/LiveNowFragment;

    invoke-static {v4}, Lru/cn/tv/mobile/live/LiveNowFragment;->access$500(Lru/cn/tv/mobile/live/LiveNowFragment;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 165
    iget-object v4, p0, Lru/cn/tv/mobile/live/LiveNowFragment$3;->this$0:Lru/cn/tv/mobile/live/LiveNowFragment;

    invoke-static {v4}, Lru/cn/tv/mobile/live/LiveNowFragment;->access$400(Lru/cn/tv/mobile/live/LiveNowFragment;)Lru/cn/tv/mobile/live/LiveNowViewModel;

    move-result-object v4

    invoke-virtual {v4}, Lru/cn/tv/mobile/live/LiveNowViewModel;->reload()V

    .line 185
    :goto_0
    return-void

    .line 169
    :cond_0
    iget-object v4, p0, Lru/cn/tv/mobile/live/LiveNowFragment$3;->this$0:Lru/cn/tv/mobile/live/LiveNowFragment;

    invoke-static {v4}, Lru/cn/tv/mobile/live/LiveNowFragment;->access$300(Lru/cn/tv/mobile/live/LiveNowFragment;)Landroid/support/v7/widget/LinearLayoutManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v7/widget/LinearLayoutManager;->findFirstCompletelyVisibleItemPosition()I

    move-result v0

    .line 170
    .local v0, "firstVisibleBeforeLoad":I
    iget-object v4, p0, Lru/cn/tv/mobile/live/LiveNowFragment$3;->this$0:Lru/cn/tv/mobile/live/LiveNowFragment;

    invoke-static {v4}, Lru/cn/tv/mobile/live/LiveNowFragment;->access$300(Lru/cn/tv/mobile/live/LiveNowFragment;)Landroid/support/v7/widget/LinearLayoutManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v7/widget/LinearLayoutManager;->findLastCompletelyVisibleItemPosition()I

    move-result v3

    .line 172
    .local v3, "lastVisibleBeforeLoad":I
    iget-object v4, p0, Lru/cn/tv/mobile/live/LiveNowFragment$3;->this$0:Lru/cn/tv/mobile/live/LiveNowFragment;

    invoke-static {v4}, Lru/cn/tv/mobile/live/LiveNowFragment;->access$000(Lru/cn/tv/mobile/live/LiveNowFragment;)Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 173
    move v1, v0

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_2

    .line 175
    iget-object v4, p0, Lru/cn/tv/mobile/live/LiveNowFragment$3;->this$0:Lru/cn/tv/mobile/live/LiveNowFragment;

    invoke-static {v4}, Lru/cn/tv/mobile/live/LiveNowFragment;->access$000(Lru/cn/tv/mobile/live/LiveNowFragment;)Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;

    move-result-object v4

    invoke-virtual {v4, v1}, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->getItem(I)Landroid/database/Cursor;

    move-result-object v2

    .line 176
    .local v2, "item":Landroid/database/Cursor;
    iget-object v4, p0, Lru/cn/tv/mobile/live/LiveNowFragment$3;->this$0:Lru/cn/tv/mobile/live/LiveNowFragment;

    invoke-static {v4, v2}, Lru/cn/tv/mobile/live/LiveNowFragment;->access$600(Lru/cn/tv/mobile/live/LiveNowFragment;Landroid/database/Cursor;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lru/cn/tv/mobile/live/LiveNowFragment$3;->this$0:Lru/cn/tv/mobile/live/LiveNowFragment;

    invoke-static {v4}, Lru/cn/tv/mobile/live/LiveNowFragment;->access$200(Lru/cn/tv/mobile/live/LiveNowFragment;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 177
    iget-object v4, p0, Lru/cn/tv/mobile/live/LiveNowFragment$3;->this$0:Lru/cn/tv/mobile/live/LiveNowFragment;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lru/cn/tv/mobile/live/LiveNowFragment;->access$502(Lru/cn/tv/mobile/live/LiveNowFragment;Z)Z

    .line 180
    :cond_1
    iget-object v4, p0, Lru/cn/tv/mobile/live/LiveNowFragment$3;->this$0:Lru/cn/tv/mobile/live/LiveNowFragment;

    invoke-static {v4}, Lru/cn/tv/mobile/live/LiveNowFragment;->access$000(Lru/cn/tv/mobile/live/LiveNowFragment;)Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;

    move-result-object v4

    invoke-virtual {v4, v1}, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->notifyItemChanged(I)V

    .line 173
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 184
    .end local v1    # "i":I
    .end local v2    # "item":Landroid/database/Cursor;
    :cond_2
    iget-object v4, p0, Lru/cn/tv/mobile/live/LiveNowFragment$3;->this$0:Lru/cn/tv/mobile/live/LiveNowFragment;

    invoke-static {v4}, Lru/cn/tv/mobile/live/LiveNowFragment;->access$700(Lru/cn/tv/mobile/live/LiveNowFragment;)V

    goto :goto_0
.end method
