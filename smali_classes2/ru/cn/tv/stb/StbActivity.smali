.class public Lru/cn/tv/stb/StbActivity;
.super Lru/cn/tv/FullScreenActivity;
.source "StbActivity.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lru/cn/tv/FullScreenActivity;",
        "Landroid/os/Handler$Callback;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private audioTrackInfoAdapter:Lru/cn/tv/player/controller/TracksAdapter;

.field private audioTrackInfoProvider:Lru/cn/player/ITrackSelector;

.field private billingFragment:Lru/cn/tv/billing/BillingFragment;

.field private billingFragmentWrapper:Landroid/view/View;

.field private blockedContentListener:Landroid/view/View$OnClickListener;

.field private bottomSheetBehavior:Landroid/support/design/widget/BottomSheetBehavior;

.field private calendarFragment:Lru/cn/tv/stb/calendar/CalendarFragment;

.field private calendarFragmentList:Landroid/widget/ListView;

.field private calendarFragmentWrapper:Landroid/view/View;

.field private calendarItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private calendarKeyListener:Landroid/view/View$OnKeyListener;

.field private categoryFragment:Lru/cn/tv/stb/categories/CategoryFragment;

.field private categoryFragmentList:Landroid/widget/ListView;

.field private categoryFragmentListener:Lru/cn/tv/stb/categories/CategoryFragment$CategoryFragmentListener;

.field private categoryFragmentWrapper:Landroid/view/View;

.field private categoryKeyListener:Landroid/view/View$OnKeyListener;

.field private channelNumber:Landroid/widget/TextView;

.field private channelsFragment:Lru/cn/tv/stb/channels/ChannelsFragment;

.field private channelsFragmentList:Landroid/widget/ListView;

.field private channelsFragmentListener:Lru/cn/tv/stb/channels/ChannelsFragment$ChannelsFragmentListener;

.field private channelsFragmentWrapper:Landroid/view/View;

.field private channelsKeyListener:Landroid/view/View$OnKeyListener;

.field private channelsListTouched:Z

.field private collectionFragmentWrapper:Landroid/view/View;

.field private collectionsFragment:Lru/cn/tv/stb/collections/CollectionsFragment;

.field private currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

.field private currentPornoSubcategory:I

.field private currentSettingSubcategory:I

.field private handler:Landroid/os/Handler;

.field private informingFragment:Lru/cn/tv/stb/informing/InformingFragment;

.field private isPlaying:Z

.field private kidsMode:Z

.field private lastChannelPrefs:Lru/cn/domain/LastChannel;

.field private loadChannelFrom:Lru/cn/domain/tv/CurrentCategory$Type;

.field private nextTelecastView:Lru/cn/tv/player/controller/StbNextTelecastView;

.field private notificationView:Landroid/view/View;

.field private pinCode:Lru/cn/domain/PinCode;

.field private playerController:Lru/cn/tv/player/controller/StbPlayerController;

.field private playingAdvertisement:Z

.field private receiver:Lru/cn/network/NetworkChangeReceiver;

.field private relatedPanelListener:Lru/cn/tv/player/controller/StbPlayerRelatedPanel$Listener;

.field private remoteContentController:Lru/cn/peersay/controllers/PlayContentController;

.field private remoteController:Lru/cn/peersay/controllers/PlayerRemoteController;

.field private scheduleFragment:Lru/cn/tv/stb/schedule/ScheduleFragment;

.field private scheduleFragmentList:Landroid/widget/ListView;

.field private scheduleFragmentWrapper:Landroid/view/View;

.field private scheduleItemClickListener:Lru/cn/tv/stb/schedule/ScheduleFragment$ScheduleItemClickListener;

.field private scheduleKeyListener:Landroid/view/View$OnKeyListener;

.field private settingFragment:Lru/cn/tv/stb/settings/SettingFragment;

.field private settingFragmentList:Landroid/widget/ListView;

.field private settingFragmentListener:Lru/cn/tv/stb/settings/SettingFragment$SettingFragmentListener;

.field private settingOnKeyListener:Landroid/view/View$OnKeyListener;

.field private settingsFragmentWrapper:Landroid/view/View;

.field private settingsPanelListener:Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;

.field private simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

.field private simplePlayerFragmentListener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

.field private startDigitInput:Z

.field private stbViewModel:Lru/cn/tv/stb/StbViewModel;

.field private subtitleTrackInfoAdapter:Lru/cn/tv/player/controller/TracksAdapter;

.field private subtitlesTrackProvider:Lru/cn/player/ITrackSelector;

.field private trackListView:Landroid/widget/ListView;

.field private trackListViewWrapper:Landroid/view/View;

.field private volumeControl:Lru/cn/view/VolumeControl;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 90
    invoke-direct {p0}, Lru/cn/tv/FullScreenActivity;-><init>()V

    .line 109
    iput v1, p0, Lru/cn/tv/stb/StbActivity;->currentPornoSubcategory:I

    .line 113
    const/4 v0, 0x2

    iput v0, p0, Lru/cn/tv/stb/StbActivity;->currentSettingSubcategory:I

    .line 124
    iput-boolean v1, p0, Lru/cn/tv/stb/StbActivity;->isPlaying:Z

    .line 172
    sget-object v0, Lru/cn/domain/tv/CurrentCategory$Type;->all:Lru/cn/domain/tv/CurrentCategory$Type;

    iput-object v0, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    .line 178
    iput-boolean v1, p0, Lru/cn/tv/stb/StbActivity;->playingAdvertisement:Z

    .line 1334
    new-instance v0, Lru/cn/tv/stb/StbActivity$17;

    invoke-direct {v0, p0}, Lru/cn/tv/stb/StbActivity$17;-><init>(Lru/cn/tv/stb/StbActivity;)V

    iput-object v0, p0, Lru/cn/tv/stb/StbActivity;->relatedPanelListener:Lru/cn/tv/player/controller/StbPlayerRelatedPanel$Listener;

    .line 1364
    new-instance v0, Lru/cn/tv/stb/StbActivity$18;

    invoke-direct {v0, p0}, Lru/cn/tv/stb/StbActivity$18;-><init>(Lru/cn/tv/stb/StbActivity;)V

    iput-object v0, p0, Lru/cn/tv/stb/StbActivity;->settingsPanelListener:Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;

    .line 1418
    new-instance v0, Lru/cn/tv/stb/StbActivity$19;

    invoke-direct {v0, p0}, Lru/cn/tv/stb/StbActivity$19;-><init>(Lru/cn/tv/stb/StbActivity;)V

    iput-object v0, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragmentListener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

    .line 1589
    new-instance v0, Lru/cn/tv/stb/StbActivity$22;

    invoke-direct {v0, p0}, Lru/cn/tv/stb/StbActivity$22;-><init>(Lru/cn/tv/stb/StbActivity;)V

    iput-object v0, p0, Lru/cn/tv/stb/StbActivity;->categoryKeyListener:Landroid/view/View$OnKeyListener;

    .line 1610
    new-instance v0, Lru/cn/tv/stb/StbActivity$23;

    invoke-direct {v0, p0}, Lru/cn/tv/stb/StbActivity$23;-><init>(Lru/cn/tv/stb/StbActivity;)V

    iput-object v0, p0, Lru/cn/tv/stb/StbActivity;->categoryFragmentListener:Lru/cn/tv/stb/categories/CategoryFragment$CategoryFragmentListener;

    .line 1620
    new-instance v0, Lru/cn/tv/stb/StbActivity$24;

    invoke-direct {v0, p0}, Lru/cn/tv/stb/StbActivity$24;-><init>(Lru/cn/tv/stb/StbActivity;)V

    iput-object v0, p0, Lru/cn/tv/stb/StbActivity;->channelsKeyListener:Landroid/view/View$OnKeyListener;

    .line 1690
    new-instance v0, Lru/cn/tv/stb/StbActivity$25;

    invoke-direct {v0, p0}, Lru/cn/tv/stb/StbActivity$25;-><init>(Lru/cn/tv/stb/StbActivity;)V

    iput-object v0, p0, Lru/cn/tv/stb/StbActivity;->channelsFragmentListener:Lru/cn/tv/stb/channels/ChannelsFragment$ChannelsFragmentListener;

    .line 1770
    new-instance v0, Lru/cn/tv/stb/StbActivity$26;

    invoke-direct {v0, p0}, Lru/cn/tv/stb/StbActivity$26;-><init>(Lru/cn/tv/stb/StbActivity;)V

    iput-object v0, p0, Lru/cn/tv/stb/StbActivity;->calendarKeyListener:Landroid/view/View$OnKeyListener;

    .line 1793
    new-instance v0, Lru/cn/tv/stb/StbActivity$27;

    invoke-direct {v0, p0}, Lru/cn/tv/stb/StbActivity$27;-><init>(Lru/cn/tv/stb/StbActivity;)V

    iput-object v0, p0, Lru/cn/tv/stb/StbActivity;->calendarItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 1806
    new-instance v0, Lru/cn/tv/stb/StbActivity$28;

    invoke-direct {v0, p0}, Lru/cn/tv/stb/StbActivity$28;-><init>(Lru/cn/tv/stb/StbActivity;)V

    iput-object v0, p0, Lru/cn/tv/stb/StbActivity;->scheduleKeyListener:Landroid/view/View$OnKeyListener;

    .line 1891
    new-instance v0, Lru/cn/tv/stb/StbActivity$29;

    invoke-direct {v0, p0}, Lru/cn/tv/stb/StbActivity$29;-><init>(Lru/cn/tv/stb/StbActivity;)V

    iput-object v0, p0, Lru/cn/tv/stb/StbActivity;->scheduleItemClickListener:Lru/cn/tv/stb/schedule/ScheduleFragment$ScheduleItemClickListener;

    .line 1943
    new-instance v0, Lru/cn/tv/stb/StbActivity$30;

    invoke-direct {v0, p0}, Lru/cn/tv/stb/StbActivity$30;-><init>(Lru/cn/tv/stb/StbActivity;)V

    iput-object v0, p0, Lru/cn/tv/stb/StbActivity;->settingOnKeyListener:Landroid/view/View$OnKeyListener;

    .line 1953
    new-instance v0, Lru/cn/tv/stb/StbActivity$31;

    invoke-direct {v0, p0}, Lru/cn/tv/stb/StbActivity$31;-><init>(Lru/cn/tv/stb/StbActivity;)V

    iput-object v0, p0, Lru/cn/tv/stb/StbActivity;->settingFragmentListener:Lru/cn/tv/stb/settings/SettingFragment$SettingFragmentListener;

    .line 2055
    iput-boolean v1, p0, Lru/cn/tv/stb/StbActivity;->startDigitInput:Z

    .line 2581
    new-instance v0, Lru/cn/tv/stb/StbActivity$40;

    invoke-direct {v0, p0}, Lru/cn/tv/stb/StbActivity$40;-><init>(Lru/cn/tv/stb/StbActivity;)V

    iput-object v0, p0, Lru/cn/tv/stb/StbActivity;->blockedContentListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private Rot()Z
    .locals 4

    .prologue
    .line 1324
    iget-object v2, p0, Lru/cn/tv/stb/StbActivity;->lastChannelPrefs:Lru/cn/domain/LastChannel;

    invoke-virtual {v2}, Lru/cn/domain/LastChannel;->getPrevChannel()J

    move-result-wide v0

    .line 1325
    .local v0, "channelId":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 1326
    iget-object v2, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v2, v0, v1}, Lru/cn/tv/player/SimplePlayerFragment;->playChannel(J)V

    .line 1327
    const/4 v2, 0x1

    .line 1330
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/SimplePlayerFragment;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/StbPlayerController;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    return-object v0
.end method

.method static synthetic access$1000(Lru/cn/tv/stb/StbActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lru/cn/tv/stb/StbActivity;->showPaidDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1100(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideCategories()V

    return-void
.end method

.method static synthetic access$1200(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showCollections()V

    return-void
.end method

.method static synthetic access$1300(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideBilling()V

    return-void
.end method

.method static synthetic access$1400(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->pinAuthorizedForCreate()V

    return-void
.end method

.method static synthetic access$1500(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->pinAuthorizedForDisable()V

    return-void
.end method

.method static synthetic access$1600(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->pinAuthorizedForRenew()V

    return-void
.end method

.method static synthetic access$1700(Lru/cn/tv/stb/StbActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->channelsFragmentWrapper:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1800(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->channelsFragmentList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$1900(Lru/cn/tv/stb/StbActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/StbNextTelecastView;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->nextTelecastView:Lru/cn/tv/player/controller/StbNextTelecastView;

    return-object v0
.end method

.method static synthetic access$2000(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showVolumeControl()V

    return-void
.end method

.method static synthetic access$2100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/TracksAdapter;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->audioTrackInfoAdapter:Lru/cn/tv/player/controller/TracksAdapter;

    return-object v0
.end method

.method static synthetic access$2102(Lru/cn/tv/stb/StbActivity;Lru/cn/tv/player/controller/TracksAdapter;)Lru/cn/tv/player/controller/TracksAdapter;
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;
    .param p1, "x1"    # Lru/cn/tv/player/controller/TracksAdapter;

    .prologue
    .line 90
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity;->audioTrackInfoAdapter:Lru/cn/tv/player/controller/TracksAdapter;

    return-object p1
.end method

.method static synthetic access$2200(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->audioTrackInfoProvider:Lru/cn/player/ITrackSelector;

    return-object v0
.end method

.method static synthetic access$2202(Lru/cn/tv/stb/StbActivity;Lru/cn/player/ITrackSelector;)Lru/cn/player/ITrackSelector;
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;
    .param p1, "x1"    # Lru/cn/player/ITrackSelector;

    .prologue
    .line 90
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity;->audioTrackInfoProvider:Lru/cn/player/ITrackSelector;

    return-object p1
.end method

.method static synthetic access$2300(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/TracksAdapter;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->subtitleTrackInfoAdapter:Lru/cn/tv/player/controller/TracksAdapter;

    return-object v0
.end method

.method static synthetic access$2302(Lru/cn/tv/stb/StbActivity;Lru/cn/tv/player/controller/TracksAdapter;)Lru/cn/tv/player/controller/TracksAdapter;
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;
    .param p1, "x1"    # Lru/cn/tv/player/controller/TracksAdapter;

    .prologue
    .line 90
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity;->subtitleTrackInfoAdapter:Lru/cn/tv/player/controller/TracksAdapter;

    return-object p1
.end method

.method static synthetic access$2400(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->subtitlesTrackProvider:Lru/cn/player/ITrackSelector;

    return-object v0
.end method

.method static synthetic access$2402(Lru/cn/tv/stb/StbActivity;Lru/cn/player/ITrackSelector;)Lru/cn/player/ITrackSelector;
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;
    .param p1, "x1"    # Lru/cn/player/ITrackSelector;

    .prologue
    .line 90
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity;->subtitlesTrackProvider:Lru/cn/player/ITrackSelector;

    return-object p1
.end method

.method static synthetic access$2500(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideTrackWidget()V

    return-void
.end method

.method static synthetic access$2600(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showTrackWidget()V

    return-void
.end method

.method static synthetic access$2700(Lru/cn/tv/stb/StbActivity;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-boolean v0, p0, Lru/cn/tv/stb/StbActivity;->playingAdvertisement:Z

    return v0
.end method

.method static synthetic access$2702(Lru/cn/tv/stb/StbActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 90
    iput-boolean p1, p0, Lru/cn/tv/stb/StbActivity;->playingAdvertisement:Z

    return p1
.end method

.method static synthetic access$2800(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideChannels()V

    return-void
.end method

.method static synthetic access$2900(Lru/cn/tv/stb/StbActivity;)Lru/cn/domain/tv/CurrentCategory$Type;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/categories/CategoryFragment;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->categoryFragment:Lru/cn/tv/stb/categories/CategoryFragment;

    return-object v0
.end method

.method static synthetic access$3002(Lru/cn/tv/stb/StbActivity;I)I
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;
    .param p1, "x1"    # I

    .prologue
    .line 90
    iput p1, p0, Lru/cn/tv/stb/StbActivity;->currentPornoSubcategory:I

    return p1
.end method

.method static synthetic access$3100(Lru/cn/tv/stb/StbActivity;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lru/cn/tv/stb/StbActivity;->checkPinAuthorization(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3200(Lru/cn/tv/stb/StbActivity;)Lru/cn/domain/PinCode;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->pinCode:Lru/cn/domain/PinCode;

    return-object v0
.end method

.method static synthetic access$3300(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showPornoChannelsPanel()V

    return-void
.end method

.method static synthetic access$3400(Lru/cn/tv/stb/StbActivity;Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$PinCodeDialogCallbacks;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;
    .param p1, "x1"    # Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;
    .param p2, "x2"    # Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$PinCodeDialogCallbacks;

    .prologue
    .line 90
    invoke-direct {p0, p1, p2}, Lru/cn/tv/stb/StbActivity;->showPinCodeDialog(Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$PinCodeDialogCallbacks;)V

    return-void
.end method

.method static synthetic access$3500(Lru/cn/tv/stb/StbActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->trackListViewWrapper:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3600(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->trackListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$3700(Lru/cn/tv/stb/StbActivity;)Lru/cn/peersay/controllers/PlayContentController;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->remoteContentController:Lru/cn/peersay/controllers/PlayContentController;

    return-object v0
.end method

.method static synthetic access$3800(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/StbViewModel;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->stbViewModel:Lru/cn/tv/stb/StbViewModel;

    return-object v0
.end method

.method static synthetic access$3900(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/channels/ChannelsFragment;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->channelsFragment:Lru/cn/tv/stb/channels/ChannelsFragment;

    return-object v0
.end method

.method static synthetic access$400(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->expandCategories()V

    return-void
.end method

.method static synthetic access$4000(Lru/cn/tv/stb/StbActivity;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-boolean v0, p0, Lru/cn/tv/stb/StbActivity;->isPlaying:Z

    return v0
.end method

.method static synthetic access$4002(Lru/cn/tv/stb/StbActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 90
    iput-boolean p1, p0, Lru/cn/tv/stb/StbActivity;->isPlaying:Z

    return p1
.end method

.method static synthetic access$4100(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showPlayerController()V

    return-void
.end method

.method static synthetic access$4200(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector$TrackNameGenerator;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->getAudioTrackNameGenerator()Lru/cn/player/ITrackSelector$TrackNameGenerator;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4300(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector$TrackNameGenerator;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->getSubtitlesTrackNameGenerator()Lru/cn/player/ITrackSelector$TrackNameGenerator;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4400(Lru/cn/tv/stb/StbActivity;)Lru/cn/domain/LastChannel;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->lastChannelPrefs:Lru/cn/domain/LastChannel;

    return-object v0
.end method

.method static synthetic access$4502(Lru/cn/tv/stb/StbActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 90
    iput-boolean p1, p0, Lru/cn/tv/stb/StbActivity;->startDigitInput:Z

    return p1
.end method

.method static synthetic access$4600(Lru/cn/tv/stb/StbActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->channelNumber:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4700(Lru/cn/tv/stb/StbActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lru/cn/tv/stb/StbActivity;->playNextChannel(Z)V

    return-void
.end method

.method static synthetic access$4800(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->foldCategories()V

    return-void
.end method

.method static synthetic access$4900(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->requestCategoryFocus()V

    return-void
.end method

.method static synthetic access$500(Lru/cn/tv/stb/StbActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->collectionFragmentWrapper:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$5000(Lru/cn/tv/stb/StbActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->billingFragmentWrapper:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$5100(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showInforming()V

    return-void
.end method

.method static synthetic access$5200(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/informing/InformingFragment;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->informingFragment:Lru/cn/tv/stb/informing/InformingFragment;

    return-object v0
.end method

.method static synthetic access$5300(Lru/cn/tv/stb/StbActivity;Landroid/support/v4/app/Fragment;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;
    .param p1, "x1"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lru/cn/tv/stb/StbActivity;->hideFragment(Landroid/support/v4/app/Fragment;)V

    return-void
.end method

.method static synthetic access$5400(Lru/cn/tv/stb/StbActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->scheduleFragmentWrapper:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$5500(Lru/cn/tv/stb/StbActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->calendarFragmentWrapper:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$5600(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideCalendar()V

    return-void
.end method

.method static synthetic access$5700(Lru/cn/tv/stb/StbActivity;Lru/cn/api/provider/cursor/ChannelCursor;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;
    .param p1, "x1"    # Lru/cn/api/provider/cursor/ChannelCursor;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lru/cn/tv/stb/StbActivity;->openScheduleForChannel(Lru/cn/api/provider/cursor/ChannelCursor;)V

    return-void
.end method

.method static synthetic access$5800(Lru/cn/tv/stb/StbActivity;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-boolean v0, p0, Lru/cn/tv/stb/StbActivity;->kidsMode:Z

    return v0
.end method

.method static synthetic access$5900(Lru/cn/tv/stb/StbActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lru/cn/tv/stb/StbActivity;->showCategories(Z)V

    return-void
.end method

.method static synthetic access$600(Lru/cn/tv/stb/StbActivity;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-boolean v0, p0, Lru/cn/tv/stb/StbActivity;->channelsListTouched:Z

    return v0
.end method

.method static synthetic access$6000(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showChannels()V

    return-void
.end method

.method static synthetic access$602(Lru/cn/tv/stb/StbActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 90
    iput-boolean p1, p0, Lru/cn/tv/stb/StbActivity;->channelsListTouched:Z

    return p1
.end method

.method static synthetic access$6100(Lru/cn/tv/stb/StbActivity;JJ)V
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;
    .param p1, "x1"    # J
    .param p3, "x2"    # J

    .prologue
    .line 90
    invoke-direct {p0, p1, p2, p3, p4}, Lru/cn/tv/stb/StbActivity;->showBilling(JJ)V

    return-void
.end method

.method static synthetic access$6200(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/billing/BillingFragment;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->billingFragment:Lru/cn/tv/billing/BillingFragment;

    return-object v0
.end method

.method static synthetic access$6302(Lru/cn/tv/stb/StbActivity;Lru/cn/domain/tv/CurrentCategory$Type;)Lru/cn/domain/tv/CurrentCategory$Type;
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;
    .param p1, "x1"    # Lru/cn/domain/tv/CurrentCategory$Type;

    .prologue
    .line 90
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity;->loadChannelFrom:Lru/cn/domain/tv/CurrentCategory$Type;

    return-object p1
.end method

.method static synthetic access$6400(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->calendarFragmentList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$6500(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->scheduleFragmentList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$6600(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->disablePin()V

    return-void
.end method

.method static synthetic access$6700(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->changePin()V

    return-void
.end method

.method static synthetic access$6800(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->renewPin()V

    return-void
.end method

.method static synthetic access$6900(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showPornoChannels()V

    return-void
.end method

.method static synthetic access$700(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->delayHidingCategories()V

    return-void
.end method

.method static synthetic access$7000(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/settings/SettingFragment;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->settingFragment:Lru/cn/tv/stb/settings/SettingFragment;

    return-object v0
.end method

.method static synthetic access$7100(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->settingFragmentList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$7202(Lru/cn/tv/stb/StbActivity;I)I
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;
    .param p1, "x1"    # I

    .prologue
    .line 90
    iput p1, p0, Lru/cn/tv/stb/StbActivity;->currentSettingSubcategory:I

    return p1
.end method

.method static synthetic access$7300(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideSetting()V

    return-void
.end method

.method static synthetic access$7400(Lru/cn/tv/stb/StbActivity;J)V
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;
    .param p1, "x1"    # J

    .prologue
    .line 90
    invoke-direct {p0, p1, p2}, Lru/cn/tv/stb/StbActivity;->unblockChannel(J)V

    return-void
.end method

.method static synthetic access$7500(Lru/cn/tv/stb/StbActivity;J)V
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;
    .param p1, "x1"    # J

    .prologue
    .line 90
    invoke-direct {p0, p1, p2}, Lru/cn/tv/stb/StbActivity;->blockChannel(J)V

    return-void
.end method

.method static synthetic access$800(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/calendar/CalendarFragment;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->calendarFragment:Lru/cn/tv/stb/calendar/CalendarFragment;

    return-object v0
.end method

.method static synthetic access$900(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/schedule/ScheduleFragment;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->scheduleFragment:Lru/cn/tv/stb/schedule/ScheduleFragment;

    return-object v0
.end method

.method private blockChannel(J)V
    .locals 3
    .param p1, "channelId"    # J

    .prologue
    .line 2555
    new-instance v0, Lru/cn/tv/stb/parental/PasswordDialog;

    new-instance v1, Lru/cn/tv/stb/StbActivity$39;

    invoke-direct {v1, p0, p1, p2}, Lru/cn/tv/stb/StbActivity$39;-><init>(Lru/cn/tv/stb/StbActivity;J)V

    invoke-direct {v0, p0, v1}, Lru/cn/tv/stb/parental/PasswordDialog;-><init>(Landroid/content/Context;Lru/cn/tv/stb/parental/PasswordDialogPresenter$PasswordDialogListener;)V

    .line 2571
    invoke-virtual {v0}, Lru/cn/tv/stb/parental/PasswordDialog;->show()Landroid/support/v7/app/AlertDialog;

    .line 2572
    return-void
.end method

.method private changePin()V
    .locals 2

    .prologue
    .line 2454
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->pinCode:Lru/cn/domain/PinCode;

    new-instance v1, Lru/cn/tv/stb/StbActivity$36;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/StbActivity$36;-><init>(Lru/cn/tv/stb/StbActivity;)V

    invoke-virtual {v0, v1}, Lru/cn/domain/PinCode;->getPinCode(Lru/cn/domain/PinCode$PinCodeCheckCallbacks;)V

    .line 2464
    return-void
.end method

.method private checkPinAuthorization(Ljava/lang/String;)Z
    .locals 4
    .param p1, "callback"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1079
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->categoryFragment:Lru/cn/tv/stb/categories/CategoryFragment;

    invoke-virtual {v1}, Lru/cn/tv/stb/categories/CategoryFragment;->hasPrivateOffice()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1080
    new-instance v1, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0e00ee

    .line 1081
    invoke-virtual {v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    .line 1082
    invoke-virtual {v1, v0}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e0034

    new-instance v3, Lru/cn/tv/stb/StbActivity$16;

    invoke-direct {v3, p0}, Lru/cn/tv/stb/StbActivity$16;-><init>(Lru/cn/tv/stb/StbActivity;)V

    .line 1083
    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    .line 1090
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 1098
    :goto_0
    return v0

    .line 1095
    :cond_0
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->categoryFragment:Lru/cn/tv/stb/categories/CategoryFragment;

    invoke-virtual {v0}, Lru/cn/tv/stb/categories/CategoryFragment;->getContractorId()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1}, Lru/cn/tv/stb/StbActivity;->showBillingForPin(JLjava/lang/String;)V

    .line 1096
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->billingFragment:Lru/cn/tv/billing/BillingFragment;

    invoke-virtual {v0}, Lru/cn/tv/billing/BillingFragment;->requestFocus()Z

    .line 1098
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private delayHidingCategories()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2247
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2248
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2250
    :cond_0
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    const-wide/32 v2, 0x9c40

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2251
    return-void
.end method

.method private detachPeerSayControllers()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1980
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->remoteContentController:Lru/cn/peersay/controllers/PlayContentController;

    if-eqz v0, :cond_0

    .line 1981
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->remoteContentController:Lru/cn/peersay/controllers/PlayContentController;

    invoke-static {v0}, Lru/cn/peersay/RemoteCommandReceiver;->detachController(Lru/cn/peersay/controllers/RemoteIntentController;)V

    .line 1982
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->remoteContentController:Lru/cn/peersay/controllers/PlayContentController;

    invoke-virtual {v0, v1}, Lru/cn/peersay/controllers/PlayContentController;->setListener(Lru/cn/peersay/controllers/PlayContentController$Listener;)V

    .line 1983
    iput-object v1, p0, Lru/cn/tv/stb/StbActivity;->remoteContentController:Lru/cn/peersay/controllers/PlayContentController;

    .line 1986
    :cond_0
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->remoteController:Lru/cn/peersay/controllers/PlayerRemoteController;

    if-eqz v0, :cond_1

    .line 1987
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->remoteController:Lru/cn/peersay/controllers/PlayerRemoteController;

    invoke-static {v0}, Lru/cn/peersay/RemoteCommandReceiver;->detachController(Lru/cn/peersay/controllers/RemoteIntentController;)V

    .line 1988
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->remoteController:Lru/cn/peersay/controllers/PlayerRemoteController;

    invoke-virtual {v0, v1}, Lru/cn/peersay/controllers/PlayerRemoteController;->setPlayer(Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;)V

    .line 1989
    iput-object v1, p0, Lru/cn/tv/stb/StbActivity;->remoteController:Lru/cn/peersay/controllers/PlayerRemoteController;

    .line 1992
    :cond_1
    invoke-static {}, Lru/cn/peersay/controllers/DialogsRemoteController;->sharedInstance()Lru/cn/peersay/controllers/DialogsRemoteController;

    move-result-object v0

    invoke-static {v0}, Lru/cn/peersay/RemoteCommandReceiver;->detachController(Lru/cn/peersay/controllers/RemoteIntentController;)V

    .line 1993
    return-void
.end method

.method private disablePin()V
    .locals 2

    .prologue
    .line 2476
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->pinCode:Lru/cn/domain/PinCode;

    new-instance v1, Lru/cn/tv/stb/StbActivity$37;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/StbActivity$37;-><init>(Lru/cn/tv/stb/StbActivity;)V

    invoke-virtual {v0, v1}, Lru/cn/domain/PinCode;->getPinCode(Lru/cn/domain/PinCode$PinCodeCheckCallbacks;)V

    .line 2507
    return-void
.end method

.method private dispatchDigitKeyEvent(Landroid/view/KeyEvent;)V
    .locals 10
    .param p1, "e"    # Landroid/view/KeyEvent;

    .prologue
    const-wide/16 v8, 0x7d0

    const/4 v6, 0x5

    const/4 v5, 0x3

    .line 2058
    iget-boolean v3, p0, Lru/cn/tv/stb/StbActivity;->startDigitInput:Z

    if-nez v3, :cond_0

    .line 2059
    iget-object v3, p0, Lru/cn/tv/stb/StbActivity;->channelNumber:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2060
    const/4 v3, 0x1

    iput-boolean v3, p0, Lru/cn/tv/stb/StbActivity;->startDigitInput:Z

    .line 2063
    :cond_0
    sget-object v3, Lru/cn/domain/tv/CurrentCategory$Type;->all:Lru/cn/domain/tv/CurrentCategory$Type;

    iput-object v3, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    .line 2064
    sget-object v3, Lru/cn/domain/tv/CurrentCategory$Type;->all:Lru/cn/domain/tv/CurrentCategory$Type;

    iput-object v3, p0, Lru/cn/tv/stb/StbActivity;->loadChannelFrom:Lru/cn/domain/tv/CurrentCategory$Type;

    .line 2065
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->setViewMode()V

    .line 2067
    iget-object v3, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v3}, Lru/cn/tv/player/controller/StbPlayerController;->hide()V

    .line 2068
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideCategories()V

    .line 2069
    iget-object v3, p0, Lru/cn/tv/stb/StbActivity;->channelNumber:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2070
    .local v2, "text":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getNumber()C

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2072
    iget-object v3, p0, Lru/cn/tv/stb/StbActivity;->channelsFragmentList:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getCount()I

    move-result v1

    .line 2073
    .local v1, "countOfChannels":I
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 2074
    .local v0, "channel":I
    if-le v0, v1, :cond_1

    .line 2075
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getNumber()C

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    .line 2076
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 2079
    :cond_1
    iget-object v3, p0, Lru/cn/tv/stb/StbActivity;->channelNumber:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2080
    iget-object v3, p0, Lru/cn/tv/stb/StbActivity;->channelNumber:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2082
    iget-object v3, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v3, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 2083
    iget-object v3, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v3, v5, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2086
    iget-object v3, p0, Lru/cn/tv/stb/StbActivity;->channelNumber:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    if-ne v0, v3, :cond_2

    .line 2087
    iget-object v3, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 2088
    iget-object v3, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v3, v6, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2090
    :cond_2
    return-void
.end method

.method private expandCategories()V
    .locals 2

    .prologue
    .line 2176
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->scheduleFragmentWrapper:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2177
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideCalendar()V

    .line 2180
    :cond_0
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->categoryFragment:Lru/cn/tv/stb/categories/CategoryFragment;

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    invoke-virtual {v0, v1}, Lru/cn/tv/stb/categories/CategoryFragment;->categoryReload(Lru/cn/domain/tv/CurrentCategory$Type;)V

    .line 2181
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->categoryFragment:Lru/cn/tv/stb/categories/CategoryFragment;

    invoke-virtual {v0}, Lru/cn/tv/stb/categories/CategoryFragment;->expand()V

    .line 2182
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->categoryFragment:Lru/cn/tv/stb/categories/CategoryFragment;

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    invoke-virtual {v0, v1}, Lru/cn/tv/stb/categories/CategoryFragment;->setSelection(Lru/cn/domain/tv/CurrentCategory$Type;)V

    .line 2184
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->collectionFragmentWrapper:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 2185
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lru/cn/tv/stb/StbActivity;->showCategories(Z)V

    .line 2186
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lru/cn/tv/stb/StbActivity$32;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/StbActivity$32;-><init>(Lru/cn/tv/stb/StbActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2194
    :cond_1
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->categoryFragmentList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    .line 2195
    return-void
.end method

.method private foldCategories()V
    .locals 2

    .prologue
    .line 2198
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->categoryFragment:Lru/cn/tv/stb/categories/CategoryFragment;

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    invoke-virtual {v0, v1}, Lru/cn/tv/stb/categories/CategoryFragment;->categoryReload(Lru/cn/domain/tv/CurrentCategory$Type;)V

    .line 2199
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->categoryFragment:Lru/cn/tv/stb/categories/CategoryFragment;

    invoke-virtual {v0}, Lru/cn/tv/stb/categories/CategoryFragment;->fold()V

    .line 2200
    return-void
.end method

.method private getAudioTrackNameGenerator()Lru/cn/player/ITrackSelector$TrackNameGenerator;
    .locals 1

    .prologue
    .line 1568
    new-instance v0, Lru/cn/tv/stb/StbActivity$21;

    invoke-direct {v0, p0}, Lru/cn/tv/stb/StbActivity$21;-><init>(Lru/cn/tv/stb/StbActivity;)V

    return-object v0
.end method

.method private getSubtitlesTrackNameGenerator()Lru/cn/player/ITrackSelector$TrackNameGenerator;
    .locals 1

    .prologue
    .line 1547
    new-instance v0, Lru/cn/tv/stb/StbActivity$20;

    invoke-direct {v0, p0}, Lru/cn/tv/stb/StbActivity$20;-><init>(Lru/cn/tv/stb/StbActivity;)V

    return-object v0
.end method

.method private handleIntent(Landroid/content/Intent;)Z
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v8, 0xb

    const/4 v4, 0x0

    const-wide/16 v6, 0x0

    .line 2000
    const-string v5, "telecastId"

    invoke-virtual {p1, v5, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 2002
    .local v2, "telecastId":J
    const-string v5, "cnId"

    invoke-virtual {p1, v5, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2003
    .local v0, "channelId":J
    const-string v5, "channelId"

    invoke-virtual {p1, v5, v0, v1}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2005
    cmp-long v5, v0, v6

    if-nez v5, :cond_0

    cmp-long v5, v2, v6

    if-nez v5, :cond_0

    .line 2033
    :goto_0
    return v4

    .line 2019
    :cond_0
    cmp-long v5, v2, v6

    if-lez v5, :cond_2

    .line 2020
    invoke-static {v8, v4}, Lru/cn/domain/statistics/inetra/InetraTracker;->setSessionParams(II)V

    .line 2024
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v4, v2, v3}, Lru/cn/tv/player/SimplePlayerFragment;->playTelecast(J)V

    .line 2033
    :cond_1
    :goto_1
    const/4 v4, 0x1

    goto :goto_0

    .line 2025
    :cond_2
    cmp-long v5, v0, v6

    if-eqz v5, :cond_1

    .line 2026
    invoke-static {v8, v4}, Lru/cn/domain/statistics/inetra/InetraTracker;->setSessionParams(II)V

    .line 2030
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v4, v0, v1}, Lru/cn/tv/player/SimplePlayerFragment;->playChannel(J)V

    goto :goto_1
.end method

.method private hideBilling()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 1001
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->billingFragmentWrapper:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 1007
    :goto_0
    return-void

    .line 1004
    :cond_0
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->billingFragment:Lru/cn/tv/billing/BillingFragment;

    invoke-virtual {v0}, Lru/cn/tv/billing/BillingFragment;->stopLoading()V

    .line 1005
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->billingFragment:Lru/cn/tv/billing/BillingFragment;

    const-string v1, "about:blank"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lru/cn/tv/billing/BillingFragment;->load(Ljava/lang/String;Z)V

    .line 1006
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->billingFragmentWrapper:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private hideBlockingHint()V
    .locals 2

    .prologue
    .line 2549
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->bottomSheetBehavior:Landroid/support/design/widget/BottomSheetBehavior;

    if-eqz v0, :cond_0

    .line 2550
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->bottomSheetBehavior:Landroid/support/design/widget/BottomSheetBehavior;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomSheetBehavior;->setState(I)V

    .line 2552
    :cond_0
    return-void
.end method

.method private hideCalendar()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 969
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->calendarFragmentWrapper:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 970
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->scheduleFragmentWrapper:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 971
    return-void
.end method

.method private hideCategories()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1143
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideCurrentCategory()V

    .line 1145
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->categoryFragmentWrapper:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1147
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1148
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1150
    :cond_0
    return-void
.end method

.method private hideChannels()V
    .locals 3

    .prologue
    .line 1111
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideCalendar()V

    .line 1112
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->billing:Lru/cn/domain/tv/CurrentCategory$Type;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->billingFragmentWrapper:Landroid/view/View;

    .line 1113
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1114
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideBilling()V

    .line 1117
    :cond_0
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->channelsFragmentList:Landroid/widget/ListView;

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->channelsFragmentList:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 1118
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->channelsFragmentWrapper:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1120
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->informingFragment:Lru/cn/tv/stb/informing/InformingFragment;

    invoke-direct {p0, v0}, Lru/cn/tv/stb/StbActivity;->hideFragment(Landroid/support/v4/app/Fragment;)V

    .line 1121
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideBlockingHint()V

    .line 1122
    return-void
.end method

.method private hideCurrentCategory()V
    .locals 2

    .prologue
    .line 1240
    sget-object v0, Lru/cn/tv/stb/StbActivity$41;->$SwitchMap$ru$cn$domain$tv$CurrentCategory$Type:[I

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    invoke-virtual {v1}, Lru/cn/domain/tv/CurrentCategory$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1276
    :cond_0
    :goto_0
    return-void

    .line 1245
    :pswitch_0
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideChannels()V

    goto :goto_0

    .line 1249
    :pswitch_1
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->pinCode:Lru/cn/domain/PinCode;

    invoke-virtual {v0}, Lru/cn/domain/PinCode;->getStatus()Lru/cn/domain/PinCode$Status;

    move-result-object v0

    sget-object v1, Lru/cn/domain/PinCode$Status;->temporarilyDisable:Lru/cn/domain/PinCode$Status;

    if-ne v0, v1, :cond_1

    .line 1250
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->pinCode:Lru/cn/domain/PinCode;

    sget-object v1, Lru/cn/domain/PinCode$Status;->require:Lru/cn/domain/PinCode$Status;

    invoke-virtual {v0, v1}, Lru/cn/domain/PinCode;->setStatus(Lru/cn/domain/PinCode$Status;)V

    .line 1252
    :cond_1
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideChannels()V

    .line 1253
    iget v0, p0, Lru/cn/tv/stb/StbActivity;->currentPornoSubcategory:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1254
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideBilling()V

    goto :goto_0

    .line 1260
    :pswitch_2
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->collectionsFragment:Lru/cn/tv/stb/collections/CollectionsFragment;

    invoke-virtual {v0}, Lru/cn/tv/stb/collections/CollectionsFragment;->scrollToStartPosition()V

    .line 1261
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->collectionFragmentWrapper:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1265
    :pswitch_3
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideBilling()V

    goto :goto_0

    .line 1269
    :pswitch_4
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideSetting()V

    goto :goto_0

    .line 1240
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private hideFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 962
    invoke-virtual {p0}, Lru/cn/tv/stb/StbActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 963
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 964
    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 965
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 966
    return-void
.end method

.method private hideSetting()V
    .locals 2

    .prologue
    .line 1139
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->settingsFragmentWrapper:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1140
    return-void
.end method

.method private hideTrackWidget()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 1317
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1318
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1320
    :cond_0
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->trackListViewWrapper:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1321
    return-void
.end method

.method private hideVolumeControl()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 1297
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->volumeControl:Lru/cn/view/VolumeControl;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lru/cn/view/VolumeControl;->setVisibility(I)V

    .line 1298
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1299
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1301
    :cond_0
    return-void
.end method

.method private openScheduleForChannel(Lru/cn/api/provider/cursor/ChannelCursor;)V
    .locals 18
    .param p1, "c"    # Lru/cn/api/provider/cursor/ChannelCursor;

    .prologue
    .line 2203
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lru/cn/api/provider/cursor/ChannelCursor;->getCount()I

    move-result v2

    if-nez v2, :cond_1

    .line 2244
    :cond_0
    :goto_0
    return-void

    .line 2206
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lru/cn/api/provider/cursor/ChannelCursor;->getChannelId()J

    move-result-wide v3

    .line 2207
    .local v3, "cnId":J
    invoke-virtual/range {p1 .. p1}, Lru/cn/api/provider/cursor/ChannelCursor;->getHasSchedule()I

    move-result v2

    const/4 v11, 0x1

    if-ne v2, v11, :cond_3

    const/4 v10, 0x1

    .line 2208
    .local v10, "hasSchedule":Z
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lru/cn/api/provider/cursor/ChannelCursor;->getIsDenied()Z

    move-result v5

    .line 2209
    .local v5, "isDenied":Z
    invoke-virtual/range {p1 .. p1}, Lru/cn/api/provider/cursor/ChannelCursor;->getContractorId()J

    move-result-wide v6

    .line 2210
    .local v6, "contractorId":J
    invoke-virtual/range {p1 .. p1}, Lru/cn/api/provider/cursor/ChannelCursor;->allowPurchase()Z

    move-result v8

    .line 2212
    .local v8, "allowPurchase":Z
    const-wide/16 v16, 0x0

    cmp-long v2, v3, v16

    if-lez v2, :cond_0

    .line 2213
    if-eqz v10, :cond_5

    if-nez v5, :cond_5

    .line 2214
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/tv/stb/StbActivity;->channelsFragmentList:Landroid/widget/ListView;

    invoke-virtual/range {p1 .. p1}, Lru/cn/api/provider/cursor/ChannelCursor;->getPosition()I

    move-result v11

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v2, v11, v0}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 2216
    const/4 v9, 0x0

    .line 2217
    .local v9, "date":Ljava/util/Calendar;
    const-wide/16 v12, 0x0

    .line 2219
    .local v12, "telecastId":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v2}, Lru/cn/tv/player/SimplePlayerFragment;->getChannelId()J

    move-result-wide v16

    cmp-long v2, v3, v16

    if-nez v2, :cond_4

    .line 2220
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v2}, Lru/cn/tv/player/SimplePlayerFragment;->getLocationTerritoryId()J

    move-result-wide v14

    .line 2221
    .local v14, "territoryId":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v2}, Lru/cn/tv/player/SimplePlayerFragment;->getCurrentTelecastId()J

    move-result-wide v12

    .line 2222
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v2}, Lru/cn/tv/player/SimplePlayerFragment;->getCurrentTelecastDate()Ljava/util/Calendar;

    move-result-object v9

    .line 2227
    :goto_2
    if-nez v9, :cond_2

    .line 2228
    invoke-static {}, Lru/cn/utils/Utils;->getCalendar()Ljava/util/Calendar;

    move-result-object v9

    .line 2231
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/tv/stb/StbActivity;->calendarFragment:Lru/cn/tv/stb/calendar/CalendarFragment;

    invoke-virtual {v2, v3, v4}, Lru/cn/tv/stb/calendar/CalendarFragment;->setChannelId(J)V

    .line 2232
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/tv/stb/StbActivity;->calendarFragment:Lru/cn/tv/stb/calendar/CalendarFragment;

    invoke-virtual {v2, v9}, Lru/cn/tv/stb/calendar/CalendarFragment;->selectDate(Ljava/util/Calendar;)V

    .line 2233
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/tv/stb/StbActivity;->calendarFragmentWrapper:Landroid/view/View;

    const/4 v11, 0x0

    invoke-virtual {v2, v11}, Landroid/view/View;->setVisibility(I)V

    .line 2235
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/tv/stb/StbActivity;->scheduleFragment:Lru/cn/tv/stb/schedule/ScheduleFragment;

    invoke-virtual {v2, v3, v4, v14, v15}, Lru/cn/tv/stb/schedule/ScheduleFragment;->setChannelId(JJ)V

    .line 2236
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/tv/stb/StbActivity;->scheduleFragment:Lru/cn/tv/stb/schedule/ScheduleFragment;

    invoke-virtual {v2, v9}, Lru/cn/tv/stb/schedule/ScheduleFragment;->setDate(Ljava/util/Calendar;)V

    .line 2237
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/tv/stb/StbActivity;->scheduleFragment:Lru/cn/tv/stb/schedule/ScheduleFragment;

    invoke-virtual {v2, v12, v13}, Lru/cn/tv/stb/schedule/ScheduleFragment;->setCurrentTelecast(J)V

    .line 2238
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/tv/stb/StbActivity;->scheduleFragmentWrapper:Landroid/view/View;

    const/4 v11, 0x0

    invoke-virtual {v2, v11}, Landroid/view/View;->setVisibility(I)V

    .line 2239
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/tv/stb/StbActivity;->scheduleFragment:Lru/cn/tv/stb/schedule/ScheduleFragment;

    const/4 v11, 0x0

    invoke-virtual {v2, v11}, Lru/cn/tv/stb/schedule/ScheduleFragment;->requestFocus(I)V

    goto/16 :goto_0

    .line 2207
    .end local v5    # "isDenied":Z
    .end local v6    # "contractorId":J
    .end local v8    # "allowPurchase":Z
    .end local v9    # "date":Ljava/util/Calendar;
    .end local v10    # "hasSchedule":Z
    .end local v12    # "telecastId":J
    .end local v14    # "territoryId":J
    :cond_3
    const/4 v10, 0x0

    goto/16 :goto_1

    .line 2224
    .restart local v5    # "isDenied":Z
    .restart local v6    # "contractorId":J
    .restart local v8    # "allowPurchase":Z
    .restart local v9    # "date":Ljava/util/Calendar;
    .restart local v10    # "hasSchedule":Z
    .restart local v12    # "telecastId":J
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lru/cn/api/provider/cursor/ChannelCursor;->getTerritoryId()J

    move-result-wide v14

    .restart local v14    # "territoryId":J
    goto :goto_2

    .line 2240
    .end local v9    # "date":Ljava/util/Calendar;
    .end local v12    # "telecastId":J
    .end local v14    # "territoryId":J
    :cond_5
    if-eqz v5, :cond_0

    .line 2241
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/tv/stb/StbActivity;->channelsFragmentListener:Lru/cn/tv/stb/channels/ChannelsFragment$ChannelsFragmentListener;

    invoke-interface/range {v2 .. v8}, Lru/cn/tv/stb/channels/ChannelsFragment$ChannelsFragmentListener;->onChannelSelected(JZJZ)V

    goto/16 :goto_0
.end method

.method private pinAuthorizedForCreate()V
    .locals 2

    .prologue
    .line 2367
    const/4 v0, 0x0

    iput v0, p0, Lru/cn/tv/stb/StbActivity;->currentPornoSubcategory:I

    .line 2369
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->pinCode:Lru/cn/domain/PinCode;

    new-instance v1, Lru/cn/tv/stb/StbActivity$33;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/StbActivity$33;-><init>(Lru/cn/tv/stb/StbActivity;)V

    invoke-virtual {v0, v1}, Lru/cn/domain/PinCode;->createPin(Lru/cn/domain/PinCode$PinOperationCallbacks;)V

    .line 2402
    return-void
.end method

.method private pinAuthorizedForDisable()V
    .locals 2

    .prologue
    .line 2405
    const/4 v0, 0x2

    iput v0, p0, Lru/cn/tv/stb/StbActivity;->currentSettingSubcategory:I

    .line 2406
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showSettings()V

    .line 2408
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->pinCode:Lru/cn/domain/PinCode;

    new-instance v1, Lru/cn/tv/stb/StbActivity$34;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/StbActivity$34;-><init>(Lru/cn/tv/stb/StbActivity;)V

    invoke-virtual {v0, v1}, Lru/cn/domain/PinCode;->disablePin(Lru/cn/domain/PinCode$PinOperationCallbacks;)V

    .line 2422
    return-void
.end method

.method private pinAuthorizedForRenew()V
    .locals 2

    .prologue
    .line 2425
    const/4 v0, 0x2

    iput v0, p0, Lru/cn/tv/stb/StbActivity;->currentSettingSubcategory:I

    .line 2426
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showSettings()V

    .line 2428
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->pinCode:Lru/cn/domain/PinCode;

    new-instance v1, Lru/cn/tv/stb/StbActivity$35;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/StbActivity$35;-><init>(Lru/cn/tv/stb/StbActivity;)V

    invoke-virtual {v0, v1}, Lru/cn/domain/PinCode;->createPin(Lru/cn/domain/PinCode$PinOperationCallbacks;)V

    .line 2451
    return-void
.end method

.method private playLastChannel()V
    .locals 3

    .prologue
    .line 2052
    invoke-virtual {p0}, Lru/cn/tv/stb/StbActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 2053
    return-void
.end method

.method private playNextChannel(Z)V
    .locals 3
    .param p1, "isFavorite"    # Z

    .prologue
    .line 2037
    iget-object v2, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v2}, Lru/cn/tv/player/SimplePlayerFragment;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2038
    iget-object v2, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v2}, Lru/cn/tv/player/SimplePlayerFragment;->stop()V

    .line 2041
    :cond_0
    invoke-virtual {p0}, Lru/cn/tv/stb/StbActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    .line 2042
    .local v1, "lm":Landroid/support/v4/app/LoaderManager;
    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 2043
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 2044
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 2046
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2047
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "favourite"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2048
    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 2049
    return-void
.end method

.method private renewPin()V
    .locals 2

    .prologue
    .line 2467
    const/4 v0, 0x1

    iput v0, p0, Lru/cn/tv/stb/StbActivity;->currentSettingSubcategory:I

    .line 2468
    const-string v0, "StbActivity"

    const-string v1, "Pin code not exists"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2470
    const-string v0, "http://closerenewpinfromsetting/"

    invoke-direct {p0, v0}, Lru/cn/tv/stb/StbActivity;->checkPinAuthorization(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2471
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideSetting()V

    .line 2473
    :cond_0
    return-void
.end method

.method private requestCategoryFocus()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1194
    sget-object v0, Lru/cn/tv/stb/StbActivity$41;->$SwitchMap$ru$cn$domain$tv$CurrentCategory$Type:[I

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    invoke-virtual {v1}, Lru/cn/domain/tv/CurrentCategory$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1237
    :cond_0
    :goto_0
    return-void

    .line 1199
    :pswitch_0
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->channelsFragmentList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    .line 1200
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showInforming()V

    goto :goto_0

    .line 1204
    :pswitch_1
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->collectionsFragment:Lru/cn/tv/stb/collections/CollectionsFragment;

    invoke-virtual {v0}, Lru/cn/tv/stb/collections/CollectionsFragment;->requestFocus()Z

    goto :goto_0

    .line 1208
    :pswitch_2
    iget v0, p0, Lru/cn/tv/stb/StbActivity;->currentPornoSubcategory:I

    if-nez v0, :cond_1

    .line 1209
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showPornoChannels()V

    goto :goto_0

    .line 1210
    :cond_1
    iget v0, p0, Lru/cn/tv/stb/StbActivity;->currentPornoSubcategory:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 1211
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->channelsFragmentList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    goto :goto_0

    .line 1212
    :cond_2
    iget v0, p0, Lru/cn/tv/stb/StbActivity;->currentPornoSubcategory:I

    if-ne v0, v2, :cond_0

    .line 1213
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->billingFragmentWrapper:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1214
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->billingFragment:Lru/cn/tv/billing/BillingFragment;

    invoke-virtual {v0}, Lru/cn/tv/billing/BillingFragment;->requestFocus()Z

    goto :goto_0

    .line 1217
    :cond_3
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showPornoChannels()V

    goto :goto_0

    .line 1223
    :pswitch_3
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->billingFragment:Lru/cn/tv/billing/BillingFragment;

    invoke-virtual {v0}, Lru/cn/tv/billing/BillingFragment;->requestFocus()Z

    goto :goto_0

    .line 1227
    :pswitch_4
    iget v0, p0, Lru/cn/tv/stb/StbActivity;->currentSettingSubcategory:I

    if-ne v0, v2, :cond_4

    .line 1228
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->billingFragment:Lru/cn/tv/billing/BillingFragment;

    invoke-virtual {v0}, Lru/cn/tv/billing/BillingFragment;->requestFocus()Z

    goto :goto_0

    .line 1230
    :cond_4
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->settingFragmentList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    goto :goto_0

    .line 1194
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private setCategory(Lru/cn/domain/tv/CurrentCategory$Type;)V
    .locals 1
    .param p1, "category"    # Lru/cn/domain/tv/CurrentCategory$Type;

    .prologue
    .line 1153
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    if-ne p1, v0, :cond_0

    .line 1159
    :goto_0
    return-void

    .line 1156
    :cond_0
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideCurrentCategory()V

    .line 1157
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    .line 1158
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showCurrentCategory()V

    goto :goto_0
.end method

.method private setRelatedRubric(Lru/cn/api/catalogue/replies/Rubric;Landroid/database/Cursor;)V
    .locals 4
    .param p1, "relatedRubric"    # Lru/cn/api/catalogue/replies/Rubric;
    .param p2, "elements"    # Landroid/database/Cursor;

    .prologue
    .line 942
    const/4 v2, 0x0

    .line 943
    .local v2, "title":Ljava/lang/String;
    const-wide/16 v0, 0x0

    .line 944
    .local v0, "rubricId":J
    if-eqz p1, :cond_0

    .line 945
    iget-object v2, p1, Lru/cn/api/catalogue/replies/Rubric;->title:Ljava/lang/String;

    .line 946
    iget-wide v0, p1, Lru/cn/api/catalogue/replies/Rubric;->id:J

    .line 949
    :cond_0
    iget-object v3, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v3, v0, v1, v2, p2}, Lru/cn/tv/player/controller/StbPlayerController;->setRelatedItems(JLjava/lang/String;Landroid/database/Cursor;)V

    .line 950
    return-void
.end method

.method private setViewMode()V
    .locals 2

    .prologue
    .line 2112
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    if-nez v1, :cond_0

    .line 2113
    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->all:Lru/cn/domain/tv/CurrentCategory$Type;

    iput-object v1, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    .line 2116
    :cond_0
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    invoke-direct {p0, v1}, Lru/cn/tv/stb/StbActivity;->viewModeByCategory(Lru/cn/domain/tv/CurrentCategory$Type;)I

    move-result v0

    .line 2117
    .local v0, "viewMode":I
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->channelsFragment:Lru/cn/tv/stb/channels/ChannelsFragment;

    invoke-virtual {v1, v0}, Lru/cn/tv/stb/channels/ChannelsFragment;->setViewMode(I)V

    .line 2118
    return-void
.end method

.method private showBilling()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 992
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->billingFragmentWrapper:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 993
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->billingFragment:Lru/cn/tv/billing/BillingFragment;

    invoke-virtual {v0}, Lru/cn/tv/billing/BillingFragment;->stopLoading()V

    .line 995
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 996
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 998
    :cond_0
    return-void
.end method

.method private showBilling(J)V
    .locals 5
    .param p1, "contractorId"    # J

    .prologue
    .line 974
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->billingFragment:Lru/cn/tv/billing/BillingFragment;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, p1, p2, v2, v3}, Lru/cn/tv/billing/BillingFragment;->purchaseOptions(JJ)V

    .line 976
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showBilling()V

    .line 977
    return-void
.end method

.method private showBilling(JJ)V
    .locals 1
    .param p1, "contractorId"    # J
    .param p3, "channelId"    # J

    .prologue
    .line 980
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->billingFragment:Lru/cn/tv/billing/BillingFragment;

    invoke-virtual {v0, p1, p2, p3, p4}, Lru/cn/tv/billing/BillingFragment;->purchaseOptions(JJ)V

    .line 982
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showBilling()V

    .line 983
    return-void
.end method

.method private showBillingForPin(JLjava/lang/String;)V
    .locals 1
    .param p1, "contractorId"    # J
    .param p3, "callback"    # Ljava/lang/String;

    .prologue
    .line 986
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->billingFragment:Lru/cn/tv/billing/BillingFragment;

    invoke-virtual {v0, p1, p2, p3}, Lru/cn/tv/billing/BillingFragment;->pinAuthorize(JLjava/lang/String;)V

    .line 988
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showBilling()V

    .line 989
    return-void
.end method

.method private showBlockingHint()V
    .locals 2

    .prologue
    .line 2539
    iget-boolean v0, p0, Lru/cn/tv/stb/StbActivity;->kidsMode:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->all:Lru/cn/domain/tv/CurrentCategory$Type;

    if-ne v0, v1, :cond_1

    .line 2540
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->bottomSheetBehavior:Landroid/support/design/widget/BottomSheetBehavior;

    if-nez v0, :cond_0

    .line 2541
    const v0, 0x7f090041

    invoke-virtual {p0, v0}, Lru/cn/tv/stb/StbActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/design/widget/BottomSheetBehavior;->from(Landroid/view/View;)Landroid/support/design/widget/BottomSheetBehavior;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/stb/StbActivity;->bottomSheetBehavior:Landroid/support/design/widget/BottomSheetBehavior;

    .line 2544
    :cond_0
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->bottomSheetBehavior:Landroid/support/design/widget/BottomSheetBehavior;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomSheetBehavior;->setState(I)V

    .line 2546
    :cond_1
    return-void
.end method

.method private showCategories(Z)V
    .locals 2
    .param p1, "checkCurrentItem"    # Z

    .prologue
    .line 1125
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideVolumeControl()V

    .line 1127
    if-eqz p1, :cond_0

    .line 1128
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->categoryFragment:Lru/cn/tv/stb/categories/CategoryFragment;

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    invoke-virtual {v0, v1}, Lru/cn/tv/stb/categories/CategoryFragment;->setSelection(Lru/cn/domain/tv/CurrentCategory$Type;)V

    .line 1130
    :cond_0
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->categoryFragmentWrapper:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1131
    return-void
.end method

.method private showChannels()V
    .locals 4

    .prologue
    .line 1010
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->channelsFragment:Lru/cn/tv/stb/channels/ChannelsFragment;

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v1}, Lru/cn/tv/player/SimplePlayerFragment;->getChannelId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lru/cn/tv/stb/channels/ChannelsFragment;->setCurrentChannel(J)V

    .line 1011
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->channelsFragmentWrapper:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1013
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showInforming()V

    .line 1015
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->delayHidingCategories()V

    .line 1016
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showBlockingHint()V

    .line 1017
    return-void
.end method

.method private showCollections()V
    .locals 2

    .prologue
    .line 1102
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->delayHidingCategories()V

    .line 1105
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->collectionsFragment:Lru/cn/tv/stb/collections/CollectionsFragment;

    invoke-virtual {v0}, Lru/cn/tv/stb/collections/CollectionsFragment;->updateIfNeeded()V

    .line 1107
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->collectionFragmentWrapper:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1108
    return-void
.end method

.method private showCurrentCategory()V
    .locals 2

    .prologue
    .line 1162
    sget-object v0, Lru/cn/tv/stb/StbActivity$41;->$SwitchMap$ru$cn$domain$tv$CurrentCategory$Type:[I

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    invoke-virtual {v1}, Lru/cn/domain/tv/CurrentCategory$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1191
    :goto_0
    return-void

    .line 1167
    :pswitch_0
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->setViewMode()V

    .line 1168
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showChannels()V

    goto :goto_0

    .line 1172
    :pswitch_1
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showPornoChannels()V

    goto :goto_0

    .line 1176
    :pswitch_2
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showCollections()V

    goto :goto_0

    .line 1180
    :pswitch_3
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->categoryFragment:Lru/cn/tv/stb/categories/CategoryFragment;

    invoke-virtual {v0}, Lru/cn/tv/stb/categories/CategoryFragment;->getContractorId()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lru/cn/tv/stb/StbActivity;->showBilling(J)V

    goto :goto_0

    .line 1184
    :pswitch_4
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showSettings()V

    goto :goto_0

    .line 1162
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private showFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 954
    invoke-virtual {p0}, Lru/cn/tv/stb/StbActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 955
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 956
    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 957
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 958
    return-void
.end method

.method private showInforming()V
    .locals 4

    .prologue
    .line 1677
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->channelsFragmentList:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/cursor/ChannelCursor;

    .line 1678
    .local v0, "c":Lru/cn/api/provider/cursor/ChannelCursor;
    if-nez v0, :cond_0

    .line 1688
    :goto_0
    return-void

    .line 1681
    :cond_0
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getIsDenied()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1682
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->informingFragment:Lru/cn/tv/stb/informing/InformingFragment;

    invoke-direct {p0, v1}, Lru/cn/tv/stb/StbActivity;->hideFragment(Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 1686
    :cond_1
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->informingFragment:Lru/cn/tv/stb/informing/InformingFragment;

    invoke-direct {p0, v1}, Lru/cn/tv/stb/StbActivity;->showFragment(Landroid/support/v4/app/Fragment;)V

    .line 1687
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->informingFragment:Lru/cn/tv/stb/informing/InformingFragment;

    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getContractorId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lru/cn/tv/stb/informing/InformingFragment;->setContractor(J)V

    goto :goto_0
.end method

.method private showPaidDialog(Ljava/lang/String;)V
    .locals 4
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 1357
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e0117

    .line 1358
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e011c

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    .line 1359
    invoke-virtual {p0, v1, v2}, Lru/cn/tv/stb/StbActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e0034

    const/4 v2, 0x0

    .line 1360
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 1361
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 1362
    return-void
.end method

.method private showPinCodeDialog(Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$PinCodeDialogCallbacks;)V
    .locals 3
    .param p1, "type"    # Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;
    .param p2, "callbacks"    # Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$PinCodeDialogCallbacks;

    .prologue
    .line 2512
    invoke-virtual {p0}, Lru/cn/tv/stb/StbActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 2513
    .local v0, "fm":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {p1}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2536
    :goto_0
    return-void

    .line 2517
    :cond_0
    new-instance v1, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;

    invoke-direct {v1}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;-><init>()V

    .line 2518
    .local v1, "pinCodeFragment":Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;
    new-instance v2, Lru/cn/tv/stb/StbActivity$38;

    invoke-direct {v2, p0, p2}, Lru/cn/tv/stb/StbActivity$38;-><init>(Lru/cn/tv/stb/StbActivity;Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$PinCodeDialogCallbacks;)V

    invoke-virtual {v1, v2}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->setPinCodeDialogListener(Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$PinCodeDialogListener;)V

    .line 2534
    invoke-virtual {v1, p1}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->setDialogType(Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;)V

    .line 2535
    invoke-virtual {p1}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private showPlayerController()V
    .locals 1

    .prologue
    .line 1279
    iget-boolean v0, p0, Lru/cn/tv/stb/StbActivity;->playingAdvertisement:Z

    if-eqz v0, :cond_0

    .line 1284
    :goto_0
    return-void

    .line 1282
    :cond_0
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideCategories()V

    .line 1283
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/StbPlayerController;->show()V

    goto :goto_0
.end method

.method private showPornoChannels()V
    .locals 2

    .prologue
    .line 1020
    const-string v0, "pin_disabled"

    invoke-static {p0, v0}, Lru/cn/domain/Preferences;->getBoolean(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1021
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->pinCode:Lru/cn/domain/PinCode;

    invoke-virtual {v0}, Lru/cn/domain/PinCode;->getStatus()Lru/cn/domain/PinCode$Status;

    move-result-object v0

    sget-object v1, Lru/cn/domain/PinCode$Status;->require:Lru/cn/domain/PinCode$Status;

    if-ne v0, v1, :cond_0

    .line 1022
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->pinCode:Lru/cn/domain/PinCode;

    new-instance v1, Lru/cn/tv/stb/StbActivity$15;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/StbActivity$15;-><init>(Lru/cn/tv/stb/StbActivity;)V

    invoke-virtual {v0, v1}, Lru/cn/domain/PinCode;->getPinCode(Lru/cn/domain/PinCode$PinCodeCheckCallbacks;)V

    .line 1070
    :goto_0
    return-void

    .line 1069
    :cond_0
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showPornoChannelsPanel()V

    goto :goto_0
.end method

.method private showPornoChannelsPanel()V
    .locals 2

    .prologue
    .line 1073
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->channelsFragment:Lru/cn/tv/stb/channels/ChannelsFragment;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lru/cn/tv/stb/channels/ChannelsFragment;->setViewMode(I)V

    .line 1074
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showChannels()V

    .line 1075
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->channelsFragmentList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    .line 1076
    return-void
.end method

.method private showSettings()V
    .locals 2

    .prologue
    .line 1134
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->settingsFragmentWrapper:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1135
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->delayHidingCategories()V

    .line 1136
    return-void
.end method

.method private showTrackWidget()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 1304
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/StbPlayerController;->hide()V

    .line 1305
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideCategories()V

    .line 1307
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->trackListViewWrapper:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1308
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->trackListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    .line 1310
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1311
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1313
    :cond_0
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1314
    return-void
.end method

.method private showVolumeControl()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 1287
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideCategories()V

    .line 1288
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->volumeControl:Lru/cn/view/VolumeControl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lru/cn/view/VolumeControl;->setVisibility(I)V

    .line 1289
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1290
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1293
    :cond_0
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1294
    return-void
.end method

.method private switchChannel(I)V
    .locals 4
    .param p1, "num"    # I

    .prologue
    .line 2093
    iget-object v2, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    sget-object v3, Lru/cn/domain/tv/CurrentCategory$Type;->porno:Lru/cn/domain/tv/CurrentCategory$Type;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lru/cn/tv/stb/StbActivity;->pinCode:Lru/cn/domain/PinCode;

    .line 2094
    invoke-virtual {v2}, Lru/cn/domain/PinCode;->getStatus()Lru/cn/domain/PinCode$Status;

    move-result-object v2

    sget-object v3, Lru/cn/domain/PinCode$Status;->temporarilyDisable:Lru/cn/domain/PinCode$Status;

    if-ne v2, v3, :cond_0

    .line 2095
    iget-object v2, p0, Lru/cn/tv/stb/StbActivity;->pinCode:Lru/cn/domain/PinCode;

    sget-object v3, Lru/cn/domain/PinCode$Status;->require:Lru/cn/domain/PinCode$Status;

    invoke-virtual {v2, v3}, Lru/cn/domain/PinCode;->setStatus(Lru/cn/domain/PinCode$Status;)V

    .line 2098
    :cond_0
    const-string v1, "all"

    .line 2099
    .local v1, "selection":Ljava/lang/String;
    iget-object v2, p0, Lru/cn/tv/stb/StbActivity;->loadChannelFrom:Lru/cn/domain/tv/CurrentCategory$Type;

    sget-object v3, Lru/cn/domain/tv/CurrentCategory$Type;->intersections:Lru/cn/domain/tv/CurrentCategory$Type;

    if-ne v2, v3, :cond_2

    .line 2100
    const-string v1, "intersections"

    .line 2105
    :cond_1
    :goto_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2106
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "number"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2107
    const-string v2, "selection"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2108
    invoke-virtual {p0}, Lru/cn/tv/stb/StbActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3, v0, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 2109
    return-void

    .line 2101
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_2
    iget-object v2, p0, Lru/cn/tv/stb/StbActivity;->loadChannelFrom:Lru/cn/domain/tv/CurrentCategory$Type;

    sget-object v3, Lru/cn/domain/tv/CurrentCategory$Type;->porno:Lru/cn/domain/tv/CurrentCategory$Type;

    if-ne v2, v3, :cond_1

    .line 2102
    const-string v1, "porno"

    goto :goto_0
.end method

.method private unblockChannel(J)V
    .locals 3
    .param p1, "channelId"    # J

    .prologue
    const/4 v2, 0x0

    .line 2575
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->stbViewModel:Lru/cn/tv/stb/StbViewModel;

    invoke-virtual {v0, p1, p2, v2}, Lru/cn/tv/stb/StbViewModel;->setBlocked(JZ)V

    .line 2576
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragment;->getChannelId()J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    .line 2577
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v0, v2}, Lru/cn/tv/player/controller/StbPlayerController;->showChannelBlocked(Z)V

    .line 2579
    :cond_0
    return-void
.end method

.method private viewModeByCategory(Lru/cn/domain/tv/CurrentCategory$Type;)I
    .locals 3
    .param p1, "category"    # Lru/cn/domain/tv/CurrentCategory$Type;

    .prologue
    const/4 v0, 0x0

    .line 2121
    sget-object v1, Lru/cn/tv/stb/StbActivity$41;->$SwitchMap$ru$cn$domain$tv$CurrentCategory$Type:[I

    invoke-virtual {p1}, Lru/cn/domain/tv/CurrentCategory$Type;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2138
    :goto_0
    :pswitch_0
    return v0

    .line 2126
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 2129
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 2132
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 2135
    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    .line 2121
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$StbActivity(Lru/cn/domain/tv/CurrentCategory$Type;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/stb/StbActivity;->setCategory(Lru/cn/domain/tv/CurrentCategory$Type;)V

    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 14
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v10, 0x8

    const/4 v13, 0x5

    const/4 v12, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 740
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->delayHidingCategories()V

    .line 742
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    .line 743
    .local v3, "keyCode":I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v9

    if-nez v9, :cond_2

    move v2, v7

    .line 744
    .local v2, "down":Z
    :goto_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v9

    if-ne v9, v7, :cond_3

    move v6, v7

    .line 745
    .local v6, "up":Z
    :goto_1
    if-eqz v2, :cond_4

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v9

    if-nez v9, :cond_4

    move v5, v7

    .line 747
    .local v5, "uniqueDown":Z
    :goto_2
    const/16 v9, 0xa6

    if-eq v3, v9, :cond_0

    const/16 v9, 0xa7

    if-ne v3, v9, :cond_b

    .line 749
    :cond_0
    iget-boolean v9, p0, Lru/cn/tv/stb/StbActivity;->playingAdvertisement:Z

    if-eqz v9, :cond_5

    .line 879
    :cond_1
    :goto_3
    return v7

    .end local v2    # "down":Z
    .end local v5    # "uniqueDown":Z
    .end local v6    # "up":Z
    :cond_2
    move v2, v8

    .line 743
    goto :goto_0

    .restart local v2    # "down":Z
    :cond_3
    move v6, v8

    .line 744
    goto :goto_1

    .restart local v6    # "up":Z
    :cond_4
    move v5, v8

    .line 745
    goto :goto_2

    .line 752
    .restart local v5    # "uniqueDown":Z
    :cond_5
    if-eqz v2, :cond_a

    .line 753
    iget-object v9, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    iget-object v10, p0, Lru/cn/tv/stb/StbActivity;->loadChannelFrom:Lru/cn/domain/tv/CurrentCategory$Type;

    if-eq v9, v10, :cond_6

    .line 754
    iget-object v9, p0, Lru/cn/tv/stb/StbActivity;->loadChannelFrom:Lru/cn/domain/tv/CurrentCategory$Type;

    iput-object v9, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    .line 755
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->setViewMode()V

    .line 758
    :cond_6
    iget-object v9, p0, Lru/cn/tv/stb/StbActivity;->channelsFragment:Lru/cn/tv/stb/channels/ChannelsFragment;

    invoke-virtual {v9}, Lru/cn/tv/stb/channels/ChannelsFragment;->getCurrentChannel()J

    move-result-wide v0

    .line 759
    .local v0, "channelId":J
    const/4 v4, -0x1

    .line 760
    .local v4, "num":I
    const/16 v9, 0xa6

    if-ne v3, v9, :cond_8

    .line 761
    iget-object v9, p0, Lru/cn/tv/stb/StbActivity;->channelsFragment:Lru/cn/tv/stb/channels/ChannelsFragment;

    invoke-virtual {v9, v0, v1}, Lru/cn/tv/stb/channels/ChannelsFragment;->switchToNextChannel(J)I

    move-result v4

    .line 766
    :cond_7
    :goto_4
    const/4 v9, -0x1

    if-le v4, v9, :cond_9

    .line 767
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideCategories()V

    .line 768
    iget-object v9, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v9}, Lru/cn/tv/player/controller/StbPlayerController;->hide()V

    .line 770
    iget-object v9, p0, Lru/cn/tv/stb/StbActivity;->channelNumber:Landroid/widget/TextView;

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 771
    iget-object v9, p0, Lru/cn/tv/stb/StbActivity;->channelNumber:Landroid/widget/TextView;

    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 773
    iget-object v8, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v8, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 774
    iget-object v8, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    const-wide/16 v10, 0x28a

    invoke-virtual {v8, v12, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 776
    iget-object v8, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v8, v13}, Landroid/os/Handler;->removeMessages(I)V

    .line 777
    iget-object v8, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    const-wide/16 v10, 0x2710

    invoke-virtual {v8, v13, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_3

    .line 762
    :cond_8
    const/16 v9, 0xa7

    if-ne v3, v9, :cond_7

    .line 763
    iget-object v9, p0, Lru/cn/tv/stb/StbActivity;->channelsFragment:Lru/cn/tv/stb/channels/ChannelsFragment;

    invoke-virtual {v9, v0, v1}, Lru/cn/tv/stb/channels/ChannelsFragment;->switchToPrevChannel(J)I

    move-result v4

    goto :goto_4

    .line 779
    :cond_9
    sget-object v8, Lru/cn/domain/tv/CurrentCategory$Type;->all:Lru/cn/domain/tv/CurrentCategory$Type;

    iput-object v8, p0, Lru/cn/tv/stb/StbActivity;->loadChannelFrom:Lru/cn/domain/tv/CurrentCategory$Type;

    .line 780
    iget-object v8, p0, Lru/cn/tv/stb/StbActivity;->pinCode:Lru/cn/domain/PinCode;

    sget-object v9, Lru/cn/domain/PinCode$Status;->require:Lru/cn/domain/PinCode$Status;

    invoke-virtual {v8, v9}, Lru/cn/domain/PinCode;->setStatus(Lru/cn/domain/PinCode$Status;)V

    goto :goto_3

    .line 782
    .end local v0    # "channelId":J
    .end local v4    # "num":I
    :cond_a
    if-eqz v6, :cond_1

    .line 783
    iget-object v8, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v8, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 784
    iget-object v8, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    const-wide/16 v10, 0x12c

    invoke-virtual {v8, v12, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_3

    .line 788
    :cond_b
    if-eqz v2, :cond_d

    const/16 v9, 0x5a

    if-eq v3, v9, :cond_c

    const/16 v9, 0x59

    if-eq v3, v9, :cond_c

    const/16 v9, 0x7f

    if-eq v3, v9, :cond_c

    const/16 v9, 0x7e

    if-eq v3, v9, :cond_c

    const/16 v9, 0x55

    if-eq v3, v9, :cond_c

    const/16 v9, 0x57

    if-eq v3, v9, :cond_c

    const/16 v9, 0x58

    if-ne v3, v9, :cond_d

    .line 795
    :cond_c
    iget-boolean v8, p0, Lru/cn/tv/stb/StbActivity;->playingAdvertisement:Z

    if-nez v8, :cond_1

    .line 796
    iget-object v8, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v8, p1}, Lru/cn/tv/player/controller/StbPlayerController;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    .line 797
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showPlayerController()V

    goto/16 :goto_3

    .line 802
    :cond_d
    const/16 v9, 0x16

    if-eq v3, v9, :cond_e

    const/16 v9, 0x72

    if-eq v3, v9, :cond_e

    const/16 v9, 0x15

    if-eq v3, v9, :cond_e

    const/16 v9, 0x71

    if-ne v3, v9, :cond_f

    .line 805
    :cond_e
    iget-object v7, p0, Lru/cn/tv/stb/StbActivity;->nextTelecastView:Lru/cn/tv/player/controller/StbNextTelecastView;

    invoke-virtual {v7}, Lru/cn/tv/player/controller/StbNextTelecastView;->getVisibility()I

    move-result v7

    if-nez v7, :cond_14

    .line 806
    iget-object v7, p0, Lru/cn/tv/stb/StbActivity;->nextTelecastView:Lru/cn/tv/player/controller/StbNextTelecastView;

    invoke-virtual {v7}, Lru/cn/tv/player/controller/StbNextTelecastView;->hide()V

    move v7, v8

    .line 807
    goto/16 :goto_3

    .line 810
    :cond_f
    const/16 v8, 0xa8

    if-ne v3, v8, :cond_10

    .line 811
    iget-object v7, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v7, p1}, Lru/cn/tv/player/controller/StbPlayerController;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v7

    goto/16 :goto_3

    .line 813
    :cond_10
    const/16 v8, 0x18

    if-eq v3, v8, :cond_11

    const/16 v8, 0x19

    if-eq v3, v8, :cond_11

    const/16 v8, 0xa4

    if-ne v3, v8, :cond_12

    .line 816
    :cond_11
    iget-object v7, p0, Lru/cn/tv/stb/StbActivity;->volumeControl:Lru/cn/view/VolumeControl;

    invoke-virtual {v7, p1}, Lru/cn/view/VolumeControl;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v7

    goto/16 :goto_3

    .line 817
    :cond_12
    const/4 v8, 0x7

    if-lt v3, v8, :cond_13

    const/16 v8, 0x10

    if-gt v3, v8, :cond_13

    iget-object v8, p0, Lru/cn/tv/stb/StbActivity;->billingFragmentWrapper:Landroid/view/View;

    .line 819
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v8

    if-eqz v8, :cond_13

    .line 821
    if-eqz v2, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v8

    if-nez v8, :cond_1

    iget-boolean v8, p0, Lru/cn/tv/stb/StbActivity;->playingAdvertisement:Z

    if-nez v8, :cond_1

    .line 822
    invoke-direct {p0, p1}, Lru/cn/tv/stb/StbActivity;->dispatchDigitKeyEvent(Landroid/view/KeyEvent;)V

    goto/16 :goto_3

    .line 825
    :cond_13
    const/16 v7, 0x85

    if-ne v3, v7, :cond_16

    .line 827
    if-eqz v2, :cond_14

    iget-boolean v7, p0, Lru/cn/tv/stb/StbActivity;->isPlaying:Z

    if-eqz v7, :cond_14

    .line 828
    iget-object v7, p0, Lru/cn/tv/stb/StbActivity;->trackListViewWrapper:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-ne v7, v10, :cond_15

    .line 829
    iget-object v7, p0, Lru/cn/tv/stb/StbActivity;->audioTrackInfoProvider:Lru/cn/player/ITrackSelector;

    if-eqz v7, :cond_14

    iget-object v7, p0, Lru/cn/tv/stb/StbActivity;->audioTrackInfoProvider:Lru/cn/player/ITrackSelector;

    invoke-interface {v7}, Lru/cn/player/ITrackSelector;->containsTracks()Z

    move-result v7

    if-eqz v7, :cond_14

    .line 830
    iget-object v7, p0, Lru/cn/tv/stb/StbActivity;->audioTrackInfoAdapter:Lru/cn/tv/player/controller/TracksAdapter;

    iget-object v8, p0, Lru/cn/tv/stb/StbActivity;->audioTrackInfoProvider:Lru/cn/player/ITrackSelector;

    invoke-interface {v8}, Lru/cn/player/ITrackSelector;->getCurrentTrackIndex()I

    move-result v8

    invoke-virtual {v7, v8}, Lru/cn/tv/player/controller/TracksAdapter;->setSelectedPosition(I)V

    .line 831
    iget-object v7, p0, Lru/cn/tv/stb/StbActivity;->trackListView:Landroid/widget/ListView;

    iget-object v8, p0, Lru/cn/tv/stb/StbActivity;->audioTrackInfoAdapter:Lru/cn/tv/player/controller/TracksAdapter;

    invoke-virtual {v7, v8}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 832
    iget-object v7, p0, Lru/cn/tv/stb/StbActivity;->trackListView:Landroid/widget/ListView;

    iget-object v8, p0, Lru/cn/tv/stb/StbActivity;->audioTrackInfoProvider:Lru/cn/player/ITrackSelector;

    invoke-interface {v8}, Lru/cn/player/ITrackSelector;->getCurrentTrackIndex()I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/ListView;->setSelection(I)V

    .line 833
    iget-object v7, p0, Lru/cn/tv/stb/StbActivity;->trackListViewWrapper:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->requestFocus()Z

    .line 834
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showTrackWidget()V

    .line 879
    :cond_14
    :goto_5
    invoke-super {p0, p1}, Lru/cn/tv/FullScreenActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v7

    goto/16 :goto_3

    .line 837
    :cond_15
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideTrackWidget()V

    goto :goto_5

    .line 840
    :cond_16
    const/16 v7, 0x84

    if-ne v3, v7, :cond_18

    if-eqz v2, :cond_18

    .line 842
    iget-boolean v7, p0, Lru/cn/tv/stb/StbActivity;->isPlaying:Z

    if-eqz v7, :cond_14

    .line 843
    iget-object v7, p0, Lru/cn/tv/stb/StbActivity;->trackListViewWrapper:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-ne v7, v10, :cond_17

    .line 844
    iget-object v7, p0, Lru/cn/tv/stb/StbActivity;->subtitlesTrackProvider:Lru/cn/player/ITrackSelector;

    if-eqz v7, :cond_14

    iget-object v7, p0, Lru/cn/tv/stb/StbActivity;->subtitlesTrackProvider:Lru/cn/player/ITrackSelector;

    invoke-interface {v7}, Lru/cn/player/ITrackSelector;->containsTracks()Z

    move-result v7

    if-eqz v7, :cond_14

    .line 845
    iget-object v7, p0, Lru/cn/tv/stb/StbActivity;->subtitleTrackInfoAdapter:Lru/cn/tv/player/controller/TracksAdapter;

    iget-object v8, p0, Lru/cn/tv/stb/StbActivity;->subtitlesTrackProvider:Lru/cn/player/ITrackSelector;

    invoke-interface {v8}, Lru/cn/player/ITrackSelector;->getCurrentTrackIndex()I

    move-result v8

    invoke-virtual {v7, v8}, Lru/cn/tv/player/controller/TracksAdapter;->setSelectedPosition(I)V

    .line 846
    iget-object v7, p0, Lru/cn/tv/stb/StbActivity;->trackListView:Landroid/widget/ListView;

    iget-object v8, p0, Lru/cn/tv/stb/StbActivity;->subtitleTrackInfoAdapter:Lru/cn/tv/player/controller/TracksAdapter;

    invoke-virtual {v7, v8}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 847
    iget-object v7, p0, Lru/cn/tv/stb/StbActivity;->trackListView:Landroid/widget/ListView;

    iget-object v8, p0, Lru/cn/tv/stb/StbActivity;->subtitlesTrackProvider:Lru/cn/player/ITrackSelector;

    invoke-interface {v8}, Lru/cn/player/ITrackSelector;->getCurrentTrackIndex()I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/ListView;->setSelection(I)V

    .line 848
    iget-object v7, p0, Lru/cn/tv/stb/StbActivity;->trackListViewWrapper:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->requestFocus()Z

    .line 849
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showTrackWidget()V

    goto :goto_5

    .line 852
    :cond_17
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideTrackWidget()V

    goto :goto_5

    .line 856
    :cond_18
    const/16 v7, 0x86

    if-ne v3, v7, :cond_19

    if-eqz v2, :cond_19

    .line 857
    iget-boolean v7, p0, Lru/cn/tv/stb/StbActivity;->playingAdvertisement:Z

    if-nez v7, :cond_14

    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->Rot()Z

    move-result v7

    if-eqz v7, :cond_14

    .line 858
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideCategories()V

    .line 859
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showPlayerController()V

    goto :goto_5

    .line 861
    :cond_19
    const/16 v7, 0xa5

    if-ne v3, v7, :cond_1b

    .line 862
    if-eqz v5, :cond_14

    .line 863
    iget-object v7, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v7}, Lru/cn/tv/player/controller/StbPlayerController;->isShown()Z

    move-result v7

    if-eqz v7, :cond_1a

    .line 864
    iget-object v7, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v7}, Lru/cn/tv/player/controller/StbPlayerController;->hide()V

    goto :goto_5

    .line 866
    :cond_1a
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showPlayerController()V

    goto :goto_5

    .line 869
    :cond_1b
    const/16 v7, 0xba

    if-ne v3, v7, :cond_14

    .line 870
    if-eqz v5, :cond_14

    .line 871
    iget-object v7, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v7}, Lru/cn/tv/player/SimplePlayerFragment;->isPornoChannelPlayed()Z

    move-result v7

    if-nez v7, :cond_14

    iget-object v7, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    .line 872
    invoke-virtual {v7}, Lru/cn/tv/player/SimplePlayerFragment;->isIntersectionChannelPlayed()Z

    move-result v7

    if-nez v7, :cond_14

    .line 873
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showPlayerController()V

    .line 874
    iget-object v7, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v7}, Lru/cn/tv/player/controller/StbPlayerController;->performFavStarClick()V

    goto/16 :goto_5
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2144
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    move v2, v3

    .line 2172
    :goto_0
    return v2

    .line 2146
    :pswitch_0
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideCategories()V

    goto :goto_0

    .line 2150
    :pswitch_1
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideVolumeControl()V

    goto :goto_0

    .line 2154
    :pswitch_2
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->channelNumber:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2155
    .local v1, "text":Ljava/lang/String;
    const-string v4, ""

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2156
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 2157
    .local v0, "num":I
    invoke-direct {p0, v0}, Lru/cn/tv/stb/StbActivity;->switchChannel(I)V

    .line 2159
    .end local v0    # "num":I
    :cond_0
    iput-boolean v3, p0, Lru/cn/tv/stb/StbActivity;->startDigitInput:Z

    goto :goto_0

    .line 2163
    .end local v1    # "text":Ljava/lang/String;
    :pswitch_3
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideTrackWidget()V

    goto :goto_0

    .line 2167
    :pswitch_4
    iget-object v3, p0, Lru/cn/tv/stb/StbActivity;->channelNumber:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2168
    iget-object v3, p0, Lru/cn/tv/stb/StbActivity;->channelNumber:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 2144
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method final synthetic lambda$onCreate$0$StbActivity(Landroid/util/Pair;)V
    .locals 2
    .param p1, "result"    # Landroid/util/Pair;

    .prologue
    .line 211
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lru/cn/api/catalogue/replies/Rubric;

    iget-object v1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Landroid/database/Cursor;

    invoke-direct {p0, v0, v1}, Lru/cn/tv/stb/StbActivity;->setRelatedRubric(Lru/cn/api/catalogue/replies/Rubric;Landroid/database/Cursor;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 884
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->volumeControl:Lru/cn/view/VolumeControl;

    invoke-virtual {v0}, Lru/cn/view/VolumeControl;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 885
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideVolumeControl()V

    .line 939
    :cond_0
    :goto_0
    return-void

    .line 889
    :cond_1
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->billingFragmentWrapper:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    .line 890
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->billingFragment:Lru/cn/tv/billing/BillingFragment;

    invoke-virtual {v0}, Lru/cn/tv/billing/BillingFragment;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 891
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->billingFragment:Lru/cn/tv/billing/BillingFragment;

    invoke-virtual {v0}, Lru/cn/tv/billing/BillingFragment;->goBack()Z

    move-result v0

    if-nez v0, :cond_0

    .line 896
    :cond_2
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->billing:Lru/cn/domain/tv/CurrentCategory$Type;

    if-ne v0, v1, :cond_3

    .line 897
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideCategories()V

    goto :goto_0

    .line 899
    :cond_3
    iput v2, p0, Lru/cn/tv/stb/StbActivity;->currentSettingSubcategory:I

    .line 900
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideBilling()V

    .line 901
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showCurrentCategory()V

    goto :goto_0

    .line 907
    :cond_4
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->setting:Lru/cn/domain/tv/CurrentCategory$Type;

    if-ne v0, v1, :cond_5

    .line 908
    iput v2, p0, Lru/cn/tv/stb/StbActivity;->currentSettingSubcategory:I

    .line 911
    :cond_5
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->trackListViewWrapper:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_6

    .line 912
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideTrackWidget()V

    goto :goto_0

    .line 916
    :cond_6
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->nextTelecastView:Lru/cn/tv/player/controller/StbNextTelecastView;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/StbNextTelecastView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_7

    .line 917
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->nextTelecastView:Lru/cn/tv/player/controller/StbNextTelecastView;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/StbNextTelecastView;->hide()V

    goto :goto_0

    .line 921
    :cond_7
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/StbPlayerController;->isShown()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 922
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/StbPlayerController;->hide()V

    goto :goto_0

    .line 926
    :cond_8
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->collectionFragmentWrapper:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->categoryFragmentList:Landroid/widget/ListView;

    .line 927
    invoke-virtual {v0}, Landroid/widget/ListView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_9

    .line 928
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lru/cn/tv/stb/StbActivity;->showCategories(Z)V

    .line 929
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->expandCategories()V

    goto :goto_0

    .line 933
    :cond_9
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->categoryFragmentWrapper:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_a

    .line 934
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideCategories()V

    goto :goto_0

    .line 938
    :cond_a
    invoke-super {p0}, Lru/cn/tv/FullScreenActivity;->onBackPressed()V

    goto/16 :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 190
    invoke-static {p0}, Lru/cn/domain/KidsObject;->isKidsMode(Landroid/content/Context;)Z

    move-result v4

    iput-boolean v4, p0, Lru/cn/tv/stb/StbActivity;->kidsMode:Z

    .line 191
    iget-boolean v4, p0, Lru/cn/tv/stb/StbActivity;->kidsMode:Z

    if-eqz v4, :cond_3

    .line 192
    const-string v4, "StbActivity"

    const-string v6, "kids theme"

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    const v4, 0x7f0f0009

    invoke-virtual {p0, v4}, Lru/cn/tv/stb/StbActivity;->setTheme(I)V

    .line 199
    :goto_0
    invoke-super {p0, p1}, Lru/cn/tv/FullScreenActivity;->onCreate(Landroid/os/Bundle;)V

    .line 200
    const v4, 0x7f0c007f

    invoke-virtual {p0, v4}, Lru/cn/tv/stb/StbActivity;->setContentView(I)V

    .line 202
    invoke-virtual {p0}, Lru/cn/tv/stb/StbActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lru/cn/ad/AdsManager;->preload(Landroid/content/Context;)V

    .line 203
    invoke-virtual {p0}, Lru/cn/tv/stb/StbActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lru/cn/api/googlepush/ApplicationRegistrar;->registerIfNeeded(Landroid/content/Context;)V

    .line 205
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    .line 207
    new-instance v4, Lru/cn/domain/LastChannel;

    iget-boolean v6, p0, Lru/cn/tv/stb/StbActivity;->kidsMode:Z

    invoke-direct {v4, p0, v6}, Lru/cn/domain/LastChannel;-><init>(Landroid/content/Context;Z)V

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->lastChannelPrefs:Lru/cn/domain/LastChannel;

    .line 208
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v4

    const-class v6, Lru/cn/domain/PinCode;

    invoke-interface {v4, v6}, Ltoothpick/Scope;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/domain/PinCode;

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->pinCode:Lru/cn/domain/PinCode;

    .line 210
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v4

    const-class v6, Lru/cn/tv/stb/StbViewModel;

    invoke-static {p0, v4, v6}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/FragmentActivity;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v4

    check-cast v4, Lru/cn/tv/stb/StbViewModel;

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->stbViewModel:Lru/cn/tv/stb/StbViewModel;

    .line 211
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->stbViewModel:Lru/cn/tv/stb/StbViewModel;

    invoke-virtual {v4}, Lru/cn/tv/stb/StbViewModel;->relatedRubric()Landroid/arch/lifecycle/LiveData;

    move-result-object v4

    new-instance v6, Lru/cn/tv/stb/StbActivity$$Lambda$0;

    invoke-direct {v6, p0}, Lru/cn/tv/stb/StbActivity$$Lambda$0;-><init>(Lru/cn/tv/stb/StbActivity;)V

    invoke-virtual {v4, p0, v6}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 212
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->stbViewModel:Lru/cn/tv/stb/StbViewModel;

    invoke-virtual {v4}, Lru/cn/tv/stb/StbViewModel;->category()Landroid/arch/lifecycle/LiveData;

    move-result-object v4

    new-instance v6, Lru/cn/tv/stb/StbActivity$$Lambda$1;

    invoke-direct {v6, p0}, Lru/cn/tv/stb/StbActivity$$Lambda$1;-><init>(Lru/cn/tv/stb/StbActivity;)V

    invoke-virtual {v4, p0, v6}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 214
    const v4, 0x7f090154

    invoke-virtual {p0, v4}, Lru/cn/tv/stb/StbActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->notificationView:Landroid/view/View;

    .line 216
    const v4, 0x7f090144

    invoke-virtual {p0, v4}, Lru/cn/tv/stb/StbActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lru/cn/tv/player/controller/StbNextTelecastView;

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->nextTelecastView:Lru/cn/tv/player/controller/StbNextTelecastView;

    .line 217
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->nextTelecastView:Lru/cn/tv/player/controller/StbNextTelecastView;

    new-instance v6, Lru/cn/tv/stb/StbActivity$1;

    invoke-direct {v6, p0}, Lru/cn/tv/stb/StbActivity$1;-><init>(Lru/cn/tv/stb/StbActivity;)V

    invoke-virtual {v4, v6}, Lru/cn/tv/player/controller/StbNextTelecastView;->setListener(Lru/cn/tv/player/controller/StbNextTelecastView$Listener;)V

    .line 224
    const v4, 0x7f090168

    invoke-virtual {p0, v4}, Lru/cn/tv/stb/StbActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lru/cn/tv/player/controller/StbPlayerController;

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    .line 225
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->settingsPanelListener:Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;

    invoke-virtual {v4, v6}, Lru/cn/tv/player/controller/StbPlayerController;->setSettingsListener(Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;)V

    .line 226
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->relatedPanelListener:Lru/cn/tv/player/controller/StbPlayerRelatedPanel$Listener;

    invoke-virtual {v4, v6}, Lru/cn/tv/player/controller/StbPlayerController;->setRelatedListener(Lru/cn/tv/player/controller/StbPlayerRelatedPanel$Listener;)V

    .line 227
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->blockedContentListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v6}, Lru/cn/tv/player/controller/StbPlayerController;->setBlockedContentListener(Landroid/view/View$OnClickListener;)V

    .line 229
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    new-instance v6, Lru/cn/tv/stb/StbActivity$2;

    invoke-direct {v6, p0}, Lru/cn/tv/stb/StbActivity$2;-><init>(Lru/cn/tv/stb/StbActivity;)V

    invoke-virtual {v4, v6}, Lru/cn/tv/player/controller/StbPlayerController;->setCompletionBehaviour(Lru/cn/tv/player/controller/PlayerController$CompletionBehaviour;)V

    .line 247
    invoke-virtual {p0}, Lru/cn/tv/stb/StbActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const v6, 0x7f090169

    .line 248
    invoke-virtual {v4, v6}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Lru/cn/tv/player/SimplePlayerFragment;

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    .line 249
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragmentListener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

    invoke-virtual {v4, v6}, Lru/cn/tv/player/SimplePlayerFragment;->setListener(Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;)V

    .line 250
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v4, v6}, Lru/cn/tv/player/SimplePlayerFragment;->setMediaController(Lru/cn/tv/player/controller/PlayerController;)V

    .line 252
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v6}, Lru/cn/tv/player/SimplePlayerFragment;->getFitMode()Lru/cn/player/SimplePlayer$FitMode;

    move-result-object v6

    invoke-virtual {v4, v6}, Lru/cn/tv/player/controller/StbPlayerController;->setFitMode(Lru/cn/player/SimplePlayer$FitMode;)V

    .line 254
    const v4, 0x7f090065

    invoke-virtual {p0, v4}, Lru/cn/tv/stb/StbActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->categoryFragmentWrapper:Landroid/view/View;

    .line 256
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->categoryFragmentWrapper:Landroid/view/View;

    new-instance v6, Lru/cn/tv/stb/StbActivity$3;

    invoke-direct {v6, p0}, Lru/cn/tv/stb/StbActivity$3;-><init>(Lru/cn/tv/stb/StbActivity;)V

    invoke-virtual {v4, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 265
    invoke-virtual {p0}, Lru/cn/tv/stb/StbActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const v6, 0x7f090064

    .line 266
    invoke-virtual {v4, v6}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Lru/cn/tv/stb/categories/CategoryFragment;

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->categoryFragment:Lru/cn/tv/stb/categories/CategoryFragment;

    .line 267
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->categoryFragment:Lru/cn/tv/stb/categories/CategoryFragment;

    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->categoryFragmentListener:Lru/cn/tv/stb/categories/CategoryFragment$CategoryFragmentListener;

    invoke-virtual {v4, v6}, Lru/cn/tv/stb/categories/CategoryFragment;->setListener(Lru/cn/tv/stb/categories/CategoryFragment$CategoryFragmentListener;)V

    .line 268
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->categoryFragment:Lru/cn/tv/stb/categories/CategoryFragment;

    invoke-virtual {v4}, Lru/cn/tv/stb/categories/CategoryFragment;->getListView()Landroid/widget/ListView;

    move-result-object v4

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->categoryFragmentList:Landroid/widget/ListView;

    .line 269
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->categoryFragmentList:Landroid/widget/ListView;

    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->categoryKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 270
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->categoryFragmentList:Landroid/widget/ListView;

    new-instance v6, Lru/cn/tv/stb/StbActivity$4;

    invoke-direct {v6, p0}, Lru/cn/tv/stb/StbActivity$4;-><init>(Lru/cn/tv/stb/StbActivity;)V

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 280
    const v4, 0x7f09006e

    invoke-virtual {p0, v4}, Lru/cn/tv/stb/StbActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->channelsFragmentWrapper:Landroid/view/View;

    .line 281
    invoke-virtual {p0}, Lru/cn/tv/stb/StbActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const v6, 0x7f09006d

    .line 282
    invoke-virtual {v4, v6}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Lru/cn/tv/stb/channels/ChannelsFragment;

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->channelsFragment:Lru/cn/tv/stb/channels/ChannelsFragment;

    .line 283
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->channelsFragment:Lru/cn/tv/stb/channels/ChannelsFragment;

    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->channelsFragmentListener:Lru/cn/tv/stb/channels/ChannelsFragment$ChannelsFragmentListener;

    invoke-virtual {v4, v6}, Lru/cn/tv/stb/channels/ChannelsFragment;->setListener(Lru/cn/tv/stb/channels/ChannelsFragment$ChannelsFragmentListener;)V

    .line 285
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->channelsFragment:Lru/cn/tv/stb/channels/ChannelsFragment;

    invoke-virtual {v4}, Lru/cn/tv/stb/channels/ChannelsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v4

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->channelsFragmentList:Landroid/widget/ListView;

    .line 286
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->channelsFragmentList:Landroid/widget/ListView;

    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->channelsKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 287
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->channelsFragmentList:Landroid/widget/ListView;

    new-instance v6, Lru/cn/tv/stb/StbActivity$5;

    invoke-direct {v6, p0}, Lru/cn/tv/stb/StbActivity$5;-><init>(Lru/cn/tv/stb/StbActivity;)V

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 295
    const v4, 0x7f090050

    invoke-virtual {p0, v4}, Lru/cn/tv/stb/StbActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->calendarFragmentWrapper:Landroid/view/View;

    .line 296
    invoke-virtual {p0}, Lru/cn/tv/stb/StbActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const v6, 0x7f09004f

    .line 297
    invoke-virtual {v4, v6}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Lru/cn/tv/stb/calendar/CalendarFragment;

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->calendarFragment:Lru/cn/tv/stb/calendar/CalendarFragment;

    .line 298
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->calendarFragment:Lru/cn/tv/stb/calendar/CalendarFragment;

    invoke-virtual {v4}, Lru/cn/tv/stb/calendar/CalendarFragment;->getListView()Landroid/widget/ListView;

    move-result-object v4

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->calendarFragmentList:Landroid/widget/ListView;

    .line 300
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->calendarFragmentList:Landroid/widget/ListView;

    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->calendarItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 301
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->calendarFragmentList:Landroid/widget/ListView;

    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->calendarKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 302
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->calendarFragmentList:Landroid/widget/ListView;

    new-instance v6, Lru/cn/tv/stb/StbActivity$6;

    invoke-direct {v6, p0}, Lru/cn/tv/stb/StbActivity$6;-><init>(Lru/cn/tv/stb/StbActivity;)V

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 311
    const v4, 0x7f090189

    invoke-virtual {p0, v4}, Lru/cn/tv/stb/StbActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->scheduleFragmentWrapper:Landroid/view/View;

    .line 312
    invoke-virtual {p0}, Lru/cn/tv/stb/StbActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const v6, 0x7f090188

    .line 313
    invoke-virtual {v4, v6}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Lru/cn/tv/stb/schedule/ScheduleFragment;

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->scheduleFragment:Lru/cn/tv/stb/schedule/ScheduleFragment;

    .line 314
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->scheduleFragment:Lru/cn/tv/stb/schedule/ScheduleFragment;

    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->scheduleItemClickListener:Lru/cn/tv/stb/schedule/ScheduleFragment$ScheduleItemClickListener;

    invoke-virtual {v4, v6}, Lru/cn/tv/stb/schedule/ScheduleFragment;->setListener(Lru/cn/tv/stb/schedule/ScheduleFragment$ScheduleItemClickListener;)V

    .line 316
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->scheduleFragment:Lru/cn/tv/stb/schedule/ScheduleFragment;

    invoke-virtual {v4}, Lru/cn/tv/stb/schedule/ScheduleFragment;->getListView()Landroid/widget/ListView;

    move-result-object v4

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->scheduleFragmentList:Landroid/widget/ListView;

    .line 317
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->scheduleFragmentList:Landroid/widget/ListView;

    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->scheduleKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 319
    const v4, 0x7f09007b

    invoke-virtual {p0, v4}, Lru/cn/tv/stb/StbActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->collectionFragmentWrapper:Landroid/view/View;

    .line 320
    invoke-virtual {p0}, Lru/cn/tv/stb/StbActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const v6, 0x7f09007a

    invoke-virtual {v4, v6}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Lru/cn/tv/stb/collections/CollectionsFragment;

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->collectionsFragment:Lru/cn/tv/stb/collections/CollectionsFragment;

    .line 322
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->collectionsFragment:Lru/cn/tv/stb/collections/CollectionsFragment;

    new-instance v6, Lru/cn/tv/stb/StbActivity$7;

    invoke-direct {v6, p0}, Lru/cn/tv/stb/StbActivity$7;-><init>(Lru/cn/tv/stb/StbActivity;)V

    invoke-virtual {v4, v6}, Lru/cn/tv/stb/collections/CollectionsFragment;->setCollectionListener(Lru/cn/tv/stb/collections/CollectionsFragment$CollectionListener;)V

    .line 350
    const v4, 0x7f09003b

    invoke-virtual {p0, v4}, Lru/cn/tv/stb/StbActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->billingFragmentWrapper:Landroid/view/View;

    .line 351
    invoke-virtual {p0}, Lru/cn/tv/stb/StbActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const v6, 0x7f09003a

    .line 352
    invoke-virtual {v4, v6}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Lru/cn/tv/billing/BillingFragment;

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->billingFragment:Lru/cn/tv/billing/BillingFragment;

    .line 354
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->billingFragment:Lru/cn/tv/billing/BillingFragment;

    const-string v6, "http://close/"

    invoke-virtual {v4, v6}, Lru/cn/tv/billing/BillingFragment;->addHandledUrl(Ljava/lang/String;)V

    .line 355
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->billingFragment:Lru/cn/tv/billing/BillingFragment;

    const-string v6, "http://closedisableenterpin/"

    invoke-virtual {v4, v6}, Lru/cn/tv/billing/BillingFragment;->addHandledUrl(Ljava/lang/String;)V

    .line 356
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->billingFragment:Lru/cn/tv/billing/BillingFragment;

    const-string v6, "http://closerenewpinfromsetting/"

    invoke-virtual {v4, v6}, Lru/cn/tv/billing/BillingFragment;->addHandledUrl(Ljava/lang/String;)V

    .line 357
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->billingFragment:Lru/cn/tv/billing/BillingFragment;

    const-string v6, "http://closepinfrombilling/"

    invoke-virtual {v4, v6}, Lru/cn/tv/billing/BillingFragment;->addHandledUrl(Ljava/lang/String;)V

    .line 359
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->billingFragment:Lru/cn/tv/billing/BillingFragment;

    new-instance v6, Lru/cn/tv/stb/StbActivity$8;

    invoke-direct {v6, p0}, Lru/cn/tv/stb/StbActivity$8;-><init>(Lru/cn/tv/stb/StbActivity;)V

    invoke-virtual {v4, v6}, Lru/cn/tv/billing/BillingFragment;->setListener(Lru/cn/tv/WebviewFragment$WebviewFragmentListener;)V

    .line 399
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->billingFragment:Lru/cn/tv/billing/BillingFragment;

    invoke-virtual {v4}, Lru/cn/tv/billing/BillingFragment;->getView()Landroid/view/View;

    move-result-object v4

    const v6, 0x7f0901f8

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 400
    .local v3, "webView":Landroid/view/View;
    new-instance v4, Lru/cn/tv/stb/StbActivity$9;

    invoke-direct {v4, p0}, Lru/cn/tv/stb/StbActivity$9;-><init>(Lru/cn/tv/stb/StbActivity;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 436
    invoke-virtual {p0}, Lru/cn/tv/stb/StbActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const v6, 0x7f0900f8

    .line 437
    invoke-virtual {v4, v6}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Lru/cn/tv/stb/informing/InformingFragment;

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->informingFragment:Lru/cn/tv/stb/informing/InformingFragment;

    .line 438
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->informingFragment:Lru/cn/tv/stb/informing/InformingFragment;

    invoke-direct {p0, v4}, Lru/cn/tv/stb/StbActivity;->hideFragment(Landroid/support/v4/app/Fragment;)V

    .line 440
    invoke-virtual {p0}, Lru/cn/tv/stb/StbActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const v6, 0x7f0901a0

    .line 441
    invoke-virtual {v4, v6}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Lru/cn/tv/stb/settings/SettingFragment;

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->settingFragment:Lru/cn/tv/stb/settings/SettingFragment;

    .line 442
    const v4, 0x7f0901a1

    invoke-virtual {p0, v4}, Lru/cn/tv/stb/StbActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->settingsFragmentWrapper:Landroid/view/View;

    .line 443
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->settingFragment:Lru/cn/tv/stb/settings/SettingFragment;

    invoke-virtual {v4}, Lru/cn/tv/stb/settings/SettingFragment;->getListView()Landroid/widget/ListView;

    move-result-object v4

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->settingFragmentList:Landroid/widget/ListView;

    .line 444
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->settingFragmentList:Landroid/widget/ListView;

    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->settingOnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 445
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->settingFragment:Lru/cn/tv/stb/settings/SettingFragment;

    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->settingFragmentListener:Lru/cn/tv/stb/settings/SettingFragment$SettingFragmentListener;

    invoke-virtual {v4, v6}, Lru/cn/tv/stb/settings/SettingFragment;->setSettingFragmentListener(Lru/cn/tv/stb/settings/SettingFragment$SettingFragmentListener;)V

    .line 447
    const v4, 0x7f0901f3

    invoke-virtual {p0, v4}, Lru/cn/tv/stb/StbActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lru/cn/view/VolumeControl;

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->volumeControl:Lru/cn/view/VolumeControl;

    .line 448
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->volumeControl:Lru/cn/view/VolumeControl;

    new-instance v6, Lru/cn/tv/stb/StbActivity$10;

    invoke-direct {v6, p0}, Lru/cn/tv/stb/StbActivity$10;-><init>(Lru/cn/tv/stb/StbActivity;)V

    invoke-virtual {v4, v6}, Lru/cn/view/VolumeControl;->setListener(Lru/cn/view/VolumeControl$VolumeControlListener;)V

    .line 455
    const v4, 0x7f09006a

    invoke-virtual {p0, v4}, Lru/cn/tv/stb/StbActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->channelNumber:Landroid/widget/TextView;

    .line 457
    const v4, 0x7f0901e5

    invoke-virtual {p0, v4}, Lru/cn/tv/stb/StbActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->trackListViewWrapper:Landroid/view/View;

    .line 458
    const v4, 0x7f0901e3

    invoke-virtual {p0, v4}, Lru/cn/tv/stb/StbActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ListView;

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->trackListView:Landroid/widget/ListView;

    .line 459
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->trackListView:Landroid/widget/ListView;

    new-instance v6, Lru/cn/tv/stb/StbActivity$11;

    invoke-direct {v6, p0}, Lru/cn/tv/stb/StbActivity$11;-><init>(Lru/cn/tv/stb/StbActivity;)V

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 483
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->trackListView:Landroid/widget/ListView;

    new-instance v6, Lru/cn/tv/stb/StbActivity$12;

    invoke-direct {v6, p0}, Lru/cn/tv/stb/StbActivity$12;-><init>(Lru/cn/tv/stb/StbActivity;)V

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 496
    new-instance v4, Lru/cn/network/NetworkChangeReceiver;

    invoke-direct {v4}, Lru/cn/network/NetworkChangeReceiver;-><init>()V

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->receiver:Lru/cn/network/NetworkChangeReceiver;

    .line 498
    invoke-static {p0}, Lru/cn/peersay/RemoteCommandReceiver;->isPeersayInstalled(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 499
    new-instance v4, Lru/cn/peersay/controllers/PlayerRemoteController;

    invoke-direct {v4}, Lru/cn/peersay/controllers/PlayerRemoteController;-><init>()V

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->remoteController:Lru/cn/peersay/controllers/PlayerRemoteController;

    .line 500
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->remoteController:Lru/cn/peersay/controllers/PlayerRemoteController;

    invoke-virtual {v4, v6}, Lru/cn/tv/player/SimplePlayerFragment;->setRemoteController(Lru/cn/peersay/controllers/PlayerRemoteController;)V

    .line 502
    new-instance v4, Lru/cn/peersay/controllers/PlayContentController;

    invoke-direct {v4}, Lru/cn/peersay/controllers/PlayContentController;-><init>()V

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->remoteContentController:Lru/cn/peersay/controllers/PlayContentController;

    .line 503
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->remoteContentController:Lru/cn/peersay/controllers/PlayContentController;

    new-instance v6, Lru/cn/tv/stb/StbActivity$13;

    invoke-direct {v6, p0}, Lru/cn/tv/stb/StbActivity$13;-><init>(Lru/cn/tv/stb/StbActivity;)V

    invoke-virtual {v4, v6}, Lru/cn/peersay/controllers/PlayContentController;->setListener(Lru/cn/peersay/controllers/PlayContentController$Listener;)V

    .line 524
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->remoteController:Lru/cn/peersay/controllers/PlayerRemoteController;

    invoke-static {v4}, Lru/cn/peersay/RemoteCommandReceiver;->attachController(Lru/cn/peersay/controllers/RemoteIntentController;)V

    .line 525
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->remoteContentController:Lru/cn/peersay/controllers/PlayContentController;

    invoke-static {v4}, Lru/cn/peersay/RemoteCommandReceiver;->attachController(Lru/cn/peersay/controllers/RemoteIntentController;)V

    .line 526
    invoke-static {}, Lru/cn/peersay/controllers/DialogsRemoteController;->sharedInstance()Lru/cn/peersay/controllers/DialogsRemoteController;

    move-result-object v4

    invoke-static {v4}, Lru/cn/peersay/RemoteCommandReceiver;->attachController(Lru/cn/peersay/controllers/RemoteIntentController;)V

    .line 529
    :cond_0
    invoke-virtual {p0}, Lru/cn/tv/stb/StbActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-direct {p0, v4}, Lru/cn/tv/stb/StbActivity;->handleIntent(Landroid/content/Intent;)Z

    move-result v2

    .line 530
    .local v2, "consumed":Z
    if-nez v2, :cond_2

    if-nez p1, :cond_2

    .line 531
    const-wide/16 v0, 0x0

    .line 532
    .local v0, "cnId":J
    invoke-static {}, Lru/cn/domain/KidsObject;->isAgeChanged()Z

    move-result v4

    if-nez v4, :cond_1

    .line 533
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->lastChannelPrefs:Lru/cn/domain/LastChannel;

    invoke-virtual {v4}, Lru/cn/domain/LastChannel;->getLastChannel()J

    move-result-wide v0

    .line 536
    :cond_1
    const/4 v4, 0x2

    invoke-static {v5, v4}, Lru/cn/domain/statistics/inetra/InetraTracker;->setSessionParams(II)V

    .line 540
    const-wide/16 v6, 0x0

    cmp-long v4, v0, v6

    if-nez v4, :cond_5

    .line 542
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    sget-object v6, Lru/cn/domain/tv/CurrentCategory$Type;->fav:Lru/cn/domain/tv/CurrentCategory$Type;

    if-ne v4, v6, :cond_4

    const/4 v4, 0x1

    :goto_1
    invoke-direct {p0, v4}, Lru/cn/tv/stb/StbActivity;->playNextChannel(Z)V

    .line 548
    .end local v0    # "cnId":J
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lru/cn/tv/stb/StbActivity;->toggleFullScreen()Z

    .line 549
    return-void

    .line 195
    .end local v2    # "consumed":Z
    .end local v3    # "webView":Landroid/view/View;
    :cond_3
    const-string v4, "StbActivity"

    const-string v6, "normal stb theme"

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    const v4, 0x7f0f000a

    invoke-virtual {p0, v4}, Lru/cn/tv/stb/StbActivity;->setTheme(I)V

    goto/16 :goto_0

    .restart local v0    # "cnId":J
    .restart local v2    # "consumed":Z
    .restart local v3    # "webView":Landroid/view/View;
    :cond_4
    move v4, v5

    .line 542
    goto :goto_1

    .line 544
    :cond_5
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->playLastChannel()V

    goto :goto_2
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 8
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2255
    const/4 v2, 0x0

    .line 2256
    .local v2, "uri":Landroid/net/Uri;
    const/4 v4, 0x0

    .line 2257
    .local v4, "selection":Ljava/lang/String;
    if-eqz p2, :cond_0

    const-string v0, "favourite"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2258
    const-string v4, "favourite"

    .line 2261
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 2286
    :cond_1
    :goto_0
    if-nez v2, :cond_3

    .line 2287
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No uri for loader "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2263
    :pswitch_0
    invoke-static {p0}, Lru/cn/domain/KidsObject;->getAgeFilterIsNeed(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 2264
    .local v7, "age":Ljava/lang/String;
    invoke-static {v7}, Lru/cn/api/provider/TvContentProviderContract;->lastChannelUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2265
    goto :goto_0

    .line 2269
    .end local v7    # "age":Ljava/lang/String;
    :pswitch_1
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragment;->getChannelId()J

    move-result-wide v0

    invoke-static {p0}, Lru/cn/domain/KidsObject;->getAgeFilterIsNeed(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v1, v5}, Lru/cn/api/provider/TvContentProviderContract;->nextChannelUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2270
    goto :goto_0

    .line 2274
    :pswitch_2
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragment;->getChannelId()J

    move-result-wide v0

    invoke-static {p0}, Lru/cn/domain/KidsObject;->getAgeFilterIsNeed(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v1, v5}, Lru/cn/api/provider/TvContentProviderContract;->prevChannelUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2275
    goto :goto_0

    .line 2278
    :pswitch_3
    if-eqz p2, :cond_2

    const-string v0, "number"

    .line 2279
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    :goto_1
    invoke-static {p0}, Lru/cn/domain/KidsObject;->getAgeFilterIsNeed(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 2278
    invoke-static {v0, v1}, Lru/cn/api/provider/TvContentProviderContract;->channelByNumberUri(ILjava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2280
    if-eqz p2, :cond_1

    .line 2281
    const-string v0, "selection"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 2279
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2290
    :cond_3
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    move-object v1, p0

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 2261
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 597
    invoke-super {p0}, Lru/cn/tv/FullScreenActivity;->onDestroy()V

    .line 599
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->nextTelecastView:Lru/cn/tv/player/controller/StbNextTelecastView;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/StbNextTelecastView;->hide()V

    .line 600
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->detachPeerSayControllers()V

    .line 601
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 714
    invoke-super {p0, p1}, Lru/cn/tv/FullScreenActivity;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lru/cn/tv/stb/StbActivity;->volumeControl:Lru/cn/view/VolumeControl;

    invoke-virtual {v3}, Lru/cn/view/VolumeControl;->isShown()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lru/cn/tv/stb/StbActivity;->volumeControl:Lru/cn/view/VolumeControl;

    invoke-virtual {v3, p1}, Lru/cn/view/VolumeControl;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    move v0, v2

    .line 715
    .local v0, "handled":Z
    :goto_0
    if-nez v0, :cond_1

    .line 716
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v1

    const/high16 v3, 0x41a00000    # 20.0f

    cmpg-float v1, v1, v3

    if-gez v1, :cond_1

    .line 717
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->categoryFragmentWrapper:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->isShown()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v1}, Lru/cn/tv/player/controller/StbPlayerController;->isShown()Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lru/cn/tv/stb/StbActivity;->playingAdvertisement:Z

    if-nez v1, :cond_1

    .line 718
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    sget-object v3, Lru/cn/domain/tv/CurrentCategory$Type;->collection:Lru/cn/domain/tv/CurrentCategory$Type;

    if-ne v1, v3, :cond_3

    .line 719
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showCollections()V

    .line 729
    :goto_1
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->foldCategories()V

    .line 730
    invoke-direct {p0, v2}, Lru/cn/tv/stb/StbActivity;->showCategories(Z)V

    .line 735
    :cond_1
    return v0

    .end local v0    # "handled":Z
    :cond_2
    move v0, v1

    .line 714
    goto :goto_0

    .line 721
    .restart local v0    # "handled":Z
    :cond_3
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    sget-object v3, Lru/cn/domain/tv/CurrentCategory$Type;->billing:Lru/cn/domain/tv/CurrentCategory$Type;

    if-eq v1, v3, :cond_4

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    sget-object v3, Lru/cn/domain/tv/CurrentCategory$Type;->setting:Lru/cn/domain/tv/CurrentCategory$Type;

    if-ne v1, v3, :cond_5

    .line 722
    :cond_4
    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->all:Lru/cn/domain/tv/CurrentCategory$Type;

    iput-object v1, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    .line 723
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->setViewMode()V

    .line 726
    :cond_5
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showChannels()V

    goto :goto_1
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 630
    invoke-super {p0, p1, p2}, Lru/cn/tv/FullScreenActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 631
    .local v1, "handled":Z
    if-eqz v1, :cond_0

    .line 709
    :goto_0
    return v4

    .line 635
    :cond_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v6

    if-nez v6, :cond_2

    move v0, v4

    .line 636
    .local v0, "down":Z
    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v6

    if-nez v6, :cond_3

    move v3, v4

    .line 637
    .local v3, "uniqueDown":Z
    :goto_2
    if-eqz v3, :cond_1

    iget-boolean v6, p0, Lru/cn/tv/stb/StbActivity;->playingAdvertisement:Z

    if-eqz v6, :cond_4

    :cond_1
    move v4, v5

    .line 638
    goto :goto_0

    .end local v0    # "down":Z
    .end local v3    # "uniqueDown":Z
    :cond_2
    move v0, v5

    .line 635
    goto :goto_1

    .restart local v0    # "down":Z
    :cond_3
    move v3, v5

    .line 636
    goto :goto_2

    .line 640
    .restart local v3    # "uniqueDown":Z
    :cond_4
    sparse-switch p1, :sswitch_data_0

    :cond_5
    :goto_3
    move v4, v5

    .line 709
    goto :goto_0

    .line 642
    :sswitch_0
    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v6}, Lru/cn/tv/player/controller/StbPlayerController;->isShown()Z

    move-result v6

    if-eqz v6, :cond_6

    if-eqz v3, :cond_6

    .line 643
    iget-object v5, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v5}, Lru/cn/tv/player/controller/StbPlayerController;->hide()V

    goto :goto_0

    .line 647
    :cond_6
    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->categoryFragmentWrapper:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->isShown()Z

    move-result v6

    if-nez v6, :cond_5

    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v6}, Lru/cn/tv/player/controller/StbPlayerController;->isShown()Z

    move-result v6

    if-nez v6, :cond_5

    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->notificationView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->isShown()Z

    move-result v6

    if-nez v6, :cond_5

    .line 648
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showPlayerController()V

    goto :goto_0

    .line 656
    :sswitch_1
    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->categoryFragmentWrapper:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->isShown()Z

    move-result v6

    if-nez v6, :cond_5

    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v6}, Lru/cn/tv/player/controller/StbPlayerController;->isShown()Z

    move-result v6

    if-nez v6, :cond_5

    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->notificationView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->isShown()Z

    move-result v6

    if-nez v6, :cond_5

    .line 657
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showPlayerController()V

    goto :goto_0

    .line 663
    :sswitch_2
    iget-object v5, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v5}, Lru/cn/tv/player/controller/StbPlayerController;->hide()V

    .line 664
    invoke-direct {p0, v4}, Lru/cn/tv/stb/StbActivity;->showCategories(Z)V

    .line 665
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showCurrentCategory()V

    .line 667
    iget-object v5, p0, Lru/cn/tv/stb/StbActivity;->categoryFragment:Lru/cn/tv/stb/categories/CategoryFragment;

    invoke-virtual {v5}, Lru/cn/tv/stb/categories/CategoryFragment;->isFolded()Z

    move-result v5

    if-nez v5, :cond_7

    .line 668
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->foldCategories()V

    .line 670
    :cond_7
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->requestCategoryFocus()V

    goto/16 :goto_0

    .line 676
    :sswitch_3
    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->categoryFragmentWrapper:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->isShown()Z

    move-result v6

    if-nez v6, :cond_5

    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v6}, Lru/cn/tv/player/controller/StbPlayerController;->isShown()Z

    move-result v6

    if-nez v6, :cond_5

    .line 677
    invoke-direct {p0, v4}, Lru/cn/tv/stb/StbActivity;->showCategories(Z)V

    .line 678
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showCurrentCategory()V

    .line 679
    iget-object v5, p0, Lru/cn/tv/stb/StbActivity;->categoryFragment:Lru/cn/tv/stb/categories/CategoryFragment;

    invoke-virtual {v5}, Lru/cn/tv/stb/categories/CategoryFragment;->isFolded()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 680
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->requestCategoryFocus()V

    goto/16 :goto_0

    .line 682
    :cond_8
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->expandCategories()V

    goto/16 :goto_0

    .line 690
    :sswitch_4
    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->bottomSheetBehavior:Landroid/support/design/widget/BottomSheetBehavior;

    if-eqz v6, :cond_5

    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->bottomSheetBehavior:Landroid/support/design/widget/BottomSheetBehavior;

    .line 691
    invoke-virtual {v6}, Landroid/support/design/widget/BottomSheetBehavior;->getState()I

    move-result v6

    const/4 v7, 0x3

    if-ne v6, v7, :cond_5

    .line 692
    iget-object v6, p0, Lru/cn/tv/stb/StbActivity;->channelsFragment:Lru/cn/tv/stb/channels/ChannelsFragment;

    invoke-virtual {v6}, Lru/cn/tv/stb/channels/ChannelsFragment;->getSelectedItemPosition()I

    move-result v2

    .line 693
    .local v2, "position":I
    const/4 v6, -0x1

    if-le v2, v6, :cond_9

    .line 694
    iget-object v5, p0, Lru/cn/tv/stb/StbActivity;->channelsFragment:Lru/cn/tv/stb/channels/ChannelsFragment;

    invoke-virtual {v5}, Lru/cn/tv/stb/channels/ChannelsFragment;->getSelectedItemId()J

    move-result-wide v6

    invoke-direct {p0, v6, v7}, Lru/cn/tv/stb/StbActivity;->blockChannel(J)V

    goto/16 :goto_0

    .line 699
    :cond_9
    invoke-virtual {p0}, Lru/cn/tv/stb/StbActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0e00f1

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    .line 700
    invoke-virtual {p0}, Lru/cn/tv/stb/StbActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0e00f0

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    .line 698
    invoke-static {p0, v4, v6}, Lru/cn/tv/errors/ErrorDialog;->create(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/Dialog;

    move-result-object v4

    .line 701
    invoke-virtual {v4}, Landroid/app/Dialog;->show()V

    goto/16 :goto_3

    .line 640
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x15 -> :sswitch_3
        0x16 -> :sswitch_3
        0x17 -> :sswitch_1
        0x42 -> :sswitch_1
        0x52 -> :sswitch_2
        0x71 -> :sswitch_3
        0x72 -> :sswitch_3
        0xb7 -> :sswitch_4
    .end sparse-switch
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 7
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    const/4 v6, 0x0

    .line 2295
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 2359
    :cond_0
    :goto_0
    return-void

    .line 2297
    :pswitch_0
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 2298
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2300
    const-string v4, "cn_id"

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 2302
    .local v2, "channelId":J
    invoke-static {v6, v6}, Lru/cn/domain/statistics/inetra/InetraTracker;->setSessionParams(II)V

    .line 2305
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v4, v2, v3}, Lru/cn/tv/player/SimplePlayerFragment;->playChannel(J)V

    .line 2307
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2315
    .end local v2    # "channelId":J
    :pswitch_1
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_0

    move-object v4, p2

    .line 2316
    check-cast v4, Landroid/database/CursorWrapper;

    .line 2317
    invoke-virtual {v4}, Landroid/database/CursorWrapper;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/cursor/ChannelCursor;

    .line 2318
    .local v0, "c":Lru/cn/api/provider/cursor/ChannelCursor;
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->moveToFirst()Z

    .line 2320
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getChannelId()J

    move-result-wide v2

    .line 2322
    .restart local v2    # "channelId":J
    invoke-static {v6, v6}, Lru/cn/domain/statistics/inetra/InetraTracker;->setSessionParams(II)V

    .line 2325
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v4, v2, v3}, Lru/cn/tv/player/SimplePlayerFragment;->playChannel(J)V

    .line 2327
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2332
    .end local v0    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    .end local v2    # "channelId":J
    :pswitch_2
    if-eqz p2, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_2

    move-object v4, p2

    .line 2333
    check-cast v4, Landroid/database/CursorWrapper;

    .line 2334
    invoke-virtual {v4}, Landroid/database/CursorWrapper;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/cursor/ChannelCursor;

    .line 2335
    .restart local v0    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->moveToFirst()Z

    .line 2336
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getChannelId()J

    move-result-wide v2

    .line 2337
    .restart local v2    # "channelId":J
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getIsDenied()Z

    move-result v1

    .line 2339
    .local v1, "isDenied":Z
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-eqz v4, :cond_1

    .line 2340
    invoke-static {v6, v6}, Lru/cn/domain/statistics/inetra/InetraTracker;->setSessionParams(II)V

    .line 2344
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v4, v2, v3}, Lru/cn/tv/player/SimplePlayerFragment;->playChannel(J)V

    .line 2345
    if-eqz v1, :cond_1

    .line 2346
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v4}, Lru/cn/tv/player/controller/StbPlayerController;->hide()V

    .line 2349
    :cond_1
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2351
    .end local v0    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    .end local v1    # "isDenied":Z
    .end local v2    # "channelId":J
    :cond_2
    invoke-virtual {p0}, Lru/cn/tv/stb/StbActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0e006f

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    .line 2352
    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 2354
    iget-object v4, p0, Lru/cn/tv/stb/StbActivity;->loadChannelFrom:Lru/cn/domain/tv/CurrentCategory$Type;

    iput-object v4, p0, Lru/cn/tv/stb/StbActivity;->currentCategory:Lru/cn/domain/tv/CurrentCategory$Type;

    .line 2355
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->setViewMode()V

    goto/16 :goto_0

    .line 2295
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 90
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lru/cn/tv/stb/StbActivity;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2364
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 605
    invoke-super {p0, p1}, Lru/cn/tv/FullScreenActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 606
    invoke-direct {p0, p1}, Lru/cn/tv/stb/StbActivity;->handleIntent(Landroid/content/Intent;)Z

    .line 607
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 585
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->receiver:Lru/cn/network/NetworkChangeReceiver;

    invoke-virtual {p0, v0}, Lru/cn/tv/stb/StbActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 586
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->pinCode:Lru/cn/domain/PinCode;

    invoke-virtual {v0}, Lru/cn/domain/PinCode;->resetState()V

    .line 588
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 589
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 592
    :cond_0
    invoke-super {p0}, Lru/cn/tv/FullScreenActivity;->onPause()V

    .line 593
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 553
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->expandCategories()V

    .line 554
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 555
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 556
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->receiver:Lru/cn/network/NetworkChangeReceiver;

    invoke-virtual {p0, v1, v0}, Lru/cn/tv/stb/StbActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 558
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->categoryFragmentWrapper:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 559
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->billingFragmentWrapper:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 560
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->channelsFragmentWrapper:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 561
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->scheduleFragmentWrapper:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 562
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->settingsFragmentWrapper:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 565
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v1}, Lru/cn/tv/player/SimplePlayerFragment;->resetPositionIfNeeded()V

    .line 566
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->simplePlayerFragment:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v1}, Lru/cn/tv/player/SimplePlayerFragment;->play()V

    .line 567
    invoke-super {p0}, Lru/cn/tv/FullScreenActivity;->onResume()V

    .line 569
    iget-boolean v1, p0, Lru/cn/tv/stb/StbActivity;->kidsMode:Z

    if-eqz v1, :cond_0

    invoke-static {}, Lru/cn/domain/KidsObject;->isAgeChanged()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 570
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lru/cn/tv/stb/StbActivity;->switchChannel(I)V

    .line 571
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity;->lastChannelPrefs:Lru/cn/domain/LastChannel;

    invoke-virtual {v1}, Lru/cn/domain/LastChannel;->clearPrevChannel()V

    .line 572
    const/4 v1, 0x0

    invoke-static {v1}, Lru/cn/domain/KidsObject;->setAgeChanged(Z)V

    .line 575
    :cond_0
    new-instance v1, Lru/cn/tv/stb/StbActivity$14;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/StbActivity$14;-><init>(Lru/cn/tv/stb/StbActivity;)V

    invoke-static {v1}, Lru/cn/domain/KidsObject;->setKidsModeListener(Lru/cn/domain/KidsObject$KidsModeListener;)V

    .line 581
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 611
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_0

    .line 612
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/StbPlayerController;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 613
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->playerController:Lru/cn/tv/player/controller/StbPlayerController;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/StbPlayerController;->hide()V

    .line 625
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lru/cn/tv/FullScreenActivity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 615
    :cond_1
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->trackListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 616
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideTrackWidget()V

    goto :goto_0

    .line 617
    :cond_2
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity;->volumeControl:Lru/cn/view/VolumeControl;

    invoke-virtual {v0}, Lru/cn/view/VolumeControl;->isShown()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 618
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->hideVolumeControl()V

    goto :goto_0

    .line 620
    :cond_3
    invoke-direct {p0}, Lru/cn/tv/stb/StbActivity;->showPlayerController()V

    goto :goto_0
.end method
