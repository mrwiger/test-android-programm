.class public final Lcom/my/target/core/presenters/e;
.super Ljava/lang/Object;
.source "InterstitialMraidPresenter.java"

# interfaces
.implements Lcom/my/target/aa$a;
.implements Lcom/my/target/core/presenters/h;


# instance fields
.field private J:Lcom/my/target/core/presenters/h$a;

.field private N:Z

.field private final P:Lcom/my/target/bw;

.field private final Q:Lcom/my/target/ad;

.field private final R:Lcom/my/target/aa;

.field private final S:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private T:Ljava/lang/String;

.field private U:Ljava/lang/Integer;

.field private V:Z

.field private W:Lcom/my/target/ac;

.field private X:Lcom/my/target/bz;

.field private Y:Z

.field private final context:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 107
    const-string v0, "interstitial"

    invoke-static {v0}, Lcom/my/target/aa;->e(Ljava/lang/String;)Lcom/my/target/aa;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/my/target/core/presenters/e;-><init>(Lcom/my/target/aa;Landroid/content/Context;)V

    .line 108
    return-void
.end method

.method private constructor <init>(Lcom/my/target/aa;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/core/presenters/e;->V:Z

    .line 68
    invoke-static {}, Lcom/my/target/ac;->s()Lcom/my/target/ac;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/presenters/e;->W:Lcom/my/target/ac;

    .line 77
    iput-object p1, p0, Lcom/my/target/core/presenters/e;->R:Lcom/my/target/aa;

    .line 78
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/presenters/e;->context:Landroid/content/Context;

    .line 80
    instance-of v0, p2, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 82
    new-instance v1, Ljava/lang/ref/WeakReference;

    move-object v0, p2

    check-cast v0, Landroid/app/Activity;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/my/target/core/presenters/e;->S:Ljava/lang/ref/WeakReference;

    .line 89
    :goto_0
    const-string v0, "loading"

    iput-object v0, p0, Lcom/my/target/core/presenters/e;->T:Ljava/lang/String;

    .line 91
    invoke-static {p2}, Lcom/my/target/ad;->d(Landroid/content/Context;)Lcom/my/target/ad;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/presenters/e;->Q:Lcom/my/target/ad;

    .line 92
    new-instance v0, Lcom/my/target/bw;

    invoke-direct {v0, p2}, Lcom/my/target/bw;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/core/presenters/e;->P:Lcom/my/target/bw;

    .line 93
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->P:Lcom/my/target/bw;

    new-instance v1, Lcom/my/target/core/presenters/e$1;

    invoke-direct {v1, p0}, Lcom/my/target/core/presenters/e$1;-><init>(Lcom/my/target/core/presenters/e;)V

    invoke-virtual {v0, v1}, Lcom/my/target/bw;->setOnCloseListener(Lcom/my/target/bw$a;)V

    .line 102
    invoke-virtual {p1, p0}, Lcom/my/target/aa;->a(Lcom/my/target/aa$a;)V

    .line 103
    return-void

    .line 86
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/my/target/core/presenters/e;->S:Ljava/lang/ref/WeakReference;

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 607
    const-string v0, "MRAID state set to "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 608
    iput-object p1, p0, Lcom/my/target/core/presenters/e;->T:Ljava/lang/String;

    .line 609
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->R:Lcom/my/target/aa;

    invoke-virtual {v0, p1}, Lcom/my/target/aa;->h(Ljava/lang/String;)V

    .line 611
    const-string v0, "hidden"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613
    const-string v0, "InterstitialMraidPresenter: Mraid on close"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 614
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->J:Lcom/my/target/core/presenters/h$a;

    if-eqz v0, :cond_0

    .line 616
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->J:Lcom/my/target/core/presenters/h$a;

    invoke-interface {v0}, Lcom/my/target/core/presenters/h$a;->bh()V

    .line 619
    :cond_0
    return-void
.end method

.method private static a(II)Z
    .locals 1

    .prologue
    .line 533
    and-int v0, p0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/my/target/ac;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 465
    const-string v0, "none"

    invoke-virtual {p1}, Lcom/my/target/ac;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 504
    :goto_0
    return v0

    .line 470
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->S:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 472
    if-nez v0, :cond_1

    move v0, v2

    .line 474
    goto :goto_0

    .line 480
    :cond_1
    :try_start_0
    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    new-instance v4, Landroid/content/ComponentName;

    .line 481
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-direct {v4, v0, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v0, 0x0

    .line 480
    invoke-virtual {v3, v4, v0}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 489
    iget v3, v0, Landroid/content/pm/ActivityInfo;->screenOrientation:I

    .line 490
    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    .line 492
    invoke-virtual {p1}, Lcom/my/target/ac;->t()I

    move-result v0

    if-ne v3, v0, :cond_2

    move v0, v1

    goto :goto_0

    .line 485
    :catch_0
    move-exception v0

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    .line 492
    goto :goto_0

    .line 497
    :cond_3
    iget v3, v0, Landroid/content/pm/ActivityInfo;->configChanges:I

    const/16 v4, 0x80

    .line 498
    invoke-static {v3, v4}, Lcom/my/target/core/presenters/e;->a(II)Z

    move-result v3

    .line 501
    if-eqz v3, :cond_4

    iget v0, v0, Landroid/content/pm/ActivityInfo;->configChanges:I

    const/16 v3, 0x400

    .line 502
    invoke-static {v0, v3}, Lcom/my/target/core/presenters/e;->a(II)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 504
    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)Lcom/my/target/core/presenters/e;
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/my/target/core/presenters/e;

    invoke-direct {v0, p0}, Lcom/my/target/core/presenters/e;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private c(I)Z
    .locals 4

    .prologue
    .line 399
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->S:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 400
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/my/target/core/presenters/e;->W:Lcom/my/target/ac;

    invoke-direct {p0, v1}, Lcom/my/target/core/presenters/e;->a(Lcom/my/target/ac;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 402
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->R:Lcom/my/target/aa;

    const-string v1, "setOrientationProperties"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Attempted to lock orientation to unsupported value: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/my/target/core/presenters/e;->W:Lcom/my/target/ac;

    .line 403
    invoke-virtual {v3}, Lcom/my/target/ac;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 402
    invoke-virtual {v0, v1, v2}, Lcom/my/target/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    const/4 v0, 0x0

    .line 413
    :goto_0
    return v0

    .line 407
    :cond_1
    iget-object v1, p0, Lcom/my/target/core/presenters/e;->U:Ljava/lang/Integer;

    if-nez v1, :cond_2

    .line 409
    invoke-virtual {v0}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/my/target/core/presenters/e;->U:Ljava/lang/Integer;

    .line 412
    :cond_2
    invoke-virtual {v0, p1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 413
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private s()V
    .locals 1

    .prologue
    .line 544
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->J:Lcom/my/target/core/presenters/h$a;

    if-eqz v0, :cond_0

    .line 546
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->J:Lcom/my/target/core/presenters/h$a;

    invoke-interface {v0}, Lcom/my/target/core/presenters/h$a;->g()V

    .line 548
    :cond_0
    return-void
.end method

.method private u()V
    .locals 2

    .prologue
    .line 453
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->S:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 454
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/my/target/core/presenters/e;->U:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 456
    iget-object v1, p0, Lcom/my/target/core/presenters/e;->U:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 458
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/core/presenters/e;->U:Ljava/lang/Integer;

    .line 459
    return-void
.end method

.method private w()V
    .locals 3

    .prologue
    .line 538
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 539
    iget-object v1, p0, Lcom/my/target/core/presenters/e;->Q:Lcom/my/target/ad;

    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v1, v2, v0}, Lcom/my/target/ad;->a(II)V

    .line 540
    return-void
.end method

.method private x()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 552
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->S:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 554
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/my/target/core/presenters/e;->X:Lcom/my/target/bz;

    if-nez v1, :cond_1

    :cond_0
    move v0, v2

    .line 602
    :goto_0
    return v0

    .line 570
    :cond_1
    iget-object v1, p0, Lcom/my/target/core/presenters/e;->X:Lcom/my/target/bz;

    .line 575
    :goto_1
    invoke-virtual {v1}, Landroid/view/View;->isHardwareAccelerated()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 576
    invoke-virtual {v1}, Landroid/view/View;->getLayerType()I

    move-result v3

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_3

    :cond_2
    move v0, v2

    .line 578
    goto :goto_0

    .line 582
    :cond_3
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    instance-of v3, v3, Landroid/view/View;

    if-eqz v3, :cond_4

    .line 587
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    goto :goto_1

    .line 595
    :cond_4
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 597
    if-eqz v0, :cond_6

    .line 599
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v1, 0x1000000

    and-int/2addr v0, v1

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_0

    :cond_6
    move v0, v2

    .line 602
    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/my/target/core/presenters/h$a;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/my/target/core/presenters/e;->J:Lcom/my/target/core/presenters/h$a;

    .line 137
    return-void
.end method

.method public final a(Lcom/my/target/dv;Lcom/my/target/core/models/banners/f;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 113
    invoke-virtual {p1}, Lcom/my/target/dv;->getRawData()Lorg/json/JSONObject;

    move-result-object v0

    .line 114
    invoke-virtual {p1}, Lcom/my/target/dv;->getHtml()Ljava/lang/String;

    move-result-object v1

    .line 115
    if-nez v0, :cond_1

    .line 117
    invoke-direct {p0}, Lcom/my/target/core/presenters/e;->s()V

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    if-nez v1, :cond_2

    .line 122
    invoke-direct {p0}, Lcom/my/target/core/presenters/e;->s()V

    goto :goto_0

    .line 126
    :cond_2
    invoke-virtual {p2}, Lcom/my/target/core/models/banners/f;->getMraidSource()Ljava/lang/String;

    move-result-object v0

    .line 127
    if-eqz v0, :cond_0

    .line 1305
    new-instance v1, Lcom/my/target/bz;

    iget-object v2, p0, Lcom/my/target/core/presenters/e;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/my/target/bz;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/my/target/core/presenters/e;->X:Lcom/my/target/bz;

    .line 1306
    iget-object v1, p0, Lcom/my/target/core/presenters/e;->R:Lcom/my/target/aa;

    iget-object v2, p0, Lcom/my/target/core/presenters/e;->X:Lcom/my/target/bz;

    invoke-virtual {v1, v2}, Lcom/my/target/aa;->a(Lcom/my/target/bz;)V

    .line 1307
    iget-object v1, p0, Lcom/my/target/core/presenters/e;->P:Lcom/my/target/bw;

    iget-object v2, p0, Lcom/my/target/core/presenters/e;->X:Lcom/my/target/bz;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Lcom/my/target/bw;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1308
    iget-object v1, p0, Lcom/my/target/core/presenters/e;->R:Lcom/my/target/aa;

    invoke-virtual {v1, v0}, Lcom/my/target/aa;->f(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(FF)Z
    .locals 3
    .param p1, "remain"    # F
    .param p2, "duration"    # F

    .prologue
    const/4 v1, 0x0

    .line 341
    iget-boolean v0, p0, Lcom/my/target/core/presenters/e;->Y:Z

    if-nez v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->R:Lcom/my/target/aa;

    const-string v1, "playheadEvent"

    const-string v2, "Calling VPAID command before VPAID init"

    invoke-virtual {v0, v1, v2}, Lcom/my/target/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    const/4 v0, 0x0

    .line 351
    :goto_0
    return v0

    .line 347
    :cond_0
    cmpl-float v0, p1, v1

    if-ltz v0, :cond_1

    cmpl-float v0, p2, v1

    if-ltz v0, :cond_1

    iget-object v0, p0, Lcom/my/target/core/presenters/e;->J:Lcom/my/target/core/presenters/h$a;

    if-eqz v0, :cond_1

    .line 349
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->J:Lcom/my/target/core/presenters/h$a;

    iget-object v1, p0, Lcom/my/target/core/presenters/e;->context:Landroid/content/Context;

    invoke-interface {v0, p1, p2, v1}, Lcom/my/target/core/presenters/h$a;->a(FFLandroid/content/Context;)V

    .line 351
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Landroid/webkit/JsResult;)Z
    .locals 2
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "result"    # Landroid/webkit/JsResult;

    .prologue
    .line 263
    const-string v0, "JS Alert: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 264
    invoke-virtual {p2}, Landroid/webkit/JsResult;->confirm()V

    .line 265
    const/4 v0, 0x1

    return v0
.end method

.method public final a(ZLcom/my/target/ac;)Z
    .locals 5
    .param p1, "allowOrientationChange"    # Z
    .param p2, "forceOrientation"    # Lcom/my/target/ac;

    .prologue
    const/4 v1, 0x0

    .line 208
    invoke-direct {p0, p2}, Lcom/my/target/core/presenters/e;->a(Lcom/my/target/ac;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->R:Lcom/my/target/aa;

    const-string v2, "setOrientationProperties"

    const-string v3, "Unable to force orientation to "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/my/target/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 1445
    :goto_0
    return v0

    .line 214
    :cond_0
    iput-boolean p1, p0, Lcom/my/target/core/presenters/e;->V:Z

    .line 215
    iput-object p2, p0, Lcom/my/target/core/presenters/e;->W:Lcom/my/target/ac;

    .line 1419
    const-string v0, "none"

    iget-object v2, p0, Lcom/my/target/core/presenters/e;->W:Lcom/my/target/ac;

    invoke-virtual {v2}, Lcom/my/target/ac;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1421
    iget-boolean v0, p0, Lcom/my/target/core/presenters/e;->V:Z

    if-eqz v0, :cond_1

    .line 1425
    invoke-direct {p0}, Lcom/my/target/core/presenters/e;->u()V

    .line 1447
    const/4 v0, 0x1

    .line 217
    goto :goto_0

    .line 1429
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->S:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1430
    if-nez v0, :cond_2

    .line 1432
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->R:Lcom/my/target/aa;

    const-string v2, "setOrientationProperties"

    const-string v3, "Unable to set MRAID expand orientation to \'none\'; expected passed in Activity Context."

    invoke-virtual {v0, v2, v3}, Lcom/my/target/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 1434
    goto :goto_0

    .line 1439
    :cond_2
    invoke-static {v0}, Lcom/my/target/cm;->a(Landroid/app/Activity;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/my/target/core/presenters/e;->c(I)Z

    move-result v0

    goto :goto_0

    .line 1445
    :cond_3
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->W:Lcom/my/target/ac;

    invoke-virtual {v0}, Lcom/my/target/ac;->t()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/my/target/core/presenters/e;->c(I)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 296
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->J:Lcom/my/target/core/presenters/h$a;

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->J:Lcom/my/target/core/presenters/h$a;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/my/target/core/presenters/e;->P:Lcom/my/target/bw;

    invoke-virtual {v2}, Lcom/my/target/bw;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/my/target/core/presenters/h$a;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 300
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 3
    .param p1, "shouldUseCustomClose"    # Z

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 284
    iget-object v2, p0, Lcom/my/target/core/presenters/e;->P:Lcom/my/target/bw;

    invoke-virtual {v2}, Lcom/my/target/bw;->bb()Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v0

    .line 285
    :goto_0
    if-ne p1, v2, :cond_1

    .line 291
    :goto_1
    return-void

    :cond_0
    move v2, v1

    .line 284
    goto :goto_0

    .line 290
    :cond_1
    iget-object v2, p0, Lcom/my/target/core/presenters/e;->P:Lcom/my/target/bw;

    if-nez p1, :cond_2

    :goto_2
    invoke-virtual {v2, v0}, Lcom/my/target/bw;->setCloseVisible(Z)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 174
    iget-boolean v0, p0, Lcom/my/target/core/presenters/e;->N:Z

    if-nez v0, :cond_0

    .line 176
    iput-boolean v1, p0, Lcom/my/target/core/presenters/e;->N:Z

    .line 179
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->X:Lcom/my/target/bz;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->X:Lcom/my/target/bz;

    invoke-virtual {v0, v1}, Lcom/my/target/bz;->j(Z)V

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->P:Lcom/my/target/bw;

    invoke-virtual {v0}, Lcom/my/target/bw;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 185
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    .line 187
    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/my/target/core/presenters/e;->P:Lcom/my/target/bw;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 190
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->R:Lcom/my/target/aa;

    invoke-virtual {v0}, Lcom/my/target/aa;->detach()V

    .line 191
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->X:Lcom/my/target/bz;

    if-eqz v0, :cond_2

    .line 193
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->X:Lcom/my/target/bz;

    invoke-virtual {v0}, Lcom/my/target/bz;->destroy()V

    .line 194
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/core/presenters/e;->X:Lcom/my/target/bz;

    .line 196
    :cond_2
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->P:Lcom/my/target/bw;

    invoke-virtual {v0}, Lcom/my/target/bw;->removeAllViews()V

    .line 197
    return-void
.end method

.method public final k(Ljava/lang/String;)Z
    .locals 3
    .param p1, "event"    # Ljava/lang/String;

    .prologue
    .line 326
    iget-boolean v0, p0, Lcom/my/target/core/presenters/e;->Y:Z

    if-nez v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->R:Lcom/my/target/aa;

    const-string v1, "vpaidEvent"

    const-string v2, "Calling VPAID command before VPAID init"

    invoke-virtual {v0, v1, v2}, Lcom/my/target/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    const/4 v0, 0x0

    .line 335
    :goto_0
    return v0

    .line 331
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->J:Lcom/my/target/core/presenters/h$a;

    if-eqz v0, :cond_1

    .line 333
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->J:Lcom/my/target/core/presenters/h$a;

    iget-object v1, p0, Lcom/my/target/core/presenters/e;->context:Landroid/content/Context;

    invoke-interface {v0, p1, v1}, Lcom/my/target/core/presenters/h$a;->b(Ljava/lang/String;Landroid/content/Context;)V

    .line 335
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final o()Landroid/view/View;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->P:Lcom/my/target/bw;

    return-object v0
.end method

.method public final onClose()V
    .locals 0

    .prologue
    .line 278
    invoke-virtual {p0}, Lcom/my/target/core/presenters/e;->v()V

    .line 279
    return-void
.end method

.method public final onConsoleMessage(Landroid/webkit/ConsoleMessage;)Z
    .locals 2
    .param p1, "consoleMessage"    # Landroid/webkit/ConsoleMessage;

    .prologue
    .line 271
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Console message: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/webkit/ConsoleMessage;->message()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 272
    const/4 v0, 0x1

    return v0
.end method

.method public final onVisibilityChanged(Z)V
    .locals 1
    .param p1, "isVisible"    # Z

    .prologue
    .line 257
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->R:Lcom/my/target/aa;

    invoke-virtual {v0, p1}, Lcom/my/target/aa;->a(Z)V

    .line 258
    return-void
.end method

.method public final p()V
    .locals 2

    .prologue
    .line 233
    const-string v0, "default"

    iput-object v0, p0, Lcom/my/target/core/presenters/e;->T:Ljava/lang/String;

    .line 234
    invoke-direct {p0}, Lcom/my/target/core/presenters/e;->w()V

    .line 236
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 237
    invoke-direct {p0}, Lcom/my/target/core/presenters/e;->x()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 239
    const-string v1, "\'inlineVideo\'"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241
    :cond_0
    const-string v1, "\'vpaid\'"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 242
    iget-object v1, p0, Lcom/my/target/core/presenters/e;->R:Lcom/my/target/aa;

    invoke-virtual {v1, v0}, Lcom/my/target/aa;->a(Ljava/util/ArrayList;)V

    .line 243
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->R:Lcom/my/target/aa;

    const-string v1, "interstitial"

    invoke-virtual {v0, v1}, Lcom/my/target/aa;->g(Ljava/lang/String;)V

    .line 244
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->R:Lcom/my/target/aa;

    iget-object v1, p0, Lcom/my/target/core/presenters/e;->R:Lcom/my/target/aa;

    invoke-virtual {v1}, Lcom/my/target/aa;->isVisible()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/aa;->a(Z)V

    .line 245
    const-string v0, "default"

    invoke-direct {p0, v0}, Lcom/my/target/core/presenters/e;->a(Ljava/lang/String;)V

    .line 246
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->R:Lcom/my/target/aa;

    invoke-virtual {v0}, Lcom/my/target/aa;->m()V

    .line 247
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->R:Lcom/my/target/aa;

    iget-object v1, p0, Lcom/my/target/core/presenters/e;->Q:Lcom/my/target/ad;

    invoke-virtual {v0, v1}, Lcom/my/target/aa;->a(Lcom/my/target/ad;)V

    .line 248
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->J:Lcom/my/target/core/presenters/h$a;

    if-eqz v0, :cond_1

    .line 250
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->J:Lcom/my/target/core/presenters/h$a;

    iget-object v1, p0, Lcom/my/target/core/presenters/e;->context:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/my/target/core/presenters/h$a;->a(Landroid/content/Context;)V

    .line 252
    :cond_1
    return-void
.end method

.method public final pause()V
    .locals 2

    .prologue
    .line 156
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/core/presenters/e;->N:Z

    .line 157
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->X:Lcom/my/target/bz;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->X:Lcom/my/target/bz;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/my/target/bz;->j(Z)V

    .line 161
    :cond_0
    return-void
.end method

.method public final q()V
    .locals 0

    .prologue
    .line 314
    invoke-direct {p0}, Lcom/my/target/core/presenters/e;->w()V

    .line 315
    return-void
.end method

.method public final r()V
    .locals 1

    .prologue
    .line 320
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/core/presenters/e;->Y:Z

    .line 321
    return-void
.end method

.method public final resume()V
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/core/presenters/e;->N:Z

    .line 166
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->X:Lcom/my/target/bz;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->X:Lcom/my/target/bz;

    invoke-virtual {v0}, Lcom/my/target/bz;->onResume()V

    .line 170
    :cond_0
    return-void
.end method

.method final v()V
    .locals 2

    .prologue
    .line 510
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->X:Lcom/my/target/bz;

    if-nez v0, :cond_1

    .line 529
    :cond_0
    :goto_0
    return-void

    .line 516
    :cond_1
    const-string v0, "loading"

    iget-object v1, p0, Lcom/my/target/core/presenters/e;->T:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "hidden"

    iget-object v1, p0, Lcom/my/target/core/presenters/e;->T:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 522
    invoke-direct {p0}, Lcom/my/target/core/presenters/e;->u()V

    .line 524
    const-string v0, "default"

    iget-object v1, p0, Lcom/my/target/core/presenters/e;->T:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 526
    iget-object v0, p0, Lcom/my/target/core/presenters/e;->P:Lcom/my/target/bw;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/my/target/bw;->setVisibility(I)V

    .line 527
    const-string v0, "hidden"

    invoke-direct {p0, v0}, Lcom/my/target/core/presenters/e;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
