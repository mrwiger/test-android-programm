.class public final Lcom/my/target/core/engines/a;
.super Ljava/lang/Object;
.source "NativeAdEngine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/core/engines/a$b;,
        Lcom/my/target/core/engines/a$a;
    }
.end annotation


# instance fields
.field private final H:Lcom/my/target/nativeads/NativeAd;

.field private final I:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/core/models/banners/b;",
            ">;"
        }
    .end annotation
.end field

.field private final J:Lcom/my/target/core/engines/a$a;

.field private final K:Lcom/my/target/ce;

.field private final L:Lcom/my/target/core/controllers/b;

.field private M:Z

.field private final y:Lcom/my/target/core/models/banners/a;


# direct methods
.method private constructor <init>(Lcom/my/target/nativeads/NativeAd;Lcom/my/target/core/models/banners/a;)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/core/engines/a;->I:Ljava/util/ArrayList;

    .line 35
    new-instance v0, Lcom/my/target/core/engines/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/my/target/core/engines/a$a;-><init>(Lcom/my/target/core/engines/a;B)V

    iput-object v0, p0, Lcom/my/target/core/engines/a;->J:Lcom/my/target/core/engines/a$a;

    .line 37
    invoke-static {}, Lcom/my/target/ce;->bw()Lcom/my/target/ce;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/engines/a;->K:Lcom/my/target/ce;

    .line 43
    iput-object p1, p0, Lcom/my/target/core/engines/a;->H:Lcom/my/target/nativeads/NativeAd;

    .line 44
    iput-object p2, p0, Lcom/my/target/core/engines/a;->y:Lcom/my/target/core/models/banners/a;

    .line 45
    new-instance v0, Lcom/my/target/core/engines/a$b;

    invoke-direct {v0, p0}, Lcom/my/target/core/engines/a$b;-><init>(Lcom/my/target/core/engines/a;)V

    invoke-static {p2, v0}, Lcom/my/target/core/controllers/b;->a(Lcom/my/target/core/models/banners/a;Lcom/my/target/core/controllers/b$a;)Lcom/my/target/core/controllers/b;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/engines/a;->L:Lcom/my/target/core/controllers/b;

    .line 46
    return-void
.end method

.method static synthetic a(Lcom/my/target/core/engines/a;)Lcom/my/target/core/controllers/b;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/my/target/core/engines/a;->L:Lcom/my/target/core/controllers/b;

    return-object v0
.end method

.method public static a(Lcom/my/target/nativeads/NativeAd;Lcom/my/target/core/models/banners/a;)Lcom/my/target/core/engines/a;
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/my/target/core/engines/a;

    invoke-direct {v0, p0, p1}, Lcom/my/target/core/engines/a;-><init>(Lcom/my/target/nativeads/NativeAd;Lcom/my/target/core/models/banners/a;)V

    return-object v0
.end method

.method static synthetic a(Lcom/my/target/core/engines/a;Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 26
    .line 1064
    iget-object v0, p0, Lcom/my/target/core/engines/a;->y:Lcom/my/target/core/models/banners/a;

    invoke-virtual {v0}, Lcom/my/target/core/models/banners/a;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    .line 1065
    const-string v1, "playbackStarted"

    invoke-virtual {v0, v1}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 1066
    iget-object v0, p0, Lcom/my/target/core/engines/a;->H:Lcom/my/target/nativeads/NativeAd;

    invoke-virtual {v0}, Lcom/my/target/nativeads/NativeAd;->getListener()Lcom/my/target/nativeads/NativeAd$NativeAdListener;

    move-result-object v0

    .line 1067
    if-eqz v0, :cond_0

    .line 1069
    iget-object v1, p0, Lcom/my/target/core/engines/a;->H:Lcom/my/target/nativeads/NativeAd;

    invoke-interface {v0, v1}, Lcom/my/target/nativeads/NativeAd$NativeAdListener;->onShow(Lcom/my/target/nativeads/NativeAd;)V

    .line 1071
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/engines/a;->L:Lcom/my/target/core/controllers/b;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/b;->q()I

    move-result v0

    .line 1072
    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 1074
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/engines/a;->L:Lcom/my/target/core/controllers/b;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/b;->t()[I

    move-result-object v2

    .line 1075
    if-eqz v2, :cond_3

    .line 1077
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget v0, v2, v1

    .line 1079
    iget-object v4, p0, Lcom/my/target/core/engines/a;->y:Lcom/my/target/core/models/banners/a;

    invoke-virtual {v4}, Lcom/my/target/core/models/banners/a;->getNativeAdCards()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/b;

    .line 1080
    iget-boolean v4, p0, Lcom/my/target/core/engines/a;->M:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/my/target/core/engines/a;->I:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1083
    if-eqz v0, :cond_2

    .line 1085
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/b;->getStatHolder()Lcom/my/target/ar;

    move-result-object v4

    .line 1086
    const-string v5, "playbackStarted"

    invoke-virtual {v4, v5}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 1087
    iget-object v4, p0, Lcom/my/target/core/engines/a;->I:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1077
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 26
    :cond_3
    return-void
.end method

.method static synthetic a(Lcom/my/target/core/engines/a;Lcom/my/target/ah;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1097
    if-eqz p1, :cond_0

    .line 1099
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1100
    if-eqz v0, :cond_0

    .line 1102
    iget-object v1, p0, Lcom/my/target/core/engines/a;->K:Lcom/my/target/ce;

    invoke-virtual {v1, p1, v0}, Lcom/my/target/ce;->a(Lcom/my/target/ah;Landroid/content/Context;)V

    .line 1105
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/engines/a;->H:Lcom/my/target/nativeads/NativeAd;

    invoke-virtual {v0}, Lcom/my/target/nativeads/NativeAd;->getListener()Lcom/my/target/nativeads/NativeAd$NativeAdListener;

    move-result-object v0

    .line 1106
    if-eqz v0, :cond_1

    .line 1108
    iget-object v1, p0, Lcom/my/target/core/engines/a;->H:Lcom/my/target/nativeads/NativeAd;

    invoke-interface {v0, v1}, Lcom/my/target/nativeads/NativeAd$NativeAdListener;->onClick(Lcom/my/target/nativeads/NativeAd;)V

    .line 26
    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/my/target/core/engines/a;)Lcom/my/target/core/engines/a$a;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/my/target/core/engines/a;->J:Lcom/my/target/core/engines/a$a;

    return-object v0
.end method

.method static synthetic c(Lcom/my/target/core/engines/a;)Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/my/target/core/engines/a;->M:Z

    return v0
.end method

.method static synthetic d(Lcom/my/target/core/engines/a;)Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/core/engines/a;->M:Z

    return v0
.end method

.method static synthetic e(Lcom/my/target/core/engines/a;)Lcom/my/target/core/models/banners/a;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/my/target/core/engines/a;->y:Lcom/my/target/core/models/banners/a;

    return-object v0
.end method

.method static synthetic f(Lcom/my/target/core/engines/a;)Lcom/my/target/nativeads/NativeAd;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/my/target/core/engines/a;->H:Lcom/my/target/nativeads/NativeAd;

    return-object v0
.end method

.method static synthetic g(Lcom/my/target/core/engines/a;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/my/target/core/engines/a;->I:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public final registerView(Landroid/view/View;Ljava/util/List;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 50
    .local p2, "clickableViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-virtual {p0}, Lcom/my/target/core/engines/a;->unregisterView()V

    .line 51
    iget-object v0, p0, Lcom/my/target/core/engines/a;->L:Lcom/my/target/core/controllers/b;

    invoke-virtual {v0, p1, p2}, Lcom/my/target/core/controllers/b;->registerView(Landroid/view/View;Ljava/util/List;)V

    .line 52
    sget-object v0, Lcom/my/target/ck;->km:Lcom/my/target/ck;

    iget-object v1, p0, Lcom/my/target/core/engines/a;->J:Lcom/my/target/core/engines/a$a;

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->d(Ljava/lang/Runnable;)V

    .line 53
    iget-object v0, p0, Lcom/my/target/core/engines/a;->J:Lcom/my/target/core/engines/a$a;

    invoke-virtual {v0}, Lcom/my/target/core/engines/a$a;->run()V

    .line 54
    return-void
.end method

.method public final unregisterView()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/my/target/core/engines/a;->L:Lcom/my/target/core/controllers/b;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/b;->unregisterView()V

    .line 59
    sget-object v0, Lcom/my/target/ck;->km:Lcom/my/target/ck;

    iget-object v1, p0, Lcom/my/target/core/engines/a;->J:Lcom/my/target/core/engines/a$a;

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    .line 60
    return-void
.end method
