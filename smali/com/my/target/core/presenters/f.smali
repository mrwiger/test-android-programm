.class public final Lcom/my/target/core/presenters/f;
.super Ljava/lang/Object;
.source "InterstitialPromoPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/core/presenters/f$b;,
        Lcom/my/target/core/presenters/f$c;,
        Lcom/my/target/core/presenters/f$a;
    }
.end annotation


# instance fields
.field private final aa:Lcom/my/target/ee;

.field private final ab:Lcom/my/target/core/presenters/f$c;

.field private final ac:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/core/models/banners/e;",
            ">;"
        }
    .end annotation
.end field

.field private final ad:Lcom/my/target/ee$b;

.field private ae:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/my/target/ap;",
            ">;"
        }
    .end annotation
.end field

.field private af:Z

.field private ag:Z

.field private ah:Z

.field private ai:J

.field private aj:J

.field private ak:Z

.field private al:Lcom/my/target/core/presenters/f$a;

.field private allowClose:Z

.field private am:Z

.field private duration:F

.field private final j:Lcom/my/target/core/models/banners/h;

.field private videoBanner:Lcom/my/target/aj;


# direct methods
.method private constructor <init>(Lcom/my/target/core/models/banners/h;Landroid/content/Context;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/my/target/core/presenters/f;->ac:Ljava/util/ArrayList;

    .line 50
    iput-boolean v0, p0, Lcom/my/target/core/presenters/f;->allowClose:Z

    .line 51
    iput-boolean v0, p0, Lcom/my/target/core/presenters/f;->ag:Z

    .line 52
    iput-boolean v1, p0, Lcom/my/target/core/presenters/f;->ah:Z

    .line 58
    iput-boolean v0, p0, Lcom/my/target/core/presenters/f;->am:Z

    .line 63
    iput-object p1, p0, Lcom/my/target/core/presenters/f;->j:Lcom/my/target/core/models/banners/h;

    .line 64
    new-instance v2, Lcom/my/target/core/presenters/f$b;

    invoke-direct {v2, p0, v1}, Lcom/my/target/core/presenters/f$b;-><init>(Lcom/my/target/core/presenters/f;B)V

    iput-object v2, p0, Lcom/my/target/core/presenters/f;->ad:Lcom/my/target/ee$b;

    .line 1230
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getInterstitialAdCards()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 1232
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v2, v2, 0xf

    const/4 v3, 0x3

    if-lt v2, v3, :cond_6

    .line 1234
    :goto_0
    if-eqz v0, :cond_7

    .line 1236
    new-instance v0, Lcom/my/target/eo;

    invoke-direct {v0, p2}, Lcom/my/target/eo;-><init>(Landroid/content/Context;)V

    .line 66
    :goto_1
    iput-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    .line 68
    new-instance v0, Lcom/my/target/core/presenters/f$c;

    iget-object v2, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    invoke-direct {v0, v2}, Lcom/my/target/core/presenters/f$c;-><init>(Lcom/my/target/ee;)V

    iput-object v0, p0, Lcom/my/target/core/presenters/f;->ab:Lcom/my/target/core/presenters/f$c;

    .line 69
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    invoke-virtual {v0, p1}, Lcom/my/target/ee;->setBanner(Lcom/my/target/core/models/banners/h;)V

    .line 70
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    iget-object v2, p0, Lcom/my/target/core/presenters/f;->ad:Lcom/my/target/ee$b;

    invoke-virtual {v0, v2}, Lcom/my/target/ee;->setInterstitialPromoViewListener(Lcom/my/target/ee$b;)V

    .line 72
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getClickArea()Lcom/my/target/af;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/my/target/ee;->setClickArea(Lcom/my/target/af;)V

    .line 74
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getVideoBanner()Lcom/my/target/aj;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/presenters/f;->videoBanner:Lcom/my/target/aj;

    .line 76
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->videoBanner:Lcom/my/target/aj;

    if-eqz v0, :cond_2

    .line 78
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->videoBanner:Lcom/my/target/aj;

    .line 2176
    invoke-virtual {v0}, Lcom/my/target/aj;->isAllowBackButton()Z

    move-result v2

    iput-boolean v2, p0, Lcom/my/target/core/presenters/f;->am:Z

    .line 2177
    invoke-virtual {v0}, Lcom/my/target/aj;->isAutoPlay()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2179
    iput-wide v4, p0, Lcom/my/target/core/presenters/f;->aj:J

    .line 2181
    :cond_0
    invoke-virtual {v0}, Lcom/my/target/aj;->isAllowClose()Z

    move-result v2

    iput-boolean v2, p0, Lcom/my/target/core/presenters/f;->allowClose:Z

    .line 2184
    iget-boolean v2, p0, Lcom/my/target/core/presenters/f;->allowClose:Z

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/my/target/aj;->getAllowCloseDelay()F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_1

    invoke-virtual {v0}, Lcom/my/target/aj;->isAutoPlay()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2186
    const-string v2, "banner is allowed to close"

    invoke-static {v2}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 2187
    iget-object v2, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    invoke-virtual {v2}, Lcom/my/target/ee;->G()V

    .line 2190
    :cond_1
    invoke-virtual {v0}, Lcom/my/target/aj;->getDuration()F

    move-result v2

    iput v2, p0, Lcom/my/target/core/presenters/f;->duration:F

    .line 2191
    invoke-virtual {v0}, Lcom/my/target/aj;->isAutoMute()Z

    move-result v2

    iput-boolean v2, p0, Lcom/my/target/core/presenters/f;->af:Z

    .line 2192
    iget-boolean v2, p0, Lcom/my/target/core/presenters/f;->af:Z

    if-eqz v2, :cond_a

    .line 2194
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    invoke-virtual {v0, v1}, Lcom/my/target/ee;->e(I)V

    .line 80
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->videoBanner:Lcom/my/target/aj;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/my/target/core/presenters/f;->videoBanner:Lcom/my/target/aj;

    invoke-virtual {v0}, Lcom/my/target/aj;->isAutoPlay()Z

    move-result v0

    if-nez v0, :cond_4

    .line 82
    :cond_3
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getAllowCloseDelay()F

    move-result v0

    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v1

    float-to-long v0, v0

    iput-wide v0, p0, Lcom/my/target/core/presenters/f;->ai:J

    .line 83
    iget-wide v0, p0, Lcom/my/target/core/presenters/f;->ai:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_c

    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "banner will be allowed to close in "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/my/target/core/presenters/f;->ai:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " millis"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 87
    iget-wide v0, p0, Lcom/my/target/core/presenters/f;->ai:J

    invoke-direct {p0, v0, v1}, Lcom/my/target/core/presenters/f;->a(J)V

    .line 95
    :cond_4
    :goto_3
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getInterstitialAdCards()Ljava/util/List;

    move-result-object v0

    .line 97
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 99
    invoke-direct {p0, v0}, Lcom/my/target/core/presenters/f;->a(Ljava/util/List;)V

    .line 101
    :cond_5
    return-void

    :cond_6
    move v0, v1

    .line 1232
    goto/16 :goto_0

    .line 1240
    :cond_7
    new-instance v0, Lcom/my/target/em;

    invoke-direct {v0, p2}, Lcom/my/target/em;-><init>(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 1243
    :cond_8
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getVideoBanner()Lcom/my/target/aj;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getStyle()I

    move-result v2

    if-ne v2, v0, :cond_9

    .line 1245
    new-instance v0, Lcom/my/target/ei;

    invoke-direct {v0, p2}, Lcom/my/target/ei;-><init>(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 1249
    :cond_9
    new-instance v0, Lcom/my/target/eb;

    invoke-direct {v0, p2}, Lcom/my/target/eb;-><init>(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 2198
    :cond_a
    invoke-virtual {v0}, Lcom/my/target/aj;->isAutoPlay()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2200
    invoke-direct {p0, p2}, Lcom/my/target/core/presenters/f;->d(Landroid/content/Context;)V

    .line 2202
    :cond_b
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/my/target/ee;->e(I)V

    goto :goto_2

    .line 91
    :cond_c
    const-string v0, "banner is allowed to close"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    invoke-virtual {v0}, Lcom/my/target/ee;->G()V

    goto :goto_3
.end method

.method static synthetic a(Lcom/my/target/core/presenters/f;)Lcom/my/target/core/presenters/f$a;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->al:Lcom/my/target/core/presenters/f$a;

    return-object v0
.end method

.method public static a(Lcom/my/target/core/models/banners/h;Landroid/content/Context;)Lcom/my/target/core/presenters/f;
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/my/target/core/presenters/f;

    invoke-direct {v0, p0, p1}, Lcom/my/target/core/presenters/f;-><init>(Lcom/my/target/core/models/banners/h;Landroid/content/Context;)V

    return-object v0
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 222
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    iget-object v1, p0, Lcom/my/target/core/presenters/f;->ab:Lcom/my/target/core/presenters/f$c;

    invoke-virtual {v0, v1}, Lcom/my/target/ee;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 223
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/my/target/core/presenters/f;->aj:J

    .line 224
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    iget-object v1, p0, Lcom/my/target/core/presenters/f;->ab:Lcom/my/target/core/presenters/f$c;

    invoke-virtual {v0, v1, p1, p2}, Lcom/my/target/ee;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 225
    return-void
.end method

.method static synthetic a(Lcom/my/target/core/presenters/f;F)V
    .locals 3

    .prologue
    .line 30
    .line 3310
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->ae:Ljava/util/Set;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/my/target/core/presenters/f;->ae:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3312
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->ae:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 3313
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3315
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/ap;

    .line 3317
    invoke-virtual {v0}, Lcom/my/target/ap;->Z()F

    move-result v2

    cmpg-float v2, v2, p1

    if-gtz v2, :cond_0

    .line 3319
    iget-object v2, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    invoke-virtual {v2}, Lcom/my/target/ee;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/my/target/cl;->a(Lcom/my/target/aq;Landroid/content/Context;)V

    .line 3320
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 30
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/my/target/core/presenters/f;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/my/target/core/presenters/f;->e(Landroid/content/Context;)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 208
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    invoke-virtual {v0}, Lcom/my/target/ee;->getNumbersOfCurrentShowingCards()[I

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget v0, v2, v1

    .line 210
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    if-ltz v0, :cond_0

    .line 212
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/e;

    .line 213
    iget-object v4, p0, Lcom/my/target/core/presenters/f;->ac:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/e;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    const-string v4, "playbackStarted"

    invoke-virtual {v0, v4}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v4, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    .line 215
    invoke-virtual {v4}, Lcom/my/target/ee;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 214
    invoke-static {v0, v4}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 208
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 218
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/my/target/core/presenters/f;Z)Z
    .locals 0

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/my/target/core/presenters/f;->af:Z

    return p1
.end method

.method static synthetic b(Lcom/my/target/core/presenters/f;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->ac:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic b(Lcom/my/target/core/presenters/f;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/my/target/core/presenters/f;->d(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    return-object v0
.end method

.method static synthetic d(Lcom/my/target/core/presenters/f;)Lcom/my/target/core/models/banners/h;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->j:Lcom/my/target/core/models/banners/h;

    return-object v0
.end method

.method private d(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 276
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    .line 277
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 278
    if-eqz v0, :cond_0

    .line 280
    iget-object v1, p0, Lcom/my/target/core/presenters/f;->ad:Lcom/my/target/ee$b;

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 284
    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/my/target/core/presenters/f;)Lcom/my/target/aj;
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/core/presenters/f;->videoBanner:Lcom/my/target/aj;

    return-object v0
.end method

.method private e(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 288
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    .line 289
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 290
    if-eqz v0, :cond_0

    .line 292
    iget-object v1, p0, Lcom/my/target/core/presenters/f;->ad:Lcom/my/target/ee$b;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 294
    :cond_0
    return-void
.end method

.method static synthetic f(Lcom/my/target/core/presenters/f;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/my/target/core/presenters/f;->af:Z

    return v0
.end method

.method static synthetic g(Lcom/my/target/core/presenters/f;)V
    .locals 2

    .prologue
    .line 30
    .line 2262
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    invoke-virtual {v0}, Lcom/my/target/ee;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2264
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    invoke-virtual {v0}, Lcom/my/target/ee;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/my/target/core/presenters/f;->d(Landroid/content/Context;)V

    .line 2266
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/my/target/ee;->e(I)V

    .line 30
    return-void
.end method

.method static synthetic h(Lcom/my/target/core/presenters/f;)V
    .locals 2

    .prologue
    .line 30
    .line 2271
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/my/target/ee;->e(I)V

    .line 30
    return-void
.end method

.method static synthetic i(Lcom/my/target/core/presenters/f;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/my/target/core/presenters/f;->ah:Z

    return v0
.end method

.method static synthetic j(Lcom/my/target/core/presenters/f;)Lcom/my/target/aj;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->videoBanner:Lcom/my/target/aj;

    return-object v0
.end method

.method static synthetic k(Lcom/my/target/core/presenters/f;)V
    .locals 2

    .prologue
    .line 30
    .line 3256
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    invoke-virtual {v0}, Lcom/my/target/ee;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/my/target/core/presenters/f;->e(Landroid/content/Context;)V

    .line 3257
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/my/target/ee;->e(I)V

    .line 30
    return-void
.end method

.method static synthetic l(Lcom/my/target/core/presenters/f;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/my/target/core/presenters/f;->allowClose:Z

    return v0
.end method

.method static synthetic m(Lcom/my/target/core/presenters/f;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/my/target/core/presenters/f;->ak:Z

    return v0
.end method

.method static synthetic n(Lcom/my/target/core/presenters/f;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/my/target/core/presenters/f;->ag:Z

    return v0
.end method

.method static synthetic o(Lcom/my/target/core/presenters/f;)V
    .locals 1

    .prologue
    .line 30
    .line 3298
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->ae:Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 3300
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->ae:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 3302
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->videoBanner:Lcom/my/target/aj;

    if-eqz v0, :cond_1

    .line 3304
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->videoBanner:Lcom/my/target/aj;

    invoke-virtual {v0}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/ar;->ae()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/presenters/f;->ae:Ljava/util/Set;

    .line 30
    :cond_1
    return-void
.end method

.method static synthetic p(Lcom/my/target/core/presenters/f;)Z
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/core/presenters/f;->ag:Z

    return v0
.end method

.method static synthetic q(Lcom/my/target/core/presenters/f;)Z
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/core/presenters/f;->ah:Z

    return v0
.end method

.method static synthetic r(Lcom/my/target/core/presenters/f;)F
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/my/target/core/presenters/f;->duration:F

    return v0
.end method

.method static synthetic s(Lcom/my/target/core/presenters/f;)Z
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/core/presenters/f;->am:Z

    return v0
.end method

.method static synthetic t(Lcom/my/target/core/presenters/f;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 30
    .line 3328
    iput-boolean v0, p0, Lcom/my/target/core/presenters/f;->ag:Z

    .line 3329
    iget-object v1, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    invoke-virtual {v1}, Lcom/my/target/ee;->G()V

    .line 3331
    iget-object v1, p0, Lcom/my/target/core/presenters/f;->videoBanner:Lcom/my/target/aj;

    if-eqz v1, :cond_0

    .line 3333
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->videoBanner:Lcom/my/target/aj;

    invoke-virtual {v0}, Lcom/my/target/aj;->isAllowReplay()Z

    move-result v0

    .line 3335
    :cond_0
    iget-object v1, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    invoke-virtual {v1}, Lcom/my/target/ee;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/my/target/core/presenters/f;->e(Landroid/content/Context;)V

    .line 3336
    iget-object v1, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    invoke-virtual {v1, v0}, Lcom/my/target/ee;->f(Z)V

    .line 30
    return-void
.end method


# virtual methods
.method public final a(Lcom/my/target/core/presenters/f$a;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/my/target/core/presenters/f;->al:Lcom/my/target/core/presenters/f$a;

    .line 106
    return-void
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 115
    iput-boolean p1, p0, Lcom/my/target/core/presenters/f;->ak:Z

    .line 116
    return-void
.end method

.method public final destroy()V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    invoke-virtual {v0}, Lcom/my/target/ee;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/my/target/core/presenters/f;->e(Landroid/content/Context;)V

    .line 172
    return-void
.end method

.method public final getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    return-object v0
.end method

.method public final pause()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 120
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    invoke-virtual {v0}, Lcom/my/target/ee;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/my/target/core/presenters/f;->e(Landroid/content/Context;)V

    .line 122
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    invoke-virtual {v0}, Lcom/my/target/ee;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    invoke-virtual {v0}, Lcom/my/target/ee;->isPaused()Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->videoBanner:Lcom/my/target/aj;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->videoBanner:Lcom/my/target/aj;

    invoke-virtual {v0}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    const-string v1, "playbackPaused"

    invoke-virtual {v0, v1}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    invoke-virtual {v1}, Lcom/my/target/ee;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    invoke-virtual {v0}, Lcom/my/target/ee;->pause()V

    .line 132
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    iget-object v1, p0, Lcom/my/target/core/presenters/f;->ab:Lcom/my/target/core/presenters/f$c;

    invoke-virtual {v0, v1}, Lcom/my/target/ee;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 134
    iget-wide v0, p0, Lcom/my/target/core/presenters/f;->aj:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 136
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/my/target/core/presenters/f;->aj:J

    sub-long/2addr v0, v2

    .line 137
    cmp-long v2, v0, v4

    if-lez v2, :cond_2

    iget-wide v2, p0, Lcom/my/target/core/presenters/f;->ai:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_2

    .line 139
    iget-wide v2, p0, Lcom/my/target/core/presenters/f;->ai:J

    sub-long v0, v2, v0

    iput-wide v0, p0, Lcom/my/target/core/presenters/f;->ai:J

    .line 146
    :cond_1
    :goto_0
    return-void

    .line 143
    :cond_2
    iput-wide v4, p0, Lcom/my/target/core/presenters/f;->ai:J

    goto :goto_0
.end method

.method public final resume()V
    .locals 4

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/my/target/core/presenters/f;->allowClose:Z

    if-eqz v0, :cond_0

    .line 152
    iget-wide v0, p0, Lcom/my/target/core/presenters/f;->ai:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 154
    iget-wide v0, p0, Lcom/my/target/core/presenters/f;->ai:J

    invoke-direct {p0, v0, v1}, Lcom/my/target/core/presenters/f;->a(J)V

    .line 157
    :cond_0
    return-void
.end method

.method public final stop()V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/my/target/core/presenters/f;->aa:Lcom/my/target/ee;

    invoke-virtual {v0}, Lcom/my/target/ee;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/my/target/core/presenters/f;->e(Landroid/content/Context;)V

    .line 167
    return-void
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/my/target/core/presenters/f;->am:Z

    return v0
.end method
