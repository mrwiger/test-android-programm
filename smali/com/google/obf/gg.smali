.class public Lcom/google/obf/gg;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/io/Flushable;


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;


# instance fields
.field private final c:Ljava/io/Writer;

.field private d:[I

.field private e:I

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 172
    const/16 v0, 0x80

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/obf/gg;->a:[Ljava/lang/String;

    move v0, v1

    .line 173
    :goto_0
    const/16 v2, 0x1f

    if-gt v0, v2, :cond_0

    .line 174
    sget-object v2, Lcom/google/obf/gg;->a:[Ljava/lang/String;

    const-string v3, "\\u%04x"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 175
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 176
    :cond_0
    sget-object v0, Lcom/google/obf/gg;->a:[Ljava/lang/String;

    const/16 v1, 0x22

    const-string v2, "\\\""

    aput-object v2, v0, v1

    .line 177
    sget-object v0, Lcom/google/obf/gg;->a:[Ljava/lang/String;

    const/16 v1, 0x5c

    const-string v2, "\\\\"

    aput-object v2, v0, v1

    .line 178
    sget-object v0, Lcom/google/obf/gg;->a:[Ljava/lang/String;

    const/16 v1, 0x9

    const-string v2, "\\t"

    aput-object v2, v0, v1

    .line 179
    sget-object v0, Lcom/google/obf/gg;->a:[Ljava/lang/String;

    const/16 v1, 0x8

    const-string v2, "\\b"

    aput-object v2, v0, v1

    .line 180
    sget-object v0, Lcom/google/obf/gg;->a:[Ljava/lang/String;

    const/16 v1, 0xa

    const-string v2, "\\n"

    aput-object v2, v0, v1

    .line 181
    sget-object v0, Lcom/google/obf/gg;->a:[Ljava/lang/String;

    const/16 v1, 0xd

    const-string v2, "\\r"

    aput-object v2, v0, v1

    .line 182
    sget-object v0, Lcom/google/obf/gg;->a:[Ljava/lang/String;

    const/16 v1, 0xc

    const-string v2, "\\f"

    aput-object v2, v0, v1

    .line 183
    sget-object v0, Lcom/google/obf/gg;->a:[Ljava/lang/String;

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/obf/gg;->b:[Ljava/lang/String;

    .line 184
    sget-object v0, Lcom/google/obf/gg;->b:[Ljava/lang/String;

    const/16 v1, 0x3c

    const-string v2, "\\u003c"

    aput-object v2, v0, v1

    .line 185
    sget-object v0, Lcom/google/obf/gg;->b:[Ljava/lang/String;

    const/16 v1, 0x3e

    const-string v2, "\\u003e"

    aput-object v2, v0, v1

    .line 186
    sget-object v0, Lcom/google/obf/gg;->b:[Ljava/lang/String;

    const/16 v1, 0x26

    const-string v2, "\\u0026"

    aput-object v2, v0, v1

    .line 187
    sget-object v0, Lcom/google/obf/gg;->b:[Ljava/lang/String;

    const/16 v1, 0x3d

    const-string v2, "\\u003d"

    aput-object v2, v0, v1

    .line 188
    sget-object v0, Lcom/google/obf/gg;->b:[Ljava/lang/String;

    const/16 v1, 0x27

    const-string v2, "\\u0027"

    aput-object v2, v0, v1

    .line 189
    return-void
.end method

.method public constructor <init>(Ljava/io/Writer;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/16 v0, 0x20

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/obf/gg;->d:[I

    .line 3
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/gg;->e:I

    .line 4
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/google/obf/gg;->a(I)V

    .line 5
    const-string v0, ":"

    iput-object v0, p0, Lcom/google/obf/gg;->g:Ljava/lang/String;

    .line 6
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/gg;->k:Z

    .line 7
    if-nez p1, :cond_0

    .line 8
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "out == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 9
    :cond_0
    iput-object p1, p0, Lcom/google/obf/gg;->c:Ljava/io/Writer;

    .line 10
    return-void
.end method

.method private a()I
    .locals 2

    .prologue
    .line 52
    iget v0, p0, Lcom/google/obf/gg;->e:I

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "JsonWriter is closed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/google/obf/gg;->d:[I

    iget v1, p0, Lcom/google/obf/gg;->e:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    return v0
.end method

.method private a(IILjava/lang/String;)Lcom/google/obf/gg;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/obf/gg;->a()I

    move-result v0

    .line 37
    if-eq v0, p2, :cond_0

    if-eq v0, p1, :cond_0

    .line 38
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Nesting problem."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_0
    iget-object v1, p0, Lcom/google/obf/gg;->j:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 40
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Dangling name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/obf/gg;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_1
    iget v1, p0, Lcom/google/obf/gg;->e:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/obf/gg;->e:I

    .line 42
    if-ne v0, p2, :cond_2

    .line 43
    invoke-direct {p0}, Lcom/google/obf/gg;->k()V

    .line 44
    :cond_2
    iget-object v0, p0, Lcom/google/obf/gg;->c:Ljava/io/Writer;

    invoke-virtual {v0, p3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 45
    return-object p0
.end method

.method private a(ILjava/lang/String;)Lcom/google/obf/gg;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/obf/gg;->m()V

    .line 33
    invoke-direct {p0, p1}, Lcom/google/obf/gg;->a(I)V

    .line 34
    iget-object v0, p0, Lcom/google/obf/gg;->c:Ljava/io/Writer;

    invoke-virtual {v0, p2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 35
    return-object p0
.end method

.method private a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 46
    iget v0, p0, Lcom/google/obf/gg;->e:I

    iget-object v1, p0, Lcom/google/obf/gg;->d:[I

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 47
    iget v0, p0, Lcom/google/obf/gg;->e:I

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [I

    .line 48
    iget-object v1, p0, Lcom/google/obf/gg;->d:[I

    iget v2, p0, Lcom/google/obf/gg;->e:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 49
    iput-object v0, p0, Lcom/google/obf/gg;->d:[I

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/google/obf/gg;->d:[I

    iget v1, p0, Lcom/google/obf/gg;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/obf/gg;->e:I

    aput p1, v0, v1

    .line 51
    return-void
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/obf/gg;->d:[I

    iget v1, p0, Lcom/google/obf/gg;->e:I

    add-int/lit8 v1, v1, -0x1

    aput p1, v0, v1

    .line 56
    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 118
    iget-boolean v0, p0, Lcom/google/obf/gg;->i:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/obf/gg;->b:[Ljava/lang/String;

    .line 119
    :goto_0
    iget-object v2, p0, Lcom/google/obf/gg;->c:Ljava/io/Writer;

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 121
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    move v3, v1

    .line 122
    :goto_1
    if-ge v3, v4, :cond_6

    .line 123
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 124
    const/16 v5, 0x80

    if-ge v2, v5, :cond_2

    .line 125
    aget-object v2, v0, v2

    .line 126
    if-nez v2, :cond_3

    .line 136
    :cond_0
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 118
    :cond_1
    sget-object v0, Lcom/google/obf/gg;->a:[Ljava/lang/String;

    goto :goto_0

    .line 128
    :cond_2
    const/16 v5, 0x2028

    if-ne v2, v5, :cond_5

    .line 129
    const-string v2, "\\u2028"

    .line 132
    :cond_3
    :goto_3
    if-ge v1, v3, :cond_4

    .line 133
    iget-object v5, p0, Lcom/google/obf/gg;->c:Ljava/io/Writer;

    sub-int v6, v3, v1

    invoke-virtual {v5, p1, v1, v6}, Ljava/io/Writer;->write(Ljava/lang/String;II)V

    .line 134
    :cond_4
    iget-object v1, p0, Lcom/google/obf/gg;->c:Ljava/io/Writer;

    invoke-virtual {v1, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 135
    add-int/lit8 v1, v3, 0x1

    goto :goto_2

    .line 130
    :cond_5
    const/16 v5, 0x2029

    if-ne v2, v5, :cond_0

    .line 131
    const-string v2, "\\u2029"

    goto :goto_3

    .line 137
    :cond_6
    if-ge v1, v4, :cond_7

    .line 138
    iget-object v0, p0, Lcom/google/obf/gg;->c:Ljava/io/Writer;

    sub-int v2, v4, v1

    invoke-virtual {v0, p1, v1, v2}, Ljava/io/Writer;->write(Ljava/lang/String;II)V

    .line 139
    :cond_7
    iget-object v0, p0, Lcom/google/obf/gg;->c:Ljava/io/Writer;

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 140
    return-void
.end method

.method private j()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/obf/gg;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 66
    invoke-direct {p0}, Lcom/google/obf/gg;->l()V

    .line 67
    iget-object v0, p0, Lcom/google/obf/gg;->j:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/obf/gg;->d(Ljava/lang/String;)V

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/gg;->j:Ljava/lang/String;

    .line 69
    :cond_0
    return-void
.end method

.method private k()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/obf/gg;->f:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 147
    :cond_0
    return-void

    .line 143
    :cond_1
    iget-object v0, p0, Lcom/google/obf/gg;->c:Ljava/io/Writer;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 144
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/obf/gg;->e:I

    :goto_0
    if-ge v0, v1, :cond_0

    .line 145
    iget-object v2, p0, Lcom/google/obf/gg;->c:Ljava/io/Writer;

    iget-object v3, p0, Lcom/google/obf/gg;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 146
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private l()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/google/obf/gg;->a()I

    move-result v0

    .line 149
    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 150
    iget-object v0, p0, Lcom/google/obf/gg;->c:Ljava/io/Writer;

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    .line 153
    :cond_0
    invoke-direct {p0}, Lcom/google/obf/gg;->k()V

    .line 154
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/obf/gg;->b(I)V

    .line 155
    return-void

    .line 151
    :cond_1
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 152
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Nesting problem."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private m()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 156
    invoke-direct {p0}, Lcom/google/obf/gg;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 170
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Nesting problem."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157
    :pswitch_1
    iget-boolean v0, p0, Lcom/google/obf/gg;->h:Z

    if-nez v0, :cond_0

    .line 158
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "JSON must have only one top-level value."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 159
    :cond_0
    :pswitch_2
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/obf/gg;->b(I)V

    .line 171
    :goto_0
    return-void

    .line 161
    :pswitch_3
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/obf/gg;->b(I)V

    .line 162
    invoke-direct {p0}, Lcom/google/obf/gg;->k()V

    goto :goto_0

    .line 164
    :pswitch_4
    iget-object v0, p0, Lcom/google/obf/gg;->c:Ljava/io/Writer;

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/io/Writer;->append(C)Ljava/io/Writer;

    .line 165
    invoke-direct {p0}, Lcom/google/obf/gg;->k()V

    goto :goto_0

    .line 167
    :pswitch_5
    iget-object v0, p0, Lcom/google/obf/gg;->c:Ljava/io/Writer;

    iget-object v1, p0, Lcom/google/obf/gg;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 168
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/obf/gg;->b(I)V

    goto :goto_0

    .line 156
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a(J)Lcom/google/obf/gg;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/google/obf/gg;->j()V

    .line 95
    invoke-direct {p0}, Lcom/google/obf/gg;->m()V

    .line 96
    iget-object v0, p0, Lcom/google/obf/gg;->c:Ljava/io/Writer;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 97
    return-object p0
.end method

.method public a(Ljava/lang/Boolean;)Lcom/google/obf/gg;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    if-nez p1, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/google/obf/gg;->f()Lcom/google/obf/gg;

    move-result-object p0

    .line 93
    :goto_0
    return-object p0

    .line 90
    :cond_0
    invoke-direct {p0}, Lcom/google/obf/gg;->j()V

    .line 91
    invoke-direct {p0}, Lcom/google/obf/gg;->m()V

    .line 92
    iget-object v1, p0, Lcom/google/obf/gg;->c:Ljava/io/Writer;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "true"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v0, "false"

    goto :goto_1
.end method

.method public a(Ljava/lang/Number;)Lcom/google/obf/gg;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    if-nez p1, :cond_0

    .line 99
    invoke-virtual {p0}, Lcom/google/obf/gg;->f()Lcom/google/obf/gg;

    move-result-object p0

    .line 107
    :goto_0
    return-object p0

    .line 100
    :cond_0
    invoke-direct {p0}, Lcom/google/obf/gg;->j()V

    .line 101
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 102
    iget-boolean v1, p0, Lcom/google/obf/gg;->h:Z

    if-nez v1, :cond_2

    const-string v1, "-Infinity"

    .line 103
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "Infinity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "NaN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 104
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Numeric values must be finite, but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :cond_2
    invoke-direct {p0}, Lcom/google/obf/gg;->m()V

    .line 106
    iget-object v1, p0, Lcom/google/obf/gg;->c:Ljava/io/Writer;

    invoke-virtual {v1, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lcom/google/obf/gg;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    if-nez p1, :cond_0

    .line 58
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "name == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/google/obf/gg;->j:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 60
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 61
    :cond_1
    iget v0, p0, Lcom/google/obf/gg;->e:I

    if-nez v0, :cond_2

    .line 62
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "JsonWriter is closed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_2
    iput-object p1, p0, Lcom/google/obf/gg;->j:Ljava/lang/String;

    .line 64
    return-object p0
.end method

.method public a(Z)Lcom/google/obf/gg;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/google/obf/gg;->j()V

    .line 85
    invoke-direct {p0}, Lcom/google/obf/gg;->m()V

    .line 86
    iget-object v1, p0, Lcom/google/obf/gg;->c:Ljava/io/Writer;

    if-eqz p1, :cond_0

    const-string v0, "true"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 87
    return-object p0

    .line 86
    :cond_0
    const-string v0, "false"

    goto :goto_0
.end method

.method public b()Lcom/google/obf/gg;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/obf/gg;->j()V

    .line 27
    const/4 v0, 0x1

    const-string v1, "["

    invoke-direct {p0, v0, v1}, Lcom/google/obf/gg;->a(ILjava/lang/String;)Lcom/google/obf/gg;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/google/obf/gg;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    if-nez p1, :cond_0

    .line 71
    invoke-virtual {p0}, Lcom/google/obf/gg;->f()Lcom/google/obf/gg;

    move-result-object p0

    .line 75
    :goto_0
    return-object p0

    .line 72
    :cond_0
    invoke-direct {p0}, Lcom/google/obf/gg;->j()V

    .line 73
    invoke-direct {p0}, Lcom/google/obf/gg;->m()V

    .line 74
    invoke-direct {p0, p1}, Lcom/google/obf/gg;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/google/obf/gg;->h:Z

    .line 18
    return-void
.end method

.method public c()Lcom/google/obf/gg;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    const/4 v0, 0x1

    const/4 v1, 0x2

    const-string v2, "]"

    invoke-direct {p0, v0, v1, v2}, Lcom/google/obf/gg;->a(IILjava/lang/String;)Lcom/google/obf/gg;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/gg;->f:Ljava/lang/String;

    .line 13
    const-string v0, ":"

    iput-object v0, p0, Lcom/google/obf/gg;->g:Ljava/lang/String;

    .line 16
    :goto_0
    return-void

    .line 14
    :cond_0
    iput-object p1, p0, Lcom/google/obf/gg;->f:Ljava/lang/String;

    .line 15
    const-string v0, ": "

    iput-object v0, p0, Lcom/google/obf/gg;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/google/obf/gg;->i:Z

    .line 21
    return-void
.end method

.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 112
    iget-object v0, p0, Lcom/google/obf/gg;->c:Ljava/io/Writer;

    invoke-virtual {v0}, Ljava/io/Writer;->close()V

    .line 113
    iget v0, p0, Lcom/google/obf/gg;->e:I

    .line 114
    if-gt v0, v1, :cond_0

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/obf/gg;->d:[I

    add-int/lit8 v0, v0, -0x1

    aget v0, v1, v0

    const/4 v1, 0x7

    if-eq v0, v1, :cond_1

    .line 115
    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Incomplete document"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 116
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/gg;->e:I

    .line 117
    return-void
.end method

.method public d()Lcom/google/obf/gg;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/obf/gg;->j()V

    .line 30
    const/4 v0, 0x3

    const-string/jumbo v1, "{"

    invoke-direct {p0, v0, v1}, Lcom/google/obf/gg;->a(ILjava/lang/String;)Lcom/google/obf/gg;

    move-result-object v0

    return-object v0
.end method

.method public final d(Z)V
    .locals 0

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/google/obf/gg;->k:Z

    .line 24
    return-void
.end method

.method public e()Lcom/google/obf/gg;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    const/4 v0, 0x3

    const/4 v1, 0x5

    const-string/jumbo v2, "}"

    invoke-direct {p0, v0, v1, v2}, Lcom/google/obf/gg;->a(IILjava/lang/String;)Lcom/google/obf/gg;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/google/obf/gg;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/obf/gg;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 77
    iget-boolean v0, p0, Lcom/google/obf/gg;->k:Z

    if-eqz v0, :cond_1

    .line 78
    invoke-direct {p0}, Lcom/google/obf/gg;->j()V

    .line 81
    :cond_0
    invoke-direct {p0}, Lcom/google/obf/gg;->m()V

    .line 82
    iget-object v0, p0, Lcom/google/obf/gg;->c:Ljava/io/Writer;

    const-string v1, "null"

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 83
    :goto_0
    return-object p0

    .line 79
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/gg;->j:Ljava/lang/String;

    goto :goto_0
.end method

.method public flush()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    iget v0, p0, Lcom/google/obf/gg;->e:I

    if-nez v0, :cond_0

    .line 109
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "JsonWriter is closed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/google/obf/gg;->c:Ljava/io/Writer;

    invoke-virtual {v0}, Ljava/io/Writer;->flush()V

    .line 111
    return-void
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/google/obf/gg;->h:Z

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/google/obf/gg;->i:Z

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/google/obf/gg;->k:Z

    return v0
.end method
