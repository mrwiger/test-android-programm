.class Lru/cn/tv/mobile/authorization/SignInFragment$1;
.super Landroid/os/AsyncTask;
.source "SignInFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/mobile/authorization/SignInFragment;->load()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lru/cn/api/authorization/CodeAuthorizationChallenge;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/mobile/authorization/SignInFragment;


# direct methods
.method constructor <init>(Lru/cn/tv/mobile/authorization/SignInFragment;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/mobile/authorization/SignInFragment;

    .prologue
    .line 50
    iput-object p1, p0, Lru/cn/tv/mobile/authorization/SignInFragment$1;->this$0:Lru/cn/tv/mobile/authorization/SignInFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 50
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lru/cn/tv/mobile/authorization/SignInFragment$1;->doInBackground([Ljava/lang/Void;)Lru/cn/api/authorization/CodeAuthorizationChallenge;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Lru/cn/api/authorization/CodeAuthorizationChallenge;
    .locals 6
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v2, 0x0

    .line 53
    iget-object v3, p0, Lru/cn/tv/mobile/authorization/SignInFragment$1;->this$0:Lru/cn/tv/mobile/authorization/SignInFragment;

    invoke-virtual {v3}, Lru/cn/tv/mobile/authorization/SignInFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 54
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_0

    .line 63
    :goto_0
    return-object v2

    .line 58
    :cond_0
    :try_start_0
    iget-object v3, p0, Lru/cn/tv/mobile/authorization/SignInFragment$1;->this$0:Lru/cn/tv/mobile/authorization/SignInFragment;

    invoke-static {v3}, Lru/cn/tv/mobile/authorization/SignInFragment;->access$000(Lru/cn/tv/mobile/authorization/SignInFragment;)J

    move-result-wide v4

    const-string v3, "ptv29783051://callback"

    invoke-static {v0, v4, v5, v3}, Lru/cn/api/authorization/Authorization;->codeChallenge(Landroid/content/Context;JLjava/lang/String;)Lru/cn/api/authorization/CodeAuthorizationChallenge;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 59
    :catch_0
    move-exception v1

    .line 60
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "SignInFragment"

    const-string v4, "Unable to get authorization uri"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 50
    check-cast p1, Lru/cn/api/authorization/CodeAuthorizationChallenge;

    invoke-virtual {p0, p1}, Lru/cn/tv/mobile/authorization/SignInFragment$1;->onPostExecute(Lru/cn/api/authorization/CodeAuthorizationChallenge;)V

    return-void
.end method

.method protected onPostExecute(Lru/cn/api/authorization/CodeAuthorizationChallenge;)V
    .locals 1
    .param p1, "challenge"    # Lru/cn/api/authorization/CodeAuthorizationChallenge;

    .prologue
    .line 68
    iget-object v0, p0, Lru/cn/tv/mobile/authorization/SignInFragment$1;->this$0:Lru/cn/tv/mobile/authorization/SignInFragment;

    invoke-static {v0, p1}, Lru/cn/tv/mobile/authorization/SignInFragment;->access$100(Lru/cn/tv/mobile/authorization/SignInFragment;Lru/cn/api/authorization/CodeAuthorizationChallenge;)V

    .line 69
    return-void
.end method
