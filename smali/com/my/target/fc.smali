.class public final Lcom/my/target/fc;
.super Lcom/my/target/cc;
.source "InterstitialVideoTextureView.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/fc$a;
    }
.end annotation


# instance fields
.field private final ek:Ljava/lang/Runnable;

.field private final el:Lcom/my/target/ck;

.field private em:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/my/target/cc;-><init>(Landroid/content/Context;)V

    .line 24
    const/16 v0, 0xc8

    invoke-static {v0}, Lcom/my/target/ck;->k(I)Lcom/my/target/ck;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/fc;->el:Lcom/my/target/ck;

    .line 25
    new-instance v0, Lcom/my/target/fc$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/my/target/fc$a;-><init>(Lcom/my/target/fc;B)V

    iput-object v0, p0, Lcom/my/target/fc;->ek:Ljava/lang/Runnable;

    .line 26
    iget-object v0, p0, Lcom/my/target/fc;->el:Lcom/my/target/ck;

    iget-object v1, p0, Lcom/my/target/fc;->ek:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->d(Ljava/lang/Runnable;)V

    .line 27
    return-void
.end method

.method private Q()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/fc;->em:Z

    .line 77
    iget-object v0, p0, Lcom/my/target/fc;->ek:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/my/target/fc;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 78
    return-void
.end method

.method private S()V
    .locals 2

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/my/target/fc;->em:Z

    if-nez v0, :cond_0

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/fc;->em:Z

    .line 85
    iget-object v0, p0, Lcom/my/target/fc;->el:Lcom/my/target/ck;

    iget-object v1, p0, Lcom/my/target/fc;->ek:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->d(Ljava/lang/Runnable;)V

    .line 87
    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(Landroid/media/MediaPlayer;)V
    .locals 0
    .param p1, "mediaPlayer"    # Landroid/media/MediaPlayer;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/my/target/fc;->S()V

    .line 71
    invoke-super {p0, p1}, Lcom/my/target/cc;->a(Landroid/media/MediaPlayer;)V

    .line 72
    return-void
.end method

.method protected final a(Landroid/view/Surface;)V
    .locals 0
    .param p1, "s"    # Landroid/view/Surface;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/my/target/fc;->S()V

    .line 54
    invoke-super {p0, p1}, Lcom/my/target/cc;->a(Landroid/view/Surface;)V

    .line 55
    return-void
.end method

.method protected final a(Landroid/view/Surface;Landroid/net/Uri;)V
    .locals 0
    .param p1, "s"    # Landroid/view/Surface;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 60
    if-eqz p1, :cond_0

    .line 62
    invoke-direct {p0}, Lcom/my/target/fc;->S()V

    .line 64
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/my/target/cc;->a(Landroid/view/Surface;Landroid/net/Uri;)V

    .line 65
    return-void
.end method

.method public final bm()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/my/target/fc;->Q()V

    .line 40
    invoke-super {p0}, Lcom/my/target/cc;->bm()V

    .line 41
    return-void
.end method

.method public final j(Z)V
    .locals 0
    .param p1, "updateScreenshot"    # Z

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcom/my/target/cc;->j(Z)V

    .line 46
    invoke-direct {p0}, Lcom/my/target/fc;->Q()V

    .line 47
    invoke-virtual {p0}, Lcom/my/target/fc;->pause()V

    .line 48
    return-void
.end method

.method public final k(Z)V
    .locals 0
    .param p1, "notifyListener"    # Z

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/my/target/fc;->Q()V

    .line 33
    invoke-super {p0, p1}, Lcom/my/target/cc;->k(Z)V

    .line 34
    return-void
.end method
