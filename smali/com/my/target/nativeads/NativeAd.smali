.class public final Lcom/my/target/nativeads/NativeAd;
.super Lcom/my/target/common/BaseAd;
.source "NativeAd.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/nativeads/NativeAd$NativeAdListener;
    }
.end annotation


# instance fields
.field private final appContext:Landroid/content/Context;

.field private banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

.field private listener:Lcom/my/target/nativeads/NativeAd$NativeAdListener;

.field private nativeAdEngine:Lcom/my/target/core/engines/a;


# direct methods
.method public constructor <init>(ILandroid/content/Context;)V
    .locals 1
    .param p1, "slotId"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    const-string v0, "nativeads"

    invoke-direct {p0, p1, v0}, Lcom/my/target/common/BaseAd;-><init>(ILjava/lang/String;)V

    .line 45
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/NativeAd;->appContext:Landroid/content/Context;

    .line 46
    const-string v0, "NativeAd created. Version: 5.1.0"

    invoke-static {v0}, Lcom/my/target/g;->c(Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/my/target/nativeads/NativeAd;Lcom/my/target/cs;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/my/target/nativeads/NativeAd;
    .param p1, "x1"    # Lcom/my/target/cs;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/my/target/nativeads/NativeAd;->handleResult(Lcom/my/target/cs;Ljava/lang/String;)V

    return-void
.end method

.method private handleResult(Lcom/my/target/cs;Ljava/lang/String;)V
    .locals 2
    .param p1, "result"    # Lcom/my/target/cs;
    .param p2, "error"    # Ljava/lang/String;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAd;->listener:Lcom/my/target/nativeads/NativeAd$NativeAdListener;

    if-eqz v0, :cond_1

    .line 130
    if-nez p1, :cond_2

    const/4 v0, 0x0

    .line 131
    :goto_0
    if-nez v0, :cond_3

    .line 133
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAd;->listener:Lcom/my/target/nativeads/NativeAd$NativeAdListener;

    if-nez p2, :cond_0

    const-string p2, "no ad"

    .end local p2    # "error":Ljava/lang/String;
    :cond_0
    invoke-interface {v0, p2, p0}, Lcom/my/target/nativeads/NativeAd$NativeAdListener;->onNoAd(Ljava/lang/String;Lcom/my/target/nativeads/NativeAd;)V

    .line 142
    :cond_1
    :goto_1
    return-void

    .line 130
    .restart local p2    # "error":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Lcom/my/target/cs;->w()Lcom/my/target/core/models/banners/a;

    move-result-object v0

    goto :goto_0

    .line 137
    :cond_3
    invoke-static {p0, v0}, Lcom/my/target/core/engines/a;->a(Lcom/my/target/nativeads/NativeAd;Lcom/my/target/core/models/banners/a;)Lcom/my/target/core/engines/a;

    move-result-object v1

    iput-object v1, p0, Lcom/my/target/nativeads/NativeAd;->nativeAdEngine:Lcom/my/target/core/engines/a;

    .line 138
    invoke-static {v0}, Lcom/my/target/nativeads/banners/NativePromoBanner;->newBanner(Lcom/my/target/core/models/banners/a;)Lcom/my/target/nativeads/banners/NativePromoBanner;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/NativeAd;->banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

    .line 139
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAd;->listener:Lcom/my/target/nativeads/NativeAd$NativeAdListener;

    invoke-interface {v0, p0}, Lcom/my/target/nativeads/NativeAd$NativeAdListener;->onLoad(Lcom/my/target/nativeads/NativeAd;)V

    goto :goto_1
.end method

.method public static loadImageToView(Lcom/my/target/common/models/ImageData;Landroid/widget/ImageView;)V
    .locals 0
    .param p0, "imageData"    # Lcom/my/target/common/models/ImageData;
    .param p1, "view"    # Landroid/widget/ImageView;

    .prologue
    .line 34
    invoke-static {p0, p1}, Lcom/my/target/ch;->a(Lcom/my/target/common/models/ImageData;Landroid/widget/ImageView;)V

    .line 35
    return-void
.end method


# virtual methods
.method public final getBanner()Lcom/my/target/nativeads/banners/NativePromoBanner;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAd;->banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

    return-object v0
.end method

.method public final getListener()Lcom/my/target/nativeads/NativeAd$NativeAdListener;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAd;->listener:Lcom/my/target/nativeads/NativeAd$NativeAdListener;

    return-object v0
.end method

.method public final isAutoLoadImages()Z
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAd;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0}, Lcom/my/target/b;->isAutoLoadImages()Z

    move-result v0

    return v0
.end method

.method public final isAutoLoadVideo()Z
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAd;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0}, Lcom/my/target/b;->isAutoLoadVideo()Z

    move-result v0

    return v0
.end method

.method public final load()V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAd;->adConfig:Lcom/my/target/b;

    invoke-static {v0}, Lcom/my/target/co;->newFactory(Lcom/my/target/b;)Lcom/my/target/c;

    move-result-object v0

    new-instance v1, Lcom/my/target/nativeads/NativeAd$1;

    invoke-direct {v1, p0}, Lcom/my/target/nativeads/NativeAd$1;-><init>(Lcom/my/target/nativeads/NativeAd;)V

    invoke-virtual {v0, v1}, Lcom/my/target/c;->a(Lcom/my/target/c$b;)Lcom/my/target/c;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/nativeads/NativeAd;->appContext:Landroid/content/Context;

    .line 93
    invoke-virtual {v0, v1}, Lcom/my/target/c;->a(Landroid/content/Context;)Lcom/my/target/c;

    .line 94
    return-void
.end method

.method public final registerView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 106
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/my/target/nativeads/NativeAd;->registerView(Landroid/view/View;Ljava/util/List;)V

    .line 107
    return-void
.end method

.method public final registerView(Landroid/view/View;Ljava/util/List;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 98
    .local p2, "clickableViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAd;->nativeAdEngine:Lcom/my/target/core/engines/a;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAd;->nativeAdEngine:Lcom/my/target/core/engines/a;

    invoke-virtual {v0, p1, p2}, Lcom/my/target/core/engines/a;->registerView(Landroid/view/View;Ljava/util/List;)V

    .line 102
    :cond_0
    return-void
.end method

.method public final setAutoLoadImages(Z)V
    .locals 1
    .param p1, "autoLoadImages"    # Z

    .prologue
    .line 56
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAd;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0, p1}, Lcom/my/target/b;->setAutoLoadImages(Z)V

    .line 57
    return-void
.end method

.method public final setAutoLoadVideo(Z)V
    .locals 1
    .param p1, "autoLoadVideo"    # Z

    .prologue
    .line 51
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAd;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0, p1}, Lcom/my/target/b;->setAutoLoadVideo(Z)V

    .line 52
    return-void
.end method

.method final setBanner(Lcom/my/target/core/models/banners/a;)V
    .locals 1
    .param p1, "nativeAdBanner"    # Lcom/my/target/core/models/banners/a;

    .prologue
    .line 122
    invoke-static {p0, p1}, Lcom/my/target/core/engines/a;->a(Lcom/my/target/nativeads/NativeAd;Lcom/my/target/core/models/banners/a;)Lcom/my/target/core/engines/a;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/NativeAd;->nativeAdEngine:Lcom/my/target/core/engines/a;

    .line 123
    invoke-static {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->newBanner(Lcom/my/target/core/models/banners/a;)Lcom/my/target/nativeads/banners/NativePromoBanner;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/NativeAd;->banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

    .line 124
    return-void
.end method

.method public final setListener(Lcom/my/target/nativeads/NativeAd$NativeAdListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/my/target/nativeads/NativeAd$NativeAdListener;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/my/target/nativeads/NativeAd;->listener:Lcom/my/target/nativeads/NativeAd$NativeAdListener;

    .line 77
    return-void
.end method

.method public final unregisterView()V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAd;->nativeAdEngine:Lcom/my/target/core/engines/a;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAd;->nativeAdEngine:Lcom/my/target/core/engines/a;

    invoke-virtual {v0}, Lcom/my/target/core/engines/a;->unregisterView()V

    .line 115
    :cond_0
    return-void
.end method
