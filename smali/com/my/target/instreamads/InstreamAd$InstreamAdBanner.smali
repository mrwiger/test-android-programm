.class public final Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;
.super Ljava/lang/Object;
.source "InstreamAd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/instreamads/InstreamAd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InstreamAdBanner"
.end annotation


# instance fields
.field public final allowClose:Z

.field public final allowCloseDelay:F

.field public final allowPause:Z

.field public final ctaText:Ljava/lang/String;

.field public final duration:F

.field public final videoHeight:I

.field public final videoWidth:I


# direct methods
.method private constructor <init>(ZFFIILjava/lang/String;Z)V
    .locals 0
    .param p1, "allowClose"    # Z
    .param p2, "allowCloseDelay"    # F
    .param p3, "duration"    # F
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "ctaText"    # Ljava/lang/String;
    .param p7, "allowPause"    # Z

    .prologue
    .line 406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407
    iput-boolean p1, p0, Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;->allowClose:Z

    .line 408
    iput p2, p0, Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;->allowCloseDelay:F

    .line 409
    iput p3, p0, Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;->duration:F

    .line 410
    iput p5, p0, Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;->videoHeight:I

    .line 411
    iput p4, p0, Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;->videoWidth:I

    .line 412
    iput-object p6, p0, Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;->ctaText:Ljava/lang/String;

    .line 413
    iput-boolean p7, p0, Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;->allowPause:Z

    .line 414
    return-void
.end method

.method public static newBanner(Lcom/my/target/aj;)Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;
    .locals 8
    .param p0, "mediaBanner"    # Lcom/my/target/aj;

    .prologue
    .line 382
    new-instance v0, Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;

    invoke-virtual {p0}, Lcom/my/target/aj;->isAllowClose()Z

    move-result v1

    .line 383
    invoke-virtual {p0}, Lcom/my/target/aj;->getAllowCloseDelay()F

    move-result v2

    .line 384
    invoke-virtual {p0}, Lcom/my/target/aj;->getDuration()F

    move-result v3

    .line 385
    invoke-virtual {p0}, Lcom/my/target/aj;->getWidth()I

    move-result v4

    .line 386
    invoke-virtual {p0}, Lcom/my/target/aj;->getHeight()I

    move-result v5

    .line 387
    invoke-virtual {p0}, Lcom/my/target/aj;->getCtaText()Ljava/lang/String;

    move-result-object v6

    .line 388
    invoke-virtual {p0}, Lcom/my/target/aj;->isAllowPause()Z

    move-result v7

    invoke-direct/range {v0 .. v7}, Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;-><init>(ZFFIILjava/lang/String;Z)V

    .line 382
    return-object v0
.end method
