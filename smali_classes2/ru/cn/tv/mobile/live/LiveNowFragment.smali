.class public Lru/cn/tv/mobile/live/LiveNowFragment;
.super Landroid/support/v4/app/Fragment;
.source "LiveNowFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/mobile/live/LiveNowFragment$LiveTelecastListener;
    }
.end annotation


# instance fields
.field private adapter:Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;

.field private errorView:Landroid/view/View;

.field private handler:Landroid/os/Handler;

.field private isScrolling:Z

.field private layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

.field private liveTelecastListener:Lru/cn/tv/mobile/live/LiveNowFragment$LiveTelecastListener;

.field private needReloadTelecasts:Z

.field private progressBar:Landroid/widget/ProgressBar;

.field private recyclerView:Landroid/support/v7/widget/RecyclerView;

.field private updateProgress:Ljava/lang/Runnable;

.field private viewModel:Lru/cn/tv/mobile/live/LiveNowViewModel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 42
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->handler:Landroid/os/Handler;

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->isScrolling:Z

    .line 160
    new-instance v0, Lru/cn/tv/mobile/live/LiveNowFragment$3;

    invoke-direct {v0, p0}, Lru/cn/tv/mobile/live/LiveNowFragment$3;-><init>(Lru/cn/tv/mobile/live/LiveNowFragment;)V

    iput-object v0, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->updateProgress:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/mobile/live/LiveNowFragment;)Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/live/LiveNowFragment;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->adapter:Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/tv/mobile/live/LiveNowFragment;)Lru/cn/tv/mobile/live/LiveNowFragment$LiveTelecastListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/live/LiveNowFragment;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->liveTelecastListener:Lru/cn/tv/mobile/live/LiveNowFragment$LiveTelecastListener;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/tv/mobile/live/LiveNowFragment;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/live/LiveNowFragment;

    .prologue
    .line 27
    iget-boolean v0, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->isScrolling:Z

    return v0
.end method

.method static synthetic access$202(Lru/cn/tv/mobile/live/LiveNowFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/mobile/live/LiveNowFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->isScrolling:Z

    return p1
.end method

.method static synthetic access$300(Lru/cn/tv/mobile/live/LiveNowFragment;)Landroid/support/v7/widget/LinearLayoutManager;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/live/LiveNowFragment;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    return-object v0
.end method

.method static synthetic access$400(Lru/cn/tv/mobile/live/LiveNowFragment;)Lru/cn/tv/mobile/live/LiveNowViewModel;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/live/LiveNowFragment;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->viewModel:Lru/cn/tv/mobile/live/LiveNowViewModel;

    return-object v0
.end method

.method static synthetic access$500(Lru/cn/tv/mobile/live/LiveNowFragment;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/live/LiveNowFragment;

    .prologue
    .line 27
    iget-boolean v0, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->needReloadTelecasts:Z

    return v0
.end method

.method static synthetic access$502(Lru/cn/tv/mobile/live/LiveNowFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/mobile/live/LiveNowFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->needReloadTelecasts:Z

    return p1
.end method

.method static synthetic access$600(Lru/cn/tv/mobile/live/LiveNowFragment;Landroid/database/Cursor;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/live/LiveNowFragment;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lru/cn/tv/mobile/live/LiveNowFragment;->isEnded(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lru/cn/tv/mobile/live/LiveNowFragment;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/mobile/live/LiveNowFragment;

    .prologue
    .line 27
    invoke-direct {p0}, Lru/cn/tv/mobile/live/LiveNowFragment;->startUpdateProgressTask()V

    return-void
.end method

.method private isEnded(Landroid/database/Cursor;)Z
    .locals 14
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 189
    check-cast p1, Landroid/database/CursorWrapper;

    .line 190
    .end local p1    # "cursor":Landroid/database/Cursor;
    invoke-virtual {p1}, Landroid/database/CursorWrapper;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/cursor/TelecastItemCursor;

    .line 192
    .local v0, "c":Lru/cn/api/provider/cursor/TelecastItemCursor;
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getTime()J

    move-result-wide v6

    .line 193
    .local v6, "time":J
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getDuration()J

    move-result-wide v2

    .line 195
    .local v2, "duration":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long v4, v10, v12

    .line 196
    .local v4, "nowSec":J
    sub-long v8, v4, v6

    .line 198
    .local v8, "timeElapsed":J
    cmp-long v1, v8, v2

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private load()V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->progressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 143
    iget-object v0, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->errorView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 145
    iget-object v0, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->viewModel:Lru/cn/tv/mobile/live/LiveNowViewModel;

    invoke-virtual {v0}, Lru/cn/tv/mobile/live/LiveNowViewModel;->load()V

    .line 146
    return-void
.end method

.method private setTelecasts(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "telecasts"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x0

    .line 149
    iget-object v0, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 151
    iget-object v0, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->adapter:Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;

    invoke-virtual {v0, v2}, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->setLoading(Z)V

    .line 152
    iget-object v0, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->adapter:Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;

    invoke-virtual {v0, p1}, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 154
    if-nez p1, :cond_0

    .line 155
    iget-object v0, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->errorView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 157
    :cond_0
    return-void
.end method

.method private startUpdateProgressTask()V
    .locals 4

    .prologue
    .line 202
    iget-object v0, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->updateProgress:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 203
    iget-object v0, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->updateProgress:Ljava/lang/Runnable;

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 204
    return-void
.end method

.method private stopUpdateProgressTask()V
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->updateProgress:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 208
    return-void
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$LiveNowFragment(Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/mobile/live/LiveNowFragment;->setTelecasts(Landroid/database/Cursor;)V

    return-void
.end method

.method final synthetic lambda$onViewCreated$0$LiveNowFragment(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 116
    invoke-direct {p0}, Lru/cn/tv/mobile/live/LiveNowFragment;->load()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 53
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 55
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v0

    const-class v1, Lru/cn/tv/mobile/live/LiveNowViewModel;

    invoke-static {p0, v0, v1}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lru/cn/tv/mobile/live/LiveNowViewModel;

    iput-object v0, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->viewModel:Lru/cn/tv/mobile/live/LiveNowViewModel;

    .line 56
    iget-object v0, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->viewModel:Lru/cn/tv/mobile/live/LiveNowViewModel;

    invoke-virtual {v0}, Lru/cn/tv/mobile/live/LiveNowViewModel;->telecasts()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/mobile/live/LiveNowFragment$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/live/LiveNowFragment$$Lambda$0;-><init>(Lru/cn/tv/mobile/live/LiveNowFragment;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 57
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 63
    const v0, 0x7f0c00a3

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 133
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 134
    invoke-direct {p0}, Lru/cn/tv/mobile/live/LiveNowFragment;->stopUpdateProgressTask()V

    .line 135
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 123
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 124
    iget-object v0, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->removeAllViews()V

    .line 128
    :cond_0
    invoke-direct {p0}, Lru/cn/tv/mobile/live/LiveNowFragment;->startUpdateProgressTask()V

    .line 129
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 68
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 70
    const v2, 0x7f090109

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/RecyclerView;

    iput-object v2, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 72
    new-instance v2, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lru/cn/tv/mobile/live/LiveNowFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    iput-object v2, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    .line 73
    new-instance v2, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;

    invoke-direct {v2}, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;-><init>()V

    iput-object v2, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->adapter:Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;

    .line 75
    iget-object v2, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->adapter:Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 76
    iget-object v2, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 78
    new-instance v2, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$Builder;

    invoke-virtual {p0}, Lru/cn/tv/mobile/live/LiveNowFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f08018e

    .line 79
    invoke-virtual {v2, v3}, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$Builder;->drawable(I)Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;

    move-result-object v2

    check-cast v2, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$Builder;

    .line 80
    invoke-virtual {v2}, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$Builder;->build()Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration;

    move-result-object v0

    .line 81
    .local v0, "itemDecoration":Landroid/support/v7/widget/RecyclerView$ItemDecoration;
    iget-object v2, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 83
    iget-object v2, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->adapter:Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;

    new-instance v3, Lru/cn/tv/mobile/live/LiveNowFragment$1;

    invoke-direct {v3, p0}, Lru/cn/tv/mobile/live/LiveNowFragment$1;-><init>(Lru/cn/tv/mobile/live/LiveNowFragment;)V

    invoke-virtual {v2, v3}, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->setOnItemClickListener(Lru/cn/view/CursorRecyclerViewAdapter$OnItemClickListener;)V

    .line 92
    iget-object v2, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, Lru/cn/tv/mobile/live/LiveNowFragment$2;

    invoke-direct {v3, p0}, Lru/cn/tv/mobile/live/LiveNowFragment$2;-><init>(Lru/cn/tv/mobile/live/LiveNowFragment;)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->addOnScrollListener(Landroid/support/v7/widget/RecyclerView$OnScrollListener;)V

    .line 112
    const v2, 0x7f090108

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 113
    const v2, 0x7f090106

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->errorView:Landroid/view/View;

    .line 115
    iget-object v2, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->errorView:Landroid/view/View;

    const v3, 0x7f09017e

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 116
    .local v1, "repeatButton":Landroid/widget/Button;
    new-instance v2, Lru/cn/tv/mobile/live/LiveNowFragment$$Lambda$1;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/live/LiveNowFragment$$Lambda$1;-><init>(Lru/cn/tv/mobile/live/LiveNowFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    invoke-direct {p0}, Lru/cn/tv/mobile/live/LiveNowFragment;->load()V

    .line 119
    return-void
.end method

.method public setLiveTelecastListener(Lru/cn/tv/mobile/live/LiveNowFragment$LiveTelecastListener;)V
    .locals 0
    .param p1, "liveTelecastListener"    # Lru/cn/tv/mobile/live/LiveNowFragment$LiveTelecastListener;

    .prologue
    .line 138
    iput-object p1, p0, Lru/cn/tv/mobile/live/LiveNowFragment;->liveTelecastListener:Lru/cn/tv/mobile/live/LiveNowFragment$LiveTelecastListener;

    .line 139
    return-void
.end method
