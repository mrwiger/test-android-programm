.class public Lcom/my/target/nativeads/factories/NativeAppwallViewsFactory;
.super Ljava/lang/Object;
.source "NativeAppwallViewsFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAppwallAppView(Landroid/content/Context;)Lcom/my/target/nativeads/views/AppwallAdTeaserView;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    new-instance v0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;

    invoke-direct {v0, p0}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static getAppwallAppView(Lcom/my/target/nativeads/banners/NativeAppwallBanner;Landroid/content/Context;)Lcom/my/target/nativeads/views/AppwallAdTeaserView;
    .locals 1
    .param p0, "banner"    # Lcom/my/target/nativeads/banners/NativeAppwallBanner;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-static {p1}, Lcom/my/target/nativeads/factories/NativeAppwallViewsFactory;->getAppwallAppView(Landroid/content/Context;)Lcom/my/target/nativeads/views/AppwallAdTeaserView;

    move-result-object v0

    .line 23
    invoke-virtual {v0, p0}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->setNativeAppwallBanner(Lcom/my/target/nativeads/banners/NativeAppwallBanner;)V

    .line 24
    return-object v0
.end method

.method public static getAppwallView(Landroid/content/Context;)Lcom/my/target/nativeads/views/AppwallAdView;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    new-instance v0, Lcom/my/target/nativeads/views/AppwallAdView;

    invoke-direct {v0, p0}, Lcom/my/target/nativeads/views/AppwallAdView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static getAppwallView(Lcom/my/target/nativeads/NativeAppwallAd;Landroid/content/Context;)Lcom/my/target/nativeads/views/AppwallAdView;
    .locals 1
    .param p0, "ad"    # Lcom/my/target/nativeads/NativeAppwallAd;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    invoke-static {p1}, Lcom/my/target/nativeads/factories/NativeAppwallViewsFactory;->getAppwallView(Landroid/content/Context;)Lcom/my/target/nativeads/views/AppwallAdView;

    move-result-object v0

    .line 35
    invoke-virtual {v0, p0}, Lcom/my/target/nativeads/views/AppwallAdView;->setupView(Lcom/my/target/nativeads/NativeAppwallAd;)V

    .line 36
    return-object v0
.end method
