.class Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;
.super Ljava/lang/Object;
.source "Channel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/multiscreen/Channel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChannelConnectionHandler"
.end annotation


# static fields
.field static final LIBVERSION_CHECK:Ljava/lang/String; = "msfVersion2"

.field static final PING:Ljava/lang/String; = "channel.ping"

.field private static final PONG:Ljava/lang/String; = "pong"


# instance fields
.field private average:D

.field private executor:Ljava/util/concurrent/ScheduledExecutorService;

.field private lastPingReceived:J

.field private longestRT:J

.field private numPings:I

.field private final pingCheckRunnable:Ljava/lang/Runnable;

.field private pingSent:J

.field private pingTimeout:I

.field private running:Z

.field private startTime:J

.field final synthetic this$0:Lcom/samsung/multiscreen/Channel;


# direct methods
.method public constructor <init>(Lcom/samsung/multiscreen/Channel;)V
    .locals 2

    .prologue
    .line 1292
    iput-object p1, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->this$0:Lcom/samsung/multiscreen/Channel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1299
    const/16 v0, 0x3a98

    iput v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->pingTimeout:I

    .line 1303
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->lastPingReceived:J

    .line 1305
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1307
    new-instance v0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler$1;

    invoke-direct {v0, p0}, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler$1;-><init>(Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;)V

    iput-object v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->pingCheckRunnable:Ljava/lang/Runnable;

    .line 1333
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->running:Z

    return-void
.end method

.method static synthetic access$1300(Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;

    .prologue
    .line 1293
    invoke-direct {p0}, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->pingCheck()V

    return-void
.end method

.method private pingCheck()V
    .locals 6

    .prologue
    .line 1322
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 1323
    .local v0, "now":J
    iget-wide v2, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->lastPingReceived:J

    iget v4, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->pingTimeout:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 1325
    const-string v2, "Channel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Ping not received in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->pingTimeout:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1326
    iget-object v2, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->this$0:Lcom/samsung/multiscreen/Channel;

    invoke-static {v2}, Lcom/samsung/multiscreen/Channel;->access$200(Lcom/samsung/multiscreen/Channel;)Lcom/koushikdutta/async/http/WebSocket;

    move-result-object v2

    invoke-interface {v2}, Lcom/koushikdutta/async/http/WebSocket;->close()V

    .line 1331
    :goto_0
    return-void

    .line 1328
    :cond_0
    iget-object v2, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->this$0:Lcom/samsung/multiscreen/Channel;

    const-string v3, "channel.ping"

    const-string v4, "pong"

    iget-object v5, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->this$0:Lcom/samsung/multiscreen/Channel;

    invoke-static {v5}, Lcom/samsung/multiscreen/Channel;->access$900(Lcom/samsung/multiscreen/Channel;)Lcom/samsung/multiscreen/Clients;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/multiscreen/Clients;->me()Lcom/samsung/multiscreen/Client;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/multiscreen/Channel;->publish(Ljava/lang/String;Ljava/lang/Object;Lcom/samsung/multiscreen/Client;)V

    .line 1329
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->pingSent:J

    goto :goto_0
.end method


# virtual methods
.method calculateAverageRT()V
    .locals 6

    .prologue
    .line 1342
    iget-wide v2, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->lastPingReceived:J

    iget-wide v4, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->pingSent:J

    sub-long v0, v2, v4

    .line 1343
    .local v0, "lastRT":J
    iget-wide v2, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->longestRT:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 1344
    iput-wide v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->longestRT:J

    .line 1346
    :cond_0
    iget v2, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->numPings:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->numPings:I

    int-to-double v2, v2

    iget-wide v4, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->average:D

    mul-double/2addr v2, v4

    long-to-double v4, v0

    add-double/2addr v2, v4

    iget v4, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->numPings:I

    int-to-double v4, v4

    div-double/2addr v2, v4

    iput-wide v2, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->average:D

    .line 1348
    return-void
.end method

.method public getPingTimeout()I
    .locals 1

    .prologue
    .line 1299
    iget v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->pingTimeout:I

    return v0
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 1333
    iget-boolean v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->running:Z

    return v0
.end method

.method resetLastPingReceived()V
    .locals 2

    .prologue
    .line 1337
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->lastPingReceived:J

    .line 1339
    return-void
.end method

.method public setPingTimeout(I)V
    .locals 0
    .param p1, "pingTimeout"    # I

    .prologue
    .line 1300
    iput p1, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->pingTimeout:I

    return-void
.end method

.method startPing()V
    .locals 8

    .prologue
    .line 1361
    const/16 v7, 0x1388

    .line 1362
    .local v7, "ping_timeout":I
    iget-boolean v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->running:Z

    if-eqz v0, :cond_0

    .line 1379
    :goto_0
    return-void

    .line 1366
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->stopPing()V

    .line 1367
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->running:Z

    .line 1369
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->numPings:I

    .line 1370
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->average:D

    .line 1371
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->longestRT:J

    .line 1372
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->this$0:Lcom/samsung/multiscreen/Channel;

    const-string v1, "msfVersion2"

    const-string v2, "msfVersion2"

    iget-object v3, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->this$0:Lcom/samsung/multiscreen/Channel;

    invoke-static {v3}, Lcom/samsung/multiscreen/Channel;->access$900(Lcom/samsung/multiscreen/Channel;)Lcom/samsung/multiscreen/Clients;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Clients;->me()Lcom/samsung/multiscreen/Client;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/multiscreen/Channel;->publish(Ljava/lang/String;Ljava/lang/Object;Lcom/samsung/multiscreen/Client;)V

    .line 1373
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->this$0:Lcom/samsung/multiscreen/Channel;

    const-string v1, "channel.ping"

    const-string v2, "pong"

    iget-object v3, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->this$0:Lcom/samsung/multiscreen/Channel;

    invoke-static {v3}, Lcom/samsung/multiscreen/Channel;->access$900(Lcom/samsung/multiscreen/Channel;)Lcom/samsung/multiscreen/Clients;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Clients;->me()Lcom/samsung/multiscreen/Client;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/multiscreen/Channel;->publish(Ljava/lang/String;Ljava/lang/Object;Lcom/samsung/multiscreen/Client;)V

    .line 1374
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->startTime:J

    .line 1375
    iget-wide v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->startTime:J

    iput-wide v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->pingSent:J

    .line 1377
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1378
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->pingCheckRunnable:Ljava/lang/Runnable;

    int-to-long v2, v7

    int-to-long v4, v7

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0
.end method

.method stopPing()V
    .locals 1

    .prologue
    .line 1352
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    if-eqz v0, :cond_0

    .line 1353
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdown()V

    .line 1354
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1357
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->running:Z

    .line 1358
    return-void
.end method
