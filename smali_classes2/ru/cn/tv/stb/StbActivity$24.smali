.class Lru/cn/tv/stb/StbActivity$24;
.super Lru/cn/tv/stb/ListKeyListener;
.source "StbActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/stb/StbActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/stb/StbActivity;


# direct methods
.method constructor <init>(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 1620
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-direct {p0}, Lru/cn/tv/stb/ListKeyListener;-><init>()V

    return-void
.end method


# virtual methods
.method protected keyDown()Z
    .locals 3

    .prologue
    .line 1622
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$5000(Lru/cn/tv/stb/StbActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1623
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$1300(Lru/cn/tv/stb/StbActivity;)V

    .line 1624
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$1800(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$1800(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 1627
    :cond_0
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$3900(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/channels/ChannelsFragment;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/stb/channels/ChannelsFragment;->selectNext()Z

    .line 1629
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$5100(Lru/cn/tv/stb/StbActivity;)V

    .line 1631
    const/4 v0, 0x1

    return v0
.end method

.method protected keyLeft()Z
    .locals 3

    .prologue
    .line 1648
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$5000(Lru/cn/tv/stb/StbActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1649
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$1300(Lru/cn/tv/stb/StbActivity;)V

    .line 1650
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$1800(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$1800(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 1653
    :cond_0
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$5200(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/informing/InformingFragment;

    move-result-object v1

    invoke-static {v0, v1}, Lru/cn/tv/stb/StbActivity;->access$5300(Lru/cn/tv/stb/StbActivity;Landroid/support/v4/app/Fragment;)V

    .line 1655
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$5400(Lru/cn/tv/stb/StbActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$5500(Lru/cn/tv/stb/StbActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1656
    :cond_1
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$5600(Lru/cn/tv/stb/StbActivity;)V

    .line 1661
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1658
    :cond_2
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$400(Lru/cn/tv/stb/StbActivity;)V

    goto :goto_0
.end method

.method protected keyRight()Z
    .locals 3

    .prologue
    .line 1665
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$1800(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/cursor/ChannelCursor;

    .line 1666
    .local v0, "c":Lru/cn/api/provider/cursor/ChannelCursor;
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1, v0}, Lru/cn/tv/stb/StbActivity;->access$5700(Lru/cn/tv/stb/StbActivity;Lru/cn/api/provider/cursor/ChannelCursor;)V

    .line 1668
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getIsDenied()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->allowPurchase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1669
    :cond_0
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    iget-object v2, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v2}, Lru/cn/tv/stb/StbActivity;->access$5200(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/informing/InformingFragment;

    move-result-object v2

    invoke-static {v1, v2}, Lru/cn/tv/stb/StbActivity;->access$5300(Lru/cn/tv/stb/StbActivity;Landroid/support/v4/app/Fragment;)V

    .line 1672
    :cond_1
    const/4 v1, 0x1

    return v1
.end method

.method protected keyUp()Z
    .locals 3

    .prologue
    .line 1635
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$5000(Lru/cn/tv/stb/StbActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1636
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$1300(Lru/cn/tv/stb/StbActivity;)V

    .line 1637
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$1800(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$1800(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 1640
    :cond_0
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$3900(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/channels/ChannelsFragment;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/stb/channels/ChannelsFragment;->selectPrev()Z

    .line 1642
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$24;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$5100(Lru/cn/tv/stb/StbActivity;)V

    .line 1644
    const/4 v0, 0x1

    return v0
.end method
