.class public interface abstract Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;
.super Ljava/lang/Object;
.source "AbstractMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/player/AbstractMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MediaPlayerListener"
.end annotation


# virtual methods
.method public abstract onComplete()V
.end method

.method public abstract onError(I)V
.end method

.method public abstract onStateChanged(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V
.end method

.method public abstract videoSizeChanged(IIF)V
.end method
