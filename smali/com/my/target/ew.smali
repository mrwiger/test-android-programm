.class public final Lcom/my/target/ew;
.super Lcom/my/target/et;
.source "RecyclerVerticalView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/ew$b;,
        Lcom/my/target/ew$a;
    }
.end annotation


# instance fields
.field private final dB:Lcom/my/target/eq;

.field private final dC:Landroid/support/v7/widget/LinearSnapHelper;

.field private final dm:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/my/target/ew;-><init>(Landroid/content/Context;B)V

    .line 52
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/my/target/ew;-><init>(Landroid/content/Context;C)V

    .line 57
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;C)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/my/target/et;-><init>(Landroid/content/Context;)V

    .line 29
    new-instance v0, Lcom/my/target/ew$1;

    invoke-direct {v0, p0}, Lcom/my/target/ew$1;-><init>(Lcom/my/target/ew;)V

    iput-object v0, p0, Lcom/my/target/ew;->dm:Landroid/view/View$OnClickListener;

    .line 62
    new-instance v0, Lcom/my/target/eq;

    invoke-direct {v0, p1}, Lcom/my/target/eq;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ew;->dB:Lcom/my/target/eq;

    .line 63
    new-instance v0, Landroid/support/v7/widget/LinearSnapHelper;

    invoke-direct {v0}, Landroid/support/v7/widget/LinearSnapHelper;-><init>()V

    iput-object v0, p0, Lcom/my/target/ew;->dC:Landroid/support/v7/widget/LinearSnapHelper;

    .line 64
    iget-object v0, p0, Lcom/my/target/ew;->dC:Landroid/support/v7/widget/LinearSnapHelper;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/LinearSnapHelper;->attachToRecyclerView(Landroid/support/v7/widget/RecyclerView;)V

    .line 65
    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 87
    iget-object v0, p0, Lcom/my/target/ew;->dC:Landroid/support/v7/widget/LinearSnapHelper;

    invoke-virtual {p0}, Lcom/my/target/ew;->getCardLayoutManager()Lcom/my/target/eq;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/widget/LinearSnapHelper;->calculateDistanceToFinalSnap(Landroid/support/v7/widget/RecyclerView$LayoutManager;Landroid/view/View;)[I

    move-result-object v0

    .line 88
    if-eqz v0, :cond_0

    .line 90
    aget v0, v0, v2

    invoke-virtual {p0, v0, v2}, Lcom/my/target/ew;->smoothScrollBy(II)V

    .line 92
    :cond_0
    return-void
.end method

.method public final c(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 71
    new-instance v1, Lcom/my/target/ew$b;

    invoke-direct {v1, p1, v0}, Lcom/my/target/ew$b;-><init>(Ljava/util/List;Landroid/content/Context;)V

    .line 72
    iput-object p1, p0, Lcom/my/target/ew;->dp:Ljava/util/List;

    .line 73
    iput-object v1, p0, Lcom/my/target/ew;->dr:Lcom/my/target/er;

    .line 74
    iget-object v1, p0, Lcom/my/target/ew;->dr:Lcom/my/target/er;

    iget-object v2, p0, Lcom/my/target/ew;->dl:Landroid/view/View$OnClickListener;

    .line 1069
    iput-object v2, v1, Lcom/my/target/er;->dl:Landroid/view/View$OnClickListener;

    .line 75
    iget-object v1, p0, Lcom/my/target/ew;->dr:Lcom/my/target/er;

    iget-object v2, p0, Lcom/my/target/ew;->dm:Landroid/view/View$OnClickListener;

    .line 2058
    iput-object v2, v1, Lcom/my/target/er;->dm:Landroid/view/View$OnClickListener;

    .line 76
    iget-object v1, p0, Lcom/my/target/ew;->dB:Lcom/my/target/eq;

    invoke-virtual {v1}, Lcom/my/target/eq;->K()V

    .line 77
    invoke-static {v0}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    .line 78
    iget-object v1, p0, Lcom/my/target/ew;->dB:Lcom/my/target/eq;

    const/4 v2, 0x6

    invoke-virtual {v0, v2}, Lcom/my/target/cm;->n(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/my/target/eq;->f(I)V

    .line 79
    iget-object v0, p0, Lcom/my/target/ew;->dB:Lcom/my/target/eq;

    invoke-virtual {p0, v0}, Lcom/my/target/ew;->setCardLayoutManager(Lcom/my/target/es;)V

    .line 81
    iget-object v0, p0, Lcom/my/target/ew;->dr:Lcom/my/target/er;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 82
    return-void
.end method

.method protected final getCardLayoutManager()Lcom/my/target/eq;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/my/target/ew;->dB:Lcom/my/target/eq;

    return-object v0
.end method

.method protected final bridge synthetic getCardLayoutManager()Lcom/my/target/es;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/my/target/ew;->getCardLayoutManager()Lcom/my/target/eq;

    move-result-object v0

    return-object v0
.end method
