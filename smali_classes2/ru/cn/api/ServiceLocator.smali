.class public Lru/cn/api/ServiceLocator;
.super Ljava/lang/Object;
.source "ServiceLocator.java"


# static fields
.field private static _whereAmI:Lru/cn/api/registry/replies/WhereAmI;

.field private static final authenticators:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lru/cn/api/authorization/AuthorizationInterceptor;",
            ">;"
        }
    .end annotation
.end field

.field private static contractors:Landroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Lru/cn/api/registry/replies/Contractor;",
            ">;"
        }
    .end annotation
.end field

.field private static final jsonConverter:Lretrofit2/Converter$Factory;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 65
    const/4 v1, 0x0

    sput-object v1, Lru/cn/api/ServiceLocator;->_whereAmI:Lru/cn/api/registry/replies/WhereAmI;

    .line 66
    new-instance v1, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v1}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    sput-object v1, Lru/cn/api/ServiceLocator;->contractors:Landroid/support/v4/util/LongSparseArray;

    .line 73
    new-instance v1, Lcom/google/gson/GsonBuilder;

    invoke-direct {v1}, Lcom/google/gson/GsonBuilder;-><init>()V

    const-class v2, Lru/cn/api/medialocator/replies/Rip$StreamingType;

    new-instance v3, Lru/cn/api/medialocator/retrofit/StreamingTypeDeserializer;

    invoke-direct {v3}, Lru/cn/api/medialocator/retrofit/StreamingTypeDeserializer;-><init>()V

    .line 74
    invoke-virtual {v1, v2, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    const-class v2, Lru/cn/api/medialocator/replies/Rip$RipStatus;

    new-instance v3, Lru/cn/api/medialocator/retrofit/RipStatusDeserializer;

    invoke-direct {v3}, Lru/cn/api/medialocator/retrofit/RipStatusDeserializer;-><init>()V

    .line 75
    invoke-virtual {v1, v2, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    const-class v2, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    new-instance v3, Lru/cn/api/tv/retrofit/ChannelCategoriesDeserializer;

    invoke-direct {v3}, Lru/cn/api/tv/retrofit/ChannelCategoriesDeserializer;-><init>()V

    .line 76
    invoke-virtual {v1, v2, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    const-class v2, Lru/cn/api/tv/replies/TelecastImage$Profile;

    new-instance v3, Lru/cn/api/tv/retrofit/ImageProfileSerializer;

    invoke-direct {v3}, Lru/cn/api/tv/retrofit/ImageProfileSerializer;-><init>()V

    .line 77
    invoke-virtual {v1, v2, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    const-class v2, Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    new-instance v3, Lru/cn/api/catalogue/retrofit/RubricUiHintDeserializer;

    invoke-direct {v3}, Lru/cn/api/catalogue/retrofit/RubricUiHintDeserializer;-><init>()V

    .line 78
    invoke-virtual {v1, v2, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    const-class v2, Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

    new-instance v3, Lru/cn/api/catalogue/retrofit/RubricOptionTypeDeserializer;

    invoke-direct {v3}, Lru/cn/api/catalogue/retrofit/RubricOptionTypeDeserializer;-><init>()V

    .line 79
    invoke-virtual {v1, v2, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    const-class v2, Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;

    new-instance v3, Lru/cn/api/catalogue/RubricItemGuideTypeDeserializer;

    invoke-direct {v3}, Lru/cn/api/catalogue/RubricItemGuideTypeDeserializer;-><init>()V

    .line 80
    invoke-virtual {v1, v2, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    const-class v2, Lru/cn/api/registry/replies/ContractorImage$ProfileType;

    new-instance v3, Lru/cn/api/registry/retrofit/ImageProfileTypeDeserializer;

    invoke-direct {v3}, Lru/cn/api/registry/retrofit/ImageProfileTypeDeserializer;-><init>()V

    .line 81
    invoke-virtual {v1, v2, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    const-class v2, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

    new-instance v3, Lru/cn/api/registry/retrofit/SupportedOfficeIdiomsDeserializer;

    invoke-direct {v3}, Lru/cn/api/registry/retrofit/SupportedOfficeIdiomsDeserializer;-><init>()V

    .line 82
    invoke-virtual {v1, v2, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    const-class v2, Lru/cn/api/userdata/replies/Attributes;

    new-instance v3, Lru/cn/api/userdata/retrofit/AttributesDeserializer;

    invoke-direct {v3}, Lru/cn/api/userdata/retrofit/AttributesDeserializer;-><init>()V

    .line 83
    invoke-virtual {v1, v2, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    const-class v2, Lru/cn/api/userdata/replies/Attributes;

    new-instance v3, Lru/cn/api/userdata/retrofit/AttributesSerializer;

    invoke-direct {v3}, Lru/cn/api/userdata/retrofit/AttributesSerializer;-><init>()V

    .line 84
    invoke-virtual {v1, v2, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    const-class v2, Lru/cn/api/userdata/replies/ChangeOperation;

    new-instance v3, Lru/cn/api/userdata/retrofit/ChangeOperationSerializer;

    invoke-direct {v3}, Lru/cn/api/userdata/retrofit/ChangeOperationSerializer;-><init>()V

    .line 85
    invoke-virtual {v1, v2, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    .line 87
    .local v0, "gsonBuilder":Lcom/google/gson/GsonBuilder;
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 88
    new-instance v1, Lru/cn/api/tv/retrofit/DescriptionExcluder;

    invoke-direct {v1}, Lru/cn/api/tv/retrofit/DescriptionExcluder;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/gson/GsonBuilder;->addDeserializationExclusionStrategy(Lcom/google/gson/ExclusionStrategy;)Lcom/google/gson/GsonBuilder;

    .line 91
    :cond_0
    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v1

    invoke-static {v1}, Lretrofit2/converter/gson/GsonConverterFactory;->create(Lcom/google/gson/Gson;)Lretrofit2/converter/gson/GsonConverterFactory;

    move-result-object v1

    sput-object v1, Lru/cn/api/ServiceLocator;->jsonConverter:Lretrofit2/Converter$Factory;

    .line 92
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lru/cn/api/ServiceLocator;->authenticators:Ljava/util/Map;

    .line 93
    return-void
.end method

.method public static auth(Landroid/content/Context;J)Lru/cn/api/authorization/retrofit/AuthApi;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contractorId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 254
    sget-object v4, Lru/cn/api/registry/replies/Service$Type;->auth:Lru/cn/api/registry/replies/Service$Type;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {p0, v4, v5}, Lru/cn/api/ServiceLocator;->getRegistryService(Landroid/content/Context;Lru/cn/api/registry/replies/Service$Type;Ljava/lang/Long;)Lru/cn/api/registry/replies/Service;

    move-result-object v2

    .line 255
    .local v2, "service":Lru/cn/api/registry/replies/Service;
    if-nez v2, :cond_0

    .line 267
    :goto_0
    return-object v3

    .line 258
    :cond_0
    const/4 v4, 0x0

    new-array v4, v4, [Lokhttp3/Interceptor;

    invoke-static {v3, v4}, Lru/cn/api/ServiceLocator;->createCallFactory(Lokhttp3/Authenticator;[Lokhttp3/Interceptor;)Lokhttp3/Call$Factory;

    move-result-object v0

    .line 260
    .local v0, "callFactory":Lokhttp3/Call$Factory;
    new-instance v3, Lretrofit2/Retrofit$Builder;

    invoke-direct {v3}, Lretrofit2/Retrofit$Builder;-><init>()V

    .line 261
    invoke-virtual {v2}, Lru/cn/api/registry/replies/Service;->getLocation()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lretrofit2/Retrofit$Builder;->baseUrl(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object v3

    sget-object v4, Lru/cn/api/ServiceLocator;->jsonConverter:Lretrofit2/Converter$Factory;

    .line 262
    invoke-virtual {v3, v4}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v3

    .line 263
    invoke-virtual {v3, v0}, Lretrofit2/Retrofit$Builder;->callFactory(Lokhttp3/Call$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v3

    .line 264
    invoke-static {}, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;->create()Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;

    move-result-object v4

    invoke-virtual {v3, v4}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v3

    .line 265
    invoke-virtual {v3}, Lretrofit2/Retrofit$Builder;->build()Lretrofit2/Retrofit;

    move-result-object v1

    .line 267
    .local v1, "retrofit":Lretrofit2/Retrofit;
    const-class v3, Lru/cn/api/authorization/retrofit/AuthApi;

    invoke-virtual {v1, v3}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lru/cn/api/authorization/retrofit/AuthApi;

    goto :goto_0
.end method

.method public static catalogue(Landroid/content/Context;)Lru/cn/api/catalogue/retrofit/CatalogueApi;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 219
    sget-object v4, Lru/cn/api/registry/replies/Service$Type;->catalogue:Lru/cn/api/registry/replies/Service$Type;

    invoke-static {p0, v4}, Lru/cn/api/ServiceLocator;->getRegistryService(Landroid/content/Context;Lru/cn/api/registry/replies/Service$Type;)Lru/cn/api/registry/replies/Service;

    move-result-object v3

    .line 220
    .local v3, "service":Lru/cn/api/registry/replies/Service;
    if-nez v3, :cond_0

    .line 221
    const/4 v4, 0x0

    .line 237
    :goto_0
    return-object v4

    .line 224
    :cond_0
    invoke-virtual {v3}, Lru/cn/api/registry/replies/Service;->getContractorId()J

    move-result-wide v4

    invoke-static {v4, v5}, Lru/cn/api/ServiceLocator;->getAuthenticator(J)Lru/cn/api/authorization/AuthorizationInterceptor;

    move-result-object v0

    .line 225
    .local v0, "authenticator":Lru/cn/api/authorization/AuthorizationInterceptor;
    if-nez v0, :cond_1

    .line 226
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Missing authenticator for catalogue"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 229
    :cond_1
    const/4 v4, 0x1

    new-array v4, v4, [Lokhttp3/Interceptor;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v0, v4}, Lru/cn/api/ServiceLocator;->createCallFactory(Lokhttp3/Authenticator;[Lokhttp3/Interceptor;)Lokhttp3/Call$Factory;

    move-result-object v1

    .line 230
    .local v1, "callFactory":Lokhttp3/Call$Factory;
    new-instance v4, Lretrofit2/Retrofit$Builder;

    invoke-direct {v4}, Lretrofit2/Retrofit$Builder;-><init>()V

    .line 231
    invoke-virtual {v3}, Lru/cn/api/registry/replies/Service;->getLocation()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lretrofit2/Retrofit$Builder;->baseUrl(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object v4

    sget-object v5, Lru/cn/api/ServiceLocator;->jsonConverter:Lretrofit2/Converter$Factory;

    .line 232
    invoke-virtual {v4, v5}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v4

    .line 233
    invoke-virtual {v4, v1}, Lretrofit2/Retrofit$Builder;->callFactory(Lokhttp3/Call$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v4

    .line 234
    invoke-static {}, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;->create()Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;

    move-result-object v5

    invoke-virtual {v4, v5}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v4

    .line 235
    invoke-virtual {v4}, Lretrofit2/Retrofit$Builder;->build()Lretrofit2/Retrofit;

    move-result-object v2

    .line 237
    .local v2, "retrofit":Lretrofit2/Retrofit;
    const-class v4, Lru/cn/api/catalogue/retrofit/CatalogueApi;

    invoke-virtual {v2, v4}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/api/catalogue/retrofit/CatalogueApi;

    goto :goto_0
.end method

.method public static clearWhereAmI()V
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    sput-object v0, Lru/cn/api/ServiceLocator;->_whereAmI:Lru/cn/api/registry/replies/WhereAmI;

    .line 121
    return-void
.end method

.method public static contractorsByService(Landroid/content/Context;Lru/cn/api/registry/replies/Service$Type;)Ljava/util/List;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # Lru/cn/api/registry/replies/Service$Type;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lru/cn/api/registry/replies/Service$Type;",
            ")",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/registry/replies/Contractor;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 345
    invoke-static {p0, p1}, Lru/cn/api/ServiceLocator;->getRegistryServices(Landroid/content/Context;Lru/cn/api/registry/replies/Service$Type;)Ljava/util/List;

    move-result-object v3

    .line 347
    .local v3, "services":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/registry/replies/Service;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 348
    .local v1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/api/registry/replies/Service;

    .line 349
    .local v2, "item":Lru/cn/api/registry/replies/Service;
    invoke-virtual {v2}, Lru/cn/api/registry/replies/Service;->getContractorId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 353
    .end local v2    # "item":Lru/cn/api/registry/replies/Service;
    :cond_0
    :try_start_0
    invoke-static {}, Lru/cn/api/ServiceLocator;->registry()Lru/cn/api/registry/retrofit/RegistryApi;

    move-result-object v4

    const-string v5, ","

    .line 354
    invoke-static {v5, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lru/cn/api/registry/retrofit/RegistryApi;->contractors(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v4

    .line 355
    invoke-virtual {v4}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/api/registry/replies/ContractorsList;

    iget-object v4, v4, Lru/cn/api/registry/replies/ContractorsList;->contractors:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 360
    :goto_1
    return-object v4

    .line 357
    :catch_0
    move-exception v0

    .line 358
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .line 360
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private static varargs createCallFactory(Lokhttp3/Authenticator;[Lokhttp3/Interceptor;)Lokhttp3/Call$Factory;
    .locals 5
    .param p0, "authenticator"    # Lokhttp3/Authenticator;
    .param p1, "interceptors"    # [Lokhttp3/Interceptor;

    .prologue
    .line 364
    new-instance v0, Lru/cn/utils/http/HttpClient$Builder;

    invoke-direct {v0}, Lru/cn/utils/http/HttpClient$Builder;-><init>()V

    .line 366
    .local v0, "builder":Lru/cn/utils/http/HttpClient$Builder;
    if-eqz p0, :cond_0

    .line 367
    invoke-virtual {v0, p0}, Lru/cn/utils/http/HttpClient$Builder;->setAuthenticator(Lokhttp3/Authenticator;)Lru/cn/utils/http/HttpClient$Builder;

    .line 370
    :cond_0
    if-eqz p1, :cond_2

    array-length v3, p1

    if-lez v3, :cond_2

    .line 371
    array-length v4, p1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v2, p1, v3

    .line 372
    .local v2, "interceptor":Lokhttp3/Interceptor;
    if-eqz v2, :cond_1

    .line 373
    invoke-virtual {v0, v2}, Lru/cn/utils/http/HttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lru/cn/utils/http/HttpClient$Builder;

    .line 371
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 378
    .end local v2    # "interceptor":Lokhttp3/Interceptor;
    :cond_2
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 379
    .local v1, "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "User-Agent"

    invoke-static {}, Lru/cn/utils/http/HttpClient;->defaultUserAgent()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 380
    const-string v3, "Accept-Language"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 381
    new-instance v3, Lru/cn/api/HeaderInterceptor;

    invoke-direct {v3, v1}, Lru/cn/api/HeaderInterceptor;-><init>(Ljava/util/Map;)V

    invoke-virtual {v0, v3}, Lru/cn/utils/http/HttpClient$Builder;->addNetworkInterceptor(Lokhttp3/Interceptor;)Lru/cn/utils/http/HttpClient$Builder;

    .line 383
    invoke-virtual {v0}, Lru/cn/utils/http/HttpClient$Builder;->build()Lru/cn/utils/http/HttpClient;

    move-result-object v3

    invoke-virtual {v3}, Lru/cn/utils/http/HttpClient;->callFactory()Lokhttp3/Call$Factory;

    move-result-object v3

    return-object v3
.end method

.method private static getAuthenticator(J)Lru/cn/api/authorization/AuthorizationInterceptor;
    .locals 4
    .param p0, "contractorId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 409
    sget-object v1, Lru/cn/api/ServiceLocator;->authenticators:Ljava/util/Map;

    monitor-enter v1

    .line 410
    :try_start_0
    sget-object v0, Lru/cn/api/ServiceLocator;->authenticators:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/authorization/AuthorizationInterceptor;

    monitor-exit v1

    return-object v0

    .line 411
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getContractor(J)Lru/cn/api/registry/replies/Contractor;
    .locals 8
    .param p0, "id"    # J

    .prologue
    .line 124
    const-wide/16 v6, 0x0

    cmp-long v5, p0, v6

    if-gtz v5, :cond_1

    .line 125
    const/4 v2, 0x0

    .line 145
    :cond_0
    :goto_0
    return-object v2

    .line 127
    :cond_1
    sget-object v5, Lru/cn/api/ServiceLocator;->contractors:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v5, p0, p1}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/api/registry/replies/Contractor;

    .line 128
    .local v2, "contractor":Lru/cn/api/registry/replies/Contractor;
    if-nez v2, :cond_0

    .line 132
    :try_start_0
    invoke-static {}, Lru/cn/api/ServiceLocator;->registry()Lru/cn/api/registry/retrofit/RegistryApi;

    move-result-object v5

    .line 133
    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lru/cn/api/registry/retrofit/RegistryApi;->contractors(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v5

    .line 134
    invoke-virtual {v5}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/api/registry/replies/ContractorsList;

    .line 136
    .local v4, "response":Lru/cn/api/registry/replies/ContractorsList;
    iget-object v5, v4, Lru/cn/api/registry/replies/ContractorsList;->contractors:Ljava/util/List;

    if-eqz v5, :cond_0

    iget-object v5, v4, Lru/cn/api/registry/replies/ContractorsList;->contractors:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 137
    iget-object v5, v4, Lru/cn/api/registry/replies/ContractorsList;->contractors:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lru/cn/api/registry/replies/Contractor;

    move-object v2, v0

    .line 138
    sget-object v5, Lru/cn/api/ServiceLocator;->contractors:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v5, p0, p1, v2}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 141
    .end local v4    # "response":Lru/cn/api/registry/replies/ContractorsList;
    :catch_0
    move-exception v3

    .line 142
    .local v3, "e":Ljava/lang/Exception;
    invoke-static {v3}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static getRegistryService(Landroid/content/Context;Lru/cn/api/registry/replies/Service$Type;)Lru/cn/api/registry/replies/Service;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # Lru/cn/api/registry/replies/Service$Type;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 387
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lru/cn/api/ServiceLocator;->getRegistryService(Landroid/content/Context;Lru/cn/api/registry/replies/Service$Type;Ljava/lang/Long;)Lru/cn/api/registry/replies/Service;

    move-result-object v0

    return-object v0
.end method

.method public static getRegistryService(Landroid/content/Context;Lru/cn/api/registry/replies/Service$Type;Ljava/lang/Long;)Lru/cn/api/registry/replies/Service;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # Lru/cn/api/registry/replies/Service$Type;
    .param p2, "contractorId"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 324
    invoke-static {p0, p1}, Lru/cn/api/ServiceLocator;->getRegistryServices(Landroid/content/Context;Lru/cn/api/registry/replies/Service$Type;)Ljava/util/List;

    move-result-object v1

    .line 325
    .local v1, "services":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/registry/replies/Service;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/registry/replies/Service;

    .line 326
    .local v0, "service":Lru/cn/api/registry/replies/Service;
    if-eqz p2, :cond_1

    .line 327
    invoke-virtual {v0}, Lru/cn/api/registry/replies/Service;->getContractorId()J

    move-result-wide v4

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 331
    .end local v0    # "service":Lru/cn/api/registry/replies/Service;
    :cond_1
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getRegistryServices(Landroid/content/Context;Lru/cn/api/registry/replies/Service$Type;)Ljava/util/List;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # Lru/cn/api/registry/replies/Service$Type;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lru/cn/api/registry/replies/Service$Type;",
            ")",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/registry/replies/Service;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 151
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 152
    .local v0, "ret":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/registry/replies/Service;>;"
    invoke-static {p0}, Lru/cn/api/ServiceLocator;->getWhereAmI(Landroid/content/Context;)Lru/cn/api/registry/replies/WhereAmI;

    move-result-object v2

    .line 153
    .local v2, "whereAmI":Lru/cn/api/registry/replies/WhereAmI;
    if-eqz v2, :cond_2

    .line 154
    iget-object v3, v2, Lru/cn/api/registry/replies/WhereAmI;->services:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/registry/replies/Service;

    .line 155
    .local v1, "service":Lru/cn/api/registry/replies/Service;
    if-eqz p1, :cond_1

    iget-object v4, v1, Lru/cn/api/registry/replies/Service;->serviceType:Lru/cn/api/registry/replies/Service$Type;

    invoke-virtual {p1, v4}, Lru/cn/api/registry/replies/Service$Type;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 156
    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 160
    .end local v1    # "service":Lru/cn/api/registry/replies/Service;
    :cond_2
    return-object v0
.end method

.method public static declared-synchronized getWhereAmI(Landroid/content/Context;)Lru/cn/api/registry/replies/WhereAmI;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 97
    const-class v3, Lru/cn/api/ServiceLocator;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lru/cn/api/ServiceLocator;->_whereAmI:Lru/cn/api/registry/replies/WhereAmI;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    .line 99
    :try_start_1
    invoke-static {}, Lru/cn/api/ServiceLocator;->registry()Lru/cn/api/registry/retrofit/RegistryApi;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 100
    invoke-interface {v2, v4, v5}, Lru/cn/api/registry/retrofit/RegistryApi;->whereAmI(Ljava/lang/Long;Ljava/lang/Long;)Lio/reactivex/Single;

    move-result-object v2

    .line 101
    invoke-virtual {v2}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/api/registry/replies/WhereAmI;

    sput-object v2, Lru/cn/api/ServiceLocator;->_whereAmI:Lru/cn/api/registry/replies/WhereAmI;

    .line 103
    sget-object v2, Lru/cn/api/ServiceLocator;->_whereAmI:Lru/cn/api/registry/replies/WhereAmI;

    iget-object v0, v2, Lru/cn/api/registry/replies/WhereAmI;->contractor:Lru/cn/api/registry/replies/Contractor;

    .line 104
    .local v0, "contractor":Lru/cn/api/registry/replies/Contractor;
    if-eqz v0, :cond_0

    .line 105
    sget-object v2, Lru/cn/api/ServiceLocator;->contractors:Landroid/support/v4/util/LongSparseArray;

    iget-wide v4, v0, Lru/cn/api/registry/replies/Contractor;->contractorId:J

    invoke-virtual {v2, v4, v5, v0}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 108
    :cond_0
    invoke-static {p0}, Lru/cn/api/ServiceLocator;->updateServices(Landroid/content/Context;)V

    .line 111
    invoke-static {p0}, Lru/cn/domain/statistics/AnalyticsManager;->trackGeoLocation(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 116
    .end local v0    # "contractor":Lru/cn/api/registry/replies/Contractor;
    :cond_1
    :try_start_2
    sget-object v2, Lru/cn/api/ServiceLocator;->_whereAmI:Lru/cn/api/registry/replies/WhereAmI;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v3

    return-object v2

    .line 112
    :catch_0
    move-exception v1

    .line 113
    .local v1, "e":Ljava/lang/Exception;
    :try_start_3
    new-instance v2, Ljava/lang/Exception;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 97
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static iptv(J)Lru/cn/api/iptv/retrofit/IptvApi;
    .locals 8
    .param p0, "contractorId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 271
    new-instance v5, Lru/cn/utils/http/HttpClient$Builder;

    invoke-direct {v5}, Lru/cn/utils/http/HttpClient$Builder;-><init>()V

    const-wide/16 v6, 0x2710

    .line 272
    invoke-virtual {v5, v6, v7}, Lru/cn/utils/http/HttpClient$Builder;->setConnectionTimeoutMillis(J)Lru/cn/utils/http/HttpClient$Builder;

    move-result-object v5

    const-wide/16 v6, 0x3a98

    .line 273
    invoke-virtual {v5, v6, v7}, Lru/cn/utils/http/HttpClient$Builder;->setSocketTimeoutMillis(J)Lru/cn/utils/http/HttpClient$Builder;

    move-result-object v1

    .line 275
    .local v1, "builder":Lru/cn/utils/http/HttpClient$Builder;
    invoke-static {p0, p1}, Lru/cn/api/ServiceLocator;->getAuthenticator(J)Lru/cn/api/authorization/AuthorizationInterceptor;

    move-result-object v0

    .line 276
    .local v0, "authenticator":Lru/cn/api/authorization/AuthorizationInterceptor;
    if-eqz v0, :cond_0

    .line 277
    invoke-virtual {v1, v0}, Lru/cn/utils/http/HttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lru/cn/utils/http/HttpClient$Builder;

    .line 278
    invoke-virtual {v1, v0}, Lru/cn/utils/http/HttpClient$Builder;->setAuthenticator(Lokhttp3/Authenticator;)Lru/cn/utils/http/HttpClient$Builder;

    .line 281
    :cond_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 282
    .local v2, "headers":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-wide/16 v6, 0x0

    cmp-long v5, p0, v6

    if-lez v5, :cond_1

    .line 283
    const-string v5, "Client-Capabilities"

    const-string v6, "paid_content,bytefog"

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    :cond_1
    const-string v5, "User-Agent"

    invoke-static {}, Lru/cn/utils/http/HttpClient;->defaultUserAgent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    const-string v5, "Accept-Language"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    new-instance v5, Lru/cn/api/HeaderInterceptor;

    invoke-direct {v5, v2}, Lru/cn/api/HeaderInterceptor;-><init>(Ljava/util/Map;)V

    invoke-virtual {v1, v5}, Lru/cn/utils/http/HttpClient$Builder;->addNetworkInterceptor(Lokhttp3/Interceptor;)Lru/cn/utils/http/HttpClient$Builder;

    .line 289
    invoke-virtual {v1}, Lru/cn/utils/http/HttpClient$Builder;->build()Lru/cn/utils/http/HttpClient;

    move-result-object v3

    .line 291
    .local v3, "httpClient":Lru/cn/utils/http/HttpClient;
    new-instance v5, Lretrofit2/Retrofit$Builder;

    invoke-direct {v5}, Lretrofit2/Retrofit$Builder;-><init>()V

    const-string v6, "http://api.peers.tv/"

    .line 293
    invoke-virtual {v5, v6}, Lretrofit2/Retrofit$Builder;->baseUrl(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object v5

    new-instance v6, Lru/cn/api/iptv/retrofit/IPTVConverterFactory;

    invoke-direct {v6}, Lru/cn/api/iptv/retrofit/IPTVConverterFactory;-><init>()V

    .line 294
    invoke-virtual {v5, v6}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v5

    .line 295
    invoke-static {}, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;->create()Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;

    move-result-object v6

    invoke-virtual {v5, v6}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v5

    .line 296
    invoke-virtual {v3}, Lru/cn/utils/http/HttpClient;->callFactory()Lokhttp3/Call$Factory;

    move-result-object v6

    invoke-virtual {v5, v6}, Lretrofit2/Retrofit$Builder;->callFactory(Lokhttp3/Call$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v5

    .line 297
    invoke-virtual {v5}, Lretrofit2/Retrofit$Builder;->build()Lretrofit2/Retrofit;

    move-result-object v4

    .line 299
    .local v4, "retrofit":Lretrofit2/Retrofit;
    const-class v5, Lru/cn/api/iptv/retrofit/IptvApi;

    invoke-virtual {v4, v5}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lru/cn/api/iptv/retrofit/IptvApi;

    return-object v5
.end method

.method public static mediaGuide(Landroid/content/Context;)Lru/cn/api/tv/retrofit/MediaGuideApi;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 203
    sget-object v4, Lru/cn/api/registry/replies/Service$Type;->tv_guide:Lru/cn/api/registry/replies/Service$Type;

    invoke-static {p0, v4}, Lru/cn/api/ServiceLocator;->getRegistryService(Landroid/content/Context;Lru/cn/api/registry/replies/Service$Type;)Lru/cn/api/registry/replies/Service;

    move-result-object v2

    .line 204
    .local v2, "service":Lru/cn/api/registry/replies/Service;
    if-nez v2, :cond_0

    .line 215
    :goto_0
    return-object v3

    .line 207
    :cond_0
    const/4 v4, 0x0

    new-array v4, v4, [Lokhttp3/Interceptor;

    invoke-static {v3, v4}, Lru/cn/api/ServiceLocator;->createCallFactory(Lokhttp3/Authenticator;[Lokhttp3/Interceptor;)Lokhttp3/Call$Factory;

    move-result-object v0

    .line 208
    .local v0, "callFactory":Lokhttp3/Call$Factory;
    new-instance v3, Lretrofit2/Retrofit$Builder;

    invoke-direct {v3}, Lretrofit2/Retrofit$Builder;-><init>()V

    .line 209
    invoke-virtual {v2}, Lru/cn/api/registry/replies/Service;->getLocation()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lretrofit2/Retrofit$Builder;->baseUrl(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object v3

    sget-object v4, Lru/cn/api/ServiceLocator;->jsonConverter:Lretrofit2/Converter$Factory;

    .line 210
    invoke-virtual {v3, v4}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v3

    .line 211
    invoke-virtual {v3, v0}, Lretrofit2/Retrofit$Builder;->callFactory(Lokhttp3/Call$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v3

    .line 212
    invoke-static {}, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;->create()Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;

    move-result-object v4

    invoke-virtual {v3, v4}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v3

    .line 213
    invoke-virtual {v3}, Lretrofit2/Retrofit$Builder;->build()Lretrofit2/Retrofit;

    move-result-object v1

    .line 215
    .local v1, "retrofit":Lretrofit2/Retrofit;
    const-class v3, Lru/cn/api/tv/retrofit/MediaGuideApi;

    invoke-virtual {v1, v3}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lru/cn/api/tv/retrofit/MediaGuideApi;

    goto :goto_0
.end method

.method public static mediaLocator(Landroid/content/Context;J)Lru/cn/api/medialocator/retrofit/MediaLocatorApi;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contractorId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 164
    sget-object v4, Lru/cn/api/registry/replies/Service$Type;->media_locator:Lru/cn/api/registry/replies/Service$Type;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {p0, v4, v5}, Lru/cn/api/ServiceLocator;->getRegistryService(Landroid/content/Context;Lru/cn/api/registry/replies/Service$Type;Ljava/lang/Long;)Lru/cn/api/registry/replies/Service;

    move-result-object v3

    .line 165
    .local v3, "service":Lru/cn/api/registry/replies/Service;
    if-nez v3, :cond_0

    .line 166
    const/4 v4, 0x0

    .line 178
    :goto_0
    return-object v4

    .line 168
    :cond_0
    invoke-virtual {v3}, Lru/cn/api/registry/replies/Service;->getContractorId()J

    move-result-wide v4

    invoke-static {v4, v5}, Lru/cn/api/ServiceLocator;->getAuthenticator(J)Lru/cn/api/authorization/AuthorizationInterceptor;

    move-result-object v0

    .line 169
    .local v0, "authenticator":Lru/cn/api/authorization/AuthorizationInterceptor;
    const/4 v4, 0x1

    new-array v4, v4, [Lokhttp3/Interceptor;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v0, v4}, Lru/cn/api/ServiceLocator;->createCallFactory(Lokhttp3/Authenticator;[Lokhttp3/Interceptor;)Lokhttp3/Call$Factory;

    move-result-object v1

    .line 171
    .local v1, "callFactory":Lokhttp3/Call$Factory;
    new-instance v4, Lretrofit2/Retrofit$Builder;

    invoke-direct {v4}, Lretrofit2/Retrofit$Builder;-><init>()V

    .line 172
    invoke-virtual {v3}, Lru/cn/api/registry/replies/Service;->getLocation()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lretrofit2/Retrofit$Builder;->baseUrl(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object v4

    sget-object v5, Lru/cn/api/ServiceLocator;->jsonConverter:Lretrofit2/Converter$Factory;

    .line 173
    invoke-virtual {v4, v5}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v4

    .line 174
    invoke-virtual {v4, v1}, Lretrofit2/Retrofit$Builder;->callFactory(Lokhttp3/Call$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v4

    .line 175
    invoke-static {}, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;->create()Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;

    move-result-object v5

    invoke-virtual {v4, v5}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v4

    .line 176
    invoke-virtual {v4}, Lretrofit2/Retrofit$Builder;->build()Lretrofit2/Retrofit;

    move-result-object v2

    .line 178
    .local v2, "retrofit":Lretrofit2/Retrofit;
    const-class v4, Lru/cn/api/medialocator/retrofit/MediaLocatorApi;

    invoke-virtual {v2, v4}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/api/medialocator/retrofit/MediaLocatorApi;

    goto :goto_0
.end method

.method public static moneyMiner(Landroid/content/Context;)Lru/cn/api/money_miner/retrofit/MoneyMinerAPI;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 182
    sget-object v4, Lru/cn/api/registry/replies/Service$Type;->money_miner:Lru/cn/api/registry/replies/Service$Type;

    invoke-static {p0, v4}, Lru/cn/api/ServiceLocator;->getRegistryService(Landroid/content/Context;Lru/cn/api/registry/replies/Service$Type;)Lru/cn/api/registry/replies/Service;

    move-result-object v3

    .line 183
    .local v3, "service":Lru/cn/api/registry/replies/Service;
    if-nez v3, :cond_0

    .line 184
    const/4 v4, 0x0

    .line 199
    :goto_0
    return-object v4

    .line 186
    :cond_0
    invoke-virtual {v3}, Lru/cn/api/registry/replies/Service;->getContractorId()J

    move-result-wide v4

    invoke-static {v4, v5}, Lru/cn/api/ServiceLocator;->getAuthenticator(J)Lru/cn/api/authorization/AuthorizationInterceptor;

    move-result-object v0

    .line 187
    .local v0, "authenticator":Lru/cn/api/authorization/AuthorizationInterceptor;
    if-nez v0, :cond_1

    .line 188
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Missing authenticator for money miner"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 191
    :cond_1
    const/4 v4, 0x1

    new-array v4, v4, [Lokhttp3/Interceptor;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v0, v4}, Lru/cn/api/ServiceLocator;->createCallFactory(Lokhttp3/Authenticator;[Lokhttp3/Interceptor;)Lokhttp3/Call$Factory;

    move-result-object v1

    .line 192
    .local v1, "callFactory":Lokhttp3/Call$Factory;
    new-instance v4, Lretrofit2/Retrofit$Builder;

    invoke-direct {v4}, Lretrofit2/Retrofit$Builder;-><init>()V

    .line 193
    invoke-virtual {v3}, Lru/cn/api/registry/replies/Service;->getLocation()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lretrofit2/Retrofit$Builder;->baseUrl(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object v4

    sget-object v5, Lru/cn/api/ServiceLocator;->jsonConverter:Lretrofit2/Converter$Factory;

    .line 194
    invoke-virtual {v4, v5}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v4

    .line 195
    invoke-virtual {v4, v1}, Lretrofit2/Retrofit$Builder;->callFactory(Lokhttp3/Call$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v4

    .line 196
    invoke-static {}, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;->create()Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;

    move-result-object v5

    invoke-virtual {v4, v5}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v4

    .line 197
    invoke-virtual {v4}, Lretrofit2/Retrofit$Builder;->build()Lretrofit2/Retrofit;

    move-result-object v2

    .line 199
    .local v2, "retrofit":Lretrofit2/Retrofit;
    const-class v4, Lru/cn/api/money_miner/retrofit/MoneyMinerAPI;

    invoke-virtual {v2, v4}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/api/money_miner/retrofit/MoneyMinerAPI;

    goto :goto_0
.end method

.method public static registry()Lru/cn/api/registry/retrofit/RegistryApi;
    .locals 4

    .prologue
    .line 241
    const/4 v2, 0x0

    const/4 v3, 0x0

    new-array v3, v3, [Lokhttp3/Interceptor;

    invoke-static {v2, v3}, Lru/cn/api/ServiceLocator;->createCallFactory(Lokhttp3/Authenticator;[Lokhttp3/Interceptor;)Lokhttp3/Call$Factory;

    move-result-object v0

    .line 243
    .local v0, "callFactory":Lokhttp3/Call$Factory;
    new-instance v2, Lretrofit2/Retrofit$Builder;

    invoke-direct {v2}, Lretrofit2/Retrofit$Builder;-><init>()V

    sget-object v3, Lru/cn/utils/customization/Config;->API_URL:Ljava/lang/String;

    .line 244
    invoke-virtual {v2, v3}, Lretrofit2/Retrofit$Builder;->baseUrl(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object v2

    sget-object v3, Lru/cn/api/ServiceLocator;->jsonConverter:Lretrofit2/Converter$Factory;

    .line 245
    invoke-virtual {v2, v3}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v2

    .line 246
    invoke-virtual {v2, v0}, Lretrofit2/Retrofit$Builder;->callFactory(Lokhttp3/Call$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v2

    .line 247
    invoke-static {}, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;->create()Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;

    move-result-object v3

    invoke-virtual {v2, v3}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v2

    .line 248
    invoke-virtual {v2}, Lretrofit2/Retrofit$Builder;->build()Lretrofit2/Retrofit;

    move-result-object v1

    .line 250
    .local v1, "retrofit":Lretrofit2/Retrofit;
    const-class v2, Lru/cn/api/registry/retrofit/RegistryApi;

    invoke-virtual {v1, v2}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/api/registry/retrofit/RegistryApi;

    return-object v2
.end method

.method private static updateServices(Landroid/content/Context;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 391
    sget-object v6, Lru/cn/api/ServiceLocator;->authenticators:Ljava/util/Map;

    monitor-enter v6

    .line 392
    :try_start_0
    sget-object v5, Lru/cn/api/ServiceLocator;->authenticators:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->clear()V

    .line 394
    sget-object v5, Lru/cn/api/ServiceLocator;->_whereAmI:Lru/cn/api/registry/replies/WhereAmI;

    iget-object v5, v5, Lru/cn/api/registry/replies/WhereAmI;->services:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/api/registry/replies/Service;

    .line 395
    .local v4, "service":Lru/cn/api/registry/replies/Service;
    iget-object v7, v4, Lru/cn/api/registry/replies/Service;->serviceType:Lru/cn/api/registry/replies/Service$Type;

    sget-object v8, Lru/cn/api/registry/replies/Service$Type;->auth:Lru/cn/api/registry/replies/Service$Type;

    if-ne v7, v8, :cond_0

    .line 396
    invoke-virtual {v4}, Lru/cn/api/registry/replies/Service;->getContractorId()J

    move-result-wide v2

    .line 397
    .local v2, "contractorId":J
    invoke-static {p0, v2, v3}, Lru/cn/api/ServiceLocator;->auth(Landroid/content/Context;J)Lru/cn/api/authorization/retrofit/AuthApi;

    move-result-object v0

    .line 398
    .local v0, "auth":Lru/cn/api/authorization/retrofit/AuthApi;
    if-eqz v0, :cond_0

    .line 399
    new-instance v1, Lru/cn/api/authorization/AuthorizationInterceptor;

    invoke-direct {v1, p0, v0, v2, v3}, Lru/cn/api/authorization/AuthorizationInterceptor;-><init>(Landroid/content/Context;Lru/cn/api/authorization/retrofit/AuthApi;J)V

    .line 400
    .local v1, "authenticator":Lru/cn/api/authorization/AuthorizationInterceptor;
    sget-object v7, Lru/cn/api/ServiceLocator;->authenticators:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v7, v8, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 404
    .end local v0    # "auth":Lru/cn/api/authorization/retrofit/AuthApi;
    .end local v1    # "authenticator":Lru/cn/api/authorization/AuthorizationInterceptor;
    .end local v2    # "contractorId":J
    .end local v4    # "service":Lru/cn/api/registry/replies/Service;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    :cond_1
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 405
    return-void
.end method

.method public static userData(Landroid/content/Context;)Lru/cn/api/userdata/retrofit/UserDataApi;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 303
    sget-object v4, Lru/cn/api/registry/replies/Service$Type;->user_data:Lru/cn/api/registry/replies/Service$Type;

    invoke-static {p0, v4}, Lru/cn/api/ServiceLocator;->getRegistryService(Landroid/content/Context;Lru/cn/api/registry/replies/Service$Type;)Lru/cn/api/registry/replies/Service;

    move-result-object v3

    .line 304
    .local v3, "service":Lru/cn/api/registry/replies/Service;
    if-nez v3, :cond_0

    .line 305
    const/4 v4, 0x0

    .line 318
    :goto_0
    return-object v4

    .line 307
    :cond_0
    invoke-virtual {v3}, Lru/cn/api/registry/replies/Service;->getContractorId()J

    move-result-wide v4

    invoke-static {v4, v5}, Lru/cn/api/ServiceLocator;->getAuthenticator(J)Lru/cn/api/authorization/AuthorizationInterceptor;

    move-result-object v0

    .line 308
    .local v0, "authenticator":Lru/cn/api/authorization/AuthorizationInterceptor;
    const/4 v4, 0x1

    new-array v4, v4, [Lokhttp3/Interceptor;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v0, v4}, Lru/cn/api/ServiceLocator;->createCallFactory(Lokhttp3/Authenticator;[Lokhttp3/Interceptor;)Lokhttp3/Call$Factory;

    move-result-object v1

    .line 311
    .local v1, "callFactory":Lokhttp3/Call$Factory;
    new-instance v4, Lretrofit2/Retrofit$Builder;

    invoke-direct {v4}, Lretrofit2/Retrofit$Builder;-><init>()V

    .line 312
    invoke-virtual {v3}, Lru/cn/api/registry/replies/Service;->getLocation()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lretrofit2/Retrofit$Builder;->baseUrl(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object v4

    sget-object v5, Lru/cn/api/ServiceLocator;->jsonConverter:Lretrofit2/Converter$Factory;

    .line 313
    invoke-virtual {v4, v5}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v4

    .line 314
    invoke-virtual {v4, v1}, Lretrofit2/Retrofit$Builder;->callFactory(Lokhttp3/Call$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v4

    .line 315
    invoke-static {}, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;->create()Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;

    move-result-object v5

    invoke-virtual {v4, v5}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v4

    .line 316
    invoke-virtual {v4}, Lretrofit2/Retrofit$Builder;->build()Lretrofit2/Retrofit;

    move-result-object v2

    .line 318
    .local v2, "retrofit":Lretrofit2/Retrofit;
    const-class v4, Lru/cn/api/userdata/retrofit/UserDataApi;

    invoke-virtual {v2, v4}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/api/userdata/retrofit/UserDataApi;

    goto :goto_0
.end method
