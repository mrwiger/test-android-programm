.class Lru/cn/guides/GuidManager$GuidGroup;
.super Ljava/lang/Object;
.source "GuidManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/guides/GuidManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "GuidGroup"
.end annotation


# instance fields
.field items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/guides/GuidManager$GuidItem;",
            ">;"
        }
    .end annotation
.end field

.field version:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/guides/GuidManager$GuidGroup;->items:Ljava/util/List;

    return-void
.end method


# virtual methods
.method addItem(III)V
    .locals 2
    .param p1, "title"    # I
    .param p2, "description"    # I
    .param p3, "image"    # I

    .prologue
    .line 41
    new-instance v0, Lru/cn/guides/GuidManager$GuidItem;

    invoke-direct {v0}, Lru/cn/guides/GuidManager$GuidItem;-><init>()V

    .line 42
    .local v0, "item":Lru/cn/guides/GuidManager$GuidItem;
    iput p1, v0, Lru/cn/guides/GuidManager$GuidItem;->title:I

    .line 43
    iput p2, v0, Lru/cn/guides/GuidManager$GuidItem;->description:I

    .line 44
    iput p3, v0, Lru/cn/guides/GuidManager$GuidItem;->image:I

    .line 45
    iget-object v1, p0, Lru/cn/guides/GuidManager$GuidGroup;->items:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    return-void
.end method
