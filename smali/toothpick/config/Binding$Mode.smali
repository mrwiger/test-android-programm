.class public final enum Ltoothpick/config/Binding$Mode;
.super Ljava/lang/Enum;
.source "Binding.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltoothpick/config/Binding;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Mode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ltoothpick/config/Binding$Mode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Ltoothpick/config/Binding$Mode;

.field public static final enum CLASS:Ltoothpick/config/Binding$Mode;

.field public static final enum INSTANCE:Ltoothpick/config/Binding$Mode;

.field public static final enum PROVIDER_CLASS:Ltoothpick/config/Binding$Mode;

.field public static final enum PROVIDER_INSTANCE:Ltoothpick/config/Binding$Mode;

.field public static final enum SIMPLE:Ltoothpick/config/Binding$Mode;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 112
    new-instance v0, Ltoothpick/config/Binding$Mode;

    const-string v1, "SIMPLE"

    invoke-direct {v0, v1, v2}, Ltoothpick/config/Binding$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltoothpick/config/Binding$Mode;->SIMPLE:Ltoothpick/config/Binding$Mode;

    .line 113
    new-instance v0, Ltoothpick/config/Binding$Mode;

    const-string v1, "CLASS"

    invoke-direct {v0, v1, v3}, Ltoothpick/config/Binding$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltoothpick/config/Binding$Mode;->CLASS:Ltoothpick/config/Binding$Mode;

    .line 114
    new-instance v0, Ltoothpick/config/Binding$Mode;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v4}, Ltoothpick/config/Binding$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltoothpick/config/Binding$Mode;->INSTANCE:Ltoothpick/config/Binding$Mode;

    .line 115
    new-instance v0, Ltoothpick/config/Binding$Mode;

    const-string v1, "PROVIDER_INSTANCE"

    invoke-direct {v0, v1, v5}, Ltoothpick/config/Binding$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltoothpick/config/Binding$Mode;->PROVIDER_INSTANCE:Ltoothpick/config/Binding$Mode;

    .line 116
    new-instance v0, Ltoothpick/config/Binding$Mode;

    const-string v1, "PROVIDER_CLASS"

    invoke-direct {v0, v1, v6}, Ltoothpick/config/Binding$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltoothpick/config/Binding$Mode;->PROVIDER_CLASS:Ltoothpick/config/Binding$Mode;

    .line 111
    const/4 v0, 0x5

    new-array v0, v0, [Ltoothpick/config/Binding$Mode;

    sget-object v1, Ltoothpick/config/Binding$Mode;->SIMPLE:Ltoothpick/config/Binding$Mode;

    aput-object v1, v0, v2

    sget-object v1, Ltoothpick/config/Binding$Mode;->CLASS:Ltoothpick/config/Binding$Mode;

    aput-object v1, v0, v3

    sget-object v1, Ltoothpick/config/Binding$Mode;->INSTANCE:Ltoothpick/config/Binding$Mode;

    aput-object v1, v0, v4

    sget-object v1, Ltoothpick/config/Binding$Mode;->PROVIDER_INSTANCE:Ltoothpick/config/Binding$Mode;

    aput-object v1, v0, v5

    sget-object v1, Ltoothpick/config/Binding$Mode;->PROVIDER_CLASS:Ltoothpick/config/Binding$Mode;

    aput-object v1, v0, v6

    sput-object v0, Ltoothpick/config/Binding$Mode;->$VALUES:[Ltoothpick/config/Binding$Mode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 111
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ltoothpick/config/Binding$Mode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 111
    const-class v0, Ltoothpick/config/Binding$Mode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ltoothpick/config/Binding$Mode;

    return-object v0
.end method

.method public static values()[Ltoothpick/config/Binding$Mode;
    .locals 1

    .prologue
    .line 111
    sget-object v0, Ltoothpick/config/Binding$Mode;->$VALUES:[Ltoothpick/config/Binding$Mode;

    invoke-virtual {v0}, [Ltoothpick/config/Binding$Mode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ltoothpick/config/Binding$Mode;

    return-object v0
.end method
