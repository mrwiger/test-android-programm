.class Lru/cn/ad/preloader/AdPreLoader$2;
.super Ljava/lang/Object;
.source "AdPreLoader.java"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/ad/preloader/AdPreLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private activeCount:I

.field final synthetic this$0:Lru/cn/ad/preloader/AdPreLoader;


# direct methods
.method constructor <init>(Lru/cn/ad/preloader/AdPreLoader;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/ad/preloader/AdPreLoader;

    .prologue
    .line 214
    iput-object p1, p0, Lru/cn/ad/preloader/AdPreLoader$2;->this$0:Lru/cn/ad/preloader/AdPreLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 220
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 260
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x0

    .line 239
    iget v2, p0, Lru/cn/ad/preloader/AdPreLoader$2;->activeCount:I

    if-lez v2, :cond_1

    const/4 v0, 0x1

    .line 240
    .local v0, "wasActive":Z
    :goto_0
    iget v2, p0, Lru/cn/ad/preloader/AdPreLoader$2;->activeCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lru/cn/ad/preloader/AdPreLoader$2;->activeCount:I

    .line 242
    iget v2, p0, Lru/cn/ad/preloader/AdPreLoader$2;->activeCount:I

    if-gtz v2, :cond_0

    if-eqz v0, :cond_0

    .line 243
    iget-object v2, p0, Lru/cn/ad/preloader/AdPreLoader$2;->this$0:Lru/cn/ad/preloader/AdPreLoader;

    invoke-static {v2, v1}, Lru/cn/ad/preloader/AdPreLoader;->access$400(Lru/cn/ad/preloader/AdPreLoader;Z)V

    .line 245
    :cond_0
    return-void

    .end local v0    # "wasActive":Z
    :cond_1
    move v0, v1

    .line 239
    goto :goto_0
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x1

    .line 229
    iget v2, p0, Lru/cn/ad/preloader/AdPreLoader$2;->activeCount:I

    if-lez v2, :cond_1

    move v0, v1

    .line 230
    .local v0, "wasActive":Z
    :goto_0
    iget v2, p0, Lru/cn/ad/preloader/AdPreLoader$2;->activeCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lru/cn/ad/preloader/AdPreLoader$2;->activeCount:I

    .line 232
    iget v2, p0, Lru/cn/ad/preloader/AdPreLoader$2;->activeCount:I

    if-lez v2, :cond_0

    if-nez v0, :cond_0

    .line 233
    iget-object v2, p0, Lru/cn/ad/preloader/AdPreLoader$2;->this$0:Lru/cn/ad/preloader/AdPreLoader;

    invoke-static {v2, v1}, Lru/cn/ad/preloader/AdPreLoader;->access$400(Lru/cn/ad/preloader/AdPreLoader;Z)V

    .line 235
    :cond_0
    return-void

    .line 229
    .end local v0    # "wasActive":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 255
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 225
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 250
    return-void
.end method
