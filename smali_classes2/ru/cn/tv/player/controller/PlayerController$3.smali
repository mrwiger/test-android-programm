.class Lru/cn/tv/player/controller/PlayerController$3;
.super Ljava/lang/Object;
.source "PlayerController.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/player/controller/PlayerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/player/controller/PlayerController;


# direct methods
.method constructor <init>(Lru/cn/tv/player/controller/PlayerController;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/player/controller/PlayerController;

    .prologue
    .line 848
    iput-object p1, p0, Lru/cn/tv/player/controller/PlayerController$3;->this$0:Lru/cn/tv/player/controller/PlayerController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 5
    .param p1, "bar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromuser"    # Z

    .prologue
    const/4 v4, 0x2

    .line 865
    if-nez p3, :cond_1

    .line 892
    :cond_0
    :goto_0
    return-void

    .line 871
    :cond_1
    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController$3;->this$0:Lru/cn/tv/player/controller/PlayerController;

    iget-object v1, v1, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    if-eqz v1, :cond_0

    .line 875
    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController$3;->this$0:Lru/cn/tv/player/controller/PlayerController;

    invoke-virtual {v1}, Lru/cn/tv/player/controller/PlayerController;->seekable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 879
    const/4 v0, 0x0

    .line 880
    .local v0, "delay":I
    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController$3;->this$0:Lru/cn/tv/player/controller/PlayerController;

    invoke-static {v1}, Lru/cn/tv/player/controller/PlayerController;->access$500(Lru/cn/tv/player/controller/PlayerController;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 881
    const/16 v0, 0x12c

    .line 884
    :cond_2
    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController$3;->this$0:Lru/cn/tv/player/controller/PlayerController;

    invoke-static {v1}, Lru/cn/tv/player/controller/PlayerController;->access$600(Lru/cn/tv/player/controller/PlayerController;)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 885
    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController$3;->this$0:Lru/cn/tv/player/controller/PlayerController;

    invoke-static {v1}, Lru/cn/tv/player/controller/PlayerController;->access$600(Lru/cn/tv/player/controller/PlayerController;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lru/cn/tv/player/controller/PlayerController$3;->this$0:Lru/cn/tv/player/controller/PlayerController;

    invoke-static {v2, p2}, Lru/cn/tv/player/controller/PlayerController;->access$700(Lru/cn/tv/player/controller/PlayerController;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 888
    :cond_3
    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController$3;->this$0:Lru/cn/tv/player/controller/PlayerController;

    invoke-static {v1}, Lru/cn/tv/player/controller/PlayerController;->access$400(Lru/cn/tv/player/controller/PlayerController;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 890
    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController$3;->this$0:Lru/cn/tv/player/controller/PlayerController;

    mul-int/lit16 v2, p2, 0x3e8

    invoke-static {v1, v2}, Lru/cn/tv/player/controller/PlayerController;->access$802(Lru/cn/tv/player/controller/PlayerController;I)I

    .line 891
    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController$3;->this$0:Lru/cn/tv/player/controller/PlayerController;

    invoke-static {v1}, Lru/cn/tv/player/controller/PlayerController;->access$400(Lru/cn/tv/player/controller/PlayerController;)Landroid/os/Handler;

    move-result-object v1

    int-to-long v2, v0

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "bar"    # Landroid/widget/SeekBar;

    .prologue
    const/4 v1, 0x1

    .line 853
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController$3;->this$0:Lru/cn/tv/player/controller/PlayerController;

    invoke-static {v0, v1}, Lru/cn/tv/player/controller/PlayerController;->access$302(Lru/cn/tv/player/controller/PlayerController;Z)Z

    .line 860
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController$3;->this$0:Lru/cn/tv/player/controller/PlayerController;

    invoke-static {v0}, Lru/cn/tv/player/controller/PlayerController;->access$400(Lru/cn/tv/player/controller/PlayerController;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 861
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 3
    .param p1, "bar"    # Landroid/widget/SeekBar;

    .prologue
    const/4 v2, 0x1

    .line 895
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController$3;->this$0:Lru/cn/tv/player/controller/PlayerController;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lru/cn/tv/player/controller/PlayerController;->access$302(Lru/cn/tv/player/controller/PlayerController;Z)Z

    .line 896
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController$3;->this$0:Lru/cn/tv/player/controller/PlayerController;

    invoke-static {v0}, Lru/cn/tv/player/controller/PlayerController;->access$900(Lru/cn/tv/player/controller/PlayerController;)I

    .line 897
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController$3;->this$0:Lru/cn/tv/player/controller/PlayerController;

    invoke-static {v0}, Lru/cn/tv/player/controller/PlayerController;->access$1000(Lru/cn/tv/player/controller/PlayerController;)V

    .line 903
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController$3;->this$0:Lru/cn/tv/player/controller/PlayerController;

    invoke-static {v0}, Lru/cn/tv/player/controller/PlayerController;->access$400(Lru/cn/tv/player/controller/PlayerController;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 904
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController$3;->this$0:Lru/cn/tv/player/controller/PlayerController;

    invoke-static {v0}, Lru/cn/tv/player/controller/PlayerController;->access$400(Lru/cn/tv/player/controller/PlayerController;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 906
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController$3;->this$0:Lru/cn/tv/player/controller/PlayerController;

    invoke-static {v0}, Lru/cn/tv/player/controller/PlayerController;->access$400(Lru/cn/tv/player/controller/PlayerController;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 907
    return-void
.end method
