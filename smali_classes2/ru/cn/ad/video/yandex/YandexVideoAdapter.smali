.class public Lru/cn/ad/video/yandex/YandexVideoAdapter;
.super Lru/cn/ad/video/VideoAdAdapter;
.source "YandexVideoAdapter.java"

# interfaces
.implements Lru/cn/ad/video/ui/VastPresenter$PresenterListener;
.implements Lru/cn/ad/video/ui/VastPresenter$Tracker;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/ad/video/yandex/YandexVideoAdapter$YMAMediaComparator;,
        Lru/cn/ad/video/yandex/YandexVideoAdapter$YMAMediaPredicate;
    }
.end annotation


# instance fields
.field private final LOG_TAG:Ljava/lang/String;

.field private adVideo:Lcom/yandex/mobile/ads/video/models/ad/VideoAd;

.field private clickUri:Ljava/lang/String;

.field private creative:Lcom/yandex/mobile/ads/video/models/ad/Creative;

.field private mBlocksInfoRequestListener:Lcom/yandex/mobile/ads/video/RequestListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/yandex/mobile/ads/video/RequestListener",
            "<",
            "Lcom/yandex/mobile/ads/video/models/blocksinfo/BlocksInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mTracker:Lcom/yandex/mobile/ads/video/tracking/Tracker;

.field private mVideoAdRequestListener:Lcom/yandex/mobile/ads/video/RequestListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/yandex/mobile/ads/video/RequestListener",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/yandex/mobile/ads/video/models/ad/VideoAd;",
            ">;>;"
        }
    .end annotation
.end field

.field private final pageRef:Ljava/lang/String;

.field private final partnerId:Ljava/lang/String;

.field private presenter:Lru/cn/ad/video/ui/VastPresenter;

.field private final targetRef:Ljava/lang/String;

.field private videoUri:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "system"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lru/cn/ad/video/VideoAdAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    .line 41
    const-string v0, "YandexVASTWrapper"

    iput-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->LOG_TAG:Ljava/lang/String;

    .line 264
    new-instance v0, Lru/cn/ad/video/yandex/YandexVideoAdapter$1;

    invoke-direct {v0, p0}, Lru/cn/ad/video/yandex/YandexVideoAdapter$1;-><init>(Lru/cn/ad/video/yandex/YandexVideoAdapter;)V

    iput-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->mBlocksInfoRequestListener:Lcom/yandex/mobile/ads/video/RequestListener;

    .line 278
    new-instance v0, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;

    invoke-direct {v0, p0}, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;-><init>(Lru/cn/ad/video/yandex/YandexVideoAdapter;)V

    iput-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->mVideoAdRequestListener:Lcom/yandex/mobile/ads/video/RequestListener;

    .line 60
    const-string v0, "partner_id"

    invoke-virtual {p2, v0}, Lru/cn/api/money_miner/replies/AdSystem;->getParamOrThrow(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->partnerId:Ljava/lang/String;

    .line 61
    const-string v0, "page_ref"

    invoke-virtual {p2, v0}, Lru/cn/api/money_miner/replies/AdSystem;->getParamOrThrow(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->pageRef:Ljava/lang/String;

    .line 62
    const-string v0, "target_ref"

    invoke-virtual {p2, v0}, Lru/cn/api/money_miner/replies/AdSystem;->getParamOrThrow(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->targetRef:Ljava/lang/String;

    .line 63
    return-void
.end method

.method static synthetic access$000(Lru/cn/ad/video/yandex/YandexVideoAdapter;Lcom/yandex/mobile/ads/video/models/blocksinfo/BlocksInfo;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/video/yandex/YandexVideoAdapter;
    .param p1, "x1"    # Lcom/yandex/mobile/ads/video/models/blocksinfo/BlocksInfo;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->loadVideoBlock(Lcom/yandex/mobile/ads/video/models/blocksinfo/BlocksInfo;)V

    return-void
.end method

.method static synthetic access$100(Lru/cn/ad/video/yandex/YandexVideoAdapter;Lcom/yandex/mobile/ads/video/VideoAdError;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/video/yandex/YandexVideoAdapter;
    .param p1, "x1"    # Lcom/yandex/mobile/ads/video/VideoAdError;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->handleError(Lcom/yandex/mobile/ads/video/VideoAdError;)V

    return-void
.end method

.method static synthetic access$1000(Lru/cn/ad/video/yandex/YandexVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/yandex/YandexVideoAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$1102(Lru/cn/ad/video/yandex/YandexVideoAdapter;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/video/yandex/YandexVideoAdapter;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 38
    iput-object p1, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->videoUri:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1202(Lru/cn/ad/video/yandex/YandexVideoAdapter;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/video/yandex/YandexVideoAdapter;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 38
    iput-object p1, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->clickUri:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1300(Lru/cn/ad/video/yandex/YandexVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/yandex/YandexVideoAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$1400(Lru/cn/ad/video/yandex/YandexVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/yandex/YandexVideoAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/ad/video/yandex/YandexVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/yandex/YandexVideoAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/ad/video/yandex/YandexVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/yandex/YandexVideoAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$400(Lru/cn/ad/video/yandex/YandexVideoAdapter;)Lcom/yandex/mobile/ads/video/models/ad/VideoAd;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/yandex/YandexVideoAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->adVideo:Lcom/yandex/mobile/ads/video/models/ad/VideoAd;

    return-object v0
.end method

.method static synthetic access$402(Lru/cn/ad/video/yandex/YandexVideoAdapter;Lcom/yandex/mobile/ads/video/models/ad/VideoAd;)Lcom/yandex/mobile/ads/video/models/ad/VideoAd;
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/video/yandex/YandexVideoAdapter;
    .param p1, "x1"    # Lcom/yandex/mobile/ads/video/models/ad/VideoAd;

    .prologue
    .line 38
    iput-object p1, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->adVideo:Lcom/yandex/mobile/ads/video/models/ad/VideoAd;

    return-object p1
.end method

.method static synthetic access$500(Lru/cn/ad/video/yandex/YandexVideoAdapter;)Lcom/yandex/mobile/ads/video/models/ad/Creative;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/yandex/YandexVideoAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->creative:Lcom/yandex/mobile/ads/video/models/ad/Creative;

    return-object v0
.end method

.method static synthetic access$502(Lru/cn/ad/video/yandex/YandexVideoAdapter;Lcom/yandex/mobile/ads/video/models/ad/Creative;)Lcom/yandex/mobile/ads/video/models/ad/Creative;
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/video/yandex/YandexVideoAdapter;
    .param p1, "x1"    # Lcom/yandex/mobile/ads/video/models/ad/Creative;

    .prologue
    .line 38
    iput-object p1, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->creative:Lcom/yandex/mobile/ads/video/models/ad/Creative;

    return-object p1
.end method

.method static synthetic access$600(Lru/cn/ad/video/yandex/YandexVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/yandex/YandexVideoAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$700(Lru/cn/ad/video/yandex/YandexVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/yandex/YandexVideoAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$800(Lru/cn/ad/video/yandex/YandexVideoAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/yandex/YandexVideoAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$900(Lru/cn/ad/video/yandex/YandexVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/yandex/YandexVideoAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method private getStatMessage()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 343
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    .line 345
    .local v3, "extra":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v4, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->adVideo:Lcom/yandex/mobile/ads/video/models/ad/VideoAd;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->adVideo:Lcom/yandex/mobile/ads/video/models/ad/VideoAd;

    invoke-virtual {v4}, Lcom/yandex/mobile/ads/video/models/ad/VideoAd;->getAdSystem()Ljava/lang/String;

    move-result-object v0

    .line 346
    .local v0, "adSystem":Ljava/lang/String;
    :goto_0
    if-eqz v0, :cond_0

    .line 347
    const-string v4, "ad_system"

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    :cond_0
    iget-object v4, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->creative:Lcom/yandex/mobile/ads/video/models/ad/Creative;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->creative:Lcom/yandex/mobile/ads/video/models/ad/Creative;

    invoke-virtual {v4}, Lcom/yandex/mobile/ads/video/models/ad/Creative;->getId()Ljava/lang/String;

    move-result-object v2

    .line 351
    .local v2, "creativeId":Ljava/lang/String;
    :goto_1
    if-eqz v2, :cond_1

    .line 352
    const-string v4, "creative_id"

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    :cond_1
    iget-object v4, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->adVideo:Lcom/yandex/mobile/ads/video/models/ad/VideoAd;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->adVideo:Lcom/yandex/mobile/ads/video/models/ad/VideoAd;

    invoke-virtual {v4}, Lcom/yandex/mobile/ads/video/models/ad/VideoAd;->getAdTitle()Ljava/lang/String;

    move-result-object v1

    .line 356
    .local v1, "adTitle":Ljava/lang/String;
    :cond_2
    if-eqz v1, :cond_3

    .line 357
    const-string v4, "banner_title"

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    :cond_3
    return-object v3

    .end local v0    # "adSystem":Ljava/lang/String;
    .end local v1    # "adTitle":Ljava/lang/String;
    .end local v2    # "creativeId":Ljava/lang/String;
    :cond_4
    move-object v0, v1

    .line 345
    goto :goto_0

    .restart local v0    # "adSystem":Ljava/lang/String;
    :cond_5
    move-object v2, v1

    .line 350
    goto :goto_1
.end method

.method private handleError(Lcom/yandex/mobile/ads/video/VideoAdError;)V
    .locals 4
    .param p1, "videoAdError"    # Lcom/yandex/mobile/ads/video/VideoAdError;

    .prologue
    .line 251
    invoke-virtual {p1}, Lcom/yandex/mobile/ads/video/VideoAdError;->getCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 260
    :goto_0
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onError()V

    .line 262
    :cond_0
    return-void

    .line 253
    :pswitch_0
    sget-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->ADV_ERROR_LOAD_BANNER:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v1, "YandexAdError"

    invoke-virtual {p1}, Lcom/yandex/mobile/ads/video/VideoAdError;->getCode()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->reportError(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/util/Map;)V

    .line 255
    new-instance v0, Lru/cn/ad/video/AdvertisementException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "YandexAds: unable to retrieve ads "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 256
    invoke-virtual {p1}, Lcom/yandex/mobile/ads/video/VideoAdError;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lru/cn/ad/video/AdvertisementException;-><init>(Ljava/lang/String;)V

    .line 255
    invoke-static {v0}, Lru/cn/utils/Logger;->logException(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 251
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private loadVideoBlock(Lcom/yandex/mobile/ads/video/models/blocksinfo/BlocksInfo;)V
    .locals 11
    .param p1, "blocksInfo"    # Lcom/yandex/mobile/ads/video/models/blocksinfo/BlocksInfo;

    .prologue
    .line 218
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->adSystem:Lru/cn/api/money_miner/replies/AdSystem;

    const-string v1, "block_num"

    invoke-virtual {v0, v1}, Lru/cn/api/money_miner/replies/AdSystem;->getParam(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 220
    .local v8, "blockNum":Ljava/lang/String;
    const/4 v9, 0x0

    .line 221
    .local v9, "requiredBlock":Lcom/yandex/mobile/ads/video/models/blocksinfo/Block;
    invoke-virtual {p1}, Lcom/yandex/mobile/ads/video/models/blocksinfo/BlocksInfo;->getBlocks()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/yandex/mobile/ads/video/models/blocksinfo/Block;

    .line 222
    .local v7, "block":Lcom/yandex/mobile/ads/video/models/blocksinfo/Block;
    invoke-virtual {v7}, Lcom/yandex/mobile/ads/video/models/blocksinfo/Block;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 223
    move-object v9, v7

    .line 228
    .end local v7    # "block":Lcom/yandex/mobile/ads/video/models/blocksinfo/Block;
    :cond_1
    if-nez v9, :cond_3

    .line 229
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    if-eqz v0, :cond_2

    .line 230
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onError()V

    .line 248
    :cond_2
    :goto_0
    return-void

    .line 236
    :cond_3
    new-instance v0, Lcom/yandex/mobile/ads/video/VideoAdRequest$Builder;

    iget-object v1, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->context:Landroid/content/Context;

    iget-object v3, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->mVideoAdRequestListener:Lcom/yandex/mobile/ads/video/RequestListener;

    iget-object v4, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->targetRef:Ljava/lang/String;

    iget-object v5, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->pageRef:Ljava/lang/String;

    .line 241
    invoke-virtual {v9}, Lcom/yandex/mobile/ads/video/models/blocksinfo/Block;->getId()Ljava/lang/String;

    move-result-object v6

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/yandex/mobile/ads/video/VideoAdRequest$Builder;-><init>(Landroid/content/Context;Lcom/yandex/mobile/ads/video/models/blocksinfo/BlocksInfo;Lcom/yandex/mobile/ads/video/RequestListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0x500

    const/16 v2, 0x2d0

    .line 242
    invoke-virtual {v0, v1, v2}, Lcom/yandex/mobile/ads/video/VideoAdRequest$Builder;->setPlayerSize(II)Lcom/yandex/mobile/ads/video/VideoAdRequest$Builder;

    move-result-object v0

    .line 243
    invoke-virtual {v0}, Lcom/yandex/mobile/ads/video/VideoAdRequest$Builder;->build()Lcom/yandex/mobile/ads/video/VideoAdRequest;

    move-result-object v10

    .line 247
    .local v10, "videoAdRequest":Lcom/yandex/mobile/ads/video/VideoAdRequest;
    invoke-static {v10}, Lcom/yandex/mobile/ads/video/YandexVideoAds;->loadVideoAds(Lcom/yandex/mobile/ads/video/VideoAdRequest;)V

    goto :goto_0
.end method


# virtual methods
.method public adCompleted()V
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdEnded()V

    .line 215
    :cond_0
    return-void
.end method

.method public adStarted()V
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdStarted()V

    .line 210
    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 0

    .prologue
    .line 92
    invoke-virtual {p0}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->stop()V

    .line 93
    return-void
.end method

.method protected onLoad()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 67
    iput-object v1, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->adVideo:Lcom/yandex/mobile/ads/video/models/ad/VideoAd;

    .line 68
    iput-object v1, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->creative:Lcom/yandex/mobile/ads/video/models/ad/Creative;

    .line 70
    new-instance v1, Lcom/yandex/mobile/ads/video/tracking/Tracker;

    iget-object v2, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/yandex/mobile/ads/video/tracking/Tracker;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->mTracker:Lcom/yandex/mobile/ads/video/tracking/Tracker;

    .line 71
    new-instance v1, Lcom/yandex/mobile/ads/video/BlocksInfoRequest$Builder;

    iget-object v2, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->context:Landroid/content/Context;

    iget-object v3, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->partnerId:Ljava/lang/String;

    iget-object v4, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->mBlocksInfoRequestListener:Lcom/yandex/mobile/ads/video/RequestListener;

    invoke-direct {v1, v2, v3, v4}, Lcom/yandex/mobile/ads/video/BlocksInfoRequest$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/yandex/mobile/ads/video/RequestListener;)V

    .line 73
    invoke-virtual {v1}, Lcom/yandex/mobile/ads/video/BlocksInfoRequest$Builder;->build()Lcom/yandex/mobile/ads/video/BlocksInfoRequest;

    move-result-object v0

    .line 74
    .local v0, "request":Lcom/yandex/mobile/ads/video/BlocksInfoRequest;
    invoke-static {v0}, Lcom/yandex/mobile/ads/video/YandexVideoAds;->loadBlocksInfo(Lcom/yandex/mobile/ads/video/BlocksInfoRequest;)V

    .line 75
    return-void
.end method

.method public show()V
    .locals 4

    .prologue
    .line 79
    iget-object v1, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->presenter:Lru/cn/ad/video/ui/VastPresenter;

    if-nez v1, :cond_0

    .line 80
    new-instance v0, Lru/cn/ad/video/ui/VastPresenter;

    invoke-virtual {p0}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->getRenderingSettings()Lru/cn/ad/video/RenderingSettings;

    move-result-object v1

    iget-object v2, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->videoUri:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p0, p0}, Lru/cn/ad/video/ui/VastPresenter;-><init>(Lru/cn/ad/video/RenderingSettings;Ljava/lang/String;Lru/cn/ad/video/ui/VastPresenter$Tracker;Lru/cn/ad/video/ui/VastPresenter$PresenterListener;)V

    .line 81
    .local v0, "vastPresenter":Lru/cn/ad/video/ui/VastPresenter;
    new-instance v1, Lru/cn/ad/video/VAST/parser/SkipOffset;

    sget-object v2, Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;->SkipTypeTime:Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;

    const/high16 v3, 0x40a00000    # 5.0f

    invoke-direct {v1, v2, v3}, Lru/cn/ad/video/VAST/parser/SkipOffset;-><init>(Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;F)V

    invoke-virtual {v0, v1}, Lru/cn/ad/video/ui/VastPresenter;->setSkipOffset(Lru/cn/ad/video/VAST/parser/SkipOffset;)V

    .line 82
    iget-object v1, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->clickUri:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lru/cn/ad/video/ui/VastPresenter;->setAdClickUri(Ljava/lang/String;)V

    .line 84
    iput-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->presenter:Lru/cn/ad/video/ui/VastPresenter;

    .line 87
    .end local v0    # "vastPresenter":Lru/cn/ad/video/ui/VastPresenter;
    :cond_0
    iget-object v1, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->presenter:Lru/cn/ad/video/ui/VastPresenter;

    invoke-virtual {v1}, Lru/cn/ad/video/ui/VastPresenter;->play()V

    .line 88
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->presenter:Lru/cn/ad/video/ui/VastPresenter;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->presenter:Lru/cn/ad/video/ui/VastPresenter;

    invoke-virtual {v0}, Lru/cn/ad/video/ui/VastPresenter;->destroy()V

    .line 99
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->presenter:Lru/cn/ad/video/ui/VastPresenter;

    .line 101
    :cond_0
    return-void
.end method

.method public trackClick(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 174
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_CLICK:Lru/cn/domain/statistics/inetra/AdvEvent;

    invoke-direct {p0}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->getStatMessage()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/util/Map;)V

    .line 175
    return-void
.end method

.method public trackError(Landroid/content/Context;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "errorCode"    # I

    .prologue
    .line 181
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 182
    .local v0, "extra":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v5, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->videoUri:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 183
    const-string v5, "ad_url"

    iget-object v6, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->videoUri:Ljava/lang/String;

    invoke-interface {v0, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    :cond_0
    invoke-direct {p0}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->getStatMessage()Ljava/util/Map;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 187
    const/16 v5, 0x193

    if-ne p2, v5, :cond_3

    .line 188
    iget-object v5, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->creative:Lcom/yandex/mobile/ads/video/models/ad/Creative;

    invoke-virtual {v5}, Lcom/yandex/mobile/ads/video/models/ad/Creative;->getMediaFiles()Ljava/util/List;

    move-result-object v2

    .line 190
    .local v2, "mediaFiles":Ljava/util/List;, "Ljava/util/List<Lcom/yandex/mobile/ads/video/models/ad/MediaFile;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 191
    .local v4, "mimeTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/yandex/mobile/ads/video/models/ad/MediaFile;

    .line 192
    .local v1, "mediaFile":Lcom/yandex/mobile/ads/video/models/ad/MediaFile;
    invoke-virtual {v1}, Lcom/yandex/mobile/ads/video/models/ad/MediaFile;->getMimeType()Ljava/lang/String;

    move-result-object v3

    .line 193
    .local v3, "mimeType":Ljava/lang/String;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_1

    .line 194
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 198
    .end local v1    # "mediaFile":Lcom/yandex/mobile/ads/video/models/ad/MediaFile;
    .end local v3    # "mimeType":Ljava/lang/String;
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_3

    .line 199
    const-string v5, "mime"

    const-string v6, ","

    invoke-static {v6, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    .end local v2    # "mediaFiles":Ljava/util/List;, "Ljava/util/List<Lcom/yandex/mobile/ads/video/models/ad/MediaFile;>;"
    .end local v4    # "mimeTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    sget-object v5, Lru/cn/domain/statistics/inetra/ErrorCode;->ADV_ERROR_PLAY:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v6, "YandexAdError"

    invoke-virtual {p0, v5, v6, p2, v0}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->reportError(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/util/Map;)V

    .line 204
    return-void
.end method

.method public trackEvent(Landroid/content/Context;Ljava/lang/String;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "eventName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 116
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_0
    move v6, v2

    :goto_0
    packed-switch v6, :pswitch_data_0

    .line 135
    :goto_1
    const/4 v0, 0x0

    .line 136
    .local v0, "adEventId":Lru/cn/domain/statistics/inetra/AdvEvent;
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_1

    :cond_1
    move v1, v2

    :goto_2
    packed-switch v1, :pswitch_data_1

    .line 159
    :goto_3
    if-eqz v0, :cond_2

    .line 160
    invoke-direct {p0}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->getStatMessage()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/util/Map;)V

    .line 162
    :cond_2
    return-void

    .line 116
    .end local v0    # "adEventId":Lru/cn/domain/statistics/inetra/AdvEvent;
    :sswitch_0
    const-string v6, "start"

    invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v6, v1

    goto :goto_0

    :sswitch_1
    const-string v6, "firstQuartile"

    invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v6, v3

    goto :goto_0

    :sswitch_2
    const-string v6, "midpoint"

    invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v6, v4

    goto :goto_0

    :sswitch_3
    const-string v6, "thirdQuartile"

    invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v6, v5

    goto :goto_0

    :sswitch_4
    const-string v6, "complete"

    invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v6, 0x4

    goto :goto_0

    .line 118
    :pswitch_0
    iget-object v6, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->mTracker:Lcom/yandex/mobile/ads/video/tracking/Tracker;

    iget-object v7, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->creative:Lcom/yandex/mobile/ads/video/models/ad/Creative;

    const-string v8, "start"

    invoke-virtual {v6, v7, v8}, Lcom/yandex/mobile/ads/video/tracking/Tracker;->trackCreativeEvent(Lcom/yandex/mobile/ads/video/models/ad/Creative;Ljava/lang/String;)V

    goto :goto_1

    .line 121
    :pswitch_1
    iget-object v6, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->mTracker:Lcom/yandex/mobile/ads/video/tracking/Tracker;

    iget-object v7, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->creative:Lcom/yandex/mobile/ads/video/models/ad/Creative;

    const-string v8, "firstQuartile"

    invoke-virtual {v6, v7, v8}, Lcom/yandex/mobile/ads/video/tracking/Tracker;->trackCreativeEvent(Lcom/yandex/mobile/ads/video/models/ad/Creative;Ljava/lang/String;)V

    goto :goto_1

    .line 124
    :pswitch_2
    iget-object v6, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->mTracker:Lcom/yandex/mobile/ads/video/tracking/Tracker;

    iget-object v7, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->creative:Lcom/yandex/mobile/ads/video/models/ad/Creative;

    const-string v8, "midpoint"

    invoke-virtual {v6, v7, v8}, Lcom/yandex/mobile/ads/video/tracking/Tracker;->trackCreativeEvent(Lcom/yandex/mobile/ads/video/models/ad/Creative;Ljava/lang/String;)V

    goto :goto_1

    .line 127
    :pswitch_3
    iget-object v6, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->mTracker:Lcom/yandex/mobile/ads/video/tracking/Tracker;

    iget-object v7, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->creative:Lcom/yandex/mobile/ads/video/models/ad/Creative;

    const-string v8, "thirdQuartile"

    invoke-virtual {v6, v7, v8}, Lcom/yandex/mobile/ads/video/tracking/Tracker;->trackCreativeEvent(Lcom/yandex/mobile/ads/video/models/ad/Creative;Ljava/lang/String;)V

    goto :goto_1

    .line 130
    :pswitch_4
    iget-object v6, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->mTracker:Lcom/yandex/mobile/ads/video/tracking/Tracker;

    iget-object v7, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->creative:Lcom/yandex/mobile/ads/video/models/ad/Creative;

    const-string v8, "complete"

    invoke-virtual {v6, v7, v8}, Lcom/yandex/mobile/ads/video/tracking/Tracker;->trackCreativeEvent(Lcom/yandex/mobile/ads/video/models/ad/Creative;Ljava/lang/String;)V

    goto :goto_1

    .line 136
    .restart local v0    # "adEventId":Lru/cn/domain/statistics/inetra/AdvEvent;
    :sswitch_5
    const-string v3, "start"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_2

    :sswitch_6
    const-string v1, "firstQuartile"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v3

    goto/16 :goto_2

    :sswitch_7
    const-string v1, "midpoint"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v4

    goto/16 :goto_2

    :sswitch_8
    const-string v1, "thirdQuartile"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v5

    goto/16 :goto_2

    :sswitch_9
    const-string v1, "complete"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x4

    goto/16 :goto_2

    :sswitch_a
    const-string v1, "skip"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x5

    goto/16 :goto_2

    .line 138
    :pswitch_5
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_START:Lru/cn/domain/statistics/inetra/AdvEvent;

    .line 139
    goto/16 :goto_3

    .line 141
    :pswitch_6
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_FIRST_QUARTILE:Lru/cn/domain/statistics/inetra/AdvEvent;

    .line 142
    goto/16 :goto_3

    .line 145
    :pswitch_7
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_MIDPOINT:Lru/cn/domain/statistics/inetra/AdvEvent;

    .line 146
    goto/16 :goto_3

    .line 148
    :pswitch_8
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_THIRD_QUARTILE:Lru/cn/domain/statistics/inetra/AdvEvent;

    .line 149
    goto/16 :goto_3

    .line 151
    :pswitch_9
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_COMPLETE:Lru/cn/domain/statistics/inetra/AdvEvent;

    .line 152
    goto/16 :goto_3

    .line 154
    :pswitch_a
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_SKIP:Lru/cn/domain/statistics/inetra/AdvEvent;

    goto/16 :goto_3

    .line 116
    :sswitch_data_0
    .sparse-switch
        -0x61aea3b8 -> :sswitch_2
        -0x4fbdabf6 -> :sswitch_3
        -0x23bacec7 -> :sswitch_4
        0x68ac462 -> :sswitch_0
        0x21644853 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 136
    :sswitch_data_1
    .sparse-switch
        -0x61aea3b8 -> :sswitch_7
        -0x4fbdabf6 -> :sswitch_8
        -0x23bacec7 -> :sswitch_9
        0x35e57f -> :sswitch_a
        0x68ac462 -> :sswitch_5
        0x21644853 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public trackImpression(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 110
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->mTracker:Lcom/yandex/mobile/ads/video/tracking/Tracker;

    iget-object v1, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter;->adVideo:Lcom/yandex/mobile/ads/video/models/ad/VideoAd;

    const-string v2, "impression"

    invoke-virtual {v0, v1, v2}, Lcom/yandex/mobile/ads/video/tracking/Tracker;->trackAdEvent(Lcom/yandex/mobile/ads/video/models/ad/VideoAd;Ljava/lang/String;)V

    .line 111
    return-void
.end method
