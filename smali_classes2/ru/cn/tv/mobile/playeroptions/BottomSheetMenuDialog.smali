.class public Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;
.super Landroid/support/design/widget/BottomSheetDialogFragment;
.source "BottomSheetMenuDialog.java"

# interfaces
.implements Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;,
        Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OnItemSelectedListener;
    }
.end annotation


# instance fields
.field private menuItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/tv/mobile/playeroptions/BottomSheetItem;",
            ">;"
        }
    .end annotation
.end field

.field private onItemSelectedListener:Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OnItemSelectedListener;

.field private selectedIndex:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/support/design/widget/BottomSheetDialogFragment;-><init>()V

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->selectedIndex:I

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;

    .prologue
    .line 25
    invoke-direct {p0}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->expandDialog()V

    return-void
.end method

.method private expandDialog()V
    .locals 5

    .prologue
    .line 152
    invoke-virtual {p0}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    .line 153
    .local v2, "dialog":Landroid/app/Dialog;
    if-eqz v2, :cond_0

    .line 154
    const v4, 0x7f09009c

    invoke-virtual {v2, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 156
    .local v1, "bottomSheet":Landroid/widget/FrameLayout;
    invoke-virtual {p0}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->getView()Landroid/view/View;

    move-result-object v3

    .line 157
    .local v3, "view":Landroid/view/View;
    if-eqz v1, :cond_0

    if-eqz v3, :cond_0

    .line 158
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setMinimumHeight(I)V

    .line 160
    invoke-static {v1}, Landroid/support/design/widget/BottomSheetBehavior;->from(Landroid/view/View;)Landroid/support/design/widget/BottomSheetBehavior;

    move-result-object v0

    .line 161
    .local v0, "behavior":Landroid/support/design/widget/BottomSheetBehavior;
    const/4 v4, 0x3

    invoke-virtual {v0, v4}, Landroid/support/design/widget/BottomSheetBehavior;->setState(I)V

    .line 162
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/support/design/widget/BottomSheetBehavior;->setPeekHeight(I)V

    .line 163
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/support/design/widget/BottomSheetBehavior;->setSkipCollapsed(Z)V

    .line 164
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/support/design/widget/BottomSheetBehavior;->setHideable(Z)V

    .line 167
    .end local v0    # "behavior":Landroid/support/design/widget/BottomSheetBehavior;
    .end local v1    # "bottomSheet":Landroid/widget/FrameLayout;
    .end local v3    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method static final synthetic lambda$newInstance$0$BottomSheetMenuDialog(Landroid/content/Context;Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;)Lru/cn/tv/mobile/playeroptions/BottomSheetItem;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    .prologue
    .line 52
    sget-object v4, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$2;->$SwitchMap$ru$cn$tv$mobile$playeroptions$BottomSheetMenuDialog$OptionType:[I

    invoke-virtual {p1}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 70
    const/4 v4, 0x0

    :goto_0
    return-object v4

    .line 54
    :pswitch_0
    const v4, 0x7f0e010c

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 55
    .local v2, "quality":Ljava/lang/String;
    new-instance v4, Lru/cn/tv/mobile/playeroptions/BottomSheetItem;

    const v5, 0x7f0802fe

    invoke-direct {v4, v5, v2}, Lru/cn/tv/mobile/playeroptions/BottomSheetItem;-><init>(ILjava/lang/String;)V

    goto :goto_0

    .line 58
    .end local v2    # "quality":Ljava/lang/String;
    :pswitch_1
    const v4, 0x7f0e0109

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 59
    .local v0, "audio":Ljava/lang/String;
    new-instance v4, Lru/cn/tv/mobile/playeroptions/BottomSheetItem;

    const v5, 0x7f0802e7

    invoke-direct {v4, v5, v0}, Lru/cn/tv/mobile/playeroptions/BottomSheetItem;-><init>(ILjava/lang/String;)V

    goto :goto_0

    .line 62
    .end local v0    # "audio":Ljava/lang/String;
    :pswitch_2
    const v4, 0x7f0e010e

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 63
    .local v3, "subtitle":Ljava/lang/String;
    new-instance v4, Lru/cn/tv/mobile/playeroptions/BottomSheetItem;

    const v5, 0x7f0802ff

    invoke-direct {v4, v5, v3}, Lru/cn/tv/mobile/playeroptions/BottomSheetItem;-><init>(ILjava/lang/String;)V

    goto :goto_0

    .line 66
    .end local v3    # "subtitle":Ljava/lang/String;
    :pswitch_3
    const v4, 0x7f0e010a

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 67
    .local v1, "complain":Ljava/lang/String;
    new-instance v4, Lru/cn/tv/mobile/playeroptions/BottomSheetItem;

    const v5, 0x7f0802f1

    invoke-direct {v4, v5, v1}, Lru/cn/tv/mobile/playeroptions/BottomSheetItem;-><init>(ILjava/lang/String;)V

    goto :goto_0

    .line 52
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static final synthetic lambda$newInstance$1$BottomSheetMenuDialog(Ljava/lang/String;)Lru/cn/tv/mobile/playeroptions/BottomSheetItem;
    .locals 2
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 80
    new-instance v0, Lru/cn/tv/mobile/playeroptions/BottomSheetItem;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lru/cn/tv/mobile/playeroptions/BottomSheetItem;-><init>(ILjava/lang/String;)V

    return-object v0
.end method

.method public static newInstance(ILjava/util/List;)Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;
    .locals 4
    .param p0, "selectedIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;"
        }
    .end annotation

    .prologue
    .line 79
    .local p1, "values":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v2

    sget-object v3, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$$Lambda$1;->$instance:Lcom/annimon/stream/function/Function;

    .line 80
    invoke-virtual {v2, v3}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v2

    .line 81
    invoke-virtual {v2}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object v1

    .line 83
    .local v1, "menuItems":Ljava/util/List;, "Ljava/util/List<Lru/cn/tv/mobile/playeroptions/BottomSheetItem;>;"
    invoke-static {v1}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->newInstance(Ljava/util/List;)Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;

    move-result-object v0

    .line 84
    .local v0, "fragment":Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;
    iput p0, v0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->selectedIndex:I

    .line 85
    return-object v0
.end method

.method public static varargs newInstance(Landroid/content/Context;[Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;)Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "types"    # [Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    .prologue
    .line 50
    invoke-static {p1}, Lcom/annimon/stream/Stream;->of([Ljava/lang/Object;)Lcom/annimon/stream/Stream;

    move-result-object v1

    new-instance v2, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$$Lambda$0;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$$Lambda$0;-><init>(Landroid/content/Context;)V

    .line 51
    invoke-virtual {v1, v2}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v1

    .line 73
    invoke-virtual {v1}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object v0

    .line 75
    .local v0, "menuItems":Ljava/util/List;, "Ljava/util/List<Lru/cn/tv/mobile/playeroptions/BottomSheetItem;>;"
    invoke-static {v0}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->newInstance(Ljava/util/List;)Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;

    move-result-object v1

    return-object v1
.end method

.method private static newInstance(Ljava/util/List;)Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/tv/mobile/playeroptions/BottomSheetItem;",
            ">;)",
            "Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;"
        }
    .end annotation

    .prologue
    .line 89
    .local p0, "items":Ljava/util/List;, "Ljava/util/List<Lru/cn/tv/mobile/playeroptions/BottomSheetItem;>;"
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 91
    .local v0, "args":Landroid/os/Bundle;
    new-instance v1, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;

    invoke-direct {v1}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;-><init>()V

    .line 92
    .local v1, "fragment":Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;
    invoke-virtual {v1, v0}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->setArguments(Landroid/os/Bundle;)V

    .line 95
    iput-object p0, v1, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->menuItems:Ljava/util/List;

    .line 97
    return-object v1
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v1, -0x1

    .line 115
    invoke-super {p0, p1}, Landroid/support/design/widget/BottomSheetDialogFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 116
    invoke-virtual {p0}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 117
    invoke-virtual {p0}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setLayout(II)V

    .line 119
    invoke-direct {p0}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->expandDialog()V

    .line 121
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 103
    invoke-super {p0, p1}, Landroid/support/design/widget/BottomSheetDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 104
    .local v0, "dialog":Landroid/app/Dialog;
    new-instance v1, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$1;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$1;-><init>(Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 110
    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 126
    const v5, 0x7f0c0095

    const/4 v6, 0x0

    invoke-virtual {p1, v5, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 128
    .local v4, "view":Landroid/view/View;
    invoke-virtual {p0}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 129
    .local v1, "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 133
    :cond_0
    new-instance v0, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;

    iget-object v5, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->menuItems:Ljava/util/List;

    invoke-direct {v0, v5, p0}, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;-><init>(Ljava/util/List;Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$OnItemClickListener;)V

    .line 134
    .local v0, "adapter":Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;
    iget v5, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->selectedIndex:I

    invoke-virtual {v0, v5}, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;->setSelection(I)V

    .line 135
    const v5, 0x7f090044

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/RecyclerView;

    .line 137
    .local v3, "recyclerView":Landroid/support/v7/widget/RecyclerView;
    new-instance v2, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v2, v5}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 138
    .local v2, "layoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    invoke-virtual {v3, v2}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 139
    invoke-virtual {v3, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 141
    return-object v4
.end method

.method public onItemClick(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 146
    iget-object v0, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->onItemSelectedListener:Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OnItemSelectedListener;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->onItemSelectedListener:Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OnItemSelectedListener;

    invoke-interface {v0, p1}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OnItemSelectedListener;->onItemSelectedClicked(I)V

    .line 149
    :cond_0
    return-void
.end method

.method public setOnItemSelectedListener(Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OnItemSelectedListener;)V
    .locals 0
    .param p1, "onItemSelectedListener"    # Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OnItemSelectedListener;

    .prologue
    .line 46
    iput-object p1, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->onItemSelectedListener:Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OnItemSelectedListener;

    .line 47
    return-void
.end method
