.class Lcom/yandex/metrica/impl/ob/br;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lcom/yandex/metrica/impl/ob/dg;

.field b:Lcom/yandex/metrica/impl/ob/df;

.field private c:Ljava/lang/String;

.field private d:Lcom/yandex/metrica/impl/ob/bq;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/yandex/metrica/impl/ob/dg;

    .line 38
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/cq;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/cq;

    move-result-object v2

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/cq;->d()Lcom/yandex/metrica/impl/ob/cr;

    move-result-object v2

    .line 39
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/yandex/metrica/impl/ob/dg;-><init>(Lcom/yandex/metrica/impl/ob/cr;Ljava/lang/String;)V

    new-instance v2, Lcom/yandex/metrica/impl/ob/df;

    .line 40
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/cq;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/cq;

    move-result-object v3

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/ob/cq;->b()Lcom/yandex/metrica/impl/ob/cr;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/yandex/metrica/impl/ob/df;-><init>(Lcom/yandex/metrica/impl/ob/cr;)V

    new-instance v3, Lcom/yandex/metrica/impl/ob/bq;

    invoke-direct {v3}, Lcom/yandex/metrica/impl/ob/bq;-><init>()V

    .line 37
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/yandex/metrica/impl/ob/br;-><init>(Ljava/lang/String;Lcom/yandex/metrica/impl/ob/dg;Lcom/yandex/metrica/impl/ob/df;Lcom/yandex/metrica/impl/ob/bq;)V

    .line 42
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/yandex/metrica/impl/ob/dg;Lcom/yandex/metrica/impl/ob/df;Lcom/yandex/metrica/impl/ob/bq;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/br;->c:Ljava/lang/String;

    .line 64
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/br;->a:Lcom/yandex/metrica/impl/ob/dg;

    .line 65
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/br;->b:Lcom/yandex/metrica/impl/ob/df;

    .line 66
    iput-object p4, p0, Lcom/yandex/metrica/impl/ob/br;->d:Lcom/yandex/metrica/impl/ob/bq;

    .line 67
    return-void
.end method


# virtual methods
.method public a()Landroid/os/Bundle;
    .locals 6

    .prologue
    .line 46
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 47
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/br;->a:Lcom/yandex/metrica/impl/ob/dg;

    .line 48
    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/dq;->a(Lcom/yandex/metrica/impl/ob/dg;)Lcom/yandex/metrica/impl/ob/dq;

    move-result-object v5

    .line 49
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/br;->a:Lcom/yandex/metrica/impl/ob/dg;

    .line 50
    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/dv;->b(Lcom/yandex/metrica/impl/ob/dg;)Lcom/yandex/metrica/impl/ob/dv;

    move-result-object v4

    .line 51
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/br;->d:Lcom/yandex/metrica/impl/ob/bq;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/br;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/br;->b:Lcom/yandex/metrica/impl/ob/df;

    .line 52
    invoke-virtual {v3}, Lcom/yandex/metrica/impl/ob/df;->c()Z

    move-result v3

    .line 51
    invoke-virtual/range {v0 .. v5}, Lcom/yandex/metrica/impl/ob/bq;->a(Landroid/os/Bundle;Ljava/lang/String;ZLcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/dq;)V

    .line 55
    return-object v1
.end method
