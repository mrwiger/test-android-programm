.class Lcom/yandex/metrica/impl/ob/u$e;
.super Lcom/yandex/metrica/impl/ob/u$f;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/impl/ob/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "e"
.end annotation


# static fields
.field static final a:Lcom/yandex/metrica/impl/ob/fm;

.field static final b:Lcom/yandex/metrica/impl/ob/fm;

.field static final c:Lcom/yandex/metrica/impl/ob/fm;

.field static final d:Lcom/yandex/metrica/impl/ob/fm;

.field static final e:Lcom/yandex/metrica/impl/ob/fm;

.field static final f:Lcom/yandex/metrica/impl/ob/fm;

.field static final g:Lcom/yandex/metrica/impl/ob/fm;

.field static final h:Lcom/yandex/metrica/impl/ob/fm;

.field static final i:Lcom/yandex/metrica/impl/ob/fm;

.field static final j:Lcom/yandex/metrica/impl/ob/fm;


# instance fields
.field private final k:Lcom/yandex/metrica/impl/ob/dd;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 66
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "SESSION_SLEEP_START"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/u$e;->a:Lcom/yandex/metrica/impl/ob/fm;

    .line 68
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "SESSION_ID"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/u$e;->b:Lcom/yandex/metrica/impl/ob/fm;

    .line 70
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "SESSION_COUNTER_ID"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/u$e;->c:Lcom/yandex/metrica/impl/ob/fm;

    .line 72
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "SESSION_INIT_TIME"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/u$e;->d:Lcom/yandex/metrica/impl/ob/fm;

    .line 74
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "SESSION_IS_ALIVE_REPORT_NEEDED"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/u$e;->e:Lcom/yandex/metrica/impl/ob/fm;

    .line 79
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "BG_SESSION_ID"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/u$e;->f:Lcom/yandex/metrica/impl/ob/fm;

    .line 81
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "BG_SESSION_SLEEP_START"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/u$e;->g:Lcom/yandex/metrica/impl/ob/fm;

    .line 83
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "BG_SESSION_COUNTER_ID"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/u$e;->h:Lcom/yandex/metrica/impl/ob/fm;

    .line 85
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "BG_SESSION_INIT_TIME"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/u$e;->i:Lcom/yandex/metrica/impl/ob/fm;

    .line 87
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "BG_SESSION_IS_ALIVE_REPORT_NEEDED"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/u$e;->j:Lcom/yandex/metrica/impl/ob/fm;

    return-void
.end method

.method constructor <init>(Lcom/yandex/metrica/impl/ob/v;)V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/u$f;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    .line 94
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->G()Lcom/yandex/metrica/impl/ob/dd;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/u$e;->k:Lcom/yandex/metrica/impl/ob/dd;

    .line 95
    return-void
.end method


# virtual methods
.method protected a()Z
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x1

    return v0
.end method

.method protected b()V
    .locals 2

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/u$e;->d()V

    .line 104
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/u$e;->c()V

    .line 1109
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$e;->k:Lcom/yandex/metrica/impl/ob/dd;

    sget-object v1, Lcom/yandex/metrica/impl/ob/u$e;->a:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dd;->r(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    .line 1110
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$e;->k:Lcom/yandex/metrica/impl/ob/dd;

    sget-object v1, Lcom/yandex/metrica/impl/ob/u$e;->b:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dd;->r(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    .line 1111
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$e;->k:Lcom/yandex/metrica/impl/ob/dd;

    sget-object v1, Lcom/yandex/metrica/impl/ob/u$e;->c:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dd;->r(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    .line 1112
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$e;->k:Lcom/yandex/metrica/impl/ob/dd;

    sget-object v1, Lcom/yandex/metrica/impl/ob/u$e;->d:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dd;->r(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    .line 1113
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$e;->k:Lcom/yandex/metrica/impl/ob/dd;

    sget-object v1, Lcom/yandex/metrica/impl/ob/u$e;->e:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dd;->r(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    .line 1115
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$e;->k:Lcom/yandex/metrica/impl/ob/dd;

    sget-object v1, Lcom/yandex/metrica/impl/ob/u$e;->f:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dd;->r(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    .line 1116
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$e;->k:Lcom/yandex/metrica/impl/ob/dd;

    sget-object v1, Lcom/yandex/metrica/impl/ob/u$e;->g:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dd;->r(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    .line 1117
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$e;->k:Lcom/yandex/metrica/impl/ob/dd;

    sget-object v1, Lcom/yandex/metrica/impl/ob/u$e;->h:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dd;->r(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    .line 1118
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$e;->k:Lcom/yandex/metrica/impl/ob/dd;

    sget-object v1, Lcom/yandex/metrica/impl/ob/u$e;->i:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dd;->r(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    .line 1119
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$e;->k:Lcom/yandex/metrica/impl/ob/dd;

    sget-object v1, Lcom/yandex/metrica/impl/ob/u$e;->j:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dd;->r(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    .line 106
    return-void
.end method

.method c()V
    .locals 12

    .prologue
    const-wide/16 v10, -0x1

    const-wide/32 v2, -0x80000000

    const-wide/high16 v8, -0x8000000000000000L

    const-wide/16 v6, 0x0

    .line 124
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$e;->k:Lcom/yandex/metrica/impl/ob/dd;

    sget-object v1, Lcom/yandex/metrica/impl/ob/u$e;->a:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v3}, Lcom/yandex/metrica/impl/ob/dd;->b(Ljava/lang/String;J)J

    move-result-wide v0

    .line 125
    cmp-long v2, v0, v2

    if-eqz v2, :cond_5

    .line 126
    new-instance v2, Lcom/yandex/metrica/impl/ob/bo;

    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/u$e;->k:Lcom/yandex/metrica/impl/ob/dd;

    const-string v4, "foreground"

    invoke-direct {v2, v3, v4}, Lcom/yandex/metrica/impl/ob/bo;-><init>(Lcom/yandex/metrica/impl/ob/dd;Ljava/lang/String;)V

    .line 127
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/bo;->h()Z

    move-result v3

    if-nez v3, :cond_5

    .line 128
    cmp-long v3, v0, v6

    if-eqz v3, :cond_0

    .line 129
    invoke-virtual {v2, v0, v1}, Lcom/yandex/metrica/impl/ob/bo;->b(J)Lcom/yandex/metrica/impl/ob/bo;

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$e;->k:Lcom/yandex/metrica/impl/ob/dd;

    sget-object v1, Lcom/yandex/metrica/impl/ob/u$e;->b:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v10, v11}, Lcom/yandex/metrica/impl/ob/dd;->b(Ljava/lang/String;J)J

    move-result-wide v0

    .line 132
    cmp-long v3, v10, v0

    if-eqz v3, :cond_1

    .line 133
    invoke-virtual {v2, v0, v1}, Lcom/yandex/metrica/impl/ob/bo;->c(J)Lcom/yandex/metrica/impl/ob/bo;

    .line 135
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$e;->k:Lcom/yandex/metrica/impl/ob/dd;

    sget-object v1, Lcom/yandex/metrica/impl/ob/u$e;->e:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3}, Lcom/yandex/metrica/impl/ob/dd;->b(Ljava/lang/String;Z)Z

    move-result v0

    .line 136
    if-eqz v0, :cond_2

    .line 137
    invoke-virtual {v2, v0}, Lcom/yandex/metrica/impl/ob/bo;->a(Z)Lcom/yandex/metrica/impl/ob/bo;

    .line 139
    :cond_2
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$e;->k:Lcom/yandex/metrica/impl/ob/dd;

    sget-object v1, Lcom/yandex/metrica/impl/ob/u$e;->d:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v8, v9}, Lcom/yandex/metrica/impl/ob/dd;->b(Ljava/lang/String;J)J

    move-result-wide v0

    .line 140
    cmp-long v3, v0, v8

    if-eqz v3, :cond_3

    .line 141
    invoke-virtual {v2, v0, v1}, Lcom/yandex/metrica/impl/ob/bo;->d(J)Lcom/yandex/metrica/impl/ob/bo;

    .line 143
    :cond_3
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$e;->k:Lcom/yandex/metrica/impl/ob/dd;

    sget-object v1, Lcom/yandex/metrica/impl/ob/u$e;->c:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v6, v7}, Lcom/yandex/metrica/impl/ob/dd;->b(Ljava/lang/String;J)J

    move-result-wide v0

    .line 144
    cmp-long v3, v0, v6

    if-eqz v3, :cond_4

    .line 145
    invoke-virtual {v2, v0, v1}, Lcom/yandex/metrica/impl/ob/bo;->a(J)Lcom/yandex/metrica/impl/ob/bo;

    .line 147
    :cond_4
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/bo;->g()V

    .line 150
    :cond_5
    return-void
.end method

.method d()V
    .locals 12

    .prologue
    const-wide/16 v10, -0x1

    const-wide/32 v2, -0x80000000

    const-wide/high16 v8, -0x8000000000000000L

    const-wide/16 v6, 0x0

    .line 154
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$e;->k:Lcom/yandex/metrica/impl/ob/dd;

    sget-object v1, Lcom/yandex/metrica/impl/ob/u$e;->g:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v3}, Lcom/yandex/metrica/impl/ob/dd;->b(Ljava/lang/String;J)J

    move-result-wide v0

    .line 155
    cmp-long v2, v0, v2

    if-eqz v2, :cond_5

    .line 156
    new-instance v2, Lcom/yandex/metrica/impl/ob/bo;

    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/u$e;->k:Lcom/yandex/metrica/impl/ob/dd;

    const-string v4, "background"

    invoke-direct {v2, v3, v4}, Lcom/yandex/metrica/impl/ob/bo;-><init>(Lcom/yandex/metrica/impl/ob/dd;Ljava/lang/String;)V

    .line 157
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/bo;->h()Z

    move-result v3

    if-nez v3, :cond_5

    .line 158
    cmp-long v3, v0, v6

    if-eqz v3, :cond_0

    .line 159
    invoke-virtual {v2, v0, v1}, Lcom/yandex/metrica/impl/ob/bo;->b(J)Lcom/yandex/metrica/impl/ob/bo;

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$e;->k:Lcom/yandex/metrica/impl/ob/dd;

    sget-object v1, Lcom/yandex/metrica/impl/ob/u$e;->f:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v10, v11}, Lcom/yandex/metrica/impl/ob/dd;->b(Ljava/lang/String;J)J

    move-result-wide v0

    .line 162
    cmp-long v3, v0, v10

    if-eqz v3, :cond_1

    .line 163
    invoke-virtual {v2, v0, v1}, Lcom/yandex/metrica/impl/ob/bo;->c(J)Lcom/yandex/metrica/impl/ob/bo;

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$e;->k:Lcom/yandex/metrica/impl/ob/dd;

    sget-object v1, Lcom/yandex/metrica/impl/ob/u$e;->j:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3}, Lcom/yandex/metrica/impl/ob/dd;->b(Ljava/lang/String;Z)Z

    move-result v0

    .line 166
    if-eqz v0, :cond_2

    .line 167
    invoke-virtual {v2, v0}, Lcom/yandex/metrica/impl/ob/bo;->a(Z)Lcom/yandex/metrica/impl/ob/bo;

    .line 169
    :cond_2
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$e;->k:Lcom/yandex/metrica/impl/ob/dd;

    sget-object v1, Lcom/yandex/metrica/impl/ob/u$e;->i:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v8, v9}, Lcom/yandex/metrica/impl/ob/dd;->b(Ljava/lang/String;J)J

    move-result-wide v0

    .line 170
    cmp-long v3, v0, v8

    if-eqz v3, :cond_3

    .line 171
    invoke-virtual {v2, v0, v1}, Lcom/yandex/metrica/impl/ob/bo;->d(J)Lcom/yandex/metrica/impl/ob/bo;

    .line 173
    :cond_3
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$e;->k:Lcom/yandex/metrica/impl/ob/dd;

    sget-object v1, Lcom/yandex/metrica/impl/ob/u$e;->h:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v6, v7}, Lcom/yandex/metrica/impl/ob/dd;->b(Ljava/lang/String;J)J

    move-result-wide v0

    .line 174
    cmp-long v3, v0, v6

    if-eqz v3, :cond_4

    .line 175
    invoke-virtual {v2, v0, v1}, Lcom/yandex/metrica/impl/ob/bo;->a(J)Lcom/yandex/metrica/impl/ob/bo;

    .line 177
    :cond_4
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/bo;->g()V

    .line 180
    :cond_5
    return-void
.end method
