.class Lcom/samsung/multiscreen/AudioPlayer$1;
.super Ljava/lang/Object;
.source "AudioPlayer.java"

# interfaces
.implements Lcom/samsung/multiscreen/Result;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/multiscreen/AudioPlayer;->addToList(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/samsung/multiscreen/Result",
        "<",
        "Lcom/samsung/multiscreen/Player$DMPStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/multiscreen/AudioPlayer;

.field final synthetic val$audioList:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/AudioPlayer;Ljava/util/List;)V
    .locals 0
    .param p1, "this$0"    # Lcom/samsung/multiscreen/AudioPlayer;

    .prologue
    .line 324
    iput-object p1, p0, Lcom/samsung/multiscreen/AudioPlayer$1;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    iput-object p2, p0, Lcom/samsung/multiscreen/AudioPlayer$1;->val$audioList:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/samsung/multiscreen/Error;)V
    .locals 3
    .param p1, "error"    # Lcom/samsung/multiscreen/Error;

    .prologue
    .line 394
    iget-object v0, p0, Lcom/samsung/multiscreen/AudioPlayer$1;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-virtual {v0}, Lcom/samsung/multiscreen/AudioPlayer;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AudioPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enQueue() Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/multiscreen/Error;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    :cond_0
    return-void
.end method

.method public onSuccess(Lcom/samsung/multiscreen/Player$DMPStatus;)V
    .locals 12
    .param p1, "status"    # Lcom/samsung/multiscreen/Player$DMPStatus;

    .prologue
    .line 327
    if-nez p1, :cond_1

    .line 328
    const-string v9, "AudioPlayer"

    const-string v10, "Error : Something went wrong with Node server!"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    :cond_0
    :goto_0
    return-void

    .line 339
    :cond_1
    iget-object v9, p1, Lcom/samsung/multiscreen/Player$DMPStatus;->mDMPRunning:Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_8

    iget-object v9, p1, Lcom/samsung/multiscreen/Player$DMPStatus;->mRunning:Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_8

    .line 340
    const/4 v7, 0x0

    .local v7, "it":I
    :goto_1
    iget-object v9, p0, Lcom/samsung/multiscreen/AudioPlayer$1;->val$audioList:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-ge v7, v9, :cond_0

    .line 341
    const/4 v0, 0x0

    .line 342
    .local v0, "albumArt":Landroid/net/Uri;
    const/4 v1, 0x0

    .line 343
    .local v1, "albumName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 344
    .local v2, "contentTitle":Ljava/lang/String;
    const/4 v5, 0x0

    .line 346
    .local v5, "endTime":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/multiscreen/AudioPlayer$1;->val$audioList:Ljava/util/List;

    invoke-interface {v9, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map;

    .line 348
    .local v8, "item":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v9, "uri"

    invoke-interface {v8, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 349
    const-string v9, "uri"

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 356
    .local v3, "contentUri":Landroid/net/Uri;
    sget-object v9, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->title:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;

    invoke-virtual {v9}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 357
    sget-object v9, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->title:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;

    invoke-virtual {v9}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "contentTitle":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 360
    .restart local v2    # "contentTitle":Ljava/lang/String;
    :cond_2
    sget-object v9, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->albumName:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;

    invoke-virtual {v9}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 361
    sget-object v9, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->albumName:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;

    invoke-virtual {v9}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "albumName":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 364
    .restart local v1    # "albumName":Ljava/lang/String;
    :cond_3
    sget-object v9, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->albumArt:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;

    invoke-virtual {v9}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 365
    sget-object v9, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->albumArt:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;

    invoke-virtual {v9}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 368
    :cond_4
    sget-object v9, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->endTime:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;

    invoke-virtual {v9}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 369
    sget-object v9, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->endTime:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;

    invoke-virtual {v9}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "endTime":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 372
    .restart local v5    # "endTime":Ljava/lang/String;
    :cond_5
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 374
    .local v4, "data":Lorg/json/JSONObject;
    :try_start_0
    const-string v9, "subEvent"

    sget-object v10, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->enqueue:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v10}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 375
    const-string v9, "playerType"

    sget-object v10, Lcom/samsung/multiscreen/Player$ContentType;->audio:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-virtual {v10}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 376
    const-string v9, "uri"

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 377
    sget-object v9, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->title:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;

    invoke-virtual {v9}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 378
    sget-object v9, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->albumName:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;

    invoke-virtual {v9}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 379
    sget-object v9, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->endTime:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;

    invoke-virtual {v9}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 380
    if-eqz v0, :cond_6

    .line 381
    sget-object v9, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->albumArt:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;

    invoke-virtual {v9}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 385
    :cond_6
    :goto_2
    sget-object v9, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v10, "playerQueueEvent"

    invoke-virtual {v9, v10, v4}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 340
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1

    .line 351
    .end local v3    # "contentUri":Landroid/net/Uri;
    .end local v4    # "data":Lorg/json/JSONObject;
    :cond_7
    iget-object v9, p0, Lcom/samsung/multiscreen/AudioPlayer$1;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-virtual {v9}, Lcom/samsung/multiscreen/AudioPlayer;->isDebug()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 352
    const-string v9, "AudioPlayer"

    const-string v10, "enQueue(): ContentUrl can not be Optional."

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 382
    .restart local v3    # "contentUri":Landroid/net/Uri;
    .restart local v4    # "data":Lorg/json/JSONObject;
    :catch_0
    move-exception v6

    .line 383
    .local v6, "exception":Ljava/lang/Exception;
    const-string v9, "AudioPlayer"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "enQueue(): Error in parsing JSON object: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 388
    .end local v0    # "albumArt":Landroid/net/Uri;
    .end local v1    # "albumName":Ljava/lang/String;
    .end local v2    # "contentTitle":Ljava/lang/String;
    .end local v3    # "contentUri":Landroid/net/Uri;
    .end local v4    # "data":Lorg/json/JSONObject;
    .end local v5    # "endTime":Ljava/lang/String;
    .end local v6    # "exception":Ljava/lang/Exception;
    .end local v7    # "it":I
    .end local v8    # "item":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_8
    iget-object v9, p0, Lcom/samsung/multiscreen/AudioPlayer$1;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-virtual {v9}, Lcom/samsung/multiscreen/AudioPlayer;->isDebug()Z

    move-result v9

    if-eqz v9, :cond_0

    const-string v9, "AudioPlayer"

    const-string v10, "enQueue() Error: DMP Un-Initialized!"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 324
    check-cast p1, Lcom/samsung/multiscreen/Player$DMPStatus;

    invoke-virtual {p0, p1}, Lcom/samsung/multiscreen/AudioPlayer$1;->onSuccess(Lcom/samsung/multiscreen/Player$DMPStatus;)V

    return-void
.end method
