.class public Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;
.super Ljava/lang/Object;
.source "TvContentProviderData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/api/provider/TvContentProviderData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RubricTelecastCacheObj"
.end annotation


# instance fields
.field cacheExpireTime:J

.field channels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/provider/Channel;",
            ">;"
        }
    .end annotation
.end field

.field noMoreData:Z

.field telecasts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/tv/replies/Telecast;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lru/cn/api/provider/TvContentProviderData;


# direct methods
.method public constructor <init>(Lru/cn/api/provider/TvContentProviderData;)V
    .locals 1
    .param p1, "this$0"    # Lru/cn/api/provider/TvContentProviderData;

    .prologue
    .line 184
    iput-object p1, p0, Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;->this$0:Lru/cn/api/provider/TvContentProviderData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;->channels:Ljava/util/List;

    .line 188
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;->noMoreData:Z

    return-void
.end method
