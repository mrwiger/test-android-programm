.class public interface abstract Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;
.super Ljava/lang/Object;
.source "PlayerRemoteController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/peersay/controllers/PlayerRemoteController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IRemotePlayer"
.end annotation


# virtual methods
.method public abstract getCurrentPosition()I
.end method

.method public abstract getDuration()I
.end method

.method public abstract getLivePosition()I
.end method

.method public abstract getState()Lru/cn/player/AbstractMediaPlayer$PlayerState;
.end method

.method public abstract isSeekable()Z
.end method

.method public abstract pause()V
.end method

.method public abstract play()V
.end method

.method public abstract seekTo(I)V
.end method
