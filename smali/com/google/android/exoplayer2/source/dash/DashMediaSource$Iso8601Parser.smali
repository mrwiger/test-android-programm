.class final Lcom/google/android/exoplayer2/source/dash/DashMediaSource$Iso8601Parser;
.super Ljava/lang/Object;
.source "DashMediaSource.java"

# interfaces
.implements Lcom/google/android/exoplayer2/upstream/ParsingLoadable$Parser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/source/dash/DashMediaSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Iso8601Parser"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/exoplayer2/upstream/ParsingLoadable$Parser",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# static fields
.field private static final TIMESTAMP_WITH_TIMEZONE_PATTERN:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1112
    const-string v0, "(.+?)(Z|((\\+|-|\u2212)(\\d\\d)(:?(\\d\\d))?))"

    .line 1113
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/source/dash/DashMediaSource$Iso8601Parser;->TIMESTAMP_WITH_TIMEZONE_PATTERN:Ljava/util/regex/Pattern;

    .line 1112
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 1110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public parse(Landroid/net/Uri;Ljava/io/InputStream;)Ljava/lang/Long;
    .locals 26
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1117
    new-instance v21, Ljava/io/BufferedReader;

    new-instance v22, Ljava/io/InputStreamReader;

    const-string v23, "UTF-8"

    .line 1118
    invoke-static/range {v23 .. v23}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, p2

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-direct/range {v21 .. v22}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 1119
    invoke-virtual/range {v21 .. v21}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 1121
    .local v5, "firstLine":Ljava/lang/String;
    :try_start_0
    sget-object v21, Lcom/google/android/exoplayer2/source/dash/DashMediaSource$Iso8601Parser;->TIMESTAMP_WITH_TIMEZONE_PATTERN:Ljava/util/regex/Pattern;

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    .line 1122
    .local v7, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v7}, Ljava/util/regex/Matcher;->matches()Z

    move-result v21

    if-nez v21, :cond_0

    .line 1123
    new-instance v21, Lcom/google/android/exoplayer2/ParserException;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Couldn\'t parse timestamp: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/String;)V

    throw v21
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1143
    .end local v7    # "matcher":Ljava/util/regex/Matcher;
    :catch_0
    move-exception v4

    .line 1144
    .local v4, "e":Ljava/text/ParseException;
    new-instance v21, Lcom/google/android/exoplayer2/ParserException;

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Lcom/google/android/exoplayer2/ParserException;-><init>(Ljava/lang/Throwable;)V

    throw v21

    .line 1126
    .end local v4    # "e":Ljava/text/ParseException;
    .restart local v7    # "matcher":Ljava/util/regex/Matcher;
    :cond_0
    const/16 v21, 0x1

    :try_start_1
    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v13

    .line 1127
    .local v13, "timestampWithoutTimezone":Ljava/lang/String;
    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v21, "yyyy-MM-dd\'T\'HH:mm:ss"

    sget-object v22, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v6, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1128
    .local v6, "format":Ljava/text/SimpleDateFormat;
    const-string v21, "UTC"

    invoke-static/range {v21 .. v21}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1129
    invoke-virtual {v6, v13}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/Date;->getTime()J

    move-result-wide v16

    .line 1131
    .local v16, "timestampMs":J
    const/16 v21, 0x2

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v20

    .line 1132
    .local v20, "timezone":Ljava/lang/String;
    const-string v21, "Z"

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_1

    .line 1142
    :goto_0
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    return-object v21

    .line 1135
    :cond_1
    const-string v21, "+"

    const/16 v22, 0x4

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_2

    const-wide/16 v14, 0x1

    .line 1136
    .local v14, "sign":J
    :goto_1
    const/16 v21, 0x5

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 1137
    .local v8, "hours":J
    const/16 v21, 0x7

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v12

    .line 1138
    .local v12, "minutesString":Ljava/lang/String;
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_3

    const-wide/16 v10, 0x0

    .line 1139
    .local v10, "minutes":J
    :goto_2
    const-wide/16 v22, 0x3c

    mul-long v22, v22, v8

    add-long v22, v22, v10

    const-wide/16 v24, 0x3c

    mul-long v22, v22, v24

    const-wide/16 v24, 0x3e8

    mul-long v22, v22, v24

    mul-long v18, v14, v22

    .line 1140
    .local v18, "timestampOffsetMs":J
    sub-long v16, v16, v18

    goto :goto_0

    .line 1135
    .end local v8    # "hours":J
    .end local v10    # "minutes":J
    .end local v12    # "minutesString":Ljava/lang/String;
    .end local v14    # "sign":J
    .end local v18    # "timestampOffsetMs":J
    :cond_2
    const-wide/16 v14, -0x1

    goto :goto_1

    .line 1138
    .restart local v8    # "hours":J
    .restart local v12    # "minutesString":Ljava/lang/String;
    .restart local v14    # "sign":J
    :cond_3
    invoke-static {v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v10

    goto :goto_2
.end method

.method public bridge synthetic parse(Landroid/net/Uri;Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1110
    invoke-virtual {p0, p1, p2}, Lcom/google/android/exoplayer2/source/dash/DashMediaSource$Iso8601Parser;->parse(Landroid/net/Uri;Ljava/io/InputStream;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
