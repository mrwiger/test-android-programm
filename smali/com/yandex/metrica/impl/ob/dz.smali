.class public Lcom/yandex/metrica/impl/ob/dz;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/yandex/metrica/impl/ob/cs;

.field private final b:Lcom/yandex/metrica/impl/ob/du;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 31
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/cq;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/cq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/cq;->g()Lcom/yandex/metrica/impl/ob/cs;

    move-result-object v0

    new-instance v1, Lcom/yandex/metrica/impl/ob/du;

    invoke-direct {v1, p1}, Lcom/yandex/metrica/impl/ob/du;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/dz;-><init>(Lcom/yandex/metrica/impl/ob/cs;Lcom/yandex/metrica/impl/ob/du;)V

    .line 32
    return-void
.end method

.method constructor <init>(Lcom/yandex/metrica/impl/ob/cs;Lcom/yandex/metrica/impl/ob/du;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/dz;->a:Lcom/yandex/metrica/impl/ob/cs;

    .line 47
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/dz;->b:Lcom/yandex/metrica/impl/ob/du;

    .line 48
    return-void
.end method


# virtual methods
.method public a(Lcom/yandex/metrica/impl/ob/ea;)V
    .locals 4

    .prologue
    .line 36
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/dz;->b:Lcom/yandex/metrica/impl/ob/du;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/du;->a(Lcom/yandex/metrica/impl/ob/ea;)Ljava/lang/String;

    move-result-object v0

    .line 37
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 40
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/dz;->a:Lcom/yandex/metrica/impl/ob/cs;

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/ea;->b()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, v0}, Lcom/yandex/metrica/impl/ob/cs;->a(JLjava/lang/String;)V

    .line 42
    :cond_0
    return-void
.end method
