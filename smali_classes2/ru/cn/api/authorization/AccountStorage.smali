.class public Lru/cn/api/authorization/AccountStorage;
.super Ljava/lang/Object;
.source "AccountStorage.java"


# static fields
.field private static final accounts:Landroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Lru/cn/api/authorization/Account;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v0}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    sput-object v0, Lru/cn/api/authorization/AccountStorage;->accounts:Landroid/support/v4/util/LongSparseArray;

    return-void
.end method

.method public static getAccount(Landroid/content/Context;J)Lru/cn/api/authorization/Account;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contractorId"    # J

    .prologue
    .line 21
    sget-object v1, Lru/cn/api/authorization/AccountStorage;->accounts:Landroid/support/v4/util/LongSparseArray;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, p2, v2}, Landroid/support/v4/util/LongSparseArray;->get(JLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/authorization/Account;

    .line 22
    .local v0, "account":Lru/cn/api/authorization/Account;
    if-eqz v0, :cond_0

    move-object v1, v0

    .line 29
    :goto_0
    return-object v1

    .line 25
    :cond_0
    invoke-static {p0, p1, p2}, Lru/cn/api/authorization/AccountStorage;->getSavedV2Account(Landroid/content/Context;J)Lru/cn/api/authorization/Account;

    move-result-object v0

    .line 26
    if-eqz v0, :cond_1

    move-object v1, v0

    .line 27
    goto :goto_0

    .line 29
    :cond_1
    invoke-static {p0, p1, p2}, Lru/cn/api/authorization/AccountStorage;->getSavedV1Account(Landroid/content/Context;J)Lru/cn/api/authorization/Account;

    move-result-object v1

    goto :goto_0
.end method

.method private static getSavedV1Account(Landroid/content/Context;J)Lru/cn/api/authorization/Account;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contractorId"    # J

    .prologue
    const/4 v6, 0x0

    .line 72
    const-string v4, "auth"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 73
    .local v2, "preferences":Landroid/content/SharedPreferences;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "token"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "accessToken":Ljava/lang/String;
    const/4 v1, 0x0

    .line 76
    .local v1, "account":Lru/cn/api/authorization/Account;
    if-eqz v0, :cond_0

    .line 77
    new-instance v3, Lru/cn/api/authorization/Account$Token;

    invoke-direct {v3}, Lru/cn/api/authorization/Account$Token;-><init>()V

    .line 78
    .local v3, "token":Lru/cn/api/authorization/Account$Token;
    iput-object v0, v3, Lru/cn/api/authorization/Account$Token;->accessToken:Ljava/lang/String;

    .line 79
    new-instance v1, Lru/cn/api/authorization/Account;

    .end local v1    # "account":Lru/cn/api/authorization/Account;
    invoke-direct {v1, v6, v3}, Lru/cn/api/authorization/Account;-><init>(Ljava/lang/String;Lru/cn/api/authorization/Account$Token;)V

    .line 80
    .restart local v1    # "account":Lru/cn/api/authorization/Account;
    sget-object v4, Lru/cn/api/authorization/AccountStorage;->accounts:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v4, p1, p2, v1}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 83
    .end local v3    # "token":Lru/cn/api/authorization/Account$Token;
    :cond_0
    return-object v1
.end method

.method private static getSavedV2Account(Landroid/content/Context;J)Lru/cn/api/authorization/Account;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contractorId"    # J

    .prologue
    const/4 v0, 0x0

    .line 57
    const-string v3, "auth"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 59
    .local v2, "preferences":Landroid/content/SharedPreferences;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "account"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 60
    .local v1, "jsonAccount":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 68
    :cond_0
    :goto_0
    return-object v0

    .line 63
    :cond_1
    new-instance v3, Lcom/google/gson/Gson;

    invoke-direct {v3}, Lcom/google/gson/Gson;-><init>()V

    const-class v4, Lru/cn/api/authorization/Account;

    invoke-virtual {v3, v1, v4}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/authorization/Account;

    .line 64
    .local v0, "account":Lru/cn/api/authorization/Account;
    if-eqz v0, :cond_0

    .line 65
    sget-object v3, Lru/cn/api/authorization/AccountStorage;->accounts:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v3, p1, p2, v0}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    goto :goto_0
.end method

.method static removeAccount(Landroid/content/Context;J)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contractorId"    # J

    .prologue
    .line 47
    sget-object v1, Lru/cn/api/authorization/AccountStorage;->accounts:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v1, p1, p2}, Landroid/support/v4/util/LongSparseArray;->remove(J)V

    .line 49
    const-string v1, "auth"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 51
    .local v0, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "account"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 52
    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 53
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 54
    return-void
.end method

.method static saveAccount(Landroid/content/Context;Lru/cn/api/authorization/Account;J)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Lru/cn/api/authorization/Account;
    .param p2, "contractorId"    # J

    .prologue
    .line 33
    new-instance v2, Lcom/google/gson/GsonBuilder;

    invoke-direct {v2}, Lcom/google/gson/GsonBuilder;-><init>()V

    .line 34
    invoke-virtual {v2}, Lcom/google/gson/GsonBuilder;->excludeFieldsWithoutExposeAnnotation()Lcom/google/gson/GsonBuilder;

    move-result-object v2

    .line 35
    invoke-virtual {v2}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 37
    .local v0, "jsonAccount":Ljava/lang/String;
    const-string v2, "auth"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 39
    .local v1, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "account"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 40
    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 41
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 43
    sget-object v2, Lru/cn/api/authorization/AccountStorage;->accounts:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v2, p2, p3, p1}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 44
    return-void
.end method
