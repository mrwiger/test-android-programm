.class final Lcom/google/obf/j;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Landroid/os/HandlerThread;

.field private final c:Landroid/os/Handler;

.field private final d:Lcom/google/obf/w;

.field private final e:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/obf/x;",
            ">;"
        }
    .end annotation
.end field

.field private final g:[[Lcom/google/obf/q;

.field private final h:[I

.field private final i:J

.field private final j:J

.field private k:[Lcom/google/obf/x;

.field private l:Lcom/google/obf/x;

.field private m:Lcom/google/obf/k;

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:I

.field private r:I

.field private s:I

.field private t:J

.field private u:J

.field private volatile v:J

.field private volatile w:J

.field private volatile x:J


# direct methods
.method public constructor <init>(Landroid/os/Handler;Z[III)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x3e8

    const-wide/16 v2, -0x1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput v0, p0, Lcom/google/obf/j;->r:I

    .line 3
    iput v0, p0, Lcom/google/obf/j;->s:I

    .line 4
    iput-object p1, p0, Lcom/google/obf/j;->c:Landroid/os/Handler;

    .line 5
    iput-boolean p2, p0, Lcom/google/obf/j;->o:Z

    .line 6
    int-to-long v0, p4

    mul-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/obf/j;->i:J

    .line 7
    int-to-long v0, p5

    mul-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/obf/j;->j:J

    .line 8
    array-length v0, p3

    invoke-static {p3, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/j;->h:[I

    .line 9
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/obf/j;->q:I

    .line 10
    iput-wide v2, p0, Lcom/google/obf/j;->v:J

    .line 11
    iput-wide v2, p0, Lcom/google/obf/j;->x:J

    .line 12
    new-instance v0, Lcom/google/obf/w;

    invoke-direct {v0}, Lcom/google/obf/w;-><init>()V

    iput-object v0, p0, Lcom/google/obf/j;->d:Lcom/google/obf/w;

    .line 13
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/google/obf/j;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/j;->f:Ljava/util/List;

    .line 15
    array-length v0, p3

    new-array v0, v0, [[Lcom/google/obf/q;

    iput-object v0, p0, Lcom/google/obf/j;->g:[[Lcom/google/obf/q;

    .line 16
    new-instance v0, Lcom/google/obf/dy;

    const-string v1, "ExoPlayerImplInternal:Handler"

    const/16 v2, -0x10

    invoke-direct {v0, v1, v2}, Lcom/google/obf/dy;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/obf/j;->b:Landroid/os/HandlerThread;

    .line 17
    iget-object v0, p0, Lcom/google/obf/j;->b:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 18
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/obf/j;->b:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/obf/j;->a:Landroid/os/Handler;

    .line 19
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 93
    iget v0, p0, Lcom/google/obf/j;->q:I

    if-eq v0, p1, :cond_0

    .line 94
    iput p1, p0, Lcom/google/obf/j;->q:I

    .line 95
    iget-object v0, p0, Lcom/google/obf/j;->c:Landroid/os/Handler;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 96
    :cond_0
    return-void
.end method

.method private a(II)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 324
    iget-object v2, p0, Lcom/google/obf/j;->h:[I

    aget v2, v2, p1

    if-ne v2, p2, :cond_1

    .line 348
    :cond_0
    :goto_0
    return-void

    .line 326
    :cond_1
    iget-object v2, p0, Lcom/google/obf/j;->h:[I

    aput p2, v2, p1

    .line 327
    iget v2, p0, Lcom/google/obf/j;->q:I

    if-eq v2, v0, :cond_0

    iget v2, p0, Lcom/google/obf/j;->q:I

    if-eq v2, v5, :cond_0

    .line 329
    iget-object v2, p0, Lcom/google/obf/j;->k:[Lcom/google/obf/x;

    aget-object v4, v2, p1

    .line 330
    invoke-virtual {v4}, Lcom/google/obf/x;->v()I

    move-result v2

    .line 331
    if-eqz v2, :cond_0

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 332
    invoke-virtual {v4}, Lcom/google/obf/x;->u()I

    move-result v3

    if-eqz v3, :cond_0

    .line 334
    if-eq v2, v5, :cond_2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_6

    :cond_2
    move v3, v0

    .line 335
    :goto_1
    if-ltz p2, :cond_7

    iget-object v2, p0, Lcom/google/obf/j;->g:[[Lcom/google/obf/q;

    aget-object v2, v2, p1

    array-length v2, v2

    if-ge p2, v2, :cond_7

    move v2, v0

    .line 336
    :goto_2
    if-eqz v3, :cond_4

    .line 337
    if-nez v2, :cond_3

    iget-object v5, p0, Lcom/google/obf/j;->l:Lcom/google/obf/x;

    if-ne v4, v5, :cond_3

    .line 338
    iget-object v5, p0, Lcom/google/obf/j;->d:Lcom/google/obf/w;

    iget-object v6, p0, Lcom/google/obf/j;->m:Lcom/google/obf/k;

    invoke-interface {v6}, Lcom/google/obf/k;->a()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/google/obf/w;->a(J)V

    .line 339
    :cond_3
    invoke-direct {p0, v4}, Lcom/google/obf/j;->e(Lcom/google/obf/x;)V

    .line 340
    iget-object v5, p0, Lcom/google/obf/j;->f:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 341
    :cond_4
    if-eqz v2, :cond_0

    .line 342
    iget-boolean v2, p0, Lcom/google/obf/j;->o:Z

    if-eqz v2, :cond_8

    iget v2, p0, Lcom/google/obf/j;->q:I

    const/4 v5, 0x4

    if-ne v2, v5, :cond_8

    move v2, v0

    .line 343
    :goto_3
    if-nez v3, :cond_9

    if-eqz v2, :cond_9

    .line 344
    :goto_4
    invoke-direct {p0, v4, p2, v0}, Lcom/google/obf/j;->a(Lcom/google/obf/x;IZ)V

    .line 345
    if-eqz v2, :cond_5

    .line 346
    invoke-virtual {v4}, Lcom/google/obf/x;->w()V

    .line 347
    :cond_5
    iget-object v0, p0, Lcom/google/obf/j;->a:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_6
    move v3, v1

    .line 334
    goto :goto_1

    :cond_7
    move v2, v1

    .line 335
    goto :goto_2

    :cond_8
    move v2, v1

    .line 342
    goto :goto_3

    :cond_9
    move v0, v1

    .line 343
    goto :goto_4
.end method

.method private a(IJJ)V
    .locals 4

    .prologue
    .line 241
    add-long v0, p2, p4

    .line 242
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 243
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 244
    iget-object v0, p0, Lcom/google/obf/j;->a:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 246
    :goto_0
    return-void

    .line 245
    :cond_0
    iget-object v2, p0, Lcom/google/obf/j;->a:Landroid/os/Handler;

    invoke-virtual {v2, p1, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private a(ILjava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 310
    :try_start_0
    check-cast p2, Landroid/util/Pair;

    .line 311
    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/obf/h$a;

    iget-object v1, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-interface {v0, p1, v1}, Lcom/google/obf/h$a;->a(ILjava/lang/Object;)V

    .line 312
    iget v0, p0, Lcom/google/obf/j;->q:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/obf/j;->q:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 313
    iget-object v0, p0, Lcom/google/obf/j;->a:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 314
    :cond_0
    monitor-enter p0

    .line 315
    :try_start_1
    iget v0, p0, Lcom/google/obf/j;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/j;->s:I

    .line 316
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 317
    monitor-exit p0

    .line 323
    return-void

    .line 317
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 319
    :catchall_1
    move-exception v0

    monitor-enter p0

    .line 320
    :try_start_2
    iget v1, p0, Lcom/google/obf/j;->s:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/obf/j;->s:I

    .line 321
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 322
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    throw v0

    :catchall_2
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v0
.end method

.method private a(Lcom/google/obf/x;IZ)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 149
    iget-wide v0, p0, Lcom/google/obf/j;->w:J

    invoke-virtual {p1, p2, v0, v1, p3}, Lcom/google/obf/x;->b(IJZ)V

    .line 150
    iget-object v0, p0, Lcom/google/obf/j;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    invoke-virtual {p1}, Lcom/google/obf/x;->b()Lcom/google/obf/k;

    move-result-object v1

    .line 152
    if-eqz v1, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/obf/j;->m:Lcom/google/obf/k;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 154
    iput-object v1, p0, Lcom/google/obf/j;->m:Lcom/google/obf/k;

    .line 155
    iput-object p1, p0, Lcom/google/obf/j;->l:Lcom/google/obf/x;

    .line 156
    :cond_0
    return-void

    .line 153
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/obf/x;)Z
    .locals 12

    .prologue
    const-wide/16 v10, -0x1

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 157
    invoke-virtual {p1}, Lcom/google/obf/x;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 166
    :cond_0
    :goto_0
    return v1

    .line 159
    :cond_1
    invoke-virtual {p1}, Lcom/google/obf/x;->f()Z

    move-result v2

    if-nez v2, :cond_2

    move v1, v0

    .line 160
    goto :goto_0

    .line 161
    :cond_2
    iget v2, p0, Lcom/google/obf/j;->q:I

    const/4 v3, 0x4

    if-eq v2, v3, :cond_0

    .line 163
    invoke-virtual {p1}, Lcom/google/obf/x;->r()J

    move-result-wide v4

    .line 164
    invoke-virtual {p1}, Lcom/google/obf/x;->q()J

    move-result-wide v6

    .line 165
    iget-boolean v2, p0, Lcom/google/obf/j;->p:Z

    if-eqz v2, :cond_5

    iget-wide v2, p0, Lcom/google/obf/j;->j:J

    .line 166
    :goto_1
    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-lez v8, :cond_3

    cmp-long v8, v6, v10

    if-eqz v8, :cond_3

    const-wide/16 v8, -0x3

    cmp-long v8, v6, v8

    if-eqz v8, :cond_3

    iget-wide v8, p0, Lcom/google/obf/j;->w:J

    add-long/2addr v2, v8

    cmp-long v2, v6, v2

    if-gez v2, :cond_3

    cmp-long v2, v4, v10

    if-eqz v2, :cond_4

    const-wide/16 v2, -0x2

    cmp-long v2, v4, v2

    if-eqz v2, :cond_4

    cmp-long v2, v6, v4

    if-ltz v2, :cond_4

    :cond_3
    move v0, v1

    :cond_4
    move v1, v0

    goto :goto_0

    .line 165
    :cond_5
    iget-wide v2, p0, Lcom/google/obf/j;->i:J

    goto :goto_1
.end method

.method private b(J)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x3e8

    const/4 v0, 0x0

    .line 247
    :try_start_0
    iget-wide v2, p0, Lcom/google/obf/j;->w:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v1, p1, v2

    if-nez v1, :cond_0

    .line 248
    iget-object v0, p0, Lcom/google/obf/j;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 267
    :goto_0
    return-void

    .line 250
    :cond_0
    const/4 v1, 0x0

    :try_start_1
    iput-boolean v1, p0, Lcom/google/obf/j;->p:Z

    .line 251
    mul-long v2, p1, v6

    iput-wide v2, p0, Lcom/google/obf/j;->w:J

    .line 252
    iget-object v1, p0, Lcom/google/obf/j;->d:Lcom/google/obf/w;

    invoke-virtual {v1}, Lcom/google/obf/w;->c()V

    .line 253
    iget-object v1, p0, Lcom/google/obf/j;->d:Lcom/google/obf/w;

    iget-wide v2, p0, Lcom/google/obf/j;->w:J

    invoke-virtual {v1, v2, v3}, Lcom/google/obf/w;->a(J)V

    .line 254
    iget v1, p0, Lcom/google/obf/j;->q:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/google/obf/j;->q:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 255
    :cond_1
    iget-object v0, p0, Lcom/google/obf/j;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    goto :goto_0

    :cond_2
    move v1, v0

    .line 257
    :goto_1
    :try_start_2
    iget-object v0, p0, Lcom/google/obf/j;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 258
    iget-object v0, p0, Lcom/google/obf/j;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/x;

    .line 259
    invoke-direct {p0, v0}, Lcom/google/obf/j;->d(Lcom/google/obf/x;)V

    .line 260
    iget-wide v2, p0, Lcom/google/obf/j;->w:J

    invoke-virtual {v0, v2, v3}, Lcom/google/obf/x;->d(J)V

    .line 261
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 262
    :cond_3
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/obf/j;->a(I)V

    .line 263
    iget-object v0, p0, Lcom/google/obf/j;->a:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 264
    iget-object v0, p0, Lcom/google/obf/j;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    goto :goto_0

    .line 266
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/obf/j;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    throw v0
.end method

.method private b(Lcom/google/obf/x;)V
    .locals 3

    .prologue
    .line 294
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/obf/j;->e(Lcom/google/obf/x;)V
    :try_end_0
    .catch Lcom/google/obf/g; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 301
    :goto_0
    return-void

    .line 296
    :catch_0
    move-exception v0

    .line 297
    const-string v1, "ExoPlayerImplInternal"

    const-string v2, "Stop failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 299
    :catch_1
    move-exception v0

    .line 300
    const-string v1, "ExoPlayerImplInternal"

    const-string v2, "Stop failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private b(Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    .line 167
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/obf/j;->p:Z

    .line 168
    iput-boolean p1, p0, Lcom/google/obf/j;->o:Z

    .line 169
    if-nez p1, :cond_1

    .line 170
    invoke-direct {p0}, Lcom/google/obf/j;->g()V

    .line 171
    invoke-direct {p0}, Lcom/google/obf/j;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/obf/j;->c:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 180
    return-void

    .line 172
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/google/obf/j;->q:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 173
    invoke-direct {p0}, Lcom/google/obf/j;->f()V

    .line 174
    iget-object v0, p0, Lcom/google/obf/j;->a:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 179
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/obf/j;->c:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    throw v0

    .line 175
    :cond_2
    :try_start_2
    iget v0, p0, Lcom/google/obf/j;->q:I

    if-ne v0, v2, :cond_0

    .line 176
    iget-object v0, p0, Lcom/google/obf/j;->a:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private b([Lcom/google/obf/x;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/google/obf/j;->l()V

    .line 98
    iput-object p1, p0, Lcom/google/obf/j;->k:[Lcom/google/obf/x;

    .line 99
    iget-object v0, p0, Lcom/google/obf/j;->g:[[Lcom/google/obf/q;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 100
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/obf/j;->a(I)V

    .line 101
    invoke-direct {p0}, Lcom/google/obf/j;->e()V

    .line 102
    return-void
.end method

.method private c(Lcom/google/obf/x;)V
    .locals 3

    .prologue
    .line 302
    :try_start_0
    invoke-virtual {p1}, Lcom/google/obf/x;->z()V
    :try_end_0
    .catch Lcom/google/obf/g; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 309
    :goto_0
    return-void

    .line 304
    :catch_0
    move-exception v0

    .line 305
    const-string v1, "ExoPlayerImplInternal"

    const-string v2, "Release failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 307
    :catch_1
    move-exception v0

    .line 308
    const-string v1, "ExoPlayerImplInternal"

    const-string v2, "Release failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private d(Lcom/google/obf/x;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 349
    invoke-virtual {p1}, Lcom/google/obf/x;->v()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 350
    invoke-virtual {p1}, Lcom/google/obf/x;->x()V

    .line 351
    :cond_0
    return-void
.end method

.method private e()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 103
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 104
    const/4 v1, 0x1

    .line 105
    const/4 v0, 0x0

    :goto_0
    iget-object v4, p0, Lcom/google/obf/j;->k:[Lcom/google/obf/x;

    array-length v4, v4

    if-ge v0, v4, :cond_1

    .line 106
    iget-object v4, p0, Lcom/google/obf/j;->k:[Lcom/google/obf/x;

    aget-object v4, v4, v0

    .line 107
    invoke-virtual {v4}, Lcom/google/obf/x;->v()I

    move-result v5

    if-nez v5, :cond_0

    .line 108
    iget-wide v6, p0, Lcom/google/obf/j;->w:J

    invoke-virtual {v4, v6, v7}, Lcom/google/obf/x;->f(J)I

    move-result v5

    .line 109
    if-nez v5, :cond_0

    .line 110
    invoke-virtual {v4}, Lcom/google/obf/x;->s()V

    .line 111
    const/4 v1, 0x0

    .line 112
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 113
    :cond_1
    if-nez v1, :cond_2

    .line 114
    const/4 v1, 0x2

    const-wide/16 v4, 0xa

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/obf/j;->a(IJJ)V

    .line 148
    :goto_1
    return-void

    .line 116
    :cond_2
    const-wide/16 v2, 0x0

    .line 117
    const/4 v4, 0x1

    .line 118
    const/4 v1, 0x1

    .line 119
    const/4 v0, 0x0

    :goto_2
    iget-object v5, p0, Lcom/google/obf/j;->k:[Lcom/google/obf/x;

    array-length v5, v5

    if-ge v0, v5, :cond_a

    .line 120
    iget-object v5, p0, Lcom/google/obf/j;->k:[Lcom/google/obf/x;

    aget-object v6, v5, v0

    .line 121
    invoke-virtual {v6}, Lcom/google/obf/x;->u()I

    move-result v7

    .line 122
    new-array v8, v7, [Lcom/google/obf/q;

    .line 123
    const/4 v5, 0x0

    :goto_3
    if-ge v5, v7, :cond_3

    .line 124
    invoke-virtual {v6, v5}, Lcom/google/obf/x;->b(I)Lcom/google/obf/q;

    move-result-object v9

    aput-object v9, v8, v5

    .line 125
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 126
    :cond_3
    iget-object v5, p0, Lcom/google/obf/j;->g:[[Lcom/google/obf/q;

    aput-object v8, v5, v0

    .line 127
    if-lez v7, :cond_5

    .line 128
    const-wide/16 v10, -0x1

    cmp-long v5, v2, v10

    if-nez v5, :cond_6

    .line 134
    :cond_4
    :goto_4
    iget-object v5, p0, Lcom/google/obf/j;->h:[I

    aget v5, v5, v0

    .line 135
    if-ltz v5, :cond_5

    array-length v7, v8

    if-ge v5, v7, :cond_5

    .line 136
    const/4 v7, 0x0

    invoke-direct {p0, v6, v5, v7}, Lcom/google/obf/j;->a(Lcom/google/obf/x;IZ)V

    .line 137
    if-eqz v4, :cond_8

    invoke-virtual {v6}, Lcom/google/obf/x;->e()Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v4, 0x1

    .line 138
    :goto_5
    if-eqz v1, :cond_9

    invoke-direct {p0, v6}, Lcom/google/obf/j;->a(Lcom/google/obf/x;)Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v1, 0x1

    .line 139
    :cond_5
    :goto_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 129
    :cond_6
    invoke-virtual {v6}, Lcom/google/obf/x;->r()J

    move-result-wide v10

    .line 130
    const-wide/16 v12, -0x1

    cmp-long v5, v10, v12

    if-nez v5, :cond_7

    .line 131
    const-wide/16 v2, -0x1

    goto :goto_4

    .line 132
    :cond_7
    const-wide/16 v12, -0x2

    cmp-long v5, v10, v12

    if-eqz v5, :cond_4

    .line 133
    invoke-static {v2, v3, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    goto :goto_4

    .line 137
    :cond_8
    const/4 v4, 0x0

    goto :goto_5

    .line 138
    :cond_9
    const/4 v1, 0x0

    goto :goto_6

    .line 140
    :cond_a
    iput-wide v2, p0, Lcom/google/obf/j;->v:J

    .line 141
    if-eqz v4, :cond_d

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_b

    iget-wide v4, p0, Lcom/google/obf/j;->w:J

    cmp-long v0, v2, v4

    if-gtz v0, :cond_d

    .line 142
    :cond_b
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/obf/j;->q:I

    .line 144
    :goto_7
    iget-object v0, p0, Lcom/google/obf/j;->c:Landroid/os/Handler;

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/obf/j;->q:I

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/obf/j;->g:[[Lcom/google/obf/q;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 145
    iget-boolean v0, p0, Lcom/google/obf/j;->o:Z

    if-eqz v0, :cond_c

    iget v0, p0, Lcom/google/obf/j;->q:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_c

    .line 146
    invoke-direct {p0}, Lcom/google/obf/j;->f()V

    .line 147
    :cond_c
    iget-object v0, p0, Lcom/google/obf/j;->a:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_1

    .line 143
    :cond_d
    if-eqz v1, :cond_e

    const/4 v0, 0x4

    :goto_8
    iput v0, p0, Lcom/google/obf/j;->q:I

    goto :goto_7

    :cond_e
    const/4 v0, 0x3

    goto :goto_8
.end method

.method private e(Lcom/google/obf/x;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 352
    invoke-direct {p0, p1}, Lcom/google/obf/j;->d(Lcom/google/obf/x;)V

    .line 353
    invoke-virtual {p1}, Lcom/google/obf/x;->v()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 354
    invoke-virtual {p1}, Lcom/google/obf/x;->y()V

    .line 355
    iget-object v0, p0, Lcom/google/obf/j;->l:Lcom/google/obf/x;

    if-ne p1, v0, :cond_0

    .line 356
    iput-object v2, p0, Lcom/google/obf/j;->m:Lcom/google/obf/k;

    .line 357
    iput-object v2, p0, Lcom/google/obf/j;->l:Lcom/google/obf/x;

    .line 358
    :cond_0
    return-void
.end method

.method private f()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 181
    iput-boolean v0, p0, Lcom/google/obf/j;->p:Z

    .line 182
    iget-object v1, p0, Lcom/google/obf/j;->d:Lcom/google/obf/w;

    invoke-virtual {v1}, Lcom/google/obf/w;->b()V

    move v1, v0

    .line 183
    :goto_0
    iget-object v0, p0, Lcom/google/obf/j;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/google/obf/j;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/x;

    invoke-virtual {v0}, Lcom/google/obf/x;->w()V

    .line 185
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 186
    :cond_0
    return-void
.end method

.method private g()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/obf/j;->d:Lcom/google/obf/w;

    invoke-virtual {v0}, Lcom/google/obf/w;->c()V

    .line 188
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/obf/j;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/google/obf/j;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/x;

    invoke-direct {p0, v0}, Lcom/google/obf/j;->d(Lcom/google/obf/x;)V

    .line 190
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 191
    :cond_0
    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/obf/j;->m:Lcom/google/obf/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/j;->f:Ljava/util/List;

    iget-object v1, p0, Lcom/google/obf/j;->l:Lcom/google/obf/x;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/j;->l:Lcom/google/obf/x;

    .line 193
    invoke-virtual {v0}, Lcom/google/obf/x;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/google/obf/j;->m:Lcom/google/obf/k;

    invoke-interface {v0}, Lcom/google/obf/k;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/obf/j;->w:J

    .line 195
    iget-object v0, p0, Lcom/google/obf/j;->d:Lcom/google/obf/w;

    iget-wide v2, p0, Lcom/google/obf/j;->w:J

    invoke-virtual {v0, v2, v3}, Lcom/google/obf/w;->a(J)V

    .line 197
    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/obf/j;->u:J

    .line 198
    return-void

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/google/obf/j;->d:Lcom/google/obf/w;

    invoke-virtual {v0}, Lcom/google/obf/w;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/obf/j;->w:J

    goto :goto_0
.end method

.method private i()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 199
    const-string v0, "doSomeWork"

    invoke-static {v0}, Lcom/google/obf/dz;->a(Ljava/lang/String;)V

    .line 200
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 201
    iget-wide v0, p0, Lcom/google/obf/j;->v:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lcom/google/obf/j;->v:J

    .line 203
    :goto_0
    const/4 v6, 0x1

    .line 204
    const/4 v5, 0x1

    .line 205
    invoke-direct {p0}, Lcom/google/obf/j;->h()V

    .line 206
    const/4 v4, 0x0

    move v14, v4

    move v4, v5

    move v5, v6

    move-wide v6, v0

    move v1, v14

    :goto_1
    iget-object v0, p0, Lcom/google/obf/j;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 207
    iget-object v0, p0, Lcom/google/obf/j;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/x;

    .line 208
    iget-wide v8, p0, Lcom/google/obf/j;->w:J

    iget-wide v10, p0, Lcom/google/obf/j;->u:J

    invoke-virtual {v0, v8, v9, v10, v11}, Lcom/google/obf/x;->b(JJ)V

    .line 209
    if-eqz v5, :cond_3

    invoke-virtual {v0}, Lcom/google/obf/x;->e()Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v5, 0x1

    .line 210
    :goto_2
    invoke-direct {p0, v0}, Lcom/google/obf/j;->a(Lcom/google/obf/x;)Z

    move-result v8

    .line 211
    if-nez v8, :cond_0

    .line 212
    invoke-virtual {v0}, Lcom/google/obf/x;->s()V

    .line 213
    :cond_0
    if-eqz v4, :cond_4

    if-eqz v8, :cond_4

    const/4 v4, 0x1

    .line 214
    :goto_3
    const-wide/16 v8, -0x1

    cmp-long v8, v6, v8

    if-nez v8, :cond_5

    .line 221
    :cond_1
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 202
    :cond_2
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0

    .line 209
    :cond_3
    const/4 v5, 0x0

    goto :goto_2

    .line 213
    :cond_4
    const/4 v4, 0x0

    goto :goto_3

    .line 215
    :cond_5
    invoke-virtual {v0}, Lcom/google/obf/x;->r()J

    move-result-wide v8

    .line 216
    invoke-virtual {v0}, Lcom/google/obf/x;->q()J

    move-result-wide v10

    .line 217
    const-wide/16 v12, -0x1

    cmp-long v0, v10, v12

    if-nez v0, :cond_6

    .line 218
    const-wide/16 v6, -0x1

    goto :goto_4

    .line 219
    :cond_6
    const-wide/16 v12, -0x3

    cmp-long v0, v10, v12

    if-eqz v0, :cond_1

    const-wide/16 v12, -0x1

    cmp-long v0, v8, v12

    if-eqz v0, :cond_7

    const-wide/16 v12, -0x2

    cmp-long v0, v8, v12

    if-eqz v0, :cond_7

    cmp-long v0, v10, v8

    if-gez v0, :cond_1

    .line 220
    :cond_7
    invoke-static {v6, v7, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    goto :goto_4

    .line 222
    :cond_8
    iput-wide v6, p0, Lcom/google/obf/j;->x:J

    .line 223
    if-eqz v5, :cond_e

    iget-wide v0, p0, Lcom/google/obf/j;->v:J

    const-wide/16 v6, -0x1

    cmp-long v0, v0, v6

    if-eqz v0, :cond_9

    iget-wide v0, p0, Lcom/google/obf/j;->v:J

    iget-wide v6, p0, Lcom/google/obf/j;->w:J

    cmp-long v0, v0, v6

    if-gtz v0, :cond_e

    .line 224
    :cond_9
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/obf/j;->a(I)V

    .line 225
    invoke-direct {p0}, Lcom/google/obf/j;->g()V

    .line 234
    :cond_a
    :goto_5
    iget-object v0, p0, Lcom/google/obf/j;->a:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 235
    iget-boolean v0, p0, Lcom/google/obf/j;->o:Z

    if-eqz v0, :cond_b

    iget v0, p0, Lcom/google/obf/j;->q:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_c

    :cond_b
    iget v0, p0, Lcom/google/obf/j;->q:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_10

    .line 236
    :cond_c
    const/4 v1, 0x7

    const-wide/16 v4, 0xa

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/obf/j;->a(IJJ)V

    .line 239
    :cond_d
    :goto_6
    invoke-static {}, Lcom/google/obf/dz;->a()V

    .line 240
    return-void

    .line 226
    :cond_e
    iget v0, p0, Lcom/google/obf/j;->q:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_f

    if-eqz v4, :cond_f

    .line 227
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/obf/j;->a(I)V

    .line 228
    iget-boolean v0, p0, Lcom/google/obf/j;->o:Z

    if-eqz v0, :cond_a

    .line 229
    invoke-direct {p0}, Lcom/google/obf/j;->f()V

    goto :goto_5

    .line 230
    :cond_f
    iget v0, p0, Lcom/google/obf/j;->q:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_a

    if-nez v4, :cond_a

    .line 231
    iget-boolean v0, p0, Lcom/google/obf/j;->o:Z

    iput-boolean v0, p0, Lcom/google/obf/j;->p:Z

    .line 232
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/obf/j;->a(I)V

    .line 233
    invoke-direct {p0}, Lcom/google/obf/j;->g()V

    goto :goto_5

    .line 237
    :cond_10
    iget-object v0, p0, Lcom/google/obf/j;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    .line 238
    const/4 v1, 0x7

    const-wide/16 v4, 0x3e8

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/obf/j;->a(IJJ)V

    goto :goto_6
.end method

.method private j()V
    .locals 1

    .prologue
    .line 268
    invoke-direct {p0}, Lcom/google/obf/j;->l()V

    .line 269
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/obf/j;->a(I)V

    .line 270
    return-void
.end method

.method private k()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 271
    invoke-direct {p0}, Lcom/google/obf/j;->l()V

    .line 272
    invoke-direct {p0, v0}, Lcom/google/obf/j;->a(I)V

    .line 273
    monitor-enter p0

    .line 274
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/obf/j;->n:Z

    .line 275
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 276
    monitor-exit p0

    .line 277
    return-void

    .line 276
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private l()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 278
    iget-object v1, p0, Lcom/google/obf/j;->a:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 279
    iget-object v1, p0, Lcom/google/obf/j;->a:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 280
    iput-boolean v0, p0, Lcom/google/obf/j;->p:Z

    .line 281
    iget-object v1, p0, Lcom/google/obf/j;->d:Lcom/google/obf/w;

    invoke-virtual {v1}, Lcom/google/obf/w;->c()V

    .line 282
    iget-object v1, p0, Lcom/google/obf/j;->k:[Lcom/google/obf/x;

    if-nez v1, :cond_0

    .line 293
    :goto_0
    return-void

    .line 284
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/google/obf/j;->k:[Lcom/google/obf/x;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 285
    iget-object v1, p0, Lcom/google/obf/j;->k:[Lcom/google/obf/x;

    aget-object v1, v1, v0

    .line 286
    invoke-direct {p0, v1}, Lcom/google/obf/j;->b(Lcom/google/obf/x;)V

    .line 287
    invoke-direct {p0, v1}, Lcom/google/obf/j;->c(Lcom/google/obf/x;)V

    .line 288
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 289
    :cond_1
    iput-object v3, p0, Lcom/google/obf/j;->k:[Lcom/google/obf/x;

    .line 290
    iput-object v3, p0, Lcom/google/obf/j;->m:Lcom/google/obf/k;

    .line 291
    iput-object v3, p0, Lcom/google/obf/j;->l:Lcom/google/obf/x;

    .line 292
    iget-object v0, p0, Lcom/google/obf/j;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 4

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/obf/j;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/google/obf/j;->t:J

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/google/obf/j;->w:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    goto :goto_0
.end method

.method public a(J)V
    .locals 5

    .prologue
    .line 28
    iput-wide p1, p0, Lcom/google/obf/j;->t:J

    .line 29
    iget-object v0, p0, Lcom/google/obf/j;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 30
    iget-object v0, p0, Lcom/google/obf/j;->a:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-static {p1, p2}, Lcom/google/obf/ea;->a(J)I

    move-result v2

    .line 31
    invoke-static {p1, p2}, Lcom/google/obf/ea;->b(J)I

    move-result v3

    .line 32
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 34
    return-void
.end method

.method public a(Lcom/google/obf/h$a;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 37
    iget v0, p0, Lcom/google/obf/j;->r:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/j;->r:I

    .line 38
    iget-object v0, p0, Lcom/google/obf/j;->a:Landroid/os/Handler;

    const/16 v1, 0x9

    const/4 v2, 0x0

    invoke-static {p1, p3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-virtual {v0, v1, p2, v2, v3}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 39
    return-void
.end method

.method public a(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 26
    iget-object v2, p0, Lcom/google/obf/j;->a:Landroid/os/Handler;

    const/4 v3, 0x3

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 27
    return-void

    :cond_0
    move v0, v1

    .line 26
    goto :goto_0
.end method

.method public varargs a([Lcom/google/obf/x;)V
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/obf/j;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 25
    return-void
.end method

.method public b()J
    .locals 4

    .prologue
    const-wide/16 v0, -0x1

    .line 21
    iget-wide v2, p0, Lcom/google/obf/j;->v:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    .line 23
    :goto_0
    return-wide v0

    .line 22
    :cond_0
    iget-wide v0, p0, Lcom/google/obf/j;->v:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    goto :goto_0
.end method

.method public declared-synchronized b(Lcom/google/obf/h$a;ILjava/lang/Object;)V
    .locals 5

    .prologue
    .line 40
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/obf/j;->n:Z

    if-eqz v0, :cond_1

    .line 41
    const-string v0, "ExoPlayerImplInternal"

    const/16 v1, 0x39

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Sent message("

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") after release. Message ignored."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    :cond_0
    monitor-exit p0

    return-void

    .line 43
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/google/obf/j;->r:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/obf/j;->r:I

    .line 44
    iget-object v1, p0, Lcom/google/obf/j;->a:Landroid/os/Handler;

    const/16 v2, 0x9

    const/4 v3, 0x0

    invoke-static {p1, p3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-virtual {v1, v2, p2, v3, v4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 45
    :goto_0
    iget v1, p0, Lcom/google/obf/j;->s:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-gt v1, v0, :cond_0

    .line 46
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 48
    :catch_0
    move-exception v1

    .line 49
    :try_start_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/obf/j;->a:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 36
    return-void
.end method

.method public declared-synchronized d()V
    .locals 2

    .prologue
    .line 52
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/obf/j;->n:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 62
    :goto_0
    monitor-exit p0

    return-void

    .line 54
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/obf/j;->a:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 55
    :goto_1
    iget-boolean v0, p0, Lcom/google/obf/j;->n:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    .line 56
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 58
    :catch_0
    move-exception v0

    .line 59
    :try_start_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 52
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 61
    :cond_1
    :try_start_4
    iget-object v0, p0, Lcom/google/obf/j;->b:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 63
    :try_start_0
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 92
    :goto_0
    return v0

    .line 64
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [Lcom/google/obf/x;

    invoke-direct {p0, v0}, Lcom/google/obf/j;->b([Lcom/google/obf/x;)V

    move v0, v1

    .line 65
    goto :goto_0

    .line 66
    :pswitch_1
    invoke-direct {p0}, Lcom/google/obf/j;->e()V

    move v0, v1

    .line 67
    goto :goto_0

    .line 68
    :pswitch_2
    iget v2, p1, Landroid/os/Message;->arg1:I

    if-eqz v2, :cond_0

    move v0, v1

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/obf/j;->b(Z)V

    move v0, v1

    .line 69
    goto :goto_0

    .line 70
    :pswitch_3
    invoke-direct {p0}, Lcom/google/obf/j;->i()V

    move v0, v1

    .line 71
    goto :goto_0

    .line 72
    :pswitch_4
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-static {v0, v2}, Lcom/google/obf/ea;->b(II)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/google/obf/j;->b(J)V

    move v0, v1

    .line 73
    goto :goto_0

    .line 74
    :pswitch_5
    invoke-direct {p0}, Lcom/google/obf/j;->j()V

    move v0, v1

    .line 75
    goto :goto_0

    .line 76
    :pswitch_6
    invoke-direct {p0}, Lcom/google/obf/j;->k()V

    move v0, v1

    .line 77
    goto :goto_0

    .line 78
    :pswitch_7
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-direct {p0, v0, v2}, Lcom/google/obf/j;->a(ILjava/lang/Object;)V

    move v0, v1

    .line 79
    goto :goto_0

    .line 80
    :pswitch_8
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-direct {p0, v0, v2}, Lcom/google/obf/j;->a(II)V
    :try_end_0
    .catch Lcom/google/obf/g; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move v0, v1

    .line 81
    goto :goto_0

    .line 83
    :catch_0
    move-exception v0

    .line 84
    const-string v2, "ExoPlayerImplInternal"

    const-string v3, "Internal track renderer error."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 85
    iget-object v2, p0, Lcom/google/obf/j;->c:Landroid/os/Handler;

    invoke-virtual {v2, v4, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 86
    invoke-direct {p0}, Lcom/google/obf/j;->j()V

    move v0, v1

    .line 87
    goto :goto_0

    .line 88
    :catch_1
    move-exception v0

    .line 89
    const-string v2, "ExoPlayerImplInternal"

    const-string v3, "Internal runtime error."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 90
    iget-object v2, p0, Lcom/google/obf/j;->c:Landroid/os/Handler;

    new-instance v3, Lcom/google/obf/g;

    invoke-direct {v3, v0, v1}, Lcom/google/obf/g;-><init>(Ljava/lang/Throwable;Z)V

    invoke-virtual {v2, v4, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 91
    invoke-direct {p0}, Lcom/google/obf/j;->j()V

    move v0, v1

    .line 92
    goto :goto_0

    .line 63
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_3
        :pswitch_8
        :pswitch_7
    .end packed-switch
.end method
