.class public Lru/cn/api/userdata/replies/Attributes;
.super Ljava/lang/Object;
.source "Attributes.java"


# instance fields
.field public attributedObjects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public classNames:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "class_name"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>([Lru/cn/api/userdata/elementclasses/BaseClass;)V
    .locals 5
    .param p1, "attributes"    # [Lru/cn/api/userdata/elementclasses/BaseClass;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lru/cn/api/userdata/replies/Attributes;->attributedObjects:Ljava/util/List;

    .line 33
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lru/cn/api/userdata/replies/Attributes;->classNames:Ljava/util/List;

    .line 34
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lru/cn/api/userdata/replies/Attributes;->attributedObjects:Ljava/util/List;

    .line 36
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v0, p1, v1

    .line 37
    .local v0, "object":Lru/cn/api/userdata/elementclasses/BaseClass;
    iget-object v3, p0, Lru/cn/api/userdata/replies/Attributes;->classNames:Ljava/util/List;

    invoke-interface {v0}, Lru/cn/api/userdata/elementclasses/BaseClass;->className()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    iget-object v3, p0, Lru/cn/api/userdata/replies/Attributes;->attributedObjects:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 40
    .end local v0    # "object":Lru/cn/api/userdata/elementclasses/BaseClass;
    :cond_0
    return-void
.end method

.method static final synthetic lambda$getAttributes$0$Attributes(Ljava/lang/Class;Ljava/lang/Object;)Z
    .locals 1
    .param p0, "className"    # Ljava/lang/Class;
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 44
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static final synthetic lambda$getAttributes$1$Attributes(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0, "object"    # Ljava/lang/Object;

    .prologue
    .line 45
    return-object p0
.end method


# virtual methods
.method public getAttributes(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 43
    .local p1, "className":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    iget-object v0, p0, Lru/cn/api/userdata/replies/Attributes;->attributedObjects:Ljava/util/List;

    invoke-static {v0}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v0

    new-instance v1, Lru/cn/api/userdata/replies/Attributes$$Lambda$0;

    invoke-direct {v1, p1}, Lru/cn/api/userdata/replies/Attributes$$Lambda$0;-><init>(Ljava/lang/Class;)V

    .line 44
    invoke-virtual {v0, v1}, Lcom/annimon/stream/Stream;->filter(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;

    move-result-object v0

    sget-object v1, Lru/cn/api/userdata/replies/Attributes$$Lambda$1;->$instance:Lcom/annimon/stream/function/Function;

    .line 45
    invoke-virtual {v0, v1}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lcom/annimon/stream/Stream;->findFirst()Lcom/annimon/stream/Optional;

    move-result-object v0

    const/4 v1, 0x0

    .line 47
    invoke-virtual {v0, v1}, Lcom/annimon/stream/Optional;->orElse(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 43
    return-object v0
.end method
