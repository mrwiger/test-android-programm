.class public Lru/cn/notification/commands/CommandMapper;
.super Ljava/lang/Object;
.source "CommandMapper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private command(Lru/cn/notifications/entity/INotification$NewFilmMessage;)Lru/cn/notification/commands/CommandWatchFilm;
    .locals 4
    .param p1, "message"    # Lru/cn/notifications/entity/INotification$NewFilmMessage;

    .prologue
    .line 32
    new-instance v0, Lru/cn/notification/commands/CommandWatchFilm;

    invoke-virtual {p1}, Lru/cn/notifications/entity/INotification$NewFilmMessage;->filmId()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lru/cn/notification/commands/CommandWatchFilm;-><init>(J)V

    return-object v0
.end method

.method private command(Lru/cn/notifications/entity/INotification$NewChannelMessage;)Lru/cn/notification/commands/CommandWatchTV;
    .locals 6
    .param p1, "message"    # Lru/cn/notifications/entity/INotification$NewChannelMessage;

    .prologue
    .line 36
    new-instance v0, Lru/cn/notification/commands/CommandWatchTV;

    invoke-virtual {p1}, Lru/cn/notifications/entity/INotification$NewChannelMessage;->channelId()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    invoke-direct {v0, v2, v3, v4, v5}, Lru/cn/notification/commands/CommandWatchTV;-><init>(JJ)V

    return-object v0
.end method

.method private command(Lru/cn/notifications/entity/INotification$NewTelecastMessage;)Lru/cn/notification/commands/CommandWatchTV;
    .locals 6
    .param p1, "message"    # Lru/cn/notifications/entity/INotification$NewTelecastMessage;

    .prologue
    .line 40
    new-instance v0, Lru/cn/notification/commands/CommandWatchTV;

    invoke-virtual {p1}, Lru/cn/notifications/entity/INotification$NewTelecastMessage;->channelId()J

    move-result-wide v2

    invoke-virtual {p1}, Lru/cn/notifications/entity/INotification$NewTelecastMessage;->telecastId()J

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lru/cn/notification/commands/CommandWatchTV;-><init>(JJ)V

    return-object v0
.end method

.method private command(Lru/cn/notifications/entity/INotification$LaunchAppSection;)Lru/cn/notification/commands/CommandWathAppSection;
    .locals 3
    .param p1, "message"    # Lru/cn/notifications/entity/INotification$LaunchAppSection;

    .prologue
    .line 44
    new-instance v0, Lru/cn/notification/commands/CommandWathAppSection;

    invoke-virtual {p1}, Lru/cn/notifications/entity/INotification$LaunchAppSection;->scheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lru/cn/notifications/entity/INotification$LaunchAppSection;->host()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lru/cn/notification/commands/CommandWathAppSection;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public map(Lru/cn/notifications/entity/INotification;)Lru/cn/notification/commands/Command;
    .locals 1
    .param p1, "notification"    # Lru/cn/notifications/entity/INotification;

    .prologue
    .line 12
    instance-of v0, p1, Lru/cn/notifications/entity/INotification$LaunchAppSection;

    if-eqz v0, :cond_0

    .line 13
    check-cast p1, Lru/cn/notifications/entity/INotification$LaunchAppSection;

    .end local p1    # "notification":Lru/cn/notifications/entity/INotification;
    invoke-direct {p0, p1}, Lru/cn/notification/commands/CommandMapper;->command(Lru/cn/notifications/entity/INotification$LaunchAppSection;)Lru/cn/notification/commands/CommandWathAppSection;

    move-result-object v0

    .line 28
    :goto_0
    return-object v0

    .line 16
    .restart local p1    # "notification":Lru/cn/notifications/entity/INotification;
    :cond_0
    instance-of v0, p1, Lru/cn/notifications/entity/INotification$NewChannelMessage;

    if-eqz v0, :cond_1

    .line 17
    check-cast p1, Lru/cn/notifications/entity/INotification$NewChannelMessage;

    .end local p1    # "notification":Lru/cn/notifications/entity/INotification;
    invoke-direct {p0, p1}, Lru/cn/notification/commands/CommandMapper;->command(Lru/cn/notifications/entity/INotification$NewChannelMessage;)Lru/cn/notification/commands/CommandWatchTV;

    move-result-object v0

    goto :goto_0

    .line 20
    .restart local p1    # "notification":Lru/cn/notifications/entity/INotification;
    :cond_1
    instance-of v0, p1, Lru/cn/notifications/entity/INotification$NewTelecastMessage;

    if-eqz v0, :cond_2

    .line 21
    check-cast p1, Lru/cn/notifications/entity/INotification$NewTelecastMessage;

    .end local p1    # "notification":Lru/cn/notifications/entity/INotification;
    invoke-direct {p0, p1}, Lru/cn/notification/commands/CommandMapper;->command(Lru/cn/notifications/entity/INotification$NewTelecastMessage;)Lru/cn/notification/commands/CommandWatchTV;

    move-result-object v0

    goto :goto_0

    .line 24
    .restart local p1    # "notification":Lru/cn/notifications/entity/INotification;
    :cond_2
    instance-of v0, p1, Lru/cn/notifications/entity/INotification$NewFilmMessage;

    if-eqz v0, :cond_3

    .line 25
    check-cast p1, Lru/cn/notifications/entity/INotification$NewFilmMessage;

    .end local p1    # "notification":Lru/cn/notifications/entity/INotification;
    invoke-direct {p0, p1}, Lru/cn/notification/commands/CommandMapper;->command(Lru/cn/notifications/entity/INotification$NewFilmMessage;)Lru/cn/notification/commands/CommandWatchFilm;

    move-result-object v0

    goto :goto_0

    .line 28
    .restart local p1    # "notification":Lru/cn/notifications/entity/INotification;
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
