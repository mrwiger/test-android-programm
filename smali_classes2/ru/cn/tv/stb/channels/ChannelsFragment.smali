.class public Lru/cn/tv/stb/channels/ChannelsFragment;
.super Lru/cn/view/CustomListFragment;
.source "ChannelsFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/stb/channels/ChannelsFragment$ChannelsFragmentListener;
    }
.end annotation


# instance fields
.field private adapter:Lru/cn/tv/stb/channels/ChannelsAdapter;

.field private channelFragmentListener:Lru/cn/tv/stb/channels/ChannelsFragment$ChannelsFragmentListener;

.field private clickedPosition:I

.field private currentChannelCnId:J

.field private emptyFavouriteText:Landroid/widget/TextView;

.field private listErrorLayout:Landroid/view/View;

.field private progressBar:Landroid/widget/ProgressBar;

.field private viewMode:I

.field private viewModel:Lru/cn/tv/stb/channels/ChannelsViewModel;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Lru/cn/view/CustomListFragment;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->viewMode:I

    .line 38
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->currentChannelCnId:J

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->channelFragmentListener:Lru/cn/tv/stb/channels/ChannelsFragment$ChannelsFragmentListener;

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/stb/channels/ChannelsFragment;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/channels/ChannelsFragment;

    .prologue
    .line 22
    iget-object v0, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->progressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/tv/stb/channels/ChannelsFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/channels/ChannelsFragment;

    .prologue
    .line 22
    iget-object v0, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->listErrorLayout:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/tv/stb/channels/ChannelsFragment;)I
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/channels/ChannelsFragment;

    .prologue
    .line 22
    iget v0, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->viewMode:I

    return v0
.end method

.method static synthetic access$300(Lru/cn/tv/stb/channels/ChannelsFragment;)Lru/cn/tv/stb/channels/ChannelsViewModel;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/channels/ChannelsFragment;

    .prologue
    .line 22
    iget-object v0, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->viewModel:Lru/cn/tv/stb/channels/ChannelsViewModel;

    return-object v0
.end method

.method private channelIdAtPosition(I)J
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 278
    if-gez p1, :cond_0

    .line 279
    const-wide/16 v2, 0x0

    .line 282
    :goto_0
    return-wide v2

    .line 281
    :cond_0
    invoke-virtual {p0}, Lru/cn/tv/stb/channels/ChannelsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/cursor/ChannelCursor;

    .line 282
    .local v0, "c":Lru/cn/api/provider/cursor/ChannelCursor;
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getChannelId()J

    move-result-wide v2

    goto :goto_0
.end method

.method private moveToCurrentChannel(Lru/cn/api/provider/cursor/ChannelCursor;J)Z
    .locals 4
    .param p1, "cursor"    # Lru/cn/api/provider/cursor/ChannelCursor;
    .param p2, "currentCnId"    # J

    .prologue
    .line 265
    invoke-virtual {p1}, Lru/cn/api/provider/cursor/ChannelCursor;->moveToFirst()Z

    .line 266
    :goto_0
    invoke-virtual {p1}, Lru/cn/api/provider/cursor/ChannelCursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_1

    .line 267
    invoke-virtual {p1}, Lru/cn/api/provider/cursor/ChannelCursor;->getChannelId()J

    move-result-wide v0

    .line 268
    .local v0, "channelId":J
    cmp-long v2, p2, v0

    if-nez v2, :cond_0

    .line 269
    const/4 v2, 0x1

    .line 274
    .end local v0    # "channelId":J
    :goto_1
    return v2

    .line 271
    .restart local v0    # "channelId":J
    :cond_0
    invoke-virtual {p1}, Lru/cn/api/provider/cursor/ChannelCursor;->moveToNext()Z

    goto :goto_0

    .line 274
    .end local v0    # "channelId":J
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private setChannels(Landroid/database/Cursor;)V
    .locals 10
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v1, 0x1

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 286
    iget-object v2, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 287
    iget-object v5, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->listErrorLayout:Landroid/view/View;

    if-nez p1, :cond_1

    move v2, v3

    :goto_0
    invoke-virtual {v5, v2}, Landroid/view/View;->setVisibility(I)V

    .line 289
    if-eqz p1, :cond_4

    .line 290
    check-cast p1, Landroid/database/CursorWrapper;

    .end local p1    # "cursor":Landroid/database/Cursor;
    invoke-virtual {p1}, Landroid/database/CursorWrapper;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/cursor/ChannelCursor;

    .line 292
    .local v0, "c":Lru/cn/api/provider/cursor/ChannelCursor;
    iget-object v2, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->adapter:Lru/cn/tv/stb/channels/ChannelsAdapter;

    invoke-virtual {v2, v0}, Lru/cn/tv/stb/channels/ChannelsAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 293
    iget-wide v6, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->currentChannelCnId:J

    const-wide/16 v8, 0x0

    cmp-long v2, v6, v8

    if-lez v2, :cond_0

    .line 294
    iget-wide v6, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->currentChannelCnId:J

    invoke-virtual {p0, v6, v7}, Lru/cn/tv/stb/channels/ChannelsFragment;->setCurrentChannel(J)V

    .line 297
    :cond_0
    iget v2, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->viewMode:I

    if-ne v2, v1, :cond_2

    iget-object v2, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->adapter:Lru/cn/tv/stb/channels/ChannelsAdapter;

    invoke-virtual {v2}, Lru/cn/tv/stb/channels/ChannelsAdapter;->getCount()I

    move-result v2

    if-nez v2, :cond_2

    .line 298
    .local v1, "isEmptyFavorite":Z
    :goto_1
    iget-object v2, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->emptyFavouriteText:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    :goto_2
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 302
    .end local v0    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    .end local v1    # "isEmptyFavorite":Z
    :goto_3
    return-void

    .restart local p1    # "cursor":Landroid/database/Cursor;
    :cond_1
    move v2, v4

    .line 287
    goto :goto_0

    .end local p1    # "cursor":Landroid/database/Cursor;
    .restart local v0    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    :cond_2
    move v1, v3

    .line 297
    goto :goto_1

    .restart local v1    # "isEmptyFavorite":Z
    :cond_3
    move v3, v4

    .line 298
    goto :goto_2

    .line 300
    .end local v0    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    .end local v1    # "isEmptyFavorite":Z
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :cond_4
    iget-object v2, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->adapter:Lru/cn/tv/stb/channels/ChannelsAdapter;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lru/cn/tv/stb/channels/ChannelsAdapter;->changeCursor(Landroid/database/Cursor;)V

    goto :goto_3
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$ChannelsFragment(Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/stb/channels/ChannelsFragment;->setChannels(Landroid/database/Cursor;)V

    return-void
.end method

.method public getClickedPosition()I
    .locals 1

    .prologue
    .line 230
    iget v0, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->clickedPosition:I

    return v0
.end method

.method public getCurrentChannel()J
    .locals 2

    .prologue
    .line 145
    iget-wide v0, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->currentChannelCnId:J

    return-wide v0
.end method

.method protected getItemHeight()I
    .locals 1

    .prologue
    .line 226
    const/16 v0, 0x40

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 53
    invoke-super {p0, p1}, Lru/cn/view/CustomListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 54
    new-instance v0, Lru/cn/tv/stb/channels/ChannelsAdapter;

    invoke-virtual {p0}, Lru/cn/tv/stb/channels/ChannelsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lru/cn/tv/stb/channels/ChannelsAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->adapter:Lru/cn/tv/stb/channels/ChannelsAdapter;

    .line 56
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v0

    const-class v1, Lru/cn/tv/stb/channels/ChannelsViewModel;

    invoke-static {p0, v0, v1}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lru/cn/tv/stb/channels/ChannelsViewModel;

    iput-object v0, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->viewModel:Lru/cn/tv/stb/channels/ChannelsViewModel;

    .line 58
    iget-object v0, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->viewModel:Lru/cn/tv/stb/channels/ChannelsViewModel;

    invoke-virtual {v0}, Lru/cn/tv/stb/channels/ChannelsViewModel;->channels()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/stb/channels/ChannelsFragment$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/channels/ChannelsFragment$$Lambda$0;-><init>(Lru/cn/tv/stb/channels/ChannelsFragment;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 59
    iget-object v0, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->viewModel:Lru/cn/tv/stb/channels/ChannelsViewModel;

    invoke-virtual {p0}, Lru/cn/tv/stb/channels/ChannelsFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lru/cn/domain/KidsObject;->getAgeFilterIsNeed(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->viewMode:I

    invoke-virtual {v0, v1, v2}, Lru/cn/tv/stb/channels/ChannelsViewModel;->setMode(Ljava/lang/String;I)V

    .line 60
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 80
    const v0, 0x7f0c0072

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 9
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 64
    iput p3, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->clickedPosition:I

    .line 65
    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lru/cn/api/provider/cursor/ChannelCursor;

    .line 66
    .local v7, "cursor":Lru/cn/api/provider/cursor/ChannelCursor;
    invoke-virtual {v7}, Lru/cn/api/provider/cursor/ChannelCursor;->getChannelId()J

    move-result-wide v1

    .line 67
    .local v1, "channelId":J
    invoke-virtual {v7}, Lru/cn/api/provider/cursor/ChannelCursor;->getIsDenied()Z

    move-result v3

    .line 68
    .local v3, "isDenied":Z
    invoke-virtual {v7}, Lru/cn/api/provider/cursor/ChannelCursor;->getContractorId()J

    move-result-wide v4

    .line 69
    .local v4, "contractorId":J
    invoke-virtual {v7}, Lru/cn/api/provider/cursor/ChannelCursor;->allowPurchase()Z

    move-result v6

    .line 71
    .local v6, "allowPurchase":Z
    invoke-virtual {p0}, Lru/cn/tv/stb/channels/ChannelsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v8, 0x0

    invoke-virtual {v0, p3, v8}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 73
    iget-object v0, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->channelFragmentListener:Lru/cn/tv/stb/channels/ChannelsFragment$ChannelsFragmentListener;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->channelFragmentListener:Lru/cn/tv/stb/channels/ChannelsFragment$ChannelsFragmentListener;

    invoke-interface/range {v0 .. v6}, Lru/cn/tv/stb/channels/ChannelsFragment$ChannelsFragmentListener;->onChannelSelected(JZJZ)V

    .line 76
    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 85
    invoke-super {p0, p1, p2}, Lru/cn/view/CustomListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 87
    const v2, 0x7f09017e

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 88
    .local v1, "repeatButton":Landroid/view/View;
    new-instance v2, Lru/cn/tv/stb/channels/ChannelsFragment$1;

    invoke-direct {v2, p0}, Lru/cn/tv/stb/channels/ChannelsFragment$1;-><init>(Lru/cn/tv/stb/channels/ChannelsFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    const v2, 0x7f090106

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->listErrorLayout:Landroid/view/View;

    .line 99
    const v2, 0x7f090175

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 100
    const v2, 0x7f0900b2

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->emptyFavouriteText:Landroid/widget/TextView;

    .line 102
    iget-object v2, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->adapter:Lru/cn/tv/stb/channels/ChannelsAdapter;

    invoke-virtual {p0, v2}, Lru/cn/tv/stb/channels/ChannelsFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 103
    iget-object v2, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->listErrorLayout:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->isShown()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->adapter:Lru/cn/tv/stb/channels/ChannelsAdapter;

    invoke-virtual {v2}, Lru/cn/tv/stb/channels/ChannelsAdapter;->getCount()I

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    .line 104
    .local v0, "isLoading":Z
    :goto_0
    iget-object v2, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->progressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 105
    return-void

    .end local v0    # "isLoading":Z
    :cond_0
    move v0, v3

    .line 103
    goto :goto_0

    .line 104
    .restart local v0    # "isLoading":Z
    :cond_1
    const/16 v3, 0x8

    goto :goto_1
.end method

.method public selectNext()Z
    .locals 4

    .prologue
    .line 251
    const/4 v2, -0x1

    .line 252
    .local v2, "position":I
    invoke-super {p0}, Lru/cn/view/CustomListFragment;->selectNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 253
    invoke-virtual {p0}, Lru/cn/tv/stb/channels/ChannelsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v2

    .line 258
    :cond_0
    :goto_0
    invoke-direct {p0, v2}, Lru/cn/tv/stb/channels/ChannelsFragment;->channelIdAtPosition(I)J

    move-result-wide v0

    .line 259
    .local v0, "channelId":J
    invoke-virtual {p0, v0, v1}, Lru/cn/tv/stb/channels/ChannelsFragment;->setCurrentChannel(J)V

    .line 261
    const/4 v3, 0x1

    return v3

    .line 254
    .end local v0    # "channelId":J
    :cond_1
    iget-object v3, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->adapter:Lru/cn/tv/stb/channels/ChannelsAdapter;

    invoke-virtual {v3}, Lru/cn/tv/stb/channels/ChannelsAdapter;->getCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 255
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public selectPrev()Z
    .locals 4

    .prologue
    .line 235
    const/4 v2, -0x1

    .line 236
    .local v2, "position":I
    invoke-super {p0}, Lru/cn/view/CustomListFragment;->selectPrev()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 237
    invoke-virtual {p0}, Lru/cn/tv/stb/channels/ChannelsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v2

    .line 243
    :cond_0
    :goto_0
    invoke-direct {p0, v2}, Lru/cn/tv/stb/channels/ChannelsFragment;->channelIdAtPosition(I)J

    move-result-wide v0

    .line 244
    .local v0, "channelId":J
    invoke-virtual {p0, v0, v1}, Lru/cn/tv/stb/channels/ChannelsFragment;->setCurrentChannel(J)V

    .line 246
    const/4 v3, 0x1

    return v3

    .line 239
    .end local v0    # "channelId":J
    :cond_1
    iget-object v3, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->adapter:Lru/cn/tv/stb/channels/ChannelsAdapter;

    invoke-virtual {v3}, Lru/cn/tv/stb/channels/ChannelsAdapter;->getCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 240
    invoke-virtual {p0}, Lru/cn/tv/stb/channels/ChannelsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getCount()I

    move-result v3

    add-int/lit8 v2, v3, -0x1

    goto :goto_0
.end method

.method public setCurrentChannel(J)V
    .locals 7
    .param p1, "cnId"    # J

    .prologue
    .line 128
    iput-wide p1, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->currentChannelCnId:J

    .line 129
    iget-object v2, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->adapter:Lru/cn/tv/stb/channels/ChannelsAdapter;

    invoke-virtual {v2}, Lru/cn/tv/stb/channels/ChannelsAdapter;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 130
    iget-object v2, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->adapter:Lru/cn/tv/stb/channels/ChannelsAdapter;

    invoke-virtual {v2}, Lru/cn/tv/stb/channels/ChannelsAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/cursor/ChannelCursor;

    .line 131
    .local v0, "c":Lru/cn/api/provider/cursor/ChannelCursor;
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->moveToFirst()Z

    .line 132
    :goto_0
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_0

    .line 133
    iget-wide v2, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->currentChannelCnId:J

    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getChannelId()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 134
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getPosition()I

    move-result v1

    .line 135
    .local v1, "position":I
    invoke-virtual {p0, v1}, Lru/cn/tv/stb/channels/ChannelsFragment;->selectPosition(I)V

    .line 142
    .end local v0    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    .end local v1    # "position":I
    :cond_0
    return-void

    .line 139
    .restart local v0    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    :cond_1
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->moveToNext()Z

    goto :goto_0
.end method

.method public setListener(Lru/cn/tv/stb/channels/ChannelsFragment$ChannelsFragmentListener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/tv/stb/channels/ChannelsFragment$ChannelsFragmentListener;

    .prologue
    .line 124
    iput-object p1, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->channelFragmentListener:Lru/cn/tv/stb/channels/ChannelsFragment$ChannelsFragmentListener;

    .line 125
    return-void
.end method

.method public setViewMode(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    const/16 v2, 0x8

    .line 108
    iget v0, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->viewMode:I

    if-ne p1, v0, :cond_1

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    iput p1, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->viewMode:I

    .line 113
    invoke-virtual {p0}, Lru/cn/tv/stb/channels/ChannelsFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->listErrorLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 115
    iget-object v0, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->adapter:Lru/cn/tv/stb/channels/ChannelsAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lru/cn/tv/stb/channels/ChannelsAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 117
    iget-object v0, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->emptyFavouriteText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 118
    iget-object v0, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->progressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 119
    iget-object v0, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->viewModel:Lru/cn/tv/stb/channels/ChannelsViewModel;

    invoke-virtual {p0}, Lru/cn/tv/stb/channels/ChannelsFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lru/cn/domain/KidsObject;->getAgeFilterIsNeed(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lru/cn/tv/stb/channels/ChannelsViewModel;->setMode(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public switchToNextChannel(J)I
    .locals 9
    .param p1, "currentCnId"    # J

    .prologue
    const/4 v4, -0x1

    .line 149
    iget-object v5, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->adapter:Lru/cn/tv/stb/channels/ChannelsAdapter;

    invoke-virtual {v5}, Lru/cn/tv/stb/channels/ChannelsAdapter;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 183
    :cond_0
    :goto_0
    return v4

    .line 152
    :cond_1
    iget-object v5, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->adapter:Lru/cn/tv/stb/channels/ChannelsAdapter;

    invoke-virtual {v5}, Lru/cn/tv/stb/channels/ChannelsAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    check-cast v2, Lru/cn/api/provider/cursor/ChannelCursor;

    .line 153
    .local v2, "cursor":Lru/cn/api/provider/cursor/ChannelCursor;
    invoke-direct {p0, v2, p1, p2}, Lru/cn/tv/stb/channels/ChannelsFragment;->moveToCurrentChannel(Lru/cn/api/provider/cursor/ChannelCursor;J)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 156
    :cond_2
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 157
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->getIsDenied()Z

    move-result v3

    .line 158
    .local v3, "denied":Z
    if-nez v3, :cond_2

    .line 159
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->getChannelId()J

    move-result-wide v0

    .line 160
    .local v0, "channelId":J
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->getNumber()I

    move-result v4

    .line 162
    .local v4, "nextNumber":I
    invoke-virtual {p0, v0, v1}, Lru/cn/tv/stb/channels/ChannelsFragment;->setCurrentChannel(J)V

    goto :goto_0

    .line 168
    .end local v0    # "channelId":J
    .end local v3    # "denied":Z
    .end local v4    # "nextNumber":I
    :cond_3
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->moveToFirst()Z

    .line 169
    :goto_1
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_0

    .line 170
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->getChannelId()J

    move-result-wide v0

    .line 171
    .restart local v0    # "channelId":J
    iget-wide v6, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->currentChannelCnId:J

    cmp-long v5, v0, v6

    if-eqz v5, :cond_0

    .line 174
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->getIsDenied()Z

    move-result v3

    .line 175
    .restart local v3    # "denied":Z
    if-nez v3, :cond_4

    .line 176
    invoke-virtual {p0, v0, v1}, Lru/cn/tv/stb/channels/ChannelsFragment;->setCurrentChannel(J)V

    .line 177
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->getNumber()I

    move-result v4

    goto :goto_0

    .line 180
    :cond_4
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->moveToNext()Z

    goto :goto_1
.end method

.method public switchToPrevChannel(J)I
    .locals 9
    .param p1, "currentCnId"    # J

    .prologue
    const/4 v4, -0x1

    .line 187
    iget-object v5, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->adapter:Lru/cn/tv/stb/channels/ChannelsAdapter;

    invoke-virtual {v5}, Lru/cn/tv/stb/channels/ChannelsAdapter;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 221
    :cond_0
    :goto_0
    return v4

    .line 190
    :cond_1
    iget-object v5, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->adapter:Lru/cn/tv/stb/channels/ChannelsAdapter;

    invoke-virtual {v5}, Lru/cn/tv/stb/channels/ChannelsAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    check-cast v2, Lru/cn/api/provider/cursor/ChannelCursor;

    .line 191
    .local v2, "cursor":Lru/cn/api/provider/cursor/ChannelCursor;
    invoke-direct {p0, v2, p1, p2}, Lru/cn/tv/stb/channels/ChannelsFragment;->moveToCurrentChannel(Lru/cn/api/provider/cursor/ChannelCursor;J)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 194
    :cond_2
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->moveToPrevious()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 195
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->getIsDenied()Z

    move-result v3

    .line 196
    .local v3, "denied":Z
    if-nez v3, :cond_2

    .line 197
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->getChannelId()J

    move-result-wide v0

    .line 198
    .local v0, "channelId":J
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->getNumber()I

    move-result v4

    .line 200
    .local v4, "nextNumber":I
    invoke-virtual {p0, v0, v1}, Lru/cn/tv/stb/channels/ChannelsFragment;->setCurrentChannel(J)V

    goto :goto_0

    .line 206
    .end local v0    # "channelId":J
    .end local v3    # "denied":Z
    .end local v4    # "nextNumber":I
    :cond_3
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->moveToLast()Z

    .line 207
    :goto_1
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_0

    .line 208
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->getChannelId()J

    move-result-wide v0

    .line 209
    .restart local v0    # "channelId":J
    iget-wide v6, p0, Lru/cn/tv/stb/channels/ChannelsFragment;->currentChannelCnId:J

    cmp-long v5, v0, v6

    if-eqz v5, :cond_0

    .line 212
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->getIsDenied()Z

    move-result v3

    .line 213
    .restart local v3    # "denied":Z
    if-nez v3, :cond_4

    .line 214
    invoke-virtual {p0, v0, v1}, Lru/cn/tv/stb/channels/ChannelsFragment;->setCurrentChannel(J)V

    .line 215
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->getNumber()I

    move-result v4

    goto :goto_0

    .line 218
    :cond_4
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->moveToPrevious()Z

    goto :goto_1
.end method
