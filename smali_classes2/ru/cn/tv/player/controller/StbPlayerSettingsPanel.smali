.class public Lru/cn/tv/player/controller/StbPlayerSettingsPanel;
.super Landroid/widget/FrameLayout;
.source "StbPlayerSettingsPanel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;
    }
.end annotation


# instance fields
.field private audio:Landroid/widget/Button;

.field private currentMode:Lru/cn/player/SimplePlayer$FitMode;

.field private fitToHeight:Landroid/widget/ImageButton;

.field private fitToSize:Landroid/widget/ImageButton;

.field private fitToUser:Landroid/widget/ImageButton;

.field private fitToWidth:Landroid/widget/ImageButton;

.field private listener:Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;

.field private subtitle:Landroid/widget/Button;

.field private userFitModeActivated:Z

.field private volume:Landroid/widget/ImageButton;

.field private zoomToast:Landroid/widget/Toast;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->listener:Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    const/4 v2, 0x0

    iput-object v2, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->listener:Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;

    .line 60
    const-string v2, "layout_inflater"

    .line 61
    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 63
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f0c0088

    .line 64
    .local v1, "layout":I
    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 68
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->listener:Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->listener:Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;

    .line 74
    return-void
.end method

.method static synthetic access$002(Lru/cn/tv/player/controller/StbPlayerSettingsPanel;Lru/cn/player/SimplePlayer$FitMode;)Lru/cn/player/SimplePlayer$FitMode;
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/player/controller/StbPlayerSettingsPanel;
    .param p1, "x1"    # Lru/cn/player/SimplePlayer$FitMode;

    .prologue
    .line 22
    iput-object p1, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->currentMode:Lru/cn/player/SimplePlayer$FitMode;

    return-object p1
.end method

.method static synthetic access$100(Lru/cn/tv/player/controller/StbPlayerSettingsPanel;)Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/controller/StbPlayerSettingsPanel;

    .prologue
    .line 22
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->listener:Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/tv/player/controller/StbPlayerSettingsPanel;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/controller/StbPlayerSettingsPanel;

    .prologue
    .line 22
    iget-boolean v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->userFitModeActivated:Z

    return v0
.end method

.method static synthetic access$202(Lru/cn/tv/player/controller/StbPlayerSettingsPanel;Z)Z
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/player/controller/StbPlayerSettingsPanel;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->userFitModeActivated:Z

    return p1
.end method

.method private selectNextSettingsElement()V
    .locals 1

    .prologue
    .line 171
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->userFitModeActivated:Z

    .line 172
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->subtitle:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 173
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->audio:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->audio:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToHeight:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    goto :goto_0

    .line 178
    :cond_2
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->audio:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 179
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToHeight:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    goto :goto_0

    .line 180
    :cond_3
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToHeight:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 181
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToWidth:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    goto :goto_0

    .line 182
    :cond_4
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToWidth:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 183
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToSize:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    goto :goto_0

    .line 184
    :cond_5
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToSize:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 185
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToUser:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    goto :goto_0

    .line 186
    :cond_6
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToUser:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->volume:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    goto :goto_0
.end method

.method private selectPrevSettingsElement()V
    .locals 1

    .prologue
    .line 192
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->userFitModeActivated:Z

    .line 193
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToHeight:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 194
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->audio:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 195
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->audio:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->subtitle:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->subtitle:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    .line 199
    :cond_2
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->audio:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 200
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->subtitle:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->subtitle:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    .line 203
    :cond_3
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToWidth:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 204
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToHeight:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    goto :goto_0

    .line 205
    :cond_4
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToSize:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 206
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToWidth:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    goto :goto_0

    .line 207
    :cond_5
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToUser:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 208
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToSize:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    goto :goto_0

    .line 209
    :cond_6
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->volume:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToUser:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    goto :goto_0
.end method

.method private showZoomToast(Lru/cn/player/SimplePlayer$FitMode;)V
    .locals 8
    .param p1, "fitMode"    # Lru/cn/player/SimplePlayer$FitMode;

    .prologue
    const/4 v7, 0x0

    .line 296
    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->zoomToast:Landroid/widget/Toast;

    if-eqz v4, :cond_0

    .line 297
    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->zoomToast:Landroid/widget/Toast;

    invoke-virtual {v4}, Landroid/widget/Toast;->cancel()V

    .line 300
    :cond_0
    const/4 v0, 0x0

    .line 301
    .local v0, "imageResource":I
    const/4 v2, 0x0

    .line 302
    .local v2, "stringResource":I
    sget-object v4, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_HEIGHT:Lru/cn/player/SimplePlayer$FitMode;

    if-ne p1, v4, :cond_2

    .line 303
    const v0, 0x7f0802bf

    .line 304
    const v2, 0x7f0e00bd

    .line 313
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v2, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    iput-object v4, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->zoomToast:Landroid/widget/Toast;

    .line 315
    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->zoomToast:Landroid/widget/Toast;

    const/16 v5, 0x11

    const/16 v6, 0x96

    invoke-virtual {v4, v5, v7, v6}, Landroid/widget/Toast;->setGravity(III)V

    .line 316
    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->zoomToast:Landroid/widget/Toast;

    invoke-virtual {v4}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 317
    .local v3, "toastContainer":Landroid/widget/LinearLayout;
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 318
    .local v1, "imageView":Landroid/widget/ImageView;
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 319
    invoke-virtual {v3, v1, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 320
    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->zoomToast:Landroid/widget/Toast;

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 321
    return-void

    .line 305
    .end local v1    # "imageView":Landroid/widget/ImageView;
    .end local v3    # "toastContainer":Landroid/widget/LinearLayout;
    :cond_2
    sget-object v4, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_WIDTH:Lru/cn/player/SimplePlayer$FitMode;

    if-ne p1, v4, :cond_3

    .line 306
    const v0, 0x7f0802cb

    .line 307
    const v2, 0x7f0e00c0

    goto :goto_0

    .line 308
    :cond_3
    sget-object v4, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_SIZE:Lru/cn/player/SimplePlayer$FitMode;

    if-ne p1, v4, :cond_1

    .line 309
    const v0, 0x7f0802c3

    .line 310
    const v2, 0x7f0e00bf

    goto :goto_0
.end method


# virtual methods
.method public dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 216
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToUser:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isHovered()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->listener:Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;

    sget-object v1, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_USER:Lru/cn/player/SimplePlayer$FitMode;

    invoke-interface {v0, v1}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;->fitModeChanged(Lru/cn/player/SimplePlayer$FitMode;)V

    .line 222
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 219
    :cond_0
    invoke-direct {p0}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->selectPrevSettingsElement()V

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x1

    .line 227
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_2

    .line 228
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v4

    if-nez v4, :cond_2

    move v2, v3

    .line 229
    .local v2, "uniqueDown":Z
    :goto_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 231
    .local v0, "keyCode":I
    sparse-switch v0, :sswitch_data_0

    .line 284
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v3

    :cond_1
    :goto_1
    return v3

    .line 228
    .end local v0    # "keyCode":I
    .end local v2    # "uniqueDown":Z
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 234
    .restart local v0    # "keyCode":I
    .restart local v2    # "uniqueDown":Z
    :sswitch_0
    if-eqz v2, :cond_0

    iget-boolean v4, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->userFitModeActivated:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToUser:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->isFocused()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 235
    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->listener:Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;

    invoke-interface {v4}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;->zoomIn()V

    goto :goto_1

    .line 242
    :sswitch_1
    if-eqz v2, :cond_0

    iget-boolean v4, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->userFitModeActivated:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToUser:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->isFocused()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 243
    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->listener:Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;

    invoke-interface {v4}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;->zoomOut()V

    goto :goto_1

    .line 249
    :sswitch_2
    if-eqz v2, :cond_1

    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->listener:Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;

    if-eqz v4, :cond_1

    .line 250
    invoke-direct {p0}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->selectPrevSettingsElement()V

    goto :goto_1

    .line 255
    :sswitch_3
    if-eqz v2, :cond_1

    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->listener:Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;

    if-eqz v4, :cond_1

    .line 256
    invoke-direct {p0}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->selectNextSettingsElement()V

    goto :goto_1

    .line 261
    :sswitch_4
    const/4 v1, 0x0

    .line 262
    .local v1, "mode":Lru/cn/player/SimplePlayer$FitMode;
    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->currentMode:Lru/cn/player/SimplePlayer$FitMode;

    sget-object v5, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_HEIGHT:Lru/cn/player/SimplePlayer$FitMode;

    if-ne v4, v5, :cond_4

    .line 263
    sget-object v1, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_WIDTH:Lru/cn/player/SimplePlayer$FitMode;

    .line 271
    :cond_3
    :goto_2
    if-eqz v1, :cond_1

    .line 272
    iput-object v1, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->currentMode:Lru/cn/player/SimplePlayer$FitMode;

    .line 273
    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->listener:Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;

    iget-object v5, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->currentMode:Lru/cn/player/SimplePlayer$FitMode;

    invoke-interface {v4, v5}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;->fitModeChanged(Lru/cn/player/SimplePlayer$FitMode;)V

    .line 274
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->isShown()Z

    move-result v4

    if-nez v4, :cond_1

    .line 275
    invoke-direct {p0, v1}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->showZoomToast(Lru/cn/player/SimplePlayer$FitMode;)V

    goto :goto_1

    .line 264
    :cond_4
    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->currentMode:Lru/cn/player/SimplePlayer$FitMode;

    sget-object v5, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_WIDTH:Lru/cn/player/SimplePlayer$FitMode;

    if-ne v4, v5, :cond_5

    .line 265
    sget-object v1, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_SIZE:Lru/cn/player/SimplePlayer$FitMode;

    goto :goto_2

    .line 266
    :cond_5
    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->currentMode:Lru/cn/player/SimplePlayer$FitMode;

    sget-object v5, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_SIZE:Lru/cn/player/SimplePlayer$FitMode;

    if-eq v4, v5, :cond_6

    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->currentMode:Lru/cn/player/SimplePlayer$FitMode;

    sget-object v5, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_USER:Lru/cn/player/SimplePlayer$FitMode;

    if-ne v4, v5, :cond_3

    .line 268
    :cond_6
    sget-object v1, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_HEIGHT:Lru/cn/player/SimplePlayer$FitMode;

    goto :goto_2

    .line 231
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0xa8 -> :sswitch_4
    .end sparse-switch
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 78
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 80
    const v0, 0x7f0901bd

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->subtitle:Landroid/widget/Button;

    .line 81
    const v0, 0x7f090035

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->audio:Landroid/widget/Button;

    .line 82
    const v0, 0x7f0901f2

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->volume:Landroid/widget/ImageButton;

    .line 83
    const v0, 0x7f0900dc

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToSize:Landroid/widget/ImageButton;

    .line 84
    const v0, 0x7f0900de

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToUser:Landroid/widget/ImageButton;

    .line 85
    const v0, 0x7f0900da

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToHeight:Landroid/widget/ImageButton;

    .line 86
    const v0, 0x7f0900e0

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToWidth:Landroid/widget/ImageButton;

    .line 88
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToHeight:Landroid/widget/ImageButton;

    new-instance v1, Lru/cn/tv/player/controller/StbPlayerSettingsPanel$1;

    invoke-direct {v1, p0}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel$1;-><init>(Lru/cn/tv/player/controller/StbPlayerSettingsPanel;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToWidth:Landroid/widget/ImageButton;

    new-instance v1, Lru/cn/tv/player/controller/StbPlayerSettingsPanel$2;

    invoke-direct {v1, p0}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel$2;-><init>(Lru/cn/tv/player/controller/StbPlayerSettingsPanel;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToSize:Landroid/widget/ImageButton;

    new-instance v1, Lru/cn/tv/player/controller/StbPlayerSettingsPanel$3;

    invoke-direct {v1, p0}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel$3;-><init>(Lru/cn/tv/player/controller/StbPlayerSettingsPanel;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToUser:Landroid/widget/ImageButton;

    new-instance v1, Lru/cn/tv/player/controller/StbPlayerSettingsPanel$4;

    invoke-direct {v1, p0}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel$4;-><init>(Lru/cn/tv/player/controller/StbPlayerSettingsPanel;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->audio:Landroid/widget/Button;

    new-instance v1, Lru/cn/tv/player/controller/StbPlayerSettingsPanel$5;

    invoke-direct {v1, p0}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel$5;-><init>(Lru/cn/tv/player/controller/StbPlayerSettingsPanel;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->subtitle:Landroid/widget/Button;

    new-instance v1, Lru/cn/tv/player/controller/StbPlayerSettingsPanel$6;

    invoke-direct {v1, p0}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel$6;-><init>(Lru/cn/tv/player/controller/StbPlayerSettingsPanel;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->volume:Landroid/widget/ImageButton;

    new-instance v1, Lru/cn/tv/player/controller/StbPlayerSettingsPanel$7;

    invoke-direct {v1, p0}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel$7;-><init>(Lru/cn/tv/player/controller/StbPlayerSettingsPanel;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    return-void
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .locals 2
    .param p1, "direction"    # I
    .param p2, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 146
    sget-object v0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel$8;->$SwitchMap$ru$cn$player$SimplePlayer$FitMode:[I

    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->currentMode:Lru/cn/player/SimplePlayer$FitMode;

    invoke-virtual {v1}, Lru/cn/player/SimplePlayer$FitMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 160
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 148
    :pswitch_0
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToHeight:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    move-result v0

    goto :goto_0

    .line 151
    :pswitch_1
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToWidth:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    move-result v0

    goto :goto_0

    .line 154
    :pswitch_2
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToSize:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    move-result v0

    goto :goto_0

    .line 157
    :pswitch_3
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->fitToUser:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    move-result v0

    goto :goto_0

    .line 146
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setFitMode(Lru/cn/player/SimplePlayer$FitMode;)V
    .locals 1
    .param p1, "mode"    # Lru/cn/player/SimplePlayer$FitMode;

    .prologue
    .line 164
    iput-object p1, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->currentMode:Lru/cn/player/SimplePlayer$FitMode;

    .line 166
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->userFitModeActivated:Z

    .line 167
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->requestFocus()Z

    .line 168
    return-void
.end method

.method public setListener(Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;

    .prologue
    .line 51
    iput-object p1, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->listener:Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;

    .line 52
    return-void
.end method

.method public showAudioTracks(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 292
    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->audio:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 293
    return-void

    .line 292
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public showSubstitles(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 288
    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->subtitle:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 289
    return-void

    .line 288
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
