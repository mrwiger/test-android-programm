.class public abstract Lcom/my/target/et;
.super Landroid/support/v7/widget/RecyclerView;
.source "PromoRecyclerView.java"


# instance fields
.field final dl:Landroid/view/View$OnClickListener;

.field dp:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/e;",
            ">;"
        }
    .end annotation
.end field

.field dq:Lcom/my/target/ee$b;

.field dr:Lcom/my/target/er;

.field private ds:Z


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 65
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    new-instance v0, Lcom/my/target/et$1;

    invoke-direct {v0, p0}, Lcom/my/target/et$1;-><init>(Lcom/my/target/et;)V

    iput-object v0, p0, Lcom/my/target/et;->dl:Landroid/view/View$OnClickListener;

    .line 66
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/my/target/et;->setOverScrollMode(I)V

    .line 67
    return-void
.end method

.method private L()V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/my/target/et;->dq:Lcom/my/target/ee$b;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/my/target/et;->dq:Lcom/my/target/ee$b;

    invoke-direct {p0}, Lcom/my/target/et;->getVisibleCards()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/my/target/ee$b;->b(Ljava/util/List;)V

    .line 145
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/my/target/et;)Z
    .locals 1

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/my/target/et;->ds:Z

    return v0
.end method

.method static synthetic b(Lcom/my/target/et;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/my/target/et;->L()V

    return-void
.end method

.method private getVisibleCards()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 118
    iget-object v1, p0, Lcom/my/target/et;->dp:Ljava/util/List;

    if-nez v1, :cond_1

    .line 136
    :cond_0
    return-object v0

    .line 123
    :cond_1
    invoke-virtual {p0}, Lcom/my/target/et;->getCardLayoutManager()Lcom/my/target/es;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/es;->findFirstCompletelyVisibleItemPosition()I

    move-result v1

    .line 124
    invoke-virtual {p0}, Lcom/my/target/et;->getCardLayoutManager()Lcom/my/target/es;

    move-result-object v2

    invoke-virtual {v2}, Lcom/my/target/es;->findLastCompletelyVisibleItemPosition()I

    move-result v2

    .line 126
    if-gt v1, v2, :cond_0

    if-ltz v1, :cond_0

    iget-object v3, p0, Lcom/my/target/et;->dp:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 131
    :goto_0
    if-gt v1, v2, :cond_0

    .line 133
    iget-object v3, p0, Lcom/my/target/et;->dp:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected abstract a(Landroid/view/View;)V
.end method

.method protected abstract getCardLayoutManager()Lcom/my/target/es;
.end method

.method public onScrollStateChanged(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 87
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->onScrollStateChanged(I)V

    .line 89
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/my/target/et;->ds:Z

    .line 91
    iget-boolean v0, p0, Lcom/my/target/et;->ds:Z

    if-nez v0, :cond_0

    .line 93
    invoke-direct {p0}, Lcom/my/target/et;->L()V

    .line 95
    :cond_0
    return-void

    .line 89
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setCardLayoutManager(Lcom/my/target/es;)V
    .locals 1
    .param p1, "layoutManager"    # Lcom/my/target/es;

    .prologue
    .line 99
    new-instance v0, Lcom/my/target/et$2;

    invoke-direct {v0, p0}, Lcom/my/target/et$2;-><init>(Lcom/my/target/et;)V

    invoke-virtual {p1, v0}, Lcom/my/target/es;->a(Lcom/my/target/es$a;)V

    .line 108
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 109
    return-void
.end method

.method public setOnPromoCardListener(Lcom/my/target/ee$b;)V
    .locals 0
    .param p1, "onPromoClickListener"    # Lcom/my/target/ee$b;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/my/target/et;->dq:Lcom/my/target/ee$b;

    .line 77
    return-void
.end method

.method public setSideSlidesMargins(I)V
    .locals 1
    .param p1, "sideSlidesMargins"    # I

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/my/target/et;->getCardLayoutManager()Lcom/my/target/es;

    move-result-object v0

    .line 1038
    iput p1, v0, Lcom/my/target/es;->dn:I

    .line 82
    return-void
.end method
