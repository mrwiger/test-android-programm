.class public Lcom/yandex/metrica/ConfigurationService;
.super Landroid/app/Service;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/ConfigurationService$a;
    }
.end annotation


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/yandex/metrica/impl/ob/cd;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/yandex/metrica/impl/ob/bu;

.field private c:Lcom/yandex/metrica/impl/ob/by;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/ConfigurationService;->a:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 103
    new-instance v0, Lcom/yandex/metrica/ConfigurationService$a;

    invoke-direct {v0}, Lcom/yandex/metrica/ConfigurationService$a;-><init>()V

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    .line 62
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 64
    const-string v0, "[ConfigurationService:%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/yandex/metrica/ConfigurationService;->getPackageName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 67
    new-instance v0, Lcom/yandex/metrica/impl/ob/bu;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/ob/bu;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/ConfigurationService;->b:Lcom/yandex/metrica/impl/ob/bu;

    .line 68
    invoke-virtual {p0}, Lcom/yandex/metrica/ConfigurationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 69
    new-instance v2, Lcom/yandex/metrica/impl/ob/ca;

    iget-object v0, p0, Lcom/yandex/metrica/ConfigurationService;->b:Lcom/yandex/metrica/impl/ob/bu;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/bu;->a()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    .line 1077
    const/16 v0, 0x1a

    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1087
    new-instance v0, Lcom/yandex/metrica/impl/ob/bv;

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/bv;-><init>(Landroid/content/Context;)V

    .line 69
    :goto_0
    invoke-direct {v2, v1, v3, v0}, Lcom/yandex/metrica/impl/ob/ca;-><init>(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;Lcom/yandex/metrica/impl/ob/cb;)V

    .line 70
    invoke-virtual {p0}, Lcom/yandex/metrica/ConfigurationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/by;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/by;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/ConfigurationService;->c:Lcom/yandex/metrica/impl/ob/by;

    .line 71
    iget-object v0, p0, Lcom/yandex/metrica/ConfigurationService;->c:Lcom/yandex/metrica/impl/ob/by;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/by;->a()Z

    .line 72
    iget-object v0, p0, Lcom/yandex/metrica/ConfigurationService;->a:Ljava/util/Map;

    const-string v1, "com.yandex.metrica.configuration.ACTION_START"

    new-instance v3, Lcom/yandex/metrica/impl/ob/cf;

    invoke-virtual {p0}, Lcom/yandex/metrica/ConfigurationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Lcom/yandex/metrica/impl/ob/cf;-><init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/ca;)V

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    iget-object v0, p0, Lcom/yandex/metrica/ConfigurationService;->a:Ljava/util/Map;

    const-string v1, "com.yandex.metrica.configuration.ACTION_SCHEDULED_START"

    new-instance v3, Lcom/yandex/metrica/impl/ob/ce;

    invoke-virtual {p0}, Lcom/yandex/metrica/ConfigurationService;->getApplicationContext()Landroid/content/Context;

    invoke-direct {v3, v2}, Lcom/yandex/metrica/impl/ob/ce;-><init>(Lcom/yandex/metrica/impl/ob/ca;)V

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    return-void

    .line 2082
    :cond_0
    new-instance v0, Lcom/yandex/metrica/impl/ob/bt;

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/bt;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 108
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 109
    invoke-static {p0}, Lcom/yandex/metrica/impl/ob/by;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/by;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/by;->b()V

    .line 110
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v1, 0x0

    .line 92
    if-nez p1, :cond_1

    move-object v0, v1

    .line 94
    :goto_0
    iget-object v2, p0, Lcom/yandex/metrica/ConfigurationService;->a:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/cd;

    .line 95
    if-eqz v0, :cond_0

    .line 96
    iget-object v2, p0, Lcom/yandex/metrica/ConfigurationService;->b:Lcom/yandex/metrica/impl/ob/bu;

    if-nez p1, :cond_2

    :goto_1
    invoke-virtual {v2, v0, v1}, Lcom/yandex/metrica/impl/ob/bu;->a(Lcom/yandex/metrica/impl/ob/cd;Landroid/os/Bundle;)V

    .line 98
    :cond_0
    const/4 v0, 0x2

    return v0

    .line 92
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 96
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    goto :goto_1
.end method
