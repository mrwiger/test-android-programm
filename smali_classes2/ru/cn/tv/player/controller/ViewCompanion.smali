.class public Lru/cn/tv/player/controller/ViewCompanion;
.super Ljava/lang/Object;
.source "ViewCompanion.java"

# interfaces
.implements Lru/cn/tv/player/controller/PlayerController$Companion;


# instance fields
.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lru/cn/tv/player/controller/ViewCompanion;->view:Landroid/view/View;

    .line 12
    return-void
.end method


# virtual methods
.method public hide()V
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lru/cn/tv/player/controller/ViewCompanion;->view:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 22
    return-void
.end method

.method public show()V
    .locals 2

    .prologue
    .line 16
    iget-object v0, p0, Lru/cn/tv/player/controller/ViewCompanion;->view:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 17
    return-void
.end method
