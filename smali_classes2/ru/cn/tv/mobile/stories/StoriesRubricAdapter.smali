.class public Lru/cn/tv/mobile/stories/StoriesRubricAdapter;
.super Landroid/support/v4/widget/CursorAdapter;
.source "StoriesRubricAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/mobile/stories/StoriesRubricAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private final itemLayout:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "itemLayout"    # I

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 31
    iput p3, p0, Lru/cn/tv/mobile/stories/StoriesRubricAdapter;->itemLayout:I

    .line 32
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v7, 0x0

    .line 50
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/tv/mobile/stories/StoriesRubricAdapter$ViewHolder;

    .line 51
    .local v1, "holder":Lru/cn/tv/mobile/stories/StoriesRubricAdapter$ViewHolder;
    invoke-static {p3}, Lru/cn/api/provider/TvContentProviderContract$Helper;->getRubricTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    .line 52
    .local v4, "title":Ljava/lang/String;
    const/4 v0, 0x0

    .line 53
    .local v0, "description":Ljava/lang/String;
    invoke-static {p3}, Lru/cn/api/provider/TvContentProviderContract$Helper;->getRubricUiHint(Landroid/database/Cursor;)Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    move-result-object v5

    sget-object v6, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->STORIES:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    if-ne v5, v6, :cond_0

    .line 54
    invoke-static {p3}, Lru/cn/api/provider/TvContentProviderContract$Helper;->getRubricDescription(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 57
    :cond_0
    const-string v5, "image_resource"

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 58
    .local v2, "image":I
    const-string v5, "image_url"

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 59
    .local v3, "imageUrl":Ljava/lang/String;
    iget-object v5, v1, Lru/cn/tv/mobile/stories/StoriesRubricAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    if-eqz v5, :cond_1

    .line 60
    if-lez v2, :cond_3

    .line 61
    iget-object v5, v1, Lru/cn/tv/mobile/stories/StoriesRubricAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 62
    iget-object v5, v1, Lru/cn/tv/mobile/stories/StoriesRubricAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 75
    :cond_1
    :goto_0
    iget-object v5, v1, Lru/cn/tv/mobile/stories/StoriesRubricAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    if-eqz v0, :cond_2

    .line 77
    iget-object v5, v1, Lru/cn/tv/mobile/stories/StoriesRubricAdapter$ViewHolder;->description:Landroid/widget/TextView;

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 78
    iget-object v5, v1, Lru/cn/tv/mobile/stories/StoriesRubricAdapter$ViewHolder;->description:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    :cond_2
    return-void

    .line 63
    :cond_3
    if-eqz v3, :cond_4

    .line 64
    iget-object v5, v1, Lru/cn/tv/mobile/stories/StoriesRubricAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 66
    invoke-static {p2}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v5

    .line 67
    invoke-virtual {v5, v3}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v5

    .line 68
    invoke-virtual {v5}, Lcom/squareup/picasso/RequestCreator;->fit()Lcom/squareup/picasso/RequestCreator;

    move-result-object v5

    iget-object v6, v1, Lru/cn/tv/mobile/stories/StoriesRubricAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    .line 69
    invoke-virtual {v5, v6}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    goto :goto_0

    .line 71
    :cond_4
    iget-object v5, v1, Lru/cn/tv/mobile/stories/StoriesRubricAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 36
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 38
    .local v1, "inflater":Landroid/view/LayoutInflater;
    iget v3, p0, Lru/cn/tv/mobile/stories/StoriesRubricAdapter;->itemLayout:I

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 39
    .local v2, "view":Landroid/view/View;
    new-instance v0, Lru/cn/tv/mobile/stories/StoriesRubricAdapter$ViewHolder;

    invoke-direct {v0, v5}, Lru/cn/tv/mobile/stories/StoriesRubricAdapter$ViewHolder;-><init>(Lru/cn/tv/mobile/stories/StoriesRubricAdapter$1;)V

    .line 40
    .local v0, "holder":Lru/cn/tv/mobile/stories/StoriesRubricAdapter$ViewHolder;
    const v3, 0x7f0900f3

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v0, Lru/cn/tv/mobile/stories/StoriesRubricAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    .line 41
    const v3, 0x7f0901d7

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lru/cn/tv/mobile/stories/StoriesRubricAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 42
    const v3, 0x7f09009b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lru/cn/tv/mobile/stories/StoriesRubricAdapter$ViewHolder;->description:Landroid/widget/TextView;

    .line 44
    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 45
    return-object v2
.end method
