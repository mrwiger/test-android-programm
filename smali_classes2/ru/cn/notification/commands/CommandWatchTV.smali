.class Lru/cn/notification/commands/CommandWatchTV;
.super Ljava/lang/Object;
.source "CommandWatchTV.java"

# interfaces
.implements Lru/cn/notification/commands/Command;


# instance fields
.field private final channelId:J

.field private final mimeType:Ljava/lang/String;

.field private final telecastId:J


# direct methods
.method constructor <init>(JJ)V
    .locals 1
    .param p1, "channelId"    # J
    .param p3, "telecastId"    # J

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const-string v0, "application/inetra.peerstv"

    iput-object v0, p0, Lru/cn/notification/commands/CommandWatchTV;->mimeType:Ljava/lang/String;

    .line 13
    iput-wide p1, p0, Lru/cn/notification/commands/CommandWatchTV;->channelId:J

    .line 14
    iput-wide p3, p0, Lru/cn/notification/commands/CommandWatchTV;->telecastId:J

    .line 15
    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v6, 0x0

    .line 19
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 21
    .local v1, "manager":Landroid/content/pm/PackageManager;
    const-string v2, "ru.cn.tv"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 22
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_2

    .line 23
    const-string v2, "application/inetra.peerstv"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "android.intent.category.LAUNCHER"

    .line 24
    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const v3, 0x10008000

    .line 25
    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 26
    iget-wide v2, p0, Lru/cn/notification/commands/CommandWatchTV;->channelId:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    .line 27
    const-string v2, "channelId"

    iget-wide v4, p0, Lru/cn/notification/commands/CommandWatchTV;->channelId:J

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 29
    :cond_0
    iget-wide v2, p0, Lru/cn/notification/commands/CommandWatchTV;->telecastId:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_1

    .line 30
    const-string v2, "telecastId"

    iget-wide v4, p0, Lru/cn/notification/commands/CommandWatchTV;->telecastId:J

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 32
    :cond_1
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 34
    :cond_2
    return-void
.end method
