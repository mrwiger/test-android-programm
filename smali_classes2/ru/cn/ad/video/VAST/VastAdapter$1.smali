.class Lru/cn/ad/video/VAST/VastAdapter$1;
.super Landroid/os/AsyncTask;
.source "VastAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/ad/video/VAST/VastAdapter;->onLoad()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/ad/video/VAST/VastAdapter;


# direct methods
.method constructor <init>(Lru/cn/ad/video/VAST/VastAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/ad/video/VAST/VastAdapter;

    .prologue
    .line 25
    iput-object p1, p0, Lru/cn/ad/video/VAST/VastAdapter$1;->this$0:Lru/cn/ad/video/VAST/VastAdapter;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lru/cn/ad/video/VAST/VastAdapter$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 29
    iget-object v0, p0, Lru/cn/ad/video/VAST/VastAdapter$1;->this$0:Lru/cn/ad/video/VAST/VastAdapter;

    iget-object v1, p0, Lru/cn/ad/video/VAST/VastAdapter$1;->this$0:Lru/cn/ad/video/VAST/VastAdapter;

    invoke-static {v1}, Lru/cn/ad/video/VAST/VastAdapter;->access$000(Lru/cn/ad/video/VAST/VastAdapter;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lru/cn/ad/video/VAST/VastAdapter$1;->this$0:Lru/cn/ad/video/VAST/VastAdapter;

    invoke-static {v2}, Lru/cn/ad/video/VAST/VastAdapter;->access$100(Lru/cn/ad/video/VAST/VastAdapter;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lru/cn/ad/video/VAST/VastAdapter$1;->this$0:Lru/cn/ad/video/VAST/VastAdapter;

    invoke-static {v3}, Lru/cn/ad/video/VAST/VastAdapter;->access$200(Lru/cn/ad/video/VAST/VastAdapter;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lru/cn/ad/video/VAST/VASTUtils;->resolveVASTAdTagMacros(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lru/cn/ad/video/VAST/VastAdapter;->getVastResult(Landroid/content/Context;Ljava/lang/String;)Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    .line 31
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lru/cn/ad/video/VAST/VastAdapter$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1
    .param p1, "aVoid"    # Ljava/lang/Void;

    .prologue
    .line 36
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 38
    iget-object v0, p0, Lru/cn/ad/video/VAST/VastAdapter$1;->this$0:Lru/cn/ad/video/VAST/VastAdapter;

    invoke-static {v0}, Lru/cn/ad/video/VAST/VastAdapter;->access$300(Lru/cn/ad/video/VAST/VastAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    if-nez v0, :cond_0

    .line 46
    :goto_0
    return-void

    .line 41
    :cond_0
    iget-object v0, p0, Lru/cn/ad/video/VAST/VastAdapter$1;->this$0:Lru/cn/ad/video/VAST/VastAdapter;

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lru/cn/ad/video/VAST/VastAdapter$1;->this$0:Lru/cn/ad/video/VAST/VastAdapter;

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->videoUri:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 42
    iget-object v0, p0, Lru/cn/ad/video/VAST/VastAdapter$1;->this$0:Lru/cn/ad/video/VAST/VastAdapter;

    invoke-static {v0}, Lru/cn/ad/video/VAST/VastAdapter;->access$400(Lru/cn/ad/video/VAST/VastAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdLoaded()V

    goto :goto_0

    .line 44
    :cond_1
    iget-object v0, p0, Lru/cn/ad/video/VAST/VastAdapter$1;->this$0:Lru/cn/ad/video/VAST/VastAdapter;

    invoke-static {v0}, Lru/cn/ad/video/VAST/VastAdapter;->access$500(Lru/cn/ad/video/VAST/VastAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onError()V

    goto :goto_0
.end method
