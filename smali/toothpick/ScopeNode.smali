.class public abstract Ltoothpick/ScopeNode;
.super Ljava/lang/Object;
.source "ScopeNode.java"

# interfaces
.implements Ltoothpick/Scope;


# instance fields
.field protected final childrenScopes:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Object;",
            "Ltoothpick/ScopeNode;",
            ">;"
        }
    .end annotation
.end field

.field protected isOpen:Z

.field protected name:Ljava/lang/Object;

.field protected final parentScopes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltoothpick/ScopeNode;",
            ">;"
        }
    .end annotation
.end field

.field protected final scopeAnnotationClasses:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/Object;

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Ltoothpick/ScopeNode;->childrenScopes:Ljava/util/concurrent/ConcurrentHashMap;

    .line 81
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Ltoothpick/ScopeNode;->parentScopes:Ljava/util/List;

    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltoothpick/ScopeNode;->isOpen:Z

    .line 85
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Ltoothpick/ScopeNode;->scopeAnnotationClasses:Ljava/util/Set;

    .line 88
    if-nez p1, :cond_0

    .line 89
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A scope can\'t have a null name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_0
    iput-object p1, p0, Ltoothpick/ScopeNode;->name:Ljava/lang/Object;

    .line 93
    invoke-direct {p0}, Ltoothpick/ScopeNode;->bindScopeAnnotationIfNameIsScopeAnnotation()V

    .line 94
    return-void
.end method

.method private bindScopeAnnotationIfNameIsScopeAnnotation()V
    .locals 2

    .prologue
    .line 290
    iget-object v0, p0, Ltoothpick/ScopeNode;->name:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Ljava/lang/Class;

    if-ne v0, v1, :cond_0

    const-class v1, Ljava/lang/annotation/Annotation;

    iget-object v0, p0, Ltoothpick/ScopeNode;->name:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Class;

    .line 291
    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltoothpick/ScopeNode;->name:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Class;

    .line 292
    invoke-direct {p0, v0}, Ltoothpick/ScopeNode;->isScopeAnnotationClass(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Ltoothpick/ScopeNode;->name:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {p0, v0}, Ltoothpick/ScopeNode;->bindScopeAnnotation(Ljava/lang/Class;)V

    .line 295
    :cond_0
    return-void
.end method

.method private checkIsAnnotationScope(Ljava/lang/Class;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 298
    .local p1, "scopeAnnotationClass":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/annotation/Annotation;>;"
    invoke-direct {p0, p1}, Ltoothpick/ScopeNode;->isScopeAnnotationClass(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 299
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The annotation %s is not a scope annotation, it is not qualified by javax.inject.Scope."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 300
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 299
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 302
    :cond_0
    return-void
.end method

.method private isScopeAnnotationClass(Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 305
    .local p1, "scopeAnnotationClass":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/annotation/Annotation;>;"
    const-class v0, Ljavax/inject/Scope;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method addChild(Ltoothpick/ScopeNode;)Ltoothpick/ScopeNode;
    .locals 6
    .param p1, "child"    # Ltoothpick/ScopeNode;

    .prologue
    .line 224
    if-nez p1, :cond_0

    .line 225
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Child must be non null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 229
    :cond_0
    invoke-virtual {p1}, Ltoothpick/ScopeNode;->getParentScope()Ltoothpick/ScopeNode;

    move-result-object v0

    .line 230
    .local v0, "parentScope":Ltoothpick/ScopeNode;
    if-ne v0, p0, :cond_1

    .line 249
    .end local p1    # "child":Ltoothpick/ScopeNode;
    :goto_0
    return-object p1

    .line 234
    .restart local p1    # "child":Ltoothpick/ScopeNode;
    :cond_1
    if-eqz v0, :cond_2

    .line 235
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Scope %s already has a parent: %s which is not %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    const/4 v5, 0x2

    aput-object p0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 240
    :cond_2
    iget-object v2, p0, Ltoothpick/ScopeNode;->childrenScopes:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Ltoothpick/ScopeNode;->getName()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3, p1}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltoothpick/ScopeNode;

    .line 241
    .local v1, "scope":Ltoothpick/ScopeNode;
    if-eqz v1, :cond_3

    move-object p1, v1

    .line 242
    goto :goto_0

    .line 247
    :cond_3
    iget-object v2, p1, Ltoothpick/ScopeNode;->parentScopes:Ljava/util/List;

    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 248
    iget-object v2, p1, Ltoothpick/ScopeNode;->parentScopes:Ljava/util/List;

    iget-object v3, p0, Ltoothpick/ScopeNode;->parentScopes:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public bindScopeAnnotation(Ljava/lang/Class;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 182
    .local p1, "scopeAnnotationClass":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/annotation/Annotation;>;"
    invoke-direct {p0, p1}, Ltoothpick/ScopeNode;->checkIsAnnotationScope(Ljava/lang/Class;)V

    .line 183
    const-class v0, Ljavax/inject/Singleton;

    if-ne p1, v0, :cond_0

    .line 184
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The annotation @Singleton is already bound to the root scope of any scope. It can\'t be bound dynamically."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 188
    :cond_0
    iget-object v0, p0, Ltoothpick/ScopeNode;->scopeAnnotationClasses:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 189
    return-void
.end method

.method close()V
    .locals 1

    .prologue
    .line 274
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltoothpick/ScopeNode;->isOpen:Z

    .line 275
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 103
    if-ne p0, p1, :cond_1

    .line 108
    :cond_0
    :goto_0
    return v1

    .line 104
    :cond_1
    if-eqz p1, :cond_2

    instance-of v3, p1, Ltoothpick/Scope;

    if-nez v3, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 106
    check-cast v0, Ltoothpick/ScopeNode;

    .line 108
    .local v0, "scopeNode":Ltoothpick/ScopeNode;
    iget-object v3, p0, Ltoothpick/ScopeNode;->name:Ljava/lang/Object;

    if-eqz v3, :cond_5

    iget-object v3, p0, Ltoothpick/ScopeNode;->name:Ljava/lang/Object;

    iget-object v4, v0, Ltoothpick/ScopeNode;->name:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0

    :cond_5
    iget-object v3, v0, Ltoothpick/ScopeNode;->name:Ljava/lang/Object;

    if-nez v3, :cond_4

    goto :goto_0
.end method

.method getChildrenScopes()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ltoothpick/ScopeNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 212
    iget-object v0, p0, Ltoothpick/ScopeNode;->childrenScopes:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Ltoothpick/ScopeNode;->name:Ljava/lang/Object;

    return-object v0
.end method

.method public bridge synthetic getParentScope()Ltoothpick/Scope;
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0}, Ltoothpick/ScopeNode;->getParentScope()Ltoothpick/ScopeNode;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getParentScope(Ljava/lang/Class;)Ltoothpick/Scope;
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0, p1}, Ltoothpick/ScopeNode;->getParentScope(Ljava/lang/Class;)Ltoothpick/ScopeNode;

    move-result-object v0

    return-object v0
.end method

.method public getParentScope()Ltoothpick/ScopeNode;
    .locals 3

    .prologue
    .line 121
    iget-object v2, p0, Ltoothpick/ScopeNode;->parentScopes:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 122
    .local v1, "parentIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ltoothpick/ScopeNode;>;"
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    .line 123
    .local v0, "hasParent":Z
    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ltoothpick/ScopeNode;

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getParentScope(Ljava/lang/Class;)Ltoothpick/ScopeNode;
    .locals 6
    .param p1, "scopeAnnotationClass"    # Ljava/lang/Class;

    .prologue
    .line 139
    invoke-direct {p0, p1}, Ltoothpick/ScopeNode;->checkIsAnnotationScope(Ljava/lang/Class;)V

    .line 141
    const-class v1, Ljavax/inject/Singleton;

    if-ne p1, v1, :cond_1

    .line 142
    invoke-virtual {p0}, Ltoothpick/ScopeNode;->getRootScope()Ltoothpick/ScopeNode;

    move-result-object v0

    .line 148
    :cond_0
    return-object v0

    .line 145
    :cond_1
    move-object v0, p0

    .line 146
    .local v0, "currentScope":Ltoothpick/ScopeNode;
    :goto_0
    if-eqz v0, :cond_2

    .line 147
    invoke-virtual {v0, p1}, Ltoothpick/ScopeNode;->isBoundToScopeAnnotation(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 150
    invoke-virtual {v0}, Ltoothpick/ScopeNode;->getParentScope()Ltoothpick/ScopeNode;

    move-result-object v0

    goto :goto_0

    .line 152
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "There is no parent scope of %s bound to scope scopeAnnotationClass %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Ltoothpick/ScopeNode;->name:Ljava/lang/Object;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 154
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 152
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method getParentScopesNames()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 278
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 279
    .local v1, "parentScopesNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    iget-object v2, p0, Ltoothpick/ScopeNode;->parentScopes:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltoothpick/ScopeNode;

    .line 280
    .local v0, "parentScope":Ltoothpick/ScopeNode;
    invoke-virtual {v0}, Ltoothpick/ScopeNode;->getName()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 282
    .end local v0    # "parentScope":Ltoothpick/ScopeNode;
    :cond_0
    return-object v1
.end method

.method public bridge synthetic getRootScope()Ltoothpick/Scope;
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0}, Ltoothpick/ScopeNode;->getRootScope()Ltoothpick/ScopeNode;

    move-result-object v0

    return-object v0
.end method

.method public getRootScope()Ltoothpick/ScopeNode;
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Ltoothpick/ScopeNode;->parentScopes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    .end local p0    # "this":Ltoothpick/ScopeNode;
    :goto_0
    return-object p0

    .restart local p0    # "this":Ltoothpick/ScopeNode;
    :cond_0
    iget-object v0, p0, Ltoothpick/ScopeNode;->parentScopes:Ljava/util/List;

    iget-object v1, p0, Ltoothpick/ScopeNode;->parentScopes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltoothpick/ScopeNode;

    move-object p0, v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Ltoothpick/ScopeNode;->name:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public isBoundToScopeAnnotation(Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 193
    .local p1, "scopeAnnotationClass":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/annotation/Annotation;>;"
    const-class v0, Ljavax/inject/Singleton;

    if-ne p1, v0, :cond_0

    .line 194
    iget-object v0, p0, Ltoothpick/ScopeNode;->parentScopes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    .line 197
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ltoothpick/ScopeNode;->scopeAnnotationClasses:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method removeChild(Ltoothpick/ScopeNode;)V
    .locals 7
    .param p1, "child"    # Ltoothpick/ScopeNode;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 253
    if-nez p1, :cond_0

    .line 254
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Child must be non null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 258
    :cond_0
    invoke-virtual {p1}, Ltoothpick/ScopeNode;->getParentScope()Ltoothpick/ScopeNode;

    move-result-object v0

    .line 259
    .local v0, "parentScope":Ltoothpick/ScopeNode;
    if-nez v0, :cond_1

    .line 260
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "The scope has no parent: %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Ltoothpick/ScopeNode;->getName()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 263
    :cond_1
    if-eq v0, p0, :cond_2

    .line 264
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "The scope %s has parent: different of this: %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    .line 265
    invoke-virtual {p1}, Ltoothpick/ScopeNode;->getName()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0}, Ltoothpick/ScopeNode;->getName()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x2

    invoke-virtual {p0}, Ltoothpick/ScopeNode;->getName()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    .line 264
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 268
    :cond_2
    iget-object v1, p0, Ltoothpick/ScopeNode;->childrenScopes:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Ltoothpick/ScopeNode;->getName()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    iget-object v1, p1, Ltoothpick/ScopeNode;->parentScopes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 271
    return-void
.end method

.method protected reset()V
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Ltoothpick/ScopeNode;->scopeAnnotationClasses:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 206
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltoothpick/ScopeNode;->isOpen:Z

    .line 207
    invoke-direct {p0}, Ltoothpick/ScopeNode;->bindScopeAnnotationIfNameIsScopeAnnotation()V

    .line 208
    return-void
.end method
