.class Lcom/google/obf/hf$a;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/h$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/hf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/obf/hf;


# direct methods
.method constructor <init>(Lcom/google/obf/hf;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/obf/hf$a;->a:Lcom/google/obf/hf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 10
    return-void
.end method

.method public a(Lcom/google/obf/g;)V
    .locals 4

    .prologue
    .line 2
    iget-object v0, p0, Lcom/google/obf/hf$a;->a:Lcom/google/obf/hf;

    invoke-static {v0}, Lcom/google/obf/hf;->b(Lcom/google/obf/hf;)V

    .line 3
    const-string v0, "IMA SDK"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xd

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Player Error:"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4
    return-void
.end method

.method public a(ZI)V
    .locals 2

    .prologue
    .line 5
    const/4 v0, 0x5

    if-ne p2, v0, :cond_0

    .line 6
    iget-object v0, p0, Lcom/google/obf/hf$a;->a:Lcom/google/obf/hf;

    invoke-static {v0}, Lcom/google/obf/hf;->d(Lcom/google/obf/hf;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;

    .line 7
    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;->onEnded()V

    goto :goto_0

    .line 9
    :cond_0
    return-void
.end method
