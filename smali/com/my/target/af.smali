.class public Lcom/my/target/af;
.super Ljava/lang/Object;
.source "ClickArea.java"


# static fields
.field public static final cd:Lcom/my/target/af;

.field public static final ce:Lcom/my/target/af;

.field private static final cf:I = 0x1

.field private static final cg:I = 0x2

.field private static final ch:I = 0x4

.field private static final ci:I = 0x8

.field private static final cj:I = 0x10

.field private static final ck:I = 0x20

.field private static final cl:I = 0x40

.field private static final cm:I = 0x80

.field private static final cn:I = 0x100

.field private static final co:I = 0x200

.field private static final cp:I = 0x400

.field private static final cq:I = 0x800

.field private static final cr:I = 0x1000


# instance fields
.field public final cA:Z

.field public final cB:Z

.field public final cC:Z

.field public final cD:Z

.field public final cE:Z

.field private final cF:I

.field public final cs:Z

.field public final ct:Z

.field public final cu:Z

.field public final cv:Z

.field public final cw:Z

.field public final cx:Z

.field public final cy:Z

.field public final cz:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 7
    new-instance v0, Lcom/my/target/af;

    const/16 v1, 0x1000

    invoke-direct {v0, v1}, Lcom/my/target/af;-><init>(I)V

    sput-object v0, Lcom/my/target/af;->cd:Lcom/my/target/af;

    .line 8
    new-instance v0, Lcom/my/target/af;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Lcom/my/target/af;-><init>(I)V

    sput-object v0, Lcom/my/target/af;->ce:Lcom/my/target/af;

    return-void
.end method

.method private constructor <init>(I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput p1, p0, Lcom/my/target/af;->cF:I

    .line 48
    and-int/lit8 v0, p1, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/my/target/af;->cs:Z

    .line 49
    and-int/lit8 v0, p1, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/my/target/af;->ct:Z

    .line 50
    and-int/lit8 v0, p1, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/my/target/af;->cu:Z

    .line 51
    and-int/lit8 v0, p1, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/my/target/af;->cv:Z

    .line 52
    and-int/lit8 v0, p1, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/my/target/af;->cw:Z

    .line 53
    and-int/lit8 v0, p1, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/my/target/af;->cx:Z

    .line 54
    and-int/lit8 v0, p1, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/my/target/af;->cy:Z

    .line 55
    and-int/lit16 v0, p1, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/my/target/af;->cz:Z

    .line 56
    and-int/lit16 v0, p1, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/my/target/af;->cA:Z

    .line 57
    and-int/lit16 v0, p1, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_9

    move v0, v1

    :goto_9
    iput-boolean v0, p0, Lcom/my/target/af;->cB:Z

    .line 58
    and-int/lit16 v0, p1, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_a

    move v0, v1

    :goto_a
    iput-boolean v0, p0, Lcom/my/target/af;->cC:Z

    .line 59
    and-int/lit16 v0, p1, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_b

    move v0, v1

    :goto_b
    iput-boolean v0, p0, Lcom/my/target/af;->cD:Z

    .line 60
    and-int/lit16 v0, p1, 0x1000

    const/16 v3, 0x1000

    if-ne v0, v3, :cond_c

    :goto_c
    iput-boolean v1, p0, Lcom/my/target/af;->cE:Z

    .line 61
    return-void

    :cond_0
    move v0, v2

    .line 48
    goto :goto_0

    :cond_1
    move v0, v2

    .line 49
    goto :goto_1

    :cond_2
    move v0, v2

    .line 50
    goto :goto_2

    :cond_3
    move v0, v2

    .line 51
    goto :goto_3

    :cond_4
    move v0, v2

    .line 52
    goto :goto_4

    :cond_5
    move v0, v2

    .line 53
    goto :goto_5

    :cond_6
    move v0, v2

    .line 54
    goto :goto_6

    :cond_7
    move v0, v2

    .line 55
    goto :goto_7

    :cond_8
    move v0, v2

    .line 56
    goto :goto_8

    :cond_9
    move v0, v2

    .line 57
    goto :goto_9

    :cond_a
    move v0, v2

    .line 58
    goto :goto_a

    :cond_b
    move v0, v2

    .line 59
    goto :goto_b

    :cond_c
    move v1, v2

    .line 60
    goto :goto_c
.end method

.method public static d(I)Lcom/my/target/af;
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/my/target/af;

    invoke-direct {v0, p0}, Lcom/my/target/af;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method public O()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/my/target/af;->cF:I

    return v0
.end method
