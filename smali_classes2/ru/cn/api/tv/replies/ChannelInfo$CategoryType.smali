.class public final enum Lru/cn/api/tv/replies/ChannelInfo$CategoryType;
.super Ljava/lang/Enum;
.source "ChannelInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/api/tv/replies/ChannelInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CategoryType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/api/tv/replies/ChannelInfo$CategoryType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

.field public static final enum DOCUMENTAL:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

.field public static final enum ENTERTAINMENT:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

.field public static final enum FASHION:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

.field public static final enum FOR_KIDS:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

.field public static final enum HD:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

.field public static final enum HUMOROUS:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

.field public static final enum INFORMATIONAL:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

.field public static final enum INTERSECTIONS:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

.field public static final enum MUSIC:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

.field public static final enum PORNOGRAPHIC:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

.field public static final enum SPORT:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 36
    new-instance v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    const-string v1, "PORNOGRAPHIC"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->PORNOGRAPHIC:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    .line 37
    new-instance v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    const-string v1, "DOCUMENTAL"

    invoke-direct {v0, v1, v4, v5}, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->DOCUMENTAL:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    .line 38
    new-instance v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    const-string v1, "HUMOROUS"

    invoke-direct {v0, v1, v5, v6}, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->HUMOROUS:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    .line 39
    new-instance v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    const-string v1, "MUSIC"

    invoke-direct {v0, v1, v6, v7}, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->MUSIC:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    .line 40
    new-instance v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    const-string v1, "INFORMATIONAL"

    invoke-direct {v0, v1, v7, v8}, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->INFORMATIONAL:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    .line 41
    new-instance v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    const-string v1, "ENTERTAINMENT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->ENTERTAINMENT:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    .line 42
    new-instance v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    const-string v1, "FOR_KIDS"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->FOR_KIDS:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    .line 43
    new-instance v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    const-string v1, "SPORT"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->SPORT:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    .line 44
    new-instance v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    const-string v1, "FASHION"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->FASHION:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    .line 45
    new-instance v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    const-string v1, "HD"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->HD:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    .line 46
    new-instance v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    const-string v1, "INTERSECTIONS"

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->INTERSECTIONS:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    .line 35
    const/16 v0, 0xb

    new-array v0, v0, [Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    const/4 v1, 0x0

    sget-object v2, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->PORNOGRAPHIC:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    aput-object v2, v0, v1

    sget-object v1, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->DOCUMENTAL:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    aput-object v1, v0, v4

    sget-object v1, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->HUMOROUS:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    aput-object v1, v0, v5

    sget-object v1, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->MUSIC:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->INFORMATIONAL:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    aput-object v1, v0, v7

    sget-object v1, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->ENTERTAINMENT:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->FOR_KIDS:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->SPORT:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->FASHION:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->HD:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->INTERSECTIONS:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    aput-object v2, v0, v1

    sput-object v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->$VALUES:[Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 51
    iput p3, p0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->value:I

    .line 52
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/api/tv/replies/ChannelInfo$CategoryType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 35
    const-class v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    return-object v0
.end method

.method public static values()[Lru/cn/api/tv/replies/ChannelInfo$CategoryType;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->$VALUES:[Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    invoke-virtual {v0}, [Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->value:I

    return v0
.end method
