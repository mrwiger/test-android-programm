.class public Lru/cn/api/provider/cursor/MergeCursorEx;
.super Landroid/database/MergeCursor;
.source "MergeCursorEx.java"


# instance fields
.field private extras:Landroid/os/Bundle;


# direct methods
.method public constructor <init>([Landroid/database/Cursor;)V
    .locals 0
    .param p1, "cursors"    # [Landroid/database/Cursor;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 15
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lru/cn/api/provider/cursor/MergeCursorEx;->extras:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lru/cn/api/provider/cursor/MergeCursorEx;->extras:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    .line 32
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/api/provider/cursor/MergeCursorEx;->extras:Landroid/os/Bundle;

    .line 33
    invoke-super {p0}, Landroid/database/MergeCursor;->close()V

    .line 34
    return-void
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lru/cn/api/provider/cursor/MergeCursorEx;->extras:Landroid/os/Bundle;

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lru/cn/api/provider/cursor/MergeCursorEx;->extras:Landroid/os/Bundle;

    goto :goto_0
.end method

.method public setExtras(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "extras"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "Override"
        }
    .end annotation

    .prologue
    .line 24
    iput-object p1, p0, Lru/cn/api/provider/cursor/MergeCursorEx;->extras:Landroid/os/Bundle;

    .line 25
    return-void
.end method
