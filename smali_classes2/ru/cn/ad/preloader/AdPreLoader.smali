.class public Lru/cn/ad/preloader/AdPreLoader;
.super Ljava/lang/Object;
.source "AdPreLoader.java"

# interfaces
.implements Lru/cn/ad/AdapterLoader$Cache;
.implements Lru/cn/ad/AdsManager$AdapterCache;


# instance fields
.field private final context:Landroid/content/Context;

.field private final failedPlaces:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final handler:Landroid/os/Handler;

.field private lifecycleCallbacks:Landroid/app/Application$ActivityLifecycleCallbacks;

.field private final loadedAdapters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/ad/AdAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final loaders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/ad/preloader/LoadingEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final loadingQueue:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/ad/AdapterLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final precacheData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lru/cn/ad/AdAdapter$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private sessionActive:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 214
    new-instance v1, Lru/cn/ad/preloader/AdPreLoader$2;

    invoke-direct {v1, p0}, Lru/cn/ad/preloader/AdPreLoader$2;-><init>(Lru/cn/ad/preloader/AdPreLoader;)V

    iput-object v1, p0, Lru/cn/ad/preloader/AdPreLoader;->lifecycleCallbacks:Landroid/app/Application$ActivityLifecycleCallbacks;

    .line 42
    iput-object p1, p0, Lru/cn/ad/preloader/AdPreLoader;->context:Landroid/content/Context;

    .line 43
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lru/cn/ad/preloader/AdPreLoader;->handler:Landroid/os/Handler;

    .line 45
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lru/cn/ad/preloader/AdPreLoader;->precacheData:Ljava/util/Map;

    .line 46
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lru/cn/ad/preloader/AdPreLoader;->loaders:Ljava/util/List;

    .line 47
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lru/cn/ad/preloader/AdPreLoader;->loadingQueue:Ljava/util/List;

    .line 48
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lru/cn/ad/preloader/AdPreLoader;->failedPlaces:Ljava/util/List;

    .line 50
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lru/cn/ad/preloader/AdPreLoader;->loadedAdapters:Ljava/util/List;

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    .line 53
    .local v0, "application":Landroid/app/Application;
    iget-object v1, p0, Lru/cn/ad/preloader/AdPreLoader;->lifecycleCallbacks:Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-virtual {v0, v1}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 54
    return-void
.end method

.method static synthetic access$000(Lru/cn/ad/preloader/AdPreLoader;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/preloader/AdPreLoader;

    .prologue
    .line 25
    iget-object v0, p0, Lru/cn/ad/preloader/AdPreLoader;->loadedAdapters:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/ad/preloader/AdPreLoader;Lru/cn/ad/AdapterLoader;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/preloader/AdPreLoader;
    .param p1, "x1"    # Lru/cn/ad/AdapterLoader;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lru/cn/ad/preloader/AdPreLoader;->completeLoading(Lru/cn/ad/AdapterLoader;)V

    return-void
.end method

.method static synthetic access$200(Lru/cn/ad/preloader/AdPreLoader;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/preloader/AdPreLoader;

    .prologue
    .line 25
    iget-object v0, p0, Lru/cn/ad/preloader/AdPreLoader;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/ad/preloader/AdPreLoader;Lru/cn/ad/AdapterLoader;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/preloader/AdPreLoader;
    .param p1, "x1"    # Lru/cn/ad/AdapterLoader;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lru/cn/ad/preloader/AdPreLoader;->continueLoading(Lru/cn/ad/AdapterLoader;)V

    return-void
.end method

.method static synthetic access$400(Lru/cn/ad/preloader/AdPreLoader;Z)V
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/preloader/AdPreLoader;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lru/cn/ad/preloader/AdPreLoader;->sessionActive(Z)V

    return-void
.end method

.method private completeLoading(Lru/cn/ad/AdapterLoader;)V
    .locals 3
    .param p1, "loader"    # Lru/cn/ad/AdapterLoader;

    .prologue
    .line 190
    iget-object v1, p0, Lru/cn/ad/preloader/AdPreLoader;->loadingQueue:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 192
    iget-object v1, p0, Lru/cn/ad/preloader/AdPreLoader;->loadingQueue:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 197
    :goto_0
    return-void

    .line 195
    :cond_0
    iget-object v1, p0, Lru/cn/ad/preloader/AdPreLoader;->loadingQueue:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/ad/AdapterLoader;

    .line 196
    .local v0, "nextLoader":Lru/cn/ad/AdapterLoader;
    invoke-virtual {v0}, Lru/cn/ad/AdapterLoader;->load()V

    goto :goto_0
.end method

.method private continueLoading(Lru/cn/ad/AdapterLoader;)V
    .locals 3
    .param p1, "loader"    # Lru/cn/ad/AdapterLoader;

    .prologue
    .line 179
    iget-object v1, p0, Lru/cn/ad/preloader/AdPreLoader;->loadingQueue:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 180
    iget-object v1, p0, Lru/cn/ad/preloader/AdPreLoader;->loadingQueue:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 183
    :cond_0
    iget-object v1, p0, Lru/cn/ad/preloader/AdPreLoader;->loadingQueue:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/ad/AdapterLoader;

    .line 184
    .local v0, "nextLoader":Lru/cn/ad/AdapterLoader;
    invoke-virtual {v0}, Lru/cn/ad/AdapterLoader;->isLoading()Z

    move-result v1

    if-nez v1, :cond_1

    .line 185
    invoke-virtual {v0}, Lru/cn/ad/AdapterLoader;->load()V

    .line 187
    :cond_1
    return-void
.end method

.method private equivalentNetworks(Lru/cn/api/money_miner/replies/AdSystem;Lru/cn/api/money_miner/replies/AdSystem;)Z
    .locals 2
    .param p1, "first"    # Lru/cn/api/money_miner/replies/AdSystem;
    .param p2, "second"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 264
    iget v0, p1, Lru/cn/api/money_miner/replies/AdSystem;->id:I

    iget v1, p2, Lru/cn/api/money_miner/replies/AdSystem;->id:I

    if-ne v0, v1, :cond_1

    iget v0, p1, Lru/cn/api/money_miner/replies/AdSystem;->type:I

    iget v1, p2, Lru/cn/api/money_miner/replies/AdSystem;->type:I

    if-ne v0, v1, :cond_1

    iget v0, p1, Lru/cn/api/money_miner/replies/AdSystem;->bannerType:I

    iget v1, p2, Lru/cn/api/money_miner/replies/AdSystem;->bannerType:I

    if-ne v0, v1, :cond_1

    iget-object v0, p1, Lru/cn/api/money_miner/replies/AdSystem;->params:Ljava/util/List;

    iget-object v1, p2, Lru/cn/api/money_miner/replies/AdSystem;->params:Ljava/util/List;

    if-eq v0, v1, :cond_0

    iget-object v0, p1, Lru/cn/api/money_miner/replies/AdSystem;->params:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lru/cn/api/money_miner/replies/AdSystem;->params:Ljava/util/List;

    iget-object v1, p2, Lru/cn/api/money_miner/replies/AdSystem;->params:Ljava/util/List;

    .line 268
    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 264
    :goto_0
    return v0

    .line 268
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadAdapters(Lru/cn/api/money_miner/replies/AdPlace;)V
    .locals 5
    .param p1, "place"    # Lru/cn/api/money_miner/replies/AdPlace;

    .prologue
    .line 148
    invoke-direct {p0, p1}, Lru/cn/ad/preloader/AdPreLoader;->uniqueLoaders(Lru/cn/api/money_miner/replies/AdPlace;)Ljava/util/List;

    move-result-object v0

    .line 150
    .local v0, "adapterLoaders":Ljava/util/List;, "Ljava/util/List<Lru/cn/ad/AdapterLoader;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/ad/AdapterLoader;

    .line 151
    .local v2, "loader":Lru/cn/ad/AdapterLoader;
    new-instance v1, Lru/cn/ad/preloader/LoadingEntry;

    invoke-direct {v1, v2}, Lru/cn/ad/preloader/LoadingEntry;-><init>(Lru/cn/ad/AdapterLoader;)V

    .line 152
    .local v1, "entry":Lru/cn/ad/preloader/LoadingEntry;
    invoke-virtual {v2, p0}, Lru/cn/ad/AdapterLoader;->setLoaderCache(Lru/cn/ad/AdapterLoader$Cache;)V

    .line 153
    iget-object v4, p0, Lru/cn/ad/preloader/AdPreLoader;->loaders:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    new-instance v4, Lru/cn/ad/preloader/AdPreLoader$1;

    invoke-direct {v4, p0, v1, v2}, Lru/cn/ad/preloader/AdPreLoader$1;-><init>(Lru/cn/ad/preloader/AdPreLoader;Lru/cn/ad/preloader/LoadingEntry;Lru/cn/ad/AdapterLoader;)V

    invoke-virtual {v2, v4}, Lru/cn/ad/AdapterLoader;->setListener(Lru/cn/ad/AdapterLoader$Listener;)V

    .line 174
    invoke-direct {p0, v2}, Lru/cn/ad/preloader/AdPreLoader;->continueLoading(Lru/cn/ad/AdapterLoader;)V

    goto :goto_0

    .line 176
    .end local v1    # "entry":Lru/cn/ad/preloader/LoadingEntry;
    .end local v2    # "loader":Lru/cn/ad/AdapterLoader;
    :cond_0
    return-void
.end method

.method private loadPlace(Ljava/lang/String;)V
    .locals 2
    .param p1, "placeId"    # Ljava/lang/String;

    .prologue
    .line 118
    sget-object v0, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-instance v1, Lru/cn/ad/preloader/AdPreLoader$$Lambda$0;

    invoke-direct {v1, p0, p1}, Lru/cn/ad/preloader/AdPreLoader$$Lambda$0;-><init>(Lru/cn/ad/preloader/AdPreLoader;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 145
    return-void
.end method

.method private sessionActive(Z)V
    .locals 3
    .param p1, "active"    # Z

    .prologue
    .line 200
    iput-boolean p1, p0, Lru/cn/ad/preloader/AdPreLoader;->sessionActive:Z

    .line 201
    if-nez p1, :cond_1

    .line 202
    iget-object v1, p0, Lru/cn/ad/preloader/AdPreLoader;->handler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 203
    iget-object v1, p0, Lru/cn/ad/preloader/AdPreLoader;->loadingQueue:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 212
    :cond_0
    return-void

    .line 206
    :cond_1
    iget-object v1, p0, Lru/cn/ad/preloader/AdPreLoader;->loaders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/ad/preloader/LoadingEntry;

    .line 207
    .local v0, "entry":Lru/cn/ad/preloader/LoadingEntry;
    invoke-virtual {v0}, Lru/cn/ad/preloader/LoadingEntry;->adapter()Lru/cn/ad/AdAdapter;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lru/cn/ad/preloader/LoadingEntry;->expired()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 208
    :cond_3
    iget-object v2, v0, Lru/cn/ad/preloader/LoadingEntry;->loader:Lru/cn/ad/AdapterLoader;

    invoke-direct {p0, v2}, Lru/cn/ad/preloader/AdPreLoader;->continueLoading(Lru/cn/ad/AdapterLoader;)V

    goto :goto_0
.end method

.method private uniqueLoaders(Lru/cn/api/money_miner/replies/AdPlace;)Ljava/util/List;
    .locals 12
    .param p1, "place"    # Lru/cn/api/money_miner/replies/AdPlace;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/api/money_miner/replies/AdPlace;",
            ")",
            "Ljava/util/List",
            "<",
            "Lru/cn/ad/AdapterLoader;",
            ">;"
        }
    .end annotation

    .prologue
    .line 272
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 273
    .local v8, "predicates":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/money_miner/replies/AdPredicate;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 275
    .local v6, "networksArray":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Lru/cn/api/money_miner/replies/AdSystem;>;>;"
    iget-object v9, p1, Lru/cn/api/money_miner/replies/AdPlace;->networks:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/money_miner/replies/AdSystem;

    .line 276
    .local v0, "adSystem":Lru/cn/api/money_miner/replies/AdSystem;
    iget-object v7, v0, Lru/cn/api/money_miner/replies/AdSystem;->predicate:Lru/cn/api/money_miner/replies/AdPredicate;

    .line 277
    .local v7, "predicate":Lru/cn/api/money_miner/replies/AdPredicate;
    if-nez v7, :cond_1

    .line 278
    new-instance v7, Lru/cn/api/money_miner/replies/AdPredicate;

    .end local v7    # "predicate":Lru/cn/api/money_miner/replies/AdPredicate;
    invoke-direct {v7}, Lru/cn/api/money_miner/replies/AdPredicate;-><init>()V

    .line 281
    .restart local v7    # "predicate":Lru/cn/api/money_miner/replies/AdPredicate;
    :cond_1
    invoke-interface {v8, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 282
    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 283
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v6, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 286
    :cond_2
    invoke-interface {v8, v7}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 287
    .local v3, "index":I
    const/4 v10, -0x1

    if-eq v3, v10, :cond_0

    .line 288
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 289
    .local v1, "adSystems":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/money_miner/replies/AdSystem;>;"
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 293
    .end local v0    # "adSystem":Lru/cn/api/money_miner/replies/AdSystem;
    .end local v1    # "adSystems":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/money_miner/replies/AdSystem;>;"
    .end local v3    # "index":I
    .end local v7    # "predicate":Lru/cn/api/money_miner/replies/AdPredicate;
    :cond_3
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 294
    .local v5, "loaders":Ljava/util/List;, "Ljava/util/List<Lru/cn/ad/AdapterLoader;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 295
    .restart local v1    # "adSystems":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/money_miner/replies/AdSystem;>;"
    iget-object v10, p0, Lru/cn/ad/preloader/AdPreLoader;->precacheData:Ljava/util/Map;

    iget-object v11, p1, Lru/cn/api/money_miner/replies/AdPlace;->placeId:Ljava/lang/String;

    invoke-interface {v10, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/ad/AdAdapter$Factory;

    .line 296
    .local v2, "factory":Lru/cn/ad/AdAdapter$Factory;
    new-instance v4, Lru/cn/ad/AdapterLoader;

    iget-object v10, p1, Lru/cn/api/money_miner/replies/AdPlace;->placeId:Ljava/lang/String;

    invoke-direct {v4, v10, v1, v2}, Lru/cn/ad/AdapterLoader;-><init>(Ljava/lang/String;Ljava/util/List;Lru/cn/ad/AdAdapter$Factory;)V

    .line 297
    .local v4, "loader":Lru/cn/ad/AdapterLoader;
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 300
    .end local v1    # "adSystems":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/money_miner/replies/AdSystem;>;"
    .end local v2    # "factory":Lru/cn/ad/AdAdapter$Factory;
    .end local v4    # "loader":Lru/cn/ad/AdapterLoader;
    :cond_4
    return-object v5
.end method


# virtual methods
.method public cachedAdapter(Lru/cn/api/money_miner/replies/AdSystem;)Lru/cn/ad/AdAdapter;
    .locals 3
    .param p1, "adSystem"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 109
    iget-object v1, p0, Lru/cn/ad/preloader/AdPreLoader;->loadedAdapters:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/ad/AdAdapter;

    .line 110
    .local v0, "adapter":Lru/cn/ad/AdAdapter;
    iget-object v2, v0, Lru/cn/ad/AdAdapter;->adSystem:Lru/cn/api/money_miner/replies/AdSystem;

    invoke-direct {p0, v2, p1}, Lru/cn/ad/preloader/AdPreLoader;->equivalentNetworks(Lru/cn/api/money_miner/replies/AdSystem;Lru/cn/api/money_miner/replies/AdSystem;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 114
    .end local v0    # "adapter":Lru/cn/ad/AdAdapter;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public consume(Lru/cn/ad/AdAdapter;)V
    .locals 3
    .param p1, "consumedAdapter"    # Lru/cn/ad/AdAdapter;

    .prologue
    .line 86
    iget-object v1, p0, Lru/cn/ad/preloader/AdPreLoader;->loadedAdapters:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 88
    iget-object v1, p0, Lru/cn/ad/preloader/AdPreLoader;->loaders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/ad/preloader/LoadingEntry;

    .line 89
    .local v0, "entry":Lru/cn/ad/preloader/LoadingEntry;
    invoke-virtual {v0}, Lru/cn/ad/preloader/LoadingEntry;->adapter()Lru/cn/ad/AdAdapter;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 90
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lru/cn/ad/preloader/LoadingEntry;->setAdapter(Lru/cn/ad/AdAdapter;)V

    .line 91
    iget-object v2, v0, Lru/cn/ad/preloader/LoadingEntry;->loader:Lru/cn/ad/AdapterLoader;

    invoke-direct {p0, v2}, Lru/cn/ad/preloader/AdPreLoader;->continueLoading(Lru/cn/ad/AdapterLoader;)V

    goto :goto_0

    .line 94
    .end local v0    # "entry":Lru/cn/ad/preloader/LoadingEntry;
    :cond_1
    return-void
.end method

.method public getAdapter(Ljava/lang/String;Ljava/util/List;)Lru/cn/ad/AdAdapter;
    .locals 6
    .param p1, "placeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lru/cn/ad/AdAdapter;"
        }
    .end annotation

    .prologue
    .line 62
    .local p2, "metaTags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/4 v0, 0x0

    .line 65
    .local v0, "adapter":Lru/cn/ad/AdAdapter;
    iget-object v3, p0, Lru/cn/ad/preloader/AdPreLoader;->loaders:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/ad/preloader/LoadingEntry;

    .line 66
    .local v1, "entry":Lru/cn/ad/preloader/LoadingEntry;
    iget-object v4, v1, Lru/cn/ad/preloader/LoadingEntry;->loader:Lru/cn/ad/AdapterLoader;

    iget-object v4, v4, Lru/cn/ad/AdapterLoader;->placeId:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 67
    invoke-virtual {v1}, Lru/cn/ad/preloader/LoadingEntry;->adapter()Lru/cn/ad/AdAdapter;

    move-result-object v2

    .line 68
    .local v2, "loadedAdapter":Lru/cn/ad/AdAdapter;
    if-eqz v2, :cond_0

    invoke-virtual {v2, p2}, Lru/cn/ad/AdAdapter;->eligble(Ljava/util/List;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 70
    if-eqz v0, :cond_1

    iget-object v4, v0, Lru/cn/ad/AdAdapter;->adSystem:Lru/cn/api/money_miner/replies/AdSystem;

    iget v4, v4, Lru/cn/api/money_miner/replies/AdSystem;->price:I

    iget-object v5, v2, Lru/cn/ad/AdAdapter;->adSystem:Lru/cn/api/money_miner/replies/AdSystem;

    iget v5, v5, Lru/cn/api/money_miner/replies/AdSystem;->price:I

    if-ge v4, v5, :cond_0

    .line 71
    :cond_1
    move-object v0, v2

    goto :goto_0

    .line 78
    .end local v1    # "entry":Lru/cn/ad/preloader/LoadingEntry;
    .end local v2    # "loadedAdapter":Lru/cn/ad/AdAdapter;
    :cond_2
    if-nez v0, :cond_3

    iget-object v3, p0, Lru/cn/ad/preloader/AdPreLoader;->failedPlaces:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 79
    invoke-direct {p0, p1}, Lru/cn/ad/preloader/AdPreLoader;->loadPlace(Ljava/lang/String;)V

    .line 82
    :cond_3
    return-object v0
.end method

.method public invalidate()V
    .locals 4

    .prologue
    .line 97
    iget-object v2, p0, Lru/cn/ad/preloader/AdPreLoader;->loaders:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 98
    iget-object v2, p0, Lru/cn/ad/preloader/AdPreLoader;->loadingQueue:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 99
    iget-object v2, p0, Lru/cn/ad/preloader/AdPreLoader;->handler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 101
    iget-object v2, p0, Lru/cn/ad/preloader/AdPreLoader;->precacheData:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 102
    .local v0, "keys":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 103
    .local v1, "placeId":Ljava/lang/String;
    invoke-direct {p0, v1}, Lru/cn/ad/preloader/AdPreLoader;->loadPlace(Ljava/lang/String;)V

    goto :goto_0

    .line 105
    .end local v1    # "placeId":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method final synthetic lambda$loadPlace$1$AdPreLoader(Ljava/lang/String;)V
    .locals 6
    .param p1, "placeId"    # Ljava/lang/String;

    .prologue
    .line 119
    const/4 v2, 0x0

    .line 122
    .local v2, "place":Lru/cn/api/money_miner/replies/AdPlace;
    :try_start_0
    iget-object v3, p0, Lru/cn/ad/preloader/AdPreLoader;->context:Landroid/content/Context;

    invoke-static {v3, p1}, Lru/cn/ad/AdsManager;->getPlace(Landroid/content/Context;Ljava/lang/String;)Lru/cn/api/money_miner/replies/AdPlace;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 128
    :goto_0
    move-object v0, v2

    .line 129
    .local v0, "adPlace":Lru/cn/api/money_miner/replies/AdPlace;
    iget-object v3, p0, Lru/cn/ad/preloader/AdPreLoader;->handler:Landroid/os/Handler;

    new-instance v4, Lru/cn/ad/preloader/AdPreLoader$$Lambda$1;

    invoke-direct {v4, p0, v0, p1}, Lru/cn/ad/preloader/AdPreLoader$$Lambda$1;-><init>(Lru/cn/ad/preloader/AdPreLoader;Lru/cn/api/money_miner/replies/AdPlace;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 144
    return-void

    .line 123
    .end local v0    # "adPlace":Lru/cn/api/money_miner/replies/AdPlace;
    :catch_0
    move-exception v1

    .line 124
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "AdPreLoader"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed load place "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method final synthetic lambda$null$0$AdPreLoader(Lru/cn/api/money_miner/replies/AdPlace;Ljava/lang/String;)V
    .locals 1
    .param p1, "adPlace"    # Lru/cn/api/money_miner/replies/AdPlace;
    .param p2, "placeId"    # Ljava/lang/String;

    .prologue
    .line 130
    if-nez p1, :cond_1

    .line 131
    iget-object v0, p0, Lru/cn/ad/preloader/AdPreLoader;->failedPlaces:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 132
    iget-object v0, p0, Lru/cn/ad/preloader/AdPreLoader;->failedPlaces:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    iget-object v0, p1, Lru/cn/api/money_miner/replies/AdPlace;->networks:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lru/cn/ad/preloader/AdPreLoader;->failedPlaces:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 142
    invoke-direct {p0, p1}, Lru/cn/ad/preloader/AdPreLoader;->loadAdapters(Lru/cn/api/money_miner/replies/AdPlace;)V

    goto :goto_0
.end method

.method public preload(Ljava/lang/String;Lru/cn/ad/AdAdapter$Factory;)V
    .locals 1
    .param p1, "placeId"    # Ljava/lang/String;
    .param p2, "factory"    # Lru/cn/ad/AdAdapter$Factory;

    .prologue
    .line 57
    iget-object v0, p0, Lru/cn/ad/preloader/AdPreLoader;->precacheData:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    invoke-direct {p0, p1}, Lru/cn/ad/preloader/AdPreLoader;->loadPlace(Ljava/lang/String;)V

    .line 59
    return-void
.end method
