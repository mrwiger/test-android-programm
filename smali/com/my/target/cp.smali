.class public final Lcom/my/target/cp;
.super Lcom/my/target/d;
.source "NativeAdResponseParser.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/my/target/d",
        "<",
        "Lcom/my/target/cs;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/my/target/d;-><init>()V

    .line 33
    return-void
.end method

.method public static newParser()Lcom/my/target/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/my/target/d",
            "<",
            "Lcom/my/target/cs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    new-instance v0, Lcom/my/target/cp;

    invoke-direct {v0}, Lcom/my/target/cp;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;Lcom/my/target/ae;Lcom/my/target/ak;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/ak;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 23
    check-cast p3, Lcom/my/target/cs;

    .line 1042
    invoke-virtual {p0, p1, p5}, Lcom/my/target/cp;->a(Ljava/lang/String;Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object v0

    .line 1043
    if-eqz v0, :cond_7

    .line 1048
    invoke-virtual {p4}, Lcom/my/target/b;->getFormat()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 1050
    if-eqz v0, :cond_7

    .line 1055
    const-string v1, "banners"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 1057
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-gtz v1, :cond_2

    :cond_0
    move-object p3, v2

    .line 1095
    :cond_1
    :goto_0
    return-object p3

    .line 1062
    :cond_2
    if-nez p3, :cond_3

    .line 1064
    invoke-static {}, Lcom/my/target/cs;->v()Lcom/my/target/cs;

    move-result-object p3

    .line 1066
    :cond_3
    invoke-static {p2, p4, p5}, Lcom/my/target/core/parsers/b;->a(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/b;

    move-result-object v1

    invoke-virtual {v1, v0, p3}, Lcom/my/target/core/parsers/b;->a(Lorg/json/JSONObject;Lcom/my/target/cs;)V

    .line 1068
    invoke-static {p3, p2, p4, p5}, Lcom/my/target/core/parsers/a;->a(Lcom/my/target/cs;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/a;

    move-result-object v4

    .line 1069
    invoke-virtual {p4}, Lcom/my/target/b;->getBannersCount()I

    move-result v1

    .line 1070
    if-lez v1, :cond_5

    .line 1072
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v0

    .line 1073
    if-le v1, v0, :cond_8

    .line 1083
    :goto_1
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v0, :cond_6

    .line 1085
    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 1086
    if-eqz v5, :cond_4

    .line 1088
    invoke-static {}, Lcom/my/target/core/models/banners/a;->newBanner()Lcom/my/target/core/models/banners/a;

    move-result-object v6

    .line 1089
    invoke-virtual {v4, v5, v6}, Lcom/my/target/core/parsers/a;->a(Lorg/json/JSONObject;Lcom/my/target/core/models/banners/a;)V

    .line 1090
    invoke-virtual {p3, v6}, Lcom/my/target/cs;->a(Lcom/my/target/core/models/banners/a;)V

    .line 1083
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1080
    :cond_5
    const/4 v0, 0x1

    goto :goto_1

    .line 1093
    :cond_6
    invoke-virtual {p3}, Lcom/my/target/cs;->getBannersCount()I

    move-result v0

    if-gtz v0, :cond_1

    :cond_7
    move-object p3, v2

    .line 23
    goto :goto_0

    :cond_8
    move v0, v1

    goto :goto_1
.end method
