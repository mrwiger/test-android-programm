.class Lcom/yandex/metrica/impl/ob/fs$a;
.super Lcom/yandex/metrica/impl/ob/fs$b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/impl/ob/fs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/yandex/metrica/impl/ob/fs;


# direct methods
.method constructor <init>(Lcom/yandex/metrica/impl/ob/fs;Landroid/net/Uri;Ljava/net/Socket;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 173
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/fs$a;->a:Lcom/yandex/metrica/impl/ob/fs;

    .line 174
    invoke-direct {p0, p2, p3}, Lcom/yandex/metrica/impl/ob/fs$b;-><init>(Landroid/net/Uri;Ljava/net/Socket;)V

    .line 175
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 179
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fs$a;->b:Landroid/net/Uri;

    const-string v1, "t"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 180
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fs$a;->a:Lcom/yandex/metrica/impl/ob/fs;

    invoke-static {v1}, Lcom/yandex/metrica/impl/ob/fs;->c(Lcom/yandex/metrica/impl/ob/fs;)Lcom/yandex/metrica/impl/ob/fv;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fv;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    const-string v0, "HTTP/1.1 200 OK"

    new-instance v1, Lcom/yandex/metrica/impl/ob/fs$a$1;

    invoke-direct {v1}, Lcom/yandex/metrica/impl/ob/fs$a$1;-><init>()V

    .line 188
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fs$a;->c()[B

    move-result-object v2

    .line 181
    invoke-virtual {p0, v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fs$a;->a(Ljava/lang/String;Ljava/util/Map;[B)V

    .line 192
    :goto_0
    return-void

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fs$a;->a:Lcom/yandex/metrica/impl/ob/fs;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/fs;->b(Lcom/yandex/metrica/impl/ob/fs;)Landroid/content/Context;

    move-result-object v0

    .line 1022
    const-string v1, "20799a27-fa80-4b36-b2db-0f8141f24180"

    invoke-static {v0, v1}, Lcom/yandex/metrica/YandexMetrica;->getReporter(Landroid/content/Context;Ljava/lang/String;)Lcom/yandex/metrica/IReporter;

    move-result-object v0

    .line 190
    const-string v1, "socket_request_with_wrong_token"

    invoke-interface {v0, v1}, Lcom/yandex/metrica/IReporter;->reportEvent(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fs$a;->a:Lcom/yandex/metrica/impl/ob/fs;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/fs;->b(Lcom/yandex/metrica/impl/ob/fs;)Landroid/content/Context;

    move-result-object v0

    .line 3022
    const-string v1, "20799a27-fa80-4b36-b2db-0f8141f24180"

    invoke-static {v0, v1}, Lcom/yandex/metrica/YandexMetrica;->getReporter(Landroid/content/Context;Ljava/lang/String;)Lcom/yandex/metrica/IReporter;

    move-result-object v0

    .line 201
    const-string v1, "socket_io_exception_during_sync"

    invoke-interface {v0, v1, p1}, Lcom/yandex/metrica/IReporter;->reportError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 204
    return-void
.end method

.method protected b()V
    .locals 3

    .prologue
    .line 196
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fs$a;->a:Lcom/yandex/metrica/impl/ob/fs;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/fs;->b(Lcom/yandex/metrica/impl/ob/fs;)Landroid/content/Context;

    move-result-object v0

    .line 2022
    const-string v1, "20799a27-fa80-4b36-b2db-0f8141f24180"

    invoke-static {v0, v1}, Lcom/yandex/metrica/YandexMetrica;->getReporter(Landroid/content/Context;Ljava/lang/String;)Lcom/yandex/metrica/IReporter;

    move-result-object v0

    .line 196
    const-string v1, "socket_sync_succeed"

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/fs$a;->c:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getLocalPort()I

    move-result v2

    invoke-static {v2}, Lcom/yandex/metrica/impl/ob/fs;->b(I)Ljava/util/HashMap;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/yandex/metrica/IReporter;->reportEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 197
    return-void
.end method

.method protected c()[B
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 208
    :try_start_0
    new-instance v0, Lcom/yandex/metrica/impl/utils/b;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/utils/b;-><init>()V

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fs$a;->a:Lcom/yandex/metrica/impl/ob/fs;

    invoke-static {v1}, Lcom/yandex/metrica/impl/ob/fs;->d(Lcom/yandex/metrica/impl/ob/fs;)Lcom/yandex/metrica/impl/ob/ft;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/ft;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/utils/b;->a([B)[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->encode([BI)[B
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 210
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-array v0, v2, [B

    goto :goto_0
.end method
