.class public final enum Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;
.super Ljava/lang/Enum;
.source "SimplePlayerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/player/SimplePlayerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PlaybackMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

.field public static final enum ARCHIVE:Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

.field public static final enum ON_AIR:Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

.field public static final enum TIMESHIFT:Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 73
    new-instance v0, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    const-string v1, "ON_AIR"

    invoke-direct {v0, v1, v2}, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;->ON_AIR:Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    .line 74
    new-instance v0, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    const-string v1, "TIMESHIFT"

    invoke-direct {v0, v1, v3}, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;->TIMESHIFT:Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    .line 75
    new-instance v0, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    const-string v1, "ARCHIVE"

    invoke-direct {v0, v1, v4}, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;->ARCHIVE:Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    .line 72
    const/4 v0, 0x3

    new-array v0, v0, [Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    sget-object v1, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;->ON_AIR:Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    aput-object v1, v0, v2

    sget-object v1, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;->TIMESHIFT:Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    aput-object v1, v0, v3

    sget-object v1, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;->ARCHIVE:Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    aput-object v1, v0, v4

    sput-object v0, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;->$VALUES:[Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 72
    const-class v0, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    return-object v0
.end method

.method public static values()[Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;->$VALUES:[Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    invoke-virtual {v0}, [Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    return-object v0
.end method
