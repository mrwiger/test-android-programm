.class public final enum Lru/cn/api/iptv/replies/MediaLocation$Type;
.super Ljava/lang/Enum;
.source "MediaLocation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/api/iptv/replies/MediaLocation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/api/iptv/replies/MediaLocation$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/api/iptv/replies/MediaLocation$Type;

.field public static final enum http:Lru/cn/api/iptv/replies/MediaLocation$Type;

.field public static final enum magnet:Lru/cn/api/iptv/replies/MediaLocation$Type;

.field public static final enum mcastudp:Lru/cn/api/iptv/replies/MediaLocation$Type;

.field public static final enum rtsp:Lru/cn/api/iptv/replies/MediaLocation$Type;

.field public static final enum undefined:Lru/cn/api/iptv/replies/MediaLocation$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Lru/cn/api/iptv/replies/MediaLocation$Type;

    const-string v1, "undefined"

    invoke-direct {v0, v1, v2}, Lru/cn/api/iptv/replies/MediaLocation$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/iptv/replies/MediaLocation$Type;->undefined:Lru/cn/api/iptv/replies/MediaLocation$Type;

    new-instance v0, Lru/cn/api/iptv/replies/MediaLocation$Type;

    const-string v1, "http"

    invoke-direct {v0, v1, v3}, Lru/cn/api/iptv/replies/MediaLocation$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/iptv/replies/MediaLocation$Type;->http:Lru/cn/api/iptv/replies/MediaLocation$Type;

    new-instance v0, Lru/cn/api/iptv/replies/MediaLocation$Type;

    const-string v1, "magnet"

    invoke-direct {v0, v1, v4}, Lru/cn/api/iptv/replies/MediaLocation$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/iptv/replies/MediaLocation$Type;->magnet:Lru/cn/api/iptv/replies/MediaLocation$Type;

    new-instance v0, Lru/cn/api/iptv/replies/MediaLocation$Type;

    const-string v1, "mcastudp"

    invoke-direct {v0, v1, v5}, Lru/cn/api/iptv/replies/MediaLocation$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/iptv/replies/MediaLocation$Type;->mcastudp:Lru/cn/api/iptv/replies/MediaLocation$Type;

    new-instance v0, Lru/cn/api/iptv/replies/MediaLocation$Type;

    const-string v1, "rtsp"

    invoke-direct {v0, v1, v6}, Lru/cn/api/iptv/replies/MediaLocation$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/iptv/replies/MediaLocation$Type;->rtsp:Lru/cn/api/iptv/replies/MediaLocation$Type;

    .line 7
    const/4 v0, 0x5

    new-array v0, v0, [Lru/cn/api/iptv/replies/MediaLocation$Type;

    sget-object v1, Lru/cn/api/iptv/replies/MediaLocation$Type;->undefined:Lru/cn/api/iptv/replies/MediaLocation$Type;

    aput-object v1, v0, v2

    sget-object v1, Lru/cn/api/iptv/replies/MediaLocation$Type;->http:Lru/cn/api/iptv/replies/MediaLocation$Type;

    aput-object v1, v0, v3

    sget-object v1, Lru/cn/api/iptv/replies/MediaLocation$Type;->magnet:Lru/cn/api/iptv/replies/MediaLocation$Type;

    aput-object v1, v0, v4

    sget-object v1, Lru/cn/api/iptv/replies/MediaLocation$Type;->mcastudp:Lru/cn/api/iptv/replies/MediaLocation$Type;

    aput-object v1, v0, v5

    sget-object v1, Lru/cn/api/iptv/replies/MediaLocation$Type;->rtsp:Lru/cn/api/iptv/replies/MediaLocation$Type;

    aput-object v1, v0, v6

    sput-object v0, Lru/cn/api/iptv/replies/MediaLocation$Type;->$VALUES:[Lru/cn/api/iptv/replies/MediaLocation$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/api/iptv/replies/MediaLocation$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lru/cn/api/iptv/replies/MediaLocation$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/api/iptv/replies/MediaLocation$Type;

    return-object v0
.end method

.method public static values()[Lru/cn/api/iptv/replies/MediaLocation$Type;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lru/cn/api/iptv/replies/MediaLocation$Type;->$VALUES:[Lru/cn/api/iptv/replies/MediaLocation$Type;

    invoke-virtual {v0}, [Lru/cn/api/iptv/replies/MediaLocation$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/api/iptv/replies/MediaLocation$Type;

    return-object v0
.end method
