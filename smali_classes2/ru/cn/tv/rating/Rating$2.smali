.class Lru/cn/tv/rating/Rating$2;
.super Ljava/lang/Object;
.source "Rating.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/rating/Rating;->showRecommendsPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/rating/Rating;

.field final synthetic val$dontShowAgain:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lru/cn/tv/rating/Rating;Landroid/widget/CheckBox;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/rating/Rating;

    .prologue
    .line 97
    iput-object p1, p0, Lru/cn/tv/rating/Rating$2;->this$0:Lru/cn/tv/rating/Rating;

    iput-object p2, p0, Lru/cn/tv/rating/Rating$2;->val$dontShowAgain:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v2, 0x0

    .line 99
    iget-object v0, p0, Lru/cn/tv/rating/Rating$2;->val$dontShowAgain:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lru/cn/tv/rating/Rating$2;->this$0:Lru/cn/tv/rating/Rating;

    invoke-static {v0}, Lru/cn/tv/rating/Rating;->access$000(Lru/cn/tv/rating/Rating;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "needRate"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 104
    :goto_0
    return-void

    .line 102
    :cond_0
    iget-object v0, p0, Lru/cn/tv/rating/Rating$2;->this$0:Lru/cn/tv/rating/Rating;

    invoke-static {v0}, Lru/cn/tv/rating/Rating;->access$000(Lru/cn/tv/rating/Rating;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "started_counter"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method
