.class public final enum Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;
.super Ljava/lang/Enum;
.source "BottomSheetMenuDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OptionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

.field public static final enum AUDIO_TYPE:Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

.field public static final enum COMPLAIN_TYPE:Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

.field public static final enum QUALITY_TYPE:Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

.field public static final enum SUBTITLE_TYPE:Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

.field public static final enum UNKNOWN_TYPE:Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 32
    new-instance v0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    const-string v1, "QUALITY_TYPE"

    invoke-direct {v0, v1, v2}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;->QUALITY_TYPE:Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    .line 33
    new-instance v0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    const-string v1, "AUDIO_TYPE"

    invoke-direct {v0, v1, v3}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;->AUDIO_TYPE:Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    .line 34
    new-instance v0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    const-string v1, "SUBTITLE_TYPE"

    invoke-direct {v0, v1, v4}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;->SUBTITLE_TYPE:Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    .line 35
    new-instance v0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    const-string v1, "COMPLAIN_TYPE"

    invoke-direct {v0, v1, v5}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;->COMPLAIN_TYPE:Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    .line 36
    new-instance v0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    const-string v1, "UNKNOWN_TYPE"

    invoke-direct {v0, v1, v6}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;->UNKNOWN_TYPE:Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    .line 31
    const/4 v0, 0x5

    new-array v0, v0, [Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    sget-object v1, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;->QUALITY_TYPE:Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    aput-object v1, v0, v2

    sget-object v1, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;->AUDIO_TYPE:Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    aput-object v1, v0, v3

    sget-object v1, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;->SUBTITLE_TYPE:Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    aput-object v1, v0, v4

    sget-object v1, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;->COMPLAIN_TYPE:Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    aput-object v1, v0, v5

    sget-object v1, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;->UNKNOWN_TYPE:Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    aput-object v1, v0, v6

    sput-object v0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;->$VALUES:[Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    const-class v0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    return-object v0
.end method

.method public static values()[Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;->$VALUES:[Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    invoke-virtual {v0}, [Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    return-object v0
.end method
