.class final Lru/cn/api/registry/replies/Service$Range;
.super Ljava/lang/Object;
.source "Service.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/api/registry/replies/Service;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Range"
.end annotation


# instance fields
.field public final lower:I

.field public final upper:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1, p1}, Lru/cn/api/registry/replies/Service$Range;-><init>(II)V

    .line 31
    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "lower"    # I
    .param p2, "upper"    # I

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput p1, p0, Lru/cn/api/registry/replies/Service$Range;->lower:I

    .line 35
    iput p2, p0, Lru/cn/api/registry/replies/Service$Range;->upper:I

    .line 36
    return-void
.end method


# virtual methods
.method public isInRange(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 39
    iget v0, p0, Lru/cn/api/registry/replies/Service$Range;->lower:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lru/cn/api/registry/replies/Service$Range;->upper:I

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
