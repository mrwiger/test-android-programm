.class public final Lcom/yandex/metrica/c$b$a;
.super Lcom/yandex/metrica/impl/ob/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/c$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# static fields
.field private static volatile f:[Lcom/yandex/metrica/c$b$a;


# instance fields
.field public b:J

.field public c:J

.field public d:[Lcom/yandex/metrica/c$a;

.field public e:[Lcom/yandex/metrica/c$d;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2867
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/d;-><init>()V

    .line 2868
    invoke-virtual {p0}, Lcom/yandex/metrica/c$b$a;->e()Lcom/yandex/metrica/c$b$a;

    .line 2869
    return-void
.end method

.method public static d()[Lcom/yandex/metrica/c$b$a;
    .locals 2

    .prologue
    .line 2842
    sget-object v0, Lcom/yandex/metrica/c$b$a;->f:[Lcom/yandex/metrica/c$b$a;

    if-nez v0, :cond_1

    .line 2843
    sget-object v1, Lcom/yandex/metrica/impl/ob/c;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2845
    :try_start_0
    sget-object v0, Lcom/yandex/metrica/c$b$a;->f:[Lcom/yandex/metrica/c$b$a;

    if-nez v0, :cond_0

    .line 2846
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/yandex/metrica/c$b$a;

    sput-object v0, Lcom/yandex/metrica/c$b$a;->f:[Lcom/yandex/metrica/c$b$a;

    .line 2848
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2850
    :cond_1
    sget-object v0, Lcom/yandex/metrica/c$b$a;->f:[Lcom/yandex/metrica/c$b$a;

    return-object v0

    .line 2848
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(Lcom/yandex/metrica/impl/ob/b;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2883
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/yandex/metrica/c$b$a;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/yandex/metrica/impl/ob/b;->a(IJ)V

    .line 2884
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/yandex/metrica/c$b$a;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/yandex/metrica/impl/ob/b;->a(IJ)V

    .line 2885
    iget-object v0, p0, Lcom/yandex/metrica/c$b$a;->d:[Lcom/yandex/metrica/c$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/yandex/metrica/c$b$a;->d:[Lcom/yandex/metrica/c$a;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 2886
    :goto_0
    iget-object v2, p0, Lcom/yandex/metrica/c$b$a;->d:[Lcom/yandex/metrica/c$a;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 2887
    iget-object v2, p0, Lcom/yandex/metrica/c$b$a;->d:[Lcom/yandex/metrica/c$a;

    aget-object v2, v2, v0

    .line 2888
    if-eqz v2, :cond_0

    .line 2889
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Lcom/yandex/metrica/impl/ob/b;->a(ILcom/yandex/metrica/impl/ob/d;)V

    .line 2886
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2893
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/c$b$a;->e:[Lcom/yandex/metrica/c$d;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/yandex/metrica/c$b$a;->e:[Lcom/yandex/metrica/c$d;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 2894
    :goto_1
    iget-object v0, p0, Lcom/yandex/metrica/c$b$a;->e:[Lcom/yandex/metrica/c$d;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 2895
    iget-object v0, p0, Lcom/yandex/metrica/c$b$a;->e:[Lcom/yandex/metrica/c$d;

    aget-object v0, v0, v1

    .line 2896
    if-eqz v0, :cond_2

    .line 2897
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/yandex/metrica/impl/ob/b;->a(ILcom/yandex/metrica/impl/ob/d;)V

    .line 2894
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2901
    :cond_3
    invoke-super {p0, p1}, Lcom/yandex/metrica/impl/ob/d;->a(Lcom/yandex/metrica/impl/ob/b;)V

    .line 2902
    return-void
.end method

.method protected c()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2906
    invoke-super {p0}, Lcom/yandex/metrica/impl/ob/d;->c()I

    move-result v0

    .line 2907
    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/yandex/metrica/c$b$a;->b:J

    .line 2908
    invoke-static {v2, v4, v5}, Lcom/yandex/metrica/impl/ob/b;->c(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2909
    const/4 v2, 0x2

    iget-wide v4, p0, Lcom/yandex/metrica/c$b$a;->c:J

    .line 2910
    invoke-static {v2, v4, v5}, Lcom/yandex/metrica/impl/ob/b;->c(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2911
    iget-object v2, p0, Lcom/yandex/metrica/c$b$a;->d:[Lcom/yandex/metrica/c$a;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/yandex/metrica/c$b$a;->d:[Lcom/yandex/metrica/c$a;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 2912
    :goto_0
    iget-object v3, p0, Lcom/yandex/metrica/c$b$a;->d:[Lcom/yandex/metrica/c$a;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 2913
    iget-object v3, p0, Lcom/yandex/metrica/c$b$a;->d:[Lcom/yandex/metrica/c$a;

    aget-object v3, v3, v0

    .line 2914
    if-eqz v3, :cond_0

    .line 2915
    const/4 v4, 0x3

    .line 2916
    invoke-static {v4, v3}, Lcom/yandex/metrica/impl/ob/b;->b(ILcom/yandex/metrica/impl/ob/d;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2912
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 2920
    :cond_2
    iget-object v2, p0, Lcom/yandex/metrica/c$b$a;->e:[Lcom/yandex/metrica/c$d;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/yandex/metrica/c$b$a;->e:[Lcom/yandex/metrica/c$d;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 2921
    :goto_1
    iget-object v2, p0, Lcom/yandex/metrica/c$b$a;->e:[Lcom/yandex/metrica/c$d;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 2922
    iget-object v2, p0, Lcom/yandex/metrica/c$b$a;->e:[Lcom/yandex/metrica/c$d;

    aget-object v2, v2, v1

    .line 2923
    if-eqz v2, :cond_3

    .line 2924
    const/4 v3, 0x4

    .line 2925
    invoke-static {v3, v2}, Lcom/yandex/metrica/impl/ob/b;->b(ILcom/yandex/metrica/impl/ob/d;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2921
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2929
    :cond_4
    return v0
.end method

.method public e()Lcom/yandex/metrica/c$b$a;
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 2872
    iput-wide v0, p0, Lcom/yandex/metrica/c$b$a;->b:J

    .line 2873
    iput-wide v0, p0, Lcom/yandex/metrica/c$b$a;->c:J

    .line 2874
    invoke-static {}, Lcom/yandex/metrica/c$a;->d()[Lcom/yandex/metrica/c$a;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/c$b$a;->d:[Lcom/yandex/metrica/c$a;

    .line 2875
    invoke-static {}, Lcom/yandex/metrica/c$d;->d()[Lcom/yandex/metrica/c$d;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/c$b$a;->e:[Lcom/yandex/metrica/c$d;

    .line 2876
    const/4 v0, -0x1

    iput v0, p0, Lcom/yandex/metrica/c$b$a;->a:I

    .line 2877
    return-object p0
.end method
