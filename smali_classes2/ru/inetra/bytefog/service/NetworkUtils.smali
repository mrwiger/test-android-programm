.class Lru/inetra/bytefog/service/NetworkUtils;
.super Ljava/lang/Object;
.source "NetworkUtils.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static calculateBufferSize([Lru/inetra/bytefog/service/NetworkInterfaceInfo;)I
    .locals 5
    .param p0, "networkInterfaceInfo"    # [Lru/inetra/bytefog/service/NetworkInterfaceInfo;

    .prologue
    .line 51
    const/4 v0, 0x4

    .line 52
    .local v0, "bufferSize":I
    array-length v3, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, p0, v2

    .line 53
    .local v1, "info":Lru/inetra/bytefog/service/NetworkInterfaceInfo;
    add-int/lit8 v0, v0, 0x4

    .line 54
    invoke-virtual {v1}, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v0, v4

    .line 55
    add-int/lit8 v0, v0, 0x4

    .line 56
    add-int/lit8 v0, v0, 0x4

    .line 57
    invoke-virtual {v1}, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->getUnicastAddresses()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x8

    add-int/2addr v0, v4

    .line 58
    add-int/lit8 v0, v0, 0x8

    .line 52
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 60
    .end local v1    # "info":Lru/inetra/bytefog/service/NetworkInterfaceInfo;
    :cond_0
    return v0
.end method

.method private static getInterfaceName(Landroid/net/ConnectivityManager;I)Ljava/lang/String;
    .locals 10
    .param p0, "connectivityManager"    # Landroid/net/ConnectivityManager;
    .param p1, "networkType"    # I

    .prologue
    const/4 v5, 0x0

    .line 144
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 145
    .local v4, "linkProperties":Ljava/lang/Boolean;
    const/4 v2, 0x0

    .line 146
    .local v2, "getLinkProperties":Ljava/lang/reflect/Method;
    const/4 v1, 0x0

    .line 150
    .local v1, "getInterfaceName_":Ljava/lang/reflect/Method;
    :try_start_0
    const-class v5, Landroid/net/ConnectivityManager;

    const-string v6, "getLinkProperties"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 151
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, p0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 152
    .local v4, "linkProperties":Ljava/lang/Object;
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "getInterfaceName"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 153
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    .end local v4    # "linkProperties":Ljava/lang/Object;
    :goto_0
    return-object v3

    .line 154
    :catch_0
    move-exception v0

    .line 155
    .local v0, "e":Ljava/lang/Exception;
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static getNetworkInterfaceInfo(Landroid/content/Context;)[Lru/inetra/bytefog/service/NetworkInterfaceInfo;
    .locals 26
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 64
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 66
    .local v18, "networkInterfaceInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lru/inetra/bytefog/service/NetworkInterfaceInfo;>;"
    :try_start_0
    const-string v22, "wifi"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/net/wifi/WifiManager;

    .line 67
    .local v21, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v13

    .line 68
    .local v13, "netif":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 69
    .local v7, "ifmap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/net/NetworkInterface;>;"
    :goto_0
    invoke-interface {v13}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v22

    if-eqz v22, :cond_0

    .line 70
    invoke-interface {v13}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/net/NetworkInterface;

    .line 71
    .local v16, "networkInterface":Ljava/net/NetworkInterface;
    invoke-virtual/range {v16 .. v16}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v7, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 135
    .end local v7    # "ifmap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/net/NetworkInterface;>;"
    .end local v13    # "netif":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    .end local v16    # "networkInterface":Ljava/net/NetworkInterface;
    .end local v21    # "wifiManager":Landroid/net/wifi/WifiManager;
    :catch_0
    move-exception v22

    .line 138
    :goto_1
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v22

    move/from16 v0, v22

    new-array v0, v0, [Lru/inetra/bytefog/service/NetworkInterfaceInfo;

    move-object/from16 v17, v0

    .line 139
    .local v17, "networkInterfaceInfoArray":[Lru/inetra/bytefog/service/NetworkInterfaceInfo;
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v17

    .end local v17    # "networkInterfaceInfoArray":[Lru/inetra/bytefog/service/NetworkInterfaceInfo;
    check-cast v17, [Lru/inetra/bytefog/service/NetworkInterfaceInfo;

    .line 140
    .restart local v17    # "networkInterfaceInfoArray":[Lru/inetra/bytefog/service/NetworkInterfaceInfo;
    return-object v17

    .line 74
    .end local v17    # "networkInterfaceInfoArray":[Lru/inetra/bytefog/service/NetworkInterfaceInfo;
    .restart local v7    # "ifmap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/net/NetworkInterface;>;"
    .restart local v13    # "netif":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    .restart local v21    # "wifiManager":Landroid/net/wifi/WifiManager;
    :cond_0
    :try_start_1
    const-string v22, "connectivity"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/ConnectivityManager;

    .line 83
    .local v5, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v5}, Landroid/net/ConnectivityManager;->getAllNetworkInfo()[Landroid/net/NetworkInfo;

    move-result-object v15

    .line 85
    .local v15, "networkInfos":[Landroid/net/NetworkInfo;
    array-length v0, v15

    move/from16 v23, v0

    const/16 v22, 0x0

    :goto_2
    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_5

    aget-object v14, v15, v22

    .line 86
    .local v14, "networkInfo":Landroid/net/NetworkInfo;
    invoke-virtual {v14}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v24

    if-eqz v24, :cond_4

    .line 87
    invoke-virtual {v14}, Landroid/net/NetworkInfo;->getType()I

    move-result v24

    move/from16 v0, v24

    invoke-static {v5, v0}, Lru/inetra/bytefog/service/NetworkUtils;->getInterfaceName(Landroid/net/ConnectivityManager;I)Ljava/lang/String;

    move-result-object v11

    .line 88
    .local v11, "interfaceName":Ljava/lang/String;
    if-eqz v11, :cond_4

    .line 89
    invoke-virtual {v7, v11}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/net/NetworkInterface;

    .line 90
    .restart local v16    # "networkInterface":Ljava/net/NetworkInterface;
    if-eqz v16, :cond_4

    .line 91
    invoke-virtual/range {v16 .. v16}, Ljava/net/NetworkInterface;->getInterfaceAddresses()Ljava/util/List;

    move-result-object v10

    .line 92
    .local v10, "interfaceAddresses":Ljava/util/List;, "Ljava/util/List<Ljava/net/InterfaceAddress;>;"
    if-eqz v10, :cond_4

    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v24

    if-nez v24, :cond_4

    .line 93
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 94
    .local v8, "inet4Addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/Inet4Address;>;"
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 95
    .local v19, "networkPrefixes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :cond_1
    :goto_3
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_2

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/net/InterfaceAddress;

    .line 96
    .local v6, "ia":Ljava/net/InterfaceAddress;
    invoke-virtual {v6}, Ljava/net/InterfaceAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v9

    .line 97
    .local v9, "inetAddress":Ljava/net/InetAddress;
    instance-of v0, v9, Ljava/net/Inet4Address;

    move/from16 v25, v0

    if-eqz v25, :cond_1

    .line 98
    check-cast v9, Ljava/net/Inet4Address;

    .end local v9    # "inetAddress":Ljava/net/InetAddress;
    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    invoke-virtual {v6}, Ljava/net/InterfaceAddress;->getNetworkPrefixLength()S

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v19

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 102
    .end local v6    # "ia":Ljava/net/InterfaceAddress;
    :cond_2
    new-instance v20, Lru/inetra/bytefog/service/NetworkInterfaceInfo;

    invoke-direct/range {v20 .. v20}, Lru/inetra/bytefog/service/NetworkInterfaceInfo;-><init>()V

    .line 103
    .local v20, "nii":Lru/inetra/bytefog/service/NetworkInterfaceInfo;
    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->setId(Ljava/lang/String;)V

    .line 104
    invoke-virtual {v14}, Landroid/net/NetworkInfo;->getType()I

    move-result v24

    sparse-switch v24, :sswitch_data_0

    .line 118
    const/16 v24, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->setType(I)V

    .line 120
    :goto_4
    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->setUnicastAddresses(Ljava/util/ArrayList;)V

    .line 121
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->setNetworkPrefixes(Ljava/util/ArrayList;)V

    .line 122
    invoke-virtual {v14}, Landroid/net/NetworkInfo;->getType()I

    move-result v24

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_3

    .line 123
    invoke-virtual/range {v21 .. v21}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v4

    .line 124
    .local v4, "connectionInfo":Landroid/net/wifi/WifiInfo;
    invoke-virtual {v4}, Landroid/net/wifi/WifiInfo;->getLinkSpeed()I

    move-result v12

    .line 125
    .local v12, "linkSpeed":I
    const v24, 0xf4240

    mul-int v24, v24, v12

    move/from16 v0, v24

    int-to-long v0, v0

    move-wide/from16 v24, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->setLinkSpeed(J)V

    .line 127
    .end local v4    # "connectionInfo":Landroid/net/wifi/WifiInfo;
    .end local v12    # "linkSpeed":I
    :cond_3
    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    .end local v8    # "inet4Addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/Inet4Address;>;"
    .end local v10    # "interfaceAddresses":Ljava/util/List;, "Ljava/util/List<Ljava/net/InterfaceAddress;>;"
    .end local v11    # "interfaceName":Ljava/lang/String;
    .end local v16    # "networkInterface":Ljava/net/NetworkInterface;
    .end local v19    # "networkPrefixes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v20    # "nii":Lru/inetra/bytefog/service/NetworkInterfaceInfo;
    :cond_4
    add-int/lit8 v22, v22, 0x1

    goto/16 :goto_2

    .line 106
    .restart local v8    # "inet4Addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/Inet4Address;>;"
    .restart local v10    # "interfaceAddresses":Ljava/util/List;, "Ljava/util/List<Ljava/net/InterfaceAddress;>;"
    .restart local v11    # "interfaceName":Ljava/lang/String;
    .restart local v16    # "networkInterface":Ljava/net/NetworkInterface;
    .restart local v19    # "networkPrefixes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v20    # "nii":Lru/inetra/bytefog/service/NetworkInterfaceInfo;
    :sswitch_0
    const/16 v24, 0x3

    move-object/from16 v0, v20

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->setType(I)V

    goto :goto_4

    .line 109
    :sswitch_1
    const/16 v24, 0x4

    move-object/from16 v0, v20

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->setType(I)V

    goto :goto_4

    .line 112
    :sswitch_2
    const/16 v24, 0x2

    move-object/from16 v0, v20

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->setType(I)V

    goto :goto_4

    .line 133
    .end local v8    # "inet4Addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/Inet4Address;>;"
    .end local v10    # "interfaceAddresses":Ljava/util/List;, "Ljava/util/List<Ljava/net/InterfaceAddress;>;"
    .end local v11    # "interfaceName":Ljava/lang/String;
    .end local v14    # "networkInfo":Landroid/net/NetworkInfo;
    .end local v16    # "networkInterface":Ljava/net/NetworkInterface;
    .end local v19    # "networkPrefixes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v20    # "nii":Lru/inetra/bytefog/service/NetworkInterfaceInfo;
    :cond_5
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 104
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_0
        0x9 -> :sswitch_2
    .end sparse-switch
.end method

.method public static getNetworkInterfaceInfoSerialized([Lru/inetra/bytefog/service/NetworkInterfaceInfo;)[B
    .locals 12
    .param p0, "networkInterfaceInfo"    # [Lru/inetra/bytefog/service/NetworkInterfaceInfo;

    .prologue
    .line 28
    invoke-static {p0}, Lru/inetra/bytefog/service/NetworkUtils;->calculateBufferSize([Lru/inetra/bytefog/service/NetworkInterfaceInfo;)I

    move-result v1

    .line 29
    .local v1, "bufferSize":I
    new-array v0, v1, [B

    .line 30
    .local v0, "array":[B
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 31
    .local v2, "byteBuffer":Ljava/nio/ByteBuffer;
    sget-object v7, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v7}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 32
    array-length v7, p0

    invoke-virtual {v2, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 33
    array-length v9, p0

    const/4 v7, 0x0

    move v8, v7

    :goto_0
    if-ge v8, v9, :cond_1

    aget-object v4, p0, v8

    .line 34
    .local v4, "info":Lru/inetra/bytefog/service/NetworkInterfaceInfo;
    invoke-virtual {v4}, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v2, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 35
    invoke-virtual {v4}, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->getId()Ljava/lang/String;

    move-result-object v7

    const-string v10, "ISO-8859-1"

    invoke-static {v10}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 36
    invoke-virtual {v4}, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->getType()I

    move-result v7

    invoke-virtual {v2, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 37
    invoke-virtual {v4}, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->getUnicastAddresses()Ljava/util/ArrayList;

    move-result-object v6

    .line 38
    .local v6, "unicastAddresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/Inet4Address;>;"
    invoke-virtual {v4}, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->getNetworkPrefixes()Ljava/util/ArrayList;

    move-result-object v5

    .line 39
    .local v5, "networkPrefixes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v2, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 40
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v3, v7, :cond_0

    .line 42
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/net/Inet4Address;

    invoke-virtual {v7}, Ljava/net/Inet4Address;->getAddress()[B

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 43
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v2, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 40
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 45
    :cond_0
    invoke-virtual {v4}, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->getLinkSpeed()J

    move-result-wide v10

    invoke-virtual {v2, v10, v11}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 33
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_0

    .line 47
    .end local v3    # "i":I
    .end local v4    # "info":Lru/inetra/bytefog/service/NetworkInterfaceInfo;
    .end local v5    # "networkPrefixes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v6    # "unicastAddresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/Inet4Address;>;"
    :cond_1
    return-object v0
.end method
