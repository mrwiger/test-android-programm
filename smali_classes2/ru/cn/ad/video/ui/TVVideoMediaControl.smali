.class public Lru/cn/ad/video/ui/TVVideoMediaControl;
.super Ljava/lang/Object;
.source "TVVideoMediaControl.java"

# interfaces
.implements Lru/cn/ad/video/ui/MediaControl;


# instance fields
.field private adSkipButton:Landroid/widget/Button;

.field private adSkipText:Landroid/widget/TextView;

.field private adWrapper:Landroid/view/View;

.field private final attributionFormat:Ljava/lang/String;

.field private final context:Landroid/content/Context;

.field private duration:J

.field private final formatter:Ljava/text/DateFormat;

.field private listener:Lru/cn/ad/video/ui/MediaControl$Listener;

.field private position:J

.field private skipPosition:J

.field private skippable:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "mm:ss"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->formatter:Ljava/text/DateFormat;

    .line 37
    iput-object p1, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->context:Landroid/content/Context;

    .line 38
    const v0, 0x7f0e0020

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->attributionFormat:Ljava/lang/String;

    .line 40
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0c007c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->adWrapper:Landroid/view/View;

    .line 41
    iget-object v0, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->adWrapper:Landroid/view/View;

    const v1, 0x7f090022

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->adSkipButton:Landroid/widget/Button;

    .line 42
    iget-object v0, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->adSkipButton:Landroid/widget/Button;

    new-instance v1, Lru/cn/ad/video/ui/TVVideoMediaControl$1;

    invoke-direct {v1, p0}, Lru/cn/ad/video/ui/TVVideoMediaControl$1;-><init>(Lru/cn/ad/video/ui/TVVideoMediaControl;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    iget-object v0, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->adWrapper:Landroid/view/View;

    const v1, 0x7f090023

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->adSkipText:Landroid/widget/TextView;

    .line 50
    return-void
.end method

.method static synthetic access$000(Lru/cn/ad/video/ui/TVVideoMediaControl;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/video/ui/TVVideoMediaControl;

    .prologue
    .line 19
    invoke-direct {p0}, Lru/cn/ad/video/ui/TVVideoMediaControl;->skip()V

    return-void
.end method

.method private skip()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->listener:Lru/cn/ad/video/ui/MediaControl$Listener;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->listener:Lru/cn/ad/video/ui/MediaControl$Listener;

    invoke-interface {v0}, Lru/cn/ad/video/ui/MediaControl$Listener;->onSkipped()V

    .line 87
    :cond_0
    return-void
.end method

.method private update()V
    .locals 15

    .prologue
    const/4 v6, 0x1

    const/4 v14, 0x0

    .line 90
    iget-boolean v0, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->skippable:Z

    if-eqz v0, :cond_2

    .line 93
    iget-wide v0, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->skipPosition:J

    iget-wide v2, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->position:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 94
    const-wide/16 v0, 0x1

    iget-wide v2, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->skipPosition:J

    iget-wide v10, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->position:J

    sub-long/2addr v2, v10

    const-wide/16 v10, 0x3e8

    div-long/2addr v2, v10

    add-long/2addr v0, v2

    long-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-long v8, v0

    .line 95
    .local v8, "timeUntilSkip":J
    iget-object v0, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->context:Landroid/content/Context;

    const v1, 0x7f0e0024

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v14

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 96
    .local v4, "text":Ljava/lang/String;
    iget-object v0, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->adSkipButton:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 118
    .end local v8    # "timeUntilSkip":J
    :cond_0
    :goto_0
    iget-object v0, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->adSkipText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    .end local v4    # "text":Ljava/lang/String;
    :goto_1
    return-void

    .line 99
    :cond_1
    iget-object v0, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->context:Landroid/content/Context;

    const v1, 0x7f0e0023

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 101
    .restart local v4    # "text":Ljava/lang/String;
    iget-object v0, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->adSkipButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->adSkipButton:Landroid/widget/Button;

    invoke-virtual {v0, v14}, Landroid/widget/Button;->setVisibility(I)V

    .line 103
    iget-object v0, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->adSkipButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    .line 106
    invoke-static {}, Lru/cn/peersay/controllers/DialogsRemoteController;->sharedInstance()Lru/cn/peersay/controllers/DialogsRemoteController;

    move-result-object v0

    iget-object v1, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->context:Landroid/content/Context;

    const-string v2, "ad_skip"

    iget-object v3, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->context:Landroid/content/Context;

    const v5, 0x7f0e0025

    .line 107
    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v5, v6, [Ljava/lang/String;

    iget-object v6, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->context:Landroid/content/Context;

    const v10, 0x7f0e0022

    .line 108
    invoke-virtual {v6, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v14

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    new-instance v6, Lru/cn/ad/video/ui/TVVideoMediaControl$2;

    invoke-direct {v6, p0}, Lru/cn/ad/video/ui/TVVideoMediaControl$2;-><init>(Lru/cn/ad/video/ui/TVVideoMediaControl;)V

    .line 106
    invoke-virtual/range {v0 .. v6}, Lru/cn/peersay/controllers/DialogsRemoteController;->registerDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lru/cn/peersay/controllers/DialogsRemoteController$DismissListener;)V

    goto :goto_0

    .line 120
    .end local v4    # "text":Ljava/lang/String;
    :cond_2
    iget-object v0, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->attributionFormat:Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/Object;

    iget-object v2, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->formatter:Ljava/text/DateFormat;

    new-instance v3, Ljava/util/Date;

    iget-wide v10, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->duration:J

    iget-wide v12, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->position:J

    sub-long/2addr v10, v12

    invoke-direct {v3, v10, v11}, Ljava/util/Date;-><init>(J)V

    .line 121
    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v14

    .line 120
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 122
    .local v7, "adDurationText":Ljava/lang/String;
    iget-object v0, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->adSkipText:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->adWrapper:Landroid/view/View;

    return-object v0
.end method

.method public setClickable(Z)V
    .locals 0
    .param p1, "clickable"    # Z

    .prologue
    .line 72
    return-void
.end method

.method public setDuration(J)V
    .locals 1
    .param p1, "durationMs"    # J

    .prologue
    .line 54
    iput-wide p1, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->duration:J

    .line 55
    return-void
.end method

.method public setListener(Lru/cn/ad/video/ui/MediaControl$Listener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/ad/video/ui/MediaControl$Listener;

    .prologue
    .line 81
    iput-object p1, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->listener:Lru/cn/ad/video/ui/MediaControl$Listener;

    .line 82
    return-void
.end method

.method public setPosition(J)V
    .locals 1
    .param p1, "positionMs"    # J

    .prologue
    .line 59
    iput-wide p1, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->position:J

    .line 60
    invoke-direct {p0}, Lru/cn/ad/video/ui/TVVideoMediaControl;->update()V

    .line 61
    return-void
.end method

.method public setSkipPositionMs(J)V
    .locals 1
    .param p1, "skipPosition"    # J

    .prologue
    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->skippable:Z

    .line 66
    iput-wide p1, p0, Lru/cn/ad/video/ui/TVVideoMediaControl;->skipPosition:J

    .line 67
    return-void
.end method
