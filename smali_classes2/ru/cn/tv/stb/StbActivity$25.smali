.class Lru/cn/tv/stb/StbActivity$25;
.super Ljava/lang/Object;
.source "StbActivity.java"

# interfaces
.implements Lru/cn/tv/stb/channels/ChannelsFragment$ChannelsFragmentListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/stb/StbActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/stb/StbActivity;


# direct methods
.method constructor <init>(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 1690
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChannelSelected(JZJZ)V
    .locals 7
    .param p1, "channelId"    # J
    .param p3, "isDenied"    # Z
    .param p4, "contractorId"    # J
    .param p6, "allowPurchases"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1695
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$600(Lru/cn/tv/stb/StbActivity;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1696
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$700(Lru/cn/tv/stb/StbActivity;)V

    .line 1697
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$300(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/categories/CategoryFragment;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/tv/stb/categories/CategoryFragment;->isFolded()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1698
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$4800(Lru/cn/tv/stb/StbActivity;)V

    .line 1701
    :cond_0
    if-nez p3, :cond_2

    .line 1702
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$1800(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v1

    iget-object v2, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    .line 1703
    invoke-static {v2}, Lru/cn/tv/stb/StbActivity;->access$3900(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/channels/ChannelsFragment;

    move-result-object v2

    invoke-virtual {v2}, Lru/cn/tv/stb/channels/ChannelsFragment;->getClickedPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/cursor/ChannelCursor;

    .line 1704
    .local v0, "c":Lru/cn/api/provider/cursor/ChannelCursor;
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getHasSchedule()I

    move-result v1

    if-ne v1, v5, :cond_2

    .line 1705
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1, v0}, Lru/cn/tv/stb/StbActivity;->access$5700(Lru/cn/tv/stb/StbActivity;Lru/cn/api/provider/cursor/ChannelCursor;)V

    .line 1707
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1, v4}, Lru/cn/tv/stb/StbActivity;->access$602(Lru/cn/tv/stb/StbActivity;Z)Z

    .line 1767
    .end local v0    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    :cond_1
    :goto_0
    return-void

    .line 1713
    :cond_2
    if-eqz p3, :cond_5

    .line 1714
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$000(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/SimplePlayerFragment;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/tv/player/SimplePlayerFragment;->getChannelId()J

    move-result-wide v2

    cmp-long v1, p1, v2

    if-nez v1, :cond_3

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$4000(Lru/cn/tv/stb/StbActivity;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1715
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$1100(Lru/cn/tv/stb/StbActivity;)V

    goto :goto_0

    .line 1719
    :cond_3
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$5800(Lru/cn/tv/stb/StbActivity;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1720
    const-string v1, "StbActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onChannelSelected in kids mode cnId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is denied"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1722
    new-instance v1, Landroid/support/v7/app/AlertDialog$Builder;

    iget-object v2, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-direct {v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0e0114

    .line 1723
    invoke-virtual {v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    .line 1724
    invoke-virtual {v1, v5}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e0034

    new-instance v3, Lru/cn/tv/stb/StbActivity$25$2;

    invoke-direct {v3, p0}, Lru/cn/tv/stb/StbActivity$25$2;-><init>(Lru/cn/tv/stb/StbActivity$25;)V

    .line 1725
    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lru/cn/tv/stb/StbActivity$25$1;

    invoke-direct {v2, p0}, Lru/cn/tv/stb/StbActivity$25$1;-><init>(Lru/cn/tv/stb/StbActivity$25;)V

    .line 1730
    invoke-virtual {v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    .line 1737
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 1739
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/StbPlayerController;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/tv/player/controller/StbPlayerController;->hide()V

    goto :goto_0

    .line 1741
    :cond_4
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-lez v1, :cond_1

    .line 1742
    const-string v1, "StbActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onChannelSelected. cnId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is denied"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1744
    if-eqz p6, :cond_1

    .line 1745
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$3900(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/channels/ChannelsFragment;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lru/cn/tv/stb/channels/ChannelsFragment;->setCurrentChannel(J)V

    .line 1746
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$1800(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v1

    iget-object v2, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v2}, Lru/cn/tv/stb/StbActivity;->access$1800(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v2

    invoke-virtual {v1, v2, v5}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 1748
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1, p4, p5, p1, p2}, Lru/cn/tv/stb/StbActivity;->access$6100(Lru/cn/tv/stb/StbActivity;JJ)V

    .line 1749
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$6200(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/billing/BillingFragment;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/tv/billing/BillingFragment;->requestFocus()Z

    goto/16 :goto_0

    .line 1756
    :cond_5
    const-string v1, "StbActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onChannelSelected. cnId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1757
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    iget-object v2, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v2}, Lru/cn/tv/stb/StbActivity;->access$2900(Lru/cn/tv/stb/StbActivity;)Lru/cn/domain/tv/CurrentCategory$Type;

    move-result-object v2

    invoke-static {v1, v2}, Lru/cn/tv/stb/StbActivity;->access$6302(Lru/cn/tv/stb/StbActivity;Lru/cn/domain/tv/CurrentCategory$Type;)Lru/cn/domain/tv/CurrentCategory$Type;

    .line 1759
    invoke-static {v4, v4}, Lru/cn/domain/statistics/inetra/InetraTracker;->setSessionParams(II)V

    .line 1763
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$000(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/SimplePlayerFragment;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lru/cn/tv/player/SimplePlayerFragment;->playChannel(J)V

    .line 1765
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$25;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$4100(Lru/cn/tv/stb/StbActivity;)V

    goto/16 :goto_0
.end method
