.class public final Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegAudioRenderer;
.super Lcom/google/android/exoplayer2/audio/SimpleDecoderAudioRenderer;
.source "FfmpegAudioRenderer.java"


# instance fields
.field private decoder:Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegDecoder;

.field private final enableFloatOutput:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 51
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/exoplayer2/audio/AudioProcessor;

    invoke-direct {p0, v1, v1, v0}, Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegAudioRenderer;-><init>(Landroid/os/Handler;Lcom/google/android/exoplayer2/audio/AudioRendererEventListener;[Lcom/google/android/exoplayer2/audio/AudioProcessor;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Lcom/google/android/exoplayer2/audio/AudioRendererEventListener;Lcom/google/android/exoplayer2/audio/AudioSink;Z)V
    .locals 6
    .param p1, "eventHandler"    # Landroid/os/Handler;
    .param p2, "eventListener"    # Lcom/google/android/exoplayer2/audio/AudioRendererEventListener;
    .param p3, "audioSink"    # Lcom/google/android/exoplayer2/audio/AudioSink;
    .param p4, "enableFloatOutput"    # Z

    .prologue
    .line 77
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/audio/SimpleDecoderAudioRenderer;-><init>(Landroid/os/Handler;Lcom/google/android/exoplayer2/audio/AudioRendererEventListener;Lcom/google/android/exoplayer2/drm/DrmSessionManager;ZLcom/google/android/exoplayer2/audio/AudioSink;)V

    .line 78
    iput-boolean p4, p0, Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegAudioRenderer;->enableFloatOutput:Z

    .line 79
    return-void
.end method

.method public varargs constructor <init>(Landroid/os/Handler;Lcom/google/android/exoplayer2/audio/AudioRendererEventListener;[Lcom/google/android/exoplayer2/audio/AudioProcessor;)V
    .locals 2
    .param p1, "eventHandler"    # Landroid/os/Handler;
    .param p2, "eventListener"    # Lcom/google/android/exoplayer2/audio/AudioRendererEventListener;
    .param p3, "audioProcessors"    # [Lcom/google/android/exoplayer2/audio/AudioProcessor;

    .prologue
    .line 62
    new-instance v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p3}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;-><init>(Lcom/google/android/exoplayer2/audio/AudioCapabilities;[Lcom/google/android/exoplayer2/audio/AudioProcessor;)V

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegAudioRenderer;-><init>(Landroid/os/Handler;Lcom/google/android/exoplayer2/audio/AudioRendererEventListener;Lcom/google/android/exoplayer2/audio/AudioSink;Z)V

    .line 63
    return-void
.end method

.method private isOutputSupported(Lcom/google/android/exoplayer2/Format;)Z
    .locals 1
    .param p1, "inputFormat"    # Lcom/google/android/exoplayer2/Format;

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegAudioRenderer;->shouldUseFloatOutput(Lcom/google/android/exoplayer2/Format;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegAudioRenderer;->supportsOutputEncoding(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldUseFloatOutput(Lcom/google/android/exoplayer2/Format;)Z
    .locals 6
    .param p1, "inputFormat"    # Lcom/google/android/exoplayer2/Format;

    .prologue
    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 123
    iget-boolean v2, p0, Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegAudioRenderer;->enableFloatOutput:Z

    if-eqz v2, :cond_0

    invoke-virtual {p0, v5}, Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegAudioRenderer;->supportsOutputEncoding(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 137
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 126
    :cond_1
    iget-object v3, p1, Lcom/google/android/exoplayer2/Format;->sampleMimeType:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_2
    :goto_1
    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 137
    goto :goto_0

    .line 126
    :sswitch_0
    const-string v4, "audio/raw"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v2, v0

    goto :goto_1

    :sswitch_1
    const-string v4, "audio/ac3"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v2, v1

    goto :goto_1

    .line 129
    :pswitch_1
    iget v2, p1, Lcom/google/android/exoplayer2/Format;->pcmEncoding:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_3

    iget v2, p1, Lcom/google/android/exoplayer2/Format;->pcmEncoding:I

    const/high16 v3, 0x40000000    # 2.0f

    if-eq v2, v3, :cond_3

    iget v2, p1, Lcom/google/android/exoplayer2/Format;->pcmEncoding:I

    if-ne v2, v5, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    .line 126
    nop

    :sswitch_data_0
    .sparse-switch
        0xb269698 -> :sswitch_1
        0xb26d66f -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected bridge synthetic createDecoder(Lcom/google/android/exoplayer2/Format;Lcom/google/android/exoplayer2/drm/ExoMediaCrypto;)Lcom/google/android/exoplayer2/decoder/SimpleDecoder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/audio/AudioDecoderException;
        }
    .end annotation

    .prologue
    .line 34
    invoke-virtual {p0, p1, p2}, Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegAudioRenderer;->createDecoder(Lcom/google/android/exoplayer2/Format;Lcom/google/android/exoplayer2/drm/ExoMediaCrypto;)Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegDecoder;

    move-result-object v0

    return-object v0
.end method

.method protected createDecoder(Lcom/google/android/exoplayer2/Format;Lcom/google/android/exoplayer2/drm/ExoMediaCrypto;)Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegDecoder;
    .locals 7
    .param p1, "format"    # Lcom/google/android/exoplayer2/Format;
    .param p2, "mediaCrypto"    # Lcom/google/android/exoplayer2/drm/ExoMediaCrypto;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegDecoderException;
        }
    .end annotation

    .prologue
    const/16 v1, 0x10

    .line 104
    new-instance v0, Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegDecoder;

    const/16 v3, 0x1680

    iget-object v4, p1, Lcom/google/android/exoplayer2/Format;->sampleMimeType:Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/exoplayer2/Format;->initializationData:Ljava/util/List;

    .line 105
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegAudioRenderer;->shouldUseFloatOutput(Lcom/google/android/exoplayer2/Format;)Z

    move-result v6

    move v2, v1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegDecoder;-><init>(IIILjava/lang/String;Ljava/util/List;Z)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegAudioRenderer;->decoder:Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegDecoder;

    .line 106
    iget-object v0, p0, Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegAudioRenderer;->decoder:Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegDecoder;

    return-object v0
.end method

.method public getOutputFormat()Lcom/google/android/exoplayer2/Format;
    .locals 12

    .prologue
    const/4 v3, -0x1

    const/4 v0, 0x0

    .line 111
    iget-object v1, p0, Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegAudioRenderer;->decoder:Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegDecoder;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegDecoder;->getChannelCount()I

    move-result v5

    .line 112
    .local v5, "channelCount":I
    iget-object v1, p0, Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegAudioRenderer;->decoder:Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegDecoder;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegDecoder;->getSampleRate()I

    move-result v6

    .line 113
    .local v6, "sampleRate":I
    iget-object v1, p0, Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegAudioRenderer;->decoder:Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegDecoder;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegDecoder;->getEncoding()I

    move-result v7

    .line 114
    .local v7, "encoding":I
    const-string v1, "audio/raw"

    const/4 v10, 0x0

    move-object v2, v0

    move v4, v3

    move-object v8, v0

    move-object v9, v0

    move-object v11, v0

    invoke-static/range {v0 .. v11}, Lcom/google/android/exoplayer2/Format;->createAudioSampleFormat(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIILjava/util/List;Lcom/google/android/exoplayer2/drm/DrmInitData;ILjava/lang/String;)Lcom/google/android/exoplayer2/Format;

    move-result-object v0

    return-object v0
.end method

.method protected supportsFormatInternal(Lcom/google/android/exoplayer2/drm/DrmSessionManager;Lcom/google/android/exoplayer2/Format;)I
    .locals 2
    .param p2, "format"    # Lcom/google/android/exoplayer2/Format;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/drm/DrmSessionManager",
            "<",
            "Lcom/google/android/exoplayer2/drm/ExoMediaCrypto;",
            ">;",
            "Lcom/google/android/exoplayer2/Format;",
            ")I"
        }
    .end annotation

    .prologue
    .line 84
    .local p1, "drmSessionManager":Lcom/google/android/exoplayer2/drm/DrmSessionManager;, "Lcom/google/android/exoplayer2/drm/DrmSessionManager<Lcom/google/android/exoplayer2/drm/ExoMediaCrypto;>;"
    iget-object v0, p2, Lcom/google/android/exoplayer2/Format;->sampleMimeType:Ljava/lang/String;

    .line 85
    .local v0, "sampleMimeType":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegLibrary;->isAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/MimeTypes;->isAudio(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 86
    :cond_0
    const/4 v1, 0x0

    .line 92
    :goto_0
    return v1

    .line 87
    :cond_1
    invoke-static {v0}, Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegLibrary;->supportsFormat(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, p2}, Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegAudioRenderer;->isOutputSupported(Lcom/google/android/exoplayer2/Format;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 88
    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    .line 89
    :cond_3
    iget-object v1, p2, Lcom/google/android/exoplayer2/Format;->drmInitData:Lcom/google/android/exoplayer2/drm/DrmInitData;

    invoke-static {p1, v1}, Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegAudioRenderer;->supportsFormatDrm(Lcom/google/android/exoplayer2/drm/DrmSessionManager;Lcom/google/android/exoplayer2/drm/DrmInitData;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 90
    const/4 v1, 0x2

    goto :goto_0

    .line 92
    :cond_4
    const/4 v1, 0x4

    goto :goto_0
.end method

.method public final supportsMixedMimeTypeAdaptation()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    .line 98
    const/16 v0, 0x8

    return v0
.end method
