.class Lru/cn/tv/stb/StbActivity$26;
.super Lru/cn/tv/stb/ListKeyListener;
.source "StbActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/stb/StbActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/stb/StbActivity;


# direct methods
.method constructor <init>(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 1770
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity$26;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-direct {p0}, Lru/cn/tv/stb/ListKeyListener;-><init>()V

    return-void
.end method


# virtual methods
.method protected keyDown()Z
    .locals 1

    .prologue
    .line 1773
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$26;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$800(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/calendar/CalendarFragment;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/stb/calendar/CalendarFragment;->selectNext()Z

    .line 1774
    const/4 v0, 0x1

    return v0
.end method

.method protected keyLeft()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1783
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$26;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$6400(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 1784
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 1785
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$26;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$6400(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    invoke-virtual {v1, v2, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 1786
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$26;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$6500(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->requestFocus()Z

    .line 1789
    :cond_0
    return v3
.end method

.method protected keyUp()Z
    .locals 1

    .prologue
    .line 1778
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$26;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$800(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/calendar/CalendarFragment;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/stb/calendar/CalendarFragment;->selectPrev()Z

    .line 1779
    const/4 v0, 0x1

    return v0
.end method
