.class public final Lcom/google/obf/ci;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/aj;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/ci$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/obf/cj;

.field private final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/obf/ci$a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/obf/dw;

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Lcom/google/obf/al;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 1
    new-instance v0, Lcom/google/obf/cj;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Lcom/google/obf/cj;-><init>(J)V

    invoke-direct {p0, v0}, Lcom/google/obf/ci;-><init>(Lcom/google/obf/cj;)V

    .line 2
    return-void
.end method

.method public constructor <init>(Lcom/google/obf/cj;)V
    .locals 2

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lcom/google/obf/ci;->a:Lcom/google/obf/cj;

    .line 5
    new-instance v0, Lcom/google/obf/dw;

    const/16 v1, 0x1000

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/ci;->c:Lcom/google/obf/dw;

    .line 6
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/obf/ci;->b:Landroid/util/SparseArray;

    .line 7
    return-void
.end method


# virtual methods
.method public a(Lcom/google/obf/ak;Lcom/google/obf/ao;)I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v0, -0x1

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 35
    iget-object v2, p0, Lcom/google/obf/ci;->c:Lcom/google/obf/dw;

    iget-object v2, v2, Lcom/google/obf/dw;->a:[B

    const/4 v3, 0x4

    invoke-interface {p1, v2, v1, v3, v6}, Lcom/google/obf/ak;->b([BIIZ)Z

    move-result v2

    if-nez v2, :cond_1

    .line 90
    :cond_0
    :goto_0
    return v0

    .line 37
    :cond_1
    iget-object v2, p0, Lcom/google/obf/ci;->c:Lcom/google/obf/dw;

    invoke-virtual {v2, v1}, Lcom/google/obf/dw;->c(I)V

    .line 38
    iget-object v2, p0, Lcom/google/obf/ci;->c:Lcom/google/obf/dw;

    invoke-virtual {v2}, Lcom/google/obf/dw;->m()I

    move-result v2

    .line 39
    const/16 v3, 0x1b9

    if-eq v2, v3, :cond_0

    .line 41
    const/16 v0, 0x1ba

    if-ne v2, v0, :cond_2

    .line 42
    iget-object v0, p0, Lcom/google/obf/ci;->c:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    const/16 v2, 0xa

    invoke-interface {p1, v0, v1, v2}, Lcom/google/obf/ak;->c([BII)V

    .line 43
    iget-object v0, p0, Lcom/google/obf/ci;->c:Lcom/google/obf/dw;

    invoke-virtual {v0, v1}, Lcom/google/obf/dw;->c(I)V

    .line 44
    iget-object v0, p0, Lcom/google/obf/ci;->c:Lcom/google/obf/dw;

    const/16 v2, 0x9

    invoke-virtual {v0, v2}, Lcom/google/obf/dw;->d(I)V

    .line 45
    iget-object v0, p0, Lcom/google/obf/ci;->c:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->f()I

    move-result v0

    and-int/lit8 v0, v0, 0x7

    .line 46
    add-int/lit8 v0, v0, 0xe

    invoke-interface {p1, v0}, Lcom/google/obf/ak;->b(I)V

    move v0, v1

    .line 47
    goto :goto_0

    .line 48
    :cond_2
    const/16 v0, 0x1bb

    if-ne v2, v0, :cond_3

    .line 49
    iget-object v0, p0, Lcom/google/obf/ci;->c:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    invoke-interface {p1, v0, v1, v7}, Lcom/google/obf/ak;->c([BII)V

    .line 50
    iget-object v0, p0, Lcom/google/obf/ci;->c:Lcom/google/obf/dw;

    invoke-virtual {v0, v1}, Lcom/google/obf/dw;->c(I)V

    .line 51
    iget-object v0, p0, Lcom/google/obf/ci;->c:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->g()I

    move-result v0

    .line 52
    add-int/lit8 v0, v0, 0x6

    invoke-interface {p1, v0}, Lcom/google/obf/ak;->b(I)V

    move v0, v1

    .line 53
    goto :goto_0

    .line 54
    :cond_3
    and-int/lit16 v0, v2, -0x100

    shr-int/lit8 v0, v0, 0x8

    if-eq v0, v6, :cond_4

    .line 55
    invoke-interface {p1, v6}, Lcom/google/obf/ak;->b(I)V

    move v0, v1

    .line 56
    goto :goto_0

    .line 57
    :cond_4
    and-int/lit16 v3, v2, 0xff

    .line 58
    iget-object v0, p0, Lcom/google/obf/ci;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/ci$a;

    .line 59
    iget-boolean v2, p0, Lcom/google/obf/ci;->d:Z

    if-nez v2, :cond_9

    .line 60
    if-nez v0, :cond_6

    .line 61
    const/4 v2, 0x0

    .line 62
    iget-boolean v4, p0, Lcom/google/obf/ci;->e:Z

    if-nez v4, :cond_a

    const/16 v4, 0xbd

    if-ne v3, v4, :cond_a

    .line 63
    new-instance v2, Lcom/google/obf/bx;

    iget-object v4, p0, Lcom/google/obf/ci;->g:Lcom/google/obf/al;

    invoke-interface {v4, v3}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v4

    invoke-direct {v2, v4, v1}, Lcom/google/obf/bx;-><init>(Lcom/google/obf/ar;Z)V

    .line 64
    iput-boolean v6, p0, Lcom/google/obf/ci;->e:Z

    .line 71
    :cond_5
    :goto_1
    if-eqz v2, :cond_6

    .line 72
    new-instance v0, Lcom/google/obf/ci$a;

    iget-object v4, p0, Lcom/google/obf/ci;->a:Lcom/google/obf/cj;

    invoke-direct {v0, v2, v4}, Lcom/google/obf/ci$a;-><init>(Lcom/google/obf/cb;Lcom/google/obf/cj;)V

    .line 73
    iget-object v2, p0, Lcom/google/obf/ci;->b:Landroid/util/SparseArray;

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 74
    :cond_6
    iget-boolean v2, p0, Lcom/google/obf/ci;->e:Z

    if-eqz v2, :cond_7

    iget-boolean v2, p0, Lcom/google/obf/ci;->f:Z

    if-nez v2, :cond_8

    :cond_7
    invoke-interface {p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v2

    const-wide/32 v4, 0x100000

    cmp-long v2, v2, v4

    if-lez v2, :cond_9

    .line 75
    :cond_8
    iput-boolean v6, p0, Lcom/google/obf/ci;->d:Z

    .line 76
    iget-object v2, p0, Lcom/google/obf/ci;->g:Lcom/google/obf/al;

    invoke-interface {v2}, Lcom/google/obf/al;->f()V

    .line 77
    :cond_9
    iget-object v2, p0, Lcom/google/obf/ci;->c:Lcom/google/obf/dw;

    iget-object v2, v2, Lcom/google/obf/dw;->a:[B

    invoke-interface {p1, v2, v1, v7}, Lcom/google/obf/ak;->c([BII)V

    .line 78
    iget-object v2, p0, Lcom/google/obf/ci;->c:Lcom/google/obf/dw;

    invoke-virtual {v2, v1}, Lcom/google/obf/dw;->c(I)V

    .line 79
    iget-object v2, p0, Lcom/google/obf/ci;->c:Lcom/google/obf/dw;

    invoke-virtual {v2}, Lcom/google/obf/dw;->g()I

    move-result v2

    .line 80
    add-int/lit8 v2, v2, 0x6

    .line 81
    if-nez v0, :cond_c

    .line 82
    invoke-interface {p1, v2}, Lcom/google/obf/ak;->b(I)V

    :goto_2
    move v0, v1

    .line 90
    goto/16 :goto_0

    .line 65
    :cond_a
    iget-boolean v4, p0, Lcom/google/obf/ci;->e:Z

    if-nez v4, :cond_b

    and-int/lit16 v4, v3, 0xe0

    const/16 v5, 0xc0

    if-ne v4, v5, :cond_b

    .line 66
    new-instance v2, Lcom/google/obf/cg;

    iget-object v4, p0, Lcom/google/obf/ci;->g:Lcom/google/obf/al;

    invoke-interface {v4, v3}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/google/obf/cg;-><init>(Lcom/google/obf/ar;)V

    .line 67
    iput-boolean v6, p0, Lcom/google/obf/ci;->e:Z

    goto :goto_1

    .line 68
    :cond_b
    iget-boolean v4, p0, Lcom/google/obf/ci;->f:Z

    if-nez v4, :cond_5

    and-int/lit16 v4, v3, 0xf0

    const/16 v5, 0xe0

    if-ne v4, v5, :cond_5

    .line 69
    new-instance v2, Lcom/google/obf/cc;

    iget-object v4, p0, Lcom/google/obf/ci;->g:Lcom/google/obf/al;

    invoke-interface {v4, v3}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/google/obf/cc;-><init>(Lcom/google/obf/ar;)V

    .line 70
    iput-boolean v6, p0, Lcom/google/obf/ci;->f:Z

    goto :goto_1

    .line 83
    :cond_c
    iget-object v3, p0, Lcom/google/obf/ci;->c:Lcom/google/obf/dw;

    invoke-virtual {v3}, Lcom/google/obf/dw;->e()I

    move-result v3

    if-ge v3, v2, :cond_d

    .line 84
    iget-object v3, p0, Lcom/google/obf/ci;->c:Lcom/google/obf/dw;

    new-array v4, v2, [B

    invoke-virtual {v3, v4, v2}, Lcom/google/obf/dw;->a([BI)V

    .line 85
    :cond_d
    iget-object v3, p0, Lcom/google/obf/ci;->c:Lcom/google/obf/dw;

    iget-object v3, v3, Lcom/google/obf/dw;->a:[B

    invoke-interface {p1, v3, v1, v2}, Lcom/google/obf/ak;->b([BII)V

    .line 86
    iget-object v3, p0, Lcom/google/obf/ci;->c:Lcom/google/obf/dw;

    const/4 v4, 0x6

    invoke-virtual {v3, v4}, Lcom/google/obf/dw;->c(I)V

    .line 87
    iget-object v3, p0, Lcom/google/obf/ci;->c:Lcom/google/obf/dw;

    invoke-virtual {v3, v2}, Lcom/google/obf/dw;->b(I)V

    .line 88
    iget-object v2, p0, Lcom/google/obf/ci;->c:Lcom/google/obf/dw;

    iget-object v3, p0, Lcom/google/obf/ci;->g:Lcom/google/obf/al;

    invoke-virtual {v0, v2, v3}, Lcom/google/obf/ci$a;->a(Lcom/google/obf/dw;Lcom/google/obf/al;)V

    .line 89
    iget-object v0, p0, Lcom/google/obf/ci;->c:Lcom/google/obf/dw;

    iget-object v2, p0, Lcom/google/obf/ci;->c:Lcom/google/obf/dw;

    invoke-virtual {v2}, Lcom/google/obf/dw;->e()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/obf/dw;->b(I)V

    goto :goto_2
.end method

.method public a(Lcom/google/obf/al;)V
    .locals 1

    .prologue
    .line 26
    iput-object p1, p0, Lcom/google/obf/ci;->g:Lcom/google/obf/al;

    .line 27
    sget-object v0, Lcom/google/obf/aq;->f:Lcom/google/obf/aq;

    invoke-interface {p1, v0}, Lcom/google/obf/al;->a(Lcom/google/obf/aq;)V

    .line 28
    return-void
.end method

.method public a(Lcom/google/obf/ak;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8
    const/16 v2, 0xe

    new-array v2, v2, [B

    .line 9
    const/16 v3, 0xe

    invoke-interface {p1, v2, v1, v3}, Lcom/google/obf/ak;->c([BII)V

    .line 10
    const/16 v3, 0x1ba

    aget-byte v4, v2, v1

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x18

    aget-byte v5, v2, v0

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v4, v5

    aget-byte v5, v2, v8

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v4, v5

    aget-byte v5, v2, v6

    and-int/lit16 v5, v5, 0xff

    or-int/2addr v4, v5

    if-eq v3, v4, :cond_1

    .line 25
    :cond_0
    :goto_0
    return v1

    .line 12
    :cond_1
    aget-byte v3, v2, v7

    and-int/lit16 v3, v3, 0xc4

    const/16 v4, 0x44

    if-ne v3, v4, :cond_0

    .line 14
    const/4 v3, 0x6

    aget-byte v3, v2, v3

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v7, :cond_0

    .line 16
    const/16 v3, 0x8

    aget-byte v3, v2, v3

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v7, :cond_0

    .line 18
    const/16 v3, 0x9

    aget-byte v3, v2, v3

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    .line 20
    const/16 v3, 0xc

    aget-byte v3, v2, v3

    and-int/lit8 v3, v3, 0x3

    if-ne v3, v6, :cond_0

    .line 22
    const/16 v3, 0xd

    aget-byte v3, v2, v3

    and-int/lit8 v3, v3, 0x7

    .line 23
    invoke-interface {p1, v3}, Lcom/google/obf/ak;->c(I)V

    .line 24
    invoke-interface {p1, v2, v1, v6}, Lcom/google/obf/ak;->c([BII)V

    .line 25
    aget-byte v3, v2, v1

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    aget-byte v4, v2, v0

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    or-int/2addr v3, v4

    aget-byte v2, v2, v8

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v2, v3

    if-ne v0, v2, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public b()V
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/obf/ci;->a:Lcom/google/obf/cj;

    invoke-virtual {v0}, Lcom/google/obf/cj;->a()V

    .line 30
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/obf/ci;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/google/obf/ci;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/ci$a;

    invoke-virtual {v0}, Lcom/google/obf/ci$a;->a()V

    .line 32
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 33
    :cond_0
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 34
    return-void
.end method
