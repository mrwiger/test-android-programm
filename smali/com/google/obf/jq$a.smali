.class abstract enum Lcom/google/obf/jq$a;
.super Ljava/lang/Enum;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/iw;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/jq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x440a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/obf/jq$a;",
        ">;",
        "Lcom/google/obf/iw",
        "<",
        "Ljava/util/Map$Entry",
        "<**>;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/obf/jq$a;

.field public static final enum b:Lcom/google/obf/jq$a;

.field private static final synthetic c:[Lcom/google/obf/jq$a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5
    new-instance v0, Lcom/google/obf/jq$a$1;

    const-string v1, "KEY"

    invoke-direct {v0, v1, v2}, Lcom/google/obf/jq$a$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/jq$a;->a:Lcom/google/obf/jq$a;

    .line 6
    new-instance v0, Lcom/google/obf/jq$a$2;

    const-string v1, "VALUE"

    invoke-direct {v0, v1, v3}, Lcom/google/obf/jq$a$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/jq$a;->b:Lcom/google/obf/jq$a;

    .line 7
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/obf/jq$a;

    sget-object v1, Lcom/google/obf/jq$a;->a:Lcom/google/obf/jq$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/obf/jq$a;->b:Lcom/google/obf/jq$a;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/obf/jq$a;->c:[Lcom/google/obf/jq$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/google/obf/jq$1;)V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Lcom/google/obf/jq$a;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/obf/jq$a;
    .locals 1

    .prologue
    .line 2
    const-class v0, Lcom/google/obf/jq$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/obf/jq$a;

    return-object v0
.end method

.method public static values()[Lcom/google/obf/jq$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lcom/google/obf/jq$a;->c:[Lcom/google/obf/jq$a;

    invoke-virtual {v0}, [Lcom/google/obf/jq$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/obf/jq$a;

    return-object v0
.end method
