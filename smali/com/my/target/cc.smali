.class public Lcom/my/target/cc;
.super Landroid/view/TextureView;
.source "VideoTextureView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/cc$c;,
        Lcom/my/target/cc$d;,
        Lcom/my/target/cc$b;,
        Lcom/my/target/cc$a;
    }
.end annotation


# static fields
.field protected static final jo:I = 0xc8

.field private static final jp:I = 0x2710


# instance fields
.field private jA:I

.field private jq:Lcom/my/target/cc$a;

.field private jr:Landroid/media/MediaPlayer;

.field private js:F

.field private jt:Z

.field private ju:I

.field private jv:I

.field private jw:I

.field private jx:Landroid/graphics/Bitmap;

.field private jy:Lcom/my/target/common/models/VideoData;

.field private jz:Landroid/view/Surface;

.field private uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    .line 34
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/my/target/cc;->js:F

    .line 72
    return-void
.end method

.method static synthetic a(Lcom/my/target/cc;)I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/my/target/cc;->ju:I

    return v0
.end method

.method static synthetic a(Lcom/my/target/cc;I)I
    .locals 0

    .prologue
    .line 21
    iput p1, p0, Lcom/my/target/cc;->ju:I

    return p1
.end method

.method static synthetic a(Lcom/my/target/cc;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    return-object p1
.end method

.method static synthetic a(Lcom/my/target/cc;Landroid/view/Surface;)Landroid/view/Surface;
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lcom/my/target/cc;->jz:Landroid/view/Surface;

    return-object p1
.end method

.method static synthetic b(Lcom/my/target/cc;)Landroid/media/MediaPlayer;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method private b(Landroid/media/MediaPlayer;)V
    .locals 1

    .prologue
    .line 562
    const/4 v0, 0x4

    iput v0, p0, Lcom/my/target/cc;->ju:I

    .line 563
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->pause()V

    .line 564
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/my/target/cc;->jv:I

    .line 565
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    if-eqz v0, :cond_0

    .line 567
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    invoke-interface {v0}, Lcom/my/target/cc$a;->bt()V

    .line 569
    :cond_0
    return-void
.end method

.method private bo()V
    .locals 2

    .prologue
    .line 506
    iget-object v0, p0, Lcom/my/target/cc;->jy:Lcom/my/target/common/models/VideoData;

    if-eqz v0, :cond_0

    .line 508
    iget-object v0, p0, Lcom/my/target/cc;->jy:Lcom/my/target/common/models/VideoData;

    invoke-virtual {v0}, Lcom/my/target/common/models/VideoData;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/my/target/cc;->jy:Lcom/my/target/common/models/VideoData;

    invoke-virtual {v1}, Lcom/my/target/common/models/VideoData;->getHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/my/target/cc;->getBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/cc;->jx:Landroid/graphics/Bitmap;

    .line 510
    :cond_0
    return-void
.end method

.method private bp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 529
    iget v0, p0, Lcom/my/target/cc;->ju:I

    packed-switch v0, :pswitch_data_0

    .line 542
    const-string v0, "idle"

    :goto_0
    return-object v0

    .line 532
    :pswitch_0
    const-string v0, "preparing"

    goto :goto_0

    .line 534
    :pswitch_1
    const-string v0, "wait"

    goto :goto_0

    .line 536
    :pswitch_2
    const-string v0, "playing"

    goto :goto_0

    .line 538
    :pswitch_3
    const-string v0, "paused"

    goto :goto_0

    .line 540
    :pswitch_4
    const-string v0, "stopped"

    goto :goto_0

    .line 529
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private c(Landroid/media/MediaPlayer;)V
    .locals 1

    .prologue
    .line 573
    const/4 v0, 0x2

    iput v0, p0, Lcom/my/target/cc;->ju:I

    .line 574
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->pause()V

    .line 575
    return-void
.end method

.method static synthetic c(Lcom/my/target/cc;)Z
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/my/target/cc;->isVisible()Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/my/target/cc;)Landroid/view/Surface;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/my/target/cc;->jz:Landroid/view/Surface;

    return-object v0
.end method

.method private d(Landroid/media/MediaPlayer;)V
    .locals 1

    .prologue
    .line 579
    const/4 v0, 0x3

    iput v0, p0, Lcom/my/target/cc;->ju:I

    .line 580
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->start()V

    .line 581
    iget v0, p0, Lcom/my/target/cc;->jA:I

    if-lez v0, :cond_1

    .line 583
    iget v0, p0, Lcom/my/target/cc;->jA:I

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 584
    const/4 v0, 0x0

    iput v0, p0, Lcom/my/target/cc;->jA:I

    .line 590
    :goto_0
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    if-eqz v0, :cond_0

    .line 592
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    invoke-interface {v0}, Lcom/my/target/cc$a;->bs()V

    .line 594
    :cond_0
    return-void

    .line 588
    :cond_1
    iget v0, p0, Lcom/my/target/cc;->jv:I

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->seekTo(I)V

    goto :goto_0
.end method

.method private d(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 514
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VideoTextureView: Playing video "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/my/target/cc;->bp()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " dims "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/my/target/cc;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/my/target/cc;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 516
    invoke-virtual {p0}, Lcom/my/target/cc;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 518
    iget-object v0, p0, Lcom/my/target/cc;->jz:Landroid/view/Surface;

    if-nez v0, :cond_0

    .line 520
    new-instance v0, Landroid/view/Surface;

    invoke-virtual {p0}, Lcom/my/target/cc;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, Lcom/my/target/cc;->jz:Landroid/view/Surface;

    .line 522
    :cond_0
    iget-object v0, p0, Lcom/my/target/cc;->jz:Landroid/view/Surface;

    invoke-virtual {p0, v0, p1}, Lcom/my/target/cc;->a(Landroid/view/Surface;Landroid/net/Uri;)V

    .line 524
    :cond_1
    new-instance v0, Lcom/my/target/cc$c;

    invoke-direct {v0, p0, p1}, Lcom/my/target/cc$c;-><init>(Lcom/my/target/cc;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/my/target/cc;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 525
    return-void
.end method

.method static synthetic e(Lcom/my/target/cc;)Lcom/my/target/cc$a;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    return-object v0
.end method

.method private isVisible()Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 548
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 549
    invoke-virtual {p0, v1}, Lcom/my/target/cc;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 551
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    mul-int/2addr v1, v2

    int-to-double v2, v1

    .line 552
    invoke-virtual {p0}, Lcom/my/target/cc;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/my/target/cc;->getHeight()I

    move-result v4

    mul-int/2addr v1, v4

    int-to-double v4, v1

    .line 554
    const-wide v6, 0x3fe3333340000000L    # 0.6000000238418579

    mul-double/2addr v4, v6

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    .line 557
    :cond_0
    return v0
.end method

.method private static j(I)F
    .locals 2

    .prologue
    .line 28
    int-to-float v0, p0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method protected a(Landroid/media/MediaPlayer;)V
    .locals 1

    .prologue
    .line 491
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->start()V

    .line 492
    iget v0, p0, Lcom/my/target/cc;->jA:I

    if-lez v0, :cond_0

    .line 494
    iget v0, p0, Lcom/my/target/cc;->jA:I

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 495
    const/4 v0, 0x0

    iput v0, p0, Lcom/my/target/cc;->jA:I

    .line 497
    :cond_0
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    if-eqz v0, :cond_1

    .line 499
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    invoke-interface {v0}, Lcom/my/target/cc$a;->br()V

    .line 501
    :cond_1
    const/4 v0, 0x3

    iput v0, p0, Lcom/my/target/cc;->ju:I

    .line 502
    return-void
.end method

.method protected a(Landroid/view/Surface;)V
    .locals 1

    .prologue
    .line 426
    const-string v0, "VideoTextureView: Resume textureView"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 427
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 429
    invoke-virtual {p0}, Lcom/my/target/cc;->isMuted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 431
    invoke-virtual {p0}, Lcom/my/target/cc;->bj()V

    .line 437
    :goto_0
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    .line 438
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-direct {p0, v0}, Lcom/my/target/cc;->d(Landroid/media/MediaPlayer;)V

    .line 445
    :goto_1
    return-void

    .line 435
    :cond_0
    invoke-virtual {p0}, Lcom/my/target/cc;->bk()V

    goto :goto_0

    .line 443
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/my/target/cc;->ju:I

    goto :goto_1
.end method

.method protected a(Landroid/view/Surface;Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 361
    const/4 v0, 0x0

    iput v0, p0, Lcom/my/target/cc;->jw:I

    .line 362
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VideoTextureView: call play state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/my/target/cc;->bp()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " url = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 363
    if-nez p1, :cond_1

    .line 422
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 367
    :cond_1
    iget v0, p0, Lcom/my/target/cc;->ju:I

    packed-switch v0, :pswitch_data_0

    .line 400
    :cond_2
    const/4 v0, 0x1

    iput v0, p0, Lcom/my/target/cc;->ju:I

    .line 402
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    .line 403
    new-instance v0, Lcom/my/target/cc$d;

    invoke-direct {v0, p0}, Lcom/my/target/cc$d;-><init>(Lcom/my/target/cc;)V

    .line 404
    iget-object v1, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 405
    iget-object v1, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 406
    iget-object v1, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 408
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    .line 412
    :try_start_0
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Lcom/my/target/cc;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 413
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 415
    :catch_0
    move-exception v0

    .line 417
    iget-object v1, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    if-eqz v1, :cond_0

    .line 419
    iget-object v1, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/my/target/cc$a;->L(Ljava/lang/String;)V

    goto :goto_0

    .line 372
    :pswitch_1
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 374
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    goto :goto_0

    .line 379
    :pswitch_2
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    .line 381
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VideoTextureView: trying to start paused mediaplayer, state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/my/target/cc;->bp()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 382
    invoke-virtual {p0, p1}, Lcom/my/target/cc;->a(Landroid/view/Surface;)V

    goto :goto_0

    .line 387
    :pswitch_3
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    .line 389
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VideoTextureView: trying to RESUMING mediaplayer, state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/my/target/cc;->bp()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 390
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    .line 391
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    invoke-interface {v0}, Lcom/my/target/cc$a;->bt()V

    goto/16 :goto_0

    .line 367
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lcom/my/target/common/models/VideoData;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 171
    iget v0, p0, Lcom/my/target/cc;->ju:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/my/target/cc;->ju:I

    if-ne v0, v2, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/my/target/cc;->jy:Lcom/my/target/common/models/VideoData;

    if-ne v0, p1, :cond_1

    .line 173
    invoke-virtual {p0}, Lcom/my/target/cc;->resume()V

    .line 193
    :goto_0
    return-void

    .line 177
    :cond_1
    iput-object p1, p0, Lcom/my/target/cc;->jy:Lcom/my/target/common/models/VideoData;

    .line 178
    if-eqz p2, :cond_2

    .line 180
    iput v2, p0, Lcom/my/target/cc;->ju:I

    .line 183
    :cond_2
    invoke-virtual {p1}, Lcom/my/target/common/models/VideoData;->getData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 185
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "file://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/my/target/common/models/VideoData;->getData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 191
    :goto_1
    invoke-virtual {p0, v0}, Lcom/my/target/cc;->c(Landroid/net/Uri;)V

    goto :goto_0

    .line 189
    :cond_3
    invoke-virtual {p1}, Lcom/my/target/common/models/VideoData;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_1
.end method

.method public bi()V
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x2

    iput v0, p0, Lcom/my/target/cc;->ju:I

    .line 141
    return-void
.end method

.method public bj()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 197
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/cc;->jt:Z

    .line 198
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1, v1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    if-eqz v0, :cond_1

    .line 204
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    invoke-interface {v0, v1}, Lcom/my/target/cc$a;->e(F)V

    .line 206
    :cond_1
    return-void
.end method

.method public bk()V
    .locals 3

    .prologue
    .line 210
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/cc;->jt:Z

    .line 211
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    iget v1, p0, Lcom/my/target/cc;->js:F

    iget v2, p0, Lcom/my/target/cc;->js:F

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    if-eqz v0, :cond_1

    .line 217
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    iget v1, p0, Lcom/my/target/cc;->js:F

    invoke-interface {v0, v1}, Lcom/my/target/cc$a;->e(F)V

    .line 219
    :cond_1
    return-void
.end method

.method public bl()V
    .locals 2

    .prologue
    const v1, 0x3e99999a    # 0.3f

    .line 223
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1, v1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 227
    :cond_0
    return-void
.end method

.method public bm()V
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 307
    const-string v0, "VideoTextureView: Pause textureView until available"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 308
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-direct {p0, v0}, Lcom/my/target/cc;->c(Landroid/media/MediaPlayer;)V

    .line 310
    :cond_0
    return-void
.end method

.method protected bn()V
    .locals 3

    .prologue
    const/16 v2, 0x32

    const/4 v1, 0x1

    .line 449
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/cc;->jx:Landroid/graphics/Bitmap;

    .line 450
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 452
    const/4 v0, 0x3

    iput v0, p0, Lcom/my/target/cc;->ju:I

    .line 454
    iget v0, p0, Lcom/my/target/cc;->jw:I

    if-lt v0, v2, :cond_1

    .line 456
    const-string v0, "VideoTextureView: lag common"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 457
    invoke-virtual {p0, v1}, Lcom/my/target/cc;->k(Z)V

    .line 487
    :cond_0
    :goto_0
    return-void

    .line 459
    :cond_1
    iget v0, p0, Lcom/my/target/cc;->jv:I

    iget-object v1, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 461
    const/4 v0, 0x0

    iput v0, p0, Lcom/my/target/cc;->jw:I

    .line 462
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/my/target/cc;->jv:I

    .line 463
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    .line 464
    iget-object v1, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    if-eqz v1, :cond_0

    .line 466
    iget-object v1, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    iget v2, p0, Lcom/my/target/cc;->jv:I

    invoke-static {v2}, Lcom/my/target/cc;->j(I)F

    move-result v2

    .line 467
    invoke-static {v0}, Lcom/my/target/cc;->j(I)F

    move-result v0

    .line 466
    invoke-interface {v1, v2, v0}, Lcom/my/target/cc$a;->b(FF)V

    goto :goto_0

    .line 472
    :cond_2
    iget v0, p0, Lcom/my/target/cc;->jw:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/my/target/cc;->jw:I

    goto :goto_0

    .line 475
    :cond_3
    iget v0, p0, Lcom/my/target/cc;->ju:I

    if-ne v0, v1, :cond_0

    .line 477
    iget v0, p0, Lcom/my/target/cc;->jw:I

    if-lt v0, v2, :cond_4

    .line 479
    const-string v0, "VideoTextureView: lag on preparing"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 480
    invoke-virtual {p0, v1}, Lcom/my/target/cc;->k(Z)V

    goto :goto_0

    .line 484
    :cond_4
    iget v0, p0, Lcom/my/target/cc;->jw:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/my/target/cc;->jw:I

    goto :goto_0
.end method

.method public c(Landroid/net/Uri;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 158
    iput-object p1, p0, Lcom/my/target/cc;->uri:Landroid/net/Uri;

    .line 159
    invoke-virtual {p0, v0}, Lcom/my/target/cc;->k(Z)V

    .line 160
    iput v0, p0, Lcom/my/target/cc;->ju:I

    .line 162
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    invoke-interface {v0}, Lcom/my/target/cc$a;->bu()V

    .line 166
    :cond_0
    invoke-direct {p0, p1}, Lcom/my/target/cc;->d(Landroid/net/Uri;)V

    .line 167
    return-void
.end method

.method public getScreenShot()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/my/target/cc;->jx:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method getSeekToTime()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/my/target/cc;->jA:I

    return v0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/my/target/cc;->ju:I

    return v0
.end method

.method public getTimeElapsed()F
    .locals 2

    .prologue
    .line 92
    :try_start_0
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    .line 100
    :goto_0
    return v0

    .line 97
    :catch_0
    move-exception v0

    .line 100
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVideoDuration()F
    .locals 2

    .prologue
    .line 107
    :try_start_0
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    .line 115
    :goto_0
    return v0

    .line 112
    :catch_0
    move-exception v0

    .line 115
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVideoState()I
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lcom/my/target/cc;->ju:I

    return v0
.end method

.method public isMuted()Z
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/my/target/cc;->jt:Z

    return v0
.end method

.method public j(Z)V
    .locals 0

    .prologue
    .line 314
    if-eqz p1, :cond_0

    .line 316
    invoke-direct {p0}, Lcom/my/target/cc;->bo()V

    .line 318
    :cond_0
    invoke-virtual {p0}, Lcom/my/target/cc;->pause()V

    .line 319
    return-void
.end method

.method public k(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 333
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VideoTextureView: call stop, state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/my/target/cc;->bp()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " show play "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 335
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    if-eqz v0, :cond_0

    .line 337
    if-eqz p1, :cond_0

    .line 339
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    invoke-interface {v0}, Lcom/my/target/cc$a;->bq()V

    .line 342
    :cond_0
    iput v2, p0, Lcom/my/target/cc;->jv:I

    .line 343
    iput v2, p0, Lcom/my/target/cc;->jA:I

    .line 345
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 347
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 348
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 349
    iput-object v3, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    .line 351
    :cond_1
    iget-object v0, p0, Lcom/my/target/cc;->jz:Landroid/view/Surface;

    if-eqz v0, :cond_2

    .line 353
    iget-object v0, p0, Lcom/my/target/cc;->jz:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 354
    iput-object v3, p0, Lcom/my/target/cc;->jz:Landroid/view/Surface;

    .line 356
    :cond_2
    const/4 v0, 0x5

    iput v0, p0, Lcom/my/target/cc;->ju:I

    .line 357
    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 268
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    invoke-static {v0}, Lcom/my/target/cc;->j(I)F

    move-result v0

    .line 269
    iget-object v1, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    if-eqz v1, :cond_0

    .line 271
    iget-object v1, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    invoke-interface {v1, v0, v0}, Lcom/my/target/cc$a;->b(FF)V

    .line 272
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    invoke-interface {v0}, Lcom/my/target/cc$a;->bv()V

    .line 274
    :cond_0
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 4
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/4 v3, 0x1

    .line 278
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Video error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/my/target/cc$a;->L(Ljava/lang/String;)V

    .line 282
    :cond_0
    invoke-virtual {p0, v3}, Lcom/my/target/cc;->k(Z)V

    .line 283
    return v3
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 231
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VideoTextureView: call on prepared, state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/my/target/cc;->bp()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 233
    iget v0, p0, Lcom/my/target/cc;->ju:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 235
    invoke-virtual {p0}, Lcom/my/target/cc;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 237
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VideoTextureView: call mediaplayer to start visibility "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 238
    invoke-virtual {p0}, Lcom/my/target/cc;->getVisibility()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " dims = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/my/target/cc;->getHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/my/target/cc;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 237
    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 239
    iget-object v0, p0, Lcom/my/target/cc;->jz:Landroid/view/Surface;

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    .line 240
    invoke-virtual {p0}, Lcom/my/target/cc;->isMuted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 242
    invoke-virtual {p0}, Lcom/my/target/cc;->bj()V

    .line 248
    :goto_0
    invoke-virtual {p0, p1}, Lcom/my/target/cc;->a(Landroid/media/MediaPlayer;)V

    .line 249
    iget v0, p0, Lcom/my/target/cc;->jA:I

    if-lez v0, :cond_2

    .line 251
    iget v0, p0, Lcom/my/target/cc;->jA:I

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 252
    const/4 v0, 0x0

    iput v0, p0, Lcom/my/target/cc;->jA:I

    .line 264
    :cond_0
    :goto_1
    return-void

    .line 246
    :cond_1
    invoke-virtual {p0}, Lcom/my/target/cc;->bk()V

    goto :goto_0

    .line 254
    :cond_2
    iget v0, p0, Lcom/my/target/cc;->jv:I

    if-eqz v0, :cond_0

    .line 256
    iget v0, p0, Lcom/my/target/cc;->jv:I

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->seekTo(I)V

    goto :goto_1

    .line 261
    :cond_3
    const-string v0, "VideoTextureView: mediaplayer is ready, but surface isn\'t available"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 323
    const/4 v0, 0x4

    iput v0, p0, Lcom/my/target/cc;->ju:I

    .line 324
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VideoTextureView: Pause textureView, state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/my/target/cc;->bp()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 327
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-direct {p0, v0}, Lcom/my/target/cc;->b(Landroid/media/MediaPlayer;)V

    .line 329
    :cond_0
    return-void
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/my/target/cc;->uri:Landroid/net/Uri;

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    invoke-interface {v0}, Lcom/my/target/cc$a;->bu()V

    .line 151
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/my/target/cc;->ju:I

    .line 152
    iget-object v0, p0, Lcom/my/target/cc;->uri:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/my/target/cc;->d(Landroid/net/Uri;)V

    .line 154
    :cond_1
    return-void
.end method

.method public seekTo(I)V
    .locals 2
    .param p1, "millis"    # I

    .prologue
    .line 288
    iput p1, p0, Lcom/my/target/cc;->jA:I

    .line 289
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/my/target/cc;->ju:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 293
    :try_start_0
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 294
    const/4 v0, 0x0

    iput v0, p0, Lcom/my/target/cc;->jA:I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 301
    :cond_0
    :goto_0
    return-void

    .line 296
    :catch_0
    move-exception v0

    .line 298
    const-string v0, "Cannot seek: illegal state"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method setMediaPlayer(Landroid/media/MediaPlayer;)V
    .locals 0
    .param p1, "mediaPlayer"    # Landroid/media/MediaPlayer;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    .line 67
    return-void
.end method

.method public setState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/my/target/cc;->ju:I

    .line 55
    return-void
.end method

.method public setVideoListener(Lcom/my/target/cc$a;)V
    .locals 0
    .param p1, "videoListener"    # Lcom/my/target/cc$a;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    .line 136
    return-void
.end method

.method public setVolume(F)V
    .locals 1
    .param p1, "volume"    # F

    .prologue
    .line 76
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/my/target/cc;->jt:Z

    .line 77
    iput p1, p0, Lcom/my/target/cc;->js:F

    .line 78
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/my/target/cc;->jr:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1, p1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 81
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/my/target/cc;->jq:Lcom/my/target/cc$a;

    invoke-interface {v0, p1}, Lcom/my/target/cc$a;->e(F)V

    .line 86
    :cond_0
    return-void

    .line 76
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
