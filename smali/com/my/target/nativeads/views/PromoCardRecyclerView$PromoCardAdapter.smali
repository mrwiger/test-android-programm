.class public abstract Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "PromoCardRecyclerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/nativeads/views/PromoCardRecyclerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "PromoCardAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private cardClickListener:Landroid/view/View$OnClickListener;

.field private final nativePromoCards:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/my/target/nativeads/banners/NativePromoCard;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 215
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 216
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->nativePromoCards:Ljava/util/List;

    .line 217
    return-void
.end method

.method private setBannerToView(Lcom/my/target/nativeads/banners/NativePromoCard;Lcom/my/target/nativeads/views/PromoCardView;)V
    .locals 3
    .param p1, "nativePromoCard"    # Lcom/my/target/nativeads/banners/NativePromoCard;
    .param p2, "promoCardView"    # Lcom/my/target/nativeads/views/PromoCardView;

    .prologue
    .line 300
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoCard;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 302
    invoke-interface {p2}, Lcom/my/target/nativeads/views/PromoCardView;->getMediaAdView()Lcom/my/target/nativeads/views/MediaAdView;

    move-result-object v0

    .line 303
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoCard;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/common/models/ImageData;->getWidth()I

    move-result v1

    .line 304
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoCard;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/my/target/common/models/ImageData;->getHeight()I

    move-result v2

    .line 303
    invoke-virtual {v0, v1, v2}, Lcom/my/target/nativeads/views/MediaAdView;->setPlaceHolderDimension(II)V

    .line 306
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoCard;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 308
    invoke-interface {p2}, Lcom/my/target/nativeads/views/PromoCardView;->getMediaAdView()Lcom/my/target/nativeads/views/MediaAdView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    .line 309
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoCard;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 318
    :cond_0
    :goto_0
    invoke-interface {p2}, Lcom/my/target/nativeads/views/PromoCardView;->getTitleTextView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoCard;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 319
    invoke-interface {p2}, Lcom/my/target/nativeads/views/PromoCardView;->getDescriptionTextView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoCard;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 320
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoCard;->getCtaText()Ljava/lang/String;

    move-result-object v0

    .line 321
    invoke-interface {p2}, Lcom/my/target/nativeads/views/PromoCardView;->getCtaButtonView()Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 322
    invoke-interface {p2}, Lcom/my/target/nativeads/views/PromoCardView;->getCtaButtonView()Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 323
    return-void

    .line 313
    :cond_1
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoCard;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 314
    invoke-interface {p2}, Lcom/my/target/nativeads/views/PromoCardView;->getMediaAdView()Lcom/my/target/nativeads/views/MediaAdView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/nativeads/views/MediaAdView;->getImageView()Landroid/widget/ImageView;

    move-result-object v1

    .line 313
    invoke-static {v0, v1}, Lcom/my/target/ch;->a(Lcom/my/target/common/models/ImageData;Landroid/widget/ImageView;)V

    goto :goto_0
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->nativePromoCards:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 293
    invoke-virtual {p0}, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->notifyDataSetChanged()V

    .line 294
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->cardClickListener:Landroid/view/View$OnClickListener;

    .line 295
    return-void
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->nativePromoCards:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getNativePromoCards()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/nativeads/banners/NativePromoCard;",
            ">;"
        }
    .end annotation

    .prologue
    .line 234
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->nativePromoCards:Ljava/util/List;

    return-object v0
.end method

.method public abstract getPromoCardView()Lcom/my/target/nativeads/views/PromoCardView;
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 209
    check-cast p1, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->onBindViewHolder(Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardViewHolder;I)V
    .locals 3
    .param p1, "holder"    # Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 246
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->nativePromoCards:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->nativePromoCards:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/nativeads/banners/NativePromoCard;

    .line 249
    if-eqz v0, :cond_0

    .line 251
    invoke-virtual {p1}, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardViewHolder;->getPromoCardView()Lcom/my/target/nativeads/views/PromoCardView;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->setBannerToView(Lcom/my/target/nativeads/banners/NativePromoCard;Lcom/my/target/nativeads/views/PromoCardView;)V

    .line 254
    :cond_0
    invoke-virtual {p1}, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardViewHolder;->getPromoCardView()Lcom/my/target/nativeads/views/PromoCardView;

    move-result-object v0

    invoke-interface {v0}, Lcom/my/target/nativeads/views/PromoCardView;->getView()Landroid/view/View;

    move-result-object v0

    const-string v1, "card_"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 255
    invoke-virtual {p1}, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardViewHolder;->getPromoCardView()Lcom/my/target/nativeads/views/PromoCardView;

    move-result-object v0

    invoke-interface {v0}, Lcom/my/target/nativeads/views/PromoCardView;->getView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->cardClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 256
    invoke-virtual {p1}, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardViewHolder;->getPromoCardView()Lcom/my/target/nativeads/views/PromoCardView;

    move-result-object v0

    invoke-interface {v0}, Lcom/my/target/nativeads/views/PromoCardView;->getCtaButtonView()Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->cardClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 257
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 209
    invoke-virtual {p0, p1, p2}, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardViewHolder;
    .locals 2

    .prologue
    .line 240
    new-instance v0, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardViewHolder;

    invoke-virtual {p0}, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->getPromoCardView()Lcom/my/target/nativeads/views/PromoCardView;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardViewHolder;-><init>(Lcom/my/target/nativeads/views/PromoCardView;)V

    return-object v0
.end method

.method public bridge synthetic onViewRecycled(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 0

    .prologue
    .line 209
    check-cast p1, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardViewHolder;

    invoke-virtual {p0, p1}, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->onViewRecycled(Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardViewHolder;)V

    return-void
.end method

.method public onViewRecycled(Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardViewHolder;)V
    .locals 4
    .param p1, "holder"    # Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardViewHolder;

    .prologue
    const/4 v3, 0x0

    .line 262
    invoke-virtual {p1}, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardViewHolder;->getLayoutPosition()I

    move-result v1

    .line 263
    invoke-virtual {p1}, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardViewHolder;->getPromoCardView()Lcom/my/target/nativeads/views/PromoCardView;

    move-result-object v0

    invoke-interface {v0}, Lcom/my/target/nativeads/views/PromoCardView;->getMediaAdView()Lcom/my/target/nativeads/views/MediaAdView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    check-cast v0, Lcom/my/target/bv;

    .line 264
    invoke-virtual {v0, v3}, Lcom/my/target/bv;->setImageData(Lcom/my/target/common/models/ImageData;)V

    .line 265
    if-lez v1, :cond_0

    iget-object v2, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->nativePromoCards:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 267
    iget-object v2, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->nativePromoCards:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/my/target/nativeads/banners/NativePromoCard;

    .line 268
    if-eqz v1, :cond_0

    .line 270
    invoke-virtual {v1}, Lcom/my/target/nativeads/banners/NativePromoCard;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    .line 271
    if-eqz v1, :cond_0

    .line 273
    invoke-static {v1, v0}, Lcom/my/target/ch;->b(Lcom/my/target/common/models/ImageData;Landroid/widget/ImageView;)V

    .line 277
    :cond_0
    invoke-virtual {p1}, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardViewHolder;->getPromoCardView()Lcom/my/target/nativeads/views/PromoCardView;

    move-result-object v0

    invoke-interface {v0}, Lcom/my/target/nativeads/views/PromoCardView;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 278
    invoke-virtual {p1}, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardViewHolder;->getPromoCardView()Lcom/my/target/nativeads/views/PromoCardView;

    move-result-object v0

    invoke-interface {v0}, Lcom/my/target/nativeads/views/PromoCardView;->getCtaButtonView()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 279
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$Adapter;->onViewRecycled(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 280
    return-void
.end method

.method public setCards(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/nativeads/banners/NativePromoCard;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 221
    .local p1, "nativePromoCards":Ljava/util/List;, "Ljava/util/List<Lcom/my/target/nativeads/banners/NativePromoCard;>;"
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->nativePromoCards:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 222
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->nativePromoCards:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 223
    invoke-virtual {p0}, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->notifyDataSetChanged()V

    .line 224
    return-void
.end method

.method public setClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "cardClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 228
    iput-object p1, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->cardClickListener:Landroid/view/View$OnClickListener;

    .line 229
    return-void
.end method
