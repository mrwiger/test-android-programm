.class public Lcom/yandex/metrica/impl/ob/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/yandex/metrica/impl/ob/w;


# instance fields
.field private final a:Z

.field private b:Z

.field private c:Z

.field private final d:Landroid/os/HandlerThread;

.field private final e:Landroid/os/Handler;

.field private final f:Landroid/content/Context;

.field private final g:Lcom/yandex/metrica/impl/ob/t;

.field private h:Lcom/yandex/metrica/impl/ob/dd;

.field private i:Lcom/yandex/metrica/impl/ob/df;

.field private j:Lcom/yandex/metrica/impl/ob/db;

.field private k:Lcom/yandex/metrica/impl/ob/dg;

.field private l:Lcom/yandex/metrica/CounterConfiguration;

.field private final m:Lcom/yandex/metrica/impl/bb;

.field private n:Lcom/yandex/metrica/impl/bk;

.field private o:Lcom/yandex/metrica/impl/ob/co;

.field private p:Lcom/yandex/metrica/impl/ob/x;

.field private q:Lcom/yandex/metrica/impl/a;

.field private r:Lcom/yandex/metrica/impl/ob/s;

.field private s:J

.field private t:J

.field private u:I

.field private v:I

.field private volatile w:Lcom/yandex/metrica/impl/ob/bl;

.field private final x:Lcom/yandex/metrica/impl/utils/l;

.field private y:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/yandex/metrica/impl/ob/t;Lcom/yandex/metrica/CounterConfiguration;Lcom/yandex/metrica/impl/ob/s;)V
    .locals 7

    .prologue
    .line 101
    new-instance v6, Lcom/yandex/metrica/impl/bb;

    invoke-direct {v6}, Lcom/yandex/metrica/impl/bb;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/yandex/metrica/impl/ob/v;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/yandex/metrica/impl/ob/t;Lcom/yandex/metrica/CounterConfiguration;Lcom/yandex/metrica/impl/ob/s;Lcom/yandex/metrica/impl/bb;)V

    .line 102
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/yandex/metrica/impl/ob/t;Lcom/yandex/metrica/CounterConfiguration;Lcom/yandex/metrica/impl/ob/s;Lcom/yandex/metrica/impl/bb;)V
    .locals 6

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/yandex/metrica/impl/ob/v;->b:Z

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/yandex/metrica/impl/ob/v;->c:Z

    .line 90
    new-instance v0, Lcom/yandex/metrica/impl/utils/l;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/utils/l;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->x:Lcom/yandex/metrica/impl/utils/l;

    .line 506
    new-instance v0, Lcom/yandex/metrica/impl/ob/v$2;

    invoke-direct {v0, p0}, Lcom/yandex/metrica/impl/ob/v$2;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->y:Ljava/lang/Runnable;

    .line 112
    iput-object p6, p0, Lcom/yandex/metrica/impl/ob/v;->m:Lcom/yandex/metrica/impl/bb;

    .line 113
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->f:Landroid/content/Context;

    .line 114
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/v;->g:Lcom/yandex/metrica/impl/ob/t;

    .line 115
    iput-object p4, p0, Lcom/yandex/metrica/impl/ob/v;->l:Lcom/yandex/metrica/CounterConfiguration;

    .line 1187
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/v;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1208
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/cq;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/cq;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/v;->g:Lcom/yandex/metrica/impl/ob/t;

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/cq;->b(Lcom/yandex/metrica/impl/ob/t;)Lcom/yandex/metrica/impl/ob/cr;

    move-result-object v0

    .line 1209
    new-instance v1, Lcom/yandex/metrica/impl/ob/dd;

    invoke-direct {v1, v0}, Lcom/yandex/metrica/impl/ob/dd;-><init>(Lcom/yandex/metrica/impl/ob/cr;)V

    iput-object v1, p0, Lcom/yandex/metrica/impl/ob/v;->h:Lcom/yandex/metrica/impl/ob/dd;

    .line 1210
    new-instance v1, Lcom/yandex/metrica/impl/ob/db;

    invoke-direct {v1, v0}, Lcom/yandex/metrica/impl/ob/db;-><init>(Lcom/yandex/metrica/impl/ob/cr;)V

    iput-object v1, p0, Lcom/yandex/metrica/impl/ob/v;->j:Lcom/yandex/metrica/impl/ob/db;

    .line 1191
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/cq;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/cq;

    move-result-object v0

    .line 1192
    new-instance v1, Lcom/yandex/metrica/impl/ob/df;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/cq;->b()Lcom/yandex/metrica/impl/ob/cr;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/yandex/metrica/impl/ob/df;-><init>(Lcom/yandex/metrica/impl/ob/cr;)V

    iput-object v1, p0, Lcom/yandex/metrica/impl/ob/v;->i:Lcom/yandex/metrica/impl/ob/df;

    .line 1194
    new-instance v1, Lcom/yandex/metrica/impl/ob/dg;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/cq;->d()Lcom/yandex/metrica/impl/ob/cr;

    move-result-object v0

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/v;->g:Lcom/yandex/metrica/impl/ob/t;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/t;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/yandex/metrica/impl/ob/dg;-><init>(Lcom/yandex/metrica/impl/ob/cr;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/yandex/metrica/impl/ob/v;->k:Lcom/yandex/metrica/impl/ob/dg;

    .line 1518
    invoke-static {}, Lcom/yandex/metrica/YandexMetrica;->getLibraryApiLevel()I

    move-result v0

    .line 1519
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/v;->K()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/v;->h:Lcom/yandex/metrica/impl/ob/dd;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/dd;->d()J

    move-result-wide v2

    int-to-long v4, v0

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 1520
    new-instance v1, Lcom/yandex/metrica/impl/ob/fd;

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->E()Lcom/yandex/metrica/impl/ob/fe;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/yandex/metrica/impl/ob/fd;-><init>(Lcom/yandex/metrica/impl/ob/fe;)V

    .line 1521
    new-instance v2, Lcom/yandex/metrica/impl/ob/u;

    invoke-direct {v2, p0, v1}, Lcom/yandex/metrica/impl/ob/u;-><init>(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/fd;)V

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/u;->a()V

    .line 1522
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/v;->h:Lcom/yandex/metrica/impl/ob/dd;

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Lcom/yandex/metrica/impl/ob/dd;->e(J)Lcom/yandex/metrica/impl/ob/dd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dd;->g()V

    .line 121
    :cond_1
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/v;->K()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 122
    new-instance v0, Lcom/yandex/metrica/impl/ob/co;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/v;->f:Landroid/content/Context;

    invoke-static {v1}, Lcom/yandex/metrica/impl/ob/cq;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/cq;

    move-result-object v1

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/v;->g:Lcom/yandex/metrica/impl/ob/t;

    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/ob/cq;->a(Lcom/yandex/metrica/impl/ob/t;)Lcom/yandex/metrica/impl/ob/cp;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/yandex/metrica/impl/ob/co;-><init>(Lcom/yandex/metrica/impl/ob/w;Lcom/yandex/metrica/impl/ob/cp;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->o:Lcom/yandex/metrica/impl/ob/co;

    .line 124
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->h:Lcom/yandex/metrica/impl/ob/dd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/dd;->a(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/yandex/metrica/impl/ob/v;->s:J

    .line 125
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->h:Lcom/yandex/metrica/impl/ob/dd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/dd;->b(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/yandex/metrica/impl/ob/v;->t:J

    .line 126
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->h:Lcom/yandex/metrica/impl/ob/dd;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dd;->a(I)I

    move-result v0

    iput v0, p0, Lcom/yandex/metrica/impl/ob/v;->u:I

    .line 127
    invoke-virtual {p3}, Lcom/yandex/metrica/impl/ob/t;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/yandex/metrica/impl/bl;->c(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/yandex/metrica/impl/ob/v;->v:I

    .line 129
    new-instance v0, Lcom/yandex/metrica/impl/ob/bl;

    new-instance v1, Lcom/yandex/metrica/impl/ob/v$1;

    invoke-direct {v1, p0}, Lcom/yandex/metrica/impl/ob/v$1;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    invoke-direct {v0, p0, v1}, Lcom/yandex/metrica/impl/ob/bl;-><init>(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/bl$a;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->w:Lcom/yandex/metrica/impl/ob/bl;

    .line 135
    iput-object p5, p0, Lcom/yandex/metrica/impl/ob/v;->r:Lcom/yandex/metrica/impl/ob/s;

    .line 136
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->r:Lcom/yandex/metrica/impl/ob/s;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/v;->h:Lcom/yandex/metrica/impl/ob/dd;

    invoke-virtual {v0, p0, v1}, Lcom/yandex/metrica/impl/ob/s;->a(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/dd;)Lcom/yandex/metrica/impl/a;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->q:Lcom/yandex/metrica/impl/a;

    .line 137
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->q()Lcom/yandex/metrica/impl/utils/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/utils/l;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 138
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->q()Lcom/yandex/metrica/impl/utils/l;

    move-result-object v0

    const-string v1, "Read app environment for component %s. Value: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/yandex/metrica/impl/ob/v;->g:Lcom/yandex/metrica/impl/ob/t;

    invoke-virtual {v4}, Lcom/yandex/metrica/impl/ob/t;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/yandex/metrica/impl/ob/v;->q:Lcom/yandex/metrica/impl/a;

    invoke-virtual {v4}, Lcom/yandex/metrica/impl/a;->b()Lcom/yandex/metrica/impl/a$a;

    move-result-object v4

    iget-object v4, v4, Lcom/yandex/metrica/impl/a$a;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/yandex/metrica/impl/utils/l;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 142
    :cond_2
    invoke-virtual {p3}, Lcom/yandex/metrica/impl/ob/t;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/yandex/metrica/impl/utils/j;->b(Ljava/lang/String;)Landroid/os/HandlerThread;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->d:Landroid/os/HandlerThread;

    .line 143
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->d:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 144
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/v;->d:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->e:Landroid/os/Handler;

    .line 146
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->m:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v0, p0}, Lcom/yandex/metrica/impl/bb;->a(Lcom/yandex/metrica/impl/ob/v;)V

    .line 148
    new-instance v0, Lcom/yandex/metrica/impl/bk;

    invoke-direct {v0, p0, p2}, Lcom/yandex/metrica/impl/bk;-><init>(Lcom/yandex/metrica/impl/ob/v;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->n:Lcom/yandex/metrica/impl/bk;

    .line 149
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->o:Lcom/yandex/metrica/impl/ob/co;

    if-eqz v0, :cond_3

    .line 151
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->o:Lcom/yandex/metrica/impl/ob/co;

    invoke-virtual {v0, p0}, Lcom/yandex/metrica/impl/ob/co;->a(Lcom/yandex/metrica/impl/ob/w;)V

    .line 154
    :cond_3
    new-instance v0, Lcom/yandex/metrica/impl/ob/aa;

    invoke-direct {v0, p0}, Lcom/yandex/metrica/impl/ob/aa;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    .line 155
    new-instance v1, Lcom/yandex/metrica/impl/ob/ad;

    invoke-direct {v1, v0}, Lcom/yandex/metrica/impl/ob/ad;-><init>(Lcom/yandex/metrica/impl/ob/ac;)V

    iput-object v1, p0, Lcom/yandex/metrica/impl/ob/v;->p:Lcom/yandex/metrica/impl/ob/x;

    .line 157
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/v;->g:Lcom/yandex/metrica/impl/ob/t;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/t;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/yandex/metrica/impl/ob/v;->a:Z

    .line 160
    return-void
.end method

.method private K()Z
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->g:Lcom/yandex/metrica/impl/ob/t;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/t;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private L()V
    .locals 4

    .prologue
    .line 413
    .line 4029
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 413
    iput-wide v0, p0, Lcom/yandex/metrica/impl/ob/v;->t:J

    .line 414
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->h:Lcom/yandex/metrica/impl/ob/dd;

    iget-wide v2, p0, Lcom/yandex/metrica/impl/ob/v;->t:J

    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/dd;->d(J)Lcom/yandex/metrica/impl/ob/dd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dd;->g()V

    .line 415
    return-void
.end method

.method private b(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ob/bm;)V
    .locals 2

    .prologue
    .line 232
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/yandex/metrica/impl/i;->a(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->q:Lcom/yandex/metrica/impl/a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/a;->b()Lcom/yandex/metrica/impl/a$a;

    move-result-object v0

    .line 236
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/v;->o:Lcom/yandex/metrica/impl/ob/co;

    invoke-virtual {v1, p1, p2, v0}, Lcom/yandex/metrica/impl/ob/co;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ob/bm;Lcom/yandex/metrica/impl/a$a;)V

    .line 237
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->n:Lcom/yandex/metrica/impl/bk;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/bk;->b()V

    .line 238
    return-void
.end method


# virtual methods
.method public A()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 438
    .line 6029
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 5424
    iget-wide v4, p0, Lcom/yandex/metrica/impl/ob/v;->s:J

    sub-long/2addr v2, v4

    sget-wide v4, Lcom/yandex/metrica/impl/ob/bj;->a:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    move v2, v0

    .line 438
    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->i()Lcom/yandex/metrica/impl/bb;

    move-result-object v2

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->R()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 5424
    goto :goto_0

    :cond_1
    move v0, v1

    .line 438
    goto :goto_1
.end method

.method public B()Z
    .locals 1

    .prologue
    .line 442
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->k()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 443
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->i()Lcom/yandex/metrica/impl/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/bb;->N()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->i()Lcom/yandex/metrica/impl/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/bb;->R()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    .line 442
    goto :goto_0
.end method

.method public C()Z
    .locals 1

    .prologue
    .line 449
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->i()Lcom/yandex/metrica/impl/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/bb;->O()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 451
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->i()Lcom/yandex/metrica/impl/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/bb;->R()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    .line 449
    goto :goto_0
.end method

.method public D()Lcom/yandex/metrica/impl/ob/db;
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->j:Lcom/yandex/metrica/impl/ob/db;

    return-object v0
.end method

.method public final E()Lcom/yandex/metrica/impl/ob/fe;
    .locals 3

    .prologue
    .line 460
    new-instance v0, Lcom/yandex/metrica/impl/ob/fe;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/v;->f:Landroid/content/Context;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/v;->g:Lcom/yandex/metrica/impl/ob/t;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/t;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fe;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-object v0
.end method

.method public F()Lcom/yandex/metrica/impl/ob/dg;
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->k:Lcom/yandex/metrica/impl/ob/dg;

    return-object v0
.end method

.method public G()Lcom/yandex/metrica/impl/ob/dd;
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->h:Lcom/yandex/metrica/impl/ob/dd;

    return-object v0
.end method

.method public H()Z
    .locals 4

    .prologue
    .line 472
    const/4 v0, 0x0

    .line 6485
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/v;->i:Lcom/yandex/metrica/impl/ob/df;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/df;->a()Lcom/yandex/metrica/CounterConfiguration$a;

    move-result-object v1

    .line 6489
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/v;->h:Lcom/yandex/metrica/impl/ob/dd;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/dd;->c()Lcom/yandex/metrica/CounterConfiguration$a;

    move-result-object v2

    .line 475
    sget-object v3, Lcom/yandex/metrica/CounterConfiguration$a;->c:Lcom/yandex/metrica/CounterConfiguration$a;

    if-ne v1, v3, :cond_0

    sget-object v1, Lcom/yandex/metrica/CounterConfiguration$a;->c:Lcom/yandex/metrica/CounterConfiguration$a;

    if-ne v2, v1, :cond_0

    .line 478
    const/4 v0, 0x1

    .line 481
    :cond_0
    return v0
.end method

.method public I()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 497
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/v;->j:Lcom/yandex/metrica/impl/ob/db;

    invoke-virtual {v2, v1}, Lcom/yandex/metrica/impl/ob/db;->b(Z)Z

    move-result v2

    .line 498
    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/v;->k:Lcom/yandex/metrica/impl/ob/dg;

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/ob/dg;->h()Z

    move-result v3

    .line 499
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public J()Lcom/yandex/metrica/impl/ob/dd;
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->h:Lcom/yandex/metrica/impl/ob/dd;

    return-object v0
.end method

.method public a()Lcom/yandex/metrica/impl/ob/bl;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->w:Lcom/yandex/metrica/impl/ob/bl;

    return-object v0
.end method

.method public a(Lcom/yandex/metrica/CounterConfiguration$a;)V
    .locals 2

    .prologue
    .line 330
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->h:Lcom/yandex/metrica/impl/ob/dd;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/dd;->a(Lcom/yandex/metrica/CounterConfiguration$a;)Lcom/yandex/metrica/impl/ob/dd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dd;->g()V

    .line 2345
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 2346
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/v;->g:Lcom/yandex/metrica/impl/ob/t;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/t;->b()Ljava/lang/String;

    move-result-object v1

    .line 2348
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 331
    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->i:Lcom/yandex/metrica/impl/ob/df;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/df;->a(Lcom/yandex/metrica/CounterConfiguration$a;)Lcom/yandex/metrica/impl/ob/df;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/df;->g()V

    .line 334
    :cond_0
    return-void
.end method

.method public declared-synchronized a(Lcom/yandex/metrica/CounterConfiguration;)V
    .locals 1

    .prologue
    .line 241
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/v;->l:Lcom/yandex/metrica/CounterConfiguration;

    .line 242
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->m:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v0, p0}, Lcom/yandex/metrica/impl/bb;->e(Lcom/yandex/metrica/impl/ob/v;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 243
    monitor-exit p0

    return-void

    .line 241
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/yandex/metrica/impl/i;)V
    .locals 2

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->q()Lcom/yandex/metrica/impl/utils/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/utils/l;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->q()Lcom/yandex/metrica/impl/utils/l;

    move-result-object v0

    const-string v1, "Event received on service"

    invoke-virtual {v0, p1, v1}, Lcom/yandex/metrica/impl/utils/l;->a(Lcom/yandex/metrica/impl/i;Ljava/lang/String;)V

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->m:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/bb;->I()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 178
    :goto_0
    return-void

    .line 175
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->m:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v0, p0}, Lcom/yandex/metrica/impl/bb;->c(Lcom/yandex/metrica/impl/ob/v;)V

    .line 177
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->p:Lcom/yandex/metrica/impl/ob/x;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/x;->a(Lcom/yandex/metrica/impl/i;)Z

    goto :goto_0
.end method

.method a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ob/bm;)V
    .locals 0

    .prologue
    .line 228
    invoke-direct {p0, p1, p2}, Lcom/yandex/metrica/impl/ob/v;->b(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ob/bm;)V

    .line 229
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->h:Lcom/yandex/metrica/impl/ob/dd;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/dd;->b(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dd;->g()V

    .line 392
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->k:Lcom/yandex/metrica/impl/ob/dg;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/dg;->e(Z)Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dg;->g()V

    .line 494
    return-void
.end method

.method public b(Lcom/yandex/metrica/CounterConfiguration;)V
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->l:Lcom/yandex/metrica/CounterConfiguration;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/CounterConfiguration;->a(Lcom/yandex/metrica/CounterConfiguration;)V

    .line 515
    return-void
.end method

.method public b(Lcom/yandex/metrica/impl/i;)V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->p:Lcom/yandex/metrica/impl/ob/x;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/x;->a(Lcom/yandex/metrica/impl/i;)Z

    .line 184
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->i:Lcom/yandex/metrica/impl/ob/df;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/df;->b(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/df;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/df;->g()V

    .line 396
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->k:Lcom/yandex/metrica/impl/ob/dg;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/dg;->u(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dg;->g()V

    .line 397
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 503
    iput-boolean p1, p0, Lcom/yandex/metrica/impl/ob/v;->b:Z

    .line 504
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 198
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/ob/v;->a:Z

    return v0
.end method

.method public c()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 247
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->o:Lcom/yandex/metrica/impl/ob/co;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/co;->a()J

    move-result-wide v2

    .line 249
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->l:Lcom/yandex/metrica/CounterConfiguration;

    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->c()I

    move-result v0

    int-to-long v4, v0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    .line 250
    :goto_0
    iget-boolean v2, p0, Lcom/yandex/metrica/impl/ob/v;->b:Z

    or-int/2addr v0, v2

    .line 252
    if-eqz v0, :cond_0

    .line 253
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->g()V

    .line 254
    iput-boolean v1, p0, Lcom/yandex/metrica/impl/ob/v;->b:Z

    .line 256
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 249
    goto :goto_0
.end method

.method public c(Lcom/yandex/metrica/impl/i;)V
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->w:Lcom/yandex/metrica/impl/ob/bl;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/bl;->b()Lcom/yandex/metrica/impl/ob/bm;

    move-result-object v0

    .line 215
    invoke-direct {p0, p1, v0}, Lcom/yandex/metrica/impl/ob/v;->b(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ob/bm;)V

    .line 216
    return-void
.end method

.method public declared-synchronized d()V
    .locals 2

    .prologue
    .line 259
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/yandex/metrica/impl/ob/v;->c:Z

    .line 261
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->n:Lcom/yandex/metrica/impl/bk;

    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->a(Ljava/io/Closeable;)V

    .line 262
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->o:Lcom/yandex/metrica/impl/ob/co;

    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->a(Ljava/io/Closeable;)V

    .line 264
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->e:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 265
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->d:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 268
    monitor-exit p0

    return-void

    .line 259
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d(Lcom/yandex/metrica/impl/i;)V
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->w:Lcom/yandex/metrica/impl/ob/bl;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/bl;->c(Lcom/yandex/metrica/impl/i;)Lcom/yandex/metrica/impl/ob/bh;

    .line 220
    return-void
.end method

.method public e()V
    .locals 4

    .prologue
    .line 271
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/v;->y:Ljava/lang/Runnable;

    sget-wide v2, Lcom/yandex/metrica/impl/ad;->a:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 272
    return-void
.end method

.method public e(Lcom/yandex/metrica/impl/i;)V
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->w:Lcom/yandex/metrica/impl/ob/bl;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/bl;->d(Lcom/yandex/metrica/impl/i;)Lcom/yandex/metrica/impl/ob/bm;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/yandex/metrica/impl/ob/v;->b(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ob/bm;)V

    .line 224
    return-void
.end method

.method public declared-synchronized f()V
    .locals 1

    .prologue
    .line 275
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->n:Lcom/yandex/metrica/impl/bk;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/bk;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 276
    monitor-exit p0

    return-void

    .line 275
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f(Lcom/yandex/metrica/impl/i;)V
    .locals 1

    .prologue
    .line 352
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/v;->b(Z)V

    .line 353
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/ob/v;->e(Lcom/yandex/metrica/impl/i;)V

    .line 354
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->w()V

    .line 355
    return-void
.end method

.method public declared-synchronized g()V
    .locals 1

    .prologue
    .line 285
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->n:Lcom/yandex/metrica/impl/bk;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/bk;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286
    monitor-exit p0

    return-void

    .line 285
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public g(Lcom/yandex/metrica/impl/i;)V
    .locals 0

    .prologue
    .line 358
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/ob/v;->e(Lcom/yandex/metrica/impl/i;)V

    .line 359
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/v;->L()V

    .line 360
    return-void
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 289
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->h:Lcom/yandex/metrica/impl/ob/dd;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h(Lcom/yandex/metrica/impl/i;)V
    .locals 0

    .prologue
    .line 363
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/ob/v;->e(Lcom/yandex/metrica/impl/i;)V

    .line 364
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->x()V

    .line 365
    return-void
.end method

.method public i()Lcom/yandex/metrica/impl/bb;
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->m:Lcom/yandex/metrica/impl/bb;

    return-object v0
.end method

.method public i(Lcom/yandex/metrica/impl/i;)V
    .locals 6

    .prologue
    .line 376
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->q:Lcom/yandex/metrica/impl/a;

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->j()Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/a;->a(Landroid/util/Pair;)V

    .line 377
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->q:Lcom/yandex/metrica/impl/a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/a;->b()Lcom/yandex/metrica/impl/a$a;

    move-result-object v0

    .line 378
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/v;->r:Lcom/yandex/metrica/impl/ob/s;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/v;->h:Lcom/yandex/metrica/impl/ob/dd;

    invoke-virtual {v1, v0, v2}, Lcom/yandex/metrica/impl/ob/s;->a(Lcom/yandex/metrica/impl/a$a;Lcom/yandex/metrica/impl/ob/dd;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 379
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->q()Lcom/yandex/metrica/impl/utils/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/utils/l;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 380
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->q()Lcom/yandex/metrica/impl/utils/l;

    move-result-object v1

    const-string v2, "Save new app environment for %s. Value: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->m()Lcom/yandex/metrica/impl/ob/t;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v0, v0, Lcom/yandex/metrica/impl/a$a;->a:Ljava/lang/String;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/yandex/metrica/impl/utils/l;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 383
    :cond_0
    return-void
.end method

.method public j()Lcom/yandex/metrica/impl/ob/co;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->o:Lcom/yandex/metrica/impl/ob/co;

    return-object v0
.end method

.method public k()Lcom/yandex/metrica/CounterConfiguration;
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->l:Lcom/yandex/metrica/CounterConfiguration;

    return-object v0
.end method

.method public l()Landroid/os/ResultReceiver;
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->l:Lcom/yandex/metrica/CounterConfiguration;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->l:Lcom/yandex/metrica/CounterConfiguration;

    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->a()Landroid/os/ResultReceiver;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()Lcom/yandex/metrica/impl/ob/t;
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->g:Lcom/yandex/metrica/impl/ob/t;

    return-object v0
.end method

.method public n()Landroid/content/Context;
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->f:Landroid/content/Context;

    return-object v0
.end method

.method public o()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->e:Landroid/os/Handler;

    return-object v0
.end method

.method public declared-synchronized p()Z
    .locals 1

    .prologue
    .line 326
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/ob/v;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public q()Lcom/yandex/metrica/impl/utils/l;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->x:Lcom/yandex/metrica/impl/utils/l;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/utils/l;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->l:Lcom/yandex/metrica/CounterConfiguration;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->l:Lcom/yandex/metrica/CounterConfiguration;

    .line 338
    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->x:Lcom/yandex/metrica/impl/utils/l;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/utils/l;->a()V

    .line 341
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->x:Lcom/yandex/metrica/impl/utils/l;

    return-object v0
.end method

.method public r()V
    .locals 0

    .prologue
    .line 368
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/v;->L()V

    .line 369
    return-void
.end method

.method public s()V
    .locals 0

    .prologue
    .line 372
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->x()V

    .line 373
    return-void
.end method

.method public t()V
    .locals 3

    .prologue
    .line 386
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->q:Lcom/yandex/metrica/impl/a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/a;->a()V

    .line 387
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->r:Lcom/yandex/metrica/impl/ob/s;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/v;->q:Lcom/yandex/metrica/impl/a;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/a;->b()Lcom/yandex/metrica/impl/a$a;

    move-result-object v1

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/v;->h:Lcom/yandex/metrica/impl/ob/dd;

    invoke-virtual {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/s;->b(Lcom/yandex/metrica/impl/a$a;Lcom/yandex/metrica/impl/ob/dd;)V

    .line 388
    return-void
.end method

.method public u()Ljava/lang/String;
    .locals 2

    .prologue
    .line 400
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->i:Lcom/yandex/metrica/impl/ob/df;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/df;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public v()V
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->i:Lcom/yandex/metrica/impl/ob/df;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/df;->b()Lcom/yandex/metrica/impl/ob/df;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/df;->g()V

    .line 405
    return-void
.end method

.method public w()V
    .locals 4

    .prologue
    .line 408
    .line 3029
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 408
    iput-wide v0, p0, Lcom/yandex/metrica/impl/ob/v;->s:J

    .line 409
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->h:Lcom/yandex/metrica/impl/ob/dd;

    iget-wide v2, p0, Lcom/yandex/metrica/impl/ob/v;->s:J

    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/dd;->c(J)Lcom/yandex/metrica/impl/ob/dd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dd;->g()V

    .line 410
    return-void
.end method

.method x()V
    .locals 2

    .prologue
    .line 419
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->f:Landroid/content/Context;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/v;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/yandex/metrica/impl/bl;->c(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/yandex/metrica/impl/ob/v;->u:I

    .line 420
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/v;->h:Lcom/yandex/metrica/impl/ob/dd;

    iget v1, p0, Lcom/yandex/metrica/impl/ob/v;->u:I

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dd;->b(I)Lcom/yandex/metrica/impl/ob/dd;

    .line 421
    return-void
.end method

.method y()Z
    .locals 4

    .prologue
    .line 5029
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 429
    iget-wide v2, p0, Lcom/yandex/metrica/impl/ob/v;->t:J

    sub-long/2addr v0, v2

    sget-wide v2, Lcom/yandex/metrica/impl/ob/bj;->b:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method z()Z
    .locals 2

    .prologue
    .line 434
    iget v0, p0, Lcom/yandex/metrica/impl/ob/v;->u:I

    iget v1, p0, Lcom/yandex/metrica/impl/ob/v;->v:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
