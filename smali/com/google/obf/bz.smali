.class final Lcom/google/obf/bz;
.super Lcom/google/obf/cb;
.source "IMASDK"


# static fields
.field private static final b:[B


# instance fields
.field private final c:Lcom/google/obf/dv;

.field private final d:Lcom/google/obf/dw;

.field private final e:Lcom/google/obf/ar;

.field private f:I

.field private g:I

.field private h:I

.field private i:Z

.field private j:Z

.field private k:J

.field private l:I

.field private m:J

.field private n:Lcom/google/obf/ar;

.field private o:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x3

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/obf/bz;->b:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x49t
        0x44t
        0x33t
    .end array-data
.end method

.method public constructor <init>(Lcom/google/obf/ar;Lcom/google/obf/ar;)V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lcom/google/obf/cb;-><init>(Lcom/google/obf/ar;)V

    .line 2
    iput-object p2, p0, Lcom/google/obf/bz;->e:Lcom/google/obf/ar;

    .line 3
    invoke-static {}, Lcom/google/obf/q;->a()Lcom/google/obf/q;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/google/obf/ar;->a(Lcom/google/obf/q;)V

    .line 4
    new-instance v0, Lcom/google/obf/dv;

    const/4 v1, 0x7

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Lcom/google/obf/dv;-><init>([B)V

    iput-object v0, p0, Lcom/google/obf/bz;->c:Lcom/google/obf/dv;

    .line 5
    new-instance v0, Lcom/google/obf/dw;

    sget-object v1, Lcom/google/obf/bz;->b:[B

    const/16 v2, 0xa

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>([B)V

    iput-object v0, p0, Lcom/google/obf/bz;->d:Lcom/google/obf/dw;

    .line 6
    invoke-direct {p0}, Lcom/google/obf/bz;->c()V

    .line 7
    return-void
.end method

.method private a(Lcom/google/obf/ar;JII)V
    .locals 2

    .prologue
    .line 38
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/obf/bz;->f:I

    .line 39
    iput p4, p0, Lcom/google/obf/bz;->g:I

    .line 40
    iput-object p1, p0, Lcom/google/obf/bz;->n:Lcom/google/obf/ar;

    .line 41
    iput-wide p2, p0, Lcom/google/obf/bz;->o:J

    .line 42
    iput p5, p0, Lcom/google/obf/bz;->l:I

    .line 43
    return-void
.end method

.method private a(Lcom/google/obf/dw;[BI)Z
    .locals 2

    .prologue
    .line 25
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v0

    iget v1, p0, Lcom/google/obf/bz;->g:I

    sub-int v1, p3, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 26
    iget v1, p0, Lcom/google/obf/bz;->g:I

    invoke-virtual {p1, p2, v1, v0}, Lcom/google/obf/dw;->a([BII)V

    .line 27
    iget v1, p0, Lcom/google/obf/bz;->g:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/bz;->g:I

    .line 28
    iget v0, p0, Lcom/google/obf/bz;->g:I

    if-ne v0, p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/google/obf/dw;)V
    .locals 7

    .prologue
    const/16 v6, 0x200

    const/16 v5, 0x100

    .line 47
    iget-object v2, p1, Lcom/google/obf/dw;->a:[B

    .line 48
    invoke-virtual {p1}, Lcom/google/obf/dw;->d()I

    move-result v0

    .line 49
    invoke-virtual {p1}, Lcom/google/obf/dw;->c()I

    move-result v3

    .line 50
    :goto_0
    if-ge v0, v3, :cond_2

    .line 51
    add-int/lit8 v1, v0, 0x1

    aget-byte v0, v2, v0

    and-int/lit16 v0, v0, 0xff

    .line 52
    iget v4, p0, Lcom/google/obf/bz;->h:I

    if-ne v4, v6, :cond_1

    const/16 v4, 0xf0

    if-lt v0, v4, :cond_1

    const/16 v4, 0xff

    if-eq v0, v4, :cond_1

    .line 53
    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/obf/bz;->i:Z

    .line 54
    invoke-direct {p0}, Lcom/google/obf/bz;->e()V

    .line 55
    invoke-virtual {p1, v1}, Lcom/google/obf/dw;->c(I)V

    .line 72
    :goto_2
    return-void

    .line 53
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 57
    :cond_1
    iget v4, p0, Lcom/google/obf/bz;->h:I

    or-int/2addr v0, v4

    sparse-switch v0, :sswitch_data_0

    .line 67
    iget v0, p0, Lcom/google/obf/bz;->h:I

    if-eq v0, v5, :cond_3

    .line 68
    iput v5, p0, Lcom/google/obf/bz;->h:I

    .line 69
    add-int/lit8 v0, v1, -0x1

    goto :goto_0

    .line 58
    :sswitch_0
    iput v6, p0, Lcom/google/obf/bz;->h:I

    move v0, v1

    .line 59
    goto :goto_0

    .line 60
    :sswitch_1
    const/16 v0, 0x300

    iput v0, p0, Lcom/google/obf/bz;->h:I

    move v0, v1

    .line 61
    goto :goto_0

    .line 62
    :sswitch_2
    const/16 v0, 0x400

    iput v0, p0, Lcom/google/obf/bz;->h:I

    move v0, v1

    .line 63
    goto :goto_0

    .line 64
    :sswitch_3
    invoke-direct {p0}, Lcom/google/obf/bz;->d()V

    .line 65
    invoke-virtual {p1, v1}, Lcom/google/obf/dw;->c(I)V

    goto :goto_2

    .line 71
    :cond_2
    invoke-virtual {p1, v0}, Lcom/google/obf/dw;->c(I)V

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_0

    .line 57
    :sswitch_data_0
    .sparse-switch
        0x149 -> :sswitch_1
        0x1ff -> :sswitch_0
        0x344 -> :sswitch_2
        0x433 -> :sswitch_3
    .end sparse-switch
.end method

.method private c()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    iput v0, p0, Lcom/google/obf/bz;->f:I

    .line 30
    iput v0, p0, Lcom/google/obf/bz;->g:I

    .line 31
    const/16 v0, 0x100

    iput v0, p0, Lcom/google/obf/bz;->h:I

    .line 32
    return-void
.end method

.method private c(Lcom/google/obf/dw;)V
    .locals 8

    .prologue
    .line 105
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v0

    iget v1, p0, Lcom/google/obf/bz;->l:I

    iget v2, p0, Lcom/google/obf/bz;->g:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 106
    iget-object v1, p0, Lcom/google/obf/bz;->n:Lcom/google/obf/ar;

    invoke-interface {v1, p1, v0}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 107
    iget v1, p0, Lcom/google/obf/bz;->g:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/bz;->g:I

    .line 108
    iget v0, p0, Lcom/google/obf/bz;->g:I

    iget v1, p0, Lcom/google/obf/bz;->l:I

    if-ne v0, v1, :cond_0

    .line 109
    iget-object v1, p0, Lcom/google/obf/bz;->n:Lcom/google/obf/ar;

    iget-wide v2, p0, Lcom/google/obf/bz;->m:J

    const/4 v4, 0x1

    iget v5, p0, Lcom/google/obf/bz;->l:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/google/obf/ar;->a(JIII[B)V

    .line 110
    iget-wide v0, p0, Lcom/google/obf/bz;->m:J

    iget-wide v2, p0, Lcom/google/obf/bz;->o:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/obf/bz;->m:J

    .line 111
    invoke-direct {p0}, Lcom/google/obf/bz;->c()V

    .line 112
    :cond_0
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/obf/bz;->f:I

    .line 34
    sget-object v0, Lcom/google/obf/bz;->b:[B

    array-length v0, v0

    iput v0, p0, Lcom/google/obf/bz;->g:I

    .line 35
    iput v1, p0, Lcom/google/obf/bz;->l:I

    .line 36
    iget-object v0, p0, Lcom/google/obf/bz;->d:Lcom/google/obf/dw;

    invoke-virtual {v0, v1}, Lcom/google/obf/dw;->c(I)V

    .line 37
    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/obf/bz;->f:I

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/bz;->g:I

    .line 46
    return-void
.end method

.method private f()V
    .locals 6

    .prologue
    const/16 v4, 0xa

    .line 73
    iget-object v0, p0, Lcom/google/obf/bz;->e:Lcom/google/obf/ar;

    iget-object v1, p0, Lcom/google/obf/bz;->d:Lcom/google/obf/dw;

    invoke-interface {v0, v1, v4}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 74
    iget-object v0, p0, Lcom/google/obf/bz;->d:Lcom/google/obf/dw;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/obf/dw;->c(I)V

    .line 75
    iget-object v1, p0, Lcom/google/obf/bz;->e:Lcom/google/obf/ar;

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lcom/google/obf/bz;->d:Lcom/google/obf/dw;

    .line 76
    invoke-virtual {v0}, Lcom/google/obf/dw;->r()I

    move-result v0

    add-int/lit8 v5, v0, 0xa

    move-object v0, p0

    .line 77
    invoke-direct/range {v0 .. v5}, Lcom/google/obf/bz;->a(Lcom/google/obf/ar;JII)V

    .line 78
    return-void
.end method

.method private g()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v2, -0x1

    const/4 v0, 0x2

    .line 79
    iget-object v1, p0, Lcom/google/obf/bz;->c:Lcom/google/obf/dv;

    invoke-virtual {v1, v10}, Lcom/google/obf/dv;->a(I)V

    .line 80
    iget-boolean v1, p0, Lcom/google/obf/bz;->j:Z

    if-nez v1, :cond_1

    .line 81
    iget-object v1, p0, Lcom/google/obf/bz;->c:Lcom/google/obf/dv;

    invoke-virtual {v1, v0}, Lcom/google/obf/dv;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 82
    if-eq v1, v0, :cond_2

    .line 83
    const-string v3, "AdtsReader"

    const/16 v4, 0x3d

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Detected audio object type: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", but assuming AAC LC."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    :goto_0
    iget-object v1, p0, Lcom/google/obf/bz;->c:Lcom/google/obf/dv;

    invoke-virtual {v1, v12}, Lcom/google/obf/dv;->c(I)I

    move-result v1

    .line 86
    iget-object v3, p0, Lcom/google/obf/bz;->c:Lcom/google/obf/dv;

    invoke-virtual {v3, v11}, Lcom/google/obf/dv;->b(I)V

    .line 87
    iget-object v3, p0, Lcom/google/obf/bz;->c:Lcom/google/obf/dv;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/google/obf/dv;->c(I)I

    move-result v3

    .line 88
    invoke-static {v0, v1, v3}, Lcom/google/obf/dm;->a(III)[B

    move-result-object v8

    .line 89
    invoke-static {v8}, Lcom/google/obf/dm;->a([B)Landroid/util/Pair;

    move-result-object v7

    .line 90
    const/4 v0, 0x0

    const-string v1, "audio/mp4a-latm"

    const-wide/16 v4, -0x1

    iget-object v3, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    .line 91
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v3, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    .line 92
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static {v8}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    const/4 v9, 0x0

    move v3, v2

    .line 93
    invoke-static/range {v0 .. v9}, Lcom/google/obf/q;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)Lcom/google/obf/q;

    move-result-object v0

    .line 94
    const-wide/32 v2, 0x3d090000

    iget v1, v0, Lcom/google/obf/q;->r:I

    int-to-long v4, v1

    div-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/obf/bz;->k:J

    .line 95
    iget-object v1, p0, Lcom/google/obf/bz;->a:Lcom/google/obf/ar;

    invoke-interface {v1, v0}, Lcom/google/obf/ar;->a(Lcom/google/obf/q;)V

    .line 96
    iput-boolean v11, p0, Lcom/google/obf/bz;->j:Z

    .line 99
    :goto_1
    iget-object v0, p0, Lcom/google/obf/bz;->c:Lcom/google/obf/dv;

    invoke-virtual {v0, v12}, Lcom/google/obf/dv;->b(I)V

    .line 100
    iget-object v0, p0, Lcom/google/obf/bz;->c:Lcom/google/obf/dv;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/google/obf/dv;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    add-int/lit8 v5, v0, -0x5

    .line 101
    iget-boolean v0, p0, Lcom/google/obf/bz;->i:Z

    if-eqz v0, :cond_0

    .line 102
    add-int/lit8 v5, v5, -0x2

    .line 103
    :cond_0
    iget-object v1, p0, Lcom/google/obf/bz;->a:Lcom/google/obf/ar;

    iget-wide v2, p0, Lcom/google/obf/bz;->k:J

    move-object v0, p0

    move v4, v10

    invoke-direct/range {v0 .. v5}, Lcom/google/obf/bz;->a(Lcom/google/obf/ar;JII)V

    .line 104
    return-void

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/google/obf/bz;->c:Lcom/google/obf/dv;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/google/obf/dv;->b(I)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/obf/bz;->c()V

    .line 9
    return-void
.end method

.method public a(JZ)V
    .locals 1

    .prologue
    .line 10
    iput-wide p1, p0, Lcom/google/obf/bz;->m:J

    .line 11
    return-void
.end method

.method public a(Lcom/google/obf/dw;)V
    .locals 2

    .prologue
    .line 12
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v0

    if-lez v0, :cond_2

    .line 13
    iget v0, p0, Lcom/google/obf/bz;->f:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 14
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/obf/bz;->b(Lcom/google/obf/dw;)V

    goto :goto_0

    .line 16
    :pswitch_1
    iget-object v0, p0, Lcom/google/obf/bz;->d:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    const/16 v1, 0xa

    invoke-direct {p0, p1, v0, v1}, Lcom/google/obf/bz;->a(Lcom/google/obf/dw;[BI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17
    invoke-direct {p0}, Lcom/google/obf/bz;->f()V

    goto :goto_0

    .line 18
    :pswitch_2
    iget-boolean v0, p0, Lcom/google/obf/bz;->i:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x7

    .line 19
    :goto_1
    iget-object v1, p0, Lcom/google/obf/bz;->c:Lcom/google/obf/dv;

    iget-object v1, v1, Lcom/google/obf/dv;->a:[B

    invoke-direct {p0, p1, v1, v0}, Lcom/google/obf/bz;->a(Lcom/google/obf/dw;[BI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20
    invoke-direct {p0}, Lcom/google/obf/bz;->g()V

    goto :goto_0

    .line 18
    :cond_1
    const/4 v0, 0x5

    goto :goto_1

    .line 21
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/google/obf/bz;->c(Lcom/google/obf/dw;)V

    goto :goto_0

    .line 23
    :cond_2
    return-void

    .line 13
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public b()V
    .locals 0

    .prologue
    .line 24
    return-void
.end method
