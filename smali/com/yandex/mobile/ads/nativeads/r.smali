.class public Lcom/yandex/mobile/ads/nativeads/r;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/mobile/ads/nativeads/r$b;,
        Lcom/yandex/mobile/ads/nativeads/r$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/yandex/mobile/ads/nativeads/h;

.field private final b:Lcom/yandex/mobile/ads/nativeads/l;

.field private final c:Lcom/yandex/mobile/ads/as;

.field private d:Lcom/yandex/mobile/ads/nativeads/q;


# direct methods
.method public constructor <init>(Lcom/yandex/mobile/ads/nativeads/h;Lcom/yandex/mobile/ads/nativeads/l;Lcom/yandex/mobile/ads/as;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/yandex/mobile/ads/nativeads/r;->a:Lcom/yandex/mobile/ads/nativeads/h;

    .line 38
    iput-object p2, p0, Lcom/yandex/mobile/ads/nativeads/r;->b:Lcom/yandex/mobile/ads/nativeads/l;

    .line 39
    iput-object p3, p0, Lcom/yandex/mobile/ads/nativeads/r;->c:Lcom/yandex/mobile/ads/as;

    .line 40
    return-void
.end method

.method static synthetic a(Lcom/yandex/mobile/ads/nativeads/r;)Lcom/yandex/mobile/ads/as;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/r;->c:Lcom/yandex/mobile/ads/as;

    return-object v0
.end method

.method static synthetic b(Lcom/yandex/mobile/ads/nativeads/r;)Lcom/yandex/mobile/ads/nativeads/l;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/r;->b:Lcom/yandex/mobile/ads/nativeads/l;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 62
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/r;->d:Lcom/yandex/mobile/ads/nativeads/q;

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/r;->a:Lcom/yandex/mobile/ads/nativeads/h;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/h;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/b;

    .line 64
    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/r;->d:Lcom/yandex/mobile/ads/nativeads/q;

    invoke-virtual {v2, v0}, Lcom/yandex/mobile/ads/nativeads/q;->a(Lcom/yandex/mobile/ads/nativeads/b;)Lcom/yandex/mobile/ads/nativeads/ac;

    move-result-object v2

    .line 65
    if-eqz v2, :cond_0

    instance-of v3, v2, Lcom/yandex/mobile/ads/nativeads/ac$a;

    if-eqz v3, :cond_0

    .line 66
    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/b;->a()Ljava/lang/Object;

    move-result-object v0

    .line 67
    invoke-virtual {v2}, Lcom/yandex/mobile/ads/nativeads/ac;->a()Landroid/view/View;

    move-result-object v3

    .line 68
    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    .line 69
    invoke-virtual {v2, v3, v0}, Lcom/yandex/mobile/ads/nativeads/ac;->a(Landroid/view/View;Ljava/lang/Object;)V

    goto :goto_0

    .line 74
    :cond_1
    return-void
.end method

.method public a(Lcom/yandex/mobile/ads/nativeads/q;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 43
    iput-object p1, p0, Lcom/yandex/mobile/ads/nativeads/r;->d:Lcom/yandex/mobile/ads/nativeads/q;

    .line 44
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/r;->a:Lcom/yandex/mobile/ads/nativeads/h;

    if-eqz v0, :cond_1

    .line 45
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/r;->a:Lcom/yandex/mobile/ads/nativeads/h;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/h;->a()Lcom/yandex/mobile/ads/nativeads/g;

    move-result-object v1

    .line 46
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/r;->a:Lcom/yandex/mobile/ads/nativeads/h;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/h;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/b;

    .line 47
    invoke-virtual {p1, v0}, Lcom/yandex/mobile/ads/nativeads/q;->a(Lcom/yandex/mobile/ads/nativeads/b;)Lcom/yandex/mobile/ads/nativeads/ac;

    move-result-object v3

    .line 48
    if-eqz v3, :cond_0

    .line 49
    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/b;->a()Ljava/lang/Object;

    move-result-object v4

    .line 50
    invoke-virtual {v3}, Lcom/yandex/mobile/ads/nativeads/ac;->a()Landroid/view/View;

    move-result-object v5

    .line 51
    if-eqz v5, :cond_0

    if-eqz v4, :cond_0

    .line 52
    invoke-virtual {v3, v5, v4}, Lcom/yandex/mobile/ads/nativeads/ac;->a(Landroid/view/View;Ljava/lang/Object;)V

    .line 53
    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 54
    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/b;->e()Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Lcom/yandex/mobile/ads/nativeads/r$a;

    invoke-direct {v3, p0, v0, v1, v6}, Lcom/yandex/mobile/ads/nativeads/r$a;-><init>(Lcom/yandex/mobile/ads/nativeads/r;Lcom/yandex/mobile/ads/nativeads/b;Lcom/yandex/mobile/ads/nativeads/g;B)V

    invoke-virtual {v5, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/yandex/mobile/ads/nativeads/r$b;

    invoke-direct {v0, v5, v6}, Lcom/yandex/mobile/ads/nativeads/r$b;-><init>(Landroid/view/View;B)V

    invoke-virtual {v5, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0

    .line 59
    :cond_1
    return-void
.end method
