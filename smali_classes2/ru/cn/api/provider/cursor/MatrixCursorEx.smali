.class public Lru/cn/api/provider/cursor/MatrixCursorEx;
.super Landroid/database/MatrixCursor;
.source "MatrixCursorEx.java"


# instance fields
.field private extras:Landroid/os/Bundle;


# direct methods
.method public constructor <init>([Ljava/lang/String;)V
    .locals 0
    .param p1, "columnNames"    # [Ljava/lang/String;

    .prologue
    .line 10
    invoke-direct {p0, p1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 11
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/api/provider/cursor/MatrixCursorEx;->extras:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lru/cn/api/provider/cursor/MatrixCursorEx;->extras:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    .line 30
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/api/provider/cursor/MatrixCursorEx;->extras:Landroid/os/Bundle;

    .line 31
    invoke-super {p0}, Landroid/database/MatrixCursor;->close()V

    .line 32
    return-void
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lru/cn/api/provider/cursor/MatrixCursorEx;->extras:Landroid/os/Bundle;

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lru/cn/api/provider/cursor/MatrixCursorEx;->extras:Landroid/os/Bundle;

    goto :goto_0
.end method

.method public setExtras(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "extras"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "Override"
        }
    .end annotation

    .prologue
    .line 22
    iput-object p1, p0, Lru/cn/api/provider/cursor/MatrixCursorEx;->extras:Landroid/os/Bundle;

    .line 23
    return-void
.end method
