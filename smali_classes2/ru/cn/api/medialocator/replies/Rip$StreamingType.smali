.class public final enum Lru/cn/api/medialocator/replies/Rip$StreamingType;
.super Ljava/lang/Enum;
.source "Rip.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/api/medialocator/replies/Rip;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "StreamingType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/api/medialocator/replies/Rip$StreamingType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/api/medialocator/replies/Rip$StreamingType;

.field public static final enum HTTP_LIVE_STREAMING:Lru/cn/api/medialocator/replies/Rip$StreamingType;

.field public static final enum PROTOCOL_DEFAULT:Lru/cn/api/medialocator/replies/Rip$StreamingType;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    new-instance v0, Lru/cn/api/medialocator/replies/Rip$StreamingType;

    const-string v1, "PROTOCOL_DEFAULT"

    invoke-direct {v0, v1, v2, v2}, Lru/cn/api/medialocator/replies/Rip$StreamingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/medialocator/replies/Rip$StreamingType;->PROTOCOL_DEFAULT:Lru/cn/api/medialocator/replies/Rip$StreamingType;

    new-instance v0, Lru/cn/api/medialocator/replies/Rip$StreamingType;

    const-string v1, "HTTP_LIVE_STREAMING"

    invoke-direct {v0, v1, v3, v3}, Lru/cn/api/medialocator/replies/Rip$StreamingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/medialocator/replies/Rip$StreamingType;->HTTP_LIVE_STREAMING:Lru/cn/api/medialocator/replies/Rip$StreamingType;

    .line 29
    const/4 v0, 0x2

    new-array v0, v0, [Lru/cn/api/medialocator/replies/Rip$StreamingType;

    sget-object v1, Lru/cn/api/medialocator/replies/Rip$StreamingType;->PROTOCOL_DEFAULT:Lru/cn/api/medialocator/replies/Rip$StreamingType;

    aput-object v1, v0, v2

    sget-object v1, Lru/cn/api/medialocator/replies/Rip$StreamingType;->HTTP_LIVE_STREAMING:Lru/cn/api/medialocator/replies/Rip$StreamingType;

    aput-object v1, v0, v3

    sput-object v0, Lru/cn/api/medialocator/replies/Rip$StreamingType;->$VALUES:[Lru/cn/api/medialocator/replies/Rip$StreamingType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 35
    iput p3, p0, Lru/cn/api/medialocator/replies/Rip$StreamingType;->value:I

    .line 36
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/api/medialocator/replies/Rip$StreamingType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 29
    const-class v0, Lru/cn/api/medialocator/replies/Rip$StreamingType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/api/medialocator/replies/Rip$StreamingType;

    return-object v0
.end method

.method public static values()[Lru/cn/api/medialocator/replies/Rip$StreamingType;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lru/cn/api/medialocator/replies/Rip$StreamingType;->$VALUES:[Lru/cn/api/medialocator/replies/Rip$StreamingType;

    invoke-virtual {v0}, [Lru/cn/api/medialocator/replies/Rip$StreamingType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/api/medialocator/replies/Rip$StreamingType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lru/cn/api/medialocator/replies/Rip$StreamingType;->value:I

    return v0
.end method
