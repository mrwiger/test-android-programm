.class Lcom/yandex/metrica/impl/ob/ge;
.super Lcom/yandex/metrica/impl/ob/gb;
.source "SourceFile"

# interfaces
.implements Lcom/yandex/metrica/impl/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ob/ge$a;
    }
.end annotation


# static fields
.field private static final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/telephony/TelephonyManager;

.field private c:Landroid/telephony/PhoneStateListener;

.field private d:Z

.field private volatile e:Z

.field private final f:Lcom/yandex/metrica/impl/e$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/yandex/metrica/impl/e$a",
            "<",
            "Lcom/yandex/metrica/impl/ob/gj;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/yandex/metrica/impl/e$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/yandex/metrica/impl/e$a",
            "<[",
            "Lcom/yandex/metrica/impl/ob/gc;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Landroid/os/Handler;

.field private final i:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lcom/yandex/metrica/impl/ob/ge$1;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/ob/ge$1;-><init>()V

    sput-object v0, Lcom/yandex/metrica/impl/ob/ge;->a:Landroid/util/SparseArray;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/gb;-><init>()V

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/yandex/metrica/impl/ob/ge;->d:Z

    .line 91
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/yandex/metrica/impl/ob/ge;->e:Z

    .line 93
    new-instance v0, Lcom/yandex/metrica/impl/e$a;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/e$a;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->f:Lcom/yandex/metrica/impl/e$a;

    .line 94
    new-instance v0, Lcom/yandex/metrica/impl/e$a;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/e$a;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->g:Lcom/yandex/metrica/impl/e$a;

    .line 100
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/ge;->i:Landroid/content/Context;

    .line 101
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->b:Landroid/telephony/TelephonyManager;

    .line 102
    const-string v0, "YMM-TP"

    invoke-static {v0}, Lcom/yandex/metrica/impl/utils/j;->a(Ljava/lang/String;)Landroid/os/HandlerThread;

    move-result-object v0

    .line 103
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 104
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/yandex/metrica/impl/ob/ge;->h:Landroid/os/Handler;

    .line 106
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->h:Landroid/os/Handler;

    new-instance v1, Lcom/yandex/metrica/impl/ob/ge$2;

    invoke-direct {v1, p0}, Lcom/yandex/metrica/impl/ob/ge$2;-><init>(Lcom/yandex/metrica/impl/ob/ge;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 111
    return-void
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/ob/ge;Landroid/telephony/PhoneStateListener;)Landroid/telephony/PhoneStateListener;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/ge;->c:Landroid/telephony/PhoneStateListener;

    return-object p1
.end method

.method private declared-synchronized a(Landroid/telephony/SignalStrength;)V
    .locals 4

    .prologue
    const/16 v3, -0x78

    .line 249
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->f:Lcom/yandex/metrica/impl/e$a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/e$a;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->f:Lcom/yandex/metrica/impl/e$a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/e$a;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 250
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->f:Lcom/yandex/metrica/impl/e$a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/gj;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/gj;->b()Lcom/yandex/metrica/impl/ob/gc;

    move-result-object v2

    .line 2403
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->isGsm()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2418
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    move-result v0

    .line 2420
    const/16 v1, 0x63

    if-ne v1, v0, :cond_2

    .line 2421
    const/4 v0, -0x1

    .line 250
    :cond_0
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/yandex/metrica/impl/ob/gc;->a(Ljava/lang/Integer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    :cond_1
    monitor-exit p0

    return-void

    .line 2423
    :cond_2
    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, -0x71

    goto :goto_0

    .line 2406
    :cond_3
    :try_start_1
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getCdmaDbm()I

    move-result v0

    .line 2407
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getEvdoDbm()I

    move-result v1

    .line 2408
    if-eq v3, v1, :cond_0

    if-ne v3, v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 249
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/ob/ge;Landroid/telephony/SignalStrength;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/ge;->a(Landroid/telephony/SignalStrength;)V

    return-void
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/ob/ge;)Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/ob/ge;->d:Z

    return v0
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/ob/ge;Z)Z
    .locals 0

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/yandex/metrica/impl/ob/ge;->d:Z

    return p1
.end method

.method static synthetic b(Lcom/yandex/metrica/impl/ob/ge;)Landroid/telephony/PhoneStateListener;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->c:Landroid/telephony/PhoneStateListener;

    return-object v0
.end method

.method static synthetic b(Lcom/yandex/metrica/impl/ob/ge;Z)Z
    .locals 0

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/yandex/metrica/impl/ob/ge;->e:Z

    return p1
.end method

.method static synthetic c(Lcom/yandex/metrica/impl/ob/ge;)Landroid/telephony/TelephonyManager;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->b:Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method private declared-synchronized k()[Lcom/yandex/metrica/impl/ob/gc;
    .locals 2

    .prologue
    .line 206
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->g:Lcom/yandex/metrica/impl/e$a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/e$a;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->g:Lcom/yandex/metrica/impl/e$a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/e$a;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207
    :cond_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/ge;->d()[Lcom/yandex/metrica/impl/ob/gc;

    move-result-object v0

    .line 208
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ge;->g:Lcom/yandex/metrica/impl/e$a;

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/e$a;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212
    :goto_0
    monitor-exit p0

    return-object v0

    .line 210
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->g:Lcom/yandex/metrica/impl/e$a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/yandex/metrica/impl/ob/gc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 206
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private l()Ljava/lang/Integer;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 256
    :try_start_0
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ge;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 257
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 262
    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private m()Ljava/lang/Integer;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 267
    :try_start_0
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ge;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 268
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 273
    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private n()Ljava/lang/Integer;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 278
    :try_start_0
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ge;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 279
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 281
    :goto_0
    return-object v0

    .line 279
    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 281
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private o()Ljava/lang/Integer;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 286
    :try_start_0
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ge;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 287
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 289
    :goto_0
    return-object v0

    .line 287
    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 289
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private p()Ljava/lang/String;
    .locals 2

    .prologue
    .line 331
    const/4 v0, 0x0

    .line 333
    :try_start_0
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ge;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 335
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private q()Ljava/lang/String;
    .locals 2

    .prologue
    .line 339
    const/4 v0, 0x0

    .line 341
    :try_start_0
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ge;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimOperatorName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 343
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private r()Ljava/lang/String;
    .locals 3

    .prologue
    .line 347
    const-string v1, "unknown"

    .line 349
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v0

    .line 350
    sget-object v2, Lcom/yandex/metrica/impl/ob/ge;->a:Landroid/util/SparseArray;

    invoke-virtual {v2, v0, v1}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 354
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method private s()Ljava/lang/String;
    .locals 3

    .prologue
    .line 359
    const/4 v0, 0x0

    .line 361
    :try_start_0
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ge;->i:Landroid/content/Context;

    .line 3047
    const-string v2, "android.permission.READ_PHONE_STATE"

    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/am;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    .line 361
    if-eqz v1, :cond_0

    .line 362
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ge;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 367
    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private t()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 372
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 375
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->i:Landroid/content/Context;

    .line 4047
    const-string v2, "android.permission.READ_PHONE_STATE"

    invoke-static {v0, v2}, Lcom/yandex/metrica/impl/am;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 375
    if-eqz v0, :cond_1

    .line 376
    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0xa

    if-ge v0, v2, :cond_1

    .line 377
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ge;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2, v0}, Landroid/telephony/TelephonyManager;->getDeviceId(I)Ljava/lang/String;

    move-result-object v2

    .line 378
    if-eqz v2, :cond_0

    .line 379
    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 376
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    .line 386
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method private u()Z
    .locals 2

    .prologue
    .line 390
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->i:Landroid/content/Context;

    .line 5047
    const-string v1, "android.permission.READ_PHONE_STATE"

    invoke-static {v0, v1}, Lcom/yandex/metrica/impl/am;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 390
    if-eqz v0, :cond_0

    .line 392
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 395
    :goto_0
    return v0

    :catch_0
    move-exception v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private v()Lcom/yandex/metrica/impl/ob/gh;
    .locals 6

    .prologue
    .line 464
    new-instance v0, Lcom/yandex/metrica/impl/ob/gh;

    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ge;->n()Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ge;->o()Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ge;->u()Z

    move-result v3

    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ge;->q()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/yandex/metrica/impl/ob/gh;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private w()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/yandex/metrica/impl/ob/gh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 469
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 470
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->i:Landroid/content/Context;

    .line 6047
    const-string v2, "android.permission.READ_PHONE_STATE"

    invoke-static {v0, v2}, Lcom/yandex/metrica/impl/am;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 470
    if-eqz v0, :cond_0

    .line 472
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->i:Landroid/content/Context;

    invoke-static {v0}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v0

    .line 473
    invoke-virtual {v0}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoList()Ljava/util/List;

    move-result-object v0

    .line 474
    if-eqz v0, :cond_0

    .line 475
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/SubscriptionInfo;

    .line 476
    new-instance v3, Lcom/yandex/metrica/impl/ob/gh;

    invoke-direct {v3, v0}, Lcom/yandex/metrica/impl/ob/gh;-><init>(Landroid/telephony/SubscriptionInfo;)V

    .line 477
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 484
    :cond_0
    return-object v1
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 2

    .prologue
    .line 114
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->h:Landroid/os/Handler;

    new-instance v1, Lcom/yandex/metrica/impl/ob/ge$3;

    invoke-direct {v1, p0}, Lcom/yandex/metrica/impl/ob/ge$3;-><init>(Lcom/yandex/metrica/impl/ob/ge;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    monitor-exit p0

    return-void

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/yandex/metrica/impl/ob/gd;)V
    .locals 1

    .prologue
    .line 169
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 170
    :try_start_0
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ge;->k()[Lcom/yandex/metrica/impl/ob/gc;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/yandex/metrica/impl/ob/gd;->a([Lcom/yandex/metrica/impl/ob/gc;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 172
    :cond_0
    monitor-exit p0

    return-void

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/yandex/metrica/impl/ob/gk;)V
    .locals 1

    .prologue
    .line 162
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 163
    :try_start_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/ge;->c()Lcom/yandex/metrica/impl/ob/gj;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/yandex/metrica/impl/ob/gk;->a(Lcom/yandex/metrica/impl/ob/gj;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 165
    :cond_0
    monitor-exit p0

    return-void

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 2

    .prologue
    .line 141
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->h:Landroid/os/Handler;

    new-instance v1, Lcom/yandex/metrica/impl/ob/ge$4;

    invoke-direct {v1, p0}, Lcom/yandex/metrica/impl/ob/ge$4;-><init>(Lcom/yandex/metrica/impl/ob/ge;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 158
    monitor-exit p0

    return-void

    .line 141
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized c()Lcom/yandex/metrica/impl/ob/gj;
    .locals 4

    .prologue
    .line 191
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->f:Lcom/yandex/metrica/impl/e$a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/e$a;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->f:Lcom/yandex/metrica/impl/e$a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/e$a;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 192
    :cond_0
    new-instance v1, Lcom/yandex/metrica/impl/ob/gj;

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/ge;->g()Lcom/yandex/metrica/impl/ob/gc;

    move-result-object v0

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/ge;->h()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/ge;->i()Ljava/util/List;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Lcom/yandex/metrica/impl/ob/gj;-><init>(Lcom/yandex/metrica/impl/ob/gc;Ljava/util/List;Ljava/util/List;)V

    .line 193
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/gj;->b()Lcom/yandex/metrica/impl/ob/gc;

    move-result-object v0

    .line 194
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/gc;->a()Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->f:Lcom/yandex/metrica/impl/e$a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/e$a;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 195
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/gj;->b()Lcom/yandex/metrica/impl/ob/gc;

    move-result-object v2

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->f:Lcom/yandex/metrica/impl/e$a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/gj;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/gj;->b()Lcom/yandex/metrica/impl/ob/gc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/gc;->a()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/yandex/metrica/impl/ob/gc;->a(Ljava/lang/Integer;)V

    .line 197
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->f:Lcom/yandex/metrica/impl/e$a;

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/e$a;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v1

    .line 201
    :goto_0
    monitor-exit p0

    return-object v0

    .line 199
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->f:Lcom/yandex/metrica/impl/e$a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/gj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 191
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method d()[Lcom/yandex/metrica/impl/ob/gc;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 219
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 220
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/ge;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 221
    const/16 v0, 0x11

    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->i:Landroid/content/Context;

    .line 1035
    const-string v2, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-static {v0, v2}, Lcom/yandex/metrica/impl/am;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 222
    if-eqz v0, :cond_2

    .line 224
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getAllCellInfo()Ljava/util/List;

    move-result-object v6

    .line 225
    invoke-static {v6}, Lcom/yandex/metrica/impl/bl;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_2

    move v3, v4

    .line 226
    :goto_0
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 227
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/CellInfo;

    .line 2166
    instance-of v2, v0, Landroid/telephony/CellInfoGsm;

    if-eqz v2, :cond_1

    .line 2167
    new-instance v2, Lcom/yandex/metrica/impl/ob/gc$c;

    invoke-direct {v2}, Lcom/yandex/metrica/impl/ob/gc$c;-><init>()V

    .line 2161
    :goto_1
    if-nez v2, :cond_6

    move-object v0, v1

    .line 228
    :goto_2
    if-eqz v0, :cond_0

    .line 229
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 2169
    :cond_1
    instance-of v2, v0, Landroid/telephony/CellInfoCdma;

    if-eqz v2, :cond_3

    .line 2170
    new-instance v2, Lcom/yandex/metrica/impl/ob/gc$a;

    invoke-direct {v2}, Lcom/yandex/metrica/impl/ob/gc$a;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 236
    :cond_2
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_7

    .line 237
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/yandex/metrica/impl/ob/gc;

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/ge;->c()Lcom/yandex/metrica/impl/ob/gj;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/gj;->b()Lcom/yandex/metrica/impl/ob/gc;

    move-result-object v1

    aput-object v1, v0, v4

    .line 239
    :goto_3
    return-object v0

    .line 2172
    :cond_3
    :try_start_1
    instance-of v2, v0, Landroid/telephony/CellInfoLte;

    if-eqz v2, :cond_4

    .line 2173
    new-instance v2, Lcom/yandex/metrica/impl/ob/gc$d;

    invoke-direct {v2}, Lcom/yandex/metrica/impl/ob/gc$d;-><init>()V

    goto :goto_1

    .line 2175
    :cond_4
    const/16 v2, 0x12

    invoke-static {v2}, Lcom/yandex/metrica/impl/bl;->a(I)Z

    move-result v2

    if-eqz v2, :cond_5

    instance-of v2, v0, Landroid/telephony/CellInfoWcdma;

    if-eqz v2, :cond_5

    .line 2176
    new-instance v2, Lcom/yandex/metrica/impl/ob/gc$e;

    invoke-direct {v2}, Lcom/yandex/metrica/impl/ob/gc$e;-><init>()V

    goto :goto_1

    :cond_5
    move-object v2, v1

    .line 2178
    goto :goto_1

    .line 2161
    :cond_6
    invoke-virtual {v2, v0}, Lcom/yandex/metrica/impl/ob/gc$b;->a(Landroid/telephony/CellInfo;)Lcom/yandex/metrica/impl/ob/gc;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_2

    .line 239
    :cond_7
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/yandex/metrica/impl/ob/gc;

    invoke-interface {v5, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/yandex/metrica/impl/ob/gc;

    goto :goto_3
.end method

.method e()Ljava/lang/Integer;
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 296
    :try_start_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/ge;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->i:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/metrica/impl/am;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 298
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v0

    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    .line 300
    if-eqz v0, :cond_2

    .line 301
    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v0

    .line 303
    :goto_0
    if-eq v2, v0, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 310
    :goto_1
    return-object v0

    :cond_0
    move-object v0, v1

    .line 303
    goto :goto_1

    :catch_0
    move-exception v0

    :cond_1
    move-object v0, v1

    .line 310
    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method f()Ljava/lang/Integer;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 317
    :try_start_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/ge;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 318
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->i:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/metrica/impl/am;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 319
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ge;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v0

    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v0

    .line 320
    const/4 v2, -0x1

    if-eq v2, v0, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 327
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    .line 320
    goto :goto_0

    :catch_0
    move-exception v0

    :cond_1
    move-object v0, v1

    .line 327
    goto :goto_0
.end method

.method g()Lcom/yandex/metrica/impl/ob/gc;
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 431
    new-instance v0, Lcom/yandex/metrica/impl/ob/gc;

    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ge;->l()Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ge;->m()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/ge;->f()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/ge;->e()Ljava/lang/Integer;

    move-result-object v4

    .line 432
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ge;->p()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ge;->r()Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v10, v7

    invoke-direct/range {v0 .. v10}, Lcom/yandex/metrica/impl/ob/gc;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;ZILjava/lang/Integer;)V

    .line 434
    return-object v0
.end method

.method h()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/yandex/metrica/impl/ob/gh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 439
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 440
    const/16 v1, 0x17

    invoke-static {v1}, Lcom/yandex/metrica/impl/bl;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 441
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ge;->w()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 442
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 443
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ge;->v()Lcom/yandex/metrica/impl/ob/gh;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 448
    :cond_0
    :goto_0
    return-object v0

    .line 446
    :cond_1
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ge;->v()Lcom/yandex/metrica/impl/ob/gh;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method i()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 453
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 454
    const/16 v1, 0x17

    invoke-static {v1}, Lcom/yandex/metrica/impl/bl;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 455
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ge;->t()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 460
    :goto_0
    return-object v0

    .line 457
    :cond_0
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ge;->s()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method j()Z
    .locals 1

    .prologue
    .line 489
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/ob/ge;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
