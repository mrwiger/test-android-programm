.class public Lcom/my/target/ce;
.super Ljava/lang/Object;
.source "ClickHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/ce$e;,
        Lcom/my/target/ce$d;,
        Lcom/my/target/ce$c;,
        Lcom/my/target/ce$b;,
        Lcom/my/target/ce$a;
    }
.end annotation


# static fields
.field private static final jH:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/my/target/ah;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lcom/my/target/ce;->jH:Ljava/util/WeakHashMap;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    return-void
.end method

.method static synthetic a(Lcom/my/target/ce;Ljava/lang/String;Lcom/my/target/ah;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/my/target/ce;->b(Ljava/lang/String;Lcom/my/target/ah;Landroid/content/Context;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/my/target/ah;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 80
    invoke-virtual {p2}, Lcom/my/target/ah;->isDirectLink()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/my/target/cn;->S(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/my/target/ce;->b(Ljava/lang/String;Lcom/my/target/ah;Landroid/content/Context;)V

    .line 100
    :goto_0
    return-void

    .line 86
    :cond_1
    sget-object v0, Lcom/my/target/ce;->jH:Ljava/util/WeakHashMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    invoke-static {p1}, Lcom/my/target/cn;->U(Ljava/lang/String;)Lcom/my/target/cn;

    move-result-object v0

    new-instance v1, Lcom/my/target/ce$1;

    invoke-direct {v1, p0, p2, p3}, Lcom/my/target/ce$1;-><init>(Lcom/my/target/ce;Lcom/my/target/ah;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/my/target/cn;->a(Lcom/my/target/cn$a;)Lcom/my/target/cn;

    move-result-object v0

    .line 98
    invoke-virtual {v0, p3}, Lcom/my/target/cn;->y(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Lcom/my/target/ah;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 104
    invoke-static {p1, p2}, Lcom/my/target/ce$a;->a(Ljava/lang/String;Lcom/my/target/ah;)Lcom/my/target/ce$a;

    move-result-object v0

    .line 105
    invoke-virtual {v0, p3}, Lcom/my/target/ce$a;->r(Landroid/content/Context;)Z

    .line 106
    return-void
.end method

.method public static bw()Lcom/my/target/ce;
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/my/target/ce;

    invoke-direct {v0}, Lcom/my/target/ce;-><init>()V

    return-object v0
.end method

.method static synthetic bx()Ljava/util/WeakHashMap;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/my/target/ce;->jH:Ljava/util/WeakHashMap;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/my/target/ah;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p1}, Lcom/my/target/ah;->getTrackingLink()Ljava/lang/String;

    move-result-object v0

    .line 56
    invoke-virtual {p0, p1, v0, p2}, Lcom/my/target/ce;->a(Lcom/my/target/ah;Ljava/lang/String;Landroid/content/Context;)V

    .line 57
    return-void
.end method

.method public a(Lcom/my/target/ah;Ljava/lang/String;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 61
    sget-object v0, Lcom/my/target/ce;->jH:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    invoke-static {p1}, Lcom/my/target/ce$a;->a(Lcom/my/target/ah;)Lcom/my/target/ce$a;

    move-result-object v0

    .line 66
    invoke-virtual {v0, p3}, Lcom/my/target/ce$a;->r(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    if-eqz p2, :cond_2

    .line 73
    invoke-direct {p0, p2, p1, p3}, Lcom/my/target/ce;->a(Ljava/lang/String;Lcom/my/target/ah;Landroid/content/Context;)V

    .line 75
    :cond_2
    invoke-virtual {p1}, Lcom/my/target/ah;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    const-string v1, "click"

    invoke-virtual {v0, v1}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    goto :goto_0
.end method
