.class public final Lcom/my/target/core/parsers/f;
.super Ljava/lang/Object;
.source "InterstitialSliderAdBannerParser.java"


# instance fields
.field private final A:Lcom/my/target/be;

.field private final B:Lcom/my/target/core/parsers/d;

.field private final n:Lcom/my/target/dw;


# direct methods
.method private constructor <init>(Lcom/my/target/dw;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/my/target/core/parsers/f;->n:Lcom/my/target/dw;

    .line 36
    invoke-static {p1, p2, p3, p4}, Lcom/my/target/be;->a(Lcom/my/target/ak;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/be;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/parsers/f;->A:Lcom/my/target/be;

    .line 37
    invoke-static {p2, p3, p4}, Lcom/my/target/core/parsers/d;->a(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/d;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/parsers/f;->B:Lcom/my/target/core/parsers/d;

    .line 38
    return-void
.end method

.method public static a(Lcom/my/target/dw;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/f;
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/my/target/core/parsers/f;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/my/target/core/parsers/f;-><init>(Lcom/my/target/dw;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;Lcom/my/target/core/models/banners/g;)Z
    .locals 2

    .prologue
    .line 42
    .line 1048
    iget-object v0, p0, Lcom/my/target/core/parsers/f;->A:Lcom/my/target/be;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/my/target/be;->a(Lorg/json/JSONObject;Lcom/my/target/ah;Lcom/my/target/af;)V

    .line 1049
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/d;->setAllowClose(Z)V

    .line 1050
    const-string v0, "close_icon_hd"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1051
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1053
    invoke-static {v0}, Lcom/my/target/common/models/ImageData;->newImageData(Ljava/lang/String;)Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 1054
    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/d;->setCloseIcon(Lcom/my/target/common/models/ImageData;)V

    .line 43
    :goto_0
    iget-object v0, p0, Lcom/my/target/core/parsers/f;->B:Lcom/my/target/core/parsers/d;

    invoke-virtual {v0, p1, p2}, Lcom/my/target/core/parsers/d;->b(Lorg/json/JSONObject;Lcom/my/target/core/models/banners/g;)Z

    move-result v0

    return v0

    .line 1058
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/parsers/f;->n:Lcom/my/target/dw;

    invoke-virtual {v0}, Lcom/my/target/dw;->getCloseIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/d;->setCloseIcon(Lcom/my/target/common/models/ImageData;)V

    goto :goto_0
.end method
