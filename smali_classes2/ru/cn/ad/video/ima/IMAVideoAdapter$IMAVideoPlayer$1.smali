.class Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer$1;
.super Landroid/os/AsyncTask;
.source "IMAVideoAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->playAd()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lru/cn/player/youtube/YoutubeVideo;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;


# direct methods
.method constructor <init>(Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;)V
    .locals 0
    .param p1, "this$1"    # Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;

    .prologue
    .line 297
    iput-object p1, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer$1;->this$1:Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 297
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer$1;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 1
    .param p1, "params"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Lru/cn/player/youtube/YoutubeVideo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 301
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer$1;->this$1:Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;

    invoke-static {v0}, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->access$000(Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lru/cn/player/youtube/YoutubeParser;->getVideoURL(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 297
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer$1;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/player/youtube/YoutubeVideo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 306
    .local p1, "videos":Ljava/util/List;, "Ljava/util/List<Lru/cn/player/youtube/YoutubeVideo;>;"
    invoke-virtual {p0}, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer$1;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 325
    :cond_0
    :goto_0
    return-void

    .line 309
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/player/youtube/YoutubeVideo;

    .line 310
    .local v1, "video":Lru/cn/player/youtube/YoutubeVideo;
    iget-object v3, v1, Lru/cn/player/youtube/YoutubeVideo;->format:Lru/cn/player/youtube/Format;

    iget v3, v3, Lru/cn/player/youtube/Format;->id:I

    invoke-static {v3}, Lru/cn/player/youtube/YoutubeParser;->isFormatSupported(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 311
    const-string v2, "IMAVideoAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Play advertisement "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer$1;->this$1:Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;

    invoke-static {v4}, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->access$000(Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " youtubeUri "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lru/cn/player/youtube/YoutubeVideo;->videoURL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lru/cn/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    iget-object v2, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer$1;->this$1:Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;

    invoke-static {v2}, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->access$100(Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;)Lru/cn/player/SimplePlayer;

    move-result-object v2

    iget-object v3, v1, Lru/cn/player/youtube/YoutubeVideo;->videoURL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lru/cn/player/SimplePlayer;->play(Ljava/lang/String;)V

    goto :goto_0

    .line 320
    .end local v1    # "video":Lru/cn/player/youtube/YoutubeVideo;
    :cond_3
    iget-object v2, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer$1;->this$1:Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;

    iget-object v2, v2, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->this$0:Lru/cn/ad/video/ima/IMAVideoAdapter;

    sget-object v3, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;->VAST_LINEAR_ASSET_MISMATCH:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;

    invoke-static {v2, v3}, Lru/cn/ad/video/ima/IMAVideoAdapter;->access$200(Lru/cn/ad/video/ima/IMAVideoAdapter;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;)V

    .line 322
    iget-object v2, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer$1;->this$1:Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;

    invoke-static {v2}, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->access$300(Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;

    .line 323
    .local v0, "callback":Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;
    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;->onError()V

    goto :goto_1
.end method
