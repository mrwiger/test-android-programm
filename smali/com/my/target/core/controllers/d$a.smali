.class final Lcom/my/target/core/controllers/d$a;
.super Ljava/lang/Object;
.source "InstreamAdVideoController.java"

# interfaces
.implements Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/core/controllers/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic t:Lcom/my/target/core/controllers/d;

.field private volume:F


# direct methods
.method private constructor <init>(Lcom/my/target/core/controllers/d;)V
    .locals 1

    .prologue
    .line 454
    iput-object p1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 456
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/my/target/core/controllers/d$a;->volume:F

    return-void
.end method

.method synthetic constructor <init>(Lcom/my/target/core/controllers/d;B)V
    .locals 0

    .prologue
    .line 454
    invoke-direct {p0, p1}, Lcom/my/target/core/controllers/d$a;-><init>(Lcom/my/target/core/controllers/d;)V

    return-void
.end method


# virtual methods
.method public final onAdVideoCompleted()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 538
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->j(Lcom/my/target/core/controllers/d;)I

    move-result v0

    if-eq v0, v3, :cond_1

    .line 540
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->i(Lcom/my/target/core/controllers/d;)Lcom/my/target/aj;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->k(Lcom/my/target/core/controllers/d;)Lcom/my/target/core/controllers/d$d;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->l(Lcom/my/target/core/controllers/d;)V

    .line 543
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->i(Lcom/my/target/core/controllers/d;)Lcom/my/target/aj;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 545
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->i(Lcom/my/target/core/controllers/d;)Lcom/my/target/aj;

    move-result-object v0

    .line 546
    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->m(Lcom/my/target/core/controllers/d;)Lcom/my/target/aj;

    .line 547
    if-eqz v0, :cond_0

    .line 549
    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-virtual {v0}, Lcom/my/target/aj;->getDuration()F

    move-result v2

    invoke-static {v1, v2}, Lcom/my/target/core/controllers/d;->b(Lcom/my/target/core/controllers/d;F)V

    .line 550
    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->k(Lcom/my/target/core/controllers/d;)Lcom/my/target/core/controllers/d$d;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/my/target/core/controllers/d$d;->onBannerCompleted(Lcom/my/target/aj;)V

    .line 555
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0, v3}, Lcom/my/target/core/controllers/d;->a(Lcom/my/target/core/controllers/d;I)I

    .line 557
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->d(Lcom/my/target/core/controllers/d;)Lcom/my/target/ck;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->c(Lcom/my/target/core/controllers/d;)Lcom/my/target/core/controllers/d$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    .line 558
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->h(Lcom/my/target/core/controllers/d;)Lcom/my/target/ck;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->g(Lcom/my/target/core/controllers/d;)Lcom/my/target/core/controllers/d$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    .line 559
    return-void
.end method

.method public final onAdVideoError(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 527
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->i(Lcom/my/target/core/controllers/d;)Lcom/my/target/aj;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->k(Lcom/my/target/core/controllers/d;)Lcom/my/target/core/controllers/d$d;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 529
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->k(Lcom/my/target/core/controllers/d;)Lcom/my/target/core/controllers/d$d;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->i(Lcom/my/target/core/controllers/d;)Lcom/my/target/aj;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/my/target/core/controllers/d$d;->onBannerError(Ljava/lang/String;Lcom/my/target/aj;)V

    .line 531
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->d(Lcom/my/target/core/controllers/d;)Lcom/my/target/ck;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->c(Lcom/my/target/core/controllers/d;)Lcom/my/target/core/controllers/d$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    .line 532
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->h(Lcom/my/target/core/controllers/d;)Lcom/my/target/ck;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->g(Lcom/my/target/core/controllers/d;)Lcom/my/target/core/controllers/d$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    .line 533
    return-void
.end method

.method public final onAdVideoPaused()V
    .locals 3

    .prologue
    .line 479
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/d;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 480
    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->i(Lcom/my/target/core/controllers/d;)Lcom/my/target/aj;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 482
    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->i(Lcom/my/target/core/controllers/d;)Lcom/my/target/aj;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v1

    const-string v2, "playbackPaused"

    invoke-virtual {v1, v2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 484
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->d(Lcom/my/target/core/controllers/d;)Lcom/my/target/ck;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->c(Lcom/my/target/core/controllers/d;)Lcom/my/target/core/controllers/d$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    .line 485
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->h(Lcom/my/target/core/controllers/d;)Lcom/my/target/ck;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->g(Lcom/my/target/core/controllers/d;)Lcom/my/target/core/controllers/d$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    .line 486
    return-void
.end method

.method public final onAdVideoResumed()V
    .locals 3

    .prologue
    .line 491
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/d;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 492
    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->i(Lcom/my/target/core/controllers/d;)Lcom/my/target/aj;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 494
    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->i(Lcom/my/target/core/controllers/d;)Lcom/my/target/aj;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v1

    const-string v2, "playbackResumed"

    invoke-virtual {v1, v2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 496
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->d(Lcom/my/target/core/controllers/d;)Lcom/my/target/ck;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->c(Lcom/my/target/core/controllers/d;)Lcom/my/target/core/controllers/d$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->d(Ljava/lang/Runnable;)V

    .line 497
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->e(Lcom/my/target/core/controllers/d;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->f(Lcom/my/target/core/controllers/d;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 499
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->h(Lcom/my/target/core/controllers/d;)Lcom/my/target/ck;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->g(Lcom/my/target/core/controllers/d;)Lcom/my/target/core/controllers/d$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->d(Ljava/lang/Runnable;)V

    .line 502
    :cond_2
    return-void
.end method

.method public final onAdVideoStarted()V
    .locals 2

    .prologue
    .line 461
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/my/target/core/controllers/d;->a(Lcom/my/target/core/controllers/d;I)I

    .line 462
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->a(Lcom/my/target/core/controllers/d;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 464
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->b(Lcom/my/target/core/controllers/d;)Lcom/my/target/instreamads/InstreamAdPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->b(Lcom/my/target/core/controllers/d;)Lcom/my/target/instreamads/InstreamAdPlayer;

    move-result-object v1

    invoke-interface {v1}, Lcom/my/target/instreamads/InstreamAdPlayer;->getAdVideoDuration()F

    move-result v1

    invoke-static {v0, v1}, Lcom/my/target/core/controllers/d;->a(Lcom/my/target/core/controllers/d;F)V

    .line 469
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->d(Lcom/my/target/core/controllers/d;)Lcom/my/target/ck;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->c(Lcom/my/target/core/controllers/d;)Lcom/my/target/core/controllers/d$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->d(Ljava/lang/Runnable;)V

    .line 470
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->e(Lcom/my/target/core/controllers/d;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->f(Lcom/my/target/core/controllers/d;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 472
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->h(Lcom/my/target/core/controllers/d;)Lcom/my/target/ck;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->g(Lcom/my/target/core/controllers/d;)Lcom/my/target/core/controllers/d$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->d(Ljava/lang/Runnable;)V

    .line 474
    :cond_2
    return-void
.end method

.method public final onAdVideoStopped()V
    .locals 3

    .prologue
    .line 507
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->j(Lcom/my/target/core/controllers/d;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 509
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->i(Lcom/my/target/core/controllers/d;)Lcom/my/target/aj;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->k(Lcom/my/target/core/controllers/d;)Lcom/my/target/core/controllers/d$d;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 511
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/d;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 512
    if-eqz v0, :cond_0

    .line 514
    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->i(Lcom/my/target/core/controllers/d;)Lcom/my/target/aj;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v1

    const-string v2, "playbackStopped"

    invoke-virtual {v1, v2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 516
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->k(Lcom/my/target/core/controllers/d;)Lcom/my/target/core/controllers/d$d;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->i(Lcom/my/target/core/controllers/d;)Lcom/my/target/aj;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/my/target/core/controllers/d$d;->onBannerStopped(Lcom/my/target/aj;)V

    .line 518
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/my/target/core/controllers/d;->a(Lcom/my/target/core/controllers/d;I)I

    .line 520
    :cond_2
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->d(Lcom/my/target/core/controllers/d;)Lcom/my/target/ck;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->c(Lcom/my/target/core/controllers/d;)Lcom/my/target/core/controllers/d$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    .line 521
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0}, Lcom/my/target/core/controllers/d;->h(Lcom/my/target/core/controllers/d;)Lcom/my/target/ck;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->g(Lcom/my/target/core/controllers/d;)Lcom/my/target/core/controllers/d$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    .line 522
    return-void
.end method

.method public final onVolumeChanged(F)V
    .locals 3
    .param p1, "volume"    # F

    .prologue
    const/4 v1, 0x0

    .line 564
    iget v0, p0, Lcom/my/target/core/controllers/d$a;->volume:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    .line 589
    :cond_0
    :goto_0
    return-void

    .line 568
    :cond_1
    iget v0, p0, Lcom/my/target/core/controllers/d$a;->volume:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    cmpg-float v0, p1, v1

    if-gtz v0, :cond_2

    .line 570
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/d;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 571
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->i(Lcom/my/target/core/controllers/d;)Lcom/my/target/aj;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 573
    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->i(Lcom/my/target/core/controllers/d;)Lcom/my/target/aj;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v1

    const-string v2, "volumeOff"

    invoke-virtual {v1, v2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 574
    iput p1, p0, Lcom/my/target/core/controllers/d$a;->volume:F

    .line 575
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0, p1}, Lcom/my/target/core/controllers/d;->c(Lcom/my/target/core/controllers/d;F)F

    goto :goto_0

    .line 578
    :cond_2
    iget v0, p0, Lcom/my/target/core/controllers/d$a;->volume:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    cmpl-float v0, p1, v1

    if-lez v0, :cond_0

    .line 580
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/d;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 581
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->i(Lcom/my/target/core/controllers/d;)Lcom/my/target/aj;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 583
    iget-object v1, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v1}, Lcom/my/target/core/controllers/d;->i(Lcom/my/target/core/controllers/d;)Lcom/my/target/aj;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v1

    const-string v2, "volumeOn"

    invoke-virtual {v1, v2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 584
    iput p1, p0, Lcom/my/target/core/controllers/d$a;->volume:F

    .line 585
    iget-object v0, p0, Lcom/my/target/core/controllers/d$a;->t:Lcom/my/target/core/controllers/d;

    invoke-static {v0, p1}, Lcom/my/target/core/controllers/d;->c(Lcom/my/target/core/controllers/d;F)F

    goto :goto_0
.end method
