.class public abstract Lcom/annimon/stream/iterator/LsaIterator;
.super Ljava/lang/Object;
.source "LsaIterator.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    .local p0, "this":Lcom/annimon/stream/iterator/LsaIterator;, "Lcom/annimon/stream/iterator/LsaIterator<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 19
    .local p0, "this":Lcom/annimon/stream/iterator/LsaIterator;, "Lcom/annimon/stream/iterator/LsaIterator<TT;>;"
    invoke-virtual {p0}, Lcom/annimon/stream/iterator/LsaIterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 20
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 22
    :cond_0
    invoke-virtual {p0}, Lcom/annimon/stream/iterator/LsaIterator;->nextIteration()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public abstract nextIteration()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 14
    .local p0, "this":Lcom/annimon/stream/iterator/LsaIterator;, "Lcom/annimon/stream/iterator/LsaIterator<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "remove not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
