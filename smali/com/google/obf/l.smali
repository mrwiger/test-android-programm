.class public Lcom/google/obf/l;
.super Lcom/google/obf/n;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/k;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/l$a;
    }
.end annotation


# instance fields
.field private final c:Lcom/google/obf/l$a;

.field private final d:Lcom/google/obf/aa;

.field private e:Z

.field private f:Landroid/media/MediaFormat;

.field private g:I

.field private h:I

.field private i:J

.field private j:Z

.field private k:Z

.field private l:J


# direct methods
.method public constructor <init>(Lcom/google/obf/u;Lcom/google/obf/m;Lcom/google/obf/ac;ZLandroid/os/Handler;Lcom/google/obf/l$a;Lcom/google/obf/z;I)V
    .locals 9

    .prologue
    .line 1
    const/4 v0, 0x1

    new-array v1, v0, [Lcom/google/obf/u;

    const/4 v0, 0x0

    aput-object p1, v1, v0

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lcom/google/obf/l;-><init>([Lcom/google/obf/u;Lcom/google/obf/m;Lcom/google/obf/ac;ZLandroid/os/Handler;Lcom/google/obf/l$a;Lcom/google/obf/z;I)V

    .line 2
    return-void
.end method

.method public constructor <init>([Lcom/google/obf/u;Lcom/google/obf/m;Lcom/google/obf/ac;ZLandroid/os/Handler;Lcom/google/obf/l$a;Lcom/google/obf/z;I)V
    .locals 1

    .prologue
    .line 3
    invoke-direct/range {p0 .. p6}, Lcom/google/obf/n;-><init>([Lcom/google/obf/u;Lcom/google/obf/m;Lcom/google/obf/ac;ZLandroid/os/Handler;Lcom/google/obf/n$b;)V

    .line 4
    iput-object p6, p0, Lcom/google/obf/l;->c:Lcom/google/obf/l$a;

    .line 5
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/l;->h:I

    .line 6
    new-instance v0, Lcom/google/obf/aa;

    invoke-direct {v0, p7, p8}, Lcom/google/obf/aa;-><init>(Lcom/google/obf/z;I)V

    iput-object v0, p0, Lcom/google/obf/l;->d:Lcom/google/obf/aa;

    .line 7
    return-void
.end method

.method static synthetic a(Lcom/google/obf/l;)Lcom/google/obf/l$a;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/obf/l;->c:Lcom/google/obf/l$a;

    return-object v0
.end method

.method private a(IJJ)V
    .locals 8

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/obf/l;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/l;->c:Lcom/google/obf/l$a;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/google/obf/l;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/obf/l$3;

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/obf/l$3;-><init>(Lcom/google/obf/l;IJJ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 132
    :cond_0
    return-void
.end method

.method private a(Lcom/google/obf/aa$d;)V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/obf/l;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/l;->c:Lcom/google/obf/l$a;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/google/obf/l;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/obf/l$1;

    invoke-direct {v1, p0, p1}, Lcom/google/obf/l$1;-><init>(Lcom/google/obf/l;Lcom/google/obf/aa$d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 126
    :cond_0
    return-void
.end method

.method private a(Lcom/google/obf/aa$f;)V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/obf/l;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/l;->c:Lcom/google/obf/l$a;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/google/obf/l;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/obf/l$2;

    invoke-direct {v1, p0, p1}, Lcom/google/obf/l$2;-><init>(Lcom/google/obf/l;Lcom/google/obf/aa$f;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 129
    :cond_0
    return-void
.end method


# virtual methods
.method public a()J
    .locals 4

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/obf/l;->d:Lcom/google/obf/aa;

    invoke-virtual {p0}, Lcom/google/obf/l;->e()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/obf/aa;->a(Z)J

    move-result-wide v0

    .line 54
    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 55
    iget-boolean v2, p0, Lcom/google/obf/l;->j:Z

    if-eqz v2, :cond_1

    .line 56
    :goto_0
    iput-wide v0, p0, Lcom/google/obf/l;->i:J

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/l;->j:Z

    .line 58
    :cond_0
    iget-wide v0, p0, Lcom/google/obf/l;->i:J

    return-wide v0

    .line 56
    :cond_1
    iget-wide v2, p0, Lcom/google/obf/l;->i:J

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method protected a(Lcom/google/obf/m;Ljava/lang/String;Z)Lcom/google/obf/f;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/o$b;
        }
    .end annotation

    .prologue
    .line 13
    invoke-virtual {p0, p2}, Lcom/google/obf/l;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 14
    invoke-interface {p1}, Lcom/google/obf/m;->a()Lcom/google/obf/f;

    move-result-object v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/obf/l;->e:Z

    .line 19
    :goto_0
    return-object v0

    .line 18
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/l;->e:Z

    .line 19
    invoke-super {p0, p1, p2, p3}, Lcom/google/obf/n;->a(Lcom/google/obf/m;Ljava/lang/String;Z)Lcom/google/obf/f;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(I)V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method public a(ILjava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 114
    packed-switch p1, :pswitch_data_0

    .line 122
    invoke-super {p0, p1, p2}, Lcom/google/obf/n;->a(ILjava/lang/Object;)V

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 115
    :pswitch_0
    iget-object v0, p0, Lcom/google/obf/l;->d:Lcom/google/obf/aa;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/obf/aa;->a(F)V

    goto :goto_0

    .line 117
    :pswitch_1
    iget-object v0, p0, Lcom/google/obf/l;->d:Lcom/google/obf/aa;

    check-cast p2, Landroid/media/PlaybackParams;

    invoke-virtual {v0, p2}, Lcom/google/obf/aa;->a(Landroid/media/PlaybackParams;)V

    goto :goto_0

    .line 119
    :pswitch_2
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 120
    iget-object v1, p0, Lcom/google/obf/l;->d:Lcom/google/obf/aa;

    invoke-virtual {v1, v0}, Lcom/google/obf/aa;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/l;->h:I

    goto :goto_0

    .line 114
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected a(J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 65
    invoke-super {p0, p1, p2}, Lcom/google/obf/n;->a(J)V

    .line 66
    iget-object v0, p0, Lcom/google/obf/l;->d:Lcom/google/obf/aa;

    invoke-virtual {v0}, Lcom/google/obf/aa;->j()V

    .line 67
    iput-wide p1, p0, Lcom/google/obf/l;->i:J

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/l;->j:Z

    .line 69
    return-void
.end method

.method protected a(Landroid/media/MediaCodec;Landroid/media/MediaFormat;)V
    .locals 5

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/obf/l;->f:Landroid/media/MediaFormat;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 36
    :goto_0
    if-eqz v1, :cond_2

    .line 37
    iget-object v0, p0, Lcom/google/obf/l;->f:Landroid/media/MediaFormat;

    const-string v2, "mime"

    invoke-virtual {v0, v2}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 39
    :goto_1
    if-eqz v1, :cond_0

    iget-object p2, p0, Lcom/google/obf/l;->f:Landroid/media/MediaFormat;

    .line 40
    :cond_0
    const-string v1, "channel-count"

    invoke-virtual {p2, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    .line 41
    const-string v2, "sample-rate"

    invoke-virtual {p2, v2}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v2

    .line 42
    iget-object v3, p0, Lcom/google/obf/l;->d:Lcom/google/obf/aa;

    iget v4, p0, Lcom/google/obf/l;->g:I

    invoke-virtual {v3, v0, v1, v2, v4}, Lcom/google/obf/aa;->a(Ljava/lang/String;III)V

    .line 43
    return-void

    .line 35
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 38
    :cond_2
    const-string v0, "audio/raw"

    goto :goto_1
.end method

.method protected a(Landroid/media/MediaCodec;ZLandroid/media/MediaFormat;Landroid/media/MediaCrypto;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 21
    const-string v0, "mime"

    invoke-virtual {p3, v0}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 22
    iget-boolean v1, p0, Lcom/google/obf/l;->e:Z

    if-eqz v1, :cond_0

    .line 23
    const-string v1, "mime"

    const-string v2, "audio/raw"

    invoke-virtual {p3, v1, v2}, Landroid/media/MediaFormat;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    invoke-virtual {p1, p3, v3, p4, v4}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 25
    const-string v1, "mime"

    invoke-virtual {p3, v1, v0}, Landroid/media/MediaFormat;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    iput-object p3, p0, Lcom/google/obf/l;->f:Landroid/media/MediaFormat;

    .line 29
    :goto_0
    return-void

    .line 27
    :cond_0
    invoke-virtual {p1, p3, v3, p4, v4}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 28
    iput-object v3, p0, Lcom/google/obf/l;->f:Landroid/media/MediaFormat;

    goto :goto_0
.end method

.method protected a(Lcom/google/obf/r;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/google/obf/n;->a(Lcom/google/obf/r;)V

    .line 32
    const-string v0, "audio/raw"

    iget-object v1, p1, Lcom/google/obf/r;->a:Lcom/google/obf/q;

    iget-object v1, v1, Lcom/google/obf/q;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/obf/r;->a:Lcom/google/obf/q;

    iget v0, v0, Lcom/google/obf/q;->s:I

    .line 33
    :goto_0
    iput v0, p0, Lcom/google/obf/l;->g:I

    .line 34
    return-void

    .line 33
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method protected a(JJLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;IZ)Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/google/obf/l;->e:Z

    if-eqz v0, :cond_0

    iget v0, p7, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 71
    const/4 v0, 0x0

    invoke-virtual {p5, p8, v0}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 72
    const/4 v0, 0x1

    .line 110
    :goto_0
    return v0

    .line 73
    :cond_0
    if-eqz p9, :cond_1

    .line 74
    const/4 v0, 0x0

    invoke-virtual {p5, p8, v0}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 75
    iget-object v0, p0, Lcom/google/obf/l;->a:Lcom/google/obf/c;

    iget v1, v0, Lcom/google/obf/c;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/obf/c;->g:I

    .line 76
    iget-object v0, p0, Lcom/google/obf/l;->d:Lcom/google/obf/aa;

    invoke-virtual {v0}, Lcom/google/obf/aa;->f()V

    .line 77
    const/4 v0, 0x1

    goto :goto_0

    .line 78
    :cond_1
    iget-object v0, p0, Lcom/google/obf/l;->d:Lcom/google/obf/aa;

    invoke-virtual {v0}, Lcom/google/obf/aa;->a()Z

    move-result v0

    if-nez v0, :cond_5

    .line 79
    :try_start_0
    iget v0, p0, Lcom/google/obf/l;->h:I

    if-eqz v0, :cond_4

    .line 80
    iget-object v0, p0, Lcom/google/obf/l;->d:Lcom/google/obf/aa;

    iget v1, p0, Lcom/google/obf/l;->h:I

    invoke-virtual {v0, v1}, Lcom/google/obf/aa;->a(I)I

    .line 83
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/l;->k:Z
    :try_end_0
    .catch Lcom/google/obf/aa$d; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    invoke-virtual {p0}, Lcom/google/obf/l;->v()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 89
    iget-object v0, p0, Lcom/google/obf/l;->d:Lcom/google/obf/aa;

    invoke-virtual {v0}, Lcom/google/obf/aa;->e()V

    .line 97
    :cond_2
    :goto_2
    :try_start_1
    iget-object v0, p0, Lcom/google/obf/l;->d:Lcom/google/obf/aa;

    iget v2, p7, Landroid/media/MediaCodec$BufferInfo;->offset:I

    iget v3, p7, Landroid/media/MediaCodec$BufferInfo;->size:I

    iget-wide v4, p7, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    move-object v1, p6

    invoke-virtual/range {v0 .. v5}, Lcom/google/obf/aa;->a(Ljava/nio/ByteBuffer;IIJ)I

    move-result v0

    .line 98
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/obf/l;->l:J
    :try_end_1
    .catch Lcom/google/obf/aa$f; {:try_start_1 .. :try_end_1} :catch_1

    .line 103
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_3

    .line 104
    invoke-virtual {p0}, Lcom/google/obf/l;->i()V

    .line 105
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/obf/l;->j:Z

    .line 106
    :cond_3
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_7

    .line 107
    const/4 v0, 0x0

    invoke-virtual {p5, p8, v0}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 108
    iget-object v0, p0, Lcom/google/obf/l;->a:Lcom/google/obf/c;

    iget v1, v0, Lcom/google/obf/c;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/obf/c;->f:I

    .line 109
    const/4 v0, 0x1

    goto :goto_0

    .line 81
    :cond_4
    :try_start_2
    iget-object v0, p0, Lcom/google/obf/l;->d:Lcom/google/obf/aa;

    invoke-virtual {v0}, Lcom/google/obf/aa;->b()I

    move-result v0

    iput v0, p0, Lcom/google/obf/l;->h:I

    .line 82
    iget v0, p0, Lcom/google/obf/l;->h:I

    invoke-virtual {p0, v0}, Lcom/google/obf/l;->a(I)V
    :try_end_2
    .catch Lcom/google/obf/aa$d; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 85
    :catch_0
    move-exception v0

    .line 86
    invoke-direct {p0, v0}, Lcom/google/obf/l;->a(Lcom/google/obf/aa$d;)V

    .line 87
    new-instance v1, Lcom/google/obf/g;

    invoke-direct {v1, v0}, Lcom/google/obf/g;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 90
    :cond_5
    iget-boolean v0, p0, Lcom/google/obf/l;->k:Z

    .line 91
    iget-object v1, p0, Lcom/google/obf/l;->d:Lcom/google/obf/aa;

    invoke-virtual {v1}, Lcom/google/obf/aa;->h()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/obf/l;->k:Z

    .line 92
    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/obf/l;->k:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/obf/l;->v()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 93
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/obf/l;->l:J

    sub-long v4, v0, v2

    .line 94
    iget-object v0, p0, Lcom/google/obf/l;->d:Lcom/google/obf/aa;

    invoke-virtual {v0}, Lcom/google/obf/aa;->d()J

    move-result-wide v0

    .line 95
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_6

    const-wide/16 v2, -0x1

    .line 96
    :goto_3
    iget-object v0, p0, Lcom/google/obf/l;->d:Lcom/google/obf/aa;

    invoke-virtual {v0}, Lcom/google/obf/aa;->c()I

    move-result v1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/obf/l;->a(IJJ)V

    goto :goto_2

    .line 95
    :cond_6
    const-wide/16 v2, 0x3e8

    div-long v2, v0, v2

    goto :goto_3

    .line 100
    :catch_1
    move-exception v0

    .line 101
    invoke-direct {p0, v0}, Lcom/google/obf/l;->a(Lcom/google/obf/aa$f;)V

    .line 102
    new-instance v1, Lcom/google/obf/g;

    invoke-direct {v1, v0}, Lcom/google/obf/g;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 110
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method protected a(Lcom/google/obf/m;Lcom/google/obf/q;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/o$b;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 8
    iget-object v1, p2, Lcom/google/obf/q;->b:Ljava/lang/String;

    .line 9
    invoke-static {v1}, Lcom/google/obf/ds;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "audio/x-unknown"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 10
    invoke-virtual {p0, v1}, Lcom/google/obf/l;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Lcom/google/obf/m;->a()Lcom/google/obf/f;

    move-result-object v2

    if-nez v2, :cond_1

    .line 11
    :cond_0
    invoke-interface {p1, v1, v0}, Lcom/google/obf/m;->a(Ljava/lang/String;Z)Lcom/google/obf/f;

    move-result-object v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 12
    :cond_2
    return v0
.end method

.method protected a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/obf/l;->d:Lcom/google/obf/aa;

    invoke-virtual {v0, p1}, Lcom/google/obf/aa;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected b()Lcom/google/obf/k;
    .locals 0

    .prologue
    .line 30
    return-object p0
.end method

.method protected c()V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0}, Lcom/google/obf/n;->c()V

    .line 46
    iget-object v0, p0, Lcom/google/obf/l;->d:Lcom/google/obf/aa;

    invoke-virtual {v0}, Lcom/google/obf/aa;->e()V

    .line 47
    return-void
.end method

.method protected d()V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/obf/l;->d:Lcom/google/obf/aa;

    invoke-virtual {v0}, Lcom/google/obf/aa;->i()V

    .line 49
    invoke-super {p0}, Lcom/google/obf/n;->d()V

    .line 50
    return-void
.end method

.method protected e()Z
    .locals 1

    .prologue
    .line 51
    invoke-super {p0}, Lcom/google/obf/n;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/l;->d:Lcom/google/obf/aa;

    invoke-virtual {v0}, Lcom/google/obf/aa;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected f()Z
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/obf/l;->d:Lcom/google/obf/aa;

    invoke-virtual {v0}, Lcom/google/obf/aa;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/google/obf/n;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected g()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/l;->h:I

    .line 60
    :try_start_0
    iget-object v0, p0, Lcom/google/obf/l;->d:Lcom/google/obf/aa;

    invoke-virtual {v0}, Lcom/google/obf/aa;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    invoke-super {p0}, Lcom/google/obf/n;->g()V

    .line 64
    return-void

    .line 63
    :catchall_0
    move-exception v0

    invoke-super {p0}, Lcom/google/obf/n;->g()V

    throw v0
.end method

.method protected h()V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/obf/l;->d:Lcom/google/obf/aa;

    invoke-virtual {v0}, Lcom/google/obf/aa;->g()V

    .line 112
    return-void
.end method

.method protected i()V
    .locals 0

    .prologue
    .line 113
    return-void
.end method
