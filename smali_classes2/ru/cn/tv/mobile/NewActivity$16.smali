.class Lru/cn/tv/mobile/NewActivity$16;
.super Ljava/lang/Object;
.source "NewActivity.java"

# interfaces
.implements Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/mobile/NewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/mobile/NewActivity;


# direct methods
.method constructor <init>(Lru/cn/tv/mobile/NewActivity;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/mobile/NewActivity;

    .prologue
    .line 900
    iput-object p1, p0, Lru/cn/tv/mobile/NewActivity$16;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOnAirClicked(J)V
    .locals 1
    .param p1, "channelId"    # J

    .prologue
    const/4 v0, 0x0

    .line 919
    invoke-static {v0, v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->setSessionParams(II)V

    .line 923
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity$16;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-static {v0, p1, p2}, Lru/cn/tv/mobile/NewActivity;->access$1100(Lru/cn/tv/mobile/NewActivity;J)V

    .line 924
    return-void
.end method

.method public onScheduleItemClicked(JLru/cn/api/provider/cursor/ScheduleItemCursor;)V
    .locals 9
    .param p1, "telecastId"    # J
    .param p3, "telecast"    # Lru/cn/api/provider/cursor/ScheduleItemCursor;

    .prologue
    const/4 v4, 0x0

    .line 903
    invoke-static {v4, v4}, Lru/cn/domain/statistics/inetra/InetraTracker;->setSessionParams(II)V

    .line 907
    iget-object v4, p0, Lru/cn/tv/mobile/NewActivity$16;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-static {v4, p1, p2}, Lru/cn/tv/mobile/NewActivity;->access$1000(Lru/cn/tv/mobile/NewActivity;J)V

    .line 909
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 910
    .local v2, "nowMs":J
    const-wide/16 v4, 0x3e8

    div-long v4, v2, v4

    invoke-virtual {p3}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getTime()J

    move-result-wide v6

    sub-long v0, v4, v6

    .line 912
    .local v0, "elapsed":J
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-gez v4, :cond_0

    .line 913
    invoke-static {}, Lru/cn/domain/statistics/AnalyticsManager;->future_telecast_selected()V

    .line 915
    :cond_0
    return-void
.end method
