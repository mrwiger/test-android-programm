.class public Lru/cn/tv/stb/collections/CollectionTelecastAdapter;
.super Lru/cn/view/CursorRecyclerViewAdapter;
.source "CollectionTelecastAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lru/cn/view/CursorRecyclerViewAdapter",
        "<",
        "Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private kidsMode:Z

.field private layout:I


# direct methods
.method public constructor <init>(IZ)V
    .locals 1
    .param p1, "layout"    # I
    .param p2, "kidsMode"    # Z

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lru/cn/view/CursorRecyclerViewAdapter;-><init>(Landroid/database/Cursor;)V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter;->kidsMode:Z

    .line 30
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lru/cn/tv/stb/collections/CollectionTelecastAdapter;->setHasStableIds(Z)V

    .line 32
    iput p1, p0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter;->layout:I

    .line 33
    iput-boolean p2, p0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter;->kidsMode:Z

    .line 34
    return-void
.end method

.method private calculateElapsed(J)I
    .locals 9
    .param p1, "time"    # J

    .prologue
    .line 129
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long v2, v4, v6

    .line 130
    .local v2, "timeNowSec":J
    sub-long v0, v2, p1

    .line 132
    .local v0, "timeElapsed":J
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-gez v4, :cond_0

    .line 133
    const-wide/16 v0, 0x0

    .line 136
    :cond_0
    long-to-int v4, v0

    return v4
.end method


# virtual methods
.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;

    invoke-virtual {p0, p1, p2}, Lru/cn/tv/stb/collections/CollectionTelecastAdapter;->onBindViewHolder(Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;Landroid/database/Cursor;)V

    return-void
.end method

.method public onBindViewHolder(Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;Landroid/database/Cursor;)V
    .locals 19
    .param p1, "viewHolder"    # Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 45
    check-cast p2, Landroid/database/CursorWrapper;

    .end local p2    # "cursor":Landroid/database/Cursor;
    invoke-virtual/range {p2 .. p2}, Landroid/database/CursorWrapper;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v2

    check-cast v2, Lru/cn/api/provider/cursor/TelecastItemCursor;

    .line 47
    .local v2, "c":Lru/cn/api/provider/cursor/TelecastItemCursor;
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getChannelTitle()Ljava/lang/String;

    move-result-object v4

    .line 48
    .local v4, "channelTitle":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v0, v0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->channelTitle:Landroid/widget/TextView;

    move-object/from16 v16, v0

    if-eqz v16, :cond_0

    .line 49
    move-object/from16 v0, p1

    iget-object v0, v0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->channelTitle:Landroid/widget/TextView;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    :cond_0
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getTitle()Ljava/lang/String;

    move-result-object v12

    .line 53
    .local v12, "telecastTitle":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v0, v0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->telecastTitle:Landroid/widget/TextView;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getImage()Ljava/lang/String;

    move-result-object v10

    .line 56
    .local v10, "image":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v0, v0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->itemView:Landroid/view/View;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 57
    .local v5, "context":Landroid/content/Context;
    move-object/from16 v0, p1

    iget-object v0, v0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->telecastImage:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    if-eqz v16, :cond_1

    .line 58
    move-object/from16 v0, p1

    iget-object v0, v0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->telecastImage:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 59
    if-eqz v10, :cond_1

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v16

    if-lez v16, :cond_1

    .line 60
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter;->kidsMode:Z

    move/from16 v16, v0

    if-eqz v16, :cond_5

    .line 61
    invoke-static {v5}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v16

    .line 62
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v16

    new-instance v17, Lru/cn/utils/MaskTransformation;

    const v18, 0x7f080097

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v0, v5, v1}, Lru/cn/utils/MaskTransformation;-><init>(Landroid/content/Context;I)V

    .line 63
    invoke-virtual/range {v16 .. v17}, Lcom/squareup/picasso/RequestCreator;->transform(Lcom/squareup/picasso/Transformation;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v16

    move-object/from16 v0, p1

    iget-object v0, v0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->telecastImage:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    .line 64
    invoke-virtual/range {v16 .. v17}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 73
    :cond_1
    :goto_0
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getViewsCount()I

    move-result v13

    .line 74
    .local v13, "viewsCount":I
    if-lez v13, :cond_2

    .line 75
    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    .line 76
    .local v11, "stringViewsCount":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v0, v0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->viewsCountContainer:Landroid/view/View;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->setVisibility(I)V

    .line 77
    move-object/from16 v0, p1

    iget-object v0, v0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->viewsCountText:Landroid/widget/TextView;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    .end local v11    # "stringViewsCount":Ljava/lang/String;
    :cond_2
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getTime()J

    move-result-wide v14

    .line 81
    .local v14, "time":J
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getDuration()J

    move-result-wide v6

    .line 83
    .local v6, "duration":J
    move-object/from16 v0, p1

    iget-object v0, v0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->telecastDate:Landroid/widget/TextView;

    move-object/from16 v16, v0

    if-eqz v16, :cond_3

    .line 84
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 85
    .local v3, "calendar":Ljava/util/Calendar;
    const-wide/16 v16, 0x3e8

    mul-long v16, v16, v14

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 86
    const-string v16, "dd MMMM, HH:mm"

    move-object/from16 v0, v16

    invoke-static {v3, v0}, Lru/cn/utils/Utils;->format(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 88
    .local v9, "format":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v0, v0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->telecastDate:Landroid/widget/TextView;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    .end local v3    # "calendar":Ljava/util/Calendar;
    .end local v9    # "format":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lru/cn/tv/stb/collections/CollectionTelecastAdapter;->calculateElapsed(J)I

    move-result v8

    .line 92
    .local v8, "elapsedTime":I
    move-object/from16 v0, p1

    iget-object v0, v0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->airTelecastText:Landroid/widget/TextView;

    move-object/from16 v16, v0

    if-eqz v16, :cond_4

    .line 93
    int-to-long v0, v8

    move-wide/from16 v16, v0

    cmp-long v16, v16, v6

    if-gez v16, :cond_6

    .line 94
    move-object/from16 v0, p1

    iget-object v0, v0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->airTelecastText:Landroid/widget/TextView;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/TextView;->setVisibility(I)V

    .line 100
    :cond_4
    :goto_1
    move-object/from16 v0, p1

    iget-object v0, v0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->paidIndicator:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    invoke-virtual {v2}, Lru/cn/api/provider/cursor/TelecastItemCursor;->isPaid()Z

    move-result v16

    if-eqz v16, :cond_7

    const/16 v16, 0x0

    :goto_2
    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 101
    return-void

    .line 66
    .end local v6    # "duration":J
    .end local v8    # "elapsedTime":I
    .end local v13    # "viewsCount":I
    .end local v14    # "time":J
    :cond_5
    invoke-static {v5}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v16

    .line 67
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v16

    move-object/from16 v0, p1

    iget-object v0, v0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->telecastImage:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    .line 68
    invoke-virtual/range {v16 .. v17}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    goto/16 :goto_0

    .line 96
    .restart local v6    # "duration":J
    .restart local v8    # "elapsedTime":I
    .restart local v13    # "viewsCount":I
    .restart local v14    # "time":J
    :cond_6
    move-object/from16 v0, p1

    iget-object v0, v0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->airTelecastText:Landroid/widget/TextView;

    move-object/from16 v16, v0

    const/16 v17, 0x8

    invoke-virtual/range {v16 .. v17}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 100
    :cond_7
    const/16 v16, 0x8

    goto :goto_2
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0, p1, p2}, Lru/cn/tv/stb/collections/CollectionTelecastAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 38
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 39
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget v2, p0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter;->layout:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 40
    .local v1, "view":Landroid/view/View;
    new-instance v2, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;

    invoke-direct {v2, v1}, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;-><init>(Landroid/view/View;)V

    return-object v2
.end method
