.class public Lru/cn/api/userdata/UserData;
.super Ljava/lang/Object;
.source "UserData.java"


# direct methods
.method public static addApplication(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "deviceId"    # Ljava/lang/String;
    .param p2, "uuid"    # Ljava/lang/String;
    .param p3, "pushToken"    # Ljava/lang/String;
    .param p4, "version"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 97
    sget-object v1, Lru/cn/api/userdata/replies/ChangeOperation;->ADD:Lru/cn/api/userdata/replies/ChangeOperation;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lru/cn/api/userdata/UserData;->sendApplication(Landroid/content/Context;Lru/cn/api/userdata/replies/ChangeOperation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static addFavoriteChannel(Landroid/content/Context;Lru/cn/api/userdata/elementclasses/CatalogueItemClass;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "item"    # Lru/cn/api/userdata/elementclasses/CatalogueItemClass;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 53
    invoke-static {p0}, Lru/cn/api/ServiceLocator;->userData(Landroid/content/Context;)Lru/cn/api/userdata/retrofit/UserDataApi;

    move-result-object v0

    .line 54
    .local v0, "userDataApi":Lru/cn/api/userdata/retrofit/UserDataApi;
    invoke-static {p0}, Lru/cn/api/userdata/UserData;->favoritesPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lru/cn/api/userdata/replies/ChangeOperation;->ADD:Lru/cn/api/userdata/replies/ChangeOperation;

    iget-object v3, p1, Lru/cn/api/userdata/elementclasses/CatalogueItemClass;->catalogue_item_id:Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Lru/cn/api/userdata/elementclasses/CatalogueItemClass;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v0, v1, v2, v3, v4}, Lru/cn/api/userdata/UserData;->modifyElement(Lru/cn/api/userdata/retrofit/UserDataApi;Ljava/lang/String;Lru/cn/api/userdata/replies/ChangeOperation;Ljava/lang/String;[Lru/cn/api/userdata/elementclasses/BaseClass;)Z

    move-result v1

    return v1
.end method

.method public static addPinCode(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "pinCode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 122
    invoke-static {p0}, Lru/cn/api/ServiceLocator;->userData(Landroid/content/Context;)Lru/cn/api/userdata/retrofit/UserDataApi;

    move-result-object v1

    .line 125
    .local v1, "userDataApi":Lru/cn/api/userdata/retrofit/UserDataApi;
    new-instance v0, Lru/cn/api/userdata/elementclasses/CatalogueItemClass;

    invoke-direct {v0}, Lru/cn/api/userdata/elementclasses/CatalogueItemClass;-><init>()V

    .line 126
    .local v0, "item":Lru/cn/api/userdata/elementclasses/CatalogueItemClass;
    iput-object p1, v0, Lru/cn/api/userdata/elementclasses/CatalogueItemClass;->catalogue_item_id:Ljava/lang/String;

    .line 127
    const-string v2, "pincode"

    iput-object v2, v0, Lru/cn/api/userdata/elementclasses/CatalogueItemClass;->catalogue_id:Ljava/lang/String;

    .line 129
    const-string v2, "parental_control/pincode"

    sget-object v3, Lru/cn/api/userdata/replies/ChangeOperation;->ADD:Lru/cn/api/userdata/replies/ChangeOperation;

    const/4 v4, 0x1

    new-array v4, v4, [Lru/cn/api/userdata/elementclasses/CatalogueItemClass;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v1, v2, v3, p1, v4}, Lru/cn/api/userdata/UserData;->modifyElement(Lru/cn/api/userdata/retrofit/UserDataApi;Ljava/lang/String;Lru/cn/api/userdata/replies/ChangeOperation;Ljava/lang/String;[Lru/cn/api/userdata/elementclasses/BaseClass;)Z

    move-result v2

    return v2
.end method

.method public static addUserPlaylist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 143
    sget-object v0, Lru/cn/api/userdata/replies/ChangeOperation;->ADD:Lru/cn/api/userdata/replies/ChangeOperation;

    invoke-static {p0, v0, p1, p2}, Lru/cn/api/userdata/UserData;->sendUserPlaylistData(Landroid/content/Context;Lru/cn/api/userdata/replies/ChangeOperation;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static clearPin(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 138
    invoke-static {p0}, Lru/cn/api/ServiceLocator;->userData(Landroid/content/Context;)Lru/cn/api/userdata/retrofit/UserDataApi;

    move-result-object v0

    .line 139
    .local v0, "userDataApi":Lru/cn/api/userdata/retrofit/UserDataApi;
    const-string v1, "parental_control/pincode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lru/cn/api/userdata/UserData;->deleteElement(Lru/cn/api/userdata/retrofit/UserDataApi;Ljava/lang/String;Ljava/lang/String;)Z

    .line 140
    return-void
.end method

.method public static delFavoriteChannel(Landroid/content/Context;Lru/cn/api/userdata/elementclasses/CatalogueItemClass;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "item"    # Lru/cn/api/userdata/elementclasses/CatalogueItemClass;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 59
    invoke-static {p0}, Lru/cn/api/ServiceLocator;->userData(Landroid/content/Context;)Lru/cn/api/userdata/retrofit/UserDataApi;

    move-result-object v0

    .line 60
    .local v0, "userDataApi":Lru/cn/api/userdata/retrofit/UserDataApi;
    invoke-static {p0}, Lru/cn/api/userdata/UserData;->favoritesPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lru/cn/api/userdata/elementclasses/CatalogueItemClass;->catalogue_item_id:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lru/cn/api/userdata/UserData;->deleteElement(Lru/cn/api/userdata/retrofit/UserDataApi;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public static delUserPlaylist(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 170
    invoke-static {p0}, Lru/cn/api/ServiceLocator;->userData(Landroid/content/Context;)Lru/cn/api/userdata/retrofit/UserDataApi;

    move-result-object v0

    .line 171
    .local v0, "userDataApi":Lru/cn/api/userdata/retrofit/UserDataApi;
    const-string v1, "bookmarks/playlists"

    invoke-static {v0, v1, p1}, Lru/cn/api/userdata/UserData;->deleteElement(Lru/cn/api/userdata/retrofit/UserDataApi;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method private static deleteElement(Lru/cn/api/userdata/retrofit/UserDataApi;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p0, "userDataApi"    # Lru/cn/api/userdata/retrofit/UserDataApi;
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 202
    if-nez p0, :cond_1

    .line 218
    :cond_0
    :goto_0
    return v3

    .line 207
    :cond_1
    :try_start_0
    const-string v4, "delete"

    const-string v5, "json"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "id="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p0, p1, v4, v5, v6}, Lru/cn/api/userdata/retrofit/UserDataApi;->delete(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v4

    .line 208
    invoke-virtual {v4}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/api/userdata/replies/ActionResults;

    .line 210
    .local v2, "results":Lru/cn/api/userdata/replies/ActionResults;
    iget-object v4, v2, Lru/cn/api/userdata/replies/ActionResults;->results:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/userdata/replies/ActionResult;

    .line 211
    .local v1, "result":Lru/cn/api/userdata/replies/ActionResult;
    iget v4, v1, Lru/cn/api/userdata/replies/ActionResult;->code:I

    const/16 v5, 0xc8

    if-eq v4, v5, :cond_2

    iget v4, v1, Lru/cn/api/userdata/replies/ActionResult;->code:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v5, 0xc9

    if-ne v4, v5, :cond_0

    .line 212
    :cond_2
    const/4 v3, 0x1

    goto :goto_0

    .line 214
    .end local v1    # "result":Lru/cn/api/userdata/replies/ActionResult;
    .end local v2    # "results":Lru/cn/api/userdata/replies/ActionResults;
    :catch_0
    move-exception v0

    .line 215
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "UserDataAPI"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static elements(Lru/cn/api/userdata/retrofit/UserDataApi;Ljava/lang/String;)Ljava/util/List;
    .locals 13
    .param p0, "userDataApi"    # Lru/cn/api/userdata/retrofit/UserDataApi;
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/api/userdata/retrofit/UserDataApi;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/userdata/replies/Element;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    .line 232
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 233
    .local v10, "ret":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/userdata/replies/Element;>;"
    if-nez p0, :cond_0

    .line 253
    .end local v10    # "ret":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/userdata/replies/Element;>;"
    :goto_0
    return-object v10

    .line 236
    .restart local v10    # "ret":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/userdata/replies/Element;>;"
    :cond_0
    const/4 v5, 0x0

    .line 237
    .local v5, "skip":I
    const/4 v11, 0x0

    .line 240
    .local v11, "totalCount":I
    :cond_1
    :try_start_0
    const-string v2, "list"

    const-string v3, "json"

    const/16 v4, 0x14

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-interface/range {v0 .. v7}, Lru/cn/api/userdata/retrofit/UserDataApi;->elements(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 241
    invoke-virtual {v0}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lru/cn/api/userdata/replies/ElementsReply;

    .line 243
    .local v9, "reply":Lru/cn/api/userdata/replies/ElementsReply;
    iget-object v0, v9, Lru/cn/api/userdata/replies/ElementsReply;->elements:Ljava/util/List;

    invoke-interface {v10, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 244
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v5

    .line 245
    iget v11, v9, Lru/cn/api/userdata/replies/ElementsReply;->total_count:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 251
    if-lt v5, v11, :cond_1

    goto :goto_0

    .line 246
    .end local v9    # "reply":Lru/cn/api/userdata/replies/ElementsReply;
    :catch_0
    move-exception v8

    .line 247
    .local v8, "e":Ljava/lang/Exception;
    invoke-static {v8}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    move-object v10, v12

    .line 248
    goto :goto_0
.end method

.method private static elements(Lru/cn/api/userdata/retrofit/UserDataApi;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/List;
    .locals 3
    .param p0, "userDataApi"    # Lru/cn/api/userdata/retrofit/UserDataApi;
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lru/cn/api/userdata/retrofit/UserDataApi;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 222
    .local p2, "type":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-static {p0, p1}, Lru/cn/api/userdata/UserData;->elements(Lru/cn/api/userdata/retrofit/UserDataApi;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 223
    .local v0, "elements":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/userdata/replies/Element;>;"
    if-nez v0, :cond_0

    .line 224
    const/4 v1, 0x0

    .line 226
    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v1

    new-instance v2, Lru/cn/api/userdata/UserData$$Lambda$0;

    invoke-direct {v2, p2}, Lru/cn/api/userdata/UserData$$Lambda$0;-><init>(Ljava/lang/Class;)V

    .line 227
    invoke-virtual {v1, v2}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v1

    .line 228
    invoke-virtual {v1}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method private static favoritesPath(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    invoke-static {p0}, Lru/cn/domain/KidsObject;->isKidsMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    const-string v0, "child_favourites/tv/channels"

    .line 49
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "favourites/tv/channels"

    goto :goto_0
.end method

.method public static getFavoriteChannels(Landroid/content/Context;)Ljava/util/List;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/userdata/elementclasses/CatalogueItemClass;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 64
    invoke-static {p0}, Lru/cn/api/ServiceLocator;->userData(Landroid/content/Context;)Lru/cn/api/userdata/retrofit/UserDataApi;

    move-result-object v0

    .line 65
    .local v0, "userDataApi":Lru/cn/api/userdata/retrofit/UserDataApi;
    invoke-static {p0}, Lru/cn/api/userdata/UserData;->favoritesPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lru/cn/api/userdata/elementclasses/CatalogueItemClass;

    invoke-static {v0, v1, v2}, Lru/cn/api/userdata/UserData;->elements(Lru/cn/api/userdata/retrofit/UserDataApi;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public static getPinCode(Landroid/content/Context;)Ljava/util/List;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/userdata/elementclasses/CatalogueItemClass;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 133
    invoke-static {p0}, Lru/cn/api/ServiceLocator;->userData(Landroid/content/Context;)Lru/cn/api/userdata/retrofit/UserDataApi;

    move-result-object v0

    .line 134
    .local v0, "userDataApi":Lru/cn/api/userdata/retrofit/UserDataApi;
    const-string v1, "parental_control/pincode"

    const-class v2, Lru/cn/api/userdata/elementclasses/CatalogueItemClass;

    invoke-static {v0, v1, v2}, Lru/cn/api/userdata/UserData;->elements(Lru/cn/api/userdata/retrofit/UserDataApi;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public static getUserPlaylists(Landroid/content/Context;)Ljava/util/List;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/userdata/elementclasses/BookmarkClass;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 165
    invoke-static {p0}, Lru/cn/api/ServiceLocator;->userData(Landroid/content/Context;)Lru/cn/api/userdata/retrofit/UserDataApi;

    move-result-object v0

    .line 166
    .local v0, "userDataApi":Lru/cn/api/userdata/retrofit/UserDataApi;
    const-string v1, "bookmarks/playlists"

    const-class v2, Lru/cn/api/userdata/elementclasses/BookmarkClass;

    invoke-static {v0, v1, v2}, Lru/cn/api/userdata/UserData;->elements(Lru/cn/api/userdata/retrofit/UserDataApi;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method static final synthetic lambda$elements$0$UserData(Ljava/lang/Class;Lru/cn/api/userdata/replies/Element;)Ljava/lang/Object;
    .locals 1
    .param p0, "type"    # Ljava/lang/Class;
    .param p1, "element"    # Lru/cn/api/userdata/replies/Element;

    .prologue
    .line 227
    invoke-virtual {p1, p0}, Lru/cn/api/userdata/replies/Element;->getElement(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static modifyApplication(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "deviceId"    # Ljava/lang/String;
    .param p2, "uuid"    # Ljava/lang/String;
    .param p3, "pushToken"    # Ljava/lang/String;
    .param p4, "version"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 103
    sget-object v1, Lru/cn/api/userdata/replies/ChangeOperation;->REPLACE:Lru/cn/api/userdata/replies/ChangeOperation;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lru/cn/api/userdata/UserData;->sendApplication(Landroid/content/Context;Lru/cn/api/userdata/replies/ChangeOperation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static varargs modifyElement(Lru/cn/api/userdata/retrofit/UserDataApi;Ljava/lang/String;Lru/cn/api/userdata/replies/ChangeOperation;Ljava/lang/String;[Lru/cn/api/userdata/elementclasses/BaseClass;)Z
    .locals 9
    .param p0, "userDataApi"    # Lru/cn/api/userdata/retrofit/UserDataApi;
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "operation"    # Lru/cn/api/userdata/replies/ChangeOperation;
    .param p3, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lru/cn/api/userdata/elementclasses/BaseClass;",
            ">(",
            "Lru/cn/api/userdata/retrofit/UserDataApi;",
            "Ljava/lang/String;",
            "Lru/cn/api/userdata/replies/ChangeOperation;",
            "Ljava/lang/String;",
            "[TT;)Z"
        }
    .end annotation

    .prologue
    .local p4, "elements":[Lru/cn/api/userdata/elementclasses/BaseClass;, "[TT;"
    const/4 v6, 0x0

    .line 179
    new-instance v2, Lru/cn/api/userdata/replies/ModifyRequest;

    invoke-direct {v2, p3}, Lru/cn/api/userdata/replies/ModifyRequest;-><init>(Ljava/lang/String;)V

    .line 180
    .local v2, "m":Lru/cn/api/userdata/replies/ModifyRequest;
    new-instance v0, Lru/cn/api/userdata/replies/ModifyRequestChange;

    invoke-direct {v0, p2}, Lru/cn/api/userdata/replies/ModifyRequestChange;-><init>(Lru/cn/api/userdata/replies/ChangeOperation;)V

    .line 181
    .local v0, "change":Lru/cn/api/userdata/replies/ModifyRequestChange;
    new-instance v7, Lru/cn/api/userdata/replies/Attributes;

    invoke-direct {v7, p4}, Lru/cn/api/userdata/replies/Attributes;-><init>([Lru/cn/api/userdata/elementclasses/BaseClass;)V

    iput-object v7, v0, Lru/cn/api/userdata/replies/ModifyRequestChange;->attributes:Lru/cn/api/userdata/replies/Attributes;

    .line 182
    iget-object v7, v2, Lru/cn/api/userdata/replies/ModifyRequest;->changes:Ljava/util/List;

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    new-instance v3, Lru/cn/api/userdata/replies/ModifyRequests;

    invoke-direct {v3}, Lru/cn/api/userdata/replies/ModifyRequests;-><init>()V

    .line 185
    .local v3, "requests":Lru/cn/api/userdata/replies/ModifyRequests;
    iget-object v7, v3, Lru/cn/api/userdata/replies/ModifyRequests;->modifyRequests:Ljava/util/List;

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    :try_start_0
    const-string v7, "modify"

    const-string v8, "json"

    invoke-interface {p0, p1, v7, v8, v3}, Lru/cn/api/userdata/retrofit/UserDataApi;->modify(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lru/cn/api/userdata/replies/ModifyRequests;)Lio/reactivex/Single;

    move-result-object v7

    .line 189
    invoke-virtual {v7}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lru/cn/api/userdata/replies/ActionResults;

    .line 191
    .local v5, "results":Lru/cn/api/userdata/replies/ActionResults;
    iget-object v7, v5, Lru/cn/api/userdata/replies/ActionResults;->results:Ljava/util/List;

    const/4 v8, 0x0

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/api/userdata/replies/ActionResult;

    .line 192
    .local v4, "result":Lru/cn/api/userdata/replies/ActionResult;
    iget v7, v4, Lru/cn/api/userdata/replies/ActionResult;->code:I

    const/16 v8, 0xc8

    if-eq v7, v8, :cond_0

    iget v7, v4, Lru/cn/api/userdata/replies/ActionResult;->code:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v8, 0xc9

    if-ne v7, v8, :cond_1

    .line 193
    :cond_0
    const/4 v6, 0x1

    .line 198
    .end local v4    # "result":Lru/cn/api/userdata/replies/ActionResult;
    .end local v5    # "results":Lru/cn/api/userdata/replies/ActionResults;
    :cond_1
    :goto_0
    return v6

    .line 195
    :catch_0
    move-exception v1

    .line 196
    .local v1, "e":Ljava/lang/Exception;
    const-string v7, "UserDataAPI"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static sendApplication(Landroid/content/Context;Lru/cn/api/userdata/replies/ChangeOperation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "operation"    # Lru/cn/api/userdata/replies/ChangeOperation;
    .param p2, "deviceId"    # Ljava/lang/String;
    .param p3, "uuid"    # Ljava/lang/String;
    .param p4, "pushToken"    # Ljava/lang/String;
    .param p5, "version"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 110
    invoke-static {p0}, Lru/cn/api/ServiceLocator;->userData(Landroid/content/Context;)Lru/cn/api/userdata/retrofit/UserDataApi;

    move-result-object v1

    .line 112
    .local v1, "userDataApi":Lru/cn/api/userdata/retrofit/UserDataApi;
    new-instance v0, Lru/cn/api/userdata/elementclasses/ApplicationClass;

    invoke-direct {v0}, Lru/cn/api/userdata/elementclasses/ApplicationClass;-><init>()V

    .line 113
    .local v0, "item":Lru/cn/api/userdata/elementclasses/ApplicationClass;
    const-string v2, "Peers.TV"

    iput-object v2, v0, Lru/cn/api/userdata/elementclasses/ApplicationClass;->title:Ljava/lang/String;

    .line 114
    iput-object p2, v0, Lru/cn/api/userdata/elementclasses/ApplicationClass;->device_id:Ljava/lang/String;

    .line 115
    iput-object p4, v0, Lru/cn/api/userdata/elementclasses/ApplicationClass;->push_token:Ljava/lang/String;

    .line 116
    iput-object p5, v0, Lru/cn/api/userdata/elementclasses/ApplicationClass;->version:Ljava/lang/String;

    .line 118
    const-string v2, "applications"

    const/4 v3, 0x1

    new-array v3, v3, [Lru/cn/api/userdata/elementclasses/ApplicationClass;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, p1, p3, v3}, Lru/cn/api/userdata/UserData;->modifyElement(Lru/cn/api/userdata/retrofit/UserDataApi;Ljava/lang/String;Lru/cn/api/userdata/replies/ChangeOperation;Ljava/lang/String;[Lru/cn/api/userdata/elementclasses/BaseClass;)Z

    move-result v2

    return v2
.end method

.method public static sendPurchase(Landroid/content/Context;Ljava/lang/String;Lru/cn/api/userdata/elementclasses/GooglePlayReceipt;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "storeId"    # Ljava/lang/String;
    .param p2, "info"    # Lru/cn/api/userdata/elementclasses/GooglePlayReceipt;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 85
    invoke-static {p0}, Lru/cn/api/ServiceLocator;->userData(Landroid/content/Context;)Lru/cn/api/userdata/retrofit/UserDataApi;

    move-result-object v1

    .line 87
    .local v1, "userDataApi":Lru/cn/api/userdata/retrofit/UserDataApi;
    new-instance v0, Lru/cn/api/userdata/elementclasses/PurchaseClass;

    invoke-direct {v0}, Lru/cn/api/userdata/elementclasses/PurchaseClass;-><init>()V

    .line 88
    .local v0, "item":Lru/cn/api/userdata/elementclasses/PurchaseClass;
    iput-object p1, v0, Lru/cn/api/userdata/elementclasses/PurchaseClass;->store_id:Ljava/lang/String;

    .line 89
    iput-object p2, v0, Lru/cn/api/userdata/elementclasses/PurchaseClass;->purchase_info:Lru/cn/api/userdata/elementclasses/GooglePlayReceipt;

    .line 91
    const-string v2, "purchases"

    sget-object v3, Lru/cn/api/userdata/replies/ChangeOperation;->ADD:Lru/cn/api/userdata/replies/ChangeOperation;

    const/4 v4, 0x0

    const/4 v5, 0x1

    new-array v5, v5, [Lru/cn/api/userdata/elementclasses/PurchaseClass;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v1, v2, v3, v4, v5}, Lru/cn/api/userdata/UserData;->modifyElement(Lru/cn/api/userdata/retrofit/UserDataApi;Ljava/lang/String;Lru/cn/api/userdata/replies/ChangeOperation;Ljava/lang/String;[Lru/cn/api/userdata/elementclasses/BaseClass;)Z

    move-result v2

    return v2
.end method

.method private static sendUserPlaylistData(Landroid/content/Context;Lru/cn/api/userdata/replies/ChangeOperation;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "operation"    # Lru/cn/api/userdata/replies/ChangeOperation;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 153
    invoke-static {p0}, Lru/cn/api/ServiceLocator;->userData(Landroid/content/Context;)Lru/cn/api/userdata/retrofit/UserDataApi;

    move-result-object v1

    .line 155
    .local v1, "userDataApi":Lru/cn/api/userdata/retrofit/UserDataApi;
    new-instance v0, Lru/cn/api/userdata/elementclasses/BookmarkClass;

    invoke-direct {v0}, Lru/cn/api/userdata/elementclasses/BookmarkClass;-><init>()V

    .line 156
    .local v0, "item":Lru/cn/api/userdata/elementclasses/BookmarkClass;
    iput-object p2, v0, Lru/cn/api/userdata/elementclasses/BookmarkClass;->bookmark_title:Ljava/lang/String;

    .line 157
    iput-object p3, v0, Lru/cn/api/userdata/elementclasses/BookmarkClass;->bookmark_url:Ljava/lang/String;

    .line 159
    const-string v2, "bookmarks/playlists"

    const/4 v3, 0x1

    new-array v3, v3, [Lru/cn/api/userdata/elementclasses/BookmarkClass;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, p1, p3, v3}, Lru/cn/api/userdata/UserData;->modifyElement(Lru/cn/api/userdata/retrofit/UserDataApi;Ljava/lang/String;Lru/cn/api/userdata/replies/ChangeOperation;Ljava/lang/String;[Lru/cn/api/userdata/elementclasses/BaseClass;)Z

    move-result v2

    return v2
.end method

.method public static updateUserPlaylist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 147
    sget-object v0, Lru/cn/api/userdata/replies/ChangeOperation;->REPLACE:Lru/cn/api/userdata/replies/ChangeOperation;

    invoke-static {p0, v0, p1, p2}, Lru/cn/api/userdata/UserData;->sendUserPlaylistData(Landroid/content/Context;Lru/cn/api/userdata/replies/ChangeOperation;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
