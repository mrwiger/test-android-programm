.class Lru/cn/tv/player/PlaybackErrors;
.super Ljava/lang/Object;
.source "PlaybackErrors.java"


# direct methods
.method static errorMessage(Landroid/content/Context;II)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "code"    # I
    .param p2, "playerType"    # I

    .prologue
    .line 14
    if-nez p2, :cond_1

    .line 15
    packed-switch p1, :pswitch_data_0

    .line 80
    :cond_0
    :goto_0
    const v0, 0x7f0e00a5

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    .line 17
    :pswitch_0
    const v0, 0x7f0e00a1

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 23
    :cond_1
    const/4 v0, 0x5

    if-ne p2, v0, :cond_2

    .line 24
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    .line 26
    :pswitch_1
    const v0, 0x7f0e0098

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 29
    :pswitch_2
    const v0, 0x7f0e0099

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 32
    :pswitch_3
    const v0, 0x7f0e009f

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 35
    :pswitch_4
    const v0, 0x7f0e00a6

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 38
    :pswitch_5
    const v0, 0x7f0e00a9

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 41
    :pswitch_6
    const v0, 0x7f0e00a0

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 44
    :pswitch_7
    const v0, 0x7f0e009d

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 47
    :pswitch_8
    const v0, 0x7f0e0094

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 50
    :pswitch_9
    const v0, 0x7f0e009c

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 53
    :pswitch_a
    const v0, 0x7f0e0096

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 56
    :pswitch_b
    const v0, 0x7f0e00a8

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 59
    :pswitch_c
    const v0, 0x7f0e00a2

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 62
    :pswitch_d
    const v0, 0x7f0e009e

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 65
    :pswitch_e
    const v0, 0x7f0e00a7

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 68
    :pswitch_f
    const v0, 0x7f0e0095

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 73
    :cond_2
    const/16 v0, 0x69

    if-ne p2, v0, :cond_0

    .line 74
    packed-switch p1, :pswitch_data_2

    goto/16 :goto_0

    .line 76
    :pswitch_10
    const v0, 0x7f0e0097

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 15
    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
    .end packed-switch

    .line 24
    :pswitch_data_1
    .packed-switch 0x2ee
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_7
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch

    .line 74
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_10
    .end packed-switch
.end method
