.class public Lcom/google/obf/gm;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;
.implements Lcom/google/obf/hn$b;


# instance fields
.field private a:Lcom/google/obf/hj;

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:Z

.field private e:Lcom/google/obf/go;


# direct methods
.method public constructor <init>(Lcom/google/obf/hj;Ljava/lang/String;Lcom/google/obf/go;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-boolean v0, p0, Lcom/google/obf/gm;->c:Z

    .line 3
    iput-boolean v0, p0, Lcom/google/obf/gm;->d:Z

    .line 4
    iput-object p1, p0, Lcom/google/obf/gm;->a:Lcom/google/obf/hj;

    .line 5
    iput-object p2, p0, Lcom/google/obf/gm;->b:Ljava/lang/String;

    .line 6
    iput-object p3, p0, Lcom/google/obf/gm;->e:Lcom/google/obf/go;

    .line 7
    return-void
.end method


# virtual methods
.method public a(Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 27
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;->getDuration()F

    move-result v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 28
    iget-boolean v0, p0, Lcom/google/obf/gm;->d:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;->getCurrentTime()F

    move-result v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 29
    sget-object v0, Lcom/google/obf/hi$c;->start:Lcom/google/obf/hi$c;

    invoke-virtual {p0, v0}, Lcom/google/obf/gm;->a(Lcom/google/obf/hi$c;)V

    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/gm;->d:Z

    .line 31
    :cond_0
    sget-object v0, Lcom/google/obf/hi$c;->timeupdate:Lcom/google/obf/hi$c;

    invoke-virtual {p0, v0, p1}, Lcom/google/obf/gm;->a(Lcom/google/obf/hi$c;Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;)V

    .line 32
    :cond_1
    return-void
.end method

.method a(Lcom/google/obf/hi$c;)V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/obf/gm;->a(Lcom/google/obf/hi$c;Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;)V

    .line 34
    return-void
.end method

.method a(Lcom/google/obf/hi$c;Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;)V
    .locals 3

    .prologue
    .line 35
    new-instance v0, Lcom/google/obf/hi;

    sget-object v1, Lcom/google/obf/hi$b;->videoDisplay:Lcom/google/obf/hi$b;

    iget-object v2, p0, Lcom/google/obf/gm;->b:Ljava/lang/String;

    invoke-direct {v0, v1, p1, v2, p2}, Lcom/google/obf/hi;-><init>(Lcom/google/obf/hi$b;Lcom/google/obf/hi$c;Ljava/lang/String;Ljava/lang/Object;)V

    .line 36
    iget-object v1, p0, Lcom/google/obf/gm;->a:Lcom/google/obf/hj;

    invoke-virtual {v1, v0}, Lcom/google/obf/hj;->b(Lcom/google/obf/hi;)V

    .line 37
    return-void
.end method

.method public onEnded()V
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/google/obf/hi$c;->end:Lcom/google/obf/hi$c;

    invoke-virtual {p0, v0}, Lcom/google/obf/gm;->a(Lcom/google/obf/hi$c;)V

    .line 17
    return-void
.end method

.method public onError()V
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/google/obf/hi$c;->error:Lcom/google/obf/hi$c;

    invoke-virtual {p0, v0}, Lcom/google/obf/gm;->a(Lcom/google/obf/hi$c;)V

    .line 19
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lcom/google/obf/gm;->e:Lcom/google/obf/go;

    invoke-virtual {v0}, Lcom/google/obf/go;->c()V

    .line 11
    sget-object v0, Lcom/google/obf/hi$c;->pause:Lcom/google/obf/hi$c;

    invoke-virtual {p0, v0}, Lcom/google/obf/gm;->a(Lcom/google/obf/hi$c;)V

    .line 12
    return-void
.end method

.method public onPlay()V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/gm;->d:Z

    .line 9
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/google/obf/gm;->e:Lcom/google/obf/go;

    invoke-virtual {v0}, Lcom/google/obf/go;->b()V

    .line 14
    sget-object v0, Lcom/google/obf/hi$c;->play:Lcom/google/obf/hi$c;

    invoke-virtual {p0, v0}, Lcom/google/obf/gm;->a(Lcom/google/obf/hi$c;)V

    .line 15
    return-void
.end method
