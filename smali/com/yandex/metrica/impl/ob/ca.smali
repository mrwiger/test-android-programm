.class public Lcom/yandex/metrica/impl/ob/ca;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/yandex/metrica/impl/ob/bw;

.field private c:Lcom/yandex/metrica/impl/ob/bx;

.field private d:Lcom/yandex/metrica/impl/ob/bz;

.field private e:Lcom/yandex/metrica/impl/ob/ci;

.field private f:Lcom/yandex/metrica/impl/ob/cj;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/bw;Lcom/yandex/metrica/impl/ob/bx;Lcom/yandex/metrica/impl/ob/bz;Lcom/yandex/metrica/impl/ob/ci;Lcom/yandex/metrica/impl/ob/cj;)V
    .locals 0

    .prologue
    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/ca;->a:Landroid/content/Context;

    .line 162
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/ca;->b:Lcom/yandex/metrica/impl/ob/bw;

    .line 163
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/ca;->c:Lcom/yandex/metrica/impl/ob/bx;

    .line 164
    iput-object p4, p0, Lcom/yandex/metrica/impl/ob/ca;->d:Lcom/yandex/metrica/impl/ob/bz;

    .line 165
    iput-object p5, p0, Lcom/yandex/metrica/impl/ob/ca;->e:Lcom/yandex/metrica/impl/ob/ci;

    .line 166
    iput-object p6, p0, Lcom/yandex/metrica/impl/ob/ca;->f:Lcom/yandex/metrica/impl/ob/cj;

    .line 167
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;Lcom/yandex/metrica/impl/ob/cb;)V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lcom/yandex/metrica/impl/ob/bz;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/bz;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/yandex/metrica/impl/ob/ca;-><init>(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;Lcom/yandex/metrica/impl/ob/cb;Lcom/yandex/metrica/impl/ob/bz;)V

    .line 49
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;Lcom/yandex/metrica/impl/ob/cb;Lcom/yandex/metrica/impl/ob/bz;)V
    .locals 7

    .prologue
    .line 143
    new-instance v2, Lcom/yandex/metrica/impl/ob/bw;

    invoke-direct {v2, p1, p4}, Lcom/yandex/metrica/impl/ob/bw;-><init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/bz;)V

    new-instance v3, Lcom/yandex/metrica/impl/ob/bx;

    invoke-direct {v3, p1, p2, p3}, Lcom/yandex/metrica/impl/ob/bx;-><init>(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;Lcom/yandex/metrica/impl/ob/cb;)V

    .line 148
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/ci;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/ci;

    move-result-object v5

    new-instance v6, Lcom/yandex/metrica/impl/ob/cj;

    invoke-direct {v6, p1}, Lcom/yandex/metrica/impl/ob/cj;-><init>(Landroid/content/Context;)V

    move-object v0, p0

    move-object v1, p1

    move-object v4, p4

    .line 143
    invoke-direct/range {v0 .. v6}, Lcom/yandex/metrica/impl/ob/ca;-><init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/bw;Lcom/yandex/metrica/impl/ob/bx;Lcom/yandex/metrica/impl/ob/bz;Lcom/yandex/metrica/impl/ob/ci;Lcom/yandex/metrica/impl/ob/cj;)V

    .line 151
    return-void
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/ob/ca;)Lcom/yandex/metrica/impl/ob/bz;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ca;->d:Lcom/yandex/metrica/impl/ob/bz;

    return-object v0
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 98
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ca;->c:Lcom/yandex/metrica/impl/ob/bx;

    invoke-virtual {v0, p1, p2}, Lcom/yandex/metrica/impl/ob/bx;->a(J)V

    .line 99
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ca;->f:Lcom/yandex/metrica/impl/ob/cj;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/cj;->a(Z)Lcom/yandex/metrica/impl/ob/cj;

    .line 100
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ca;->f:Lcom/yandex/metrica/impl/ob/cj;

    invoke-virtual {v0, p1, p2}, Lcom/yandex/metrica/impl/ob/cj;->c(J)Lcom/yandex/metrica/impl/ob/cj;

    .line 101
    return-void
.end method

.method static synthetic b(Lcom/yandex/metrica/impl/ob/cc;)V
    .locals 0

    .prologue
    .line 27
    invoke-static {p0}, Lcom/yandex/metrica/impl/ob/ca;->c(Lcom/yandex/metrica/impl/ob/cc;)V

    return-void
.end method

.method private static c(Lcom/yandex/metrica/impl/ob/cc;)V
    .locals 0

    .prologue
    .line 135
    if-eqz p0, :cond_0

    .line 136
    invoke-interface {p0}, Lcom/yandex/metrica/impl/ob/cc;->a()V

    .line 138
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 53
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ca;->e:Lcom/yandex/metrica/impl/ob/ci;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ca;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/ci;->a(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/ch;

    move-result-object v2

    .line 54
    if-eqz v2, :cond_1

    iget-object v0, v2, Lcom/yandex/metrica/impl/ob/ch;->b:Lcom/yandex/metrica/impl/ob/cg;

    if-eqz v0, :cond_1

    iget-object v0, v2, Lcom/yandex/metrica/impl/ob/ch;->b:Lcom/yandex/metrica/impl/ob/cg;

    iget-object v0, v0, Lcom/yandex/metrica/impl/ob/cg;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    iget-object v0, v2, Lcom/yandex/metrica/impl/ob/ch;->b:Lcom/yandex/metrica/impl/ob/cg;

    iget-object v0, v0, Lcom/yandex/metrica/impl/ob/cg;->a:Ljava/lang/Long;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 58
    :goto_0
    if-eqz v0, :cond_0

    .line 59
    iget-object v0, v2, Lcom/yandex/metrica/impl/ob/ch;->b:Lcom/yandex/metrica/impl/ob/cg;

    iget-boolean v0, v0, Lcom/yandex/metrica/impl/ob/cg;->i:Z

    .line 60
    iget-object v2, v2, Lcom/yandex/metrica/impl/ob/ch;->b:Lcom/yandex/metrica/impl/ob/cg;

    iget-object v2, v2, Lcom/yandex/metrica/impl/ob/cg;->b:Ljava/lang/Long;

    .line 61
    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/ca;->f:Lcom/yandex/metrica/impl/ob/cj;

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/yandex/metrica/impl/ob/cj;->d(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 62
    iget-object v4, p0, Lcom/yandex/metrica/impl/ob/ca;->f:Lcom/yandex/metrica/impl/ob/cj;

    invoke-virtual {v4, v1}, Lcom/yandex/metrica/impl/ob/cj;->b(Z)Z

    move-result v4

    .line 63
    iget-object v5, p0, Lcom/yandex/metrica/impl/ob/ca;->c:Lcom/yandex/metrica/impl/ob/bx;

    invoke-virtual {v5}, Lcom/yandex/metrica/impl/ob/bx;->b()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 64
    if-eqz v0, :cond_0

    .line 66
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/ca;->a(J)V

    .line 95
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 54
    goto :goto_0

    .line 71
    :cond_2
    if-eq v0, v4, :cond_4

    .line 73
    if-eqz v0, :cond_3

    .line 75
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/ca;->a(J)V

    goto :goto_1

    .line 1104
    :cond_3
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ca;->c:Lcom/yandex/metrica/impl/ob/bx;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/bx;->a()V

    .line 1105
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ca;->f:Lcom/yandex/metrica/impl/ob/cj;

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/cj;->a(Z)Lcom/yandex/metrica/impl/ob/cj;

    goto :goto_1

    .line 82
    :cond_4
    if-eqz v0, :cond_0

    .line 85
    invoke-virtual {v2, v3}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/ca;->a(J)V

    goto :goto_1
.end method

.method public a(Lcom/yandex/metrica/impl/ob/cc;)V
    .locals 6

    .prologue
    .line 109
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ca;->e:Lcom/yandex/metrica/impl/ob/ci;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ca;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/ci;->a(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/ch;

    move-result-object v0

    .line 112
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/yandex/metrica/impl/ob/ch;->b:Lcom/yandex/metrica/impl/ob/cg;

    if-eqz v1, :cond_1

    .line 113
    iget-object v0, v0, Lcom/yandex/metrica/impl/ob/ch;->b:Lcom/yandex/metrica/impl/ob/cg;

    iget-object v0, v0, Lcom/yandex/metrica/impl/ob/cg;->a:Ljava/lang/Long;

    .line 114
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 116
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ca;->d:Lcom/yandex/metrica/impl/ob/bz;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ca;->b:Lcom/yandex/metrica/impl/ob/bw;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/bw;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/ob/bz;->a(Ljava/lang/String;)V

    .line 117
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ca;->c:Lcom/yandex/metrica/impl/ob/bx;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    new-instance v0, Lcom/yandex/metrica/impl/ob/ca$1;

    invoke-direct {v0, p0, p1}, Lcom/yandex/metrica/impl/ob/ca$1;-><init>(Lcom/yandex/metrica/impl/ob/ca;Lcom/yandex/metrica/impl/ob/cc;)V

    invoke-virtual {v1, v2, v3, v0}, Lcom/yandex/metrica/impl/ob/bx;->a(JLcom/yandex/metrica/impl/ob/bx$a;)V

    .line 132
    :goto_0
    return-void

    .line 126
    :cond_0
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/ca;->c(Lcom/yandex/metrica/impl/ob/cc;)V

    goto :goto_0

    .line 130
    :cond_1
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/ca;->c(Lcom/yandex/metrica/impl/ob/cc;)V

    goto :goto_0
.end method
