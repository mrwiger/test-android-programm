.class public abstract Lcom/google/obf/jl;
.super Lcom/google/obf/jj;
.source "IMASDK"

# interfaces
.implements Ljava/util/List;
.implements Ljava/util/RandomAccess;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/jl$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/obf/jj",
        "<TE;>;",
        "Ljava/util/List",
        "<TE;>;",
        "Ljava/util/RandomAccess;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/obf/jj;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/Collection;)Lcom/google/obf/jl;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<+TE;>;)",
            "Lcom/google/obf/jl",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 2
    instance-of v0, p0, Lcom/google/obf/jj;

    if-eqz v0, :cond_1

    .line 3
    check-cast p0, Lcom/google/obf/jj;

    invoke-virtual {p0}, Lcom/google/obf/jj;->b()Lcom/google/obf/jl;

    move-result-object v0

    .line 4
    invoke-virtual {v0}, Lcom/google/obf/jl;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/obf/jl;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/obf/jl;->a([Ljava/lang/Object;)Lcom/google/obf/jl;

    move-result-object v0

    .line 5
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/obf/jl;->b([Ljava/lang/Object;)Lcom/google/obf/jl;

    move-result-object v0

    goto :goto_0
.end method

.method static a([Ljava/lang/Object;)Lcom/google/obf/jl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">([",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/obf/jl",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 7
    array-length v0, p0

    invoke-static {p0, v0}, Lcom/google/obf/jl;->b([Ljava/lang/Object;I)Lcom/google/obf/jl;

    move-result-object v0

    return-object v0
.end method

.method private static varargs b([Ljava/lang/Object;)Lcom/google/obf/jl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">([",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/obf/jl",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 6
    invoke-static {p0}, Lcom/google/obf/jr;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/obf/jl;->a([Ljava/lang/Object;)Lcom/google/obf/jl;

    move-result-object v0

    return-object v0
.end method

.method static b([Ljava/lang/Object;I)Lcom/google/obf/jl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">([",
            "Ljava/lang/Object;",
            "I)",
            "Lcom/google/obf/jl",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 8
    if-nez p1, :cond_0

    .line 9
    invoke-static {}, Lcom/google/obf/jl;->d()Lcom/google/obf/jl;

    move-result-object v0

    .line 10
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/obf/ju;

    invoke-direct {v0, p0, p1}, Lcom/google/obf/ju;-><init>([Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method public static d()Lcom/google/obf/jl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/obf/jl",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1
    sget-object v0, Lcom/google/obf/ju;->a:Lcom/google/obf/jl;

    return-object v0
.end method


# virtual methods
.method a([Ljava/lang/Object;I)I
    .locals 4

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/google/obf/jl;->size()I

    move-result v1

    .line 32
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 33
    add-int v2, p2, v0

    invoke-virtual {p0, v0}, Lcom/google/obf/jl;->get(I)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, p1, v2

    .line 34
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 35
    :cond_0
    add-int v0, p2, v1

    return v0
.end method

.method public a(II)Lcom/google/obf/jl;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lcom/google/obf/jl",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/google/obf/jl;->size()I

    move-result v0

    invoke-static {p1, p2, v0}, Lcom/google/obf/ja;->a(III)V

    .line 19
    sub-int v0, p2, p1

    .line 20
    invoke-virtual {p0}, Lcom/google/obf/jl;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 24
    :goto_0
    return-object p0

    .line 22
    :cond_0
    if-nez v0, :cond_1

    .line 23
    invoke-static {}, Lcom/google/obf/jl;->d()Lcom/google/obf/jl;

    move-result-object p0

    goto :goto_0

    .line 24
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/google/obf/jl;->b(II)Lcom/google/obf/jl;

    move-result-object p0

    goto :goto_0
.end method

.method public a()Lcom/google/obf/jy;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/obf/jy",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/google/obf/jl;->e()Lcom/google/obf/jz;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lcom/google/obf/jz;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/obf/jz",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 14
    new-instance v0, Lcom/google/obf/jl$1;

    invoke-virtual {p0}, Lcom/google/obf/jl;->size()I

    move-result v1

    invoke-direct {v0, p0, v1, p1}, Lcom/google/obf/jl$1;-><init>(Lcom/google/obf/jl;II)V

    return-object v0
.end method

.method public final add(ILjava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 28
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final addAll(ILjava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 26
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b()Lcom/google/obf/jl;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/obf/jl",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 30
    return-object p0
.end method

.method b(II)Lcom/google/obf/jl;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lcom/google/obf/jl",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 25
    new-instance v0, Lcom/google/obf/jl$a;

    sub-int v1, p2, p1

    invoke-direct {v0, p0, p1, v1}, Lcom/google/obf/jl$a;-><init>(Lcom/google/obf/jl;II)V

    return-object v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/google/obf/jl;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Lcom/google/obf/jz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/obf/jz",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 13
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/obf/jl;->a(I)Lcom/google/obf/jz;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 36
    invoke-static {p0, p1}, Lcom/google/obf/jp;->a(Ljava/util/List;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 37
    const/4 v1, 0x1

    .line 38
    invoke-virtual {p0}, Lcom/google/obf/jl;->size()I

    move-result v2

    .line 39
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 40
    mul-int/lit8 v1, v1, 0x1f

    invoke-virtual {p0, v0}, Lcom/google/obf/jl;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    add-int/2addr v1, v3

    .line 41
    xor-int/lit8 v1, v1, -0x1

    xor-int/lit8 v1, v1, -0x1

    .line 42
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 43
    :cond_0
    return v1
.end method

.method public indexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 15
    if-nez p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-static {p0, p1}, Lcom/google/obf/jp;->b(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/google/obf/jl;->a()Lcom/google/obf/jy;

    move-result-object v0

    return-object v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 16
    if-nez p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-static {p0, p1}, Lcom/google/obf/jp;->c(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public synthetic listIterator()Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/google/obf/jl;->e()Lcom/google/obf/jz;

    move-result-object v0

    return-object v0
.end method

.method public synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/google/obf/jl;->a(I)Lcom/google/obf/jz;

    move-result-object v0

    return-object v0
.end method

.method public final remove(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 29
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)TE;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 27
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic subList(II)Ljava/util/List;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0, p1, p2}, Lcom/google/obf/jl;->a(II)Lcom/google/obf/jl;

    move-result-object v0

    return-object v0
.end method
