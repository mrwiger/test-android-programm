.class public Lru/cn/tv/mobile/FaqWebViewActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "FaqWebViewActivity.java"


# instance fields
.field private errorText:Landroid/widget/TextView;

.field private progress:Landroid/view/View;

.field private webView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/mobile/FaqWebViewActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/FaqWebViewActivity;

    .prologue
    .line 22
    iget-object v0, p0, Lru/cn/tv/mobile/FaqWebViewActivity;->progress:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/tv/mobile/FaqWebViewActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/FaqWebViewActivity;

    .prologue
    .line 22
    iget-object v0, p0, Lru/cn/tv/mobile/FaqWebViewActivity;->errorText:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    .line 32
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    const v1, 0x7f0c001b

    invoke-virtual {p0, v1}, Lru/cn/tv/mobile/FaqWebViewActivity;->setContentView(I)V

    .line 35
    const v1, 0x7f0901e2

    invoke-virtual {p0, v1}, Lru/cn/tv/mobile/FaqWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 36
    .local v0, "toolbar":Landroid/support/v7/widget/Toolbar;
    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/FaqWebViewActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 38
    invoke-virtual {p0}, Lru/cn/tv/mobile/FaqWebViewActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 39
    invoke-virtual {p0}, Lru/cn/tv/mobile/FaqWebViewActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 41
    const v1, 0x7f0901f8

    invoke-virtual {p0, v1}, Lru/cn/tv/mobile/FaqWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/webkit/WebView;

    iput-object v1, p0, Lru/cn/tv/mobile/FaqWebViewActivity;->webView:Landroid/webkit/WebView;

    .line 42
    const v1, 0x7f090175

    invoke-virtual {p0, v1}, Lru/cn/tv/mobile/FaqWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lru/cn/tv/mobile/FaqWebViewActivity;->progress:Landroid/view/View;

    .line 43
    const v1, 0x7f0900ba

    invoke-virtual {p0, v1}, Lru/cn/tv/mobile/FaqWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lru/cn/tv/mobile/FaqWebViewActivity;->errorText:Landroid/widget/TextView;

    .line 45
    iget-object v1, p0, Lru/cn/tv/mobile/FaqWebViewActivity;->webView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 46
    iget-object v1, p0, Lru/cn/tv/mobile/FaqWebViewActivity;->webView:Landroid/webkit/WebView;

    new-instance v2, Lru/cn/tv/mobile/FaqWebViewActivity$1;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/FaqWebViewActivity$1;-><init>(Lru/cn/tv/mobile/FaqWebViewActivity;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 90
    iget-object v1, p0, Lru/cn/tv/mobile/FaqWebViewActivity;->webView:Landroid/webkit/WebView;

    new-instance v2, Lru/cn/tv/mobile/FaqWebViewActivity$2;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/FaqWebViewActivity$2;-><init>(Lru/cn/tv/mobile/FaqWebViewActivity;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 113
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 117
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onResume()V

    .line 118
    iget-object v0, p0, Lru/cn/tv/mobile/FaqWebViewActivity;->webView:Landroid/webkit/WebView;

    const-string v1, "http://peers.tv/faq/android/"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 119
    return-void
.end method
