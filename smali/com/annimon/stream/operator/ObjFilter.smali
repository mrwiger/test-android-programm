.class public Lcom/annimon/stream/operator/ObjFilter;
.super Ljava/lang/Object;
.source "ObjFilter.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private hasNext:Z

.field private hasNextEvaluated:Z

.field private final iterator:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<+TT;>;"
        }
    .end annotation
.end field

.field private next:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final predicate:Lcom/annimon/stream/function/Predicate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/annimon/stream/function/Predicate",
            "<-TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Iterator;Lcom/annimon/stream/function/Predicate;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<+TT;>;",
            "Lcom/annimon/stream/function/Predicate",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 14
    .local p0, "this":Lcom/annimon/stream/operator/ObjFilter;, "Lcom/annimon/stream/operator/ObjFilter<TT;>;"
    .local p1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<+TT;>;"
    .local p2, "predicate":Lcom/annimon/stream/function/Predicate;, "Lcom/annimon/stream/function/Predicate<-TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/annimon/stream/operator/ObjFilter;->iterator:Ljava/util/Iterator;

    .line 16
    iput-object p2, p0, Lcom/annimon/stream/operator/ObjFilter;->predicate:Lcom/annimon/stream/function/Predicate;

    .line 17
    return-void
.end method

.method private nextIteration()V
    .locals 2

    .prologue
    .line 41
    .local p0, "this":Lcom/annimon/stream/operator/ObjFilter;, "Lcom/annimon/stream/operator/ObjFilter<TT;>;"
    :cond_0
    iget-object v0, p0, Lcom/annimon/stream/operator/ObjFilter;->iterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42
    iget-object v0, p0, Lcom/annimon/stream/operator/ObjFilter;->iterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/annimon/stream/operator/ObjFilter;->next:Ljava/lang/Object;

    .line 43
    iget-object v0, p0, Lcom/annimon/stream/operator/ObjFilter;->predicate:Lcom/annimon/stream/function/Predicate;

    iget-object v1, p0, Lcom/annimon/stream/operator/ObjFilter;->next:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/annimon/stream/function/Predicate;->test(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/annimon/stream/operator/ObjFilter;->hasNext:Z

    .line 49
    :goto_0
    return-void

    .line 48
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/annimon/stream/operator/ObjFilter;->hasNext:Z

    goto :goto_0
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 21
    .local p0, "this":Lcom/annimon/stream/operator/ObjFilter;, "Lcom/annimon/stream/operator/ObjFilter<TT;>;"
    iget-boolean v0, p0, Lcom/annimon/stream/operator/ObjFilter;->hasNextEvaluated:Z

    if-nez v0, :cond_0

    .line 22
    invoke-direct {p0}, Lcom/annimon/stream/operator/ObjFilter;->nextIteration()V

    .line 23
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/annimon/stream/operator/ObjFilter;->hasNextEvaluated:Z

    .line 25
    :cond_0
    iget-boolean v0, p0, Lcom/annimon/stream/operator/ObjFilter;->hasNext:Z

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 30
    .local p0, "this":Lcom/annimon/stream/operator/ObjFilter;, "Lcom/annimon/stream/operator/ObjFilter<TT;>;"
    iget-boolean v0, p0, Lcom/annimon/stream/operator/ObjFilter;->hasNextEvaluated:Z

    if-nez v0, :cond_0

    .line 31
    invoke-virtual {p0}, Lcom/annimon/stream/operator/ObjFilter;->hasNext()Z

    move-result v0

    iput-boolean v0, p0, Lcom/annimon/stream/operator/ObjFilter;->hasNext:Z

    .line 33
    :cond_0
    iget-boolean v0, p0, Lcom/annimon/stream/operator/ObjFilter;->hasNext:Z

    if-nez v0, :cond_1

    .line 34
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 36
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/annimon/stream/operator/ObjFilter;->hasNextEvaluated:Z

    .line 37
    iget-object v0, p0, Lcom/annimon/stream/operator/ObjFilter;->next:Ljava/lang/Object;

    return-object v0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 53
    .local p0, "this":Lcom/annimon/stream/operator/ObjFilter;, "Lcom/annimon/stream/operator/ObjFilter<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "remove not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
