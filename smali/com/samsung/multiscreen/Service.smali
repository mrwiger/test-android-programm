.class public Lcom/samsung/multiscreen/Service;
.super Ljava/lang/Object;
.source "Service.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/multiscreen/Service$SecureModeState;
    }
.end annotation


# static fields
.field private static final DEFAULT_WOW_TIMEOUT_VALUE:I = 0xea60

.field private static final ENDPOINT_PROPERTY:Ljava/lang/String; = "se"

.field private static final ID_PROPERTY:Ljava/lang/String; = "id"

.field private static final NAME_PROPERTY:Ljava/lang/String; = "fn"

.field private static final PROPERTY_DEVICE:Ljava/lang/String; = "device"

.field private static final PROPERTY_DUID:Ljava/lang/String; = "duid"

.field private static final PROPERTY_ISSUPPORT:Ljava/lang/String; = "isSupport"

.field private static final PROPERTY_NAME:Ljava/lang/String; = "name"

.field private static final PROPERTY_TYPE:Ljava/lang/String; = "type"

.field private static final PROPERTY_URI:Ljava/lang/String; = "uri"

.field private static final PROPERTY_VERSION:Ljava/lang/String; = "version"

.field public static final SUPPORT_DMP:Ljava/lang/String; = "DMP_available"

.field private static final TAG:Ljava/lang/String; = "Service"

.field private static final TV_YEAR_DMP_SUPPORT:I = 0xf

.field private static final TV_YEAR_SSL_SUPPORT:I = 0xf

.field private static final TYPE_PROPERTY:Ljava/lang/String; = "md"

.field public static final TYPE_SMART_TV:Ljava/lang/String; = "Samsung SmartTV"

.field public static final TYPE_SPEAKER:Ljava/lang/String; = "Samsung Speaker"

.field private static final VERSION_PROPERTY:Ljava/lang/String; = "ve"

.field private static isWoWAndConnectStarted:Z


# instance fields
.field private final id:Ljava/lang/String;

.field private isSecureModeSupported:Lcom/samsung/multiscreen/Service$SecureModeState;

.field final isStandbyService:Ljava/lang/Boolean;

.field private final isSupport:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final name:Ljava/lang/String;

.field private final type:Ljava/lang/String;

.field private final uri:Landroid/net/Uri;

.field private final version:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/multiscreen/Service;->isWoWAndConnectStarted:Z

    return-void
.end method

.method protected constructor <init>(Lcom/samsung/multiscreen/Service;)V
    .locals 2
    .param p1, "service"    # Lcom/samsung/multiscreen/Service;

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    sget-object v0, Lcom/samsung/multiscreen/Service$SecureModeState;->Unknown:Lcom/samsung/multiscreen/Service$SecureModeState;

    iput-object v0, p0, Lcom/samsung/multiscreen/Service;->isSecureModeSupported:Lcom/samsung/multiscreen/Service$SecureModeState;

    .line 117
    iget-object v0, p1, Lcom/samsung/multiscreen/Service;->id:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/multiscreen/Service;->id:Ljava/lang/String;

    .line 118
    iget-object v0, p1, Lcom/samsung/multiscreen/Service;->version:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/multiscreen/Service;->version:Ljava/lang/String;

    .line 119
    iget-object v0, p1, Lcom/samsung/multiscreen/Service;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/multiscreen/Service;->name:Ljava/lang/String;

    .line 120
    iget-object v0, p1, Lcom/samsung/multiscreen/Service;->type:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/multiscreen/Service;->type:Ljava/lang/String;

    .line 121
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p1, Lcom/samsung/multiscreen/Service;->isSupport:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/samsung/multiscreen/Service;->isSupport:Ljava/util/Map;

    .line 122
    iget-object v0, p1, Lcom/samsung/multiscreen/Service;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/multiscreen/Service;->uri:Landroid/net/Uri;

    .line 123
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/multiscreen/Service;->isStandbyService:Ljava/lang/Boolean;

    .line 124
    iget-object v0, p1, Lcom/samsung/multiscreen/Service;->isSecureModeSupported:Lcom/samsung/multiscreen/Service$SecureModeState;

    iput-object v0, p0, Lcom/samsung/multiscreen/Service;->isSecureModeSupported:Lcom/samsung/multiscreen/Service$SecureModeState;

    .line 125
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Landroid/net/Uri;Ljava/lang/Boolean;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "version"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "type"    # Ljava/lang/String;
    .param p6, "uri"    # Landroid/net/Uri;
    .param p7, "isStandbyService"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Landroid/net/Uri;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 69
    .local p5, "isSupport":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    sget-object v0, Lcom/samsung/multiscreen/Service$SecureModeState;->Unknown:Lcom/samsung/multiscreen/Service$SecureModeState;

    iput-object v0, p0, Lcom/samsung/multiscreen/Service;->isSecureModeSupported:Lcom/samsung/multiscreen/Service$SecureModeState;

    .line 69
    iput-object p1, p0, Lcom/samsung/multiscreen/Service;->id:Ljava/lang/String;

    iput-object p2, p0, Lcom/samsung/multiscreen/Service;->version:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsung/multiscreen/Service;->name:Ljava/lang/String;

    iput-object p4, p0, Lcom/samsung/multiscreen/Service;->type:Ljava/lang/String;

    iput-object p5, p0, Lcom/samsung/multiscreen/Service;->isSupport:Ljava/util/Map;

    iput-object p6, p0, Lcom/samsung/multiscreen/Service;->uri:Landroid/net/Uri;

    iput-object p7, p0, Lcom/samsung/multiscreen/Service;->isStandbyService:Ljava/lang/Boolean;

    return-void
.end method

.method public static WakeOnWirelessAndConnect(Ljava/lang/String;Landroid/net/Uri;ILcom/samsung/multiscreen/Result;)V
    .locals 4
    .param p0, "macAddr"    # Ljava/lang/String;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "timeout"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "I",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/Service;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 453
    .local p3, "connectCallback":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Service;>;"
    sget-boolean v1, Lcom/samsung/multiscreen/Service;->isWoWAndConnectStarted:Z

    if-eqz v1, :cond_0

    .line 469
    :goto_0
    return-void

    .line 455
    :cond_0
    const/4 v1, 0x1

    sput-boolean v1, Lcom/samsung/multiscreen/Service;->isWoWAndConnectStarted:Z

    .line 456
    if-nez p0, :cond_1

    .line 457
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 459
    :cond_1
    invoke-static {p0}, Lcom/samsung/multiscreen/Service;->WakeOnWirelessLan(Ljava/lang/String;)V

    .line 460
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 461
    .local v0, "timerHandler":Landroid/os/Handler;
    invoke-static {p1, p3}, Lcom/samsung/multiscreen/Service;->WakeUpAndConnect(Landroid/net/Uri;Lcom/samsung/multiscreen/Result;)V

    .line 462
    new-instance v1, Lcom/samsung/multiscreen/Service$6;

    invoke-direct {v1}, Lcom/samsung/multiscreen/Service$6;-><init>()V

    int-to-long v2, p2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public static WakeOnWirelessAndConnect(Ljava/lang/String;Landroid/net/Uri;Lcom/samsung/multiscreen/Result;)V
    .locals 1
    .param p0, "macAddr"    # Ljava/lang/String;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/Service;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 441
    .local p2, "connectCallback":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Service;>;"
    const v0, 0xea60

    invoke-static {p0, p1, v0, p2}, Lcom/samsung/multiscreen/Service;->WakeOnWirelessAndConnect(Ljava/lang/String;Landroid/net/Uri;ILcom/samsung/multiscreen/Result;)V

    .line 442
    return-void
.end method

.method public static WakeOnWirelessLan(Ljava/lang/String;)V
    .locals 18
    .param p0, "macAddr"    # Ljava/lang/String;

    .prologue
    .line 356
    const/16 v2, 0x7de

    .line 357
    .local v2, "SRC_PORT":I
    const/16 v1, 0x7de

    .line 358
    .local v1, "DST_PORT":I
    const-string v7, "FF:FF:FF:FF:FF:FF"

    .line 359
    .local v7, "magicPacketId":Ljava/lang/String;
    const-string v4, "255.255.255.255"

    .line 360
    .local v4, "broadCastAddr":Ljava/lang/String;
    const-string v11, "SECWOW"

    .line 361
    .local v11, "wakeUpIdentifier":Ljava/lang/String;
    const-string v10, "00:00:00:00:00:00"

    .line 363
    .local v10, "secureOn":Ljava/lang/String;
    const/16 v13, 0x88

    .line 364
    .local v13, "wow_packet_max_size":I
    const/16 v14, 0x66

    .line 365
    .local v14, "wow_packet_min_size":I
    const/4 v15, 0x6

    .line 366
    .local v15, "wow_packet_sec_size":I
    const/16 v16, 0xc

    .line 367
    .local v16, "wow_packet_ss_size":I
    const/16 v8, 0x66

    .line 368
    .local v8, "packetSizeAlloc":I
    const/4 v9, 0x0

    .line 369
    .local v9, "reservedField":I
    const/4 v3, 0x0

    .line 381
    .local v3, "applicationID":I
    add-int/lit8 v8, v8, 0x6

    .line 382
    add-int/lit8 v8, v8, 0xc

    .line 386
    invoke-static {v8}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v12

    .line 387
    .local v12, "wowPacketBuffer":Ljava/nio/ByteBuffer;
    const-string v17, "FF:FF:FF:FF:FF:FF"

    invoke-static/range {v17 .. v17}, Lcom/samsung/multiscreen/Service;->convertMacAddrToBytes(Ljava/lang/String;)[B

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 389
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    const/16 v17, 0x10

    move/from16 v0, v17

    if-ge v5, v0, :cond_0

    .line 391
    invoke-static/range {p0 .. p0}, Lcom/samsung/multiscreen/Service;->convertMacAddrToBytes(Ljava/lang/String;)[B

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 389
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 395
    :cond_0
    const-string v17, "00:00:00:00:00:00"

    invoke-static/range {v17 .. v17}, Lcom/samsung/multiscreen/Service;->convertMacAddrToBytes(Ljava/lang/String;)[B

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 396
    const-string v17, "SECWOW"

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->getBytes()[B

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 397
    invoke-virtual {v12, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 398
    int-to-byte v0, v3

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 406
    invoke-virtual {v12}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    .line 408
    .local v6, "magicPacket":[B
    new-instance v17, Lcom/samsung/multiscreen/Service$5;

    move-object/from16 v0, v17

    invoke-direct {v0, v6}, Lcom/samsung/multiscreen/Service$5;-><init>([B)V

    invoke-static/range {v17 .. v17}, Lcom/samsung/multiscreen/util/RunUtil;->runInBackground(Ljava/lang/Runnable;)V

    .line 430
    return-void
.end method

.method private static WakeUpAndConnect(Landroid/net/Uri;Lcom/samsung/multiscreen/Result;)V
    .locals 1
    .param p0, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/Service;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 478
    .local p1, "connectCallback":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Service;>;"
    new-instance v0, Lcom/samsung/multiscreen/Service$7;

    invoke-direct {v0, p0, p1}, Lcom/samsung/multiscreen/Service$7;-><init>(Landroid/net/Uri;Lcom/samsung/multiscreen/Result;)V

    invoke-static {p0, v0}, Lcom/samsung/multiscreen/Service;->getByURI(Landroid/net/Uri;Lcom/samsung/multiscreen/Result;)V

    .line 495
    return-void
.end method

.method static synthetic access$000(Ljava/util/Map;)Lcom/samsung/multiscreen/Service;
    .locals 1
    .param p0, "x0"    # Ljava/util/Map;

    .prologue
    .line 71
    invoke-static {p0}, Lcom/samsung/multiscreen/Service;->create(Ljava/util/Map;)Lcom/samsung/multiscreen/Service;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/multiscreen/Service;Lcom/samsung/multiscreen/Service$SecureModeState;)Lcom/samsung/multiscreen/Service$SecureModeState;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/Service;
    .param p1, "x1"    # Lcom/samsung/multiscreen/Service$SecureModeState;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/samsung/multiscreen/Service;->isSecureModeSupported:Lcom/samsung/multiscreen/Service$SecureModeState;

    return-object p1
.end method

.method static synthetic access$200()Z
    .locals 1

    .prologue
    .line 71
    sget-boolean v0, Lcom/samsung/multiscreen/Service;->isWoWAndConnectStarted:Z

    return v0
.end method

.method static synthetic access$202(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 71
    sput-boolean p0, Lcom/samsung/multiscreen/Service;->isWoWAndConnectStarted:Z

    return p0
.end method

.method static synthetic access$300(Landroid/net/Uri;Lcom/samsung/multiscreen/Result;)V
    .locals 0
    .param p0, "x0"    # Landroid/net/Uri;
    .param p1, "x1"    # Lcom/samsung/multiscreen/Result;

    .prologue
    .line 71
    invoke-static {p0, p1}, Lcom/samsung/multiscreen/Service;->WakeUpAndConnect(Landroid/net/Uri;Lcom/samsung/multiscreen/Result;)V

    return-void
.end method

.method private static convertMacAddrToBytes(Ljava/lang/String;)[B
    .locals 7
    .param p0, "macAddr"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x6

    .line 506
    const-string v4, ":"

    invoke-virtual {p0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 508
    .local v2, "macAddrAtoms":[Ljava/lang/String;
    new-array v3, v6, [B

    .line 509
    .local v3, "macAddressBytes":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v6, :cond_0

    .line 510
    aget-object v4, v2, v1

    const/16 v5, 0x10

    invoke-static {v4, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 511
    .local v0, "hex":Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->byteValue()B

    move-result v4

    aput-byte v4, v3, v1

    .line 509
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 514
    .end local v0    # "hex":Ljava/lang/Integer;
    :cond_0
    return-object v3
.end method

.method private static create(Ljava/util/Map;)Lcom/samsung/multiscreen/Service;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/samsung/multiscreen/Service;"
        }
    .end annotation

    .prologue
    .line 703
    .local p0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-nez p0, :cond_0

    .line 704
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 707
    :cond_0
    const-string v0, "id"

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 708
    .local v1, "id":Ljava/lang/String;
    const-string v0, "version"

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 709
    .local v2, "version":Ljava/lang/String;
    const-string v0, "name"

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 710
    .local v3, "name":Ljava/lang/String;
    const-string v0, "type"

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 711
    .local v4, "type":Ljava/lang/String;
    const-string v0, "isSupport"

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/multiscreen/util/JSONUtil;->parse(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v5

    .line 712
    .local v5, "isSupport":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "uri"

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 714
    .local v6, "endPoint":Landroid/net/Uri;
    new-instance v0, Lcom/samsung/multiscreen/Service;

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/samsung/multiscreen/Service;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Landroid/net/Uri;Ljava/lang/Boolean;)V

    return-object v0
.end method

.method static create(Ljavax/jmdns/ServiceInfo;)Lcom/samsung/multiscreen/Service;
    .locals 8
    .param p0, "info"    # Ljavax/jmdns/ServiceInfo;

    .prologue
    .line 687
    if-nez p0, :cond_0

    .line 688
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 691
    :cond_0
    const-string v0, "id"

    invoke-virtual {p0, v0}, Ljavax/jmdns/ServiceInfo;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 692
    .local v1, "id":Ljava/lang/String;
    const-string v0, "ve"

    invoke-virtual {p0, v0}, Ljavax/jmdns/ServiceInfo;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 693
    .local v2, "version":Ljava/lang/String;
    const-string v0, "fn"

    invoke-virtual {p0, v0}, Ljavax/jmdns/ServiceInfo;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 694
    .local v3, "name":Ljava/lang/String;
    const-string v0, "md"

    invoke-virtual {p0, v0}, Ljavax/jmdns/ServiceInfo;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 695
    .local v4, "type":Ljava/lang/String;
    const-string v0, "isSupport"

    invoke-virtual {p0, v0}, Ljavax/jmdns/ServiceInfo;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/multiscreen/util/JSONUtil;->parse(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v5

    .line 696
    .local v5, "isSupport":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "se"

    invoke-virtual {p0, v0}, Ljavax/jmdns/ServiceInfo;->getPropertyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 698
    .local v6, "endPoint":Landroid/net/Uri;
    new-instance v0, Lcom/samsung/multiscreen/Service;

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/samsung/multiscreen/Service;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Landroid/net/Uri;Ljava/lang/Boolean;)V

    return-object v0
.end method

.method protected static create(Lorg/json/JSONObject;)Lcom/samsung/multiscreen/Service;
    .locals 10
    .param p0, "jsonObject"    # Lorg/json/JSONObject;

    .prologue
    .line 718
    if-nez p0, :cond_0

    .line 719
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 722
    :cond_0
    const/4 v1, 0x0

    .line 723
    .local v1, "id":Ljava/lang/String;
    const-string v2, "Unknown"

    .line 724
    .local v2, "version":Ljava/lang/String;
    const/4 v3, 0x0

    .line 725
    .local v3, "name":Ljava/lang/String;
    const-string v4, "Samsung SmartTV"

    .line 726
    .local v4, "type":Ljava/lang/String;
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 727
    .local v5, "isSupport":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v6, 0x0

    .line 729
    .local v6, "endPoint":Landroid/net/Uri;
    :try_start_0
    const-string v0, "id"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 730
    const-string v0, "name"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 731
    const-string v0, "(standby)"

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 732
    const-string v0, "uri"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 737
    :goto_0
    new-instance v0, Lcom/samsung/multiscreen/Service;

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/samsung/multiscreen/Service;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Landroid/net/Uri;Ljava/lang/Boolean;)V

    return-object v0

    .line 733
    :catch_0
    move-exception v8

    .line 734
    .local v8, "e":Ljava/lang/Exception;
    const-string v0, "Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "create(): Error: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getById(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/multiscreen/Result;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/Service;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 232
    .local p2, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Service;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 234
    .local v3, "threads":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/multiscreen/ProviderThread;>;"
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 236
    .local v0, "results":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    new-instance v1, Lcom/samsung/multiscreen/Service$2;

    invoke-direct {v1, v0, v3}, Lcom/samsung/multiscreen/Service$2;-><init>(Ljava/util/List;Ljava/util/List;)V

    .line 261
    .local v1, "searchResult":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Service;>;"
    invoke-static {p0, p1, v1}, Lcom/samsung/multiscreen/MDNSSearchProvider;->getById(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/multiscreen/Result;)Lcom/samsung/multiscreen/ProviderThread;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 262
    invoke-static {p0, p1, v1}, Lcom/samsung/multiscreen/MSFDSearchProvider;->getById(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/multiscreen/Result;)Lcom/samsung/multiscreen/ProviderThread;

    move-result-object v2

    .line 263
    .local v2, "thread":Lcom/samsung/multiscreen/ProviderThread;
    if-eqz v2, :cond_0

    .line 264
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 268
    :cond_0
    new-instance v4, Lcom/samsung/multiscreen/Service$3;

    invoke-direct {v4, v3, v0, p2}, Lcom/samsung/multiscreen/Service$3;-><init>(Ljava/util/List;Ljava/util/List;Lcom/samsung/multiscreen/Result;)V

    invoke-static {v4}, Lcom/samsung/multiscreen/util/RunUtil;->runInBackground(Ljava/lang/Runnable;)V

    .line 304
    return-void
.end method

.method public static getByURI(Landroid/net/Uri;ILcom/samsung/multiscreen/Result;)V
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "timeout"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "I",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/Service;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 212
    .local p2, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Service;>;"
    new-instance v1, Lcom/samsung/multiscreen/Service$1;

    invoke-direct {v1}, Lcom/samsung/multiscreen/Service$1;-><init>()V

    invoke-static {v1, p2}, Lcom/samsung/multiscreen/HttpHelper;->createHttpCallback(Lcom/samsung/multiscreen/util/HttpUtil$ResultCreator;Lcom/samsung/multiscreen/Result;)Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;

    move-result-object v0

    .line 219
    .local v0, "httpStringCallback":Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;
    const-string v1, "GET"

    invoke-static {p0, v1, p1, v0}, Lcom/samsung/multiscreen/util/HttpUtil;->executeJSONRequest(Landroid/net/Uri;Ljava/lang/String;ILcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;)V

    .line 220
    return-void
.end method

.method public static getByURI(Landroid/net/Uri;Lcom/samsung/multiscreen/Result;)V
    .locals 1
    .param p0, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/Service;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 201
    .local p1, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Service;>;"
    const/16 v0, 0x7530

    invoke-static {p0, v0, p1}, Lcom/samsung/multiscreen/Service;->getByURI(Landroid/net/Uri;ILcom/samsung/multiscreen/Result;)V

    .line 202
    return-void
.end method

.method private isSupport(Ljava/lang/String;)Z
    .locals 2
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 793
    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->isSupport:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->isSupport:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static search(Landroid/content/Context;)Lcom/samsung/multiscreen/Search;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 191
    invoke-static {p0}, Lcom/samsung/multiscreen/Search;->getInstance(Landroid/content/Context;)Lcom/samsung/multiscreen/Search;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected canEqual(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 70
    instance-of v0, p1, Lcom/samsung/multiscreen/Service;

    return v0
.end method

.method createApplication(Landroid/net/Uri;)Lcom/samsung/multiscreen/Application;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 555
    if-nez p1, :cond_0

    .line 556
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 559
    :cond_0
    invoke-static {p0, p1}, Lcom/samsung/multiscreen/Application;->create(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;)Lcom/samsung/multiscreen/Application;

    move-result-object v0

    return-object v0
.end method

.method public createApplication(Landroid/net/Uri;Ljava/lang/String;)Lcom/samsung/multiscreen/Application;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "channelId"    # Ljava/lang/String;

    .prologue
    .line 571
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 572
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 575
    :cond_1
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/samsung/multiscreen/Application;->create(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;Ljava/lang/String;Ljava/util/Map;)Lcom/samsung/multiscreen/Application;

    move-result-object v0

    return-object v0
.end method

.method public createApplication(Landroid/net/Uri;Ljava/lang/String;Ljava/util/Map;)Lcom/samsung/multiscreen/Application;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "channelId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/samsung/multiscreen/Application;"
        }
    .end annotation

    .prologue
    .line 589
    .local p3, "startArgs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 590
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 593
    :cond_1
    invoke-static {p0, p1, p2, p3}, Lcom/samsung/multiscreen/Application;->create(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;Ljava/lang/String;Ljava/util/Map;)Lcom/samsung/multiscreen/Application;

    move-result-object v0

    return-object v0
.end method

.method createApplication(Ljava/lang/String;)Lcom/samsung/multiscreen/Application;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 604
    if-nez p1, :cond_0

    .line 605
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 608
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 609
    .local v0, "uri":Landroid/net/Uri;
    invoke-static {p0, v0}, Lcom/samsung/multiscreen/Application;->create(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;)Lcom/samsung/multiscreen/Application;

    move-result-object v1

    return-object v1
.end method

.method public createApplication(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/multiscreen/Application;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "channelId"    # Ljava/lang/String;

    .prologue
    .line 621
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 622
    :cond_0
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 625
    :cond_1
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 626
    .local v0, "uri":Landroid/net/Uri;
    const/4 v1, 0x0

    invoke-static {p0, v0, p2, v1}, Lcom/samsung/multiscreen/Application;->create(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;Ljava/lang/String;Ljava/util/Map;)Lcom/samsung/multiscreen/Application;

    move-result-object v1

    return-object v1
.end method

.method public createApplication(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lcom/samsung/multiscreen/Application;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "channelId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/samsung/multiscreen/Application;"
        }
    .end annotation

    .prologue
    .line 678
    .local p3, "startArgs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 679
    :cond_0
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 682
    :cond_1
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 683
    .local v0, "uri":Landroid/net/Uri;
    invoke-static {p0, v0, p2, p3}, Lcom/samsung/multiscreen/Application;->create(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;Ljava/lang/String;Ljava/util/Map;)Lcom/samsung/multiscreen/Application;

    move-result-object v1

    return-object v1
.end method

.method public createAudioPlayer(Ljava/lang/String;)Lcom/samsung/multiscreen/AudioPlayer;
    .locals 2
    .param p1, "appName"    # Ljava/lang/String;

    .prologue
    .line 661
    iget-object v1, p0, Lcom/samsung/multiscreen/Service;->id:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 662
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 664
    :cond_0
    iget-object v1, p0, Lcom/samsung/multiscreen/Service;->id:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 665
    .local v0, "uri":Landroid/net/Uri;
    new-instance v1, Lcom/samsung/multiscreen/AudioPlayer;

    invoke-direct {v1, p0, v0, p1}, Lcom/samsung/multiscreen/AudioPlayer;-><init>(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;Ljava/lang/String;)V

    return-object v1
.end method

.method public createChannel(Landroid/net/Uri;)Lcom/samsung/multiscreen/Channel;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 544
    invoke-static {p0, p1}, Lcom/samsung/multiscreen/Channel;->create(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;)Lcom/samsung/multiscreen/Channel;

    move-result-object v0

    return-object v0
.end method

.method public createPhotoPlayer(Ljava/lang/String;)Lcom/samsung/multiscreen/PhotoPlayer;
    .locals 2
    .param p1, "appName"    # Ljava/lang/String;

    .prologue
    .line 648
    iget-object v1, p0, Lcom/samsung/multiscreen/Service;->id:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 649
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 651
    :cond_0
    iget-object v1, p0, Lcom/samsung/multiscreen/Service;->id:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 652
    .local v0, "uri":Landroid/net/Uri;
    new-instance v1, Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-direct {v1, p0, v0, p1}, Lcom/samsung/multiscreen/PhotoPlayer;-><init>(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;Ljava/lang/String;)V

    return-object v1
.end method

.method public createVideoPlayer(Ljava/lang/String;)Lcom/samsung/multiscreen/VideoPlayer;
    .locals 2
    .param p1, "appName"    # Ljava/lang/String;

    .prologue
    .line 635
    iget-object v1, p0, Lcom/samsung/multiscreen/Service;->id:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 636
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 638
    :cond_0
    iget-object v1, p0, Lcom/samsung/multiscreen/Service;->id:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 639
    .local v0, "uri":Landroid/net/Uri;
    new-instance v1, Lcom/samsung/multiscreen/VideoPlayer;

    invoke-direct {v1, p0, v0, p1}, Lcom/samsung/multiscreen/VideoPlayer;-><init>(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;Ljava/lang/String;)V

    return-object v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 70
    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    instance-of v5, p1, Lcom/samsung/multiscreen/Service;

    if-nez v5, :cond_2

    move v3, v4

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/samsung/multiscreen/Service;

    .local v0, "other":Lcom/samsung/multiscreen/Service;
    invoke-virtual {v0, p0}, Lcom/samsung/multiscreen/Service;->canEqual(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    move v3, v4

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Service;->getId()Ljava/lang/String;

    move-result-object v2

    .local v2, "this$id":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/samsung/multiscreen/Service;->getId()Ljava/lang/String;

    move-result-object v1

    .local v1, "other$id":Ljava/lang/String;
    if-nez v2, :cond_4

    if-eqz v1, :cond_0

    :goto_1
    move v3, v4

    goto :goto_0

    :cond_4
    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    goto :goto_1
.end method

.method public getDeviceInfo(Lcom/samsung/multiscreen/Result;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/Device;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 524
    .local p1, "callback":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Device;>;"
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Service;->getUri()Landroid/net/Uri;

    move-result-object v1

    .line 525
    .local v1, "uri":Landroid/net/Uri;
    new-instance v2, Lcom/samsung/multiscreen/Service$8;

    invoke-direct {v2, p0}, Lcom/samsung/multiscreen/Service$8;-><init>(Lcom/samsung/multiscreen/Service;)V

    invoke-static {v2, p1}, Lcom/samsung/multiscreen/HttpHelper;->createHttpCallback(Lcom/samsung/multiscreen/util/HttpUtil$ResultCreator;Lcom/samsung/multiscreen/Result;)Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;

    move-result-object v0

    .line 533
    .local v0, "httpStringCallback":Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;
    const-string v2, "GET"

    invoke-static {v1, v2, v0}, Lcom/samsung/multiscreen/util/HttpUtil;->executeJSONRequest(Landroid/net/Uri;Ljava/lang/String;Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;)V

    .line 534
    return-void
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getIsSecureModeSupported()Lcom/samsung/multiscreen/Service$SecureModeState;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->isSecureModeSupported:Lcom/samsung/multiscreen/Service$SecureModeState;

    return-object v0
.end method

.method public getIsStandbyService()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->isStandbyService:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getIsSupport()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173
    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->isSupport:Ljava/util/Map;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->type:Ljava/lang/String;

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->uri:Landroid/net/Uri;

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->version:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 70
    const/16 v1, 0x3b

    .local v1, "PRIME":I
    const/4 v2, 0x1

    .local v2, "result":I
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Service;->getId()Ljava/lang/String;

    move-result-object v0

    .local v0, "$id":Ljava/lang/String;
    if-nez v0, :cond_0

    const/4 v3, 0x0

    :goto_0
    add-int/lit8 v2, v3, 0x3b

    return v2

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    goto :goto_0
.end method

.method public isDMPSupported(Lcom/samsung/multiscreen/Result;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 754
    .local p1, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Ljava/lang/Boolean;>;"
    if-eqz p1, :cond_0

    .line 755
    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->isStandbyService:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 759
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/samsung/multiscreen/Result;->onSuccess(Ljava/lang/Object;)V

    .line 785
    :cond_0
    :goto_0
    return-void

    .line 760
    :cond_1
    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->isSupport:Ljava/util/Map;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->isSupport:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 761
    const-string v0, "DMP_available"

    invoke-direct {p0, v0}, Lcom/samsung/multiscreen/Service;->isSupport(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/samsung/multiscreen/Result;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0

    .line 763
    :cond_2
    new-instance v0, Lcom/samsung/multiscreen/Service$9;

    invoke-direct {v0, p0, p1}, Lcom/samsung/multiscreen/Service$9;-><init>(Lcom/samsung/multiscreen/Service;Lcom/samsung/multiscreen/Result;)V

    invoke-virtual {p0, v0}, Lcom/samsung/multiscreen/Service;->getDeviceInfo(Lcom/samsung/multiscreen/Result;)V

    goto :goto_0
.end method

.method isEqualTo(Lcom/samsung/multiscreen/Service;)Ljava/lang/Boolean;
    .locals 3
    .param p1, "service"    # Lcom/samsung/multiscreen/Service;

    .prologue
    const/4 v2, 0x0

    .line 133
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Service;->hashCode()I

    move-result v0

    invoke-virtual {p1}, Lcom/samsung/multiscreen/Service;->hashCode()I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 142
    :goto_0
    return-object v0

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/samsung/multiscreen/Service;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 135
    :cond_1
    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->isStandbyService:Ljava/lang/Boolean;

    iget-object v1, p1, Lcom/samsung/multiscreen/Service;->isStandbyService:Ljava/lang/Boolean;

    if-eq v0, v1, :cond_2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 136
    :cond_2
    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/samsung/multiscreen/Service;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 137
    :cond_3
    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->uri:Landroid/net/Uri;

    iget-object v1, p1, Lcom/samsung/multiscreen/Service;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 138
    :cond_4
    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->type:Ljava/lang/String;

    iget-object v1, p1, Lcom/samsung/multiscreen/Service;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 139
    :cond_5
    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->version:Ljava/lang/String;

    iget-object v1, p1, Lcom/samsung/multiscreen/Service;->version:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 140
    :cond_6
    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->isSupport:Ljava/util/Map;

    iget-object v1, p1, Lcom/samsung/multiscreen/Service;->isSupport:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 141
    :cond_7
    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->isSecureModeSupported:Lcom/samsung/multiscreen/Service$SecureModeState;

    iget-object v1, p1, Lcom/samsung/multiscreen/Service;->isSecureModeSupported:Lcom/samsung/multiscreen/Service$SecureModeState;

    if-eq v0, v1, :cond_8

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 142
    :cond_8
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method isSecurityModeSupported(Lcom/samsung/multiscreen/Result;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Ljava/lang/Boolean;>;"
    const/4 v2, 0x1

    .line 312
    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->isStandbyService:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/samsung/multiscreen/Result;->onSuccess(Ljava/lang/Object;)V

    .line 346
    :goto_0
    return-void

    .line 314
    :cond_0
    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->isSecureModeSupported:Lcom/samsung/multiscreen/Service$SecureModeState;

    sget-object v1, Lcom/samsung/multiscreen/Service$SecureModeState;->Unknown:Lcom/samsung/multiscreen/Service$SecureModeState;

    if-ne v0, v1, :cond_1

    .line 315
    new-instance v0, Lcom/samsung/multiscreen/Service$4;

    invoke-direct {v0, p0, p1}, Lcom/samsung/multiscreen/Service$4;-><init>(Lcom/samsung/multiscreen/Service;Lcom/samsung/multiscreen/Result;)V

    invoke-virtual {p0, v0}, Lcom/samsung/multiscreen/Service;->getDeviceInfo(Lcom/samsung/multiscreen/Result;)V

    goto :goto_0

    .line 341
    :cond_1
    iget-object v0, p0, Lcom/samsung/multiscreen/Service;->isSecureModeSupported:Lcom/samsung/multiscreen/Service$SecureModeState;

    sget-object v1, Lcom/samsung/multiscreen/Service$SecureModeState;->Supported:Lcom/samsung/multiscreen/Service$SecureModeState;

    if-ne v0, v1, :cond_2

    .line 342
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/samsung/multiscreen/Result;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0

    .line 344
    :cond_2
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/samsung/multiscreen/Result;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 744
    invoke-static {}, Lcom/samsung/multiscreen/StandbyDeviceList;->getInstance()Lcom/samsung/multiscreen/StandbyDeviceList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 745
    invoke-static {}, Lcom/samsung/multiscreen/StandbyDeviceList;->getInstance()Lcom/samsung/multiscreen/StandbyDeviceList;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/multiscreen/StandbyDeviceList;->remove(Lcom/samsung/multiscreen/Service;)V

    .line 747
    :cond_0
    return-void
.end method

.method public setIsSecureModeSupported(Lcom/samsung/multiscreen/Service$SecureModeState;)V
    .locals 0
    .param p1, "isSecureModeSupported"    # Lcom/samsung/multiscreen/Service$SecureModeState;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/samsung/multiscreen/Service;->isSecureModeSupported:Lcom/samsung/multiscreen/Service$SecureModeState;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Service(isSecureModeSupported="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Service;->getIsSecureModeSupported()Lcom/samsung/multiscreen/Service$SecureModeState;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Service;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Service;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Service;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Service;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isSupport="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Service;->getIsSupport()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Service;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isStandbyService="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Service;->getIsStandbyService()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
