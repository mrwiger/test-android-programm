.class Lcom/samsung/multiscreen/Player$4$1;
.super Ljava/lang/Object;
.source "Player.java"

# interfaces
.implements Lcom/samsung/multiscreen/Result;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/multiscreen/Player$4;->onSuccess(Lcom/samsung/multiscreen/Player$DMPStatus;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/samsung/multiscreen/Result",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/multiscreen/Player$4;


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/Player$4;)V
    .locals 0
    .param p1, "this$1"    # Lcom/samsung/multiscreen/Player$4;

    .prologue
    .line 509
    iput-object p1, p0, Lcom/samsung/multiscreen/Player$4$1;->this$1:Lcom/samsung/multiscreen/Player$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/samsung/multiscreen/Error;)V
    .locals 3
    .param p1, "error"    # Lcom/samsung/multiscreen/Error;

    .prologue
    .line 528
    sget-object v0, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    invoke-virtual {v0}, Lcom/samsung/multiscreen/Application;->closeConnection()V

    .line 529
    iget-object v0, p0, Lcom/samsung/multiscreen/Player$4$1;->this$1:Lcom/samsung/multiscreen/Player$4;

    iget-object v0, v0, Lcom/samsung/multiscreen/Player$4;->this$0:Lcom/samsung/multiscreen/Player;

    invoke-virtual {v0}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Player"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DMP Launch Failed with error message : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/multiscreen/Error;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    :cond_0
    iget-object v0, p0, Lcom/samsung/multiscreen/Player$4$1;->this$1:Lcom/samsung/multiscreen/Player$4;

    iget-object v0, v0, Lcom/samsung/multiscreen/Player$4;->val$result:Lcom/samsung/multiscreen/Result;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/multiscreen/Player$4$1;->this$1:Lcom/samsung/multiscreen/Player$4;

    iget-object v0, v0, Lcom/samsung/multiscreen/Player$4;->val$result:Lcom/samsung/multiscreen/Result;

    invoke-interface {v0, p1}, Lcom/samsung/multiscreen/Result;->onError(Lcom/samsung/multiscreen/Error;)V

    .line 532
    :cond_1
    return-void
.end method

.method public onSuccess(Ljava/lang/Boolean;)V
    .locals 4
    .param p1, "success"    # Ljava/lang/Boolean;

    .prologue
    .line 513
    iget-object v1, p0, Lcom/samsung/multiscreen/Player$4$1;->this$1:Lcom/samsung/multiscreen/Player$4;

    iget-object v1, v1, Lcom/samsung/multiscreen/Player$4;->this$0:Lcom/samsung/multiscreen/Player;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Player"

    const-string v2, "DMP Launched Successfully, Sending ChangePlayingContent Request.."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 515
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/multiscreen/Player$4$1;->this$1:Lcom/samsung/multiscreen/Player$4;

    iget-object v1, v1, Lcom/samsung/multiscreen/Player$4;->val$data:Lorg/json/JSONObject;

    const-string v2, "subEvent"

    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerContentSubEvents;->CHANGEPLAYINGCONTENT:Lcom/samsung/multiscreen/Player$PlayerContentSubEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerContentSubEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 516
    iget-object v1, p0, Lcom/samsung/multiscreen/Player$4$1;->this$1:Lcom/samsung/multiscreen/Player$4;

    iget-object v1, v1, Lcom/samsung/multiscreen/Player$4;->val$data:Lorg/json/JSONObject;

    const-string v2, "playerType"

    iget-object v3, p0, Lcom/samsung/multiscreen/Player$4$1;->this$1:Lcom/samsung/multiscreen/Player$4;

    iget-object v3, v3, Lcom/samsung/multiscreen/Player$4;->this$0:Lcom/samsung/multiscreen/Player;

    invoke-static {v3}, Lcom/samsung/multiscreen/Player;->access$200(Lcom/samsung/multiscreen/Player;)Lcom/samsung/multiscreen/Player$ContentType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 521
    sget-object v1, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v2, "playerContentChange"

    iget-object v3, p0, Lcom/samsung/multiscreen/Player$4$1;->this$1:Lcom/samsung/multiscreen/Player$4;

    iget-object v3, v3, Lcom/samsung/multiscreen/Player$4;->val$data:Lorg/json/JSONObject;

    invoke-virtual {v1, v2, v3}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 523
    iget-object v1, p0, Lcom/samsung/multiscreen/Player$4$1;->this$1:Lcom/samsung/multiscreen/Player$4;

    iget-object v1, v1, Lcom/samsung/multiscreen/Player$4;->val$result:Lcom/samsung/multiscreen/Result;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/multiscreen/Player$4$1;->this$1:Lcom/samsung/multiscreen/Player$4;

    iget-object v1, v1, Lcom/samsung/multiscreen/Player$4;->val$result:Lcom/samsung/multiscreen/Result;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/multiscreen/Result;->onSuccess(Ljava/lang/Object;)V

    .line 524
    :cond_1
    :goto_0
    return-void

    .line 517
    :catch_0
    move-exception v0

    .line 518
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/samsung/multiscreen/Player$4$1;->this$1:Lcom/samsung/multiscreen/Player$4;

    iget-object v1, v1, Lcom/samsung/multiscreen/Player$4;->this$0:Lcom/samsung/multiscreen/Player;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "Player"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error while creating ChangePlayingContent Request : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 509
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/samsung/multiscreen/Player$4$1;->onSuccess(Ljava/lang/Boolean;)V

    return-void
.end method
