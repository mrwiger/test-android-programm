.class public Lru/cn/tv/player/controller/MediaTooltipView;
.super Landroid/widget/FrameLayout;
.source "MediaTooltipView.java"


# instance fields
.field private telecastDateTime:Landroid/widget/TextView;

.field private telecastImage:Landroid/widget/ImageView;

.field private telecastTitle:Landroid/widget/TextView;

.field private wrapperTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lru/cn/tv/player/controller/MediaTooltipView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    invoke-virtual {p0}, Lru/cn/tv/player/controller/MediaTooltipView;->onFinishInflate()V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lru/cn/tv/player/controller/MediaTooltipView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 44
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f0c0080

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 45
    return-void
.end method


# virtual methods
.method public configure(Ljava/lang/String;Lru/cn/api/tv/replies/Telecast;)V
    .locals 8
    .param p1, "headline"    # Ljava/lang/String;
    .param p2, "telecast"    # Lru/cn/api/tv/replies/Telecast;

    .prologue
    .line 66
    if-eqz p2, :cond_0

    .line 67
    iget-object v6, p0, Lru/cn/tv/player/controller/MediaTooltipView;->wrapperTitle:Landroid/widget/TextView;

    invoke-virtual {v6, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    sget-object v6, Lru/cn/api/tv/replies/TelecastImage$Profile;->IMAGE:Lru/cn/api/tv/replies/TelecastImage$Profile;

    invoke-virtual {p2, v6}, Lru/cn/api/tv/replies/Telecast;->getImage(Lru/cn/api/tv/replies/TelecastImage$Profile;)Lru/cn/api/tv/replies/TelecastImage;

    move-result-object v2

    .line 70
    .local v2, "image":Lru/cn/api/tv/replies/TelecastImage;
    if-eqz v2, :cond_1

    iget-object v3, v2, Lru/cn/api/tv/replies/TelecastImage;->location:Ljava/lang/String;

    .line 71
    .local v3, "imageURL":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Lru/cn/tv/player/controller/MediaTooltipView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v6

    .line 72
    invoke-virtual {v6, v3}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v6

    const v7, 0x7f0802dc

    .line 73
    invoke-virtual {v6, v7}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v6

    .line 74
    invoke-virtual {v6}, Lcom/squareup/picasso/RequestCreator;->fit()Lcom/squareup/picasso/RequestCreator;

    move-result-object v6

    iget-object v7, p0, Lru/cn/tv/player/controller/MediaTooltipView;->telecastImage:Landroid/widget/ImageView;

    invoke-virtual {v6, v7}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 76
    iget-object v6, p0, Lru/cn/tv/player/controller/MediaTooltipView;->telecastTitle:Landroid/widget/TextView;

    iget-object v7, p2, Lru/cn/api/tv/replies/Telecast;->title:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v6, p2, Lru/cn/api/tv/replies/Telecast;->date:Lru/cn/api/tv/replies/DateTime;

    invoke-virtual {v6}, Lru/cn/api/tv/replies/DateTime;->toSeconds()J

    move-result-wide v4

    .line 79
    .local v4, "time":J
    new-instance v1, Ljava/util/Date;

    const-wide/16 v6, 0x3e8

    mul-long/2addr v6, v4

    invoke-direct {v1, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 80
    .local v1, "d":Ljava/util/Date;
    invoke-static {v1}, Lru/cn/utils/Utils;->getCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v0

    .line 82
    .local v0, "calendar":Ljava/util/Calendar;
    iget-object v6, p0, Lru/cn/tv/player/controller/MediaTooltipView;->telecastDateTime:Landroid/widget/TextView;

    const-string v7, "dd MMMM, HH:mm"

    invoke-static {v0, v7}, Lru/cn/utils/Utils;->format(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    .end local v0    # "calendar":Ljava/util/Calendar;
    .end local v1    # "d":Ljava/util/Date;
    .end local v2    # "image":Lru/cn/api/tv/replies/TelecastImage;
    .end local v3    # "imageURL":Ljava/lang/String;
    .end local v4    # "time":J
    :cond_0
    return-void

    .line 70
    .restart local v2    # "image":Lru/cn/api/tv/replies/TelecastImage;
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 54
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 56
    const v1, 0x7f090142

    invoke-virtual {p0, v1}, Lru/cn/tv/player/controller/MediaTooltipView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 57
    .local v0, "wrapper":Landroid/view/View;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 58
    const v1, 0x7f090143

    invoke-virtual {p0, v1}, Lru/cn/tv/player/controller/MediaTooltipView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lru/cn/tv/player/controller/MediaTooltipView;->wrapperTitle:Landroid/widget/TextView;

    .line 59
    const v1, 0x7f090140

    invoke-virtual {p0, v1}, Lru/cn/tv/player/controller/MediaTooltipView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lru/cn/tv/player/controller/MediaTooltipView;->telecastImage:Landroid/widget/ImageView;

    .line 60
    const v1, 0x7f090141

    invoke-virtual {p0, v1}, Lru/cn/tv/player/controller/MediaTooltipView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lru/cn/tv/player/controller/MediaTooltipView;->telecastTitle:Landroid/widget/TextView;

    .line 61
    const v1, 0x7f09013f

    invoke-virtual {p0, v1}, Lru/cn/tv/player/controller/MediaTooltipView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lru/cn/tv/player/controller/MediaTooltipView;->telecastDateTime:Landroid/widget/TextView;

    .line 62
    return-void
.end method
