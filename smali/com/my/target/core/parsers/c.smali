.class public final Lcom/my/target/core/parsers/c;
.super Ljava/lang/Object;
.source "InterstitialAdBannerParser.java"


# instance fields
.field private final A:Lcom/my/target/be;

.field private final adConfig:Lcom/my/target/b;

.field private final context:Landroid/content/Context;

.field private final e:Lcom/my/target/dv;

.field private final z:Lcom/my/target/ae;


# direct methods
.method private constructor <init>(Lcom/my/target/dv;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/my/target/core/parsers/c;->e:Lcom/my/target/dv;

    .line 51
    iput-object p2, p0, Lcom/my/target/core/parsers/c;->z:Lcom/my/target/ae;

    .line 52
    iput-object p3, p0, Lcom/my/target/core/parsers/c;->adConfig:Lcom/my/target/b;

    .line 53
    iput-object p4, p0, Lcom/my/target/core/parsers/c;->context:Landroid/content/Context;

    .line 54
    invoke-static {p1, p2, p3, p4}, Lcom/my/target/be;->a(Lcom/my/target/ak;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/be;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/parsers/c;->A:Lcom/my/target/be;

    .line 55
    return-void
.end method

.method public static a(Lcom/my/target/dv;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/c;
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/my/target/core/parsers/c;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/my/target/core/parsers/c;-><init>(Lcom/my/target/dv;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 205
    invoke-static {p1}, Lcom/my/target/az;->y(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/my/target/az;->z(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/parsers/c;->adConfig:Lcom/my/target/b;

    .line 206
    invoke-virtual {v1}, Lcom/my/target/b;->getSlotId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/az;->h(I)Lcom/my/target/az;

    move-result-object v0

    .line 207
    invoke-virtual {v0, p3}, Lcom/my/target/az;->B(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/parsers/c;->z:Lcom/my/target/ae;

    .line 208
    invoke-virtual {v1}, Lcom/my/target/ae;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/az;->A(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/parsers/c;->context:Landroid/content/Context;

    .line 209
    invoke-virtual {v0, v1}, Lcom/my/target/az;->e(Landroid/content/Context;)V

    .line 210
    return-void
.end method

.method private a(Lorg/json/JSONObject;Lcom/my/target/core/models/banners/d;)V
    .locals 4

    .prologue
    .line 188
    iget-object v0, p0, Lcom/my/target/core/parsers/c;->A:Lcom/my/target/be;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/my/target/be;->a(Lorg/json/JSONObject;Lcom/my/target/ah;Lcom/my/target/af;)V

    .line 189
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/d;->setAllowClose(Z)V

    .line 190
    const-string v0, "allowCloseDelay"

    iget-object v1, p0, Lcom/my/target/core/parsers/c;->e:Lcom/my/target/dv;

    invoke-virtual {v1}, Lcom/my/target/dv;->getAllowCloseDelay()F

    move-result v1

    float-to-double v2, v1

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/d;->setAllowCloseDelay(F)V

    .line 191
    const-string v0, "close_icon_hd"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 192
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 194
    invoke-static {v0}, Lcom/my/target/common/models/ImageData;->newImageData(Ljava/lang/String;)Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 195
    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/d;->setCloseIcon(Lcom/my/target/common/models/ImageData;)V

    .line 201
    :goto_0
    return-void

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/parsers/c;->e:Lcom/my/target/dv;

    invoke-virtual {v0}, Lcom/my/target/dv;->getCloseIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/d;->setCloseIcon(Lcom/my/target/common/models/ImageData;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;Lcom/my/target/core/models/banners/f;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 142
    const-string v0, "source"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 143
    if-nez v0, :cond_0

    .line 145
    const-string v0, "Required field"

    const-string v1, "Banner with type \'html\' has no source field"

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/f;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/my/target/core/parsers/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const/4 v0, 0x0

    .line 161
    :goto_0
    return v0

    .line 148
    :cond_0
    invoke-static {v0}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 149
    invoke-direct {p0, p1, p2}, Lcom/my/target/core/parsers/c;->a(Lorg/json/JSONObject;Lcom/my/target/core/models/banners/d;)V

    .line 150
    if-eqz p3, :cond_1

    .line 152
    invoke-static {p3, v1}, Lcom/my/target/be;->g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 153
    if-eqz v0, :cond_1

    .line 156
    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/f;->setMraidSource(Ljava/lang/String;)V

    .line 157
    const-string v1, "mraid"

    invoke-virtual {p2, v1}, Lcom/my/target/core/models/banners/f;->setType(Ljava/lang/String;)V

    .line 160
    :goto_1
    iget-object v1, p0, Lcom/my/target/core/parsers/c;->A:Lcom/my/target/be;

    invoke-virtual {v1, v0, p1}, Lcom/my/target/be;->a(Ljava/lang/String;Lorg/json/JSONObject;)Z

    move-result v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(Lorg/json/JSONObject;Lcom/my/target/core/models/banners/g;)Z
    .locals 3

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/my/target/core/parsers/c;->a(Lorg/json/JSONObject;Lcom/my/target/core/models/banners/d;)V

    .line 61
    iget-object v0, p0, Lcom/my/target/core/parsers/c;->z:Lcom/my/target/ae;

    iget-object v1, p0, Lcom/my/target/core/parsers/c;->adConfig:Lcom/my/target/b;

    iget-object v2, p0, Lcom/my/target/core/parsers/c;->context:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/my/target/core/parsers/d;->a(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/d;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/my/target/core/parsers/d;->b(Lorg/json/JSONObject;Lcom/my/target/core/models/banners/g;)Z

    move-result v0

    return v0
.end method

.method public final a(Lorg/json/JSONObject;Lcom/my/target/core/models/banners/h;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-direct {p0, p1, p2}, Lcom/my/target/core/parsers/c;->a(Lorg/json/JSONObject;Lcom/my/target/core/models/banners/d;)V

    .line 68
    const-string v0, "footerColor"

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/h;->getFooterColor()I

    move-result v2

    invoke-static {p1, v0, v2}, Lcom/my/target/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/h;->setFooterColor(I)V

    .line 69
    const-string v0, "ctaButtonColor"

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/h;->getCtaButtonColor()I

    move-result v2

    invoke-static {p1, v0, v2}, Lcom/my/target/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/h;->setCtaButtonColor(I)V

    .line 70
    const-string v0, "ctaButtonTouchColor"

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/h;->getCtaButtonTouchColor()I

    move-result v2

    invoke-static {p1, v0, v2}, Lcom/my/target/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/h;->setCtaButtonTouchColor(I)V

    .line 71
    const-string v0, "ctaButtonTextColor"

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/h;->getCtaButtonTextColor()I

    move-result v2

    invoke-static {p1, v0, v2}, Lcom/my/target/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/h;->setCtaButtonTextColor(I)V

    .line 72
    const-string v0, "style"

    iget-object v2, p0, Lcom/my/target/core/parsers/c;->e:Lcom/my/target/dv;

    invoke-virtual {v2}, Lcom/my/target/dv;->getStyle()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/h;->setStyle(I)V

    .line 74
    const-string v0, "play_icon_hd"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 77
    invoke-static {v0}, Lcom/my/target/common/models/ImageData;->newImageData(Ljava/lang/String;)Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 78
    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/h;->setPlayIcon(Lcom/my/target/common/models/ImageData;)V

    .line 85
    :goto_0
    const-string v0, "store_icon_hd"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 86
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 88
    invoke-static {v0}, Lcom/my/target/common/models/ImageData;->newImageData(Ljava/lang/String;)Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 89
    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/h;->setStoreIcon(Lcom/my/target/common/models/ImageData;)V

    .line 96
    :goto_1
    const-string v0, "cards"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 97
    if-eqz v3, :cond_5

    .line 99
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    .line 100
    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v4, :cond_5

    .line 102
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 103
    if-eqz v5, :cond_0

    .line 1167
    invoke-static {p2}, Lcom/my/target/core/models/banners/e;->newCard(Lcom/my/target/core/models/banners/d;)Lcom/my/target/core/models/banners/e;

    move-result-object v0

    .line 1168
    iget-object v6, p0, Lcom/my/target/core/parsers/c;->A:Lcom/my/target/be;

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/d;->getClickArea()Lcom/my/target/af;

    move-result-object v7

    invoke-virtual {v6, v5, v0, v7}, Lcom/my/target/be;->a(Lorg/json/JSONObject;Lcom/my/target/ah;Lcom/my/target/af;)V

    .line 1170
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/e;->getTrackingLink()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1172
    const-string v0, "Required field"

    const-string v5, "no tracking link in interstitialAdCard"

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/d;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v0, v5, v6}, Lcom/my/target/core/parsers/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 106
    :goto_3
    if-eqz v0, :cond_0

    .line 108
    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/h;->addInterstitialAdCard(Lcom/my/target/core/models/banners/e;)V

    .line 100
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 82
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/parsers/c;->e:Lcom/my/target/dv;

    invoke-virtual {v0}, Lcom/my/target/dv;->getPlayIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/h;->setPlayIcon(Lcom/my/target/common/models/ImageData;)V

    goto :goto_0

    .line 93
    :cond_2
    iget-object v0, p0, Lcom/my/target/core/parsers/c;->e:Lcom/my/target/dv;

    invoke-virtual {v0}, Lcom/my/target/dv;->getStoreIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/h;->setStoreIcon(Lcom/my/target/common/models/ImageData;)V

    goto :goto_1

    .line 1176
    :cond_3
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/e;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v6

    if-nez v6, :cond_4

    .line 1178
    const-string v0, "Required field"

    const-string v5, "no image in interstitialAdCard"

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/d;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v0, v5, v6}, Lcom/my/target/core/parsers/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 1179
    goto :goto_3

    .line 1182
    :cond_4
    const-string v6, "cardID"

    invoke-virtual {v0}, Lcom/my/target/core/models/banners/e;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/my/target/core/models/banners/e;->setId(Ljava/lang/String;)V

    goto :goto_3

    .line 114
    :cond_5
    invoke-virtual {p2}, Lcom/my/target/core/models/banners/h;->getInterstitialAdCards()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 116
    const-string v0, "video"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 117
    if-eqz v0, :cond_6

    .line 119
    invoke-static {}, Lcom/my/target/aj;->newVideoBanner()Lcom/my/target/aj;

    move-result-object v1

    .line 120
    invoke-virtual {p2}, Lcom/my/target/core/models/banners/h;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/my/target/aj;->setId(Ljava/lang/String;)V

    .line 121
    iget-object v2, p0, Lcom/my/target/core/parsers/c;->e:Lcom/my/target/dv;

    iget-object v3, p0, Lcom/my/target/core/parsers/c;->z:Lcom/my/target/ae;

    iget-object v4, p0, Lcom/my/target/core/parsers/c;->adConfig:Lcom/my/target/b;

    iget-object v5, p0, Lcom/my/target/core/parsers/c;->context:Landroid/content/Context;

    invoke-static {v2, v3, v4, v5}, Lcom/my/target/bg;->b(Lcom/my/target/ak;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/bg;

    move-result-object v2

    .line 122
    iget-object v3, p0, Lcom/my/target/core/parsers/c;->e:Lcom/my/target/dv;

    invoke-virtual {v3}, Lcom/my/target/dv;->getVideoSettings()Lcom/my/target/am;

    move-result-object v3

    invoke-virtual {v2, v0, v1, v3}, Lcom/my/target/bg;->a(Lorg/json/JSONObject;Lcom/my/target/aj;Lcom/my/target/am;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 124
    invoke-virtual {p2, v1}, Lcom/my/target/core/models/banners/h;->setVideoBanner(Lcom/my/target/aj;)V

    .line 125
    const-string v2, "allowClose"

    iget-object v3, p0, Lcom/my/target/core/parsers/c;->e:Lcom/my/target/dv;

    invoke-virtual {v3}, Lcom/my/target/dv;->getVideoSettings()Lcom/my/target/am;

    move-result-object v3

    invoke-virtual {v3}, Lcom/my/target/am;->isAllowClose()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {p2, v2}, Lcom/my/target/core/models/banners/h;->setAllowClose(Z)V

    .line 126
    invoke-virtual {v1}, Lcom/my/target/aj;->isAutoPlay()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 128
    const-string v1, "allowCloseDelay"

    iget-object v2, p0, Lcom/my/target/core/parsers/c;->e:Lcom/my/target/dv;

    invoke-virtual {v2}, Lcom/my/target/dv;->getVideoSettings()Lcom/my/target/am;

    move-result-object v2

    invoke-virtual {v2}, Lcom/my/target/am;->getAllowCloseDelay()F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/h;->setAllowCloseDelay(F)V

    .line 134
    :cond_6
    const/4 v0, 0x1

    return v0
.end method
