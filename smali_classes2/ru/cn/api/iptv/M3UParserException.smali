.class public final Lru/cn/api/iptv/M3UParserException;
.super Lru/cn/api/BaseAPI$ParseException;
.source "M3UParserException.java"


# instance fields
.field public final code:I

.field public final line:I


# direct methods
.method constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "code"    # I
    .param p3, "line"    # I

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lru/cn/api/BaseAPI$ParseException;-><init>(Ljava/lang/String;)V

    .line 17
    iput p2, p0, Lru/cn/api/iptv/M3UParserException;->code:I

    .line 18
    iput p3, p0, Lru/cn/api/iptv/M3UParserException;->line:I

    .line 19
    return-void
.end method
