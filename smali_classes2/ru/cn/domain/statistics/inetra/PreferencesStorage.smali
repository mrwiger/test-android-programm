.class final Lru/cn/domain/statistics/inetra/PreferencesStorage;
.super Ljava/lang/Object;
.source "PreferencesStorage.java"

# interfaces
.implements Lru/cn/domain/statistics/inetra/EventsStorage;


# instance fields
.field private final events:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/domain/statistics/inetra/TrackingEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final preferences:Landroid/content/SharedPreferences;

.field private sequenceNumber:J


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "baseUrl"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lru/cn/domain/statistics/inetra/PreferencesStorage;->sequenceNumber:J

    .line 23
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v5}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lru/cn/domain/statistics/inetra/PreferencesStorage;->events:Ljava/util/List;

    .line 26
    const-string v5, "tracking.events"

    const/4 v6, 0x0

    invoke-virtual {p1, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    iput-object v5, p0, Lru/cn/domain/statistics/inetra/PreferencesStorage;->preferences:Landroid/content/SharedPreferences;

    .line 27
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 29
    .local v3, "restoredEvents":Ljava/util/List;, "Ljava/util/List<Lru/cn/domain/statistics/inetra/TrackingEvent;>;"
    iget-object v5, p0, Lru/cn/domain/statistics/inetra/PreferencesStorage;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v2

    .line 30
    .local v2, "prefsEntries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 31
    .local v1, "key":Ljava/lang/String;
    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 33
    .local v4, "value":Ljava/lang/String;
    new-instance v6, Lcom/google/gson/Gson;

    invoke-direct {v6}, Lcom/google/gson/Gson;-><init>()V

    const-class v7, Lru/cn/domain/statistics/inetra/TrackingEvent;

    invoke-virtual {v6, v4, v7}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/domain/statistics/inetra/TrackingEvent;

    .line 34
    .local v0, "event":Lru/cn/domain/statistics/inetra/TrackingEvent;
    if-eqz v0, :cond_0

    .line 35
    invoke-virtual {v0, p2}, Lru/cn/domain/statistics/inetra/TrackingEvent;->setBaseURL(Ljava/lang/String;)V

    .line 36
    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iput-wide v6, v0, Lru/cn/domain/statistics/inetra/TrackingEvent;->id:J

    .line 38
    iget-wide v6, v0, Lru/cn/domain/statistics/inetra/TrackingEvent;->id:J

    iget-wide v8, p0, Lru/cn/domain/statistics/inetra/PreferencesStorage;->sequenceNumber:J

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    iput-wide v6, p0, Lru/cn/domain/statistics/inetra/PreferencesStorage;->sequenceNumber:J

    .line 40
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 44
    .end local v0    # "event":Lru/cn/domain/statistics/inetra/TrackingEvent;
    .end local v1    # "key":Ljava/lang/String;
    .end local v4    # "value":Ljava/lang/String;
    :cond_1
    iget-object v5, p0, Lru/cn/domain/statistics/inetra/PreferencesStorage;->events:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 45
    return-void
.end method

.method private declared-synchronized nextId()J
    .locals 4

    .prologue
    .line 48
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lru/cn/domain/statistics/inetra/PreferencesStorage;->sequenceNumber:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lru/cn/domain/statistics/inetra/PreferencesStorage;->sequenceNumber:J

    .line 49
    iget-wide v0, p0, Lru/cn/domain/statistics/inetra/PreferencesStorage;->sequenceNumber:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    .line 48
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public addEvent(Lru/cn/domain/statistics/inetra/TrackingEvent;)V
    .locals 8
    .param p1, "event"    # Lru/cn/domain/statistics/inetra/TrackingEvent;

    .prologue
    .line 55
    iget-wide v4, p1, Lru/cn/domain/statistics/inetra/TrackingEvent;->id:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 56
    invoke-direct {p0}, Lru/cn/domain/statistics/inetra/PreferencesStorage;->nextId()J

    move-result-wide v4

    iput-wide v4, p1, Lru/cn/domain/statistics/inetra/TrackingEvent;->id:J

    .line 59
    :cond_0
    iget-wide v4, p1, Lru/cn/domain/statistics/inetra/TrackingEvent;->id:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 60
    .local v2, "eventId":Ljava/lang/String;
    iget-object v3, p0, Lru/cn/domain/statistics/inetra/PreferencesStorage;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v3, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 61
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "TrackingAPI:Trying to add event "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 62
    invoke-virtual {p1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->toJson()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " while there already has event "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lru/cn/domain/statistics/inetra/PreferencesStorage;->preferences:Landroid/content/SharedPreferences;

    const-string v5, ""

    .line 63
    invoke-interface {v4, v2, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 65
    .local v0, "collisionException":Ljava/lang/Exception;
    invoke-static {v0}, Lru/cn/utils/Logger;->logException(Ljava/lang/Throwable;)V

    .line 66
    const-string v3, "TrackingStorage"

    const-string v4, "collision in tracking events"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 69
    .end local v0    # "collisionException":Ljava/lang/Exception;
    :cond_1
    iget-object v3, p0, Lru/cn/domain/statistics/inetra/PreferencesStorage;->events:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    iget-object v3, p0, Lru/cn/domain/statistics/inetra/PreferencesStorage;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 72
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-virtual {p1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->toJson()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 73
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 74
    return-void
.end method

.method public allEvents()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lru/cn/domain/statistics/inetra/TrackingEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lru/cn/domain/statistics/inetra/PreferencesStorage;->events:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public contains(Lru/cn/domain/statistics/inetra/TrackingEvent;)Z
    .locals 1
    .param p1, "event"    # Lru/cn/domain/statistics/inetra/TrackingEvent;

    .prologue
    .line 78
    iget-object v0, p0, Lru/cn/domain/statistics/inetra/PreferencesStorage;->events:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeEvent(Lru/cn/domain/statistics/inetra/TrackingEvent;)V
    .locals 4
    .param p1, "event"    # Lru/cn/domain/statistics/inetra/TrackingEvent;

    .prologue
    .line 83
    iget-object v1, p0, Lru/cn/domain/statistics/inetra/PreferencesStorage;->events:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 85
    iget-object v1, p0, Lru/cn/domain/statistics/inetra/PreferencesStorage;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 86
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    iget-wide v2, p1, Lru/cn/domain/statistics/inetra/TrackingEvent;->id:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 87
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 88
    return-void
.end method
