.class public final Lcom/my/target/core/controllers/c;
.super Ljava/lang/Object;
.source "InstreamAdAudioController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/core/controllers/c$c;,
        Lcom/my/target/core/controllers/c$a;,
        Lcom/my/target/core/controllers/c$b;
    }
.end annotation


# instance fields
.field private final e:Lcom/my/target/core/controllers/c$a;

.field private final f:Lcom/my/target/ck;

.field private final g:Lcom/my/target/core/controllers/c$c;

.field private final h:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/my/target/ap;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcom/my/target/core/controllers/c$b;

.field private j:Lcom/my/target/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/my/target/aj",
            "<",
            "Lcom/my/target/common/models/AudioData;",
            ">;"
        }
    .end annotation
.end field

.field private k:I

.field private l:F

.field private m:I

.field private n:Z

.field private o:I

.field private player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

.field private volume:F


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/my/target/core/controllers/c;->volume:F

    .line 53
    iput v1, p0, Lcom/my/target/core/controllers/c;->o:I

    .line 57
    new-instance v0, Lcom/my/target/core/controllers/c$a;

    invoke-direct {v0, p0, v1}, Lcom/my/target/core/controllers/c$a;-><init>(Lcom/my/target/core/controllers/c;B)V

    iput-object v0, p0, Lcom/my/target/core/controllers/c;->e:Lcom/my/target/core/controllers/c$a;

    .line 58
    const/16 v0, 0xc8

    invoke-static {v0}, Lcom/my/target/ck;->k(I)Lcom/my/target/ck;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/controllers/c;->f:Lcom/my/target/ck;

    .line 59
    new-instance v0, Lcom/my/target/core/controllers/c$c;

    invoke-direct {v0, p0, v1}, Lcom/my/target/core/controllers/c$c;-><init>(Lcom/my/target/core/controllers/c;B)V

    iput-object v0, p0, Lcom/my/target/core/controllers/c;->g:Lcom/my/target/core/controllers/c$c;

    .line 60
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/my/target/core/controllers/c;->h:Ljava/util/Stack;

    .line 61
    return-void
.end method

.method static synthetic a(Lcom/my/target/core/controllers/c;I)I
    .locals 0

    .prologue
    .line 28
    iput p1, p0, Lcom/my/target/core/controllers/c;->o:I

    return p1
.end method

.method private a(F)V
    .locals 2

    .prologue
    .line 292
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 293
    :goto_0
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/controllers/c;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/ap;

    invoke-virtual {v0}, Lcom/my/target/ap;->Z()F

    move-result v0

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 297
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    if-eqz v0, :cond_1

    .line 299
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAudioAdPlayer;->getCurrentContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 301
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/my/target/core/controllers/c;F)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 28
    .line 1251
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/controllers/c;->i:Lcom/my/target/core/controllers/c$b;

    if-eqz v0, :cond_0

    .line 1253
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->i:Lcom/my/target/core/controllers/c$b;

    iget-object v1, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    invoke-interface {v0, v1}, Lcom/my/target/core/controllers/c$b;->onBannerStarted(Lcom/my/target/aj;)V

    .line 1255
    :cond_0
    invoke-virtual {p0}, Lcom/my/target/core/controllers/c;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1256
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    if-eqz v1, :cond_1

    .line 1258
    iget-object v1, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    invoke-virtual {v1}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v1

    const-string v2, "playbackStarted"

    invoke-virtual {v1, v2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 1261
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->i:Lcom/my/target/core/controllers/c$b;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    if-eqz v0, :cond_2

    .line 1263
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->i:Lcom/my/target/core/controllers/c$b;

    iget-object v1, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    invoke-interface {v0, v3, p1, v1}, Lcom/my/target/core/controllers/c$b;->onBannerProgressChanged(FFLcom/my/target/aj;)V

    .line 1265
    :cond_2
    invoke-direct {p0, v3}, Lcom/my/target/core/controllers/c;->a(F)V

    .line 1266
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/core/controllers/c;->n:Z

    .line 28
    return-void
.end method

.method static synthetic a(Lcom/my/target/core/controllers/c;)Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/my/target/core/controllers/c;->n:Z

    return v0
.end method

.method static synthetic b(Lcom/my/target/core/controllers/c;)Lcom/my/target/instreamads/InstreamAudioAdPlayer;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    return-object v0
.end method

.method static synthetic b(Lcom/my/target/core/controllers/c;F)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/my/target/core/controllers/c;->a(F)V

    return-void
.end method

.method static synthetic c(Lcom/my/target/core/controllers/c;F)F
    .locals 0

    .prologue
    .line 28
    iput p1, p0, Lcom/my/target/core/controllers/c;->volume:F

    return p1
.end method

.method static synthetic c(Lcom/my/target/core/controllers/c;)Lcom/my/target/core/controllers/c$c;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->g:Lcom/my/target/core/controllers/c$c;

    return-object v0
.end method

.method static synthetic d(Lcom/my/target/core/controllers/c;)Lcom/my/target/ck;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->f:Lcom/my/target/ck;

    return-object v0
.end method

.method static synthetic e(Lcom/my/target/core/controllers/c;)Lcom/my/target/aj;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    return-object v0
.end method

.method static synthetic f(Lcom/my/target/core/controllers/c;)I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/my/target/core/controllers/c;->o:I

    return v0
.end method

.method public static f()Lcom/my/target/core/controllers/c;
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/my/target/core/controllers/c;

    invoke-direct {v0}, Lcom/my/target/core/controllers/c;-><init>()V

    return-object v0
.end method

.method static synthetic g(Lcom/my/target/core/controllers/c;)Lcom/my/target/core/controllers/c$b;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->i:Lcom/my/target/core/controllers/c$b;

    return-object v0
.end method

.method static synthetic h(Lcom/my/target/core/controllers/c;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 2174
    .line 2178
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    if-eqz v0, :cond_8

    .line 2180
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    invoke-virtual {v0}, Lcom/my/target/aj;->getDuration()F

    move-result v0

    .line 2182
    :goto_0
    iget-object v2, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    if-nez v2, :cond_1

    .line 2184
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->f:Lcom/my/target/ck;

    iget-object v1, p0, Lcom/my/target/core/controllers/c;->g:Lcom/my/target/core/controllers/c$c;

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    .line 2185
    :cond_0
    :goto_1
    return-void

    .line 2188
    :cond_1
    iget v2, p0, Lcom/my/target/core/controllers/c;->o:I

    if-ne v2, v6, :cond_7

    iget-object v2, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    if-eqz v2, :cond_7

    .line 2190
    iget-object v2, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    invoke-interface {v2}, Lcom/my/target/instreamads/InstreamAudioAdPlayer;->getAdAudioDuration()F

    move-result v2

    .line 2191
    iget-object v3, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    invoke-interface {v3}, Lcom/my/target/instreamads/InstreamAudioAdPlayer;->getAdAudioPosition()F

    move-result v3

    .line 2192
    sub-float v4, v0, v3

    .line 2195
    :goto_2
    iget v5, p0, Lcom/my/target/core/controllers/c;->o:I

    if-ne v5, v6, :cond_6

    iget v5, p0, Lcom/my/target/core/controllers/c;->l:F

    cmpl-float v5, v5, v3

    if-eqz v5, :cond_6

    cmpl-float v2, v2, v1

    if-lez v2, :cond_6

    .line 2212
    const/4 v2, 0x0

    iput v2, p0, Lcom/my/target/core/controllers/c;->k:I

    .line 2213
    iput v3, p0, Lcom/my/target/core/controllers/c;->l:F

    .line 2215
    cmpg-float v2, v3, v0

    if-gez v2, :cond_3

    .line 2217
    invoke-direct {p0, v3}, Lcom/my/target/core/controllers/c;->a(F)V

    .line 2218
    iget-object v1, p0, Lcom/my/target/core/controllers/c;->i:Lcom/my/target/core/controllers/c$b;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    if-eqz v1, :cond_2

    .line 2220
    iget-object v1, p0, Lcom/my/target/core/controllers/c;->i:Lcom/my/target/core/controllers/c$b;

    iget-object v2, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    invoke-interface {v1, v4, v0, v2}, Lcom/my/target/core/controllers/c$b;->onBannerProgressChanged(FFLcom/my/target/aj;)V

    .line 2204
    :cond_2
    :goto_3
    iget v0, p0, Lcom/my/target/core/controllers/c;->k:I

    iget v1, p0, Lcom/my/target/core/controllers/c;->m:I

    mul-int/lit16 v1, v1, 0x3e8

    div-int/lit16 v1, v1, 0xc8

    if-lt v0, v1, :cond_0

    .line 3241
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "video freeze more then "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/my/target/core/controllers/c;->m:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " seconds, stopping"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 3242
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->f:Lcom/my/target/ck;

    iget-object v1, p0, Lcom/my/target/core/controllers/c;->g:Lcom/my/target/core/controllers/c$c;

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    .line 3243
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->i:Lcom/my/target/core/controllers/c$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    if-eqz v0, :cond_0

    .line 3245
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->i:Lcom/my/target/core/controllers/c$b;

    const-string v1, "Timeout"

    iget-object v2, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    invoke-interface {v0, v1, v2}, Lcom/my/target/core/controllers/c$b;->onBannerError(Ljava/lang/String;Lcom/my/target/aj;)V

    goto :goto_1

    .line 2231
    :cond_3
    invoke-direct {p0, v0}, Lcom/my/target/core/controllers/c;->a(F)V

    .line 2232
    iget-object v2, p0, Lcom/my/target/core/controllers/c;->i:Lcom/my/target/core/controllers/c$b;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    if-eqz v2, :cond_4

    .line 2234
    iget-object v2, p0, Lcom/my/target/core/controllers/c;->i:Lcom/my/target/core/controllers/c$b;

    iget-object v3, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    invoke-interface {v2, v1, v0, v3}, Lcom/my/target/core/controllers/c$b;->onBannerProgressChanged(FFLcom/my/target/aj;)V

    .line 2271
    :cond_4
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->f:Lcom/my/target/ck;

    iget-object v1, p0, Lcom/my/target/core/controllers/c;->g:Lcom/my/target/core/controllers/c$c;

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    .line 2273
    iget v0, p0, Lcom/my/target/core/controllers/c;->o:I

    if-eq v0, v7, :cond_2

    .line 2275
    iput v7, p0, Lcom/my/target/core/controllers/c;->o:I

    .line 2276
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    if-eqz v0, :cond_5

    .line 2278
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAudioAdPlayer;->stopAdAudio()V

    .line 2280
    :cond_5
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/my/target/core/controllers/c;->i:Lcom/my/target/core/controllers/c$b;

    if-eqz v0, :cond_2

    .line 2282
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    .line 2283
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    .line 2284
    iget-object v1, p0, Lcom/my/target/core/controllers/c;->i:Lcom/my/target/core/controllers/c$b;

    invoke-interface {v1, v0}, Lcom/my/target/core/controllers/c$b;->onBannerCompleted(Lcom/my/target/aj;)V

    goto :goto_3

    .line 2201
    :cond_6
    iget v0, p0, Lcom/my/target/core/controllers/c;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/my/target/core/controllers/c;->k:I

    goto :goto_3

    :cond_7
    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_2

    :cond_8
    move v0, v1

    goto/16 :goto_0
.end method

.method static synthetic i(Lcom/my/target/core/controllers/c;)Lcom/my/target/aj;
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/my/target/core/controllers/c$b;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/my/target/core/controllers/c;->i:Lcom/my/target/core/controllers/c$b;

    .line 89
    return-void
.end method

.method public final destroy()V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAudioAdPlayer;->destroy()V

    .line 159
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    .line 160
    return-void
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAudioAdPlayer;->getCurrentContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0
.end method

.method public final getPlayer()Lcom/my/target/instreamads/InstreamAudioAdPlayer;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    return-object v0
.end method

.method public final getVolume()F
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lcom/my/target/core/controllers/c;->volume:F

    return v0
.end method

.method public final pause()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAudioAdPlayer;->pauseAdAudio()V

    .line 122
    :cond_0
    return-void
.end method

.method public final play(Lcom/my/target/aj;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/aj",
            "<",
            "Lcom/my/target/common/models/AudioData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 98
    .local p1, "banner":Lcom/my/target/aj;, "Lcom/my/target/aj<Lcom/my/target/common/models/AudioData;>;"
    iput-object p1, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/core/controllers/c;->n:Z

    .line 100
    invoke-virtual {p1}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/c;->h:Ljava/util/Stack;

    invoke-virtual {v0, v1}, Lcom/my/target/ar;->a(Ljava/util/Stack;)V

    .line 102
    invoke-virtual {p1}, Lcom/my/target/aj;->getMediaData()Lcom/my/target/ag;

    move-result-object v0

    check-cast v0, Lcom/my/target/common/models/AudioData;

    .line 103
    if-nez v0, :cond_1

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 108
    :cond_1
    invoke-virtual {v0}, Lcom/my/target/common/models/AudioData;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 109
    iget-object v1, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    if-eqz v1, :cond_0

    .line 111
    iget-object v1, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    iget v2, p0, Lcom/my/target/core/controllers/c;->volume:F

    invoke-interface {v1, v2}, Lcom/my/target/instreamads/InstreamAudioAdPlayer;->setVolume(F)V

    .line 112
    iget-object v1, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    invoke-interface {v1, v0}, Lcom/my/target/instreamads/InstreamAudioAdPlayer;->playAdAudio(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public final resume()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAudioAdPlayer;->resumeAdAudio()V

    .line 130
    :cond_0
    return-void
.end method

.method public final setConnectionTimeout(I)V
    .locals 0
    .param p1, "seconds"    # I

    .prologue
    .line 164
    iput p1, p0, Lcom/my/target/core/controllers/c;->m:I

    .line 165
    return-void
.end method

.method public final setPlayer(Lcom/my/target/instreamads/InstreamAudioAdPlayer;)V
    .locals 1
    .param p1, "player"    # Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    .line 80
    if-eqz p1, :cond_0

    .line 82
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->e:Lcom/my/target/core/controllers/c$a;

    invoke-interface {p1, v0}, Lcom/my/target/instreamads/InstreamAudioAdPlayer;->setAdPlayerListener(Lcom/my/target/instreamads/InstreamAudioAdPlayer$AdPlayerListener;)V

    .line 84
    :cond_0
    return-void
.end method

.method public final setVolume(F)V
    .locals 1
    .param p1, "volume"    # F

    .prologue
    .line 65
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    invoke-interface {v0, p1}, Lcom/my/target/instreamads/InstreamAudioAdPlayer;->setVolume(F)V

    .line 69
    :cond_0
    iput p1, p0, Lcom/my/target/core/controllers/c;->volume:F

    .line 70
    return-void
.end method

.method public final stop()V
    .locals 3

    .prologue
    .line 134
    iget v0, p0, Lcom/my/target/core/controllers/c;->o:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 136
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/my/target/core/controllers/c;->i:Lcom/my/target/core/controllers/c$b;

    if-eqz v0, :cond_1

    .line 138
    invoke-virtual {p0}, Lcom/my/target/core/controllers/c;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 139
    if-eqz v0, :cond_0

    .line 141
    iget-object v1, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    invoke-virtual {v1}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v1

    const-string v2, "playbackStopped"

    invoke-virtual {v1, v2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->i:Lcom/my/target/core/controllers/c$b;

    iget-object v1, p0, Lcom/my/target/core/controllers/c;->j:Lcom/my/target/aj;

    invoke-interface {v0, v1}, Lcom/my/target/core/controllers/c$b;->onBannerStopped(Lcom/my/target/aj;)V

    .line 145
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/my/target/core/controllers/c;->o:I

    .line 147
    :cond_2
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    if-eqz v0, :cond_3

    .line 149
    iget-object v0, p0, Lcom/my/target/core/controllers/c;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAudioAdPlayer;->stopAdAudio()V

    .line 151
    :cond_3
    return-void
.end method
