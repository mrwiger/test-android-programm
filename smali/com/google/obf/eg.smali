.class public final Lcom/google/obf/eg;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/eg$a;
    }
.end annotation


# static fields
.field private static final a:Lcom/google/obf/gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/gd",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/Map",
            "<",
            "Lcom/google/obf/gd",
            "<*>;",
            "Lcom/google/obf/eg$a",
            "<*>;>;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/obf/gd",
            "<*>;",
            "Lcom/google/obf/ew",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/obf/ex;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/google/obf/ff;

.field private final f:Lcom/google/obf/fg;

.field private final g:Lcom/google/obf/ef;

.field private final h:Z

.field private final i:Z

.field private final j:Z

.field private final k:Z

.field private final l:Z

.field private final m:Lcom/google/obf/fr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 236
    new-instance v0, Lcom/google/obf/eg$1;

    invoke-direct {v0}, Lcom/google/obf/eg$1;-><init>()V

    sput-object v0, Lcom/google/obf/eg;->a:Lcom/google/obf/gd;

    return-void
.end method

.method public constructor <init>()V
    .locals 13

    .prologue
    const/4 v4, 0x0

    .line 1
    sget-object v1, Lcom/google/obf/fg;->a:Lcom/google/obf/fg;

    sget-object v2, Lcom/google/obf/ee;->a:Lcom/google/obf/ee;

    .line 2
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v3

    const/4 v7, 0x1

    sget-object v11, Lcom/google/obf/ev;->a:Lcom/google/obf/ev;

    .line 3
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v12

    move-object v0, p0

    move v5, v4

    move v6, v4

    move v8, v4

    move v9, v4

    move v10, v4

    .line 4
    invoke-direct/range {v0 .. v12}, Lcom/google/obf/eg;-><init>(Lcom/google/obf/fg;Lcom/google/obf/ef;Ljava/util/Map;ZZZZZZZLcom/google/obf/ev;Ljava/util/List;)V

    .line 5
    return-void
.end method

.method constructor <init>(Lcom/google/obf/fg;Lcom/google/obf/ef;Ljava/util/Map;ZZZZZZZLcom/google/obf/ev;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/fg;",
            "Lcom/google/obf/ef;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/obf/ei",
            "<*>;>;ZZZZZZZ",
            "Lcom/google/obf/ev;",
            "Ljava/util/List",
            "<",
            "Lcom/google/obf/ex;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    new-instance v1, Ljava/lang/ThreadLocal;

    invoke-direct {v1}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v1, p0, Lcom/google/obf/eg;->b:Ljava/lang/ThreadLocal;

    .line 8
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v1, p0, Lcom/google/obf/eg;->c:Ljava/util/Map;

    .line 9
    new-instance v1, Lcom/google/obf/ff;

    invoke-direct {v1, p3}, Lcom/google/obf/ff;-><init>(Ljava/util/Map;)V

    iput-object v1, p0, Lcom/google/obf/eg;->e:Lcom/google/obf/ff;

    .line 10
    iput-object p1, p0, Lcom/google/obf/eg;->f:Lcom/google/obf/fg;

    .line 11
    iput-object p2, p0, Lcom/google/obf/eg;->g:Lcom/google/obf/ef;

    .line 12
    iput-boolean p4, p0, Lcom/google/obf/eg;->h:Z

    .line 13
    iput-boolean p6, p0, Lcom/google/obf/eg;->j:Z

    .line 14
    iput-boolean p7, p0, Lcom/google/obf/eg;->i:Z

    .line 15
    iput-boolean p8, p0, Lcom/google/obf/eg;->k:Z

    .line 16
    iput-boolean p9, p0, Lcom/google/obf/eg;->l:Z

    .line 17
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 18
    sget-object v2, Lcom/google/obf/gb;->Y:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 19
    sget-object v2, Lcom/google/obf/fv;->a:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 20
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 21
    move-object/from16 v0, p12

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 22
    sget-object v2, Lcom/google/obf/gb;->D:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 23
    sget-object v2, Lcom/google/obf/gb;->m:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 24
    sget-object v2, Lcom/google/obf/gb;->g:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 25
    sget-object v2, Lcom/google/obf/gb;->i:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26
    sget-object v2, Lcom/google/obf/gb;->k:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 27
    invoke-static/range {p11 .. p11}, Lcom/google/obf/eg;->a(Lcom/google/obf/ev;)Lcom/google/obf/ew;

    move-result-object v2

    .line 28
    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const-class v4, Ljava/lang/Long;

    invoke-static {v3, v4, v2}, Lcom/google/obf/gb;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/obf/ew;)Lcom/google/obf/ex;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 29
    sget-object v3, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    const-class v4, Ljava/lang/Double;

    .line 30
    move/from16 v0, p10

    invoke-direct {p0, v0}, Lcom/google/obf/eg;->a(Z)Lcom/google/obf/ew;

    move-result-object v5

    .line 31
    invoke-static {v3, v4, v5}, Lcom/google/obf/gb;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/obf/ew;)Lcom/google/obf/ex;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32
    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    const-class v4, Ljava/lang/Float;

    .line 33
    move/from16 v0, p10

    invoke-direct {p0, v0}, Lcom/google/obf/eg;->b(Z)Lcom/google/obf/ew;

    move-result-object v5

    .line 34
    invoke-static {v3, v4, v5}, Lcom/google/obf/gb;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/obf/ew;)Lcom/google/obf/ex;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 35
    sget-object v3, Lcom/google/obf/gb;->x:Lcom/google/obf/ex;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    sget-object v3, Lcom/google/obf/gb;->o:Lcom/google/obf/ex;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37
    sget-object v3, Lcom/google/obf/gb;->q:Lcom/google/obf/ex;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    const-class v3, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {v2}, Lcom/google/obf/eg;->a(Lcom/google/obf/ew;)Lcom/google/obf/ew;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/obf/gb;->a(Ljava/lang/Class;Lcom/google/obf/ew;)Lcom/google/obf/ex;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    const-class v3, Ljava/util/concurrent/atomic/AtomicLongArray;

    invoke-static {v2}, Lcom/google/obf/eg;->b(Lcom/google/obf/ew;)Lcom/google/obf/ew;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/obf/gb;->a(Ljava/lang/Class;Lcom/google/obf/ew;)Lcom/google/obf/ex;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    sget-object v2, Lcom/google/obf/gb;->s:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    sget-object v2, Lcom/google/obf/gb;->z:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    sget-object v2, Lcom/google/obf/gb;->F:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    sget-object v2, Lcom/google/obf/gb;->H:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    const-class v2, Ljava/math/BigDecimal;

    sget-object v3, Lcom/google/obf/gb;->B:Lcom/google/obf/ew;

    invoke-static {v2, v3}, Lcom/google/obf/gb;->a(Ljava/lang/Class;Lcom/google/obf/ew;)Lcom/google/obf/ex;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    const-class v2, Ljava/math/BigInteger;

    sget-object v3, Lcom/google/obf/gb;->C:Lcom/google/obf/ew;

    invoke-static {v2, v3}, Lcom/google/obf/gb;->a(Ljava/lang/Class;Lcom/google/obf/ew;)Lcom/google/obf/ex;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    sget-object v2, Lcom/google/obf/gb;->J:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    sget-object v2, Lcom/google/obf/gb;->L:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    sget-object v2, Lcom/google/obf/gb;->P:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    sget-object v2, Lcom/google/obf/gb;->R:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    sget-object v2, Lcom/google/obf/gb;->W:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    sget-object v2, Lcom/google/obf/gb;->N:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    sget-object v2, Lcom/google/obf/gb;->d:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    sget-object v2, Lcom/google/obf/fq;->a:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    sget-object v2, Lcom/google/obf/gb;->U:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    sget-object v2, Lcom/google/obf/fy;->a:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    sget-object v2, Lcom/google/obf/fx;->a:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    sget-object v2, Lcom/google/obf/gb;->S:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    sget-object v2, Lcom/google/obf/fo;->a:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    sget-object v2, Lcom/google/obf/gb;->b:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    new-instance v2, Lcom/google/obf/fp;

    iget-object v3, p0, Lcom/google/obf/eg;->e:Lcom/google/obf/ff;

    invoke-direct {v2, v3}, Lcom/google/obf/fp;-><init>(Lcom/google/obf/ff;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    new-instance v2, Lcom/google/obf/fu;

    iget-object v3, p0, Lcom/google/obf/eg;->e:Lcom/google/obf/ff;

    invoke-direct {v2, v3, p5}, Lcom/google/obf/fu;-><init>(Lcom/google/obf/ff;Z)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    new-instance v2, Lcom/google/obf/fr;

    iget-object v3, p0, Lcom/google/obf/eg;->e:Lcom/google/obf/ff;

    invoke-direct {v2, v3}, Lcom/google/obf/fr;-><init>(Lcom/google/obf/ff;)V

    iput-object v2, p0, Lcom/google/obf/eg;->m:Lcom/google/obf/fr;

    .line 63
    iget-object v2, p0, Lcom/google/obf/eg;->m:Lcom/google/obf/fr;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    sget-object v2, Lcom/google/obf/gb;->Z:Lcom/google/obf/ex;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    new-instance v2, Lcom/google/obf/fw;

    iget-object v3, p0, Lcom/google/obf/eg;->e:Lcom/google/obf/ff;

    iget-object v4, p0, Lcom/google/obf/eg;->m:Lcom/google/obf/fr;

    invoke-direct {v2, v3, p2, p1, v4}, Lcom/google/obf/fw;-><init>(Lcom/google/obf/ff;Lcom/google/obf/ef;Lcom/google/obf/fg;Lcom/google/obf/fr;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/obf/eg;->d:Ljava/util/List;

    .line 67
    return-void
.end method

.method private static a(Lcom/google/obf/ev;)Lcom/google/obf/ew;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/ev;",
            ")",
            "Lcom/google/obf/ew",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    sget-object v0, Lcom/google/obf/ev;->a:Lcom/google/obf/ev;

    if-ne p0, v0, :cond_0

    .line 78
    sget-object v0, Lcom/google/obf/gb;->t:Lcom/google/obf/ew;

    .line 79
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/obf/eg$4;

    invoke-direct {v0}, Lcom/google/obf/eg$4;-><init>()V

    goto :goto_0
.end method

.method private static a(Lcom/google/obf/ew;)Lcom/google/obf/ew;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/ew",
            "<",
            "Ljava/lang/Number;",
            ">;)",
            "Lcom/google/obf/ew",
            "<",
            "Ljava/util/concurrent/atomic/AtomicLong;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    new-instance v0, Lcom/google/obf/eg$5;

    invoke-direct {v0, p0}, Lcom/google/obf/eg$5;-><init>(Lcom/google/obf/ew;)V

    .line 81
    invoke-virtual {v0}, Lcom/google/obf/eg$5;->nullSafe()Lcom/google/obf/ew;

    move-result-object v0

    return-object v0
.end method

.method private a(Z)Lcom/google/obf/ew;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/obf/ew",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    if-eqz p1, :cond_0

    .line 69
    sget-object v0, Lcom/google/obf/gb;->v:Lcom/google/obf/ew;

    .line 70
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/obf/eg$2;

    invoke-direct {v0, p0}, Lcom/google/obf/eg$2;-><init>(Lcom/google/obf/eg;)V

    goto :goto_0
.end method

.method static a(D)V
    .locals 4

    .prologue
    .line 74
    invoke-static {p0, p1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0, p1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid double value as per JSON specification. To override this"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " behavior, use GsonBuilder.serializeSpecialFloatingPointValues() method."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :cond_1
    return-void
.end method

.method private static a(Ljava/lang/Object;Lcom/google/obf/ge;)V
    .locals 2

    .prologue
    .line 202
    if-eqz p0, :cond_0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    move-result-object v0

    sget-object v1, Lcom/google/obf/gf;->j:Lcom/google/obf/gf;

    if-eq v0, v1, :cond_0

    .line 203
    new-instance v0, Lcom/google/obf/en;

    const-string v1, "JSON document was not fully consumed."

    invoke-direct {v0, v1}, Lcom/google/obf/en;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/google/obf/gh; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 205
    :catch_0
    move-exception v0

    .line 206
    new-instance v1, Lcom/google/obf/eu;

    invoke-direct {v1, v0}, Lcom/google/obf/eu;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 207
    :catch_1
    move-exception v0

    .line 208
    new-instance v1, Lcom/google/obf/en;

    invoke-direct {v1, v0}, Lcom/google/obf/en;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 209
    :cond_0
    return-void
.end method

.method private static b(Lcom/google/obf/ew;)Lcom/google/obf/ew;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/ew",
            "<",
            "Ljava/lang/Number;",
            ">;)",
            "Lcom/google/obf/ew",
            "<",
            "Ljava/util/concurrent/atomic/AtomicLongArray;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    new-instance v0, Lcom/google/obf/eg$6;

    invoke-direct {v0, p0}, Lcom/google/obf/eg$6;-><init>(Lcom/google/obf/ew;)V

    .line 83
    invoke-virtual {v0}, Lcom/google/obf/eg$6;->nullSafe()Lcom/google/obf/ew;

    move-result-object v0

    return-object v0
.end method

.method private b(Z)Lcom/google/obf/ew;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/obf/ew",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    if-eqz p1, :cond_0

    .line 72
    sget-object v0, Lcom/google/obf/gb;->u:Lcom/google/obf/ew;

    .line 73
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/obf/eg$3;

    invoke-direct {v0, p0}, Lcom/google/obf/eg$3;-><init>(Lcom/google/obf/eg;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/obf/ex;Lcom/google/obf/gd;)Lcom/google/obf/ew;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/obf/ex;",
            "Lcom/google/obf/gd",
            "<TT;>;)",
            "Lcom/google/obf/ew",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/obf/eg;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    iget-object p1, p0, Lcom/google/obf/eg;->m:Lcom/google/obf/fr;

    .line 114
    :cond_0
    const/4 v0, 0x0

    .line 115
    iget-object v1, p0, Lcom/google/obf/eg;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/ex;

    .line 116
    if-nez v1, :cond_2

    .line 117
    if-ne v0, p1, :cond_1

    .line 118
    const/4 v0, 0x1

    move v1, v0

    goto :goto_0

    .line 119
    :cond_2
    invoke-interface {v0, p0, p2}, Lcom/google/obf/ex;->a(Lcom/google/obf/eg;Lcom/google/obf/gd;)Lcom/google/obf/ew;

    move-result-object v0

    .line 120
    if-eqz v0, :cond_1

    .line 121
    return-object v0

    .line 123
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GSON cannot serialize "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lcom/google/obf/gd;)Lcom/google/obf/ew;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/obf/gd",
            "<TT;>;)",
            "Lcom/google/obf/ew",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 84
    iget-object v1, p0, Lcom/google/obf/eg;->c:Ljava/util/Map;

    if-nez p1, :cond_1

    sget-object v0, Lcom/google/obf/eg;->a:Lcom/google/obf/gd;

    :goto_0
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/ew;

    .line 85
    if-eqz v0, :cond_2

    .line 106
    :cond_0
    :goto_1
    return-object v0

    :cond_1
    move-object v0, p1

    .line 84
    goto :goto_0

    .line 87
    :cond_2
    iget-object v0, p0, Lcom/google/obf/eg;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 88
    const/4 v1, 0x0

    .line 89
    if-nez v0, :cond_6

    .line 90
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 91
    iget-object v0, p0, Lcom/google/obf/eg;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 92
    const/4 v0, 0x1

    move-object v2, v1

    move v1, v0

    .line 93
    :goto_2
    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/eg$a;

    .line 94
    if-nez v0, :cond_0

    .line 96
    :try_start_0
    new-instance v3, Lcom/google/obf/eg$a;

    invoke-direct {v3}, Lcom/google/obf/eg$a;-><init>()V

    .line 97
    invoke-interface {v2, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    iget-object v0, p0, Lcom/google/obf/eg;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/ex;

    .line 99
    invoke-interface {v0, p0, p1}, Lcom/google/obf/ex;->a(Lcom/google/obf/eg;Lcom/google/obf/gd;)Lcom/google/obf/ew;

    move-result-object v0

    .line 100
    if-eqz v0, :cond_3

    .line 101
    invoke-virtual {v3, v0}, Lcom/google/obf/eg$a;->a(Lcom/google/obf/ew;)V

    .line 102
    iget-object v3, p0, Lcom/google/obf/eg;->c:Ljava/util/Map;

    invoke-interface {v3, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    if-eqz v1, :cond_0

    .line 106
    iget-object v1, p0, Lcom/google/obf/eg;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->remove()V

    goto :goto_1

    .line 108
    :cond_4
    :try_start_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GSON cannot handle "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 109
    :catchall_0
    move-exception v0

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    if-eqz v1, :cond_5

    .line 111
    iget-object v1, p0, Lcom/google/obf/eg;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->remove()V

    :cond_5
    throw v0

    :cond_6
    move-object v2, v0

    goto :goto_2
.end method

.method public a(Ljava/lang/Class;)Lcom/google/obf/ew;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lcom/google/obf/ew",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 124
    invoke-static {p1}, Lcom/google/obf/gd;->b(Ljava/lang/Class;)Lcom/google/obf/gd;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/obf/eg;->a(Lcom/google/obf/gd;)Lcom/google/obf/ew;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/io/Reader;)Lcom/google/obf/ge;
    .locals 2

    .prologue
    .line 171
    new-instance v0, Lcom/google/obf/ge;

    invoke-direct {v0, p1}, Lcom/google/obf/ge;-><init>(Ljava/io/Reader;)V

    .line 172
    iget-boolean v1, p0, Lcom/google/obf/eg;->l:Z

    invoke-virtual {v0, v1}, Lcom/google/obf/ge;->a(Z)V

    .line 173
    return-object v0
.end method

.method public a(Ljava/io/Writer;)Lcom/google/obf/gg;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/google/obf/eg;->j:Z

    if-eqz v0, :cond_0

    .line 165
    const-string v0, ")]}\'\n"

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 166
    :cond_0
    new-instance v0, Lcom/google/obf/gg;

    invoke-direct {v0, p1}, Lcom/google/obf/gg;-><init>(Ljava/io/Writer;)V

    .line 167
    iget-boolean v1, p0, Lcom/google/obf/eg;->k:Z

    if-eqz v1, :cond_1

    .line 168
    const-string v1, "  "

    invoke-virtual {v0, v1}, Lcom/google/obf/gg;->c(Ljava/lang/String;)V

    .line 169
    :cond_1
    iget-boolean v1, p0, Lcom/google/obf/eg;->h:Z

    invoke-virtual {v0, v1}, Lcom/google/obf/gg;->d(Z)V

    .line 170
    return-object v0
.end method

.method public a(Lcom/google/obf/ge;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/obf/ge;",
            "Ljava/lang/reflect/Type;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/en;,
            Lcom/google/obf/eu;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 210
    .line 211
    invoke-virtual {p1}, Lcom/google/obf/ge;->q()Z

    move-result v2

    .line 212
    invoke-virtual {p1, v1}, Lcom/google/obf/ge;->a(Z)V

    .line 213
    :try_start_0
    invoke-virtual {p1}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    .line 214
    const/4 v1, 0x0

    .line 215
    invoke-static {p2}, Lcom/google/obf/gd;->a(Ljava/lang/reflect/Type;)Lcom/google/obf/gd;

    move-result-object v0

    .line 216
    invoke-virtual {p0, v0}, Lcom/google/obf/eg;->a(Lcom/google/obf/gd;)Lcom/google/obf/ew;

    move-result-object v0

    .line 217
    invoke-virtual {v0, p1}, Lcom/google/obf/ew;->read(Lcom/google/obf/ge;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 219
    invoke-virtual {p1, v2}, Lcom/google/obf/ge;->a(Z)V

    .line 223
    :goto_0
    return-object v0

    .line 220
    :catch_0
    move-exception v0

    .line 221
    if-eqz v1, :cond_0

    .line 222
    const/4 v0, 0x0

    .line 223
    invoke-virtual {p1, v2}, Lcom/google/obf/ge;->a(Z)V

    goto :goto_0

    .line 224
    :cond_0
    :try_start_1
    new-instance v1, Lcom/google/obf/eu;

    invoke-direct {v1, v0}, Lcom/google/obf/eu;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 229
    :catchall_0
    move-exception v0

    invoke-virtual {p1, v2}, Lcom/google/obf/ge;->a(Z)V

    throw v0

    .line 225
    :catch_1
    move-exception v0

    .line 226
    :try_start_2
    new-instance v1, Lcom/google/obf/eu;

    invoke-direct {v1, v0}, Lcom/google/obf/eu;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 227
    :catch_2
    move-exception v0

    .line 228
    new-instance v1, Lcom/google/obf/eu;

    invoke-direct {v1, v0}, Lcom/google/obf/eu;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public a(Ljava/io/Reader;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/io/Reader;",
            "Ljava/lang/reflect/Type;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/en;,
            Lcom/google/obf/eu;
        }
    .end annotation

    .prologue
    .line 198
    invoke-virtual {p0, p1}, Lcom/google/obf/eg;->a(Ljava/io/Reader;)Lcom/google/obf/ge;

    move-result-object v0

    .line 199
    invoke-virtual {p0, v0, p2}, Lcom/google/obf/eg;->a(Lcom/google/obf/ge;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v1

    .line 200
    invoke-static {v1, v0}, Lcom/google/obf/eg;->a(Ljava/lang/Object;Lcom/google/obf/ge;)V

    .line 201
    return-object v1
.end method

.method public a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/eu;
        }
    .end annotation

    .prologue
    .line 191
    invoke-virtual {p0, p1, p2}, Lcom/google/obf/eg;->a(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    .line 192
    invoke-static {p2}, Lcom/google/obf/fl;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Type;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/eu;
        }
    .end annotation

    .prologue
    .line 193
    if-nez p1, :cond_0

    .line 194
    const/4 v0, 0x0

    .line 197
    :goto_0
    return-object v0

    .line 195
    :cond_0
    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 196
    invoke-virtual {p0, v0, p2}, Lcom/google/obf/eg;->a(Ljava/io/Reader;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/google/obf/em;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 156
    invoke-virtual {p0, p1, v0}, Lcom/google/obf/eg;->a(Lcom/google/obf/em;Ljava/lang/Appendable;)V

    .line 157
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    if-nez p1, :cond_0

    .line 126
    sget-object v0, Lcom/google/obf/eo;->a:Lcom/google/obf/eo;

    invoke-virtual {p0, v0}, Lcom/google/obf/eg;->a(Lcom/google/obf/em;)Ljava/lang/String;

    move-result-object v0

    .line 127
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/obf/eg;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 129
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/obf/eg;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;Ljava/lang/Appendable;)V

    .line 130
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/obf/em;Lcom/google/obf/gg;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/en;
        }
    .end annotation

    .prologue
    .line 174
    invoke-virtual {p2}, Lcom/google/obf/gg;->g()Z

    move-result v1

    .line 175
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/google/obf/gg;->b(Z)V

    .line 176
    invoke-virtual {p2}, Lcom/google/obf/gg;->h()Z

    move-result v2

    .line 177
    iget-boolean v0, p0, Lcom/google/obf/eg;->i:Z

    invoke-virtual {p2, v0}, Lcom/google/obf/gg;->c(Z)V

    .line 178
    invoke-virtual {p2}, Lcom/google/obf/gg;->i()Z

    move-result v3

    .line 179
    iget-boolean v0, p0, Lcom/google/obf/eg;->h:Z

    invoke-virtual {p2, v0}, Lcom/google/obf/gg;->d(Z)V

    .line 180
    :try_start_0
    invoke-static {p1, p2}, Lcom/google/obf/fm;->a(Lcom/google/obf/em;Lcom/google/obf/gg;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    invoke-virtual {p2, v1}, Lcom/google/obf/gg;->b(Z)V

    .line 182
    invoke-virtual {p2, v2}, Lcom/google/obf/gg;->c(Z)V

    .line 183
    invoke-virtual {p2, v3}, Lcom/google/obf/gg;->d(Z)V

    .line 190
    return-void

    .line 185
    :catch_0
    move-exception v0

    .line 186
    :try_start_1
    new-instance v4, Lcom/google/obf/en;

    invoke-direct {v4, v0}, Lcom/google/obf/en;-><init>(Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 187
    :catchall_0
    move-exception v0

    invoke-virtual {p2, v1}, Lcom/google/obf/gg;->b(Z)V

    .line 188
    invoke-virtual {p2, v2}, Lcom/google/obf/gg;->c(Z)V

    .line 189
    invoke-virtual {p2, v3}, Lcom/google/obf/gg;->d(Z)V

    throw v0
.end method

.method public a(Lcom/google/obf/em;Ljava/lang/Appendable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/en;
        }
    .end annotation

    .prologue
    .line 158
    :try_start_0
    invoke-static {p2}, Lcom/google/obf/fm;->a(Ljava/lang/Appendable;)Ljava/io/Writer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/obf/eg;->a(Ljava/io/Writer;)Lcom/google/obf/gg;

    move-result-object v0

    .line 159
    invoke-virtual {p0, p1, v0}, Lcom/google/obf/eg;->a(Lcom/google/obf/em;Lcom/google/obf/gg;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    return-void

    .line 161
    :catch_0
    move-exception v0

    .line 162
    new-instance v1, Lcom/google/obf/en;

    invoke-direct {v1, v0}, Lcom/google/obf/en;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/obf/gg;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/en;
        }
    .end annotation

    .prologue
    .line 137
    invoke-static {p2}, Lcom/google/obf/gd;->a(Ljava/lang/reflect/Type;)Lcom/google/obf/gd;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/obf/eg;->a(Lcom/google/obf/gd;)Lcom/google/obf/ew;

    move-result-object v0

    .line 138
    invoke-virtual {p3}, Lcom/google/obf/gg;->g()Z

    move-result v1

    .line 139
    const/4 v2, 0x1

    invoke-virtual {p3, v2}, Lcom/google/obf/gg;->b(Z)V

    .line 140
    invoke-virtual {p3}, Lcom/google/obf/gg;->h()Z

    move-result v2

    .line 141
    iget-boolean v3, p0, Lcom/google/obf/eg;->i:Z

    invoke-virtual {p3, v3}, Lcom/google/obf/gg;->c(Z)V

    .line 142
    invoke-virtual {p3}, Lcom/google/obf/gg;->i()Z

    move-result v3

    .line 143
    iget-boolean v4, p0, Lcom/google/obf/eg;->h:Z

    invoke-virtual {p3, v4}, Lcom/google/obf/gg;->d(Z)V

    .line 144
    :try_start_0
    invoke-virtual {v0, p3, p1}, Lcom/google/obf/ew;->write(Lcom/google/obf/gg;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    invoke-virtual {p3, v1}, Lcom/google/obf/gg;->b(Z)V

    .line 146
    invoke-virtual {p3, v2}, Lcom/google/obf/gg;->c(Z)V

    .line 147
    invoke-virtual {p3, v3}, Lcom/google/obf/gg;->d(Z)V

    .line 154
    return-void

    .line 149
    :catch_0
    move-exception v0

    .line 150
    :try_start_1
    new-instance v4, Lcom/google/obf/en;

    invoke-direct {v4, v0}, Lcom/google/obf/en;-><init>(Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151
    :catchall_0
    move-exception v0

    invoke-virtual {p3, v1}, Lcom/google/obf/gg;->b(Z)V

    .line 152
    invoke-virtual {p3, v2}, Lcom/google/obf/gg;->c(Z)V

    .line 153
    invoke-virtual {p3, v3}, Lcom/google/obf/gg;->d(Z)V

    throw v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/reflect/Type;Ljava/lang/Appendable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/en;
        }
    .end annotation

    .prologue
    .line 131
    :try_start_0
    invoke-static {p3}, Lcom/google/obf/fm;->a(Ljava/lang/Appendable;)Ljava/io/Writer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/obf/eg;->a(Ljava/io/Writer;)Lcom/google/obf/gg;

    move-result-object v0

    .line 132
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/obf/eg;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/obf/gg;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    return-void

    .line 134
    :catch_0
    move-exception v0

    .line 135
    new-instance v1, Lcom/google/obf/en;

    invoke-direct {v1, v0}, Lcom/google/obf/en;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 230
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "{serializeNulls:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/obf/eg;->h:Z

    .line 231
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "factories:"

    .line 232
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/obf/eg;->d:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",instanceCreators:"

    .line 233
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/obf/eg;->e:Lcom/google/obf/ff;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    .line 234
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 235
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
