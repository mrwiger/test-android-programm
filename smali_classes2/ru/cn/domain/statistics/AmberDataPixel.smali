.class Lru/cn/domain/statistics/AmberDataPixel;
.super Ljava/lang/Object;
.source "AmberDataPixel.java"


# static fields
.field private static final HEX_DIGITS:[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-string v0, "0123456789ABCDEF"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lru/cn/domain/statistics/AmberDataPixel;->HEX_DIGITS:[C

    return-void
.end method

.method static convertParams(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;
    .locals 3
    .param p0, "appId"    # Ljava/lang/String;
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "androidId"    # Ljava/lang/String;
    .param p3, "adId"    # Ljava/lang/String;
    .param p4, "ipv4"    # Ljava/lang/String;
    .param p5, "userAgent"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 45
    .local v0, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    .line 46
    const-string v1, "dvid"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    :cond_0
    if-eqz p3, :cond_1

    .line 50
    const-string v1, "adid"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    :cond_1
    if-eqz p4, :cond_2

    .line 55
    const-string v1, "ipv4"

    invoke-interface {v0, v1, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    :cond_2
    const-string v1, "sync"

    const-string v2, "up"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    const-string v1, "ss:634.up634"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    const-string v1, "ss:635.up635"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    const-string v1, "ss:636.up636"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    const-string v1, "ua"

    invoke-interface {v0, v1, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    return-object v0
.end method

.method private static encodedValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 74
    if-nez p0, :cond_1

    move-object p0, v10

    .line 149
    .end local p0    # "s":Ljava/lang/String;
    .local v2, "current":I
    .local v4, "encoded":Ljava/lang/StringBuilder;
    .local v8, "oldLength":I
    :cond_0
    :goto_0
    return-object p0

    .line 79
    .end local v2    # "current":I
    .end local v4    # "encoded":Ljava/lang/StringBuilder;
    .end local v8    # "oldLength":I
    .restart local p0    # "s":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    .line 81
    .restart local v4    # "encoded":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    .line 86
    .restart local v8    # "oldLength":I
    const/4 v2, 0x0

    .line 87
    .restart local v2    # "current":I
    :goto_1
    if-ge v2, v8, :cond_8

    .line 91
    move v7, v2

    .line 92
    .local v7, "nextToEncode":I
    :goto_2
    if-ge v7, v8, :cond_2

    .line 93
    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v11

    invoke-static {v11, v10}, Lru/cn/domain/statistics/AmberDataPixel;->isAllowed(CLjava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 94
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 98
    :cond_2
    if-ne v7, v8, :cond_3

    .line 99
    if-eqz v2, :cond_0

    .line 104
    invoke-virtual {v4, p0, v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    .line 105
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 109
    :cond_3
    if-nez v4, :cond_4

    .line 110
    new-instance v4, Ljava/lang/StringBuilder;

    .end local v4    # "encoded":Ljava/lang/StringBuilder;
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 113
    .restart local v4    # "encoded":Ljava/lang/StringBuilder;
    :cond_4
    if-le v7, v2, :cond_5

    .line 115
    invoke-virtual {v4, p0, v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    .line 123
    :cond_5
    move v2, v7

    .line 124
    add-int/lit8 v6, v2, 0x1

    .line 125
    .local v6, "nextAllowed":I
    :goto_3
    if-ge v6, v8, :cond_6

    .line 126
    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v11

    invoke-static {v11, v10}, Lru/cn/domain/statistics/AmberDataPixel;->isAllowed(CLjava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_6

    .line 127
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 132
    :cond_6
    invoke-virtual {p0, v2, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 134
    .local v9, "toEncode":Ljava/lang/String;
    :try_start_0
    const-string v11, "UTF-8"

    invoke-virtual {v9, v11}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 135
    .local v0, "bytes":[B
    array-length v1, v0

    .line 136
    .local v1, "bytesLength":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_4
    if-ge v5, v1, :cond_7

    .line 137
    const/16 v11, 0x25

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 138
    sget-object v11, Lru/cn/domain/statistics/AmberDataPixel;->HEX_DIGITS:[C

    aget-byte v12, v0, v5

    and-int/lit16 v12, v12, 0xf0

    shr-int/lit8 v12, v12, 0x4

    aget-char v11, v11, v12

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 139
    sget-object v11, Lru/cn/domain/statistics/AmberDataPixel;->HEX_DIGITS:[C

    aget-byte v12, v0, v5

    and-int/lit8 v12, v12, 0xf

    aget-char v11, v11, v12

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 141
    .end local v0    # "bytes":[B
    .end local v1    # "bytesLength":I
    .end local v5    # "i":I
    :catch_0
    move-exception v3

    .line 142
    .local v3, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v10

    .line 145
    .end local v3    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v0    # "bytes":[B
    .restart local v1    # "bytesLength":I
    .restart local v5    # "i":I
    :cond_7
    move v2, v6

    .line 146
    goto :goto_1

    .line 149
    .end local v0    # "bytes":[B
    .end local v1    # "bytesLength":I
    .end local v5    # "i":I
    .end local v6    # "nextAllowed":I
    .end local v7    # "nextToEncode":I
    .end local v9    # "toEncode":Ljava/lang/String;
    :cond_8
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private static isAllowed(CLjava/lang/String;)Z
    .locals 2
    .param p0, "c"    # C
    .param p1, "allow"    # Ljava/lang/String;

    .prologue
    .line 153
    const/16 v0, 0x41

    if-lt p0, v0, :cond_0

    const/16 v0, 0x5a

    if-le p0, v0, :cond_3

    :cond_0
    const/16 v0, 0x61

    if-lt p0, v0, :cond_1

    const/16 v0, 0x7a

    if-le p0, v0, :cond_3

    :cond_1
    const/16 v0, 0x30

    if-lt p0, v0, :cond_2

    const/16 v0, 0x39

    if-le p0, v0, :cond_3

    :cond_2
    const/16 v0, 0x2e

    if-eq p0, v0, :cond_3

    if-eqz p1, :cond_4

    .line 157
    invoke-virtual {p1, p0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_4

    :cond_3
    const/4 v0, 0x1

    .line 153
    :goto_0
    return v0

    .line 157
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static final synthetic lambda$trackingUrl$0$AmberDataPixel(Ljava/util/Map$Entry;)Ljava/lang/String;
    .locals 3
    .param p0, "entry"    # Ljava/util/Map$Entry;

    .prologue
    .line 25
    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 24
    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lru/cn/domain/statistics/AmberDataPixel;->encodedValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v1}, Lcom/annimon/stream/Stream;->of([Ljava/lang/Object;)Lcom/annimon/stream/Stream;

    move-result-object v0

    const-string v1, ":"

    .line 25
    invoke-static {v1}, Lcom/annimon/stream/Collectors;->joining(Ljava/lang/CharSequence;)Lcom/annimon/stream/Collector;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/annimon/stream/Stream;->collect(Lcom/annimon/stream/Collector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method static trackingUrl(Ljava/util/Map;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 23
    .local p0, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {p0}, Lcom/annimon/stream/Stream;->of(Ljava/util/Map;)Lcom/annimon/stream/Stream;

    move-result-object v2

    sget-object v3, Lru/cn/domain/statistics/AmberDataPixel$$Lambda$0;->$instance:Lcom/annimon/stream/function/Function;

    .line 24
    invoke-virtual {v2, v3}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v2

    const-string v3, "."

    .line 26
    invoke-static {v3}, Lcom/annimon/stream/Collectors;->joining(Ljava/lang/CharSequence;)Lcom/annimon/stream/Collector;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/annimon/stream/Stream;->collect(Lcom/annimon/stream/Collector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 28
    .local v0, "encodedParams":Ljava/lang/String;
    const-string v2, "https://dmg.digitaltarget.ru/1/6341/i/i"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 29
    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "q"

    const-string v4, "s2s"

    .line 30
    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "c"

    .line 31
    invoke-virtual {v2, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    .line 32
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 34
    .local v1, "trackingUri":Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
