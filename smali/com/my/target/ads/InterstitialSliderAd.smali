.class public final Lcom/my/target/ads/InterstitialSliderAd;
.super Lcom/my/target/common/BaseAd;
.source "InterstitialSliderAd.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/ads/InterstitialSliderAd$InterstitialSliderAdListener;
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private engine:Lcom/my/target/core/engines/g;

.field private hideStatusBarInDialog:Z

.field private listener:Lcom/my/target/ads/InterstitialSliderAd$InterstitialSliderAdListener;


# direct methods
.method public constructor <init>(ILandroid/content/Context;)V
    .locals 1
    .param p1, "slotId"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    const-string v0, "fullscreenslider"

    invoke-direct {p0, p1, v0}, Lcom/my/target/common/BaseAd;-><init>(ILjava/lang/String;)V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/ads/InterstitialSliderAd;->hideStatusBarInDialog:Z

    .line 55
    iput-object p2, p0, Lcom/my/target/ads/InterstitialSliderAd;->context:Landroid/content/Context;

    .line 56
    const-string v0, "InterstitialSliderAd created. Version: 5.1.0"

    invoke-static {v0}, Lcom/my/target/g;->c(Ljava/lang/String;)V

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/my/target/ads/InterstitialSliderAd;Lcom/my/target/dw;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/my/target/ads/InterstitialSliderAd;
    .param p1, "x1"    # Lcom/my/target/dw;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/my/target/ads/InterstitialSliderAd;->handleResult(Lcom/my/target/dw;Ljava/lang/String;)V

    return-void
.end method

.method private handleResult(Lcom/my/target/dw;Ljava/lang/String;)V
    .locals 1
    .param p1, "result"    # Lcom/my/target/dw;
    .param p2, "error"    # Ljava/lang/String;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/my/target/ads/InterstitialSliderAd;->listener:Lcom/my/target/ads/InterstitialSliderAd$InterstitialSliderAdListener;

    if-eqz v0, :cond_1

    .line 114
    if-nez p1, :cond_2

    .line 116
    iget-object v0, p0, Lcom/my/target/ads/InterstitialSliderAd;->listener:Lcom/my/target/ads/InterstitialSliderAd$InterstitialSliderAdListener;

    if-nez p2, :cond_0

    const-string p2, "no ad"

    .end local p2    # "error":Ljava/lang/String;
    :cond_0
    invoke-interface {v0, p2, p0}, Lcom/my/target/ads/InterstitialSliderAd$InterstitialSliderAdListener;->onNoAd(Ljava/lang/String;Lcom/my/target/ads/InterstitialSliderAd;)V

    .line 124
    :cond_1
    :goto_0
    return-void

    .line 120
    .restart local p2    # "error":Ljava/lang/String;
    :cond_2
    invoke-static {p0, p1}, Lcom/my/target/core/engines/g;->a(Lcom/my/target/ads/InterstitialSliderAd;Lcom/my/target/dw;)Lcom/my/target/core/engines/g;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/ads/InterstitialSliderAd;->engine:Lcom/my/target/core/engines/g;

    .line 121
    iget-object v0, p0, Lcom/my/target/ads/InterstitialSliderAd;->listener:Lcom/my/target/ads/InterstitialSliderAd$InterstitialSliderAdListener;

    invoke-interface {v0, p0}, Lcom/my/target/ads/InterstitialSliderAd$InterstitialSliderAdListener;->onLoad(Lcom/my/target/ads/InterstitialSliderAd;)V

    goto :goto_0
.end method


# virtual methods
.method public final destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 102
    iget-object v0, p0, Lcom/my/target/ads/InterstitialSliderAd;->engine:Lcom/my/target/core/engines/g;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/my/target/ads/InterstitialSliderAd;->engine:Lcom/my/target/core/engines/g;

    .line 1101
    invoke-virtual {v0}, Lcom/my/target/core/engines/g;->dismiss()V

    .line 105
    iput-object v1, p0, Lcom/my/target/ads/InterstitialSliderAd;->engine:Lcom/my/target/core/engines/g;

    .line 107
    :cond_0
    iput-object v1, p0, Lcom/my/target/ads/InterstitialSliderAd;->listener:Lcom/my/target/ads/InterstitialSliderAd$InterstitialSliderAdListener;

    .line 108
    return-void
.end method

.method public final dismiss()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/my/target/ads/InterstitialSliderAd;->engine:Lcom/my/target/core/engines/g;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/my/target/ads/InterstitialSliderAd;->engine:Lcom/my/target/core/engines/g;

    invoke-virtual {v0}, Lcom/my/target/core/engines/g;->dismiss()V

    .line 98
    :cond_0
    return-void
.end method

.method public final getListener()Lcom/my/target/ads/InterstitialSliderAd$InterstitialSliderAdListener;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/my/target/ads/InterstitialSliderAd;->listener:Lcom/my/target/ads/InterstitialSliderAd$InterstitialSliderAdListener;

    return-object v0
.end method

.method public final isHideStatusBarInDialog()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/my/target/ads/InterstitialSliderAd;->hideStatusBarInDialog:Z

    return v0
.end method

.method public final load()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/my/target/ads/InterstitialSliderAd;->adConfig:Lcom/my/target/b;

    invoke-static {v0}, Lcom/my/target/ds;->newFactory(Lcom/my/target/b;)Lcom/my/target/c;

    move-result-object v0

    new-instance v1, Lcom/my/target/ads/InterstitialSliderAd$1;

    invoke-direct {v1, p0}, Lcom/my/target/ads/InterstitialSliderAd$1;-><init>(Lcom/my/target/ads/InterstitialSliderAd;)V

    invoke-virtual {v0, v1}, Lcom/my/target/c;->a(Lcom/my/target/c$b;)Lcom/my/target/c;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/ads/InterstitialSliderAd;->context:Landroid/content/Context;

    .line 68
    invoke-virtual {v0, v1}, Lcom/my/target/c;->a(Landroid/content/Context;)Lcom/my/target/c;

    .line 69
    return-void
.end method

.method public final setHideStatusBarInDialog(Z)V
    .locals 0
    .param p1, "hideStatusBarInDialog"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/my/target/ads/InterstitialSliderAd;->hideStatusBarInDialog:Z

    .line 40
    return-void
.end method

.method public final setListener(Lcom/my/target/ads/InterstitialSliderAd$InterstitialSliderAdListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/my/target/ads/InterstitialSliderAd$InterstitialSliderAdListener;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/my/target/ads/InterstitialSliderAd;->listener:Lcom/my/target/ads/InterstitialSliderAd$InterstitialSliderAdListener;

    .line 50
    return-void
.end method

.method public final show()V
    .locals 3

    .prologue
    .line 73
    iget-object v0, p0, Lcom/my/target/ads/InterstitialSliderAd;->engine:Lcom/my/target/core/engines/g;

    if-nez v0, :cond_0

    .line 75
    const-string v0, "InterstitialSliderAd.show: No ad"

    invoke-static {v0}, Lcom/my/target/g;->c(Ljava/lang/String;)V

    .line 80
    :goto_0
    return-void

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/my/target/ads/InterstitialSliderAd;->engine:Lcom/my/target/core/engines/g;

    iget-object v1, p0, Lcom/my/target/ads/InterstitialSliderAd;->context:Landroid/content/Context;

    .line 1061
    sput-object v0, Lcom/my/target/common/MyTargetActivity;->activityEngine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

    .line 1062
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/my/target/common/MyTargetActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1063
    instance-of v2, v1, Landroid/app/Activity;

    if-nez v2, :cond_1

    .line 1065
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1067
    :cond_1
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final showDialog()V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/my/target/ads/InterstitialSliderAd;->engine:Lcom/my/target/core/engines/g;

    if-nez v0, :cond_0

    .line 86
    const-string v0, "InterstitialSliderAd.showDialog: No ad"

    invoke-static {v0}, Lcom/my/target/g;->c(Ljava/lang/String;)V

    .line 90
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/my/target/ads/InterstitialSliderAd;->engine:Lcom/my/target/core/engines/g;

    iget-object v1, p0, Lcom/my/target/ads/InterstitialSliderAd;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/my/target/core/engines/g;->showDialog(Landroid/content/Context;)V

    goto :goto_0
.end method
