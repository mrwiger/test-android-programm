.class Lru/cn/tv/mobile/channels/CurrentTelecastLoader$RequestLoader;
.super Landroid/os/AsyncTask;
.source "CurrentTelecastLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/mobile/channels/CurrentTelecastLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RequestLoader"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final requests:Landroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/support/v4/util/LongSparseArray;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    .local p1, "pendingRequests":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;>;"
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 71
    new-instance v2, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v2}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    iput-object v2, p0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$RequestLoader;->requests:Landroid/support/v4/util/LongSparseArray;

    .line 72
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/support/v4/util/LongSparseArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 73
    invoke-virtual {p1, v0}, Landroid/support/v4/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;

    .line 74
    .local v1, "request":Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;
    iget-boolean v2, v1, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;->scheduled:Z

    if-nez v2, :cond_0

    .line 75
    const/4 v2, 0x1

    iput-boolean v2, v1, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;->scheduled:Z

    .line 76
    iget-object v2, p0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$RequestLoader;->requests:Landroid/support/v4/util/LongSparseArray;

    iget-wide v4, v1, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;->channelId:J

    invoke-virtual {v2, v4, v5, v1}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 72
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 79
    .end local v1    # "request":Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;
    :cond_1
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 67
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$RequestLoader;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 12
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v2, 0x0

    .line 83
    iget-object v0, p0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$RequestLoader;->requests:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/support/v4/util/LongSparseArray;->size()I

    move-result v0

    new-array v4, v0, [Ljava/lang/String;

    .line 84
    .local v4, "ids":[Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, "index":I
    :goto_0
    iget-object v0, p0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$RequestLoader;->requests:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/support/v4/util/LongSparseArray;->size()I

    move-result v0

    if-ge v7, v0, :cond_0

    .line 85
    iget-object v0, p0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$RequestLoader;->requests:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0, v7}, Landroid/support/v4/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;

    .line 86
    .local v8, "request":Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;
    iget-wide v10, v8, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;->channelId:J

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    .line 84
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 89
    .end local v8    # "request":Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;
    :cond_0
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->currentTelecasts()Landroid/net/Uri;

    move-result-object v1

    .line 90
    .local v1, "uri":Landroid/net/Uri;
    invoke-static {}, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->access$200()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v5, v2

    .line 91
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    check-cast v9, Landroid/database/CursorWrapper;

    .line 93
    .local v9, "wrapper":Landroid/database/CursorWrapper;
    invoke-virtual {v9}, Landroid/database/CursorWrapper;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v6

    check-cast v6, Lru/cn/api/provider/cursor/TelecastItemCursor;

    .line 95
    .local v6, "cursor":Lru/cn/api/provider/cursor/TelecastItemCursor;
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 108
    :cond_1
    :goto_1
    return-object v2

    .line 99
    :cond_2
    :goto_2
    invoke-virtual {v6}, Lru/cn/api/provider/cursor/TelecastItemCursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 100
    iget-object v0, p0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$RequestLoader;->requests:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v6}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getChannelId()J

    move-result-wide v10

    invoke-virtual {v0, v10, v11}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;

    .line 101
    .restart local v8    # "request":Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;
    new-instance v0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;

    invoke-direct {v0, v2}, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;-><init>(Lru/cn/tv/mobile/channels/CurrentTelecastLoader$1;)V

    iput-object v0, v8, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;->result:Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;

    .line 102
    iget-object v0, v8, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;->result:Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;

    invoke-virtual {v6}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getTitle()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;->title:Ljava/lang/String;

    .line 103
    iget-object v0, v8, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;->result:Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;

    invoke-virtual {v6}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getTime()J

    move-result-wide v10

    iput-wide v10, v0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;->time:J

    .line 104
    iget-object v0, v8, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;->result:Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;

    invoke-virtual {v6}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getDuration()J

    move-result-wide v10

    iput-wide v10, v0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;->duration:J

    goto :goto_2

    .line 107
    .end local v8    # "request":Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;
    :cond_3
    invoke-virtual {v6}, Lru/cn/api/provider/cursor/TelecastItemCursor;->close()V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 67
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$RequestLoader;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 9
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 114
    invoke-static {}, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->access$000()Landroid/support/v4/util/LongSparseArray;

    move-result-object v8

    monitor-enter v8

    .line 115
    const/4 v6, 0x0

    .local v6, "index":I
    :goto_0
    :try_start_0
    iget-object v0, p0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$RequestLoader;->requests:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/support/v4/util/LongSparseArray;->size()I

    move-result v0

    if-ge v6, v0, :cond_2

    .line 116
    iget-object v0, p0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$RequestLoader;->requests:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0, v6}, Landroid/support/v4/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;

    .line 117
    .local v7, "request":Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;
    invoke-static {}, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->access$000()Landroid/support/v4/util/LongSparseArray;

    move-result-object v0

    iget-wide v2, v7, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;->channelId:J

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/util/LongSparseArray;->remove(J)V

    .line 118
    invoke-static {}, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->access$400()Landroid/support/v4/util/LongSparseArray;

    move-result-object v0

    iget-wide v2, v7, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;->channelId:J

    iget-object v1, v7, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;->result:Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;

    invoke-virtual {v0, v2, v3, v1}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 120
    iget-object v0, v7, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;->target:Lru/cn/tv/mobile/channels/CurrentTelecastLoader;

    if-eqz v0, :cond_0

    iget-object v0, v7, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;->target:Lru/cn/tv/mobile/channels/CurrentTelecastLoader;

    invoke-static {v0}, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->access$500(Lru/cn/tv/mobile/channels/CurrentTelecastLoader;)J

    move-result-wide v0

    iget-wide v2, v7, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;->channelId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 121
    iget-object v0, v7, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;->result:Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;

    if-eqz v0, :cond_1

    .line 122
    iget-object v0, v7, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;->target:Lru/cn/tv/mobile/channels/CurrentTelecastLoader;

    iget-object v1, v7, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;->result:Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;

    iget-object v1, v1, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;->title:Ljava/lang/String;

    iget-object v2, v7, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;->result:Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;

    iget-wide v2, v2, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;->time:J

    iget-object v4, v7, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;->result:Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;

    iget-wide v4, v4, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;->duration:J

    invoke-virtual/range {v0 .. v5}, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->telecastTitle(Ljava/lang/String;JJ)V

    .line 115
    :cond_0
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 126
    :cond_1
    iget-object v0, v7, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;->target:Lru/cn/tv/mobile/channels/CurrentTelecastLoader;

    const-string v1, ""

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->telecastTitle(Ljava/lang/String;JJ)V

    goto :goto_1

    .line 130
    .end local v7    # "request":Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;
    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 131
    return-void
.end method
