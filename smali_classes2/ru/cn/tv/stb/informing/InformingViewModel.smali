.class Lru/cn/tv/stb/informing/InformingViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "InformingViewModel.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private final contractorIn:Lio/reactivex/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/BehaviorSubject",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final informings:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Lru/cn/tv/stb/informing/Informing;",
            ">;"
        }
    .end annotation
.end field

.field private final loader:Lru/cn/mvvm/RxLoader;


# direct methods
.method constructor <init>(Landroid/content/Context;Lru/cn/mvvm/RxLoader;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loader"    # Lru/cn/mvvm/RxLoader;

    .prologue
    .line 30
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 31
    iput-object p1, p0, Lru/cn/tv/stb/informing/InformingViewModel;->context:Landroid/content/Context;

    .line 32
    iput-object p2, p0, Lru/cn/tv/stb/informing/InformingViewModel;->loader:Lru/cn/mvvm/RxLoader;

    .line 33
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/stb/informing/InformingViewModel;->informings:Landroid/arch/lifecycle/MutableLiveData;

    .line 35
    invoke-static {}, Lio/reactivex/subjects/BehaviorSubject;->create()Lio/reactivex/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/stb/informing/InformingViewModel;->contractorIn:Lio/reactivex/subjects/BehaviorSubject;

    .line 37
    iget-object v0, p0, Lru/cn/tv/stb/informing/InformingViewModel;->contractorIn:Lio/reactivex/subjects/BehaviorSubject;

    new-instance v1, Lru/cn/tv/stb/informing/InformingViewModel$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/informing/InformingViewModel$$Lambda$0;-><init>(Lru/cn/tv/stb/informing/InformingViewModel;)V

    .line 38
    invoke-virtual {v0, v1}, Lio/reactivex/subjects/BehaviorSubject;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lru/cn/tv/stb/informing/InformingViewModel$$Lambda$1;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/informing/InformingViewModel$$Lambda$1;-><init>(Lru/cn/tv/stb/informing/InformingViewModel;)V

    .line 39
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 40
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/stb/informing/InformingViewModel;->informings:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lru/cn/tv/stb/informing/InformingViewModel$$Lambda$2;->get$Lambda(Landroid/arch/lifecycle/MutableLiveData;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 41
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 42
    return-void
.end method

.method private contractor(J)Lio/reactivex/Observable;
    .locals 3
    .param p1, "contractorId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    invoke-static {p1, p2}, Lru/cn/api/provider/TvContentProviderContract;->contractor(J)Landroid/net/Uri;

    move-result-object v0

    .line 61
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/stb/informing/InformingViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v1

    .line 62
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lru/cn/tv/stb/informing/InformingViewModel$$Lambda$3;->$instance:Lio/reactivex/functions/Function;

    .line 63
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lru/cn/tv/stb/informing/InformingViewModel$$Lambda$4;->$instance:Lio/reactivex/functions/Function;

    .line 77
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 61
    return-object v1
.end method

.method static final synthetic lambda$contractor$0$InformingViewModel(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 64
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_1

    .line 65
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 66
    const-string v2, "name"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 67
    .local v1, "name":Ljava/lang/String;
    const-string v2, "brand_name"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 69
    .local v0, "brandName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 75
    .end local v0    # "brandName":Ljava/lang/String;
    .end local v1    # "name":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "brandName":Ljava/lang/String;
    .restart local v1    # "name":Ljava/lang/String;
    :cond_0
    move-object v0, v1

    .line 72
    goto :goto_0

    .line 75
    .end local v0    # "brandName":Ljava/lang/String;
    .end local v1    # "name":Ljava/lang/String;
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method static final synthetic lambda$contractor$1$InformingViewModel(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 1
    .param p0, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 77
    const-string v0, ""

    return-object v0
.end method

.method private paidInforming(Ljava/lang/String;)Lru/cn/tv/stb/informing/Informing;
    .locals 6
    .param p1, "contractorName"    # Ljava/lang/String;

    .prologue
    .line 53
    new-instance v0, Lru/cn/tv/stb/informing/Informing;

    iget-object v1, p0, Lru/cn/tv/stb/informing/InformingViewModel;->context:Landroid/content/Context;

    const v2, 0x7f0e00c3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lru/cn/tv/stb/informing/InformingViewModel;->context:Landroid/content/Context;

    const v3, 0x7f0e00c2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    .line 54
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f08027e

    invoke-direct {v0, v1, v2, v3}, Lru/cn/tv/stb/informing/Informing;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 53
    return-object v0
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$InformingViewModel(J)Lio/reactivex/Observable;
    .locals 1

    invoke-direct {p0, p1, p2}, Lru/cn/tv/stb/informing/InformingViewModel;->contractor(J)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method final bridge synthetic bridge$lambda$1$InformingViewModel(Ljava/lang/String;)Lru/cn/tv/stb/informing/Informing;
    .locals 1

    invoke-direct {p0, p1}, Lru/cn/tv/stb/informing/InformingViewModel;->paidInforming(Ljava/lang/String;)Lru/cn/tv/stb/informing/Informing;

    move-result-object v0

    return-object v0
.end method

.method informings()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Lru/cn/tv/stb/informing/Informing;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lru/cn/tv/stb/informing/InformingViewModel;->informings:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method setContractor(J)V
    .locals 3
    .param p1, "contractor"    # J

    .prologue
    .line 49
    iget-object v0, p0, Lru/cn/tv/stb/informing/InformingViewModel;->contractorIn:Lio/reactivex/subjects/BehaviorSubject;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 50
    return-void
.end method
