.class public final Lcom/yandex/metrica/c$b;
.super Lcom/yandex/metrica/impl/ob/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/c$b$a;,
        Lcom/yandex/metrica/c$b$b;
    }
.end annotation


# instance fields
.field public b:[Lcom/yandex/metrica/c$b$b;

.field public c:[Lcom/yandex/metrica/c$b$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3033
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/d;-><init>()V

    .line 3034
    invoke-virtual {p0}, Lcom/yandex/metrica/c$b;->d()Lcom/yandex/metrica/c$b;

    .line 3035
    return-void
.end method


# virtual methods
.method public a(Lcom/yandex/metrica/impl/ob/b;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 3047
    iget-object v0, p0, Lcom/yandex/metrica/c$b;->b:[Lcom/yandex/metrica/c$b$b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/yandex/metrica/c$b;->b:[Lcom/yandex/metrica/c$b$b;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 3048
    :goto_0
    iget-object v2, p0, Lcom/yandex/metrica/c$b;->b:[Lcom/yandex/metrica/c$b$b;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 3049
    iget-object v2, p0, Lcom/yandex/metrica/c$b;->b:[Lcom/yandex/metrica/c$b$b;

    aget-object v2, v2, v0

    .line 3050
    if-eqz v2, :cond_0

    .line 3051
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/yandex/metrica/impl/ob/b;->a(ILcom/yandex/metrica/impl/ob/d;)V

    .line 3048
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3055
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/c$b;->c:[Lcom/yandex/metrica/c$b$a;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/yandex/metrica/c$b;->c:[Lcom/yandex/metrica/c$b$a;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 3056
    :goto_1
    iget-object v0, p0, Lcom/yandex/metrica/c$b;->c:[Lcom/yandex/metrica/c$b$a;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 3057
    iget-object v0, p0, Lcom/yandex/metrica/c$b;->c:[Lcom/yandex/metrica/c$b$a;

    aget-object v0, v0, v1

    .line 3058
    if-eqz v0, :cond_2

    .line 3059
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/yandex/metrica/impl/ob/b;->a(ILcom/yandex/metrica/impl/ob/d;)V

    .line 3056
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3063
    :cond_3
    invoke-super {p0, p1}, Lcom/yandex/metrica/impl/ob/d;->a(Lcom/yandex/metrica/impl/ob/b;)V

    .line 3064
    return-void
.end method

.method protected c()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 3068
    invoke-super {p0}, Lcom/yandex/metrica/impl/ob/d;->c()I

    move-result v0

    .line 3069
    iget-object v2, p0, Lcom/yandex/metrica/c$b;->b:[Lcom/yandex/metrica/c$b$b;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/yandex/metrica/c$b;->b:[Lcom/yandex/metrica/c$b$b;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 3070
    :goto_0
    iget-object v3, p0, Lcom/yandex/metrica/c$b;->b:[Lcom/yandex/metrica/c$b$b;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 3071
    iget-object v3, p0, Lcom/yandex/metrica/c$b;->b:[Lcom/yandex/metrica/c$b$b;

    aget-object v3, v3, v0

    .line 3072
    if-eqz v3, :cond_0

    .line 3073
    const/4 v4, 0x1

    .line 3074
    invoke-static {v4, v3}, Lcom/yandex/metrica/impl/ob/b;->b(ILcom/yandex/metrica/impl/ob/d;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3070
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 3078
    :cond_2
    iget-object v2, p0, Lcom/yandex/metrica/c$b;->c:[Lcom/yandex/metrica/c$b$a;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/yandex/metrica/c$b;->c:[Lcom/yandex/metrica/c$b$a;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 3079
    :goto_1
    iget-object v2, p0, Lcom/yandex/metrica/c$b;->c:[Lcom/yandex/metrica/c$b$a;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 3080
    iget-object v2, p0, Lcom/yandex/metrica/c$b;->c:[Lcom/yandex/metrica/c$b$a;

    aget-object v2, v2, v1

    .line 3081
    if-eqz v2, :cond_3

    .line 3082
    const/4 v3, 0x2

    .line 3083
    invoke-static {v3, v2}, Lcom/yandex/metrica/impl/ob/b;->b(ILcom/yandex/metrica/impl/ob/d;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3079
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3087
    :cond_4
    return v0
.end method

.method public d()Lcom/yandex/metrica/c$b;
    .locals 1

    .prologue
    .line 3038
    invoke-static {}, Lcom/yandex/metrica/c$b$b;->d()[Lcom/yandex/metrica/c$b$b;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/c$b;->b:[Lcom/yandex/metrica/c$b$b;

    .line 3039
    invoke-static {}, Lcom/yandex/metrica/c$b$a;->d()[Lcom/yandex/metrica/c$b$a;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/c$b;->c:[Lcom/yandex/metrica/c$b$a;

    .line 3040
    const/4 v0, -0x1

    iput v0, p0, Lcom/yandex/metrica/c$b;->a:I

    .line 3041
    return-object p0
.end method
