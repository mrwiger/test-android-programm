.class public Lru/cn/ad/interstitial/DefaultInterstitialFactory;
.super Ljava/lang/Object;
.source "DefaultInterstitialFactory.java"

# interfaces
.implements Lru/cn/ad/AdAdapter$Factory;


# instance fields
.field private final context:Landroid/content/Context;

.field private final excludeYandex:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    sget-object v0, Landroid/os/Build;->BRAND:Ljava/lang/String;

    const-string v1, "LEAGOO"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->BRAND:Ljava/lang/String;

    const-string v1, "NOMU"

    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lru/cn/ad/interstitial/DefaultInterstitialFactory;->excludeYandex:Z

    .line 22
    iput-object p1, p0, Lru/cn/ad/interstitial/DefaultInterstitialFactory;->context:Landroid/content/Context;

    .line 23
    return-void

    .line 18
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public create(Lru/cn/api/money_miner/replies/AdSystem;)Lru/cn/ad/AdAdapter;
    .locals 3
    .param p1, "system"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    const/4 v0, 0x0

    .line 27
    iget v1, p1, Lru/cn/api/money_miner/replies/AdSystem;->bannerType:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 48
    :cond_0
    :goto_0
    return-object v0

    .line 31
    :cond_1
    invoke-static {}, Lru/cn/utils/Utils;->shouldAvoidWebView()Z

    move-result v1

    if-nez v1, :cond_0

    .line 34
    iget v1, p1, Lru/cn/api/money_miner/replies/AdSystem;->type:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 36
    :sswitch_0
    new-instance v0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;

    iget-object v1, p0, Lru/cn/ad/interstitial/DefaultInterstitialFactory;->context:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    goto :goto_0

    .line 39
    :sswitch_1
    new-instance v0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;

    iget-object v1, p0, Lru/cn/ad/interstitial/DefaultInterstitialFactory;->context:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    goto :goto_0

    .line 42
    :sswitch_2
    iget-boolean v1, p0, Lru/cn/ad/interstitial/DefaultInterstitialFactory;->excludeYandex:Z

    if-nez v1, :cond_0

    .line 45
    new-instance v0, Lru/cn/ad/interstitial/adapters/YandexInterstitial;

    iget-object v1, p0, Lru/cn/ad/interstitial/DefaultInterstitialFactory;->context:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lru/cn/ad/interstitial/adapters/YandexInterstitial;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    goto :goto_0

    .line 34
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x8 -> :sswitch_1
        0x9 -> :sswitch_2
    .end sparse-switch
.end method
