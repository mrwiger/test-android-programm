.class public Lcom/yandex/metrica/impl/ob/fl;
.super Lcom/yandex/metrica/impl/ob/ff;
.source "SourceFile"


# instance fields
.field private c:Lcom/yandex/metrica/impl/ob/fm;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/yandex/metrica/impl/ob/fl;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/yandex/metrica/impl/ob/ff;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 33
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "LOCATION_TRACKING_ENABLED"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fl;->c:Lcom/yandex/metrica/impl/ob/fm;

    .line 34
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 3

    .prologue
    .line 46
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fl;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fl;->c:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fl;->c:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/fl;->h(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/ff;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/ff;->j()V

    .line 51
    return-void
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    const-string v0, "_serviceproviderspreferences"

    return-object v0
.end method
