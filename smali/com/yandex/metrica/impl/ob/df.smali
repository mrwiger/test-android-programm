.class public Lcom/yandex/metrica/impl/ob/df;
.super Lcom/yandex/metrica/impl/ob/de;
.source "SourceFile"


# static fields
.field static final a:Lcom/yandex/metrica/impl/ob/fm;

.field static final b:Lcom/yandex/metrica/impl/ob/fm;

.field static final c:Lcom/yandex/metrica/impl/ob/fm;

.field static final d:Lcom/yandex/metrica/impl/ob/fm;

.field static final e:Lcom/yandex/metrica/impl/ob/fm;

.field static final f:Lcom/yandex/metrica/impl/ob/fm;

.field static final g:Lcom/yandex/metrica/impl/ob/fm;

.field static final h:Lcom/yandex/metrica/impl/ob/fm;

.field private static final i:Lcom/yandex/metrica/impl/ob/fm;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "LOCATION_TRACKING_ENABLED"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/df;->a:Lcom/yandex/metrica/impl/ob/fm;

    .line 21
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "COLLECT_INSTALLED_APPS"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/df;->b:Lcom/yandex/metrica/impl/ob/fm;

    .line 22
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "REFERRER"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/df;->c:Lcom/yandex/metrica/impl/ob/fm;

    .line 23
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "PREF_KEY_OFFSET"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/df;->d:Lcom/yandex/metrica/impl/ob/fm;

    .line 24
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "UNCHECKED_TIME"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/df;->e:Lcom/yandex/metrica/impl/ob/fm;

    .line 25
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "L_REQ_NUM"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/df;->f:Lcom/yandex/metrica/impl/ob/fm;

    .line 26
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "L_ID"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/df;->g:Lcom/yandex/metrica/impl/ob/fm;

    .line 27
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "LBS_ID"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/df;->h:Lcom/yandex/metrica/impl/ob/fm;

    .line 29
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "LAST_MIGRATION_VERSION"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/df;->i:Lcom/yandex/metrica/impl/ob/fm;

    return-void
.end method

.method public constructor <init>(Lcom/yandex/metrica/impl/ob/cr;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/de;-><init>(Lcom/yandex/metrica/impl/ob/cr;)V

    .line 33
    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/yandex/metrica/impl/ob/df;->i:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/df;->b(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public a()Lcom/yandex/metrica/CounterConfiguration$a;
    .locals 4

    .prologue
    .line 36
    sget-object v0, Lcom/yandex/metrica/impl/ob/df;->b:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/yandex/metrica/CounterConfiguration$a;->a:Lcom/yandex/metrica/CounterConfiguration$a;

    iget v1, v1, Lcom/yandex/metrica/CounterConfiguration$a;->d:I

    int-to-long v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/yandex/metrica/impl/ob/df;->b(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/yandex/metrica/CounterConfiguration$a;->a(I)Lcom/yandex/metrica/CounterConfiguration$a;

    move-result-object v0

    return-object v0
.end method

.method public a(J)Lcom/yandex/metrica/impl/ob/df;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/yandex/metrica/impl/ob/df;->d:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/df;->a(Ljava/lang/String;J)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/df;

    return-object v0
.end method

.method public a(Lcom/yandex/metrica/CounterConfiguration$a;)Lcom/yandex/metrica/impl/ob/df;
    .locals 4

    .prologue
    .line 45
    sget-object v0, Lcom/yandex/metrica/impl/ob/df;->b:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p1, Lcom/yandex/metrica/CounterConfiguration$a;->d:I

    int-to-long v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/yandex/metrica/impl/ob/df;->a(Ljava/lang/String;J)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/df;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/yandex/metrica/impl/ob/df;->c:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/df;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/yandex/metrica/impl/ob/df;->a:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/df;->a(Ljava/lang/String;Z)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/de;->g()V

    .line 66
    return-void
.end method

.method public b(J)J
    .locals 3

    .prologue
    .line 81
    sget-object v0, Lcom/yandex/metrica/impl/ob/df;->f:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/df;->b(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()Lcom/yandex/metrica/impl/ob/df;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/yandex/metrica/impl/ob/df;->c:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/df;->r(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/df;

    return-object v0
.end method

.method public b(I)Lcom/yandex/metrica/impl/ob/df;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/yandex/metrica/impl/ob/df;->i:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/df;->a(Ljava/lang/String;I)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/df;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/df;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/yandex/metrica/impl/ob/df;->c:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/df;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/df;

    return-object v0
.end method

.method public b(Z)Z
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/yandex/metrica/impl/ob/df;->e:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/df;->b(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public c(I)J
    .locals 4

    .prologue
    .line 73
    sget-object v0, Lcom/yandex/metrica/impl/ob/df;->d:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    int-to-long v2, p1

    invoke-virtual {p0, v0, v2, v3}, Lcom/yandex/metrica/impl/ob/df;->b(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public c(J)Lcom/yandex/metrica/impl/ob/df;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/yandex/metrica/impl/ob/df;->f:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/df;->a(Ljava/lang/String;J)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/df;

    return-object v0
.end method

.method public c(Z)Lcom/yandex/metrica/impl/ob/df;
    .locals 1

    .prologue
    .line 109
    sget-object v0, Lcom/yandex/metrica/impl/ob/df;->e:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/df;->a(Ljava/lang/String;Z)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/df;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 69
    sget-object v0, Lcom/yandex/metrica/impl/ob/df;->a:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/df;->b(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public d(J)J
    .locals 3

    .prologue
    .line 89
    sget-object v0, Lcom/yandex/metrica/impl/ob/df;->g:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/df;->b(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public e(J)Lcom/yandex/metrica/impl/ob/df;
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/yandex/metrica/impl/ob/df;->g:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/df;->a(Ljava/lang/String;J)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/df;

    return-object v0
.end method

.method public f(J)J
    .locals 3

    .prologue
    .line 97
    sget-object v0, Lcom/yandex/metrica/impl/ob/df;->h:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/df;->b(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public g(J)Lcom/yandex/metrica/impl/ob/df;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/yandex/metrica/impl/ob/df;->h:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/df;->a(Ljava/lang/String;J)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/df;

    return-object v0
.end method
