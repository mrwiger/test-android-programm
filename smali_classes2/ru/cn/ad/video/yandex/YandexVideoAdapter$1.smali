.class Lru/cn/ad/video/yandex/YandexVideoAdapter$1;
.super Ljava/lang/Object;
.source "YandexVideoAdapter.java"

# interfaces
.implements Lcom/yandex/mobile/ads/video/RequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/ad/video/yandex/YandexVideoAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/yandex/mobile/ads/video/RequestListener",
        "<",
        "Lcom/yandex/mobile/ads/video/models/blocksinfo/BlocksInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;


# direct methods
.method constructor <init>(Lru/cn/ad/video/yandex/YandexVideoAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/ad/video/yandex/YandexVideoAdapter;

    .prologue
    .line 264
    iput-object p1, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$1;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lcom/yandex/mobile/ads/video/VideoAdError;)V
    .locals 3
    .param p1, "videoAdError"    # Lcom/yandex/mobile/ads/video/VideoAdError;

    .prologue
    .line 272
    const-string v0, "YandexVASTWrapper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BlocksInfoRequestListener::onFailure: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/yandex/mobile/ads/video/VideoAdError;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$1;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    invoke-static {v0, p1}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->access$100(Lru/cn/ad/video/yandex/YandexVideoAdapter;Lcom/yandex/mobile/ads/video/VideoAdError;)V

    .line 275
    return-void
.end method

.method public onSuccess(Lcom/yandex/mobile/ads/video/models/blocksinfo/BlocksInfo;)V
    .locals 1
    .param p1, "result"    # Lcom/yandex/mobile/ads/video/models/blocksinfo/BlocksInfo;

    .prologue
    .line 267
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$1;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    invoke-static {v0, p1}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->access$000(Lru/cn/ad/video/yandex/YandexVideoAdapter;Lcom/yandex/mobile/ads/video/models/blocksinfo/BlocksInfo;)V

    .line 268
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 264
    check-cast p1, Lcom/yandex/mobile/ads/video/models/blocksinfo/BlocksInfo;

    invoke-virtual {p0, p1}, Lru/cn/ad/video/yandex/YandexVideoAdapter$1;->onSuccess(Lcom/yandex/mobile/ads/video/models/blocksinfo/BlocksInfo;)V

    return-void
.end method
