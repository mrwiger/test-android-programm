.class final Lcom/google/obf/ap;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/ap$b;,
        Lcom/google/obf/ap$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/obf/cx;

.field private final b:I

.field private final c:Lcom/google/obf/ap$a;

.field private final d:Ljava/util/concurrent/LinkedBlockingDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingDeque",
            "<",
            "Lcom/google/obf/cw;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/google/obf/ap$b;

.field private final f:Lcom/google/obf/dw;

.field private g:J

.field private h:J

.field private i:Lcom/google/obf/cw;

.field private j:I


# direct methods
.method public constructor <init>(Lcom/google/obf/cx;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/obf/ap;->a:Lcom/google/obf/cx;

    .line 3
    invoke-interface {p1}, Lcom/google/obf/cx;->b()I

    move-result v0

    iput v0, p0, Lcom/google/obf/ap;->b:I

    .line 4
    new-instance v0, Lcom/google/obf/ap$a;

    invoke-direct {v0}, Lcom/google/obf/ap$a;-><init>()V

    iput-object v0, p0, Lcom/google/obf/ap;->c:Lcom/google/obf/ap$a;

    .line 5
    new-instance v0, Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>()V

    iput-object v0, p0, Lcom/google/obf/ap;->d:Ljava/util/concurrent/LinkedBlockingDeque;

    .line 6
    new-instance v0, Lcom/google/obf/ap$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/obf/ap$b;-><init>(Lcom/google/obf/ap$1;)V

    iput-object v0, p0, Lcom/google/obf/ap;->e:Lcom/google/obf/ap$b;

    .line 7
    new-instance v0, Lcom/google/obf/dw;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/ap;->f:Lcom/google/obf/dw;

    .line 8
    iget v0, p0, Lcom/google/obf/ap;->b:I

    iput v0, p0, Lcom/google/obf/ap;->j:I

    .line 9
    return-void
.end method

.method private a(I)I
    .locals 2

    .prologue
    .line 131
    iget v0, p0, Lcom/google/obf/ap;->j:I

    iget v1, p0, Lcom/google/obf/ap;->b:I

    if-ne v0, v1, :cond_0

    .line 132
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/ap;->j:I

    .line 133
    iget-object v0, p0, Lcom/google/obf/ap;->a:Lcom/google/obf/cx;

    invoke-interface {v0}, Lcom/google/obf/cx;->a()Lcom/google/obf/cw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/ap;->i:Lcom/google/obf/cw;

    .line 134
    iget-object v0, p0, Lcom/google/obf/ap;->d:Ljava/util/concurrent/LinkedBlockingDeque;

    iget-object v1, p0, Lcom/google/obf/ap;->i:Lcom/google/obf/cw;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingDeque;->add(Ljava/lang/Object;)Z

    .line 135
    :cond_0
    iget v0, p0, Lcom/google/obf/ap;->b:I

    iget v1, p0, Lcom/google/obf/ap;->j:I

    sub-int/2addr v0, v1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method private a(JLjava/nio/ByteBuffer;I)V
    .locals 5

    .prologue
    .line 77
    .line 78
    :goto_0
    if-lez p4, :cond_0

    .line 79
    invoke-direct {p0, p1, p2}, Lcom/google/obf/ap;->b(J)V

    .line 80
    iget-wide v0, p0, Lcom/google/obf/ap;->g:J

    sub-long v0, p1, v0

    long-to-int v1, v0

    .line 81
    iget v0, p0, Lcom/google/obf/ap;->b:I

    sub-int/2addr v0, v1

    invoke-static {p4, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 82
    iget-object v0, p0, Lcom/google/obf/ap;->d:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/cw;

    .line 83
    iget-object v3, v0, Lcom/google/obf/cw;->a:[B

    invoke-virtual {v0, v1}, Lcom/google/obf/cw;->a(I)I

    move-result v0

    invoke-virtual {p3, v3, v0, v2}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 84
    int-to-long v0, v2

    add-long/2addr p1, v0

    .line 85
    sub-int/2addr p4, v2

    .line 86
    goto :goto_0

    .line 87
    :cond_0
    return-void
.end method

.method private a(J[BI)V
    .locals 7

    .prologue
    .line 88
    const/4 v0, 0x0

    move v1, v0

    .line 89
    :goto_0
    if-ge v1, p4, :cond_0

    .line 90
    invoke-direct {p0, p1, p2}, Lcom/google/obf/ap;->b(J)V

    .line 91
    iget-wide v2, p0, Lcom/google/obf/ap;->g:J

    sub-long v2, p1, v2

    long-to-int v2, v2

    .line 92
    sub-int v0, p4, v1

    iget v3, p0, Lcom/google/obf/ap;->b:I

    sub-int/2addr v3, v2

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 93
    iget-object v0, p0, Lcom/google/obf/ap;->d:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/cw;

    .line 94
    iget-object v4, v0, Lcom/google/obf/cw;->a:[B

    invoke-virtual {v0, v2}, Lcom/google/obf/cw;->a(I)I

    move-result v0

    invoke-static {v4, v0, p3, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 95
    int-to-long v4, v3

    add-long/2addr p1, v4

    .line 96
    add-int v0, v1, v3

    move v1, v0

    .line 97
    goto :goto_0

    .line 98
    :cond_0
    return-void
.end method

.method private a(Lcom/google/obf/t;Lcom/google/obf/ap$b;)V
    .locals 12

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 37
    iget-wide v0, p2, Lcom/google/obf/ap$b;->a:J

    .line 38
    iget-object v2, p0, Lcom/google/obf/ap;->f:Lcom/google/obf/dw;

    iget-object v2, v2, Lcom/google/obf/dw;->a:[B

    invoke-direct {p0, v0, v1, v2, v6}, Lcom/google/obf/ap;->a(J[BI)V

    .line 39
    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    .line 40
    iget-object v0, p0, Lcom/google/obf/ap;->f:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    aget-byte v1, v0, v4

    .line 41
    and-int/lit16 v0, v1, 0x80

    if-eqz v0, :cond_5

    move v0, v6

    .line 42
    :goto_0
    and-int/lit8 v1, v1, 0x7f

    .line 43
    iget-object v5, p1, Lcom/google/obf/t;->a:Lcom/google/obf/e;

    iget-object v5, v5, Lcom/google/obf/e;->a:[B

    if-nez v5, :cond_0

    .line 44
    iget-object v5, p1, Lcom/google/obf/t;->a:Lcom/google/obf/e;

    const/16 v7, 0x10

    new-array v7, v7, [B

    iput-object v7, v5, Lcom/google/obf/e;->a:[B

    .line 45
    :cond_0
    iget-object v5, p1, Lcom/google/obf/t;->a:Lcom/google/obf/e;

    iget-object v5, v5, Lcom/google/obf/e;->a:[B

    invoke-direct {p0, v2, v3, v5, v1}, Lcom/google/obf/ap;->a(J[BI)V

    .line 46
    int-to-long v8, v1

    add-long/2addr v2, v8

    .line 47
    if-eqz v0, :cond_6

    .line 48
    iget-object v1, p0, Lcom/google/obf/ap;->f:Lcom/google/obf/dw;

    iget-object v1, v1, Lcom/google/obf/dw;->a:[B

    const/4 v5, 0x2

    invoke-direct {p0, v2, v3, v1, v5}, Lcom/google/obf/ap;->a(J[BI)V

    .line 49
    const-wide/16 v8, 0x2

    add-long/2addr v2, v8

    .line 50
    iget-object v1, p0, Lcom/google/obf/ap;->f:Lcom/google/obf/dw;

    invoke-virtual {v1, v4}, Lcom/google/obf/dw;->c(I)V

    .line 51
    iget-object v1, p0, Lcom/google/obf/ap;->f:Lcom/google/obf/dw;

    invoke-virtual {v1}, Lcom/google/obf/dw;->g()I

    move-result v1

    move-wide v8, v2

    .line 53
    :goto_1
    iget-object v2, p1, Lcom/google/obf/t;->a:Lcom/google/obf/e;

    iget-object v2, v2, Lcom/google/obf/e;->d:[I

    .line 54
    if-eqz v2, :cond_1

    array-length v3, v2

    if-ge v3, v1, :cond_2

    .line 55
    :cond_1
    new-array v2, v1, [I

    .line 56
    :cond_2
    iget-object v3, p1, Lcom/google/obf/t;->a:Lcom/google/obf/e;

    iget-object v3, v3, Lcom/google/obf/e;->e:[I

    .line 57
    if-eqz v3, :cond_3

    array-length v5, v3

    if-ge v5, v1, :cond_4

    .line 58
    :cond_3
    new-array v3, v1, [I

    .line 59
    :cond_4
    if-eqz v0, :cond_7

    .line 60
    mul-int/lit8 v0, v1, 0x6

    .line 61
    iget-object v5, p0, Lcom/google/obf/ap;->f:Lcom/google/obf/dw;

    invoke-static {v5, v0}, Lcom/google/obf/ap;->b(Lcom/google/obf/dw;I)V

    .line 62
    iget-object v5, p0, Lcom/google/obf/ap;->f:Lcom/google/obf/dw;

    iget-object v5, v5, Lcom/google/obf/dw;->a:[B

    invoke-direct {p0, v8, v9, v5, v0}, Lcom/google/obf/ap;->a(J[BI)V

    .line 63
    int-to-long v10, v0

    add-long/2addr v8, v10

    .line 64
    iget-object v0, p0, Lcom/google/obf/ap;->f:Lcom/google/obf/dw;

    invoke-virtual {v0, v4}, Lcom/google/obf/dw;->c(I)V

    .line 65
    :goto_2
    if-ge v4, v1, :cond_8

    .line 66
    iget-object v0, p0, Lcom/google/obf/ap;->f:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->g()I

    move-result v0

    aput v0, v2, v4

    .line 67
    iget-object v0, p0, Lcom/google/obf/ap;->f:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->s()I

    move-result v0

    aput v0, v3, v4

    .line 68
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_5
    move v0, v4

    .line 41
    goto :goto_0

    :cond_6
    move v1, v6

    move-wide v8, v2

    .line 52
    goto :goto_1

    .line 70
    :cond_7
    aput v4, v2, v4

    .line 71
    iget v0, p1, Lcom/google/obf/t;->c:I

    iget-wide v10, p2, Lcom/google/obf/ap$b;->a:J

    sub-long v10, v8, v10

    long-to-int v5, v10

    sub-int/2addr v0, v5

    aput v0, v3, v4

    .line 72
    :cond_8
    iget-object v0, p1, Lcom/google/obf/t;->a:Lcom/google/obf/e;

    iget-object v4, p2, Lcom/google/obf/ap$b;->b:[B

    iget-object v5, p1, Lcom/google/obf/t;->a:Lcom/google/obf/e;

    iget-object v5, v5, Lcom/google/obf/e;->a:[B

    invoke-virtual/range {v0 .. v6}, Lcom/google/obf/e;->a(I[I[I[B[BI)V

    .line 73
    iget-wide v0, p2, Lcom/google/obf/ap$b;->a:J

    sub-long v0, v8, v0

    long-to-int v0, v0

    .line 74
    iget-wide v2, p2, Lcom/google/obf/ap$b;->a:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p2, Lcom/google/obf/ap$b;->a:J

    .line 75
    iget v1, p1, Lcom/google/obf/t;->c:I

    sub-int v0, v1, v0

    iput v0, p1, Lcom/google/obf/t;->c:I

    .line 76
    return-void
.end method

.method private b(J)V
    .locals 9

    .prologue
    .line 99
    iget-wide v0, p0, Lcom/google/obf/ap;->g:J

    sub-long v0, p1, v0

    long-to-int v0, v0

    .line 100
    iget v1, p0, Lcom/google/obf/ap;->b:I

    div-int v2, v0, v1

    .line 101
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 102
    iget-object v3, p0, Lcom/google/obf/ap;->a:Lcom/google/obf/cx;

    iget-object v0, p0, Lcom/google/obf/ap;->d:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/cw;

    invoke-interface {v3, v0}, Lcom/google/obf/cx;->a(Lcom/google/obf/cw;)V

    .line 103
    iget-wide v4, p0, Lcom/google/obf/ap;->g:J

    iget v0, p0, Lcom/google/obf/ap;->b:I

    int-to-long v6, v0

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/obf/ap;->g:J

    .line 104
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 105
    :cond_0
    return-void
.end method

.method private static b(Lcom/google/obf/dw;I)V
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/google/obf/dw;->c()I

    move-result v0

    if-ge v0, p1, :cond_0

    .line 107
    new-array v0, p1, [B

    invoke-virtual {p0, v0, p1}, Lcom/google/obf/dw;->a([BI)V

    .line 108
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/google/obf/ak;IZ)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 110
    invoke-direct {p0, p2}, Lcom/google/obf/ap;->a(I)I

    move-result v1

    .line 111
    iget-object v2, p0, Lcom/google/obf/ap;->i:Lcom/google/obf/cw;

    iget-object v2, v2, Lcom/google/obf/cw;->a:[B

    iget-object v3, p0, Lcom/google/obf/ap;->i:Lcom/google/obf/cw;

    iget v4, p0, Lcom/google/obf/ap;->j:I

    .line 112
    invoke-virtual {v3, v4}, Lcom/google/obf/cw;->a(I)I

    move-result v3

    .line 113
    invoke-interface {p1, v2, v3, v1}, Lcom/google/obf/ak;->a([BII)I

    move-result v1

    .line 114
    if-ne v1, v0, :cond_1

    .line 115
    if-eqz p3, :cond_0

    .line 120
    :goto_0
    return v0

    .line 117
    :cond_0
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 118
    :cond_1
    iget v0, p0, Lcom/google/obf/ap;->j:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/ap;->j:I

    .line 119
    iget-wide v2, p0, Lcom/google/obf/ap;->h:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/obf/ap;->h:J

    move v0, v1

    .line 120
    goto :goto_0
.end method

.method public a()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 10
    iget-object v0, p0, Lcom/google/obf/ap;->c:Lcom/google/obf/ap$a;

    invoke-virtual {v0}, Lcom/google/obf/ap$a;->a()V

    .line 11
    iget-object v1, p0, Lcom/google/obf/ap;->a:Lcom/google/obf/cx;

    iget-object v0, p0, Lcom/google/obf/ap;->d:Ljava/util/concurrent/LinkedBlockingDeque;

    iget-object v2, p0, Lcom/google/obf/ap;->d:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v2}, Ljava/util/concurrent/LinkedBlockingDeque;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/obf/cw;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/LinkedBlockingDeque;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/obf/cw;

    invoke-interface {v1, v0}, Lcom/google/obf/cx;->a([Lcom/google/obf/cw;)V

    .line 12
    iget-object v0, p0, Lcom/google/obf/ap;->d:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingDeque;->clear()V

    .line 13
    iput-wide v4, p0, Lcom/google/obf/ap;->g:J

    .line 14
    iput-wide v4, p0, Lcom/google/obf/ap;->h:J

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/ap;->i:Lcom/google/obf/cw;

    .line 16
    iget v0, p0, Lcom/google/obf/ap;->b:I

    iput v0, p0, Lcom/google/obf/ap;->j:I

    .line 17
    return-void
.end method

.method public a(JIJI[B)V
    .locals 8

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/obf/ap;->c:Lcom/google/obf/ap$a;

    move-wide v1, p1

    move v3, p3

    move-wide v4, p4

    move v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/google/obf/ap$a;->a(JIJI[B)V

    .line 130
    return-void
.end method

.method public a(Lcom/google/obf/dw;I)V
    .locals 6

    .prologue
    .line 121
    :goto_0
    if-lez p2, :cond_0

    .line 122
    invoke-direct {p0, p2}, Lcom/google/obf/ap;->a(I)I

    move-result v0

    .line 123
    iget-object v1, p0, Lcom/google/obf/ap;->i:Lcom/google/obf/cw;

    iget-object v1, v1, Lcom/google/obf/cw;->a:[B

    iget-object v2, p0, Lcom/google/obf/ap;->i:Lcom/google/obf/cw;

    iget v3, p0, Lcom/google/obf/ap;->j:I

    invoke-virtual {v2, v3}, Lcom/google/obf/cw;->a(I)I

    move-result v2

    invoke-virtual {p1, v1, v2, v0}, Lcom/google/obf/dw;->a([BII)V

    .line 124
    iget v1, p0, Lcom/google/obf/ap;->j:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/obf/ap;->j:I

    .line 125
    iget-wide v2, p0, Lcom/google/obf/ap;->h:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/obf/ap;->h:J

    .line 126
    sub-int/2addr p2, v0

    .line 127
    goto :goto_0

    .line 128
    :cond_0
    return-void
.end method

.method public a(J)Z
    .locals 5

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/obf/ap;->c:Lcom/google/obf/ap$a;

    invoke-virtual {v0, p1, p2}, Lcom/google/obf/ap$a;->a(J)J

    move-result-wide v0

    .line 23
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 26
    :goto_0
    return v0

    .line 25
    :cond_0
    invoke-direct {p0, v0, v1}, Lcom/google/obf/ap;->b(J)V

    .line 26
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Lcom/google/obf/t;)Z
    .locals 2

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/obf/ap;->c:Lcom/google/obf/ap$a;

    iget-object v1, p0, Lcom/google/obf/ap;->e:Lcom/google/obf/ap$b;

    invoke-virtual {v0, p1, v1}, Lcom/google/obf/ap$a;->a(Lcom/google/obf/t;Lcom/google/obf/ap$b;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/obf/ap;->c:Lcom/google/obf/ap$a;

    invoke-virtual {v0}, Lcom/google/obf/ap$a;->b()J

    move-result-wide v0

    .line 20
    invoke-direct {p0, v0, v1}, Lcom/google/obf/ap;->b(J)V

    .line 21
    return-void
.end method

.method public b(Lcom/google/obf/t;)Z
    .locals 4

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/obf/ap;->c:Lcom/google/obf/ap$a;

    iget-object v1, p0, Lcom/google/obf/ap;->e:Lcom/google/obf/ap$b;

    invoke-virtual {v0, p1, v1}, Lcom/google/obf/ap$a;->a(Lcom/google/obf/t;Lcom/google/obf/ap$b;)Z

    move-result v0

    .line 28
    if-nez v0, :cond_0

    .line 29
    const/4 v0, 0x0

    .line 36
    :goto_0
    return v0

    .line 30
    :cond_0
    invoke-virtual {p1}, Lcom/google/obf/t;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 31
    iget-object v0, p0, Lcom/google/obf/ap;->e:Lcom/google/obf/ap$b;

    invoke-direct {p0, p1, v0}, Lcom/google/obf/ap;->a(Lcom/google/obf/t;Lcom/google/obf/ap$b;)V

    .line 32
    :cond_1
    iget v0, p1, Lcom/google/obf/t;->c:I

    invoke-virtual {p1, v0}, Lcom/google/obf/t;->a(I)V

    .line 33
    iget-object v0, p0, Lcom/google/obf/ap;->e:Lcom/google/obf/ap$b;

    iget-wide v0, v0, Lcom/google/obf/ap$b;->a:J

    iget-object v2, p1, Lcom/google/obf/t;->b:Ljava/nio/ByteBuffer;

    iget v3, p1, Lcom/google/obf/t;->c:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/obf/ap;->a(JLjava/nio/ByteBuffer;I)V

    .line 34
    iget-object v0, p0, Lcom/google/obf/ap;->c:Lcom/google/obf/ap$a;

    invoke-virtual {v0}, Lcom/google/obf/ap$a;->b()J

    move-result-wide v0

    .line 35
    invoke-direct {p0, v0, v1}, Lcom/google/obf/ap;->b(J)V

    .line 36
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 109
    iget-wide v0, p0, Lcom/google/obf/ap;->h:J

    return-wide v0
.end method
