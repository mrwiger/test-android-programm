.class public Lru/cn/tv/mobile/schedule/ScheduleAdapter;
.super Landroid/widget/SimpleCursorAdapter;
.source "ScheduleAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/mobile/schedule/ScheduleAdapter$ViewHolder;
    }
.end annotation


# static fields
.field private static from:[Ljava/lang/String;

.field private static to:[I


# instance fields
.field private layout:I

.field private textColor:Landroid/content/res/ColorStateList;

.field private textColorOnAir:Landroid/content/res/ColorStateList;

.field private textColorWithRecord:Landroid/content/res/ColorStateList;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 24
    new-array v0, v1, [Ljava/lang/String;

    sput-object v0, Lru/cn/tv/mobile/schedule/ScheduleAdapter;->from:[Ljava/lang/String;

    .line 25
    new-array v0, v1, [I

    sput-object v0, Lru/cn/tv/mobile/schedule/ScheduleAdapter;->to:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x0

    .line 35
    sget-object v4, Lru/cn/tv/mobile/schedule/ScheduleAdapter;->from:[Ljava/lang/String;

    sget-object v5, Lru/cn/tv/mobile/schedule/ScheduleAdapter;->to:[I

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v6, v2

    invoke-direct/range {v0 .. v6}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 37
    const/4 v0, 0x3

    new-array v8, v0, [I

    fill-array-data v8, :array_0

    .line 42
    .local v8, "textSizeAttr":[I
    invoke-virtual {p1, v8}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v7

    .line 44
    .local v7, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v7, v2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/mobile/schedule/ScheduleAdapter;->textColor:Landroid/content/res/ColorStateList;

    .line 45
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/mobile/schedule/ScheduleAdapter;->textColorOnAir:Landroid/content/res/ColorStateList;

    .line 46
    const/4 v0, 0x2

    invoke-virtual {v7, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/mobile/schedule/ScheduleAdapter;->textColorWithRecord:Landroid/content/res/ColorStateList;

    .line 47
    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    .line 49
    const v0, 0x7f0c00ad

    iput v0, p0, Lru/cn/tv/mobile/schedule/ScheduleAdapter;->layout:I

    .line 50
    return-void

    .line 37
    nop

    :array_0
    .array-data 4
        0x7f04018a
        0x7f04018b
        0x7f04018c
    .end array-data
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 12
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 74
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lru/cn/tv/mobile/schedule/ScheduleAdapter$ViewHolder;

    .local v3, "holder":Lru/cn/tv/mobile/schedule/ScheduleAdapter$ViewHolder;
    move-object v0, p3

    .line 75
    check-cast v0, Lru/cn/api/provider/cursor/ScheduleItemCursor;

    .line 77
    .local v0, "c":Lru/cn/api/provider/cursor/ScheduleItemCursor;
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getTime()J

    move-result-wide v8

    .line 78
    .local v8, "time":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 80
    .local v4, "currentTimeMs":J
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->hasRecords()Z

    move-result v10

    if-eqz v10, :cond_0

    iget-object v2, p0, Lru/cn/tv/mobile/schedule/ScheduleAdapter;->textColorWithRecord:Landroid/content/res/ColorStateList;

    .line 81
    .local v2, "color":Landroid/content/res/ColorStateList;
    :goto_0
    iget-object v10, v3, Lru/cn/tv/mobile/schedule/ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    invoke-virtual {v10, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 82
    iget-object v10, v3, Lru/cn/tv/mobile/schedule/ScheduleAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v10, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 84
    invoke-virtual {v0, v4, v5}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->isOnTime(J)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 85
    iget-object v10, v3, Lru/cn/tv/mobile/schedule/ScheduleAdapter$ViewHolder;->onAirIndicator:Landroid/view/View;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    .line 87
    iget-object v10, v3, Lru/cn/tv/mobile/schedule/ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    iget-object v11, p0, Lru/cn/tv/mobile/schedule/ScheduleAdapter;->textColorOnAir:Landroid/content/res/ColorStateList;

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 88
    iget-object v10, v3, Lru/cn/tv/mobile/schedule/ScheduleAdapter$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v11, p0, Lru/cn/tv/mobile/schedule/ScheduleAdapter;->textColorOnAir:Landroid/content/res/ColorStateList;

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 89
    iget-object v10, v3, Lru/cn/tv/mobile/schedule/ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    sget-object v11, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 90
    iget-object v10, v3, Lru/cn/tv/mobile/schedule/ScheduleAdapter$ViewHolder;->title:Landroid/widget/TextView;

    sget-object v11, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 99
    :goto_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 100
    .local v1, "calendar":Ljava/util/Calendar;
    const-wide/16 v10, 0x3e8

    mul-long/2addr v10, v8

    invoke-virtual {v1, v10, v11}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 102
    const-string v10, "HH:mm"

    invoke-static {v1, v10}, Lru/cn/utils/Utils;->format(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 103
    .local v6, "s":Ljava/lang/String;
    new-instance v7, Landroid/text/SpannableString;

    invoke-direct {v7, v6}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 106
    .local v7, "ss":Landroid/text/SpannableString;
    iget-object v10, v3, Lru/cn/tv/mobile/schedule/ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    invoke-virtual {v10, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    iget-object v10, v3, Lru/cn/tv/mobile/schedule/ScheduleAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getTitle()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    return-void

    .line 80
    .end local v1    # "calendar":Ljava/util/Calendar;
    .end local v2    # "color":Landroid/content/res/ColorStateList;
    .end local v6    # "s":Ljava/lang/String;
    .end local v7    # "ss":Landroid/text/SpannableString;
    :cond_0
    iget-object v2, p0, Lru/cn/tv/mobile/schedule/ScheduleAdapter;->textColor:Landroid/content/res/ColorStateList;

    goto :goto_0

    .line 93
    .restart local v2    # "color":Landroid/content/res/ColorStateList;
    :cond_1
    iget-object v10, v3, Lru/cn/tv/mobile/schedule/ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    sget-object v11, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 94
    iget-object v10, v3, Lru/cn/tv/mobile/schedule/ScheduleAdapter$ViewHolder;->title:Landroid/widget/TextView;

    sget-object v11, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 96
    iget-object v10, v3, Lru/cn/tv/mobile/schedule/ScheduleAdapter$ViewHolder;->onAirIndicator:Landroid/view/View;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 60
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 62
    .local v1, "inflater":Landroid/view/LayoutInflater;
    iget v3, p0, Lru/cn/tv/mobile/schedule/ScheduleAdapter;->layout:I

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 64
    .local v2, "view":Landroid/view/View;
    new-instance v0, Lru/cn/tv/mobile/schedule/ScheduleAdapter$ViewHolder;

    invoke-direct {v0, v5}, Lru/cn/tv/mobile/schedule/ScheduleAdapter$ViewHolder;-><init>(Lru/cn/tv/mobile/schedule/ScheduleAdapter$1;)V

    .line 65
    .local v0, "holder":Lru/cn/tv/mobile/schedule/ScheduleAdapter$ViewHolder;
    const v3, 0x7f0901d4

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lru/cn/tv/mobile/schedule/ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    .line 66
    const v3, 0x7f0901d7

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lru/cn/tv/mobile/schedule/ScheduleAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 67
    const v3, 0x7f090157

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v0, Lru/cn/tv/mobile/schedule/ScheduleAdapter$ViewHolder;->onAirIndicator:Landroid/view/View;

    .line 68
    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 70
    return-object v2
.end method
