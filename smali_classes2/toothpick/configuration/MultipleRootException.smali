.class public Ltoothpick/configuration/MultipleRootException;
.super Ljava/lang/RuntimeException;
.source "MultipleRootException.java"


# direct methods
.method public constructor <init>(Ltoothpick/Scope;)V
    .locals 4
    .param p1, "scope"    # Ltoothpick/Scope;

    .prologue
    .line 7
    const-string v0, "Scope %s is a new root in TP scope forest. Only one root is allowed in this configuration."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-interface {p1}, Ltoothpick/Scope;->getName()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 8
    return-void
.end method
