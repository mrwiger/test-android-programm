.class Lru/cn/view/CursorRecyclerViewAdapter$NotifyingDataSetObserver;
.super Landroid/database/DataSetObserver;
.source "CursorRecyclerViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/view/CursorRecyclerViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NotifyingDataSetObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/view/CursorRecyclerViewAdapter;


# direct methods
.method private constructor <init>(Lru/cn/view/CursorRecyclerViewAdapter;)V
    .locals 0

    .prologue
    .line 154
    .local p0, "this":Lru/cn/view/CursorRecyclerViewAdapter$NotifyingDataSetObserver;, "Lru/cn/view/CursorRecyclerViewAdapter<TVH;>.NotifyingDataSetObserver;"
    iput-object p1, p0, Lru/cn/view/CursorRecyclerViewAdapter$NotifyingDataSetObserver;->this$0:Lru/cn/view/CursorRecyclerViewAdapter;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lru/cn/view/CursorRecyclerViewAdapter;Lru/cn/view/CursorRecyclerViewAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lru/cn/view/CursorRecyclerViewAdapter;
    .param p2, "x1"    # Lru/cn/view/CursorRecyclerViewAdapter$1;

    .prologue
    .line 154
    .local p0, "this":Lru/cn/view/CursorRecyclerViewAdapter$NotifyingDataSetObserver;, "Lru/cn/view/CursorRecyclerViewAdapter<TVH;>.NotifyingDataSetObserver;"
    invoke-direct {p0, p1}, Lru/cn/view/CursorRecyclerViewAdapter$NotifyingDataSetObserver;-><init>(Lru/cn/view/CursorRecyclerViewAdapter;)V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 2

    .prologue
    .line 157
    .local p0, "this":Lru/cn/view/CursorRecyclerViewAdapter$NotifyingDataSetObserver;, "Lru/cn/view/CursorRecyclerViewAdapter<TVH;>.NotifyingDataSetObserver;"
    invoke-super {p0}, Landroid/database/DataSetObserver;->onChanged()V

    .line 158
    iget-object v0, p0, Lru/cn/view/CursorRecyclerViewAdapter$NotifyingDataSetObserver;->this$0:Lru/cn/view/CursorRecyclerViewAdapter;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lru/cn/view/CursorRecyclerViewAdapter;->access$202(Lru/cn/view/CursorRecyclerViewAdapter;Z)Z

    .line 159
    iget-object v0, p0, Lru/cn/view/CursorRecyclerViewAdapter$NotifyingDataSetObserver;->this$0:Lru/cn/view/CursorRecyclerViewAdapter;

    invoke-virtual {v0}, Lru/cn/view/CursorRecyclerViewAdapter;->notifyDataSetChanged()V

    .line 160
    return-void
.end method

.method public onInvalidated()V
    .locals 2

    .prologue
    .line 164
    .local p0, "this":Lru/cn/view/CursorRecyclerViewAdapter$NotifyingDataSetObserver;, "Lru/cn/view/CursorRecyclerViewAdapter<TVH;>.NotifyingDataSetObserver;"
    invoke-super {p0}, Landroid/database/DataSetObserver;->onInvalidated()V

    .line 165
    iget-object v0, p0, Lru/cn/view/CursorRecyclerViewAdapter$NotifyingDataSetObserver;->this$0:Lru/cn/view/CursorRecyclerViewAdapter;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lru/cn/view/CursorRecyclerViewAdapter;->access$202(Lru/cn/view/CursorRecyclerViewAdapter;Z)Z

    .line 166
    iget-object v0, p0, Lru/cn/view/CursorRecyclerViewAdapter$NotifyingDataSetObserver;->this$0:Lru/cn/view/CursorRecyclerViewAdapter;

    invoke-virtual {v0}, Lru/cn/view/CursorRecyclerViewAdapter;->notifyDataSetChanged()V

    .line 168
    return-void
.end method
