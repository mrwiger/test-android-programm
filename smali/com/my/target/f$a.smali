.class public Lcom/my/target/f$a;
.super Lcom/my/target/f;
.source "AdServiceBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# static fields
.field private static A:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-string v0, "https://ad.mail.ru/mobile/"

    sput-object v0, Lcom/my/target/f$a;->A:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/my/target/f;-><init>()V

    .line 41
    return-void
.end method

.method private c(Lcom/my/target/b;Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 104
    invoke-virtual {p0, p1, p2}, Lcom/my/target/f$a;->b(Lcom/my/target/b;Landroid/content/Context;)Ljava/util/Map;

    move-result-object v1

    .line 105
    new-instance v4, Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/my/target/f$a;->A:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/my/target/b;->getSlotId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 106
    const/4 v0, 0x1

    .line 107
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 109
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 110
    if-eqz v1, :cond_1

    .line 112
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 115
    :try_start_0
    const-string v3, "UTF-8"

    invoke-static {v1, v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 121
    :goto_1
    if-eqz v2, :cond_0

    .line 123
    const-string v2, "?"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    const/4 v2, 0x0

    move v0, v2

    :goto_2
    move v2, v0

    .line 131
    goto :goto_0

    .line 117
    :catch_0
    move-exception v3

    .line 119
    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 128
    :cond_0
    const-string v3, "&"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    move v0, v2

    goto :goto_2

    .line 132
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/ae;
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lcom/my/target/f$a;->c(Lcom/my/target/b;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 47
    invoke-static {v0}, Lcom/my/target/ae;->m(Ljava/lang/String;)Lcom/my/target/ae;

    move-result-object v0

    return-object v0
.end method

.method protected b(Lcom/my/target/b;Landroid/content/Context;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/b;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 53
    const-string v0, "formats"

    invoke-virtual {p1}, Lcom/my/target/b;->getFormat()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    const-string v0, "adman_ver"

    const-string v2, "5.1.0"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    invoke-static {}, Lcom/my/target/common/MyTargetPrivacy;->isConsentSpecified()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    const-string v2, "user_consent"

    invoke-static {}, Lcom/my/target/common/MyTargetPrivacy;->isUserConsent()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "1"

    :goto_0
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    :cond_0
    invoke-static {}, Lcom/my/target/common/MyTargetPrivacy;->isUserAgeRestricted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    const-string v0, "user_age_restricted"

    const-string v2, "1"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    :cond_1
    invoke-virtual {p1}, Lcom/my/target/b;->isAutoLoadVideo()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 68
    const-string v0, "preloadvideo"

    const-string v2, "1"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    :cond_2
    invoke-virtual {p1}, Lcom/my/target/b;->getBannersCount()I

    move-result v0

    .line 72
    if-lez v0, :cond_3

    .line 74
    const-string v2, "count"

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    :cond_3
    invoke-static {}, Lcom/my/target/common/MyTargetPrivacy;->isConsentSpecified()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/my/target/common/MyTargetPrivacy;->isUserConsent()Z

    move-result v0

    if-nez v0, :cond_5

    move-object v0, v1

    .line 99
    :goto_1
    return-object v0

    .line 58
    :cond_4
    const-string v0, "0"

    goto :goto_0

    .line 82
    :cond_5
    invoke-virtual {p1}, Lcom/my/target/b;->getCustomParams()Lcom/my/target/common/CustomParams;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/my/target/common/CustomParams;->putDataTo(Ljava/util/Map;)V

    .line 86
    :try_start_0
    invoke-static {}, Lcom/my/target/bn;->aN()Lcom/my/target/bn;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lcom/my/target/bn;->aP()Lcom/my/target/bl;

    move-result-object v0

    .line 88
    invoke-virtual {p1}, Lcom/my/target/b;->isTrackingEnvironmentEnabled()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/my/target/bl;->g(Z)V

    .line 89
    invoke-static {}, Lcom/my/target/bn;->aN()Lcom/my/target/bn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/bn;->aP()Lcom/my/target/bl;

    move-result-object v0

    invoke-virtual {p1}, Lcom/my/target/b;->isTrackingLocationEnabled()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/my/target/bl;->h(Z)V

    .line 91
    invoke-static {}, Lcom/my/target/bn;->aN()Lcom/my/target/bn;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/my/target/bn;->collectData(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    :goto_2
    invoke-static {}, Lcom/my/target/bn;->aN()Lcom/my/target/bn;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/my/target/bn;->putDataTo(Ljava/util/Map;)V

    move-object v0, v1

    .line 99
    goto :goto_1

    .line 93
    :catch_0
    move-exception v0

    .line 95
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error collecting data: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_2
.end method
