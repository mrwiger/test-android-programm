.class public final Lru/cn/api/iptv/XSPFChannelsParser;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "XSPFChannelsParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/api/iptv/XSPFChannelsParser$XSPFParseException;
    }
.end annotation


# instance fields
.field private builder:Ljava/lang/StringBuilder;

.field private final content:Ljava/lang/String;

.field public final ignoredExceptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Exception;",
            ">;"
        }
    .end annotation
.end field

.field private locationParams:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private locations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .param p1, "content"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Exception;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p2, "ignoredExceptions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Exception;>;"
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lru/cn/api/iptv/XSPFChannelsParser;->locationParams:Ljava/util/Map;

    .line 47
    iput-object p2, p0, Lru/cn/api/iptv/XSPFChannelsParser;->ignoredExceptions:Ljava/util/List;

    .line 48
    iput-object p1, p0, Lru/cn/api/iptv/XSPFChannelsParser;->content:Ljava/lang/String;

    .line 49
    return-void
.end method

.method private static buildLocation(Ljava/util/Map;)Lru/cn/api/iptv/replies/MediaLocation;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lru/cn/api/iptv/replies/MediaLocation;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lru/cn/api/iptv/XSPFChannelsParser$XSPFParseException;
        }
    .end annotation

    .prologue
    .local p0, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v12, 0x32

    const/4 v11, 0x2

    const/4 v10, 0x0

    .line 141
    const-string v8, "location"

    invoke-interface {p0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 142
    .local v7, "value":Ljava/lang/String;
    if-nez v7, :cond_1

    .line 143
    const/4 v5, 0x0

    .line 230
    :cond_0
    :goto_0
    return-object v5

    .line 145
    :cond_1
    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 146
    .local v6, "uri":Landroid/net/Uri;
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Landroid/net/Uri;->isAbsolute()Z

    move-result v8

    if-nez v8, :cond_3

    .line 147
    :cond_2
    new-instance v8, Lru/cn/api/iptv/XSPFChannelsParser$XSPFParseException;

    const-string v9, "Incorrect URL"

    const/16 v10, 0x3eb

    invoke-direct {v8, v9, v10}, Lru/cn/api/iptv/XSPFChannelsParser$XSPFParseException;-><init>(Ljava/lang/String;I)V

    throw v8

    .line 150
    :cond_3
    new-instance v5, Lru/cn/api/iptv/replies/MediaLocation;

    invoke-direct {v5, v7}, Lru/cn/api/iptv/replies/MediaLocation;-><init>(Ljava/lang/String;)V

    .line 151
    .local v5, "location":Lru/cn/api/iptv/replies/MediaLocation;
    const-string v8, "title"

    invoke-interface {p0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, v5, Lru/cn/api/iptv/replies/MediaLocation;->title:Ljava/lang/String;

    .line 153
    const-string v8, "channel-id"

    invoke-interface {p0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "value":Ljava/lang/String;
    check-cast v7, Ljava/lang/String;

    .line 154
    .restart local v7    # "value":Ljava/lang/String;
    if-eqz v7, :cond_4

    .line 156
    :try_start_0
    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, v5, Lru/cn/api/iptv/replies/MediaLocation;->channelId:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    :cond_4
    :goto_1
    const-string v8, "territory-id"

    invoke-interface {p0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "value":Ljava/lang/String;
    check-cast v7, Ljava/lang/String;

    .line 163
    .restart local v7    # "value":Ljava/lang/String;
    if-eqz v7, :cond_5

    .line 165
    :try_start_1
    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, v5, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 171
    :cond_5
    :goto_2
    const-string v8, "recordable"

    invoke-interface {p0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "value":Ljava/lang/String;
    check-cast v7, Ljava/lang/String;

    .line 172
    .restart local v7    # "value":Ljava/lang/String;
    if-eqz v7, :cond_6

    .line 173
    invoke-static {v7}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v8

    iput-boolean v8, v5, Lru/cn/api/iptv/replies/MediaLocation;->hasRecords:Z

    .line 176
    :cond_6
    const-string v8, "has-timeshift"

    invoke-interface {p0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "value":Ljava/lang/String;
    check-cast v7, Ljava/lang/String;

    .line 177
    .restart local v7    # "value":Ljava/lang/String;
    if-eqz v7, :cond_7

    .line 178
    invoke-static {v7}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v8

    iput-boolean v8, v5, Lru/cn/api/iptv/replies/MediaLocation;->hasTimeshift:Z

    .line 181
    :cond_7
    const-string v8, "crop"

    invoke-interface {p0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "value":Ljava/lang/String;
    check-cast v7, Ljava/lang/String;

    .line 182
    .restart local v7    # "value":Ljava/lang/String;
    if-eqz v7, :cond_c

    .line 183
    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 184
    .local v1, "crop":[Ljava/lang/String;
    const/16 v2, 0x64

    .line 185
    .local v2, "cropX":I
    const/16 v3, 0x64

    .line 186
    .local v3, "cropY":I
    array-length v8, v1

    if-ne v8, v11, :cond_b

    .line 188
    const/4 v8, 0x0

    :try_start_2
    aget-object v8, v1, v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    move-result v2

    .line 192
    :goto_3
    const/4 v8, 0x1

    :try_start_3
    aget-object v8, v1, v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move-result v3

    .line 195
    :goto_4
    const/16 v8, 0x64

    if-le v2, v8, :cond_8

    .line 196
    const/16 v2, 0x64

    .line 198
    :cond_8
    const/16 v8, 0x64

    if-le v3, v8, :cond_9

    .line 199
    const/16 v3, 0x64

    .line 201
    :cond_9
    if-ge v2, v12, :cond_a

    .line 202
    const/16 v2, 0x32

    .line 204
    :cond_a
    if-ge v3, v12, :cond_b

    .line 205
    const/16 v3, 0x32

    .line 207
    :cond_b
    iput v2, v5, Lru/cn/api/iptv/replies/MediaLocation;->cropX:I

    .line 208
    iput v3, v5, Lru/cn/api/iptv/replies/MediaLocation;->cropY:I

    .line 211
    .end local v1    # "crop":[Ljava/lang/String;
    .end local v2    # "cropX":I
    .end local v3    # "cropY":I
    :cond_c
    const-string v8, "aspect-ratio"

    invoke-interface {p0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "value":Ljava/lang/String;
    check-cast v7, Ljava/lang/String;

    .line 212
    .restart local v7    # "value":Ljava/lang/String;
    if-eqz v7, :cond_d

    .line 213
    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 214
    .local v0, "aspectRatio":[Ljava/lang/String;
    array-length v8, v0

    if-ne v8, v11, :cond_d

    .line 216
    const/4 v8, 0x0

    :try_start_4
    aget-object v8, v0, v8

    invoke-static {v8}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v8

    iput v8, v5, Lru/cn/api/iptv/replies/MediaLocation;->aspectW:F

    .line 217
    const/4 v8, 0x1

    aget-object v8, v0, v8

    invoke-static {v8}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v8

    iput v8, v5, Lru/cn/api/iptv/replies/MediaLocation;->aspectH:F
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 225
    .end local v0    # "aspectRatio":[Ljava/lang/String;
    :cond_d
    :goto_5
    const-string v8, "allowed-till"

    invoke-interface {p0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "value":Ljava/lang/String;
    check-cast v7, Ljava/lang/String;

    .line 226
    .restart local v7    # "value":Ljava/lang/String;
    if-eqz v7, :cond_0

    .line 227
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    iput v8, v5, Lru/cn/api/iptv/replies/MediaLocation;->allowedTill:I

    goto/16 :goto_0

    .line 157
    :catch_0
    move-exception v4

    .line 158
    .local v4, "e":Ljava/lang/Exception;
    const-string v8, "XSPFChannelsParser"

    const-string v9, "Unable to parse channel-id"

    invoke-static {v8, v9, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    .line 166
    .end local v4    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v4

    .line 167
    .restart local v4    # "e":Ljava/lang/Exception;
    const-string v8, "XSPFChannelsParser"

    const-string v9, "Unable to parse territory-id"

    invoke-static {v8, v9, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    .line 218
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v0    # "aspectRatio":[Ljava/lang/String;
    :catch_2
    move-exception v4

    .line 219
    .restart local v4    # "e":Ljava/lang/Exception;
    iput v10, v5, Lru/cn/api/iptv/replies/MediaLocation;->aspectW:F

    .line 220
    iput v10, v5, Lru/cn/api/iptv/replies/MediaLocation;->aspectH:F

    goto :goto_5

    .line 193
    .end local v0    # "aspectRatio":[Ljava/lang/String;
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v1    # "crop":[Ljava/lang/String;
    .restart local v2    # "cropX":I
    .restart local v3    # "cropY":I
    :catch_3
    move-exception v8

    goto :goto_4

    .line 189
    :catch_4
    move-exception v8

    goto :goto_3
.end method


# virtual methods
.method public characters([CII)V
    .locals 1
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 75
    invoke-super {p0, p1, p2, p3}, Lorg/xml/sax/helpers/DefaultHandler;->characters([CII)V

    .line 77
    iget-object v0, p0, Lru/cn/api/iptv/XSPFChannelsParser;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2, p3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 78
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "namespaceURI"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 95
    invoke-super {p0, p1, p2, p3}, Lorg/xml/sax/helpers/DefaultHandler;->endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iget-object v3, p0, Lru/cn/api/iptv/XSPFChannelsParser;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 99
    .local v2, "trimmedString":Ljava/lang/String;
    const-string v3, "title"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 100
    iget-object v3, p0, Lru/cn/api/iptv/XSPFChannelsParser;->locationParams:Ljava/util/Map;

    const-string v4, "title"

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    :cond_0
    :goto_0
    iget-object v3, p0, Lru/cn/api/iptv/XSPFChannelsParser;->builder:Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 138
    return-void

    .line 101
    :cond_1
    const-string v3, "location"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 102
    iget-object v3, p0, Lru/cn/api/iptv/XSPFChannelsParser;->locationParams:Ljava/util/Map;

    const-string v4, "location"

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 103
    :cond_2
    const-string v3, "track"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 104
    const/4 v1, 0x0

    .line 106
    .local v1, "location":Lru/cn/api/iptv/replies/MediaLocation;
    :try_start_0
    iget-object v3, p0, Lru/cn/api/iptv/XSPFChannelsParser;->locationParams:Ljava/util/Map;

    invoke-static {v3}, Lru/cn/api/iptv/XSPFChannelsParser;->buildLocation(Ljava/util/Map;)Lru/cn/api/iptv/replies/MediaLocation;

    move-result-object v1

    .line 107
    if-eqz v1, :cond_3

    .line 108
    iget-object v3, p0, Lru/cn/api/iptv/XSPFChannelsParser;->locations:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lru/cn/api/iptv/XSPFChannelsParser$XSPFParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :cond_3
    :goto_1
    iget-object v3, p0, Lru/cn/api/iptv/XSPFChannelsParser;->locationParams:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    goto :goto_0

    .line 111
    :catch_0
    move-exception v0

    .line 112
    .local v0, "e":Lru/cn/api/iptv/XSPFChannelsParser$XSPFParseException;
    iget-object v3, p0, Lru/cn/api/iptv/XSPFChannelsParser;->ignoredExceptions:Ljava/util/List;

    if-eqz v3, :cond_4

    iget v3, v0, Lru/cn/api/iptv/XSPFChannelsParser$XSPFParseException;->code:I

    const/16 v4, 0x3eb

    if-ne v3, v4, :cond_4

    .line 113
    iget-object v3, p0, Lru/cn/api/iptv/XSPFChannelsParser;->ignoredExceptions:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 116
    :cond_4
    new-instance v3, Lorg/xml/sax/SAXException;

    invoke-direct {v3, v0}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/Exception;)V

    throw v3

    .line 121
    .end local v0    # "e":Lru/cn/api/iptv/XSPFChannelsParser$XSPFParseException;
    .end local v1    # "location":Lru/cn/api/iptv/replies/MediaLocation;
    :cond_5
    const-string v3, "channel-id"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 122
    iget-object v3, p0, Lru/cn/api/iptv/XSPFChannelsParser;->locationParams:Ljava/util/Map;

    const-string v4, "channel-id"

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 123
    :cond_6
    const-string v3, "territory-id"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 124
    iget-object v3, p0, Lru/cn/api/iptv/XSPFChannelsParser;->locationParams:Ljava/util/Map;

    const-string v4, "territory-id"

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 125
    :cond_7
    const-string v3, "recordable"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 126
    iget-object v3, p0, Lru/cn/api/iptv/XSPFChannelsParser;->locationParams:Ljava/util/Map;

    const-string v4, "recordable"

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 127
    :cond_8
    const-string v3, "crop"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 128
    iget-object v3, p0, Lru/cn/api/iptv/XSPFChannelsParser;->locationParams:Ljava/util/Map;

    const-string v4, "crop"

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 129
    :cond_9
    const-string v3, "aspect-ratio"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 130
    iget-object v3, p0, Lru/cn/api/iptv/XSPFChannelsParser;->locationParams:Ljava/util/Map;

    const-string v4, "aspect-ratio"

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 131
    :cond_a
    const-string v3, "allowed-till"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 132
    iget-object v3, p0, Lru/cn/api/iptv/XSPFChannelsParser;->locationParams:Ljava/util/Map;

    const-string v4, "allowed-till"

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 133
    :cond_b
    const-string v3, "has-timeshift"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 134
    iget-object v3, p0, Lru/cn/api/iptv/XSPFChannelsParser;->locationParams:Ljava/util/Map;

    const-string v4, "has-timeshift"

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method public getLocations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 234
    iget-object v0, p0, Lru/cn/api/iptv/XSPFChannelsParser;->locations:Ljava/util/List;

    return-object v0
.end method

.method public parse()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xml/sax/SAXException;,
            Ljavax/xml/parsers/ParserConfigurationException;
        }
    .end annotation

    .prologue
    .line 52
    new-instance v2, Lorg/xml/sax/InputSource;

    new-instance v4, Ljava/io/StringReader;

    iget-object v5, p0, Lru/cn/api/iptv/XSPFChannelsParser;->content:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v4}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    .line 54
    .local v2, "stream":Lorg/xml/sax/InputSource;
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v0

    .line 55
    .local v0, "saxFactory":Ljavax/xml/parsers/SAXParserFactory;
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljavax/xml/parsers/SAXParserFactory;->setValidating(Z)V

    .line 56
    invoke-virtual {v0}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v1

    .line 58
    .local v1, "saxParser":Ljavax/xml/parsers/SAXParser;
    invoke-virtual {v1}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v3

    .line 59
    .local v3, "xmlReader":Lorg/xml/sax/XMLReader;
    invoke-interface {v3, p0}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 61
    invoke-interface {v3, v2}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 62
    return-void
.end method

.method public startDocument()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 66
    invoke-super {p0}, Lorg/xml/sax/helpers/DefaultHandler;->startDocument()V

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lru/cn/api/iptv/XSPFChannelsParser;->builder:Ljava/lang/StringBuilder;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/api/iptv/XSPFChannelsParser;->locations:Ljava/util/List;

    .line 70
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "namespaceURI"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "atts"    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 83
    invoke-super {p0, p1, p2, p2, p4}, Lorg/xml/sax/helpers/DefaultHandler;->startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 85
    const-string v0, "track"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lru/cn/api/iptv/XSPFChannelsParser;->locationParams:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 88
    iget-object v0, p0, Lru/cn/api/iptv/XSPFChannelsParser;->builder:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 90
    :cond_0
    return-void
.end method
