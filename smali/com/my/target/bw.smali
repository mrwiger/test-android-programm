.class public Lcom/my/target/bw;
.super Landroid/widget/FrameLayout;
.source "CloseableLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/bw$a;
    }
.end annotation


# static fields
.field private static final iu:I = 0x1e

.field private static final iv:I = 0x32

.field private static final iw:I = 0x8


# instance fields
.field private final iA:I

.field private final iB:I

.field private final iC:Landroid/graphics/Rect;

.field private final iD:Landroid/graphics/Rect;

.field private final iE:Landroid/graphics/Rect;

.field private final iF:Landroid/graphics/Rect;

.field private final iG:Lcom/my/target/cm;

.field private iH:Lcom/my/target/bw$a;

.field private iI:Z

.field private iJ:Z

.field private final ix:I

.field private final iy:Landroid/graphics/drawable/BitmapDrawable;

.field private final iz:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/16 v2, 0x1e

    .line 48
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 34
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/my/target/bw;->iC:Landroid/graphics/Rect;

    .line 35
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/my/target/bw;->iD:Landroid/graphics/Rect;

    .line 36
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/my/target/bw;->iE:Landroid/graphics/Rect;

    .line 37
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/my/target/bw;->iF:Landroid/graphics/Rect;

    .line 49
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/bw;->iG:Lcom/my/target/cm;

    .line 50
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/my/target/bw;->iG:Lcom/my/target/cm;

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-static {v1}, Lcom/my/target/bq;->i(I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/my/target/bw;->iy:Landroid/graphics/drawable/BitmapDrawable;

    .line 52
    iget-object v0, p0, Lcom/my/target/bw;->iy:Landroid/graphics/drawable/BitmapDrawable;

    sget-object v1, Lcom/my/target/bw;->EMPTY_STATE_SET:[I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setState([I)Z

    .line 53
    iget-object v0, p0, Lcom/my/target/bw;->iy:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/BitmapDrawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 55
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/my/target/bw;->ix:I

    .line 57
    const/16 v0, 0x32

    invoke-static {v0, p1}, Lcom/my/target/cm;->a(ILandroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/my/target/bw;->iz:I

    .line 58
    invoke-static {v2, p1}, Lcom/my/target/cm;->a(ILandroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/my/target/bw;->iA:I

    .line 59
    const/16 v0, 0x8

    invoke-static {v0, p1}, Lcom/my/target/cm;->a(ILandroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/my/target/bw;->iB:I

    .line 61
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/my/target/bw;->setWillNotDraw(Z)V

    .line 62
    return-void
.end method

.method private a(ILandroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 189
    const v0, 0x800035

    invoke-static {v0, p1, p1, p2, p3}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 190
    return-void
.end method

.method private bc()V
    .locals 1

    .prologue
    .line 194
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/my/target/bw;->playSoundEffect(I)V

    .line 195
    iget-object v0, p0, Lcom/my/target/bw;->iH:Lcom/my/target/bw$a;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/my/target/bw;->iH:Lcom/my/target/bw$a;

    invoke-interface {v0}, Lcom/my/target/bw$a;->onClose()V

    .line 199
    :cond_0
    return-void
.end method


# virtual methods
.method b(III)Z
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/my/target/bw;->iD:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, p3

    if-lt p1, v0, :cond_0

    iget-object v0, p0, Lcom/my/target/bw;->iD:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, p3

    if-lt p2, v0, :cond_0

    iget-object v0, p0, Lcom/my/target/bw;->iD:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, p3

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/my/target/bw;->iD:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, p3

    if-ge p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bb()Z
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/my/target/bw;->iy:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->isVisible()Z

    move-result v0

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v3, 0x0

    .line 113
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 116
    iget-boolean v0, p0, Lcom/my/target/bw;->iI:Z

    if-eqz v0, :cond_0

    .line 118
    iput-boolean v3, p0, Lcom/my/target/bw;->iI:Z

    .line 120
    iget-object v0, p0, Lcom/my/target/bw;->iC:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/my/target/bw;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/my/target/bw;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 122
    iget v0, p0, Lcom/my/target/bw;->iz:I

    iget-object v1, p0, Lcom/my/target/bw;->iC:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/my/target/bw;->iD:Landroid/graphics/Rect;

    invoke-direct {p0, v0, v1, v2}, Lcom/my/target/bw;->a(ILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 125
    iget-object v0, p0, Lcom/my/target/bw;->iF:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/my/target/bw;->iD:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 126
    iget-object v0, p0, Lcom/my/target/bw;->iF:Landroid/graphics/Rect;

    iget v1, p0, Lcom/my/target/bw;->iB:I

    iget v2, p0, Lcom/my/target/bw;->iB:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->inset(II)V

    .line 129
    iget v0, p0, Lcom/my/target/bw;->iA:I

    iget-object v1, p0, Lcom/my/target/bw;->iF:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/my/target/bw;->iE:Landroid/graphics/Rect;

    invoke-direct {p0, v0, v1, v2}, Lcom/my/target/bw;->a(ILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 130
    iget-object v0, p0, Lcom/my/target/bw;->iy:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/my/target/bw;->iE:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/my/target/bw;->iy:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 137
    iget-object v0, p0, Lcom/my/target/bw;->iy:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 139
    :cond_1
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x0

    .line 148
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-eqz v1, :cond_0

    .line 157
    :goto_0
    return v0

    .line 155
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    .line 156
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    .line 157
    invoke-virtual {p0, v1, v2, v0}, Lcom/my/target/bw;->b(III)Z

    move-result v0

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "oldWidth"    # I
    .param p4, "oldHeight"    # I

    .prologue
    .line 183
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 184
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/bw;->iI:Z

    .line 185
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 83
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    .line 84
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    .line 85
    iget v4, p0, Lcom/my/target/bw;->ix:I

    invoke-virtual {p0, v2, v3, v4}, Lcom/my/target/bw;->b(III)Z

    move-result v2

    if-nez v2, :cond_0

    .line 87
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 107
    :goto_0
    return v0

    .line 91
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_1
    :goto_1
    :pswitch_0
    move v0, v1

    .line 107
    goto :goto_0

    .line 94
    :pswitch_1
    iput-boolean v1, p0, Lcom/my/target/bw;->iJ:Z

    goto :goto_1

    .line 97
    :pswitch_2
    iput-boolean v0, p0, Lcom/my/target/bw;->iJ:Z

    goto :goto_1

    .line 100
    :pswitch_3
    iget-boolean v2, p0, Lcom/my/target/bw;->iJ:Z

    if-eqz v2, :cond_1

    .line 102
    invoke-direct {p0}, Lcom/my/target/bw;->bc()V

    .line 103
    iput-boolean v0, p0, Lcom/my/target/bw;->iJ:Z

    goto :goto_1

    .line 91
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method setCloseBounds(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "closeBounds"    # Landroid/graphics/Rect;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/my/target/bw;->iD:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 169
    return-void
.end method

.method public setCloseVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 71
    iget-object v0, p0, Lcom/my/target/bw;->iy:Landroid/graphics/drawable/BitmapDrawable;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/graphics/drawable/BitmapDrawable;->setVisible(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/my/target/bw;->iD:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/my/target/bw;->invalidate(Landroid/graphics/Rect;)V

    .line 75
    :cond_0
    return-void
.end method

.method public setOnCloseListener(Lcom/my/target/bw$a;)V
    .locals 0
    .param p1, "onCloseListener"    # Lcom/my/target/bw$a;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/my/target/bw;->iH:Lcom/my/target/bw$a;

    .line 67
    return-void
.end method
