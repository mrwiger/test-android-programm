.class Lru/cn/view/VolumeControl$1;
.super Ljava/lang/Object;
.source "VolumeControl.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/view/VolumeControl;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/view/VolumeControl;


# direct methods
.method constructor <init>(Lru/cn/view/VolumeControl;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/view/VolumeControl;

    .prologue
    .line 73
    iput-object p1, p0, Lru/cn/view/VolumeControl$1;->this$0:Lru/cn/view/VolumeControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 76
    iget-object v0, p0, Lru/cn/view/VolumeControl$1;->this$0:Lru/cn/view/VolumeControl;

    invoke-static {v0}, Lru/cn/view/VolumeControl;->access$000(Lru/cn/view/VolumeControl;)Lru/cn/view/VolumeControl$VolumeControlListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 77
    iget-object v0, p0, Lru/cn/view/VolumeControl$1;->this$0:Lru/cn/view/VolumeControl;

    invoke-static {v0}, Lru/cn/view/VolumeControl;->access$100(Lru/cn/view/VolumeControl;)Landroid/media/AudioManager;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    if-eq p2, v0, :cond_0

    .line 78
    iget-object v0, p0, Lru/cn/view/VolumeControl$1;->this$0:Lru/cn/view/VolumeControl;

    invoke-static {v0, p2}, Lru/cn/view/VolumeControl;->access$200(Lru/cn/view/VolumeControl;I)V

    .line 81
    :cond_0
    iget-object v0, p0, Lru/cn/view/VolumeControl$1;->this$0:Lru/cn/view/VolumeControl;

    invoke-static {v0}, Lru/cn/view/VolumeControl;->access$300(Lru/cn/view/VolumeControl;)V

    .line 82
    iget-object v0, p0, Lru/cn/view/VolumeControl$1;->this$0:Lru/cn/view/VolumeControl;

    invoke-static {v0}, Lru/cn/view/VolumeControl;->access$000(Lru/cn/view/VolumeControl;)Lru/cn/view/VolumeControl$VolumeControlListener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/view/VolumeControl$VolumeControlListener;->change()V

    .line 84
    :cond_1
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 89
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 94
    return-void
.end method
