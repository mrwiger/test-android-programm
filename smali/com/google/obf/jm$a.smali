.class public Lcom/google/obf/jm$a;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/jm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<-TV;>;"
        }
    .end annotation
.end field

.field b:[Ljava/lang/Object;

.field c:I

.field d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/obf/jm$a;-><init>(I)V

    .line 2
    return-void
.end method

.method constructor <init>(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    mul-int/lit8 v0, p1, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/obf/jm$a;->b:[Ljava/lang/Object;

    .line 5
    iput v1, p0, Lcom/google/obf/jm$a;->c:I

    .line 6
    iput-boolean v1, p0, Lcom/google/obf/jm$a;->d:Z

    .line 7
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 8
    mul-int/lit8 v0, p1, 0x2

    iget-object v1, p0, Lcom/google/obf/jm$a;->b:[Ljava/lang/Object;

    array-length v1, v1

    if-le v0, v1, :cond_0

    .line 9
    iget-object v0, p0, Lcom/google/obf/jm$a;->b:[Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/obf/jm$a;->b:[Ljava/lang/Object;

    array-length v1, v1

    mul-int/lit8 v2, p1, 0x2

    .line 10
    invoke-static {v1, v2}, Lcom/google/obf/jj$a;->a(II)I

    move-result v1

    .line 11
    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/jm$a;->b:[Ljava/lang/Object;

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/jm$a;->d:Z

    .line 13
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/obf/jm$a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)",
            "Lcom/google/obf/jm$a",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 14
    iget v0, p0, Lcom/google/obf/jm$a;->c:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/obf/jm$a;->a(I)V

    .line 15
    invoke-static {p1, p2}, Lcom/google/obf/jf;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 16
    iget-object v0, p0, Lcom/google/obf/jm$a;->b:[Ljava/lang/Object;

    iget v1, p0, Lcom/google/obf/jm$a;->c:I

    mul-int/lit8 v1, v1, 0x2

    aput-object p1, v0, v1

    .line 17
    iget-object v0, p0, Lcom/google/obf/jm$a;->b:[Ljava/lang/Object;

    iget v1, p0, Lcom/google/obf/jm$a;->c:I

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    aput-object p2, v0, v1

    .line 18
    iget v0, p0, Lcom/google/obf/jm$a;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/jm$a;->c:I

    .line 19
    return-object p0
.end method

.method public a()Lcom/google/obf/jm;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/obf/jm",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/google/obf/jm$a;->b()V

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/jm$a;->d:Z

    .line 22
    iget v0, p0, Lcom/google/obf/jm$a;->c:I

    iget-object v1, p0, Lcom/google/obf/jm$a;->b:[Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/obf/jv;->a(I[Ljava/lang/Object;)Lcom/google/obf/jv;

    move-result-object v0

    return-object v0
.end method

.method b()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 23
    iget-object v0, p0, Lcom/google/obf/jm$a;->a:Ljava/util/Comparator;

    if-eqz v0, :cond_2

    .line 24
    iget-boolean v0, p0, Lcom/google/obf/jm$a;->d:Z

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p0, Lcom/google/obf/jm$a;->b:[Ljava/lang/Object;

    iget v2, p0, Lcom/google/obf/jm$a;->c:I

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/jm$a;->b:[Ljava/lang/Object;

    .line 26
    :cond_0
    iget v0, p0, Lcom/google/obf/jm$a;->c:I

    new-array v2, v0, [Ljava/util/Map$Entry;

    move v0, v1

    .line 27
    :goto_0
    iget v3, p0, Lcom/google/obf/jm$a;->c:I

    if-ge v0, v3, :cond_1

    .line 28
    new-instance v3, Ljava/util/AbstractMap$SimpleImmutableEntry;

    iget-object v4, p0, Lcom/google/obf/jm$a;->b:[Ljava/lang/Object;

    mul-int/lit8 v5, v0, 0x2

    aget-object v4, v4, v5

    iget-object v5, p0, Lcom/google/obf/jm$a;->b:[Ljava/lang/Object;

    mul-int/lit8 v6, v0, 0x2

    add-int/lit8 v6, v6, 0x1

    aget-object v5, v5, v6

    invoke-direct {v3, v4, v5}, Ljava/util/AbstractMap$SimpleImmutableEntry;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v2, v0

    .line 29
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 30
    :cond_1
    iget v0, p0, Lcom/google/obf/jm$a;->c:I

    iget-object v3, p0, Lcom/google/obf/jm$a;->a:Ljava/util/Comparator;

    .line 31
    invoke-static {v3}, Lcom/google/obf/js;->a(Ljava/util/Comparator;)Lcom/google/obf/js;

    move-result-object v3

    invoke-static {}, Lcom/google/obf/jq;->a()Lcom/google/obf/iw;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/obf/js;->a(Lcom/google/obf/iw;)Lcom/google/obf/js;

    move-result-object v3

    .line 32
    invoke-static {v2, v1, v0, v3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 33
    :goto_1
    iget v0, p0, Lcom/google/obf/jm$a;->c:I

    if-ge v1, v0, :cond_2

    .line 34
    iget-object v0, p0, Lcom/google/obf/jm$a;->b:[Ljava/lang/Object;

    mul-int/lit8 v3, v1, 0x2

    aget-object v4, v2, v1

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v0, v3

    .line 35
    iget-object v0, p0, Lcom/google/obf/jm$a;->b:[Ljava/lang/Object;

    mul-int/lit8 v3, v1, 0x2

    add-int/lit8 v3, v3, 0x1

    aget-object v4, v2, v1

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v0, v3

    .line 36
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 37
    :cond_2
    return-void
.end method
