.class final Lcom/my/target/core/presenters/d$b;
.super Ljava/lang/Object;
.source "InterstitialHtmlPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/core/presenters/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private M:Z

.field private N:Z

.field private O:Z


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 406
    invoke-direct {p0}, Lcom/my/target/core/presenters/d$b;-><init>()V

    return-void
.end method


# virtual methods
.method final isPaused()Z
    .locals 1

    .prologue
    .line 414
    iget-boolean v0, p0, Lcom/my/target/core/presenters/d$b;->N:Z

    return v0
.end method

.method final isReady()Z
    .locals 1

    .prologue
    .line 419
    iget-boolean v0, p0, Lcom/my/target/core/presenters/d$b;->O:Z

    return v0
.end method

.method final isStarted()Z
    .locals 1

    .prologue
    .line 424
    iget-boolean v0, p0, Lcom/my/target/core/presenters/d$b;->M:Z

    return v0
.end method

.method final reset()V
    .locals 1

    .prologue
    .line 444
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/core/presenters/d$b;->O:Z

    iput-boolean v0, p0, Lcom/my/target/core/presenters/d$b;->N:Z

    iput-boolean v0, p0, Lcom/my/target/core/presenters/d$b;->M:Z

    .line 445
    return-void
.end method

.method final setPaused(Z)V
    .locals 0
    .param p1, "paused"    # Z

    .prologue
    .line 429
    iput-boolean p1, p0, Lcom/my/target/core/presenters/d$b;->N:Z

    .line 430
    return-void
.end method

.method final setStarted(Z)V
    .locals 0
    .param p1, "started"    # Z

    .prologue
    .line 439
    iput-boolean p1, p0, Lcom/my/target/core/presenters/d$b;->M:Z

    .line 440
    return-void
.end method

.method final t()V
    .locals 1

    .prologue
    .line 434
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/core/presenters/d$b;->O:Z

    .line 435
    return-void
.end method
