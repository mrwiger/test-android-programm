.class public abstract Lcom/google/obf/ii;
.super Lcom/google/obf/ih;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/ii$a;
    }
.end annotation


# static fields
.field static d:Z

.field private static e:Ljava/lang/reflect/Method;

.field private static f:Ljava/lang/reflect/Method;

.field private static g:Ljava/lang/reflect/Method;

.field private static h:Ljava/lang/reflect/Method;

.field private static i:Ljava/lang/reflect/Method;

.field private static j:Ljava/lang/reflect/Method;

.field private static k:Ljava/lang/reflect/Method;

.field private static l:Ljava/lang/reflect/Method;

.field private static m:Ljava/lang/reflect/Method;

.field private static n:Ljava/lang/reflect/Method;

.field private static o:Ljava/lang/reflect/Method;

.field private static p:Ljava/lang/reflect/Method;

.field private static q:Ljava/lang/reflect/Method;

.field private static r:Ljava/lang/String;

.field private static s:Ljava/lang/String;

.field private static t:Ljava/lang/String;

.field private static u:J

.field private static v:Lcom/google/obf/io;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 327
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/google/obf/ii;->u:J

    .line 328
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/obf/ii;->d:Z

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/google/obf/im;Lcom/google/obf/in;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0, p1, p2, p3}, Lcom/google/obf/ih;-><init>(Landroid/content/Context;Lcom/google/obf/im;Lcom/google/obf/in;)V

    .line 13
    return-void
.end method

.method static a()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/ii$a;
        }
    .end annotation

    .prologue
    .line 88
    sget-object v0, Lcom/google/obf/ii;->r:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 89
    new-instance v0, Lcom/google/obf/ii$a;

    invoke-direct {v0}, Lcom/google/obf/ii$a;-><init>()V

    throw v0

    .line 90
    :cond_0
    sget-object v0, Lcom/google/obf/ii;->r:Ljava/lang/String;

    return-object v0
.end method

.method static a(Landroid/content/Context;Lcom/google/obf/im;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/ii$a;
        }
    .end annotation

    .prologue
    .line 140
    sget-object v0, Lcom/google/obf/ii;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 141
    sget-object v0, Lcom/google/obf/ii;->s:Ljava/lang/String;

    .line 149
    :goto_0
    return-object v0

    .line 142
    :cond_0
    sget-object v0, Lcom/google/obf/ii;->h:Ljava/lang/reflect/Method;

    if-nez v0, :cond_1

    .line 143
    new-instance v0, Lcom/google/obf/ii$a;

    invoke-direct {v0}, Lcom/google/obf/ii$a;-><init>()V

    throw v0

    .line 144
    :cond_1
    :try_start_0
    sget-object v0, Lcom/google/obf/ii;->h:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    .line 145
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    .line 146
    if-nez v0, :cond_2

    .line 147
    new-instance v0, Lcom/google/obf/ii$a;

    invoke-direct {v0}, Lcom/google/obf/ii$a;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    .line 150
    :catch_0
    move-exception v0

    .line 151
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 148
    :cond_2
    :try_start_1
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Lcom/google/obf/im;->a([BZ)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/obf/ii;->s:Ljava/lang/String;

    .line 149
    sget-object v0, Lcom/google/obf/ii;->s:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 152
    :catch_1
    move-exception v0

    .line 153
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static a(Landroid/view/MotionEvent;Landroid/util/DisplayMetrics;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/MotionEvent;",
            "Landroid/util/DisplayMetrics;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/ii$a;
        }
    .end annotation

    .prologue
    .line 115
    sget-object v0, Lcom/google/obf/ii;->i:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    if-nez p0, :cond_1

    .line 116
    :cond_0
    new-instance v0, Lcom/google/obf/ii$a;

    invoke-direct {v0}, Lcom/google/obf/ii$a;-><init>()V

    throw v0

    .line 117
    :cond_1
    :try_start_0
    sget-object v0, Lcom/google/obf/ii;->i:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    .line 118
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    .line 119
    return-object v0

    .line 120
    :catch_0
    move-exception v0

    .line 121
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 122
    :catch_1
    move-exception v0

    .line 123
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected static declared-synchronized a(Ljava/lang/String;Landroid/content/Context;Lcom/google/obf/im;)V
    .locals 4

    .prologue
    .line 1
    const-class v1, Lcom/google/obf/ii;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/google/obf/ii;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 2
    :try_start_1
    new-instance v0, Lcom/google/obf/io;

    const/4 v2, 0x0

    invoke-direct {v0, p2, v2}, Lcom/google/obf/io;-><init>(Lcom/google/obf/im;Ljava/security/SecureRandom;)V

    sput-object v0, Lcom/google/obf/ii;->v:Lcom/google/obf/io;

    .line 3
    sput-object p0, Lcom/google/obf/ii;->r:Ljava/lang/String;

    .line 4
    invoke-static {p1}, Lcom/google/obf/ii;->k(Landroid/content/Context;)V

    .line 5
    invoke-static {}, Lcom/google/obf/ii;->b()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sput-wide v2, Lcom/google/obf/ii;->u:J

    .line 6
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/obf/ii;->d:Z
    :try_end_1
    .catch Lcom/google/obf/ii$a; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 11
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 1
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 10
    :catch_0
    move-exception v0

    goto :goto_0

    .line 8
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method static b()Ljava/lang/Long;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/ii$a;
        }
    .end annotation

    .prologue
    .line 91
    sget-object v0, Lcom/google/obf/ii;->e:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 92
    new-instance v0, Lcom/google/obf/ii$a;

    invoke-direct {v0}, Lcom/google/obf/ii$a;-><init>()V

    throw v0

    .line 93
    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/obf/ii;->e:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 94
    :catch_0
    move-exception v0

    .line 95
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 96
    :catch_1
    move-exception v0

    .line 97
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static b(Landroid/content/Context;Lcom/google/obf/im;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/ii$a;
        }
    .end annotation

    .prologue
    .line 165
    sget-object v0, Lcom/google/obf/ii;->t:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 166
    sget-object v0, Lcom/google/obf/ii;->t:Ljava/lang/String;

    .line 173
    :goto_0
    return-object v0

    .line 167
    :cond_0
    sget-object v0, Lcom/google/obf/ii;->k:Ljava/lang/reflect/Method;

    if-nez v0, :cond_1

    .line 168
    new-instance v0, Lcom/google/obf/ii$a;

    invoke-direct {v0}, Lcom/google/obf/ii$a;-><init>()V

    throw v0

    .line 169
    :cond_1
    :try_start_0
    sget-object v0, Lcom/google/obf/ii;->k:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    .line 170
    if-nez v0, :cond_2

    .line 171
    new-instance v0, Lcom/google/obf/ii$a;

    invoke-direct {v0}, Lcom/google/obf/ii$a;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    .line 174
    :catch_0
    move-exception v0

    .line 175
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 172
    :cond_2
    :try_start_1
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Lcom/google/obf/im;->a([BZ)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/obf/ii;->t:Ljava/lang/String;

    .line 173
    sget-object v0, Lcom/google/obf/ii;->t:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 176
    :catch_1
    move-exception v0

    .line 177
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static b([BLjava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/ii$a;
        }
    .end annotation

    .prologue
    .line 206
    :try_start_0
    new-instance v0, Ljava/lang/String;

    sget-object v1, Lcom/google/obf/ii;->v:Lcom/google/obf/io;

    .line 207
    invoke-virtual {v1, p0, p1}, Lcom/google/obf/io;->a([BLjava/lang/String;)[B

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Lcom/google/obf/io$a; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 208
    :catch_0
    move-exception v0

    .line 209
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 210
    :catch_1
    move-exception v0

    .line 211
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static c()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/ii$a;
        }
    .end annotation

    .prologue
    .line 108
    sget-object v0, Lcom/google/obf/ii;->g:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 109
    new-instance v0, Lcom/google/obf/ii$a;

    invoke-direct {v0}, Lcom/google/obf/ii$a;-><init>()V

    throw v0

    .line 110
    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/obf/ii;->g:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 111
    :catch_0
    move-exception v0

    .line 112
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 113
    :catch_1
    move-exception v0

    .line 114
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static d()Ljava/lang/Long;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/ii$a;
        }
    .end annotation

    .prologue
    .line 178
    sget-object v0, Lcom/google/obf/ii;->f:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 179
    new-instance v0, Lcom/google/obf/ii$a;

    invoke-direct {v0}, Lcom/google/obf/ii$a;-><init>()V

    throw v0

    .line 180
    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/obf/ii;->f:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 181
    :catch_0
    move-exception v0

    .line 182
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 183
    :catch_1
    move-exception v0

    .line 184
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static d(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/ii$a;
        }
    .end annotation

    .prologue
    .line 98
    sget-object v0, Lcom/google/obf/ii;->j:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 99
    new-instance v0, Lcom/google/obf/ii$a;

    invoke-direct {v0}, Lcom/google/obf/ii$a;-><init>()V

    throw v0

    .line 100
    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/obf/ii;->j:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 101
    if-nez v0, :cond_1

    .line 102
    new-instance v0, Lcom/google/obf/ii$a;

    invoke-direct {v0}, Lcom/google/obf/ii$a;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    .line 104
    :catch_0
    move-exception v0

    .line 105
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 106
    :catch_1
    move-exception v0

    .line 107
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 103
    :cond_1
    return-object v0
.end method

.method static e(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/ii$a;
        }
    .end annotation

    .prologue
    .line 124
    sget-object v0, Lcom/google/obf/ii;->n:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 125
    new-instance v0, Lcom/google/obf/ii$a;

    invoke-direct {v0}, Lcom/google/obf/ii$a;-><init>()V

    throw v0

    .line 126
    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/obf/ii;->n:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    .line 127
    return-object v0

    .line 128
    :catch_0
    move-exception v0

    .line 129
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 130
    :catch_1
    move-exception v0

    .line 131
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static f(Landroid/content/Context;)Ljava/lang/Long;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/ii$a;
        }
    .end annotation

    .prologue
    .line 132
    sget-object v0, Lcom/google/obf/ii;->o:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 133
    new-instance v0, Lcom/google/obf/ii$a;

    invoke-direct {v0}, Lcom/google/obf/ii$a;-><init>()V

    throw v0

    .line 134
    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/obf/ii;->o:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    .line 135
    return-object v0

    .line 136
    :catch_0
    move-exception v0

    .line 137
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 138
    :catch_1
    move-exception v0

    .line 139
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static g(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/ii$a;
        }
    .end annotation

    .prologue
    .line 154
    sget-object v0, Lcom/google/obf/ii;->l:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 155
    new-instance v0, Lcom/google/obf/ii$a;

    invoke-direct {v0}, Lcom/google/obf/ii$a;-><init>()V

    throw v0

    .line 156
    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/obf/ii;->l:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    .line 157
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 158
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    .line 159
    :cond_1
    new-instance v0, Lcom/google/obf/ii$a;

    invoke-direct {v0}, Lcom/google/obf/ii$a;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    .line 161
    :catch_0
    move-exception v0

    .line 162
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 163
    :catch_1
    move-exception v0

    .line 164
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 160
    :cond_2
    return-object v0
.end method

.method static h(Landroid/content/Context;)[I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/ii$a;
        }
    .end annotation

    .prologue
    .line 185
    sget-object v0, Lcom/google/obf/ii;->m:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 186
    new-instance v0, Lcom/google/obf/ii$a;

    invoke-direct {v0}, Lcom/google/obf/ii$a;-><init>()V

    throw v0

    .line 187
    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/obf/ii;->m:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 188
    :catch_0
    move-exception v0

    .line 189
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 190
    :catch_1
    move-exception v0

    .line 191
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static i(Landroid/content/Context;)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/ii$a;
        }
    .end annotation

    .prologue
    .line 192
    sget-object v0, Lcom/google/obf/ii;->p:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 193
    new-instance v0, Lcom/google/obf/ii$a;

    invoke-direct {v0}, Lcom/google/obf/ii$a;-><init>()V

    throw v0

    .line 194
    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/obf/ii;->p:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    return v0

    .line 195
    :catch_0
    move-exception v0

    .line 196
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 197
    :catch_1
    move-exception v0

    .line 198
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static j(Landroid/content/Context;)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/ii$a;
        }
    .end annotation

    .prologue
    .line 199
    sget-object v0, Lcom/google/obf/ii;->q:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 200
    new-instance v0, Lcom/google/obf/ii$a;

    invoke-direct {v0}, Lcom/google/obf/ii$a;-><init>()V

    throw v0

    .line 201
    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/obf/ii;->q:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    return v0

    .line 202
    :catch_0
    move-exception v0

    .line 203
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 204
    :catch_1
    move-exception v0

    .line 205
    new-instance v1, Lcom/google/obf/ii$a;

    invoke-direct {v1, v0}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static k(Landroid/content/Context;)V
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/ii$a;
        }
    .end annotation

    .prologue
    .line 212
    :try_start_0
    sget-object v2, Lcom/google/obf/ii;->v:Lcom/google/obf/io;

    invoke-static {}, Lcom/google/obf/iq;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/obf/io;->a(Ljava/lang/String;)[B

    move-result-object v4

    .line 213
    sget-object v2, Lcom/google/obf/ii;->v:Lcom/google/obf/io;

    invoke-static {}, Lcom/google/obf/iq;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Lcom/google/obf/io;->a([BLjava/lang/String;)[B

    move-result-object v5

    .line 214
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    .line 215
    if-nez v2, :cond_0

    .line 216
    const-string v2, "dex"

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v2

    .line 217
    if-nez v2, :cond_0

    .line 218
    new-instance v2, Lcom/google/obf/ii$a;

    invoke-direct {v2}, Lcom/google/obf/ii$a;-><init>()V

    throw v2
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/obf/io$a; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_5

    .line 314
    :catch_0
    move-exception v2

    .line 315
    new-instance v3, Lcom/google/obf/ii$a;

    invoke-direct {v3, v2}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v3

    :cond_0
    move-object v3, v2

    .line 219
    :try_start_1
    const-string v2, "ads"

    const-string v6, ".jar"

    invoke-static {v2, v6, v3}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v6

    .line 220
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 221
    const/4 v7, 0x0

    array-length v8, v5

    invoke-virtual {v2, v5, v7, v8}, Ljava/io/FileOutputStream;->write([BII)V

    .line 222
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/google/obf/io$a; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_5

    .line 223
    :try_start_2
    new-instance v2, Ldalvik/system/DexClassLoader;

    .line 224
    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    .line 225
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    .line 226
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v9

    invoke-direct {v2, v5, v7, v8, v9}, Ldalvik/system/DexClassLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 228
    invoke-static {}, Lcom/google/obf/iq;->k()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 229
    invoke-virtual {v2, v5}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    .line 231
    invoke-static {}, Lcom/google/obf/iq;->y()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 232
    invoke-virtual {v2, v7}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    .line 234
    invoke-static {}, Lcom/google/obf/iq;->s()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 235
    invoke-virtual {v2, v8}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    .line 237
    invoke-static {}, Lcom/google/obf/iq;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v9}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 238
    invoke-virtual {v2, v9}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v9

    .line 240
    invoke-static {}, Lcom/google/obf/iq;->A()Ljava/lang/String;

    move-result-object v10

    invoke-static {v4, v10}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 241
    invoke-virtual {v2, v10}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v10

    .line 243
    invoke-static {}, Lcom/google/obf/iq;->m()Ljava/lang/String;

    move-result-object v11

    invoke-static {v4, v11}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 244
    invoke-virtual {v2, v11}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v11

    .line 246
    invoke-static {}, Lcom/google/obf/iq;->w()Ljava/lang/String;

    move-result-object v12

    invoke-static {v4, v12}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 247
    invoke-virtual {v2, v12}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v12

    .line 249
    invoke-static {}, Lcom/google/obf/iq;->u()Ljava/lang/String;

    move-result-object v13

    invoke-static {v4, v13}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 250
    invoke-virtual {v2, v13}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v13

    .line 252
    invoke-static {}, Lcom/google/obf/iq;->i()Ljava/lang/String;

    move-result-object v14

    invoke-static {v4, v14}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 253
    invoke-virtual {v2, v14}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v14

    .line 255
    invoke-static {}, Lcom/google/obf/iq;->g()Ljava/lang/String;

    move-result-object v15

    invoke-static {v4, v15}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 256
    invoke-virtual {v2, v15}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v15

    .line 258
    invoke-static {}, Lcom/google/obf/iq;->e()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v4, v0}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 259
    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v16

    .line 261
    invoke-static {}, Lcom/google/obf/iq;->q()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v4, v0}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 262
    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v17

    .line 264
    invoke-static {}, Lcom/google/obf/iq;->c()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v4, v0}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 265
    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 267
    invoke-static {}, Lcom/google/obf/iq;->l()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v4, v0}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x0

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v19, v0

    .line 268
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    sput-object v5, Lcom/google/obf/ii;->e:Ljava/lang/reflect/Method;

    .line 270
    invoke-static {}, Lcom/google/obf/iq;->z()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v18, v0

    .line 271
    move-object/from16 v0, v18

    invoke-virtual {v7, v5, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    sput-object v5, Lcom/google/obf/ii;->f:Ljava/lang/reflect/Method;

    .line 273
    invoke-static {}, Lcom/google/obf/iq;->t()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Class;

    .line 274
    invoke-virtual {v8, v5, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    sput-object v5, Lcom/google/obf/ii;->g:Ljava/lang/reflect/Method;

    .line 276
    invoke-static {}, Lcom/google/obf/iq;->p()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v18, Landroid/content/Context;

    aput-object v18, v7, v8

    .line 277
    invoke-virtual {v9, v5, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    sput-object v5, Lcom/google/obf/ii;->h:Ljava/lang/reflect/Method;

    .line 279
    invoke-static {}, Lcom/google/obf/iq;->B()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Landroid/view/MotionEvent;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-class v9, Landroid/util/DisplayMetrics;

    aput-object v9, v7, v8

    .line 280
    invoke-virtual {v10, v5, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    sput-object v5, Lcom/google/obf/ii;->i:Ljava/lang/reflect/Method;

    .line 282
    invoke-static {}, Lcom/google/obf/iq;->n()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v7, v8

    .line 283
    invoke-virtual {v11, v5, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    sput-object v5, Lcom/google/obf/ii;->j:Ljava/lang/reflect/Method;

    .line 285
    invoke-static {}, Lcom/google/obf/iq;->x()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v7, v8

    .line 286
    invoke-virtual {v12, v5, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    sput-object v5, Lcom/google/obf/ii;->k:Ljava/lang/reflect/Method;

    .line 288
    invoke-static {}, Lcom/google/obf/iq;->v()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v7, v8

    .line 289
    invoke-virtual {v13, v5, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    sput-object v5, Lcom/google/obf/ii;->l:Ljava/lang/reflect/Method;

    .line 291
    invoke-static {}, Lcom/google/obf/iq;->j()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v7, v8

    .line 292
    invoke-virtual {v14, v5, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    sput-object v5, Lcom/google/obf/ii;->m:Ljava/lang/reflect/Method;

    .line 294
    invoke-static {}, Lcom/google/obf/iq;->h()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v7, v8

    .line 295
    invoke-virtual {v15, v5, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    sput-object v5, Lcom/google/obf/ii;->n:Ljava/lang/reflect/Method;

    .line 297
    invoke-static {}, Lcom/google/obf/iq;->f()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v7, v8

    .line 298
    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    sput-object v5, Lcom/google/obf/ii;->o:Ljava/lang/reflect/Method;

    .line 300
    invoke-static {}, Lcom/google/obf/iq;->r()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v7, v8

    .line 301
    move-object/from16 v0, v17

    invoke-virtual {v0, v5, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    sput-object v5, Lcom/google/obf/ii;->p:Ljava/lang/reflect/Method;

    .line 303
    invoke-static {}, Lcom/google/obf/iq;->d()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/obf/ii;->b([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/content/Context;

    aput-object v8, v5, v7

    .line 304
    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Lcom/google/obf/ii;->q:Ljava/lang/reflect/Method;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 305
    :try_start_3
    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    .line 306
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 307
    new-instance v4, Ljava/io/File;

    const-string v5, ".jar"

    const-string v6, ".dex"

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v3, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 326
    return-void

    .line 309
    :catchall_0
    move-exception v2

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    .line 310
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 311
    new-instance v5, Ljava/io/File;

    const-string v6, ".jar"

    const-string v7, ".dex"

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 312
    throw v2
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/google/obf/io$a; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_5

    .line 316
    :catch_1
    move-exception v2

    .line 317
    new-instance v3, Lcom/google/obf/ii$a;

    invoke-direct {v3, v2}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 318
    :catch_2
    move-exception v2

    .line 319
    new-instance v3, Lcom/google/obf/ii$a;

    invoke-direct {v3, v2}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 320
    :catch_3
    move-exception v2

    .line 321
    new-instance v3, Lcom/google/obf/ii$a;

    invoke-direct {v3, v2}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 322
    :catch_4
    move-exception v2

    .line 323
    new-instance v3, Lcom/google/obf/ii$a;

    invoke-direct {v3, v2}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 324
    :catch_5
    move-exception v2

    .line 325
    new-instance v3, Lcom/google/obf/ii$a;

    invoke-direct {v3, v2}, Lcom/google/obf/ii$a;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method


# virtual methods
.method protected b(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 14
    const/4 v0, 0x1

    :try_start_0
    invoke-static {}, Lcom/google/obf/ii;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/obf/ii;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Lcom/google/obf/ii$a; {:try_start_0 .. :try_end_0} :catch_c
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 17
    :goto_0
    const/4 v0, 0x2

    :try_start_1
    invoke-static {}, Lcom/google/obf/ii;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/obf/ii;->a(ILjava/lang/String;)V
    :try_end_1
    .catch Lcom/google/obf/ii$a; {:try_start_1 .. :try_end_1} :catch_b
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 20
    :goto_1
    :try_start_2
    invoke-static {}, Lcom/google/obf/ii;->b()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 21
    const/16 v2, 0x19

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/obf/ii;->a(IJ)V

    .line 22
    sget-wide v2, Lcom/google/obf/ii;->u:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 23
    const/16 v2, 0x11

    sget-wide v4, Lcom/google/obf/ii;->u:J

    sub-long/2addr v0, v4

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/obf/ii;->a(IJ)V

    .line 24
    const/16 v0, 0x17

    sget-wide v2, Lcom/google/obf/ii;->u:J

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/obf/ii;->a(IJ)V
    :try_end_2
    .catch Lcom/google/obf/ii$a; {:try_start_2 .. :try_end_2} :catch_a
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 27
    :cond_0
    :goto_2
    :try_start_3
    invoke-static {p1}, Lcom/google/obf/ii;->g(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    .line 28
    const/16 v2, 0x1f

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p0, v2, v4, v5}, Lcom/google/obf/ii;->a(IJ)V

    .line 29
    const/16 v2, 0x20

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/obf/ii;->a(IJ)V
    :try_end_3
    .catch Lcom/google/obf/ii$a; {:try_start_3 .. :try_end_3} :catch_9
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 32
    :goto_3
    const/16 v0, 0x21

    :try_start_4
    invoke-static {}, Lcom/google/obf/ii;->d()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/obf/ii;->a(IJ)V
    :try_end_4
    .catch Lcom/google/obf/ii$a; {:try_start_4 .. :try_end_4} :catch_8
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 35
    :goto_4
    const/16 v0, 0x1b

    :try_start_5
    iget-object v1, p0, Lcom/google/obf/ii;->c:Lcom/google/obf/im;

    invoke-static {p1, v1}, Lcom/google/obf/ii;->a(Landroid/content/Context;Lcom/google/obf/im;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/obf/ii;->a(ILjava/lang/String;)V
    :try_end_5
    .catch Lcom/google/obf/ii$a; {:try_start_5 .. :try_end_5} :catch_7
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    .line 38
    :goto_5
    const/16 v0, 0x1d

    :try_start_6
    iget-object v1, p0, Lcom/google/obf/ii;->c:Lcom/google/obf/im;

    invoke-static {p1, v1}, Lcom/google/obf/ii;->b(Landroid/content/Context;Lcom/google/obf/im;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/obf/ii;->a(ILjava/lang/String;)V
    :try_end_6
    .catch Lcom/google/obf/ii$a; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0

    .line 41
    :goto_6
    :try_start_7
    invoke-static {p1}, Lcom/google/obf/ii;->h(Landroid/content/Context;)[I

    move-result-object v0

    .line 42
    const/4 v1, 0x5

    const/4 v2, 0x0

    aget v2, v0, v2

    int-to-long v2, v2

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/obf/ii;->a(IJ)V

    .line 43
    const/4 v1, 0x6

    const/4 v2, 0x1

    aget v0, v0, v2

    int-to-long v2, v0

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/obf/ii;->a(IJ)V
    :try_end_7
    .catch Lcom/google/obf/ii$a; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0

    .line 46
    :goto_7
    :try_start_8
    invoke-static {p1}, Lcom/google/obf/ii;->i(Landroid/content/Context;)I

    move-result v0

    .line 47
    const/16 v1, 0xc

    int-to-long v2, v0

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/obf/ii;->a(IJ)V
    :try_end_8
    .catch Lcom/google/obf/ii$a; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0

    .line 50
    :goto_8
    :try_start_9
    invoke-static {p1}, Lcom/google/obf/ii;->j(Landroid/content/Context;)I

    move-result v0

    .line 51
    const/4 v1, 0x3

    int-to-long v2, v0

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/obf/ii;->a(IJ)V
    :try_end_9
    .catch Lcom/google/obf/ii$a; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_0

    .line 54
    :goto_9
    const/16 v0, 0x22

    :try_start_a
    invoke-static {p1}, Lcom/google/obf/ii;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/obf/ii;->a(ILjava/lang/String;)V
    :try_end_a
    .catch Lcom/google/obf/ii$a; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0

    .line 57
    :goto_a
    const/16 v0, 0x23

    :try_start_b
    invoke-static {p1}, Lcom/google/obf/ii;->f(Landroid/content/Context;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/obf/ii;->a(IJ)V
    :try_end_b
    .catch Lcom/google/obf/ii$a; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_0

    .line 62
    :goto_b
    return-void

    .line 61
    :catch_0
    move-exception v0

    goto :goto_b

    .line 59
    :catch_1
    move-exception v0

    goto :goto_b

    .line 56
    :catch_2
    move-exception v0

    goto :goto_a

    .line 53
    :catch_3
    move-exception v0

    goto :goto_9

    .line 49
    :catch_4
    move-exception v0

    goto :goto_8

    .line 45
    :catch_5
    move-exception v0

    goto :goto_7

    .line 40
    :catch_6
    move-exception v0

    goto :goto_6

    .line 37
    :catch_7
    move-exception v0

    goto :goto_5

    .line 34
    :catch_8
    move-exception v0

    goto :goto_4

    .line 31
    :catch_9
    move-exception v0

    goto :goto_3

    .line 26
    :catch_a
    move-exception v0

    goto/16 :goto_2

    .line 19
    :catch_b
    move-exception v0

    goto/16 :goto_1

    .line 16
    :catch_c
    move-exception v0

    goto/16 :goto_0
.end method

.method protected c(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 63
    const/4 v0, 0x2

    :try_start_0
    invoke-static {}, Lcom/google/obf/ii;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/obf/ii;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Lcom/google/obf/ii$a; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    :goto_0
    const/4 v0, 0x1

    :try_start_1
    invoke-static {}, Lcom/google/obf/ii;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/obf/ii;->a(ILjava/lang/String;)V
    :try_end_1
    .catch Lcom/google/obf/ii$a; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 69
    :goto_1
    const/16 v0, 0x19

    :try_start_2
    invoke-static {}, Lcom/google/obf/ii;->b()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/obf/ii;->a(IJ)V
    :try_end_2
    .catch Lcom/google/obf/ii$a; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 72
    :goto_2
    :try_start_3
    iget-object v0, p0, Lcom/google/obf/ii;->a:Landroid/view/MotionEvent;

    iget-object v1, p0, Lcom/google/obf/ii;->b:Landroid/util/DisplayMetrics;

    invoke-static {v0, v1}, Lcom/google/obf/ii;->a(Landroid/view/MotionEvent;Landroid/util/DisplayMetrics;)Ljava/util/ArrayList;

    move-result-object v1

    .line 73
    const/16 v2, 0xe

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p0, v2, v4, v5}, Lcom/google/obf/ii;->a(IJ)V

    .line 74
    const/16 v2, 0xf

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p0, v2, v4, v5}, Lcom/google/obf/ii;->a(IJ)V

    .line 75
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x3

    if-lt v0, v2, :cond_0

    .line 76
    const/16 v2, 0x10

    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/obf/ii;->a(IJ)V
    :try_end_3
    .catch Lcom/google/obf/ii$a; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 79
    :cond_0
    :goto_3
    const/16 v0, 0x22

    :try_start_4
    invoke-static {p1}, Lcom/google/obf/ii;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/obf/ii;->a(ILjava/lang/String;)V
    :try_end_4
    .catch Lcom/google/obf/ii$a; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 82
    :goto_4
    const/16 v0, 0x23

    :try_start_5
    invoke-static {p1}, Lcom/google/obf/ii;->f(Landroid/content/Context;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/obf/ii;->a(IJ)V
    :try_end_5
    .catch Lcom/google/obf/ii$a; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    .line 87
    :goto_5
    return-void

    .line 86
    :catch_0
    move-exception v0

    goto :goto_5

    .line 84
    :catch_1
    move-exception v0

    goto :goto_5

    .line 81
    :catch_2
    move-exception v0

    goto :goto_4

    .line 78
    :catch_3
    move-exception v0

    goto :goto_3

    .line 71
    :catch_4
    move-exception v0

    goto :goto_2

    .line 68
    :catch_5
    move-exception v0

    goto :goto_1

    .line 65
    :catch_6
    move-exception v0

    goto :goto_0
.end method
