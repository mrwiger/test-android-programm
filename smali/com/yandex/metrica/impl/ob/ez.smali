.class public Lcom/yandex/metrica/impl/ob/ez;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/ex;
    .locals 2

    .prologue
    .line 36
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 37
    new-instance v0, Lcom/yandex/metrica/impl/ob/fa;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/fa;-><init>(Landroid/content/Context;)V

    .line 39
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/yandex/metrica/impl/ob/fb;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/fb;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/yandex/metrica/impl/ob/ey;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/yandex/metrica/impl/ob/ey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/ob/ez;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/ex;

    move-result-object v0

    invoke-interface {v0}, Lcom/yandex/metrica/impl/ob/ex;->a()Ljava/util/List;

    move-result-object v0

    .line 27
    invoke-static {v0, p2}, Lcom/yandex/metrica/impl/utils/d;->a(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 28
    const/4 v0, 0x0

    .line 30
    :cond_0
    return-object v0
.end method
