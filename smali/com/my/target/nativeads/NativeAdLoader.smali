.class public final Lcom/my/target/nativeads/NativeAdLoader;
.super Lcom/my/target/common/BaseAd;
.source "NativeAdLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/nativeads/NativeAdLoader$OnLoad;
    }
.end annotation


# instance fields
.field private adFactory:Lcom/my/target/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/my/target/c",
            "<",
            "Lcom/my/target/cs;",
            ">;"
        }
    .end annotation
.end field

.field private final appContext:Landroid/content/Context;

.field private onLoad:Lcom/my/target/nativeads/NativeAdLoader$OnLoad;


# direct methods
.method private constructor <init>(IILandroid/content/Context;)V
    .locals 1
    .param p1, "slotId"    # I
    .param p2, "bannersCount"    # I
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    const-string v0, "nativeads"

    invoke-direct {p0, p1, v0}, Lcom/my/target/common/BaseAd;-><init>(ILjava/lang/String;)V

    .line 41
    if-gtz p2, :cond_0

    .line 43
    const/4 p2, 0x1

    .line 44
    const-string v0, "NativeAdLoader: invalid bannersCount < 1, bannersCount set to 1"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAdLoader;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0, p2}, Lcom/my/target/b;->setBannersCount(I)V

    .line 47
    invoke-virtual {p3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/NativeAdLoader;->appContext:Landroid/content/Context;

    .line 48
    const-string v0, "NativeAdLoader created. Version: 5.1.0"

    invoke-static {v0}, Lcom/my/target/g;->c(Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method static synthetic access$000(Lcom/my/target/nativeads/NativeAdLoader;)Lcom/my/target/c;
    .locals 1
    .param p0, "x0"    # Lcom/my/target/nativeads/NativeAdLoader;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAdLoader;->adFactory:Lcom/my/target/c;

    return-object v0
.end method

.method static synthetic access$002(Lcom/my/target/nativeads/NativeAdLoader;Lcom/my/target/c;)Lcom/my/target/c;
    .locals 0
    .param p0, "x0"    # Lcom/my/target/nativeads/NativeAdLoader;
    .param p1, "x1"    # Lcom/my/target/c;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/my/target/nativeads/NativeAdLoader;->adFactory:Lcom/my/target/c;

    return-object p1
.end method

.method static synthetic access$100(Lcom/my/target/nativeads/NativeAdLoader;Lcom/my/target/cs;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/my/target/nativeads/NativeAdLoader;
    .param p1, "x1"    # Lcom/my/target/cs;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/my/target/nativeads/NativeAdLoader;->handleResult(Lcom/my/target/cs;Ljava/lang/String;)V

    return-void
.end method

.method private handleResult(Lcom/my/target/cs;Ljava/lang/String;)V
    .locals 6
    .param p1, "result"    # Lcom/my/target/cs;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAdLoader;->onLoad:Lcom/my/target/nativeads/NativeAdLoader$OnLoad;

    if-eqz v0, :cond_1

    .line 102
    if-nez p1, :cond_2

    const/4 v0, 0x0

    .line 104
    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_3

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAdLoader;->onLoad:Lcom/my/target/nativeads/NativeAdLoader$OnLoad;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, v1}, Lcom/my/target/nativeads/NativeAdLoader$OnLoad;->onLoad(Ljava/util/List;)V

    .line 123
    :cond_1
    :goto_1
    return-void

    .line 102
    :cond_2
    invoke-virtual {p1}, Lcom/my/target/cs;->R()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 110
    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 111
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/a;

    .line 113
    new-instance v3, Lcom/my/target/nativeads/NativeAd;

    iget-object v4, p0, Lcom/my/target/nativeads/NativeAdLoader;->adConfig:Lcom/my/target/b;

    invoke-virtual {v4}, Lcom/my/target/b;->getSlotId()I

    move-result v4

    iget-object v5, p0, Lcom/my/target/nativeads/NativeAdLoader;->appContext:Landroid/content/Context;

    invoke-direct {v3, v4, v5}, Lcom/my/target/nativeads/NativeAd;-><init>(ILandroid/content/Context;)V

    .line 114
    iget-object v4, p0, Lcom/my/target/nativeads/NativeAdLoader;->adConfig:Lcom/my/target/b;

    invoke-virtual {v4}, Lcom/my/target/b;->isAutoLoadImages()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/my/target/nativeads/NativeAd;->setAutoLoadImages(Z)V

    .line 115
    iget-object v4, p0, Lcom/my/target/nativeads/NativeAdLoader;->adConfig:Lcom/my/target/b;

    invoke-virtual {v4}, Lcom/my/target/b;->isAutoLoadVideo()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/my/target/nativeads/NativeAd;->setAutoLoadVideo(Z)V

    .line 116
    iget-object v4, p0, Lcom/my/target/nativeads/NativeAdLoader;->adConfig:Lcom/my/target/b;

    invoke-virtual {v4}, Lcom/my/target/b;->isTrackingLocationEnabled()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/my/target/nativeads/NativeAd;->setTrackingLocationEnabled(Z)V

    .line 117
    iget-object v4, p0, Lcom/my/target/nativeads/NativeAdLoader;->adConfig:Lcom/my/target/b;

    invoke-virtual {v4}, Lcom/my/target/b;->isTrackingEnvironmentEnabled()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/my/target/nativeads/NativeAd;->setTrackingEnvironmentEnabled(Z)V

    .line 118
    invoke-virtual {v3, v0}, Lcom/my/target/nativeads/NativeAd;->setBanner(Lcom/my/target/core/models/banners/a;)V

    .line 119
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 121
    :cond_4
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAdLoader;->onLoad:Lcom/my/target/nativeads/NativeAdLoader$OnLoad;

    invoke-interface {v0, v1}, Lcom/my/target/nativeads/NativeAdLoader$OnLoad;->onLoad(Ljava/util/List;)V

    goto :goto_1
.end method

.method public static newLoader(IILandroid/content/Context;)Lcom/my/target/nativeads/NativeAdLoader;
    .locals 1
    .param p0, "slotId"    # I
    .param p1, "bannersCount"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    new-instance v0, Lcom/my/target/nativeads/NativeAdLoader;

    invoke-direct {v0, p0, p1, p2}, Lcom/my/target/nativeads/NativeAdLoader;-><init>(IILandroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final isAutoLoadImages()Z
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAdLoader;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0}, Lcom/my/target/b;->isAutoLoadImages()Z

    move-result v0

    return v0
.end method

.method public final isAutoLoadVideo()Z
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAdLoader;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0}, Lcom/my/target/b;->isAutoLoadVideo()Z

    move-result v0

    return v0
.end method

.method public final load()Lcom/my/target/nativeads/NativeAdLoader;
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAdLoader;->adConfig:Lcom/my/target/b;

    invoke-static {v0}, Lcom/my/target/co;->newFactory(Lcom/my/target/b;)Lcom/my/target/c;

    move-result-object v0

    .line 82
    iput-object v0, p0, Lcom/my/target/nativeads/NativeAdLoader;->adFactory:Lcom/my/target/c;

    .line 83
    new-instance v1, Lcom/my/target/nativeads/NativeAdLoader$1;

    invoke-direct {v1, p0, v0}, Lcom/my/target/nativeads/NativeAdLoader$1;-><init>(Lcom/my/target/nativeads/NativeAdLoader;Lcom/my/target/c;)V

    invoke-virtual {v0, v1}, Lcom/my/target/c;->a(Lcom/my/target/c$b;)Lcom/my/target/c;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/nativeads/NativeAdLoader;->appContext:Landroid/content/Context;

    .line 94
    invoke-virtual {v0, v1}, Lcom/my/target/c;->a(Landroid/content/Context;)Lcom/my/target/c;

    .line 95
    return-object p0
.end method

.method public final setAutoLoadImages(Z)V
    .locals 1
    .param p1, "autoLoadImages"    # Z

    .prologue
    .line 65
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAdLoader;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0, p1}, Lcom/my/target/b;->setAutoLoadImages(Z)V

    .line 66
    return-void
.end method

.method public final setAutoLoadVideo(Z)V
    .locals 1
    .param p1, "autoLoadVideo"    # Z

    .prologue
    .line 60
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAdLoader;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0, p1}, Lcom/my/target/b;->setAutoLoadVideo(Z)V

    .line 61
    return-void
.end method

.method public final setOnLoad(Lcom/my/target/nativeads/NativeAdLoader$OnLoad;)Lcom/my/target/nativeads/NativeAdLoader;
    .locals 0
    .param p1, "onLoad"    # Lcom/my/target/nativeads/NativeAdLoader$OnLoad;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/my/target/nativeads/NativeAdLoader;->onLoad:Lcom/my/target/nativeads/NativeAdLoader$OnLoad;

    .line 55
    return-object p0
.end method
