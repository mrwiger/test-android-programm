.class final Lru/cn/player/exoplayer/ExoTrackSelector;
.super Ljava/lang/Object;
.source "ExoTrackSelector.java"

# interfaces
.implements Lru/cn/player/Selector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lru/cn/player/Selector",
        "<",
        "Lru/cn/player/exoplayer/ExoTrackInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final disableableSubtitles:Z

.field private final player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

.field private final trackSelectedListener:Lru/cn/player/Selector$TrackSelector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lru/cn/player/Selector$TrackSelector",
            "<",
            "Lru/cn/player/exoplayer/ExoTrackInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;


# direct methods
.method constructor <init>(Lcom/google/android/exoplayer2/SimpleExoPlayer;Lru/cn/player/exoplayer/SpleenyTrackSelector;ZLru/cn/player/Selector$TrackSelector;)V
    .locals 0
    .param p1, "player"    # Lcom/google/android/exoplayer2/SimpleExoPlayer;
    .param p2, "trackSelector"    # Lru/cn/player/exoplayer/SpleenyTrackSelector;
    .param p3, "disableableSubtitles"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/SimpleExoPlayer;",
            "Lru/cn/player/exoplayer/SpleenyTrackSelector;",
            "Z",
            "Lru/cn/player/Selector$TrackSelector",
            "<",
            "Lru/cn/player/exoplayer/ExoTrackInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p4, "trackSelectedListener":Lru/cn/player/Selector$TrackSelector;, "Lru/cn/player/Selector$TrackSelector<Lru/cn/player/exoplayer/ExoTrackInfo;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    .line 30
    iput-object p2, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    .line 31
    iput-object p4, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->trackSelectedListener:Lru/cn/player/Selector$TrackSelector;

    .line 32
    iput-boolean p3, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->disableableSubtitles:Z

    .line 33
    return-void
.end method

.method private getCurrentTrackByType(I)Lru/cn/player/exoplayer/ExoTrackInfo;
    .locals 5
    .param p1, "trackType"    # I

    .prologue
    .line 105
    iget-object v3, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    iget-object v4, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v3, v4, p1}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->getRendererIndex(Lcom/google/android/exoplayer2/SimpleExoPlayer;I)I

    move-result v0

    .line 107
    .local v0, "rendererIndex":I
    if-gez v0, :cond_0

    .line 108
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "These is\'t the renderer you\'re looking for"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 111
    :cond_0
    iget-object v3, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->getCurrentTrackSelections()Lcom/google/android/exoplayer2/trackselection/TrackSelectionArray;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/exoplayer2/trackselection/TrackSelectionArray;->get(I)Lcom/google/android/exoplayer2/trackselection/TrackSelection;

    move-result-object v2

    .line 112
    .local v2, "trackSelection":Lcom/google/android/exoplayer2/trackselection/TrackSelection;
    if-nez v2, :cond_1

    .line 113
    const/4 v3, 0x0

    .line 118
    :goto_0
    return-object v3

    .line 116
    :cond_1
    invoke-interface {v2}, Lcom/google/android/exoplayer2/trackselection/TrackSelection;->getSelectedFormat()Lcom/google/android/exoplayer2/Format;

    move-result-object v1

    .line 121
    .local v1, "selectedFormat":Lcom/google/android/exoplayer2/Format;
    invoke-direct {p0, p1, v1}, Lru/cn/player/exoplayer/ExoTrackSelector;->getGroupIndex(ILcom/google/android/exoplayer2/Format;)I

    move-result v3

    .line 122
    invoke-interface {v2}, Lcom/google/android/exoplayer2/trackselection/TrackSelection;->getSelectedIndexInTrackGroup()I

    move-result v4

    .line 118
    invoke-static {v1, v0, v3, v4}, Lru/cn/player/exoplayer/ExoTrackInfo;->createTrackInfo(Lcom/google/android/exoplayer2/Format;III)Lru/cn/player/exoplayer/ExoTrackInfo;

    move-result-object v3

    goto :goto_0
.end method

.method private getGroupIndex(ILcom/google/android/exoplayer2/Format;)I
    .locals 8
    .param p1, "trackType"    # I
    .param p2, "format"    # Lcom/google/android/exoplayer2/Format;

    .prologue
    const/4 v5, -0x1

    .line 127
    iget-object v6, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v6}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->getPlaybackState()I

    move-result v6

    const/4 v7, 0x3

    if-eq v6, v7, :cond_1

    move v2, v5

    .line 146
    :cond_0
    :goto_0
    return v2

    .line 131
    :cond_1
    iget-object v6, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    iget-object v7, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v6, v7, p1}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->getGroupArrayByTrackType(Lcom/google/android/exoplayer2/SimpleExoPlayer;I)Lcom/google/android/exoplayer2/source/TrackGroupArray;

    move-result-object v4

    .line 132
    .local v4, "typeTrackGroups":Lcom/google/android/exoplayer2/source/TrackGroupArray;
    if-nez v4, :cond_2

    move v2, v5

    .line 133
    goto :goto_0

    .line 136
    :cond_2
    const/4 v2, 0x0

    .local v2, "groupIndex":I
    :goto_1
    iget v6, v4, Lcom/google/android/exoplayer2/source/TrackGroupArray;->length:I

    if-ge v2, v6, :cond_4

    .line 137
    invoke-virtual {v4, v2}, Lcom/google/android/exoplayer2/source/TrackGroupArray;->get(I)Lcom/google/android/exoplayer2/source/TrackGroup;

    move-result-object v1

    .line 138
    .local v1, "group":Lcom/google/android/exoplayer2/source/TrackGroup;
    const/4 v3, 0x0

    .local v3, "trackIndex":I
    :goto_2
    iget v6, v1, Lcom/google/android/exoplayer2/source/TrackGroup;->length:I

    if-ge v3, v6, :cond_3

    .line 139
    invoke-virtual {v1, v3}, Lcom/google/android/exoplayer2/source/TrackGroup;->getFormat(I)Lcom/google/android/exoplayer2/Format;

    move-result-object v0

    .line 140
    .local v0, "currentFormat":Lcom/google/android/exoplayer2/Format;
    invoke-virtual {v0, p2}, Lcom/google/android/exoplayer2/Format;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 138
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 136
    .end local v0    # "currentFormat":Lcom/google/android/exoplayer2/Format;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v1    # "group":Lcom/google/android/exoplayer2/source/TrackGroup;
    .end local v3    # "trackIndex":I
    :cond_4
    move v2, v5

    .line 146
    goto :goto_0
.end method

.method private getTrackInfoByType(I)Ljava/util/List;
    .locals 11
    .param p1, "trackType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lru/cn/player/exoplayer/ExoTrackInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 56
    iget-object v8, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v8}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->getPlaybackState()I

    move-result v8

    const/4 v9, 0x3

    if-eq v8, v9, :cond_0

    .line 57
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 83
    :goto_0
    return-object v8

    .line 60
    :cond_0
    iget-object v8, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    iget-object v9, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v8, v9, p1}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->getRendererIndex(Lcom/google/android/exoplayer2/SimpleExoPlayer;I)I

    move-result v4

    .line 61
    .local v4, "rendererIndex":I
    iget-object v8, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    iget-object v9, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v8, v9, p1}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->getGroupArrayByTrackType(Lcom/google/android/exoplayer2/SimpleExoPlayer;I)Lcom/google/android/exoplayer2/source/TrackGroupArray;

    move-result-object v7

    .line 63
    .local v7, "typeTrackGroups":Lcom/google/android/exoplayer2/source/TrackGroupArray;
    if-nez v7, :cond_1

    .line 64
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v10}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0

    .line 67
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 69
    .local v5, "resultTracksInfo":Ljava/util/List;, "Ljava/util/List<Lru/cn/player/exoplayer/ExoTrackInfo;>;"
    const/4 v2, 0x0

    .local v2, "groupIndex":I
    :goto_1
    iget v8, v7, Lcom/google/android/exoplayer2/source/TrackGroupArray;->length:I

    if-ge v2, v8, :cond_3

    .line 70
    invoke-virtual {v7, v2}, Lcom/google/android/exoplayer2/source/TrackGroupArray;->get(I)Lcom/google/android/exoplayer2/source/TrackGroup;

    move-result-object v1

    .line 71
    .local v1, "group":Lcom/google/android/exoplayer2/source/TrackGroup;
    const/4 v6, 0x0

    .local v6, "trackIndex":I
    :goto_2
    iget v8, v1, Lcom/google/android/exoplayer2/source/TrackGroup;->length:I

    if-ge v6, v8, :cond_2

    .line 72
    invoke-virtual {v1, v6}, Lcom/google/android/exoplayer2/source/TrackGroup;->getFormat(I)Lcom/google/android/exoplayer2/Format;

    move-result-object v0

    .line 74
    .local v0, "format":Lcom/google/android/exoplayer2/Format;
    invoke-static {v0, v4, v2, v6}, Lru/cn/player/exoplayer/ExoTrackInfo;->createTrackInfo(Lcom/google/android/exoplayer2/Format;III)Lru/cn/player/exoplayer/ExoTrackInfo;

    move-result-object v3

    .line 79
    .local v3, "info":Lru/cn/player/exoplayer/ExoTrackInfo;
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 69
    .end local v0    # "format":Lcom/google/android/exoplayer2/Format;
    .end local v3    # "info":Lru/cn/player/exoplayer/ExoTrackInfo;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 83
    .end local v1    # "group":Lcom/google/android/exoplayer2/source/TrackGroup;
    .end local v6    # "trackIndex":I
    :cond_3
    invoke-static {v5}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v8

    sget-object v9, Lru/cn/player/exoplayer/ExoTrackSelector$$Lambda$0;->$instance:Lcom/annimon/stream/function/Predicate;

    .line 84
    invoke-virtual {v8, v9}, Lcom/annimon/stream/Stream;->filter(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;

    move-result-object v8

    .line 85
    invoke-virtual {v8}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object v8

    goto :goto_0
.end method

.method static final synthetic lambda$getTrackInfoByType$0$ExoTrackSelector(Lru/cn/player/exoplayer/ExoTrackInfo;)Z
    .locals 2
    .param p0, "trackInfo"    # Lru/cn/player/exoplayer/ExoTrackInfo;

    .prologue
    .line 84
    const-string v0, "application/cea-608"

    iget-object v1, p0, Lru/cn/player/exoplayer/ExoTrackInfo;->sampleMimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private trackSelected(II)V
    .locals 3
    .param p1, "trackType"    # I
    .param p2, "index"    # I

    .prologue
    .line 171
    invoke-direct {p0, p1}, Lru/cn/player/exoplayer/ExoTrackSelector;->getTrackInfoByType(I)Ljava/util/List;

    move-result-object v0

    .line 172
    .local v0, "tracksInfoByType":Ljava/util/List;, "Ljava/util/List<Lru/cn/player/exoplayer/ExoTrackInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, p2, :cond_0

    .line 173
    iget-object v1, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->trackSelectedListener:Lru/cn/player/Selector$TrackSelector;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Lru/cn/player/Selector$TrackSelector;->onTrackSelected(Ljava/lang/Object;)V

    .line 175
    :cond_0
    return-void
.end method


# virtual methods
.method public canAdaptiveAudioStreaming()Z
    .locals 1

    .prologue
    .line 242
    const/4 v0, 0x0

    return v0
.end method

.method public canAdaptiveVideoStreaming()Z
    .locals 1

    .prologue
    .line 237
    const/4 v0, 0x1

    return v0
.end method

.method public canDeactivateAudio()Z
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x0

    return v0
.end method

.method public canDeactivateSubtitles()Z
    .locals 1

    .prologue
    .line 222
    iget-boolean v0, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->disableableSubtitles:Z

    return v0
.end method

.method public disableAudio()V
    .locals 0

    .prologue
    .line 206
    return-void
.end method

.method public disableSubtitles()V
    .locals 4

    .prologue
    .line 215
    iget-object v1, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    iget-object v2, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->getRendererIndex(Lcom/google/android/exoplayer2/SimpleExoPlayer;I)I

    move-result v0

    .line 216
    .local v0, "rendererIndex":I
    iget-object v1, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->setRendererDisabled(IZ)V

    .line 218
    return-void
.end method

.method public disableVideo()V
    .locals 0

    .prologue
    .line 211
    return-void
.end method

.method public getAudioTracks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lru/cn/player/exoplayer/ExoTrackInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lru/cn/player/exoplayer/ExoTrackSelector;->getTrackInfoByType(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAudioTracksCount()I
    .locals 1

    .prologue
    .line 90
    invoke-virtual {p0}, Lru/cn/player/exoplayer/ExoTrackSelector;->getAudioTracks()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCurrentAudioTrackIndex()I
    .locals 2

    .prologue
    .line 151
    invoke-virtual {p0}, Lru/cn/player/exoplayer/ExoTrackSelector;->getAudioTracks()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lru/cn/player/exoplayer/ExoTrackSelector;->getCurrentTrackByType(I)Lru/cn/player/exoplayer/ExoTrackInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getCurrentSubtitlesTrackIndex()I
    .locals 2

    .prologue
    .line 167
    invoke-virtual {p0}, Lru/cn/player/exoplayer/ExoTrackSelector;->getSubtitleTracks()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lru/cn/player/exoplayer/ExoTrackSelector;->getCurrentTrackByType(I)Lru/cn/player/exoplayer/ExoTrackInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getCurrentVideoTrackIndex()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 156
    iget-object v2, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    iget-object v3, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v2, v3, v4}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->getSelectionOverride(Lcom/google/android/exoplayer2/SimpleExoPlayer;I)Lcom/google/android/exoplayer2/trackselection/MappingTrackSelector$SelectionOverride;

    move-result-object v1

    .line 157
    .local v1, "selectionOverride":Lcom/google/android/exoplayer2/trackselection/MappingTrackSelector$SelectionOverride;
    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 158
    .local v0, "isAdaptive":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 159
    const/4 v2, -0x1

    .line 162
    :goto_1
    return v2

    .line 157
    .end local v0    # "isAdaptive":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 162
    .restart local v0    # "isAdaptive":Z
    :cond_1
    invoke-virtual {p0}, Lru/cn/player/exoplayer/ExoTrackSelector;->getVideoTracks()Ljava/util/List;

    move-result-object v2

    invoke-direct {p0, v4}, Lru/cn/player/exoplayer/ExoTrackSelector;->getCurrentTrackByType(I)Lru/cn/player/exoplayer/ExoTrackInfo;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    goto :goto_1
.end method

.method public getSubtitleTracks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lru/cn/player/exoplayer/ExoTrackInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lru/cn/player/exoplayer/ExoTrackSelector;->getTrackInfoByType(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSubtitlesTracksCount()I
    .locals 1

    .prologue
    .line 100
    invoke-virtual {p0}, Lru/cn/player/exoplayer/ExoTrackSelector;->getSubtitleTracks()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getVideoTracks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lru/cn/player/exoplayer/ExoTrackInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lru/cn/player/exoplayer/ExoTrackSelector;->getTrackInfoByType(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getVideoTracksCount()I
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0}, Lru/cn/player/exoplayer/ExoTrackSelector;->getVideoTracks()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public selectAudioTrack(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 179
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lru/cn/player/exoplayer/ExoTrackSelector;->trackSelected(II)V

    .line 180
    return-void
.end method

.method public selectSubtitlesTrack(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 189
    const/4 v0, 0x3

    invoke-direct {p0, v0, p1}, Lru/cn/player/exoplayer/ExoTrackSelector;->trackSelected(II)V

    .line 190
    return-void
.end method

.method public selectVideoTrack(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 184
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lru/cn/player/exoplayer/ExoTrackSelector;->trackSelected(II)V

    .line 185
    return-void
.end method

.method public setAdaptiveVideoStream()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 194
    iget-object v2, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    iget-object v3, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v2, v3, v4}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->getRendererIndex(Lcom/google/android/exoplayer2/SimpleExoPlayer;I)I

    move-result v0

    .line 195
    .local v0, "rendererIndex":I
    iget-object v2, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    iget-object v3, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v2, v3, v4}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->getGroupArrayByTrackType(Lcom/google/android/exoplayer2/SimpleExoPlayer;I)Lcom/google/android/exoplayer2/source/TrackGroupArray;

    move-result-object v1

    .line 197
    .local v1, "typeTrackGroups":Lcom/google/android/exoplayer2/source/TrackGroupArray;
    if-nez v1, :cond_0

    .line 198
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "typeTrackGroups == null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 200
    :cond_0
    iget-object v2, p0, Lru/cn/player/exoplayer/ExoTrackSelector;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    invoke-virtual {v2, v0, v1}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->clearSelectionOverride(ILcom/google/android/exoplayer2/source/TrackGroupArray;)V

    .line 201
    return-void
.end method
