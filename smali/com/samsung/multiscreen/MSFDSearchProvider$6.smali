.class final Lcom/samsung/multiscreen/MSFDSearchProvider$6;
.super Ljava/lang/Object;
.source "MSFDSearchProvider.java"

# interfaces
.implements Lcom/samsung/multiscreen/MSFDSearchProvider$FutureRunnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/multiscreen/MSFDSearchProvider;->getById(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/multiscreen/Result;)Lcom/samsung/multiscreen/ProviderThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field private future:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field private numDiscover:I

.field final synthetic val$discoverPacket:Ljava/net/DatagramPacket;

.field final synthetic val$threadSocket:Ljava/net/MulticastSocket;


# direct methods
.method constructor <init>(Ljava/net/MulticastSocket;Ljava/net/DatagramPacket;)V
    .locals 1

    .prologue
    .line 543
    iput-object p1, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$6;->val$threadSocket:Ljava/net/MulticastSocket;

    iput-object p2, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$6;->val$discoverPacket:Ljava/net/DatagramPacket;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 545
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$6;->numDiscover:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 551
    :try_start_0
    iget v1, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$6;->numDiscover:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$6;->numDiscover:I

    const/4 v2, 0x3

    if-ge v1, v2, :cond_0

    .line 552
    iget-object v1, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$6;->val$threadSocket:Ljava/net/MulticastSocket;

    iget-object v2, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$6;->val$discoverPacket:Ljava/net/DatagramPacket;

    invoke-virtual {v1, v2}, Ljava/net/MulticastSocket;->send(Ljava/net/DatagramPacket;)V

    .line 559
    :goto_0
    return-void

    .line 554
    :cond_0
    iget-object v1, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$6;->future:Ljava/util/concurrent/ScheduledFuture;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 556
    :catch_0
    move-exception v0

    .line 557
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "MSFDSearchProvider"

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setFuture(Ljava/util/concurrent/ScheduledFuture;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 563
    .local p1, "future":Ljava/util/concurrent/ScheduledFuture;, "Ljava/util/concurrent/ScheduledFuture<*>;"
    iput-object p1, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$6;->future:Ljava/util/concurrent/ScheduledFuture;

    .line 564
    return-void
.end method
