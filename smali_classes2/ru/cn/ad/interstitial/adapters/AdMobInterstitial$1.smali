.class Lru/cn/ad/interstitial/adapters/AdMobInterstitial$1;
.super Lcom/google/android/gms/ads/AdListener;
.source "AdMobInterstitial.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->onLoad()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/ad/interstitial/adapters/AdMobInterstitial;


# direct methods
.method constructor <init>(Lru/cn/ad/interstitial/adapters/AdMobInterstitial;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/ad/interstitial/adapters/AdMobInterstitial;

    .prologue
    .line 31
    iput-object p1, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/AdMobInterstitial;

    invoke-direct {p0}, Lcom/google/android/gms/ads/AdListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAdClosed()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/AdMobInterstitial;

    invoke-static {v0}, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->access$400(Lru/cn/ad/interstitial/adapters/AdMobInterstitial;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/AdMobInterstitial;

    invoke-static {v0}, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->access$500(Lru/cn/ad/interstitial/adapters/AdMobInterstitial;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdEnded()V

    .line 62
    :cond_0
    return-void
.end method

.method public onAdFailedToLoad(I)V
    .locals 4
    .param p1, "errorCode"    # I

    .prologue
    .line 34
    packed-switch p1, :pswitch_data_0

    .line 43
    :goto_0
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/AdMobInterstitial;

    invoke-static {v0}, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->access$000(Lru/cn/ad/interstitial/adapters/AdMobInterstitial;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/AdMobInterstitial;

    invoke-static {v0}, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->access$100(Lru/cn/ad/interstitial/adapters/AdMobInterstitial;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onError()V

    .line 45
    :cond_0
    return-void

    .line 37
    :pswitch_0
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/AdMobInterstitial;

    sget-object v1, Lru/cn/domain/statistics/inetra/ErrorCode;->ADV_ERROR_LOAD_BANNER:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v2, "AdmobError"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, p1, v3}, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->reportError(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/util/Map;)V

    goto :goto_0

    .line 34
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onAdLeftApplication()V
    .locals 3

    .prologue
    .line 55
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/AdMobInterstitial;

    sget-object v1, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_CLICK:Lru/cn/domain/statistics/inetra/AdvEvent;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/util/Map;)V

    .line 56
    return-void
.end method

.method public onAdLoaded()V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/AdMobInterstitial;

    invoke-static {v0}, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->access$200(Lru/cn/ad/interstitial/adapters/AdMobInterstitial;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/AdMobInterstitial;

    invoke-static {v0}, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->access$300(Lru/cn/ad/interstitial/adapters/AdMobInterstitial;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdLoaded()V

    .line 51
    :cond_0
    return-void
.end method

.method public onAdOpened()V
    .locals 3

    .prologue
    .line 66
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/AdMobInterstitial;

    sget-object v1, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_START:Lru/cn/domain/statistics/inetra/AdvEvent;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/util/Map;)V

    .line 67
    return-void
.end method
