.class final Lcom/google/android/exoplayer2/extractor/mp4/FixedSampleSizeRechunker;
.super Ljava/lang/Object;
.source "FixedSampleSizeRechunker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/extractor/mp4/FixedSampleSizeRechunker$Results;
    }
.end annotation


# direct methods
.method public static rechunk(I[J[IJ)Lcom/google/android/exoplayer2/extractor/mp4/FixedSampleSizeRechunker$Results;
    .locals 25
    .param p0, "fixedSampleSize"    # I
    .param p1, "chunkOffsets"    # [J
    .param p2, "chunkSampleCounts"    # [I
    .param p3, "timestampDeltaInTimeUnits"    # J

    .prologue
    .line 70
    const/16 v2, 0x2000

    div-int v15, v2, p0

    .line 73
    .local v15, "maxSampleCount":I
    const/16 v18, 0x0

    .line 74
    .local v18, "rechunkedSampleCount":I
    move-object/from16 v0, p2

    array-length v10, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v10, :cond_0

    aget v13, p2, v2

    .line 75
    .local v13, "chunkSampleCount":I
    invoke-static {v13, v15}, Lcom/google/android/exoplayer2/util/Util;->ceilDivide(II)I

    move-result v19

    add-int v18, v18, v19

    .line 74
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 78
    .end local v13    # "chunkSampleCount":I
    :cond_0
    move/from16 v0, v18

    new-array v3, v0, [J

    .line 79
    .local v3, "offsets":[J
    move/from16 v0, v18

    new-array v4, v0, [I

    .line 80
    .local v4, "sizes":[I
    const/4 v5, 0x0

    .line 81
    .local v5, "maximumSize":I
    move/from16 v0, v18

    new-array v6, v0, [J

    .line 82
    .local v6, "timestamps":[J
    move/from16 v0, v18

    new-array v7, v0, [I

    .line 84
    .local v7, "flags":[I
    const/16 v17, 0x0

    .line 85
    .local v17, "originalSampleIndex":I
    const/16 v16, 0x0

    .line 86
    .local v16, "newSampleIndex":I
    const/4 v12, 0x0

    .local v12, "chunkIndex":I
    :goto_1
    move-object/from16 v0, p2

    array-length v2, v0

    if-ge v12, v2, :cond_2

    .line 87
    aget v14, p2, v12

    .line 88
    .local v14, "chunkSamplesRemaining":I
    aget-wide v20, p1, v12

    .line 90
    .local v20, "sampleOffset":J
    :goto_2
    if-lez v14, :cond_1

    .line 91
    invoke-static {v15, v14}, Ljava/lang/Math;->min(II)I

    move-result v11

    .line 93
    .local v11, "bufferSampleCount":I
    aput-wide v20, v3, v16

    .line 94
    mul-int v2, p0, v11

    aput v2, v4, v16

    .line 95
    aget v2, v4, v16

    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 96
    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v22, v0

    mul-long v22, v22, p3

    aput-wide v22, v6, v16

    .line 97
    const/4 v2, 0x1

    aput v2, v7, v16

    .line 99
    aget v2, v4, v16

    int-to-long v0, v2

    move-wide/from16 v22, v0

    add-long v20, v20, v22

    .line 100
    add-int v17, v17, v11

    .line 102
    sub-int/2addr v14, v11

    .line 103
    add-int/lit8 v16, v16, 0x1

    .line 104
    goto :goto_2

    .line 86
    .end local v11    # "bufferSampleCount":I
    :cond_1
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 106
    .end local v14    # "chunkSamplesRemaining":I
    .end local v20    # "sampleOffset":J
    :cond_2
    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v22, v0

    mul-long v8, p3, v22

    .line 108
    .local v8, "duration":J
    new-instance v2, Lcom/google/android/exoplayer2/extractor/mp4/FixedSampleSizeRechunker$Results;

    const/4 v10, 0x0

    invoke-direct/range {v2 .. v10}, Lcom/google/android/exoplayer2/extractor/mp4/FixedSampleSizeRechunker$Results;-><init>([J[II[J[IJLcom/google/android/exoplayer2/extractor/mp4/FixedSampleSizeRechunker$1;)V

    return-object v2
.end method
