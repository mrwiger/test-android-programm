.class public Lcom/flurry/sdk/ll;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flurry/sdk/ll$a;
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/String;

.field private static c:Lcom/flurry/sdk/ll;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/flurry/sdk/lt;",
            "[B>;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/flurry/sdk/ll$a;

.field private f:Lcom/flurry/sdk/lv;

.field private g:Ljava/lang/String;

.field private final h:Lcom/flurry/sdk/mh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/flurry/sdk/mh",
            "<",
            "Lcom/flurry/sdk/nn;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/flurry/sdk/ll;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/flurry/sdk/ll;->b:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1404
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 1405
    const-string v1, "null"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1406
    const-string v1, "9774d56d682e549c"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1407
    const-string v1, "dead00beef"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1408
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 48
    iput-object v0, p0, Lcom/flurry/sdk/ll;->d:Ljava/util/Set;

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/flurry/sdk/ll;->a:Ljava/util/Map;

    .line 50
    sget-object v0, Lcom/flurry/sdk/ll$a;->a:Lcom/flurry/sdk/ll$a;

    iput-object v0, p0, Lcom/flurry/sdk/ll;->e:Lcom/flurry/sdk/ll$a;

    .line 53
    new-instance v0, Lcom/flurry/sdk/ll$1;

    invoke-direct {v0, p0}, Lcom/flurry/sdk/ll$1;-><init>(Lcom/flurry/sdk/ll;)V

    iput-object v0, p0, Lcom/flurry/sdk/ll;->h:Lcom/flurry/sdk/mh;

    .line 75
    invoke-static {}, Lcom/flurry/sdk/mi;->a()Lcom/flurry/sdk/mi;

    move-result-object v0

    const-string v1, "com.flurry.android.sdk.FlurrySessionEvent"

    iget-object v2, p0, Lcom/flurry/sdk/ll;->h:Lcom/flurry/sdk/mh;

    .line 76
    invoke-virtual {v0, v1, v2}, Lcom/flurry/sdk/mi;->a(Ljava/lang/String;Lcom/flurry/sdk/mh;)V

    .line 77
    invoke-static {}, Lcom/flurry/sdk/ly;->a()Lcom/flurry/sdk/ly;

    move-result-object v0

    new-instance v1, Lcom/flurry/sdk/ll$2;

    invoke-direct {v1, p0}, Lcom/flurry/sdk/ll$2;-><init>(Lcom/flurry/sdk/ll;)V

    invoke-virtual {v0, v1}, Lcom/flurry/sdk/ly;->b(Ljava/lang/Runnable;)V

    .line 83
    return-void
.end method

.method public static declared-synchronized a()Lcom/flurry/sdk/ll;
    .locals 2

    .prologue
    .line 86
    const-class v1, Lcom/flurry/sdk/ll;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/flurry/sdk/ll;->c:Lcom/flurry/sdk/ll;

    if-nez v0, :cond_0

    .line 87
    new-instance v0, Lcom/flurry/sdk/ll;

    invoke-direct {v0}, Lcom/flurry/sdk/ll;-><init>()V

    sput-object v0, Lcom/flurry/sdk/ll;->c:Lcom/flurry/sdk/ll;

    .line 89
    :cond_0
    sget-object v0, Lcom/flurry/sdk/ll;->c:Lcom/flurry/sdk/ll;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/flurry/sdk/ll;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/flurry/sdk/ll;->d()V

    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/io/File;)V
    .locals 5

    .prologue
    .line 299
    const/4 v2, 0x0

    .line 301
    :try_start_0
    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2370
    const/4 v0, 0x1

    :try_start_1
    invoke-interface {v1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 2371
    invoke-interface {v1, p0}, Ljava/io/DataOutput;->writeUTF(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 306
    invoke-static {v1}, Lcom/flurry/sdk/nx;->a(Ljava/io/Closeable;)V

    .line 307
    :goto_0
    return-void

    .line 303
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 304
    :goto_1
    const/4 v2, 0x6

    :try_start_2
    sget-object v3, Lcom/flurry/sdk/ll;->b:Ljava/lang/String;

    const-string v4, "Error when saving deviceId"

    invoke-static {v2, v3, v4, v0}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 306
    invoke-static {v1}, Lcom/flurry/sdk/nx;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    invoke-static {v1}, Lcom/flurry/sdk/nx;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    .line 303
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method static synthetic b(Lcom/flurry/sdk/ll;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x25

    .line 8126
    :goto_0
    sget-object v0, Lcom/flurry/sdk/ll$a;->e:Lcom/flurry/sdk/ll$a;

    iget-object v1, p0, Lcom/flurry/sdk/ll;->e:Lcom/flurry/sdk/ll$a;

    invoke-virtual {v0, v1}, Lcom/flurry/sdk/ll$a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 8128
    sget-object v0, Lcom/flurry/sdk/ll$4;->b:[I

    iget-object v1, p0, Lcom/flurry/sdk/ll;->e:Lcom/flurry/sdk/ll$a;

    invoke-virtual {v1}, Lcom/flurry/sdk/ll$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 8152
    :goto_1
    :try_start_0
    sget-object v0, Lcom/flurry/sdk/ll$4;->b:[I

    iget-object v1, p0, Lcom/flurry/sdk/ll;->e:Lcom/flurry/sdk/ll$a;

    invoke-virtual {v1}, Lcom/flurry/sdk/ll$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 8154
    :pswitch_0
    invoke-direct {p0}, Lcom/flurry/sdk/ll;->d()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 8169
    :catch_0
    move-exception v0

    .line 8170
    const/4 v1, 0x4

    sget-object v2, Lcom/flurry/sdk/ll;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception during id fetch:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/flurry/sdk/ll;->e:Lcom/flurry/sdk/ll$a;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 8130
    :pswitch_1
    sget-object v0, Lcom/flurry/sdk/ll$a;->b:Lcom/flurry/sdk/ll$a;

    iput-object v0, p0, Lcom/flurry/sdk/ll;->e:Lcom/flurry/sdk/ll$a;

    goto :goto_1

    .line 8134
    :pswitch_2
    sget-object v0, Lcom/flurry/sdk/ll$a;->c:Lcom/flurry/sdk/ll$a;

    iput-object v0, p0, Lcom/flurry/sdk/ll;->e:Lcom/flurry/sdk/ll$a;

    goto :goto_1

    .line 8138
    :pswitch_3
    sget-object v0, Lcom/flurry/sdk/ll$a;->d:Lcom/flurry/sdk/ll$a;

    iput-object v0, p0, Lcom/flurry/sdk/ll;->e:Lcom/flurry/sdk/ll$a;

    goto :goto_1

    .line 8142
    :pswitch_4
    sget-object v0, Lcom/flurry/sdk/ll$a;->e:Lcom/flurry/sdk/ll$a;

    iput-object v0, p0, Lcom/flurry/sdk/ll;->e:Lcom/flurry/sdk/ll$a;

    goto :goto_1

    .line 8206
    :pswitch_5
    :try_start_1
    invoke-static {}, Lcom/flurry/sdk/nx;->b()V

    .line 8245
    invoke-static {}, Lcom/flurry/sdk/ly;->a()Lcom/flurry/sdk/ly;

    move-result-object v0

    .line 9098
    iget-object v0, v0, Lcom/flurry/sdk/ly;->a:Landroid/content/Context;

    .line 8245
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 9268
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 9272
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 9400
    iget-object v2, p0, Lcom/flurry/sdk/ll;->d:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 9272
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 8247
    :goto_2
    if-nez v0, :cond_2

    .line 8248
    const/4 v0, 0x0

    .line 8236
    :goto_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 8208
    :cond_0
    :goto_4
    iput-object v0, p0, Lcom/flurry/sdk/ll;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 9272
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 8251
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "AND"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 10255
    :cond_3
    invoke-static {}, Lcom/flurry/sdk/ll;->f()Ljava/lang/String;

    move-result-object v0

    .line 10256
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10258
    invoke-direct {p0}, Lcom/flurry/sdk/ll;->g()Ljava/lang/String;

    move-result-object v0

    .line 10259
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 10277
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    .line 10278
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    .line 10279
    invoke-static {}, Lcom/flurry/sdk/ly;->a()Lcom/flurry/sdk/ly;

    move-result-object v4

    .line 11098
    iget-object v4, v4, Lcom/flurry/sdk/ly;->a:Landroid/content/Context;

    .line 10279
    invoke-static {v4}, Lcom/flurry/sdk/nu;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 10278
    invoke-static {v4}, Lcom/flurry/sdk/nx;->i(Ljava/lang/String;)J

    move-result-wide v4

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    mul-long/2addr v2, v6

    add-long/2addr v0, v2

    .line 10280
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ID"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x10

    invoke-static {v0, v1, v3}, Ljava/lang/Long;->toString(JI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 11284
    :cond_4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 11288
    invoke-static {}, Lcom/flurry/sdk/ly;->a()Lcom/flurry/sdk/ly;

    move-result-object v1

    .line 12098
    iget-object v1, v1, Lcom/flurry/sdk/ly;->a:Landroid/content/Context;

    .line 12396
    const-string v2, ".flurryb."

    .line 11289
    invoke-virtual {v1, v2}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 11291
    invoke-static {v1}, Lcom/flurry/sdk/nw;->a(Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 11295
    invoke-static {v0, v1}, Lcom/flurry/sdk/ll;->a(Ljava/lang/String;Ljava/io/File;)V

    goto :goto_4

    .line 8162
    :pswitch_6
    invoke-direct {p0}, Lcom/flurry/sdk/ll;->h()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 8175
    :cond_5
    new-instance v0, Lcom/flurry/sdk/lm;

    invoke-direct {v0}, Lcom/flurry/sdk/lm;-><init>()V

    .line 8176
    invoke-static {}, Lcom/flurry/sdk/mi;->a()Lcom/flurry/sdk/mi;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/flurry/sdk/mi;->a(Lcom/flurry/sdk/mg;)V

    .line 36
    return-void

    .line 8128
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 8152
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private d()V
    .locals 2

    .prologue
    .line 181
    invoke-static {}, Lcom/flurry/sdk/nx;->b()V

    .line 184
    invoke-static {}, Lcom/flurry/sdk/ll;->e()Lcom/flurry/sdk/lv;

    move-result-object v0

    iput-object v0, p0, Lcom/flurry/sdk/ll;->f:Lcom/flurry/sdk/lv;

    .line 187
    invoke-virtual {p0}, Lcom/flurry/sdk/ll;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    invoke-direct {p0}, Lcom/flurry/sdk/ll;->h()V

    .line 194
    new-instance v0, Lcom/flurry/sdk/ln;

    invoke-direct {v0}, Lcom/flurry/sdk/ln;-><init>()V

    .line 195
    invoke-static {}, Lcom/flurry/sdk/mi;->a()Lcom/flurry/sdk/mi;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/flurry/sdk/mi;->a(Lcom/flurry/sdk/mg;)V

    .line 197
    :cond_0
    return-void
.end method

.method private static e()Lcom/flurry/sdk/lv;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 214
    :try_start_0
    invoke-static {}, Lcom/flurry/sdk/ly;->a()Lcom/flurry/sdk/ly;

    move-result-object v0

    .line 2098
    iget-object v0, v0, Lcom/flurry/sdk/ly;->a:Landroid/content/Context;

    .line 214
    invoke-static {v0}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;->getAdvertisingIdInfo(Landroid/content/Context;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    move-result-object v2

    .line 215
    new-instance v0, Lcom/flurry/sdk/lv;

    invoke-virtual {v2}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->isLimitAdTrackingEnabled()Z

    move-result v2

    invoke-direct {v0, v3, v2}, Lcom/flurry/sdk/lv;-><init>(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 230
    :goto_0
    return-object v0

    .line 217
    :catch_0
    move-exception v0

    sget-object v0, Lcom/flurry/sdk/ll;->b:Ljava/lang/String;

    const-string v2, "There is a problem with the Google Play Services library, which is required for Android Advertising ID support. The Google Play Services library should be integrated in any app shipping in the Play Store that uses analytics or advertising."

    invoke-static {v0, v2}, Lcom/flurry/sdk/mm;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 222
    goto :goto_0

    .line 223
    :catch_1
    move-exception v0

    .line 224
    sget-object v2, Lcom/flurry/sdk/ll;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GOOGLE PLAY SERVICES ERROR: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/flurry/sdk/mm;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    sget-object v0, Lcom/flurry/sdk/ll;->b:Ljava/lang/String;

    const-string v2, "There is a problem with the Google Play Services library, which is required for Android Advertising ID support. The Google Play Services library should be integrated in any app shipping in the Play Store that uses analytics or advertising."

    invoke-static {v0, v2}, Lcom/flurry/sdk/mm;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 230
    goto :goto_0
.end method

.method private static f()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 312
    invoke-static {}, Lcom/flurry/sdk/ly;->a()Lcom/flurry/sdk/ly;

    move-result-object v1

    .line 3098
    iget-object v1, v1, Lcom/flurry/sdk/ly;->a:Landroid/content/Context;

    .line 3396
    const-string v2, ".flurryb."

    .line 313
    invoke-virtual {v1, v2}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 314
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 328
    :cond_0
    :goto_0
    return-object v0

    .line 321
    :cond_1
    :try_start_0
    new-instance v2, Ljava/io/DataInputStream;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4375
    const/4 v1, 0x1

    :try_start_1
    invoke-interface {v2}, Ljava/io/DataInput;->readInt()I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    if-eq v1, v3, :cond_2

    .line 326
    :goto_1
    invoke-static {v2}, Lcom/flurry/sdk/nx;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 4378
    :cond_2
    :try_start_2
    invoke-interface {v2}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    goto :goto_1

    .line 323
    :catch_0
    move-exception v1

    move-object v2, v0

    .line 324
    :goto_2
    const/4 v3, 0x6

    :try_start_3
    sget-object v4, Lcom/flurry/sdk/ll;->b:Ljava/lang/String;

    const-string v5, "Error when loading deviceId"

    invoke-static {v3, v4, v5, v1}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 326
    invoke-static {v2}, Lcom/flurry/sdk/nx;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_3
    invoke-static {v2}, Lcom/flurry/sdk/nx;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    .line 323
    :catch_1
    move-exception v1

    goto :goto_2
.end method

.method private g()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 332
    invoke-static {}, Lcom/flurry/sdk/ly;->a()Lcom/flurry/sdk/ly;

    move-result-object v1

    .line 5098
    iget-object v1, v1, Lcom/flurry/sdk/ly;->a:Landroid/content/Context;

    .line 332
    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    .line 333
    if-nez v1, :cond_1

    .line 366
    :cond_0
    :goto_0
    return-object v0

    .line 337
    :cond_1
    new-instance v2, Lcom/flurry/sdk/ll$3;

    invoke-direct {v2, p0}, Lcom/flurry/sdk/ll$3;-><init>(Lcom/flurry/sdk/ll;)V

    invoke-virtual {v1, v2}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v1

    .line 344
    if-eqz v1, :cond_0

    array-length v2, v1

    if-eqz v2, :cond_0

    .line 348
    const/4 v2, 0x0

    aget-object v1, v1, v2

    .line 350
    invoke-static {}, Lcom/flurry/sdk/ly;->a()Lcom/flurry/sdk/ly;

    move-result-object v2

    .line 6098
    iget-object v2, v2, Lcom/flurry/sdk/ly;->a:Landroid/content/Context;

    .line 351
    invoke-virtual {v2, v1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 352
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 359
    :try_start_0
    new-instance v2, Ljava/io/DataInputStream;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6382
    const v1, 0xb5fa

    :try_start_1
    invoke-interface {v2}, Ljava/io/DataInput;->readUnsignedShort()I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    if-eq v1, v3, :cond_3

    .line 364
    :cond_2
    :goto_1
    invoke-static {v2}, Lcom/flurry/sdk/nx;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 6386
    :cond_3
    const/4 v1, 0x2

    :try_start_2
    invoke-interface {v2}, Ljava/io/DataInput;->readUnsignedShort()I

    move-result v3

    if-ne v1, v3, :cond_2

    .line 6390
    invoke-interface {v2}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    .line 6392
    invoke-interface {v2}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    goto :goto_1

    .line 361
    :catch_0
    move-exception v1

    move-object v2, v0

    .line 362
    :goto_2
    const/4 v3, 0x6

    :try_start_3
    sget-object v4, Lcom/flurry/sdk/ll;->b:Ljava/lang/String;

    const-string v5, "Error when loading deviceId"

    invoke-static {v3, v4, v5, v1}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 364
    invoke-static {v2}, Lcom/flurry/sdk/nx;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_3
    invoke-static {v2}, Lcom/flurry/sdk/nx;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    .line 361
    :catch_1
    move-exception v1

    goto :goto_2
.end method

.method private h()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 412
    .line 7101
    iget-object v0, p0, Lcom/flurry/sdk/ll;->f:Lcom/flurry/sdk/lv;

    if-nez v0, :cond_2

    .line 7102
    const/4 v0, 0x0

    .line 413
    :goto_0
    if-eqz v0, :cond_0

    .line 414
    sget-object v1, Lcom/flurry/sdk/ll;->b:Ljava/lang/String;

    const-string v2, "Fetched advertising id"

    invoke-static {v3, v1, v2}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 415
    iget-object v1, p0, Lcom/flurry/sdk/ll;->a:Ljava/util/Map;

    sget-object v2, Lcom/flurry/sdk/lt;->b:Lcom/flurry/sdk/lt;

    .line 416
    invoke-static {v0}, Lcom/flurry/sdk/nx;->d(Ljava/lang/String;)[B

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 8117
    :cond_0
    iget-object v0, p0, Lcom/flurry/sdk/ll;->g:Ljava/lang/String;

    .line 420
    if-eqz v0, :cond_1

    .line 421
    sget-object v1, Lcom/flurry/sdk/ll;->b:Ljava/lang/String;

    const-string v2, "Fetched device id"

    invoke-static {v3, v1, v2}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 422
    iget-object v1, p0, Lcom/flurry/sdk/ll;->a:Ljava/util/Map;

    sget-object v2, Lcom/flurry/sdk/lt;->a:Lcom/flurry/sdk/lt;

    invoke-static {v0}, Lcom/flurry/sdk/nx;->d(Ljava/lang/String;)[B

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 424
    :cond_1
    return-void

    .line 7105
    :cond_2
    iget-object v0, p0, Lcom/flurry/sdk/ll;->f:Lcom/flurry/sdk/lv;

    .line 8028
    iget-object v0, v0, Lcom/flurry/sdk/lv;->a:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final b()Z
    .locals 2

    .prologue
    .line 97
    sget-object v0, Lcom/flurry/sdk/ll$a;->e:Lcom/flurry/sdk/ll$a;

    iget-object v1, p0, Lcom/flurry/sdk/ll;->e:Lcom/flurry/sdk/ll$a;

    invoke-virtual {v0, v1}, Lcom/flurry/sdk/ll$a;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 109
    iget-object v1, p0, Lcom/flurry/sdk/ll;->f:Lcom/flurry/sdk/lv;

    if-nez v1, :cond_1

    .line 113
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/flurry/sdk/ll;->f:Lcom/flurry/sdk/lv;

    .line 2037
    iget-boolean v1, v1, Lcom/flurry/sdk/lv;->b:Z

    .line 113
    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method
