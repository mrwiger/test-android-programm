.class public Lru/cn/ad/video/facebook/FacebookVideoAdapter;
.super Lru/cn/ad/video/VideoAdAdapter;
.source "FacebookVideoAdapter.java"


# instance fields
.field private adView:Lcom/facebook/ads/InstreamVideoAdView;

.field private final placementId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "adSystem"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lru/cn/ad/video/VideoAdAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    .line 28
    const-string v0, "placement_id"

    invoke-virtual {p2, v0}, Lru/cn/api/money_miner/replies/AdSystem;->getParamOrThrow(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->placementId:Ljava/lang/String;

    .line 29
    return-void
.end method

.method static synthetic access$000(Lru/cn/ad/video/facebook/FacebookVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/facebook/FacebookVideoAdapter;

    .prologue
    .line 19
    iget-object v0, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/ad/video/facebook/FacebookVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/facebook/FacebookVideoAdapter;

    .prologue
    .line 19
    iget-object v0, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/ad/video/facebook/FacebookVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/facebook/FacebookVideoAdapter;

    .prologue
    .line 19
    iget-object v0, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/ad/video/facebook/FacebookVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/facebook/FacebookVideoAdapter;

    .prologue
    .line 19
    iget-object v0, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .locals 3

    .prologue
    .line 90
    iget-object v1, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->adView:Lcom/facebook/ads/InstreamVideoAdView;

    if-eqz v1, :cond_0

    .line 91
    invoke-virtual {p0}, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->getRenderingSettings()Lru/cn/ad/video/RenderingSettings;

    move-result-object v0

    .line 92
    .local v0, "settings":Lru/cn/ad/video/RenderingSettings;
    iget-object v1, v0, Lru/cn/ad/video/RenderingSettings;->container:Landroid/view/ViewGroup;

    iget-object v2, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->adView:Lcom/facebook/ads/InstreamVideoAdView;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 94
    iget-object v1, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->adView:Lcom/facebook/ads/InstreamVideoAdView;

    invoke-virtual {v1}, Lcom/facebook/ads/InstreamVideoAdView;->destroy()V

    .line 96
    .end local v0    # "settings":Lru/cn/ad/video/RenderingSettings;
    :cond_0
    return-void
.end method

.method protected onLoad()V
    .locals 6

    .prologue
    .line 33
    new-instance v0, Lcom/facebook/ads/InstreamVideoAdView;

    iget-object v1, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->context:Landroid/content/Context;

    iget-object v2, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->placementId:Ljava/lang/String;

    new-instance v3, Lcom/facebook/ads/AdSize;

    const/16 v4, 0x3c0

    const/16 v5, 0x21c

    invoke-direct {v3, v4, v5}, Lcom/facebook/ads/AdSize;-><init>(II)V

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/ads/InstreamVideoAdView;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/ads/AdSize;)V

    iput-object v0, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->adView:Lcom/facebook/ads/InstreamVideoAdView;

    .line 34
    iget-object v0, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->adView:Lcom/facebook/ads/InstreamVideoAdView;

    new-instance v1, Lru/cn/ad/video/facebook/FacebookVideoAdapter$1;

    invoke-direct {v1, p0}, Lru/cn/ad/video/facebook/FacebookVideoAdapter$1;-><init>(Lru/cn/ad/video/facebook/FacebookVideoAdapter;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ads/InstreamVideoAdView;->setAdListener(Lcom/facebook/ads/InstreamVideoAdListener;)V

    .line 69
    iget-object v0, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->adView:Lcom/facebook/ads/InstreamVideoAdView;

    invoke-virtual {v0}, Lcom/facebook/ads/InstreamVideoAdView;->loadAd()V

    .line 70
    return-void
.end method

.method public show()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 74
    invoke-virtual {p0}, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->getRenderingSettings()Lru/cn/ad/video/RenderingSettings;

    move-result-object v0

    .line 75
    .local v0, "settings":Lru/cn/ad/video/RenderingSettings;
    iget-object v1, v0, Lru/cn/ad/video/RenderingSettings;->container:Landroid/view/ViewGroup;

    iget-object v2, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->adView:Lcom/facebook/ads/InstreamVideoAdView;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 76
    iget-object v1, v0, Lru/cn/ad/video/RenderingSettings;->container:Landroid/view/ViewGroup;

    iget-object v2, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->adView:Lcom/facebook/ads/InstreamVideoAdView;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 79
    iget-object v1, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->adView:Lcom/facebook/ads/InstreamVideoAdView;

    invoke-virtual {v1}, Lcom/facebook/ads/InstreamVideoAdView;->show()Z

    .line 82
    sget-object v1, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_START:Lru/cn/domain/statistics/inetra/AdvEvent;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/util/Map;)V

    .line 84
    iget-object v1, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    invoke-interface {v1}, Lru/cn/ad/AdAdapter$Listener;->onAdStarted()V

    .line 86
    :cond_0
    return-void
.end method
