.class public final enum Lru/cn/domain/tv/CurrentCategory$Type;
.super Ljava/lang/Enum;
.source "CurrentCategory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/domain/tv/CurrentCategory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/domain/tv/CurrentCategory$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/domain/tv/CurrentCategory$Type;

.field public static final enum all:Lru/cn/domain/tv/CurrentCategory$Type;

.field public static final enum billing:Lru/cn/domain/tv/CurrentCategory$Type;

.field public static final enum collection:Lru/cn/domain/tv/CurrentCategory$Type;

.field public static final enum fav:Lru/cn/domain/tv/CurrentCategory$Type;

.field public static final enum hd:Lru/cn/domain/tv/CurrentCategory$Type;

.field public static final enum intersections:Lru/cn/domain/tv/CurrentCategory$Type;

.field public static final enum porno:Lru/cn/domain/tv/CurrentCategory$Type;

.field public static final enum setting:Lru/cn/domain/tv/CurrentCategory$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 18
    new-instance v0, Lru/cn/domain/tv/CurrentCategory$Type;

    const-string v1, "fav"

    invoke-direct {v0, v1, v3}, Lru/cn/domain/tv/CurrentCategory$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/domain/tv/CurrentCategory$Type;->fav:Lru/cn/domain/tv/CurrentCategory$Type;

    .line 19
    new-instance v0, Lru/cn/domain/tv/CurrentCategory$Type;

    const-string v1, "collection"

    invoke-direct {v0, v1, v4}, Lru/cn/domain/tv/CurrentCategory$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/domain/tv/CurrentCategory$Type;->collection:Lru/cn/domain/tv/CurrentCategory$Type;

    .line 20
    new-instance v0, Lru/cn/domain/tv/CurrentCategory$Type;

    const-string v1, "all"

    invoke-direct {v0, v1, v5}, Lru/cn/domain/tv/CurrentCategory$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/domain/tv/CurrentCategory$Type;->all:Lru/cn/domain/tv/CurrentCategory$Type;

    .line 21
    new-instance v0, Lru/cn/domain/tv/CurrentCategory$Type;

    const-string v1, "billing"

    invoke-direct {v0, v1, v6}, Lru/cn/domain/tv/CurrentCategory$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/domain/tv/CurrentCategory$Type;->billing:Lru/cn/domain/tv/CurrentCategory$Type;

    .line 22
    new-instance v0, Lru/cn/domain/tv/CurrentCategory$Type;

    const-string v1, "porno"

    invoke-direct {v0, v1, v7}, Lru/cn/domain/tv/CurrentCategory$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/domain/tv/CurrentCategory$Type;->porno:Lru/cn/domain/tv/CurrentCategory$Type;

    .line 23
    new-instance v0, Lru/cn/domain/tv/CurrentCategory$Type;

    const-string v1, "setting"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lru/cn/domain/tv/CurrentCategory$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/domain/tv/CurrentCategory$Type;->setting:Lru/cn/domain/tv/CurrentCategory$Type;

    .line 24
    new-instance v0, Lru/cn/domain/tv/CurrentCategory$Type;

    const-string v1, "hd"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lru/cn/domain/tv/CurrentCategory$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/domain/tv/CurrentCategory$Type;->hd:Lru/cn/domain/tv/CurrentCategory$Type;

    .line 25
    new-instance v0, Lru/cn/domain/tv/CurrentCategory$Type;

    const-string v1, "intersections"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lru/cn/domain/tv/CurrentCategory$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/domain/tv/CurrentCategory$Type;->intersections:Lru/cn/domain/tv/CurrentCategory$Type;

    .line 17
    const/16 v0, 0x8

    new-array v0, v0, [Lru/cn/domain/tv/CurrentCategory$Type;

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->fav:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v3

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->collection:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->all:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v5

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->billing:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->porno:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lru/cn/domain/tv/CurrentCategory$Type;->setting:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lru/cn/domain/tv/CurrentCategory$Type;->hd:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lru/cn/domain/tv/CurrentCategory$Type;->intersections:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v2, v0, v1

    sput-object v0, Lru/cn/domain/tv/CurrentCategory$Type;->$VALUES:[Lru/cn/domain/tv/CurrentCategory$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/domain/tv/CurrentCategory$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    const-class v0, Lru/cn/domain/tv/CurrentCategory$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/domain/tv/CurrentCategory$Type;

    return-object v0
.end method

.method public static values()[Lru/cn/domain/tv/CurrentCategory$Type;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lru/cn/domain/tv/CurrentCategory$Type;->$VALUES:[Lru/cn/domain/tv/CurrentCategory$Type;

    invoke-virtual {v0}, [Lru/cn/domain/tv/CurrentCategory$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/domain/tv/CurrentCategory$Type;

    return-object v0
.end method
