.class final Lcom/google/ads/interactivemedia/v3/impl/data/i;
.super Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;
.source "IMASDK"


# instance fields
.field private final disableExperiments:Z

.field private final enableMonitorAppLifecycle:Z

.field private final extraParams:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final forceExperimentIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final forceTvMode:Z

.field private final ignoreStrictModeFalsePositives:Z

.field private final useQAStreamBaseUrl:Z

.field private final useTestStreamManager:Z

.field private final useVideoElementMock:Z

.field private final videoElementMockDuration:F


# virtual methods
.method public disableExperiments()Z
    .locals 1

    .prologue
    .line 13
    iget-boolean v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->disableExperiments:Z

    return v0
.end method

.method public enableMonitorAppLifecycle()Z
    .locals 1

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->enableMonitorAppLifecycle:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 24
    if-ne p1, p0, :cond_1

    .line 39
    :cond_0
    :goto_0
    return v0

    .line 26
    :cond_1
    instance-of v2, p1, Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;

    if-eqz v2, :cond_5

    .line 27
    check-cast p1, Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;

    .line 28
    iget-boolean v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->disableExperiments:Z

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;->disableExperiments()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->useVideoElementMock:Z

    .line 29
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;->useVideoElementMock()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->videoElementMockDuration:F

    .line 30
    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;->videoElementMockDuration()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->useQAStreamBaseUrl:Z

    .line 31
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;->useQAStreamBaseUrl()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->useTestStreamManager:Z

    .line 32
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;->useTestStreamManager()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->enableMonitorAppLifecycle:Z

    .line 33
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;->enableMonitorAppLifecycle()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->forceExperimentIds:Ljava/util/List;

    if-nez v2, :cond_3

    .line 34
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;->forceExperimentIds()Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1
    iget-boolean v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->forceTvMode:Z

    .line 35
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;->forceTvMode()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->ignoreStrictModeFalsePositives:Z

    .line 36
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;->ignoreStrictModeFalsePositives()Z

    move-result v3

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->extraParams:Ljava/util/Map;

    if-nez v2, :cond_4

    .line 37
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;->extraParams()Ljava/util/Map;

    move-result-object v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 34
    :cond_3
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->forceExperimentIds:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;->forceExperimentIds()Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_1

    .line 37
    :cond_4
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->extraParams:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;->extraParams()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    :cond_5
    move v0, v1

    .line 39
    goto :goto_0
.end method

.method public extraParams()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->extraParams:Ljava/util/Map;

    return-object v0
.end method

.method public forceExperimentIds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->forceExperimentIds:Ljava/util/List;

    return-object v0
.end method

.method public forceTvMode()Z
    .locals 1

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->forceTvMode:Z

    return v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    const v5, 0xf4243

    .line 40
    .line 42
    iget-boolean v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->disableExperiments:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    xor-int/2addr v0, v5

    .line 43
    mul-int v4, v0, v5

    .line 44
    iget-boolean v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->useVideoElementMock:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    xor-int/2addr v0, v4

    .line 45
    mul-int/2addr v0, v5

    .line 46
    iget v4, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->videoElementMockDuration:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    xor-int/2addr v0, v4

    .line 47
    mul-int v4, v0, v5

    .line 48
    iget-boolean v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->useQAStreamBaseUrl:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    xor-int/2addr v0, v4

    .line 49
    mul-int v4, v0, v5

    .line 50
    iget-boolean v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->useTestStreamManager:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    xor-int/2addr v0, v4

    .line 51
    mul-int v4, v0, v5

    .line 52
    iget-boolean v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->enableMonitorAppLifecycle:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    xor-int/2addr v0, v4

    .line 53
    mul-int v4, v0, v5

    .line 54
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->forceExperimentIds:Ljava/util/List;

    if-nez v0, :cond_5

    move v0, v3

    :goto_5
    xor-int/2addr v0, v4

    .line 55
    mul-int v4, v0, v5

    .line 56
    iget-boolean v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->forceTvMode:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    xor-int/2addr v0, v4

    .line 57
    mul-int/2addr v0, v5

    .line 58
    iget-boolean v4, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->ignoreStrictModeFalsePositives:Z

    if-eqz v4, :cond_7

    :goto_7
    xor-int/2addr v0, v1

    .line 59
    mul-int/2addr v0, v5

    .line 60
    iget-object v1, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->extraParams:Ljava/util/Map;

    if-nez v1, :cond_8

    :goto_8
    xor-int/2addr v0, v3

    .line 61
    return v0

    :cond_0
    move v0, v2

    .line 42
    goto :goto_0

    :cond_1
    move v0, v2

    .line 44
    goto :goto_1

    :cond_2
    move v0, v2

    .line 48
    goto :goto_2

    :cond_3
    move v0, v2

    .line 50
    goto :goto_3

    :cond_4
    move v0, v2

    .line 52
    goto :goto_4

    .line 54
    :cond_5
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->forceExperimentIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    goto :goto_5

    :cond_6
    move v0, v2

    .line 56
    goto :goto_6

    :cond_7
    move v1, v2

    .line 58
    goto :goto_7

    .line 60
    :cond_8
    iget-object v1, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->extraParams:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->hashCode()I

    move-result v3

    goto :goto_8
.end method

.method public ignoreStrictModeFalsePositives()Z
    .locals 1

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->ignoreStrictModeFalsePositives:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 12

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->disableExperiments:Z

    iget-boolean v1, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->useVideoElementMock:Z

    iget v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->videoElementMockDuration:F

    iget-boolean v3, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->useQAStreamBaseUrl:Z

    iget-boolean v4, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->useTestStreamManager:Z

    iget-boolean v5, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->enableMonitorAppLifecycle:Z

    iget-object v6, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->forceExperimentIds:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iget-boolean v7, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->forceTvMode:Z

    iget-boolean v8, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->ignoreStrictModeFalsePositives:Z

    iget-object v9, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->extraParams:Ljava/util/Map;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit16 v10, v10, 0x126

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "TestingConfiguration{disableExperiments="

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v10, ", useVideoElementMock="

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", videoElementMockDuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", useQAStreamBaseUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", useTestStreamManager="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", enableMonitorAppLifecycle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", forceExperimentIds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", forceTvMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ignoreStrictModeFalsePositives="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", extraParams="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public useQAStreamBaseUrl()Z
    .locals 1

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->useQAStreamBaseUrl:Z

    return v0
.end method

.method public useTestStreamManager()Z
    .locals 1

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->useTestStreamManager:Z

    return v0
.end method

.method public useVideoElementMock()Z
    .locals 1

    .prologue
    .line 14
    iget-boolean v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->useVideoElementMock:Z

    return v0
.end method

.method public videoElementMockDuration()F
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/i;->videoElementMockDuration:F

    return v0
.end method
