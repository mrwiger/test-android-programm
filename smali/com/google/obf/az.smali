.class public final Lcom/google/obf/az;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/aj;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/az$a;
    }
.end annotation


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I


# instance fields
.field private final d:J

.field private final e:Lcom/google/obf/dw;

.field private final f:Lcom/google/obf/dt;

.field private g:Lcom/google/obf/al;

.field private h:Lcom/google/obf/ar;

.field private i:I

.field private j:Lcom/google/obf/an;

.field private k:Lcom/google/obf/az$a;

.field private l:J

.field private m:J

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 142
    const-string v0, "Xing"

    invoke-static {v0}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/obf/az;->a:I

    .line 143
    const-string v0, "Info"

    invoke-static {v0}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/obf/az;->b:I

    .line 144
    const-string v0, "VBRI"

    invoke-static {v0}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/obf/az;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1
    const-wide/16 v0, -0x1

    invoke-direct {p0, v0, v1}, Lcom/google/obf/az;-><init>(J)V

    .line 2
    return-void
.end method

.method public constructor <init>(J)V
    .locals 3

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-wide p1, p0, Lcom/google/obf/az;->d:J

    .line 5
    new-instance v0, Lcom/google/obf/dw;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/az;->e:Lcom/google/obf/dw;

    .line 6
    new-instance v0, Lcom/google/obf/dt;

    invoke-direct {v0}, Lcom/google/obf/dt;-><init>()V

    iput-object v0, p0, Lcom/google/obf/az;->f:Lcom/google/obf/dt;

    .line 7
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/obf/az;->l:J

    .line 8
    return-void
.end method

.method private a(Lcom/google/obf/ak;Z)Z
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x4

    const v9, -0x1f400

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 69
    .line 73
    invoke-interface {p1}, Lcom/google/obf/ak;->a()V

    .line 74
    invoke-interface {p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v0

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-nez v0, :cond_b

    .line 75
    invoke-static {p1}, Lcom/google/obf/ay;->a(Lcom/google/obf/ak;)Lcom/google/obf/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/az;->j:Lcom/google/obf/an;

    .line 76
    invoke-interface {p1}, Lcom/google/obf/ak;->b()J

    move-result-wide v0

    long-to-int v0, v0

    .line 77
    if-nez p2, :cond_0

    .line 78
    invoke-interface {p1, v0}, Lcom/google/obf/ak;->b(I)V

    :cond_0
    move v6, v0

    move v1, v2

    move v3, v2

    move v4, v2

    .line 79
    :goto_0
    if-eqz p2, :cond_2

    const/16 v0, 0x1000

    if-ne v4, v0, :cond_2

    .line 108
    :cond_1
    :goto_1
    return v2

    .line 81
    :cond_2
    if-nez p2, :cond_3

    const/high16 v0, 0x20000

    if-ne v4, v0, :cond_3

    .line 82
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Searched too many bytes."

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_3
    iget-object v0, p0, Lcom/google/obf/az;->e:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    invoke-interface {p1, v0, v2, v10, v5}, Lcom/google/obf/ak;->b([BIIZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    iget-object v0, p0, Lcom/google/obf/az;->e:Lcom/google/obf/dw;

    invoke-virtual {v0, v2}, Lcom/google/obf/dw;->c(I)V

    .line 86
    iget-object v0, p0, Lcom/google/obf/az;->e:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->m()I

    move-result v0

    .line 87
    if-eqz v1, :cond_4

    and-int v7, v0, v9

    and-int v8, v1, v9

    if-ne v7, v8, :cond_5

    .line 88
    :cond_4
    invoke-static {v0}, Lcom/google/obf/dt;->a(I)I

    move-result v7

    const/4 v8, -0x1

    if-ne v7, v8, :cond_7

    .line 91
    :cond_5
    add-int/lit8 v0, v4, 0x1

    .line 92
    if-eqz p2, :cond_6

    .line 93
    invoke-interface {p1}, Lcom/google/obf/ak;->a()V

    .line 94
    add-int v1, v6, v0

    invoke-interface {p1, v1}, Lcom/google/obf/ak;->c(I)V

    move v1, v2

    move v3, v0

    move v0, v2

    :goto_2
    move v4, v3

    move v3, v1

    move v1, v0

    .line 103
    goto :goto_0

    .line 95
    :cond_6
    invoke-interface {p1, v5}, Lcom/google/obf/ak;->b(I)V

    move v1, v2

    move v3, v0

    move v0, v2

    goto :goto_2

    .line 96
    :cond_7
    add-int/lit8 v3, v3, 0x1

    .line 97
    if-ne v3, v5, :cond_8

    .line 98
    iget-object v1, p0, Lcom/google/obf/az;->f:Lcom/google/obf/dt;

    invoke-static {v0, v1}, Lcom/google/obf/dt;->a(ILcom/google/obf/dt;)Z

    .line 102
    :goto_3
    add-int/lit8 v1, v7, -0x4

    invoke-interface {p1, v1}, Lcom/google/obf/ak;->c(I)V

    move v1, v3

    move v3, v4

    goto :goto_2

    .line 100
    :cond_8
    if-ne v3, v10, :cond_a

    .line 104
    if-eqz p2, :cond_9

    .line 105
    add-int v0, v6, v4

    invoke-interface {p1, v0}, Lcom/google/obf/ak;->b(I)V

    .line 107
    :goto_4
    iput v1, p0, Lcom/google/obf/az;->i:I

    move v2, v5

    .line 108
    goto :goto_1

    .line 106
    :cond_9
    invoke-interface {p1}, Lcom/google/obf/ak;->a()V

    goto :goto_4

    :cond_a
    move v0, v1

    goto :goto_3

    :cond_b
    move v6, v2

    move v1, v2

    move v3, v2

    move v4, v2

    goto :goto_0
.end method

.method private b(Lcom/google/obf/ak;)I
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, -0x1

    const/4 v4, 0x1

    const/4 v0, -0x1

    const/4 v6, 0x0

    .line 33
    iget v1, p0, Lcom/google/obf/az;->n:I

    if-nez v1, :cond_3

    .line 34
    invoke-direct {p0, p1}, Lcom/google/obf/az;->c(Lcom/google/obf/ak;)Z

    move-result v1

    if-nez v1, :cond_1

    move v6, v0

    .line 52
    :cond_0
    :goto_0
    return v6

    .line 36
    :cond_1
    iget-wide v2, p0, Lcom/google/obf/az;->l:J

    cmp-long v1, v2, v8

    if-nez v1, :cond_2

    .line 37
    iget-object v1, p0, Lcom/google/obf/az;->k:Lcom/google/obf/az$a;

    invoke-interface {p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/google/obf/az$a;->a(J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/obf/az;->l:J

    .line 38
    iget-wide v2, p0, Lcom/google/obf/az;->d:J

    cmp-long v1, v2, v8

    if-eqz v1, :cond_2

    .line 39
    iget-object v1, p0, Lcom/google/obf/az;->k:Lcom/google/obf/az$a;

    const-wide/16 v2, 0x0

    invoke-interface {v1, v2, v3}, Lcom/google/obf/az$a;->a(J)J

    move-result-wide v2

    .line 40
    iget-wide v8, p0, Lcom/google/obf/az;->l:J

    iget-wide v10, p0, Lcom/google/obf/az;->d:J

    sub-long v2, v10, v2

    add-long/2addr v2, v8

    iput-wide v2, p0, Lcom/google/obf/az;->l:J

    .line 41
    :cond_2
    iget-object v1, p0, Lcom/google/obf/az;->f:Lcom/google/obf/dt;

    iget v1, v1, Lcom/google/obf/dt;->c:I

    iput v1, p0, Lcom/google/obf/az;->n:I

    .line 42
    :cond_3
    iget-object v1, p0, Lcom/google/obf/az;->h:Lcom/google/obf/ar;

    iget v2, p0, Lcom/google/obf/az;->n:I

    invoke-interface {v1, p1, v2, v4}, Lcom/google/obf/ar;->a(Lcom/google/obf/ak;IZ)I

    move-result v1

    .line 43
    if-ne v1, v0, :cond_4

    move v6, v0

    .line 44
    goto :goto_0

    .line 45
    :cond_4
    iget v0, p0, Lcom/google/obf/az;->n:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/az;->n:I

    .line 46
    iget v0, p0, Lcom/google/obf/az;->n:I

    if-gtz v0, :cond_0

    .line 48
    iget-wide v0, p0, Lcom/google/obf/az;->l:J

    iget-wide v2, p0, Lcom/google/obf/az;->m:J

    const-wide/32 v8, 0xf4240

    mul-long/2addr v2, v8

    iget-object v5, p0, Lcom/google/obf/az;->f:Lcom/google/obf/dt;

    iget v5, v5, Lcom/google/obf/dt;->d:I

    int-to-long v8, v5

    div-long/2addr v2, v8

    add-long/2addr v2, v0

    .line 49
    iget-object v1, p0, Lcom/google/obf/az;->h:Lcom/google/obf/ar;

    iget-object v0, p0, Lcom/google/obf/az;->f:Lcom/google/obf/dt;

    iget v5, v0, Lcom/google/obf/dt;->c:I

    const/4 v7, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/google/obf/ar;->a(JIII[B)V

    .line 50
    iget-wide v0, p0, Lcom/google/obf/az;->m:J

    iget-object v2, p0, Lcom/google/obf/az;->f:Lcom/google/obf/dt;

    iget v2, v2, Lcom/google/obf/dt;->g:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/obf/az;->m:J

    .line 51
    iput v6, p0, Lcom/google/obf/az;->n:I

    goto :goto_0
.end method

.method private c(Lcom/google/obf/ak;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const v5, -0x1f400

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 53
    invoke-interface {p1}, Lcom/google/obf/ak;->a()V

    .line 54
    iget-object v2, p0, Lcom/google/obf/az;->e:Lcom/google/obf/dw;

    iget-object v2, v2, Lcom/google/obf/dw;->a:[B

    const/4 v3, 0x4

    invoke-interface {p1, v2, v0, v3, v1}, Lcom/google/obf/ak;->b([BIIZ)Z

    move-result v2

    if-nez v2, :cond_0

    .line 65
    :goto_0
    return v0

    .line 56
    :cond_0
    iget-object v2, p0, Lcom/google/obf/az;->e:Lcom/google/obf/dw;

    invoke-virtual {v2, v0}, Lcom/google/obf/dw;->c(I)V

    .line 57
    iget-object v2, p0, Lcom/google/obf/az;->e:Lcom/google/obf/dw;

    invoke-virtual {v2}, Lcom/google/obf/dw;->m()I

    move-result v2

    .line 58
    and-int v3, v2, v5

    iget v4, p0, Lcom/google/obf/az;->i:I

    and-int/2addr v4, v5

    if-ne v3, v4, :cond_1

    .line 59
    invoke-static {v2}, Lcom/google/obf/dt;->a(I)I

    move-result v3

    .line 60
    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 61
    iget-object v0, p0, Lcom/google/obf/az;->f:Lcom/google/obf/dt;

    invoke-static {v2, v0}, Lcom/google/obf/dt;->a(ILcom/google/obf/dt;)Z

    move v0, v1

    .line 62
    goto :goto_0

    .line 63
    :cond_1
    iput v0, p0, Lcom/google/obf/az;->i:I

    .line 64
    invoke-interface {p1, v1}, Lcom/google/obf/ak;->b(I)V

    .line 65
    invoke-direct {p0, p1}, Lcom/google/obf/az;->d(Lcom/google/obf/ak;)Z

    move-result v0

    goto :goto_0
.end method

.method private d(Lcom/google/obf/ak;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 66
    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, p1, v1}, Lcom/google/obf/az;->a(Lcom/google/obf/ak;Z)Z
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 68
    :goto_0
    return v0

    .line 67
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private e(Lcom/google/obf/ak;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/16 v0, 0x24

    const/16 v6, 0x15

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 109
    new-instance v1, Lcom/google/obf/dw;

    iget-object v2, p0, Lcom/google/obf/az;->f:Lcom/google/obf/dt;

    iget v2, v2, Lcom/google/obf/dt;->c:I

    invoke-direct {v1, v2}, Lcom/google/obf/dw;-><init>(I)V

    .line 110
    iget-object v2, v1, Lcom/google/obf/dw;->a:[B

    iget-object v3, p0, Lcom/google/obf/az;->f:Lcom/google/obf/dt;

    iget v3, v3, Lcom/google/obf/dt;->c:I

    invoke-interface {p1, v2, v8, v3}, Lcom/google/obf/ak;->c([BII)V

    .line 111
    invoke-interface {p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v2

    .line 112
    invoke-interface {p1}, Lcom/google/obf/ak;->d()J

    move-result-wide v4

    .line 114
    iget-object v7, p0, Lcom/google/obf/az;->f:Lcom/google/obf/dt;

    iget v7, v7, Lcom/google/obf/dt;->a:I

    and-int/lit8 v7, v7, 0x1

    if-eqz v7, :cond_5

    .line 115
    iget-object v7, p0, Lcom/google/obf/az;->f:Lcom/google/obf/dt;

    iget v7, v7, Lcom/google/obf/dt;->e:I

    if-eq v7, v9, :cond_0

    move v6, v0

    .line 117
    :cond_0
    :goto_0
    invoke-virtual {v1}, Lcom/google/obf/dw;->c()I

    move-result v7

    add-int/lit8 v9, v6, 0x4

    if-lt v7, v9, :cond_7

    .line 118
    invoke-virtual {v1, v6}, Lcom/google/obf/dw;->c(I)V

    .line 119
    invoke-virtual {v1}, Lcom/google/obf/dw;->m()I

    move-result v7

    .line 120
    :goto_1
    sget v9, Lcom/google/obf/az;->a:I

    if-eq v7, v9, :cond_1

    sget v9, Lcom/google/obf/az;->b:I

    if-ne v7, v9, :cond_6

    .line 121
    :cond_1
    iget-object v0, p0, Lcom/google/obf/az;->f:Lcom/google/obf/dt;

    invoke-static/range {v0 .. v5}, Lcom/google/obf/bb;->a(Lcom/google/obf/dt;Lcom/google/obf/dw;JJ)Lcom/google/obf/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/az;->k:Lcom/google/obf/az$a;

    .line 122
    iget-object v0, p0, Lcom/google/obf/az;->k:Lcom/google/obf/az$a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/obf/az;->j:Lcom/google/obf/an;

    if-nez v0, :cond_2

    .line 123
    invoke-interface {p1}, Lcom/google/obf/ak;->a()V

    .line 124
    add-int/lit16 v0, v6, 0x8d

    invoke-interface {p1, v0}, Lcom/google/obf/ak;->c(I)V

    .line 125
    iget-object v0, p0, Lcom/google/obf/az;->e:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    const/4 v1, 0x3

    invoke-interface {p1, v0, v8, v1}, Lcom/google/obf/ak;->c([BII)V

    .line 126
    iget-object v0, p0, Lcom/google/obf/az;->e:Lcom/google/obf/dw;

    invoke-virtual {v0, v8}, Lcom/google/obf/dw;->c(I)V

    .line 127
    iget-object v0, p0, Lcom/google/obf/az;->e:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->j()I

    move-result v0

    invoke-static {v0}, Lcom/google/obf/an;->a(I)Lcom/google/obf/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/az;->j:Lcom/google/obf/an;

    .line 128
    :cond_2
    iget-object v0, p0, Lcom/google/obf/az;->f:Lcom/google/obf/dt;

    iget v0, v0, Lcom/google/obf/dt;->c:I

    invoke-interface {p1, v0}, Lcom/google/obf/ak;->b(I)V

    .line 135
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/google/obf/az;->k:Lcom/google/obf/az$a;

    if-nez v0, :cond_4

    .line 136
    invoke-interface {p1}, Lcom/google/obf/ak;->a()V

    .line 137
    iget-object v0, p0, Lcom/google/obf/az;->e:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    const/4 v1, 0x4

    invoke-interface {p1, v0, v8, v1}, Lcom/google/obf/ak;->c([BII)V

    .line 138
    iget-object v0, p0, Lcom/google/obf/az;->e:Lcom/google/obf/dw;

    invoke-virtual {v0, v8}, Lcom/google/obf/dw;->c(I)V

    .line 139
    iget-object v0, p0, Lcom/google/obf/az;->e:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->m()I

    move-result v0

    iget-object v1, p0, Lcom/google/obf/az;->f:Lcom/google/obf/dt;

    invoke-static {v0, v1}, Lcom/google/obf/dt;->a(ILcom/google/obf/dt;)Z

    .line 140
    new-instance v0, Lcom/google/obf/ax;

    invoke-interface {p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/obf/az;->f:Lcom/google/obf/dt;

    iget v3, v3, Lcom/google/obf/dt;->f:I

    invoke-direct/range {v0 .. v5}, Lcom/google/obf/ax;-><init>(JIJ)V

    iput-object v0, p0, Lcom/google/obf/az;->k:Lcom/google/obf/az$a;

    .line 141
    :cond_4
    return-void

    .line 116
    :cond_5
    iget-object v7, p0, Lcom/google/obf/az;->f:Lcom/google/obf/dt;

    iget v7, v7, Lcom/google/obf/dt;->e:I

    if-ne v7, v9, :cond_0

    const/16 v6, 0xd

    goto/16 :goto_0

    .line 129
    :cond_6
    invoke-virtual {v1}, Lcom/google/obf/dw;->c()I

    move-result v6

    const/16 v7, 0x28

    if-lt v6, v7, :cond_3

    .line 130
    invoke-virtual {v1, v0}, Lcom/google/obf/dw;->c(I)V

    .line 131
    invoke-virtual {v1}, Lcom/google/obf/dw;->m()I

    move-result v0

    .line 132
    sget v6, Lcom/google/obf/az;->c:I

    if-ne v0, v6, :cond_3

    .line 133
    iget-object v0, p0, Lcom/google/obf/az;->f:Lcom/google/obf/dt;

    invoke-static/range {v0 .. v5}, Lcom/google/obf/ba;->a(Lcom/google/obf/dt;Lcom/google/obf/dw;JJ)Lcom/google/obf/ba;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/az;->k:Lcom/google/obf/az$a;

    .line 134
    iget-object v0, p0, Lcom/google/obf/az;->f:Lcom/google/obf/dt;

    iget v0, v0, Lcom/google/obf/dt;->c:I

    invoke-interface {p1, v0}, Lcom/google/obf/ak;->b(I)V

    goto :goto_2

    :cond_7
    move v7, v8

    goto/16 :goto_1
.end method


# virtual methods
.method public a(Lcom/google/obf/ak;Lcom/google/obf/ao;)I
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v2, -0x1

    const/4 v0, 0x0

    .line 20
    iget v1, p0, Lcom/google/obf/az;->i:I

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/obf/az;->d(Lcom/google/obf/ak;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 32
    :goto_0
    return v2

    .line 22
    :cond_0
    iget-object v1, p0, Lcom/google/obf/az;->k:Lcom/google/obf/az$a;

    if-nez v1, :cond_2

    .line 23
    invoke-direct {p0, p1}, Lcom/google/obf/az;->e(Lcom/google/obf/ak;)V

    .line 24
    iget-object v1, p0, Lcom/google/obf/az;->g:Lcom/google/obf/al;

    iget-object v3, p0, Lcom/google/obf/az;->k:Lcom/google/obf/az$a;

    invoke-interface {v1, v3}, Lcom/google/obf/al;->a(Lcom/google/obf/aq;)V

    .line 25
    iget-object v1, p0, Lcom/google/obf/az;->f:Lcom/google/obf/dt;

    iget-object v1, v1, Lcom/google/obf/dt;->b:Ljava/lang/String;

    const/16 v3, 0x1000

    iget-object v4, p0, Lcom/google/obf/az;->k:Lcom/google/obf/az$a;

    .line 26
    invoke-interface {v4}, Lcom/google/obf/az$a;->b()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/obf/az;->f:Lcom/google/obf/dt;

    iget v6, v6, Lcom/google/obf/dt;->e:I

    iget-object v7, p0, Lcom/google/obf/az;->f:Lcom/google/obf/dt;

    iget v7, v7, Lcom/google/obf/dt;->d:I

    move-object v8, v0

    move-object v9, v0

    .line 27
    invoke-static/range {v0 .. v9}, Lcom/google/obf/q;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)Lcom/google/obf/q;

    move-result-object v0

    .line 28
    iget-object v1, p0, Lcom/google/obf/az;->j:Lcom/google/obf/an;

    if-eqz v1, :cond_1

    .line 29
    iget-object v1, p0, Lcom/google/obf/az;->j:Lcom/google/obf/an;

    iget v1, v1, Lcom/google/obf/an;->a:I

    iget-object v2, p0, Lcom/google/obf/az;->j:Lcom/google/obf/an;

    iget v2, v2, Lcom/google/obf/an;->b:I

    .line 30
    invoke-virtual {v0, v1, v2}, Lcom/google/obf/q;->a(II)Lcom/google/obf/q;

    move-result-object v0

    .line 31
    :cond_1
    iget-object v1, p0, Lcom/google/obf/az;->h:Lcom/google/obf/ar;

    invoke-interface {v1, v0}, Lcom/google/obf/ar;->a(Lcom/google/obf/q;)V

    .line 32
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/obf/az;->b(Lcom/google/obf/ak;)I

    move-result v2

    goto :goto_0
.end method

.method public a(Lcom/google/obf/al;)V
    .locals 1

    .prologue
    .line 10
    iput-object p1, p0, Lcom/google/obf/az;->g:Lcom/google/obf/al;

    .line 11
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/az;->h:Lcom/google/obf/ar;

    .line 12
    invoke-interface {p1}, Lcom/google/obf/al;->f()V

    .line 13
    return-void
.end method

.method public a(Lcom/google/obf/ak;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 9
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/obf/az;->a(Lcom/google/obf/ak;Z)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 14
    iput v2, p0, Lcom/google/obf/az;->i:I

    .line 15
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/obf/az;->m:J

    .line 16
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/obf/az;->l:J

    .line 17
    iput v2, p0, Lcom/google/obf/az;->n:I

    .line 18
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 19
    return-void
.end method
