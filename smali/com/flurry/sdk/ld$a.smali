.class public final Lcom/flurry/sdk/ld$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/flurry/sdk/nh;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flurry/sdk/ld;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/flurry/sdk/nh",
        "<",
        "Lcom/flurry/sdk/ld;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 279
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 276
    .line 1312
    if-nez p1, :cond_1

    .line 1313
    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    .line 1316
    :cond_1
    new-instance v1, Lcom/flurry/sdk/ld$a$2;

    invoke-direct {v1, p0, p1}, Lcom/flurry/sdk/ld$a$2;-><init>(Lcom/flurry/sdk/ld$a;Ljava/io/InputStream;)V

    .line 1323
    new-instance v0, Lcom/flurry/sdk/ld;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/flurry/sdk/ld;-><init>(B)V

    .line 1327
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v2

    .line 1328
    if-lez v2, :cond_0

    .line 1329
    new-array v2, v2, [B

    .line 1330
    invoke-virtual {v1, v2}, Ljava/io/DataInputStream;->readFully([B)V

    .line 1333
    iput-object v2, v0, Lcom/flurry/sdk/ld;->a:[B

    goto :goto_0
.end method

.method public final synthetic a(Ljava/io/OutputStream;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 276
    check-cast p2, Lcom/flurry/sdk/ld;

    .line 2285
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 2286
    :cond_0
    :goto_0
    return-void

    .line 2289
    :cond_1
    new-instance v1, Lcom/flurry/sdk/ld$a$1;

    invoke-direct {v1, p0, p1}, Lcom/flurry/sdk/ld$a$1;-><init>(Lcom/flurry/sdk/ld$a;Ljava/io/OutputStream;)V

    .line 2296
    const/4 v0, 0x0

    .line 2297
    iget-object v2, p2, Lcom/flurry/sdk/ld;->a:[B

    if-eqz v2, :cond_2

    .line 2298
    iget-object v0, p2, Lcom/flurry/sdk/ld;->a:[B

    array-length v0, v0

    .line 2302
    :cond_2
    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 2303
    if-lez v0, :cond_3

    .line 2304
    iget-object v0, p2, Lcom/flurry/sdk/ld;->a:[B

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->write([B)V

    .line 2307
    :cond_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->flush()V

    goto :goto_0
.end method
