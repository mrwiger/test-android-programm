.class public Ltoothpick/ThreadSafeProviderImpl;
.super Ljava/lang/Object;
.source "ThreadSafeProviderImpl.java"

# interfaces
.implements Ljavax/inject/Provider;
.implements Ltoothpick/Lazy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljavax/inject/Provider",
        "<TT;>;",
        "Ltoothpick/Lazy",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private clazz:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field private volatile instance:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private isLazy:Z

.field private name:Ljava/lang/String;

.field private scope:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ltoothpick/Scope;",
            ">;"
        }
    .end annotation
.end field

.field private scopeName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ltoothpick/Scope;Ljava/lang/Class;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "scope"    # Ltoothpick/Scope;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "isLazy"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltoothpick/Scope;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p0, "this":Ltoothpick/ThreadSafeProviderImpl;, "Ltoothpick/ThreadSafeProviderImpl<TT;>;"
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ltoothpick/ThreadSafeProviderImpl;->scope:Ljava/lang/ref/WeakReference;

    .line 21
    invoke-interface {p1}, Ltoothpick/Scope;->getName()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltoothpick/ThreadSafeProviderImpl;->scopeName:Ljava/lang/String;

    .line 22
    iput-object p2, p0, Ltoothpick/ThreadSafeProviderImpl;->clazz:Ljava/lang/Class;

    .line 23
    iput-object p3, p0, Ltoothpick/ThreadSafeProviderImpl;->name:Ljava/lang/String;

    .line 24
    iput-boolean p4, p0, Ltoothpick/ThreadSafeProviderImpl;->isLazy:Z

    .line 25
    return-void
.end method

.method private getScope()Ltoothpick/Scope;
    .locals 6

    .prologue
    .line 51
    .local p0, "this":Ltoothpick/ThreadSafeProviderImpl;, "Ltoothpick/ThreadSafeProviderImpl<TT;>;"
    iget-object v1, p0, Ltoothpick/ThreadSafeProviderImpl;->scope:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltoothpick/Scope;

    .line 52
    .local v0, "scope":Ltoothpick/Scope;
    if-nez v0, :cond_1

    .line 53
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "The instance provided by the %s cannot be created when the associated scope: %s has been closed"

    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-boolean v1, p0, Ltoothpick/ThreadSafeProviderImpl;->isLazy:Z

    if-eqz v1, :cond_0

    const-string v1, "lazy"

    :goto_0
    aput-object v1, v4, v5

    const/4 v1, 0x1

    iget-object v5, p0, Ltoothpick/ThreadSafeProviderImpl;->scopeName:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const-string v1, "provider"

    goto :goto_0

    .line 58
    :cond_1
    return-object v0
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 30
    .local p0, "this":Ltoothpick/ThreadSafeProviderImpl;, "Ltoothpick/ThreadSafeProviderImpl<TT;>;"
    iget-boolean v0, p0, Ltoothpick/ThreadSafeProviderImpl;->isLazy:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltoothpick/ThreadSafeProviderImpl;->instance:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Ltoothpick/ThreadSafeProviderImpl;->instance:Ljava/lang/Object;

    .line 46
    :goto_0
    return-object v0

    .line 37
    :cond_0
    monitor-enter p0

    .line 38
    :try_start_0
    iget-boolean v0, p0, Ltoothpick/ThreadSafeProviderImpl;->isLazy:Z

    if-eqz v0, :cond_2

    .line 40
    iget-object v0, p0, Ltoothpick/ThreadSafeProviderImpl;->instance:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 41
    invoke-direct {p0}, Ltoothpick/ThreadSafeProviderImpl;->getScope()Ltoothpick/Scope;

    move-result-object v0

    iget-object v1, p0, Ltoothpick/ThreadSafeProviderImpl;->clazz:Ljava/lang/Class;

    iget-object v2, p0, Ltoothpick/ThreadSafeProviderImpl;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ltoothpick/Scope;->getInstance(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Ltoothpick/ThreadSafeProviderImpl;->instance:Ljava/lang/Object;

    .line 42
    iget-object v0, p0, Ltoothpick/ThreadSafeProviderImpl;->scope:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    .line 44
    :cond_1
    iget-object v0, p0, Ltoothpick/ThreadSafeProviderImpl;->instance:Ljava/lang/Object;

    monitor-exit p0

    goto :goto_0

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 46
    :cond_2
    :try_start_1
    invoke-direct {p0}, Ltoothpick/ThreadSafeProviderImpl;->getScope()Ltoothpick/Scope;

    move-result-object v0

    iget-object v1, p0, Ltoothpick/ThreadSafeProviderImpl;->clazz:Ljava/lang/Class;

    iget-object v2, p0, Ltoothpick/ThreadSafeProviderImpl;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ltoothpick/Scope;->getInstance(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
