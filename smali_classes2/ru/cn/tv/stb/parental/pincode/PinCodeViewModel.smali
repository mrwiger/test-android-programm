.class Lru/cn/tv/stb/parental/pincode/PinCodeViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "PinCodeViewModel.java"


# instance fields
.field private final loader:Lru/cn/mvvm/RxLoader;

.field private final pinCode:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Throwable;",
            ">;>;"
        }
    .end annotation
.end field

.field private final pinCodeDomain:Lru/cn/domain/PinCode;


# direct methods
.method constructor <init>(Lru/cn/mvvm/RxLoader;Lru/cn/domain/PinCode;)V
    .locals 3
    .param p1, "loader"    # Lru/cn/mvvm/RxLoader;
    .param p2, "pinCodeDomain"    # Lru/cn/domain/PinCode;

    .prologue
    .line 28
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 29
    iput-object p1, p0, Lru/cn/tv/stb/parental/pincode/PinCodeViewModel;->loader:Lru/cn/mvvm/RxLoader;

    .line 30
    iput-object p2, p0, Lru/cn/tv/stb/parental/pincode/PinCodeViewModel;->pinCodeDomain:Lru/cn/domain/PinCode;

    .line 32
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/stb/parental/pincode/PinCodeViewModel;->pinCode:Landroid/arch/lifecycle/MutableLiveData;

    .line 34
    invoke-direct {p0}, Lru/cn/tv/stb/parental/pincode/PinCodeViewModel;->pinCodeSource()Lio/reactivex/Observable;

    move-result-object v0

    .line 35
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lru/cn/tv/stb/parental/pincode/PinCodeViewModel$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/parental/pincode/PinCodeViewModel$$Lambda$0;-><init>(Lru/cn/tv/stb/parental/pincode/PinCodeViewModel;)V

    sget-object v2, Lru/cn/tv/stb/parental/pincode/PinCodeViewModel$$Lambda$1;->$instance:Lio/reactivex/functions/Consumer;

    .line 36
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 34
    invoke-virtual {p0, v0}, Lru/cn/tv/stb/parental/pincode/PinCodeViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 47
    return-void
.end method

.method static final synthetic lambda$new$1$PinCodeViewModel(Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "throwable"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-static {v0, p0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    return-void
.end method

.method private pinCodeSource()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->userPornoPinCode()Landroid/net/Uri;

    move-result-object v0

    .line 60
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/stb/parental/pincode/PinCodeViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v1

    .line 61
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 60
    return-object v1
.end method


# virtual methods
.method changePin(Ljava/lang/String;)V
    .locals 2
    .param p1, "pinValue"    # Ljava/lang/String;

    .prologue
    .line 54
    iget-object v0, p0, Lru/cn/tv/stb/parental/pincode/PinCodeViewModel;->pinCodeDomain:Lru/cn/domain/PinCode;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lru/cn/domain/PinCode;->savePinCode(Ljava/lang/String;Lru/cn/domain/PinCode$PinCodeSaveCallbacks;)V

    .line 55
    return-void
.end method

.method final synthetic lambda$new$0$PinCodeViewModel(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 37
    const-string v0, ""

    .line 38
    .local v0, "pin":Ljava/lang/String;
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 39
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 40
    const-string v1, "pincode"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 43
    :cond_0
    iget-object v1, p0, Lru/cn/tv/stb/parental/pincode/PinCodeViewModel;->pinCode:Landroid/arch/lifecycle/MutableLiveData;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 44
    return-void
.end method

.method pinCode()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Throwable;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lru/cn/tv/stb/parental/pincode/PinCodeViewModel;->pinCode:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method
