.class public Lcom/yandex/metrica/impl/ob/bx;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ob/bx$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/ScheduledExecutorService;

.field private final b:Lcom/yandex/metrica/impl/ob/cj;

.field private final c:Lcom/yandex/metrica/impl/ob/cb;

.field private final d:Lcom/yandex/metrica/impl/utils/q;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;Lcom/yandex/metrica/impl/ob/cb;)V
    .locals 2

    .prologue
    .line 45
    new-instance v0, Lcom/yandex/metrica/impl/ob/cj;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/cj;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/yandex/metrica/impl/utils/p;

    invoke-direct {v1}, Lcom/yandex/metrica/impl/utils/p;-><init>()V

    invoke-direct {p0, p2, v0, p3, v1}, Lcom/yandex/metrica/impl/ob/bx;-><init>(Ljava/util/concurrent/ScheduledExecutorService;Lcom/yandex/metrica/impl/ob/cj;Lcom/yandex/metrica/impl/ob/cb;Lcom/yandex/metrica/impl/utils/q;)V

    .line 46
    return-void
.end method

.method constructor <init>(Ljava/util/concurrent/ScheduledExecutorService;Lcom/yandex/metrica/impl/ob/cj;Lcom/yandex/metrica/impl/ob/cb;Lcom/yandex/metrica/impl/utils/q;)V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/bx;->a:Ljava/util/concurrent/ScheduledExecutorService;

    .line 103
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/bx;->b:Lcom/yandex/metrica/impl/ob/cj;

    .line 104
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/bx;->c:Lcom/yandex/metrica/impl/ob/cb;

    .line 105
    iput-object p4, p0, Lcom/yandex/metrica/impl/ob/bx;->d:Lcom/yandex/metrica/impl/utils/q;

    .line 107
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bx;->c:Lcom/yandex/metrica/impl/ob/cb;

    invoke-interface {v0}, Lcom/yandex/metrica/impl/ob/cb;->a()V

    .line 72
    return-void
.end method

.method public a(J)V
    .locals 5

    .prologue
    .line 65
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bx;->c:Lcom/yandex/metrica/impl/ob/cb;

    invoke-interface {v0, p1, p2}, Lcom/yandex/metrica/impl/ob/cb;->a(J)V

    .line 1093
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bx;->b:Lcom/yandex/metrica/impl/ob/cj;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/bx;->d:Lcom/yandex/metrica/impl/utils/q;

    invoke-interface {v1}, Lcom/yandex/metrica/impl/utils/q;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/cj;->a(J)Lcom/yandex/metrica/impl/ob/cj;

    .line 67
    return-void
.end method

.method public a(JLcom/yandex/metrica/impl/ob/bx$a;)V
    .locals 3

    .prologue
    .line 50
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bx;->a:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/yandex/metrica/impl/ob/bx$1;

    invoke-direct {v1, p3}, Lcom/yandex/metrica/impl/ob/bx$1;-><init>(Lcom/yandex/metrica/impl/ob/bx$a;)V

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, p1, p2, v2}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 61
    return-void
.end method

.method public b()Z
    .locals 6

    .prologue
    .line 85
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bx;->d:Lcom/yandex/metrica/impl/utils/q;

    invoke-interface {v0}, Lcom/yandex/metrica/impl/utils/q;->a()J

    move-result-wide v0

    .line 2089
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/bx;->b:Lcom/yandex/metrica/impl/ob/cj;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/yandex/metrica/impl/ob/cj;->b(J)J

    move-result-wide v2

    .line 85
    sub-long/2addr v0, v2

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/bx;->d:Lcom/yandex/metrica/impl/utils/q;

    invoke-interface {v2}, Lcom/yandex/metrica/impl/utils/q;->b()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
