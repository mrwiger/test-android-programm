.class public Lru/cn/tv/mobile/settings/MainPreferenceFragment;
.super Landroid/support/v7/preference/PreferenceFragmentCompat;
.source "MainPreferenceFragment.java"


# instance fields
.field private linkDevicePreference:Landroid/support/v7/preference/Preference;

.field private logOutPreference:Landroid/support/v7/preference/Preference;

.field private profilePreference:Landroid/support/v7/preference/Preference;

.field private signInPreference:Landroid/support/v7/preference/Preference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/support/v7/preference/PreferenceFragmentCompat;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/mobile/settings/MainPreferenceFragment;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/mobile/settings/MainPreferenceFragment;

    .prologue
    .line 25
    invoke-direct {p0}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->updateAccountSection()V

    return-void
.end method

.method private openFeedback()V
    .locals 6

    .prologue
    .line 221
    :try_start_0
    invoke-static {}, Lru/cn/utils/FeedbackBuilder;->create()Lru/cn/utils/FeedbackBuilder;

    move-result-object v3

    .line 222
    invoke-virtual {p0}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v3, v4}, Lru/cn/utils/FeedbackBuilder;->setContext(Landroid/content/Context;)Lru/cn/utils/FeedbackBuilder;

    move-result-object v3

    .line 223
    invoke-virtual {v3}, Lru/cn/utils/FeedbackBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    .line 225
    .local v1, "emailIntent":Landroid/content/Intent;
    const v3, 0x7f0e0072

    .line 226
    invoke-virtual {p0, v3}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 225
    invoke-static {v1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    .end local v1    # "emailIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 227
    :catch_0
    move-exception v0

    .line 228
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f0e00b8

    .line 229
    invoke-virtual {p0, v4}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    const/4 v5, 0x0

    .line 228
    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    .line 231
    .local v2, "toast":Landroid/widget/Toast;
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private openFragment(Landroid/support/v4/app/Fragment;)Z
    .locals 2
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 210
    invoke-virtual {p0}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 211
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f090085

    .line 212
    invoke-virtual {v0, v1, p1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 213
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 214
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 216
    const/4 v0, 0x1

    return v0
.end method

.method private updateAccountSection()V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 196
    invoke-virtual {p0}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    const-wide/16 v6, 0x2

    invoke-static {v5, v6, v7}, Lru/cn/api/authorization/AccountStorage;->getAccount(Landroid/content/Context;J)Lru/cn/api/authorization/Account;

    move-result-object v0

    .line 197
    .local v0, "currentAccount":Lru/cn/api/authorization/Account;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lru/cn/api/authorization/Account;->getLogin()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    move v1, v3

    .line 198
    .local v1, "personalAccount":Z
    :goto_0
    iget-object v6, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->signInPreference:Landroid/support/v7/preference/Preference;

    if-nez v1, :cond_2

    move v5, v3

    :goto_1
    invoke-virtual {v6, v5}, Landroid/support/v7/preference/Preference;->setVisible(Z)V

    .line 199
    iget-object v5, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->profilePreference:Landroid/support/v7/preference/Preference;

    invoke-virtual {v5, v1}, Landroid/support/v7/preference/Preference;->setVisible(Z)V

    .line 200
    iget-object v5, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->logOutPreference:Landroid/support/v7/preference/Preference;

    invoke-virtual {v5, v1}, Landroid/support/v7/preference/Preference;->setVisible(Z)V

    .line 201
    iget-object v5, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->linkDevicePreference:Landroid/support/v7/preference/Preference;

    invoke-virtual {v5, v1}, Landroid/support/v7/preference/Preference;->setVisible(Z)V

    .line 203
    if-eqz v1, :cond_0

    .line 204
    const v5, 0x7f0e0152

    invoke-virtual {p0, v5}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lru/cn/api/authorization/Account;->getLogin()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v4

    invoke-static {v5, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 205
    .local v2, "profileTitle":Ljava/lang/String;
    iget-object v3, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->profilePreference:Landroid/support/v7/preference/Preference;

    invoke-virtual {v3, v2}, Landroid/support/v7/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 207
    .end local v2    # "profileTitle":Ljava/lang/String;
    :cond_0
    return-void

    .end local v1    # "personalAccount":Z
    :cond_1
    move v1, v4

    .line 197
    goto :goto_0

    .restart local v1    # "personalAccount":Z
    :cond_2
    move v5, v4

    .line 198
    goto :goto_1
.end method


# virtual methods
.method final synthetic lambda$onCreate$0$MainPreferenceFragment(Landroid/support/v7/preference/Preference;)Z
    .locals 1
    .param p1, "preference"    # Landroid/support/v7/preference/Preference;

    .prologue
    .line 87
    new-instance v0, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;

    invoke-direct {v0}, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;-><init>()V

    invoke-direct {p0, v0}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->openFragment(Landroid/support/v4/app/Fragment;)Z

    move-result v0

    return v0
.end method

.method final synthetic lambda$onCreate$1$MainPreferenceFragment(Landroid/support/v7/preference/Preference;)Z
    .locals 1
    .param p1, "preference"    # Landroid/support/v7/preference/Preference;

    .prologue
    .line 88
    new-instance v0, Lru/cn/tv/mobile/settings/LegalPreferenceFragment;

    invoke-direct {v0}, Lru/cn/tv/mobile/settings/LegalPreferenceFragment;-><init>()V

    invoke-direct {p0, v0}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->openFragment(Landroid/support/v4/app/Fragment;)Z

    move-result v0

    return v0
.end method

.method final synthetic lambda$onCreate$2$MainPreferenceFragment(Landroid/support/v7/preference/Preference;)Z
    .locals 1
    .param p1, "preference"    # Landroid/support/v7/preference/Preference;

    .prologue
    .line 90
    invoke-direct {p0}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->openFeedback()V

    .line 91
    const/4 v0, 0x1

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    const v2, 0x7f0e0034

    const/4 v1, -0x1

    .line 178
    const/16 v0, 0x65

    if-ne p1, v0, :cond_1

    .line 179
    if-ne p2, v1, :cond_0

    .line 180
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e0157

    .line 181
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 182
    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 183
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    const/16 v0, 0x66

    if-ne p1, v0, :cond_0

    .line 186
    if-ne p2, v1, :cond_0

    .line 187
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e014e

    .line 188
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 189
    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 190
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 38
    invoke-super {p0, p1}, Landroid/support/v7/preference/PreferenceFragmentCompat;->onCreate(Landroid/os/Bundle;)V

    .line 40
    const/high16 v5, 0x7f110000

    invoke-virtual {p0, v5}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->addPreferencesFromResource(I)V

    .line 42
    const-string v5, "sign_in"

    invoke-virtual {p0, v5}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v5

    iput-object v5, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->signInPreference:Landroid/support/v7/preference/Preference;

    .line 43
    const-string v5, "profile"

    invoke-virtual {p0, v5}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v5

    iput-object v5, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->profilePreference:Landroid/support/v7/preference/Preference;

    .line 44
    const-string v5, "log_out"

    invoke-virtual {p0, v5}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v5

    iput-object v5, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->logOutPreference:Landroid/support/v7/preference/Preference;

    .line 45
    const-string v5, "link_device"

    invoke-virtual {p0, v5}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v5

    iput-object v5, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->linkDevicePreference:Landroid/support/v7/preference/Preference;

    .line 47
    iget-object v5, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->signInPreference:Landroid/support/v7/preference/Preference;

    new-instance v6, Lru/cn/tv/mobile/settings/MainPreferenceFragment$1;

    invoke-direct {v6, p0}, Lru/cn/tv/mobile/settings/MainPreferenceFragment$1;-><init>(Lru/cn/tv/mobile/settings/MainPreferenceFragment;)V

    invoke-virtual {v5, v6}, Landroid/support/v7/preference/Preference;->setOnPreferenceClickListener(Landroid/support/v7/preference/Preference$OnPreferenceClickListener;)V

    .line 57
    iget-object v5, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->logOutPreference:Landroid/support/v7/preference/Preference;

    new-instance v6, Lru/cn/tv/mobile/settings/MainPreferenceFragment$2;

    invoke-direct {v6, p0}, Lru/cn/tv/mobile/settings/MainPreferenceFragment$2;-><init>(Lru/cn/tv/mobile/settings/MainPreferenceFragment;)V

    invoke-virtual {v5, v6}, Landroid/support/v7/preference/Preference;->setOnPreferenceClickListener(Landroid/support/v7/preference/Preference$OnPreferenceClickListener;)V

    .line 67
    iget-object v5, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->linkDevicePreference:Landroid/support/v7/preference/Preference;

    new-instance v6, Lru/cn/tv/mobile/settings/MainPreferenceFragment$3;

    invoke-direct {v6, p0}, Lru/cn/tv/mobile/settings/MainPreferenceFragment$3;-><init>(Lru/cn/tv/mobile/settings/MainPreferenceFragment;)V

    invoke-virtual {v5, v6}, Landroid/support/v7/preference/Preference;->setOnPreferenceClickListener(Landroid/support/v7/preference/Preference$OnPreferenceClickListener;)V

    .line 77
    const-string v5, "udp_proxy"

    invoke-virtual {p0, v5}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v5

    new-instance v6, Lru/cn/tv/mobile/settings/MainPreferenceFragment$4;

    invoke-direct {v6, p0}, Lru/cn/tv/mobile/settings/MainPreferenceFragment$4;-><init>(Lru/cn/tv/mobile/settings/MainPreferenceFragment;)V

    invoke-virtual {v5, v6}, Landroid/support/v7/preference/Preference;->setOnPreferenceClickListener(Landroid/support/v7/preference/Preference$OnPreferenceClickListener;)V

    .line 87
    const-string v5, "accounts_inner"

    invoke-virtual {p0, v5}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v5

    new-instance v6, Lru/cn/tv/mobile/settings/MainPreferenceFragment$$Lambda$0;

    invoke-direct {v6, p0}, Lru/cn/tv/mobile/settings/MainPreferenceFragment$$Lambda$0;-><init>(Lru/cn/tv/mobile/settings/MainPreferenceFragment;)V

    invoke-virtual {v5, v6}, Landroid/support/v7/preference/Preference;->setOnPreferenceClickListener(Landroid/support/v7/preference/Preference$OnPreferenceClickListener;)V

    .line 88
    const-string v5, "legal_inner"

    invoke-virtual {p0, v5}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v5

    new-instance v6, Lru/cn/tv/mobile/settings/MainPreferenceFragment$$Lambda$1;

    invoke-direct {v6, p0}, Lru/cn/tv/mobile/settings/MainPreferenceFragment$$Lambda$1;-><init>(Lru/cn/tv/mobile/settings/MainPreferenceFragment;)V

    invoke-virtual {v5, v6}, Landroid/support/v7/preference/Preference;->setOnPreferenceClickListener(Landroid/support/v7/preference/Preference$OnPreferenceClickListener;)V

    .line 89
    const-string v5, "feedback"

    invoke-virtual {p0, v5}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v5

    new-instance v6, Lru/cn/tv/mobile/settings/MainPreferenceFragment$$Lambda$2;

    invoke-direct {v6, p0}, Lru/cn/tv/mobile/settings/MainPreferenceFragment$$Lambda$2;-><init>(Lru/cn/tv/mobile/settings/MainPreferenceFragment;)V

    invoke-virtual {v5, v6}, Landroid/support/v7/preference/Preference;->setOnPreferenceClickListener(Landroid/support/v7/preference/Preference$OnPreferenceClickListener;)V

    .line 95
    const-string v5, "advanced_playback"

    invoke-virtual {p0, v5}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/support/v7/preference/ListPreference;

    .line 96
    .local v0, "advancedPlayback":Landroid/support/v7/preference/ListPreference;
    const-string v5, "caching_level"

    invoke-virtual {p0, v5}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/support/v7/preference/ListPreference;

    .line 97
    .local v1, "cachingLevel":Landroid/support/v7/preference/ListPreference;
    const-string v5, "quality_level"

    invoke-virtual {p0, v5}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/support/v7/preference/ListPreference;

    .line 100
    .local v4, "qualityLevel":Landroid/support/v7/preference/ListPreference;
    invoke-virtual {p0}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-static {v5}, Lru/cn/player/exoplayer/ExoPlayerUtils;->isAvailable(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 101
    invoke-virtual {v0, v3}, Landroid/support/v7/preference/ListPreference;->setVisible(Z)V

    .line 102
    invoke-virtual {v1, v3}, Landroid/support/v7/preference/ListPreference;->setVisible(Z)V

    .line 103
    invoke-virtual {v4, v3}, Landroid/support/v7/preference/ListPreference;->setVisible(Z)V

    .line 162
    :goto_0
    return-void

    .line 106
    :cond_0
    invoke-virtual {p0}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lru/cn/player/exoplayer/ExoPlayerUtils;->isUsable(Landroid/content/Context;)Z

    move-result v2

    .line 108
    .local v2, "playbackEnabled":Z
    if-eqz v2, :cond_1

    const/4 v3, 0x1

    .line 109
    .local v3, "playerIndex":I
    :cond_1
    invoke-virtual {v0, v3}, Landroid/support/v7/preference/ListPreference;->setValueIndex(I)V

    .line 110
    invoke-virtual {v0}, Landroid/support/v7/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/support/v7/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 112
    invoke-virtual {p0}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "caching_level"

    invoke-static {v5, v6}, Lru/cn/domain/Preferences;->getInt(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/support/v7/preference/ListPreference;->setValueIndex(I)V

    .line 113
    invoke-virtual {v1}, Landroid/support/v7/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/support/v7/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 114
    invoke-virtual {v1, v2}, Landroid/support/v7/preference/ListPreference;->setEnabled(Z)V

    .line 116
    invoke-virtual {p0}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "quality_level"

    invoke-static {v5, v6}, Lru/cn/domain/Preferences;->getInt(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/support/v7/preference/ListPreference;->setValueIndex(I)V

    .line 117
    invoke-virtual {v4}, Landroid/support/v7/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v7/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 118
    invoke-virtual {v4, v2}, Landroid/support/v7/preference/ListPreference;->setEnabled(Z)V

    .line 120
    new-instance v5, Lru/cn/tv/mobile/settings/MainPreferenceFragment$5;

    invoke-direct {v5, p0, v0, v1, v4}, Lru/cn/tv/mobile/settings/MainPreferenceFragment$5;-><init>(Lru/cn/tv/mobile/settings/MainPreferenceFragment;Landroid/support/v7/preference/ListPreference;Landroid/support/v7/preference/ListPreference;Landroid/support/v7/preference/ListPreference;)V

    invoke-virtual {v0, v5}, Landroid/support/v7/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/support/v7/preference/Preference$OnPreferenceChangeListener;)V

    .line 136
    new-instance v5, Lru/cn/tv/mobile/settings/MainPreferenceFragment$6;

    invoke-direct {v5, p0, v1}, Lru/cn/tv/mobile/settings/MainPreferenceFragment$6;-><init>(Lru/cn/tv/mobile/settings/MainPreferenceFragment;Landroid/support/v7/preference/ListPreference;)V

    invoke-virtual {v1, v5}, Landroid/support/v7/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/support/v7/preference/Preference$OnPreferenceChangeListener;)V

    .line 149
    new-instance v5, Lru/cn/tv/mobile/settings/MainPreferenceFragment$7;

    invoke-direct {v5, p0, v4}, Lru/cn/tv/mobile/settings/MainPreferenceFragment$7;-><init>(Lru/cn/tv/mobile/settings/MainPreferenceFragment;Landroid/support/v7/preference/ListPreference;)V

    invoke-virtual {v4, v5}, Landroid/support/v7/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/support/v7/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_0
.end method

.method public onCreatePreferences(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "s"    # Ljava/lang/String;

    .prologue
    .line 174
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 166
    invoke-super {p0}, Landroid/support/v7/preference/PreferenceFragmentCompat;->onResume()V

    .line 168
    invoke-direct {p0}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->updateAccountSection()V

    .line 169
    return-void
.end method
