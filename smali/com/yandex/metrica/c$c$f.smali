.class public final Lcom/yandex/metrica/c$c$f;
.super Lcom/yandex/metrica/impl/ob/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/c$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "f"
.end annotation


# instance fields
.field public b:J

.field public c:I

.field public d:J

.field public e:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/d;-><init>()V

    .line 61
    invoke-virtual {p0}, Lcom/yandex/metrica/c$c$f;->d()Lcom/yandex/metrica/c$c$f;

    .line 62
    return-void
.end method


# virtual methods
.method public a(Lcom/yandex/metrica/impl/ob/b;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/yandex/metrica/c$c$f;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/yandex/metrica/impl/ob/b;->a(IJ)V

    .line 77
    const/4 v0, 0x2

    iget v1, p0, Lcom/yandex/metrica/c$c$f;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->c(II)V

    .line 78
    iget-wide v0, p0, Lcom/yandex/metrica/c$c$f;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 79
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/yandex/metrica/c$c$f;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/yandex/metrica/impl/ob/b;->b(IJ)V

    .line 81
    :cond_0
    iget-boolean v0, p0, Lcom/yandex/metrica/c$c$f;->e:Z

    if-eqz v0, :cond_1

    .line 82
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/yandex/metrica/c$c$f;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(IZ)V

    .line 84
    :cond_1
    invoke-super {p0, p1}, Lcom/yandex/metrica/impl/ob/d;->a(Lcom/yandex/metrica/impl/ob/b;)V

    .line 85
    return-void
.end method

.method protected c()I
    .locals 6

    .prologue
    .line 89
    invoke-super {p0}, Lcom/yandex/metrica/impl/ob/d;->c()I

    move-result v0

    .line 90
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/yandex/metrica/c$c$f;->b:J

    .line 91
    invoke-static {v1, v2, v3}, Lcom/yandex/metrica/impl/ob/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 92
    const/4 v1, 0x2

    iget v2, p0, Lcom/yandex/metrica/c$c$f;->c:I

    .line 93
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 94
    iget-wide v2, p0, Lcom/yandex/metrica/c$c$f;->d:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 95
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/yandex/metrica/c$c$f;->d:J

    .line 96
    invoke-static {v1, v2, v3}, Lcom/yandex/metrica/impl/ob/b;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 98
    :cond_0
    iget-boolean v1, p0, Lcom/yandex/metrica/c$c$f;->e:Z

    if-eqz v1, :cond_1

    .line 99
    const/4 v1, 0x4

    .line 100
    invoke-static {v1}, Lcom/yandex/metrica/impl/ob/b;->e(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    :cond_1
    return v0
.end method

.method public d()Lcom/yandex/metrica/c$c$f;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 65
    iput-wide v2, p0, Lcom/yandex/metrica/c$c$f;->b:J

    .line 66
    iput v0, p0, Lcom/yandex/metrica/c$c$f;->c:I

    .line 67
    iput-wide v2, p0, Lcom/yandex/metrica/c$c$f;->d:J

    .line 68
    iput-boolean v0, p0, Lcom/yandex/metrica/c$c$f;->e:Z

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Lcom/yandex/metrica/c$c$f;->a:I

    .line 70
    return-object p0
.end method
