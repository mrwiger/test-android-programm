.class public Lcom/google/obf/hl;
.super Ljava/lang/Object;
.source "IMASDK"


# instance fields
.field private final a:J

.field private final b:Lcom/google/obf/hi$a;


# direct methods
.method constructor <init>(JLcom/google/obf/hi$a;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-wide p1, p0, Lcom/google/obf/hl;->a:J

    .line 3
    iput-object p3, p0, Lcom/google/obf/hl;->b:Lcom/google/obf/hi$a;

    .line 4
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 5
    iget-wide v0, p0, Lcom/google/obf/hl;->a:J

    return-wide v0
.end method

.method public b()Lcom/google/obf/hi$a;
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lcom/google/obf/hl;->b:Lcom/google/obf/hi$a;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 9
    if-ne p0, p1, :cond_1

    .line 20
    :cond_0
    :goto_0
    return v0

    .line 11
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 12
    goto :goto_0

    .line 13
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 14
    goto :goto_0

    .line 15
    :cond_3
    check-cast p1, Lcom/google/obf/hl;

    .line 16
    iget-wide v2, p0, Lcom/google/obf/hl;->a:J

    iget-wide v4, p1, Lcom/google/obf/hl;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    .line 17
    goto :goto_0

    .line 18
    :cond_4
    iget-object v2, p0, Lcom/google/obf/hl;->b:Lcom/google/obf/hi$a;

    iget-object v3, p1, Lcom/google/obf/hl;->b:Lcom/google/obf/hi$a;

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 19
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 7
    .line 8
    iget-wide v0, p0, Lcom/google/obf/hl;->a:J

    long-to-int v0, v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/obf/hl;->b:Lcom/google/obf/hi$a;

    invoke-virtual {v1}, Lcom/google/obf/hi$a;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/google/obf/hl;->a:J

    iget-object v2, p0, Lcom/google/obf/hl;->b:Lcom/google/obf/hi$a;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x44

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "NativeBridgeConfig [adTimeUpdateMs="

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", adUiStyle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
