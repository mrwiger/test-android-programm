.class public Lcom/google/obf/hs;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/ads/interactivemedia/v3/api/player/VideoStreamPlayer$VideoStreamPlayerCallback;
.implements Lcom/google/obf/hn$b;
.implements Lcom/google/obf/ht;


# instance fields
.field private a:Lcom/google/ads/interactivemedia/v3/api/player/VideoStreamPlayer;

.field private b:Lcom/google/obf/hj;

.field private final c:Lcom/google/obf/hq;

.field private d:Lcom/google/obf/hc;

.field private e:Lcom/google/obf/gs;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/obf/hl;Lcom/google/obf/hj;Lcom/google/obf/hq;Lcom/google/ads/interactivemedia/v3/api/StreamDisplayContainer;Ljava/lang/String;Landroid/content/Context;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/ads/interactivemedia/v3/api/AdError;
        }
    .end annotation

    .prologue
    .line 1
    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v9, p7

    invoke-direct/range {v0 .. v9}, Lcom/google/obf/hs;-><init>(Ljava/lang/String;Lcom/google/obf/hl;Lcom/google/obf/hj;Lcom/google/obf/hq;Lcom/google/ads/interactivemedia/v3/api/StreamDisplayContainer;Ljava/lang/String;Lcom/google/obf/hc;Lcom/google/obf/gs;Landroid/content/Context;)V

    .line 2
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/obf/hl;Lcom/google/obf/hj;Lcom/google/obf/hq;Lcom/google/ads/interactivemedia/v3/api/StreamDisplayContainer;Ljava/lang/String;Lcom/google/obf/hc;Lcom/google/obf/gs;Landroid/content/Context;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/ads/interactivemedia/v3/api/AdError;
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    invoke-interface {p5}, Lcom/google/ads/interactivemedia/v3/api/StreamDisplayContainer;->getVideoStreamPlayer()Lcom/google/ads/interactivemedia/v3/api/player/VideoStreamPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/hs;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoStreamPlayer;

    .line 5
    iget-object v0, p0, Lcom/google/obf/hs;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoStreamPlayer;

    if-nez v0, :cond_0

    .line 6
    new-instance v0, Lcom/google/ads/interactivemedia/v3/api/AdError;

    sget-object v1, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;->LOAD:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;

    sget-object v2, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;->INVALID_ARGUMENTS:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;

    const-string v3, "Server-side ad insertion player was not provided."

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/v3/api/AdError;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V

    throw v0

    .line 7
    :cond_0
    iput-object p4, p0, Lcom/google/obf/hs;->c:Lcom/google/obf/hq;

    .line 8
    iput-object p1, p0, Lcom/google/obf/hs;->f:Ljava/lang/String;

    .line 9
    iput-object p3, p0, Lcom/google/obf/hs;->b:Lcom/google/obf/hj;

    .line 10
    iput-object p6, p0, Lcom/google/obf/hs;->g:Ljava/lang/String;

    .line 11
    iput-object p7, p0, Lcom/google/obf/hs;->d:Lcom/google/obf/hc;

    .line 12
    iget-object v0, p0, Lcom/google/obf/hs;->d:Lcom/google/obf/hc;

    if-nez v0, :cond_1

    .line 13
    new-instance v0, Lcom/google/obf/hc;

    iget-object v1, p0, Lcom/google/obf/hs;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoStreamPlayer;

    .line 14
    invoke-virtual {p2}, Lcom/google/obf/hl;->a()J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Lcom/google/obf/hc;-><init>(Lcom/google/ads/interactivemedia/v3/api/player/ContentProgressProvider;J)V

    iput-object v0, p0, Lcom/google/obf/hs;->d:Lcom/google/obf/hc;

    .line 15
    :cond_1
    iput-object p8, p0, Lcom/google/obf/hs;->e:Lcom/google/obf/gs;

    .line 16
    iget-object v0, p0, Lcom/google/obf/hs;->e:Lcom/google/obf/gs;

    if-nez v0, :cond_2

    .line 17
    :try_start_0
    new-instance v0, Lcom/google/obf/gs;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move-object v5, p9

    invoke-direct/range {v0 .. v5}, Lcom/google/obf/gs;-><init>(Ljava/lang/String;Lcom/google/obf/hl;Lcom/google/obf/hj;Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/obf/hs;->e:Lcom/google/obf/gs;
    :try_end_0
    .catch Lcom/google/ads/interactivemedia/v3/api/AdError; {:try_start_0 .. :try_end_0} :catch_0

    .line 22
    :cond_2
    :goto_0
    return-void

    .line 19
    :catch_0
    move-exception v0

    .line 20
    const-string v1, "IMASDK"

    const-string v2, "Error creating ad UI: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private a(Lcom/google/obf/hi$c;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/obf/hs;->b:Lcom/google/obf/hj;

    new-instance v1, Lcom/google/obf/hi;

    sget-object v2, Lcom/google/obf/hi$b;->videoDisplay:Lcom/google/obf/hi$b;

    iget-object v3, p0, Lcom/google/obf/hs;->f:Ljava/lang/String;

    invoke-direct {v1, v2, p1, v3, p2}, Lcom/google/obf/hi;-><init>(Lcom/google/obf/hi$b;Lcom/google/obf/hi$c;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/google/obf/hj;->b(Lcom/google/obf/hi;)V

    .line 69
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 70
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/obf/hs;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/hs;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 93
    :cond_0
    :goto_0
    return-object p1

    .line 72
    :cond_1
    iget-object v0, p0, Lcom/google/obf/hs;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\s+"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 73
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x3f

    if-ne v1, v2, :cond_2

    .line 74
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 75
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 77
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 78
    invoke-static {v1}, Lcom/google/obf/id;->a(Landroid/net/Uri;)Ljava/util/Map;

    move-result-object v2

    .line 79
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 80
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    .line 81
    invoke-virtual {v4}, Landroid/net/Uri$Builder;->clearQuery()Landroid/net/Uri$Builder;

    .line 82
    const-string v1, "http://www.dom.com/path?"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 83
    invoke-static {v0}, Lcom/google/obf/id;->a(Landroid/net/Uri;)Ljava/util/Map;

    move-result-object v5

    .line 84
    invoke-interface {v3, v5}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 85
    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 86
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 87
    invoke-interface {v5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 88
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 82
    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 90
    :cond_5
    invoke-static {v3}, Lcom/google/obf/id;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 91
    invoke-virtual {v4, v0}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 92
    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/obf/hs;->e:Lcom/google/obf/gs;

    invoke-virtual {v0}, Lcom/google/obf/gs;->a()V

    .line 41
    return-void
.end method

.method public a(Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;)V
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/google/obf/hi$c;->timeupdate:Lcom/google/obf/hi$c;

    invoke-direct {p0, v0, p1}, Lcom/google/obf/hs;->a(Lcom/google/obf/hi$c;Ljava/lang/Object;)V

    .line 58
    return-void
.end method

.method public a(Lcom/google/ads/interactivemedia/v3/impl/data/b;)V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/obf/hs;->e:Lcom/google/obf/gs;

    invoke-virtual {v0, p1}, Lcom/google/obf/gs;->a(Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    .line 39
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/obf/hs;->d:Lcom/google/obf/hc;

    iget-object v1, p0, Lcom/google/obf/hs;->e:Lcom/google/obf/gs;

    invoke-virtual {v0, v1}, Lcom/google/obf/hc;->a(Lcom/google/obf/hn$b;)V

    .line 24
    iget-object v0, p0, Lcom/google/obf/hs;->d:Lcom/google/obf/hc;

    invoke-virtual {v0, p0}, Lcom/google/obf/hc;->a(Lcom/google/obf/hn$b;)V

    .line 25
    return-void
.end method

.method public a(Lcom/google/obf/hi$c;Lcom/google/ads/interactivemedia/v3/impl/data/l;)Z
    .locals 6

    .prologue
    .line 27
    sget-object v0, Lcom/google/obf/hs$1;->a:[I

    invoke-virtual {p1}, Lcom/google/obf/hi$c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 35
    const/4 v0, 0x0

    .line 36
    :goto_0
    return v0

    .line 28
    :pswitch_0
    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/google/ads/interactivemedia/v3/impl/data/l;->streamUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 29
    iget-object v0, p0, Lcom/google/obf/hs;->d:Lcom/google/obf/hc;

    invoke-virtual {v0}, Lcom/google/obf/hc;->b()V

    .line 30
    iget-object v0, p2, Lcom/google/ads/interactivemedia/v3/impl/data/l;->streamUrl:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/obf/hs;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 31
    iget-object v1, p0, Lcom/google/obf/hs;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoStreamPlayer;

    iget-object v2, p2, Lcom/google/ads/interactivemedia/v3/impl/data/l;->subtitles:Ljava/util/List;

    invoke-interface {v1, v0, v2}, Lcom/google/ads/interactivemedia/v3/api/player/VideoStreamPlayer;->loadUrl(Ljava/lang/String;Ljava/util/List;)V

    .line 36
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/google/obf/hs;->c:Lcom/google/obf/hq;

    new-instance v1, Lcom/google/obf/gk;

    new-instance v2, Lcom/google/ads/interactivemedia/v3/api/AdError;

    sget-object v3, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;->LOAD:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;

    sget-object v4, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;->INTERNAL_ERROR:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;

    const-string v5, "Load message must contain video url."

    invoke-direct {v2, v3, v4, v5}, Lcom/google/ads/interactivemedia/v3/api/AdError;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/google/obf/gk;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError;)V

    invoke-virtual {v0, v1}, Lcom/google/obf/hq;->a(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent;)V

    goto :goto_1

    .line 27
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public b()V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/obf/hs;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoStreamPlayer;

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/player/VideoStreamPlayer;->onAdBreakStarted()V

    .line 60
    return-void
.end method

.method public b(Lcom/google/obf/hi$c;Lcom/google/ads/interactivemedia/v3/impl/data/l;)Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/obf/hs;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoStreamPlayer;

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/player/VideoStreamPlayer;->onAdBreakEnded()V

    .line 62
    return-void
.end method

.method public d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 43
    const-string v0, "SDK_DEBUG"

    const-string v1, "Destroying StreamVideoDisplay"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    invoke-virtual {p0}, Lcom/google/obf/hs;->g()V

    .line 45
    iput-object v2, p0, Lcom/google/obf/hs;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoStreamPlayer;

    .line 46
    iput-object v2, p0, Lcom/google/obf/hs;->b:Lcom/google/obf/hj;

    .line 47
    iget-object v0, p0, Lcom/google/obf/hs;->d:Lcom/google/obf/hc;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/google/obf/hs;->d:Lcom/google/obf/hc;

    invoke-virtual {v0}, Lcom/google/obf/hc;->c()V

    .line 49
    iget-object v0, p0, Lcom/google/obf/hs;->d:Lcom/google/obf/hc;

    iget-object v1, p0, Lcom/google/obf/hs;->e:Lcom/google/obf/gs;

    invoke-virtual {v0, v1}, Lcom/google/obf/hc;->b(Lcom/google/obf/hn$b;)V

    .line 50
    iget-object v0, p0, Lcom/google/obf/hs;->d:Lcom/google/obf/hc;

    invoke-virtual {v0, p0}, Lcom/google/obf/hc;->b(Lcom/google/obf/hn$b;)V

    .line 51
    :cond_0
    iput-object v2, p0, Lcom/google/obf/hs;->d:Lcom/google/obf/hc;

    .line 52
    iget-object v0, p0, Lcom/google/obf/hs;->e:Lcom/google/obf/gs;

    invoke-virtual {v0}, Lcom/google/obf/gs;->b()V

    .line 53
    iput-object v2, p0, Lcom/google/obf/hs;->e:Lcom/google/obf/gs;

    .line 54
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x1

    return v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/obf/hs;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoStreamPlayer;

    invoke-interface {v0, p0}, Lcom/google/ads/interactivemedia/v3/api/player/VideoStreamPlayer;->addCallback(Lcom/google/ads/interactivemedia/v3/api/player/VideoStreamPlayer$VideoStreamPlayerCallback;)V

    .line 65
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/obf/hs;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoStreamPlayer;

    invoke-interface {v0, p0}, Lcom/google/ads/interactivemedia/v3/api/player/VideoStreamPlayer;->removeCallback(Lcom/google/ads/interactivemedia/v3/api/player/VideoStreamPlayer$VideoStreamPlayerCallback;)V

    .line 67
    return-void
.end method

.method public getAdProgress()Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/obf/hs;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoStreamPlayer;

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/player/VideoStreamPlayer;->getContentProgress()Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;

    move-result-object v0

    return-object v0
.end method

.method public onAdError(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent;)V
    .locals 0

    .prologue
    .line 42
    return-void
.end method
