.class public Lru/cn/domain/UDPProxySettings;
.super Ljava/lang/Object;
.source "UDPProxySettings.java"


# instance fields
.field private address:Ljava/lang/String;

.field private enabled:Z

.field private port:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const-string v0, "udp_proxy_address"

    invoke-static {p1, v0}, Lru/cn/domain/Preferences;->getString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/domain/UDPProxySettings;->address:Ljava/lang/String;

    .line 14
    const-string v0, "udp_proxy_port"

    invoke-static {p1, v0}, Lru/cn/domain/Preferences;->getInt(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/domain/UDPProxySettings;->port:I

    .line 15
    const-string v0, "udp_proxy_enabled"

    invoke-static {p1, v0}, Lru/cn/domain/Preferences;->getBoolean(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lru/cn/domain/UDPProxySettings;->enabled:Z

    .line 16
    return-void
.end method


# virtual methods
.method public address(Ljava/lang/String;)Lru/cn/domain/UDPProxySettings;
    .locals 0
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 36
    iput-object p1, p0, Lru/cn/domain/UDPProxySettings;->address:Ljava/lang/String;

    .line 37
    return-object p0
.end method

.method public enabled(Z)Lru/cn/domain/UDPProxySettings;
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 31
    iput-boolean p1, p0, Lru/cn/domain/UDPProxySettings;->enabled:Z

    .line 32
    return-object p0
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lru/cn/domain/UDPProxySettings;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getPort()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lru/cn/domain/UDPProxySettings;->port:I

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lru/cn/domain/UDPProxySettings;->enabled:Z

    return v0
.end method

.method public port(I)Lru/cn/domain/UDPProxySettings;
    .locals 0
    .param p1, "port"    # I

    .prologue
    .line 41
    iput p1, p0, Lru/cn/domain/UDPProxySettings;->port:I

    .line 42
    return-object p0
.end method

.method public save(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    iget-object v0, p0, Lru/cn/domain/UDPProxySettings;->address:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 47
    const-string v0, "udp_proxy_address"

    iget-object v1, p0, Lru/cn/domain/UDPProxySettings;->address:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lru/cn/domain/Preferences;->setString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    :cond_0
    iget v0, p0, Lru/cn/domain/UDPProxySettings;->port:I

    if-lez v0, :cond_1

    .line 51
    const-string v0, "udp_proxy_port"

    iget v1, p0, Lru/cn/domain/UDPProxySettings;->port:I

    invoke-static {p1, v0, v1}, Lru/cn/domain/Preferences;->setInt(Landroid/content/Context;Ljava/lang/String;I)V

    .line 54
    :cond_1
    const-string v0, "udp_proxy_enabled"

    iget-boolean v1, p0, Lru/cn/domain/UDPProxySettings;->enabled:Z

    invoke-static {p1, v0, v1}, Lru/cn/domain/Preferences;->setBoolean(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 55
    return-void
.end method
