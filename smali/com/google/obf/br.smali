.class final Lcom/google/obf/br;
.super Ljava/lang/Object;
.source "IMASDK"


# instance fields
.field private final a:Lcom/google/obf/bs$b;

.field private final b:Lcom/google/obf/dw;

.field private c:J

.field private d:J


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/google/obf/bs$b;

    invoke-direct {v0}, Lcom/google/obf/bs$b;-><init>()V

    iput-object v0, p0, Lcom/google/obf/br;->a:Lcom/google/obf/bs$b;

    .line 3
    new-instance v0, Lcom/google/obf/dw;

    const/16 v1, 0x11a

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/br;->b:Lcom/google/obf/dw;

    .line 4
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/obf/br;->c:J

    return-void
.end method


# virtual methods
.method public a(JLcom/google/obf/ak;)J
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-wide/16 v8, 0x0

    .line 9
    iget-wide v6, p0, Lcom/google/obf/br;->c:J

    cmp-long v0, v6, v4

    if-eqz v0, :cond_2

    iget-wide v6, p0, Lcom/google/obf/br;->d:J

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 10
    iget-object v0, p0, Lcom/google/obf/br;->a:Lcom/google/obf/bs$b;

    iget-object v3, p0, Lcom/google/obf/br;->b:Lcom/google/obf/dw;

    invoke-static {p3, v0, v3, v2}, Lcom/google/obf/bs;->a(Lcom/google/obf/ak;Lcom/google/obf/bs$b;Lcom/google/obf/dw;Z)Z

    .line 11
    iget-object v0, p0, Lcom/google/obf/br;->a:Lcom/google/obf/bs$b;

    iget-wide v2, v0, Lcom/google/obf/bs$b;->c:J

    sub-long v2, p1, v2

    .line 12
    cmp-long v0, v2, v8

    if-lez v0, :cond_0

    const-wide/32 v6, 0x11940

    cmp-long v0, v2, v6

    if-lez v0, :cond_3

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/google/obf/br;->a:Lcom/google/obf/bs$b;

    iget v0, v0, Lcom/google/obf/bs$b;->i:I

    iget-object v4, p0, Lcom/google/obf/br;->a:Lcom/google/obf/bs$b;

    iget v4, v4, Lcom/google/obf/bs$b;->h:I

    add-int/2addr v0, v4

    .line 14
    cmp-long v4, v2, v8

    if-gtz v4, :cond_1

    const/4 v1, 0x2

    :cond_1
    mul-int/2addr v0, v1

    int-to-long v0, v0

    .line 15
    invoke-interface {p3}, Lcom/google/obf/ak;->c()J

    move-result-wide v4

    sub-long v0, v4, v0

    iget-wide v4, p0, Lcom/google/obf/br;->c:J

    mul-long/2addr v2, v4

    iget-wide v4, p0, Lcom/google/obf/br;->d:J

    div-long/2addr v2, v4

    add-long/2addr v0, v2

    .line 17
    :goto_1
    return-wide v0

    :cond_2
    move v0, v2

    .line 9
    goto :goto_0

    .line 16
    :cond_3
    invoke-interface {p3}, Lcom/google/obf/ak;->a()V

    move-wide v0, v4

    .line 17
    goto :goto_1
.end method

.method public a(JJ)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 5
    cmp-long v0, p1, v2

    if-lez v0, :cond_0

    cmp-long v0, p3, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->a(Z)V

    .line 6
    iput-wide p1, p0, Lcom/google/obf/br;->c:J

    .line 7
    iput-wide p3, p0, Lcom/google/obf/br;->d:J

    .line 8
    return-void

    .line 5
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
