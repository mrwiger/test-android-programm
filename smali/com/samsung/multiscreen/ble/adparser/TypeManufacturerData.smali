.class public Lcom/samsung/multiscreen/ble/adparser/TypeManufacturerData;
.super Lcom/samsung/multiscreen/ble/adparser/AdElement;
.source "TypeManufacturerData.java"


# instance fields
.field b:[B

.field manufacturer:I


# direct methods
.method public constructor <init>([BII)V
    .locals 5
    .param p1, "data"    # [B
    .param p2, "pos"    # I
    .param p3, "len"    # I

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/samsung/multiscreen/ble/adparser/AdElement;-><init>()V

    .line 27
    move v1, p2

    .line 28
    .local v1, "ptr":I
    aget-byte v3, p1, v1

    and-int/lit16 v2, v3, 0xff

    .line 29
    .local v2, "v":I
    add-int/lit8 v1, v1, 0x1

    .line 30
    aget-byte v3, p1, v1

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    .line 31
    add-int/lit8 v1, v1, 0x1

    .line 32
    iput v2, p0, Lcom/samsung/multiscreen/ble/adparser/TypeManufacturerData;->manufacturer:I

    .line 33
    add-int/lit8 v0, p3, -0x2

    .line 34
    .local v0, "blen":I
    new-array v3, v0, [B

    iput-object v3, p0, Lcom/samsung/multiscreen/ble/adparser/TypeManufacturerData;->b:[B

    .line 35
    iget-object v3, p0, Lcom/samsung/multiscreen/ble/adparser/TypeManufacturerData;->b:[B

    const/4 v4, 0x0

    invoke-static {p1, v1, v3, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 36
    return-void
.end method


# virtual methods
.method public getBytes()[B
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/multiscreen/ble/adparser/TypeManufacturerData;->b:[B

    return-object v0
.end method

.method public getManufacturer()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/samsung/multiscreen/ble/adparser/TypeManufacturerData;->manufacturer:I

    invoke-static {v1}, Lcom/samsung/multiscreen/ble/adparser/TypeManufacturerData;->hex16(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 52
    .local v0, "sb":Ljava/lang/StringBuffer;
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/StringBuffer;)V

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 40
    new-instance v1, Ljava/lang/StringBuffer;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Manufacturer data (manufacturer: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/multiscreen/ble/adparser/TypeManufacturerData;->manufacturer:I

    invoke-static {v4}, Lcom/samsung/multiscreen/ble/adparser/TypeManufacturerData;->hex16(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 41
    .local v1, "sb":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/multiscreen/ble/adparser/TypeManufacturerData;->b:[B

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 42
    if-lez v0, :cond_0

    .line 43
    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 44
    :cond_0
    iget-object v3, p0, Lcom/samsung/multiscreen/ble/adparser/TypeManufacturerData;->b:[B

    aget-byte v3, v3, v0

    and-int/lit16 v2, v3, 0xff

    .line 45
    .local v2, "v":I
    invoke-static {v2}, Lcom/samsung/multiscreen/ble/adparser/TypeManufacturerData;->hex8(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 41
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 47
    .end local v2    # "v":I
    :cond_1
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>(Ljava/lang/StringBuffer;)V

    return-object v3
.end method
