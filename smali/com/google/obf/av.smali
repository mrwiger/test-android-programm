.class abstract Lcom/google/obf/av;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/av$a;
    }
.end annotation


# instance fields
.field protected final a:Lcom/google/obf/ar;

.field private b:J


# direct methods
.method protected constructor <init>(Lcom/google/obf/ar;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/obf/av;->a:Lcom/google/obf/ar;

    .line 3
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/obf/av;->b:J

    .line 4
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 7
    iget-wide v0, p0, Lcom/google/obf/av;->b:J

    return-wide v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 5
    iput-wide p1, p0, Lcom/google/obf/av;->b:J

    .line 6
    return-void
.end method

.method protected abstract a(Lcom/google/obf/dw;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation
.end method

.method protected abstract a(Lcom/google/obf/dw;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation
.end method

.method public final b(Lcom/google/obf/dw;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/obf/av;->a(Lcom/google/obf/dw;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/obf/av;->a(Lcom/google/obf/dw;J)V

    .line 10
    :cond_0
    return-void
.end method
