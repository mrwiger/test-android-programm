.class public Lru/cn/tv/mobile/channels/ChannelsFragment;
.super Lru/cn/view/CustomListFragment;
.source "ChannelsFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/mobile/channels/ChannelsFragment$ChannelsFragmentListener;
    }
.end annotation


# instance fields
.field private adView:Lcom/google/android/gms/ads/AdView;

.field private adViewWrapper:Landroid/widget/FrameLayout;

.field private adapter:Lru/cn/tv/mobile/channels/ChannelsAdapter;

.field private channelFragmentListener:Lru/cn/tv/mobile/channels/ChannelsFragment$ChannelsFragmentListener;

.field private channelTitlesMap:Landroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private checkCurrentChannel:Z

.field private currentChannelCnId:J

.field private emptyFavouriteText:Landroid/widget/TextView;

.field private listErrorLayout:Landroid/view/View;

.field private progressBar:Landroid/widget/ProgressBar;

.field private viewMode:I

.field private viewModel:Lru/cn/tv/mobile/channels/ChannelsViewModel;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 28
    invoke-direct {p0}, Lru/cn/view/CustomListFragment;-><init>()V

    .line 40
    iput v2, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->viewMode:I

    .line 45
    new-instance v0, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v0}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->channelTitlesMap:Landroid/support/v4/util/LongSparseArray;

    .line 47
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->currentChannelCnId:J

    .line 49
    iput-boolean v2, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->checkCurrentChannel:Z

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->channelFragmentListener:Lru/cn/tv/mobile/channels/ChannelsFragment$ChannelsFragmentListener;

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/mobile/channels/ChannelsFragment;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/channels/ChannelsFragment;

    .prologue
    .line 28
    iget-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->progressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/tv/mobile/channels/ChannelsFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/channels/ChannelsFragment;

    .prologue
    .line 28
    iget-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->listErrorLayout:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/tv/mobile/channels/ChannelsFragment;)I
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/channels/ChannelsFragment;

    .prologue
    .line 28
    iget v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->viewMode:I

    return v0
.end method

.method static synthetic access$300(Lru/cn/tv/mobile/channels/ChannelsFragment;)Lru/cn/tv/mobile/channels/ChannelsViewModel;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/channels/ChannelsFragment;

    .prologue
    .line 28
    iget-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->viewModel:Lru/cn/tv/mobile/channels/ChannelsViewModel;

    return-object v0
.end method

.method static synthetic access$400(Lru/cn/tv/mobile/channels/ChannelsFragment;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/channels/ChannelsFragment;

    .prologue
    .line 28
    iget-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adViewWrapper:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$500(Lru/cn/tv/mobile/channels/ChannelsFragment;)Lcom/google/android/gms/ads/AdView;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/channels/ChannelsFragment;

    .prologue
    .line 28
    iget-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    return-object v0
.end method

.method private setChannels(Landroid/database/Cursor;)V
    .locals 12
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v1, 0x1

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 247
    iget-object v5, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v5, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 249
    iget-object v8, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->listErrorLayout:Landroid/view/View;

    if-nez p1, :cond_0

    move v5, v6

    :goto_0
    invoke-virtual {v8, v5}, Landroid/view/View;->setVisibility(I)V

    .line 250
    if-eqz p1, :cond_5

    .line 251
    check-cast p1, Landroid/database/CursorWrapper;

    .end local p1    # "cursor":Landroid/database/Cursor;
    invoke-virtual {p1}, Landroid/database/CursorWrapper;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/cursor/ChannelCursor;

    .line 253
    .local v0, "c":Lru/cn/api/provider/cursor/ChannelCursor;
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->moveToFirst()Z

    .line 254
    :goto_1
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_1

    .line 255
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getChannelId()J

    move-result-wide v2

    .line 257
    .local v2, "channelId":J
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getTitle()Ljava/lang/String;

    move-result-object v4

    .line 258
    .local v4, "title":Ljava/lang/String;
    iget-object v5, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->channelTitlesMap:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v5, v2, v3, v4}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 259
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->moveToNext()Z

    goto :goto_1

    .end local v0    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    .end local v2    # "channelId":J
    .end local v4    # "title":Ljava/lang/String;
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :cond_0
    move v5, v7

    .line 249
    goto :goto_0

    .line 262
    .end local p1    # "cursor":Landroid/database/Cursor;
    .restart local v0    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    :cond_1
    iget-object v5, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adapter:Lru/cn/tv/mobile/channels/ChannelsAdapter;

    invoke-virtual {v5, v0}, Lru/cn/tv/mobile/channels/ChannelsAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 263
    iget-wide v8, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->currentChannelCnId:J

    const-wide/16 v10, 0x0

    cmp-long v5, v8, v10

    if-lez v5, :cond_2

    .line 264
    iget-wide v8, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->currentChannelCnId:J

    invoke-virtual {p0, v8, v9}, Lru/cn/tv/mobile/channels/ChannelsFragment;->setCurrentChannel(J)V

    .line 267
    :cond_2
    iget v5, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->viewMode:I

    if-ne v5, v1, :cond_3

    iget-object v5, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adapter:Lru/cn/tv/mobile/channels/ChannelsAdapter;

    invoke-virtual {v5}, Lru/cn/tv/mobile/channels/ChannelsAdapter;->getCount()I

    move-result v5

    if-nez v5, :cond_3

    .line 268
    .local v1, "isEmptyFavorite":Z
    :goto_2
    iget-object v5, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->emptyFavouriteText:Landroid/widget/TextView;

    if-eqz v1, :cond_4

    :goto_3
    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 272
    .end local v0    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    .end local v1    # "isEmptyFavorite":Z
    :goto_4
    return-void

    .restart local v0    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    :cond_3
    move v1, v6

    .line 267
    goto :goto_2

    .restart local v1    # "isEmptyFavorite":Z
    :cond_4
    move v6, v7

    .line 268
    goto :goto_3

    .line 270
    .end local v0    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    .end local v1    # "isEmptyFavorite":Z
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :cond_5
    iget-object v5, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adapter:Lru/cn/tv/mobile/channels/ChannelsAdapter;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lru/cn/tv/mobile/channels/ChannelsAdapter;->changeCursor(Landroid/database/Cursor;)V

    goto :goto_4
.end method

.method private showAdvBanner()V
    .locals 3

    .prologue
    .line 219
    iget-object v1, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    if-eqz v1, :cond_1

    .line 244
    :cond_0
    :goto_0
    return-void

    .line 223
    :cond_1
    invoke-static {}, Lru/cn/utils/Utils;->shouldAvoidWebView()Z

    move-result v1

    if-nez v1, :cond_0

    .line 226
    sget-object v0, Lcom/google/android/gms/ads/AdSize;->BANNER:Lcom/google/android/gms/ads/AdSize;

    .line 227
    .local v0, "adSize":Lcom/google/android/gms/ads/AdSize;
    invoke-virtual {p0}, Lru/cn/tv/mobile/channels/ChannelsFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lru/cn/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 228
    sget-object v0, Lcom/google/android/gms/ads/AdSize;->FULL_BANNER:Lcom/google/android/gms/ads/AdSize;

    .line 231
    :cond_2
    new-instance v1, Lcom/google/android/gms/ads/AdView;

    invoke-virtual {p0}, Lru/cn/tv/mobile/channels/ChannelsFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/ads/AdView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    .line 232
    iget-object v1, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/ads/AdView;->setAdSize(Lcom/google/android/gms/ads/AdSize;)V

    .line 233
    iget-object v1, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    new-instance v2, Lru/cn/tv/mobile/channels/ChannelsFragment$2;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/channels/ChannelsFragment$2;-><init>(Lru/cn/tv/mobile/channels/ChannelsFragment;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/ads/AdView;->setAdListener(Lcom/google/android/gms/ads/AdListener;)V

    .line 243
    invoke-virtual {p0}, Lru/cn/tv/mobile/channels/ChannelsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    invoke-static {v1, v2}, Lru/cn/ad/AdMobManager;->loadBanner(Landroid/content/Context;Lcom/google/android/gms/ads/AdView;)V

    goto :goto_0
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$ChannelsFragment(Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/mobile/channels/ChannelsFragment;->setChannels(Landroid/database/Cursor;)V

    return-void
.end method

.method public checkCurrentChannel(Z)V
    .locals 8
    .param p1, "check"    # Z

    .prologue
    .line 193
    iput-boolean p1, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->checkCurrentChannel:Z

    .line 194
    iget-object v3, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adapter:Lru/cn/tv/mobile/channels/ChannelsAdapter;

    invoke-virtual {v3}, Lru/cn/tv/mobile/channels/ChannelsAdapter;->getCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 195
    invoke-virtual {p0}, Lru/cn/tv/mobile/channels/ChannelsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v1

    .line 196
    .local v1, "headerViewsCount":I
    invoke-virtual {p0}, Lru/cn/tv/mobile/channels/ChannelsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/cursor/ChannelCursor;

    .line 197
    .local v0, "c":Lru/cn/api/provider/cursor/ChannelCursor;
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->moveToFirst()Z

    .line 198
    :goto_0
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_0

    .line 199
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getPosition()I

    move-result v3

    add-int v2, v3, v1

    .line 200
    .local v2, "position":I
    iget-wide v4, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->currentChannelCnId:J

    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getChannelId()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_1

    .line 201
    invoke-virtual {p0}, Lru/cn/tv/mobile/channels/ChannelsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v3

    iget-boolean v4, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->checkCurrentChannel:Z

    invoke-virtual {v3, v2, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 207
    .end local v0    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    .end local v1    # "headerViewsCount":I
    .end local v2    # "position":I
    :cond_0
    return-void

    .line 204
    .restart local v0    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    .restart local v1    # "headerViewsCount":I
    .restart local v2    # "position":I
    :cond_1
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->moveToNext()Z

    goto :goto_0
.end method

.method public getChannelTitle(J)Ljava/lang/String;
    .locals 1
    .param p1, "cnId"    # J

    .prologue
    .line 210
    iget-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->channelTitlesMap:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentChannel()J
    .locals 2

    .prologue
    .line 189
    iget-wide v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->currentChannelCnId:J

    return-wide v0
.end method

.method protected getItemHeight()I
    .locals 1

    .prologue
    .line 215
    const/16 v0, 0x40

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 62
    invoke-super {p0, p1}, Lru/cn/view/CustomListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 63
    new-instance v0, Lru/cn/tv/mobile/channels/ChannelsAdapter;

    invoke-virtual {p0}, Lru/cn/tv/mobile/channels/ChannelsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lru/cn/tv/mobile/channels/ChannelsAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adapter:Lru/cn/tv/mobile/channels/ChannelsAdapter;

    .line 65
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v0

    const-class v1, Lru/cn/tv/mobile/channels/ChannelsViewModel;

    invoke-static {p0, v0, v1}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lru/cn/tv/mobile/channels/ChannelsViewModel;

    iput-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->viewModel:Lru/cn/tv/mobile/channels/ChannelsViewModel;

    .line 67
    iget-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->viewModel:Lru/cn/tv/mobile/channels/ChannelsViewModel;

    invoke-virtual {v0}, Lru/cn/tv/mobile/channels/ChannelsViewModel;->channels()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/mobile/channels/ChannelsFragment$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/channels/ChannelsFragment$$Lambda$0;-><init>(Lru/cn/tv/mobile/channels/ChannelsFragment;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 68
    iget-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->viewModel:Lru/cn/tv/mobile/channels/ChannelsViewModel;

    iget v1, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->viewMode:I

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/channels/ChannelsViewModel;->setMode(I)V

    .line 69
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x0

    .line 86
    invoke-virtual {p0}, Lru/cn/tv/mobile/channels/ChannelsFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lru/cn/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    const v0, 0x7f0c001d

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adViewWrapper:Landroid/widget/FrameLayout;

    .line 92
    :goto_0
    const v0, 0x7f0c0098

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 89
    :cond_0
    const v0, 0x7f0c001c

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adViewWrapper:Landroid/widget/FrameLayout;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 145
    invoke-super {p0}, Lru/cn/view/CustomListFragment;->onDestroy()V

    .line 147
    iget-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->destroy()V

    .line 150
    :cond_0
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 73
    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/api/provider/cursor/ChannelCursor;

    .line 74
    .local v2, "cursor":Lru/cn/api/provider/cursor/ChannelCursor;
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->getChannelId()J

    move-result-wide v0

    .line 75
    .local v0, "cnId":J
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->getIsDenied()Z

    move-result v3

    .line 77
    .local v3, "isDenied":Z
    invoke-virtual {p0}, Lru/cn/tv/mobile/channels/ChannelsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, p3, v5}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 79
    iget-object v4, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->channelFragmentListener:Lru/cn/tv/mobile/channels/ChannelsFragment$ChannelsFragmentListener;

    if-eqz v4, :cond_0

    .line 80
    iget-object v4, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->channelFragmentListener:Lru/cn/tv/mobile/channels/ChannelsFragment$ChannelsFragmentListener;

    invoke-interface {v4, v0, v1, v3}, Lru/cn/tv/mobile/channels/ChannelsFragment$ChannelsFragmentListener;->onChannelSelected(JZ)V

    .line 82
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->pause()V

    .line 130
    :cond_0
    invoke-super {p0}, Lru/cn/view/CustomListFragment;->onPause()V

    .line 131
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 135
    invoke-super {p0}, Lru/cn/view/CustomListFragment;->onResume()V

    .line 136
    iget-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->resume()V

    .line 141
    :goto_0
    return-void

    .line 139
    :cond_0
    invoke-direct {p0}, Lru/cn/tv/mobile/channels/ChannelsFragment;->showAdvBanner()V

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 97
    invoke-super {p0, p1, p2}, Lru/cn/view/CustomListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 99
    const v2, 0x7f09017e

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 100
    .local v1, "repeatButton":Landroid/view/View;
    new-instance v2, Lru/cn/tv/mobile/channels/ChannelsFragment$1;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/channels/ChannelsFragment$1;-><init>(Lru/cn/tv/mobile/channels/ChannelsFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    const v2, 0x7f090106

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->listErrorLayout:Landroid/view/View;

    .line 111
    const v2, 0x7f090175

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 112
    const v2, 0x7f0900b2

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->emptyFavouriteText:Landroid/widget/TextView;

    .line 114
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lru/cn/tv/mobile/channels/ChannelsFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 116
    invoke-virtual {p0}, Lru/cn/tv/mobile/channels/ChannelsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v2

    if-nez v2, :cond_0

    .line 117
    invoke-virtual {p0}, Lru/cn/tv/mobile/channels/ChannelsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v2

    iget-object v4, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adViewWrapper:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v4}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 120
    :cond_0
    iget-object v2, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adapter:Lru/cn/tv/mobile/channels/ChannelsAdapter;

    invoke-virtual {p0, v2}, Lru/cn/tv/mobile/channels/ChannelsFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 121
    iget-object v2, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->listErrorLayout:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->isShown()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adapter:Lru/cn/tv/mobile/channels/ChannelsAdapter;

    invoke-virtual {v2}, Lru/cn/tv/mobile/channels/ChannelsAdapter;->getCount()I

    move-result v2

    if-nez v2, :cond_1

    const/4 v0, 0x1

    .line 122
    .local v0, "isLoading":Z
    :goto_0
    iget-object v2, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->progressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_2

    :goto_1
    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 123
    return-void

    .end local v0    # "isLoading":Z
    :cond_1
    move v0, v3

    .line 121
    goto :goto_0

    .line 122
    .restart local v0    # "isLoading":Z
    :cond_2
    const/16 v3, 0x8

    goto :goto_1
.end method

.method public setCurrentChannel(J)V
    .locals 7
    .param p1, "cnId"    # J

    .prologue
    .line 172
    iput-wide p1, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->currentChannelCnId:J

    .line 173
    iget-object v2, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adapter:Lru/cn/tv/mobile/channels/ChannelsAdapter;

    invoke-virtual {v2}, Lru/cn/tv/mobile/channels/ChannelsAdapter;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 174
    iget-object v2, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adapter:Lru/cn/tv/mobile/channels/ChannelsAdapter;

    invoke-virtual {v2}, Lru/cn/tv/mobile/channels/ChannelsAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/cursor/ChannelCursor;

    .line 175
    .local v0, "c":Lru/cn/api/provider/cursor/ChannelCursor;
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->moveToFirst()Z

    .line 176
    :goto_0
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_0

    .line 177
    iget-wide v2, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->currentChannelCnId:J

    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getChannelId()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 178
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getPosition()I

    move-result v1

    .line 179
    .local v1, "position":I
    invoke-virtual {p0}, Lru/cn/tv/mobile/channels/ChannelsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v2

    iget-boolean v3, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->checkCurrentChannel:Z

    invoke-virtual {v2, v1, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 186
    .end local v0    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    .end local v1    # "position":I
    :cond_0
    return-void

    .line 183
    .restart local v0    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    :cond_1
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->moveToNext()Z

    goto :goto_0
.end method

.method public setListener(Lru/cn/tv/mobile/channels/ChannelsFragment$ChannelsFragmentListener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/tv/mobile/channels/ChannelsFragment$ChannelsFragmentListener;

    .prologue
    .line 168
    iput-object p1, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->channelFragmentListener:Lru/cn/tv/mobile/channels/ChannelsFragment$ChannelsFragmentListener;

    .line 169
    return-void
.end method

.method public setViewMode(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 153
    iget v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->viewMode:I

    if-ne p1, v0, :cond_1

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 157
    :cond_1
    iput p1, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->viewMode:I

    .line 158
    invoke-virtual {p0}, Lru/cn/tv/mobile/channels/ChannelsFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->listErrorLayout:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 160
    iget-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->adapter:Lru/cn/tv/mobile/channels/ChannelsAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/channels/ChannelsAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 162
    iget-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->progressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 163
    iget-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsFragment;->viewModel:Lru/cn/tv/mobile/channels/ChannelsViewModel;

    invoke-virtual {v0, p1}, Lru/cn/tv/mobile/channels/ChannelsViewModel;->setMode(I)V

    goto :goto_0
.end method
