.class public Lru/cn/tv/player/controller/TouchNextProgramView;
.super Landroid/widget/FrameLayout;
.source "TouchNextProgramView.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/player/controller/TouchNextProgramView$EventListener;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private eventListener:Lru/cn/tv/player/controller/TouchNextProgramView$EventListener;

.field private handler:Landroid/os/Handler;

.field private image:Landroid/widget/ImageView;

.field private playNextRecommendation:Landroid/widget/ImageButton;

.field private progressBar:Landroid/widget/ProgressBar;

.field private telecastTitle:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lru/cn/tv/player/controller/TouchNextProgramView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lru/cn/tv/player/controller/TouchNextProgramView;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lru/cn/tv/player/controller/TouchNextProgramView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 56
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f0c00ab

    invoke-virtual {v1, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 58
    const v2, 0x7f0900f3

    invoke-virtual {p0, v2}, Lru/cn/tv/player/controller/TouchNextProgramView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lru/cn/tv/player/controller/TouchNextProgramView;->image:Landroid/widget/ImageView;

    .line 59
    const v2, 0x7f0901d7

    invoke-virtual {p0, v2}, Lru/cn/tv/player/controller/TouchNextProgramView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lru/cn/tv/player/controller/TouchNextProgramView;->telecastTitle:Landroid/widget/TextView;

    .line 60
    const v2, 0x7f090077

    invoke-virtual {p0, v2}, Lru/cn/tv/player/controller/TouchNextProgramView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 61
    .local v0, "closeText":Landroid/widget/TextView;
    const v2, 0x7f090072

    invoke-virtual {p0, v2}, Lru/cn/tv/player/controller/TouchNextProgramView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lru/cn/tv/player/controller/TouchNextProgramView;->progressBar:Landroid/widget/ProgressBar;

    .line 62
    const v2, 0x7f090164

    invoke-virtual {p0, v2}, Lru/cn/tv/player/controller/TouchNextProgramView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lru/cn/tv/player/controller/TouchNextProgramView;->playNextRecommendation:Landroid/widget/ImageButton;

    .line 64
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v2, p0, Lru/cn/tv/player/controller/TouchNextProgramView;->handler:Landroid/os/Handler;

    .line 66
    new-instance v2, Lru/cn/tv/player/controller/TouchNextProgramView$$Lambda$0;

    invoke-direct {v2, p0}, Lru/cn/tv/player/controller/TouchNextProgramView$$Lambda$0;-><init>(Lru/cn/tv/player/controller/TouchNextProgramView;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    return-void
.end method

.method private hide()V
    .locals 1

    .prologue
    .line 163
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/TouchNextProgramView;->setVisibility(I)V

    .line 164
    return-void
.end method

.method private showImage(Lru/cn/api/tv/replies/Telecast;)V
    .locals 4
    .param p1, "telecast"    # Lru/cn/api/tv/replies/Telecast;

    .prologue
    .line 153
    sget-object v2, Lru/cn/api/tv/replies/TelecastImage$Profile;->MAIN:Lru/cn/api/tv/replies/TelecastImage$Profile;

    invoke-virtual {p1, v2}, Lru/cn/api/tv/replies/Telecast;->getImage(Lru/cn/api/tv/replies/TelecastImage$Profile;)Lru/cn/api/tv/replies/TelecastImage;

    move-result-object v1

    .line 154
    .local v1, "telecastImage":Lru/cn/api/tv/replies/TelecastImage;
    if-eqz v1, :cond_0

    iget-object v0, v1, Lru/cn/api/tv/replies/TelecastImage;->location:Ljava/lang/String;

    .line 155
    .local v0, "imageUri":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Lru/cn/tv/player/controller/TouchNextProgramView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v2

    .line 156
    invoke-virtual {v2, v0}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    .line 157
    invoke-virtual {v2}, Lcom/squareup/picasso/RequestCreator;->fit()Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    const v3, 0x7f0802dc

    .line 158
    invoke-virtual {v2, v3}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    iget-object v3, p0, Lru/cn/tv/player/controller/TouchNextProgramView;->image:Landroid/widget/ImageView;

    .line 159
    invoke-virtual {v2, v3}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 160
    return-void

    .line 154
    .end local v0    # "imageUri":Ljava/lang/String;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private startNextRecommendationAnimation(J)V
    .locals 5
    .param p1, "time"    # J

    .prologue
    .line 146
    iget-object v1, p0, Lru/cn/tv/player/controller/TouchNextProgramView;->progressBar:Landroid/widget/ProgressBar;

    const-string v2, "progress"

    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 147
    .local v0, "animator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, p1, p2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 148
    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 149
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 150
    return-void

    .line 146
    nop

    :array_0
    .array-data 4
        0x0
        0x64
    .end array-data
.end method

.method private startTimer(JJ)V
    .locals 5
    .param p1, "time"    # J
    .param p3, "telecastId"    # J

    .prologue
    .line 116
    iget-object v2, p0, Lru/cn/tv/player/controller/TouchNextProgramView;->handler:Landroid/os/Handler;

    const/16 v3, 0x2a

    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 117
    .local v1, "message":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 118
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "telecastId"

    invoke-virtual {v0, v2, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 119
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 120
    iget-object v2, p0, Lru/cn/tv/player/controller/TouchNextProgramView;->handler:Landroid/os/Handler;

    invoke-virtual {v2, v1, p1, p2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 121
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v6, -0x1

    const/4 v1, 0x1

    .line 125
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 141
    const/4 v1, 0x0

    :cond_0
    :goto_0
    return v1

    .line 127
    :pswitch_0
    iget-object v4, p0, Lru/cn/tv/player/controller/TouchNextProgramView;->eventListener:Lru/cn/tv/player/controller/TouchNextProgramView$EventListener;

    if-eqz v4, :cond_0

    .line 131
    invoke-virtual {p1}, Landroid/os/Message;->peekData()Landroid/os/Bundle;

    move-result-object v0

    .line 132
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "telecastId"

    invoke-virtual {v0, v4, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 133
    .local v2, "telecastId":J
    cmp-long v4, v2, v6

    if-eqz v4, :cond_0

    .line 134
    iget-object v4, p0, Lru/cn/tv/player/controller/TouchNextProgramView;->eventListener:Lru/cn/tv/player/controller/TouchNextProgramView$EventListener;

    invoke-interface {v4, v2, v3, v1}, Lru/cn/tv/player/controller/TouchNextProgramView$EventListener;->onSelectMedia(JZ)V

    .line 136
    const-string v4, "watch_auto"

    invoke-static {v4}, Lru/cn/domain/statistics/AnalyticsManager;->recommend_next(Ljava/lang/String;)V

    goto :goto_0

    .line 125
    nop

    :pswitch_data_0
    .packed-switch 0x2a
        :pswitch_0
    .end packed-switch
.end method

.method final synthetic lambda$new$0$TouchNextProgramView(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 67
    sget-object v0, Lru/cn/tv/player/controller/TouchNextProgramView;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onClick: closeButton"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    const-string v0, "close"

    invoke-static {v0}, Lru/cn/domain/statistics/AnalyticsManager;->recommend_next(Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchNextProgramView;->handler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchNextProgramView;->handler:Landroid/os/Handler;

    const/16 v1, 0x2a

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 74
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchNextProgramView;->eventListener:Lru/cn/tv/player/controller/TouchNextProgramView$EventListener;

    if-eqz v0, :cond_1

    .line 75
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchNextProgramView;->eventListener:Lru/cn/tv/player/controller/TouchNextProgramView$EventListener;

    invoke-interface {v0}, Lru/cn/tv/player/controller/TouchNextProgramView$EventListener;->onClose()V

    .line 78
    :cond_1
    invoke-direct {p0}, Lru/cn/tv/player/controller/TouchNextProgramView;->hide()V

    .line 79
    return-void
.end method

.method final synthetic lambda$show$1$TouchNextProgramView(Lru/cn/api/tv/replies/Telecast;Landroid/view/View;)V
    .locals 4
    .param p1, "telecast"    # Lru/cn/api/tv/replies/Telecast;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 96
    sget-object v0, Lru/cn/tv/player/controller/TouchNextProgramView;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onClick: watch"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    const-string v0, "watch"

    invoke-static {v0}, Lru/cn/domain/statistics/AnalyticsManager;->recommend_next(Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchNextProgramView;->handler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchNextProgramView;->handler:Landroid/os/Handler;

    const/16 v1, 0x2a

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 103
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchNextProgramView;->eventListener:Lru/cn/tv/player/controller/TouchNextProgramView$EventListener;

    if-eqz v0, :cond_1

    .line 104
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchNextProgramView;->eventListener:Lru/cn/tv/player/controller/TouchNextProgramView$EventListener;

    iget-wide v2, p1, Lru/cn/api/tv/replies/Telecast;->id:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, Lru/cn/tv/player/controller/TouchNextProgramView$EventListener;->onSelectMedia(JZ)V

    .line 107
    :cond_1
    invoke-direct {p0}, Lru/cn/tv/player/controller/TouchNextProgramView;->hide()V

    .line 108
    return-void
.end method

.method public setListener(Lru/cn/tv/player/controller/TouchNextProgramView$EventListener;)V
    .locals 0
    .param p1, "eventListener"    # Lru/cn/tv/player/controller/TouchNextProgramView$EventListener;

    .prologue
    .line 83
    iput-object p1, p0, Lru/cn/tv/player/controller/TouchNextProgramView;->eventListener:Lru/cn/tv/player/controller/TouchNextProgramView$EventListener;

    .line 84
    return-void
.end method

.method public show(Lru/cn/api/tv/replies/Telecast;)V
    .locals 4
    .param p1, "telecast"    # Lru/cn/api/tv/replies/Telecast;

    .prologue
    const-wide/16 v2, 0x1f40

    .line 87
    if-nez p1, :cond_0

    .line 113
    :goto_0
    return-void

    .line 91
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchNextProgramView;->telecastTitle:Landroid/widget/TextView;

    iget-object v1, p1, Lru/cn/api/tv/replies/Telecast;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    invoke-direct {p0, p1}, Lru/cn/tv/player/controller/TouchNextProgramView;->showImage(Lru/cn/api/tv/replies/Telecast;)V

    .line 95
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchNextProgramView;->playNextRecommendation:Landroid/widget/ImageButton;

    new-instance v1, Lru/cn/tv/player/controller/TouchNextProgramView$$Lambda$1;

    invoke-direct {v1, p0, p1}, Lru/cn/tv/player/controller/TouchNextProgramView$$Lambda$1;-><init>(Lru/cn/tv/player/controller/TouchNextProgramView;Lru/cn/api/tv/replies/Telecast;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    iget-wide v0, p1, Lru/cn/api/tv/replies/Telecast;->id:J

    invoke-direct {p0, v2, v3, v0, v1}, Lru/cn/tv/player/controller/TouchNextProgramView;->startTimer(JJ)V

    .line 111
    invoke-direct {p0, v2, v3}, Lru/cn/tv/player/controller/TouchNextProgramView;->startNextRecommendationAnimation(J)V

    .line 112
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/TouchNextProgramView;->setVisibility(I)V

    goto :goto_0
.end method
