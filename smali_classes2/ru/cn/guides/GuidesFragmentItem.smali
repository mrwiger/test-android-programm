.class public Lru/cn/guides/GuidesFragmentItem;
.super Landroid/support/v4/app/Fragment;
.source "GuidesFragmentItem.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static newInstance(III)Lru/cn/guides/GuidesFragmentItem;
    .locals 3
    .param p0, "titleId"    # I
    .param p1, "descriptionId"    # I
    .param p2, "imageId"    # I

    .prologue
    .line 15
    new-instance v1, Lru/cn/guides/GuidesFragmentItem;

    invoke-direct {v1}, Lru/cn/guides/GuidesFragmentItem;-><init>()V

    .line 17
    .local v1, "fragment":Lru/cn/guides/GuidesFragmentItem;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 18
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "titleId"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 19
    const-string v2, "descriptionId"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 20
    const-string v2, "imageId"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 22
    invoke-virtual {v1, v0}, Lru/cn/guides/GuidesFragmentItem;->setArguments(Landroid/os/Bundle;)V

    .line 23
    return-object v1
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    sget v0, Lru/cn/guides/R$layout;->guides_fragment_item:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 37
    sget v7, Lru/cn/guides/R$id;->title:I

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 38
    .local v6, "titleView":Landroid/widget/TextView;
    sget v7, Lru/cn/guides/R$id;->description:I

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 39
    .local v2, "descriptionView":Landroid/widget/TextView;
    sget v7, Lru/cn/guides/R$id;->image:I

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 41
    .local v4, "imageView":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lru/cn/guides/GuidesFragmentItem;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 42
    .local v0, "args":Landroid/os/Bundle;
    const-string v7, "titleId"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 43
    .local v5, "title":I
    const-string v7, "descriptionId"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 44
    .local v1, "description":I
    const-string v7, "imageId"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 46
    .local v3, "image":I
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(I)V

    .line 47
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    .line 48
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 49
    return-void
.end method
