.class public final Lcom/annimon/stream/ComparatorCompat;
.super Ljava/lang/Object;
.source "ComparatorCompat.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final NATURAL_ORDER:Lcom/annimon/stream/ComparatorCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/annimon/stream/ComparatorCompat",
            "<",
            "Ljava/lang/Comparable",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final REVERSE_ORDER:Lcom/annimon/stream/ComparatorCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/annimon/stream/ComparatorCompat",
            "<",
            "Ljava/lang/Comparable",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final comparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<-TT;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/annimon/stream/ComparatorCompat;

    new-instance v1, Lcom/annimon/stream/ComparatorCompat$1;

    invoke-direct {v1}, Lcom/annimon/stream/ComparatorCompat$1;-><init>()V

    invoke-direct {v0, v1}, Lcom/annimon/stream/ComparatorCompat;-><init>(Ljava/util/Comparator;)V

    sput-object v0, Lcom/annimon/stream/ComparatorCompat;->NATURAL_ORDER:Lcom/annimon/stream/ComparatorCompat;

    .line 27
    new-instance v0, Lcom/annimon/stream/ComparatorCompat;

    .line 28
    invoke-static {}, Ljava/util/Collections;->reverseOrder()Ljava/util/Comparator;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/annimon/stream/ComparatorCompat;-><init>(Ljava/util/Comparator;)V

    sput-object v0, Lcom/annimon/stream/ComparatorCompat;->REVERSE_ORDER:Lcom/annimon/stream/ComparatorCompat;

    .line 27
    return-void
.end method

.method public constructor <init>(Ljava/util/Comparator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 292
    .local p0, "this":Lcom/annimon/stream/ComparatorCompat;, "Lcom/annimon/stream/ComparatorCompat<TT;>;"
    .local p1, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 293
    iput-object p1, p0, Lcom/annimon/stream/ComparatorCompat;->comparator:Ljava/util/Comparator;

    .line 294
    return-void
.end method

.method static synthetic access$000(Lcom/annimon/stream/ComparatorCompat;)Ljava/util/Comparator;
    .locals 1
    .param p0, "x0"    # Lcom/annimon/stream/ComparatorCompat;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/annimon/stream/ComparatorCompat;->comparator:Ljava/util/Comparator;

    return-object v0
.end method

.method public static comparing(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/ComparatorCompat;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U::",
            "Ljava/lang/Comparable",
            "<-TU;>;>(",
            "Lcom/annimon/stream/function/Function",
            "<-TT;+TU;>;)",
            "Lcom/annimon/stream/ComparatorCompat",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 132
    .local p0, "keyExtractor":Lcom/annimon/stream/function/Function;, "Lcom/annimon/stream/function/Function<-TT;+TU;>;"
    invoke-static {p0}, Lcom/annimon/stream/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    new-instance v0, Lcom/annimon/stream/ComparatorCompat;

    new-instance v1, Lcom/annimon/stream/ComparatorCompat$4;

    invoke-direct {v1, p0}, Lcom/annimon/stream/ComparatorCompat$4;-><init>(Lcom/annimon/stream/function/Function;)V

    invoke-direct {v0, v1}, Lcom/annimon/stream/ComparatorCompat;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)I"
        }
    .end annotation

    .prologue
    .line 399
    .local p0, "this":Lcom/annimon/stream/ComparatorCompat;, "Lcom/annimon/stream/ComparatorCompat<TT;>;"
    .local p1, "o1":Ljava/lang/Object;, "TT;"
    .local p2, "o2":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lcom/annimon/stream/ComparatorCompat;->comparator:Ljava/util/Comparator;

    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public reversed()Lcom/annimon/stream/ComparatorCompat;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/annimon/stream/ComparatorCompat",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 303
    .local p0, "this":Lcom/annimon/stream/ComparatorCompat;, "Lcom/annimon/stream/ComparatorCompat<TT;>;"
    new-instance v0, Lcom/annimon/stream/ComparatorCompat;

    iget-object v1, p0, Lcom/annimon/stream/ComparatorCompat;->comparator:Ljava/util/Comparator;

    invoke-static {v1}, Ljava/util/Collections;->reverseOrder(Ljava/util/Comparator;)Ljava/util/Comparator;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/annimon/stream/ComparatorCompat;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method

.method public bridge synthetic reversed()Ljava/util/Comparator;
    .locals 1

    .prologue
    .line 15
    .local p0, "this":Lcom/annimon/stream/ComparatorCompat;, "Lcom/annimon/stream/ComparatorCompat<TT;>;"
    invoke-virtual {p0}, Lcom/annimon/stream/ComparatorCompat;->reversed()Lcom/annimon/stream/ComparatorCompat;

    move-result-object v0

    return-object v0
.end method

.method public thenComparing(Ljava/util/Comparator;)Lcom/annimon/stream/ComparatorCompat;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TT;>;)",
            "Lcom/annimon/stream/ComparatorCompat",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 314
    .local p0, "this":Lcom/annimon/stream/ComparatorCompat;, "Lcom/annimon/stream/ComparatorCompat<TT;>;"
    .local p1, "other":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    invoke-static {p1}, Lcom/annimon/stream/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 315
    new-instance v0, Lcom/annimon/stream/ComparatorCompat;

    new-instance v1, Lcom/annimon/stream/ComparatorCompat$9;

    invoke-direct {v1, p0, p1}, Lcom/annimon/stream/ComparatorCompat$9;-><init>(Lcom/annimon/stream/ComparatorCompat;Ljava/util/Comparator;)V

    invoke-direct {v0, v1}, Lcom/annimon/stream/ComparatorCompat;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method

.method public bridge synthetic thenComparing(Ljava/util/Comparator;)Ljava/util/Comparator;
    .locals 1

    .prologue
    .line 15
    .local p0, "this":Lcom/annimon/stream/ComparatorCompat;, "Lcom/annimon/stream/ComparatorCompat<TT;>;"
    invoke-virtual {p0, p1}, Lcom/annimon/stream/ComparatorCompat;->thenComparing(Ljava/util/Comparator;)Lcom/annimon/stream/ComparatorCompat;

    move-result-object v0

    return-object v0
.end method
