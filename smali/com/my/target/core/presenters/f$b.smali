.class final Lcom/my/target/core/presenters/f$b;
.super Ljava/lang/Object;
.source "InterstitialPromoPresenter.java"

# interfaces
.implements Lcom/my/target/ee$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/core/presenters/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic an:Lcom/my/target/core/presenters/f;


# direct methods
.method private constructor <init>(Lcom/my/target/core/presenters/f;)V
    .locals 0

    .prologue
    .line 367
    iput-object p1, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/my/target/core/presenters/f;B)V
    .locals 0

    .prologue
    .line 367
    invoke-direct {p0, p1}, Lcom/my/target/core/presenters/f$b;-><init>(Lcom/my/target/core/presenters/f;)V

    return-void
.end method


# virtual methods
.method public final A()V
    .locals 2

    .prologue
    .line 467
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->f(Lcom/my/target/core/presenters/f;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 469
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->k(Lcom/my/target/core/presenters/f;)V

    .line 470
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->j(Lcom/my/target/core/presenters/f;)Lcom/my/target/aj;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 472
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->j(Lcom/my/target/core/presenters/f;)Lcom/my/target/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    const-string v1, "volumeOff"

    invoke-virtual {v0, v1}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v1}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/ee;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 474
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/my/target/core/presenters/f;->a(Lcom/my/target/core/presenters/f;Z)Z

    .line 485
    :goto_0
    return-void

    .line 478
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->g(Lcom/my/target/core/presenters/f;)V

    .line 479
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->j(Lcom/my/target/core/presenters/f;)Lcom/my/target/aj;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 481
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->j(Lcom/my/target/core/presenters/f;)Lcom/my/target/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    const-string v1, "volumeOn"

    invoke-virtual {v0, v1}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v1}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/ee;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 483
    :cond_2
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/my/target/core/presenters/f;->a(Lcom/my/target/core/presenters/f;Z)Z

    goto :goto_0
.end method

.method public final D()V
    .locals 2

    .prologue
    .line 504
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->j(Lcom/my/target/core/presenters/f;)Lcom/my/target/aj;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 506
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->j(Lcom/my/target/core/presenters/f;)Lcom/my/target/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    const-string v1, "playbackResumed"

    invoke-virtual {v0, v1}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v1}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/ee;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 507
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/ee;->resume()V

    .line 508
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->f(Lcom/my/target/core/presenters/f;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 510
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->k(Lcom/my/target/core/presenters/f;)V

    .line 517
    :cond_0
    :goto_0
    return-void

    .line 514
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->g(Lcom/my/target/core/presenters/f;)V

    goto :goto_0
.end method

.method public final E()V
    .locals 2

    .prologue
    .line 522
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->a(Lcom/my/target/core/presenters/f;)Lcom/my/target/core/presenters/f$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 524
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->a(Lcom/my/target/core/presenters/f;)Lcom/my/target/core/presenters/f$a;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v1}, Lcom/my/target/core/presenters/f;->d(Lcom/my/target/core/presenters/f;)Lcom/my/target/core/models/banners/h;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/my/target/core/presenters/f$a;->a(Lcom/my/target/core/models/banners/h;)V

    .line 526
    :cond_0
    return-void
.end method

.method public final L(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 626
    const-string v0, "Video playing error: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 627
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->t(Lcom/my/target/core/presenters/f;)V

    .line 628
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/ee;->G()V

    .line 629
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v1}, Lcom/my/target/core/presenters/f;->d(Lcom/my/target/core/presenters/f;)Lcom/my/target/core/models/banners/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ee;->b(Lcom/my/target/core/models/banners/h;)V

    .line 630
    return-void
.end method

.method public final a(Lcom/my/target/core/models/banners/e;)V
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->a(Lcom/my/target/core/presenters/f;)Lcom/my/target/core/presenters/f$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->a(Lcom/my/target/core/presenters/f;)Lcom/my/target/core/presenters/f$a;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/my/target/core/presenters/f$a;->a(Lcom/my/target/core/models/banners/e;)V

    .line 377
    :cond_0
    return-void
.end method

.method public final b(FF)V
    .locals 3
    .param p1, "position"    # F

    .prologue
    const/4 v2, 0x0

    .line 573
    :goto_0
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/my/target/ee;->setTimeChanged(F)V

    .line 575
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->n(Lcom/my/target/core/presenters/f;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 577
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->o(Lcom/my/target/core/presenters/f;)V

    .line 578
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->j(Lcom/my/target/core/presenters/f;)Lcom/my/target/aj;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 580
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->j(Lcom/my/target/core/presenters/f;)Lcom/my/target/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    const-string v1, "playbackStarted"

    invoke-virtual {v0, v1}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v1}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/ee;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 583
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0, v2}, Lcom/my/target/core/presenters/f;->a(Lcom/my/target/core/presenters/f;F)V

    .line 584
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->p(Lcom/my/target/core/presenters/f;)Z

    .line 587
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->i(Lcom/my/target/core/presenters/f;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 589
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->q(Lcom/my/target/core/presenters/f;)Z

    .line 592
    :cond_2
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->l(Lcom/my/target/core/presenters/f;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->j(Lcom/my/target/core/presenters/f;)Lcom/my/target/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/aj;->isAutoPlay()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 594
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->j(Lcom/my/target/core/presenters/f;)Lcom/my/target/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/aj;->getAllowCloseDelay()F

    move-result v0

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_3

    .line 596
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/ee;->G()V

    .line 600
    :cond_3
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->r(Lcom/my/target/core/presenters/f;)F

    move-result v0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_7

    .line 602
    cmpl-float v0, p1, v2

    if-eqz v0, :cond_4

    .line 604
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0, p1}, Lcom/my/target/core/presenters/f;->a(Lcom/my/target/core/presenters/f;F)V

    .line 606
    :cond_4
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->r(Lcom/my/target/core/presenters/f;)F

    move-result v0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_6

    .line 608
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->s(Lcom/my/target/core/presenters/f;)Z

    .line 609
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->t(Lcom/my/target/core/presenters/f;)V

    .line 610
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->a(Lcom/my/target/core/presenters/f;)Lcom/my/target/core/presenters/f$a;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 612
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->a(Lcom/my/target/core/presenters/f;)Lcom/my/target/core/presenters/f$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/my/target/core/presenters/f$a;->h()V

    .line 614
    :cond_5
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/ee;->finish()V

    .line 621
    :cond_6
    return-void

    .line 619
    :cond_7
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->r(Lcom/my/target/core/presenters/f;)F

    move-result p1

    goto/16 :goto_0
.end method

.method public final b(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 382
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/e;

    .line 384
    iget-object v2, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v2}, Lcom/my/target/core/presenters/f;->b(Lcom/my/target/core/presenters/f;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 386
    iget-object v2, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v2}, Lcom/my/target/core/presenters/f;->b(Lcom/my/target/core/presenters/f;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 387
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/e;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    const-string v2, "playbackStarted"

    invoke-virtual {v0, v2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v2}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v2

    invoke-virtual {v2}, Lcom/my/target/ee;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    goto :goto_0

    .line 390
    :cond_1
    return-void
.end method

.method public final bq()V
    .locals 0

    .prologue
    .line 532
    return-void
.end method

.method public final br()V
    .locals 2

    .prologue
    .line 543
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->l(Lcom/my/target/core/presenters/f;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->j(Lcom/my/target/core/presenters/f;)Lcom/my/target/aj;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->j(Lcom/my/target/core/presenters/f;)Lcom/my/target/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/aj;->getAllowCloseDelay()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 545
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/ee;->G()V

    .line 547
    :cond_0
    return-void
.end method

.method public final bs()V
    .locals 0

    .prologue
    .line 553
    return-void
.end method

.method public final bt()V
    .locals 1

    .prologue
    .line 558
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->m(Lcom/my/target/core/presenters/f;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 560
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/ee;->pause()V

    .line 562
    :cond_0
    return-void
.end method

.method public final bu()V
    .locals 0

    .prologue
    .line 568
    return-void
.end method

.method public final bv()V
    .locals 1

    .prologue
    .line 635
    const-string v0, "Video playing complete:"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 636
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->t(Lcom/my/target/core/presenters/f;)V

    .line 637
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/ee;->G()V

    .line 638
    return-void
.end method

.method public final d(Z)V
    .locals 2

    .prologue
    .line 395
    if-nez p1, :cond_0

    .line 397
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v1}, Lcom/my/target/core/presenters/f;->d(Lcom/my/target/core/presenters/f;)Lcom/my/target/core/models/banners/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ee;->b(Lcom/my/target/core/models/banners/h;)V

    .line 398
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->e(Lcom/my/target/core/presenters/f;)Lcom/my/target/aj;

    .line 400
    :cond_0
    return-void
.end method

.method public final e(F)V
    .locals 2
    .param p1, "volume"    # F

    .prologue
    .line 537
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v1

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/my/target/ee;->g(Z)V

    .line 538
    return-void

    .line 537
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onAudioFocusChange(I)V
    .locals 1
    .param p1, "focusChange"    # I

    .prologue
    .line 404
    packed-switch p1, :pswitch_data_0

    .line 430
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 408
    :pswitch_1
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-virtual {v0}, Lcom/my/target/core/presenters/f;->pause()V

    .line 409
    const-string v0, "Audiofocus loss, pausing"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 414
    :pswitch_2
    const-string v0, "Audiofocus gain, unmuting"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 415
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->f(Lcom/my/target/core/presenters/f;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 417
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->g(Lcom/my/target/core/presenters/f;)V

    goto :goto_0

    .line 421
    :pswitch_3
    const-string v0, "Audiofocus loss can duck, set volume to 0.3"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 422
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->f(Lcom/my/target/core/presenters/f;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 424
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->h(Lcom/my/target/core/presenters/f;)V

    goto :goto_0

    .line 404
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final onPauseClicked()V
    .locals 2

    .prologue
    .line 490
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->j(Lcom/my/target/core/presenters/f;)Lcom/my/target/aj;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 492
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    iget-object v1, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v1}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/ee;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/my/target/core/presenters/f;->a(Lcom/my/target/core/presenters/f;Landroid/content/Context;)V

    .line 493
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->j(Lcom/my/target/core/presenters/f;)Lcom/my/target/aj;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 495
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->j(Lcom/my/target/core/presenters/f;)Lcom/my/target/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    const-string v1, "playbackPaused"

    invoke-virtual {v0, v1}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v1}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/ee;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 497
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/ee;->pause()V

    .line 499
    :cond_1
    return-void
.end method

.method public final onPlayClicked()V
    .locals 2

    .prologue
    .line 454
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->j(Lcom/my/target/core/presenters/f;)Lcom/my/target/aj;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 456
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->f(Lcom/my/target/core/presenters/f;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 458
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    iget-object v1, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v1}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/ee;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/my/target/core/presenters/f;->b(Lcom/my/target/core/presenters/f;Landroid/content/Context;)V

    .line 460
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/ee;->play()V

    .line 462
    :cond_1
    return-void
.end method

.method public final z()V
    .locals 2

    .prologue
    .line 435
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    iget-object v1, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v1}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/ee;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/my/target/core/presenters/f;->a(Lcom/my/target/core/presenters/f;Landroid/content/Context;)V

    .line 436
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/my/target/ee;->f(Z)V

    .line 438
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->i(Lcom/my/target/core/presenters/f;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 440
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->j(Lcom/my/target/core/presenters/f;)Lcom/my/target/aj;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 442
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->j(Lcom/my/target/core/presenters/f;)Lcom/my/target/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    const-string v1, "closedByUser"

    invoke-virtual {v0, v1}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v1}, Lcom/my/target/core/presenters/f;->c(Lcom/my/target/core/presenters/f;)Lcom/my/target/ee;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/ee;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 445
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->a(Lcom/my/target/core/presenters/f;)Lcom/my/target/core/presenters/f$a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 447
    iget-object v0, p0, Lcom/my/target/core/presenters/f$b;->an:Lcom/my/target/core/presenters/f;

    invoke-static {v0}, Lcom/my/target/core/presenters/f;->a(Lcom/my/target/core/presenters/f;)Lcom/my/target/core/presenters/f$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/my/target/core/presenters/f$a;->bh()V

    .line 449
    :cond_1
    return-void
.end method
