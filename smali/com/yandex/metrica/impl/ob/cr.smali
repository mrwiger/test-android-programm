.class public Lcom/yandex/metrica/impl/ob/cr;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ob/cr$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private final d:Lcom/yandex/metrica/impl/ob/cr$a;

.field private volatile e:Z

.field private final f:Lcom/yandex/metrica/impl/ob/cy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/yandex/metrica/impl/ob/cr;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/yandex/metrica/impl/ob/cp;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lcom/yandex/metrica/impl/ob/da;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/da;-><init>(Lcom/yandex/metrica/impl/ob/cp;)V

    invoke-direct {p0, p2, v0}, Lcom/yandex/metrica/impl/ob/cr;-><init>(Ljava/lang/String;Lcom/yandex/metrica/impl/ob/cy;)V

    .line 53
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Lcom/yandex/metrica/impl/ob/cy;)V
    .locals 6

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/cr;->a:Ljava/util/Map;

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/cr;->b:Ljava/util/Map;

    .line 56
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/cr;->f:Lcom/yandex/metrica/impl/ob/cy;

    .line 57
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/cr;->c:Ljava/lang/String;

    .line 59
    new-instance v0, Lcom/yandex/metrica/impl/ob/cr$a;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "YMM-DW-%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 60
    invoke-static {}, Lcom/yandex/metrica/impl/utils/j;->b()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 59
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/yandex/metrica/impl/ob/cr$a;-><init>(Lcom/yandex/metrica/impl/ob/cr;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/cr;->d:Lcom/yandex/metrica/impl/ob/cr$a;

    .line 61
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cr;->d:Lcom/yandex/metrica/impl/ob/cr$a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/cr$a;->start()V

    .line 62
    return-void
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/ob/cr;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cr;->a:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/ob/cr;Ljava/util/Map;)V
    .locals 7

    .prologue
    .line 35
    .line 1155
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    new-array v3, v0, [Landroid/content/ContentValues;

    .line 1156
    const/4 v0, 0x0

    .line 1157
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1158
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1159
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 1160
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1161
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 1163
    const-string v6, "key"

    invoke-virtual {v5, v6, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1164
    if-ne v0, p0, :cond_1

    .line 1165
    const-string v0, "value"

    invoke-virtual {v5, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1184
    :cond_0
    :goto_1
    aput-object v5, v3, v2

    .line 1157
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1166
    :cond_1
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1167
    const-string v1, "value"

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1168
    const-string v0, "type"

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    .line 1169
    :cond_2
    instance-of v1, v0, Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 1170
    const-string v1, "value"

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v5, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1171
    const-string v0, "type"

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    .line 1172
    :cond_3
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 1173
    const-string v1, "value"

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v5, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1174
    const-string v0, "type"

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    .line 1175
    :cond_4
    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 1176
    const-string v1, "value"

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1177
    const-string v0, "type"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    .line 1178
    :cond_5
    instance-of v1, v0, Ljava/lang/Float;

    if-eqz v1, :cond_6

    .line 1179
    const-string v1, "value"

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v5, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1180
    const-string v0, "type"

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    .line 1181
    :cond_6
    if-eqz v0, :cond_0

    .line 1182
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1186
    :cond_7
    invoke-direct {p0, v3}, Lcom/yandex/metrica/impl/ob/cr;->a([Landroid/content/ContentValues;)V

    .line 35
    return-void
.end method

.method private a([Landroid/content/ContentValues;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 190
    if-nez p1, :cond_0

    .line 214
    :goto_0
    return-void

    .line 196
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/cr;->f:Lcom/yandex/metrica/impl/ob/cy;

    invoke-interface {v2}, Lcom/yandex/metrica/impl/ob/cy;->a()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 197
    :try_start_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 198
    array-length v2, p1

    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v3, p1, v1

    .line 199
    const-string v4, "value"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    .line 200
    const-string v4, "key"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 201
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/cr;->a()Ljava/lang/String;

    move-result-object v4

    const-string v5, "key = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    invoke-virtual {v0, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 198
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 204
    :cond_1
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/cr;->a()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x5

    invoke-virtual {v0, v4, v5, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    .line 212
    :catch_0
    move-exception v1

    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 213
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cr;->f:Lcom/yandex/metrica/impl/ob/cy;

    invoke-interface {v1, v0}, Lcom/yandex/metrica/impl/ob/cy;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 208
    :cond_2
    :try_start_2
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 212
    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 213
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cr;->f:Lcom/yandex/metrica/impl/ob/cy;

    invoke-interface {v1, v0}, Lcom/yandex/metrica/impl/ob/cy;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 212
    :catchall_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_3
    invoke-static {v1}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 213
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/cr;->f:Lcom/yandex/metrica/impl/ob/cy;

    invoke-interface {v2, v1}, Lcom/yandex/metrica/impl/ob/cy;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 214
    throw v0

    .line 212
    :catchall_1
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_3
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/ob/cr;Z)Z
    .locals 0

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/yandex/metrica/impl/ob/cr;->e:Z

    return p1
.end method

.method private b(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 322
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cr;->a:Ljava/util/Map;

    monitor-enter v1

    .line 323
    :try_start_0
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/cr;->c()V

    .line 324
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cr;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 325
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic b(Lcom/yandex/metrica/impl/ob/cr;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 35
    .line 1104
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cr;->f:Lcom/yandex/metrica/impl/ob/cy;

    invoke-interface {v0}, Lcom/yandex/metrica/impl/ob/cy;->a()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1105
    :try_start_1
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/cr;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "key"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "value"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "type"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    .line 1108
    :cond_0
    :goto_0
    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1109
    const-string v1, "key"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1110
    const-string v1, "value"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1111
    const-string v4, "type"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 1112
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1114
    packed-switch v4, :pswitch_data_0

    move-object v1, v8

    .line 1131
    :goto_1
    :pswitch_0
    if-eqz v1, :cond_0

    .line 1132
    iget-object v4, p0, Lcom/yandex/metrica/impl/ob/cr;->a:Ljava/util/Map;

    invoke-interface {v4, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    .line 1139
    :catch_0
    move-exception v1

    move-object v8, v2

    :goto_2
    invoke-static {v8}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/Cursor;)V

    .line 1140
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cr;->f:Lcom/yandex/metrica/impl/ob/cy;

    invoke-interface {v1, v0}, Lcom/yandex/metrica/impl/ob/cy;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1141
    :goto_3
    return-void

    .line 1116
    :pswitch_1
    :try_start_3
    const-string v4, "true"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_1

    :cond_1
    const-string v4, "false"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_1

    :cond_2
    move-object v1, v8

    goto :goto_1

    .line 1119
    :pswitch_2
    invoke-static {v1}, Lcom/yandex/metrica/impl/utils/k;->c(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_1

    .line 1122
    :pswitch_3
    invoke-static {v1}, Lcom/yandex/metrica/impl/utils/k;->a(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    goto :goto_1

    .line 1128
    :pswitch_4
    invoke-static {v1}, Lcom/yandex/metrica/impl/utils/k;->b(Ljava/lang/String;)Ljava/lang/Float;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v1

    goto :goto_1

    .line 1139
    :cond_3
    invoke-static {v2}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/Cursor;)V

    .line 1140
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cr;->f:Lcom/yandex/metrica/impl/ob/cy;

    invoke-interface {v1, v0}, Lcom/yandex/metrica/impl/ob/cy;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_3

    .line 1139
    :catchall_0
    move-exception v0

    move-object v2, v8

    :goto_4
    invoke-static {v2}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/Cursor;)V

    .line 1140
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cr;->f:Lcom/yandex/metrica/impl/ob/cy;

    invoke-interface {v1, v8}, Lcom/yandex/metrica/impl/ob/cy;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1141
    throw v0

    .line 1139
    :catchall_1
    move-exception v1

    move-object v2, v8

    move-object v8, v0

    move-object v0, v1

    goto :goto_4

    :catchall_2
    move-exception v1

    move-object v8, v0

    move-object v0, v1

    goto :goto_4

    :catch_1
    move-exception v0

    move-object v0, v8

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_2

    .line 1114
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method static synthetic c(Lcom/yandex/metrica/impl/ob/cr;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cr;->b:Ljava/util/Map;

    return-object v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 329
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/ob/cr;->e:Z

    if-nez v0, :cond_0

    .line 331
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cr;->a:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 334
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 229
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/cr;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 230
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 231
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p2

    .line 236
    :cond_0
    return p2
.end method

.method public a(Ljava/lang/String;J)J
    .locals 2

    .prologue
    .line 240
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/cr;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 241
    instance-of v1, v0, Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 242
    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    .line 247
    :cond_0
    return-wide p2
.end method

.method public a(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/cr;
    .locals 2

    .prologue
    .line 273
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cr;->a:Ljava/util/Map;

    monitor-enter v1

    .line 274
    :try_start_0
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/cr;->c()V

    .line 275
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cr;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 277
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cr;->d:Lcom/yandex/metrica/impl/ob/cr$a;

    monitor-enter v1

    .line 278
    :try_start_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cr;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cr;->d:Lcom/yandex/metrica/impl/ob/cr$a;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 280
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 281
    return-object p0

    .line 276
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 280
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cr;->c:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 218
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/cr;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 219
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 220
    check-cast v0, Ljava/lang/String;

    .line 225
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p2

    goto :goto_0
.end method

.method a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 311
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cr;->a:Ljava/util/Map;

    monitor-enter v1

    .line 312
    :try_start_0
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/cr;->c()V

    .line 313
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cr;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 315
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cr;->d:Lcom/yandex/metrica/impl/ob/cr$a;

    monitor-enter v1

    .line 316
    :try_start_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cr;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 317
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cr;->d:Lcom/yandex/metrica/impl/ob/cr$a;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 318
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    .line 314
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 318
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public a(Ljava/lang/String;Z)Z
    .locals 2

    .prologue
    .line 251
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/cr;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 252
    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 253
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    .line 258
    :cond_0
    return p2
.end method

.method public declared-synchronized b(Ljava/lang/String;I)Lcom/yandex/metrica/impl/ob/cr;
    .locals 1

    .prologue
    .line 295
    monitor-enter p0

    :try_start_0
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/yandex/metrica/impl/ob/cr;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296
    monitor-exit p0

    return-object p0

    .line 295
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Ljava/lang/String;J)Lcom/yandex/metrica/impl/ob/cr;
    .locals 2

    .prologue
    .line 290
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/yandex/metrica/impl/ob/cr;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 291
    return-object p0
.end method

.method public declared-synchronized b(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/cr;
    .locals 1

    .prologue
    .line 285
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/yandex/metrica/impl/ob/cr;->a(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286
    monitor-exit p0

    return-object p0

    .line 285
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Ljava/lang/String;Z)Lcom/yandex/metrica/impl/ob/cr;
    .locals 1

    .prologue
    .line 300
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/yandex/metrica/impl/ob/cr;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 301
    return-object p0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 149
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cr;->d:Lcom/yandex/metrica/impl/ob/cr$a;

    monitor-enter v1

    .line 150
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cr;->d:Lcom/yandex/metrica/impl/ob/cr$a;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 151
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
