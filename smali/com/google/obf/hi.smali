.class public Lcom/google/obf/hi;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/hi$a;,
        Lcom/google/obf/hi$c;,
        Lcom/google/obf/hi$b;
    }
.end annotation


# static fields
.field private static final a:Lcom/google/obf/eg;


# instance fields
.field private final b:Lcom/google/obf/hi$b;

.field private final c:Ljava/lang/Object;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/google/obf/hi$c;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 48
    new-instance v0, Lcom/google/obf/eh;

    invoke-direct {v0}, Lcom/google/obf/eh;-><init>()V

    const-class v1, Lcom/google/ads/interactivemedia/v3/api/UiElement;

    sget-object v2, Lcom/google/ads/interactivemedia/v3/impl/data/m;->GSON_TYPE_ADAPTER:Lcom/google/obf/ew;

    .line 49
    invoke-virtual {v0, v1, v2}, Lcom/google/obf/eh;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/obf/eh;

    move-result-object v0

    const-class v1, Lcom/google/ads/interactivemedia/v3/api/CompanionAdSlot;

    new-instance v2, Lcom/google/obf/hi$1;

    invoke-direct {v2}, Lcom/google/obf/hi$1;-><init>()V

    .line 50
    invoke-virtual {v0, v1, v2}, Lcom/google/obf/eh;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/obf/eh;

    move-result-object v0

    new-instance v1, Lcom/google/obf/it;

    invoke-direct {v1}, Lcom/google/obf/it;-><init>()V

    .line 51
    invoke-virtual {v0, v1}, Lcom/google/obf/eh;->a(Lcom/google/obf/ex;)Lcom/google/obf/eh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/obf/eh;->a()Lcom/google/obf/eg;

    move-result-object v0

    sput-object v0, Lcom/google/obf/hi;->a:Lcom/google/obf/eg;

    .line 52
    return-void
.end method

.method public constructor <init>(Lcom/google/obf/hi$b;Lcom/google/obf/hi$c;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/obf/hi;-><init>(Lcom/google/obf/hi$b;Lcom/google/obf/hi$c;Ljava/lang/String;Ljava/lang/Object;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Lcom/google/obf/hi$b;Lcom/google/obf/hi$c;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/google/obf/hi;->b:Lcom/google/obf/hi$b;

    .line 13
    iput-object p2, p0, Lcom/google/obf/hi;->e:Lcom/google/obf/hi$c;

    .line 14
    iput-object p3, p0, Lcom/google/obf/hi;->d:Ljava/lang/String;

    .line 15
    iput-object p4, p0, Lcom/google/obf/hi;->c:Ljava/lang/Object;

    .line 16
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/obf/hi;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;,
            Lcom/google/obf/eu;
        }
    .end annotation

    .prologue
    .line 1
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 3
    const-string v2, "sid"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4
    if-nez v2, :cond_0

    .line 5
    new-instance v0, Ljava/net/MalformedURLException;

    const-string v1, "Session id must be provided in message."

    invoke-direct {v0, v1}, Ljava/net/MalformedURLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 6
    :cond_0
    new-instance v2, Lcom/google/obf/hi;

    invoke-static {v1}, Lcom/google/obf/hi$b;->valueOf(Ljava/lang/String;)Lcom/google/obf/hi$b;

    move-result-object v1

    const-string v3, "type"

    .line 7
    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/obf/hi$c;->valueOf(Ljava/lang/String;)Lcom/google/obf/hi$c;

    move-result-object v3

    const-string v4, "sid"

    .line 8
    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/google/obf/hi;->a:Lcom/google/obf/eg;

    const-string v6, "data"

    .line 9
    invoke-virtual {v0, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-class v6, Lcom/google/ads/interactivemedia/v3/impl/data/l;

    invoke-virtual {v5, v0, v6}, Lcom/google/obf/eg;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v2, v1, v3, v4, v0}, Lcom/google/obf/hi;-><init>(Lcom/google/obf/hi$b;Lcom/google/obf/hi$c;Ljava/lang/String;Ljava/lang/Object;)V

    .line 10
    return-object v2
.end method


# virtual methods
.method public a()Lcom/google/obf/hi$b;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/obf/hi;->b:Lcom/google/obf/hi$b;

    return-object v0
.end method

.method public b()Lcom/google/obf/hi$c;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/obf/hi;->e:Lcom/google/obf/hi$c;

    return-object v0
.end method

.method public c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/obf/hi;->c:Ljava/lang/Object;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/obf/hi;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 5

    .prologue
    .line 23
    new-instance v0, Lcom/google/obf/jm$a;

    invoke-direct {v0}, Lcom/google/obf/jm$a;-><init>()V

    const-string v1, "type"

    iget-object v2, p0, Lcom/google/obf/hi;->e:Lcom/google/obf/hi$c;

    .line 24
    invoke-virtual {v0, v1, v2}, Lcom/google/obf/jm$a;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/obf/jm$a;

    move-result-object v0

    const-string v1, "sid"

    iget-object v2, p0, Lcom/google/obf/hi;->d:Ljava/lang/String;

    .line 25
    invoke-virtual {v0, v1, v2}, Lcom/google/obf/jm$a;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/obf/jm$a;

    move-result-object v0

    .line 26
    iget-object v1, p0, Lcom/google/obf/hi;->c:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 27
    const-string v1, "data"

    iget-object v2, p0, Lcom/google/obf/hi;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/obf/jm$a;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/obf/jm$a;

    .line 28
    :cond_0
    invoke-virtual {v0}, Lcom/google/obf/jm$a;->a()Lcom/google/obf/jm;

    move-result-object v0

    .line 29
    const-string v1, "%s(\'%s\', %s);"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "javascript:adsense.mobileads.afmanotify.receiveMessage"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/obf/hi;->b:Lcom/google/obf/hi$b;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Lcom/google/obf/hi;->a:Lcom/google/obf/eg;

    invoke-virtual {v4, v0}, Lcom/google/obf/eg;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 30
    if-ne p0, p1, :cond_1

    .line 45
    :cond_0
    :goto_0
    return v0

    .line 32
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 33
    goto :goto_0

    .line 34
    :cond_2
    instance-of v2, p1, Lcom/google/obf/hi;

    if-nez v2, :cond_3

    move v0, v1

    .line 35
    goto :goto_0

    .line 36
    :cond_3
    check-cast p1, Lcom/google/obf/hi;

    .line 37
    iget-object v2, p0, Lcom/google/obf/hi;->b:Lcom/google/obf/hi$b;

    iget-object v3, p1, Lcom/google/obf/hi;->b:Lcom/google/obf/hi$b;

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 38
    goto :goto_0

    .line 39
    :cond_4
    iget-object v2, p0, Lcom/google/obf/hi;->c:Ljava/lang/Object;

    iget-object v3, p1, Lcom/google/obf/hi;->c:Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/obf/ix;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 40
    goto :goto_0

    .line 41
    :cond_5
    iget-object v2, p0, Lcom/google/obf/hi;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/obf/hi;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/obf/ix;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 42
    goto :goto_0

    .line 43
    :cond_6
    iget-object v2, p0, Lcom/google/obf/hi;->e:Lcom/google/obf/hi$c;

    iget-object v3, p1, Lcom/google/obf/hi;->e:Lcom/google/obf/hi$c;

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 44
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 46
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/obf/hi;->b:Lcom/google/obf/hi$b;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/obf/hi;->c:Ljava/lang/Object;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/obf/hi;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/obf/hi;->e:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/obf/ix;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 47
    const-string v0, "JavaScriptMessage [command=%s, type=%s, sid=%s, data=%s]"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/obf/hi;->b:Lcom/google/obf/hi$b;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/obf/hi;->e:Lcom/google/obf/hi$c;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/obf/hi;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/obf/hi;->c:Ljava/lang/Object;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
