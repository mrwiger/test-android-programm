.class Lcom/samsung/multiscreen/MSFDSearchProvider$2;
.super Ljava/lang/Object;
.source "MSFDSearchProvider.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/multiscreen/MSFDSearchProvider;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private numDiscover:I

.field final synthetic this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/MSFDSearchProvider;)V
    .locals 1
    .param p1, "this$0"    # Lcom/samsung/multiscreen/MSFDSearchProvider;

    .prologue
    .line 296
    iput-object p1, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$2;->this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$2;->numDiscover:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 303
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 304
    .local v0, "currentapiVersion":I
    const/16 v2, 0x13

    if-ne v0, v2, :cond_0

    .line 305
    iget-object v2, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$2;->this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;

    invoke-static {v2}, Lcom/samsung/multiscreen/MSFDSearchProvider;->access$100(Lcom/samsung/multiscreen/MSFDSearchProvider;)Ljava/net/MulticastSocket;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$2;->this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;

    invoke-static {v3}, Lcom/samsung/multiscreen/MSFDSearchProvider;->access$400(Lcom/samsung/multiscreen/MSFDSearchProvider;)Ljava/net/DatagramPacket;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/net/MulticastSocket;->send(Ljava/net/DatagramPacket;)V

    .line 316
    .end local v0    # "currentapiVersion":I
    :goto_0
    return-void

    .line 307
    .restart local v0    # "currentapiVersion":I
    :cond_0
    iget v2, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$2;->numDiscover:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$2;->numDiscover:I

    const/4 v3, 0x3

    if-ge v2, v3, :cond_1

    .line 308
    iget-object v2, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$2;->this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;

    invoke-static {v2}, Lcom/samsung/multiscreen/MSFDSearchProvider;->access$100(Lcom/samsung/multiscreen/MSFDSearchProvider;)Ljava/net/MulticastSocket;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$2;->this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;

    invoke-static {v3}, Lcom/samsung/multiscreen/MSFDSearchProvider;->access$400(Lcom/samsung/multiscreen/MSFDSearchProvider;)Ljava/net/DatagramPacket;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/net/MulticastSocket;->send(Ljava/net/DatagramPacket;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 313
    .end local v0    # "currentapiVersion":I
    :catch_0
    move-exception v1

    .line 314
    .local v1, "e":Ljava/io/IOException;
    const-string v2, "MSFDSearchProvider"

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 310
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v0    # "currentapiVersion":I
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$2;->this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;

    invoke-static {v2}, Lcom/samsung/multiscreen/MSFDSearchProvider;->access$500(Lcom/samsung/multiscreen/MSFDSearchProvider;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/ScheduledExecutorService;->shutdown()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
