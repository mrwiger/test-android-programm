.class public Lru/cn/ad/AdsManager;
.super Ljava/lang/Object;
.source "AdsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/ad/AdsManager$AdapterCache;
    }
.end annotation


# static fields
.field private static adPlaces:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/money_miner/replies/AdPlace;",
            ">;"
        }
    .end annotation
.end field

.field private static opportunityMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static preloader:Lru/cn/ad/AdsManager$AdapterCache;

.field private static updateTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 67
    const/4 v0, 0x0

    sput-object v0, Lru/cn/ad/AdsManager;->adPlaces:Ljava/util/List;

    .line 68
    const-wide/16 v0, 0x0

    sput-wide v0, Lru/cn/ad/AdsManager;->updateTime:J

    .line 70
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lru/cn/ad/AdsManager;->opportunityMap:Ljava/util/Map;

    .line 72
    sget-object v0, Lru/cn/ad/AdsManager$AdapterCache;->NONE:Lru/cn/ad/AdsManager$AdapterCache;

    sput-object v0, Lru/cn/ad/AdsManager;->preloader:Lru/cn/ad/AdsManager$AdapterCache;

    return-void
.end method

.method public static consumeOpportunity(Ljava/lang/String;)V
    .locals 4
    .param p0, "placeId"    # Ljava/lang/String;

    .prologue
    .line 166
    sget-object v0, Lru/cn/ad/AdsManager;->opportunityMap:Ljava/util/Map;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    return-void
.end method

.method public static getDeviceId(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 145
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getPlace(Landroid/content/Context;Ljava/lang/String;)Lru/cn/api/money_miner/replies/AdPlace;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "placeId"    # Ljava/lang/String;

    .prologue
    .line 80
    invoke-static {p0}, Lru/cn/ad/AdsManager;->updatePlaceList(Landroid/content/Context;)V

    .line 81
    sget-object v1, Lru/cn/ad/AdsManager;->adPlaces:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 82
    sget-object v1, Lru/cn/ad/AdsManager;->adPlaces:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/money_miner/replies/AdPlace;

    .line 83
    .local v0, "place":Lru/cn/api/money_miner/replies/AdPlace;
    iget-object v2, v0, Lru/cn/api/money_miner/replies/AdPlace;->placeId:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 89
    .end local v0    # "place":Lru/cn/api/money_miner/replies/AdPlace;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getPreRollPlaceId(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isLive"    # Z

    .prologue
    .line 98
    if-nez p0, :cond_0

    .line 99
    const/4 v0, 0x0

    .line 119
    :goto_0
    return-object v0

    .line 101
    :cond_0
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 102
    invoke-static {p0}, Lru/cn/domain/KidsObject;->isKidsMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 103
    if-eqz p1, :cond_1

    .line 104
    const-string v0, "d63KNJ6f"

    goto :goto_0

    .line 106
    :cond_1
    const-string v0, "KcHsHKJ"

    goto :goto_0

    .line 109
    :cond_2
    if-eqz p1, :cond_3

    .line 110
    const-string v0, "qpXuIJ8e"

    goto :goto_0

    .line 112
    :cond_3
    const-string v0, "GZBCy4hS"

    goto :goto_0

    .line 116
    :cond_4
    if-eqz p1, :cond_5

    .line 117
    const-string v0, "FfNFWuZu"

    goto :goto_0

    .line 119
    :cond_5
    const-string v0, "0hejLO2s"

    goto :goto_0
.end method

.method static getStoredPlace(Ljava/lang/String;)Lru/cn/api/money_miner/replies/AdPlace;
    .locals 4
    .param p0, "placeId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 170
    sget-object v2, Lru/cn/ad/AdsManager;->adPlaces:Ljava/util/List;

    if-nez v2, :cond_0

    move-object v0, v1

    .line 179
    :goto_0
    return-object v0

    .line 173
    :cond_0
    sget-object v2, Lru/cn/ad/AdsManager;->adPlaces:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/money_miner/replies/AdPlace;

    .line 174
    .local v0, "place":Lru/cn/api/money_miner/replies/AdPlace;
    iget-object v3, v0, Lru/cn/api/money_miner/replies/AdPlace;->placeId:Ljava/lang/String;

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    .end local v0    # "place":Lru/cn/api/money_miner/replies/AdPlace;
    :cond_2
    move-object v0, v1

    .line 179
    goto :goto_0
.end method

.method public static hasOpportunity(Ljava/lang/String;)Z
    .locals 8
    .param p0, "placeId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 157
    sget-object v3, Lru/cn/ad/AdsManager;->opportunityMap:Ljava/util/Map;

    invoke-interface {v3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 158
    .local v0, "lastConsumed":Ljava/lang/Long;
    if-nez v0, :cond_1

    .line 162
    :cond_0
    :goto_0
    return v2

    .line 161
    :cond_1
    invoke-static {p0}, Lru/cn/ad/AdsManager;->getStoredPlace(Ljava/lang/String;)Lru/cn/api/money_miner/replies/AdPlace;

    move-result-object v1

    .line 162
    .local v1, "place":Lru/cn/api/money_miner/replies/AdPlace;
    if-eqz v1, :cond_0

    iget v3, v1, Lru/cn/api/money_miner/replies/AdPlace;->requestDelay:I

    mul-int/lit16 v3, v3, 0x3e8

    int-to-long v4, v3

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    add-long/2addr v4, v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-ltz v3, :cond_0

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static preload(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 125
    sget-object v1, Lru/cn/ad/AdsManager;->preloader:Lru/cn/ad/AdsManager$AdapterCache;

    sget-object v2, Lru/cn/ad/AdsManager$AdapterCache;->NONE:Lru/cn/ad/AdsManager$AdapterCache;

    if-eq v1, v2, :cond_1

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    new-instance v0, Lru/cn/ad/preloader/AdPreLoader;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lru/cn/ad/preloader/AdPreLoader;-><init>(Landroid/content/Context;)V

    .line 129
    .local v0, "loader":Lru/cn/ad/preloader/AdPreLoader;
    sput-object v0, Lru/cn/ad/AdsManager;->preloader:Lru/cn/ad/AdsManager$AdapterCache;

    .line 132
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v1

    if-nez v1, :cond_0

    .line 133
    invoke-static {p0}, Lru/cn/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 134
    const-string v1, "QDhB8u"

    new-instance v2, Lru/cn/ad/interstitial/DefaultInterstitialFactory;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lru/cn/ad/interstitial/DefaultInterstitialFactory;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Lru/cn/ad/preloader/AdPreLoader;->preload(Ljava/lang/String;Lru/cn/ad/AdAdapter$Factory;)V

    goto :goto_0
.end method

.method public static preloader()Lru/cn/ad/AdsManager$AdapterCache;
    .locals 1

    .prologue
    .line 140
    sget-object v0, Lru/cn/ad/AdsManager;->preloader:Lru/cn/ad/AdsManager$AdapterCache;

    return-object v0
.end method

.method public static setNeedsUpdate()V
    .locals 2

    .prologue
    .line 93
    const-wide/16 v0, 0x0

    sput-wide v0, Lru/cn/ad/AdsManager;->updateTime:J

    .line 94
    sget-object v0, Lru/cn/ad/AdsManager;->preloader:Lru/cn/ad/AdsManager$AdapterCache;

    invoke-interface {v0}, Lru/cn/ad/AdsManager$AdapterCache;->invalidate()V

    .line 95
    return-void
.end method

.method private static declared-synchronized updatePlaceList(Landroid/content/Context;)V
    .locals 14
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v7, 0x0

    .line 183
    const-class v8, Lru/cn/ad/AdsManager;

    monitor-enter v8

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 185
    .local v2, "now":J
    sget-wide v10, Lru/cn/ad/AdsManager;->updateTime:J

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-lez v9, :cond_1

    sget-wide v10, Lru/cn/ad/AdsManager;->updateTime:J

    const-wide/32 v12, 0x36ee80

    add-long/2addr v10, v12

    cmp-long v9, v10, v2

    if-gez v9, :cond_1

    .line 186
    .local v0, "expiredCache":Z
    :goto_0
    sget-wide v10, Lru/cn/ad/AdsManager;->updateTime:J

    const-wide/32 v12, 0x36ee80

    add-long/2addr v10, v12

    cmp-long v7, v10, v2

    if-gez v7, :cond_0

    .line 189
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 190
    const/4 v7, 0x5

    new-array v5, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v9, "GZBCy4hS"

    aput-object v9, v5, v7

    const/4 v7, 0x1

    const-string v9, "qpXuIJ8e"

    aput-object v9, v5, v7

    const/4 v7, 0x2

    const-string v9, "KcHsHKJ"

    aput-object v9, v5, v7

    const/4 v7, 0x3

    const-string v9, "d63KNJ6f"

    aput-object v9, v5, v7

    const/4 v7, 0x4

    const-string v9, "0sCyVJeb"

    aput-object v9, v5, v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    .local v5, "places":[Ljava/lang/String;
    :goto_1
    :try_start_1
    invoke-static {p0}, Lru/cn/api/ServiceLocator;->moneyMiner(Landroid/content/Context;)Lru/cn/api/money_miner/retrofit/MoneyMinerAPI;

    move-result-object v1

    .line 209
    .local v1, "moneyMinerAPI":Lru/cn/api/money_miner/retrofit/MoneyMinerAPI;
    if-eqz v1, :cond_0

    .line 210
    const-string v7, ","

    invoke-static {v7, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v1, v7}, Lru/cn/api/money_miner/retrofit/MoneyMinerAPI;->places(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v7

    .line 211
    invoke-virtual {v7}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/api/money_miner/replies/AdPlacesReply;

    .line 213
    .local v4, "placeList":Lru/cn/api/money_miner/replies/AdPlacesReply;
    iget-object v7, v4, Lru/cn/api/money_miner/replies/AdPlacesReply;->places:Ljava/util/List;

    sput-object v7, Lru/cn/ad/AdsManager;->adPlaces:Ljava/util/List;

    .line 214
    sput-wide v2, Lru/cn/ad/AdsManager;->updateTime:J

    .line 216
    if-eqz v0, :cond_0

    .line 217
    sget-object v7, Lru/cn/ad/AdsManager;->preloader:Lru/cn/ad/AdsManager$AdapterCache;

    invoke-interface {v7}, Lru/cn/ad/AdsManager$AdapterCache;->invalidate()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 224
    .end local v1    # "moneyMinerAPI":Lru/cn/api/money_miner/retrofit/MoneyMinerAPI;
    .end local v4    # "placeList":Lru/cn/api/money_miner/replies/AdPlacesReply;
    .end local v5    # "places":[Ljava/lang/String;
    :cond_0
    :goto_2
    monitor-exit v8

    return-void

    .end local v0    # "expiredCache":Z
    :cond_1
    move v0, v7

    .line 185
    goto :goto_0

    .line 198
    .restart local v0    # "expiredCache":Z
    :cond_2
    const/4 v7, 0x5

    :try_start_2
    new-array v5, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v9, "0hejLO2s"

    aput-object v9, v5, v7

    const/4 v7, 0x1

    const-string v9, "FfNFWuZu"

    aput-object v9, v5, v7

    const/4 v7, 0x2

    const-string v9, "Vq0Ps6K9"

    aput-object v9, v5, v7

    const/4 v7, 0x3

    const-string v9, "QDhB8u"

    aput-object v9, v5, v7

    const/4 v7, 0x4

    const-string v9, "PIbEnlAp"

    aput-object v9, v5, v7

    .restart local v5    # "places":[Ljava/lang/String;
    goto :goto_1

    .line 220
    :catch_0
    move-exception v6

    .line 221
    .local v6, "t":Ljava/lang/Throwable;
    invoke-static {v6}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 183
    .end local v0    # "expiredCache":Z
    .end local v2    # "now":J
    .end local v5    # "places":[Ljava/lang/String;
    .end local v6    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v7

    monitor-exit v8

    throw v7
.end method
