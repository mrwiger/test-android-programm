.class public Lru/cn/tv/stb/categories/CategoryAdapter;
.super Landroid/support/v4/widget/CursorAdapter;
.source "CategoryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/stb/categories/CategoryAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private foldedItemLayout:I

.field private groupLayout:I

.field private itemLayout:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;III)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "itemLayout"    # I
    .param p4, "groupLayout"    # I
    .param p5, "foldedItemLayout"    # I

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 32
    iput p3, p0, Lru/cn/tv/stb/categories/CategoryAdapter;->itemLayout:I

    .line 33
    iput p4, p0, Lru/cn/tv/stb/categories/CategoryAdapter;->groupLayout:I

    .line 34
    iput p5, p0, Lru/cn/tv/stb/categories/CategoryAdapter;->foldedItemLayout:I

    .line 35
    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x0

    return v0
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 67
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/tv/stb/categories/CategoryAdapter$ViewHolder;

    .line 68
    .local v0, "holder":Lru/cn/tv/stb/categories/CategoryAdapter$ViewHolder;
    const-string v3, "title"

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 70
    .local v2, "title":Ljava/lang/String;
    const-string v3, "image_resource"

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 71
    .local v1, "image":I
    iget-object v3, v0, Lru/cn/tv/stb/categories/CategoryAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    if-eqz v3, :cond_0

    .line 72
    if-lez v1, :cond_2

    .line 73
    iget-object v3, v0, Lru/cn/tv/stb/categories/CategoryAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 74
    iget-object v3, v0, Lru/cn/tv/stb/categories/CategoryAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 80
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    .line 81
    iget-object v3, v0, Lru/cn/tv/stb/categories/CategoryAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    :cond_1
    return-void

    .line 76
    :cond_2
    iget-object v3, v0, Lru/cn/tv/stb/categories/CategoryAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 87
    invoke-virtual {p0, p1}, Lru/cn/tv/stb/categories/CategoryAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 88
    .local v0, "c":Landroid/database/Cursor;
    const-string v1, "item_type"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    return v1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x3

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 93
    invoke-virtual {p0, p1}, Lru/cn/tv/stb/categories/CategoryAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 94
    .local v0, "c":Landroid/database/Cursor;
    const-string v1, "item_type"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 39
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 40
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const/4 v2, 0x0

    .line 41
    .local v2, "layout":I
    const-string v4, "item_type"

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 55
    :goto_0
    const/4 v4, 0x0

    invoke-virtual {v1, v2, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 57
    .local v3, "view":Landroid/view/View;
    new-instance v0, Lru/cn/tv/stb/categories/CategoryAdapter$ViewHolder;

    invoke-direct {v0, v5}, Lru/cn/tv/stb/categories/CategoryAdapter$ViewHolder;-><init>(Lru/cn/tv/stb/categories/CategoryAdapter$1;)V

    .line 58
    .local v0, "holder":Lru/cn/tv/stb/categories/CategoryAdapter$ViewHolder;
    const v4, 0x7f0900f3

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v0, Lru/cn/tv/stb/categories/CategoryAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    .line 59
    const v4, 0x7f0901d7

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v0, Lru/cn/tv/stb/categories/CategoryAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 61
    invoke-virtual {v3, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 62
    return-object v3

    .line 43
    .end local v0    # "holder":Lru/cn/tv/stb/categories/CategoryAdapter$ViewHolder;
    .end local v3    # "view":Landroid/view/View;
    :pswitch_0
    iget v2, p0, Lru/cn/tv/stb/categories/CategoryAdapter;->groupLayout:I

    .line 44
    goto :goto_0

    .line 47
    :pswitch_1
    iget v2, p0, Lru/cn/tv/stb/categories/CategoryAdapter;->itemLayout:I

    .line 48
    goto :goto_0

    .line 51
    :pswitch_2
    iget v2, p0, Lru/cn/tv/stb/categories/CategoryAdapter;->foldedItemLayout:I

    goto :goto_0

    .line 41
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
