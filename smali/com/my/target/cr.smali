.class public final Lcom/my/target/cr;
.super Lcom/my/target/f$a;
.source "NativeAdServiceBuilder.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/my/target/f$a;-><init>()V

    .line 29
    return-void
.end method

.method public static f()Lcom/my/target/cr;
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/my/target/cr;

    invoke-direct {v0}, Lcom/my/target/cr;-><init>()V

    return-object v0
.end method


# virtual methods
.method protected final b(Lcom/my/target/b;Landroid/content/Context;)Ljava/util/Map;
    .locals 6
    .param p1, "adConfig"    # Lcom/my/target/b;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/b;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    invoke-super {p0, p1, p2}, Lcom/my/target/f$a;->b(Lcom/my/target/b;Landroid/content/Context;)Ljava/util/Map;

    move-result-object v2

    .line 36
    invoke-static {}, Lcom/my/target/common/MyTargetPrivacy;->isConsentSpecified()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/my/target/common/MyTargetPrivacy;->isUserConsent()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v2

    .line 63
    :goto_0
    return-object v0

    .line 41
    :cond_0
    invoke-static {}, Lcom/my/target/cs;->u()Landroid/util/LruCache;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v1

    .line 42
    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 44
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 45
    const/4 v0, 0x0

    .line 46
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 48
    if-eqz v1, :cond_1

    .line 50
    const-string v5, ","

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 54
    :cond_1
    const/4 v1, 0x1

    goto :goto_2

    .line 58
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 59
    const-string v1, "exb"

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    const-string v1, "Exclude list: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    :cond_3
    move-object v0, v2

    .line 63
    goto :goto_0
.end method
