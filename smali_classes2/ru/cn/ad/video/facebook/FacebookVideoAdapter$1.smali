.class Lru/cn/ad/video/facebook/FacebookVideoAdapter$1;
.super Ljava/lang/Object;
.source "FacebookVideoAdapter.java"

# interfaces
.implements Lcom/facebook/ads/InstreamVideoAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/ad/video/facebook/FacebookVideoAdapter;->onLoad()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/ad/video/facebook/FacebookVideoAdapter;


# direct methods
.method constructor <init>(Lru/cn/ad/video/facebook/FacebookVideoAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/ad/video/facebook/FacebookVideoAdapter;

    .prologue
    .line 34
    iput-object p1, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter$1;->this$0:Lru/cn/ad/video/facebook/FacebookVideoAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAdClicked(Lcom/facebook/ads/Ad;)V
    .locals 3
    .param p1, "ad"    # Lcom/facebook/ads/Ad;

    .prologue
    .line 60
    iget-object v0, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter$1;->this$0:Lru/cn/ad/video/facebook/FacebookVideoAdapter;

    sget-object v1, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_CLICK:Lru/cn/domain/statistics/inetra/AdvEvent;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/util/Map;)V

    .line 61
    return-void
.end method

.method public onAdLoaded(Lcom/facebook/ads/Ad;)V
    .locals 1
    .param p1, "ad"    # Lcom/facebook/ads/Ad;

    .prologue
    .line 54
    iget-object v0, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter$1;->this$0:Lru/cn/ad/video/facebook/FacebookVideoAdapter;

    invoke-static {v0}, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->access$200(Lru/cn/ad/video/facebook/FacebookVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter$1;->this$0:Lru/cn/ad/video/facebook/FacebookVideoAdapter;

    invoke-static {v0}, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->access$300(Lru/cn/ad/video/facebook/FacebookVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdLoaded()V

    .line 56
    :cond_0
    return-void
.end method

.method public onAdVideoComplete(Lcom/facebook/ads/Ad;)V
    .locals 3
    .param p1, "ad"    # Lcom/facebook/ads/Ad;

    .prologue
    .line 37
    iget-object v0, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter$1;->this$0:Lru/cn/ad/video/facebook/FacebookVideoAdapter;

    sget-object v1, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_COMPLETE:Lru/cn/domain/statistics/inetra/AdvEvent;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/util/Map;)V

    .line 39
    iget-object v0, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter$1;->this$0:Lru/cn/ad/video/facebook/FacebookVideoAdapter;

    invoke-static {v0}, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->access$000(Lru/cn/ad/video/facebook/FacebookVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter$1;->this$0:Lru/cn/ad/video/facebook/FacebookVideoAdapter;

    invoke-static {v0}, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->access$100(Lru/cn/ad/video/facebook/FacebookVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdEnded()V

    .line 41
    :cond_0
    return-void
.end method

.method public onError(Lcom/facebook/ads/Ad;Lcom/facebook/ads/AdError;)V
    .locals 5
    .param p1, "ad"    # Lcom/facebook/ads/Ad;
    .param p2, "adError"    # Lcom/facebook/ads/AdError;

    .prologue
    .line 45
    invoke-virtual {p2}, Lcom/facebook/ads/AdError;->getErrorCode()I

    move-result v0

    const/16 v1, 0x3e9

    if-ne v0, v1, :cond_0

    .line 50
    :goto_0
    return-void

    .line 48
    :cond_0
    iget-object v0, p0, Lru/cn/ad/video/facebook/FacebookVideoAdapter$1;->this$0:Lru/cn/ad/video/facebook/FacebookVideoAdapter;

    sget-object v1, Lru/cn/domain/statistics/inetra/ErrorCode;->ADV_ERROR_LOAD_BANNER:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v2, "FacebookError"

    .line 49
    invoke-virtual {p2}, Lcom/facebook/ads/AdError;->getErrorCode()I

    move-result v3

    const/4 v4, 0x0

    .line 48
    invoke-virtual {v0, v1, v2, v3, v4}, Lru/cn/ad/video/facebook/FacebookVideoAdapter;->reportError(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/util/Map;)V

    goto :goto_0
.end method

.method public onLoggingImpression(Lcom/facebook/ads/Ad;)V
    .locals 0
    .param p1, "ad"    # Lcom/facebook/ads/Ad;

    .prologue
    .line 66
    return-void
.end method
