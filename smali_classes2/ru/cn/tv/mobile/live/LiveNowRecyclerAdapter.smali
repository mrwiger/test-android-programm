.class public Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;
.super Lru/cn/view/CursorRecyclerViewAdapter;
.source "LiveNowRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;,
        Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LoadingViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lru/cn/view/CursorRecyclerViewAdapter",
        "<",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private loading:Z

.field private final sdf:Ljava/text/SimpleDateFormat;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lru/cn/view/CursorRecyclerViewAdapter;-><init>(Landroid/database/Cursor;)V

    .line 36
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->setHasStableIds(Z)V

    .line 38
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "HH:mm"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->sdf:Ljava/text/SimpleDateFormat;

    .line 39
    return-void
.end method

.method private formatDate(JJ)Ljava/lang/String;
    .locals 9
    .param p1, "time"    # J
    .param p3, "duration"    # J

    .prologue
    const-wide/16 v6, 0x3e8

    .line 169
    iget-object v1, p0, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->sdf:Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->setCalendar(Ljava/util/Calendar;)V

    .line 171
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->sdf:Ljava/text/SimpleDateFormat;

    new-instance v3, Ljava/util/Date;

    mul-long v4, p1, v6

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 172
    .local v0, "startEndTime":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->sdf:Ljava/text/SimpleDateFormat;

    new-instance v3, Ljava/util/Date;

    add-long v4, p1, p3

    mul-long/2addr v4, v6

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 174
    return-object v0
.end method

.method private setProgress(Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;JJ)V
    .locals 10
    .param p1, "holder"    # Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;
    .param p2, "time"    # J
    .param p4, "duration"    # J

    .prologue
    .line 153
    const/4 v0, 0x0

    .line 154
    .local v0, "p":I
    const-wide/16 v6, 0x0

    cmp-long v1, p4, v6

    if-lez v1, :cond_1

    .line 155
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long v4, v6, v8

    .line 156
    .local v4, "timeSec":J
    sub-long v2, v4, p2

    .line 158
    .local v2, "timeElapsed":J
    const-wide/16 v6, 0x0

    cmp-long v1, v2, v6

    if-gez v1, :cond_0

    .line 159
    const-wide/16 v2, 0x0

    .line 162
    :cond_0
    iget-object v1, p1, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;->telecastProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getMax()I

    move-result v1

    int-to-long v6, v1

    mul-long/2addr v6, v2

    div-long/2addr v6, p4

    long-to-int v0, v6

    .line 165
    .end local v2    # "timeElapsed":J
    .end local v4    # "timeSec":J
    :cond_1
    iget-object v1, p1, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;->telecastProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 166
    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 2

    .prologue
    .line 135
    invoke-super {p0}, Lru/cn/view/CursorRecyclerViewAdapter;->getItemCount()I

    move-result v0

    .line 136
    .local v0, "itemCount":I
    iget-boolean v1, p0, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->loading:Z

    if-eqz v1, :cond_0

    .line 137
    add-int/lit8 v0, v0, 0x1

    .line 140
    :cond_0
    return v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 127
    iget-boolean v0, p0, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->loading:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 128
    const-wide/16 v0, 0x1

    .line 130
    :goto_0
    return-wide v0

    :cond_0
    invoke-super {p0, p1}, Lru/cn/view/CursorRecyclerViewAdapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 118
    iget-boolean v0, p0, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->loading:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 119
    const/4 v0, 0x1

    .line 121
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 2
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 59
    invoke-virtual {p0, p2}, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 60
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/database/Cursor;)V

    .line 64
    :goto_0
    return-void

    .line 62
    :cond_0
    invoke-super {p0, p1, p2}, Lru/cn/view/CursorRecyclerViewAdapter;->onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V

    goto :goto_0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/database/Cursor;)V
    .locals 17
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 68
    move-object/from16 v0, p1

    instance-of v2, v0, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;

    if-eqz v2, :cond_5

    move-object/from16 v3, p1

    .line 70
    check-cast v3, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;

    .line 71
    .local v3, "itemViewHolder":Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;
    check-cast p2, Landroid/database/CursorWrapper;

    .line 72
    .end local p2    # "cursor":Landroid/database/Cursor;
    invoke-virtual/range {p2 .. p2}, Landroid/database/CursorWrapper;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v8

    check-cast v8, Lru/cn/api/provider/cursor/TelecastItemCursor;

    .line 73
    .local v8, "c":Lru/cn/api/provider/cursor/TelecastItemCursor;
    invoke-virtual {v8}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getImage()Ljava/lang/String;

    move-result-object v12

    .line 75
    .local v12, "image":Ljava/lang/String;
    invoke-virtual {v8}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getDescription()Ljava/lang/String;

    move-result-object v11

    .line 77
    .local v11, "description":Ljava/lang/String;
    iget-object v2, v3, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;->channelTitle:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 78
    invoke-virtual {v8}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getChannelTitle()Ljava/lang/String;

    move-result-object v9

    .line 79
    .local v9, "channelTitle":Ljava/lang/String;
    iget-object v2, v3, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;->channelTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    .end local v9    # "channelTitle":Ljava/lang/String;
    :cond_0
    iget-object v2, v3, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;->telecastTitle:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .line 83
    invoke-virtual {v8}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getTitle()Ljava/lang/String;

    move-result-object v15

    .line 84
    .local v15, "title":Ljava/lang/String;
    iget-object v2, v3, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;->telecastTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    .end local v15    # "title":Ljava/lang/String;
    :cond_1
    iget-object v2, v3, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;->telecastImage:Landroid/widget/ImageView;

    if-eqz v2, :cond_2

    .line 88
    iget-object v2, v3, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;->telecastImage:Landroid/widget/ImageView;

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 90
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v10

    .line 91
    .local v10, "context":Landroid/content/Context;
    invoke-static {v10}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v2

    .line 92
    invoke-virtual {v2, v12}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    .line 93
    invoke-virtual {v2}, Lcom/squareup/picasso/RequestCreator;->fit()Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    const v16, 0x7f0802dc

    .line 94
    move/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    iget-object v0, v3, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;->telecastImage:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    .line 95
    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 98
    .end local v10    # "context":Landroid/content/Context;
    :cond_2
    invoke-virtual {v8}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getTime()J

    move-result-wide v4

    .line 99
    .local v4, "time":J
    invoke-virtual {v8}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getDuration()J

    move-result-wide v6

    .line 101
    .local v6, "duration":J
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6, v7}, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->formatDate(JJ)Ljava/lang/String;

    move-result-object v14

    .line 102
    .local v14, "startEndTime":Ljava/lang/String;
    iget-object v2, v3, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;->startEndTime:Landroid/widget/TextView;

    invoke-virtual {v2, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    iget-object v2, v3, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;->description:Landroid/widget/TextView;

    if-eqz v2, :cond_3

    .line 105
    iget-object v2, v3, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;->description:Landroid/widget/TextView;

    invoke-virtual {v2, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    move-object/from16 v2, p0

    .line 108
    invoke-direct/range {v2 .. v7}, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->setProgress(Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;JJ)V

    .line 114
    .end local v3    # "itemViewHolder":Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;
    .end local v4    # "time":J
    .end local v6    # "duration":J
    .end local v8    # "c":Lru/cn/api/provider/cursor/TelecastItemCursor;
    .end local v11    # "description":Ljava/lang/String;
    .end local v12    # "image":Ljava/lang/String;
    .end local v14    # "startEndTime":Ljava/lang/String;
    :cond_4
    :goto_0
    return-void

    .line 110
    .restart local p2    # "cursor":Landroid/database/Cursor;
    :cond_5
    move-object/from16 v0, p1

    instance-of v2, v0, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LoadingViewHolder;

    if-eqz v2, :cond_4

    move-object/from16 v13, p1

    .line 111
    check-cast v13, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LoadingViewHolder;

    .line 112
    .local v13, "loadingViewHolder":Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LoadingViewHolder;
    iget-object v2, v13, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LoadingViewHolder;->itemProgress:Landroid/widget/ProgressBar;

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    goto :goto_0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    const/4 v3, 0x0

    .line 43
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 45
    .local v0, "inflater":Landroid/view/LayoutInflater;
    if-nez p2, :cond_0

    .line 46
    const v2, 0x7f0c00a4

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 47
    .local v1, "view":Landroid/view/View;
    new-instance v2, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;

    invoke-direct {v2, v1}, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;-><init>(Landroid/view/View;)V

    .line 54
    .end local v1    # "view":Landroid/view/View;
    :goto_0
    return-object v2

    .line 49
    :cond_0
    const/4 v2, 0x1

    if-ne p2, v2, :cond_1

    .line 50
    const v2, 0x7f0c00a1

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 51
    .restart local v1    # "view":Landroid/view/View;
    new-instance v2, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LoadingViewHolder;

    invoke-direct {v2, v1}, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LoadingViewHolder;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 54
    .end local v1    # "view":Landroid/view/View;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setLoading(Z)V
    .locals 1
    .param p1, "loading"    # Z

    .prologue
    .line 144
    invoke-virtual {p0}, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->getItemCount()I

    move-result v0

    .line 146
    .local v0, "itemCount":I
    iput-boolean p1, p0, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->loading:Z

    .line 147
    if-eqz p1, :cond_0

    .line 148
    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->notifyItemInserted(I)V

    .line 150
    :cond_0
    return-void
.end method
