.class final Lcom/google/obf/cf;
.super Lcom/google/obf/cb;
.source "IMASDK"


# instance fields
.field private final b:Lcom/google/obf/dw;

.field private c:Z

.field private d:J

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(Lcom/google/obf/ar;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lcom/google/obf/cb;-><init>(Lcom/google/obf/ar;)V

    .line 2
    invoke-static {}, Lcom/google/obf/q;->a()Lcom/google/obf/q;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/obf/ar;->a(Lcom/google/obf/q;)V

    .line 3
    new-instance v0, Lcom/google/obf/dw;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/cf;->b:Lcom/google/obf/dw;

    .line 4
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/cf;->c:Z

    .line 6
    return-void
.end method

.method public a(JZ)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 7
    if-nez p3, :cond_0

    .line 13
    :goto_0
    return-void

    .line 9
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/cf;->c:Z

    .line 10
    iput-wide p1, p0, Lcom/google/obf/cf;->d:J

    .line 11
    iput v1, p0, Lcom/google/obf/cf;->e:I

    .line 12
    iput v1, p0, Lcom/google/obf/cf;->f:I

    goto :goto_0
.end method

.method public a(Lcom/google/obf/dw;)V
    .locals 8

    .prologue
    const/16 v7, 0xa

    const/4 v6, 0x0

    .line 14
    iget-boolean v0, p0, Lcom/google/obf/cf;->c:Z

    if-nez v0, :cond_0

    .line 32
    :goto_0
    return-void

    .line 16
    :cond_0
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v0

    .line 17
    iget v1, p0, Lcom/google/obf/cf;->f:I

    if-ge v1, v7, :cond_3

    .line 18
    iget v1, p0, Lcom/google/obf/cf;->f:I

    rsub-int/lit8 v1, v1, 0xa

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 19
    iget-object v2, p1, Lcom/google/obf/dw;->a:[B

    invoke-virtual {p1}, Lcom/google/obf/dw;->d()I

    move-result v3

    iget-object v4, p0, Lcom/google/obf/cf;->b:Lcom/google/obf/dw;

    iget-object v4, v4, Lcom/google/obf/dw;->a:[B

    iget v5, p0, Lcom/google/obf/cf;->f:I

    invoke-static {v2, v3, v4, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 20
    iget v2, p0, Lcom/google/obf/cf;->f:I

    add-int/2addr v1, v2

    if-ne v1, v7, :cond_3

    .line 21
    iget-object v1, p0, Lcom/google/obf/cf;->b:Lcom/google/obf/dw;

    invoke-virtual {v1, v6}, Lcom/google/obf/dw;->c(I)V

    .line 22
    const/16 v1, 0x49

    iget-object v2, p0, Lcom/google/obf/cf;->b:Lcom/google/obf/dw;

    invoke-virtual {v2}, Lcom/google/obf/dw;->f()I

    move-result v2

    if-ne v1, v2, :cond_1

    const/16 v1, 0x44

    iget-object v2, p0, Lcom/google/obf/cf;->b:Lcom/google/obf/dw;

    invoke-virtual {v2}, Lcom/google/obf/dw;->f()I

    move-result v2

    if-ne v1, v2, :cond_1

    const/16 v1, 0x33

    iget-object v2, p0, Lcom/google/obf/cf;->b:Lcom/google/obf/dw;

    .line 23
    invoke-virtual {v2}, Lcom/google/obf/dw;->f()I

    move-result v2

    if-eq v1, v2, :cond_2

    .line 24
    :cond_1
    const-string v0, "Id3Reader"

    const-string v1, "Discarding invalid ID3 tag"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    iput-boolean v6, p0, Lcom/google/obf/cf;->c:Z

    goto :goto_0

    .line 27
    :cond_2
    iget-object v1, p0, Lcom/google/obf/cf;->b:Lcom/google/obf/dw;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/obf/dw;->d(I)V

    .line 28
    iget-object v1, p0, Lcom/google/obf/cf;->b:Lcom/google/obf/dw;

    invoke-virtual {v1}, Lcom/google/obf/dw;->r()I

    move-result v1

    add-int/lit8 v1, v1, 0xa

    iput v1, p0, Lcom/google/obf/cf;->e:I

    .line 29
    :cond_3
    iget v1, p0, Lcom/google/obf/cf;->e:I

    iget v2, p0, Lcom/google/obf/cf;->f:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 30
    iget-object v1, p0, Lcom/google/obf/cf;->a:Lcom/google/obf/ar;

    invoke-interface {v1, p1, v0}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 31
    iget v1, p0, Lcom/google/obf/cf;->f:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/cf;->f:I

    goto :goto_0
.end method

.method public b()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 33
    iget-boolean v0, p0, Lcom/google/obf/cf;->c:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/obf/cf;->e:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/obf/cf;->f:I

    iget v1, p0, Lcom/google/obf/cf;->e:I

    if-eq v0, v1, :cond_1

    .line 37
    :cond_0
    :goto_0
    return-void

    .line 35
    :cond_1
    iget-object v1, p0, Lcom/google/obf/cf;->a:Lcom/google/obf/ar;

    iget-wide v2, p0, Lcom/google/obf/cf;->d:J

    const/4 v4, 0x1

    iget v5, p0, Lcom/google/obf/cf;->e:I

    const/4 v7, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/google/obf/ar;->a(JIII[B)V

    .line 36
    iput-boolean v6, p0, Lcom/google/obf/cf;->c:Z

    goto :goto_0
.end method
