.class Lru/cn/ad/video/yandex/YandexVideoAdapter$2;
.super Ljava/lang/Object;
.source "YandexVideoAdapter.java"

# interfaces
.implements Lcom/yandex/mobile/ads/video/RequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/ad/video/yandex/YandexVideoAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/yandex/mobile/ads/video/RequestListener",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/yandex/mobile/ads/video/models/ad/VideoAd;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;


# direct methods
.method constructor <init>(Lru/cn/ad/video/yandex/YandexVideoAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/ad/video/yandex/YandexVideoAdapter;

    .prologue
    .line 278
    iput-object p1, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lcom/yandex/mobile/ads/video/VideoAdError;)V
    .locals 3
    .param p1, "videoAdError"    # Lcom/yandex/mobile/ads/video/VideoAdError;

    .prologue
    .line 336
    const-string v0, "YandexVASTWrapper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "VideoAdRequestListener::onFailure: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/yandex/mobile/ads/video/VideoAdError;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    iget-object v0, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    invoke-static {v0, p1}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->access$100(Lru/cn/ad/video/yandex/YandexVideoAdapter;Lcom/yandex/mobile/ads/video/VideoAdError;)V

    .line 339
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 278
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;->onSuccess(Ljava/util/List;)V

    return-void
.end method

.method public onSuccess(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/yandex/mobile/ads/video/models/ad/VideoAd;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/yandex/mobile/ads/video/models/ad/VideoAd;>;"
    const/4 v9, 0x0

    .line 282
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    if-nez v7, :cond_1

    .line 283
    iget-object v7, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    invoke-static {v7}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->access$200(Lru/cn/ad/video/yandex/YandexVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 284
    iget-object v7, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    invoke-static {v7}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->access$300(Lru/cn/ad/video/yandex/YandexVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v7

    invoke-interface {v7}, Lru/cn/ad/AdAdapter$Listener;->onError()V

    .line 332
    :cond_0
    :goto_0
    return-void

    .line 288
    :cond_1
    iget-object v8, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/yandex/mobile/ads/video/models/ad/VideoAd;

    invoke-static {v8, v7}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->access$402(Lru/cn/ad/video/yandex/YandexVideoAdapter;Lcom/yandex/mobile/ads/video/models/ad/VideoAd;)Lcom/yandex/mobile/ads/video/models/ad/VideoAd;

    .line 289
    iget-object v8, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    iget-object v7, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    invoke-static {v7}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->access$400(Lru/cn/ad/video/yandex/YandexVideoAdapter;)Lcom/yandex/mobile/ads/video/models/ad/VideoAd;

    move-result-object v7

    invoke-virtual {v7}, Lcom/yandex/mobile/ads/video/models/ad/VideoAd;->getCreatives()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/yandex/mobile/ads/video/models/ad/Creative;

    invoke-static {v8, v7}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->access$502(Lru/cn/ad/video/yandex/YandexVideoAdapter;Lcom/yandex/mobile/ads/video/models/ad/Creative;)Lcom/yandex/mobile/ads/video/models/ad/Creative;

    .line 291
    iget-object v7, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    invoke-static {v7}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->access$500(Lru/cn/ad/video/yandex/YandexVideoAdapter;)Lcom/yandex/mobile/ads/video/models/ad/Creative;

    move-result-object v7

    invoke-virtual {v7}, Lcom/yandex/mobile/ads/video/models/ad/Creative;->getMediaFiles()Ljava/util/List;

    move-result-object v3

    .line 292
    .local v3, "mediaFiles":Ljava/util/List;, "Ljava/util/List<Lcom/yandex/mobile/ads/video/models/ad/MediaFile;>;"
    if-nez v3, :cond_2

    .line 293
    iget-object v7, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    invoke-static {v7}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->access$600(Lru/cn/ad/video/yandex/YandexVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 294
    iget-object v7, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    invoke-static {v7}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->access$700(Lru/cn/ad/video/yandex/YandexVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v7

    invoke-interface {v7}, Lru/cn/ad/AdAdapter$Listener;->onError()V

    goto :goto_0

    .line 299
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 300
    .end local v3    # "mediaFiles":Ljava/util/List;, "Ljava/util/List<Lcom/yandex/mobile/ads/video/models/ad/MediaFile;>;"
    .local v4, "mediaFiles":Ljava/util/List;, "Ljava/util/List<Lcom/yandex/mobile/ads/video/models/ad/MediaFile;>;"
    new-instance v1, Lru/cn/ad/video/yandex/YandexVideoAdapter$YMAMediaComparator;

    sget-object v7, Lru/cn/ad/video/VAST/MediaPredicate;->DEFAULT_MIME_TYPES:Ljava/util/List;

    iget-object v8, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    iget v8, v8, Lru/cn/ad/video/yandex/YandexVideoAdapter;->optimalVideoDimension:I

    invoke-direct {v1, v7, v8}, Lru/cn/ad/video/yandex/YandexVideoAdapter$YMAMediaComparator;-><init>(Ljava/util/List;I)V

    .line 301
    .local v1, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/yandex/mobile/ads/video/models/ad/MediaFile;>;"
    invoke-static {v4, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 303
    const/4 v6, 0x0

    .line 304
    .local v6, "supportedMediaFile":Lcom/yandex/mobile/ads/video/models/ad/MediaFile;
    new-instance v5, Lru/cn/ad/video/yandex/YandexVideoAdapter$YMAMediaPredicate;

    sget-object v7, Lru/cn/ad/video/VAST/MediaPredicate;->DEFAULT_MIME_TYPES:Ljava/util/List;

    iget-object v8, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    iget v8, v8, Lru/cn/ad/video/yandex/YandexVideoAdapter;->maxVideoDimension:I

    invoke-direct {v5, v7, v8}, Lru/cn/ad/video/yandex/YandexVideoAdapter$YMAMediaPredicate;-><init>(Ljava/util/List;I)V

    .line 305
    .local v5, "mediaPredicate":Lcom/android/internal/util/Predicate;, "Lcom/android/internal/util/Predicate<Lcom/yandex/mobile/ads/video/models/ad/MediaFile;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/yandex/mobile/ads/video/models/ad/MediaFile;

    .line 306
    .local v2, "mediaFile":Lcom/yandex/mobile/ads/video/models/ad/MediaFile;
    invoke-interface {v5, v2}, Lcom/android/internal/util/Predicate;->apply(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 307
    move-object v6, v2

    .line 313
    .end local v2    # "mediaFile":Lcom/yandex/mobile/ads/video/models/ad/MediaFile;
    :cond_4
    if-nez v6, :cond_5

    .line 314
    iget-object v7, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    iget-object v8, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    invoke-static {v8}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->access$800(Lru/cn/ad/video/yandex/YandexVideoAdapter;)Landroid/content/Context;

    move-result-object v8

    const/16 v9, 0x193

    invoke-virtual {v7, v8, v9}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->trackError(Landroid/content/Context;I)V

    .line 316
    iget-object v7, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    invoke-static {v7}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->access$900(Lru/cn/ad/video/yandex/YandexVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 317
    iget-object v7, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    invoke-static {v7}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->access$1000(Lru/cn/ad/video/yandex/YandexVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v7

    invoke-interface {v7}, Lru/cn/ad/AdAdapter$Listener;->onError()V

    goto/16 :goto_0

    .line 321
    :cond_5
    iget-object v7, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    invoke-virtual {v6}, Lcom/yandex/mobile/ads/video/models/ad/MediaFile;->getUri()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->access$1102(Lru/cn/ad/video/yandex/YandexVideoAdapter;Ljava/lang/String;)Ljava/lang/String;

    .line 323
    iget-object v7, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    invoke-static {v7}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->access$500(Lru/cn/ad/video/yandex/YandexVideoAdapter;)Lcom/yandex/mobile/ads/video/models/ad/Creative;

    move-result-object v7

    invoke-virtual {v7}, Lcom/yandex/mobile/ads/video/models/ad/Creative;->getClickThroughUrl()Ljava/lang/String;

    move-result-object v0

    .line 324
    .local v0, "adOwnerUri":Ljava/lang/String;
    if-eqz v0, :cond_6

    const-string v7, "http://"

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_6

    const-string v7, "https://"

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 325
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "http://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 328
    :cond_6
    iget-object v7, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    invoke-static {v7, v0}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->access$1202(Lru/cn/ad/video/yandex/YandexVideoAdapter;Ljava/lang/String;)Ljava/lang/String;

    .line 330
    iget-object v7, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    invoke-static {v7}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->access$1300(Lru/cn/ad/video/yandex/YandexVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 331
    iget-object v7, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$2;->this$0:Lru/cn/ad/video/yandex/YandexVideoAdapter;

    invoke-static {v7}, Lru/cn/ad/video/yandex/YandexVideoAdapter;->access$1400(Lru/cn/ad/video/yandex/YandexVideoAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v7

    invoke-interface {v7}, Lru/cn/ad/AdAdapter$Listener;->onAdLoaded()V

    goto/16 :goto_0
.end method
