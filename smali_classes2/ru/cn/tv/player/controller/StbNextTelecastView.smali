.class public Lru/cn/tv/player/controller/StbNextTelecastView;
.super Landroid/widget/FrameLayout;
.source "StbNextTelecastView.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/player/controller/StbNextTelecastView$Listener;
    }
.end annotation


# instance fields
.field private handler:Landroid/os/Handler;

.field private listener:Lru/cn/tv/player/controller/StbNextTelecastView$Listener;

.field private nextTelecastDateTime:Landroid/widget/TextView;

.field private nextTelecastImage:Landroid/widget/ImageView;

.field private nextTelecastImageWrapper:Landroid/view/ViewGroup;

.field private nextTelecastTimer:Landroid/widget/TextView;

.field private nextTelecastTimerCount:I

.field private nextTelecastTitle:Landroid/widget/TextView;

.field private telecastId:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lru/cn/tv/player/controller/StbNextTelecastView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    const/16 v1, 0xa

    iput v1, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->nextTelecastTimerCount:I

    .line 51
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->handler:Landroid/os/Handler;

    .line 52
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 53
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f0c0081

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 55
    const v1, 0x7f090147

    invoke-virtual {p0, v1}, Lru/cn/tv/player/controller/StbNextTelecastView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->nextTelecastImageWrapper:Landroid/view/ViewGroup;

    .line 56
    const v1, 0x7f090146

    invoke-virtual {p0, v1}, Lru/cn/tv/player/controller/StbNextTelecastView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->nextTelecastImage:Landroid/widget/ImageView;

    .line 57
    const v1, 0x7f090148

    invoke-virtual {p0, v1}, Lru/cn/tv/player/controller/StbNextTelecastView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->nextTelecastTimer:Landroid/widget/TextView;

    .line 58
    const v1, 0x7f090149

    invoke-virtual {p0, v1}, Lru/cn/tv/player/controller/StbNextTelecastView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->nextTelecastTitle:Landroid/widget/TextView;

    .line 59
    const v1, 0x7f090145

    invoke-virtual {p0, v1}, Lru/cn/tv/player/controller/StbNextTelecastView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->nextTelecastDateTime:Landroid/widget/TextView;

    .line 60
    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/player/controller/StbNextTelecastView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/controller/StbNextTelecastView;

    .prologue
    .line 25
    iget-object v0, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/tv/player/controller/StbNextTelecastView;)J
    .locals 2
    .param p0, "x0"    # Lru/cn/tv/player/controller/StbNextTelecastView;

    .prologue
    .line 25
    iget-wide v0, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->telecastId:J

    return-wide v0
.end method

.method static synthetic access$200(Lru/cn/tv/player/controller/StbNextTelecastView;)Lru/cn/tv/player/controller/StbNextTelecastView$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/controller/StbNextTelecastView;

    .prologue
    .line 25
    iget-object v0, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->listener:Lru/cn/tv/player/controller/StbNextTelecastView$Listener;

    return-object v0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 114
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 130
    :goto_0
    return v0

    .line 116
    :pswitch_0
    iget v2, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->nextTelecastTimerCount:I

    if-lez v2, :cond_0

    .line 117
    iget v2, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->nextTelecastTimerCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->nextTelecastTimerCount:I

    .line 118
    iget-object v2, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->nextTelecastTimer:Landroid/widget/TextView;

    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbNextTelecastView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e014a

    .line 119
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v0, [Ljava/lang/Object;

    iget v5, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->nextTelecastTimerCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    .line 118
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v1, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->handler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 122
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lru/cn/tv/player/controller/StbNextTelecastView;->setVisibility(I)V

    .line 124
    iget-object v1, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->listener:Lru/cn/tv/player/controller/StbNextTelecastView$Listener;

    iget-wide v2, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->telecastId:J

    invoke-interface {v1, v2, v3, v0}, Lru/cn/tv/player/controller/StbNextTelecastView$Listener;->playNextTelecast(JZ)V

    goto :goto_0

    .line 114
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public hide()V
    .locals 2

    .prologue
    .line 106
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbNextTelecastView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 107
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/StbNextTelecastView;->setVisibility(I)V

    .line 109
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 110
    return-void
.end method

.method public setListener(Lru/cn/tv/player/controller/StbNextTelecastView$Listener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/tv/player/controller/StbNextTelecastView$Listener;

    .prologue
    .line 63
    iput-object p1, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->listener:Lru/cn/tv/player/controller/StbNextTelecastView$Listener;

    .line 64
    return-void
.end method

.method public show(Lru/cn/api/tv/replies/Telecast;)V
    .locals 9
    .param p1, "telecast"    # Lru/cn/api/tv/replies/Telecast;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 67
    const/16 v3, 0xa

    iput v3, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->nextTelecastTimerCount:I

    .line 68
    if-eqz p1, :cond_0

    .line 69
    iget-wide v4, p1, Lru/cn/api/tv/replies/Telecast;->id:J

    iput-wide v4, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->telecastId:J

    .line 71
    sget-object v3, Lru/cn/api/tv/replies/TelecastImage$Profile;->MAIN:Lru/cn/api/tv/replies/TelecastImage$Profile;

    invoke-virtual {p1, v3}, Lru/cn/api/tv/replies/Telecast;->getImage(Lru/cn/api/tv/replies/TelecastImage$Profile;)Lru/cn/api/tv/replies/TelecastImage;

    move-result-object v1

    .line 72
    .local v1, "image":Lru/cn/api/tv/replies/TelecastImage;
    if-eqz v1, :cond_1

    iget-object v2, v1, Lru/cn/api/tv/replies/TelecastImage;->location:Ljava/lang/String;

    .line 73
    .local v2, "imageUri":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbNextTelecastView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v3

    .line 74
    invoke-virtual {v3, v2}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/picasso/RequestCreator;->fit()Lcom/squareup/picasso/RequestCreator;

    move-result-object v3

    const v4, 0x7f0802dc

    .line 75
    invoke-virtual {v3, v4}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v3

    iget-object v4, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->nextTelecastImage:Landroid/widget/ImageView;

    .line 76
    invoke-virtual {v3, v4}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 78
    iget-object v3, p1, Lru/cn/api/tv/replies/Telecast;->date:Lru/cn/api/tv/replies/DateTime;

    invoke-virtual {v3}, Lru/cn/api/tv/replies/DateTime;->toCalendar()Ljava/util/Calendar;

    move-result-object v3

    const-string v4, "dd MMMM, HH:mm "

    invoke-static {v3, v4}, Lru/cn/utils/Utils;->format(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 80
    .local v0, "dateText":Ljava/lang/String;
    iget-object v3, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->nextTelecastTimer:Landroid/widget/TextView;

    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbNextTelecastView;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0e014a

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    iget v6, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->nextTelecastTimerCount:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v3, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->nextTelecastTitle:Landroid/widget/TextView;

    iget-object v4, p1, Lru/cn/api/tv/replies/Telecast;->title:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v3, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->nextTelecastDateTime:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v3, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->handler:Landroid/os/Handler;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v3, v8, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 85
    iget-object v3, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->nextTelecastImageWrapper:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->requestFocus()Z

    .line 86
    iget-object v3, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->nextTelecastImageWrapper:Landroid/view/ViewGroup;

    new-instance v4, Lru/cn/tv/player/controller/StbNextTelecastView$1;

    invoke-direct {v4, p0}, Lru/cn/tv/player/controller/StbNextTelecastView$1;-><init>(Lru/cn/tv/player/controller/StbNextTelecastView;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    iget-object v3, p0, Lru/cn/tv/player/controller/StbNextTelecastView;->nextTelecastImageWrapper:Landroid/view/ViewGroup;

    new-instance v4, Lru/cn/tv/player/controller/StbNextTelecastView$2;

    invoke-direct {v4, p0}, Lru/cn/tv/player/controller/StbNextTelecastView$2;-><init>(Lru/cn/tv/player/controller/StbNextTelecastView;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 101
    invoke-virtual {p0, v7}, Lru/cn/tv/player/controller/StbNextTelecastView;->setVisibility(I)V

    .line 103
    .end local v0    # "dateText":Ljava/lang/String;
    .end local v1    # "image":Lru/cn/api/tv/replies/TelecastImage;
    .end local v2    # "imageUri":Ljava/lang/String;
    :cond_0
    return-void

    .line 72
    .restart local v1    # "image":Lru/cn/api/tv/replies/TelecastImage;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
