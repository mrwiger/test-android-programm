.class public Lru/cn/domain/stores/iabhelper/IabHelper;
.super Ljava/lang/Object;
.source "IabHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/domain/stores/iabhelper/IabHelper$QueryInventoryFinishedListener;,
        Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;,
        Lru/cn/domain/stores/iabhelper/IabHelper$OnIabSetupFinishedListener;
    }
.end annotation


# instance fields
.field mAsyncInProgress:Z

.field mAsyncOperation:Ljava/lang/String;

.field mContext:Landroid/content/Context;

.field mDebugLog:Z

.field mDebugTag:Ljava/lang/String;

.field mDisposed:Z

.field mPurchaseListener:Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;

.field mPurchasingItemType:Ljava/lang/String;

.field mRequestCode:I

.field mService:Lcom/android/vending/billing/IInAppBillingService;

.field mServiceConn:Landroid/content/ServiceConnection;

.field mSetupDone:Z

.field mSignatureBase64:Ljava/lang/String;

.field mSubscriptionsSupported:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "base64PublicKey"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-boolean v1, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mDebugLog:Z

    .line 75
    const-string v0, "IabHelper"

    iput-object v0, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mDebugTag:Ljava/lang/String;

    .line 78
    iput-boolean v1, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mSetupDone:Z

    .line 81
    iput-boolean v1, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mDisposed:Z

    .line 84
    iput-boolean v1, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mSubscriptionsSupported:Z

    .line 88
    iput-boolean v1, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mAsyncInProgress:Z

    .line 92
    const-string v0, ""

    iput-object v0, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mAsyncOperation:Ljava/lang/String;

    .line 108
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mSignatureBase64:Ljava/lang/String;

    .line 164
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mContext:Landroid/content/Context;

    .line 165
    iput-object p2, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mSignatureBase64:Ljava/lang/String;

    .line 166
    const-string v0, "IAB helper created."

    invoke-virtual {p0, v0}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 167
    return-void
.end method

.method private checkNotDisposed()V
    .locals 2

    .prologue
    .line 300
    iget-boolean v0, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mDisposed:Z

    if-eqz v0, :cond_0

    .line 301
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "IabHelper was disposed of, so it cannot be used."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 302
    :cond_0
    return-void
.end method

.method public static getResponseDesc(I)Ljava/lang/String;
    .locals 5
    .param p0, "code"    # I

    .prologue
    .line 745
    const-string v3, "0:OK/1:User Canceled/2:Unknown/3:Billing Unavailable/4:Item unavailable/5:Developer Error/6:Error/7:Item Already Owned/8:Item not owned"

    const-string v4, "/"

    .line 748
    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 749
    .local v0, "iab_msgs":[Ljava/lang/String;
    const-string v3, "0:OK/-1001:Remote exception during initialization/-1002:Bad response received/-1003:Purchase signature verification failed/-1004:Send intent failed/-1005:User cancelled/-1006:Unknown purchase response/-1007:Missing token/-1008:Unknown error/-1009:Subscriptions not available/-1010:Invalid consumption attempt"

    const-string v4, "/"

    .line 758
    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 760
    .local v1, "iabhelper_msgs":[Ljava/lang/String;
    const/16 v3, -0x3e8

    if-gt p0, v3, :cond_1

    .line 761
    rsub-int v2, p0, -0x3e8

    .line 762
    .local v2, "index":I
    if-ltz v2, :cond_0

    array-length v3, v1

    if-ge v2, v3, :cond_0

    aget-object v3, v1, v2

    .line 767
    .end local v2    # "index":I
    :goto_0
    return-object v3

    .line 763
    .restart local v2    # "index":I
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":Unknown IAB Helper Error"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 764
    .end local v2    # "index":I
    :cond_1
    if-ltz p0, :cond_2

    array-length v3, v0

    if-lt p0, v3, :cond_3

    .line 765
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":Unknown"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 767
    :cond_3
    aget-object v3, v0, p0

    goto :goto_0
.end method


# virtual methods
.method checkSetupDone(Ljava/lang/String;)V
    .locals 3
    .param p1, "operation"    # Ljava/lang/String;

    .prologue
    .line 773
    iget-boolean v0, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mSetupDone:Z

    if-nez v0, :cond_0

    .line 774
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Illegal state for operation ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "): IAB helper is not set up."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lru/cn/domain/stores/iabhelper/IabHelper;->logError(Ljava/lang/String;)V

    .line 775
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IAB helper is not set up. Can\'t perform operation: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 777
    :cond_0
    return-void
.end method

.method public dispose()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 286
    const-string v0, "Disposing."

    invoke-virtual {p0, v0}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 287
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mSetupDone:Z

    .line 288
    iget-object v0, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mServiceConn:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 289
    const-string v0, "Unbinding from service."

    invoke-virtual {p0, v0}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 290
    iget-object v0, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mServiceConn:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 292
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mDisposed:Z

    .line 293
    iput-object v2, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mContext:Landroid/content/Context;

    .line 294
    iput-object v2, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mServiceConn:Landroid/content/ServiceConnection;

    .line 295
    iput-object v2, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mService:Lcom/android/vending/billing/IInAppBillingService;

    .line 296
    iput-object v2, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mPurchaseListener:Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;

    .line 297
    return-void
.end method

.method flagEndAsync()V
    .locals 2

    .prologue
    .line 818
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Ending async operation: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mAsyncOperation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 819
    const-string v0, ""

    iput-object v0, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mAsyncOperation:Ljava/lang/String;

    .line 820
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mAsyncInProgress:Z

    .line 821
    return-void
.end method

.method flagStartAsync(Ljava/lang/String;)V
    .locals 3
    .param p1, "operation"    # Ljava/lang/String;

    .prologue
    .line 810
    iget-boolean v0, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mAsyncInProgress:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t start async operation ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") because another async operation("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mAsyncOperation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is in progress."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 812
    :cond_0
    iput-object p1, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mAsyncOperation:Ljava/lang/String;

    .line 813
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mAsyncInProgress:Z

    .line 814
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Starting async operation: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 815
    return-void
.end method

.method getResponseCodeFromBundle(Landroid/os/Bundle;)I
    .locals 4
    .param p1, "b"    # Landroid/os/Bundle;

    .prologue
    .line 781
    const-string v1, "RESPONSE_CODE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 782
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 783
    const-string v1, "Bundle with null response code, assuming OK (known issue)"

    invoke-virtual {p0, v1}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 784
    const/4 v1, 0x0

    .line 786
    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return v1

    .line 785
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Integer;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0

    .line 786
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_1
    instance-of v1, v0, Ljava/lang/Long;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/Long;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v1, v2

    goto :goto_0

    .line 788
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_2
    const-string v1, "Unexpected type for bundle response code."

    invoke-virtual {p0, v1}, Lru/cn/domain/stores/iabhelper/IabHelper;->logError(Ljava/lang/String;)V

    .line 789
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lru/cn/domain/stores/iabhelper/IabHelper;->logError(Ljava/lang/String;)V

    .line 790
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected type for bundle response code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method getResponseCodeFromIntent(Landroid/content/Intent;)I
    .locals 4
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 796
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "RESPONSE_CODE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 797
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 798
    const-string v1, "Intent with no response code, assuming OK (known issue)"

    invoke-virtual {p0, v1}, Lru/cn/domain/stores/iabhelper/IabHelper;->logError(Ljava/lang/String;)V

    .line 799
    const/4 v1, 0x0

    .line 801
    .end local v0    # "o":Ljava/lang/Object;
    :goto_0
    return v1

    .line 800
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Integer;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0

    .line 801
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_1
    instance-of v1, v0, Ljava/lang/Long;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/Long;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v1, v2

    goto :goto_0

    .line 803
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_2
    const-string v1, "Unexpected type for intent response code."

    invoke-virtual {p0, v1}, Lru/cn/domain/stores/iabhelper/IabHelper;->logError(Ljava/lang/String;)V

    .line 804
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lru/cn/domain/stores/iabhelper/IabHelper;->logError(Ljava/lang/String;)V

    .line 805
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected type for intent response code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public handleActivityResult(IILandroid/content/Intent;)Z
    .locals 12
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 438
    iget v8, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mRequestCode:I

    if-eq p1, v8, :cond_0

    const/4 v8, 0x0

    .line 516
    :goto_0
    return v8

    .line 440
    :cond_0
    invoke-direct {p0}, Lru/cn/domain/stores/iabhelper/IabHelper;->checkNotDisposed()V

    .line 441
    const-string v8, "handleActivityResult"

    invoke-virtual {p0, v8}, Lru/cn/domain/stores/iabhelper/IabHelper;->checkSetupDone(Ljava/lang/String;)V

    .line 444
    invoke-virtual {p0}, Lru/cn/domain/stores/iabhelper/IabHelper;->flagEndAsync()V

    .line 446
    if-nez p3, :cond_2

    .line 447
    const-string v8, "Null data in IAB activity result."

    invoke-virtual {p0, v8}, Lru/cn/domain/stores/iabhelper/IabHelper;->logError(Ljava/lang/String;)V

    .line 448
    new-instance v6, Lru/cn/domain/stores/iabhelper/IabResult;

    const/16 v8, -0x3ea

    const-string v9, "Null data in IAB result"

    invoke-direct {v6, v8, v9}, Lru/cn/domain/stores/iabhelper/IabResult;-><init>(ILjava/lang/String;)V

    .line 449
    .local v6, "result":Lru/cn/domain/stores/iabhelper/IabResult;
    iget-object v8, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mPurchaseListener:Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mPurchaseListener:Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;

    const/4 v9, 0x0

    invoke-interface {v8, v6, v9}, Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lru/cn/domain/stores/iabhelper/IabResult;Lru/cn/domain/stores/iabhelper/Purchase;)V

    .line 450
    :cond_1
    const/4 v8, 0x1

    goto :goto_0

    .line 453
    .end local v6    # "result":Lru/cn/domain/stores/iabhelper/IabResult;
    :cond_2
    invoke-virtual {p0, p3}, Lru/cn/domain/stores/iabhelper/IabHelper;->getResponseCodeFromIntent(Landroid/content/Intent;)I

    move-result v5

    .line 454
    .local v5, "responseCode":I
    const-string v8, "INAPP_PURCHASE_DATA"

    invoke-virtual {p3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 455
    .local v4, "purchaseData":Ljava/lang/String;
    const-string v8, "INAPP_DATA_SIGNATURE"

    invoke-virtual {p3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 457
    .local v0, "dataSignature":Ljava/lang/String;
    const/4 v8, -0x1

    if-ne p2, v8, :cond_a

    if-nez v5, :cond_a

    .line 458
    const-string v8, "Successful resultcode from purchase activity."

    invoke-virtual {p0, v8}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 459
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Purchase data: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 460
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Data signature: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 461
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Extras: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 462
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Expected item type: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mPurchasingItemType:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 464
    if-eqz v4, :cond_3

    if-nez v0, :cond_5

    .line 465
    :cond_3
    const-string v8, "BUG: either purchaseData or dataSignature is null."

    invoke-virtual {p0, v8}, Lru/cn/domain/stores/iabhelper/IabHelper;->logError(Ljava/lang/String;)V

    .line 466
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Extras: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v9

    invoke-virtual {v9}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 467
    new-instance v6, Lru/cn/domain/stores/iabhelper/IabResult;

    const/16 v8, -0x3f0

    const-string v9, "IAB returned null purchaseData or dataSignature"

    invoke-direct {v6, v8, v9}, Lru/cn/domain/stores/iabhelper/IabResult;-><init>(ILjava/lang/String;)V

    .line 468
    .restart local v6    # "result":Lru/cn/domain/stores/iabhelper/IabResult;
    iget-object v8, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mPurchaseListener:Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v8, :cond_4

    .line 469
    iget-object v8, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mPurchaseListener:Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;

    const/4 v9, 0x0

    invoke-interface {v8, v6, v9}, Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lru/cn/domain/stores/iabhelper/IabResult;Lru/cn/domain/stores/iabhelper/Purchase;)V

    .line 470
    :cond_4
    const/4 v8, 0x1

    goto/16 :goto_0

    .line 473
    .end local v6    # "result":Lru/cn/domain/stores/iabhelper/IabResult;
    :cond_5
    const/4 v2, 0x0

    .line 475
    .local v2, "purchase":Lru/cn/domain/stores/iabhelper/Purchase;
    :try_start_0
    new-instance v3, Lru/cn/domain/stores/iabhelper/Purchase;

    iget-object v8, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mPurchasingItemType:Ljava/lang/String;

    invoke-direct {v3, v8, v4, v0}, Lru/cn/domain/stores/iabhelper/Purchase;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 476
    .end local v2    # "purchase":Lru/cn/domain/stores/iabhelper/Purchase;
    .local v3, "purchase":Lru/cn/domain/stores/iabhelper/Purchase;
    :try_start_1
    invoke-virtual {v3}, Lru/cn/domain/stores/iabhelper/Purchase;->getSku()Ljava/lang/String;

    move-result-object v7

    .line 479
    .local v7, "sku":Ljava/lang/String;
    iget-object v8, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mSignatureBase64:Ljava/lang/String;

    invoke-static {v8, v4, v0}, Lru/cn/domain/stores/iabhelper/Security;->verifyPurchase(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 480
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Purchase signature verification FAILED for sku "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lru/cn/domain/stores/iabhelper/IabHelper;->logError(Ljava/lang/String;)V

    .line 481
    new-instance v6, Lru/cn/domain/stores/iabhelper/IabResult;

    const/16 v8, -0x3eb

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Signature verification failed for sku "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v8, v9}, Lru/cn/domain/stores/iabhelper/IabResult;-><init>(ILjava/lang/String;)V

    .line 482
    .restart local v6    # "result":Lru/cn/domain/stores/iabhelper/IabResult;
    iget-object v8, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mPurchaseListener:Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v8, :cond_6

    .line 483
    iget-object v8, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mPurchaseListener:Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;

    invoke-interface {v8, v6, v3}, Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lru/cn/domain/stores/iabhelper/IabResult;Lru/cn/domain/stores/iabhelper/Purchase;)V

    .line 484
    :cond_6
    const/4 v8, 0x1

    goto/16 :goto_0

    .line 486
    .end local v6    # "result":Lru/cn/domain/stores/iabhelper/IabResult;
    :cond_7
    const-string v8, "Purchase signature successfully verified."

    invoke-virtual {p0, v8}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 496
    iget-object v8, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mPurchaseListener:Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v8, :cond_8

    .line 497
    iget-object v8, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mPurchaseListener:Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;

    new-instance v9, Lru/cn/domain/stores/iabhelper/IabResult;

    const/4 v10, 0x0

    const-string v11, "Success"

    invoke-direct {v9, v10, v11}, Lru/cn/domain/stores/iabhelper/IabResult;-><init>(ILjava/lang/String;)V

    invoke-interface {v8, v9, v3}, Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lru/cn/domain/stores/iabhelper/IabResult;Lru/cn/domain/stores/iabhelper/Purchase;)V

    .line 516
    .end local v3    # "purchase":Lru/cn/domain/stores/iabhelper/Purchase;
    .end local v7    # "sku":Ljava/lang/String;
    :cond_8
    :goto_1
    const/4 v8, 0x1

    goto/16 :goto_0

    .line 487
    .restart local v2    # "purchase":Lru/cn/domain/stores/iabhelper/Purchase;
    :catch_0
    move-exception v1

    .line 488
    .local v1, "e":Lorg/json/JSONException;
    :goto_2
    const-string v8, "Failed to parse purchase data."

    invoke-virtual {p0, v8}, Lru/cn/domain/stores/iabhelper/IabHelper;->logError(Ljava/lang/String;)V

    .line 489
    invoke-static {v1}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .line 490
    new-instance v6, Lru/cn/domain/stores/iabhelper/IabResult;

    const/16 v8, -0x3ea

    const-string v9, "Failed to parse purchase data."

    invoke-direct {v6, v8, v9}, Lru/cn/domain/stores/iabhelper/IabResult;-><init>(ILjava/lang/String;)V

    .line 491
    .restart local v6    # "result":Lru/cn/domain/stores/iabhelper/IabResult;
    iget-object v8, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mPurchaseListener:Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v8, :cond_9

    .line 492
    iget-object v8, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mPurchaseListener:Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;

    const/4 v9, 0x0

    invoke-interface {v8, v6, v9}, Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lru/cn/domain/stores/iabhelper/IabResult;Lru/cn/domain/stores/iabhelper/Purchase;)V

    .line 493
    :cond_9
    const/4 v8, 0x1

    goto/16 :goto_0

    .line 499
    .end local v1    # "e":Lorg/json/JSONException;
    .end local v2    # "purchase":Lru/cn/domain/stores/iabhelper/Purchase;
    .end local v6    # "result":Lru/cn/domain/stores/iabhelper/IabResult;
    :cond_a
    const/4 v8, -0x1

    if-ne p2, v8, :cond_b

    .line 501
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Result code was OK but in-app billing response was not OK: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v5}, Lru/cn/domain/stores/iabhelper/IabHelper;->getResponseDesc(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 502
    iget-object v8, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mPurchaseListener:Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v8, :cond_8

    .line 503
    new-instance v6, Lru/cn/domain/stores/iabhelper/IabResult;

    const-string v8, "Problem purchashing item."

    invoke-direct {v6, v5, v8}, Lru/cn/domain/stores/iabhelper/IabResult;-><init>(ILjava/lang/String;)V

    .line 504
    .restart local v6    # "result":Lru/cn/domain/stores/iabhelper/IabResult;
    iget-object v8, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mPurchaseListener:Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;

    const/4 v9, 0x0

    invoke-interface {v8, v6, v9}, Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lru/cn/domain/stores/iabhelper/IabResult;Lru/cn/domain/stores/iabhelper/Purchase;)V

    goto :goto_1

    .line 506
    .end local v6    # "result":Lru/cn/domain/stores/iabhelper/IabResult;
    :cond_b
    if-nez p2, :cond_c

    .line 507
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Purchase canceled - Response: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v5}, Lru/cn/domain/stores/iabhelper/IabHelper;->getResponseDesc(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 508
    new-instance v6, Lru/cn/domain/stores/iabhelper/IabResult;

    const/16 v8, -0x3ed

    const-string v9, "User canceled."

    invoke-direct {v6, v8, v9}, Lru/cn/domain/stores/iabhelper/IabResult;-><init>(ILjava/lang/String;)V

    .line 509
    .restart local v6    # "result":Lru/cn/domain/stores/iabhelper/IabResult;
    iget-object v8, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mPurchaseListener:Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v8, :cond_8

    iget-object v8, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mPurchaseListener:Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;

    const/4 v9, 0x0

    invoke-interface {v8, v6, v9}, Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lru/cn/domain/stores/iabhelper/IabResult;Lru/cn/domain/stores/iabhelper/Purchase;)V

    goto :goto_1

    .line 511
    .end local v6    # "result":Lru/cn/domain/stores/iabhelper/IabResult;
    :cond_c
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Purchase failed. Result code: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ". Response: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 512
    invoke-static {v5}, Lru/cn/domain/stores/iabhelper/IabHelper;->getResponseDesc(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 511
    invoke-virtual {p0, v8}, Lru/cn/domain/stores/iabhelper/IabHelper;->logError(Ljava/lang/String;)V

    .line 513
    new-instance v6, Lru/cn/domain/stores/iabhelper/IabResult;

    const/16 v8, -0x3ee

    const-string v9, "Unknown purchase response."

    invoke-direct {v6, v8, v9}, Lru/cn/domain/stores/iabhelper/IabResult;-><init>(ILjava/lang/String;)V

    .line 514
    .restart local v6    # "result":Lru/cn/domain/stores/iabhelper/IabResult;
    iget-object v8, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mPurchaseListener:Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v8, :cond_8

    iget-object v8, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mPurchaseListener:Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;

    const/4 v9, 0x0

    invoke-interface {v8, v6, v9}, Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lru/cn/domain/stores/iabhelper/IabResult;Lru/cn/domain/stores/iabhelper/Purchase;)V

    goto/16 :goto_1

    .line 487
    .end local v6    # "result":Lru/cn/domain/stores/iabhelper/IabResult;
    .restart local v3    # "purchase":Lru/cn/domain/stores/iabhelper/Purchase;
    :catch_1
    move-exception v1

    move-object v2, v3

    .end local v3    # "purchase":Lru/cn/domain/stores/iabhelper/Purchase;
    .restart local v2    # "purchase":Lru/cn/domain/stores/iabhelper/Purchase;
    goto/16 :goto_2
.end method

.method public launchPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;ILru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)V
    .locals 14
    .param p1, "act"    # Landroid/app/Activity;
    .param p2, "sku"    # Ljava/lang/String;
    .param p3, "itemType"    # Ljava/lang/String;
    .param p4, "requestCode"    # I
    .param p5, "listener"    # Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;
    .param p6, "extraData"    # Ljava/lang/String;

    .prologue
    .line 372
    invoke-direct {p0}, Lru/cn/domain/stores/iabhelper/IabHelper;->checkNotDisposed()V

    .line 373
    const-string v1, "launchPurchaseFlow"

    invoke-virtual {p0, v1}, Lru/cn/domain/stores/iabhelper/IabHelper;->checkSetupDone(Ljava/lang/String;)V

    .line 374
    const-string v1, "launchPurchaseFlow"

    invoke-virtual {p0, v1}, Lru/cn/domain/stores/iabhelper/IabHelper;->flagStartAsync(Ljava/lang/String;)V

    .line 377
    const-string v1, "subs"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mSubscriptionsSupported:Z

    if-nez v1, :cond_1

    .line 378
    new-instance v11, Lru/cn/domain/stores/iabhelper/IabResult;

    const/16 v1, -0x3f1

    const-string v2, "Subscriptions are not available."

    invoke-direct {v11, v1, v2}, Lru/cn/domain/stores/iabhelper/IabResult;-><init>(ILjava/lang/String;)V

    .line 380
    .local v11, "r":Lru/cn/domain/stores/iabhelper/IabResult;
    invoke-virtual {p0}, Lru/cn/domain/stores/iabhelper/IabHelper;->flagEndAsync()V

    .line 381
    if-eqz p5, :cond_0

    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v11, v1}, Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lru/cn/domain/stores/iabhelper/IabResult;Lru/cn/domain/stores/iabhelper/Purchase;)V

    .line 421
    .end local v11    # "r":Lru/cn/domain/stores/iabhelper/IabResult;
    :cond_0
    :goto_0
    return-void

    .line 386
    :cond_1
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Constructing buy intent for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", item type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 387
    iget-object v1, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mService:Lcom/android/vending/billing/IInAppBillingService;

    const/4 v2, 0x3

    iget-object v3, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p6

    invoke-interface/range {v1 .. v6}, Lcom/android/vending/billing/IInAppBillingService;->getBuyIntent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v8

    .line 388
    .local v8, "buyIntentBundle":Landroid/os/Bundle;
    invoke-virtual {p0, v8}, Lru/cn/domain/stores/iabhelper/IabHelper;->getResponseCodeFromBundle(Landroid/os/Bundle;)I

    move-result v12

    .line 389
    .local v12, "response":I
    if-eqz v12, :cond_2

    .line 390
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to buy item, Error response: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v12}, Lru/cn/domain/stores/iabhelper/IabHelper;->getResponseDesc(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lru/cn/domain/stores/iabhelper/IabHelper;->logError(Ljava/lang/String;)V

    .line 391
    invoke-virtual {p0}, Lru/cn/domain/stores/iabhelper/IabHelper;->flagEndAsync()V

    .line 392
    new-instance v13, Lru/cn/domain/stores/iabhelper/IabResult;

    const-string v1, "Unable to buy item"

    invoke-direct {v13, v12, v1}, Lru/cn/domain/stores/iabhelper/IabResult;-><init>(ILjava/lang/String;)V

    .line 393
    .local v13, "result":Lru/cn/domain/stores/iabhelper/IabResult;
    if-eqz p5, :cond_0

    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v13, v1}, Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lru/cn/domain/stores/iabhelper/IabResult;Lru/cn/domain/stores/iabhelper/Purchase;)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 406
    .end local v8    # "buyIntentBundle":Landroid/os/Bundle;
    .end local v12    # "response":I
    .end local v13    # "result":Lru/cn/domain/stores/iabhelper/IabResult;
    :catch_0
    move-exception v9

    .line 407
    .local v9, "e":Landroid/content/IntentSender$SendIntentException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SendIntentException while launching purchase flow for sku "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lru/cn/domain/stores/iabhelper/IabHelper;->logError(Ljava/lang/String;)V

    .line 408
    invoke-static {v9}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .line 409
    invoke-virtual {p0}, Lru/cn/domain/stores/iabhelper/IabHelper;->flagEndAsync()V

    .line 411
    new-instance v13, Lru/cn/domain/stores/iabhelper/IabResult;

    const/16 v1, -0x3ec

    const-string v2, "Failed to send intent."

    invoke-direct {v13, v1, v2}, Lru/cn/domain/stores/iabhelper/IabResult;-><init>(ILjava/lang/String;)V

    .line 412
    .restart local v13    # "result":Lru/cn/domain/stores/iabhelper/IabResult;
    if-eqz p5, :cond_0

    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v13, v1}, Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lru/cn/domain/stores/iabhelper/IabResult;Lru/cn/domain/stores/iabhelper/Purchase;)V

    goto/16 :goto_0

    .line 397
    .end local v9    # "e":Landroid/content/IntentSender$SendIntentException;
    .end local v13    # "result":Lru/cn/domain/stores/iabhelper/IabResult;
    .restart local v8    # "buyIntentBundle":Landroid/os/Bundle;
    .restart local v12    # "response":I
    :cond_2
    :try_start_1
    const-string v1, "BUY_INTENT"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Landroid/app/PendingIntent;

    .line 398
    .local v10, "pendingIntent":Landroid/app/PendingIntent;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Launching buy intent for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Request code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 399
    move/from16 v0, p4

    iput v0, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mRequestCode:I

    .line 400
    move-object/from16 v0, p5

    iput-object v0, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mPurchaseListener:Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;

    .line 401
    move-object/from16 v0, p3

    iput-object v0, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mPurchasingItemType:Ljava/lang/String;

    .line 402
    invoke-virtual {v10}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v2

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const/4 v1, 0x0

    .line 404
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/4 v1, 0x0

    .line 405
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    move-object v1, p1

    move/from16 v3, p4

    .line 402
    invoke-virtual/range {v1 .. v7}, Landroid/app/Activity;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V
    :try_end_1
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 413
    .end local v8    # "buyIntentBundle":Landroid/os/Bundle;
    .end local v10    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v12    # "response":I
    :catch_1
    move-exception v9

    .line 414
    .local v9, "e":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RemoteException while launching purchase flow for sku "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lru/cn/domain/stores/iabhelper/IabHelper;->logError(Ljava/lang/String;)V

    .line 415
    invoke-static {v9}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .line 416
    invoke-virtual {p0}, Lru/cn/domain/stores/iabhelper/IabHelper;->flagEndAsync()V

    .line 418
    new-instance v13, Lru/cn/domain/stores/iabhelper/IabResult;

    const/16 v1, -0x3e9

    const-string v2, "Remote exception while starting purchase flow"

    invoke-direct {v13, v1, v2}, Lru/cn/domain/stores/iabhelper/IabResult;-><init>(ILjava/lang/String;)V

    .line 419
    .restart local v13    # "result":Lru/cn/domain/stores/iabhelper/IabResult;
    if-eqz p5, :cond_0

    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v13, v1}, Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lru/cn/domain/stores/iabhelper/IabResult;Lru/cn/domain/stores/iabhelper/Purchase;)V

    goto/16 :goto_0
.end method

.method public launchSubscriptionPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;)V
    .locals 6
    .param p1, "act"    # Landroid/app/Activity;
    .param p2, "sku"    # Ljava/lang/String;
    .param p3, "requestCode"    # I
    .param p4, "listener"    # Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;

    .prologue
    .line 344
    const-string v5, ""

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lru/cn/domain/stores/iabhelper/IabHelper;->launchSubscriptionPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)V

    .line 345
    return-void
.end method

.method public launchSubscriptionPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)V
    .locals 7
    .param p1, "act"    # Landroid/app/Activity;
    .param p2, "sku"    # Ljava/lang/String;
    .param p3, "requestCode"    # I
    .param p4, "listener"    # Lru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;
    .param p5, "extraData"    # Ljava/lang/String;

    .prologue
    .line 349
    const-string v3, "subs"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lru/cn/domain/stores/iabhelper/IabHelper;->launchPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;ILru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)V

    .line 350
    return-void
.end method

.method logDebug(Ljava/lang/String;)V
    .locals 1
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 969
    iget-boolean v0, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mDebugLog:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mDebugTag:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 970
    :cond_0
    return-void
.end method

.method logError(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 973
    iget-object v0, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mDebugTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "In-app billing error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 974
    return-void
.end method

.method logWarn(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 977
    iget-object v0, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mDebugTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "In-app billing warning: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 978
    return-void
.end method

.method public queryInventory(ZLjava/util/List;)Lru/cn/domain/stores/iabhelper/Inventory;
    .locals 1
    .param p1, "querySkuDetails"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lru/cn/domain/stores/iabhelper/Inventory;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lru/cn/domain/stores/iabhelper/IabException;
        }
    .end annotation

    .prologue
    .line 520
    .local p2, "moreSkus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lru/cn/domain/stores/iabhelper/IabHelper;->queryInventory(ZLjava/util/List;Ljava/util/List;)Lru/cn/domain/stores/iabhelper/Inventory;

    move-result-object v0

    return-object v0
.end method

.method public queryInventory(ZLjava/util/List;Ljava/util/List;)Lru/cn/domain/stores/iabhelper/Inventory;
    .locals 6
    .param p1, "querySkuDetails"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lru/cn/domain/stores/iabhelper/Inventory;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lru/cn/domain/stores/iabhelper/IabException;
        }
    .end annotation

    .prologue
    .line 538
    .local p2, "moreItemSkus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "moreSubsSkus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lru/cn/domain/stores/iabhelper/IabHelper;->checkNotDisposed()V

    .line 539
    const-string v3, "queryInventory"

    invoke-virtual {p0, v3}, Lru/cn/domain/stores/iabhelper/IabHelper;->checkSetupDone(Ljava/lang/String;)V

    .line 541
    :try_start_0
    new-instance v1, Lru/cn/domain/stores/iabhelper/Inventory;

    invoke-direct {v1}, Lru/cn/domain/stores/iabhelper/Inventory;-><init>()V

    .line 542
    .local v1, "inv":Lru/cn/domain/stores/iabhelper/Inventory;
    const-string v3, "inapp"

    invoke-virtual {p0, v1, v3}, Lru/cn/domain/stores/iabhelper/IabHelper;->queryPurchases(Lru/cn/domain/stores/iabhelper/Inventory;Ljava/lang/String;)I

    move-result v2

    .line 543
    .local v2, "r":I
    if-eqz v2, :cond_0

    .line 544
    new-instance v3, Lru/cn/domain/stores/iabhelper/IabException;

    const-string v4, "Error refreshing inventory (querying owned items)."

    invoke-direct {v3, v2, v4}, Lru/cn/domain/stores/iabhelper/IabException;-><init>(ILjava/lang/String;)V

    throw v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 570
    .end local v1    # "inv":Lru/cn/domain/stores/iabhelper/Inventory;
    .end local v2    # "r":I
    :catch_0
    move-exception v0

    .line 571
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v3, Lru/cn/domain/stores/iabhelper/IabException;

    const/16 v4, -0x3e9

    const-string v5, "Remote exception while refreshing inventory."

    invoke-direct {v3, v4, v5, v0}, Lru/cn/domain/stores/iabhelper/IabException;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    throw v3

    .line 547
    .end local v0    # "e":Landroid/os/RemoteException;
    .restart local v1    # "inv":Lru/cn/domain/stores/iabhelper/Inventory;
    .restart local v2    # "r":I
    :cond_0
    if-eqz p1, :cond_1

    .line 548
    :try_start_1
    const-string v3, "inapp"

    invoke-virtual {p0, v3, v1, p2}, Lru/cn/domain/stores/iabhelper/IabHelper;->querySkuDetails(Ljava/lang/String;Lru/cn/domain/stores/iabhelper/Inventory;Ljava/util/List;)I

    move-result v2

    .line 549
    if-eqz v2, :cond_1

    .line 550
    new-instance v3, Lru/cn/domain/stores/iabhelper/IabException;

    const-string v4, "Error refreshing inventory (querying prices of items)."

    invoke-direct {v3, v2, v4}, Lru/cn/domain/stores/iabhelper/IabException;-><init>(ILjava/lang/String;)V

    throw v3
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 572
    .end local v1    # "inv":Lru/cn/domain/stores/iabhelper/Inventory;
    .end local v2    # "r":I
    :catch_1
    move-exception v0

    .line 573
    .local v0, "e":Lorg/json/JSONException;
    new-instance v3, Lru/cn/domain/stores/iabhelper/IabException;

    const/16 v4, -0x3ea

    const-string v5, "Error parsing JSON response while refreshing inventory."

    invoke-direct {v3, v4, v5, v0}, Lru/cn/domain/stores/iabhelper/IabException;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    throw v3

    .line 555
    .end local v0    # "e":Lorg/json/JSONException;
    .restart local v1    # "inv":Lru/cn/domain/stores/iabhelper/Inventory;
    .restart local v2    # "r":I
    :cond_1
    :try_start_2
    iget-boolean v3, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mSubscriptionsSupported:Z

    if-eqz v3, :cond_3

    .line 556
    const-string v3, "subs"

    invoke-virtual {p0, v1, v3}, Lru/cn/domain/stores/iabhelper/IabHelper;->queryPurchases(Lru/cn/domain/stores/iabhelper/Inventory;Ljava/lang/String;)I

    move-result v2

    .line 557
    if-eqz v2, :cond_2

    .line 558
    new-instance v3, Lru/cn/domain/stores/iabhelper/IabException;

    const-string v4, "Error refreshing inventory (querying owned subscriptions)."

    invoke-direct {v3, v2, v4}, Lru/cn/domain/stores/iabhelper/IabException;-><init>(ILjava/lang/String;)V

    throw v3

    .line 561
    :cond_2
    if-eqz p1, :cond_3

    .line 562
    const-string v3, "subs"

    invoke-virtual {p0, v3, v1, p2}, Lru/cn/domain/stores/iabhelper/IabHelper;->querySkuDetails(Ljava/lang/String;Lru/cn/domain/stores/iabhelper/Inventory;Ljava/util/List;)I

    move-result v2

    .line 563
    if-eqz v2, :cond_3

    .line 564
    new-instance v3, Lru/cn/domain/stores/iabhelper/IabException;

    const-string v4, "Error refreshing inventory (querying prices of subscriptions)."

    invoke-direct {v3, v2, v4}, Lru/cn/domain/stores/iabhelper/IabException;-><init>(ILjava/lang/String;)V

    throw v3
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    .line 569
    :cond_3
    return-object v1
.end method

.method public queryInventoryAsync(Lru/cn/domain/stores/iabhelper/IabHelper$QueryInventoryFinishedListener;)V
    .locals 2
    .param p1, "listener"    # Lru/cn/domain/stores/iabhelper/IabHelper$QueryInventoryFinishedListener;

    .prologue
    .line 634
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p1}, Lru/cn/domain/stores/iabhelper/IabHelper;->queryInventoryAsync(ZLjava/util/List;Lru/cn/domain/stores/iabhelper/IabHelper$QueryInventoryFinishedListener;)V

    .line 635
    return-void
.end method

.method public queryInventoryAsync(ZLjava/util/List;Lru/cn/domain/stores/iabhelper/IabHelper$QueryInventoryFinishedListener;)V
    .locals 7
    .param p1, "querySkuDetails"    # Z
    .param p3, "listener"    # Lru/cn/domain/stores/iabhelper/IabHelper$QueryInventoryFinishedListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lru/cn/domain/stores/iabhelper/IabHelper$QueryInventoryFinishedListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 604
    .local p2, "moreSkus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    .line 605
    .local v5, "handler":Landroid/os/Handler;
    invoke-direct {p0}, Lru/cn/domain/stores/iabhelper/IabHelper;->checkNotDisposed()V

    .line 606
    const-string v0, "queryInventory"

    invoke-virtual {p0, v0}, Lru/cn/domain/stores/iabhelper/IabHelper;->checkSetupDone(Ljava/lang/String;)V

    .line 607
    const-string v0, "refresh inventory"

    invoke-virtual {p0, v0}, Lru/cn/domain/stores/iabhelper/IabHelper;->flagStartAsync(Ljava/lang/String;)V

    .line 608
    new-instance v6, Ljava/lang/Thread;

    new-instance v0, Lru/cn/domain/stores/iabhelper/IabHelper$2;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lru/cn/domain/stores/iabhelper/IabHelper$2;-><init>(Lru/cn/domain/stores/iabhelper/IabHelper;ZLjava/util/List;Lru/cn/domain/stores/iabhelper/IabHelper$QueryInventoryFinishedListener;Landroid/os/Handler;)V

    invoke-direct {v6, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 630
    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 631
    return-void
.end method

.method queryPurchases(Lru/cn/domain/stores/iabhelper/Inventory;Ljava/lang/String;)I
    .locals 16
    .param p1, "inv"    # Lru/cn/domain/stores/iabhelper/Inventory;
    .param p2, "itemType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 826
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Querying owned items, item type: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 827
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Package name: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lru/cn/domain/stores/iabhelper/IabHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 828
    const/4 v12, 0x0

    .line 829
    .local v12, "verificationFailed":Z
    const/4 v1, 0x0

    .line 832
    .local v1, "continueToken":Ljava/lang/String;
    :cond_0
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Calling getPurchases with continuation token: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 833
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/domain/stores/iabhelper/IabHelper;->mService:Lcom/android/vending/billing/IInAppBillingService;

    const/4 v14, 0x3

    move-object/from16 v0, p0

    iget-object v15, v0, Lru/cn/domain/stores/iabhelper/IabHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-interface {v13, v14, v15, v0, v1}, Lcom/android/vending/billing/IInAppBillingService;->getPurchases(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    .line 836
    .local v3, "ownedItems":Landroid/os/Bundle;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lru/cn/domain/stores/iabhelper/IabHelper;->getResponseCodeFromBundle(Landroid/os/Bundle;)I

    move-result v8

    .line 837
    .local v8, "response":I
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Owned items response: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 838
    if-eqz v8, :cond_1

    .line 839
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "getPurchases() failed: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v8}, Lru/cn/domain/stores/iabhelper/IabHelper;->getResponseDesc(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 883
    .end local v8    # "response":I
    :goto_0
    return v8

    .line 842
    .restart local v8    # "response":I
    :cond_1
    const-string v13, "INAPP_PURCHASE_ITEM_LIST"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_2

    const-string v13, "INAPP_PURCHASE_DATA_LIST"

    .line 843
    invoke-virtual {v3, v13}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_2

    const-string v13, "INAPP_DATA_SIGNATURE_LIST"

    .line 844
    invoke-virtual {v3, v13}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_3

    .line 845
    :cond_2
    const-string v13, "Bundle returned from getPurchases() doesn\'t contain required fields."

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lru/cn/domain/stores/iabhelper/IabHelper;->logError(Ljava/lang/String;)V

    .line 846
    const/16 v8, -0x3ea

    goto :goto_0

    .line 849
    :cond_3
    const-string v13, "INAPP_PURCHASE_ITEM_LIST"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 851
    .local v4, "ownedSkus":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v13, "INAPP_PURCHASE_DATA_LIST"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 853
    .local v7, "purchaseDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v13, "INAPP_DATA_SIGNATURE_LIST"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v10

    .line 856
    .local v10, "signatureList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-ge v2, v13, :cond_6

    .line 857
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 858
    .local v6, "purchaseData":Ljava/lang/String;
    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 859
    .local v9, "signature":Ljava/lang/String;
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 860
    .local v11, "sku":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/domain/stores/iabhelper/IabHelper;->mSignatureBase64:Ljava/lang/String;

    invoke-static {v13, v6, v9}, Lru/cn/domain/stores/iabhelper/Security;->verifyPurchase(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 861
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Sku is owned: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 862
    new-instance v5, Lru/cn/domain/stores/iabhelper/Purchase;

    move-object/from16 v0, p2

    invoke-direct {v5, v0, v6, v9}, Lru/cn/domain/stores/iabhelper/Purchase;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 864
    .local v5, "purchase":Lru/cn/domain/stores/iabhelper/Purchase;
    invoke-virtual {v5}, Lru/cn/domain/stores/iabhelper/Purchase;->getToken()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 865
    const-string v13, "BUG: empty/null token!"

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lru/cn/domain/stores/iabhelper/IabHelper;->logWarn(Ljava/lang/String;)V

    .line 866
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Purchase data: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 870
    :cond_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lru/cn/domain/stores/iabhelper/Inventory;->addPurchase(Lru/cn/domain/stores/iabhelper/Purchase;)V

    .line 856
    .end local v5    # "purchase":Lru/cn/domain/stores/iabhelper/Purchase;
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 872
    :cond_5
    const-string v13, "Purchase signature verification **FAILED**. Not adding item."

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lru/cn/domain/stores/iabhelper/IabHelper;->logWarn(Ljava/lang/String;)V

    .line 873
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "   Purchase data: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 874
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "   Signature: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 875
    const/4 v12, 0x1

    goto :goto_2

    .line 879
    .end local v6    # "purchaseData":Ljava/lang/String;
    .end local v9    # "signature":Ljava/lang/String;
    .end local v11    # "sku":Ljava/lang/String;
    :cond_6
    const-string v13, "INAPP_CONTINUATION_TOKEN"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 880
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Continuation token: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 881
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 883
    if-eqz v12, :cond_7

    const/16 v13, -0x3eb

    :goto_3
    move v8, v13

    goto/16 :goto_0

    :cond_7
    const/4 v13, 0x0

    goto :goto_3
.end method

.method querySkuDetails(Ljava/lang/String;Lru/cn/domain/stores/iabhelper/Inventory;Ljava/util/List;)I
    .locals 11
    .param p1, "itemType"    # Ljava/lang/String;
    .param p2, "inv"    # Lru/cn/domain/stores/iabhelper/Inventory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lru/cn/domain/stores/iabhelper/Inventory;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .local p3, "moreSkus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 888
    const-string v8, "Querying SKU details."

    invoke-virtual {p0, v8}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 889
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 890
    .local v6, "skuList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p2, p1}, Lru/cn/domain/stores/iabhelper/Inventory;->getAllOwnedSkus(Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 891
    if-eqz p3, :cond_1

    .line 892
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 893
    .local v4, "sku":Ljava/lang/String;
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 894
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 899
    .end local v4    # "sku":Ljava/lang/String;
    :cond_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-nez v8, :cond_3

    .line 900
    const-string v8, "queryPrices: nothing to do because there are no SKUs."

    invoke-virtual {p0, v8}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 928
    :cond_2
    :goto_1
    return v2

    .line 904
    :cond_3
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 905
    .local v1, "querySkus":Landroid/os/Bundle;
    const-string v8, "ITEM_ID_LIST"

    invoke-virtual {v1, v8, v6}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 906
    iget-object v8, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mService:Lcom/android/vending/billing/IInAppBillingService;

    const/4 v9, 0x3

    iget-object v10, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v8, v9, v10, p1, v1}, Lcom/android/vending/billing/IInAppBillingService;->getSkuDetails(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v5

    .line 909
    .local v5, "skuDetails":Landroid/os/Bundle;
    const-string v8, "DETAILS_LIST"

    invoke-virtual {v5, v8}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 910
    invoke-virtual {p0, v5}, Lru/cn/domain/stores/iabhelper/IabHelper;->getResponseCodeFromBundle(Landroid/os/Bundle;)I

    move-result v2

    .line 911
    .local v2, "response":I
    if-eqz v2, :cond_4

    .line 912
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getSkuDetails() failed: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v2}, Lru/cn/domain/stores/iabhelper/IabHelper;->getResponseDesc(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    goto :goto_1

    .line 915
    :cond_4
    const-string v8, "getSkuDetails() returned a bundle with neither an error nor a detail list."

    invoke-virtual {p0, v8}, Lru/cn/domain/stores/iabhelper/IabHelper;->logError(Ljava/lang/String;)V

    .line 916
    const/16 v2, -0x3ea

    goto :goto_1

    .line 920
    .end local v2    # "response":I
    :cond_5
    const-string v8, "DETAILS_LIST"

    invoke-virtual {v5, v8}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 923
    .local v3, "responseList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 924
    .local v7, "thisResponse":Ljava/lang/String;
    new-instance v0, Lru/cn/domain/stores/iabhelper/SkuDetails;

    invoke-direct {v0, p1, v7}, Lru/cn/domain/stores/iabhelper/SkuDetails;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 925
    .local v0, "d":Lru/cn/domain/stores/iabhelper/SkuDetails;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Got sku details: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 926
    invoke-virtual {p2, v0}, Lru/cn/domain/stores/iabhelper/Inventory;->addSkuDetails(Lru/cn/domain/stores/iabhelper/SkuDetails;)V

    goto :goto_2
.end method

.method public startSetup(Lru/cn/domain/stores/iabhelper/IabHelper$OnIabSetupFinishedListener;)V
    .locals 5
    .param p1, "listener"    # Lru/cn/domain/stores/iabhelper/IabHelper$OnIabSetupFinishedListener;

    .prologue
    .line 205
    invoke-direct {p0}, Lru/cn/domain/stores/iabhelper/IabHelper;->checkNotDisposed()V

    .line 206
    iget-boolean v2, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mSetupDone:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "IAB helper is already set up."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 209
    :cond_0
    const-string v2, "Starting in-app billing setup."

    invoke-virtual {p0, v2}, Lru/cn/domain/stores/iabhelper/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 210
    new-instance v2, Lru/cn/domain/stores/iabhelper/IabHelper$1;

    invoke-direct {v2, p0, p1}, Lru/cn/domain/stores/iabhelper/IabHelper$1;-><init>(Lru/cn/domain/stores/iabhelper/IabHelper;Lru/cn/domain/stores/iabhelper/IabHelper$OnIabSetupFinishedListener;)V

    iput-object v2, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mServiceConn:Landroid/content/ServiceConnection;

    .line 263
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.vending.billing.InAppBillingService.BIND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 264
    .local v1, "serviceIntent":Landroid/content/Intent;
    const-string v2, "com.android.vending"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 265
    iget-object v2, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 266
    .local v0, "intentServices":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 268
    iget-object v2, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lru/cn/domain/stores/iabhelper/IabHelper;->mServiceConn:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v2, v1, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 277
    :cond_1
    :goto_0
    return-void

    .line 271
    :cond_2
    if-eqz p1, :cond_1

    .line 272
    new-instance v2, Lru/cn/domain/stores/iabhelper/IabResult;

    const/4 v3, 0x3

    const-string v4, "Billing service unavailable on device."

    invoke-direct {v2, v3, v4}, Lru/cn/domain/stores/iabhelper/IabResult;-><init>(ILjava/lang/String;)V

    invoke-interface {p1, v2}, Lru/cn/domain/stores/iabhelper/IabHelper$OnIabSetupFinishedListener;->onIabSetupFinished(Lru/cn/domain/stores/iabhelper/IabResult;)V

    goto :goto_0
.end method
