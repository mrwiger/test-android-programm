.class public abstract Lcom/my/target/core/engines/c;
.super Ljava/lang/Object;
.source "InterstitialAdEngine.java"

# interfaces
.implements Lcom/my/target/br$a;
.implements Lcom/my/target/common/MyTargetActivity$ActivityEngine;


# instance fields
.field protected final a:Lcom/my/target/ads/InterstitialAd;

.field private b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/my/target/common/MyTargetActivity;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/my/target/br;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/my/target/ads/InterstitialAd;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/my/target/core/engines/c;->a:Lcom/my/target/ads/InterstitialAd;

    .line 60
    return-void
.end method

.method public static a(Lcom/my/target/ads/InterstitialAd;Lcom/my/target/core/models/banners/d;Lcom/my/target/dv;)Lcom/my/target/core/engines/c;
    .locals 1

    .prologue
    .line 38
    instance-of v0, p1, Lcom/my/target/core/models/banners/h;

    if-eqz v0, :cond_0

    .line 40
    check-cast p1, Lcom/my/target/core/models/banners/h;

    invoke-static {p0, p1, p2}, Lcom/my/target/core/engines/f;->a(Lcom/my/target/ads/InterstitialAd;Lcom/my/target/core/models/banners/h;Lcom/my/target/dv;)Lcom/my/target/core/engines/f;

    move-result-object v0

    .line 50
    :goto_0
    return-object v0

    .line 42
    :cond_0
    instance-of v0, p1, Lcom/my/target/core/models/banners/f;

    if-eqz v0, :cond_1

    .line 44
    check-cast p1, Lcom/my/target/core/models/banners/f;

    invoke-static {p0, p1, p2}, Lcom/my/target/core/engines/d;->a(Lcom/my/target/ads/InterstitialAd;Lcom/my/target/core/models/banners/f;Lcom/my/target/dv;)Lcom/my/target/core/engines/d;

    move-result-object v0

    goto :goto_0

    .line 46
    :cond_1
    instance-of v0, p1, Lcom/my/target/core/models/banners/g;

    if-eqz v0, :cond_2

    .line 48
    check-cast p1, Lcom/my/target/core/models/banners/g;

    invoke-static {p0, p1}, Lcom/my/target/core/engines/e;->a(Lcom/my/target/ads/InterstitialAd;Lcom/my/target/core/models/banners/g;)Lcom/my/target/core/engines/e;

    move-result-object v0

    goto :goto_0

    .line 50
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/my/target/br;Landroid/widget/FrameLayout;)V
    .locals 2
    .param p1, "dialog"    # Lcom/my/target/br;

    .prologue
    .line 173
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/my/target/core/engines/c;->c:Ljava/lang/ref/WeakReference;

    .line 174
    iget-object v0, p0, Lcom/my/target/core/engines/c;->a:Lcom/my/target/ads/InterstitialAd;

    invoke-virtual {v0}, Lcom/my/target/ads/InterstitialAd;->isHideStatusBarInDialog()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    invoke-virtual {p1}, Lcom/my/target/br;->aT()V

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/engines/c;->a:Lcom/my/target/ads/InterstitialAd;

    invoke-virtual {v0}, Lcom/my/target/ads/InterstitialAd;->getListener()Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;

    move-result-object v0

    .line 179
    if-eqz v0, :cond_1

    .line 181
    iget-object v1, p0, Lcom/my/target/core/engines/c;->a:Lcom/my/target/ads/InterstitialAd;

    invoke-interface {v0, v1}, Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;->onDisplay(Lcom/my/target/ads/InterstitialAd;)V

    .line 183
    :cond_1
    return-void
.end method

.method public aU()V
    .locals 2

    .prologue
    .line 188
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/core/engines/c;->c:Ljava/lang/ref/WeakReference;

    .line 189
    iget-object v0, p0, Lcom/my/target/core/engines/c;->a:Lcom/my/target/ads/InterstitialAd;

    invoke-virtual {v0}, Lcom/my/target/ads/InterstitialAd;->getListener()Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;

    move-result-object v0

    .line 190
    if-eqz v0, :cond_0

    .line 192
    iget-object v1, p0, Lcom/my/target/core/engines/c;->a:Lcom/my/target/ads/InterstitialAd;

    invoke-interface {v0, v1}, Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;->onDismiss(Lcom/my/target/ads/InterstitialAd;)V

    .line 194
    :cond_0
    return-void
.end method

.method public final dismiss()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 88
    iget-object v0, p0, Lcom/my/target/core/engines/c;->b:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 89
    :goto_0
    if-eqz v0, :cond_2

    .line 91
    invoke-virtual {v0}, Lcom/my/target/common/MyTargetActivity;->finish()V

    .line 100
    :cond_0
    :goto_1
    return-void

    .line 88
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/engines/c;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/common/MyTargetActivity;

    goto :goto_0

    .line 95
    :cond_2
    iget-object v0, p0, Lcom/my/target/core/engines/c;->c:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_3

    move-object v0, v1

    .line 96
    :goto_2
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/my/target/br;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    invoke-virtual {v0}, Lcom/my/target/br;->dismiss()V

    goto :goto_1

    .line 95
    :cond_3
    iget-object v0, p0, Lcom/my/target/core/engines/c;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/br;

    goto :goto_2
.end method

.method public i(Z)V
    .locals 0

    .prologue
    .line 200
    return-void
.end method

.method public onActivityBackPressed()Z
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x1

    return v0
.end method

.method public onActivityCreate(Lcom/my/target/common/MyTargetActivity;Landroid/content/Intent;Landroid/widget/FrameLayout;)V
    .locals 2
    .param p1, "activity"    # Lcom/my/target/common/MyTargetActivity;

    .prologue
    const/16 v1, 0x400

    .line 112
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/my/target/core/engines/c;->b:Ljava/lang/ref/WeakReference;

    .line 113
    const v0, 0x1030006

    invoke-virtual {p1, v0}, Lcom/my/target/common/MyTargetActivity;->setTheme(I)V

    .line 114
    invoke-virtual {p1}, Lcom/my/target/common/MyTargetActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 116
    iget-object v0, p0, Lcom/my/target/core/engines/c;->a:Lcom/my/target/ads/InterstitialAd;

    invoke-virtual {v0}, Lcom/my/target/ads/InterstitialAd;->getListener()Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;

    move-result-object v0

    .line 117
    if-eqz v0, :cond_0

    .line 119
    iget-object v1, p0, Lcom/my/target/core/engines/c;->a:Lcom/my/target/ads/InterstitialAd;

    invoke-interface {v0, v1}, Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;->onDisplay(Lcom/my/target/ads/InterstitialAd;)V

    .line 121
    :cond_0
    return-void
.end method

.method public onActivityDestroy()V
    .locals 2

    .prologue
    .line 150
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/core/engines/c;->b:Ljava/lang/ref/WeakReference;

    .line 151
    iget-object v0, p0, Lcom/my/target/core/engines/c;->a:Lcom/my/target/ads/InterstitialAd;

    invoke-virtual {v0}, Lcom/my/target/ads/InterstitialAd;->getListener()Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;

    move-result-object v0

    .line 152
    if-eqz v0, :cond_0

    .line 154
    iget-object v1, p0, Lcom/my/target/core/engines/c;->a:Lcom/my/target/ads/InterstitialAd;

    invoke-interface {v0, v1}, Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;->onDismiss(Lcom/my/target/ads/InterstitialAd;)V

    .line 156
    :cond_0
    return-void
.end method

.method public onActivityOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    return v0
.end method

.method public onActivityPause()V
    .locals 0

    .prologue
    .line 139
    return-void
.end method

.method public onActivityResume()V
    .locals 0

    .prologue
    .line 145
    return-void
.end method

.method public onActivityStart()V
    .locals 0

    .prologue
    .line 127
    return-void
.end method

.method public onActivityStop()V
    .locals 0

    .prologue
    .line 133
    return-void
.end method

.method public final showDialog(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/my/target/core/engines/c;->c:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 76
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/my/target/br;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    const-string v0, "InterstitialAdEngine.showDialog: dialog already showing"

    invoke-static {v0}, Lcom/my/target/g;->c(Ljava/lang/String;)V

    .line 84
    :goto_1
    return-void

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/engines/c;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/br;

    goto :goto_0

    .line 82
    :cond_1
    invoke-static {p0, p1}, Lcom/my/target/br;->a(Lcom/my/target/br$a;Landroid/content/Context;)Lcom/my/target/br;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Lcom/my/target/br;->show()V

    goto :goto_1
.end method
