.class public final Lcom/my/target/core/controllers/b;
.super Ljava/lang/Object;
.source "NativeAdViewController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/core/controllers/b$a;
    }
.end annotation


# instance fields
.field private A:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private B:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/my/target/da;",
            ">;"
        }
    .end annotation
.end field

.field private C:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation
.end field

.field private D:Lcom/my/target/core/controllers/a;

.field private E:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/my/target/nativeads/views/MediaAdView;",
            ">;"
        }
    .end annotation
.end field

.field private F:Z

.field private G:Landroid/os/Parcelable;

.field private final v:Z

.field private final w:Z

.field private final x:Lcom/my/target/core/controllers/b$a;

.field private final y:Lcom/my/target/core/models/banners/a;

.field private z:I


# direct methods
.method private constructor <init>(Lcom/my/target/core/models/banners/a;Lcom/my/target/core/controllers/b$a;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput v2, p0, Lcom/my/target/core/controllers/b;->z:I

    .line 56
    iput-object p2, p0, Lcom/my/target/core/controllers/b;->x:Lcom/my/target/core/controllers/b$a;

    .line 57
    iput-object p1, p0, Lcom/my/target/core/controllers/b;->y:Lcom/my/target/core/models/banners/a;

    .line 58
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getNativeAdCards()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/my/target/core/controllers/b;->v:Z

    .line 59
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getVideoBanner()Lcom/my/target/aj;

    move-result-object v0

    .line 60
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/my/target/aj;->getMediaData()Lcom/my/target/ag;

    move-result-object v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/my/target/core/controllers/b;->w:Z

    .line 61
    return-void

    :cond_0
    move v0, v2

    .line 58
    goto :goto_0

    :cond_1
    move v1, v2

    .line 60
    goto :goto_1
.end method

.method public static a(Lcom/my/target/core/models/banners/a;Lcom/my/target/core/controllers/b$a;)Lcom/my/target/core/controllers/b;
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/my/target/core/controllers/b;

    invoke-direct {v0, p0, p1}, Lcom/my/target/core/controllers/b;-><init>(Lcom/my/target/core/models/banners/a;Lcom/my/target/core/controllers/b$a;)V

    return-object v0
.end method

.method private a(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 267
    instance-of v0, p1, Lcom/my/target/nativeads/views/PromoCardRecyclerView;

    if-eqz v0, :cond_1

    .line 269
    check-cast p1, Lcom/my/target/nativeads/views/PromoCardRecyclerView;

    invoke-direct {p0, p1}, Lcom/my/target/core/controllers/b;->a(Lcom/my/target/da;)V

    .line 290
    :cond_0
    :goto_0
    return-void

    .line 271
    :cond_1
    instance-of v0, p1, Lcom/my/target/nativeads/views/MediaAdView;

    if-eqz v0, :cond_2

    .line 273
    check-cast p1, Lcom/my/target/nativeads/views/MediaAdView;

    invoke-direct {p0, p1}, Lcom/my/target/core/controllers/b;->b(Lcom/my/target/nativeads/views/MediaAdView;)V

    goto :goto_0

    .line 277
    :cond_2
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->C:Ljava/util/HashSet;

    if-nez v0, :cond_3

    .line 279
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->x:Lcom/my/target/core/controllers/b$a;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 281
    :cond_3
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 283
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 284
    if-eqz v1, :cond_4

    .line 286
    invoke-direct {p0, v1}, Lcom/my/target/core/controllers/b;->b(Landroid/view/View;)V

    .line 281
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private a(Lcom/my/target/core/models/banners/a;Lcom/my/target/nativeads/views/MediaAdView;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 365
    invoke-virtual {p2}, Lcom/my/target/nativeads/views/MediaAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 366
    invoke-static {p2}, Lcom/my/target/core/controllers/b;->c(Lcom/my/target/nativeads/views/MediaAdView;)Lcom/my/target/cz;

    move-result-object v0

    .line 368
    if-nez v0, :cond_0

    .line 371
    new-instance v0, Lcom/my/target/cz;

    invoke-direct {v0, v1}, Lcom/my/target/cz;-><init>(Landroid/content/Context;)V

    .line 372
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p2, v0, v1}, Lcom/my/target/nativeads/views/MediaAdView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 376
    :cond_0
    iget-object v1, p0, Lcom/my/target/core/controllers/b;->C:Ljava/util/HashSet;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/my/target/core/controllers/b;->F:Z

    if-eqz v1, :cond_2

    :cond_1
    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 377
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getNativeAdCards()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/cz;->setupCards(Ljava/util/List;)V

    .line 378
    iget-object v1, p0, Lcom/my/target/core/controllers/b;->x:Lcom/my/target/core/controllers/b$a;

    invoke-virtual {v0, v1}, Lcom/my/target/cz;->setPromoCardSliderListener(Lcom/my/target/da$a;)V

    .line 379
    invoke-virtual {p2, v2}, Lcom/my/target/nativeads/views/MediaAdView;->setBackgroundColor(I)V

    .line 380
    invoke-virtual {v0, v2}, Lcom/my/target/cz;->setVisibility(I)V

    .line 381
    return-void

    :cond_2
    move v1, v2

    .line 376
    goto :goto_0
.end method

.method private a(Lcom/my/target/da;)V
    .locals 1

    .prologue
    .line 294
    const/4 v0, 0x2

    iput v0, p0, Lcom/my/target/core/controllers/b;->z:I

    .line 295
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->x:Lcom/my/target/core/controllers/b$a;

    invoke-interface {p1, v0}, Lcom/my/target/da;->setPromoCardSliderListener(Lcom/my/target/da$a;)V

    .line 296
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->G:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->G:Landroid/os/Parcelable;

    invoke-interface {p1, v0}, Lcom/my/target/da;->restoreState(Landroid/os/Parcelable;)V

    .line 300
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/my/target/core/controllers/b;->B:Ljava/lang/ref/WeakReference;

    .line 301
    return-void
.end method

.method private static a(Lcom/my/target/nativeads/views/MediaAdView;Lcom/my/target/common/models/ImageData;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 398
    invoke-virtual {p0}, Lcom/my/target/nativeads/views/MediaAdView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    check-cast v0, Lcom/my/target/bv;

    .line 400
    if-eqz p1, :cond_1

    .line 402
    invoke-virtual {p1}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 403
    if-eqz v1, :cond_0

    .line 405
    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 417
    :goto_0
    return-void

    .line 409
    :cond_0
    invoke-virtual {v0, v2}, Lcom/my/target/bv;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 410
    invoke-static {p1, v0}, Lcom/my/target/ch;->a(Lcom/my/target/common/models/ImageData;Landroid/widget/ImageView;)V

    goto :goto_0

    .line 415
    :cond_1
    invoke-virtual {v0, v2}, Lcom/my/target/bv;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private a(Lcom/my/target/nativeads/views/MediaAdView;Lcom/my/target/core/controllers/a$c;)V
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->D:Lcom/my/target/core/controllers/a;

    if-eqz v0, :cond_0

    .line 424
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->D:Lcom/my/target/core/controllers/a;

    invoke-virtual {v0, p2}, Lcom/my/target/core/controllers/a;->a(Lcom/my/target/core/controllers/a$c;)V

    .line 425
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->D:Lcom/my/target/core/controllers/a;

    invoke-direct {p0, p1, v0}, Lcom/my/target/core/controllers/b;->a(Lcom/my/target/nativeads/views/MediaAdView;Lcom/my/target/core/controllers/a;)V

    .line 427
    :cond_0
    return-void
.end method

.method private a(Lcom/my/target/nativeads/views/MediaAdView;Lcom/my/target/core/controllers/a;)V
    .locals 1

    .prologue
    .line 431
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->x:Lcom/my/target/core/controllers/b$a;

    invoke-virtual {p2, v0}, Lcom/my/target/core/controllers/a;->a(Landroid/view/View$OnClickListener;)V

    .line 432
    invoke-virtual {p2, p1}, Lcom/my/target/core/controllers/a;->a(Lcom/my/target/nativeads/views/MediaAdView;)V

    .line 433
    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 252
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 254
    check-cast p1, Landroid/view/ViewGroup;

    invoke-direct {p0, p1}, Lcom/my/target/core/controllers/b;->a(Landroid/view/ViewGroup;)V

    .line 263
    :cond_0
    :goto_0
    return-void

    .line 258
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->C:Ljava/util/HashSet;

    if-nez v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->x:Lcom/my/target/core/controllers/b$a;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private b(Lcom/my/target/nativeads/views/MediaAdView;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 305
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/my/target/core/controllers/b;->E:Ljava/lang/ref/WeakReference;

    .line 306
    iget-object v1, p0, Lcom/my/target/core/controllers/b;->y:Lcom/my/target/core/models/banners/a;

    invoke-virtual {v1}, Lcom/my/target/core/models/banners/a;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v2

    .line 309
    if-eqz v2, :cond_8

    .line 311
    invoke-virtual {v2}, Lcom/my/target/common/models/ImageData;->getWidth()I

    move-result v1

    .line 312
    invoke-virtual {v2}, Lcom/my/target/common/models/ImageData;->getHeight()I

    move-result v0

    .line 313
    invoke-virtual {p1, v1, v0}, Lcom/my/target/nativeads/views/MediaAdView;->setPlaceHolderDimension(II)V

    .line 316
    :goto_0
    iget-boolean v3, p0, Lcom/my/target/core/controllers/b;->v:Z

    if-eqz v3, :cond_1

    .line 318
    iget v0, p0, Lcom/my/target/core/controllers/b;->z:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 320
    const/4 v0, 0x3

    iput v0, p0, Lcom/my/target/core/controllers/b;->z:I

    .line 321
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->y:Lcom/my/target/core/models/banners/a;

    invoke-direct {p0, v0, p1}, Lcom/my/target/core/controllers/b;->a(Lcom/my/target/core/models/banners/a;Lcom/my/target/nativeads/views/MediaAdView;)V

    .line 361
    :cond_0
    :goto_1
    return-void

    .line 326
    :cond_1
    invoke-static {p1, v2}, Lcom/my/target/core/controllers/b;->a(Lcom/my/target/nativeads/views/MediaAdView;Lcom/my/target/common/models/ImageData;)V

    .line 328
    iget-boolean v2, p0, Lcom/my/target/core/controllers/b;->w:Z

    if-eqz v2, :cond_5

    .line 330
    iget-object v2, p0, Lcom/my/target/core/controllers/b;->y:Lcom/my/target/core/models/banners/a;

    invoke-virtual {v2}, Lcom/my/target/core/models/banners/a;->getVideoBanner()Lcom/my/target/aj;

    move-result-object v3

    .line 331
    const/4 v2, 0x0

    .line 332
    if-eqz v3, :cond_7

    .line 334
    if-eqz v0, :cond_2

    if-nez v1, :cond_3

    .line 336
    :cond_2
    invoke-virtual {v3}, Lcom/my/target/aj;->getWidth()I

    move-result v0

    invoke-virtual {v3}, Lcom/my/target/aj;->getHeight()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/my/target/nativeads/views/MediaAdView;->setPlaceHolderDimension(II)V

    .line 338
    :cond_3
    invoke-virtual {v3}, Lcom/my/target/aj;->getMediaData()Lcom/my/target/ag;

    move-result-object v0

    check-cast v0, Lcom/my/target/common/models/VideoData;

    .line 340
    :goto_2
    iget-object v1, p0, Lcom/my/target/core/controllers/b;->D:Lcom/my/target/core/controllers/a;

    if-nez v1, :cond_4

    .line 342
    if-eqz v0, :cond_4

    .line 344
    const/4 v1, 0x1

    iput v1, p0, Lcom/my/target/core/controllers/b;->z:I

    .line 345
    new-instance v1, Lcom/my/target/core/controllers/a;

    iget-object v2, p0, Lcom/my/target/core/controllers/b;->y:Lcom/my/target/core/models/banners/a;

    invoke-direct {v1, v2, v3, v0}, Lcom/my/target/core/controllers/a;-><init>(Lcom/my/target/core/models/banners/a;Lcom/my/target/aj;Lcom/my/target/common/models/VideoData;)V

    iput-object v1, p0, Lcom/my/target/core/controllers/b;->D:Lcom/my/target/core/controllers/a;

    .line 348
    :cond_4
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->D:Lcom/my/target/core/controllers/a;

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->x:Lcom/my/target/core/controllers/b$a;

    invoke-direct {p0, p1, v0}, Lcom/my/target/core/controllers/b;->a(Lcom/my/target/nativeads/views/MediaAdView;Lcom/my/target/core/controllers/a$c;)V

    goto :goto_1

    .line 355
    :cond_5
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->C:Ljava/util/HashSet;

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/my/target/core/controllers/b;->F:Z

    if-eqz v0, :cond_0

    .line 357
    :cond_6
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->x:Lcom/my/target/core/controllers/b$a;

    invoke-virtual {p1, v0}, Lcom/my/target/nativeads/views/MediaAdView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_7
    move-object v0, v2

    goto :goto_2

    :cond_8
    move v1, v0

    goto :goto_0
.end method

.method private static c(Lcom/my/target/nativeads/views/MediaAdView;)Lcom/my/target/cz;
    .locals 3

    .prologue
    .line 385
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/my/target/nativeads/views/MediaAdView;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 387
    invoke-virtual {p0, v1}, Lcom/my/target/nativeads/views/MediaAdView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 388
    instance-of v2, v0, Lcom/my/target/cz;

    if-eqz v2, :cond_0

    .line 390
    check-cast v0, Lcom/my/target/cz;

    .line 393
    :goto_1
    return-object v0

    .line 385
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 393
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private c(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 437
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    .line 439
    instance-of v0, p1, Landroid/support/v7/widget/RecyclerView;

    if-nez v0, :cond_2

    instance-of v0, p1, Lcom/my/target/nativeads/views/MediaAdView;

    if-nez v0, :cond_2

    .line 441
    check-cast p1, Landroid/view/ViewGroup;

    .line 442
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 444
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 445
    if-eqz v1, :cond_0

    .line 447
    invoke-direct {p0, v1}, Lcom/my/target/core/controllers/b;->c(Landroid/view/View;)V

    .line 442
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 450
    :cond_1
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 457
    :cond_2
    :goto_1
    return-void

    .line 455
    :cond_3
    invoke-virtual {p1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method


# virtual methods
.method public final b(Z)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->D:Lcom/my/target/core/controllers/a;

    if-eqz v0, :cond_0

    .line 85
    if-eqz p1, :cond_1

    .line 87
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->D:Lcom/my/target/core/controllers/a;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/a;->g()V

    .line 94
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->D:Lcom/my/target/core/controllers/a;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/a;->i()V

    goto :goto_0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->A:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->A:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 68
    if-eqz v0, :cond_0

    .line 70
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 73
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/my/target/core/controllers/b;->z:I

    return v0
.end method

.method public final r()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 98
    const/4 v0, 0x0

    .line 99
    iget-object v2, p0, Lcom/my/target/core/controllers/b;->A:Ljava/lang/ref/WeakReference;

    if-eqz v2, :cond_0

    .line 101
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->A:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 103
    :cond_0
    if-nez v0, :cond_1

    .line 105
    const/4 v0, -0x1

    .line 126
    :goto_0
    return v0

    .line 107
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    .line 108
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 109
    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v2

    const/high16 v3, 0x3f000000    # 0.5f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_3

    :cond_2
    move v0, v1

    .line 111
    goto :goto_0

    .line 114
    :cond_3
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 116
    invoke-virtual {v0, v2}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 118
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    mul-int/2addr v2, v3

    int-to-double v2, v2

    .line 119
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    mul-int/2addr v0, v4

    int-to-double v4, v0

    .line 121
    const-wide v6, 0x3fe3333340000000L    # 0.6000000238418579

    mul-double/2addr v4, v6

    cmpl-double v0, v2, v4

    if-ltz v0, :cond_4

    .line 123
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 126
    goto :goto_0
.end method

.method public final registerView(Landroid/view/View;Ljava/util/List;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 231
    .local p2, "clickableViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    if-eqz p2, :cond_1

    .line 233
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/my/target/core/controllers/b;->C:Ljava/util/HashSet;

    .line 235
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 237
    iget-object v2, p0, Lcom/my/target/core/controllers/b;->C:Ljava/util/HashSet;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 238
    instance-of v2, v0, Lcom/my/target/nativeads/views/MediaAdView;

    if-eqz v2, :cond_0

    .line 240
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/core/controllers/b;->F:Z

    goto :goto_0

    .line 243
    :cond_0
    iget-object v2, p0, Lcom/my/target/core/controllers/b;->x:Lcom/my/target/core/controllers/b$a;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 246
    :cond_1
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/my/target/core/controllers/b;->A:Ljava/lang/ref/WeakReference;

    .line 247
    invoke-direct {p0, p1}, Lcom/my/target/core/controllers/b;->b(Landroid/view/View;)V

    .line 248
    return-void
.end method

.method public final s()V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->D:Lcom/my/target/core/controllers/a;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->D:Lcom/my/target/core/controllers/a;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/a;->unregister()V

    .line 135
    :cond_0
    return-void
.end method

.method public final t()[I
    .locals 2

    .prologue
    .line 198
    iget v0, p0, Lcom/my/target/core/controllers/b;->z:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 200
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->B:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    .line 202
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->B:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/da;

    .line 203
    if-eqz v0, :cond_1

    .line 205
    invoke-interface {v0}, Lcom/my/target/da;->getVisibleCardNumbers()[I

    move-result-object v0

    .line 226
    :goto_0
    return-object v0

    .line 210
    :cond_0
    iget v0, p0, Lcom/my/target/core/controllers/b;->z:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 212
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->E:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    .line 214
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->E:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/nativeads/views/MediaAdView;

    .line 215
    if-eqz v0, :cond_1

    .line 217
    invoke-static {v0}, Lcom/my/target/core/controllers/b;->c(Lcom/my/target/nativeads/views/MediaAdView;)Lcom/my/target/cz;

    move-result-object v0

    .line 218
    if-eqz v0, :cond_1

    .line 220
    invoke-virtual {v0}, Lcom/my/target/cz;->getVisibleCardNumbers()[I

    move-result-object v0

    goto :goto_0

    .line 226
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final unregisterView()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 139
    invoke-virtual {p0}, Lcom/my/target/core/controllers/b;->s()V

    .line 140
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->B:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    .line 142
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->B:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/da;

    .line 143
    if-eqz v0, :cond_0

    .line 145
    invoke-interface {v0, v3}, Lcom/my/target/da;->setPromoCardSliderListener(Lcom/my/target/da$a;)V

    .line 146
    invoke-interface {v0}, Lcom/my/target/da;->getState()Landroid/os/Parcelable;

    move-result-object v1

    iput-object v1, p0, Lcom/my/target/core/controllers/b;->G:Landroid/os/Parcelable;

    .line 147
    invoke-interface {v0}, Lcom/my/target/da;->dispose()V

    .line 149
    :cond_0
    iput-object v3, p0, Lcom/my/target/core/controllers/b;->B:Ljava/lang/ref/WeakReference;

    .line 152
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->E:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_4

    .line 154
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->E:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/nativeads/views/MediaAdView;

    .line 155
    if-eqz v0, :cond_3

    .line 1461
    iget-object v1, p0, Lcom/my/target/core/controllers/b;->y:Lcom/my/target/core/models/banners/a;

    invoke-virtual {v1}, Lcom/my/target/core/models/banners/a;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v2

    .line 1462
    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getImageView()Landroid/widget/ImageView;

    move-result-object v1

    check-cast v1, Lcom/my/target/bv;

    .line 1463
    if-eqz v2, :cond_2

    .line 1465
    invoke-static {v2, v1}, Lcom/my/target/ch;->b(Lcom/my/target/common/models/ImageData;Landroid/widget/ImageView;)V

    .line 1467
    :cond_2
    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getProgressBarView()Landroid/widget/ProgressBar;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1468
    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getPlayButtonView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1469
    invoke-virtual {v1, v3}, Lcom/my/target/bv;->setImageData(Lcom/my/target/common/models/ImageData;)V

    .line 1470
    invoke-virtual {v0, v5, v5}, Lcom/my/target/nativeads/views/MediaAdView;->setPlaceHolderDimension(II)V

    .line 1471
    invoke-virtual {v0, v3}, Lcom/my/target/nativeads/views/MediaAdView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1472
    const v1, -0x111112

    invoke-virtual {v0, v1}, Lcom/my/target/nativeads/views/MediaAdView;->setBackgroundColor(I)V

    .line 1473
    invoke-static {v0}, Lcom/my/target/core/controllers/b;->c(Lcom/my/target/nativeads/views/MediaAdView;)Lcom/my/target/cz;

    move-result-object v0

    .line 1474
    if-eqz v0, :cond_3

    .line 1476
    invoke-interface {v0}, Lcom/my/target/da;->dispose()V

    .line 1477
    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 159
    :cond_3
    iput-object v3, p0, Lcom/my/target/core/controllers/b;->E:Ljava/lang/ref/WeakReference;

    .line 162
    :cond_4
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->C:Ljava/util/HashSet;

    if-eqz v0, :cond_9

    .line 164
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->C:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 166
    if-eqz v0, :cond_5

    .line 168
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 169
    if-eqz v0, :cond_5

    .line 171
    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 175
    :cond_6
    iput-object v3, p0, Lcom/my/target/core/controllers/b;->C:Ljava/util/HashSet;

    .line 189
    :cond_7
    :goto_1
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->A:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_8

    .line 191
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->A:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    .line 192
    iput-object v3, p0, Lcom/my/target/core/controllers/b;->A:Ljava/lang/ref/WeakReference;

    .line 194
    :cond_8
    return-void

    .line 179
    :cond_9
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->A:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_7

    .line 181
    iget-object v0, p0, Lcom/my/target/core/controllers/b;->A:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 182
    if-eqz v0, :cond_7

    .line 184
    invoke-direct {p0, v0}, Lcom/my/target/core/controllers/b;->c(Landroid/view/View;)V

    goto :goto_1
.end method
