.class Lcom/yandex/metrica/impl/ob/ek;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ob/ek$a;,
        Lcom/yandex/metrica/impl/ob/ek$c;,
        Lcom/yandex/metrica/impl/ob/ek$b;
    }
.end annotation


# instance fields
.field private a:Lcom/yandex/metrica/impl/ob/ek$c;

.field private b:Lcom/yandex/metrica/impl/ob/ek$a;

.field private c:Lcom/yandex/metrica/impl/ob/ek$b;

.field private d:Landroid/content/Context;

.field private e:Lcom/yandex/metrica/impl/ob/dv;

.field private f:Lcom/yandex/metrica/impl/ob/em;

.field private g:Lcom/yandex/metrica/impl/ob/en;

.field private h:Lcom/yandex/metrica/impl/ob/dr;

.field private i:Lcom/yandex/metrica/impl/ob/dw;

.field private j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/yandex/metrica/impl/ob/ed;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/ek$c;Lcom/yandex/metrica/impl/ob/ek$a;Lcom/yandex/metrica/impl/ob/ek$b;Lcom/yandex/metrica/impl/ob/en;Lcom/yandex/metrica/impl/ob/dr;)V
    .locals 1

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ek;->j:Ljava/util/Map;

    .line 141
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/ek;->d:Landroid/content/Context;

    .line 142
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/ek;->e:Lcom/yandex/metrica/impl/ob/dv;

    .line 143
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/ek;->a:Lcom/yandex/metrica/impl/ob/ek$c;

    .line 144
    iput-object p4, p0, Lcom/yandex/metrica/impl/ob/ek;->b:Lcom/yandex/metrica/impl/ob/ek$a;

    .line 145
    iput-object p5, p0, Lcom/yandex/metrica/impl/ob/ek;->c:Lcom/yandex/metrica/impl/ob/ek$b;

    .line 146
    iput-object p6, p0, Lcom/yandex/metrica/impl/ob/ek;->g:Lcom/yandex/metrica/impl/ob/en;

    .line 147
    iput-object p7, p0, Lcom/yandex/metrica/impl/ob/ek;->h:Lcom/yandex/metrica/impl/ob/dr;

    .line 148
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/en;Lcom/yandex/metrica/impl/ob/dr;)V
    .locals 8

    .prologue
    .line 83
    new-instance v3, Lcom/yandex/metrica/impl/ob/ek$c;

    invoke-direct {v3}, Lcom/yandex/metrica/impl/ob/ek$c;-><init>()V

    new-instance v4, Lcom/yandex/metrica/impl/ob/ek$a;

    invoke-direct {v4}, Lcom/yandex/metrica/impl/ob/ek$a;-><init>()V

    new-instance v5, Lcom/yandex/metrica/impl/ob/ek$b;

    invoke-direct {v5}, Lcom/yandex/metrica/impl/ob/ek$b;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/yandex/metrica/impl/ob/ek;-><init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/ek$c;Lcom/yandex/metrica/impl/ob/ek$a;Lcom/yandex/metrica/impl/ob/ek$b;Lcom/yandex/metrica/impl/ob/en;Lcom/yandex/metrica/impl/ob/dr;)V

    .line 85
    return-void
.end method


# virtual methods
.method public a()Landroid/location/Location;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ek;->i:Lcom/yandex/metrica/impl/ob/dw;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ek;->i:Lcom/yandex/metrica/impl/ob/dw;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dw;->a()Landroid/location/Location;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/location/Location;)V
    .locals 6

    .prologue
    .line 88
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    .line 89
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ek;->j:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/ed;

    .line 90
    if-nez v0, :cond_2

    .line 1112
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ek;->f:Lcom/yandex/metrica/impl/ob/em;

    if-nez v0, :cond_0

    .line 1113
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ek;->a:Lcom/yandex/metrica/impl/ob/ek$c;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ek;->d:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/ek$c;->a(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/ds;)Lcom/yandex/metrica/impl/ob/em;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ek;->f:Lcom/yandex/metrica/impl/ob/em;

    .line 1115
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ek;->i:Lcom/yandex/metrica/impl/ob/dw;

    if-nez v0, :cond_1

    .line 1116
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ek;->b:Lcom/yandex/metrica/impl/ob/ek$a;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ek;->f:Lcom/yandex/metrica/impl/ob/em;

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/ek$a;->a(Lcom/yandex/metrica/impl/ob/ds;)Lcom/yandex/metrica/impl/ob/dw;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ek;->i:Lcom/yandex/metrica/impl/ob/dw;

    .line 1118
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ek;->c:Lcom/yandex/metrica/impl/ob/ek$b;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ek;->e:Lcom/yandex/metrica/impl/ob/dv;

    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/ek;->f:Lcom/yandex/metrica/impl/ob/em;

    iget-object v4, p0, Lcom/yandex/metrica/impl/ob/ek;->g:Lcom/yandex/metrica/impl/ob/en;

    iget-object v5, p0, Lcom/yandex/metrica/impl/ob/ek;->h:Lcom/yandex/metrica/impl/ob/dr;

    invoke-virtual/range {v0 .. v5}, Lcom/yandex/metrica/impl/ob/ek$b;->a(Ljava/lang/String;Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/em;Lcom/yandex/metrica/impl/ob/en;Lcom/yandex/metrica/impl/ob/dr;)Lcom/yandex/metrica/impl/ob/ed;

    move-result-object v0

    .line 92
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ek;->j:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    :goto_0
    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/ed;->a(Landroid/location/Location;)V

    .line 99
    return-void

    .line 96
    :cond_2
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ek;->e:Lcom/yandex/metrica/impl/ob/dv;

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/ed;->a(Lcom/yandex/metrica/impl/ob/dv;)V

    goto :goto_0
.end method

.method public a(Lcom/yandex/metrica/impl/ob/dv;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/ek;->e:Lcom/yandex/metrica/impl/ob/dv;

    .line 124
    return-void
.end method
