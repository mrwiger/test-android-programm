.class public Lcom/my/target/common/MyTargetUtils;
.super Ljava/lang/Object;
.source "MyTargetUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static collectInfo(Landroid/content/Context;)Ljava/util/Map;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    invoke-static {}, Lcom/my/target/bn;->aN()Lcom/my/target/bn;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/my/target/bn;->collectData(Landroid/content/Context;)V

    .line 30
    invoke-static {}, Lcom/my/target/bn;->aN()Lcom/my/target/bn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/bn;->getData()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public static sendStat(Ljava/lang/String;Landroid/content/Context;)V
    .locals 0
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-static {p0, p1}, Lcom/my/target/cl;->n(Ljava/lang/String;Landroid/content/Context;)V

    .line 24
    return-void
.end method
