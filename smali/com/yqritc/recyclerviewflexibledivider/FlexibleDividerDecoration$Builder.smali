.class public Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;
.super Ljava/lang/Object;
.source "FlexibleDividerDecoration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mColorProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$ColorProvider;

.field private mContext:Landroid/content/Context;

.field private mDrawableProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DrawableProvider;

.field private mPaintProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$PaintProvider;

.field private mPositionInsideItem:Z

.field protected mResources:Landroid/content/res/Resources;

.field private mShowLastDivider:Z

.field private mSizeProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$SizeProvider;

.field private mVisibilityProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$VisibilityProvider;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .local p0, "this":Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;, "Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder<TT;>;"
    const/4 v1, 0x0

    .line 338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 329
    new-instance v0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder$1;

    invoke-direct {v0, p0}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder$1;-><init>(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)V

    iput-object v0, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->mVisibilityProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$VisibilityProvider;

    .line 335
    iput-boolean v1, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->mShowLastDivider:Z

    .line 336
    iput-boolean v1, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->mPositionInsideItem:Z

    .line 339
    iput-object p1, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->mContext:Landroid/content/Context;

    .line 340
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->mResources:Landroid/content/res/Resources;

    .line 341
    return-void
.end method

.method static synthetic access$000(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$PaintProvider;
    .locals 1
    .param p0, "x0"    # Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;

    .prologue
    .line 321
    iget-object v0, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->mPaintProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$PaintProvider;

    return-object v0
.end method

.method static synthetic access$100(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$ColorProvider;
    .locals 1
    .param p0, "x0"    # Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;

    .prologue
    .line 321
    iget-object v0, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->mColorProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$ColorProvider;

    return-object v0
.end method

.method static synthetic access$200(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DrawableProvider;
    .locals 1
    .param p0, "x0"    # Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;

    .prologue
    .line 321
    iget-object v0, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->mDrawableProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DrawableProvider;

    return-object v0
.end method

.method static synthetic access$300(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;

    .prologue
    .line 321
    iget-object v0, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$SizeProvider;
    .locals 1
    .param p0, "x0"    # Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;

    .prologue
    .line 321
    iget-object v0, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->mSizeProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$SizeProvider;

    return-object v0
.end method

.method static synthetic access$500(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$VisibilityProvider;
    .locals 1
    .param p0, "x0"    # Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;

    .prologue
    .line 321
    iget-object v0, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->mVisibilityProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$VisibilityProvider;

    return-object v0
.end method

.method static synthetic access$600(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;

    .prologue
    .line 321
    iget-boolean v0, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->mShowLastDivider:Z

    return v0
.end method

.method static synthetic access$700(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;

    .prologue
    .line 321
    iget-boolean v0, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->mPositionInsideItem:Z

    return v0
.end method


# virtual methods
.method protected checkBuilderParams()V
    .locals 2

    .prologue
    .line 427
    .local p0, "this":Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;, "Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder<TT;>;"
    iget-object v0, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->mPaintProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$PaintProvider;

    if-eqz v0, :cond_1

    .line 428
    iget-object v0, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->mColorProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$ColorProvider;

    if-eqz v0, :cond_0

    .line 429
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Use setColor method of Paint class to specify line color. Do not provider ColorProvider if you set PaintProvider."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 432
    :cond_0
    iget-object v0, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->mSizeProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$SizeProvider;

    if-eqz v0, :cond_1

    .line 433
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Use setStrokeWidth method of Paint class to specify line size. Do not provider SizeProvider if you set PaintProvider."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 437
    :cond_1
    return-void
.end method

.method public drawable(I)Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;
    .locals 1
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 376
    .local p0, "this":Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;, "Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder<TT;>;"
    iget-object v0, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->drawable(Landroid/graphics/drawable/Drawable;)Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;

    move-result-object v0

    return-object v0
.end method

.method public drawable(Landroid/graphics/drawable/Drawable;)Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/drawable/Drawable;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 380
    .local p0, "this":Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;, "Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder<TT;>;"
    new-instance v0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder$4;

    invoke-direct {v0, p0, p1}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder$4;-><init>(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v0}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->drawableProvider(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DrawableProvider;)Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;

    move-result-object v0

    return-object v0
.end method

.method public drawableProvider(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DrawableProvider;)Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;
    .locals 0
    .param p1, "provider"    # Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DrawableProvider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DrawableProvider;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 389
    .local p0, "this":Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;, "Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder<TT;>;"
    iput-object p1, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->mDrawableProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DrawableProvider;

    .line 390
    return-object p0
.end method
