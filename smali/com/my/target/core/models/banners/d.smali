.class public abstract Lcom/my/target/core/models/banners/d;
.super Lcom/my/target/ah;
.source "InterstitialAdBanner.java"


# instance fields
.field private allowClose:Z

.field private allowCloseDelay:F

.field private closeIcon:Lcom/my/target/common/models/ImageData;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/my/target/ah;-><init>()V

    .line 20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/core/models/banners/d;->allowClose:Z

    .line 24
    sget-object v0, Lcom/my/target/af;->ce:Lcom/my/target/af;

    iput-object v0, p0, Lcom/my/target/core/models/banners/d;->clickArea:Lcom/my/target/af;

    .line 25
    return-void
.end method


# virtual methods
.method public getAllowCloseDelay()F
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/my/target/core/models/banners/d;->allowCloseDelay:F

    return v0
.end method

.method public getCloseIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/my/target/core/models/banners/d;->closeIcon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public isAllowClose()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/my/target/core/models/banners/d;->allowClose:Z

    return v0
.end method

.method public setAllowClose(Z)V
    .locals 0
    .param p1, "allowClose"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/my/target/core/models/banners/d;->allowClose:Z

    .line 50
    return-void
.end method

.method public setAllowCloseDelay(F)V
    .locals 0
    .param p1, "allowCloseDelay"    # F

    .prologue
    .line 44
    iput p1, p0, Lcom/my/target/core/models/banners/d;->allowCloseDelay:F

    .line 45
    return-void
.end method

.method public setCloseIcon(Lcom/my/target/common/models/ImageData;)V
    .locals 0
    .param p1, "closeIcon"    # Lcom/my/target/common/models/ImageData;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/my/target/core/models/banners/d;->closeIcon:Lcom/my/target/common/models/ImageData;

    .line 35
    return-void
.end method
