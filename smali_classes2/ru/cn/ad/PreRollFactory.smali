.class public Lru/cn/ad/PreRollFactory;
.super Ljava/lang/Object;
.source "PreRollFactory.java"

# interfaces
.implements Lru/cn/ad/AdAdapter$Factory;


# instance fields
.field private final context:Landroid/content/Context;

.field private final isTV:Z

.field private final maxDimension:I

.field private final optimalDimension:I


# direct methods
.method constructor <init>(Landroid/content/Context;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isTV"    # Z

    .prologue
    const/16 v1, 0x2d0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lru/cn/ad/PreRollFactory;->context:Landroid/content/Context;

    .line 26
    iput-boolean p2, p0, Lru/cn/ad/PreRollFactory;->isTV:Z

    .line 27
    if-eqz p2, :cond_0

    const/16 v0, 0x438

    :goto_0
    iput v0, p0, Lru/cn/ad/PreRollFactory;->maxDimension:I

    .line 28
    if-eqz p2, :cond_1

    :goto_1
    iput v1, p0, Lru/cn/ad/PreRollFactory;->optimalDimension:I

    .line 29
    return-void

    :cond_0
    move v0, v1

    .line 27
    goto :goto_0

    .line 28
    :cond_1
    const/16 v1, 0x1e0

    goto :goto_1
.end method

.method private createMobileAdapter(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)Lru/cn/ad/AdAdapter;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "system"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 70
    iget v0, p2, Lru/cn/api/money_miner/replies/AdSystem;->bannerType:I

    packed-switch v0, :pswitch_data_0

    .line 78
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 72
    :pswitch_0
    invoke-direct {p0, p1, p2}, Lru/cn/ad/PreRollFactory;->createMobileVideoAdapter(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)Lru/cn/ad/AdAdapter;

    move-result-object v0

    goto :goto_0

    .line 75
    :pswitch_1
    invoke-direct {p0, p1, p2}, Lru/cn/ad/PreRollFactory;->createMobileNativeAdapter(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)Lru/cn/ad/AdAdapter;

    move-result-object v0

    goto :goto_0

    .line 70
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private createMobileNativeAdapter(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)Lru/cn/ad/AdAdapter;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "system"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 106
    iget v0, p2, Lru/cn/api/money_miner/replies/AdSystem;->type:I

    sparse-switch v0, :sswitch_data_0

    .line 117
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 108
    :sswitch_0
    new-instance v0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    invoke-direct {v0, p1, p2}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    goto :goto_0

    .line 111
    :sswitch_1
    new-instance v0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;

    invoke-direct {v0, p1, p2}, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    goto :goto_0

    .line 114
    :sswitch_2
    new-instance v0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;

    invoke-direct {v0, p1, p2}, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    goto :goto_0

    .line 106
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x8 -> :sswitch_1
        0xb -> :sswitch_2
    .end sparse-switch
.end method

.method private createMobileVideoAdapter(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)Lru/cn/ad/AdAdapter;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "system"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 82
    iget v0, p2, Lru/cn/api/money_miner/replies/AdSystem;->type:I

    packed-switch v0, :pswitch_data_0

    .line 102
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 84
    :pswitch_1
    new-instance v0, Lru/cn/ad/video/VAST/VastAdapter;

    invoke-direct {v0, p1, p2}, Lru/cn/ad/video/VAST/VastAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    goto :goto_0

    .line 87
    :pswitch_2
    new-instance v0, Lru/cn/ad/video/Wapstart/WapStartAdapter;

    invoke-direct {v0, p1, p2}, Lru/cn/ad/video/Wapstart/WapStartAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    goto :goto_0

    .line 90
    :pswitch_3
    new-instance v0, Lru/cn/ad/video/yandex/YandexVideoAdapter;

    invoke-direct {v0, p1, p2}, Lru/cn/ad/video/yandex/YandexVideoAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    goto :goto_0

    .line 93
    :pswitch_4
    new-instance v0, Lru/cn/ad/video/ima/IMAVideoAdapter;

    invoke-direct {v0, p1, p2}, Lru/cn/ad/video/ima/IMAVideoAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    goto :goto_0

    .line 96
    :pswitch_5
    new-instance v0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;

    invoke-direct {v0, p1, p2}, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    goto :goto_0

    .line 99
    :pswitch_6
    new-instance v0, Lru/cn/ad/video/facebook/FacebookVideoAdapter;

    invoke-direct {v0, p1, p2}, Lru/cn/ad/video/facebook/FacebookVideoAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    goto :goto_0

    .line 82
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method private createTVAdapter(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)Lru/cn/ad/AdAdapter;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "system"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    const/4 v0, 0x0

    .line 52
    iget v1, p2, Lru/cn/api/money_miner/replies/AdSystem;->bannerType:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 66
    :goto_0
    return-object v0

    .line 55
    :cond_0
    iget v1, p2, Lru/cn/api/money_miner/replies/AdSystem;->type:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 57
    :pswitch_1
    new-instance v0, Lru/cn/ad/video/VAST/VastAdapter;

    invoke-direct {v0, p1, p2}, Lru/cn/ad/video/VAST/VastAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    goto :goto_0

    .line 60
    :pswitch_2
    new-instance v0, Lru/cn/ad/video/Wapstart/WapStartAdapter;

    invoke-direct {v0, p1, p2}, Lru/cn/ad/video/Wapstart/WapStartAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    goto :goto_0

    .line 63
    :pswitch_3
    new-instance v0, Lru/cn/ad/video/yandex/YandexVideoAdapter;

    invoke-direct {v0, p1, p2}, Lru/cn/ad/video/yandex/YandexVideoAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    goto :goto_0

    .line 55
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public create(Lru/cn/api/money_miner/replies/AdSystem;)Lru/cn/ad/AdAdapter;
    .locals 3
    .param p1, "system"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 34
    iget-boolean v2, p0, Lru/cn/ad/PreRollFactory;->isTV:Z

    if-eqz v2, :cond_1

    .line 35
    iget-object v2, p0, Lru/cn/ad/PreRollFactory;->context:Landroid/content/Context;

    invoke-direct {p0, v2, p1}, Lru/cn/ad/PreRollFactory;->createTVAdapter(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)Lru/cn/ad/AdAdapter;

    move-result-object v0

    .line 42
    .local v0, "adapter":Lru/cn/ad/AdAdapter;
    :goto_0
    if-eqz v0, :cond_0

    instance-of v2, v0, Lru/cn/ad/video/VideoAdAdapter;

    if-eqz v2, :cond_0

    move-object v1, v0

    .line 43
    check-cast v1, Lru/cn/ad/video/VideoAdAdapter;

    .line 44
    .local v1, "videoAdAdapter":Lru/cn/ad/video/VideoAdAdapter;
    iget v2, p0, Lru/cn/ad/PreRollFactory;->maxDimension:I

    iput v2, v1, Lru/cn/ad/video/VideoAdAdapter;->maxVideoDimension:I

    .line 45
    iget v2, p0, Lru/cn/ad/PreRollFactory;->optimalDimension:I

    iput v2, v1, Lru/cn/ad/video/VideoAdAdapter;->optimalVideoDimension:I

    .line 48
    .end local v1    # "videoAdAdapter":Lru/cn/ad/video/VideoAdAdapter;
    :cond_0
    return-object v0

    .line 37
    .end local v0    # "adapter":Lru/cn/ad/AdAdapter;
    :cond_1
    iget-object v2, p0, Lru/cn/ad/PreRollFactory;->context:Landroid/content/Context;

    invoke-direct {p0, v2, p1}, Lru/cn/ad/PreRollFactory;->createMobileAdapter(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)Lru/cn/ad/AdAdapter;

    move-result-object v0

    .restart local v0    # "adapter":Lru/cn/ad/AdAdapter;
    goto :goto_0
.end method
