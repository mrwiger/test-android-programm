.class public Lcom/yandex/metrica/impl/ob/dc;
.super Lcom/yandex/metrica/impl/ob/de;
.source "SourceFile"


# static fields
.field static final a:Lcom/yandex/metrica/impl/ob/fm;

.field static final b:Lcom/yandex/metrica/impl/ob/fm;

.field static final c:Lcom/yandex/metrica/impl/ob/fm;

.field static final d:Lcom/yandex/metrica/impl/ob/fm;

.field static final e:Lcom/yandex/metrica/impl/ob/fm;

.field static final f:Lcom/yandex/metrica/impl/ob/fm;

.field static final g:Lcom/yandex/metrica/impl/ob/fm;

.field static final h:Lcom/yandex/metrica/impl/ob/fm;

.field static final i:Lcom/yandex/metrica/impl/ob/fm;

.field static final j:Lcom/yandex/metrica/impl/ob/fm;

.field static final k:Lcom/yandex/metrica/impl/ob/fm;

.field static final l:Lcom/yandex/metrica/impl/ob/fm;

.field static final m:Lcom/yandex/metrica/impl/ob/fm;

.field static final n:Lcom/yandex/metrica/impl/ob/fm;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "UUID"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dc;->a:Lcom/yandex/metrica/impl/ob/fm;

    .line 25
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "DEVICE_ID_POSSIBLE"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dc;->b:Lcom/yandex/metrica/impl/ob/fm;

    .line 26
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "DEVICE_ID"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dc;->c:Lcom/yandex/metrica/impl/ob/fm;

    .line 27
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "AD_URL_GET"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dc;->d:Lcom/yandex/metrica/impl/ob/fm;

    .line 28
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "AD_URL_REPORT"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dc;->e:Lcom/yandex/metrica/impl/ob/fm;

    .line 29
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "CUSTOM_HOSTS"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dc;->f:Lcom/yandex/metrica/impl/ob/fm;

    .line 30
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "SERVER_TIME_OFFSET"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dc;->g:Lcom/yandex/metrica/impl/ob/fm;

    .line 31
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "STARTUP_REQUEST_TIME"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dc;->h:Lcom/yandex/metrica/impl/ob/fm;

    .line 32
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "CLIDS"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dc;->i:Lcom/yandex/metrica/impl/ob/fm;

    .line 33
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "COOKIE_BROWSERS"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dc;->j:Lcom/yandex/metrica/impl/ob/fm;

    .line 34
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "BIND_ID_URL"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dc;->k:Lcom/yandex/metrica/impl/ob/fm;

    .line 35
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "REFERRER"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dc;->l:Lcom/yandex/metrica/impl/ob/fm;

    .line 36
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "DEFERRED_DEEP_LINK_WAS_CHECKED"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dc;->m:Lcom/yandex/metrica/impl/ob/fm;

    .line 38
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "API_LEVEL"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dc;->n:Lcom/yandex/metrica/impl/ob/fm;

    return-void
.end method

.method public constructor <init>(Lcom/yandex/metrica/impl/ob/cr;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/de;-><init>(Lcom/yandex/metrica/impl/ob/cr;)V

    .line 42
    return-void
.end method


# virtual methods
.method public a(J)J
    .locals 3

    .prologue
    .line 71
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->g:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/dc;->b(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Ljava/util/List;)Lcom/yandex/metrica/impl/ob/dc;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/yandex/metrica/impl/ob/dc;"
        }
    .end annotation

    .prologue
    .line 107
    invoke-static {p1}, Lcom/yandex/metrica/impl/utils/g;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 108
    sget-object v1, Lcom/yandex/metrica/impl/ob/dc;->f:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/yandex/metrica/impl/ob/dc;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dc;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 53
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->b:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/dc;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->a:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/dc;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(J)J
    .locals 3

    .prologue
    .line 75
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->h:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/dc;->b(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->c:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/dc;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 65
    sget-object v1, Lcom/yandex/metrica/impl/ob/dc;->f:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/yandex/metrica/impl/ob/dc;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 66
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 67
    :goto_0
    return-object v0

    .line 66
    :cond_0
    invoke-static {v1}, Lcom/yandex/metrica/impl/utils/g;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public c(J)J
    .locals 3

    .prologue
    .line 83
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->n:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/dc;->b(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 87
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->l:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/dc;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->d:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/dc;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(J)Lcom/yandex/metrica/impl/ob/dc;
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->g:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/dc;->a(Ljava/lang/String;J)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dc;

    return-object v0
.end method

.method public d(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->e:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/dc;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 91
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->m:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/dc;->b(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public e()Lcom/yandex/metrica/impl/ob/dc;
    .locals 2

    .prologue
    .line 156
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->m:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/dc;->a(Ljava/lang/String;Z)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dc;

    return-object v0
.end method

.method public e(J)Lcom/yandex/metrica/impl/ob/dc;
    .locals 1

    .prologue
    .line 120
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->h:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/dc;->a(Ljava/lang/String;J)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dc;

    return-object v0
.end method

.method public e(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->i:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/dc;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f(J)Lcom/yandex/metrica/impl/ob/dc;
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->n:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/dc;->a(Ljava/lang/String;J)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dc;

    return-object v0
.end method

.method public f(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dc;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->a:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/dc;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dc;

    return-object v0
.end method

.method public g(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dc;
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->c:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/dc;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dc;

    return-object v0
.end method

.method public h(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dc;
    .locals 1

    .prologue
    .line 103
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->d:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/dc;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dc;

    return-object v0
.end method

.method public i(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dc;
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->e:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/dc;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dc;

    return-object v0
.end method

.method public j(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dc;
    .locals 1

    .prologue
    .line 124
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->i:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/dc;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dc;

    return-object v0
.end method

.method public k(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dc;
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->b:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/dc;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dc;

    return-object v0
.end method

.method public l(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->j:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/dc;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dc;
    .locals 1

    .prologue
    .line 140
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->j:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/dc;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dc;

    return-object v0
.end method

.method public n(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->k:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/dc;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public o(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dc;
    .locals 1

    .prologue
    .line 148
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->k:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/dc;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dc;

    return-object v0
.end method

.method public p(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dc;
    .locals 1

    .prologue
    .line 152
    sget-object v0, Lcom/yandex/metrica/impl/ob/dc;->l:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/dc;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dc;

    return-object v0
.end method
