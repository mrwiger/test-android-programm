.class public interface abstract Lcom/my/target/dm;
.super Ljava/lang/Object;
.source "StandardNativeView.java"


# virtual methods
.method public abstract H()Z
.end method

.method public abstract O()Landroid/view/View;
.end method

.method public abstract P()V
.end method

.method public abstract a(Lcom/my/target/af;ZLandroid/view/View$OnClickListener;)V
.end method

.method public abstract b(II)V
.end method

.method public abstract b(Lcom/my/target/ah;)V
.end method

.method public abstract getAgeRestrictionsView()Lcom/my/target/bu;
.end method

.method public abstract getBannerImage()Lcom/my/target/bx;
.end method

.method public abstract getCtaButton()Landroid/widget/Button;
.end method

.method public abstract getDescriptionTextView()Landroid/widget/TextView;
.end method

.method public abstract getDisclaimerTextView()Landroid/widget/TextView;
.end method

.method public abstract getDomainTextView()Landroid/widget/TextView;
.end method

.method public abstract getIconImage()Lcom/my/target/bx;
.end method

.method public abstract getMainImage()Lcom/my/target/bx;
.end method

.method public abstract getRatingTextView()Landroid/widget/TextView;
.end method

.method public abstract getStarsRatingView()Lcom/my/target/ca;
.end method

.method public abstract getTitleTextView()Landroid/widget/TextView;
.end method

.method public abstract start()V
.end method

.method public abstract stop()V
.end method
