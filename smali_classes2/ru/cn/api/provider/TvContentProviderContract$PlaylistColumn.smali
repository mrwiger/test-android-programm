.class public final enum Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;
.super Ljava/lang/Enum;
.source "TvContentProviderContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/api/provider/TvContentProviderContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PlaylistColumn"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

.field public static final enum _id:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

.field public static final enum contractor_id:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

.field public static final enum load_channels_time:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

.field public static final enum location:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

.field public static final enum priority:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

.field public static final enum title:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 41
    new-instance v0, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    const-string v1, "_id"

    invoke-direct {v0, v1, v3}, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->_id:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    new-instance v0, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    const-string v1, "contractor_id"

    invoke-direct {v0, v1, v4}, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->contractor_id:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    new-instance v0, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    const-string v1, "load_channels_time"

    invoke-direct {v0, v1, v5}, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->load_channels_time:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    new-instance v0, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    const-string v1, "title"

    invoke-direct {v0, v1, v6}, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->title:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    new-instance v0, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    const-string v1, "location"

    invoke-direct {v0, v1, v7}, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->location:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    new-instance v0, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    const-string v1, "priority"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->priority:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    .line 40
    const/4 v0, 0x6

    new-array v0, v0, [Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    sget-object v1, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->_id:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    aput-object v1, v0, v3

    sget-object v1, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->contractor_id:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    aput-object v1, v0, v4

    sget-object v1, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->load_channels_time:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    aput-object v1, v0, v5

    sget-object v1, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->title:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->location:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->priority:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    aput-object v2, v0, v1

    sput-object v0, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->$VALUES:[Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 40
    const-class v0, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    return-object v0
.end method

.method public static values()[Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->$VALUES:[Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    invoke-virtual {v0}, [Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    return-object v0
.end method
