.class public final Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;
.super Ljava/lang/Object;
.source "InstreamAudioAd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/instreamads/InstreamAudioAd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InstreamAudioAdBanner"
.end annotation


# instance fields
.field public final adText:Ljava/lang/String;

.field public final allowPause:Z

.field public final allowSeek:Z

.field public final allowSkip:Z

.field public final allowTrackChange:Z

.field public final companionBanners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;",
            ">;"
        }
    .end annotation
.end field

.field public final duration:F


# direct methods
.method private constructor <init>(ZZZFLjava/lang/String;ZLjava/util/List;)V
    .locals 0
    .param p1, "allowSeek"    # Z
    .param p2, "allowSkip"    # Z
    .param p3, "allowTrackChange"    # Z
    .param p4, "duration"    # F
    .param p5, "adText"    # Ljava/lang/String;
    .param p6, "allowPause"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZF",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 453
    .local p7, "companionBanners":Ljava/util/List;, "Ljava/util/List<Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 454
    iput-boolean p1, p0, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;->allowSeek:Z

    .line 455
    iput-boolean p2, p0, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;->allowSkip:Z

    .line 456
    iput-boolean p6, p0, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;->allowPause:Z

    .line 457
    iput-boolean p3, p0, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;->allowTrackChange:Z

    .line 458
    iput p4, p0, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;->duration:F

    .line 459
    iput-object p5, p0, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;->adText:Ljava/lang/String;

    .line 460
    iput-object p7, p0, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;->companionBanners:Ljava/util/List;

    .line 461
    return-void
.end method

.method public static newBanner(Lcom/my/target/aj;)Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/aj",
            "<",
            "Lcom/my/target/common/models/AudioData;",
            ">;)",
            "Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;"
        }
    .end annotation

    .prologue
    .line 423
    .local p0, "mediaBanner":Lcom/my/target/aj;, "Lcom/my/target/aj<Lcom/my/target/common/models/AudioData;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 424
    invoke-virtual {p0}, Lcom/my/target/aj;->getCompanionBanners()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/ai;

    .line 426
    invoke-static {v0}, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;->newBanner(Lcom/my/target/ai;)Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 429
    :cond_0
    new-instance v0, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;

    invoke-virtual {p0}, Lcom/my/target/aj;->isAllowSeek()Z

    move-result v1

    .line 430
    invoke-virtual {p0}, Lcom/my/target/aj;->isAllowSkip()Z

    move-result v2

    .line 431
    invoke-virtual {p0}, Lcom/my/target/aj;->isAllowTrackChange()Z

    move-result v3

    .line 432
    invoke-virtual {p0}, Lcom/my/target/aj;->getDuration()F

    move-result v4

    .line 433
    invoke-virtual {p0}, Lcom/my/target/aj;->getAdText()Ljava/lang/String;

    move-result-object v5

    .line 434
    invoke-virtual {p0}, Lcom/my/target/aj;->isAllowPause()Z

    move-result v6

    invoke-direct/range {v0 .. v7}, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;-><init>(ZZZFLjava/lang/String;ZLjava/util/List;)V

    .line 429
    return-object v0
.end method
