.class Lru/cn/ad/MiddleRollBanner$1;
.super Ljava/lang/Object;
.source "MiddleRollBanner.java"

# interfaces
.implements Lru/cn/ad/AdAdapter$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/ad/MiddleRollBanner;->show(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/ad/MiddleRollBanner;

.field final synthetic val$blocking:Z

.field final synthetic val$presenter:Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;


# direct methods
.method constructor <init>(Lru/cn/ad/MiddleRollBanner;ZLru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/ad/MiddleRollBanner;

    .prologue
    .line 308
    iput-object p1, p0, Lru/cn/ad/MiddleRollBanner$1;->this$0:Lru/cn/ad/MiddleRollBanner;

    iput-boolean p2, p0, Lru/cn/ad/MiddleRollBanner$1;->val$blocking:Z

    iput-object p3, p0, Lru/cn/ad/MiddleRollBanner$1;->val$presenter:Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAdEnded()V
    .locals 2

    .prologue
    .line 328
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner$1;->this$0:Lru/cn/ad/MiddleRollBanner;

    invoke-virtual {v0}, Lru/cn/ad/MiddleRollBanner;->dismiss()V

    .line 330
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner$1;->this$0:Lru/cn/ad/MiddleRollBanner;

    invoke-static {v0}, Lru/cn/ad/MiddleRollBanner;->access$100(Lru/cn/ad/MiddleRollBanner;)Lru/cn/ad/MiddleRollBanner$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner$1;->this$0:Lru/cn/ad/MiddleRollBanner;

    invoke-static {v0}, Lru/cn/ad/MiddleRollBanner;->access$100(Lru/cn/ad/MiddleRollBanner;)Lru/cn/ad/MiddleRollBanner$Listener;

    move-result-object v0

    iget-boolean v1, p0, Lru/cn/ad/MiddleRollBanner$1;->val$blocking:Z

    invoke-interface {v0, v1}, Lru/cn/ad/MiddleRollBanner$Listener;->adCompleted(Z)V

    .line 333
    :cond_0
    return-void
.end method

.method public onAdLoaded()V
    .locals 0

    .prologue
    .line 311
    return-void
.end method

.method public onAdStarted()V
    .locals 3

    .prologue
    .line 319
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner$1;->this$0:Lru/cn/ad/MiddleRollBanner;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lru/cn/ad/MiddleRollBanner;->access$002(Lru/cn/ad/MiddleRollBanner;Z)Z

    .line 321
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner$1;->this$0:Lru/cn/ad/MiddleRollBanner;

    invoke-static {v0}, Lru/cn/ad/MiddleRollBanner;->access$100(Lru/cn/ad/MiddleRollBanner;)Lru/cn/ad/MiddleRollBanner$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner$1;->this$0:Lru/cn/ad/MiddleRollBanner;

    invoke-static {v0}, Lru/cn/ad/MiddleRollBanner;->access$100(Lru/cn/ad/MiddleRollBanner;)Lru/cn/ad/MiddleRollBanner$Listener;

    move-result-object v0

    iget-boolean v1, p0, Lru/cn/ad/MiddleRollBanner$1;->val$blocking:Z

    iget-object v2, p0, Lru/cn/ad/MiddleRollBanner$1;->val$presenter:Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    invoke-interface {v2}, Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;->presentingView()Landroid/view/View;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lru/cn/ad/MiddleRollBanner$Listener;->adStarted(ZLandroid/view/View;)V

    .line 323
    :cond_0
    return-void
.end method

.method public onError()V
    .locals 0

    .prologue
    .line 315
    return-void
.end method
