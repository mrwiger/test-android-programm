.class final Lcom/google/obf/co;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/co$a;
    }
.end annotation


# direct methods
.method public static a(Lcom/google/obf/ak;)Lcom/google/obf/cn;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;,
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/16 v4, 0x10

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 1
    invoke-static {p0}, Lcom/google/obf/dl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2
    new-instance v5, Lcom/google/obf/dw;

    invoke-direct {v5, v4}, Lcom/google/obf/dw;-><init>(I)V

    .line 3
    invoke-static {p0, v5}, Lcom/google/obf/co$a;->a(Lcom/google/obf/ak;Lcom/google/obf/dw;)Lcom/google/obf/co$a;

    move-result-object v0

    .line 4
    iget v0, v0, Lcom/google/obf/co$a;->a:I

    const-string v1, "RIFF"

    invoke-static {v1}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_0

    move-object v0, v8

    .line 36
    :goto_0
    return-object v0

    .line 6
    :cond_0
    iget-object v0, v5, Lcom/google/obf/dw;->a:[B

    const/4 v1, 0x4

    invoke-interface {p0, v0, v2, v1}, Lcom/google/obf/ak;->c([BII)V

    .line 7
    invoke-virtual {v5, v2}, Lcom/google/obf/dw;->c(I)V

    .line 8
    invoke-virtual {v5}, Lcom/google/obf/dw;->m()I

    move-result v0

    .line 9
    const-string v1, "WAVE"

    invoke-static {v1}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 10
    const-string v1, "WavHeaderReader"

    const/16 v2, 0x24

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unsupported RIFF format: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v8

    .line 11
    goto :goto_0

    .line 12
    :cond_1
    invoke-static {p0, v5}, Lcom/google/obf/co$a;->a(Lcom/google/obf/ak;Lcom/google/obf/dw;)Lcom/google/obf/co$a;

    move-result-object v0

    .line 13
    :goto_1
    iget v1, v0, Lcom/google/obf/co$a;->a:I

    const-string v3, "fmt "

    invoke-static {v3}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v3

    if-eq v1, v3, :cond_2

    .line 14
    iget-wide v0, v0, Lcom/google/obf/co$a;->b:J

    long-to-int v0, v0

    invoke-interface {p0, v0}, Lcom/google/obf/ak;->c(I)V

    .line 15
    invoke-static {p0, v5}, Lcom/google/obf/co$a;->a(Lcom/google/obf/ak;Lcom/google/obf/dw;)Lcom/google/obf/co$a;

    move-result-object v0

    goto :goto_1

    .line 16
    :cond_2
    iget-wide v10, v0, Lcom/google/obf/co$a;->b:J

    const-wide/16 v12, 0x10

    cmp-long v1, v10, v12

    if-ltz v1, :cond_3

    move v1, v7

    :goto_2
    invoke-static {v1}, Lcom/google/obf/dl;->b(Z)V

    .line 17
    iget-object v1, v5, Lcom/google/obf/dw;->a:[B

    invoke-interface {p0, v1, v2, v4}, Lcom/google/obf/ak;->c([BII)V

    .line 18
    invoke-virtual {v5, v2}, Lcom/google/obf/dw;->c(I)V

    .line 19
    invoke-virtual {v5}, Lcom/google/obf/dw;->h()I

    move-result v9

    .line 20
    invoke-virtual {v5}, Lcom/google/obf/dw;->h()I

    move-result v1

    .line 21
    invoke-virtual {v5}, Lcom/google/obf/dw;->t()I

    move-result v2

    .line 22
    invoke-virtual {v5}, Lcom/google/obf/dw;->t()I

    move-result v3

    .line 23
    invoke-virtual {v5}, Lcom/google/obf/dw;->h()I

    move-result v4

    .line 24
    invoke-virtual {v5}, Lcom/google/obf/dw;->h()I

    move-result v5

    .line 25
    mul-int v6, v1, v5

    div-int/lit8 v6, v6, 0x8

    .line 26
    if-eq v4, v6, :cond_4

    .line 27
    new-instance v0, Lcom/google/obf/s;

    const/16 v1, 0x37

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Expected block alignment: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; got: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move v1, v2

    .line 16
    goto :goto_2

    .line 28
    :cond_4
    invoke-static {v5}, Lcom/google/obf/ea;->a(I)I

    move-result v6

    .line 29
    if-nez v6, :cond_5

    .line 30
    const-string v0, "WavHeaderReader"

    const/16 v1, 0x26

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unsupported WAV bit depth: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v8

    .line 31
    goto/16 :goto_0

    .line 32
    :cond_5
    if-eq v9, v7, :cond_6

    const v7, 0xfffe

    if-eq v9, v7, :cond_6

    .line 33
    const-string v0, "WavHeaderReader"

    const/16 v1, 0x28

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unsupported WAV format type: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v8

    .line 34
    goto/16 :goto_0

    .line 35
    :cond_6
    iget-wide v8, v0, Lcom/google/obf/co$a;->b:J

    long-to-int v0, v8

    add-int/lit8 v0, v0, -0x10

    invoke-interface {p0, v0}, Lcom/google/obf/ak;->c(I)V

    .line 36
    new-instance v0, Lcom/google/obf/cn;

    invoke-direct/range {v0 .. v6}, Lcom/google/obf/cn;-><init>(IIIIII)V

    goto/16 :goto_0
.end method

.method public static a(Lcom/google/obf/ak;Lcom/google/obf/cn;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;,
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/16 v6, 0x8

    .line 37
    invoke-static {p0}, Lcom/google/obf/dl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    invoke-static {p1}, Lcom/google/obf/dl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    invoke-interface {p0}, Lcom/google/obf/ak;->a()V

    .line 40
    new-instance v1, Lcom/google/obf/dw;

    invoke-direct {v1, v6}, Lcom/google/obf/dw;-><init>(I)V

    .line 41
    invoke-static {p0, v1}, Lcom/google/obf/co$a;->a(Lcom/google/obf/ak;Lcom/google/obf/dw;)Lcom/google/obf/co$a;

    move-result-object v0

    .line 42
    :goto_0
    iget v2, v0, Lcom/google/obf/co$a;->a:I

    const-string v3, "data"

    invoke-static {v3}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v3

    if-eq v2, v3, :cond_2

    .line 43
    const-string v2, "WavHeaderReader"

    iget v3, v0, Lcom/google/obf/co$a;->a:I

    const/16 v4, 0x27

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Ignoring unknown WAV chunk: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    const-wide/16 v2, 0x8

    iget-wide v4, v0, Lcom/google/obf/co$a;->b:J

    add-long/2addr v2, v4

    .line 45
    iget v4, v0, Lcom/google/obf/co$a;->a:I

    const-string v5, "RIFF"

    invoke-static {v5}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 46
    const-wide/16 v2, 0xc

    .line 47
    :cond_0
    const-wide/32 v4, 0x7fffffff

    cmp-long v4, v2, v4

    if-lez v4, :cond_1

    .line 48
    new-instance v1, Lcom/google/obf/s;

    iget v0, v0, Lcom/google/obf/co$a;->a:I

    const/16 v2, 0x33

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Chunk is too large (~2GB+) to skip; id: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v1

    .line 49
    :cond_1
    long-to-int v0, v2

    invoke-interface {p0, v0}, Lcom/google/obf/ak;->b(I)V

    .line 50
    invoke-static {p0, v1}, Lcom/google/obf/co$a;->a(Lcom/google/obf/ak;Lcom/google/obf/dw;)Lcom/google/obf/co$a;

    move-result-object v0

    goto :goto_0

    .line 52
    :cond_2
    invoke-interface {p0, v6}, Lcom/google/obf/ak;->b(I)V

    .line 53
    invoke-interface {p0}, Lcom/google/obf/ak;->c()J

    move-result-wide v2

    iget-wide v0, v0, Lcom/google/obf/co$a;->b:J

    invoke-virtual {p1, v2, v3, v0, v1}, Lcom/google/obf/cn;->a(JJ)V

    .line 54
    return-void
.end method
