.class public final Lcom/my/target/common/CustomParams;
.super Lcom/my/target/bm;
.source "CustomParams.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/common/CustomParams$Gender;
    }
.end annotation


# instance fields
.field private emails:[Ljava/lang/String;

.field private icqIds:[Ljava/lang/String;

.field private okIds:[Ljava/lang/String;

.field private vkIds:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/my/target/bm;-><init>()V

    .line 28
    return-void
.end method


# virtual methods
.method public collectData(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 251
    return-void
.end method

.method public getAge()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 298
    const-string v1, "a"

    invoke-virtual {p0, v1}, Lcom/my/target/common/CustomParams;->getParam(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 299
    if-nez v1, :cond_0

    .line 309
    :goto_0
    return v0

    .line 305
    :cond_0
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 307
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public getCustomParam(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 334
    invoke-virtual {p0, p1}, Lcom/my/target/common/CustomParams;->getParam(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/my/target/common/CustomParams;->emails:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/common/CustomParams;->emails:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/my/target/common/CustomParams;->emails:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 66
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getEmails()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/my/target/common/CustomParams;->emails:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 87
    const/4 v0, 0x0

    .line 89
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/my/target/common/CustomParams;->emails:[Ljava/lang/String;

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getGender()I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 265
    const-string v1, "g"

    invoke-virtual {p0, v1}, Lcom/my/target/common/CustomParams;->getParam(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 266
    if-nez v1, :cond_0

    .line 276
    :goto_0
    return v0

    .line 272
    :cond_0
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 274
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public getIcqId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/my/target/common/CustomParams;->icqIds:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/common/CustomParams;->icqIds:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/my/target/common/CustomParams;->icqIds:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 114
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIcqIds()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/my/target/common/CustomParams;->icqIds:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 134
    const/4 v0, 0x0

    .line 136
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/my/target/common/CustomParams;->icqIds:[Ljava/lang/String;

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getLang()Ljava/lang/String;
    .locals 1

    .prologue
    .line 260
    const-string v0, "lang"

    invoke-virtual {p0, v0}, Lcom/my/target/common/CustomParams;->getParam(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMrgsAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    const-string v0, "mrgs_app_id"

    invoke-virtual {p0, v0}, Lcom/my/target/common/CustomParams;->getParam(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMrgsId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const-string v0, "mrgs_device_id"

    invoke-virtual {p0, v0}, Lcom/my/target/common/CustomParams;->getParam(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMrgsUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    const-string v0, "mrgs_user_id"

    invoke-virtual {p0, v0}, Lcom/my/target/common/CustomParams;->getParam(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOkId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/my/target/common/CustomParams;->okIds:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/common/CustomParams;->okIds:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/my/target/common/CustomParams;->okIds:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 161
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOkIds()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/my/target/common/CustomParams;->okIds:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 181
    const/4 v0, 0x0

    .line 183
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/my/target/common/CustomParams;->okIds:[Ljava/lang/String;

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getVKId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/my/target/common/CustomParams;->vkIds:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/common/CustomParams;->vkIds:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/my/target/common/CustomParams;->vkIds:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 208
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVKIds()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/my/target/common/CustomParams;->vkIds:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 228
    const/4 v0, 0x0

    .line 230
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/my/target/common/CustomParams;->vkIds:[Ljava/lang/String;

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method public setAge(I)V
    .locals 2
    .param p1, "age"    # I

    .prologue
    .line 315
    if-ltz p1, :cond_0

    .line 317
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "age param set to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 318
    const-string v0, "a"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/my/target/common/CustomParams;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 325
    :goto_0
    return-void

    .line 322
    :cond_0
    const-string v0, "age param removed"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 323
    const-string v0, "a"

    invoke-virtual {p0, v0}, Lcom/my/target/common/CustomParams;->removeParam(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public setCustomParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 329
    invoke-virtual {p0, p1, p2}, Lcom/my/target/common/CustomParams;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 330
    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 2
    .param p1, "email"    # Ljava/lang/String;

    .prologue
    .line 71
    if-nez p1, :cond_0

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/common/CustomParams;->emails:[Ljava/lang/String;

    .line 80
    :goto_0
    const-string v0, "email"

    invoke-virtual {p0, v0, p1}, Lcom/my/target/common/CustomParams;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 81
    return-void

    .line 77
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    iput-object v0, p0, Lcom/my/target/common/CustomParams;->emails:[Ljava/lang/String;

    goto :goto_0
.end method

.method public setEmails([Ljava/lang/String;)V
    .locals 3
    .param p1, "emails"    # [Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 95
    if-nez p1, :cond_0

    .line 97
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/common/CustomParams;->emails:[Ljava/lang/String;

    .line 98
    const-string v0, "email"

    invoke-virtual {p0, v0}, Lcom/my/target/common/CustomParams;->removeParam(Ljava/lang/String;)Z

    .line 106
    :goto_0
    return-void

    .line 102
    :cond_0
    array-length v0, p1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/my/target/common/CustomParams;->emails:[Ljava/lang/String;

    .line 103
    iget-object v0, p0, Lcom/my/target/common/CustomParams;->emails:[Ljava/lang/String;

    array-length v1, p1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 104
    const-string v0, "email"

    invoke-static {p1}, Lcom/my/target/cd;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/my/target/common/CustomParams;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public setGender(I)V
    .locals 2
    .param p1, "gender"    # I

    .prologue
    .line 282
    packed-switch p1, :pswitch_data_0

    .line 291
    const-string v0, "g"

    invoke-virtual {p0, v0}, Lcom/my/target/common/CustomParams;->removeParam(Ljava/lang/String;)Z

    .line 292
    const-string v0, "gender param removed"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 294
    :goto_0
    return-void

    .line 287
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "gender param is set to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 288
    const-string v0, "g"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/my/target/common/CustomParams;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 282
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setIcqId(Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 119
    if-nez p1, :cond_0

    .line 121
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/common/CustomParams;->icqIds:[Ljava/lang/String;

    .line 127
    :goto_0
    const-string v0, "icq_id"

    invoke-virtual {p0, v0, p1}, Lcom/my/target/common/CustomParams;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 128
    return-void

    .line 125
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    iput-object v0, p0, Lcom/my/target/common/CustomParams;->icqIds:[Ljava/lang/String;

    goto :goto_0
.end method

.method public setIcqIds([Ljava/lang/String;)V
    .locals 3
    .param p1, "ids"    # [Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 142
    if-nez p1, :cond_0

    .line 144
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/common/CustomParams;->icqIds:[Ljava/lang/String;

    .line 145
    const-string v0, "icq_id"

    invoke-virtual {p0, v0}, Lcom/my/target/common/CustomParams;->removeParam(Ljava/lang/String;)Z

    .line 153
    :goto_0
    return-void

    .line 149
    :cond_0
    array-length v0, p1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/my/target/common/CustomParams;->icqIds:[Ljava/lang/String;

    .line 150
    iget-object v0, p0, Lcom/my/target/common/CustomParams;->icqIds:[Ljava/lang/String;

    array-length v1, p1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 151
    const-string v0, "icq_id"

    invoke-static {p1}, Lcom/my/target/cd;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/my/target/common/CustomParams;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public setLang(Ljava/lang/String;)V
    .locals 1
    .param p1, "lang"    # Ljava/lang/String;

    .prologue
    .line 255
    const-string v0, "lang"

    invoke-virtual {p0, v0, p1}, Lcom/my/target/common/CustomParams;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 256
    return-void
.end method

.method public setMrgsAppId(Ljava/lang/String;)V
    .locals 1
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 37
    const-string v0, "mrgs_app_id"

    invoke-virtual {p0, v0, p1}, Lcom/my/target/common/CustomParams;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 38
    return-void
.end method

.method public setMrgsId(Ljava/lang/String;)V
    .locals 1
    .param p1, "mrgsId"    # Ljava/lang/String;

    .prologue
    .line 57
    const-string v0, "mrgs_device_id"

    invoke-virtual {p0, v0, p1}, Lcom/my/target/common/CustomParams;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 58
    return-void
.end method

.method public setMrgsUserId(Ljava/lang/String;)V
    .locals 1
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 47
    const-string v0, "mrgs_user_id"

    invoke-virtual {p0, v0, p1}, Lcom/my/target/common/CustomParams;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 48
    return-void
.end method

.method public setOkId(Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 166
    if-nez p1, :cond_0

    .line 168
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/common/CustomParams;->okIds:[Ljava/lang/String;

    .line 174
    :goto_0
    const-string v0, "ok_id"

    invoke-virtual {p0, v0, p1}, Lcom/my/target/common/CustomParams;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 175
    return-void

    .line 172
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    iput-object v0, p0, Lcom/my/target/common/CustomParams;->okIds:[Ljava/lang/String;

    goto :goto_0
.end method

.method public setOkIds([Ljava/lang/String;)V
    .locals 3
    .param p1, "ids"    # [Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 189
    if-nez p1, :cond_0

    .line 191
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/common/CustomParams;->okIds:[Ljava/lang/String;

    .line 192
    const-string v0, "ok_id"

    invoke-virtual {p0, v0}, Lcom/my/target/common/CustomParams;->removeParam(Ljava/lang/String;)Z

    .line 200
    :goto_0
    return-void

    .line 196
    :cond_0
    array-length v0, p1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/my/target/common/CustomParams;->okIds:[Ljava/lang/String;

    .line 197
    iget-object v0, p0, Lcom/my/target/common/CustomParams;->okIds:[Ljava/lang/String;

    array-length v1, p1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 198
    const-string v0, "ok_id"

    invoke-static {p1}, Lcom/my/target/cd;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/my/target/common/CustomParams;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public setVKId(Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 213
    if-nez p1, :cond_0

    .line 215
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/common/CustomParams;->vkIds:[Ljava/lang/String;

    .line 221
    :goto_0
    const-string v0, "vk_id"

    invoke-virtual {p0, v0, p1}, Lcom/my/target/common/CustomParams;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 222
    return-void

    .line 219
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    iput-object v0, p0, Lcom/my/target/common/CustomParams;->vkIds:[Ljava/lang/String;

    goto :goto_0
.end method

.method public setVKIds([Ljava/lang/String;)V
    .locals 3
    .param p1, "ids"    # [Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 235
    if-nez p1, :cond_0

    .line 237
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/common/CustomParams;->vkIds:[Ljava/lang/String;

    .line 238
    const-string v0, "vk_id"

    invoke-virtual {p0, v0}, Lcom/my/target/common/CustomParams;->removeParam(Ljava/lang/String;)Z

    .line 246
    :goto_0
    return-void

    .line 242
    :cond_0
    array-length v0, p1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/my/target/common/CustomParams;->vkIds:[Ljava/lang/String;

    .line 243
    iget-object v0, p0, Lcom/my/target/common/CustomParams;->vkIds:[Ljava/lang/String;

    array-length v1, p1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 244
    const-string v0, "vk_id"

    invoke-static {p1}, Lcom/my/target/cd;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/my/target/common/CustomParams;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method
