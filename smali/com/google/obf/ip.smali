.class Lcom/google/obf/ip;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/in;


# instance fields
.field private a:Lcom/google/obf/kb;

.field private b:[B

.field private final c:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lcom/google/obf/ip;->c:I

    .line 3
    invoke-virtual {p0}, Lcom/google/obf/ip;->a()V

    .line 4
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 5
    iget v0, p0, Lcom/google/obf/ip;->c:I

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/obf/ip;->b:[B

    .line 6
    iget-object v0, p0, Lcom/google/obf/ip;->b:[B

    invoke-static {v0}, Lcom/google/obf/kb;->a([B)Lcom/google/obf/kb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/ip;->a:Lcom/google/obf/kb;

    .line 7
    return-void
.end method

.method public a(IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    iget-object v0, p0, Lcom/google/obf/ip;->a:Lcom/google/obf/kb;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/obf/kb;->a(IJ)V

    .line 9
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 10
    iget-object v0, p0, Lcom/google/obf/ip;->a:Lcom/google/obf/kb;

    invoke-virtual {v0, p1, p2}, Lcom/google/obf/kb;->a(ILjava/lang/String;)V

    .line 11
    return-void
.end method

.method public b()[B
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 12
    iget-object v0, p0, Lcom/google/obf/ip;->a:Lcom/google/obf/kb;

    invoke-virtual {v0}, Lcom/google/obf/kb;->a()I

    move-result v0

    .line 13
    if-gez v0, :cond_0

    .line 14
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 15
    :cond_0
    if-nez v0, :cond_1

    .line 16
    iget-object v0, p0, Lcom/google/obf/ip;->b:[B

    .line 19
    :goto_0
    return-object v0

    .line 17
    :cond_1
    iget-object v1, p0, Lcom/google/obf/ip;->b:[B

    array-length v1, v1

    sub-int v0, v1, v0

    new-array v0, v0, [B

    .line 18
    iget-object v1, p0, Lcom/google/obf/ip;->b:[B

    array-length v2, v0

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method
