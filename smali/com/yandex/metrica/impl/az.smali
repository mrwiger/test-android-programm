.class public Lcom/yandex/metrica/impl/az;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/yandex/metrica/impl/t;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lcom/yandex/metrica/impl/ad;

.field private final c:Lcom/yandex/metrica/impl/NativeCrashesHelper;

.field private final d:Ljava/util/concurrent/ExecutorService;

.field private e:Lcom/yandex/metrica/impl/z;

.field private f:Lcom/yandex/metrica/impl/v;

.field private g:Lcom/yandex/metrica/impl/ob/fz;

.field private final h:Lcom/yandex/metrica/impl/ba;


# direct methods
.method constructor <init>(Ljava/util/concurrent/ExecutorService;Landroid/content/Context;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Lcom/yandex/metrica/impl/ad;

    invoke-direct {v0, p2, p3}, Lcom/yandex/metrica/impl/ad;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/az;->b:Lcom/yandex/metrica/impl/ad;

    .line 49
    iput-object p1, p0, Lcom/yandex/metrica/impl/az;->d:Ljava/util/concurrent/ExecutorService;

    .line 51
    iput-object p2, p0, Lcom/yandex/metrica/impl/az;->a:Landroid/content/Context;

    .line 52
    new-instance v0, Lcom/yandex/metrica/impl/NativeCrashesHelper;

    invoke-direct {v0, p2}, Lcom/yandex/metrica/impl/NativeCrashesHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/az;->c:Lcom/yandex/metrica/impl/NativeCrashesHelper;

    .line 55
    new-instance v0, Lcom/yandex/metrica/impl/v;

    invoke-direct {v0, p2}, Lcom/yandex/metrica/impl/v;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/az;->f:Lcom/yandex/metrica/impl/v;

    .line 57
    new-instance v0, Lcom/yandex/metrica/impl/ba;

    invoke-direct {v0, p0}, Lcom/yandex/metrica/impl/ba;-><init>(Lcom/yandex/metrica/impl/t;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/az;->h:Lcom/yandex/metrica/impl/ba;

    .line 58
    return-void
.end method

.method private a(Lcom/yandex/metrica/impl/ba$d;)Ljava/util/concurrent/Future;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/yandex/metrica/impl/ba$d;",
            ")",
            "Ljava/util/concurrent/Future",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 232
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ba$d;->a()Lcom/yandex/metrica/impl/ax;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/az;->g:Lcom/yandex/metrica/impl/ob/fz;

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ax;->a(Lcom/yandex/metrica/impl/ob/fz;)V

    .line 234
    iget-object v0, p0, Lcom/yandex/metrica/impl/az;->h:Lcom/yandex/metrica/impl/ba;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ba;->a(Lcom/yandex/metrica/impl/ba$d;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)Lcom/yandex/metrica/impl/i;
    .locals 1

    .prologue
    .line 32
    invoke-static {p0, p1}, Lcom/yandex/metrica/impl/az;->c(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    return-object v0
.end method

.method private static c(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)Lcom/yandex/metrica/impl/i;
    .locals 2

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/i;->c()I

    move-result v0

    sget-object v1, Lcom/yandex/metrica/impl/q$a;->g:Lcom/yandex/metrica/impl/q$a;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 96
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ax;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/i;->e(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    .line 98
    :cond_0
    return-object p0
.end method


# virtual methods
.method public a()Lcom/yandex/metrica/impl/ad;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/yandex/metrica/impl/az;->b:Lcom/yandex/metrica/impl/ad;

    return-object v0
.end method

.method public a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;Ljava/util/Map;)Ljava/util/concurrent/Future;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/yandex/metrica/impl/i;",
            "Lcom/yandex/metrica/impl/ax;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/concurrent/Future",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Lcom/yandex/metrica/impl/az;->b:Lcom/yandex/metrica/impl/ad;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ad;->c()V

    .line 107
    new-instance v0, Lcom/yandex/metrica/impl/ba$d;

    invoke-direct {v0, p1, p2}, Lcom/yandex/metrica/impl/ba$d;-><init>(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)V

    .line 108
    invoke-static {p3}, Lcom/yandex/metrica/impl/bl;->a(Ljava/util/Map;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 109
    new-instance v1, Lcom/yandex/metrica/impl/az$1;

    invoke-direct {v1, p3, p2}, Lcom/yandex/metrica/impl/az$1;-><init>(Ljava/util/Map;Lcom/yandex/metrica/impl/ax;)V

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ba$d;->a(Lcom/yandex/metrica/impl/ba$c;)Lcom/yandex/metrica/impl/ba$d;

    .line 115
    :cond_0
    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/az;->a(Lcom/yandex/metrica/impl/ba$d;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/yandex/metrica/IMetricaService;Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 184
    invoke-virtual {p0, p3}, Lcom/yandex/metrica/impl/az;->c(Lcom/yandex/metrica/impl/ax;)V

    .line 187
    invoke-virtual {p3}, Lcom/yandex/metrica/impl/ax;->b()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/yandex/metrica/impl/az;->c:Lcom/yandex/metrica/impl/NativeCrashesHelper;

    iget-object v1, p0, Lcom/yandex/metrica/impl/az;->d:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {v0, p0, v1}, Lcom/yandex/metrica/impl/NativeCrashesHelper;->a(Lcom/yandex/metrica/impl/az;Ljava/util/concurrent/ExecutorService;)V

    .line 1227
    :cond_0
    invoke-virtual {p3}, Lcom/yandex/metrica/impl/ax;->c()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/yandex/metrica/impl/i;->a(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/yandex/metrica/IMetricaService;->reportData(Landroid/os/Bundle;)V

    .line 2217
    iget-object v0, p0, Lcom/yandex/metrica/impl/az;->e:Lcom/yandex/metrica/impl/z;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/yandex/metrica/impl/az;->e:Lcom/yandex/metrica/impl/z;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/z;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2218
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/az;->b:Lcom/yandex/metrica/impl/ad;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ad;->b()V

    .line 196
    :cond_2
    return-void
.end method

.method public a(Lcom/yandex/metrica/impl/ax;)V
    .locals 1

    .prologue
    .line 127
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ax;->g()Lcom/yandex/metrica/impl/ao;

    move-result-object v0

    invoke-static {v0}, Lcom/yandex/metrica/impl/q;->a(Lcom/yandex/metrica/impl/ao;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/az;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)V

    .line 128
    return-void
.end method

.method a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)V
    .locals 2

    .prologue
    .line 102
    invoke-static {p1, p2}, Lcom/yandex/metrica/impl/az;->c(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p2, v1}, Lcom/yandex/metrica/impl/az;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;Ljava/util/Map;)Ljava/util/concurrent/Future;

    .line 103
    return-void
.end method

.method a(Lcom/yandex/metrica/impl/k;)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/yandex/metrica/impl/az;->f:Lcom/yandex/metrica/impl/v;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/v;->a(Lcom/yandex/metrica/impl/k;)V

    .line 73
    return-void
.end method

.method a(Lcom/yandex/metrica/impl/ob/fz;)V
    .locals 1

    .prologue
    .line 65
    iput-object p1, p0, Lcom/yandex/metrica/impl/az;->g:Lcom/yandex/metrica/impl/ob/fz;

    .line 68
    iget-object v0, p0, Lcom/yandex/metrica/impl/az;->f:Lcom/yandex/metrica/impl/v;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/v;->b(Lcom/yandex/metrica/impl/ob/fz;)V

    .line 69
    return-void
.end method

.method a(Lcom/yandex/metrica/impl/z;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/yandex/metrica/impl/az;->e:Lcom/yandex/metrica/impl/z;

    .line 62
    return-void
.end method

.method a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/yandex/metrica/impl/az;->e:Lcom/yandex/metrica/impl/z;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/z;->d()Lcom/yandex/metrica/impl/ax;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/yandex/metrica/impl/az;->a(Ljava/lang/String;Lcom/yandex/metrica/impl/ax;)V

    .line 92
    return-void
.end method

.method a(Ljava/lang/String;Lcom/yandex/metrica/impl/ax;)V
    .locals 2

    .prologue
    .line 85
    invoke-static {}, Lcom/yandex/metrica/impl/utils/l;->f()Lcom/yandex/metrica/impl/utils/l;

    move-result-object v0

    const-string v1, "Error received: native"

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/utils/l;->a(Ljava/lang/String;)V

    .line 87
    sget-object v0, Lcom/yandex/metrica/impl/q$a;->o:Lcom/yandex/metrica/impl/q$a;

    invoke-static {v0, p1}, Lcom/yandex/metrica/impl/q;->a(Lcom/yandex/metrica/impl/q$a;Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/yandex/metrica/impl/az;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)V

    .line 88
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/yandex/metrica/impl/ax;)V
    .locals 3

    .prologue
    .line 199
    new-instance v0, Lcom/yandex/metrica/impl/ba$d;

    .line 3099
    new-instance v1, Lcom/yandex/metrica/impl/i;

    invoke-direct {v1}, Lcom/yandex/metrica/impl/i;-><init>()V

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->w:Lcom/yandex/metrica/impl/q$a;

    .line 3100
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/i;->a(I)Lcom/yandex/metrica/impl/i;

    move-result-object v1

    .line 3101
    invoke-virtual {v1, p1, p2}, Lcom/yandex/metrica/impl/i;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v1

    .line 200
    invoke-direct {v0, v1, p3}, Lcom/yandex/metrica/impl/ba$d;-><init>(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)V

    .line 199
    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/az;->a(Lcom/yandex/metrica/impl/ba$d;)Ljava/util/concurrent/Future;

    .line 201
    return-void
.end method

.method a(Ljava/lang/Throwable;Lcom/yandex/metrica/impl/ax;)V
    .locals 2

    .prologue
    .line 147
    invoke-virtual {p2}, Lcom/yandex/metrica/impl/ax;->b()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    invoke-static {}, Lcom/yandex/metrica/impl/utils/l;->f()Lcom/yandex/metrica/impl/utils/l;

    move-result-object v0

    const-string v1, "Error received: uncaught"

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/utils/l;->a(Ljava/lang/String;)V

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/az;->b:Lcom/yandex/metrica/impl/ad;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ad;->c()V

    .line 154
    const/4 v0, 0x0

    invoke-static {v0, p1}, Lcom/yandex/metrica/impl/bl;->a(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    .line 155
    if-nez p1, :cond_1

    const-string v0, ""

    :goto_0
    invoke-static {v0, v1}, Lcom/yandex/metrica/impl/q;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    .line 158
    invoke-virtual {p2}, Lcom/yandex/metrica/impl/ax;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/i;->e(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    .line 160
    :try_start_0
    new-instance v1, Lcom/yandex/metrica/impl/ba$d;

    invoke-direct {v1, v0, p2}, Lcom/yandex/metrica/impl/ba$d;-><init>(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/ba$d;->a(Z)Lcom/yandex/metrica/impl/ba$d;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/az;->a(Lcom/yandex/metrica/impl/ba$d;)Ljava/util/concurrent/Future;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    :goto_1
    return-void

    .line 156
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 166
    :catch_0
    move-exception v0

    goto :goto_1

    .line 165
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 131
    iget-object v0, p0, Lcom/yandex/metrica/impl/az;->f:Lcom/yandex/metrica/impl/v;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/v;->b()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/CounterConfiguration;->a(Ljava/util/List;)V

    .line 132
    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lcom/yandex/metrica/impl/az;->f:Lcom/yandex/metrica/impl/v;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/v;->b()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/CounterConfiguration;->a(Ljava/util/Map;)V

    .line 136
    return-void
.end method

.method a(ZLcom/yandex/metrica/impl/ax;)V
    .locals 1

    .prologue
    .line 76
    invoke-virtual {p2}, Lcom/yandex/metrica/impl/ax;->b()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/CounterConfiguration;->b(Z)V

    .line 77
    iget-object v0, p0, Lcom/yandex/metrica/impl/az;->c:Lcom/yandex/metrica/impl/NativeCrashesHelper;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/NativeCrashesHelper;->a(Z)V

    .line 78
    return-void
.end method

.method public b()Landroid/content/Context;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/yandex/metrica/impl/az;->a:Landroid/content/Context;

    return-object v0
.end method

.method public b(Lcom/yandex/metrica/impl/ax;)V
    .locals 3

    .prologue
    .line 204
    new-instance v0, Lcom/yandex/metrica/impl/ba$d;

    .line 3105
    new-instance v1, Lcom/yandex/metrica/impl/i;

    invoke-direct {v1}, Lcom/yandex/metrica/impl/i;-><init>()V

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->x:Lcom/yandex/metrica/impl/q$a;

    .line 3106
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/i;->a(I)Lcom/yandex/metrica/impl/i;

    move-result-object v1

    .line 205
    invoke-direct {v0, v1, p1}, Lcom/yandex/metrica/impl/ba$d;-><init>(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)V

    .line 204
    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/az;->a(Lcom/yandex/metrica/impl/ba$d;)Ljava/util/concurrent/Future;

    .line 206
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 123
    invoke-static {p1}, Lcom/yandex/metrica/impl/q;->d(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/az;->f:Lcom/yandex/metrica/impl/v;

    invoke-virtual {p0, v0, v1}, Lcom/yandex/metrica/impl/az;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)V

    .line 124
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 119
    sget-object v0, Lcom/yandex/metrica/impl/q$a;->q:Lcom/yandex/metrica/impl/q$a;

    invoke-static {v0}, Lcom/yandex/metrica/impl/q;->d(Lcom/yandex/metrica/impl/q$a;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/az;->f:Lcom/yandex/metrica/impl/v;

    invoke-virtual {p0, v0, v1}, Lcom/yandex/metrica/impl/az;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)V

    .line 120
    return-void
.end method

.method c(Lcom/yandex/metrica/impl/ax;)V
    .locals 2

    .prologue
    .line 210
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ax;->b()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ax;->b()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-static {}, Lcom/yandex/metrica/impl/utils/l;->f()Lcom/yandex/metrica/impl/utils/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/utils/l;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/CounterConfiguration;->e(Z)V

    .line 213
    :cond_0
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/yandex/metrica/impl/az;->f:Lcom/yandex/metrica/impl/v;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/v;->b()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/CounterConfiguration;->h(Ljava/lang/String;)V

    .line 140
    return-void
.end method

.method d()V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/yandex/metrica/impl/az;->b:Lcom/yandex/metrica/impl/ad;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ad;->c()V

    .line 171
    return-void
.end method

.method e()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/yandex/metrica/impl/az;->b:Lcom/yandex/metrica/impl/ad;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ad;->b()V

    .line 176
    return-void
.end method
