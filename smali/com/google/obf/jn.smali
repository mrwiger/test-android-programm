.class public abstract Lcom/google/obf/jn;
.super Lcom/google/obf/jj;
.source "IMASDK"

# interfaces
.implements Ljava/util/Set;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/obf/jj",
        "<TE;>;",
        "Ljava/util/Set",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private transient a:Lcom/google/obf/jl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/jl",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/obf/jj;-><init>()V

    return-void
.end method

.method static a(I)I
    .locals 6

    .prologue
    const/high16 v1, 0x40000000    # 2.0f

    .line 1
    const v0, 0x2ccccccc

    if-ge p0, v0, :cond_0

    .line 2
    add-int/lit8 v0, p0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v0

    shl-int/lit8 v0, v0, 0x1

    .line 3
    :goto_0
    int-to-double v2, v0

    const-wide v4, 0x3fe6666666666666L    # 0.7

    mul-double/2addr v2, v4

    int-to-double v4, p0

    cmpg-double v1, v2, v4

    if-gez v1, :cond_1

    .line 4
    shl-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6
    :cond_0
    if-ge p0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    const-string v2, "collection too large"

    invoke-static {v0, v2}, Lcom/google/obf/ja;->a(ZLjava/lang/Object;)V

    move v0, v1

    .line 7
    :cond_1
    return v0

    .line 6
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public abstract a()Lcom/google/obf/jy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/obf/jy",
            "<TE;>;"
        }
    .end annotation
.end method

.method public b()Lcom/google/obf/jl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/obf/jl",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/obf/jn;->a:Lcom/google/obf/jl;

    .line 20
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/obf/jn;->e()Lcom/google/obf/jl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/jn;->a:Lcom/google/obf/jl;

    :cond_0
    return-object v0
.end method

.method d()Z
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    return v0
.end method

.method e()Lcom/google/obf/jl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/obf/jl",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/google/obf/jn;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/obf/jl;->a([Ljava/lang/Object;)Lcom/google/obf/jl;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 10
    if-ne p1, p0, :cond_0

    .line 11
    const/4 v0, 0x1

    .line 17
    :goto_0
    return v0

    .line 12
    :cond_0
    instance-of v0, p1, Lcom/google/obf/jn;

    if-eqz v0, :cond_1

    .line 13
    invoke-virtual {p0}, Lcom/google/obf/jn;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/google/obf/jn;

    .line 14
    invoke-virtual {v0}, Lcom/google/obf/jn;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 15
    invoke-virtual {p0}, Lcom/google/obf/jn;->hashCode()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 16
    const/4 v0, 0x0

    goto :goto_0

    .line 17
    :cond_1
    invoke-static {p0, p1}, Lcom/google/obf/jw;->a(Ljava/util/Set;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 18
    invoke-static {p0}, Lcom/google/obf/jw;->a(Ljava/util/Set;)I

    move-result v0

    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/google/obf/jn;->a()Lcom/google/obf/jy;

    move-result-object v0

    return-object v0
.end method
