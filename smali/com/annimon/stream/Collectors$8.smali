.class final Lcom/annimon/stream/Collectors$8;
.super Ljava/lang/Object;
.source "Collectors.java"

# interfaces
.implements Lcom/annimon/stream/function/BiConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/annimon/stream/Collectors;->toMap(Lcom/annimon/stream/function/Function;Lcom/annimon/stream/function/Function;Lcom/annimon/stream/function/Supplier;)Lcom/annimon/stream/Collector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/annimon/stream/function/BiConsumer",
        "<TM;TT;>;"
    }
.end annotation


# instance fields
.field final synthetic val$keyMapper:Lcom/annimon/stream/function/Function;

.field final synthetic val$valueMapper:Lcom/annimon/stream/function/Function;


# direct methods
.method constructor <init>(Lcom/annimon/stream/function/Function;Lcom/annimon/stream/function/Function;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/annimon/stream/Collectors$8;->val$keyMapper:Lcom/annimon/stream/function/Function;

    iput-object p2, p0, Lcom/annimon/stream/Collectors$8;->val$valueMapper:Lcom/annimon/stream/function/Function;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 160
    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1, p2}, Lcom/annimon/stream/Collectors$8;->accept(Ljava/util/Map;Ljava/lang/Object;)V

    return-void
.end method

.method public accept(Ljava/util/Map;Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;TT;)V"
        }
    .end annotation

    .prologue
    .line 163
    .local p1, "map":Ljava/util/Map;, "TM;"
    .local p2, "t":Ljava/lang/Object;, "TT;"
    iget-object v4, p0, Lcom/annimon/stream/Collectors$8;->val$keyMapper:Lcom/annimon/stream/function/Function;

    invoke-interface {v4, p2}, Lcom/annimon/stream/function/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 164
    .local v0, "key":Ljava/lang/Object;, "TK;"
    iget-object v4, p0, Lcom/annimon/stream/Collectors$8;->val$valueMapper:Lcom/annimon/stream/function/Function;

    invoke-interface {v4, p2}, Lcom/annimon/stream/function/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 165
    .local v3, "value":Ljava/lang/Object;, "TV;"
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 166
    .local v2, "oldValue":Ljava/lang/Object;, "TV;"
    if-nez v2, :cond_0

    move-object v1, v3

    .line 167
    .local v1, "newValue":Ljava/lang/Object;, "TV;"
    :goto_0
    if-nez v1, :cond_1

    .line 168
    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    :goto_1
    return-void

    .end local v1    # "newValue":Ljava/lang/Object;, "TV;"
    :cond_0
    move-object v1, v2

    .line 166
    goto :goto_0

    .line 170
    .restart local v1    # "newValue":Ljava/lang/Object;, "TV;"
    :cond_1
    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method
