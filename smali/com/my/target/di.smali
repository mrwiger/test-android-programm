.class public final Lcom/my/target/di;
.super Landroid/widget/FrameLayout;
.source "ContainerAdView.java"


# instance fields
.field private aq:I

.field private ar:I

.field private as:Z

.field private maxWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 23
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 0

    .prologue
    .line 53
    iput p1, p0, Lcom/my/target/di;->aq:I

    .line 54
    iput p2, p0, Lcom/my/target/di;->ar:I

    .line 55
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 4
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v2, -0x80000000

    .line 60
    iget v0, p0, Lcom/my/target/di;->aq:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/my/target/di;->ar:I

    if-gtz v0, :cond_1

    .line 62
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 82
    :goto_0
    return-void

    .line 66
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 67
    iget-boolean v1, p0, Lcom/my/target/di;->as:Z

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/my/target/di;->aq:I

    if-ge v1, v0, :cond_3

    .line 69
    iget v1, p0, Lcom/my/target/di;->maxWidth:I

    if-lez v1, :cond_2

    .line 71
    iget v1, p0, Lcom/my/target/di;->maxWidth:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 73
    :cond_2
    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iget v1, p0, Lcom/my/target/di;->ar:I

    .line 74
    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 73
    invoke-super {p0, v0, v1}, Landroid/widget/FrameLayout;->onMeasure(II)V

    goto :goto_0

    .line 78
    :cond_3
    iget v0, p0, Lcom/my/target/di;->aq:I

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iget v1, p0, Lcom/my/target/di;->ar:I

    .line 79
    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 78
    invoke-super {p0, v0, v1}, Landroid/widget/FrameLayout;->onMeasure(II)V

    goto :goto_0
.end method

.method public final setFlexibleWidth(Z)V
    .locals 0
    .param p1, "flexibleWidth"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/my/target/di;->as:Z

    .line 44
    return-void
.end method

.method public final setMaxWidth(I)V
    .locals 0
    .param p1, "maxWidth"    # I

    .prologue
    .line 48
    iput p1, p0, Lcom/my/target/di;->maxWidth:I

    .line 49
    return-void
.end method
