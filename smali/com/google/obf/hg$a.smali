.class public final enum Lcom/google/obf/hg$a;
.super Ljava/lang/Enum;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/hg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/obf/hg$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/obf/hg$a;

.field public static final enum b:Lcom/google/obf/hg$a;

.field public static final enum c:Lcom/google/obf/hg$a;

.field public static final enum d:Lcom/google/obf/hg$a;

.field private static final synthetic e:[Lcom/google/obf/hg$a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5
    new-instance v0, Lcom/google/obf/hg$a;

    const-string v1, "VERBOSE"

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hg$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hg$a;->a:Lcom/google/obf/hg$a;

    .line 6
    new-instance v0, Lcom/google/obf/hg$a;

    const-string v1, "ABRIDGED"

    invoke-direct {v0, v1, v3}, Lcom/google/obf/hg$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hg$a;->b:Lcom/google/obf/hg$a;

    .line 7
    new-instance v0, Lcom/google/obf/hg$a;

    const-string v1, "LIFECYCLE"

    invoke-direct {v0, v1, v4}, Lcom/google/obf/hg$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hg$a;->c:Lcom/google/obf/hg$a;

    .line 8
    new-instance v0, Lcom/google/obf/hg$a;

    const-string v1, "PROD"

    invoke-direct {v0, v1, v5}, Lcom/google/obf/hg$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hg$a;->d:Lcom/google/obf/hg$a;

    .line 9
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/obf/hg$a;

    sget-object v1, Lcom/google/obf/hg$a;->a:Lcom/google/obf/hg$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/obf/hg$a;->b:Lcom/google/obf/hg$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/obf/hg$a;->c:Lcom/google/obf/hg$a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/obf/hg$a;->d:Lcom/google/obf/hg$a;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/obf/hg$a;->e:[Lcom/google/obf/hg$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Lcom/google/obf/hg$a;)Z
    .locals 2

    .prologue
    .line 4
    invoke-virtual {p0}, Lcom/google/obf/hg$a;->ordinal()I

    move-result v0

    sget-object v1, Lcom/google/obf/hg;->a:Lcom/google/obf/hg$a;

    invoke-virtual {v1}, Lcom/google/obf/hg$a;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/obf/hg$a;
    .locals 1

    .prologue
    .line 2
    const-class v0, Lcom/google/obf/hg$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/obf/hg$a;

    return-object v0
.end method

.method public static values()[Lcom/google/obf/hg$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lcom/google/obf/hg$a;->e:[Lcom/google/obf/hg$a;

    invoke-virtual {v0}, [Lcom/google/obf/hg$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/obf/hg$a;

    return-object v0
.end method
