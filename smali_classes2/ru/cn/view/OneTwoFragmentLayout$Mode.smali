.class public final enum Lru/cn/view/OneTwoFragmentLayout$Mode;
.super Ljava/lang/Enum;
.source "OneTwoFragmentLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/view/OneTwoFragmentLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Mode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/view/OneTwoFragmentLayout$Mode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/view/OneTwoFragmentLayout$Mode;

.field public static final enum MODE_1:Lru/cn/view/OneTwoFragmentLayout$Mode;

.field public static final enum MODE_2:Lru/cn/view/OneTwoFragmentLayout$Mode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 34
    new-instance v0, Lru/cn/view/OneTwoFragmentLayout$Mode;

    const-string v1, "MODE_1"

    invoke-direct {v0, v1, v2}, Lru/cn/view/OneTwoFragmentLayout$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/view/OneTwoFragmentLayout$Mode;->MODE_1:Lru/cn/view/OneTwoFragmentLayout$Mode;

    new-instance v0, Lru/cn/view/OneTwoFragmentLayout$Mode;

    const-string v1, "MODE_2"

    invoke-direct {v0, v1, v3}, Lru/cn/view/OneTwoFragmentLayout$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/view/OneTwoFragmentLayout$Mode;->MODE_2:Lru/cn/view/OneTwoFragmentLayout$Mode;

    .line 33
    const/4 v0, 0x2

    new-array v0, v0, [Lru/cn/view/OneTwoFragmentLayout$Mode;

    sget-object v1, Lru/cn/view/OneTwoFragmentLayout$Mode;->MODE_1:Lru/cn/view/OneTwoFragmentLayout$Mode;

    aput-object v1, v0, v2

    sget-object v1, Lru/cn/view/OneTwoFragmentLayout$Mode;->MODE_2:Lru/cn/view/OneTwoFragmentLayout$Mode;

    aput-object v1, v0, v3

    sput-object v0, Lru/cn/view/OneTwoFragmentLayout$Mode;->$VALUES:[Lru/cn/view/OneTwoFragmentLayout$Mode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/view/OneTwoFragmentLayout$Mode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 33
    const-class v0, Lru/cn/view/OneTwoFragmentLayout$Mode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/view/OneTwoFragmentLayout$Mode;

    return-object v0
.end method

.method public static values()[Lru/cn/view/OneTwoFragmentLayout$Mode;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lru/cn/view/OneTwoFragmentLayout$Mode;->$VALUES:[Lru/cn/view/OneTwoFragmentLayout$Mode;

    invoke-virtual {v0}, [Lru/cn/view/OneTwoFragmentLayout$Mode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/view/OneTwoFragmentLayout$Mode;

    return-object v0
.end method
