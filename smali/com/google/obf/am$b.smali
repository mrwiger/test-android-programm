.class Lcom/google/obf/am$b;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/dh$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/am;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private final a:Landroid/net/Uri;

.field private final b:Lcom/google/obf/da;

.field private final c:Lcom/google/obf/am$c;

.field private final d:Lcom/google/obf/cx;

.field private final e:I

.field private final f:Lcom/google/obf/ao;

.field private volatile g:Z

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/net/Uri;Lcom/google/obf/da;Lcom/google/obf/am$c;Lcom/google/obf/cx;IJ)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lcom/google/obf/dl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/obf/am$b;->a:Landroid/net/Uri;

    .line 3
    invoke-static {p2}, Lcom/google/obf/dl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/da;

    iput-object v0, p0, Lcom/google/obf/am$b;->b:Lcom/google/obf/da;

    .line 4
    invoke-static {p3}, Lcom/google/obf/dl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/am$c;

    iput-object v0, p0, Lcom/google/obf/am$b;->c:Lcom/google/obf/am$c;

    .line 5
    invoke-static {p4}, Lcom/google/obf/dl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/cx;

    iput-object v0, p0, Lcom/google/obf/am$b;->d:Lcom/google/obf/cx;

    .line 6
    iput p5, p0, Lcom/google/obf/am$b;->e:I

    .line 7
    new-instance v0, Lcom/google/obf/ao;

    invoke-direct {v0}, Lcom/google/obf/ao;-><init>()V

    iput-object v0, p0, Lcom/google/obf/am$b;->f:Lcom/google/obf/ao;

    .line 8
    iget-object v0, p0, Lcom/google/obf/am$b;->f:Lcom/google/obf/ao;

    iput-wide p6, v0, Lcom/google/obf/ao;->a:J

    .line 9
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/am$b;->h:Z

    .line 10
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 11
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/am$b;->g:Z

    .line 12
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 13
    iget-boolean v0, p0, Lcom/google/obf/am$b;->g:Z

    return v0
.end method

.method public c()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const-wide/16 v12, -0x1

    const/4 v8, 0x0

    const/4 v11, 0x1

    const/4 v7, 0x0

    .line 14
    move v9, v7

    .line 15
    :goto_0
    if-nez v9, :cond_7

    iget-boolean v0, p0, Lcom/google/obf/am$b;->g:Z

    if-nez v0, :cond_7

    .line 17
    :try_start_0
    iget-object v0, p0, Lcom/google/obf/am$b;->f:Lcom/google/obf/ao;

    iget-wide v2, v0, Lcom/google/obf/ao;->a:J

    .line 18
    iget-object v10, p0, Lcom/google/obf/am$b;->b:Lcom/google/obf/da;

    new-instance v0, Lcom/google/obf/db;

    iget-object v1, p0, Lcom/google/obf/am$b;->a:Landroid/net/Uri;

    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/obf/db;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    invoke-interface {v10, v0}, Lcom/google/obf/da;->a(Lcom/google/obf/db;)J

    move-result-wide v4

    .line 19
    cmp-long v0, v4, v12

    if-eqz v0, :cond_0

    .line 20
    add-long/2addr v4, v2

    .line 21
    :cond_0
    new-instance v0, Lcom/google/obf/ag;

    iget-object v1, p0, Lcom/google/obf/am$b;->b:Lcom/google/obf/da;

    invoke-direct/range {v0 .. v5}, Lcom/google/obf/ag;-><init>(Lcom/google/obf/da;JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :try_start_1
    iget-object v1, p0, Lcom/google/obf/am$b;->c:Lcom/google/obf/am$c;

    invoke-virtual {v1, v0}, Lcom/google/obf/am$c;->a(Lcom/google/obf/ak;)Lcom/google/obf/aj;

    move-result-object v2

    .line 23
    iget-boolean v1, p0, Lcom/google/obf/am$b;->h:Z

    if-eqz v1, :cond_1

    .line 24
    invoke-interface {v2}, Lcom/google/obf/aj;->b()V

    .line 25
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/obf/am$b;->h:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_1
    move v1, v9

    .line 26
    :goto_1
    if-nez v1, :cond_2

    :try_start_2
    iget-boolean v3, p0, Lcom/google/obf/am$b;->g:Z

    if-nez v3, :cond_2

    .line 27
    iget-object v3, p0, Lcom/google/obf/am$b;->d:Lcom/google/obf/cx;

    iget v4, p0, Lcom/google/obf/am$b;->e:I

    invoke-interface {v3, v4}, Lcom/google/obf/cx;->b(I)V

    .line 28
    iget-object v3, p0, Lcom/google/obf/am$b;->f:Lcom/google/obf/ao;

    invoke-interface {v2, v0, v3}, Lcom/google/obf/aj;->a(Lcom/google/obf/ak;Lcom/google/obf/ao;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v9

    move v1, v9

    goto :goto_1

    .line 29
    :cond_2
    if-ne v1, v11, :cond_3

    move v0, v7

    .line 33
    :goto_2
    iget-object v1, p0, Lcom/google/obf/am$b;->b:Lcom/google/obf/da;

    invoke-static {v1}, Lcom/google/obf/ea;->a(Lcom/google/obf/da;)V

    move v9, v0

    .line 40
    goto :goto_0

    .line 31
    :cond_3
    if-eqz v0, :cond_4

    .line 32
    iget-object v2, p0, Lcom/google/obf/am$b;->f:Lcom/google/obf/ao;

    invoke-interface {v0}, Lcom/google/obf/ak;->c()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/google/obf/ao;->a:J

    :cond_4
    move v0, v1

    goto :goto_2

    .line 35
    :catchall_0
    move-exception v0

    move-object v1, v8

    move v2, v9

    :goto_3
    if-ne v2, v11, :cond_6

    .line 39
    :cond_5
    :goto_4
    iget-object v1, p0, Lcom/google/obf/am$b;->b:Lcom/google/obf/da;

    invoke-static {v1}, Lcom/google/obf/ea;->a(Lcom/google/obf/da;)V

    throw v0

    .line 37
    :cond_6
    if-eqz v1, :cond_5

    .line 38
    iget-object v2, p0, Lcom/google/obf/am$b;->f:Lcom/google/obf/ao;

    invoke-interface {v1}, Lcom/google/obf/ak;->c()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/google/obf/ao;->a:J

    goto :goto_4

    .line 41
    :cond_7
    return-void

    .line 35
    :catchall_1
    move-exception v1

    move v2, v9

    move-object v14, v0

    move-object v0, v1

    move-object v1, v14

    goto :goto_3

    :catchall_2
    move-exception v2

    move-object v14, v2

    move v2, v1

    move-object v1, v0

    move-object v0, v14

    goto :goto_3
.end method
