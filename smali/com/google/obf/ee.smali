.class public abstract enum Lcom/google/obf/ee;
.super Ljava/lang/Enum;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/ef;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/obf/ee;",
        ">;",
        "Lcom/google/obf/ef;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/obf/ee;

.field public static final enum b:Lcom/google/obf/ee;

.field public static final enum c:Lcom/google/obf/ee;

.field public static final enum d:Lcom/google/obf/ee;

.field public static final enum e:Lcom/google/obf/ee;

.field private static final synthetic f:[Lcom/google/obf/ee;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    new-instance v0, Lcom/google/obf/ee$1;

    const-string v1, "IDENTITY"

    invoke-direct {v0, v1, v2}, Lcom/google/obf/ee$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/ee;->a:Lcom/google/obf/ee;

    .line 31
    new-instance v0, Lcom/google/obf/ee$2;

    const-string v1, "UPPER_CAMEL_CASE"

    invoke-direct {v0, v1, v3}, Lcom/google/obf/ee$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/ee;->b:Lcom/google/obf/ee;

    .line 32
    new-instance v0, Lcom/google/obf/ee$3;

    const-string v1, "UPPER_CAMEL_CASE_WITH_SPACES"

    invoke-direct {v0, v1, v4}, Lcom/google/obf/ee$3;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/ee;->c:Lcom/google/obf/ee;

    .line 33
    new-instance v0, Lcom/google/obf/ee$4;

    const-string v1, "LOWER_CASE_WITH_UNDERSCORES"

    invoke-direct {v0, v1, v5}, Lcom/google/obf/ee$4;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/ee;->d:Lcom/google/obf/ee;

    .line 34
    new-instance v0, Lcom/google/obf/ee$5;

    const-string v1, "LOWER_CASE_WITH_DASHES"

    invoke-direct {v0, v1, v6}, Lcom/google/obf/ee$5;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/ee;->e:Lcom/google/obf/ee;

    .line 35
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/obf/ee;

    sget-object v1, Lcom/google/obf/ee;->a:Lcom/google/obf/ee;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/obf/ee;->b:Lcom/google/obf/ee;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/obf/ee;->c:Lcom/google/obf/ee;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/obf/ee;->d:Lcom/google/obf/ee;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/obf/ee;->e:Lcom/google/obf/ee;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/obf/ee;->f:[Lcom/google/obf/ee;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/google/obf/ee$1;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/google/obf/ee;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static a(CLjava/lang/String;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 26
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ge p2, v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 27
    invoke-virtual {p1, p2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 28
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 12
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    const/4 v1, 0x0

    .line 14
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 15
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_0

    .line 16
    invoke-static {v0}, Ljava/lang/Character;->isLetter(C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 20
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v1, v3, :cond_3

    .line 21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 25
    :cond_1
    :goto_1
    return-object p0

    .line 18
    :cond_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 19
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_0

    .line 22
    :cond_3
    invoke-static {v0}, Ljava/lang/Character;->isUpperCase(C)Z

    move-result v3

    if-nez v3, :cond_1

    .line 23
    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, p0, v1}, Lcom/google/obf/ee;->a(CLjava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 24
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_1
.end method

.method static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 5
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 6
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 7
    invoke-static {v2}, Ljava/lang/Character;->isUpperCase(C)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-eqz v3, :cond_0

    .line 8
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    :cond_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 10
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 11
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/obf/ee;
    .locals 1

    .prologue
    .line 2
    const-class v0, Lcom/google/obf/ee;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/obf/ee;

    return-object v0
.end method

.method public static values()[Lcom/google/obf/ee;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lcom/google/obf/ee;->f:[Lcom/google/obf/ee;

    invoke-virtual {v0}, [Lcom/google/obf/ee;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/obf/ee;

    return-object v0
.end method
