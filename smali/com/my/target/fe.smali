.class public final Lcom/my/target/fe;
.super Lcom/my/target/d;
.source "NativeAppwallAdResponseParser.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/my/target/d",
        "<",
        "Lcom/my/target/fg;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/my/target/d;-><init>()V

    .line 40
    return-void
.end method

.method public static newParser()Lcom/my/target/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/my/target/d",
            "<",
            "Lcom/my/target/fg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    new-instance v0, Lcom/my/target/fe;

    invoke-direct {v0}, Lcom/my/target/fe;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;Lcom/my/target/ae;Lcom/my/target/ak;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/ak;
    .locals 16

    .prologue
    .line 30
    check-cast p3, Lcom/my/target/fg;

    .line 1049
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    invoke-virtual {v0, v1, v2}, Lcom/my/target/fe;->a(Ljava/lang/String;Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object v8

    .line 1050
    if-eqz v8, :cond_9

    .line 1054
    invoke-virtual {v8}, Lorg/json/JSONObject;->names()Lorg/json/JSONArray;

    move-result-object v9

    .line 1055
    move-object/from16 v0, p2

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    invoke-static {v0, v1, v2}, Lcom/my/target/core/parsers/i;->a(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/i;

    move-result-object v10

    .line 1057
    const/4 v7, 0x0

    .line 1058
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v4, v5, :cond_a

    .line 1060
    invoke-virtual {v9, v4}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v5

    .line 1061
    const-string v6, "appwall"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "showcaseApps"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "showcaseGames"

    .line 1062
    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "showcase"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1090
    :cond_0
    invoke-virtual {v8, v5}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 1092
    if-nez v6, :cond_2

    .line 1094
    const/16 p3, 0x0

    .line 1066
    :cond_1
    :goto_1
    if-eqz p3, :cond_8

    invoke-virtual/range {p3 .. p3}, Lcom/my/target/fg;->R()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_8

    .line 1068
    const/4 v4, 0x1

    move v5, v4

    move-object/from16 v4, p3

    .line 1074
    :goto_2
    if-eqz v5, :cond_9

    .line 1076
    invoke-virtual/range {p2 .. p2}, Lcom/my/target/ae;->C()Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/my/target/fg;->d(Z)V

    .line 1077
    invoke-virtual {v4, v8}, Lcom/my/target/fg;->setRawData(Lorg/json/JSONObject;)V

    .line 1078
    :goto_3
    return-object v4

    .line 1097
    :cond_2
    const-string v11, "banners"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v11

    .line 1098
    if-eqz v11, :cond_3

    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-gtz v12, :cond_4

    .line 1100
    :cond_3
    const/16 p3, 0x0

    goto :goto_1

    .line 1103
    :cond_4
    invoke-static {v5}, Lcom/my/target/fg;->a(Ljava/lang/String;)Lcom/my/target/fg;

    move-result-object p3

    .line 1104
    move-object/from16 v0, p3

    invoke-virtual {v10, v6, v0}, Lcom/my/target/core/parsers/i;->a(Lorg/json/JSONObject;Lcom/my/target/fg;)V

    .line 1106
    move-object/from16 v0, p3

    move-object/from16 v1, p2

    move-object/from16 v2, p4

    move-object/from16 v3, p5

    invoke-static {v0, v1, v2, v3}, Lcom/my/target/core/parsers/h;->a(Lcom/my/target/fg;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/h;

    move-result-object v12

    .line 1107
    const/4 v5, 0x0

    move v6, v5

    :goto_4
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v6, v5, :cond_1

    .line 1109
    invoke-virtual {v11, v6}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 1110
    if-eqz v5, :cond_6

    .line 1112
    invoke-static {}, Lcom/my/target/core/models/banners/i;->newBanner()Lcom/my/target/core/models/banners/i;

    move-result-object v13

    .line 1113
    invoke-virtual {v12, v5, v13}, Lcom/my/target/core/parsers/h;->a(Lorg/json/JSONObject;Lcom/my/target/core/models/banners/i;)V

    .line 1114
    invoke-virtual {v13}, Lcom/my/target/core/models/banners/i;->getBundleId()Ljava/lang/String;

    move-result-object v5

    .line 1115
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_5

    .line 1127
    invoke-virtual/range {p5 .. p5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    .line 1128
    invoke-virtual {v14, v5}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 1129
    if-eqz v5, :cond_7

    .line 1133
    const/high16 v15, 0x10000

    invoke-virtual {v14, v5, v15}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    .line 1134
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_7

    const/4 v5, 0x1

    .line 1117
    :goto_5
    invoke-virtual {v13, v5}, Lcom/my/target/core/models/banners/i;->setAppInstalled(Z)V

    .line 1119
    :cond_5
    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Lcom/my/target/fg;->a(Lcom/my/target/core/models/banners/i;)V

    .line 1107
    :cond_6
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_4

    .line 1134
    :cond_7
    const/4 v5, 0x0

    goto :goto_5

    .line 1058
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 1080
    :cond_9
    const/4 v4, 0x0

    .line 30
    goto :goto_3

    :cond_a
    move v5, v7

    move-object/from16 v4, p3

    goto :goto_2
.end method
