.class Lru/cn/player/VideoSizeCalculator;
.super Ljava/lang/Object;
.source "VideoSizeCalculator.java"


# instance fields
.field private cropX:I

.field private cropY:I

.field private mode:Lru/cn/player/SimplePlayer$FitMode;

.field private videoHeight:I

.field private videoWidth:I

.field private zoom:F


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0x64

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    sget-object v0, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_USER:Lru/cn/player/SimplePlayer$FitMode;

    iput-object v0, p0, Lru/cn/player/VideoSizeCalculator;->mode:Lru/cn/player/SimplePlayer$FitMode;

    .line 19
    iput v1, p0, Lru/cn/player/VideoSizeCalculator;->videoWidth:I

    .line 20
    iput v1, p0, Lru/cn/player/VideoSizeCalculator;->videoHeight:I

    .line 21
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lru/cn/player/VideoSizeCalculator;->zoom:F

    .line 22
    iput v2, p0, Lru/cn/player/VideoSizeCalculator;->cropX:I

    .line 23
    iput v2, p0, Lru/cn/player/VideoSizeCalculator;->cropY:I

    .line 24
    return-void
.end method


# virtual methods
.method calculate(IIII)Landroid/graphics/Rect;
    .locals 13
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 49
    sub-int v9, p3, p1

    .line 50
    .local v9, "w":I
    sub-int v3, p4, p2

    .line 52
    .local v3, "h":I
    move v10, v9

    .line 53
    .local v10, "width":I
    move v4, v3

    .line 54
    .local v4, "height":I
    iget v11, p0, Lru/cn/player/VideoSizeCalculator;->videoWidth:I

    if-lez v11, :cond_0

    iget v11, p0, Lru/cn/player/VideoSizeCalculator;->videoHeight:I

    if-lez v11, :cond_0

    .line 56
    const/high16 v11, 0x42c80000    # 100.0f

    iget v12, p0, Lru/cn/player/VideoSizeCalculator;->cropX:I

    int-to-float v12, v12

    div-float v1, v11, v12

    .line 57
    .local v1, "cropScaleFactorX":F
    const/high16 v11, 0x42c80000    # 100.0f

    iget v12, p0, Lru/cn/player/VideoSizeCalculator;->cropY:I

    int-to-float v12, v12

    div-float v2, v11, v12

    .line 58
    .local v2, "cropScaleFactorY":F
    sget-object v11, Lru/cn/player/VideoSizeCalculator$1;->$SwitchMap$ru$cn$player$SimplePlayer$FitMode:[I

    iget-object v12, p0, Lru/cn/player/VideoSizeCalculator;->mode:Lru/cn/player/SimplePlayer$FitMode;

    invoke-virtual {v12}, Lru/cn/player/SimplePlayer$FitMode;->ordinal()I

    move-result v12

    aget v11, v11, v12

    packed-switch v11, :pswitch_data_0

    .line 95
    .end local v1    # "cropScaleFactorX":F
    .end local v2    # "cropScaleFactorY":F
    :cond_0
    :goto_0
    sub-int v11, p3, p1

    sub-int/2addr v11, v9

    div-int/lit8 v5, v11, 0x2

    .line 96
    .local v5, "l":I
    add-int v6, v5, v9

    .line 97
    .local v6, "r":I
    sub-int v11, p4, p2

    sub-int/2addr v11, v3

    div-int/lit8 v8, v11, 0x2

    .line 98
    .local v8, "t":I
    add-int v0, v8, v3

    .line 100
    .local v0, "b":I
    new-instance v11, Landroid/graphics/Rect;

    invoke-direct {v11, v5, v8, v6, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v11

    .line 60
    .end local v0    # "b":I
    .end local v5    # "l":I
    .end local v6    # "r":I
    .end local v8    # "t":I
    .restart local v1    # "cropScaleFactorX":F
    .restart local v2    # "cropScaleFactorY":F
    :pswitch_0
    int-to-float v11, v10

    mul-float/2addr v11, v1

    iget v12, p0, Lru/cn/player/VideoSizeCalculator;->videoWidth:I

    int-to-float v12, v12

    div-float v7, v11, v12

    .line 61
    .local v7, "scale":F
    iget v11, p0, Lru/cn/player/VideoSizeCalculator;->videoWidth:I

    int-to-float v11, v11

    mul-float/2addr v11, v7

    float-to-int v9, v11

    .line 62
    iget v11, p0, Lru/cn/player/VideoSizeCalculator;->videoHeight:I

    int-to-float v11, v11

    mul-float/2addr v11, v7

    float-to-int v3, v11

    .line 63
    goto :goto_0

    .line 66
    .end local v7    # "scale":F
    :pswitch_1
    int-to-float v11, v4

    mul-float/2addr v11, v2

    iget v12, p0, Lru/cn/player/VideoSizeCalculator;->videoHeight:I

    int-to-float v12, v12

    div-float v7, v11, v12

    .line 67
    .restart local v7    # "scale":F
    iget v11, p0, Lru/cn/player/VideoSizeCalculator;->videoWidth:I

    int-to-float v11, v11

    mul-float/2addr v11, v7

    float-to-int v9, v11

    .line 68
    iget v11, p0, Lru/cn/player/VideoSizeCalculator;->videoHeight:I

    int-to-float v11, v11

    mul-float/2addr v11, v7

    float-to-int v3, v11

    .line 69
    goto :goto_0

    .line 72
    .end local v7    # "scale":F
    :pswitch_2
    int-to-float v11, v10

    mul-float/2addr v11, v1

    float-to-int v9, v11

    .line 73
    int-to-float v11, v4

    mul-float/2addr v11, v2

    float-to-int v3, v11

    .line 74
    goto :goto_0

    .line 77
    :pswitch_3
    move v9, v10

    .line 78
    move v3, v4

    .line 80
    iget v11, p0, Lru/cn/player/VideoSizeCalculator;->videoWidth:I

    mul-int/2addr v11, v3

    iget v12, p0, Lru/cn/player/VideoSizeCalculator;->videoHeight:I

    mul-int/2addr v12, v9

    if-ge v11, v12, :cond_2

    .line 81
    iget v11, p0, Lru/cn/player/VideoSizeCalculator;->videoWidth:I

    mul-int/2addr v11, v3

    iget v12, p0, Lru/cn/player/VideoSizeCalculator;->videoHeight:I

    div-int v9, v11, v12

    .line 85
    :cond_1
    :goto_1
    int-to-float v11, v9

    iget v12, p0, Lru/cn/player/VideoSizeCalculator;->zoom:F

    mul-float/2addr v11, v12

    float-to-int v9, v11

    .line 86
    int-to-float v11, v3

    iget v12, p0, Lru/cn/player/VideoSizeCalculator;->zoom:F

    mul-float/2addr v11, v12

    float-to-int v3, v11

    .line 88
    goto :goto_0

    .line 82
    :cond_2
    iget v11, p0, Lru/cn/player/VideoSizeCalculator;->videoWidth:I

    mul-int/2addr v11, v3

    iget v12, p0, Lru/cn/player/VideoSizeCalculator;->videoHeight:I

    mul-int/2addr v12, v9

    if-le v11, v12, :cond_1

    .line 83
    iget v11, p0, Lru/cn/player/VideoSizeCalculator;->videoHeight:I

    mul-int/2addr v11, v9

    iget v12, p0, Lru/cn/player/VideoSizeCalculator;->videoWidth:I

    div-int v3, v11, v12

    goto :goto_1

    .line 58
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getMode()Lru/cn/player/SimplePlayer$FitMode;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lru/cn/player/VideoSizeCalculator;->mode:Lru/cn/player/SimplePlayer$FitMode;

    return-object v0
.end method

.method getZoom()F
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lru/cn/player/VideoSizeCalculator;->zoom:F

    return v0
.end method

.method setCrop(II)V
    .locals 0
    .param p1, "cropX"    # I
    .param p2, "cropY"    # I

    .prologue
    .line 36
    iput p1, p0, Lru/cn/player/VideoSizeCalculator;->cropX:I

    .line 37
    iput p2, p0, Lru/cn/player/VideoSizeCalculator;->cropY:I

    .line 38
    return-void
.end method

.method setMode(Lru/cn/player/SimplePlayer$FitMode;)V
    .locals 0
    .param p1, "mode"    # Lru/cn/player/SimplePlayer$FitMode;

    .prologue
    .line 27
    iput-object p1, p0, Lru/cn/player/VideoSizeCalculator;->mode:Lru/cn/player/SimplePlayer$FitMode;

    .line 28
    return-void
.end method

.method setVideoSize(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 31
    iput p1, p0, Lru/cn/player/VideoSizeCalculator;->videoWidth:I

    .line 32
    iput p2, p0, Lru/cn/player/VideoSizeCalculator;->videoHeight:I

    .line 33
    return-void
.end method

.method setZoom(F)V
    .locals 0
    .param p1, "zoom"    # F

    .prologue
    .line 41
    iput p1, p0, Lru/cn/player/VideoSizeCalculator;->zoom:F

    .line 42
    return-void
.end method
