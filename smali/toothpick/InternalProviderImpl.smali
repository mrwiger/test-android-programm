.class public Ltoothpick/InternalProviderImpl;
.super Ljava/lang/Object;
.source "InternalProviderImpl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private factory:Ltoothpick/Factory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ltoothpick/Factory",
            "<TT;>;"
        }
    .end annotation
.end field

.field private factoryClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field private volatile instance:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private isCreatingSingletonInScope:Z

.field protected isProvidingSingletonInScope:Z

.field private providerFactory:Ltoothpick/Factory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ltoothpick/Factory",
            "<",
            "Ljavax/inject/Provider",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private providerFactoryClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Ljavax/inject/Provider",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private volatile providerInstance:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;ZZZ)V
    .locals 2
    .param p2, "isProviderFactoryClass"    # Z
    .param p3, "isCreatingSingletonInScope"    # Z
    .param p4, "isProvidingSingletonInScope"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;ZZZ)V"
        }
    .end annotation

    .prologue
    .line 54
    .local p0, "this":Ltoothpick/InternalProviderImpl;, "Ltoothpick/InternalProviderImpl<TT;>;"
    .local p1, "factoryKeyClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    if-nez p1, :cond_0

    .line 56
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The factory class can\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_0
    if-eqz p2, :cond_1

    .line 60
    iput-object p1, p0, Ltoothpick/InternalProviderImpl;->providerFactoryClass:Ljava/lang/Class;

    .line 64
    :goto_0
    iput-boolean p3, p0, Ltoothpick/InternalProviderImpl;->isCreatingSingletonInScope:Z

    .line 65
    iput-boolean p4, p0, Ltoothpick/InternalProviderImpl;->isProvidingSingletonInScope:Z

    .line 66
    return-void

    .line 62
    :cond_1
    iput-object p1, p0, Ltoothpick/InternalProviderImpl;->factoryClass:Ljava/lang/Class;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "this":Ltoothpick/InternalProviderImpl;, "Ltoothpick/InternalProviderImpl<TT;>;"
    .local p1, "instance":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    if-nez p1, :cond_0

    .line 24
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The instance can\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 27
    :cond_0
    iput-object p1, p0, Ltoothpick/InternalProviderImpl;->instance:Ljava/lang/Object;

    .line 28
    return-void
.end method

.method public constructor <init>(Ljavax/inject/Provider;Z)V
    .locals 2
    .param p2, "isProvidingSingletonInScope"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<+TT;>;Z)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p0, "this":Ltoothpick/InternalProviderImpl;, "Ltoothpick/InternalProviderImpl<TT;>;"
    .local p1, "providerInstance":Ljavax/inject/Provider;, "Ljavax/inject/Provider<+TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    if-nez p1, :cond_0

    .line 32
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The provider can\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_0
    iput-object p1, p0, Ltoothpick/InternalProviderImpl;->providerInstance:Ljavax/inject/Provider;

    .line 36
    iput-boolean p2, p0, Ltoothpick/InternalProviderImpl;->isProvidingSingletonInScope:Z

    .line 37
    return-void
.end method

.method public constructor <init>(Ltoothpick/Factory;Z)V
    .locals 2
    .param p2, "isProviderFactory"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltoothpick/Factory",
            "<*>;Z)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p0, "this":Ltoothpick/InternalProviderImpl;, "Ltoothpick/InternalProviderImpl<TT;>;"
    .local p1, "factory":Ltoothpick/Factory;, "Ltoothpick/Factory<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    if-nez p1, :cond_0

    .line 41
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The factory can\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_0
    if-eqz p2, :cond_1

    .line 45
    iput-object p1, p0, Ltoothpick/InternalProviderImpl;->providerFactory:Ltoothpick/Factory;

    .line 49
    :goto_0
    return-void

    .line 47
    :cond_1
    iput-object p1, p0, Ltoothpick/InternalProviderImpl;->factory:Ltoothpick/Factory;

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized get(Ltoothpick/Scope;)Ljava/lang/Object;
    .locals 2
    .param p1, "scope"    # Ltoothpick/Scope;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltoothpick/Scope;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 71
    .local p0, "this":Ltoothpick/InternalProviderImpl;, "Ltoothpick/InternalProviderImpl<TT;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->instance:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->instance:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    :goto_0
    monitor-exit p0

    return-object v0

    .line 75
    :cond_0
    :try_start_1
    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->providerInstance:Ljavax/inject/Provider;

    if-eqz v0, :cond_2

    .line 76
    iget-boolean v0, p0, Ltoothpick/InternalProviderImpl;->isProvidingSingletonInScope:Z

    if-eqz v0, :cond_1

    .line 77
    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->providerInstance:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Ltoothpick/InternalProviderImpl;->instance:Ljava/lang/Object;

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Ltoothpick/InternalProviderImpl;->providerInstance:Ljavax/inject/Provider;

    .line 80
    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->instance:Ljava/lang/Object;

    goto :goto_0

    .line 83
    :cond_1
    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->providerInstance:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 86
    :cond_2
    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->factoryClass:Ljava/lang/Class;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->factory:Ltoothpick/Factory;

    if-nez v0, :cond_3

    .line 87
    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->factoryClass:Ljava/lang/Class;

    invoke-static {v0}, Ltoothpick/registries/FactoryRegistryLocator;->getFactory(Ljava/lang/Class;)Ltoothpick/Factory;

    move-result-object v0

    iput-object v0, p0, Ltoothpick/InternalProviderImpl;->factory:Ltoothpick/Factory;

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Ltoothpick/InternalProviderImpl;->factoryClass:Ljava/lang/Class;

    .line 92
    :cond_3
    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->factory:Ltoothpick/Factory;

    if-eqz v0, :cond_5

    .line 93
    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->factory:Ltoothpick/Factory;

    invoke-interface {v0}, Ltoothpick/Factory;->hasScopeAnnotation()Z

    move-result v0

    if-nez v0, :cond_4

    iget-boolean v0, p0, Ltoothpick/InternalProviderImpl;->isCreatingSingletonInScope:Z

    if-nez v0, :cond_4

    .line 94
    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->factory:Ltoothpick/Factory;

    invoke-interface {v0, p1}, Ltoothpick/Factory;->createInstance(Ltoothpick/Scope;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 96
    :cond_4
    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->factory:Ltoothpick/Factory;

    invoke-interface {v0, p1}, Ltoothpick/Factory;->createInstance(Ltoothpick/Scope;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Ltoothpick/InternalProviderImpl;->instance:Ljava/lang/Object;

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Ltoothpick/InternalProviderImpl;->factory:Ltoothpick/Factory;

    .line 99
    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->instance:Ljava/lang/Object;

    goto :goto_0

    .line 102
    :cond_5
    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->providerFactoryClass:Ljava/lang/Class;

    if-eqz v0, :cond_6

    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->providerFactory:Ltoothpick/Factory;

    if-nez v0, :cond_6

    .line 103
    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->providerFactoryClass:Ljava/lang/Class;

    invoke-static {v0}, Ltoothpick/registries/FactoryRegistryLocator;->getFactory(Ljava/lang/Class;)Ltoothpick/Factory;

    move-result-object v0

    iput-object v0, p0, Ltoothpick/InternalProviderImpl;->providerFactory:Ltoothpick/Factory;

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Ltoothpick/InternalProviderImpl;->providerFactoryClass:Ljava/lang/Class;

    .line 108
    :cond_6
    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->providerFactory:Ltoothpick/Factory;

    if-eqz v0, :cond_b

    .line 109
    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->providerFactory:Ltoothpick/Factory;

    invoke-interface {v0}, Ltoothpick/Factory;->hasProvidesSingletonInScopeAnnotation()Z

    move-result v0

    if-nez v0, :cond_7

    iget-boolean v0, p0, Ltoothpick/InternalProviderImpl;->isProvidingSingletonInScope:Z

    if-eqz v0, :cond_8

    .line 110
    :cond_7
    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->providerFactory:Ltoothpick/Factory;

    invoke-interface {v0, p1}, Ltoothpick/Factory;->createInstance(Ltoothpick/Scope;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Ltoothpick/InternalProviderImpl;->instance:Ljava/lang/Object;

    .line 112
    const/4 v0, 0x0

    iput-object v0, p0, Ltoothpick/InternalProviderImpl;->providerFactory:Ltoothpick/Factory;

    .line 113
    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->instance:Ljava/lang/Object;

    goto/16 :goto_0

    .line 115
    :cond_8
    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->providerFactory:Ltoothpick/Factory;

    invoke-interface {v0}, Ltoothpick/Factory;->hasScopeAnnotation()Z

    move-result v0

    if-nez v0, :cond_9

    iget-boolean v0, p0, Ltoothpick/InternalProviderImpl;->isCreatingSingletonInScope:Z

    if-eqz v0, :cond_a

    .line 116
    :cond_9
    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->providerFactory:Ltoothpick/Factory;

    invoke-interface {v0, p1}, Ltoothpick/Factory;->createInstance(Ltoothpick/Scope;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljavax/inject/Provider;

    iput-object v0, p0, Ltoothpick/InternalProviderImpl;->providerInstance:Ljavax/inject/Provider;

    .line 118
    const/4 v0, 0x0

    iput-object v0, p0, Ltoothpick/InternalProviderImpl;->providerFactory:Ltoothpick/Factory;

    .line 119
    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->providerInstance:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_0

    .line 122
    :cond_a
    iget-object v0, p0, Ltoothpick/InternalProviderImpl;->providerFactory:Ltoothpick/Factory;

    invoke-interface {v0, p1}, Ltoothpick/Factory;->createInstance(Ltoothpick/Scope;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_0

    .line 125
    :cond_b
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "A provider can only be used with an instance, a provider, a factory or a provider factory. Should not happen."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 71
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
