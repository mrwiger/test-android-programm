.class public Lcom/google/obf/hu;
.super Ljava/lang/Object;
.source "IMASDK"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/obf/hu;->a:Landroid/content/Context;

    .line 3
    iput-object p2, p0, Lcom/google/obf/hu;->b:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lcom/google/obf/hu;->c:Landroid/net/Uri;

    .line 5
    return-void
.end method


# virtual methods
.method public a(Lcom/google/obf/hf;Landroid/os/Handler;)[Lcom/google/obf/x;
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v7, 0x0

    const/4 v11, 0x1

    .line 6
    new-instance v3, Lcom/google/obf/dc;

    const/high16 v0, 0x10000

    invoke-direct {v3, v0}, Lcom/google/obf/dc;-><init>(I)V

    .line 7
    new-instance v2, Lcom/google/obf/de;

    iget-object v0, p0, Lcom/google/obf/hu;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/obf/hu;->b:Ljava/lang/String;

    invoke-direct {v2, v0, v12, v1, v11}, Lcom/google/obf/de;-><init>(Landroid/content/Context;Lcom/google/obf/di;Ljava/lang/String;Z)V

    .line 8
    new-instance v0, Lcom/google/obf/am;

    iget-object v1, p0, Lcom/google/obf/hu;->c:Landroid/net/Uri;

    const/high16 v4, 0x1000000

    .line 9
    invoke-virtual {p1}, Lcom/google/obf/hf;->d()Lcom/google/obf/hf$b;

    move-result-object v6

    new-array v8, v7, [Lcom/google/obf/aj;

    move-object v5, p2

    invoke-direct/range {v0 .. v8}, Lcom/google/obf/am;-><init>(Landroid/net/Uri;Lcom/google/obf/da;Lcom/google/obf/cx;ILandroid/os/Handler;Lcom/google/obf/am$a;I[Lcom/google/obf/aj;)V

    .line 10
    new-instance v1, Lcom/google/obf/p;

    iget-object v2, p0, Lcom/google/obf/hu;->a:Landroid/content/Context;

    sget-object v4, Lcom/google/obf/m;->a:Lcom/google/obf/m;

    const-wide/16 v6, 0x1388

    .line 11
    invoke-virtual {p1}, Lcom/google/obf/hf;->e()Lcom/google/obf/hf$e;

    move-result-object v9

    const/16 v10, 0x32

    move-object v3, v0

    move v5, v11

    move-object v8, p2

    invoke-direct/range {v1 .. v10}, Lcom/google/obf/p;-><init>(Landroid/content/Context;Lcom/google/obf/u;Lcom/google/obf/m;IJLandroid/os/Handler;Lcom/google/obf/p$a;I)V

    .line 12
    new-instance v2, Lcom/google/obf/l;

    sget-object v4, Lcom/google/obf/m;->a:Lcom/google/obf/m;

    .line 13
    invoke-virtual {p1}, Lcom/google/obf/hf;->f()Lcom/google/obf/hf$c;

    move-result-object v8

    iget-object v3, p0, Lcom/google/obf/hu;->a:Landroid/content/Context;

    .line 14
    invoke-static {v3}, Lcom/google/obf/z;->a(Landroid/content/Context;)Lcom/google/obf/z;

    move-result-object v9

    const/4 v10, 0x3

    move-object v3, v0

    move-object v5, v12

    move v6, v11

    move-object v7, p2

    invoke-direct/range {v2 .. v10}, Lcom/google/obf/l;-><init>(Lcom/google/obf/u;Lcom/google/obf/m;Lcom/google/obf/ac;ZLandroid/os/Handler;Lcom/google/obf/l$a;Lcom/google/obf/z;I)V

    .line 15
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/obf/x;

    .line 16
    sget-object v3, Lcom/google/obf/hf$g;->a:Lcom/google/obf/hf$g;

    invoke-virtual {v3}, Lcom/google/obf/hf$g;->a()I

    move-result v3

    aput-object v1, v0, v3

    .line 17
    sget-object v1, Lcom/google/obf/hf$g;->b:Lcom/google/obf/hf$g;

    invoke-virtual {v1}, Lcom/google/obf/hf$g;->a()I

    move-result v1

    aput-object v2, v0, v1

    .line 18
    return-object v0
.end method
