.class Lru/cn/ad/natives/adapters/AdmobNativeAdapter$3;
.super Ljava/lang/Object;
.source "AdmobNativeAdapter.java"

# interfaces
.implements Lcom/google/android/gms/ads/formats/NativeAppInstallAd$OnAppInstallAdLoadedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->onLoad()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/ad/natives/adapters/AdmobNativeAdapter;


# direct methods
.method constructor <init>(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    .prologue
    .line 43
    iput-object p1, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$3;->this$0:Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAppInstallAdLoaded(Lcom/google/android/gms/ads/formats/NativeAppInstallAd;)V
    .locals 3
    .param p1, "nativeAppInstallAd"    # Lcom/google/android/gms/ads/formats/NativeAppInstallAd;

    .prologue
    .line 46
    const-string v0, "AdmobNativeAdapter"

    const-string v1, "install ad"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$3;->this$0:Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    new-instance v1, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;

    iget-object v2, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$3;->this$0:Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    invoke-static {v2}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->access$800(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/formats/NativeAppInstallAd;)V

    invoke-static {v0, v1}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->access$402(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;Lru/cn/ad/natives/adapters/AdmobNativeAdapter$AdmobBinder;)Lru/cn/ad/natives/adapters/AdmobNativeAdapter$AdmobBinder;

    .line 49
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$3;->this$0:Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    invoke-static {v0}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->access$900(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$3;->this$0:Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    invoke-static {v0}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->access$1000(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdLoaded()V

    .line 51
    :cond_0
    return-void
.end method
