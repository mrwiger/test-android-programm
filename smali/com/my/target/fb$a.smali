.class final Lcom/my/target/fb$a;
.super Ljava/lang/Object;
.source "SliderRecyclerView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/fb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic eg:Lcom/my/target/fb;


# direct methods
.method private constructor <init>(Lcom/my/target/fb;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, Lcom/my/target/fb$a;->eg:Lcom/my/target/fb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/my/target/fb;B)V
    .locals 0

    .prologue
    .line 360
    invoke-direct {p0, p1}, Lcom/my/target/fb$a;-><init>(Lcom/my/target/fb;)V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 365
    iget-object v0, p0, Lcom/my/target/fb$a;->eg:Lcom/my/target/fb;

    invoke-static {v0}, Lcom/my/target/fb;->c(Lcom/my/target/fb;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 392
    :cond_0
    :goto_0
    return-void

    .line 370
    :cond_1
    iget-object v0, p0, Lcom/my/target/fb$a;->eg:Lcom/my/target/fb;

    invoke-static {v0}, Lcom/my/target/fb;->b(Lcom/my/target/fb;)Lcom/my/target/fa;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/my/target/fa;->findContainingItemView(Landroid/view/View;)Landroid/view/View;

    move-result-object p1

    .line 372
    if-eqz p1, :cond_0

    .line 377
    iget-object v0, p0, Lcom/my/target/fb$a;->eg:Lcom/my/target/fb;

    invoke-static {v0}, Lcom/my/target/fb;->b(Lcom/my/target/fb;)Lcom/my/target/fa;

    move-result-object v0

    .line 1103
    invoke-virtual {v0}, Lcom/my/target/fa;->findFirstCompletelyVisibleItemPosition()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/my/target/fa;->findViewByPosition(I)Landroid/view/View;

    move-result-object v0

    if-ne v0, p1, :cond_2

    const/4 v0, 0x1

    .line 377
    :goto_1
    if-eqz v0, :cond_3

    .line 379
    iget-object v0, p0, Lcom/my/target/fb$a;->eg:Lcom/my/target/fb;

    invoke-static {v0}, Lcom/my/target/fb;->d(Lcom/my/target/fb;)Lcom/my/target/fb$c;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/fb$a;->eg:Lcom/my/target/fb;

    invoke-static {v0}, Lcom/my/target/fb;->e(Lcom/my/target/fb;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 381
    iget-object v0, p0, Lcom/my/target/fb$a;->eg:Lcom/my/target/fb;

    invoke-static {v0}, Lcom/my/target/fb;->d(Lcom/my/target/fb;)Lcom/my/target/fb$c;

    move-result-object v1

    iget-object v0, p0, Lcom/my/target/fb$a;->eg:Lcom/my/target/fb;

    invoke-static {v0}, Lcom/my/target/fb;->e(Lcom/my/target/fb;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/my/target/fb$a;->eg:Lcom/my/target/fb;

    invoke-static {v2}, Lcom/my/target/fb;->b(Lcom/my/target/fb;)Lcom/my/target/fa;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/my/target/fa;->getPosition(Landroid/view/View;)I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/g;

    invoke-interface {v1, v0}, Lcom/my/target/fb$c;->e(Lcom/my/target/core/models/banners/g;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1103
    goto :goto_1

    .line 386
    :cond_3
    iget-object v0, p0, Lcom/my/target/fb$a;->eg:Lcom/my/target/fb;

    invoke-static {v0}, Lcom/my/target/fb;->f(Lcom/my/target/fb;)Landroid/support/v7/widget/PagerSnapHelper;

    move-result-object v0

    iget-object v2, p0, Lcom/my/target/fb$a;->eg:Lcom/my/target/fb;

    invoke-static {v2}, Lcom/my/target/fb;->b(Lcom/my/target/fb;)Lcom/my/target/fa;

    move-result-object v2

    invoke-virtual {v0, v2, p1}, Landroid/support/v7/widget/PagerSnapHelper;->calculateDistanceToFinalSnap(Landroid/support/v7/widget/RecyclerView$LayoutManager;Landroid/view/View;)[I

    move-result-object v0

    .line 387
    if-eqz v0, :cond_0

    .line 389
    iget-object v2, p0, Lcom/my/target/fb$a;->eg:Lcom/my/target/fb;

    aget v0, v0, v1

    invoke-virtual {v2, v0, v1}, Lcom/my/target/fb;->smoothScrollBy(II)V

    goto :goto_0
.end method
