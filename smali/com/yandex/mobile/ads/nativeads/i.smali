.class public Lcom/yandex/mobile/ads/nativeads/i;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/mobile/ads/nativeads/i$a;,
        Lcom/yandex/mobile/ads/nativeads/i$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/yandex/mobile/ads/nativeads/h;

.field private final b:Lcom/yandex/mobile/ads/nativeads/s;

.field private c:Ljava/lang/String;

.field private d:Lcom/yandex/mobile/ads/nativeads/q;


# direct methods
.method constructor <init>(Lcom/yandex/mobile/ads/nativeads/h;Lcom/yandex/mobile/ads/nativeads/s;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/yandex/mobile/ads/nativeads/i;->a:Lcom/yandex/mobile/ads/nativeads/h;

    .line 57
    iput-object p2, p0, Lcom/yandex/mobile/ads/nativeads/i;->b:Lcom/yandex/mobile/ads/nativeads/s;

    .line 58
    return-void
.end method

.method static synthetic a(Lcom/yandex/mobile/ads/nativeads/i;)Lcom/yandex/mobile/ads/nativeads/q;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/i;->d:Lcom/yandex/mobile/ads/nativeads/q;

    return-object v0
.end method

.method static synthetic a(Lcom/yandex/mobile/ads/nativeads/i;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Lcom/yandex/mobile/ads/nativeads/i;->c:Ljava/lang/String;

    return-object p1
.end method

.method private a(Lcom/yandex/mobile/ads/nativeads/i$b;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 163
    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/i;->d:Lcom/yandex/mobile/ads/nativeads/q;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/i;->a:Lcom/yandex/mobile/ads/nativeads/h;

    if-nez v1, :cond_1

    .line 174
    :cond_0
    :goto_0
    return v0

    .line 167
    :cond_1
    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/i;->b:Lcom/yandex/mobile/ads/nativeads/s;

    invoke-virtual {v1}, Lcom/yandex/mobile/ads/nativeads/s;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 168
    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/i;->a:Lcom/yandex/mobile/ads/nativeads/h;

    invoke-virtual {v1}, Lcom/yandex/mobile/ads/nativeads/h;->c()Ljava/util/List;

    move-result-object v1

    .line 169
    if-eqz v1, :cond_0

    invoke-interface {p1, v1}, Lcom/yandex/mobile/ads/nativeads/i$b;->a(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 174
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/yandex/mobile/ads/nativeads/i$a;
    .locals 3

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/yandex/mobile/ads/nativeads/i;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    sget-object v0, Lcom/yandex/mobile/ads/ax$a;->f:Lcom/yandex/mobile/ads/ax$a;

    .line 77
    :goto_0
    new-instance v1, Lcom/yandex/mobile/ads/nativeads/i$a;

    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/i;->c:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Lcom/yandex/mobile/ads/nativeads/i$a;-><init>(Lcom/yandex/mobile/ads/ax$a;Ljava/lang/String;)V

    return-object v1

    .line 69
    :cond_0
    invoke-virtual {p0}, Lcom/yandex/mobile/ads/nativeads/i;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    sget-object v0, Lcom/yandex/mobile/ads/ax$a;->i:Lcom/yandex/mobile/ads/ax$a;

    goto :goto_0

    .line 71
    :cond_1
    invoke-virtual {p0}, Lcom/yandex/mobile/ads/nativeads/i;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 72
    sget-object v0, Lcom/yandex/mobile/ads/ax$a;->d:Lcom/yandex/mobile/ads/ax$a;

    goto :goto_0

    .line 74
    :cond_2
    sget-object v0, Lcom/yandex/mobile/ads/ax$a;->a:Lcom/yandex/mobile/ads/ax$a;

    goto :goto_0
.end method

.method a(Lcom/yandex/mobile/ads/nativeads/q;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/yandex/mobile/ads/nativeads/i;->d:Lcom/yandex/mobile/ads/nativeads/q;

    .line 62
    return-void
.end method

.method a(Lcom/yandex/mobile/ads/nativeads/b;Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 147
    if-eqz p1, :cond_0

    .line 148
    invoke-virtual {p1}, Lcom/yandex/mobile/ads/nativeads/b;->a()Ljava/lang/Object;

    move-result-object v0

    .line 149
    if-eqz v0, :cond_0

    .line 150
    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/i;->d:Lcom/yandex/mobile/ads/nativeads/q;

    invoke-virtual {v1, p1}, Lcom/yandex/mobile/ads/nativeads/q;->a(Lcom/yandex/mobile/ads/nativeads/b;)Lcom/yandex/mobile/ads/nativeads/ac;

    move-result-object v1

    .line 151
    if-eqz v1, :cond_0

    invoke-virtual {v1, p2, v0}, Lcom/yandex/mobile/ads/nativeads/ac;->b(Landroid/view/View;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    const/4 v0, 0x1

    .line 157
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()Z
    .locals 1

    .prologue
    .line 83
    new-instance v0, Lcom/yandex/mobile/ads/nativeads/i$1;

    invoke-direct {v0, p0}, Lcom/yandex/mobile/ads/nativeads/i$1;-><init>(Lcom/yandex/mobile/ads/nativeads/i;)V

    invoke-direct {p0, v0}, Lcom/yandex/mobile/ads/nativeads/i;->a(Lcom/yandex/mobile/ads/nativeads/i$b;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c()Z
    .locals 1

    .prologue
    .line 106
    new-instance v0, Lcom/yandex/mobile/ads/nativeads/i$2;

    invoke-direct {v0, p0}, Lcom/yandex/mobile/ads/nativeads/i$2;-><init>(Lcom/yandex/mobile/ads/nativeads/i;)V

    invoke-direct {p0, v0}, Lcom/yandex/mobile/ads/nativeads/i;->a(Lcom/yandex/mobile/ads/nativeads/i$b;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 127
    new-instance v0, Lcom/yandex/mobile/ads/nativeads/i$3;

    invoke-direct {v0, p0}, Lcom/yandex/mobile/ads/nativeads/i$3;-><init>(Lcom/yandex/mobile/ads/nativeads/i;)V

    invoke-direct {p0, v0}, Lcom/yandex/mobile/ads/nativeads/i;->a(Lcom/yandex/mobile/ads/nativeads/i$b;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method e()Z
    .locals 1

    .prologue
    .line 180
    new-instance v0, Lcom/yandex/mobile/ads/nativeads/i$4;

    invoke-direct {v0, p0}, Lcom/yandex/mobile/ads/nativeads/i$4;-><init>(Lcom/yandex/mobile/ads/nativeads/i;)V

    invoke-direct {p0, v0}, Lcom/yandex/mobile/ads/nativeads/i;->a(Lcom/yandex/mobile/ads/nativeads/i$b;)Z

    move-result v0

    return v0
.end method
