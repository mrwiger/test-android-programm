.class public Lcom/my/target/bu;
.super Landroid/widget/TextView;
.source "BorderedTextView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "AppCompatCustomView"
    }
.end annotation


# static fields
.field public static final ij:I = -0xcccccd

.field private static final ik:I = 0x2


# instance fields
.field private final il:Landroid/graphics/drawable/GradientDrawable;

.field private final im:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/my/target/bu;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/my/target/bu;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    iput-object v0, p0, Lcom/my/target/bu;->il:Landroid/graphics/drawable/GradientDrawable;

    .line 38
    iget-object v0, p0, Lcom/my/target/bu;->il:Landroid/graphics/drawable/GradientDrawable;

    const v1, -0xcccccd

    invoke-virtual {v0, v2, v1}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 39
    iget-object v0, p0, Lcom/my/target/bu;->il:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 42
    const/4 v1, 0x1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/my/target/bu;->im:I

    .line 43
    return-void
.end method


# virtual methods
.method public a(III)V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/my/target/bu;->il:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 53
    iget-object v0, p0, Lcom/my/target/bu;->il:Landroid/graphics/drawable/GradientDrawable;

    int-to-float v1, p3

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 54
    invoke-virtual {p0}, Lcom/my/target/bu;->invalidate()V

    .line 55
    return-void
.end method

.method public c(II)V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/my/target/bu;->a(III)V

    .line 48
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/my/target/bu;->il:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {p0}, Lcom/my/target/bu;->getPaddingLeft()I

    move-result v1

    iget v2, p0, Lcom/my/target/bu;->im:I

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/my/target/bu;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/my/target/bu;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/my/target/bu;->getHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    .line 68
    iget-object v0, p0, Lcom/my/target/bu;->il:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 69
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 70
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 75
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->onMeasure(II)V

    .line 76
    invoke-virtual {p0}, Lcom/my/target/bu;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/my/target/bu;->im:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/my/target/bu;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/my/target/bu;->setMeasuredDimension(II)V

    .line 77
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 60
    iget-object v0, p0, Lcom/my/target/bu;->il:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 61
    invoke-virtual {p0}, Lcom/my/target/bu;->invalidate()V

    .line 62
    return-void
.end method
