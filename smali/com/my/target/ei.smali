.class public final Lcom/my/target/ei;
.super Lcom/my/target/ee;
.source "VideoStyleView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/ei$c;,
        Lcom/my/target/ei$b;,
        Lcom/my/target/ei$a;
    }
.end annotation


# instance fields
.field private allowClose:Z

.field private final aw:Lcom/my/target/cm;

.field private final bL:Landroid/widget/Button;

.field private final bm:Lcom/my/target/eg;

.field private final bp:Lcom/my/target/by;

.field private cA:Ljava/lang/String;

.field private cB:Ljava/lang/String;

.field private cC:Z

.field private final ch:Landroid/widget/TextView;

.field private final ci:Lcom/my/target/ca;

.field private final cj:Landroid/widget/TextView;

.field private final ck:Landroid/widget/LinearLayout;

.field private final cl:Landroid/widget/TextView;

.field private final cm:Landroid/widget/FrameLayout;

.field private final cn:Landroid/widget/TextView;

.field private final co:Lcom/my/target/dz;

.field private final cp:Lcom/my/target/ed;

.field private final cq:Lcom/my/target/ed;

.field private final cr:Lcom/my/target/ed;

.field private final cs:Ljava/lang/Runnable;

.field private final ct:Lcom/my/target/ei$c;

.field private final cu:Lcom/my/target/ei$a;

.field private final cv:I

.field private cw:I

.field private cx:F

.field private cy:Z

.field private final cz:I

.field private final padding:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x0

    const/16 v8, 0x10

    const/4 v7, 0x1

    const/4 v2, -0x1

    .line 81
    invoke-direct {p0, p1, v7}, Lcom/my/target/ee;-><init>(Landroid/content/Context;I)V

    .line 83
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    .line 84
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    .line 85
    new-instance v0, Lcom/my/target/ca;

    invoke-direct {v0, p1}, Lcom/my/target/ca;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    .line 86
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    .line 87
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ei;->cl:Landroid/widget/TextView;

    .line 88
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ei;->cm:Landroid/widget/FrameLayout;

    .line 89
    new-instance v0, Lcom/my/target/ed;

    invoke-direct {v0, p1}, Lcom/my/target/ed;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ei;->cp:Lcom/my/target/ed;

    .line 90
    new-instance v0, Lcom/my/target/ed;

    invoke-direct {v0, p1}, Lcom/my/target/ed;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ei;->cq:Lcom/my/target/ed;

    .line 91
    new-instance v0, Lcom/my/target/ed;

    invoke-direct {v0, p1}, Lcom/my/target/ed;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ei;->cr:Lcom/my/target/ed;

    .line 92
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    .line 93
    new-instance v0, Lcom/my/target/eg;

    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v1

    invoke-direct {v0, p1, v1, v9}, Lcom/my/target/eg;-><init>(Landroid/content/Context;Lcom/my/target/cm;Z)V

    iput-object v0, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    .line 94
    new-instance v0, Lcom/my/target/dz;

    invoke-direct {v0, p1}, Lcom/my/target/dz;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ei;->co:Lcom/my/target/dz;

    .line 95
    new-instance v0, Lcom/my/target/by;

    invoke-direct {v0, p1}, Lcom/my/target/by;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ei;->bp:Lcom/my/target/by;

    .line 96
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ei;->ck:Landroid/widget/LinearLayout;

    .line 97
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    .line 98
    new-instance v0, Lcom/my/target/ei$b;

    invoke-direct {v0, p0, v9}, Lcom/my/target/ei$b;-><init>(Lcom/my/target/ei;B)V

    iput-object v0, p0, Lcom/my/target/ei;->cs:Ljava/lang/Runnable;

    .line 99
    new-instance v0, Lcom/my/target/ei$c;

    invoke-direct {v0, p0, v9}, Lcom/my/target/ei$c;-><init>(Lcom/my/target/ei;B)V

    iput-object v0, p0, Lcom/my/target/ei;->ct:Lcom/my/target/ei$c;

    .line 100
    new-instance v0, Lcom/my/target/ei$a;

    invoke-direct {v0, p0, v9}, Lcom/my/target/ei$a;-><init>(Lcom/my/target/ei;B)V

    iput-object v0, p0, Lcom/my/target/ei;->cu:Lcom/my/target/ei$a;

    .line 102
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    const-string v1, "dismiss_button"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    const-string v1, "title_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    const-string v1, "stars_view"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    const-string v1, "cta_button"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/my/target/ei;->cl:Landroid/widget/TextView;

    const-string v1, "replay_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/my/target/ei;->cm:Landroid/widget/FrameLayout;

    const-string v1, "shadow"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/my/target/ei;->cp:Lcom/my/target/ed;

    const-string v1, "pause_button"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/my/target/ei;->cq:Lcom/my/target/ed;

    const-string v1, "play_button"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/my/target/ei;->cr:Lcom/my/target/ed;

    const-string v1, "replay_button"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    const-string v1, "domain_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    const-string v1, "media_view"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lcom/my/target/ei;->co:Lcom/my/target/dz;

    const-string v1, "video_progress_wheel"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/my/target/ei;->bp:Lcom/my/target/by;

    const-string v1, "sound_button"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    const/16 v1, 0x1c

    invoke-virtual {v0, v1}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iput v0, p0, Lcom/my/target/ei;->cz:I

    .line 116
    iget-object v0, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    invoke-virtual {v0, v8}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iput v0, p0, Lcom/my/target/ei;->padding:I

    .line 117
    iget-object v0, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    invoke-virtual {v0, v10}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iput v0, p0, Lcom/my/target/ei;->cv:I

    .line 1471
    const/high16 v0, -0x1000000

    invoke-virtual {p0, v0}, Lcom/my/target/ei;->setBackgroundColor(I)V

    .line 1472
    iget v6, p0, Lcom/my/target/ei;->padding:I

    .line 1474
    iget-object v0, p0, Lcom/my/target/ei;->bp:Lcom/my/target/by;

    sget v1, Lcom/my/target/ei;->bw:I

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setId(I)V

    .line 1476
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v3, -0x2

    invoke-direct {v0, v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1478
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1479
    iget-object v1, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    invoke-virtual {v1, v0}, Lcom/my/target/eg;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1480
    iget-object v0, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    iget-object v1, p0, Lcom/my/target/ei;->ct:Lcom/my/target/ei$c;

    invoke-virtual {v0, v1}, Lcom/my/target/eg;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1481
    iget-object v0, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/my/target/eg;->setBackgroundColor(I)V

    .line 1482
    iget-object v0, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    invoke-virtual {v0}, Lcom/my/target/eg;->H()V

    .line 1484
    iget-object v0, p0, Lcom/my/target/ei;->cm:Landroid/widget/FrameLayout;

    const/high16 v1, -0x67000000

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 1485
    iget-object v0, p0, Lcom/my/target/ei;->cm:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1487
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    const/4 v1, 0x2

    const/high16 v3, 0x41800000    # 16.0f

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1488
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1489
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1490
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1491
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 1492
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 1494
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setTextAlignment(I)V

    .line 1497
    :cond_0
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1498
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    const/high16 v1, -0x78000000

    iget-object v3, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    .line 1499
    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v3, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    invoke-virtual {v3, v10}, Lcom/my/target/cm;->n(I)I

    move-result v5

    move v3, v2

    .line 1498
    invoke-static/range {v0 .. v5}, Lcom/my/target/cm;->a(Landroid/view/View;IIIII)V

    .line 1501
    iget-object v0, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1502
    iget-object v0, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1503
    iget-object v0, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    const/4 v1, 0x2

    const/high16 v3, 0x41900000    # 18.0f

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1504
    iget-object v0, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1506
    iget-object v0, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    const/high16 v1, -0x78000000

    iget-object v3, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    .line 1507
    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v3, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    invoke-virtual {v3, v10}, Lcom/my/target/cm;->n(I)I

    move-result v5

    move v3, v2

    .line 1506
    invoke-static/range {v0 .. v5}, Lcom/my/target/cm;->a(Landroid/view/View;IIIII)V

    .line 1509
    iget-object v0, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTextColor(I)V

    .line 1510
    iget-object v0, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1511
    iget-object v0, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setGravity(I)V

    .line 1512
    iget-object v0, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    const/4 v1, 0x2

    const/high16 v3, 0x41800000    # 16.0f

    invoke-virtual {v0, v1, v3}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1513
    iget-object v0, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    iget-object v1, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    const/16 v3, 0x64

    invoke-virtual {v1, v3}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setMinimumWidth(I)V

    .line 1514
    iget-object v0, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    invoke-virtual {v0, v6, v6, v6, v6}, Landroid/widget/Button;->setPadding(IIII)V

    .line 1515
    iget-object v0, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v7}, Lcom/my/target/cm;->n(I)I

    move-result v1

    int-to-float v1, v1

    iget-object v3, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    .line 1516
    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    .line 1517
    invoke-virtual {v4, v7}, Lcom/my/target/cm;->n(I)I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, -0x1000000

    .line 1515
    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 1520
    iget-object v0, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    const v1, -0x333334

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1521
    iget-object v0, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxEms(I)V

    .line 1522
    iget-object v0, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v7}, Lcom/my/target/cm;->n(I)I

    move-result v1

    int-to-float v1, v1

    iget-object v3, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    .line 1523
    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    .line 1524
    invoke-virtual {v4, v7}, Lcom/my/target/cm;->n(I)I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, -0x1000000

    .line 1522
    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 1527
    iget-object v0, p0, Lcom/my/target/ei;->ck:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/ei;->cu:Lcom/my/target/ei$a;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1528
    iget-object v0, p0, Lcom/my/target/ei;->ck:Landroid/widget/LinearLayout;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 1529
    iget-object v0, p0, Lcom/my/target/ei;->ck:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1530
    iget-object v0, p0, Lcom/my/target/ei;->ck:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v3, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    invoke-virtual {v0, v1, v9, v3, v9}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 1532
    iget-object v0, p0, Lcom/my/target/ei;->cl:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 1533
    iget-object v0, p0, Lcom/my/target/ei;->cl:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1534
    iget-object v0, p0, Lcom/my/target/ei;->cl:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/ei;->cl:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1535
    iget-object v0, p0, Lcom/my/target/ei;->cl:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1536
    iget-object v0, p0, Lcom/my/target/ei;->cl:Landroid/widget/TextView;

    const/4 v1, 0x2

    const/high16 v3, 0x41800000    # 16.0f

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1537
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v0, -0x2

    const/4 v1, -0x2

    invoke-direct {v6, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1538
    iget-object v0, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    invoke-virtual {v0, v10}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iput v0, v6, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 1540
    iget-object v0, p0, Lcom/my/target/ei;->cr:Lcom/my/target/ed;

    iget-object v1, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v8}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v3, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    .line 1541
    invoke-virtual {v3, v8}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    .line 1542
    invoke-virtual {v4, v8}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v5, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    .line 1543
    invoke-virtual {v5, v8}, Lcom/my/target/cm;->n(I)I

    move-result v5

    .line 1540
    invoke-virtual {v0, v1, v3, v4, v5}, Lcom/my/target/ed;->setPadding(IIII)V

    .line 1545
    iget-object v0, p0, Lcom/my/target/ei;->cp:Lcom/my/target/ed;

    iget-object v1, p0, Lcom/my/target/ei;->cu:Lcom/my/target/ei$a;

    invoke-virtual {v0, v1}, Lcom/my/target/ed;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1546
    iget-object v0, p0, Lcom/my/target/ei;->cp:Lcom/my/target/ed;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/my/target/ed;->setVisibility(I)V

    .line 1547
    iget-object v0, p0, Lcom/my/target/ei;->cp:Lcom/my/target/ed;

    iget-object v1, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v8}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v3, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    .line 1548
    invoke-virtual {v3, v8}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    .line 1549
    invoke-virtual {v4, v8}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v5, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    .line 1550
    invoke-virtual {v5, v8}, Lcom/my/target/cm;->n(I)I

    move-result v5

    .line 1547
    invoke-virtual {v0, v1, v3, v4, v5}, Lcom/my/target/ed;->setPadding(IIII)V

    .line 1552
    iget-object v0, p0, Lcom/my/target/ei;->cq:Lcom/my/target/ed;

    iget-object v1, p0, Lcom/my/target/ei;->cu:Lcom/my/target/ei$a;

    invoke-virtual {v0, v1}, Lcom/my/target/ed;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1553
    iget-object v0, p0, Lcom/my/target/ei;->cq:Lcom/my/target/ed;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/my/target/ed;->setVisibility(I)V

    .line 1554
    iget-object v0, p0, Lcom/my/target/ei;->cq:Lcom/my/target/ed;

    iget-object v1, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v8}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v3, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    .line 1555
    invoke-virtual {v3, v8}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    .line 1556
    invoke-virtual {v4, v8}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v5, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    .line 1557
    invoke-virtual {v5, v8}, Lcom/my/target/cm;->n(I)I

    move-result v5

    .line 1554
    invoke-virtual {v0, v1, v3, v4, v5}, Lcom/my/target/ed;->setPadding(IIII)V

    .line 1559
    invoke-virtual {p0}, Lcom/my/target/ei;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2250
    const-string v1, "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AMXCy8fw79+rQAAAhVJREFUeNrt2y9IXlEYx3H3ooIiiCAIC4JgMRgsCyaLwWaxLK0srZhWVtYWVtYWlpYMNsvK0sKKRTANBivDIIggIiLiZ+URDncHFgzbznN+8d77nvPwvec99zz/xsa6uv4oPMWjzADgK55kBnCvj3icGQBc4hWmsgK41w/sZAPwswLiC9ayAJjGa1wNrt/hAxaaBlBcW8ReZTVc4CUmmwZQ3FvHYQXEd2w3DyDuj/AMJxUQn7HaNIDiuRm8wfUAwi3eY75pAMXzS9ivrIZz7GKiaQDF7zZwVAHxDVvNAyj2h+c4rYD4hJWmARRjzOItbir7wzvMNQ2gGGsZB5XVcIYXGG8aQDHmJo4rII6x2TyAGHc83vpZBcQBlpsGUIw/F/vA7QDCTewbs00DKOZZiS/DUKfxJRk1DaCYbyvOCkMdYaN5ADHnRJwazysg9rHUNIBi7vnwI4b7w3X4HTNNAyhsWA3PcqiT8ERHTQMobNmOWMNQh1hvHkDYMxlRp4sKiD0sNg2gsGsh4pB3AwhXEbecbhpAYd9aRKZVItgPT+v96wAKO3ciVzHUw9J6/wuAsHUqslaXFRC/pfVGY139L9A3wf4Z7AehfhTuzlB3h3tApIfE/jqAtEHRtGHx1ImRtKmxtMnRtOnx1AUSaUtk0hZJpS2TS10ombZUNm2xdPpy+d4w0VtmetNU2ra51I2TuVtnuxrWL/YiKQ6CN9uRAAAAAElFTkSuQmCC"

    invoke-static {v1, v9}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    .line 2251
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 2252
    const/16 v4, 0x1a4

    iput v4, v3, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    .line 2253
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v0, v3, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    .line 2254
    array-length v0, v1

    invoke-static {v1, v9, v0, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1560
    if-eqz v0, :cond_1

    .line 1562
    iget-object v1, p0, Lcom/my/target/ei;->cq:Lcom/my/target/ed;

    invoke-virtual {v1, v0}, Lcom/my/target/ed;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1564
    :cond_1
    invoke-virtual {p0}, Lcom/my/target/ei;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2259
    const-string v1, "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AMXCjITNKc0rQAAAJFJREFUeNrt2tENgCAMQEEwLuD+QzpC3cBURWLsvV+JNRfhi9YkSSpbP3sYETF0WO89s27m3KX6H1AeYL2wdrs5Y3/4ja/OTZ8B2f074h0z5zoDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/lr6rvDoK+xfmWsLNEmSVLUD47EiX/OuE8UAAAAASUVORK5CYII="

    invoke-static {v1, v9}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    .line 2260
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 2261
    const/16 v4, 0x1a4

    iput v4, v3, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    .line 2262
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v0, v3, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    .line 2263
    array-length v0, v1

    invoke-static {v1, v9, v0, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1565
    if-eqz v0, :cond_2

    .line 1567
    iget-object v1, p0, Lcom/my/target/ei;->cp:Lcom/my/target/ed;

    invoke-virtual {v1, v0}, Lcom/my/target/ed;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1570
    :cond_2
    iget-object v0, p0, Lcom/my/target/ei;->cp:Lcom/my/target/ed;

    const/high16 v1, -0x78000000

    iget-object v3, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    .line 1574
    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v3, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    .line 1575
    invoke-virtual {v3, v10}, Lcom/my/target/cm;->n(I)I

    move-result v5

    move v3, v2

    .line 1570
    invoke-static/range {v0 .. v5}, Lcom/my/target/cm;->a(Landroid/view/View;IIIII)V

    .line 1576
    iget-object v0, p0, Lcom/my/target/ei;->cq:Lcom/my/target/ed;

    const/high16 v1, -0x78000000

    iget-object v3, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    .line 1580
    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v3, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    .line 1581
    invoke-virtual {v3, v10}, Lcom/my/target/cm;->n(I)I

    move-result v5

    move v3, v2

    .line 1576
    invoke-static/range {v0 .. v5}, Lcom/my/target/cm;->a(Landroid/view/View;IIIII)V

    .line 1582
    iget-object v0, p0, Lcom/my/target/ei;->cr:Lcom/my/target/ed;

    const/high16 v1, -0x78000000

    iget-object v3, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    .line 1586
    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v3, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    .line 1587
    invoke-virtual {v3, v10}, Lcom/my/target/cm;->n(I)I

    move-result v5

    move v3, v2

    .line 1582
    invoke-static/range {v0 .. v5}, Lcom/my/target/cm;->a(Landroid/view/View;IIIII)V

    .line 1589
    iget-object v0, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    iget-object v1, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/ca;->setStarSize(I)V

    .line 1591
    iget-object v0, p0, Lcom/my/target/ei;->co:Lcom/my/target/dz;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/my/target/dz;->setVisibility(I)V

    .line 1593
    iget-object v0, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    invoke-virtual {p0, v0}, Lcom/my/target/ei;->addView(Landroid/view/View;)V

    .line 1594
    iget-object v0, p0, Lcom/my/target/ei;->cm:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/my/target/ei;->addView(Landroid/view/View;)V

    .line 1595
    iget-object v0, p0, Lcom/my/target/ei;->bp:Lcom/my/target/by;

    invoke-virtual {p0, v0}, Lcom/my/target/ei;->addView(Landroid/view/View;)V

    .line 1596
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/my/target/ei;->addView(Landroid/view/View;)V

    .line 1597
    iget-object v0, p0, Lcom/my/target/ei;->co:Lcom/my/target/dz;

    invoke-virtual {p0, v0}, Lcom/my/target/ei;->addView(Landroid/view/View;)V

    .line 1598
    iget-object v0, p0, Lcom/my/target/ei;->ck:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/my/target/ei;->addView(Landroid/view/View;)V

    .line 1599
    iget-object v0, p0, Lcom/my/target/ei;->cp:Lcom/my/target/ed;

    invoke-virtual {p0, v0}, Lcom/my/target/ei;->addView(Landroid/view/View;)V

    .line 1600
    iget-object v0, p0, Lcom/my/target/ei;->cq:Lcom/my/target/ed;

    invoke-virtual {p0, v0}, Lcom/my/target/ei;->addView(Landroid/view/View;)V

    .line 1601
    iget-object v0, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    invoke-virtual {p0, v0}, Lcom/my/target/ei;->addView(Landroid/view/View;)V

    .line 1602
    iget-object v0, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/my/target/ei;->addView(Landroid/view/View;)V

    .line 1603
    iget-object v0, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    invoke-virtual {p0, v0}, Lcom/my/target/ei;->addView(Landroid/view/View;)V

    .line 1604
    iget-object v0, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/my/target/ei;->addView(Landroid/view/View;)V

    .line 1606
    iget-object v0, p0, Lcom/my/target/ei;->ck:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/ei;->cr:Lcom/my/target/ed;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1607
    iget-object v0, p0, Lcom/my/target/ei;->ck:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/ei;->cl:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 120
    return-void
.end method

.method private B()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 719
    const/4 v0, 0x1

    iput v0, p0, Lcom/my/target/ei;->cw:I

    .line 720
    iget-object v0, p0, Lcom/my/target/ei;->ck:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 721
    iget-object v0, p0, Lcom/my/target/ei;->cq:Lcom/my/target/ed;

    invoke-virtual {v0, v1}, Lcom/my/target/ed;->setVisibility(I)V

    .line 722
    iget-object v0, p0, Lcom/my/target/ei;->cp:Lcom/my/target/ed;

    invoke-virtual {v0, v2}, Lcom/my/target/ed;->setVisibility(I)V

    .line 723
    iget-object v0, p0, Lcom/my/target/ei;->cm:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 724
    return-void
.end method

.method private C()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 732
    iget-object v0, p0, Lcom/my/target/ei;->ck:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 733
    iget-object v0, p0, Lcom/my/target/ei;->cq:Lcom/my/target/ed;

    invoke-virtual {v0, v2}, Lcom/my/target/ed;->setVisibility(I)V

    .line 734
    iget v0, p0, Lcom/my/target/ei;->cw:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 736
    iget-object v0, p0, Lcom/my/target/ei;->cp:Lcom/my/target/ed;

    invoke-virtual {v0, v2}, Lcom/my/target/ed;->setVisibility(I)V

    .line 738
    :cond_0
    return-void
.end method

.method private J()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 707
    const/4 v0, 0x0

    iput v0, p0, Lcom/my/target/ei;->cw:I

    .line 708
    iget-object v0, p0, Lcom/my/target/ei;->ck:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 709
    iget-object v0, p0, Lcom/my/target/ei;->cq:Lcom/my/target/ed;

    invoke-virtual {v0, v1}, Lcom/my/target/ed;->setVisibility(I)V

    .line 710
    iget-object v0, p0, Lcom/my/target/ei;->cp:Lcom/my/target/ed;

    invoke-virtual {v0, v1}, Lcom/my/target/ed;->setVisibility(I)V

    .line 711
    iget-object v0, p0, Lcom/my/target/ei;->cm:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 712
    return-void
.end method

.method static synthetic a(Lcom/my/target/ei;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/my/target/ei;->ck:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic b(Lcom/my/target/ei;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/my/target/ei;->J()V

    return-void
.end method

.method static synthetic c(Lcom/my/target/ei;)Lcom/my/target/ed;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/my/target/ei;->cp:Lcom/my/target/ed;

    return-object v0
.end method

.method static synthetic d(Lcom/my/target/ei;)Lcom/my/target/eg;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    return-object v0
.end method

.method static synthetic e(Lcom/my/target/ei;)Lcom/my/target/ed;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/my/target/ei;->cq:Lcom/my/target/ed;

    return-object v0
.end method

.method static synthetic f(Lcom/my/target/ei;)I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/my/target/ei;->cw:I

    return v0
.end method

.method static synthetic g(Lcom/my/target/ei;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/my/target/ei;->cs:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic h(Lcom/my/target/ei;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 38
    .line 3761
    const/4 v0, 0x2

    iput v0, p0, Lcom/my/target/ei;->cw:I

    .line 3762
    iget-object v0, p0, Lcom/my/target/ei;->ck:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3763
    iget-object v0, p0, Lcom/my/target/ei;->cq:Lcom/my/target/ed;

    invoke-virtual {v0, v2}, Lcom/my/target/ed;->setVisibility(I)V

    .line 3764
    iget-object v0, p0, Lcom/my/target/ei;->cp:Lcom/my/target/ed;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/my/target/ed;->setVisibility(I)V

    .line 3765
    iget-object v0, p0, Lcom/my/target/ei;->cm:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 38
    return-void
.end method


# virtual methods
.method public final G()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, -0x1

    .line 206
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/ei;->cA:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    const/4 v1, 0x2

    const/high16 v3, 0x41800000    # 16.0f

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 208
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 209
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 210
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 211
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    iget v1, p0, Lcom/my/target/ei;->padding:I

    iget v3, p0, Lcom/my/target/ei;->padding:I

    iget v4, p0, Lcom/my/target/ei;->padding:I

    iget v5, p0, Lcom/my/target/ei;->padding:I

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 212
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    const/high16 v1, -0x78000000

    iget-object v3, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    .line 213
    invoke-virtual {v3, v6}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v3, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    const/4 v5, 0x4

    invoke-virtual {v3, v5}, Lcom/my/target/cm;->n(I)I

    move-result v5

    move v3, v2

    .line 212
    invoke-static/range {v0 .. v5}, Lcom/my/target/cm;->a(Landroid/view/View;IIIII)V

    .line 214
    iput-boolean v6, p0, Lcom/my/target/ei;->cC:Z

    .line 215
    return-void
.end method

.method public final b(Lcom/my/target/core/models/banners/h;)V
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/my/target/eg;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 268
    iget-object v0, p0, Lcom/my/target/ei;->bp:Lcom/my/target/by;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setVisibility(I)V

    .line 269
    invoke-virtual {p0}, Lcom/my/target/ei;->G()V

    .line 270
    invoke-direct {p0}, Lcom/my/target/ei;->J()V

    .line 271
    return-void
.end method

.method public final e(I)V
    .locals 1

    .prologue
    .line 694
    iget-object v0, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    invoke-virtual {v0, p1}, Lcom/my/target/eg;->e(I)V

    .line 695
    return-void
.end method

.method public final f(Z)V
    .locals 2

    .prologue
    .line 681
    iget-object v0, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/my/target/eg;->h(Z)V

    .line 682
    return-void
.end method

.method public final finish()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 220
    iget-object v0, p0, Lcom/my/target/ei;->co:Lcom/my/target/dz;

    invoke-virtual {v0, v1}, Lcom/my/target/dz;->setVisibility(I)V

    .line 3745
    const/4 v0, 0x4

    iput v0, p0, Lcom/my/target/ei;->cw:I

    .line 3746
    iget-boolean v0, p0, Lcom/my/target/ei;->cy:Z

    if-eqz v0, :cond_0

    .line 3748
    iget-object v0, p0, Lcom/my/target/ei;->ck:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3749
    iget-object v0, p0, Lcom/my/target/ei;->cm:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 3752
    :cond_0
    iget-object v0, p0, Lcom/my/target/ei;->cq:Lcom/my/target/ed;

    invoke-virtual {v0, v1}, Lcom/my/target/ed;->setVisibility(I)V

    .line 3753
    iget-object v0, p0, Lcom/my/target/ei;->cp:Lcom/my/target/ed;

    invoke-virtual {v0, v1}, Lcom/my/target/ed;->setVisibility(I)V

    .line 222
    return-void
.end method

.method public final getCloseButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getSoundButton()Lcom/my/target/by;
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/my/target/ei;->bp:Lcom/my/target/by;

    return-object v0
.end method

.method public final isPaused()Z
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    invoke-virtual {v0}, Lcom/my/target/eg;->isPaused()Z

    move-result v0

    return v0
.end method

.method public final isPlaying()Z
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    invoke-virtual {v0}, Lcom/my/target/eg;->isPlaying()Z

    move-result v0

    return v0
.end method

.method protected final onLayout(ZIIII)V
    .locals 8
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 361
    sub-int v0, p4, p2

    .line 362
    sub-int v1, p5, p3

    .line 364
    iget-object v2, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    invoke-virtual {v2}, Lcom/my/target/eg;->getMeasuredWidth()I

    move-result v2

    .line 365
    iget-object v3, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    invoke-virtual {v3}, Lcom/my/target/eg;->getMeasuredHeight()I

    move-result v3

    .line 367
    sub-int v4, v0, v2

    shr-int/lit8 v4, v4, 0x1

    .line 368
    sub-int v5, v1, v3

    shr-int/lit8 v5, v5, 0x1

    .line 369
    iget-object v6, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    add-int/2addr v2, v4

    add-int/2addr v3, v5

    invoke-virtual {v6, v4, v5, v2, v3}, Lcom/my/target/eg;->layout(IIII)V

    .line 370
    iget-object v2, p0, Lcom/my/target/ei;->cm:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    invoke-virtual {v3}, Lcom/my/target/eg;->getLeft()I

    move-result v3

    iget-object v4, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    invoke-virtual {v4}, Lcom/my/target/eg;->getTop()I

    move-result v4

    iget-object v5, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    invoke-virtual {v5}, Lcom/my/target/eg;->getRight()I

    move-result v5

    iget-object v6, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    invoke-virtual {v6}, Lcom/my/target/eg;->getBottom()I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/widget/FrameLayout;->layout(IIII)V

    .line 372
    iget-object v2, p0, Lcom/my/target/ei;->cq:Lcom/my/target/ed;

    invoke-virtual {v2}, Lcom/my/target/ed;->getMeasuredWidth()I

    move-result v2

    .line 373
    iget-object v3, p0, Lcom/my/target/ei;->cq:Lcom/my/target/ed;

    invoke-virtual {v3}, Lcom/my/target/ed;->getMeasuredHeight()I

    move-result v3

    .line 374
    iget-object v4, p0, Lcom/my/target/ei;->cq:Lcom/my/target/ed;

    shr-int/lit8 v5, p4, 0x1

    shr-int/lit8 v6, v2, 0x1

    sub-int/2addr v5, v6

    shr-int/lit8 v6, p5, 0x1

    shr-int/lit8 v7, v3, 0x1

    sub-int/2addr v6, v7

    shr-int/lit8 v7, p4, 0x1

    shr-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v7

    shr-int/lit8 v7, p5, 0x1

    shr-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v7

    invoke-virtual {v4, v5, v6, v2, v3}, Lcom/my/target/ed;->layout(IIII)V

    .line 379
    iget-object v2, p0, Lcom/my/target/ei;->cp:Lcom/my/target/ed;

    invoke-virtual {v2}, Lcom/my/target/ed;->getMeasuredWidth()I

    move-result v2

    .line 380
    iget-object v3, p0, Lcom/my/target/ei;->cp:Lcom/my/target/ed;

    invoke-virtual {v3}, Lcom/my/target/ed;->getMeasuredHeight()I

    move-result v3

    .line 381
    iget-object v4, p0, Lcom/my/target/ei;->cp:Lcom/my/target/ed;

    shr-int/lit8 v5, p4, 0x1

    shr-int/lit8 v6, v2, 0x1

    sub-int/2addr v5, v6

    shr-int/lit8 v6, p5, 0x1

    shr-int/lit8 v7, v3, 0x1

    sub-int/2addr v6, v7

    shr-int/lit8 v7, p4, 0x1

    shr-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v7

    shr-int/lit8 v7, p5, 0x1

    shr-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v7

    invoke-virtual {v4, v5, v6, v2, v3}, Lcom/my/target/ed;->layout(IIII)V

    .line 386
    iget-object v2, p0, Lcom/my/target/ei;->ck:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v2

    .line 387
    iget-object v3, p0, Lcom/my/target/ei;->ck:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v3

    .line 388
    iget-object v4, p0, Lcom/my/target/ei;->ck:Landroid/widget/LinearLayout;

    shr-int/lit8 v5, p4, 0x1

    shr-int/lit8 v6, v2, 0x1

    sub-int/2addr v5, v6

    shr-int/lit8 v6, p5, 0x1

    shr-int/lit8 v7, v3, 0x1

    sub-int/2addr v6, v7

    shr-int/lit8 v7, p4, 0x1

    shr-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v7

    shr-int/lit8 v7, p5, 0x1

    shr-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v7

    invoke-virtual {v4, v5, v6, v2, v3}, Landroid/widget/LinearLayout;->layout(IIII)V

    .line 393
    iget-object v2, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    iget v3, p0, Lcom/my/target/ei;->padding:I

    iget v4, p0, Lcom/my/target/ei;->padding:I

    iget v5, p0, Lcom/my/target/ei;->padding:I

    iget-object v6, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    .line 395
    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v5, v6

    iget v6, p0, Lcom/my/target/ei;->padding:I

    iget-object v7, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    .line 396
    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v6, v7

    .line 393
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/widget/TextView;->layout(IIII)V

    .line 398
    if-le v0, v1, :cond_0

    .line 400
    iget-object v2, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v2

    iget-object v3, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    .line 401
    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    iget-object v4, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    .line 402
    invoke-virtual {v4}, Lcom/my/target/ca;->getMeasuredHeight()I

    move-result v4

    .line 401
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 400
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 405
    iget-object v3, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    iget v4, p0, Lcom/my/target/ei;->padding:I

    sub-int v4, v0, v4

    iget-object v5, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    invoke-virtual {v5}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v5

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/my/target/ei;->padding:I

    sub-int v5, v1, v5

    iget-object v6, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    .line 406
    invoke-virtual {v6}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v6

    sub-int/2addr v5, v6

    iget-object v6, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    invoke-virtual {v6}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v6

    sub-int v6, v2, v6

    shr-int/lit8 v6, v6, 0x1

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/my/target/ei;->padding:I

    sub-int/2addr v0, v6

    iget v6, p0, Lcom/my/target/ei;->padding:I

    sub-int v6, v1, v6

    iget-object v7, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    .line 408
    invoke-virtual {v7}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v7

    sub-int v7, v2, v7

    shr-int/lit8 v7, v7, 0x1

    sub-int/2addr v6, v7

    .line 405
    invoke-virtual {v3, v4, v5, v0, v6}, Landroid/widget/Button;->layout(IIII)V

    .line 409
    iget-object v0, p0, Lcom/my/target/ei;->bp:Lcom/my/target/by;

    iget-object v3, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getRight()I

    move-result v3

    iget-object v4, p0, Lcom/my/target/ei;->bp:Lcom/my/target/by;

    invoke-virtual {v4}, Lcom/my/target/by;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/my/target/ei;->bp:Lcom/my/target/by;

    invoke-virtual {v4}, Lcom/my/target/by;->getPadding()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    .line 410
    invoke-virtual {v4}, Lcom/my/target/eg;->getBottom()I

    move-result v4

    iget v5, p0, Lcom/my/target/ei;->padding:I

    shl-int/lit8 v5, v5, 0x1

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/ei;->bp:Lcom/my/target/by;

    invoke-virtual {v5}, Lcom/my/target/by;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v4, v5

    sub-int/2addr v4, v2

    iget-object v5, p0, Lcom/my/target/ei;->bp:Lcom/my/target/by;

    invoke-virtual {v5}, Lcom/my/target/by;->getPadding()I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    .line 411
    invoke-virtual {v5}, Landroid/widget/Button;->getRight()I

    move-result v5

    iget-object v6, p0, Lcom/my/target/ei;->bp:Lcom/my/target/by;

    invoke-virtual {v6}, Lcom/my/target/by;->getPadding()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    .line 412
    invoke-virtual {v6}, Lcom/my/target/eg;->getBottom()I

    move-result v6

    iget v7, p0, Lcom/my/target/ei;->padding:I

    shl-int/lit8 v7, v7, 0x1

    sub-int/2addr v6, v7

    sub-int/2addr v6, v2

    iget-object v7, p0, Lcom/my/target/ei;->bp:Lcom/my/target/by;

    invoke-virtual {v7}, Lcom/my/target/by;->getPadding()I

    move-result v7

    add-int/2addr v6, v7

    .line 409
    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/my/target/by;->layout(IIII)V

    .line 414
    iget-object v0, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    iget-object v3, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getLeft()I

    move-result v3

    iget v4, p0, Lcom/my/target/ei;->padding:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    invoke-virtual {v4}, Lcom/my/target/ca;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/my/target/ei;->padding:I

    sub-int v4, v1, v4

    iget-object v5, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    .line 415
    invoke-virtual {v5}, Lcom/my/target/ca;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    invoke-virtual {v5}, Lcom/my/target/ca;->getMeasuredHeight()I

    move-result v5

    sub-int v5, v2, v5

    shr-int/lit8 v5, v5, 0x1

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    .line 416
    invoke-virtual {v5}, Landroid/widget/Button;->getLeft()I

    move-result v5

    iget v6, p0, Lcom/my/target/ei;->padding:I

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/my/target/ei;->padding:I

    sub-int v6, v1, v6

    iget-object v7, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    .line 417
    invoke-virtual {v7}, Lcom/my/target/ca;->getMeasuredHeight()I

    move-result v7

    sub-int v7, v2, v7

    shr-int/lit8 v7, v7, 0x1

    sub-int/2addr v6, v7

    .line 414
    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/my/target/ca;->layout(IIII)V

    .line 418
    iget-object v0, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getLeft()I

    move-result v3

    iget v4, p0, Lcom/my/target/ei;->padding:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/my/target/ei;->padding:I

    sub-int v4, v1, v4

    iget-object v5, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    .line 419
    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    sub-int v5, v2, v5

    shr-int/lit8 v5, v5, 0x1

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    .line 420
    invoke-virtual {v5}, Landroid/widget/Button;->getLeft()I

    move-result v5

    iget v6, p0, Lcom/my/target/ei;->padding:I

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/my/target/ei;->padding:I

    sub-int v6, v1, v6

    iget-object v7, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    .line 421
    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v7

    sub-int v7, v2, v7

    shr-int/lit8 v7, v7, 0x1

    sub-int/2addr v6, v7

    .line 418
    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/widget/TextView;->layout(IIII)V

    .line 423
    iget-object v0, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    invoke-virtual {v0}, Lcom/my/target/ca;->getLeft()I

    move-result v0

    iget-object v3, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getLeft()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 424
    iget-object v3, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    iget v4, p0, Lcom/my/target/ei;->padding:I

    sub-int v4, v0, v4

    iget-object v5, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/my/target/ei;->padding:I

    sub-int v5, v1, v5

    iget-object v6, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    .line 425
    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    sub-int/2addr v5, v6

    iget-object v6, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    sub-int v6, v2, v6

    shr-int/lit8 v6, v6, 0x1

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/my/target/ei;->padding:I

    sub-int/2addr v0, v6

    iget v6, p0, Lcom/my/target/ei;->padding:I

    sub-int v6, v1, v6

    iget-object v7, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    .line 427
    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v7

    sub-int v7, v2, v7

    shr-int/lit8 v7, v7, 0x1

    sub-int/2addr v6, v7

    .line 424
    invoke-virtual {v3, v4, v5, v0, v6}, Landroid/widget/TextView;->layout(IIII)V

    .line 429
    iget-object v0, p0, Lcom/my/target/ei;->co:Lcom/my/target/dz;

    iget v3, p0, Lcom/my/target/ei;->padding:I

    iget v4, p0, Lcom/my/target/ei;->padding:I

    sub-int v4, v1, v4

    iget-object v5, p0, Lcom/my/target/ei;->co:Lcom/my/target/dz;

    .line 430
    invoke-virtual {v5}, Lcom/my/target/dz;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/ei;->co:Lcom/my/target/dz;

    invoke-virtual {v5}, Lcom/my/target/dz;->getMeasuredHeight()I

    move-result v5

    sub-int v5, v2, v5

    shr-int/lit8 v5, v5, 0x1

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/my/target/ei;->padding:I

    iget-object v6, p0, Lcom/my/target/ei;->co:Lcom/my/target/dz;

    .line 431
    invoke-virtual {v6}, Lcom/my/target/dz;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v5, v6

    iget v6, p0, Lcom/my/target/ei;->padding:I

    sub-int/2addr v1, v6

    iget-object v6, p0, Lcom/my/target/ei;->co:Lcom/my/target/dz;

    .line 432
    invoke-virtual {v6}, Lcom/my/target/dz;->getMeasuredHeight()I

    move-result v6

    sub-int/2addr v2, v6

    shr-int/lit8 v2, v2, 0x1

    sub-int/2addr v1, v2

    .line 429
    invoke-virtual {v0, v3, v4, v5, v1}, Lcom/my/target/dz;->layout(IIII)V

    .line 467
    :goto_0
    return-void

    .line 437
    :cond_0
    iget-object v1, p0, Lcom/my/target/ei;->bp:Lcom/my/target/by;

    iget-object v2, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    invoke-virtual {v2}, Lcom/my/target/eg;->getRight()I

    move-result v2

    iget v3, p0, Lcom/my/target/ei;->padding:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/my/target/ei;->bp:Lcom/my/target/by;

    invoke-virtual {v3}, Lcom/my/target/by;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/my/target/ei;->bp:Lcom/my/target/by;

    invoke-virtual {v3}, Lcom/my/target/by;->getPadding()I

    move-result v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    .line 438
    invoke-virtual {v3}, Lcom/my/target/eg;->getBottom()I

    move-result v3

    iget v4, p0, Lcom/my/target/ei;->padding:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/my/target/ei;->bp:Lcom/my/target/by;

    invoke-virtual {v4}, Lcom/my/target/by;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/my/target/ei;->bp:Lcom/my/target/by;

    invoke-virtual {v4}, Lcom/my/target/by;->getPadding()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    .line 439
    invoke-virtual {v4}, Lcom/my/target/eg;->getRight()I

    move-result v4

    iget v5, p0, Lcom/my/target/ei;->padding:I

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/ei;->bp:Lcom/my/target/by;

    invoke-virtual {v5}, Lcom/my/target/by;->getPadding()I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    .line 440
    invoke-virtual {v5}, Lcom/my/target/eg;->getBottom()I

    move-result v5

    iget v6, p0, Lcom/my/target/ei;->padding:I

    sub-int/2addr v5, v6

    iget-object v6, p0, Lcom/my/target/ei;->bp:Lcom/my/target/by;

    invoke-virtual {v6}, Lcom/my/target/by;->getPadding()I

    move-result v6

    add-int/2addr v5, v6

    .line 437
    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/my/target/by;->layout(IIII)V

    .line 442
    iget-object v1, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    shr-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    shr-int/lit8 v3, v3, 0x1

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    .line 443
    invoke-virtual {v3}, Lcom/my/target/eg;->getBottom()I

    move-result v3

    iget v4, p0, Lcom/my/target/ei;->padding:I

    add-int/2addr v3, v4

    shr-int/lit8 v4, v0, 0x1

    iget-object v5, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    .line 444
    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    shr-int/lit8 v5, v5, 0x1

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    .line 445
    invoke-virtual {v5}, Lcom/my/target/eg;->getBottom()I

    move-result v5

    iget v6, p0, Lcom/my/target/ei;->padding:I

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    .line 442
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/TextView;->layout(IIII)V

    .line 448
    iget-object v1, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    shr-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    invoke-virtual {v3}, Lcom/my/target/ca;->getMeasuredWidth()I

    move-result v3

    shr-int/lit8 v3, v3, 0x1

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    .line 449
    invoke-virtual {v3}, Landroid/widget/TextView;->getBottom()I

    move-result v3

    iget v4, p0, Lcom/my/target/ei;->padding:I

    add-int/2addr v3, v4

    shr-int/lit8 v4, v0, 0x1

    iget-object v5, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    .line 450
    invoke-virtual {v5}, Lcom/my/target/ca;->getMeasuredWidth()I

    move-result v5

    shr-int/lit8 v5, v5, 0x1

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    .line 451
    invoke-virtual {v5}, Landroid/widget/TextView;->getBottom()I

    move-result v5

    iget v6, p0, Lcom/my/target/ei;->padding:I

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    invoke-virtual {v6}, Lcom/my/target/ca;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    .line 448
    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/my/target/ca;->layout(IIII)V

    .line 453
    iget-object v1, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    shr-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    shr-int/lit8 v3, v3, 0x1

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    .line 454
    invoke-virtual {v3}, Landroid/widget/TextView;->getBottom()I

    move-result v3

    iget v4, p0, Lcom/my/target/ei;->padding:I

    add-int/2addr v3, v4

    shr-int/lit8 v4, v0, 0x1

    iget-object v5, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    .line 455
    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    shr-int/lit8 v5, v5, 0x1

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    .line 456
    invoke-virtual {v5}, Landroid/widget/TextView;->getBottom()I

    move-result v5

    iget v6, p0, Lcom/my/target/ei;->padding:I

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    .line 453
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/TextView;->layout(IIII)V

    .line 458
    iget-object v1, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    shr-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v3

    shr-int/lit8 v3, v3, 0x1

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    .line 459
    invoke-virtual {v3}, Lcom/my/target/ca;->getBottom()I

    move-result v3

    iget v4, p0, Lcom/my/target/ei;->padding:I

    add-int/2addr v3, v4

    shr-int/lit8 v0, v0, 0x1

    iget-object v4, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    .line 460
    invoke-virtual {v4}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v4

    shr-int/lit8 v4, v4, 0x1

    add-int/2addr v0, v4

    iget-object v4, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    .line 461
    invoke-virtual {v4}, Lcom/my/target/ca;->getBottom()I

    move-result v4

    iget v5, p0, Lcom/my/target/ei;->padding:I

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    invoke-virtual {v5}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 458
    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/widget/Button;->layout(IIII)V

    .line 462
    iget-object v0, p0, Lcom/my/target/ei;->co:Lcom/my/target/dz;

    iget v1, p0, Lcom/my/target/ei;->padding:I

    iget-object v2, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    .line 463
    invoke-virtual {v2}, Lcom/my/target/eg;->getBottom()I

    move-result v2

    iget v3, p0, Lcom/my/target/ei;->padding:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/my/target/ei;->co:Lcom/my/target/dz;

    invoke-virtual {v3}, Lcom/my/target/dz;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/my/target/ei;->padding:I

    iget-object v4, p0, Lcom/my/target/ei;->co:Lcom/my/target/dz;

    .line 464
    invoke-virtual {v4}, Lcom/my/target/dz;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    .line 465
    invoke-virtual {v4}, Lcom/my/target/eg;->getBottom()I

    move-result v4

    iget v5, p0, Lcom/my/target/ei;->padding:I

    sub-int/2addr v4, v5

    .line 462
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/my/target/dz;->layout(IIII)V

    goto/16 :goto_0
.end method

.method protected final onMeasure(II)V
    .locals 9
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v8, -0x80000000

    .line 277
    iget-object v0, p0, Lcom/my/target/ei;->bp:Lcom/my/target/by;

    iget v1, p0, Lcom/my/target/ei;->cz:I

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget v2, p0, Lcom/my/target/ei;->cz:I

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/my/target/by;->measure(II)V

    .line 278
    iget-object v0, p0, Lcom/my/target/ei;->co:Lcom/my/target/dz;

    iget v1, p0, Lcom/my/target/ei;->cz:I

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget v2, p0, Lcom/my/target/ei;->cz:I

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/my/target/dz;->measure(II)V

    .line 280
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 281
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 284
    iget-object v2, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/my/target/eg;->measure(II)V

    .line 286
    iget v2, p0, Lcom/my/target/ei;->padding:I

    shl-int/lit8 v2, v2, 0x1

    sub-int v2, v0, v2

    .line 287
    iget v3, p0, Lcom/my/target/ei;->padding:I

    shl-int/lit8 v3, v3, 0x1

    sub-int v3, v1, v3

    .line 289
    iget-object v4, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    div-int/lit8 v5, v2, 0x2

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->measure(II)V

    .line 290
    iget-object v4, p0, Lcom/my/target/ei;->cp:Lcom/my/target/ed;

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/my/target/ed;->measure(II)V

    .line 291
    iget-object v4, p0, Lcom/my/target/ei;->cq:Lcom/my/target/ed;

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/my/target/ed;->measure(II)V

    .line 293
    iget-object v4, p0, Lcom/my/target/ei;->ck:Landroid/widget/LinearLayout;

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/LinearLayout;->measure(II)V

    .line 294
    iget-object v4, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/my/target/ca;->measure(II)V

    .line 297
    iget-object v4, p0, Lcom/my/target/ei;->cm:Landroid/widget/FrameLayout;

    iget-object v5, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    invoke-virtual {v5}, Lcom/my/target/eg;->getMeasuredWidth()I

    move-result v5

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    iget-object v6, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    invoke-virtual {v6}, Lcom/my/target/eg;->getMeasuredHeight()I

    move-result v6

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/FrameLayout;->measure(II)V

    .line 299
    iget-object v4, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/Button;->measure(II)V

    .line 300
    iget-object v4, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->measure(II)V

    .line 301
    iget-object v4, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->measure(II)V

    .line 303
    if-le v0, v1, :cond_0

    .line 305
    iget-object v4, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v4

    .line 306
    iget-object v5, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    .line 307
    iget-object v6, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    invoke-virtual {v6}, Lcom/my/target/ca;->getMeasuredWidth()I

    move-result v6

    iget-object v7, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 308
    iget-object v7, p0, Lcom/my/target/ei;->co:Lcom/my/target/dz;

    invoke-virtual {v7}, Lcom/my/target/dz;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v5, v7

    add-int/2addr v5, v6

    add-int/2addr v4, v5

    iget v5, p0, Lcom/my/target/ei;->padding:I

    mul-int/lit8 v5, v5, 0x3

    add-int/2addr v4, v5

    .line 313
    if-le v4, v2, :cond_0

    .line 315
    iget-object v4, p0, Lcom/my/target/ei;->co:Lcom/my/target/dz;

    invoke-virtual {v4}, Lcom/my/target/dz;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v2, v4

    iget v4, p0, Lcom/my/target/ei;->padding:I

    mul-int/lit8 v4, v4, 0x3

    sub-int/2addr v2, v4

    .line 316
    iget-object v4, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    div-int/lit8 v5, v2, 0x3

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/Button;->measure(II)V

    .line 317
    iget-object v4, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    div-int/lit8 v5, v2, 0x3

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/my/target/ca;->measure(II)V

    .line 318
    iget-object v4, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    div-int/lit8 v5, v2, 0x3

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->measure(II)V

    .line 320
    iget-object v4, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v2, v4

    iget-object v4, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v2, v4

    iget-object v4, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    invoke-virtual {v4}, Lcom/my/target/ca;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v2, v4

    .line 321
    iget-object v4, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v4, v2, v3}, Landroid/widget/TextView;->measure(II)V

    .line 324
    :cond_0
    invoke-virtual {p0, v0, v1}, Lcom/my/target/ei;->setMeasuredDimension(II)V

    .line 325
    return-void
.end method

.method public final pause()V
    .locals 1

    .prologue
    .line 687
    invoke-direct {p0}, Lcom/my/target/ei;->B()V

    .line 688
    iget-object v0, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    invoke-virtual {v0}, Lcom/my/target/eg;->pause()V

    .line 689
    return-void
.end method

.method public final play()V
    .locals 1

    .prologue
    .line 354
    invoke-direct {p0}, Lcom/my/target/ei;->C()V

    .line 355
    iget-object v0, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    invoke-virtual {v0}, Lcom/my/target/eg;->I()V

    .line 356
    return-void
.end method

.method public final resume()V
    .locals 1

    .prologue
    .line 621
    invoke-direct {p0}, Lcom/my/target/ei;->C()V

    .line 622
    iget-object v0, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    invoke-virtual {v0}, Lcom/my/target/eg;->resume()V

    .line 623
    return-void
.end method

.method public final setBanner(Lcom/my/target/core/models/banners/h;)V
    .locals 9
    .param p1, "banner"    # Lcom/my/target/core/models/banners/h;

    .prologue
    const v3, -0x333334

    const/high16 v1, -0x78000000

    const/4 v5, 0x0

    const/16 v4, 0x8

    const/4 v8, 0x0

    .line 125
    invoke-super {p0, p1}, Lcom/my/target/ee;->setBanner(Lcom/my/target/core/models/banners/h;)V

    .line 126
    iget-object v0, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    iget v2, p0, Lcom/my/target/ei;->style:I

    invoke-virtual {v0, p1, v2}, Lcom/my/target/eg;->a(Lcom/my/target/core/models/banners/h;I)V

    .line 127
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getVideoBanner()Lcom/my/target/aj;

    move-result-object v6

    .line 128
    if-nez v6, :cond_0

    .line 201
    :goto_0
    return-void

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/my/target/ei;->co:Lcom/my/target/dz;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getDuration()F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/my/target/dz;->setMax(F)V

    .line 135
    invoke-virtual {v6}, Lcom/my/target/aj;->isAllowReplay()Z

    move-result v0

    iput-boolean v0, p0, Lcom/my/target/ei;->cy:Z

    .line 136
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getAllowCloseDelay()F

    move-result v0

    iput v0, p0, Lcom/my/target/ei;->cx:F

    .line 137
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->isAllowClose()Z

    move-result v0

    iput-boolean v0, p0, Lcom/my/target/ei;->allowClose:Z

    .line 139
    iget-object v0, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getCtaText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 140
    iget-object v0, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    const-string v0, "store"

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getNavigationType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 144
    iget-object v0, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 145
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getVotes()I

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getRating()F

    move-result v0

    cmpl-float v0, v0, v5

    if-lez v0, :cond_3

    .line 147
    iget-object v0, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    invoke-virtual {v0, v8}, Lcom/my/target/ca;->setVisibility(I)V

    .line 148
    iget-object v0, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getRating()F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/my/target/ca;->setRating(F)V

    .line 162
    :goto_1
    invoke-virtual {v6}, Lcom/my/target/aj;->getCloseActionText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/ei;->cA:Ljava/lang/String;

    .line 163
    invoke-virtual {v6}, Lcom/my/target/aj;->getCloseDelayActionText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/ei;->cB:Ljava/lang/String;

    .line 164
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/my/target/ei;->cA:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    invoke-virtual {v6}, Lcom/my/target/aj;->isAllowClose()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 168
    invoke-virtual {v6}, Lcom/my/target/aj;->getAllowCloseDelay()F

    move-result v0

    cmpl-float v0, v0, v5

    if-lez v0, :cond_5

    .line 170
    invoke-virtual {v6}, Lcom/my/target/aj;->getAllowCloseDelay()F

    move-result v0

    iput v0, p0, Lcom/my/target/ei;->cx:F

    .line 171
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 172
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 173
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    iget v2, p0, Lcom/my/target/ei;->cv:I

    iget v4, p0, Lcom/my/target/ei;->cv:I

    iget v5, p0, Lcom/my/target/ei;->cv:I

    iget v7, p0, Lcom/my/target/ei;->cv:I

    invoke-virtual {v0, v2, v4, v5, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 174
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    const/4 v4, 0x1

    .line 175
    invoke-virtual {v2, v4}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v2, p0, Lcom/my/target/ei;->aw:Lcom/my/target/cm;

    const/4 v5, 0x4

    invoke-virtual {v2, v5}, Lcom/my/target/cm;->n(I)I

    move-result v5

    move v2, v1

    .line 174
    invoke-static/range {v0 .. v5}, Lcom/my/target/cm;->a(Landroid/view/View;IIIII)V

    .line 176
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    const/4 v1, 0x2

    const/high16 v2, 0x41400000    # 12.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 185
    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/my/target/ei;->cl:Landroid/widget/TextView;

    invoke-virtual {v6}, Lcom/my/target/aj;->getReplayActionText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    invoke-virtual {p0}, Lcom/my/target/ei;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 3241
    const-string v1, "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AMXCjM59gfMOgAAA59JREFUeNrtmkloFEEUhl/N6KgxriiJOHEXF4gENYlgRFA8uyAoCNGggl68CCJ6EQx6cCFqUFzABQQRMYh4EfSi4IJbiFERQVxRgxuRMWri5yE1UBY9yWh6Znq6+z+96a6ZV/8/tbz3qkRChAgRwiMAFgJngWgQyVcDHXTiFKCCRH4Df6MFKAkK+W0W+VfAxCAQV8A+i/wTIB4E8lHgpEX+LjA8COQHAM8t8o3ATKC338kXA59IjXbgEVAPLAb6+k2AHRbhVrrGV+AwMNVPIpx3INncjRi/gTPAWL+IcMIieBGIAXFgGXAU+OggRALY7ItIEdhvkTsNRIz3MWCp3h1sXAGK/RAHnLKIHUzRdgnwwmr7Epjsh3jggkWsNkXbQuC4Q8g8Ld9F6ANctYht7KL9GiNpAnib94ujDo5uaUIdwMo00uafhghNQL98F2EYcA9YlGb75daoOeqHnSHyj+23WyLMC1oRJQpct7LJXkETYSrwyxBhlQQNwAFrFKigCTDK2hXmBnEUnDMEOBZEARYaArwLogAF1jRIq4YQ8YsASqmEiNw2HpUHSgCNB4Y9KYgCPDXs8UEU4LNhF6bzhW7DRmCEiFTqjx+VUtc8LECrYfd3RQA9lxq03SgiZR4WIGbYv9yaAt8Me5DHp8DAFP3ukQDvDTvu8WxrnGG/dUUApdQrEUkYU2a8hwWYnGJH6PEu0GzYczwaCSoRqTIeNbkpwFXDnu/Rf79URJKnyz9E5Kab6i4wYuxvQKEHR8BO8+DE7R+PAu8MB6s9Rj4GvDH6tzYTTvZaVZeIhwSoMfr2HRiUCSd21aXGI+T7WRcu6jLp7Ijh6AMw1AMCmKXxtozeLgOKgC+Gw4ZcFiCB2fpWSRLbsuF0nXUYsSlH5EfqE+IknmblSo0+1m6wRKjOMvnB+jzQHPpl2ezAEOCx0YEOYH2WfMct8rlZkIES4LXVkXqgTwZ9Vjn43JrLFXgC8Mzq0H2g0mU/BTrSa7d8bfHCPlwE3HC41XWyp9fbdIS3Tt8fxprzq7wUivYGdmnithCXgRXpxgw67K4A9ljhdxKPgVK3+q5cFqJcRA6JyAyn1zpFfaBz9RZdtYnpStNonc/PEpEhDt9vE5FaEdmtlPrh2YoEEAEWpbje9j9o1aNrhOQbgOlAncNC2R0SwCVgZaZTb5VFMUpEpEJEpojIGBEZLJ21+zbpLGe3iMgTEXkoIneUUj8lRIgQIUKECJFJ/AEepzU1TSID5QAAAABJRU5ErkJggg=="

    invoke-static {v1, v8}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    .line 3242
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 3243
    const/16 v3, 0x1a4

    iput v3, v2, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    .line 3244
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v0, v2, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    .line 3245
    array-length v0, v1

    invoke-static {v1, v8, v0, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 187
    if-eqz v0, :cond_2

    .line 189
    iget-object v1, p0, Lcom/my/target/ei;->cr:Lcom/my/target/ed;

    invoke-virtual {v1, v0}, Lcom/my/target/ed;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 192
    :cond_2
    invoke-virtual {v6}, Lcom/my/target/aj;->isAutoPlay()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 194
    iget-object v0, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    invoke-virtual {v0}, Lcom/my/target/eg;->I()V

    .line 195
    invoke-direct {p0}, Lcom/my/target/ei;->J()V

    goto/16 :goto_0

    .line 152
    :cond_3
    iget-object v0, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    invoke-virtual {v0, v4}, Lcom/my/target/ca;->setVisibility(I)V

    goto/16 :goto_1

    .line 157
    :cond_4
    iget-object v0, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    invoke-virtual {v0, v4}, Lcom/my/target/ca;->setVisibility(I)V

    .line 158
    iget-object v0, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 159
    iget-object v0, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getDomain()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 180
    :cond_5
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    iget v1, p0, Lcom/my/target/ei;->padding:I

    iget v2, p0, Lcom/my/target/ei;->padding:I

    iget v3, p0, Lcom/my/target/ei;->padding:I

    iget v4, p0, Lcom/my/target/ei;->padding:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 181
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 199
    :cond_6
    invoke-direct {p0}, Lcom/my/target/ei;->B()V

    goto/16 :goto_0
.end method

.method public final setClickArea(Lcom/my/target/af;)V
    .locals 3
    .param p1, "area"    # Lcom/my/target/af;

    .prologue
    const/4 v2, 0x0

    .line 628
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Apply click area "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/my/target/af;->O()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to view"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 630
    iget-boolean v0, p1, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_0

    .line 632
    iget-object v0, p0, Lcom/my/target/ei;->bx:Lcom/my/target/ee$a;

    invoke-virtual {p0, v0}, Lcom/my/target/ei;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 635
    :cond_0
    iget-boolean v0, p1, Lcom/my/target/af;->cy:Z

    if-nez v0, :cond_1

    iget-boolean v0, p1, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_7

    .line 637
    :cond_1
    iget-object v0, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    iget-object v1, p0, Lcom/my/target/ei;->bx:Lcom/my/target/ee$a;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 645
    :goto_0
    iget-boolean v0, p1, Lcom/my/target/af;->cs:Z

    if-nez v0, :cond_2

    iget-boolean v0, p1, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_8

    .line 647
    :cond_2
    iget-object v0, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/ei;->bx:Lcom/my/target/ee$a;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 653
    :goto_1
    iget-boolean v0, p1, Lcom/my/target/af;->cw:Z

    if-nez v0, :cond_3

    iget-boolean v0, p1, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_9

    .line 655
    :cond_3
    iget-object v0, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    iget-object v1, p0, Lcom/my/target/ei;->bx:Lcom/my/target/ee$a;

    invoke-virtual {v0, v1}, Lcom/my/target/ca;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 663
    :goto_2
    iget-boolean v0, p1, Lcom/my/target/af;->cB:Z

    if-nez v0, :cond_4

    iget-boolean v0, p1, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_a

    .line 665
    :cond_4
    iget-object v0, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/ei;->bx:Lcom/my/target/ee$a;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 672
    :goto_3
    iget-boolean v0, p1, Lcom/my/target/af;->cD:Z

    if-nez v0, :cond_5

    iget-boolean v0, p1, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_6

    .line 674
    :cond_5
    iget-object v0, p0, Lcom/my/target/ei;->bx:Lcom/my/target/ee$a;

    invoke-virtual {p0, v0}, Lcom/my/target/ei;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 676
    :cond_6
    return-void

    .line 641
    :cond_7
    iget-object v0, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 642
    iget-object v0, p0, Lcom/my/target/ei;->bL:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 651
    :cond_8
    iget-object v0, p0, Lcom/my/target/ei;->ch:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 659
    :cond_9
    iget-object v0, p0, Lcom/my/target/ei;->ci:Lcom/my/target/ca;

    invoke-virtual {v0, v2}, Lcom/my/target/ca;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 669
    :cond_a
    iget-object v0, p0, Lcom/my/target/ei;->cn:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3
.end method

.method public final setInterstitialPromoViewListener(Lcom/my/target/ee$b;)V
    .locals 1
    .param p1, "interstitialPromoViewListener"    # Lcom/my/target/ee$b;

    .prologue
    .line 614
    invoke-super {p0, p1}, Lcom/my/target/ee;->setInterstitialPromoViewListener(Lcom/my/target/ee$b;)V

    .line 615
    iget-object v0, p0, Lcom/my/target/ei;->bm:Lcom/my/target/eg;

    invoke-virtual {v0, p1}, Lcom/my/target/eg;->setVideoListener(Lcom/my/target/cc$a;)V

    .line 616
    return-void
.end method

.method protected final setLayoutOrientation(I)V
    .locals 0

    .prologue
    .line 700
    return-void
.end method

.method public final setTimeChanged(F)V
    .locals 5
    .param p1, "progress"    # F

    .prologue
    const/4 v4, 0x0

    .line 227
    iget-boolean v0, p0, Lcom/my/target/ei;->cC:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/my/target/ei;->allowClose:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/my/target/ei;->cx:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    iget v0, p0, Lcom/my/target/ei;->cx:F

    cmpl-float v0, v0, p1

    if-ltz v0, :cond_2

    .line 229
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/my/target/ei;->cB:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 236
    iget v0, p0, Lcom/my/target/ei;->cx:F

    sub-float/2addr v0, p1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v1, v0

    .line 237
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 238
    iget v2, p0, Lcom/my/target/ei;->cx:F

    const/high16 v3, 0x41100000    # 9.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 240
    const/16 v2, 0x9

    if-gt v1, v2, :cond_1

    .line 242
    const-string v1, "0"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 246
    :cond_1
    iget-object v1, p0, Lcom/my/target/ei;->cB:Ljava/lang/String;

    const-string v2, "%d"

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 247
    iget-object v1, p0, Lcom/my/target/ei;->cj:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    :cond_2
    iget-object v0, p0, Lcom/my/target/ei;->co:Lcom/my/target/dz;

    invoke-virtual {v0}, Lcom/my/target/dz;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_3

    .line 252
    iget-object v0, p0, Lcom/my/target/ei;->co:Lcom/my/target/dz;

    invoke-virtual {v0, v4}, Lcom/my/target/dz;->setVisibility(I)V

    .line 254
    :cond_3
    iget-object v0, p0, Lcom/my/target/ei;->co:Lcom/my/target/dz;

    iget v1, p0, Lcom/my/target/ei;->bA:F

    div-float v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/my/target/dz;->setProgress(F)V

    .line 255
    iget-object v0, p0, Lcom/my/target/ei;->co:Lcom/my/target/dz;

    iget v1, p0, Lcom/my/target/ei;->bA:F

    sub-float/2addr v1, p1

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/my/target/dz;->setDigit(I)V

    .line 256
    return-void
.end method
