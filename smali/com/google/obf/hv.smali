.class public Lcom/google/obf/hv;
.super Landroid/widget/LinearLayout;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/hv$a;
    }
.end annotation


# instance fields
.field private a:Lcom/google/obf/hy;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/obf/hv$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/obf/hy;)V
    .locals 2

    .prologue
    .line 1
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/obf/hv;-><init>(Landroid/content/Context;Lcom/google/obf/hy;Landroid/widget/TextView;Landroid/widget/TextView;)V

    .line 2
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/obf/hy;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 6

    .prologue
    const/16 v5, 0x10

    const/4 v4, 0x0

    const/16 v2, 0xa

    const/4 v3, -0x2

    .line 3
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/obf/hv;->d:Ljava/util/List;

    .line 5
    iput-object p2, p0, Lcom/google/obf/hv;->a:Lcom/google/obf/hy;

    .line 6
    iput-object p3, p0, Lcom/google/obf/hv;->b:Landroid/widget/TextView;

    .line 7
    iput-object p4, p0, Lcom/google/obf/hv;->c:Landroid/widget/TextView;

    .line 8
    iget v0, p2, Lcom/google/obf/hy;->i:I

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 9
    invoke-virtual {p3, v4}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 10
    invoke-virtual {p3, v5}, Landroid/widget/TextView;->setGravity(I)V

    .line 11
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 12
    invoke-virtual {p3}, Landroid/widget/TextView;->setSingleLine()V

    .line 13
    iget v0, p2, Lcom/google/obf/hy;->l:I

    .line 14
    invoke-virtual {p0}, Lcom/google/obf/hv;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 15
    invoke-static {v0, v1}, Lcom/google/obf/hx;->a(IF)I

    move-result v0

    .line 16
    invoke-virtual {p3, v0, v0, v0, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 17
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v3, v3, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 18
    invoke-virtual {p0, p3, v0}, Lcom/google/obf/hv;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 19
    iget-boolean v0, p2, Lcom/google/obf/hy;->m:Z

    if-eqz v0, :cond_0

    .line 20
    iget v0, p2, Lcom/google/obf/hy;->p:I

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 21
    iget v0, p2, Lcom/google/obf/hy;->q:F

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setTextSize(F)V

    .line 22
    iget-object v0, p2, Lcom/google/obf/hy;->o:Ljava/lang/String;

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 23
    invoke-virtual {p4, v4}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 24
    invoke-virtual {p4, v2, v2, v2, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 25
    invoke-virtual {p4, v5}, Landroid/widget/TextView;->setGravity(I)V

    .line 26
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 27
    invoke-virtual {p4}, Landroid/widget/TextView;->setSingleLine()V

    .line 28
    new-instance v0, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v1, Lcom/google/obf/hv$1;

    invoke-direct {v1, p0}, Lcom/google/obf/hv$1;-><init>(Lcom/google/obf/hv;)V

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 29
    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    iget v2, p2, Lcom/google/obf/hy;->f:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 30
    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    iget v2, p2, Lcom/google/obf/hy;->g:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 31
    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 32
    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 33
    new-instance v0, Lcom/google/obf/hv$2;

    invoke-direct {v0, p0}, Lcom/google/obf/hv$2;-><init>(Lcom/google/obf/hv;)V

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 34
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 35
    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 36
    invoke-virtual {p0, p4, v0}, Lcom/google/obf/hv;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 37
    :cond_0
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/obf/hv;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/hv$a;

    .line 45
    invoke-interface {v0}, Lcom/google/obf/hv$a;->c()V

    goto :goto_0

    .line 47
    :cond_0
    return-void
.end method

.method public a(Lcom/google/obf/hv$a;)V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/obf/hv;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/obf/hv;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/obf/hv;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 48
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    iget-object v2, p0, Lcom/google/obf/hv;->a:Lcom/google/obf/hy;

    iget-object v2, v2, Lcom/google/obf/hy;->c:[I

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    .line 49
    invoke-virtual {v0, v4, v4, p1, p2}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    .line 50
    new-instance v1, Lcom/google/obf/hv$3;

    invoke-direct {v1, p0}, Lcom/google/obf/hv$3;-><init>(Lcom/google/obf/hv;)V

    .line 51
    new-instance v2, Landroid/graphics/drawable/ShapeDrawable;

    invoke-direct {v2, v1}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 52
    invoke-virtual {v2}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    iget-object v3, p0, Lcom/google/obf/hv;->a:Lcom/google/obf/hy;

    iget v3, v3, Lcom/google/obf/hy;->d:I

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 53
    invoke-virtual {v2}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    iget-object v3, p0, Lcom/google/obf/hv;->a:Lcom/google/obf/hy;

    iget v3, v3, Lcom/google/obf/hy;->e:I

    int-to-float v3, v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 54
    invoke-virtual {v2}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 55
    invoke-virtual {v2, v4, v4, p1, p2}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    .line 56
    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    aput-object v0, v1, v4

    const/4 v0, 0x1

    aput-object v2, v1, v0

    .line 57
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v0, v1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 58
    invoke-virtual {p0, v0}, Lcom/google/obf/hv;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 59
    return-void
.end method
