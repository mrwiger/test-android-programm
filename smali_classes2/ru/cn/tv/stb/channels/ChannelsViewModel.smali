.class Lru/cn/tv/stb/channels/ChannelsViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "ChannelsViewModel.java"


# instance fields
.field private final channels:Lru/cn/domain/Channels;

.field private final channelsOut:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;


# direct methods
.method constructor <init>(Lru/cn/domain/Channels;)V
    .locals 2
    .param p1, "channels"    # Lru/cn/domain/Channels;

    .prologue
    .line 25
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 26
    iput-object p1, p0, Lru/cn/tv/stb/channels/ChannelsViewModel;->channels:Lru/cn/domain/Channels;

    .line 28
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/stb/channels/ChannelsViewModel;->channelsOut:Landroid/arch/lifecycle/MutableLiveData;

    .line 30
    iget-object v0, p0, Lru/cn/tv/stb/channels/ChannelsViewModel;->timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Lru/cn/domain/statistics/inetra/TimeMeasure;

    invoke-direct {v0}, Lru/cn/domain/statistics/inetra/TimeMeasure;-><init>()V

    iput-object v0, p0, Lru/cn/tv/stb/channels/ChannelsViewModel;->timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

    .line 32
    iget-object v0, p0, Lru/cn/tv/stb/channels/ChannelsViewModel;->timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

    const-string v1, "channels"

    invoke-virtual {v0, v1}, Lru/cn/domain/statistics/inetra/TimeMeasure;->measureStart(Ljava/lang/String;)V

    .line 34
    :cond_0
    return-void
.end method

.method private load(Ljava/lang/String;I)V
    .locals 3
    .param p1, "ageFilter"    # Ljava/lang/String;
    .param p2, "mode"    # I

    .prologue
    .line 47
    iget-object v0, p0, Lru/cn/tv/stb/channels/ChannelsViewModel;->channels:Lru/cn/domain/Channels;

    invoke-direct {p0, p2}, Lru/cn/tv/stb/channels/ChannelsViewModel;->modeSelection(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lru/cn/domain/Channels;->channels(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v0

    .line 48
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lru/cn/tv/stb/channels/ChannelsViewModel$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/channels/ChannelsViewModel$$Lambda$0;-><init>(Lru/cn/tv/stb/channels/ChannelsViewModel;)V

    new-instance v2, Lru/cn/tv/stb/channels/ChannelsViewModel$$Lambda$1;

    invoke-direct {v2, p0}, Lru/cn/tv/stb/channels/ChannelsViewModel$$Lambda$1;-><init>(Lru/cn/tv/stb/channels/ChannelsViewModel;)V

    .line 49
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 47
    invoke-virtual {p0, v0}, Lru/cn/tv/stb/channels/ChannelsViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 62
    return-void
.end method

.method private modeSelection(I)Ljava/lang/String;
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 65
    packed-switch p1, :pswitch_data_0

    .line 82
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 70
    :pswitch_1
    const-string v0, "favourite"

    goto :goto_0

    .line 73
    :pswitch_2
    const-string v0, "porno"

    goto :goto_0

    .line 76
    :pswitch_3
    const-string v0, "hd"

    goto :goto_0

    .line 79
    :pswitch_4
    const-string v0, "intersections"

    goto :goto_0

    .line 65
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public channels()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lru/cn/tv/stb/channels/ChannelsViewModel;->channelsOut:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method final synthetic lambda$load$0$ChannelsViewModel(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "it"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lru/cn/tv/stb/channels/ChannelsViewModel;->channelsOut:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v0, p1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 52
    iget-object v0, p0, Lru/cn/tv/stb/channels/ChannelsViewModel;->timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lru/cn/tv/stb/channels/ChannelsViewModel;->timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

    invoke-virtual {v0}, Lru/cn/domain/statistics/inetra/TimeMeasure;->measureEnd()V

    .line 54
    sget-object v0, Lru/cn/domain/statistics/inetra/TypeOfMeasure;->CHANNELS_LIST_LOAD_AND_DISPLAYING:Lru/cn/domain/statistics/inetra/TypeOfMeasure;

    iget-object v1, p0, Lru/cn/tv/stb/channels/ChannelsViewModel;->timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

    invoke-static {v0, v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->timeMeasure(Lru/cn/domain/statistics/inetra/TypeOfMeasure;Lru/cn/domain/statistics/inetra/TimeMeasure;)V

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/tv/stb/channels/ChannelsViewModel;->timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

    .line 59
    :cond_0
    return-void
.end method

.method final synthetic lambda$load$1$ChannelsViewModel(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "throwable"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lru/cn/tv/stb/channels/ChannelsViewModel;->channelsOut:Landroid/arch/lifecycle/MutableLiveData;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 61
    return-void
.end method

.method public setMode(Ljava/lang/String;I)V
    .locals 0
    .param p1, "ageFilter"    # Ljava/lang/String;
    .param p2, "mode"    # I

    .prologue
    .line 41
    invoke-virtual {p0}, Lru/cn/tv/stb/channels/ChannelsViewModel;->unbindAll()V

    .line 43
    invoke-direct {p0, p1, p2}, Lru/cn/tv/stb/channels/ChannelsViewModel;->load(Ljava/lang/String;I)V

    .line 44
    return-void
.end method
