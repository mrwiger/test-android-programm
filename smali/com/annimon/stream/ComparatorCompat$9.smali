.class Lcom/annimon/stream/ComparatorCompat$9;
.super Ljava/lang/Object;
.source "ComparatorCompat.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/annimon/stream/ComparatorCompat;->thenComparing(Ljava/util/Comparator;)Lcom/annimon/stream/ComparatorCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/annimon/stream/ComparatorCompat;

.field final synthetic val$other:Ljava/util/Comparator;


# direct methods
.method constructor <init>(Lcom/annimon/stream/ComparatorCompat;Ljava/util/Comparator;)V
    .locals 0
    .param p1, "this$0"    # Lcom/annimon/stream/ComparatorCompat;

    .prologue
    .line 315
    .local p0, "this":Lcom/annimon/stream/ComparatorCompat$9;, "Lcom/annimon/stream/ComparatorCompat$9;"
    iput-object p1, p0, Lcom/annimon/stream/ComparatorCompat$9;->this$0:Lcom/annimon/stream/ComparatorCompat;

    iput-object p2, p0, Lcom/annimon/stream/ComparatorCompat$9;->val$other:Ljava/util/Comparator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)I"
        }
    .end annotation

    .prologue
    .line 319
    .local p0, "this":Lcom/annimon/stream/ComparatorCompat$9;, "Lcom/annimon/stream/ComparatorCompat$9;"
    .local p1, "t1":Ljava/lang/Object;, "TT;"
    .local p2, "t2":Ljava/lang/Object;, "TT;"
    iget-object v1, p0, Lcom/annimon/stream/ComparatorCompat$9;->this$0:Lcom/annimon/stream/ComparatorCompat;

    invoke-static {v1}, Lcom/annimon/stream/ComparatorCompat;->access$000(Lcom/annimon/stream/ComparatorCompat;)Ljava/util/Comparator;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 320
    .local v0, "result":I
    if-eqz v0, :cond_0

    .end local v0    # "result":I
    :goto_0
    return v0

    .restart local v0    # "result":I
    :cond_0
    iget-object v1, p0, Lcom/annimon/stream/ComparatorCompat$9;->val$other:Ljava/util/Comparator;

    invoke-interface {v1, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method
