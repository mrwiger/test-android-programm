.class public final Lcom/google/obf/fs;
.super Lcom/google/obf/ge;
.source "IMASDK"


# static fields
.field private static final b:Ljava/io/Reader;

.field private static final c:Ljava/lang/Object;


# instance fields
.field private d:[Ljava/lang/Object;

.field private e:I

.field private f:[Ljava/lang/String;

.field private g:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 165
    new-instance v0, Lcom/google/obf/fs$1;

    invoke-direct {v0}, Lcom/google/obf/fs$1;-><init>()V

    sput-object v0, Lcom/google/obf/fs;->b:Ljava/io/Reader;

    .line 166
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/obf/fs;->c:Ljava/lang/Object;

    return-void
.end method

.method private a(Lcom/google/obf/gf;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/google/obf/fs;->f()Lcom/google/obf/gf;

    move-result-object v0

    if-eq v0, p1, :cond_0

    .line 66
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 67
    invoke-virtual {p0}, Lcom/google/obf/fs;->f()Lcom/google/obf/gf;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/obf/fs;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 140
    iget v0, p0, Lcom/google/obf/fs;->e:I

    iget-object v1, p0, Lcom/google/obf/fs;->d:[Ljava/lang/Object;

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 141
    iget v0, p0, Lcom/google/obf/fs;->e:I

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 142
    iget v1, p0, Lcom/google/obf/fs;->e:I

    mul-int/lit8 v1, v1, 0x2

    new-array v1, v1, [I

    .line 143
    iget v2, p0, Lcom/google/obf/fs;->e:I

    mul-int/lit8 v2, v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    .line 144
    iget-object v3, p0, Lcom/google/obf/fs;->d:[Ljava/lang/Object;

    iget v4, p0, Lcom/google/obf/fs;->e:I

    invoke-static {v3, v5, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 145
    iget-object v3, p0, Lcom/google/obf/fs;->g:[I

    iget v4, p0, Lcom/google/obf/fs;->e:I

    invoke-static {v3, v5, v1, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 146
    iget-object v3, p0, Lcom/google/obf/fs;->f:[Ljava/lang/String;

    iget v4, p0, Lcom/google/obf/fs;->e:I

    invoke-static {v3, v5, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 147
    iput-object v0, p0, Lcom/google/obf/fs;->d:[Ljava/lang/Object;

    .line 148
    iput-object v1, p0, Lcom/google/obf/fs;->g:[I

    .line 149
    iput-object v2, p0, Lcom/google/obf/fs;->f:[Ljava/lang/String;

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/google/obf/fs;->d:[Ljava/lang/Object;

    iget v1, p0, Lcom/google/obf/fs;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/obf/fs;->e:I

    aput-object p1, v0, v1

    .line 151
    return-void
.end method

.method private s()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/obf/fs;->d:[Ljava/lang/Object;

    iget v1, p0, Lcom/google/obf/fs;->e:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method private t()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/obf/fs;->d:[Ljava/lang/Object;

    iget v1, p0, Lcom/google/obf/fs;->e:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/obf/fs;->e:I

    aget-object v0, v0, v1

    .line 63
    iget-object v1, p0, Lcom/google/obf/fs;->d:[Ljava/lang/Object;

    iget v2, p0, Lcom/google/obf/fs;->e:I

    const/4 v3, 0x0

    aput-object v3, v1, v2

    .line 64
    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2

    .prologue
    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " at path "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/obf/fs;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    sget-object v0, Lcom/google/obf/gf;->a:Lcom/google/obf/gf;

    invoke-direct {p0, v0}, Lcom/google/obf/fs;->a(Lcom/google/obf/gf;)V

    .line 9
    invoke-direct {p0}, Lcom/google/obf/fs;->s()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/ej;

    .line 10
    invoke-virtual {v0}, Lcom/google/obf/ej;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/obf/fs;->a(Ljava/lang/Object;)V

    .line 11
    iget-object v0, p0, Lcom/google/obf/fs;->g:[I

    iget v1, p0, Lcom/google/obf/fs;->e:I

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    aput v2, v0, v1

    .line 12
    return-void
.end method

.method public b()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13
    sget-object v0, Lcom/google/obf/gf;->b:Lcom/google/obf/gf;

    invoke-direct {p0, v0}, Lcom/google/obf/fs;->a(Lcom/google/obf/gf;)V

    .line 14
    invoke-direct {p0}, Lcom/google/obf/fs;->t()Ljava/lang/Object;

    .line 15
    invoke-direct {p0}, Lcom/google/obf/fs;->t()Ljava/lang/Object;

    .line 16
    iget v0, p0, Lcom/google/obf/fs;->e:I

    if-lez v0, :cond_0

    .line 17
    iget-object v0, p0, Lcom/google/obf/fs;->g:[I

    iget v1, p0, Lcom/google/obf/fs;->e:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 18
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    sget-object v0, Lcom/google/obf/gf;->c:Lcom/google/obf/gf;

    invoke-direct {p0, v0}, Lcom/google/obf/fs;->a(Lcom/google/obf/gf;)V

    .line 20
    invoke-direct {p0}, Lcom/google/obf/fs;->s()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/ep;

    .line 21
    invoke-virtual {v0}, Lcom/google/obf/ep;->o()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/obf/fs;->a(Ljava/lang/Object;)V

    .line 22
    return-void
.end method

.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 123
    new-array v0, v3, [Ljava/lang/Object;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/obf/fs;->c:Ljava/lang/Object;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/obf/fs;->d:[Ljava/lang/Object;

    .line 124
    iput v3, p0, Lcom/google/obf/fs;->e:I

    .line 125
    return-void
.end method

.method public d()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23
    sget-object v0, Lcom/google/obf/gf;->d:Lcom/google/obf/gf;

    invoke-direct {p0, v0}, Lcom/google/obf/fs;->a(Lcom/google/obf/gf;)V

    .line 24
    invoke-direct {p0}, Lcom/google/obf/fs;->t()Ljava/lang/Object;

    .line 25
    invoke-direct {p0}, Lcom/google/obf/fs;->t()Ljava/lang/Object;

    .line 26
    iget v0, p0, Lcom/google/obf/fs;->e:I

    if-lez v0, :cond_0

    .line 27
    iget-object v0, p0, Lcom/google/obf/fs;->g:[I

    iget v1, p0, Lcom/google/obf/fs;->e:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 28
    :cond_0
    return-void
.end method

.method public e()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/obf/fs;->f()Lcom/google/obf/gf;

    move-result-object v0

    .line 30
    sget-object v1, Lcom/google/obf/gf;->d:Lcom/google/obf/gf;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/obf/gf;->b:Lcom/google/obf/gf;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/google/obf/gf;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    iget v0, p0, Lcom/google/obf/fs;->e:I

    if-nez v0, :cond_0

    .line 32
    sget-object v0, Lcom/google/obf/gf;->j:Lcom/google/obf/gf;

    .line 57
    :goto_0
    return-object v0

    .line 33
    :cond_0
    invoke-direct {p0}, Lcom/google/obf/fs;->s()Ljava/lang/Object;

    move-result-object v0

    .line 34
    instance-of v1, v0, Ljava/util/Iterator;

    if-eqz v1, :cond_4

    .line 35
    iget-object v1, p0, Lcom/google/obf/fs;->d:[Ljava/lang/Object;

    iget v2, p0, Lcom/google/obf/fs;->e:I

    add-int/lit8 v2, v2, -0x2

    aget-object v1, v1, v2

    instance-of v1, v1, Lcom/google/obf/ep;

    .line 36
    check-cast v0, Ljava/util/Iterator;

    .line 37
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 38
    if-eqz v1, :cond_1

    .line 39
    sget-object v0, Lcom/google/obf/gf;->e:Lcom/google/obf/gf;

    goto :goto_0

    .line 40
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/obf/fs;->a(Ljava/lang/Object;)V

    .line 41
    invoke-virtual {p0}, Lcom/google/obf/fs;->f()Lcom/google/obf/gf;

    move-result-object v0

    goto :goto_0

    .line 42
    :cond_2
    if-eqz v1, :cond_3

    sget-object v0, Lcom/google/obf/gf;->d:Lcom/google/obf/gf;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/obf/gf;->b:Lcom/google/obf/gf;

    goto :goto_0

    .line 43
    :cond_4
    instance-of v1, v0, Lcom/google/obf/ep;

    if-eqz v1, :cond_5

    .line 44
    sget-object v0, Lcom/google/obf/gf;->c:Lcom/google/obf/gf;

    goto :goto_0

    .line 45
    :cond_5
    instance-of v1, v0, Lcom/google/obf/ej;

    if-eqz v1, :cond_6

    .line 46
    sget-object v0, Lcom/google/obf/gf;->a:Lcom/google/obf/gf;

    goto :goto_0

    .line 47
    :cond_6
    instance-of v1, v0, Lcom/google/obf/er;

    if-eqz v1, :cond_a

    .line 48
    check-cast v0, Lcom/google/obf/er;

    .line 49
    invoke-virtual {v0}, Lcom/google/obf/er;->q()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 50
    sget-object v0, Lcom/google/obf/gf;->f:Lcom/google/obf/gf;

    goto :goto_0

    .line 51
    :cond_7
    invoke-virtual {v0}, Lcom/google/obf/er;->o()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 52
    sget-object v0, Lcom/google/obf/gf;->h:Lcom/google/obf/gf;

    goto :goto_0

    .line 53
    :cond_8
    invoke-virtual {v0}, Lcom/google/obf/er;->p()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 54
    sget-object v0, Lcom/google/obf/gf;->g:Lcom/google/obf/gf;

    goto :goto_0

    .line 55
    :cond_9
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 56
    :cond_a
    instance-of v1, v0, Lcom/google/obf/eo;

    if-eqz v1, :cond_b

    .line 57
    sget-object v0, Lcom/google/obf/gf;->i:Lcom/google/obf/gf;

    goto :goto_0

    .line 58
    :cond_b
    sget-object v1, Lcom/google/obf/fs;->c:Ljava/lang/Object;

    if-ne v0, v1, :cond_c

    .line 59
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "JsonReader is closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_c
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public g()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    sget-object v0, Lcom/google/obf/gf;->e:Lcom/google/obf/gf;

    invoke-direct {p0, v0}, Lcom/google/obf/fs;->a(Lcom/google/obf/gf;)V

    .line 70
    invoke-direct {p0}, Lcom/google/obf/fs;->s()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    .line 71
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 72
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 73
    iget-object v2, p0, Lcom/google/obf/fs;->f:[Ljava/lang/String;

    iget v3, p0, Lcom/google/obf/fs;->e:I

    add-int/lit8 v3, v3, -0x1

    aput-object v1, v2, v3

    .line 74
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/obf/fs;->a(Ljava/lang/Object;)V

    .line 75
    return-object v1
.end method

.method public h()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/google/obf/fs;->f()Lcom/google/obf/gf;

    move-result-object v0

    .line 77
    sget-object v1, Lcom/google/obf/gf;->f:Lcom/google/obf/gf;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/obf/gf;->g:Lcom/google/obf/gf;

    if-eq v0, v1, :cond_0

    .line 78
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/google/obf/gf;->f:Lcom/google/obf/gf;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 79
    invoke-direct {p0}, Lcom/google/obf/fs;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 80
    :cond_0
    invoke-direct {p0}, Lcom/google/obf/fs;->t()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/er;

    invoke-virtual {v0}, Lcom/google/obf/er;->b()Ljava/lang/String;

    move-result-object v0

    .line 81
    iget v1, p0, Lcom/google/obf/fs;->e:I

    if-lez v1, :cond_1

    .line 82
    iget-object v1, p0, Lcom/google/obf/fs;->g:[I

    iget v2, p0, Lcom/google/obf/fs;->e:I

    add-int/lit8 v2, v2, -0x1

    aget v3, v1, v2

    add-int/lit8 v3, v3, 0x1

    aput v3, v1, v2

    .line 83
    :cond_1
    return-object v0
.end method

.method public i()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    sget-object v0, Lcom/google/obf/gf;->h:Lcom/google/obf/gf;

    invoke-direct {p0, v0}, Lcom/google/obf/fs;->a(Lcom/google/obf/gf;)V

    .line 85
    invoke-direct {p0}, Lcom/google/obf/fs;->t()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/er;

    invoke-virtual {v0}, Lcom/google/obf/er;->f()Z

    move-result v0

    .line 86
    iget v1, p0, Lcom/google/obf/fs;->e:I

    if-lez v1, :cond_0

    .line 87
    iget-object v1, p0, Lcom/google/obf/fs;->g:[I

    iget v2, p0, Lcom/google/obf/fs;->e:I

    add-int/lit8 v2, v2, -0x1

    aget v3, v1, v2

    add-int/lit8 v3, v3, 0x1

    aput v3, v1, v2

    .line 88
    :cond_0
    return v0
.end method

.method public j()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    sget-object v0, Lcom/google/obf/gf;->i:Lcom/google/obf/gf;

    invoke-direct {p0, v0}, Lcom/google/obf/fs;->a(Lcom/google/obf/gf;)V

    .line 90
    invoke-direct {p0}, Lcom/google/obf/fs;->t()Ljava/lang/Object;

    .line 91
    iget v0, p0, Lcom/google/obf/fs;->e:I

    if-lez v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/obf/fs;->g:[I

    iget v1, p0, Lcom/google/obf/fs;->e:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 93
    :cond_0
    return-void
.end method

.method public k()D
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/google/obf/fs;->f()Lcom/google/obf/gf;

    move-result-object v0

    .line 95
    sget-object v1, Lcom/google/obf/gf;->g:Lcom/google/obf/gf;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/obf/gf;->f:Lcom/google/obf/gf;

    if-eq v0, v1, :cond_0

    .line 96
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/google/obf/gf;->g:Lcom/google/obf/gf;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 97
    invoke-direct {p0}, Lcom/google/obf/fs;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 98
    :cond_0
    invoke-direct {p0}, Lcom/google/obf/fs;->s()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/er;

    invoke-virtual {v0}, Lcom/google/obf/er;->c()D

    move-result-wide v0

    .line 99
    invoke-virtual {p0}, Lcom/google/obf/fs;->q()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0, v1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 100
    :cond_1
    new-instance v2, Ljava/lang/NumberFormatException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "JSON forbids NaN and infinities: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 101
    :cond_2
    invoke-direct {p0}, Lcom/google/obf/fs;->t()Ljava/lang/Object;

    .line 102
    iget v2, p0, Lcom/google/obf/fs;->e:I

    if-lez v2, :cond_3

    .line 103
    iget-object v2, p0, Lcom/google/obf/fs;->g:[I

    iget v3, p0, Lcom/google/obf/fs;->e:I

    add-int/lit8 v3, v3, -0x1

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    .line 104
    :cond_3
    return-wide v0
.end method

.method public l()J
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/google/obf/fs;->f()Lcom/google/obf/gf;

    move-result-object v0

    .line 106
    sget-object v1, Lcom/google/obf/gf;->g:Lcom/google/obf/gf;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/obf/gf;->f:Lcom/google/obf/gf;

    if-eq v0, v1, :cond_0

    .line 107
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/google/obf/gf;->g:Lcom/google/obf/gf;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 108
    invoke-direct {p0}, Lcom/google/obf/fs;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 109
    :cond_0
    invoke-direct {p0}, Lcom/google/obf/fs;->s()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/er;

    invoke-virtual {v0}, Lcom/google/obf/er;->d()J

    move-result-wide v0

    .line 110
    invoke-direct {p0}, Lcom/google/obf/fs;->t()Ljava/lang/Object;

    .line 111
    iget v2, p0, Lcom/google/obf/fs;->e:I

    if-lez v2, :cond_1

    .line 112
    iget-object v2, p0, Lcom/google/obf/fs;->g:[I

    iget v3, p0, Lcom/google/obf/fs;->e:I

    add-int/lit8 v3, v3, -0x1

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    .line 113
    :cond_1
    return-wide v0
.end method

.method public m()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/google/obf/fs;->f()Lcom/google/obf/gf;

    move-result-object v0

    .line 115
    sget-object v1, Lcom/google/obf/gf;->g:Lcom/google/obf/gf;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/obf/gf;->f:Lcom/google/obf/gf;

    if-eq v0, v1, :cond_0

    .line 116
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/google/obf/gf;->g:Lcom/google/obf/gf;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 117
    invoke-direct {p0}, Lcom/google/obf/fs;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 118
    :cond_0
    invoke-direct {p0}, Lcom/google/obf/fs;->s()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/er;

    invoke-virtual {v0}, Lcom/google/obf/er;->e()I

    move-result v0

    .line 119
    invoke-direct {p0}, Lcom/google/obf/fs;->t()Ljava/lang/Object;

    .line 120
    iget v1, p0, Lcom/google/obf/fs;->e:I

    if-lez v1, :cond_1

    .line 121
    iget-object v1, p0, Lcom/google/obf/fs;->g:[I

    iget v2, p0, Lcom/google/obf/fs;->e:I

    add-int/lit8 v2, v2, -0x1

    aget v3, v1, v2

    add-int/lit8 v3, v3, 0x1

    aput v3, v1, v2

    .line 122
    :cond_1
    return v0
.end method

.method public n()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/google/obf/fs;->f()Lcom/google/obf/gf;

    move-result-object v0

    sget-object v1, Lcom/google/obf/gf;->e:Lcom/google/obf/gf;

    if-ne v0, v1, :cond_0

    .line 127
    invoke-virtual {p0}, Lcom/google/obf/fs;->g()Ljava/lang/String;

    .line 128
    iget-object v0, p0, Lcom/google/obf/fs;->f:[Ljava/lang/String;

    iget v1, p0, Lcom/google/obf/fs;->e:I

    add-int/lit8 v1, v1, -0x2

    const-string v2, "null"

    aput-object v2, v0, v1

    .line 131
    :goto_0
    iget-object v0, p0, Lcom/google/obf/fs;->g:[I

    iget v1, p0, Lcom/google/obf/fs;->e:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 132
    return-void

    .line 129
    :cond_0
    invoke-direct {p0}, Lcom/google/obf/fs;->t()Ljava/lang/Object;

    .line 130
    iget-object v0, p0, Lcom/google/obf/fs;->f:[Ljava/lang/String;

    iget v1, p0, Lcom/google/obf/fs;->e:I

    add-int/lit8 v1, v1, -0x1

    const-string v2, "null"

    aput-object v2, v0, v1

    goto :goto_0
.end method

.method public o()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 134
    sget-object v0, Lcom/google/obf/gf;->e:Lcom/google/obf/gf;

    invoke-direct {p0, v0}, Lcom/google/obf/fs;->a(Lcom/google/obf/gf;)V

    .line 135
    invoke-direct {p0}, Lcom/google/obf/fs;->s()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    .line 136
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 137
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/obf/fs;->a(Ljava/lang/Object;)V

    .line 138
    new-instance v1, Lcom/google/obf/er;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, Lcom/google/obf/er;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/google/obf/fs;->a(Ljava/lang/Object;)V

    .line 139
    return-void
.end method

.method public p()Ljava/lang/String;
    .locals 4

    .prologue
    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 153
    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/google/obf/fs;->e:I

    if-ge v0, v2, :cond_2

    .line 154
    iget-object v2, p0, Lcom/google/obf/fs;->d:[Ljava/lang/Object;

    aget-object v2, v2, v0

    instance-of v2, v2, Lcom/google/obf/ej;

    if-eqz v2, :cond_1

    .line 155
    iget-object v2, p0, Lcom/google/obf/fs;->d:[Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    aget-object v2, v2, v0

    instance-of v2, v2, Ljava/util/Iterator;

    if-eqz v2, :cond_0

    .line 156
    const/16 v2, 0x5b

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/obf/fs;->g:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x5d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 162
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 157
    :cond_1
    iget-object v2, p0, Lcom/google/obf/fs;->d:[Ljava/lang/Object;

    aget-object v2, v2, v0

    instance-of v2, v2, Lcom/google/obf/ep;

    if-eqz v2, :cond_0

    .line 158
    iget-object v2, p0, Lcom/google/obf/fs;->d:[Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    aget-object v2, v2, v0

    instance-of v2, v2, Ljava/util/Iterator;

    if-eqz v2, :cond_0

    .line 159
    const/16 v2, 0x2e

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 160
    iget-object v2, p0, Lcom/google/obf/fs;->f:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 161
    iget-object v2, p0, Lcom/google/obf/fs;->f:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 163
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
