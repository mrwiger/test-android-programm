.class public final Lru/cn/utils/http/HttpClient$Builder;
.super Ljava/lang/Object;
.source "HttpClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/utils/http/HttpClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private authenticator:Lokhttp3/Authenticator;

.field private connectionTimeoutMillis:J

.field private interceptors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lokhttp3/Interceptor;",
            ">;"
        }
    .end annotation
.end field

.field private networkInterceptors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lokhttp3/Interceptor;",
            ">;"
        }
    .end annotation
.end field

.field private socketTimeoutMillis:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 249
    const-wide/16 v0, 0x2710

    iput-wide v0, p0, Lru/cn/utils/http/HttpClient$Builder;->connectionTimeoutMillis:J

    .line 250
    const-wide/16 v0, 0x4e20

    iput-wide v0, p0, Lru/cn/utils/http/HttpClient$Builder;->socketTimeoutMillis:J

    .line 257
    return-void
.end method


# virtual methods
.method public addInterceptor(Lokhttp3/Interceptor;)Lru/cn/utils/http/HttpClient$Builder;
    .locals 1
    .param p1, "interceptor"    # Lokhttp3/Interceptor;

    .prologue
    .line 275
    iget-object v0, p0, Lru/cn/utils/http/HttpClient$Builder;->interceptors:Ljava/util/List;

    if-nez v0, :cond_0

    .line 276
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/utils/http/HttpClient$Builder;->interceptors:Ljava/util/List;

    .line 279
    :cond_0
    iget-object v0, p0, Lru/cn/utils/http/HttpClient$Builder;->interceptors:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 281
    return-object p0
.end method

.method public addNetworkInterceptor(Lokhttp3/Interceptor;)Lru/cn/utils/http/HttpClient$Builder;
    .locals 1
    .param p1, "interceptor"    # Lokhttp3/Interceptor;

    .prologue
    .line 285
    iget-object v0, p0, Lru/cn/utils/http/HttpClient$Builder;->networkInterceptors:Ljava/util/List;

    if-nez v0, :cond_0

    .line 286
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/utils/http/HttpClient$Builder;->networkInterceptors:Ljava/util/List;

    .line 289
    :cond_0
    iget-object v0, p0, Lru/cn/utils/http/HttpClient$Builder;->networkInterceptors:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 291
    return-object p0
.end method

.method public build()Lru/cn/utils/http/HttpClient;
    .locals 6

    .prologue
    .line 295
    invoke-static {}, Lru/cn/utils/http/HttpClient;->access$000()Lokhttp3/OkHttpClient;

    move-result-object v2

    invoke-virtual {v2}, Lokhttp3/OkHttpClient;->newBuilder()Lokhttp3/OkHttpClient$Builder;

    move-result-object v2

    iget-wide v4, p0, Lru/cn/utils/http/HttpClient$Builder;->connectionTimeoutMillis:J

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 296
    invoke-virtual {v2, v4, v5, v3}, Lokhttp3/OkHttpClient$Builder;->connectTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v2

    iget-wide v4, p0, Lru/cn/utils/http/HttpClient$Builder;->socketTimeoutMillis:J

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 297
    invoke-virtual {v2, v4, v5, v3}, Lokhttp3/OkHttpClient$Builder;->readTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v2

    iget-wide v4, p0, Lru/cn/utils/http/HttpClient$Builder;->socketTimeoutMillis:J

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 298
    invoke-virtual {v2, v4, v5, v3}, Lokhttp3/OkHttpClient$Builder;->writeTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 300
    .local v0, "builder":Lokhttp3/OkHttpClient$Builder;
    iget-object v2, p0, Lru/cn/utils/http/HttpClient$Builder;->authenticator:Lokhttp3/Authenticator;

    if-eqz v2, :cond_0

    .line 301
    iget-object v2, p0, Lru/cn/utils/http/HttpClient$Builder;->authenticator:Lokhttp3/Authenticator;

    invoke-virtual {v0, v2}, Lokhttp3/OkHttpClient$Builder;->authenticator(Lokhttp3/Authenticator;)Lokhttp3/OkHttpClient$Builder;

    .line 304
    :cond_0
    iget-object v2, p0, Lru/cn/utils/http/HttpClient$Builder;->interceptors:Ljava/util/List;

    if-eqz v2, :cond_1

    .line 305
    iget-object v2, p0, Lru/cn/utils/http/HttpClient$Builder;->interceptors:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lokhttp3/Interceptor;

    .line 306
    .local v1, "interceptor":Lokhttp3/Interceptor;
    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    goto :goto_0

    .line 310
    .end local v1    # "interceptor":Lokhttp3/Interceptor;
    :cond_1
    iget-object v2, p0, Lru/cn/utils/http/HttpClient$Builder;->networkInterceptors:Ljava/util/List;

    if-eqz v2, :cond_2

    .line 311
    iget-object v2, p0, Lru/cn/utils/http/HttpClient$Builder;->networkInterceptors:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lokhttp3/Interceptor;

    .line 312
    .restart local v1    # "interceptor":Lokhttp3/Interceptor;
    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->addNetworkInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    goto :goto_1

    .line 316
    .end local v1    # "interceptor":Lokhttp3/Interceptor;
    :cond_2
    new-instance v2, Lru/cn/utils/http/HttpClient;

    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lru/cn/utils/http/HttpClient;-><init>(Lokhttp3/OkHttpClient;Lru/cn/utils/http/HttpClient$1;)V

    return-object v2
.end method

.method public setAuthenticator(Lokhttp3/Authenticator;)Lru/cn/utils/http/HttpClient$Builder;
    .locals 0
    .param p1, "authenticator"    # Lokhttp3/Authenticator;

    .prologue
    .line 270
    iput-object p1, p0, Lru/cn/utils/http/HttpClient$Builder;->authenticator:Lokhttp3/Authenticator;

    .line 271
    return-object p0
.end method

.method public setConnectionTimeoutMillis(J)Lru/cn/utils/http/HttpClient$Builder;
    .locals 1
    .param p1, "timeoutMillis"    # J

    .prologue
    .line 260
    iput-wide p1, p0, Lru/cn/utils/http/HttpClient$Builder;->connectionTimeoutMillis:J

    .line 261
    return-object p0
.end method

.method public setSocketTimeoutMillis(J)Lru/cn/utils/http/HttpClient$Builder;
    .locals 1
    .param p1, "timeoutMillis"    # J

    .prologue
    .line 265
    iput-wide p1, p0, Lru/cn/utils/http/HttpClient$Builder;->socketTimeoutMillis:J

    .line 266
    return-object p0
.end method
