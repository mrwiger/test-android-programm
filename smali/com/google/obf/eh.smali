.class public final Lcom/google/obf/eh;
.super Ljava/lang/Object;
.source "IMASDK"


# instance fields
.field private a:Lcom/google/obf/fg;

.field private b:Lcom/google/obf/ev;

.field private c:Lcom/google/obf/ef;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/obf/ei",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/obf/ex;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/obf/ex;",
            ">;"
        }
    .end annotation
.end field

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:I

.field private j:I

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    sget-object v0, Lcom/google/obf/fg;->a:Lcom/google/obf/fg;

    iput-object v0, p0, Lcom/google/obf/eh;->a:Lcom/google/obf/fg;

    .line 3
    sget-object v0, Lcom/google/obf/ev;->a:Lcom/google/obf/ev;

    iput-object v0, p0, Lcom/google/obf/eh;->b:Lcom/google/obf/ev;

    .line 4
    sget-object v0, Lcom/google/obf/ee;->a:Lcom/google/obf/ee;

    iput-object v0, p0, Lcom/google/obf/eh;->c:Lcom/google/obf/ef;

    .line 5
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/obf/eh;->d:Ljava/util/Map;

    .line 6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/obf/eh;->e:Ljava/util/List;

    .line 7
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/obf/eh;->f:Ljava/util/List;

    .line 8
    iput-boolean v1, p0, Lcom/google/obf/eh;->g:Z

    .line 9
    iput v2, p0, Lcom/google/obf/eh;->i:I

    .line 10
    iput v2, p0, Lcom/google/obf/eh;->j:I

    .line 11
    iput-boolean v1, p0, Lcom/google/obf/eh;->k:Z

    .line 12
    iput-boolean v1, p0, Lcom/google/obf/eh;->l:Z

    .line 13
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/eh;->m:Z

    .line 14
    iput-boolean v1, p0, Lcom/google/obf/eh;->n:Z

    .line 15
    iput-boolean v1, p0, Lcom/google/obf/eh;->o:Z

    .line 16
    iput-boolean v1, p0, Lcom/google/obf/eh;->p:Z

    .line 17
    return-void
.end method

.method private a(Ljava/lang/String;IILjava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/List",
            "<",
            "Lcom/google/obf/ex;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 37
    if-eqz p1, :cond_1

    const-string v0, ""

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 38
    new-instance v0, Lcom/google/obf/eb;

    invoke-direct {v0, p1}, Lcom/google/obf/eb;-><init>(Ljava/lang/String;)V

    .line 42
    :goto_0
    const-class v1, Ljava/util/Date;

    invoke-static {v1}, Lcom/google/obf/gd;->b(Ljava/lang/Class;)Lcom/google/obf/gd;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/obf/fz;->a(Lcom/google/obf/gd;Ljava/lang/Object;)Lcom/google/obf/ex;

    move-result-object v1

    invoke-interface {p4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    const-class v1, Ljava/sql/Timestamp;

    invoke-static {v1}, Lcom/google/obf/gd;->b(Ljava/lang/Class;)Lcom/google/obf/gd;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/obf/fz;->a(Lcom/google/obf/gd;Ljava/lang/Object;)Lcom/google/obf/ex;

    move-result-object v1

    invoke-interface {p4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    const-class v1, Ljava/sql/Date;

    invoke-static {v1}, Lcom/google/obf/gd;->b(Ljava/lang/Class;)Lcom/google/obf/gd;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/obf/fz;->a(Lcom/google/obf/gd;Ljava/lang/Object;)Lcom/google/obf/ex;

    move-result-object v0

    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    :cond_0
    return-void

    .line 39
    :cond_1
    if-eq p2, v2, :cond_0

    if-eq p3, v2, :cond_0

    .line 40
    new-instance v0, Lcom/google/obf/eb;

    invoke-direct {v0, p2, p3}, Lcom/google/obf/eb;-><init>(II)V

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/google/obf/eg;
    .locals 13

    .prologue
    .line 31
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 32
    iget-object v0, p0, Lcom/google/obf/eh;->e:Ljava/util/List;

    invoke-interface {v12, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 33
    invoke-static {v12}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 34
    iget-object v0, p0, Lcom/google/obf/eh;->f:Ljava/util/List;

    invoke-interface {v12, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 35
    iget-object v0, p0, Lcom/google/obf/eh;->h:Ljava/lang/String;

    iget v1, p0, Lcom/google/obf/eh;->i:I

    iget v2, p0, Lcom/google/obf/eh;->j:I

    invoke-direct {p0, v0, v1, v2, v12}, Lcom/google/obf/eh;->a(Ljava/lang/String;IILjava/util/List;)V

    .line 36
    new-instance v0, Lcom/google/obf/eg;

    iget-object v1, p0, Lcom/google/obf/eh;->a:Lcom/google/obf/fg;

    iget-object v2, p0, Lcom/google/obf/eh;->c:Lcom/google/obf/ef;

    iget-object v3, p0, Lcom/google/obf/eh;->d:Ljava/util/Map;

    iget-boolean v4, p0, Lcom/google/obf/eh;->g:Z

    iget-boolean v5, p0, Lcom/google/obf/eh;->k:Z

    iget-boolean v6, p0, Lcom/google/obf/eh;->o:Z

    iget-boolean v7, p0, Lcom/google/obf/eh;->m:Z

    iget-boolean v8, p0, Lcom/google/obf/eh;->n:Z

    iget-boolean v9, p0, Lcom/google/obf/eh;->p:Z

    iget-boolean v10, p0, Lcom/google/obf/eh;->l:Z

    iget-object v11, p0, Lcom/google/obf/eh;->b:Lcom/google/obf/ev;

    invoke-direct/range {v0 .. v12}, Lcom/google/obf/eg;-><init>(Lcom/google/obf/fg;Lcom/google/obf/ef;Ljava/util/Map;ZZZZZZZLcom/google/obf/ev;Ljava/util/List;)V

    return-object v0
.end method

.method public a(Lcom/google/obf/ec;)Lcom/google/obf/eh;
    .locals 3

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/obf/eh;->a:Lcom/google/obf/fg;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/obf/fg;->a(Lcom/google/obf/ec;ZZ)Lcom/google/obf/fg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/eh;->a:Lcom/google/obf/fg;

    .line 19
    return-object p0
.end method

.method public a(Lcom/google/obf/ex;)Lcom/google/obf/eh;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/obf/eh;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30
    return-object p0
.end method

.method public a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/obf/eh;
    .locals 2

    .prologue
    .line 20
    instance-of v0, p2, Lcom/google/obf/et;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/obf/el;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/obf/ei;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/obf/ew;

    if-eqz v0, :cond_5

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/fd;->a(Z)V

    .line 21
    instance-of v0, p2, Lcom/google/obf/ei;

    if-eqz v0, :cond_1

    .line 22
    iget-object v1, p0, Lcom/google/obf/eh;->d:Ljava/util/Map;

    move-object v0, p2

    check-cast v0, Lcom/google/obf/ei;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    :cond_1
    instance-of v0, p2, Lcom/google/obf/et;

    if-nez v0, :cond_2

    instance-of v0, p2, Lcom/google/obf/el;

    if-eqz v0, :cond_3

    .line 24
    :cond_2
    invoke-static {p1}, Lcom/google/obf/gd;->a(Ljava/lang/reflect/Type;)Lcom/google/obf/gd;

    move-result-object v0

    .line 25
    iget-object v1, p0, Lcom/google/obf/eh;->e:Ljava/util/List;

    invoke-static {v0, p2}, Lcom/google/obf/fz;->b(Lcom/google/obf/gd;Ljava/lang/Object;)Lcom/google/obf/ex;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26
    :cond_3
    instance-of v0, p2, Lcom/google/obf/ew;

    if-eqz v0, :cond_4

    .line 27
    iget-object v0, p0, Lcom/google/obf/eh;->e:Ljava/util/List;

    invoke-static {p1}, Lcom/google/obf/gd;->a(Ljava/lang/reflect/Type;)Lcom/google/obf/gd;

    move-result-object v1

    check-cast p2, Lcom/google/obf/ew;

    invoke-static {v1, p2}, Lcom/google/obf/gb;->a(Lcom/google/obf/gd;Lcom/google/obf/ew;)Lcom/google/obf/ex;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    :cond_4
    return-object p0

    .line 20
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method
