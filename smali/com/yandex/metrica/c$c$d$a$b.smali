.class public final Lcom/yandex/metrica/c$c$d$a$b;
.super Lcom/yandex/metrica/impl/ob/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/c$c$d$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/c$c$d$a$b$a;
    }
.end annotation


# instance fields
.field public b:[Lcom/yandex/metrica/c$a;

.field public c:[Lcom/yandex/metrica/c$d;

.field public d:I

.field public e:Ljava/lang/String;

.field public f:Lcom/yandex/metrica/c$c$d$a$b$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 884
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/d;-><init>()V

    .line 885
    invoke-virtual {p0}, Lcom/yandex/metrica/c$c$d$a$b;->d()Lcom/yandex/metrica/c$c$d$a$b;

    .line 886
    return-void
.end method


# virtual methods
.method public a(Lcom/yandex/metrica/impl/ob/b;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    .line 901
    iget-object v0, p0, Lcom/yandex/metrica/c$c$d$a$b;->b:[Lcom/yandex/metrica/c$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/yandex/metrica/c$c$d$a$b;->b:[Lcom/yandex/metrica/c$a;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 902
    :goto_0
    iget-object v2, p0, Lcom/yandex/metrica/c$c$d$a$b;->b:[Lcom/yandex/metrica/c$a;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 903
    iget-object v2, p0, Lcom/yandex/metrica/c$c$d$a$b;->b:[Lcom/yandex/metrica/c$a;

    aget-object v2, v2, v0

    .line 904
    if-eqz v2, :cond_0

    .line 905
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/yandex/metrica/impl/ob/b;->a(ILcom/yandex/metrica/impl/ob/d;)V

    .line 902
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 909
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/c$c$d$a$b;->c:[Lcom/yandex/metrica/c$d;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/yandex/metrica/c$c$d$a$b;->c:[Lcom/yandex/metrica/c$d;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 910
    :goto_1
    iget-object v0, p0, Lcom/yandex/metrica/c$c$d$a$b;->c:[Lcom/yandex/metrica/c$d;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 911
    iget-object v0, p0, Lcom/yandex/metrica/c$c$d$a$b;->c:[Lcom/yandex/metrica/c$d;

    aget-object v0, v0, v1

    .line 912
    if-eqz v0, :cond_2

    .line 913
    invoke-virtual {p1, v4, v0}, Lcom/yandex/metrica/impl/ob/b;->a(ILcom/yandex/metrica/impl/ob/d;)V

    .line 910
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 917
    :cond_3
    iget v0, p0, Lcom/yandex/metrica/c$c$d$a$b;->d:I

    if-eq v0, v4, :cond_4

    .line 918
    const/4 v0, 0x3

    iget v1, p0, Lcom/yandex/metrica/c$c$d$a$b;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(II)V

    .line 920
    :cond_4
    iget-object v0, p0, Lcom/yandex/metrica/c$c$d$a$b;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 921
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/yandex/metrica/c$c$d$a$b;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(ILjava/lang/String;)V

    .line 923
    :cond_5
    iget-object v0, p0, Lcom/yandex/metrica/c$c$d$a$b;->f:Lcom/yandex/metrica/c$c$d$a$b$a;

    if-eqz v0, :cond_6

    .line 924
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/yandex/metrica/c$c$d$a$b;->f:Lcom/yandex/metrica/c$c$d$a$b$a;

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(ILcom/yandex/metrica/impl/ob/d;)V

    .line 926
    :cond_6
    invoke-super {p0, p1}, Lcom/yandex/metrica/impl/ob/d;->a(Lcom/yandex/metrica/impl/ob/b;)V

    .line 927
    return-void
.end method

.method protected c()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x0

    .line 931
    invoke-super {p0}, Lcom/yandex/metrica/impl/ob/d;->c()I

    move-result v0

    .line 932
    iget-object v2, p0, Lcom/yandex/metrica/c$c$d$a$b;->b:[Lcom/yandex/metrica/c$a;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/yandex/metrica/c$c$d$a$b;->b:[Lcom/yandex/metrica/c$a;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 933
    :goto_0
    iget-object v3, p0, Lcom/yandex/metrica/c$c$d$a$b;->b:[Lcom/yandex/metrica/c$a;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 934
    iget-object v3, p0, Lcom/yandex/metrica/c$c$d$a$b;->b:[Lcom/yandex/metrica/c$a;

    aget-object v3, v3, v0

    .line 935
    if-eqz v3, :cond_0

    .line 936
    const/4 v4, 0x1

    .line 937
    invoke-static {v4, v3}, Lcom/yandex/metrica/impl/ob/b;->b(ILcom/yandex/metrica/impl/ob/d;)I

    move-result v3

    add-int/2addr v2, v3

    .line 933
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 941
    :cond_2
    iget-object v2, p0, Lcom/yandex/metrica/c$c$d$a$b;->c:[Lcom/yandex/metrica/c$d;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/yandex/metrica/c$c$d$a$b;->c:[Lcom/yandex/metrica/c$d;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 942
    :goto_1
    iget-object v2, p0, Lcom/yandex/metrica/c$c$d$a$b;->c:[Lcom/yandex/metrica/c$d;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 943
    iget-object v2, p0, Lcom/yandex/metrica/c$c$d$a$b;->c:[Lcom/yandex/metrica/c$d;

    aget-object v2, v2, v1

    .line 944
    if-eqz v2, :cond_3

    .line 946
    invoke-static {v5, v2}, Lcom/yandex/metrica/impl/ob/b;->b(ILcom/yandex/metrica/impl/ob/d;)I

    move-result v2

    add-int/2addr v0, v2

    .line 942
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 950
    :cond_4
    iget v1, p0, Lcom/yandex/metrica/c$c$d$a$b;->d:I

    if-eq v1, v5, :cond_5

    .line 951
    const/4 v1, 0x3

    iget v2, p0, Lcom/yandex/metrica/c$c$d$a$b;->d:I

    .line 952
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 954
    :cond_5
    iget-object v1, p0, Lcom/yandex/metrica/c$c$d$a$b;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 955
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/yandex/metrica/c$c$d$a$b;->e:Ljava/lang/String;

    .line 956
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 958
    :cond_6
    iget-object v1, p0, Lcom/yandex/metrica/c$c$d$a$b;->f:Lcom/yandex/metrica/c$c$d$a$b$a;

    if-eqz v1, :cond_7

    .line 959
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/yandex/metrica/c$c$d$a$b;->f:Lcom/yandex/metrica/c$c$d$a$b$a;

    .line 960
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->b(ILcom/yandex/metrica/impl/ob/d;)I

    move-result v1

    add-int/2addr v0, v1

    .line 962
    :cond_7
    return v0
.end method

.method public d()Lcom/yandex/metrica/c$c$d$a$b;
    .locals 1

    .prologue
    .line 889
    invoke-static {}, Lcom/yandex/metrica/c$a;->d()[Lcom/yandex/metrica/c$a;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/c$c$d$a$b;->b:[Lcom/yandex/metrica/c$a;

    .line 890
    invoke-static {}, Lcom/yandex/metrica/c$d;->d()[Lcom/yandex/metrica/c$d;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/c$c$d$a$b;->c:[Lcom/yandex/metrica/c$d;

    .line 891
    const/4 v0, 0x2

    iput v0, p0, Lcom/yandex/metrica/c$c$d$a$b;->d:I

    .line 892
    const-string v0, ""

    iput-object v0, p0, Lcom/yandex/metrica/c$c$d$a$b;->e:Ljava/lang/String;

    .line 893
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/yandex/metrica/c$c$d$a$b;->f:Lcom/yandex/metrica/c$c$d$a$b$a;

    .line 894
    const/4 v0, -0x1

    iput v0, p0, Lcom/yandex/metrica/c$c$d$a$b;->a:I

    .line 895
    return-object p0
.end method
