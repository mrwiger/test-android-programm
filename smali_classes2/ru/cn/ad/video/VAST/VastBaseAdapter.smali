.class public abstract Lru/cn/ad/video/VAST/VastBaseAdapter;
.super Lru/cn/ad/video/VideoAdAdapter;
.source "VastBaseAdapter.java"

# interfaces
.implements Lru/cn/ad/video/ui/VastPresenter$PresenterListener;
.implements Lru/cn/ad/video/ui/VastPresenter$Tracker;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;
    }
.end annotation


# static fields
.field protected static defaultUserAgent:Ljava/lang/String;


# instance fields
.field protected final LOG_TAG:Ljava/lang/String;

.field protected parsers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/ad/video/VAST/parser/VastParser;",
            ">;"
        }
    .end annotation
.end field

.field private presenter:Lru/cn/ad/video/ui/VastPresenter;

.field private tracker:Lru/cn/ad/video/ui/VastPresenter$Tracker;

.field public videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    sput-object v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->defaultUserAgent:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "system"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lru/cn/ad/video/VideoAdAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    .line 53
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->LOG_TAG:Ljava/lang/String;

    .line 67
    sget-object v1, Lru/cn/ad/video/VAST/VastBaseAdapter;->defaultUserAgent:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    .local v0, "b":Ljava/lang/StringBuilder;
    invoke-static {}, Lru/cn/utils/Utils;->getOSString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    invoke-static {p1}, Lru/cn/utils/Utils;->getDeviceTypeString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    invoke-static {}, Lru/cn/utils/Utils;->getDeviceString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lru/cn/ad/video/VAST/VastBaseAdapter;->defaultUserAgent:Ljava/lang/String;

    .line 76
    .end local v0    # "b":Ljava/lang/StringBuilder;
    :cond_0
    return-void
.end method

.method private getStatMessage(Ljava/lang/String;)Ljava/util/Map;
    .locals 3
    .param p1, "event"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 327
    .local v0, "extra":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v1, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    iget-object v1, v1, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->bannerId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 328
    const-string v1, "banner_id"

    iget-object v2, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    iget-object v2, v2, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->bannerId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 331
    :cond_0
    iget-object v1, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    iget-object v1, v1, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->bannerTitle:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 332
    const-string v1, "banner_title"

    iget-object v2, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    iget-object v2, v2, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->bannerTitle:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    :cond_1
    iget-object v1, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    iget-object v1, v1, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->creativeId:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 336
    const-string v1, "creative_id"

    iget-object v2, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    iget-object v2, v2, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->creativeId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    :cond_2
    iget-object v1, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    iget-object v1, v1, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->adSystem:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 340
    const-string v1, "ad_system"

    iget-object v2, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    iget-object v2, v2, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->adSystem:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 343
    :cond_3
    const-string v1, "start"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    iget-object v1, v1, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->videoUri:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 344
    const-string v1, "url"

    iget-object v2, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    iget-object v2, v2, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->videoUri:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 347
    :cond_4
    return-object v0
.end method


# virtual methods
.method public adCompleted()V
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdEnded()V

    .line 358
    :cond_0
    return-void
.end method

.method public adStarted()V
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdStarted()V

    .line 353
    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 0

    .prologue
    .line 375
    invoke-virtual {p0}, Lru/cn/ad/video/VAST/VastBaseAdapter;->stop()V

    .line 376
    return-void
.end method

.method protected getVastResult(Landroid/content/Context;Ljava/lang/String;)Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;
    .locals 24
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 79
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->LOG_TAG:Ljava/lang/String;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Send request to "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    new-instance v20, Lru/cn/ad/video/VAST/VastTracker;

    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->parsers:Ljava/util/List;

    move-object/from16 v21, v0

    sget-object v22, Lru/cn/ad/video/VAST/VastBaseAdapter;->defaultUserAgent:Ljava/lang/String;

    invoke-direct/range {v20 .. v22}, Lru/cn/ad/video/VAST/VastTracker;-><init>(Ljava/util/List;Ljava/lang/String;)V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lru/cn/ad/video/VAST/VastBaseAdapter;->tracker:Lru/cn/ad/video/ui/VastPresenter$Tracker;

    .line 84
    const/16 v17, 0x0

    .line 85
    .local v17, "vastObj":Lru/cn/ad/video/VAST/parser/VastParser;
    new-instance v20, Lru/cn/utils/http/HttpClient$Builder;

    invoke-direct/range {v20 .. v20}, Lru/cn/utils/http/HttpClient$Builder;-><init>()V

    const-wide/16 v22, 0xbb8

    .line 86
    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lru/cn/utils/http/HttpClient$Builder;->setConnectionTimeoutMillis(J)Lru/cn/utils/http/HttpClient$Builder;

    move-result-object v20

    const-wide/16 v22, 0x1388

    .line 87
    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lru/cn/utils/http/HttpClient$Builder;->setSocketTimeoutMillis(J)Lru/cn/utils/http/HttpClient$Builder;

    move-result-object v20

    .line 88
    invoke-virtual/range {v20 .. v20}, Lru/cn/utils/http/HttpClient$Builder;->build()Lru/cn/utils/http/HttpClient;

    move-result-object v9

    .line 89
    .local v9, "httpClient":Lru/cn/utils/http/HttpClient;
    sget-object v20, Lru/cn/ad/video/VAST/VastBaseAdapter;->defaultUserAgent:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v9, v0}, Lru/cn/utils/http/HttpClient;->setUserAgent(Ljava/lang/String;)Lru/cn/utils/http/HttpClient;

    .line 92
    :try_start_0
    move-object/from16 v0, p2

    invoke-virtual {v9, v0}, Lru/cn/utils/http/HttpClient;->sendRequest(Ljava/lang/String;)V

    .line 94
    invoke-virtual {v9}, Lru/cn/utils/http/HttpClient;->getStatusCode()I

    move-result v14

    .line 95
    .local v14, "statusCode":I
    const/16 v20, 0xcc

    move/from16 v0, v20

    if-ne v14, v0, :cond_0

    .line 96
    const/16 v20, 0x12f

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lru/cn/ad/video/VAST/VastBaseAdapter;->trackError(Landroid/content/Context;I)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lru/cn/ad/video/VAST/parser/VASTParseException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    const/16 v20, 0x0

    .line 127
    invoke-virtual {v9}, Lru/cn/utils/http/HttpClient;->close()V

    .line 205
    .end local v14    # "statusCode":I
    :goto_0
    return-object v20

    .line 99
    .restart local v14    # "statusCode":I
    :cond_0
    const/16 v20, 0xc8

    move/from16 v0, v20

    if-eq v14, v0, :cond_1

    .line 100
    :try_start_1
    new-instance v20, Lru/cn/ad/video/AdvertisementException;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Unexpected Http code "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " url \u2014 "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Lru/cn/ad/video/AdvertisementException;-><init>(Ljava/lang/String;)V

    invoke-static/range {v20 .. v20}, Lru/cn/utils/Logger;->logException(Ljava/lang/Throwable;)V

    .line 102
    const/16 v20, 0x384

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lru/cn/ad/video/VAST/VastBaseAdapter;->trackError(Landroid/content/Context;I)V
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lru/cn/ad/video/VAST/parser/VASTParseException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 103
    const/16 v20, 0x0

    .line 127
    invoke-virtual {v9}, Lru/cn/utils/http/HttpClient;->close()V

    goto :goto_0

    .line 106
    :cond_1
    :try_start_2
    invoke-virtual {v9}, Lru/cn/utils/http/HttpClient;->getContentReader()Ljava/io/Reader;

    move-result-object v6

    .line 107
    .local v6, "contentReader":Ljava/io/Reader;
    new-instance v18, Lru/cn/ad/video/VAST/parser/VastParser;

    new-instance v20, Ljava/io/BufferedReader;

    move-object/from16 v0, v20

    invoke-direct {v0, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lru/cn/ad/video/VAST/parser/VastParser;-><init>(Ljava/io/Reader;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lru/cn/ad/video/VAST/parser/VASTParseException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 127
    .end local v17    # "vastObj":Lru/cn/ad/video/VAST/parser/VastParser;
    .local v18, "vastObj":Lru/cn/ad/video/VAST/parser/VastParser;
    invoke-virtual {v9}, Lru/cn/utils/http/HttpClient;->close()V

    move-object/from16 v17, v18

    .line 130
    .end local v6    # "contentReader":Ljava/io/Reader;
    .end local v14    # "statusCode":I
    .end local v18    # "vastObj":Lru/cn/ad/video/VAST/parser/VastParser;
    .restart local v17    # "vastObj":Lru/cn/ad/video/VAST/parser/VastParser;
    :goto_1
    if-nez v17, :cond_3

    .line 131
    const/16 v20, 0x0

    goto :goto_0

    .line 108
    :catch_0
    move-exception v7

    .line 109
    .local v7, "e":Ljava/net/SocketTimeoutException;
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->LOG_TAG:Ljava/lang/String;

    move-object/from16 v20, v0

    const-string v21, "Timeout error"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 111
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->parsers:Ljava/util/List;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v20

    if-lez v20, :cond_2

    .line 112
    const/16 v20, 0x12d

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lru/cn/ad/video/VAST/VastBaseAdapter;->trackError(Landroid/content/Context;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 127
    :cond_2
    invoke-virtual {v9}, Lru/cn/utils/http/HttpClient;->close()V

    goto :goto_1

    .line 114
    .end local v7    # "e":Ljava/net/SocketTimeoutException;
    :catch_1
    move-exception v7

    .line 116
    .local v7, "e":Ljava/io/IOException;
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->LOG_TAG:Ljava/lang/String;

    move-object/from16 v20, v0

    const-string v21, "Network error"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 127
    invoke-virtual {v9}, Lru/cn/utils/http/HttpClient;->close()V

    goto :goto_1

    .line 117
    .end local v7    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v7

    .line 118
    .local v7, "e":Lru/cn/ad/video/VAST/parser/VASTParseException;
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->LOG_TAG:Ljava/lang/String;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Parsing VAST URL "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " from adv system "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->adSystem:Lru/cn/api/money_miner/replies/AdSystem;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lru/cn/api/money_miner/replies/AdSystem;->id:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lru/cn/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    invoke-static {v7}, Lru/cn/utils/Logger;->logException(Ljava/lang/Throwable;)V

    .line 120
    iget v0, v7, Lru/cn/ad/video/VAST/parser/VASTParseException;->code:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lru/cn/ad/video/VAST/VastBaseAdapter;->trackError(Landroid/content/Context;I)V

    .line 122
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->LOG_TAG:Ljava/lang/String;

    move-object/from16 v20, v0

    const-string v21, "Vast parsing "

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 127
    invoke-virtual {v9}, Lru/cn/utils/http/HttpClient;->close()V

    goto/16 :goto_1

    .line 123
    .end local v7    # "e":Lru/cn/ad/video/VAST/parser/VASTParseException;
    :catch_3
    move-exception v7

    .line 124
    .local v7, "e":Ljava/lang/Exception;
    :try_start_6
    new-instance v20, Lru/cn/ad/video/AdvertisementException;

    const-string v21, "Unexpected error "

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v7}, Lru/cn/ad/video/AdvertisementException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static/range {v20 .. v20}, Lru/cn/utils/Logger;->logException(Ljava/lang/Throwable;)V

    .line 125
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lru/cn/ad/video/VAST/VastBaseAdapter;->trackError(Landroid/content/Context;I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 127
    invoke-virtual {v9}, Lru/cn/utils/http/HttpClient;->close()V

    goto/16 :goto_1

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v20

    invoke-virtual {v9}, Lru/cn/utils/http/HttpClient;->close()V

    .line 128
    throw v20

    .line 134
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->parsers:Ljava/util/List;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v17

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 135
    invoke-virtual/range {v17 .. v17}, Lru/cn/ad/video/VAST/parser/VastParser;->hasWrapper()Z

    move-result v20

    if-eqz v20, :cond_5

    .line 136
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->parsers:Ljava/util/List;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v20

    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-lt v0, v1, :cond_4

    .line 137
    const/16 v20, 0x12e

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lru/cn/ad/video/VAST/VastBaseAdapter;->trackError(Landroid/content/Context;I)V

    .line 138
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 141
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->LOG_TAG:Ljava/lang/String;

    move-object/from16 v20, v0

    const-string v21, "Find wrapper"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    invoke-virtual/range {v17 .. v17}, Lru/cn/ad/video/VAST/parser/VastParser;->getVastAdTagUri()Ljava/lang/String;

    move-result-object v13

    .line 143
    .local v13, "newUrl":Ljava/lang/String;
    if-eqz v13, :cond_f

    .line 144
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lru/cn/ad/video/VAST/VastBaseAdapter;->getVastResult(Landroid/content/Context;Ljava/lang/String;)Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    move-result-object v20

    goto/16 :goto_0

    .line 147
    .end local v13    # "newUrl":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->parsers:Ljava/util/List;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-interface/range {v20 .. v21}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lru/cn/ad/video/VAST/parser/VastParser;

    .line 149
    .local v8, "firstParser":Lru/cn/ad/video/VAST/parser/VastParser;
    new-instance v20, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    invoke-direct/range {v20 .. v20}, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    .line 150
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    move-object/from16 v20, v0

    invoke-virtual {v8}, Lru/cn/ad/video/VAST/parser/VastParser;->getAdSystem()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    iput-object v0, v1, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->adSystem:Ljava/lang/String;

    .line 151
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    move-object/from16 v20, v0

    invoke-virtual {v8}, Lru/cn/ad/video/VAST/parser/VastParser;->getBannerId()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    iput-object v0, v1, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->bannerId:Ljava/lang/String;

    .line 152
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    move-object/from16 v20, v0

    invoke-virtual {v8}, Lru/cn/ad/video/VAST/parser/VastParser;->getCreativeId()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    iput-object v0, v1, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->creativeId:Ljava/lang/String;

    .line 154
    invoke-virtual/range {v17 .. v17}, Lru/cn/ad/video/VAST/parser/VastParser;->getVastVersion()Ljava/lang/String;

    move-result-object v19

    .line 155
    .local v19, "version":Ljava/lang/String;
    const-string v20, "3.0"

    invoke-static/range {v19 .. v20}, Lru/cn/utils/Utils;->versionCompare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v20

    if-gez v20, :cond_7

    .line 156
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    move-object/from16 v20, v0

    new-instance v21, Lru/cn/ad/video/VAST/parser/SkipOffset;

    sget-object v22, Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;->SkipTypeTime:Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;

    const/high16 v23, 0x40a00000    # 5.0f

    invoke-direct/range {v21 .. v23}, Lru/cn/ad/video/VAST/parser/SkipOffset;-><init>(Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;F)V

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    iput-object v0, v1, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->skipOffset:Lru/cn/ad/video/VAST/parser/SkipOffset;

    .line 161
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->parsers:Ljava/util/List;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :cond_6
    :goto_3
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_8

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lru/cn/ad/video/VAST/parser/VastParser;

    .line 162
    .local v16, "vasObj":Lru/cn/ad/video/VAST/parser/VastParser;
    invoke-virtual/range {v16 .. v16}, Lru/cn/ad/video/VAST/parser/VastParser;->getAdTitle()Ljava/lang/String;

    move-result-object v21

    if-eqz v21, :cond_6

    invoke-virtual/range {v16 .. v16}, Lru/cn/ad/video/VAST/parser/VastParser;->getAdTitle()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    if-lez v21, :cond_6

    .line 163
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    move-object/from16 v21, v0

    invoke-virtual/range {v16 .. v16}, Lru/cn/ad/video/VAST/parser/VastParser;->getAdTitle()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    iput-object v0, v1, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->bannerTitle:Ljava/lang/String;

    goto :goto_3

    .line 158
    .end local v16    # "vasObj":Lru/cn/ad/video/VAST/parser/VastParser;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    move-object/from16 v20, v0

    invoke-virtual/range {v17 .. v17}, Lru/cn/ad/video/VAST/parser/VastParser;->getSkipOffset()Lru/cn/ad/video/VAST/parser/SkipOffset;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    iput-object v0, v1, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->skipOffset:Lru/cn/ad/video/VAST/parser/SkipOffset;

    goto :goto_2

    .line 167
    :cond_8
    invoke-virtual/range {v17 .. v17}, Lru/cn/ad/video/VAST/parser/VastParser;->getMediaFiles()Ljava/util/List;

    move-result-object v11

    .line 169
    .local v11, "mediaFiles":Ljava/util/List;, "Ljava/util/List<Lru/cn/ad/video/VAST/parser/MediaFile;>;"
    if-nez v11, :cond_a

    .line 170
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->parsers:Ljava/util/List;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_9

    .line 171
    const/16 v20, 0x12f

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lru/cn/ad/video/VAST/VastBaseAdapter;->trackError(Landroid/content/Context;I)V

    .line 173
    :cond_9
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 176
    :cond_a
    new-instance v5, Lru/cn/ad/video/VAST/MediaComparator;

    sget-object v20, Lru/cn/ad/video/VAST/MediaPredicate;->DEFAULT_MIME_TYPES:Ljava/util/List;

    move-object/from16 v0, p0

    iget v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->optimalVideoDimension:I

    move/from16 v21, v0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v5, v0, v1}, Lru/cn/ad/video/VAST/MediaComparator;-><init>(Ljava/util/List;I)V

    .line 177
    .local v5, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lru/cn/ad/video/VAST/parser/MediaFile;>;"
    invoke-static {v11, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 179
    const/4 v15, 0x0

    .line 180
    .local v15, "supportedMediaFile":Lru/cn/ad/video/VAST/parser/MediaFile;
    new-instance v12, Lru/cn/ad/video/VAST/MediaPredicate;

    move-object/from16 v0, p0

    iget v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->maxVideoDimension:I

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-direct {v12, v0}, Lru/cn/ad/video/VAST/MediaPredicate;-><init>(I)V

    .line 181
    .local v12, "mediaPredicate":Lcom/android/internal/util/Predicate;, "Lcom/android/internal/util/Predicate<Lru/cn/ad/video/VAST/parser/MediaFile;>;"
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :cond_b
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_c

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lru/cn/ad/video/VAST/parser/MediaFile;

    .line 182
    .local v10, "mediaFile":Lru/cn/ad/video/VAST/parser/MediaFile;
    invoke-interface {v12, v10}, Lcom/android/internal/util/Predicate;->apply(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_b

    .line 183
    move-object v15, v10

    .line 189
    .end local v10    # "mediaFile":Lru/cn/ad/video/VAST/parser/MediaFile;
    :cond_c
    if-nez v15, :cond_d

    .line 190
    const/16 v20, 0x193

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lru/cn/ad/video/VAST/VastBaseAdapter;->trackError(Landroid/content/Context;I)V

    .line 191
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 194
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    move-object/from16 v20, v0

    iget-object v0, v15, Lru/cn/ad/video/VAST/parser/MediaFile;->mediaFileUrl:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    iput-object v0, v1, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->videoUri:Ljava/lang/String;

    .line 196
    invoke-virtual/range {v17 .. v17}, Lru/cn/ad/video/VAST/parser/VastParser;->getClickThroughUrl()Ljava/lang/String;

    move-result-object v4

    .line 197
    .local v4, "adOwnerUri":Ljava/lang/String;
    if-eqz v4, :cond_e

    const-string v20, "http://"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v20

    if-nez v20, :cond_e

    const-string v20, "https://"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v20

    if-nez v20, :cond_e

    .line 198
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "http://"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 201
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iput-object v4, v0, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->adOwnerUri:Ljava/lang/String;

    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    move-object/from16 v20, v0

    goto/16 :goto_0

    .line 205
    .end local v4    # "adOwnerUri":Ljava/lang/String;
    .end local v5    # "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lru/cn/ad/video/VAST/parser/MediaFile;>;"
    .end local v8    # "firstParser":Lru/cn/ad/video/VAST/parser/VastParser;
    .end local v11    # "mediaFiles":Ljava/util/List;, "Ljava/util/List<Lru/cn/ad/video/VAST/parser/MediaFile;>;"
    .end local v12    # "mediaPredicate":Lcom/android/internal/util/Predicate;, "Lcom/android/internal/util/Predicate<Lru/cn/ad/video/VAST/parser/MediaFile;>;"
    .end local v15    # "supportedMediaFile":Lru/cn/ad/video/VAST/parser/MediaFile;
    .end local v19    # "version":Ljava/lang/String;
    .restart local v13    # "newUrl":Ljava/lang/String;
    :cond_f
    const/16 v20, 0x0

    goto/16 :goto_0
.end method

.method public show()V
    .locals 3

    .prologue
    .line 362
    iget-object v1, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->presenter:Lru/cn/ad/video/ui/VastPresenter;

    if-nez v1, :cond_0

    .line 363
    new-instance v0, Lru/cn/ad/video/ui/VastPresenter;

    invoke-virtual {p0}, Lru/cn/ad/video/VAST/VastBaseAdapter;->getRenderingSettings()Lru/cn/ad/video/RenderingSettings;

    move-result-object v1

    iget-object v2, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    iget-object v2, v2, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->videoUri:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p0, p0}, Lru/cn/ad/video/ui/VastPresenter;-><init>(Lru/cn/ad/video/RenderingSettings;Ljava/lang/String;Lru/cn/ad/video/ui/VastPresenter$Tracker;Lru/cn/ad/video/ui/VastPresenter$PresenterListener;)V

    .line 364
    .local v0, "vastPresenter":Lru/cn/ad/video/ui/VastPresenter;
    iget-object v1, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    iget-object v1, v1, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->skipOffset:Lru/cn/ad/video/VAST/parser/SkipOffset;

    invoke-virtual {v0, v1}, Lru/cn/ad/video/ui/VastPresenter;->setSkipOffset(Lru/cn/ad/video/VAST/parser/SkipOffset;)V

    .line 365
    iget-object v1, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    iget-object v1, v1, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->adOwnerUri:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lru/cn/ad/video/ui/VastPresenter;->setAdClickUri(Ljava/lang/String;)V

    .line 367
    iput-object v0, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->presenter:Lru/cn/ad/video/ui/VastPresenter;

    .line 370
    .end local v0    # "vastPresenter":Lru/cn/ad/video/ui/VastPresenter;
    :cond_0
    iget-object v1, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->presenter:Lru/cn/ad/video/ui/VastPresenter;

    invoke-virtual {v1}, Lru/cn/ad/video/ui/VastPresenter;->play()V

    .line 371
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->presenter:Lru/cn/ad/video/ui/VastPresenter;

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->presenter:Lru/cn/ad/video/ui/VastPresenter;

    invoke-virtual {v0}, Lru/cn/ad/video/ui/VastPresenter;->destroy()V

    .line 387
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->presenter:Lru/cn/ad/video/ui/VastPresenter;

    .line 389
    :cond_0
    return-void
.end method

.method public trackClick(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 253
    iget-object v0, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->tracker:Lru/cn/ad/video/ui/VastPresenter$Tracker;

    invoke-interface {v0, p1}, Lru/cn/ad/video/ui/VastPresenter$Tracker;->trackClick(Landroid/content/Context;)V

    .line 256
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_CLICK:Lru/cn/domain/statistics/inetra/AdvEvent;

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lru/cn/ad/video/VAST/VastBaseAdapter;->getStatMessage(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lru/cn/ad/video/VAST/VastBaseAdapter;->reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/util/Map;)V

    .line 257
    return-void
.end method

.method public trackError(Landroid/content/Context;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "errorCode"    # I

    .prologue
    .line 261
    iget-object v6, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->tracker:Lru/cn/ad/video/ui/VastPresenter$Tracker;

    invoke-interface {v6, p1, p2}, Lru/cn/ad/video/ui/VastPresenter$Tracker;->trackError(Landroid/content/Context;I)V

    .line 265
    sparse-switch p2, :sswitch_data_0

    .line 279
    sget-object v5, Lru/cn/domain/statistics/inetra/ErrorCode;->ADV_ERROR_LOAD_BANNER:Lru/cn/domain/statistics/inetra/ErrorCode;

    .line 283
    .local v5, "trackingCode":Lru/cn/domain/statistics/inetra/ErrorCode;
    :goto_0
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 284
    .local v0, "extra":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v6, 0x193

    if-ne p2, v6, :cond_2

    .line 285
    iget-object v6, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->parsers:Ljava/util/List;

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/ad/video/VAST/parser/VastParser;

    .line 286
    .local v1, "firstParser":Lru/cn/ad/video/VAST/parser/VastParser;
    invoke-virtual {v1}, Lru/cn/ad/video/VAST/parser/VastParser;->getMediaFiles()Ljava/util/List;

    move-result-object v3

    .line 288
    .local v3, "mediaFiles":Ljava/util/List;, "Ljava/util/List<Lru/cn/ad/video/VAST/parser/MediaFile;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 289
    .local v4, "mimeTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v3, :cond_1

    .line 290
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/ad/video/VAST/parser/MediaFile;

    .line 291
    .local v2, "mediaFile":Lru/cn/ad/video/VAST/parser/MediaFile;
    iget-object v7, v2, Lru/cn/ad/video/VAST/parser/MediaFile;->type:Ljava/lang/String;

    if-eqz v7, :cond_0

    iget-object v7, v2, Lru/cn/ad/video/VAST/parser/MediaFile;->type:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_0

    .line 292
    iget-object v7, v2, Lru/cn/ad/video/VAST/parser/MediaFile;->type:Ljava/lang/String;

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 270
    .end local v0    # "extra":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "firstParser":Lru/cn/ad/video/VAST/parser/VastParser;
    .end local v2    # "mediaFile":Lru/cn/ad/video/VAST/parser/MediaFile;
    .end local v3    # "mediaFiles":Ljava/util/List;, "Ljava/util/List<Lru/cn/ad/video/VAST/parser/MediaFile;>;"
    .end local v4    # "mimeTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v5    # "trackingCode":Lru/cn/domain/statistics/inetra/ErrorCode;
    :sswitch_0
    sget-object v5, Lru/cn/domain/statistics/inetra/ErrorCode;->ADV_ERROR_PLAY:Lru/cn/domain/statistics/inetra/ErrorCode;

    .line 271
    .restart local v5    # "trackingCode":Lru/cn/domain/statistics/inetra/ErrorCode;
    goto :goto_0

    .line 297
    .restart local v0    # "extra":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v1    # "firstParser":Lru/cn/ad/video/VAST/parser/VastParser;
    .restart local v3    # "mediaFiles":Ljava/util/List;, "Ljava/util/List<Lru/cn/ad/video/VAST/parser/MediaFile;>;"
    .restart local v4    # "mimeTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_2

    .line 298
    const-string v6, "mime"

    const-string v7, ","

    invoke-static {v7, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    .end local v1    # "firstParser":Lru/cn/ad/video/VAST/parser/VastParser;
    .end local v3    # "mediaFiles":Ljava/util/List;, "Ljava/util/List<Lru/cn/ad/video/VAST/parser/MediaFile;>;"
    .end local v4    # "mimeTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    iget-object v6, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    if-eqz v6, :cond_6

    .line 304
    iget-object v6, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    iget-object v6, v6, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->videoUri:Ljava/lang/String;

    if-eqz v6, :cond_3

    .line 305
    const-string v6, "ad_url"

    iget-object v7, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    iget-object v7, v7, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->videoUri:Ljava/lang/String;

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    :cond_3
    iget-object v6, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    iget-object v6, v6, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->adSystem:Ljava/lang/String;

    if-eqz v6, :cond_4

    .line 309
    const-string v6, "ad_system"

    iget-object v7, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    iget-object v7, v7, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->adSystem:Ljava/lang/String;

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    :cond_4
    iget-object v6, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    iget-object v6, v6, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->bannerId:Ljava/lang/String;

    if-eqz v6, :cond_5

    .line 313
    const-string v6, "ad_id"

    iget-object v7, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    iget-object v7, v7, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->bannerId:Ljava/lang/String;

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 316
    :cond_5
    iget-object v6, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    iget-object v6, v6, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->creativeId:Ljava/lang/String;

    if-eqz v6, :cond_6

    .line 317
    const-string v6, "creative_id"

    iget-object v7, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    iget-object v7, v7, Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;->creativeId:Ljava/lang/String;

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    :cond_6
    const-string v6, "VASTError"

    invoke-virtual {p0, v5, v6, p2, v0}, Lru/cn/ad/video/VAST/VastBaseAdapter;->reportError(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/util/Map;)V

    .line 322
    .end local v0    # "extra":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5    # "trackingCode":Lru/cn/domain/statistics/inetra/ErrorCode;
    :sswitch_1
    return-void

    .line 265
    nop

    :sswitch_data_0
    .sparse-switch
        0x12d -> :sswitch_1
        0x12f -> :sswitch_1
        0x190 -> :sswitch_0
        0x191 -> :sswitch_0
        0x192 -> :sswitch_0
        0x195 -> :sswitch_0
    .end sparse-switch
.end method

.method public trackEvent(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "eventName"    # Ljava/lang/String;

    .prologue
    .line 215
    iget-object v1, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->tracker:Lru/cn/ad/video/ui/VastPresenter$Tracker;

    invoke-interface {v1, p1, p2}, Lru/cn/ad/video/ui/VastPresenter$Tracker;->trackEvent(Landroid/content/Context;Ljava/lang/String;)V

    .line 218
    const/4 v0, 0x0

    .line 219
    .local v0, "adEventId":Lru/cn/domain/statistics/inetra/AdvEvent;
    const/4 v1, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 246
    :goto_1
    if-eqz v0, :cond_1

    .line 247
    invoke-direct {p0, p2}, Lru/cn/ad/video/VAST/VastBaseAdapter;->getStatMessage(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lru/cn/ad/video/VAST/VastBaseAdapter;->reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/util/Map;)V

    .line 249
    :cond_1
    return-void

    .line 219
    :sswitch_0
    const-string v2, "start"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "firstQuartile"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "midpoint"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "thirdQuartile"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v2, "complete"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_5
    const-string v2, "skip"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    .line 221
    :pswitch_0
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_START:Lru/cn/domain/statistics/inetra/AdvEvent;

    .line 222
    goto :goto_1

    .line 225
    :pswitch_1
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_FIRST_QUARTILE:Lru/cn/domain/statistics/inetra/AdvEvent;

    .line 226
    goto :goto_1

    .line 229
    :pswitch_2
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_MIDPOINT:Lru/cn/domain/statistics/inetra/AdvEvent;

    .line 230
    goto :goto_1

    .line 233
    :pswitch_3
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_THIRD_QUARTILE:Lru/cn/domain/statistics/inetra/AdvEvent;

    .line 234
    goto :goto_1

    .line 237
    :pswitch_4
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_COMPLETE:Lru/cn/domain/statistics/inetra/AdvEvent;

    .line 238
    goto :goto_1

    .line 241
    :pswitch_5
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_SKIP:Lru/cn/domain/statistics/inetra/AdvEvent;

    goto :goto_1

    .line 219
    nop

    :sswitch_data_0
    .sparse-switch
        -0x61aea3b8 -> :sswitch_2
        -0x4fbdabf6 -> :sswitch_3
        -0x23bacec7 -> :sswitch_4
        0x35e57f -> :sswitch_5
        0x68ac462 -> :sswitch_0
        0x21644853 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public trackImpression(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 210
    iget-object v0, p0, Lru/cn/ad/video/VAST/VastBaseAdapter;->tracker:Lru/cn/ad/video/ui/VastPresenter$Tracker;

    invoke-interface {v0, p1}, Lru/cn/ad/video/ui/VastPresenter$Tracker;->trackImpression(Landroid/content/Context;)V

    .line 211
    return-void
.end method
