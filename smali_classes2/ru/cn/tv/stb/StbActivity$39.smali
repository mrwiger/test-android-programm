.class Lru/cn/tv/stb/StbActivity$39;
.super Ljava/lang/Object;
.source "StbActivity.java"

# interfaces
.implements Lru/cn/tv/stb/parental/PasswordDialogPresenter$PasswordDialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/stb/StbActivity;->blockChannel(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/stb/StbActivity;

.field final synthetic val$channelId:J


# direct methods
.method constructor <init>(Lru/cn/tv/stb/StbActivity;J)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 2555
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity$39;->this$0:Lru/cn/tv/stb/StbActivity;

    iput-wide p2, p0, Lru/cn/tv/stb/StbActivity$39;->val$channelId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onForgotPassword()V
    .locals 4

    .prologue
    .line 2566
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$39;->this$0:Lru/cn/tv/stb/StbActivity;

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$39;->this$0:Lru/cn/tv/stb/StbActivity;

    .line 2567
    invoke-virtual {v1}, Lru/cn/tv/stb/StbActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e00e6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, p0, Lru/cn/tv/stb/StbActivity$39;->this$0:Lru/cn/tv/stb/StbActivity;

    .line 2568
    invoke-virtual {v2}, Lru/cn/tv/stb/StbActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e00f2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 2566
    invoke-static {v0, v1, v2}, Lru/cn/tv/errors/ErrorDialog;->create(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/Dialog;

    move-result-object v0

    .line 2569
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 2570
    return-void
.end method

.method public onPasswordAccepted()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2558
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$39;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$3800(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/StbViewModel;

    move-result-object v0

    iget-wide v2, p0, Lru/cn/tv/stb/StbActivity$39;->val$channelId:J

    invoke-virtual {v0, v2, v3, v4}, Lru/cn/tv/stb/StbViewModel;->setBlocked(JZ)V

    .line 2559
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$39;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$000(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/SimplePlayerFragment;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragment;->getChannelId()J

    move-result-wide v0

    iget-wide v2, p0, Lru/cn/tv/stb/StbActivity$39;->val$channelId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2560
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$39;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/StbPlayerController;

    move-result-object v0

    invoke-virtual {v0, v4}, Lru/cn/tv/player/controller/StbPlayerController;->showChannelBlocked(Z)V

    .line 2562
    :cond_0
    return-void
.end method
