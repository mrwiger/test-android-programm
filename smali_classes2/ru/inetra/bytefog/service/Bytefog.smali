.class public Lru/inetra/bytefog/service/Bytefog;
.super Ljava/lang/Object;
.source "Bytefog.java"


# static fields
.field private static serviceProxy:Lru/inetra/bytefog/service/ServiceProxy;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static initialize(Landroid/content/Context;Lru/inetra/bytefog/service/BytefogConfig;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "config"    # Lru/inetra/bytefog/service/BytefogConfig;

    .prologue
    .line 12
    sget-object v0, Lru/inetra/bytefog/service/Bytefog;->serviceProxy:Lru/inetra/bytefog/service/ServiceProxy;

    if-nez v0, :cond_0

    .line 13
    new-instance v0, Lru/inetra/bytefog/service/ServiceProxy;

    invoke-direct {v0, p0, p1}, Lru/inetra/bytefog/service/ServiceProxy;-><init>(Landroid/content/Context;Lru/inetra/bytefog/service/BytefogConfig;)V

    sput-object v0, Lru/inetra/bytefog/service/Bytefog;->serviceProxy:Lru/inetra/bytefog/service/ServiceProxy;

    .line 15
    :cond_0
    return-void
.end method

.method public static instance()Lru/inetra/bytefog/service/Instance;
    .locals 2

    .prologue
    .line 18
    sget-object v0, Lru/inetra/bytefog/service/Bytefog;->serviceProxy:Lru/inetra/bytefog/service/ServiceProxy;

    if-nez v0, :cond_0

    .line 19
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Bytefog is not initialized. Call Bytefog.initialize(...) first."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_0
    sget-object v0, Lru/inetra/bytefog/service/Bytefog;->serviceProxy:Lru/inetra/bytefog/service/ServiceProxy;

    return-object v0
.end method
