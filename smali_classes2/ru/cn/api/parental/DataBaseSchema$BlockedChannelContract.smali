.class final enum Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;
.super Ljava/lang/Enum;
.source "DataBaseSchema.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/api/parental/DataBaseSchema;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "BlockedChannelContract"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;

.field public static final enum CN_ID:Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;

.field public static final enum IMAGE_URL:Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;

.field public static final enum NAME_CHANNEL:Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;


# instance fields
.field private final isUnique:Z

.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 7
    new-instance v0, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;

    const-string v1, "CN_ID"

    const-string v2, "cnId"

    invoke-direct {v0, v1, v4, v2, v3}, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->CN_ID:Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;

    .line 8
    new-instance v0, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;

    const-string v1, "NAME_CHANNEL"

    const-string v2, "title"

    invoke-direct {v0, v1, v3, v2, v3}, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->NAME_CHANNEL:Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;

    .line 9
    new-instance v0, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;

    const-string v1, "IMAGE_URL"

    const-string v2, "logo"

    invoke-direct {v0, v1, v5, v2, v4}, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->IMAGE_URL:Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;

    .line 6
    const/4 v0, 0x3

    new-array v0, v0, [Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;

    sget-object v1, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->CN_ID:Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;

    aput-object v1, v0, v4

    sget-object v1, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->NAME_CHANNEL:Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;

    aput-object v1, v0, v3

    sget-object v1, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->IMAGE_URL:Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;

    aput-object v1, v0, v5

    sput-object v0, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->$VALUES:[Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .param p4, "isUnique"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 15
    iput-object p3, p0, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->value:Ljava/lang/String;

    .line 16
    iput-boolean p4, p0, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->isUnique:Z

    .line 17
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 6
    const-class v0, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;

    return-object v0
.end method

.method public static values()[Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->$VALUES:[Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;

    invoke-virtual {v0}, [Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;

    return-object v0
.end method


# virtual methods
.method public getColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->value:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 25
    iget-boolean v0, p0, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->isUnique:Z

    if-eqz v0, :cond_0

    .line 26
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->getColumnName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " UNIQUE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 28
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->getColumnName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
