.class public final Lcom/my/target/core/models/banners/i;
.super Lcom/my/target/ah;
.source "NativeAppwallAdBanner.java"


# instance fields
.field private appInstalled:Z

.field private bubbleIcon:Lcom/my/target/common/models/ImageData;

.field private bubbleId:Ljava/lang/String;

.field private coins:I

.field private coinsIcon:Lcom/my/target/common/models/ImageData;

.field private coinsIconBgColor:I

.field private coinsIconTextColor:I

.field private crossNotifIcon:Lcom/my/target/common/models/ImageData;

.field private gotoAppIcon:Lcom/my/target/common/models/ImageData;

.field private hasNotification:Z

.field private isBanner:Z

.field private isItemHighlight:Z

.field private isMain:Z

.field private isRequireCategoryHighlight:Z

.field private isRequireWifi:Z

.field private isSubItem:Z

.field private itemHighlightIcon:Lcom/my/target/common/models/ImageData;

.field private labelIcon:Lcom/my/target/common/models/ImageData;

.field private labelType:Ljava/lang/String;

.field private mrgsId:I

.field private paidType:Ljava/lang/String;

.field private status:Ljava/lang/String;

.field private statusIcon:Lcom/my/target/common/models/ImageData;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/my/target/ah;-><init>()V

    .line 38
    const v0, -0x86de2

    iput v0, p0, Lcom/my/target/core/models/banners/i;->coinsIconBgColor:I

    .line 39
    const/4 v0, -0x1

    iput v0, p0, Lcom/my/target/core/models/banners/i;->coinsIconTextColor:I

    .line 59
    return-void
.end method

.method public static newBanner()Lcom/my/target/core/models/banners/i;
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/my/target/core/models/banners/i;

    invoke-direct {v0}, Lcom/my/target/core/models/banners/i;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final getBubbleIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/my/target/core/models/banners/i;->bubbleIcon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public final getBubbleId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/my/target/core/models/banners/i;->bubbleId:Ljava/lang/String;

    return-object v0
.end method

.method public final getCoins()I
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lcom/my/target/core/models/banners/i;->coins:I

    return v0
.end method

.method public final getCoinsIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/my/target/core/models/banners/i;->coinsIcon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public final getCoinsIconBgColor()I
    .locals 1

    .prologue
    .line 220
    iget v0, p0, Lcom/my/target/core/models/banners/i;->coinsIconBgColor:I

    return v0
.end method

.method public final getCoinsIconTextColor()I
    .locals 1

    .prologue
    .line 230
    iget v0, p0, Lcom/my/target/core/models/banners/i;->coinsIconTextColor:I

    return v0
.end method

.method public final getCrossNotifIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/my/target/core/models/banners/i;->crossNotifIcon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public final getGotoAppIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/my/target/core/models/banners/i;->gotoAppIcon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public final getItemHighlightIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/my/target/core/models/banners/i;->itemHighlightIcon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public final getLabelIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/my/target/core/models/banners/i;->labelIcon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public final getLabelType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/my/target/core/models/banners/i;->labelType:Ljava/lang/String;

    return-object v0
.end method

.method public final getMrgsId()I
    .locals 1

    .prologue
    .line 200
    iget v0, p0, Lcom/my/target/core/models/banners/i;->mrgsId:I

    return v0
.end method

.method public final getPaidType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/my/target/core/models/banners/i;->paidType:Ljava/lang/String;

    return-object v0
.end method

.method public final getStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/my/target/core/models/banners/i;->status:Ljava/lang/String;

    return-object v0
.end method

.method public final getStatusIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/my/target/core/models/banners/i;->statusIcon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public final isAppInstalled()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/my/target/core/models/banners/i;->appInstalled:Z

    return v0
.end method

.method public final isBanner()Z
    .locals 1

    .prologue
    .line 270
    iget-boolean v0, p0, Lcom/my/target/core/models/banners/i;->isBanner:Z

    return v0
.end method

.method public final isHasNotification()Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/my/target/core/models/banners/i;->hasNotification:Z

    return v0
.end method

.method public final isItemHighlight()Z
    .locals 1

    .prologue
    .line 260
    iget-boolean v0, p0, Lcom/my/target/core/models/banners/i;->isItemHighlight:Z

    return v0
.end method

.method public final isMain()Z
    .locals 1

    .prologue
    .line 240
    iget-boolean v0, p0, Lcom/my/target/core/models/banners/i;->isMain:Z

    return v0
.end method

.method public final isRequireCategoryHighlight()Z
    .locals 1

    .prologue
    .line 250
    iget-boolean v0, p0, Lcom/my/target/core/models/banners/i;->isRequireCategoryHighlight:Z

    return v0
.end method

.method public final isRequireWifi()Z
    .locals 1

    .prologue
    .line 280
    iget-boolean v0, p0, Lcom/my/target/core/models/banners/i;->isRequireWifi:Z

    return v0
.end method

.method public final isSubItem()Z
    .locals 1

    .prologue
    .line 290
    iget-boolean v0, p0, Lcom/my/target/core/models/banners/i;->isSubItem:Z

    return v0
.end method

.method public final setAppInstalled(Z)V
    .locals 0
    .param p1, "appInstalled"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/my/target/core/models/banners/i;->appInstalled:Z

    .line 64
    return-void
.end method

.method public final setBanner(Z)V
    .locals 0
    .param p1, "banner"    # Z

    .prologue
    .line 275
    iput-boolean p1, p0, Lcom/my/target/core/models/banners/i;->isBanner:Z

    .line 276
    return-void
.end method

.method public final setBubbleIcon(Lcom/my/target/common/models/ImageData;)V
    .locals 0
    .param p1, "bubbleIcon"    # Lcom/my/target/common/models/ImageData;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/my/target/core/models/banners/i;->bubbleIcon:Lcom/my/target/common/models/ImageData;

    .line 134
    return-void
.end method

.method public final setBubbleId(Ljava/lang/String;)V
    .locals 0
    .param p1, "bubbleId"    # Ljava/lang/String;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/my/target/core/models/banners/i;->bubbleId:Ljava/lang/String;

    .line 166
    return-void
.end method

.method public final setCoins(I)V
    .locals 0
    .param p1, "coins"    # I

    .prologue
    .line 215
    iput p1, p0, Lcom/my/target/core/models/banners/i;->coins:I

    .line 216
    return-void
.end method

.method public final setCoinsIcon(Lcom/my/target/common/models/ImageData;)V
    .locals 0
    .param p1, "coinsIcon"    # Lcom/my/target/common/models/ImageData;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/my/target/core/models/banners/i;->coinsIcon:Lcom/my/target/common/models/ImageData;

    .line 90
    return-void
.end method

.method public final setCoinsIconBgColor(I)V
    .locals 0
    .param p1, "coinsIconBgColor"    # I

    .prologue
    .line 225
    iput p1, p0, Lcom/my/target/core/models/banners/i;->coinsIconBgColor:I

    .line 226
    return-void
.end method

.method public final setCoinsIconTextColor(I)V
    .locals 0
    .param p1, "coinsIconTextColor"    # I

    .prologue
    .line 235
    iput p1, p0, Lcom/my/target/core/models/banners/i;->coinsIconTextColor:I

    .line 236
    return-void
.end method

.method public final setCrossNotifIcon(Lcom/my/target/common/models/ImageData;)V
    .locals 0
    .param p1, "crossNotifIcon"    # Lcom/my/target/common/models/ImageData;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/my/target/core/models/banners/i;->crossNotifIcon:Lcom/my/target/common/models/ImageData;

    .line 156
    return-void
.end method

.method public final setGotoAppIcon(Lcom/my/target/common/models/ImageData;)V
    .locals 0
    .param p1, "gotoAppIcon"    # Lcom/my/target/common/models/ImageData;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/my/target/core/models/banners/i;->gotoAppIcon:Lcom/my/target/common/models/ImageData;

    .line 112
    return-void
.end method

.method public final setHasNotification(Z)V
    .locals 0
    .param p1, "hasNotification"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/my/target/core/models/banners/i;->hasNotification:Z

    .line 74
    return-void
.end method

.method public final setItemHighlight(Z)V
    .locals 0
    .param p1, "itemHighlight"    # Z

    .prologue
    .line 265
    iput-boolean p1, p0, Lcom/my/target/core/models/banners/i;->isItemHighlight:Z

    .line 266
    return-void
.end method

.method public final setItemHighlightIcon(Lcom/my/target/common/models/ImageData;)V
    .locals 0
    .param p1, "itemHighlightIcon"    # Lcom/my/target/common/models/ImageData;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/my/target/core/models/banners/i;->itemHighlightIcon:Lcom/my/target/common/models/ImageData;

    .line 145
    return-void
.end method

.method public final setLabelIcon(Lcom/my/target/common/models/ImageData;)V
    .locals 0
    .param p1, "labelIcon"    # Lcom/my/target/common/models/ImageData;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/my/target/core/models/banners/i;->labelIcon:Lcom/my/target/common/models/ImageData;

    .line 101
    return-void
.end method

.method public final setLabelType(Ljava/lang/String;)V
    .locals 0
    .param p1, "labelType"    # Ljava/lang/String;

    .prologue
    .line 175
    iput-object p1, p0, Lcom/my/target/core/models/banners/i;->labelType:Ljava/lang/String;

    .line 176
    return-void
.end method

.method public final setMain(Z)V
    .locals 0
    .param p1, "main"    # Z

    .prologue
    .line 245
    iput-boolean p1, p0, Lcom/my/target/core/models/banners/i;->isMain:Z

    .line 246
    return-void
.end method

.method public final setMrgsId(I)V
    .locals 0
    .param p1, "mrgsId"    # I

    .prologue
    .line 205
    iput p1, p0, Lcom/my/target/core/models/banners/i;->mrgsId:I

    .line 206
    return-void
.end method

.method public final setPaidType(Ljava/lang/String;)V
    .locals 0
    .param p1, "paidType"    # Ljava/lang/String;

    .prologue
    .line 195
    iput-object p1, p0, Lcom/my/target/core/models/banners/i;->paidType:Ljava/lang/String;

    .line 196
    return-void
.end method

.method public final setRequireCategoryHighlight(Z)V
    .locals 0
    .param p1, "requireCategoryHighlight"    # Z

    .prologue
    .line 255
    iput-boolean p1, p0, Lcom/my/target/core/models/banners/i;->isRequireCategoryHighlight:Z

    .line 256
    return-void
.end method

.method public final setRequireWifi(Z)V
    .locals 0
    .param p1, "requireWifi"    # Z

    .prologue
    .line 285
    iput-boolean p1, p0, Lcom/my/target/core/models/banners/i;->isRequireWifi:Z

    .line 286
    return-void
.end method

.method public final setStatus(Ljava/lang/String;)V
    .locals 0
    .param p1, "status"    # Ljava/lang/String;

    .prologue
    .line 185
    iput-object p1, p0, Lcom/my/target/core/models/banners/i;->status:Ljava/lang/String;

    .line 186
    return-void
.end method

.method public final setStatusIcon(Lcom/my/target/common/models/ImageData;)V
    .locals 0
    .param p1, "statusIcon"    # Lcom/my/target/common/models/ImageData;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/my/target/core/models/banners/i;->statusIcon:Lcom/my/target/common/models/ImageData;

    .line 123
    return-void
.end method

.method public final setSubItem(Z)V
    .locals 0
    .param p1, "subItem"    # Z

    .prologue
    .line 295
    iput-boolean p1, p0, Lcom/my/target/core/models/banners/i;->isSubItem:Z

    .line 296
    return-void
.end method
