.class public Ltoothpick/config/Binding;
.super Ljava/lang/Object;
.source "Binding.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltoothpick/config/Binding$BoundStateForProviderInstanceBinding;,
        Ltoothpick/config/Binding$BoundStateForProviderClassBinding;,
        Ltoothpick/config/Binding$BoundStateForClassBinding;,
        Ltoothpick/config/Binding$Mode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private implementationClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+TT;>;"
        }
    .end annotation
.end field

.field private instance:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private isCreatingInstancesInScope:Z

.field private isCreatingSingletonInScope:Z

.field private isProvidingSingletonInScope:Z

.field private key:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mode:Ltoothpick/config/Binding$Mode;

.field private name:Ljava/lang/String;

.field private providerClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljavax/inject/Provider",
            "<+TT;>;>;"
        }
    .end annotation
.end field

.field private providerInstance:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p0, "this":Ltoothpick/config/Binding;, "Ltoothpick/config/Binding<TT;>;"
    .local p1, "key":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Ltoothpick/config/Binding;->key:Ljava/lang/Class;

    .line 21
    sget-object v0, Ltoothpick/config/Binding$Mode;->SIMPLE:Ltoothpick/config/Binding$Mode;

    iput-object v0, p0, Ltoothpick/config/Binding;->mode:Ltoothpick/config/Binding$Mode;

    .line 22
    return-void
.end method

.method static synthetic access$002(Ltoothpick/config/Binding;Z)Z
    .locals 0
    .param p0, "x0"    # Ltoothpick/config/Binding;
    .param p1, "x1"    # Z

    .prologue
    .line 7
    iput-boolean p1, p0, Ltoothpick/config/Binding;->isProvidingSingletonInScope:Z

    return p1
.end method


# virtual methods
.method public getImplementationClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+TT;>;"
        }
    .end annotation

    .prologue
    .line 80
    .local p0, "this":Ltoothpick/config/Binding;, "Ltoothpick/config/Binding<TT;>;"
    iget-object v0, p0, Ltoothpick/config/Binding;->implementationClass:Ljava/lang/Class;

    return-object v0
.end method

.method public getInstance()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 84
    .local p0, "this":Ltoothpick/config/Binding;, "Ltoothpick/config/Binding<TT;>;"
    iget-object v0, p0, Ltoothpick/config/Binding;->instance:Ljava/lang/Object;

    return-object v0
.end method

.method public getKey()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 76
    .local p0, "this":Ltoothpick/config/Binding;, "Ltoothpick/config/Binding<TT;>;"
    iget-object v0, p0, Ltoothpick/config/Binding;->key:Ljava/lang/Class;

    return-object v0
.end method

.method public getMode()Ltoothpick/config/Binding$Mode;
    .locals 1

    .prologue
    .line 72
    .local p0, "this":Ltoothpick/config/Binding;, "Ltoothpick/config/Binding<TT;>;"
    iget-object v0, p0, Ltoothpick/config/Binding;->mode:Ltoothpick/config/Binding$Mode;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    .local p0, "this":Ltoothpick/config/Binding;, "Ltoothpick/config/Binding<TT;>;"
    iget-object v0, p0, Ltoothpick/config/Binding;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getProviderClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Ljavax/inject/Provider",
            "<+TT;>;>;"
        }
    .end annotation

    .prologue
    .line 92
    .local p0, "this":Ltoothpick/config/Binding;, "Ltoothpick/config/Binding<TT;>;"
    iget-object v0, p0, Ltoothpick/config/Binding;->providerClass:Ljava/lang/Class;

    return-object v0
.end method

.method public getProviderInstance()Ljavax/inject/Provider;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljavax/inject/Provider",
            "<+TT;>;"
        }
    .end annotation

    .prologue
    .line 88
    .local p0, "this":Ltoothpick/config/Binding;, "Ltoothpick/config/Binding<TT;>;"
    iget-object v0, p0, Ltoothpick/config/Binding;->providerInstance:Ljavax/inject/Provider;

    return-object v0
.end method

.method public instancesInScope()V
    .locals 1

    .prologue
    .line 40
    .local p0, "this":Ltoothpick/config/Binding;, "Ltoothpick/config/Binding<TT;>;"
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltoothpick/config/Binding;->isCreatingInstancesInScope:Z

    .line 41
    return-void
.end method

.method public isCreatingInstancesInScope()Z
    .locals 1

    .prologue
    .line 100
    .local p0, "this":Ltoothpick/config/Binding;, "Ltoothpick/config/Binding<TT;>;"
    iget-boolean v0, p0, Ltoothpick/config/Binding;->isCreatingInstancesInScope:Z

    return v0
.end method

.method public isCreatingSingletonInScope()Z
    .locals 1

    .prologue
    .line 104
    .local p0, "this":Ltoothpick/config/Binding;, "Ltoothpick/config/Binding<TT;>;"
    iget-boolean v0, p0, Ltoothpick/config/Binding;->isCreatingSingletonInScope:Z

    return v0
.end method

.method public isProvidingSingletonInScope()Z
    .locals 1

    .prologue
    .line 108
    .local p0, "this":Ltoothpick/config/Binding;, "Ltoothpick/config/Binding<TT;>;"
    iget-boolean v0, p0, Ltoothpick/config/Binding;->isProvidingSingletonInScope:Z

    return v0
.end method

.method public singletonInScope()V
    .locals 1

    .prologue
    .local p0, "this":Ltoothpick/config/Binding;, "Ltoothpick/config/Binding<TT;>;"
    const/4 v0, 0x1

    .line 44
    iput-boolean v0, p0, Ltoothpick/config/Binding;->isCreatingInstancesInScope:Z

    .line 45
    iput-boolean v0, p0, Ltoothpick/config/Binding;->isCreatingSingletonInScope:Z

    .line 46
    return-void
.end method

.method public to(Ljava/lang/Class;)Ltoothpick/config/Binding$BoundStateForClassBinding;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+TT;>;)",
            "Ltoothpick/config/Binding",
            "<TT;>.BoundStateForClassBinding;"
        }
    .end annotation

    .prologue
    .line 49
    .local p0, "this":Ltoothpick/config/Binding;, "Ltoothpick/config/Binding<TT;>;"
    .local p1, "implClass":Ljava/lang/Class;, "Ljava/lang/Class<+TT;>;"
    iput-object p1, p0, Ltoothpick/config/Binding;->implementationClass:Ljava/lang/Class;

    .line 50
    sget-object v0, Ltoothpick/config/Binding$Mode;->CLASS:Ltoothpick/config/Binding$Mode;

    iput-object v0, p0, Ltoothpick/config/Binding;->mode:Ltoothpick/config/Binding$Mode;

    .line 51
    new-instance v0, Ltoothpick/config/Binding$BoundStateForClassBinding;

    invoke-direct {v0, p0}, Ltoothpick/config/Binding$BoundStateForClassBinding;-><init>(Ltoothpick/config/Binding;)V

    return-object v0
.end method

.method public toInstance(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p0, "this":Ltoothpick/config/Binding;, "Ltoothpick/config/Binding<TT;>;"
    .local p1, "instance":Ljava/lang/Object;, "TT;"
    iput-object p1, p0, Ltoothpick/config/Binding;->instance:Ljava/lang/Object;

    .line 56
    sget-object v0, Ltoothpick/config/Binding$Mode;->INSTANCE:Ltoothpick/config/Binding$Mode;

    iput-object v0, p0, Ltoothpick/config/Binding;->mode:Ltoothpick/config/Binding$Mode;

    .line 57
    return-void
.end method

.method public toProvider(Ljava/lang/Class;)Ltoothpick/config/Binding$BoundStateForProviderClassBinding;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljavax/inject/Provider",
            "<+TT;>;>;)",
            "Ltoothpick/config/Binding",
            "<TT;>.BoundStateForProviderClassBinding;"
        }
    .end annotation

    .prologue
    .line 60
    .local p0, "this":Ltoothpick/config/Binding;, "Ltoothpick/config/Binding<TT;>;"
    .local p1, "providerClass":Ljava/lang/Class;, "Ljava/lang/Class<+Ljavax/inject/Provider<+TT;>;>;"
    iput-object p1, p0, Ltoothpick/config/Binding;->providerClass:Ljava/lang/Class;

    .line 61
    sget-object v0, Ltoothpick/config/Binding$Mode;->PROVIDER_CLASS:Ltoothpick/config/Binding$Mode;

    iput-object v0, p0, Ltoothpick/config/Binding;->mode:Ltoothpick/config/Binding$Mode;

    .line 62
    new-instance v0, Ltoothpick/config/Binding$BoundStateForProviderClassBinding;

    invoke-direct {v0, p0}, Ltoothpick/config/Binding$BoundStateForProviderClassBinding;-><init>(Ltoothpick/config/Binding;)V

    return-object v0
.end method

.method public toProviderInstance(Ljavax/inject/Provider;)Ltoothpick/config/Binding$BoundStateForProviderInstanceBinding;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<+TT;>;)",
            "Ltoothpick/config/Binding",
            "<TT;>.BoundStateForProviderInstanceBinding;"
        }
    .end annotation

    .prologue
    .line 66
    .local p0, "this":Ltoothpick/config/Binding;, "Ltoothpick/config/Binding<TT;>;"
    .local p1, "providerInstance":Ljavax/inject/Provider;, "Ljavax/inject/Provider<+TT;>;"
    iput-object p1, p0, Ltoothpick/config/Binding;->providerInstance:Ljavax/inject/Provider;

    .line 67
    sget-object v0, Ltoothpick/config/Binding$Mode;->PROVIDER_INSTANCE:Ltoothpick/config/Binding$Mode;

    iput-object v0, p0, Ltoothpick/config/Binding;->mode:Ltoothpick/config/Binding$Mode;

    .line 68
    new-instance v0, Ltoothpick/config/Binding$BoundStateForProviderInstanceBinding;

    invoke-direct {v0, p0}, Ltoothpick/config/Binding$BoundStateForProviderInstanceBinding;-><init>(Ltoothpick/config/Binding;)V

    return-object v0
.end method

.method public withName(Ljava/lang/Class;)Ltoothpick/config/Binding;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "Ljava/lang/annotation/Annotation;",
            ">(",
            "Ljava/lang/Class",
            "<TA;>;)",
            "Ltoothpick/config/Binding",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 30
    .local p0, "this":Ltoothpick/config/Binding;, "Ltoothpick/config/Binding<TT;>;"
    .local p1, "annotationClassWithQualifierAnnotation":Ljava/lang/Class;, "Ljava/lang/Class<TA;>;"
    const-class v0, Ljavax/inject/Qualifier;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only qualifier annotation annotations can be used to define a binding name. Add @Qualifier to %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    .line 32
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltoothpick/config/Binding;->name:Ljava/lang/String;

    .line 36
    return-object p0
.end method

.method public withName(Ljava/lang/String;)Ltoothpick/config/Binding;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ltoothpick/config/Binding",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 25
    .local p0, "this":Ltoothpick/config/Binding;, "Ltoothpick/config/Binding<TT;>;"
    iput-object p1, p0, Ltoothpick/config/Binding;->name:Ljava/lang/String;

    .line 26
    return-object p0
.end method
