.class public Lcom/my/target/ck;
.super Ljava/lang/Object;
.source "Repeater.java"


# static fields
.field private static final handler:Landroid/os/Handler;

.field public static final km:Lcom/my/target/ck;


# instance fields
.field private final kn:I

.field private final ko:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/Runnable;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final kp:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lcom/my/target/ck;

    const/16 v1, 0x3e8

    invoke-direct {v0, v1}, Lcom/my/target/ck;-><init>(I)V

    sput-object v0, Lcom/my/target/ck;->km:Lcom/my/target/ck;

    .line 27
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/my/target/ck;->handler:Landroid/os/Handler;

    return-void
.end method

.method private constructor <init>(I)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/my/target/ck;->ko:Ljava/util/WeakHashMap;

    .line 31
    new-instance v0, Lcom/my/target/ck$1;

    invoke-direct {v0, p0}, Lcom/my/target/ck$1;-><init>(Lcom/my/target/ck;)V

    iput-object v0, p0, Lcom/my/target/ck;->kp:Ljava/lang/Runnable;

    .line 41
    iput p1, p0, Lcom/my/target/ck;->kn:I

    .line 42
    return-void
.end method

.method static synthetic a(Lcom/my/target/ck;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/my/target/ck;->bC()V

    return-void
.end method

.method private bC()V
    .locals 2

    .prologue
    .line 72
    monitor-enter p0

    .line 74
    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/my/target/ck;->ko:Ljava/util/WeakHashMap;

    invoke-virtual {v1}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 75
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 77
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 83
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 79
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/my/target/ck;->ko:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 81
    invoke-direct {p0}, Lcom/my/target/ck;->bD()V

    .line 83
    :cond_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84
    return-void
.end method

.method private bD()V
    .locals 4

    .prologue
    .line 88
    sget-object v0, Lcom/my/target/ck;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/my/target/ck;->kp:Ljava/lang/Runnable;

    iget v2, p0, Lcom/my/target/ck;->kn:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 89
    return-void
.end method

.method public static final k(I)Lcom/my/target/ck;
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/my/target/ck;

    invoke-direct {v0, p0}, Lcom/my/target/ck;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method public d(Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 47
    monitor-enter p0

    .line 49
    :try_start_0
    iget-object v0, p0, Lcom/my/target/ck;->ko:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->size()I

    move-result v0

    .line 50
    iget-object v1, p0, Lcom/my/target/ck;->ko:Ljava/util/WeakHashMap;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    .line 52
    invoke-direct {p0}, Lcom/my/target/ck;->bD()V

    .line 54
    :cond_0
    monitor-exit p0

    .line 55
    return-void

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public e(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 60
    monitor-enter p0

    .line 62
    :try_start_0
    iget-object v0, p0, Lcom/my/target/ck;->ko:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    iget-object v0, p0, Lcom/my/target/ck;->ko:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 65
    sget-object v0, Lcom/my/target/ck;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/my/target/ck;->kp:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 67
    :cond_0
    monitor-exit p0

    .line 68
    return-void

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
