.class final Lru/cn/player/exoplayer/DetailObserver;
.super Ljava/lang/Object;
.source "DetailObserver.java"

# interfaces
.implements Lcom/google/android/exoplayer2/audio/AudioRendererEventListener;
.implements Lcom/google/android/exoplayer2/video/VideoRendererEventListener;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private audioCodec:Ljava/lang/String;

.field private audioDecoderCounters:Lcom/google/android/exoplayer2/decoder/DecoderCounters;

.field private audioFormat:Lcom/google/android/exoplayer2/Format;

.field private needsCheckDiscontinuity:Z

.field private playerRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/exoplayer2/SimpleExoPlayer;",
            ">;"
        }
    .end annotation
.end field

.field private qualityListener:Lru/cn/player/ChangeQualityListener;

.field private successfulSequence:I

.field private videoCodec:Ljava/lang/String;

.field private videoDecoderCounters:Lcom/google/android/exoplayer2/decoder/DecoderCounters;

.field private videoFormat:Lcom/google/android/exoplayer2/Format;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lru/cn/player/exoplayer/DetailObserver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lru/cn/player/exoplayer/DetailObserver;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/android/exoplayer2/SimpleExoPlayer;)V
    .locals 1
    .param p1, "player"    # Lcom/google/android/exoplayer2/SimpleExoPlayer;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/player/exoplayer/DetailObserver;->needsCheckDiscontinuity:Z

    .line 44
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lru/cn/player/exoplayer/DetailObserver;->playerRef:Ljava/lang/ref/WeakReference;

    .line 46
    invoke-virtual {p1, p0}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->addAudioDebugListener(Lcom/google/android/exoplayer2/audio/AudioRendererEventListener;)V

    .line 47
    invoke-virtual {p1, p0}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->addVideoDebugListener(Lcom/google/android/exoplayer2/video/VideoRendererEventListener;)V

    .line 48
    return-void
.end method

.method private getSelectedFormat(I)Lcom/google/android/exoplayer2/Format;
    .locals 6
    .param p1, "trackType"    # I

    .prologue
    const/4 v4, 0x0

    .line 189
    iget-object v5, p0, Lru/cn/player/exoplayer/DetailObserver;->playerRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/SimpleExoPlayer;

    .line 190
    .local v1, "player":Lcom/google/android/exoplayer2/SimpleExoPlayer;
    if-nez v1, :cond_1

    .line 201
    :cond_0
    :goto_0
    return-object v4

    .line 193
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->getCurrentTrackSelections()Lcom/google/android/exoplayer2/trackselection/TrackSelectionArray;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/exoplayer2/trackselection/TrackSelectionArray;->getAll()[Lcom/google/android/exoplayer2/trackselection/TrackSelection;

    move-result-object v3

    .line 194
    .local v3, "selections":[Lcom/google/android/exoplayer2/trackselection/TrackSelection;
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_1
    array-length v5, v3

    if-ge v0, v5, :cond_0

    .line 195
    aget-object v2, v3, v0

    .line 196
    .local v2, "selection":Lcom/google/android/exoplayer2/trackselection/TrackSelection;
    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->getRendererType(I)I

    move-result v5

    if-ne v5, p1, :cond_2

    .line 197
    invoke-interface {v2}, Lcom/google/android/exoplayer2/trackselection/TrackSelection;->getSelectedFormat()Lcom/google/android/exoplayer2/Format;

    move-result-object v4

    goto :goto_0

    .line 194
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private serializedCounters(Lcom/google/android/exoplayer2/decoder/DecoderCounters;)Ljava/lang/String;
    .locals 4
    .param p1, "counters"    # Lcom/google/android/exoplayer2/decoder/DecoderCounters;

    .prologue
    .line 137
    const-string v0, ","

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    iget v3, p1, Lcom/google/android/exoplayer2/decoder/DecoderCounters;->inputBufferCount:I

    .line 138
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p1, Lcom/google/android/exoplayer2/decoder/DecoderCounters;->droppedBufferCount:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p1, Lcom/google/android/exoplayer2/decoder/DecoderCounters;->skippedOutputBufferCount:I

    .line 139
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p1, Lcom/google/android/exoplayer2/decoder/DecoderCounters;->renderedOutputBufferCount:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget v3, p1, Lcom/google/android/exoplayer2/decoder/DecoderCounters;->maxConsecutiveDroppedBufferCount:I

    .line 140
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 138
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 137
    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method checkPlaybackDiscontinuities(Landroid/net/Uri;)V
    .locals 9
    .param p1, "contentUri"    # Landroid/net/Uri;

    .prologue
    const v8, 0x3d4ccccd    # 0.05f

    const/4 v7, 0x0

    .line 85
    iget-object v5, p0, Lru/cn/player/exoplayer/DetailObserver;->videoDecoderCounters:Lcom/google/android/exoplayer2/decoder/DecoderCounters;

    if-nez v5, :cond_1

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    iget-object v5, p0, Lru/cn/player/exoplayer/DetailObserver;->audioDecoderCounters:Lcom/google/android/exoplayer2/decoder/DecoderCounters;

    if-eqz v5, :cond_0

    .line 91
    iget-boolean v5, p0, Lru/cn/player/exoplayer/DetailObserver;->needsCheckDiscontinuity:Z

    if-eqz v5, :cond_0

    .line 94
    iput-boolean v7, p0, Lru/cn/player/exoplayer/DetailObserver;->needsCheckDiscontinuity:Z

    .line 96
    iget-object v5, p0, Lru/cn/player/exoplayer/DetailObserver;->videoDecoderCounters:Lcom/google/android/exoplayer2/decoder/DecoderCounters;

    iget v5, v5, Lcom/google/android/exoplayer2/decoder/DecoderCounters;->inputBufferCount:I

    const/16 v6, 0x14

    if-lt v5, v6, :cond_0

    .line 99
    iget-object v5, p0, Lru/cn/player/exoplayer/DetailObserver;->videoDecoderCounters:Lcom/google/android/exoplayer2/decoder/DecoderCounters;

    iget v5, v5, Lcom/google/android/exoplayer2/decoder/DecoderCounters;->droppedBufferCount:I

    int-to-float v5, v5

    iget-object v6, p0, Lru/cn/player/exoplayer/DetailObserver;->videoDecoderCounters:Lcom/google/android/exoplayer2/decoder/DecoderCounters;

    iget v6, v6, Lcom/google/android/exoplayer2/decoder/DecoderCounters;->inputBufferCount:I

    int-to-float v6, v6

    div-float v0, v5, v6

    .line 100
    .local v0, "droppedVideoPct":F
    iget-object v5, p0, Lru/cn/player/exoplayer/DetailObserver;->videoDecoderCounters:Lcom/google/android/exoplayer2/decoder/DecoderCounters;

    iget v5, v5, Lcom/google/android/exoplayer2/decoder/DecoderCounters;->renderedOutputBufferCount:I

    int-to-float v5, v5

    iget-object v6, p0, Lru/cn/player/exoplayer/DetailObserver;->videoDecoderCounters:Lcom/google/android/exoplayer2/decoder/DecoderCounters;

    iget v6, v6, Lcom/google/android/exoplayer2/decoder/DecoderCounters;->inputBufferCount:I

    int-to-float v6, v6

    div-float v4, v5, v6

    .line 101
    .local v4, "renderedVideoPct":F
    iget-object v5, p0, Lru/cn/player/exoplayer/DetailObserver;->audioDecoderCounters:Lcom/google/android/exoplayer2/decoder/DecoderCounters;

    iget v5, v5, Lcom/google/android/exoplayer2/decoder/DecoderCounters;->renderedOutputBufferCount:I

    int-to-float v5, v5

    iget-object v6, p0, Lru/cn/player/exoplayer/DetailObserver;->audioDecoderCounters:Lcom/google/android/exoplayer2/decoder/DecoderCounters;

    iget v6, v6, Lcom/google/android/exoplayer2/decoder/DecoderCounters;->inputBufferCount:I

    int-to-float v6, v6

    div-float v3, v5, v6

    .line 103
    .local v3, "renderedAudioPct":F
    cmpl-float v5, v0, v8

    if-gtz v5, :cond_2

    sub-float v5, v3, v4

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpl-float v5, v5, v8

    if-lez v5, :cond_7

    .line 104
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "video="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lru/cn/player/exoplayer/DetailObserver;->videoDecoderCounters:Lcom/google/android/exoplayer2/decoder/DecoderCounters;

    invoke-direct {p0, v6}, Lru/cn/player/exoplayer/DetailObserver;->serializedCounters(Lcom/google/android/exoplayer2/decoder/DecoderCounters;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 105
    .local v1, "message":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ";audio="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lru/cn/player/exoplayer/DetailObserver;->audioDecoderCounters:Lcom/google/android/exoplayer2/decoder/DecoderCounters;

    invoke-direct {p0, v6}, Lru/cn/player/exoplayer/DetailObserver;->serializedCounters(Lcom/google/android/exoplayer2/decoder/DecoderCounters;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 107
    const-string v2, ""

    .line 108
    .local v2, "mime":Ljava/lang/String;
    iget-object v5, p0, Lru/cn/player/exoplayer/DetailObserver;->videoFormat:Lcom/google/android/exoplayer2/Format;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lru/cn/player/exoplayer/DetailObserver;->videoFormat:Lcom/google/android/exoplayer2/Format;

    iget-object v5, v5, Lcom/google/android/exoplayer2/Format;->sampleMimeType:Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 109
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lru/cn/player/exoplayer/DetailObserver;->videoFormat:Lcom/google/android/exoplayer2/Format;

    iget-object v6, v6, Lcom/google/android/exoplayer2/Format;->sampleMimeType:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 112
    :cond_3
    iget-object v5, p0, Lru/cn/player/exoplayer/DetailObserver;->audioFormat:Lcom/google/android/exoplayer2/Format;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lru/cn/player/exoplayer/DetailObserver;->audioFormat:Lcom/google/android/exoplayer2/Format;

    iget-object v5, v5, Lcom/google/android/exoplayer2/Format;->sampleMimeType:Ljava/lang/String;

    if-eqz v5, :cond_5

    .line 113
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_4

    .line 114
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 116
    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lru/cn/player/exoplayer/DetailObserver;->audioFormat:Lcom/google/android/exoplayer2/Format;

    iget-object v6, v6, Lcom/google/android/exoplayer2/Format;->sampleMimeType:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 119
    :cond_5
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_6

    .line 120
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ";mime="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 123
    :cond_6
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ";decoders="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lru/cn/player/exoplayer/DetailObserver;->videoCodec:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lru/cn/player/exoplayer/DetailObserver;->audioCodec:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 124
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ";seq="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lru/cn/player/exoplayer/DetailObserver;->successfulSequence:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 125
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ";uri="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 127
    sget-object v5, Lru/cn/domain/statistics/inetra/ErrorCode;->UNKNOWN_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v6, "Log_FrameDrops"

    invoke-static {v5, v6, v7, v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    .line 130
    iput v7, p0, Lru/cn/player/exoplayer/DetailObserver;->successfulSequence:I

    goto/16 :goto_0

    .line 132
    .end local v1    # "message":Ljava/lang/String;
    .end local v2    # "mime":Ljava/lang/String;
    :cond_7
    iget v5, p0, Lru/cn/player/exoplayer/DetailObserver;->successfulSequence:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lru/cn/player/exoplayer/DetailObserver;->successfulSequence:I

    goto/16 :goto_0
.end method

.method getCodecs()Ljava/lang/String;
    .locals 2

    .prologue
    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 56
    .local v0, "codecs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v1, p0, Lru/cn/player/exoplayer/DetailObserver;->videoCodec:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 57
    iget-object v1, p0, Lru/cn/player/exoplayer/DetailObserver;->videoCodec:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 60
    :cond_0
    iget-object v1, p0, Lru/cn/player/exoplayer/DetailObserver;->audioCodec:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 61
    iget-object v1, p0, Lru/cn/player/exoplayer/DetailObserver;->audioCodec:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 65
    const/4 v1, 0x0

    .line 68
    :goto_0
    return-object v1

    :cond_2
    const-string v1, ","

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public onAudioDecoderInitialized(Ljava/lang/String;JJ)V
    .locals 3
    .param p1, "decoderName"    # Ljava/lang/String;
    .param p2, "elapsedRealtimeMs"    # J
    .param p4, "initializationDurationMs"    # J

    .prologue
    .line 146
    sget-object v0, Lru/cn/player/exoplayer/DetailObserver;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Init audio decoder "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lru/cn/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    iput-object p1, p0, Lru/cn/player/exoplayer/DetailObserver;->audioCodec:Ljava/lang/String;

    .line 150
    return-void
.end method

.method public onAudioDisabled(Lcom/google/android/exoplayer2/decoder/DecoderCounters;)V
    .locals 1
    .param p1, "decoderCounters"    # Lcom/google/android/exoplayer2/decoder/DecoderCounters;

    .prologue
    const/4 v0, 0x0

    .line 206
    iput-object v0, p0, Lru/cn/player/exoplayer/DetailObserver;->audioFormat:Lcom/google/android/exoplayer2/Format;

    .line 207
    iput-object v0, p0, Lru/cn/player/exoplayer/DetailObserver;->audioDecoderCounters:Lcom/google/android/exoplayer2/decoder/DecoderCounters;

    .line 208
    return-void
.end method

.method public onAudioEnabled(Lcom/google/android/exoplayer2/decoder/DecoderCounters;)V
    .locals 0
    .param p1, "decoderCounters"    # Lcom/google/android/exoplayer2/decoder/DecoderCounters;

    .prologue
    .line 223
    iput-object p1, p0, Lru/cn/player/exoplayer/DetailObserver;->audioDecoderCounters:Lcom/google/android/exoplayer2/decoder/DecoderCounters;

    .line 224
    return-void
.end method

.method public onAudioInputFormatChanged(Lcom/google/android/exoplayer2/Format;)V
    .locals 3
    .param p1, "format"    # Lcom/google/android/exoplayer2/Format;

    .prologue
    .line 175
    iget-object v2, p0, Lru/cn/player/exoplayer/DetailObserver;->audioFormat:Lcom/google/android/exoplayer2/Format;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lru/cn/player/exoplayer/DetailObserver;->audioFormat:Lcom/google/android/exoplayer2/Format;

    invoke-virtual {v2, p1}, Lcom/google/android/exoplayer2/Format;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 176
    iget-object v2, p0, Lru/cn/player/exoplayer/DetailObserver;->qualityListener:Lru/cn/player/ChangeQualityListener;

    if-eqz v2, :cond_0

    .line 178
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lru/cn/player/exoplayer/DetailObserver;->getSelectedFormat(I)Lcom/google/android/exoplayer2/Format;

    move-result-object v1

    .line 179
    .local v1, "selectedFormat":Lcom/google/android/exoplayer2/Format;
    if-nez v1, :cond_1

    const/4 v0, 0x0

    .line 181
    .local v0, "bitrate":I
    :goto_0
    iget-object v2, p0, Lru/cn/player/exoplayer/DetailObserver;->qualityListener:Lru/cn/player/ChangeQualityListener;

    invoke-interface {v2, v0}, Lru/cn/player/ChangeQualityListener;->qualityChanged(I)V

    .line 185
    .end local v0    # "bitrate":I
    .end local v1    # "selectedFormat":Lcom/google/android/exoplayer2/Format;
    :cond_0
    iput-object p1, p0, Lru/cn/player/exoplayer/DetailObserver;->audioFormat:Lcom/google/android/exoplayer2/Format;

    .line 186
    return-void

    .line 179
    .restart local v1    # "selectedFormat":Lcom/google/android/exoplayer2/Format;
    :cond_1
    iget v0, v1, Lcom/google/android/exoplayer2/Format;->bitrate:I

    goto :goto_0
.end method

.method public onAudioSessionId(I)V
    .locals 0
    .param p1, "i"    # I

    .prologue
    .line 229
    return-void
.end method

.method public onAudioSinkUnderrun(IJJ)V
    .locals 0
    .param p1, "i"    # I
    .param p2, "l"    # J
    .param p4, "l1"    # J

    .prologue
    .line 234
    return-void
.end method

.method public onDroppedFrames(IJ)V
    .locals 0
    .param p1, "i"    # I
    .param p2, "l"    # J

    .prologue
    .line 239
    return-void
.end method

.method public onRenderedFirstFrame(Landroid/view/Surface;)V
    .locals 0
    .param p1, "surface"    # Landroid/view/Surface;

    .prologue
    .line 249
    return-void
.end method

.method public onVideoDecoderInitialized(Ljava/lang/String;JJ)V
    .locals 3
    .param p1, "decoderName"    # Ljava/lang/String;
    .param p2, "elapsedRealtimeMs"    # J
    .param p4, "initializationDurationMs"    # J

    .prologue
    .line 155
    sget-object v0, Lru/cn/player/exoplayer/DetailObserver;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Init video decoder "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lru/cn/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    iput-object p1, p0, Lru/cn/player/exoplayer/DetailObserver;->videoCodec:Ljava/lang/String;

    .line 158
    return-void
.end method

.method public onVideoDisabled(Lcom/google/android/exoplayer2/decoder/DecoderCounters;)V
    .locals 1
    .param p1, "decoderCounters"    # Lcom/google/android/exoplayer2/decoder/DecoderCounters;

    .prologue
    const/4 v0, 0x0

    .line 212
    iput-object v0, p0, Lru/cn/player/exoplayer/DetailObserver;->videoFormat:Lcom/google/android/exoplayer2/Format;

    .line 213
    iput-object v0, p0, Lru/cn/player/exoplayer/DetailObserver;->videoDecoderCounters:Lcom/google/android/exoplayer2/decoder/DecoderCounters;

    .line 214
    return-void
.end method

.method public onVideoEnabled(Lcom/google/android/exoplayer2/decoder/DecoderCounters;)V
    .locals 0
    .param p1, "decoderCounters"    # Lcom/google/android/exoplayer2/decoder/DecoderCounters;

    .prologue
    .line 218
    iput-object p1, p0, Lru/cn/player/exoplayer/DetailObserver;->videoDecoderCounters:Lcom/google/android/exoplayer2/decoder/DecoderCounters;

    .line 219
    return-void
.end method

.method public onVideoInputFormatChanged(Lcom/google/android/exoplayer2/Format;)V
    .locals 3
    .param p1, "format"    # Lcom/google/android/exoplayer2/Format;

    .prologue
    .line 162
    iget-object v2, p0, Lru/cn/player/exoplayer/DetailObserver;->videoFormat:Lcom/google/android/exoplayer2/Format;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lru/cn/player/exoplayer/DetailObserver;->videoFormat:Lcom/google/android/exoplayer2/Format;

    invoke-virtual {v2, p1}, Lcom/google/android/exoplayer2/Format;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 163
    iget-object v2, p0, Lru/cn/player/exoplayer/DetailObserver;->qualityListener:Lru/cn/player/ChangeQualityListener;

    if-eqz v2, :cond_0

    .line 164
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lru/cn/player/exoplayer/DetailObserver;->getSelectedFormat(I)Lcom/google/android/exoplayer2/Format;

    move-result-object v1

    .line 165
    .local v1, "selectedFormat":Lcom/google/android/exoplayer2/Format;
    if-nez v1, :cond_1

    const/4 v0, 0x0

    .line 166
    .local v0, "bitrate":I
    :goto_0
    iget-object v2, p0, Lru/cn/player/exoplayer/DetailObserver;->qualityListener:Lru/cn/player/ChangeQualityListener;

    invoke-interface {v2, v0}, Lru/cn/player/ChangeQualityListener;->qualityChanged(I)V

    .line 170
    .end local v0    # "bitrate":I
    .end local v1    # "selectedFormat":Lcom/google/android/exoplayer2/Format;
    :cond_0
    iput-object p1, p0, Lru/cn/player/exoplayer/DetailObserver;->videoFormat:Lcom/google/android/exoplayer2/Format;

    .line 171
    return-void

    .line 165
    .restart local v1    # "selectedFormat":Lcom/google/android/exoplayer2/Format;
    :cond_1
    iget v0, v1, Lcom/google/android/exoplayer2/Format;->bitrate:I

    goto :goto_0
.end method

.method public onVideoSizeChanged(IIIF)V
    .locals 0
    .param p1, "i"    # I
    .param p2, "i1"    # I
    .param p3, "i2"    # I
    .param p4, "v"    # F

    .prologue
    .line 244
    return-void
.end method

.method reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 72
    iput-object v0, p0, Lru/cn/player/exoplayer/DetailObserver;->audioCodec:Ljava/lang/String;

    .line 73
    iput-object v0, p0, Lru/cn/player/exoplayer/DetailObserver;->videoCodec:Ljava/lang/String;

    .line 75
    iput-object v0, p0, Lru/cn/player/exoplayer/DetailObserver;->videoFormat:Lcom/google/android/exoplayer2/Format;

    .line 76
    iput-object v0, p0, Lru/cn/player/exoplayer/DetailObserver;->audioFormat:Lcom/google/android/exoplayer2/Format;

    .line 78
    iput-object v0, p0, Lru/cn/player/exoplayer/DetailObserver;->videoDecoderCounters:Lcom/google/android/exoplayer2/decoder/DecoderCounters;

    .line 79
    iput-object v0, p0, Lru/cn/player/exoplayer/DetailObserver;->audioDecoderCounters:Lcom/google/android/exoplayer2/decoder/DecoderCounters;

    .line 81
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/player/exoplayer/DetailObserver;->needsCheckDiscontinuity:Z

    .line 82
    return-void
.end method

.method setQualityListener(Lru/cn/player/ChangeQualityListener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/player/ChangeQualityListener;

    .prologue
    .line 51
    iput-object p1, p0, Lru/cn/player/exoplayer/DetailObserver;->qualityListener:Lru/cn/player/ChangeQualityListener;

    .line 52
    return-void
.end method
