.class public Lru/cn/tv/player/controller/StbPlayerController;
.super Lru/cn/tv/player/controller/PlayerController;
.source "StbPlayerController.java"


# instance fields
.field private blockedButton:Landroid/widget/ImageButton;

.field private focusableButtons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private isKidsMode:Z

.field private mediaInfoPanel:Lru/cn/tv/player/controller/MediaInfoPanel;

.field private mediaTooltip:Lru/cn/tv/player/controller/MediaTooltipView;

.field private nextMediaPaddingLeft:I

.field private panelAnimator:Lru/cn/tv/player/controller/PanelAnimator;

.field private playerPanel:Landroid/view/View;

.field private playerRelatedPanel:Lru/cn/tv/player/controller/StbPlayerRelatedPanel;

.field private playerSettingsPanel:Lru/cn/tv/player/controller/StbPlayerSettingsPanel;

.field private prevMediaPaddingLeft:I

.field private showNext:Z

.field private showPrev:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lru/cn/tv/player/controller/PlayerController;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    invoke-static {p1}, Lru/cn/domain/KidsObject;->isKidsMode(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lru/cn/tv/player/controller/StbPlayerController;->isKidsMode:Z

    .line 47
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070126

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lru/cn/tv/player/controller/StbPlayerController;->prevMediaPaddingLeft:I

    .line 48
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07011a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lru/cn/tv/player/controller/StbPlayerController;->nextMediaPaddingLeft:I

    .line 49
    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/player/controller/StbPlayerController;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/player/controller/StbPlayerController;

    .prologue
    .line 23
    invoke-direct {p0}, Lru/cn/tv/player/controller/StbPlayerController;->showPrevWrapper()V

    return-void
.end method

.method static synthetic access$102(Lru/cn/tv/player/controller/StbPlayerController;Z)Z
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/player/controller/StbPlayerController;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    iput-boolean p1, p0, Lru/cn/tv/player/controller/StbPlayerController;->showPrev:Z

    return p1
.end method

.method static synthetic access$200(Lru/cn/tv/player/controller/StbPlayerController;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/player/controller/StbPlayerController;

    .prologue
    .line 23
    invoke-direct {p0}, Lru/cn/tv/player/controller/StbPlayerController;->hideMediaTooltip()V

    return-void
.end method

.method static synthetic access$300(Lru/cn/tv/player/controller/StbPlayerController;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/player/controller/StbPlayerController;

    .prologue
    .line 23
    invoke-direct {p0}, Lru/cn/tv/player/controller/StbPlayerController;->showNextWrapper()V

    return-void
.end method

.method static synthetic access$402(Lru/cn/tv/player/controller/StbPlayerController;Z)Z
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/player/controller/StbPlayerController;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    iput-boolean p1, p0, Lru/cn/tv/player/controller/StbPlayerController;->showNext:Z

    return p1
.end method

.method private handleKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 171
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v5

    if-nez v5, :cond_1

    move v0, v4

    .line 172
    .local v0, "down":Z
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v5

    if-nez v5, :cond_2

    move v2, v4

    .line 174
    .local v2, "uniqueDown":Z
    :goto_1
    if-nez v0, :cond_3

    .line 235
    :cond_0
    :goto_2
    return v3

    .end local v0    # "down":Z
    .end local v2    # "uniqueDown":Z
    :cond_1
    move v0, v3

    .line 171
    goto :goto_0

    .restart local v0    # "down":Z
    :cond_2
    move v2, v3

    .line 172
    goto :goto_1

    .line 178
    .restart local v2    # "uniqueDown":Z
    :cond_3
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    .line 179
    .local v1, "keyCode":I
    sparse-switch v1, :sswitch_data_0

    .line 221
    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->isShown()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 222
    packed-switch v1, :pswitch_data_0

    goto :goto_2

    .line 229
    :pswitch_0
    iget-object v5, p0, Lru/cn/tv/player/controller/StbPlayerController;->panelAnimator:Lru/cn/tv/player/controller/PanelAnimator;

    invoke-virtual {v5}, Lru/cn/tv/player/controller/PanelAnimator;->up()Z

    move-result v5

    if-eqz v5, :cond_0

    move v3, v4

    .line 230
    goto :goto_2

    .line 181
    :sswitch_0
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->Ffwd()V

    .line 182
    iget-object v3, p0, Lru/cn/tv/player/controller/StbPlayerController;->buttonFfwd:Landroid/widget/ImageButton;

    if-eqz v3, :cond_4

    .line 183
    iget-object v3, p0, Lru/cn/tv/player/controller/StbPlayerController;->buttonFfwd:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->requestFocus()Z

    :cond_4
    move v3, v4

    .line 185
    goto :goto_2

    .line 188
    :sswitch_1
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->Rew()V

    .line 189
    iget-object v3, p0, Lru/cn/tv/player/controller/StbPlayerController;->buttonRew:Landroid/widget/ImageButton;

    if-eqz v3, :cond_5

    .line 190
    iget-object v3, p0, Lru/cn/tv/player/controller/StbPlayerController;->buttonRew:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->requestFocus()Z

    move-result v3

    goto :goto_2

    :cond_5
    move v3, v4

    .line 192
    goto :goto_2

    .line 195
    :sswitch_2
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->pause()V

    move v3, v4

    .line 196
    goto :goto_2

    .line 199
    :sswitch_3
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->play()V

    move v3, v4

    .line 200
    goto :goto_2

    .line 203
    :sswitch_4
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->doPauseResume()V

    move v3, v4

    .line 204
    goto :goto_2

    .line 207
    :sswitch_5
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->nextMedia()V

    move v3, v4

    .line 208
    goto :goto_2

    .line 211
    :sswitch_6
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->prevMedia()V

    move v3, v4

    .line 212
    goto :goto_2

    .line 215
    :sswitch_7
    if-eqz v2, :cond_6

    .line 216
    iget-object v3, p0, Lru/cn/tv/player/controller/StbPlayerController;->playerSettingsPanel:Lru/cn/tv/player/controller/StbPlayerSettingsPanel;

    invoke-virtual {v3, p1}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    :cond_6
    move v3, v4

    .line 218
    goto :goto_2

    .line 224
    :pswitch_1
    iget-object v5, p0, Lru/cn/tv/player/controller/StbPlayerController;->panelAnimator:Lru/cn/tv/player/controller/PanelAnimator;

    invoke-virtual {v5}, Lru/cn/tv/player/controller/PanelAnimator;->down()Z

    move-result v5

    if-eqz v5, :cond_0

    move v3, v4

    .line 225
    goto :goto_2

    .line 179
    :sswitch_data_0
    .sparse-switch
        0x55 -> :sswitch_4
        0x57 -> :sswitch_5
        0x58 -> :sswitch_6
        0x59 -> :sswitch_1
        0x5a -> :sswitch_0
        0x7e -> :sswitch_3
        0x7f -> :sswitch_2
        0xa8 -> :sswitch_7
    .end sparse-switch

    .line 222
    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private hideMediaTooltip()V
    .locals 2

    .prologue
    .line 330
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerController;->mediaTooltip:Lru/cn/tv/player/controller/MediaTooltipView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lru/cn/tv/player/controller/MediaTooltipView;->setVisibility(I)V

    .line 331
    return-void
.end method

.method private showMediaTooltip(Lru/cn/api/tv/replies/Telecast;Ljava/lang/String;I)V
    .locals 4
    .param p1, "telecast"    # Lru/cn/api/tv/replies/Telecast;
    .param p2, "headline"    # Ljava/lang/String;
    .param p3, "paddingLeft"    # I

    .prologue
    const/4 v3, 0x0

    .line 322
    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerController;->mediaTooltip:Lru/cn/tv/player/controller/MediaTooltipView;

    invoke-virtual {v1, v3}, Lru/cn/tv/player/controller/MediaTooltipView;->setVisibility(I)V

    .line 324
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070125

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 325
    .local v0, "paddingTop":I
    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerController;->mediaTooltip:Lru/cn/tv/player/controller/MediaTooltipView;

    invoke-virtual {v1, p3, v0, v3, v3}, Lru/cn/tv/player/controller/MediaTooltipView;->setPadding(IIII)V

    .line 326
    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerController;->mediaTooltip:Lru/cn/tv/player/controller/MediaTooltipView;

    invoke-virtual {v1, p2, p1}, Lru/cn/tv/player/controller/MediaTooltipView;->configure(Ljava/lang/String;Lru/cn/api/tv/replies/Telecast;)V

    .line 327
    return-void
.end method

.method private showNextWrapper()V
    .locals 3

    .prologue
    .line 289
    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerController;->buttonNext:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerController;->buttonNext:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->isHovered()Z

    move-result v1

    if-nez v1, :cond_1

    .line 301
    :cond_0
    :goto_0
    return-void

    .line 293
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lru/cn/tv/player/controller/StbPlayerController;->showNext:Z

    .line 294
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->canGoLive()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 295
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e00eb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 296
    .local v0, "headline":Ljava/lang/String;
    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerController;->currentTelecast:Lru/cn/api/tv/replies/Telecast;

    iget v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->nextMediaPaddingLeft:I

    invoke-direct {p0, v1, v0, v2}, Lru/cn/tv/player/controller/StbPlayerController;->showMediaTooltip(Lru/cn/api/tv/replies/Telecast;Ljava/lang/String;I)V

    goto :goto_0

    .line 297
    .end local v0    # "headline":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->hasNextMedia()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 298
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e00e5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 299
    .restart local v0    # "headline":Ljava/lang/String;
    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerController;->nextTelecast:Lru/cn/api/tv/replies/Telecast;

    iget v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->nextMediaPaddingLeft:I

    invoke-direct {p0, v1, v0, v2}, Lru/cn/tv/player/controller/StbPlayerController;->showMediaTooltip(Lru/cn/api/tv/replies/Telecast;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private showPrevWrapper()V
    .locals 3

    .prologue
    const v2, 0x7f0e0142

    .line 304
    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerController;->buttonPrev:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerController;->buttonPrev:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->isHovered()Z

    move-result v1

    if-nez v1, :cond_1

    .line 319
    :cond_0
    :goto_0
    return-void

    .line 308
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lru/cn/tv/player/controller/StbPlayerController;->showPrev:Z

    .line 309
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->canStartOver()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 310
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 311
    .local v0, "headline":Ljava/lang/String;
    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerController;->currentTelecast:Lru/cn/api/tv/replies/Telecast;

    iget v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->prevMediaPaddingLeft:I

    invoke-direct {p0, v1, v0, v2}, Lru/cn/tv/player/controller/StbPlayerController;->showMediaTooltip(Lru/cn/api/tv/replies/Telecast;Ljava/lang/String;I)V

    goto :goto_0

    .line 312
    .end local v0    # "headline":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->hasPreviousMedia()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 313
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0112

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 314
    .restart local v0    # "headline":Ljava/lang/String;
    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerController;->prevTelecast:Lru/cn/api/tv/replies/Telecast;

    iget v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->prevMediaPaddingLeft:I

    invoke-direct {p0, v1, v0, v2}, Lru/cn/tv/player/controller/StbPlayerController;->showMediaTooltip(Lru/cn/api/tv/replies/Telecast;Ljava/lang/String;I)V

    goto :goto_0

    .line 315
    .end local v0    # "headline":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->seekable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 316
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 317
    .restart local v0    # "headline":Ljava/lang/String;
    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerController;->currentTelecast:Lru/cn/api/tv/replies/Telecast;

    iget v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->prevMediaPaddingLeft:I

    invoke-direct {p0, v1, v0, v2}, Lru/cn/tv/player/controller/StbPlayerController;->showMediaTooltip(Lru/cn/api/tv/replies/Telecast;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private updateFocusNavigation()V
    .locals 8

    .prologue
    .line 360
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 361
    .local v0, "buttons":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget-object v6, p0, Lru/cn/tv/player/controller/StbPlayerController;->focusableButtons:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    .line 362
    .local v5, "view":Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->isEnabled()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v5}, Landroid/view/View;->isShown()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 363
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 367
    .end local v5    # "view":Landroid/view/View;
    :cond_1
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    if-ge v2, v6, :cond_4

    .line 368
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 370
    .local v1, "currentButton":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v3

    .line 371
    .local v3, "leftFocusId":I
    if-lez v2, :cond_2

    .line 372
    add-int/lit8 v6, v2, -0x1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getId()I

    move-result v3

    .line 375
    :cond_2
    invoke-virtual {v1, v3}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 377
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v4

    .line 378
    .local v4, "rightFocusId":I
    add-int/lit8 v6, v2, 0x1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    if-ge v6, v7, :cond_3

    .line 379
    add-int/lit8 v6, v2, 0x1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getId()I

    move-result v4

    .line 382
    :cond_3
    invoke-virtual {v1, v4}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 367
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 384
    .end local v1    # "currentButton":Landroid/view/View;
    .end local v3    # "leftFocusId":I
    .end local v4    # "rightFocusId":I
    :cond_4
    return-void
.end method


# virtual methods
.method protected dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 152
    invoke-super {p0, p1}, Lru/cn/tv/player/controller/PlayerController;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 153
    .local v0, "handled":Z
    if-eqz v0, :cond_1

    .line 154
    iget-object v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->focusableButtons:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 155
    .local v1, "view":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->isShown()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->isHovered()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 156
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 162
    .end local v1    # "view":Landroid/view/View;
    :cond_1
    return v0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 167
    invoke-super {p0, p1}, Lru/cn/tv/player/controller/PlayerController;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lru/cn/tv/player/controller/StbPlayerController;->handleKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getLayout()I
    .locals 1

    .prologue
    .line 53
    const v0, 0x7f0c0086

    return v0
.end method

.method public hide()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 269
    invoke-super {p0}, Lru/cn/tv/player/controller/PlayerController;->hide()V

    .line 271
    iput-boolean v0, p0, Lru/cn/tv/player/controller/StbPlayerController;->showNext:Z

    .line 272
    iput-boolean v0, p0, Lru/cn/tv/player/controller/StbPlayerController;->showPrev:Z

    .line 273
    invoke-direct {p0}, Lru/cn/tv/player/controller/StbPlayerController;->hideMediaTooltip()V

    .line 274
    return-void
.end method

.method protected onChannelChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "imageURL"    # Ljava/lang/String;

    .prologue
    .line 340
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerController;->mediaInfoPanel:Lru/cn/tv/player/controller/MediaInfoPanel;

    invoke-virtual {v0, p1, p2}, Lru/cn/tv/player/controller/MediaInfoPanel;->setChannel(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    return-void
.end method

.method public onFinishInflate()V
    .locals 11

    .prologue
    const/16 v4, 0x8

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v6, -0x2

    const/4 v3, 0x0

    .line 58
    invoke-super {p0}, Lru/cn/tv/player/controller/PlayerController;->onFinishInflate()V

    .line 60
    new-instance v2, Lru/cn/tv/player/controller/MediaTooltipView;

    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v2, v5}, Lru/cn/tv/player/controller/MediaTooltipView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->mediaTooltip:Lru/cn/tv/player/controller/MediaTooltipView;

    .line 61
    iget-object v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->mediaTooltip:Lru/cn/tv/player/controller/MediaTooltipView;

    invoke-virtual {v2, v4}, Lru/cn/tv/player/controller/MediaTooltipView;->setVisibility(I)V

    .line 62
    iget-object v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->mediaTooltip:Lru/cn/tv/player/controller/MediaTooltipView;

    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v5, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v2, v5}, Lru/cn/tv/player/controller/StbPlayerController;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 65
    const v2, 0x7f09016a

    invoke-virtual {p0, v2}, Lru/cn/tv/player/controller/StbPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lru/cn/tv/player/controller/MediaInfoPanel;

    iput-object v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->mediaInfoPanel:Lru/cn/tv/player/controller/MediaInfoPanel;

    .line 67
    const v2, 0x7f090171

    invoke-virtual {p0, v2}, Lru/cn/tv/player/controller/StbPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;

    iput-object v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->playerSettingsPanel:Lru/cn/tv/player/controller/StbPlayerSettingsPanel;

    .line 68
    const v2, 0x7f090170

    invoke-virtual {p0, v2}, Lru/cn/tv/player/controller/StbPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;

    iput-object v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->playerRelatedPanel:Lru/cn/tv/player/controller/StbPlayerRelatedPanel;

    .line 69
    const v2, 0x7f09016d

    invoke-virtual {p0, v2}, Lru/cn/tv/player/controller/StbPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->playerPanel:Landroid/view/View;

    .line 71
    const v2, 0x7f09016e

    invoke-virtual {p0, v2}, Lru/cn/tv/player/controller/StbPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 72
    .local v0, "panelsTitleView":Landroid/widget/TextView;
    new-instance v2, Lru/cn/tv/player/controller/PanelAnimator;

    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v2, v5, v0}, Lru/cn/tv/player/controller/PanelAnimator;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    iput-object v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->panelAnimator:Lru/cn/tv/player/controller/PanelAnimator;

    .line 74
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->getContext()Landroid/content/Context;

    move-result-object v2

    const v5, 0x7f0e006d

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 75
    .local v1, "settingsTitle":Ljava/lang/String;
    iget-object v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->panelAnimator:Lru/cn/tv/player/controller/PanelAnimator;

    new-array v5, v10, [Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    new-instance v6, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    iget-object v7, p0, Lru/cn/tv/player/controller/StbPlayerController;->playerPanel:Landroid/view/View;

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8}, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;-><init>(Landroid/view/View;Ljava/lang/String;)V

    aput-object v6, v5, v3

    new-instance v6, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    iget-object v7, p0, Lru/cn/tv/player/controller/StbPlayerController;->playerSettingsPanel:Lru/cn/tv/player/controller/StbPlayerSettingsPanel;

    invoke-direct {v6, v7, v1}, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;-><init>(Landroid/view/View;Ljava/lang/String;)V

    aput-object v6, v5, v9

    invoke-virtual {v2, v5}, Lru/cn/tv/player/controller/PanelAnimator;->setPanels([Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;)V

    .line 78
    iget-object v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->buttonPrev:Landroid/widget/ImageButton;

    new-instance v5, Lru/cn/tv/player/controller/StbPlayerController$1;

    invoke-direct {v5, p0}, Lru/cn/tv/player/controller/StbPlayerController$1;-><init>(Lru/cn/tv/player/controller/StbPlayerController;)V

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 90
    iget-object v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->buttonNext:Landroid/widget/ImageButton;

    new-instance v5, Lru/cn/tv/player/controller/StbPlayerController$2;

    invoke-direct {v5, p0}, Lru/cn/tv/player/controller/StbPlayerController$2;-><init>(Lru/cn/tv/player/controller/StbPlayerController;)V

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 102
    iget-object v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->buttonFfwd:Landroid/widget/ImageButton;

    new-instance v5, Lru/cn/tv/player/controller/StbPlayerController$3;

    invoke-direct {v5, p0}, Lru/cn/tv/player/controller/StbPlayerController$3;-><init>(Lru/cn/tv/player/controller/StbPlayerController;)V

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 116
    iget-object v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->buttonRew:Landroid/widget/ImageButton;

    new-instance v5, Lru/cn/tv/player/controller/StbPlayerController$4;

    invoke-direct {v5, p0}, Lru/cn/tv/player/controller/StbPlayerController$4;-><init>(Lru/cn/tv/player/controller/StbPlayerController;)V

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 130
    const v2, 0x7f09003c

    invoke-virtual {p0, v2}, Lru/cn/tv/player/controller/StbPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->blockedButton:Landroid/widget/ImageButton;

    .line 131
    iget-object v5, p0, Lru/cn/tv/player/controller/StbPlayerController;->blockedButton:Landroid/widget/ImageButton;

    iget-boolean v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->isKidsMode:Z

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    invoke-virtual {v5, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 133
    const/4 v2, 0x7

    new-array v2, v2, [Landroid/widget/ImageView;

    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerController;->blockedButton:Landroid/widget/ImageButton;

    aput-object v4, v2, v3

    iget-object v3, p0, Lru/cn/tv/player/controller/StbPlayerController;->favouriteStar:Lru/cn/tv/FavouriteStar;

    aput-object v3, v2, v9

    iget-object v3, p0, Lru/cn/tv/player/controller/StbPlayerController;->buttonPrev:Landroid/widget/ImageButton;

    aput-object v3, v2, v10

    const/4 v3, 0x3

    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerController;->buttonRew:Landroid/widget/ImageButton;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerController;->buttonPlayPause:Landroid/widget/ImageButton;

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerController;->buttonFfwd:Landroid/widget/ImageButton;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerController;->buttonNext:Landroid/widget/ImageButton;

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->focusableButtons:Ljava/util/List;

    .line 135
    return-void

    :cond_0
    move v2, v4

    .line 131
    goto :goto_0
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 240
    invoke-super {p0, p1}, Lru/cn/tv/player/controller/PlayerController;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 241
    .local v0, "handled":Z
    if-eqz v0, :cond_1

    .line 254
    :cond_0
    :goto_0
    return v1

    .line 244
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 245
    const/16 v2, 0x9

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v2

    float-to-double v2, v2

    const-wide/16 v4, 0x0

    cmpg-double v2, v2, v4

    if-gez v2, :cond_2

    .line 246
    iget-object v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->panelAnimator:Lru/cn/tv/player/controller/PanelAnimator;

    invoke-virtual {v2}, Lru/cn/tv/player/controller/PanelAnimator;->down()Z

    goto :goto_0

    .line 249
    :cond_2
    iget-object v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->panelAnimator:Lru/cn/tv/player/controller/PanelAnimator;

    invoke-virtual {v2}, Lru/cn/tv/player/controller/PanelAnimator;->up()Z

    move-result v2

    if-nez v2, :cond_0

    .line 254
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onTelecastChanged(Lru/cn/api/tv/replies/Telecast;)V
    .locals 1
    .param p1, "telecast"    # Lru/cn/api/tv/replies/Telecast;

    .prologue
    .line 335
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerController;->mediaInfoPanel:Lru/cn/tv/player/controller/MediaInfoPanel;

    invoke-virtual {v0, p1}, Lru/cn/tv/player/controller/MediaInfoPanel;->setTelecast(Lru/cn/api/tv/replies/Telecast;)V

    .line 336
    return-void
.end method

.method public prevMedia()V
    .locals 4

    .prologue
    .line 278
    invoke-super {p0}, Lru/cn/tv/player/controller/PlayerController;->prevMedia()V

    .line 280
    new-instance v0, Lru/cn/tv/player/controller/StbPlayerController$5;

    invoke-direct {v0, p0}, Lru/cn/tv/player/controller/StbPlayerController$5;-><init>(Lru/cn/tv/player/controller/StbPlayerController;)V

    const-wide/16 v2, 0x258

    invoke-virtual {p0, v0, v2, v3}, Lru/cn/tv/player/controller/StbPlayerController;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 286
    return-void
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .locals 1
    .param p1, "direction"    # I
    .param p2, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 139
    iget-boolean v0, p0, Lru/cn/tv/player/controller/StbPlayerController;->showPrev:Z

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerController;->buttonPrev:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    move-result v0

    .line 147
    :goto_0
    return v0

    .line 143
    :cond_0
    iget-boolean v0, p0, Lru/cn/tv/player/controller/StbPlayerController;->showNext:Z

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerController;->buttonNext:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    move-result v0

    goto :goto_0

    .line 147
    :cond_1
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerController;->buttonPlayPause:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    move-result v0

    goto :goto_0
.end method

.method public setBlockedContentListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "blockedContentListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 429
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerController;->blockedButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 430
    return-void
.end method

.method public setFitMode(Lru/cn/player/SimplePlayer$FitMode;)V
    .locals 1
    .param p1, "mode"    # Lru/cn/player/SimplePlayer$FitMode;

    .prologue
    .line 421
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerController;->playerSettingsPanel:Lru/cn/tv/player/controller/StbPlayerSettingsPanel;

    invoke-virtual {v0, p1}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->setFitMode(Lru/cn/player/SimplePlayer$FitMode;)V

    .line 422
    return-void
.end method

.method public setRelatedItems(JLjava/lang/String;Landroid/database/Cursor;)V
    .locals 9
    .param p1, "rubricId"    # J
    .param p3, "relatedItemsTitle"    # Ljava/lang/String;
    .param p4, "relatedItems"    # Landroid/database/Cursor;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 395
    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerController;->playerRelatedPanel:Lru/cn/tv/player/controller/StbPlayerRelatedPanel;

    invoke-virtual {v1, p1, p2, p4}, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;->setRelatedItems(JLandroid/database/Cursor;)V

    .line 397
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e006d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 398
    .local v0, "settingsTitle":Ljava/lang/String;
    if-nez p4, :cond_1

    .line 399
    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerController;->panelAnimator:Lru/cn/tv/player/controller/PanelAnimator;

    new-array v2, v7, [Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    new-instance v3, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerController;->playerPanel:Landroid/view/View;

    invoke-direct {v3, v4, v8}, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;-><init>(Landroid/view/View;Ljava/lang/String;)V

    aput-object v3, v2, v5

    new-instance v3, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerController;->playerSettingsPanel:Lru/cn/tv/player/controller/StbPlayerSettingsPanel;

    invoke-direct {v3, v4, v0}, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;-><init>(Landroid/view/View;Ljava/lang/String;)V

    aput-object v3, v2, v6

    invoke-virtual {v1, v2}, Lru/cn/tv/player/controller/PanelAnimator;->setPanels([Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;)V

    .line 407
    :goto_0
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->isShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 408
    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerController;->panelAnimator:Lru/cn/tv/player/controller/PanelAnimator;

    invoke-virtual {v1}, Lru/cn/tv/player/controller/PanelAnimator;->reset()V

    .line 410
    :cond_0
    return-void

    .line 402
    :cond_1
    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerController;->panelAnimator:Lru/cn/tv/player/controller/PanelAnimator;

    const/4 v2, 0x3

    new-array v2, v2, [Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    new-instance v3, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerController;->playerPanel:Landroid/view/View;

    invoke-direct {v3, v4, v8}, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;-><init>(Landroid/view/View;Ljava/lang/String;)V

    aput-object v3, v2, v5

    new-instance v3, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerController;->playerRelatedPanel:Lru/cn/tv/player/controller/StbPlayerRelatedPanel;

    invoke-direct {v3, v4, p3}, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;-><init>(Landroid/view/View;Ljava/lang/String;)V

    aput-object v3, v2, v6

    new-instance v3, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerController;->playerSettingsPanel:Lru/cn/tv/player/controller/StbPlayerSettingsPanel;

    invoke-direct {v3, v4, v0}, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;-><init>(Landroid/view/View;Ljava/lang/String;)V

    aput-object v3, v2, v7

    invoke-virtual {v1, v2}, Lru/cn/tv/player/controller/PanelAnimator;->setPanels([Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;)V

    goto :goto_0
.end method

.method public setRelatedListener(Lru/cn/tv/player/controller/StbPlayerRelatedPanel$Listener;)V
    .locals 1
    .param p1, "listener"    # Lru/cn/tv/player/controller/StbPlayerRelatedPanel$Listener;

    .prologue
    .line 391
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerController;->playerRelatedPanel:Lru/cn/tv/player/controller/StbPlayerRelatedPanel;

    invoke-virtual {v0, p1}, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;->setListener(Lru/cn/tv/player/controller/StbPlayerRelatedPanel$Listener;)V

    .line 392
    return-void
.end method

.method public setSettingsListener(Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;)V
    .locals 1
    .param p1, "listener"    # Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;

    .prologue
    .line 387
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerController;->playerSettingsPanel:Lru/cn/tv/player/controller/StbPlayerSettingsPanel;

    invoke-virtual {v0, p1}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->setListener(Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;)V

    .line 388
    return-void
.end method

.method public show()V
    .locals 2

    .prologue
    .line 259
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->isShown()Z

    move-result v0

    .line 260
    .local v0, "wasShown":Z
    invoke-super {p0}, Lru/cn/tv/player/controller/PlayerController;->show()V

    .line 262
    if-nez v0, :cond_0

    .line 263
    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerController;->panelAnimator:Lru/cn/tv/player/controller/PanelAnimator;

    invoke-virtual {v1}, Lru/cn/tv/player/controller/PanelAnimator;->reset()V

    .line 265
    :cond_0
    return-void
.end method

.method public showAudioTracks(Z)V
    .locals 1
    .param p1, "show"    # Z

    .prologue
    .line 417
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerController;->playerSettingsPanel:Lru/cn/tv/player/controller/StbPlayerSettingsPanel;

    invoke-virtual {v0, p1}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->showAudioTracks(Z)V

    .line 418
    return-void
.end method

.method public showChannelBlocked(Z)V
    .locals 1
    .param p1, "isBlocked"    # Z

    .prologue
    .line 425
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerController;->blockedButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setActivated(Z)V

    .line 426
    return-void
.end method

.method public showSubtitles(Z)V
    .locals 1
    .param p1, "show"    # Z

    .prologue
    .line 413
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerController;->playerSettingsPanel:Lru/cn/tv/player/controller/StbPlayerSettingsPanel;

    invoke-virtual {v0, p1}, Lru/cn/tv/player/controller/StbPlayerSettingsPanel;->showSubstitles(Z)V

    .line 414
    return-void
.end method

.method public updateControls()V
    .locals 4

    .prologue
    .line 345
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->findFocus()Landroid/view/View;

    move-result-object v1

    .line 346
    .local v1, "focusedView":Landroid/view/View;
    invoke-super {p0}, Lru/cn/tv/player/controller/PlayerController;->updateControls()V

    .line 349
    iget-object v2, p0, Lru/cn/tv/player/controller/StbPlayerController;->mediaInfoPanel:Lru/cn/tv/player/controller/MediaInfoPanel;

    iget-object v3, p0, Lru/cn/tv/player/controller/StbPlayerController;->currentTelecast:Lru/cn/api/tv/replies/Telecast;

    invoke-virtual {v2, v3}, Lru/cn/tv/player/controller/MediaInfoPanel;->setTelecast(Lru/cn/api/tv/replies/Telecast;)V

    .line 351
    invoke-direct {p0}, Lru/cn/tv/player/controller/StbPlayerController;->updateFocusNavigation()V

    .line 353
    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->findFocus()Landroid/view/View;

    move-result-object v2

    if-eq v1, v2, :cond_2

    const/4 v0, 0x1

    .line 354
    .local v0, "focusCleared":Z
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->isShown()Z

    move-result v2

    if-nez v2, :cond_1

    .line 355
    :cond_0
    invoke-virtual {p0}, Lru/cn/tv/player/controller/StbPlayerController;->requestFocus()Z

    .line 357
    :cond_1
    return-void

    .line 353
    .end local v0    # "focusCleared":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
