.class public Lru/cn/api/medialocator/replies/Rip;
.super Ljava/lang/Object;
.source "Rip.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/api/medialocator/replies/Rip$StreamingType;,
        Lru/cn/api/medialocator/replies/Rip$RipStatus;
    }
.end annotation


# instance fields
.field public adTargeting:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ad_targeting"
    .end annotation
.end field

.field public parts:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "parts"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/medialocator/replies/RipPart;",
            ">;"
        }
    .end annotation
.end field

.field public status:Lru/cn/api/medialocator/replies/Rip$RipStatus;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "status"
    .end annotation
.end field

.field public streaming_type:Lru/cn/api/medialocator/replies/Rip$StreamingType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "streaming_type"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    sget-object v0, Lru/cn/api/medialocator/replies/Rip$RipStatus;->ACCESS_ALLOWED:Lru/cn/api/medialocator/replies/Rip$RipStatus;

    iput-object v0, p0, Lru/cn/api/medialocator/replies/Rip;->status:Lru/cn/api/medialocator/replies/Rip$RipStatus;

    .line 58
    sget-object v0, Lru/cn/api/medialocator/replies/Rip$StreamingType;->PROTOCOL_DEFAULT:Lru/cn/api/medialocator/replies/Rip$StreamingType;

    iput-object v0, p0, Lru/cn/api/medialocator/replies/Rip;->streaming_type:Lru/cn/api/medialocator/replies/Rip$StreamingType;

    return-void
.end method
