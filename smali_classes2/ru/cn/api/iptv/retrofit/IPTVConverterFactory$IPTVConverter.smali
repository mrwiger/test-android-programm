.class final Lru/cn/api/iptv/retrofit/IPTVConverterFactory$IPTVConverter;
.super Ljava/lang/Object;
.source "IPTVConverterFactory.java"

# interfaces
.implements Lretrofit2/Converter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/api/iptv/retrofit/IPTVConverterFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "IPTVConverter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit2/Converter",
        "<",
        "Lokhttp3/ResponseBody;",
        "Lru/cn/api/iptv/replies/IptvReply;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lru/cn/api/iptv/retrofit/IPTVConverterFactory$1;)V
    .locals 0
    .param p1, "x0"    # Lru/cn/api/iptv/retrofit/IPTVConverterFactory$1;

    .prologue
    .line 27
    invoke-direct {p0}, Lru/cn/api/iptv/retrofit/IPTVConverterFactory$IPTVConverter;-><init>()V

    return-void
.end method

.method private parse(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 2
    .param p1, "content"    # Ljava/lang/String;
    .param p2, "contentType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Exception;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lru/cn/api/BaseAPI$ParseException;
        }
    .end annotation

    .prologue
    .line 53
    .local p3, "ignoredExceptions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Exception;>;"
    if-eqz p2, :cond_2

    .line 54
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p2

    .line 56
    const-string v1, "application/xspf+xml"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    invoke-direct {p0, p1, p3}, Lru/cn/api/iptv/retrofit/IPTVConverterFactory$IPTVConverter;->parseXspf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 67
    :goto_0
    return-object v1

    .line 58
    :cond_0
    const-string v1, "application/x-mpegurl"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "video/x-mpegurl"

    .line 59
    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 60
    :cond_1
    invoke-direct {p0, p1, p3}, Lru/cn/api/iptv/retrofit/IPTVConverterFactory$IPTVConverter;->parseM3U(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    goto :goto_0

    .line 65
    :cond_2
    :try_start_0
    invoke-direct {p0, p1, p3}, Lru/cn/api/iptv/retrofit/IPTVConverterFactory$IPTVConverter;->parseXspf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 66
    :catch_0
    move-exception v0

    .line 67
    .local v0, "e":Ljava/lang/Exception;
    invoke-direct {p0, p1, p3}, Lru/cn/api/iptv/retrofit/IPTVConverterFactory$IPTVConverter;->parseM3U(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method private parseM3U(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 2
    .param p1, "playlistContent"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Exception;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lru/cn/api/BaseAPI$ParseException;
        }
    .end annotation

    .prologue
    .line 73
    .local p2, "ignoredExceptions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Exception;>;"
    new-instance v0, Lru/cn/api/iptv/M3UChannelsParser;

    invoke-direct {v0, p1, p2}, Lru/cn/api/iptv/M3UChannelsParser;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 74
    .local v0, "m3uChannelsParser":Lru/cn/api/iptv/M3UChannelsParser;
    invoke-virtual {v0}, Lru/cn/api/iptv/M3UChannelsParser;->parse()Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method private parseXspf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 4
    .param p1, "content"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Exception;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lru/cn/api/BaseAPI$ParseException;
        }
    .end annotation

    .prologue
    .line 81
    .local p2, "ignoredExceptions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Exception;>;"
    :try_start_0
    new-instance v1, Lru/cn/api/iptv/XSPFChannelsParser;

    invoke-direct {v1, p1, p2}, Lru/cn/api/iptv/XSPFChannelsParser;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 82
    .local v1, "parser":Lru/cn/api/iptv/XSPFChannelsParser;
    invoke-virtual {v1}, Lru/cn/api/iptv/XSPFChannelsParser;->parse()V

    .line 84
    invoke-virtual {v1}, Lru/cn/api/iptv/XSPFChannelsParser;->getLocations()Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 85
    .end local v1    # "parser":Lru/cn/api/iptv/XSPFChannelsParser;
    :catch_0
    move-exception v0

    .line 86
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Lru/cn/api/BaseAPI$ParseException;

    const-string v3, "Wrong XSPF format"

    invoke-direct {v2, v3, v0}, Lru/cn/api/BaseAPI$ParseException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method


# virtual methods
.method public bridge synthetic convert(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27
    check-cast p1, Lokhttp3/ResponseBody;

    invoke-virtual {p0, p1}, Lru/cn/api/iptv/retrofit/IPTVConverterFactory$IPTVConverter;->convert(Lokhttp3/ResponseBody;)Lru/cn/api/iptv/replies/IptvReply;

    move-result-object v0

    return-object v0
.end method

.method public convert(Lokhttp3/ResponseBody;)Lru/cn/api/iptv/replies/IptvReply;
    .locals 6
    .param p1, "responseBody"    # Lokhttp3/ResponseBody;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    invoke-virtual {p1}, Lokhttp3/ResponseBody;->contentType()Lokhttp3/MediaType;

    move-result-object v2

    .line 32
    .local v2, "mediaType":Lokhttp3/MediaType;
    invoke-virtual {p1}, Lokhttp3/ResponseBody;->string()Ljava/lang/String;

    move-result-object v0

    .line 35
    .local v0, "content":Ljava/lang/String;
    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 36
    :cond_0
    new-instance v4, Lru/cn/api/BaseAPI$ParseException;

    const-string v5, "No content"

    invoke-direct {v4, v5}, Lru/cn/api/BaseAPI$ParseException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Lru/cn/api/BaseAPI$ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    :catch_0
    move-exception v1

    .line 47
    .local v1, "e":Lru/cn/api/BaseAPI$ParseException;
    new-instance v4, Ljava/io/IOException;

    invoke-direct {v4, v1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 39
    .end local v1    # "e":Lru/cn/api/BaseAPI$ParseException;
    :cond_1
    :try_start_1
    new-instance v3, Lru/cn/api/iptv/replies/IptvReply;

    invoke-direct {v3}, Lru/cn/api/iptv/replies/IptvReply;-><init>()V

    .line 40
    .local v3, "reply":Lru/cn/api/iptv/replies/IptvReply;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v3, Lru/cn/api/iptv/replies/IptvReply;->parsingExceptions:Ljava/util/List;

    .line 41
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lokhttp3/MediaType;->type()Ljava/lang/String;

    move-result-object v4

    :goto_0
    iget-object v5, v3, Lru/cn/api/iptv/replies/IptvReply;->parsingExceptions:Ljava/util/List;

    invoke-direct {p0, v0, v4, v5}, Lru/cn/api/iptv/retrofit/IPTVConverterFactory$IPTVConverter;->parse(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, v3, Lru/cn/api/iptv/replies/IptvReply;->locations:Ljava/util/List;
    :try_end_1
    .catch Lru/cn/api/BaseAPI$ParseException; {:try_start_1 .. :try_end_1} :catch_0

    .line 44
    return-object v3

    .line 41
    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method
