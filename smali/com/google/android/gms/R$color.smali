.class public final Lcom/google/android/gms/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final cast_expanded_controller_ad_container_white_stripe_color:I = 0x7f06003c

.field public static final cast_expanded_controller_ad_label_background_color:I = 0x7f06003d

.field public static final cast_expanded_controller_background_color:I = 0x7f06003e

.field public static final cast_expanded_controller_progress_text_color:I = 0x7f06003f

.field public static final cast_expanded_controller_seek_bar_progress_background_tint_color:I = 0x7f060040

.field public static final cast_expanded_controller_text_color:I = 0x7f060041

.field public static final cast_intro_overlay_background_color:I = 0x7f060042

.field public static final cast_intro_overlay_button_background_color:I = 0x7f060043

.field public static final cast_libraries_material_featurehighlight_outer_highlight_default_color:I = 0x7f060044

.field public static final cast_libraries_material_featurehighlight_text_body_color:I = 0x7f060045

.field public static final cast_libraries_material_featurehighlight_text_header_color:I = 0x7f060046

.field public static final common_google_signin_btn_text_dark:I = 0x7f060052

.field public static final common_google_signin_btn_text_dark_default:I = 0x7f060053

.field public static final common_google_signin_btn_text_dark_disabled:I = 0x7f060054

.field public static final common_google_signin_btn_text_dark_focused:I = 0x7f060055

.field public static final common_google_signin_btn_text_dark_pressed:I = 0x7f060056

.field public static final common_google_signin_btn_text_light:I = 0x7f060057

.field public static final common_google_signin_btn_text_light_default:I = 0x7f060058

.field public static final common_google_signin_btn_text_light_disabled:I = 0x7f060059

.field public static final common_google_signin_btn_text_light_focused:I = 0x7f06005a

.field public static final common_google_signin_btn_text_light_pressed:I = 0x7f06005b

.field public static final common_google_signin_btn_tint:I = 0x7f06005c
