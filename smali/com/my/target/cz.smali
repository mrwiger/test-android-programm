.class public final Lcom/my/target/cz;
.super Landroid/support/v7/widget/RecyclerView;
.source "PromoCardImageRecyclerView.java"

# interfaces
.implements Lcom/my/target/da;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/cz$a;,
        Lcom/my/target/cz$b;
    }
.end annotation


# instance fields
.field private final aT:Lcom/my/target/cz$b;

.field private final aU:Lcom/my/target/cy;

.field private aV:Lcom/my/target/da$a;

.field private final cardClickListener:Landroid/view/View$OnClickListener;

.field private cards:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/b;",
            ">;"
        }
    .end annotation
.end field

.field private moving:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/my/target/cz;-><init>(Landroid/content/Context;B)V

    .line 37
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/my/target/cz;-><init>(Landroid/content/Context;C)V

    .line 42
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;C)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    new-instance v0, Lcom/my/target/cz$a;

    invoke-direct {v0, p0, v1}, Lcom/my/target/cz$a;-><init>(Lcom/my/target/cz;B)V

    iput-object v0, p0, Lcom/my/target/cz;->cardClickListener:Landroid/view/View$OnClickListener;

    .line 48
    new-instance v0, Lcom/my/target/cz$b;

    invoke-direct {v0, p1}, Lcom/my/target/cz$b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cz;->aT:Lcom/my/target/cz$b;

    .line 49
    iget-object v0, p0, Lcom/my/target/cz;->aT:Lcom/my/target/cz$b;

    const/4 v1, 0x4

    invoke-static {v1, p1}, Lcom/my/target/cm;->a(ILandroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/cz$b;->setDividerPadding(I)V

    .line 50
    new-instance v0, Lcom/my/target/cy;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/my/target/cy;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cz;->aU:Lcom/my/target/cy;

    .line 52
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 53
    return-void
.end method

.method static synthetic a(Lcom/my/target/cz;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/my/target/cz;->checkCardChanged()V

    return-void
.end method

.method static synthetic b(Lcom/my/target/cz;)Z
    .locals 1

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/my/target/cz;->moving:Z

    return v0
.end method

.method static synthetic c(Lcom/my/target/cz;)Lcom/my/target/cz$b;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/my/target/cz;->aT:Lcom/my/target/cz$b;

    return-object v0
.end method

.method private checkCardChanged()V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/my/target/cz;->aV:Lcom/my/target/da$a;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/my/target/cz;->aV:Lcom/my/target/da$a;

    invoke-virtual {p0}, Lcom/my/target/cz;->getVisibleCardNumbers()[I

    move-result-object v1

    invoke-interface {v0, p0, v1}, Lcom/my/target/da$a;->a(Landroid/view/View;[I)V

    .line 132
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/my/target/cz;)Lcom/my/target/da$a;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/my/target/cz;->aV:Lcom/my/target/da$a;

    return-object v0
.end method

.method static synthetic e(Lcom/my/target/cz;)Ljava/util/List;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/my/target/cz;->cards:Ljava/util/List;

    return-object v0
.end method

.method private setCardLayoutManager(Lcom/my/target/cz$b;)V
    .locals 1
    .param p1, "layoutManager"    # Lcom/my/target/cz$b;

    .prologue
    .line 136
    new-instance v0, Lcom/my/target/cz$1;

    invoke-direct {v0, p0}, Lcom/my/target/cz$1;-><init>(Lcom/my/target/cz;)V

    invoke-virtual {p1, v0}, Lcom/my/target/cz$b;->a(Lcom/my/target/ct$a;)V

    .line 145
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 146
    return-void
.end method


# virtual methods
.method public final dispose()V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/my/target/cz;->aU:Lcom/my/target/cy;

    invoke-virtual {v0}, Lcom/my/target/cy;->dispose()V

    .line 124
    return-void
.end method

.method public final getState()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/my/target/cz;->aT:Lcom/my/target/cz$b;

    invoke-virtual {v0}, Lcom/my/target/cz$b;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public final getVisibleCardNumbers()[I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 84
    iget-object v1, p0, Lcom/my/target/cz;->aT:Lcom/my/target/cz$b;

    invoke-virtual {v1}, Landroid/support/v7/widget/LinearLayoutManager;->findFirstCompletelyVisibleItemPosition()I

    move-result v1

    .line 85
    iget-object v2, p0, Lcom/my/target/cz;->aT:Lcom/my/target/cz$b;

    invoke-virtual {v2}, Landroid/support/v7/widget/LinearLayoutManager;->findLastCompletelyVisibleItemPosition()I

    move-result v2

    .line 87
    iget-object v3, p0, Lcom/my/target/cz;->cards:Ljava/util/List;

    if-eqz v3, :cond_0

    if-gt v1, v2, :cond_0

    if-ltz v1, :cond_0

    iget-object v3, p0, Lcom/my/target/cz;->cards:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v2, v3, :cond_1

    .line 89
    :cond_0
    new-array v0, v0, [I

    .line 99
    :goto_0
    return-object v0

    .line 92
    :cond_1
    sub-int/2addr v2, v1

    add-int/lit8 v2, v2, 0x1

    new-array v2, v2, [I

    .line 93
    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_2

    .line 95
    aput v1, v2, v0

    .line 96
    add-int/lit8 v1, v1, 0x1

    .line 93
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 99
    goto :goto_0
.end method

.method public final onScrollStateChanged(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 71
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->onScrollStateChanged(I)V

    .line 73
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/my/target/cz;->moving:Z

    .line 75
    iget-boolean v0, p0, Lcom/my/target/cz;->moving:Z

    if-nez v0, :cond_0

    .line 77
    invoke-direct {p0}, Lcom/my/target/cz;->checkCardChanged()V

    .line 79
    :cond_0
    return-void

    .line 73
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final restoreState(Landroid/os/Parcelable;)V
    .locals 1
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/my/target/cz;->aT:Lcom/my/target/cz$b;

    invoke-virtual {v0, p1}, Lcom/my/target/cz$b;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 112
    return-void
.end method

.method public final setPromoCardSliderListener(Lcom/my/target/da$a;)V
    .locals 0
    .param p1, "promoClickListener"    # Lcom/my/target/da$a;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/my/target/cz;->aV:Lcom/my/target/da$a;

    .line 118
    return-void
.end method

.method public final setupCards(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 58
    .local p1, "cards":Ljava/util/List;, "Ljava/util/List<Lcom/my/target/core/models/banners/b;>;"
    iput-object p1, p0, Lcom/my/target/cz;->cards:Ljava/util/List;

    .line 59
    iget-object v0, p0, Lcom/my/target/cz;->aU:Lcom/my/target/cy;

    invoke-virtual {v0, p1}, Lcom/my/target/cy;->setCards(Ljava/util/List;)V

    .line 60
    invoke-virtual {p0}, Landroid/view/View;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/my/target/cz;->aU:Lcom/my/target/cy;

    iget-object v1, p0, Lcom/my/target/cz;->cardClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/my/target/cy;->setCardClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/my/target/cz;->aT:Lcom/my/target/cz$b;

    invoke-direct {p0, v0}, Lcom/my/target/cz;->setCardLayoutManager(Lcom/my/target/cz$b;)V

    .line 65
    iget-object v0, p0, Lcom/my/target/cz;->aU:Lcom/my/target/cy;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/RecyclerView;->swapAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;Z)V

    .line 66
    return-void
.end method
