.class public Lcom/my/target/nativeads/views/ContentWallAdView;
.super Landroid/widget/RelativeLayout;
.source "ContentWallAdView.java"


# static fields
.field private static final AD_ID:I

.field private static final AGE_ID:I

.field private static final IMAGE_ID:I

.field private static final STARS_ID:I

.field private static final VOTES_ID:I


# instance fields
.field private final advertisingLabel:Landroid/widget/TextView;

.field private final ageRestrictionLabel:Lcom/my/target/bu;

.field private banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

.field private final mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

.field private final starsView:Lcom/my/target/ca;

.field private final uiUtils:Lcom/my/target/cm;

.field private final votesLabel:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/ContentWallAdView;->AGE_ID:I

    .line 25
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/ContentWallAdView;->AD_ID:I

    .line 26
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/ContentWallAdView;->IMAGE_ID:I

    .line 27
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/ContentWallAdView;->STARS_ID:I

    .line 28
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/ContentWallAdView;->VOTES_ID:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/my/target/nativeads/views/ContentWallAdView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/my/target/nativeads/views/ContentWallAdView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    new-instance v0, Lcom/my/target/bu;

    invoke-direct {v0, p1}, Lcom/my/target/bu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    .line 52
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->advertisingLabel:Landroid/widget/TextView;

    .line 53
    invoke-static {p1}, Lcom/my/target/nativeads/factories/NativeViewsFactory;->getMediaAdView(Landroid/content/Context;)Lcom/my/target/nativeads/views/MediaAdView;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    .line 54
    new-instance v0, Lcom/my/target/ca;

    invoke-direct {v0, p1}, Lcom/my/target/ca;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->starsView:Lcom/my/target/ca;

    .line 55
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->votesLabel:Landroid/widget/TextView;

    .line 56
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->uiUtils:Lcom/my/target/cm;

    .line 58
    const-string v0, "ad_view"

    invoke-static {p0, v0}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    const-string v1, "age_border"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->advertisingLabel:Landroid/widget/TextView;

    const-string v1, "advertising_label"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    const-string v1, "media_view"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->starsView:Lcom/my/target/ca;

    const-string v1, "rating_view"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->votesLabel:Landroid/widget/TextView;

    const-string v1, "votes_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 64
    invoke-direct {p0}, Lcom/my/target/nativeads/views/ContentWallAdView;->initView()V

    .line 65
    return-void
.end method

.method private initView()V
    .locals 9

    .prologue
    const/16 v4, 0xc

    const/4 v8, 0x3

    const/4 v7, -0x1

    const/4 v6, -0x2

    const/4 v5, 0x0

    .line 111
    iget-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v0, v4}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iget-object v1, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->uiUtils:Lcom/my/target/cm;

    .line 112
    invoke-virtual {v1, v4}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->uiUtils:Lcom/my/target/cm;

    .line 113
    invoke-virtual {v2, v4}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->uiUtils:Lcom/my/target/cm;

    .line 114
    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    .line 111
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/my/target/nativeads/views/ContentWallAdView;->setPadding(IIII)V

    .line 117
    iget-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    sget v1, Lcom/my/target/nativeads/views/ContentWallAdView;->AGE_ID:I

    invoke-virtual {v0, v1}, Lcom/my/target/bu;->setId(I)V

    .line 118
    iget-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    iget-object v1, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->uiUtils:Lcom/my/target/cm;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v1, v5, v5, v5}, Lcom/my/target/bu;->setPadding(IIII)V

    .line 119
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 121
    iget-object v1, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 122
    iget-object v1, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    invoke-virtual {v1, v0}, Lcom/my/target/bu;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 123
    iget-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    invoke-virtual {v0, v7}, Lcom/my/target/bu;->setTextColor(I)V

    .line 124
    iget-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v7}, Lcom/my/target/bu;->c(II)V

    .line 126
    iget-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->advertisingLabel:Landroid/widget/TextView;

    sget v1, Lcom/my/target/nativeads/views/ContentWallAdView;->AD_ID:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 127
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 129
    const/4 v1, 0x1

    sget v2, Lcom/my/target/nativeads/views/ContentWallAdView;->AGE_ID:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 130
    iget-object v1, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->advertisingLabel:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 131
    iget-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->advertisingLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 132
    iget-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->advertisingLabel:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v1, v8}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v1, v5, v5, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 135
    iget-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    sget v1, Lcom/my/target/nativeads/views/ContentWallAdView;->IMAGE_ID:I

    invoke-virtual {v0, v1}, Lcom/my/target/nativeads/views/MediaAdView;->setId(I)V

    .line 136
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 138
    iget-object v1, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v1, v0}, Lcom/my/target/nativeads/views/MediaAdView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 140
    iget-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->starsView:Lcom/my/target/ca;

    sget v1, Lcom/my/target/nativeads/views/ContentWallAdView;->STARS_ID:I

    invoke-virtual {v0, v1}, Lcom/my/target/ca;->setId(I)V

    .line 141
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0x49

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->uiUtils:Lcom/my/target/cm;

    .line 142
    invoke-virtual {v2, v4}, Lcom/my/target/cm;->n(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 143
    iget-object v1, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->uiUtils:Lcom/my/target/cm;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 144
    iget-object v1, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->uiUtils:Lcom/my/target/cm;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 145
    iget-object v1, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->starsView:Lcom/my/target/ca;

    invoke-virtual {v1, v0}, Lcom/my/target/ca;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 147
    iget-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->votesLabel:Landroid/widget/TextView;

    sget v1, Lcom/my/target/nativeads/views/ContentWallAdView;->VOTES_ID:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 149
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/my/target/nativeads/views/ContentWallAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 150
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 151
    iget-object v1, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v1, v8}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->uiUtils:Lcom/my/target/cm;

    .line 152
    invoke-virtual {v2, v8}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->uiUtils:Lcom/my/target/cm;

    .line 153
    invoke-virtual {v3, v8}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->uiUtils:Lcom/my/target/cm;

    .line 154
    invoke-virtual {v4, v8}, Lcom/my/target/cm;->n(I)I

    move-result v4

    .line 151
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 155
    const-string v1, "#55000000"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 156
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 158
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 160
    const v1, -0x3a1508

    invoke-static {p0, v5, v1}, Lcom/my/target/cm;->a(Landroid/view/View;II)V

    .line 162
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/my/target/nativeads/views/ContentWallAdView;->setClickable(Z)V

    .line 164
    iget-object v1, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {p0, v1}, Lcom/my/target/nativeads/views/ContentWallAdView;->addView(Landroid/view/View;)V

    .line 166
    iget-object v1, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 167
    iget-object v1, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->advertisingLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 169
    invoke-virtual {p0, v0}, Lcom/my/target/nativeads/views/ContentWallAdView;->addView(Landroid/view/View;)V

    .line 170
    return-void
.end method


# virtual methods
.method public getAdvertisingTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->advertisingLabel:Landroid/widget/TextView;

    return-object v0
.end method

.method public getAgeRestrictionTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    return-object v0
.end method

.method public getMediaAdView()Lcom/my/target/nativeads/views/MediaAdView;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    return-object v0
.end method

.method public setupView(Lcom/my/target/nativeads/banners/NativePromoBanner;)V
    .locals 4
    .param p1, "banner"    # Lcom/my/target/nativeads/banners/NativePromoBanner;

    .prologue
    const/4 v3, 0x0

    .line 84
    if-nez p1, :cond_0

    .line 107
    :goto_0
    return-void

    .line 88
    :cond_0
    iput-object p1, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

    .line 89
    const-string v0, "Setup banner"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 91
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getAgeRestrictions()Ljava/lang/String;

    move-result-object v0

    .line 92
    if-eqz v0, :cond_1

    .line 94
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 96
    iget-object v1, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    invoke-virtual {v1, v0}, Lcom/my/target/bu;->setText(Ljava/lang/CharSequence;)V

    .line 105
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    invoke-virtual {v1, v0}, Lcom/my/target/bu;->setText(Ljava/lang/CharSequence;)V

    .line 106
    iget-object v0, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->advertisingLabel:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getAdvertisingLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 100
    :cond_2
    iget-object v1, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/my/target/bu;->setVisibility(I)V

    .line 101
    iget-object v1, p0, Lcom/my/target/nativeads/views/ContentWallAdView;->advertisingLabel:Landroid/widget/TextView;

    invoke-virtual {v1, v3, v3, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_1
.end method
