.class public final Lcom/my/target/ef;
.super Landroid/view/ViewGroup;
.source "PanelView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# static fields
.field private static final az:I

.field private static final bC:I

.field private static final bD:I

.field private static final bE:I

.field private static final bF:I

.field private static final bG:I

.field private static final bi:I


# instance fields
.field private final aS:Landroid/widget/LinearLayout;

.field private final aU:Lcom/my/target/ca;

.field private final aV:Landroid/widget/TextView;

.field private final aw:Lcom/my/target/cm;

.field private final bH:Landroid/widget/TextView;

.field private final bI:Lcom/my/target/dy;

.field private final bJ:Landroid/widget/TextView;

.field private final bK:Landroid/widget/TextView;

.field private final bL:Landroid/widget/Button;

.field private final bM:I

.field private final bN:I

.field private final bO:I

.field private final bk:Lcom/my/target/bv;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ef;->bi:I

    .line 47
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ef;->bC:I

    .line 48
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ef;->bD:I

    .line 49
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ef;->bE:I

    .line 50
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ef;->bF:I

    .line 51
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ef;->bG:I

    .line 52
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ef;->az:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/my/target/cm;)V
    .locals 2

    .prologue
    .line 71
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 73
    iput-object p2, p0, Lcom/my/target/ef;->aw:Lcom/my/target/cm;

    .line 75
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    .line 76
    iget-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    sget v1, Lcom/my/target/ef;->bC:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setId(I)V

    .line 77
    iget-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    const-string v1, "cta_button"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 78
    new-instance v0, Lcom/my/target/bv;

    invoke-direct {v0, p1}, Lcom/my/target/bv;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    .line 79
    iget-object v0, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    sget v1, Lcom/my/target/ef;->bi:I

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setId(I)V

    .line 80
    iget-object v0, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    const-string v1, "icon_image"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 82
    new-instance v0, Lcom/my/target/dy;

    invoke-direct {v0, p1}, Lcom/my/target/dy;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    .line 83
    iget-object v0, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    sget v1, Lcom/my/target/ef;->az:I

    invoke-virtual {v0, v1}, Lcom/my/target/dy;->setId(I)V

    .line 84
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ef;->bH:Landroid/widget/TextView;

    .line 85
    iget-object v0, p0, Lcom/my/target/ef;->bH:Landroid/widget/TextView;

    sget v1, Lcom/my/target/ef;->bD:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 86
    iget-object v0, p0, Lcom/my/target/ef;->bH:Landroid/widget/TextView;

    const-string v1, "description_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 87
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    .line 88
    iget-object v0, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    const-string v1, "disclaimer_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 89
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    .line 90
    new-instance v0, Lcom/my/target/ca;

    invoke-direct {v0, p1}, Lcom/my/target/ca;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ef;->aU:Lcom/my/target/ca;

    .line 91
    iget-object v0, p0, Lcom/my/target/ef;->aU:Lcom/my/target/ca;

    sget v1, Lcom/my/target/ef;->bF:I

    invoke-virtual {v0, v1}, Lcom/my/target/ca;->setId(I)V

    .line 92
    iget-object v0, p0, Lcom/my/target/ef;->aU:Lcom/my/target/ca;

    const-string v1, "stars_view"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 93
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ef;->aV:Landroid/widget/TextView;

    .line 94
    iget-object v0, p0, Lcom/my/target/ef;->aV:Landroid/widget/TextView;

    sget v1, Lcom/my/target/ef;->bG:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 95
    iget-object v0, p0, Lcom/my/target/ef;->aV:Landroid/widget/TextView;

    const-string v1, "votes_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 96
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    .line 97
    iget-object v0, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    const-string v1, "domain_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    sget v1, Lcom/my/target/ef;->bE:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 100
    const/16 v0, 0x10

    invoke-virtual {p2, v0}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iput v0, p0, Lcom/my/target/ef;->bM:I

    .line 101
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iput v0, p0, Lcom/my/target/ef;->bO:I

    .line 102
    const/16 v0, 0x40

    invoke-virtual {p2, v0}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iput v0, p0, Lcom/my/target/ef;->bN:I

    .line 103
    return-void
.end method

.method static synthetic a(Lcom/my/target/ef;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    return-object v0
.end method

.method private varargs a(I[Landroid/view/View;)V
    .locals 11

    .prologue
    const v10, 0x3e99999a    # 0.3f

    const/4 v9, 0x0

    const v7, 0x3f333333    # 0.7f

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 440
    iget-object v0, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    invoke-virtual {v0}, Lcom/my/target/bv;->getHeight()I

    move-result v0

    .line 441
    invoke-virtual {p0}, Lcom/my/target/ef;->getHeight()I

    move-result v2

    .line 442
    iget-object v3, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getWidth()I

    move-result v3

    .line 443
    iget-object v4, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getHeight()I

    move-result v4

    .line 444
    iget-object v5, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    invoke-virtual {v5}, Lcom/my/target/bv;->getWidth()I

    move-result v5

    .line 446
    iget-object v6, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    invoke-virtual {v6, v9}, Lcom/my/target/bv;->setPivotX(F)V

    .line 447
    iget-object v6, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {v6, v0}, Lcom/my/target/bv;->setPivotY(F)V

    .line 448
    iget-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setPivotX(F)V

    .line 449
    iget-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    div-int/lit8 v3, v4, 0x2

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setPivotY(F)V

    .line 451
    int-to-float v0, v2

    mul-float v2, v0, v10

    .line 453
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 455
    iget-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    sget-object v4, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v6, v8, [F

    aput v7, v6, v1

    invoke-static {v0, v4, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 456
    iget-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    sget-object v4, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v6, v8, [F

    aput v7, v6, v1

    invoke-static {v0, v4, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 457
    iget-object v0, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    sget-object v4, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v6, v8, [F

    aput v7, v6, v1

    invoke-static {v0, v4, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458
    iget-object v0, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    sget-object v4, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v6, v8, [F

    aput v7, v6, v1

    invoke-static {v0, v4, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 459
    iget-object v0, p0, Lcom/my/target/ef;->bH:Landroid/widget/TextView;

    sget-object v4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v8, [F

    aput v9, v6, v1

    invoke-static {v0, v4, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 460
    iget-object v0, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    sget-object v4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v8, [F

    aput v9, v6, v1

    invoke-static {v0, v4, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 461
    iget-object v0, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463
    iget-object v0, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    sget-object v4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v8, [F

    const/high16 v7, 0x3f800000    # 1.0f

    aput v7, v6, v1

    invoke-static {v0, v4, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 465
    :cond_0
    sget-object v0, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v8, [F

    const v6, 0x3f19999a    # 0.6f

    aput v6, v4, v1

    invoke-static {p0, v0, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 466
    int-to-float v0, v5

    mul-float/2addr v0, v10

    neg-float v0, v0

    .line 467
    iget-object v4, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    sget-object v5, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v6, v8, [F

    aput v0, v6, v1

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 468
    iget-object v4, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    sget-object v5, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v6, v8, [F

    aput v0, v6, v1

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 469
    iget-object v4, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    sget-object v5, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v6, v8, [F

    aput v0, v6, v1

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 470
    iget-object v4, p0, Lcom/my/target/ef;->bH:Landroid/widget/TextView;

    sget-object v5, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v6, v8, [F

    aput v0, v6, v1

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 471
    iget-object v4, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    sget-object v5, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v6, v8, [F

    aput v0, v6, v1

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 472
    sget-object v0, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v4, v8, [F

    aput v2, v4, v1

    invoke-static {p0, v0, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 474
    iget-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    sget-object v4, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v5, v8, [F

    neg-float v6, v2

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    aput v6, v5, v1

    invoke-static {v0, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 475
    iget-object v0, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    sget-object v4, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v5, v8, [F

    neg-float v6, v2

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    aput v6, v5, v1

    invoke-static {v0, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 477
    array-length v4, p2

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, p2, v0

    .line 479
    sget-object v6, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v7, v8, [F

    aput v2, v7, v1

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 477
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 482
    :cond_1
    iget-object v0, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 484
    iget-object v0, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 487
    :cond_2
    iget-object v0, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 489
    iget-object v0, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 492
    :cond_3
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 493
    new-instance v1, Lcom/my/target/ef$1;

    invoke-direct {v1, p0}, Lcom/my/target/ef$1;-><init>(Lcom/my/target/ef;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 520
    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 521
    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 522
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 523
    return-void
.end method

.method static synthetic b(Lcom/my/target/ef;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/my/target/ef;->bH:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lcom/my/target/ef;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic d(Lcom/my/target/ef;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public final H()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x4

    const/4 v8, 0x2

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 107
    const/high16 v0, 0x66000000

    invoke-virtual {p0, v0}, Lcom/my/target/ef;->setBackgroundColor(I)V

    .line 109
    iget-object v0, p0, Lcom/my/target/ef;->bH:Landroid/widget/TextView;

    const v1, -0x222223

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 110
    iget-object v0, p0, Lcom/my/target/ef;->bH:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 112
    iget-object v0, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    const v1, -0x666667

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 113
    iget-object v0, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 115
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 116
    invoke-virtual {v0, v6}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 117
    const v1, -0x333334

    invoke-virtual {v0, v10, v1}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 118
    iget-object v1, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/my/target/ef;->aw:Lcom/my/target/cm;

    invoke-virtual {v2, v9}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/ef;->aw:Lcom/my/target/cm;

    invoke-virtual {v3, v9}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/ef;->aw:Lcom/my/target/cm;

    .line 119
    invoke-virtual {v4, v9}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v5, p0, Lcom/my/target/ef;->aw:Lcom/my/target/cm;

    invoke-virtual {v5, v9}, Lcom/my/target/cm;->n(I)I

    move-result v5

    .line 118
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 121
    iget-object v1, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 122
    iget-object v0, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    const/high16 v1, 0x41400000    # 12.0f

    invoke-virtual {v0, v8, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 123
    iget-object v0, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    const v1, -0x333334

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 124
    iget-object v0, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 126
    iget-object v0, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 127
    iget-object v0, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 128
    iget-object v0, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 130
    iget-object v0, p0, Lcom/my/target/ef;->aV:Landroid/widget/TextView;

    const v1, -0x666667

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 131
    iget-object v0, p0, Lcom/my/target/ef;->aV:Landroid/widget/TextView;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 132
    iget-object v0, p0, Lcom/my/target/ef;->aV:Landroid/widget/TextView;

    const/high16 v1, 0x41600000    # 14.0f

    invoke-virtual {v0, v8, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 134
    iget-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    iget-object v1, p0, Lcom/my/target/ef;->aw:Lcom/my/target/cm;

    const/16 v2, 0xf

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/ef;->aw:Lcom/my/target/cm;

    const/16 v3, 0xf

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    invoke-virtual {v0, v1, v6, v2, v6}, Landroid/widget/Button;->setPadding(IIII)V

    .line 135
    iget-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    iget-object v1, p0, Lcom/my/target/ef;->aw:Lcom/my/target/cm;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setMinimumWidth(I)V

    .line 136
    iget-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 137
    iget-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    const/high16 v1, 0x41b00000    # 22.0f

    invoke-virtual {v0, v8, v1}, Landroid/widget/Button;->setTextSize(IF)V

    .line 138
    iget-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setMaxEms(I)V

    .line 139
    iget-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->setSingleLine()V

    .line 140
    iget-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 142
    iget-object v0, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    invoke-virtual {v0}, Lcom/my/target/dy;->getRightBorderedView()Lcom/my/target/bu;

    move-result-object v0

    .line 143
    const v1, -0x777778

    invoke-virtual {v0, v10, v1}, Lcom/my/target/bu;->c(II)V

    .line 144
    iget-object v1, p0, Lcom/my/target/ef;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v8}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v1, v6, v6, v6}, Lcom/my/target/bu;->setPadding(IIII)V

    .line 145
    const v1, -0x111112

    invoke-virtual {v0, v1}, Lcom/my/target/bu;->setTextColor(I)V

    .line 146
    const v1, -0x111112

    iget-object v2, p0, Lcom/my/target/ef;->aw:Lcom/my/target/cm;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    invoke-virtual {v0, v10, v1, v2}, Lcom/my/target/bu;->a(III)V

    .line 147
    const/high16 v1, 0x66000000

    invoke-virtual {v0, v1}, Lcom/my/target/bu;->setBackgroundColor(I)V

    .line 149
    iget-object v0, p0, Lcom/my/target/ef;->aU:Lcom/my/target/ca;

    iget-object v1, p0, Lcom/my/target/ef;->aw:Lcom/my/target/cm;

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/ca;->setStarSize(I)V

    .line 151
    iget-object v0, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/ef;->aU:Lcom/my/target/ca;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 152
    iget-object v0, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/ef;->aV:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 154
    iget-object v0, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 155
    iget-object v0, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 157
    iget-object v0, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    invoke-virtual {p0, v0}, Lcom/my/target/ef;->addView(Landroid/view/View;)V

    .line 158
    iget-object v0, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/my/target/ef;->addView(Landroid/view/View;)V

    .line 159
    iget-object v0, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/my/target/ef;->addView(Landroid/view/View;)V

    .line 160
    iget-object v0, p0, Lcom/my/target/ef;->bH:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/my/target/ef;->addView(Landroid/view/View;)V

    .line 161
    iget-object v0, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/my/target/ef;->addView(Landroid/view/View;)V

    .line 163
    iget-object v0, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    invoke-virtual {p0, v0}, Lcom/my/target/ef;->addView(Landroid/view/View;)V

    .line 164
    iget-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    invoke-virtual {p0, v0}, Lcom/my/target/ef;->addView(Landroid/view/View;)V

    .line 165
    return-void
.end method

.method public final a(Lcom/my/target/af;Landroid/view/View$OnClickListener;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 262
    iget-boolean v0, p1, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_0

    .line 264
    invoke-virtual {p0, p2}, Lcom/my/target/ef;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 265
    iget-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 342
    :goto_0
    return-void

    .line 269
    :cond_0
    iget-boolean v0, p1, Lcom/my/target/af;->cy:Z

    if-eqz v0, :cond_1

    .line 271
    iget-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 277
    :goto_1
    iget-boolean v0, p1, Lcom/my/target/af;->cD:Z

    if-eqz v0, :cond_2

    .line 279
    invoke-virtual {p0, p2}, Lcom/my/target/ef;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 285
    :goto_2
    iget-boolean v0, p1, Lcom/my/target/af;->cs:Z

    if-eqz v0, :cond_3

    .line 287
    iget-object v0, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    invoke-virtual {v0}, Lcom/my/target/dy;->getLeftText()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 293
    :goto_3
    iget-boolean v0, p1, Lcom/my/target/af;->cz:Z

    if-eqz v0, :cond_4

    .line 295
    iget-object v0, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    invoke-virtual {v0}, Lcom/my/target/dy;->getRightBorderedView()Lcom/my/target/bu;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/my/target/bu;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 301
    :goto_4
    iget-boolean v0, p1, Lcom/my/target/af;->cu:Z

    if-eqz v0, :cond_5

    .line 303
    iget-object v0, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    invoke-virtual {v0, p2}, Lcom/my/target/bv;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 309
    :goto_5
    iget-boolean v0, p1, Lcom/my/target/af;->ct:Z

    if-eqz v0, :cond_6

    .line 311
    iget-object v0, p0, Lcom/my/target/ef;->bH:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 317
    :goto_6
    iget-boolean v0, p1, Lcom/my/target/af;->cw:Z

    if-eqz v0, :cond_7

    .line 319
    iget-object v0, p0, Lcom/my/target/ef;->aU:Lcom/my/target/ca;

    invoke-virtual {v0, p2}, Lcom/my/target/ca;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 325
    :goto_7
    iget-boolean v0, p1, Lcom/my/target/af;->cx:Z

    if-eqz v0, :cond_8

    .line 327
    iget-object v0, p0, Lcom/my/target/ef;->aV:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 333
    :goto_8
    iget-boolean v0, p1, Lcom/my/target/af;->cB:Z

    if-eqz v0, :cond_9

    .line 335
    iget-object v0, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 275
    :cond_1
    iget-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1

    .line 283
    :cond_2
    invoke-virtual {p0, v2}, Lcom/my/target/ef;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 291
    :cond_3
    iget-object v0, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    invoke-virtual {v0}, Lcom/my/target/dy;->getLeftText()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 299
    :cond_4
    iget-object v0, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    invoke-virtual {v0}, Lcom/my/target/dy;->getRightBorderedView()Lcom/my/target/bu;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/my/target/bu;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_4

    .line 307
    :cond_5
    iget-object v0, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    invoke-virtual {v0, v2}, Lcom/my/target/bv;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_5

    .line 315
    :cond_6
    iget-object v0, p0, Lcom/my/target/ef;->bH:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_6

    .line 323
    :cond_7
    iget-object v0, p0, Lcom/my/target/ef;->aU:Lcom/my/target/ca;

    invoke-virtual {v0, v2}, Lcom/my/target/ca;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_7

    .line 331
    :cond_8
    iget-object v0, p0, Lcom/my/target/ef;->aV:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_8

    .line 339
    :cond_9
    iget-object v0, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method

.method public final varargs a([Landroid/view/View;)V
    .locals 1

    .prologue
    .line 254
    invoke-virtual {p0}, Lcom/my/target/ef;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1356
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/my/target/ef;->a(I[Landroid/view/View;)V

    .line 258
    :cond_0
    return-void
.end method

.method final varargs b([Landroid/view/View;)V
    .locals 1

    .prologue
    .line 361
    invoke-virtual {p0}, Lcom/my/target/ef;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2346
    const/16 v0, 0x12c

    invoke-direct {p0, v0, p1}, Lcom/my/target/ef;->a(I[Landroid/view/View;)V

    .line 365
    :cond_0
    return-void
.end method

.method final varargs c([Landroid/view/View;)V
    .locals 8

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 369
    .line 2527
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2529
    iget-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    sget-object v3, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v4, v6, [F

    aput v5, v4, v1

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2530
    iget-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    sget-object v3, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v4, v6, [F

    aput v5, v4, v1

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2531
    iget-object v0, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    sget-object v3, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v4, v6, [F

    aput v5, v4, v1

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2532
    iget-object v0, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    sget-object v3, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v4, v6, [F

    aput v5, v4, v1

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2533
    iget-object v0, p0, Lcom/my/target/ef;->bH:Landroid/widget/TextView;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v6, [F

    aput v5, v4, v1

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2534
    iget-object v0, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v6, [F

    aput v5, v4, v1

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2535
    iget-object v0, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2537
    iget-object v0, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v6, [F

    aput v7, v4, v1

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2539
    :cond_0
    sget-object v0, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v6, [F

    aput v5, v3, v1

    invoke-static {p0, v0, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2540
    iget-object v0, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    sget-object v3, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v4, v6, [F

    aput v7, v4, v1

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2541
    iget-object v0, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    sget-object v3, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v4, v6, [F

    aput v7, v4, v1

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2542
    iget-object v0, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    sget-object v3, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v4, v6, [F

    aput v7, v4, v1

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2543
    iget-object v0, p0, Lcom/my/target/ef;->bH:Landroid/widget/TextView;

    sget-object v3, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v4, v6, [F

    aput v7, v4, v1

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2544
    iget-object v0, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    sget-object v3, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v4, v6, [F

    aput v7, v4, v1

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2545
    sget-object v0, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v3, v6, [F

    aput v7, v3, v1

    invoke-static {p0, v0, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2546
    iget-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    sget-object v3, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v4, v6, [F

    aput v7, v4, v1

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2547
    iget-object v0, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    sget-object v3, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v4, v6, [F

    aput v7, v4, v1

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 2549
    :goto_0
    if-gtz v0, :cond_1

    aget-object v3, p1, v1

    .line 2551
    sget-object v4, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v5, v6, [F

    aput v7, v5, v1

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2549
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2554
    :cond_1
    iget-object v0, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2556
    iget-object v0, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2558
    :cond_2
    iget-object v0, p0, Lcom/my/target/ef;->bH:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2559
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 2560
    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 2561
    new-instance v1, Lcom/my/target/ef$2;

    invoke-direct {v1, p0}, Lcom/my/target/ef$2;-><init>(Lcom/my/target/ef;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2594
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 2595
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 370
    return-void
.end method

.method protected final onLayout(ZIIII)V
    .locals 8

    .prologue
    .line 412
    invoke-virtual {p0}, Lcom/my/target/ef;->getMeasuredWidth()I

    move-result v0

    .line 413
    invoke-virtual {p0}, Lcom/my/target/ef;->getMeasuredHeight()I

    move-result v1

    .line 416
    iget-object v2, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    invoke-virtual {v2}, Lcom/my/target/bv;->getMeasuredHeight()I

    move-result v2

    .line 417
    iget-object v3, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    invoke-virtual {v3}, Lcom/my/target/bv;->getMeasuredWidth()I

    move-result v3

    .line 418
    sub-int v4, v1, v2

    div-int/lit8 v4, v4, 0x2

    .line 419
    iget-object v5, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    iget v6, p0, Lcom/my/target/ef;->bM:I

    iget v7, p0, Lcom/my/target/ef;->bM:I

    add-int/2addr v7, v3

    add-int/2addr v2, v4

    invoke-virtual {v5, v6, v4, v7, v2}, Lcom/my/target/bv;->layout(IIII)V

    .line 421
    iget-object v2, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v2

    .line 422
    iget-object v4, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v4

    .line 423
    sub-int/2addr v1, v4

    div-int/lit8 v1, v1, 0x2

    .line 424
    sub-int v2, v0, v2

    iget v5, p0, Lcom/my/target/ef;->bM:I

    sub-int/2addr v2, v5

    .line 425
    iget-object v5, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    iget v6, p0, Lcom/my/target/ef;->bM:I

    sub-int/2addr v0, v6

    add-int/2addr v4, v1

    invoke-virtual {v5, v2, v1, v0, v4}, Landroid/widget/Button;->layout(IIII)V

    .line 427
    iget v0, p0, Lcom/my/target/ef;->bM:I

    add-int/2addr v0, v3

    iget v1, p0, Lcom/my/target/ef;->bM:I

    add-int/2addr v0, v1

    .line 428
    iget-object v1, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    iget v2, p0, Lcom/my/target/ef;->bO:I

    iget-object v3, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    invoke-virtual {v3}, Lcom/my/target/dy;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget v4, p0, Lcom/my/target/ef;->bO:I

    iget-object v5, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    invoke-virtual {v5}, Lcom/my/target/dy;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/my/target/dy;->layout(IIII)V

    .line 431
    iget-object v1, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    invoke-virtual {v2}, Lcom/my/target/dy;->getBottom()I

    move-result v2

    iget-object v3, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    invoke-virtual {v4}, Lcom/my/target/dy;->getBottom()I

    move-result v4

    iget-object v5, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/widget/LinearLayout;->layout(IIII)V

    .line 432
    iget-object v1, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    invoke-virtual {v2}, Lcom/my/target/dy;->getBottom()I

    move-result v2

    iget-object v3, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    invoke-virtual {v4}, Lcom/my/target/dy;->getBottom()I

    move-result v4

    iget-object v5, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 433
    iget-object v1, p0, Lcom/my/target/ef;->bH:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    invoke-virtual {v2}, Lcom/my/target/dy;->getBottom()I

    move-result v2

    iget-object v3, p0, Lcom/my/target/ef;->bH:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    invoke-virtual {v4}, Lcom/my/target/dy;->getBottom()I

    move-result v4

    iget-object v5, p0, Lcom/my/target/ef;->bH:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 435
    iget-object v1, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/my/target/ef;->bH:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getBottom()I

    move-result v2

    iget-object v3, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lcom/my/target/ef;->bH:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getBottom()I

    move-result v4

    iget-object v5, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 436
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 9
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/high16 v7, -0x80000000

    .line 375
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 377
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x4

    .line 379
    iget v2, p0, Lcom/my/target/ef;->bM:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v2, v1, v2

    .line 380
    iget v3, p0, Lcom/my/target/ef;->bO:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v0, v3

    .line 383
    iget v3, p0, Lcom/my/target/ef;->bN:I

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 384
    iget-object v4, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 385
    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 384
    invoke-virtual {v4, v5, v6}, Lcom/my/target/bv;->measure(II)V

    .line 388
    iget-object v4, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    iget v6, p0, Lcom/my/target/ef;->bO:I

    mul-int/lit8 v6, v6, 0x2

    sub-int/2addr v3, v6

    .line 389
    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 388
    invoke-virtual {v4, v5, v3}, Landroid/widget/Button;->measure(II)V

    .line 392
    iget-object v3, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    invoke-virtual {v3}, Lcom/my/target/bv;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/my/target/ef;->bM:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    .line 393
    iget-object v3, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/my/target/dy;->measure(II)V

    .line 394
    iget-object v3, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/LinearLayout;->measure(II)V

    .line 395
    iget-object v3, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/TextView;->measure(II)V

    .line 396
    iget-object v3, p0, Lcom/my/target/ef;->bH:Landroid/widget/TextView;

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget-object v5, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    invoke-virtual {v5}, Lcom/my/target/dy;->getMeasuredHeight()I

    move-result v5

    sub-int v5, v0, v5

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/TextView;->measure(II)V

    .line 397
    iget-object v3, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v3, v2, v0}, Landroid/widget/TextView;->measure(II)V

    .line 399
    iget-object v0, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    invoke-virtual {v0}, Lcom/my/target/dy;->getMeasuredHeight()I

    move-result v0

    iget-object v2, p0, Lcom/my/target/ef;->bH:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 400
    iget-object v2, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 402
    iget-object v2, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 405
    :cond_0
    iget-object v2, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v2

    iget-object v3, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    invoke-virtual {v3}, Lcom/my/target/bv;->getMeasuredHeight()I

    move-result v3

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v2, p0, Lcom/my/target/ef;->bO:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 406
    invoke-virtual {p0, v1, v0}, Lcom/my/target/ef;->setMeasuredDimension(II)V

    .line 407
    return-void
.end method

.method public final setBanner(Lcom/my/target/core/models/banners/h;)V
    .locals 9
    .param p1, "banner"    # Lcom/my/target/core/models/banners/h;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/16 v6, 0x8

    .line 169
    iget-object v0, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    invoke-virtual {v0}, Lcom/my/target/dy;->getLeftText()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    iget-object v0, p0, Lcom/my/target/ef;->bH:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getDisclaimer()Ljava/lang/String;

    move-result-object v0

    .line 172
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 174
    iget-object v1, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 175
    iget-object v1, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    :goto_0
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 183
    if-eqz v0, :cond_3

    .line 185
    iget-object v1, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    invoke-virtual {v1, v7}, Lcom/my/target/bv;->setVisibility(I)V

    .line 186
    iget-object v1, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/my/target/bv;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 193
    :goto_1
    iget-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getCtaText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 195
    const-string v0, ""

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getAgeRestrictions()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 197
    iget-object v0, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    invoke-virtual {v0}, Lcom/my/target/dy;->getRightBorderedView()Lcom/my/target/bu;

    move-result-object v0

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getAgeRestrictions()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/bu;->setText(Ljava/lang/CharSequence;)V

    .line 204
    :goto_2
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getCtaButtonColor()I

    move-result v0

    .line 205
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getCtaButtonTouchColor()I

    move-result v1

    .line 206
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getCtaButtonTextColor()I

    move-result v2

    .line 208
    iget-object v3, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    iget-object v4, p0, Lcom/my/target/ef;->aw:Lcom/my/target/cm;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/my/target/cm;->n(I)I

    move-result v4

    invoke-static {v3, v0, v1, v4}, Lcom/my/target/cm;->a(Landroid/view/View;III)V

    .line 210
    iget-object v0, p0, Lcom/my/target/ef;->bL:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTextColor(I)V

    .line 212
    const-string v0, "store"

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getNavigationType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 214
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getVotes()I

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getRating()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    .line 216
    iget-object v0, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 217
    iget-object v0, p0, Lcom/my/target/ef;->aU:Lcom/my/target/ca;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getRating()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/ca;->setRating(F)V

    .line 218
    iget-object v0, p0, Lcom/my/target/ef;->aV:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getVotes()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 226
    :goto_3
    iget-object v0, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 245
    :goto_4
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getVideoBanner()Lcom/my/target/aj;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getVideoBanner()Lcom/my/target/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/aj;->isAutoPlay()Z

    move-result v0

    if-nez v0, :cond_1

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 248
    iget-object v0, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 250
    :cond_1
    return-void

    .line 179
    :cond_2
    iget-object v0, p0, Lcom/my/target/ef;->bJ:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 190
    :cond_3
    iget-object v0, p0, Lcom/my/target/ef;->bk:Lcom/my/target/bv;

    invoke-virtual {v0, v6}, Lcom/my/target/bv;->setVisibility(I)V

    goto/16 :goto_1

    .line 201
    :cond_4
    iget-object v0, p0, Lcom/my/target/ef;->bI:Lcom/my/target/dy;

    invoke-virtual {v0}, Lcom/my/target/dy;->getRightBorderedView()Lcom/my/target/bu;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/my/target/bu;->setVisibility(I)V

    goto/16 :goto_2

    .line 222
    :cond_5
    iget-object v0, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 223
    iget-object v0, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_3

    .line 230
    :cond_6
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getDomain()Ljava/lang/String;

    move-result-object v0

    .line 231
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 233
    iget-object v1, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 234
    iget-object v1, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 242
    :goto_5
    iget-object v0, p0, Lcom/my/target/ef;->aS:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_4

    .line 238
    :cond_7
    iget-object v0, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 239
    iget-object v0, p0, Lcom/my/target/ef;->bK:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_5
.end method
