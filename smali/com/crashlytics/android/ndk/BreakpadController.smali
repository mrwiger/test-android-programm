.class Lcom/crashlytics/android/ndk/BreakpadController;
.super Ljava/lang/Object;
.source "BreakpadController.java"

# interfaces
.implements Lcom/crashlytics/android/ndk/NdkKitController;


# instance fields
.field private final context:Landroid/content/Context;

.field private final filesManager:Lcom/crashlytics/android/ndk/CrashFilesManager;

.field private final nativeApi:Lcom/crashlytics/android/ndk/NativeApi;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/crashlytics/android/ndk/NativeApi;Lcom/crashlytics/android/ndk/CrashFilesManager;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nativeApi"    # Lcom/crashlytics/android/ndk/NativeApi;
    .param p3, "filesManager"    # Lcom/crashlytics/android/ndk/CrashFilesManager;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/crashlytics/android/ndk/BreakpadController;->context:Landroid/content/Context;

    .line 20
    iput-object p2, p0, Lcom/crashlytics/android/ndk/BreakpadController;->nativeApi:Lcom/crashlytics/android/ndk/NativeApi;

    .line 21
    iput-object p3, p0, Lcom/crashlytics/android/ndk/BreakpadController;->filesManager:Lcom/crashlytics/android/ndk/CrashFilesManager;

    .line 22
    return-void
.end method


# virtual methods
.method public getNativeData()Lcom/crashlytics/android/core/CrashlyticsNdkData;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    iget-object v1, p0, Lcom/crashlytics/android/ndk/BreakpadController;->filesManager:Lcom/crashlytics/android/ndk/CrashFilesManager;

    invoke-interface {v1}, Lcom/crashlytics/android/ndk/CrashFilesManager;->getAllNativeDirectories()Ljava/util/TreeSet;

    move-result-object v0

    .line 42
    .local v0, "nativeDirectories":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/io/File;>;"
    invoke-virtual {v0}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 45
    invoke-virtual {v0}, Ljava/util/TreeSet;->pollFirst()Ljava/lang/Object;

    .line 47
    :cond_0
    new-instance v1, Lcom/crashlytics/android/core/CrashlyticsNdkData;

    invoke-direct {v1, v0}, Lcom/crashlytics/android/core/CrashlyticsNdkData;-><init>(Ljava/util/TreeSet;)V

    return-object v1
.end method

.method public initialize()Z
    .locals 7

    .prologue
    .line 26
    const/4 v3, 0x0

    .line 27
    .local v3, "initSuccess":Z
    iget-object v4, p0, Lcom/crashlytics/android/ndk/BreakpadController;->filesManager:Lcom/crashlytics/android/ndk/CrashFilesManager;

    invoke-interface {v4}, Lcom/crashlytics/android/ndk/CrashFilesManager;->getNewNativeDirectory()Ljava/io/File;

    move-result-object v0

    .line 29
    .local v0, "crashReportDirectory":Ljava/io/File;
    if-eqz v0, :cond_0

    .line 30
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v1

    .line 31
    .local v1, "crashReportPath":Ljava/lang/String;
    iget-object v4, p0, Lcom/crashlytics/android/ndk/BreakpadController;->nativeApi:Lcom/crashlytics/android/ndk/NativeApi;

    iget-object v5, p0, Lcom/crashlytics/android/ndk/BreakpadController;->context:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v5

    invoke-interface {v4, v1, v5}, Lcom/crashlytics/android/ndk/NativeApi;->initialize(Ljava/lang/String;Landroid/content/res/AssetManager;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 36
    .end local v1    # "crashReportPath":Ljava/lang/String;
    :cond_0
    :goto_0
    return v3

    .line 33
    :catch_0
    move-exception v2

    .line 34
    .local v2, "e":Ljava/io/IOException;
    invoke-static {}, Lio/fabric/sdk/android/Fabric;->getLogger()Lio/fabric/sdk/android/Logger;

    move-result-object v4

    const-string v5, "CrashlyticsNdk"

    const-string v6, "Error initializing CrashlyticsNdk"

    invoke-interface {v4, v5, v6, v2}, Lio/fabric/sdk/android/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
