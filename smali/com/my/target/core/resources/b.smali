.class public final Lcom/my/target/core/resources/b;
.super Ljava/lang/Object;
.source "InterstitialAdResources.java"


# direct methods
.method private static a(FILandroid/graphics/Paint;Landroid/graphics/Canvas;)Landroid/graphics/Canvas;
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v9, 0x1

    const/high16 v8, 0x42700000    # 60.0f

    const/high16 v7, 0x41b80000    # 23.0f

    const/high16 v6, 0x42180000    # 38.0f

    .line 352
    const/high16 v0, 0x40400000    # 3.0f

    mul-float/2addr v0, p0

    .line 354
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 356
    invoke-virtual {v1, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 357
    const/high16 v2, -0x78000000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 358
    new-instance v2, Landroid/graphics/RectF;

    int-to-float v3, p1

    int-to-float v4, p1

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 359
    invoke-virtual {p3, v2, v1}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 361
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 362
    invoke-virtual {v1, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 363
    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 364
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 365
    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 366
    div-int/lit8 v2, p1, 0x2

    int-to-float v2, v2

    div-int/lit8 v3, p1, 0x2

    int-to-float v3, v3

    div-int/lit8 v4, p1, 0x2

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v0, v5

    sub-float/2addr v4, v5

    invoke-virtual {p3, v2, v3, v4, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 368
    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 369
    const/4 v0, -0x1

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 370
    invoke-virtual {p2, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 372
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 373
    sget-object v1, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 374
    mul-float v1, v7, p0

    mul-float v2, v6, p0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 375
    mul-float v1, v7, p0

    mul-float v2, v8, p0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 376
    mul-float v1, v6, p0

    mul-float v2, v8, p0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 377
    mul-float v1, v6, p0

    mul-float v2, v6, p0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 379
    const/high16 v1, 0x42600000    # 56.0f

    mul-float/2addr v1, p0

    const/high16 v2, 0x41d80000    # 27.0f

    mul-float/2addr v2, p0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 380
    const/high16 v1, 0x42600000    # 56.0f

    mul-float/2addr v1, p0

    const/high16 v2, 0x428e0000    # 71.0f

    mul-float/2addr v2, p0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 381
    mul-float v1, v6, p0

    mul-float v2, v8, p0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 382
    mul-float v1, v6, p0

    mul-float v2, v6, p0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 383
    mul-float v1, v7, p0

    mul-float v2, v6, p0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 385
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 387
    invoke-virtual {p3, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 388
    return-object p3
.end method

.method public static b(II)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v6, 0x41200000    # 10.0f

    .line 268
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 273
    int-to-float v2, p0

    int-to-float v3, p0

    div-float/2addr v3, v6

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v3, p0

    int-to-float v4, p0

    div-float/2addr v4, v6

    add-float/2addr v3, v4

    float-to-int v3, v3

    :try_start_0
    invoke-static {v2, v3, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 279
    :goto_0
    if-nez v1, :cond_0

    .line 294
    :goto_1
    return-object v0

    .line 277
    :catch_0
    move-exception v1

    const-string v1, "cannot build play icon: OOME"

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    move-object v1, v0

    goto :goto_0

    .line 284
    :cond_0
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 286
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 287
    const/4 v3, 0x0

    const v4, -0x333334

    invoke-virtual {v2, v5, v3, v5, v4}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 289
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 290
    invoke-virtual {v2, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 291
    new-instance v3, Landroid/graphics/RectF;

    int-to-float v4, p0

    div-float/2addr v4, v6

    int-to-float v5, p0

    div-float/2addr v5, v6

    int-to-float v6, p0

    int-to-float v7, p0

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 292
    invoke-virtual {v0, v3, v2}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    move-object v0, v1

    .line 294
    goto :goto_1
.end method

.method public static d(I)Landroid/graphics/Bitmap;
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/high16 v10, 0x42b80000    # 92.0f

    const/high16 v9, 0x42820000    # 65.0f

    const/high16 v8, 0x420c0000    # 35.0f

    const/high16 v7, 0x42400000    # 48.0f

    .line 300
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 305
    :try_start_0
    invoke-static {p0, p0, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 311
    :goto_0
    if-nez v1, :cond_0

    .line 344
    :goto_1
    return-object v0

    .line 309
    :catch_0
    move-exception v1

    const-string v1, "cannot build icon: OOME"

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    move-object v1, v0

    goto :goto_0

    .line 316
    :cond_0
    int-to-float v0, p0

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v0, v2

    .line 318
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 319
    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 321
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 323
    const/high16 v4, 0x40400000    # 3.0f

    mul-float/2addr v4, v0

    .line 325
    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 326
    const/4 v4, -0x1

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 327
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 329
    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 330
    sget-object v5, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v4, v5}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 331
    mul-float v5, v7, v0

    const/high16 v6, 0x41900000    # 18.0f

    mul-float/2addr v6, v0

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 332
    const/high16 v5, 0x40e00000    # 7.0f

    mul-float/2addr v5, v0

    const/high16 v6, 0x42480000    # 50.0f

    mul-float/2addr v6, v0

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 333
    mul-float v5, v7, v0

    const/high16 v6, 0x42a40000    # 82.0f

    mul-float/2addr v6, v0

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 334
    mul-float v5, v7, v0

    mul-float v6, v9, v0

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 336
    mul-float v5, v10, v0

    mul-float v6, v9, v0

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 337
    mul-float v5, v10, v0

    mul-float v6, v8, v0

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 338
    mul-float v5, v7, v0

    mul-float/2addr v0, v8

    invoke-virtual {v4, v5, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 340
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 342
    invoke-virtual {v3, v4, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    move-object v0, v1

    .line 344
    goto :goto_1
.end method

.method public static getPlayIcon(I)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "d"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 184
    div-int/lit8 v1, p0, 0x4

    div-int/lit8 v2, p0, 0x20

    add-int/2addr v2, v1

    .line 185
    div-int/lit8 v3, p0, 0x8

    .line 187
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 192
    :try_start_0
    invoke-static {p0, p0, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 198
    :goto_0
    if-nez v1, :cond_0

    .line 236
    :goto_1
    return-object v0

    .line 196
    :catch_0
    move-exception v1

    const-string v1, "cannot build play icon: OOME"

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    move-object v1, v0

    goto :goto_0

    .line 203
    :cond_0
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 205
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 207
    invoke-virtual {v4, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 208
    const/high16 v5, -0x78000000

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 209
    new-instance v5, Landroid/graphics/RectF;

    int-to-float v6, p0

    int-to-float v7, p0

    invoke-direct {v5, v8, v8, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 210
    invoke-virtual {v0, v5, v4}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 212
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 214
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 215
    invoke-virtual {v0, v4}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    .line 217
    const/high16 v5, 0x40800000    # 4.0f

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 218
    const v5, -0xff540e

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 219
    sget-object v5, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 220
    invoke-virtual {v4, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 222
    new-instance v5, Landroid/graphics/Point;

    mul-int/lit8 v6, v3, 0x3

    invoke-direct {v5, v6, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 223
    new-instance v6, Landroid/graphics/Point;

    mul-int/lit8 v7, v3, 0x3

    sub-int v2, p0, v2

    invoke-direct {v6, v7, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 224
    new-instance v2, Landroid/graphics/Point;

    mul-int/lit8 v3, v3, 0x2

    sub-int v3, p0, v3

    div-int/lit8 v7, p0, 0x2

    invoke-direct {v2, v3, v7}, Landroid/graphics/Point;-><init>(II)V

    .line 226
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 227
    sget-object v7, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v3, v7}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 228
    iget v7, v5, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    iget v8, v5, Landroid/graphics/Point;->y:I

    int-to-float v8, v8

    invoke-virtual {v3, v7, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 229
    iget v7, v6, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    invoke-virtual {v3, v7, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 230
    iget v6, v2, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {v3, v6, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 231
    iget v2, v5, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    invoke-virtual {v3, v2, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 232
    invoke-virtual {v3}, Landroid/graphics/Path;->close()V

    .line 234
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    move-object v0, v1

    .line 236
    goto/16 :goto_1
.end method

.method public static getVolumeOffIcon(I)Landroid/graphics/Bitmap;
    .locals 11
    .param p0, "diameter"    # I

    .prologue
    const/4 v0, 0x0

    const/high16 v10, 0x42a40000    # 82.0f

    const/high16 v9, 0x42780000    # 62.0f

    const/high16 v8, 0x42700000    # 60.0f

    const/high16 v7, 0x42200000    # 40.0f

    .line 144
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 149
    :try_start_0
    invoke-static {p0, p0, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 155
    :goto_0
    if-nez v1, :cond_0

    .line 179
    :goto_1
    return-object v0

    .line 153
    :catch_0
    move-exception v1

    const-string v1, "cannot build icon: OOME"

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    move-object v1, v0

    goto :goto_0

    .line 160
    :cond_0
    int-to-float v0, p0

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v0, v2

    .line 162
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 163
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 164
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 166
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 168
    invoke-static {v0, p0, v2, v3}, Lcom/my/target/core/resources/b;->a(FILandroid/graphics/Paint;Landroid/graphics/Canvas;)Landroid/graphics/Canvas;

    .line 170
    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 171
    sget-object v5, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v4, v5}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 172
    mul-float v5, v9, v0

    mul-float v6, v7, v0

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 173
    mul-float v5, v10, v0

    mul-float v6, v8, v0

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 174
    mul-float v5, v9, v0

    mul-float v6, v8, v0

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 175
    mul-float v5, v10, v0

    mul-float/2addr v0, v7

    invoke-virtual {v4, v5, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 177
    invoke-virtual {v3, v4, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    move-object v0, v1

    .line 179
    goto :goto_1
.end method

.method public static getVolumeOnIcon(I)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "diameter"    # I

    .prologue
    const/4 v0, 0x0

    const/high16 v2, 0x42b40000    # 90.0f

    const/high16 v3, -0x3ccc0000    # -180.0f

    const/4 v4, 0x0

    .line 107
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 112
    :try_start_0
    invoke-static {p0, p0, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 118
    :goto_0
    if-nez v6, :cond_0

    .line 139
    :goto_1
    return-object v0

    .line 116
    :catch_0
    move-exception v1

    const-string v1, "cannot build icon: OOME"

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    move-object v6, v0

    goto :goto_0

    .line 123
    :cond_0
    int-to-float v0, p0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float v7, v0, v1

    .line 125
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 126
    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 127
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 129
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 131
    invoke-static {v7, p0, v5, v0}, Lcom/my/target/core/resources/b;->a(FILandroid/graphics/Paint;Landroid/graphics/Canvas;)Landroid/graphics/Canvas;

    .line 133
    new-instance v1, Landroid/graphics/RectF;

    const/high16 v8, 0x42640000    # 57.0f

    mul-float/2addr v8, v7

    const/high16 v9, 0x42340000    # 45.0f

    mul-float/2addr v9, v7

    const/high16 v10, 0x42860000    # 67.0f

    mul-float/2addr v10, v7

    const/high16 v11, 0x425c0000    # 55.0f

    mul-float/2addr v11, v7

    invoke-direct {v1, v8, v9, v10, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 134
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 136
    new-instance v1, Landroid/graphics/RectF;

    const/high16 v8, 0x42500000    # 52.0f

    mul-float/2addr v8, v7

    const/high16 v9, 0x42200000    # 40.0f

    mul-float/2addr v9, v7

    const/high16 v10, 0x42900000    # 72.0f

    mul-float/2addr v10, v7

    const/high16 v11, 0x42700000    # 60.0f

    mul-float/2addr v7, v11

    invoke-direct {v1, v8, v9, v10, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 137
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    move-object v0, v6

    .line 139
    goto :goto_1
.end method
