.class public Lcom/my/target/ap;
.super Lcom/my/target/aq;
.source "ProgressStat.java"


# static fields
.field public static final dl:F = -1.0f


# instance fields
.field private dm:F

.field private value:F


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/high16 v1, -0x40800000    # -1.0f

    .line 19
    const-string v0, "playheadReachedValue"

    invoke-direct {p0, v0, p1}, Lcom/my/target/aq;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    iput v1, p0, Lcom/my/target/ap;->value:F

    .line 15
    iput v1, p0, Lcom/my/target/ap;->dm:F

    .line 20
    return-void
.end method

.method public static v(Ljava/lang/String;)Lcom/my/target/ap;
    .locals 1

    .prologue
    .line 11
    new-instance v0, Lcom/my/target/ap;

    invoke-direct {v0, p0}, Lcom/my/target/ap;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public Z()F
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/my/target/ap;->value:F

    return v0
.end method

.method public aa()F
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/my/target/ap;->dm:F

    return v0
.end method

.method public c(F)V
    .locals 0

    .prologue
    .line 29
    iput p1, p0, Lcom/my/target/ap;->value:F

    .line 30
    return-void
.end method

.method public d(F)V
    .locals 0

    .prologue
    .line 39
    iput p1, p0, Lcom/my/target/ap;->dm:F

    .line 40
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ProgressStat{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 46
    const-string v1, "value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/my/target/ap;->value:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 47
    const-string v1, ", pvalue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/my/target/ap;->dm:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 48
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 49
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
