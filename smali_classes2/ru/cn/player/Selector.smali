.class public interface abstract Lru/cn/player/Selector;
.super Ljava/lang/Object;
.source "Selector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/player/Selector$TrackSelector;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lru/cn/player/TrackInfo;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract canAdaptiveAudioStreaming()Z
.end method

.method public abstract canAdaptiveVideoStreaming()Z
.end method

.method public abstract canDeactivateAudio()Z
.end method

.method public abstract canDeactivateSubtitles()Z
.end method

.method public abstract disableAudio()V
.end method

.method public abstract disableSubtitles()V
.end method

.method public abstract disableVideo()V
.end method

.method public abstract getAudioTracks()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract getAudioTracksCount()I
.end method

.method public abstract getCurrentAudioTrackIndex()I
.end method

.method public abstract getCurrentSubtitlesTrackIndex()I
.end method

.method public abstract getCurrentVideoTrackIndex()I
.end method

.method public abstract getSubtitleTracks()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract getSubtitlesTracksCount()I
.end method

.method public abstract getVideoTracks()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract getVideoTracksCount()I
.end method

.method public abstract selectAudioTrack(I)V
.end method

.method public abstract selectSubtitlesTrack(I)V
.end method

.method public abstract selectVideoTrack(I)V
.end method

.method public abstract setAdaptiveVideoStream()V
.end method
