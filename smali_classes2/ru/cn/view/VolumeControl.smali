.class public Lru/cn/view/VolumeControl;
.super Landroid/widget/LinearLayout;
.source "VolumeControl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/view/VolumeControl$VolumeControlListener;
    }
.end annotation


# instance fields
.field private audioManager:Landroid/media/AudioManager;

.field private listener:Lru/cn/view/VolumeControl$VolumeControlListener;

.field private seekBar:Landroid/widget/SeekBar;

.field private volumeImage:Landroid/widget/ImageView;

.field private volumeMuted:Z

.field private volumeText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v6, 0x0

    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    iput-boolean v6, p0, Lru/cn/view/VolumeControl;->volumeMuted:Z

    .line 43
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 44
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f0c0091

    invoke-virtual {v1, v4, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 46
    invoke-direct {p0}, Lru/cn/view/VolumeControl;->getSavedVolume()I

    move-result v3

    .line 48
    .local v3, "volumeLevel":I
    const-string v4, "audio"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/AudioManager;

    iput-object v4, p0, Lru/cn/view/VolumeControl;->audioManager:Landroid/media/AudioManager;

    .line 49
    iget-object v4, p0, Lru/cn/view/VolumeControl;->audioManager:Landroid/media/AudioManager;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    .line 50
    .local v0, "currentVolume":I
    invoke-direct {p0, v0}, Lru/cn/view/VolumeControl;->saveVolume(I)V

    .line 52
    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 53
    move v3, v0

    .line 56
    :cond_0
    invoke-direct {p0, v3}, Lru/cn/view/VolumeControl;->setVolumeLevel(I)V

    .line 58
    invoke-direct {p0}, Lru/cn/view/VolumeControl;->getSavedMute()Z

    move-result v2

    .line 59
    .local v2, "mute":Z
    if-eqz v2, :cond_1

    .line 60
    const/4 v4, 0x1

    iput-boolean v4, p0, Lru/cn/view/VolumeControl;->volumeMuted:Z

    .line 61
    invoke-direct {p0, v6}, Lru/cn/view/VolumeControl;->setVolumeLevel(I)V

    .line 63
    :cond_1
    return-void
.end method

.method static synthetic access$000(Lru/cn/view/VolumeControl;)Lru/cn/view/VolumeControl$VolumeControlListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/view/VolumeControl;

    .prologue
    .line 21
    iget-object v0, p0, Lru/cn/view/VolumeControl;->listener:Lru/cn/view/VolumeControl$VolumeControlListener;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/view/VolumeControl;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lru/cn/view/VolumeControl;

    .prologue
    .line 21
    iget-object v0, p0, Lru/cn/view/VolumeControl;->audioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/view/VolumeControl;I)V
    .locals 0
    .param p0, "x0"    # Lru/cn/view/VolumeControl;
    .param p1, "x1"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lru/cn/view/VolumeControl;->setVolumeLevel(I)V

    return-void
.end method

.method static synthetic access$300(Lru/cn/view/VolumeControl;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/view/VolumeControl;

    .prologue
    .line 21
    invoke-direct {p0}, Lru/cn/view/VolumeControl;->updateView()V

    return-void
.end method

.method private getSavedMute()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 295
    invoke-virtual {p0}, Lru/cn/view/VolumeControl;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "Peers_tv_volume_settings"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 297
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v1, "mute"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method private getSavedVolume()I
    .locals 4

    .prologue
    .line 289
    invoke-virtual {p0}, Lru/cn/view/VolumeControl;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "Peers_tv_volume_settings"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 291
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v1, "volumeLevel"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method private saveMute(Z)V
    .locals 5
    .param p1, "mute"    # Z

    .prologue
    .line 281
    invoke-virtual {p0}, Lru/cn/view/VolumeControl;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "Peers_tv_volume_settings"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 283
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 284
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "mute"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 285
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 286
    return-void
.end method

.method private saveVolume(I)V
    .locals 5
    .param p1, "volumeLevel"    # I

    .prologue
    .line 273
    invoke-virtual {p0}, Lru/cn/view/VolumeControl;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "Peers_tv_volume_settings"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 275
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 276
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "volumeLevel"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 277
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 278
    return-void
.end method

.method private setMute(Z)V
    .locals 3
    .param p1, "muted"    # Z

    .prologue
    .line 266
    :try_start_0
    iget-object v1, p0, Lru/cn/view/VolumeControl;->audioManager:Landroid/media/AudioManager;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, p1}, Landroid/media/AudioManager;->setStreamMute(IZ)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 270
    :goto_0
    return-void

    .line 267
    :catch_0
    move-exception v0

    .line 268
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-static {v0}, Lru/cn/utils/Logger;->logException(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private setVolumeLevel(I)V
    .locals 4
    .param p1, "volumeLevel"    # I

    .prologue
    .line 258
    :try_start_0
    iget-object v1, p0, Lru/cn/view/VolumeControl;->audioManager:Landroid/media/AudioManager;

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 262
    :goto_0
    return-void

    .line 259
    :catch_0
    move-exception v0

    .line 260
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-static {v0}, Lru/cn/utils/Logger;->logException(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private updateView()V
    .locals 3

    .prologue
    .line 245
    iget-object v1, p0, Lru/cn/view/VolumeControl;->audioManager:Landroid/media/AudioManager;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    .line 246
    .local v0, "volume":I
    iget-object v1, p0, Lru/cn/view/VolumeControl;->volumeText:Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    iget-object v1, p0, Lru/cn/view/VolumeControl;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 249
    iget-boolean v1, p0, Lru/cn/view/VolumeControl;->volumeMuted:Z

    if-eqz v1, :cond_0

    .line 250
    iget-object v1, p0, Lru/cn/view/VolumeControl;->volumeImage:Landroid/widget/ImageView;

    const v2, 0x7f080264

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 254
    :goto_0
    return-void

    .line 252
    :cond_0
    iget-object v1, p0, Lru/cn/view/VolumeControl;->volumeImage:Landroid/widget/ImageView;

    const v2, 0x7f080263

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v5, 0x8

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 197
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    .line 198
    .local v1, "keyCode":I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_1

    move v0, v2

    .line 200
    .local v0, "down":Z
    :goto_0
    sparse-switch v1, :sswitch_data_0

    move v2, v3

    .line 241
    :cond_0
    :goto_1
    return v2

    .end local v0    # "down":Z
    :cond_1
    move v0, v3

    .line 198
    goto :goto_0

    .line 202
    .restart local v0    # "down":Z
    :sswitch_0
    if-eqz v0, :cond_0

    .line 203
    invoke-virtual {p0}, Lru/cn/view/VolumeControl;->volumeUp()V

    .line 204
    invoke-virtual {p0}, Lru/cn/view/VolumeControl;->getVisibility()I

    move-result v4

    if-ne v4, v5, :cond_0

    .line 205
    invoke-virtual {p0, v3}, Lru/cn/view/VolumeControl;->setVisibility(I)V

    goto :goto_1

    .line 211
    :sswitch_1
    if-eqz v0, :cond_0

    .line 212
    invoke-virtual {p0}, Lru/cn/view/VolumeControl;->volumeDown()V

    .line 213
    invoke-virtual {p0}, Lru/cn/view/VolumeControl;->getVisibility()I

    move-result v4

    if-ne v4, v5, :cond_0

    .line 214
    invoke-virtual {p0, v3}, Lru/cn/view/VolumeControl;->setVisibility(I)V

    goto :goto_1

    .line 220
    :sswitch_2
    if-eqz v0, :cond_0

    .line 221
    invoke-virtual {p0}, Lru/cn/view/VolumeControl;->volumeMute()V

    .line 222
    invoke-virtual {p0}, Lru/cn/view/VolumeControl;->getVisibility()I

    move-result v4

    if-ne v4, v5, :cond_0

    .line 223
    invoke-virtual {p0, v3}, Lru/cn/view/VolumeControl;->setVisibility(I)V

    goto :goto_1

    .line 229
    :sswitch_3
    if-eqz v0, :cond_0

    .line 230
    invoke-virtual {p0}, Lru/cn/view/VolumeControl;->volumeUp()V

    goto :goto_1

    .line 235
    :sswitch_4
    if-eqz v0, :cond_0

    .line 236
    invoke-virtual {p0}, Lru/cn/view/VolumeControl;->volumeDown()V

    goto :goto_1

    .line 200
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_3
        0x14 -> :sswitch_4
        0x18 -> :sswitch_0
        0x19 -> :sswitch_1
        0xa4 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 67
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 69
    const v1, 0x7f09019b

    invoke-virtual {p0, v1}, Lru/cn/view/VolumeControl;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lru/cn/view/VolumeControl;->seekBar:Landroid/widget/SeekBar;

    .line 70
    iget-object v1, p0, Lru/cn/view/VolumeControl;->audioManager:Landroid/media/AudioManager;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    .line 71
    .local v0, "maxVolume":I
    iget-object v1, p0, Lru/cn/view/VolumeControl;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setMax(I)V

    .line 73
    iget-object v1, p0, Lru/cn/view/VolumeControl;->seekBar:Landroid/widget/SeekBar;

    new-instance v2, Lru/cn/view/VolumeControl$1;

    invoke-direct {v2, p0}, Lru/cn/view/VolumeControl$1;-><init>(Lru/cn/view/VolumeControl;)V

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 96
    const v1, 0x7f0901f6

    invoke-virtual {p0, v1}, Lru/cn/view/VolumeControl;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lru/cn/view/VolumeControl;->volumeText:Landroid/widget/TextView;

    .line 98
    const v1, 0x7f0901f4

    invoke-virtual {p0, v1}, Lru/cn/view/VolumeControl;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lru/cn/view/VolumeControl;->volumeImage:Landroid/widget/ImageView;

    .line 99
    iget-object v1, p0, Lru/cn/view/VolumeControl;->volumeImage:Landroid/widget/ImageView;

    new-instance v2, Lru/cn/view/VolumeControl$2;

    invoke-direct {v2, p0}, Lru/cn/view/VolumeControl$2;-><init>(Lru/cn/view/VolumeControl;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    invoke-direct {p0}, Lru/cn/view/VolumeControl;->updateView()V

    .line 106
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 178
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 179
    .local v0, "handled":Z
    if-eqz v0, :cond_0

    .line 192
    :goto_0
    return v1

    .line 182
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_2

    .line 183
    const/16 v2, 0x9

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v2

    float-to-double v2, v2

    const-wide/16 v4, 0x0

    cmpg-double v2, v2, v4

    if-gez v2, :cond_1

    .line 184
    invoke-virtual {p0}, Lru/cn/view/VolumeControl;->volumeDown()V

    goto :goto_0

    .line 186
    :cond_1
    invoke-virtual {p0}, Lru/cn/view/VolumeControl;->volumeUp()V

    goto :goto_0

    .line 192
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 4
    .param p1, "visibility"    # I

    .prologue
    .line 158
    packed-switch p1, :pswitch_data_0

    .line 173
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onWindowVisibilityChanged(I)V

    .line 174
    return-void

    .line 160
    :pswitch_0
    invoke-direct {p0}, Lru/cn/view/VolumeControl;->getSavedVolume()I

    move-result v0

    .line 161
    .local v0, "previousVolume":I
    iget-object v2, p0, Lru/cn/view/VolumeControl;->audioManager:Landroid/media/AudioManager;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    .line 162
    .local v1, "volume":I
    iget-boolean v2, p0, Lru/cn/view/VolumeControl;->volumeMuted:Z

    if-nez v2, :cond_0

    .line 163
    invoke-direct {p0, v1}, Lru/cn/view/VolumeControl;->saveVolume(I)V

    .line 166
    :cond_0
    iget-boolean v2, p0, Lru/cn/view/VolumeControl;->volumeMuted:Z

    invoke-direct {p0, v2}, Lru/cn/view/VolumeControl;->saveMute(Z)V

    .line 167
    invoke-direct {p0, v0}, Lru/cn/view/VolumeControl;->setVolumeLevel(I)V

    goto :goto_0

    .line 158
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public setListener(Lru/cn/view/VolumeControl$VolumeControlListener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/view/VolumeControl$VolumeControlListener;

    .prologue
    .line 37
    iput-object p1, p0, Lru/cn/view/VolumeControl;->listener:Lru/cn/view/VolumeControl$VolumeControlListener;

    .line 38
    return-void
.end method

.method public volumeDown()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 127
    iget-object v1, p0, Lru/cn/view/VolumeControl;->audioManager:Landroid/media/AudioManager;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    .line 128
    .local v0, "volume":I
    add-int/lit8 v0, v0, -0x1

    .line 129
    if-ltz v0, :cond_0

    .line 130
    invoke-direct {p0, v3}, Lru/cn/view/VolumeControl;->setMute(Z)V

    .line 131
    invoke-direct {p0, v0}, Lru/cn/view/VolumeControl;->setVolumeLevel(I)V

    .line 133
    :cond_0
    iget-boolean v1, p0, Lru/cn/view/VolumeControl;->volumeMuted:Z

    if-eqz v1, :cond_1

    .line 134
    iput-boolean v3, p0, Lru/cn/view/VolumeControl;->volumeMuted:Z

    .line 135
    invoke-direct {p0, v3}, Lru/cn/view/VolumeControl;->saveMute(Z)V

    .line 138
    :cond_1
    invoke-direct {p0}, Lru/cn/view/VolumeControl;->updateView()V

    .line 139
    return-void
.end method

.method public volumeMute()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 142
    iget-boolean v1, p0, Lru/cn/view/VolumeControl;->volumeMuted:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lru/cn/view/VolumeControl;->volumeMuted:Z

    .line 143
    iget-boolean v1, p0, Lru/cn/view/VolumeControl;->volumeMuted:Z

    if-eqz v1, :cond_1

    .line 144
    iget-object v1, p0, Lru/cn/view/VolumeControl;->audioManager:Landroid/media/AudioManager;

    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    .line 145
    .local v0, "volume":I
    invoke-direct {p0, v0}, Lru/cn/view/VolumeControl;->saveVolume(I)V

    .line 146
    invoke-direct {p0, v2}, Lru/cn/view/VolumeControl;->setVolumeLevel(I)V

    .line 152
    :goto_1
    iget-boolean v1, p0, Lru/cn/view/VolumeControl;->volumeMuted:Z

    invoke-direct {p0, v1}, Lru/cn/view/VolumeControl;->saveMute(Z)V

    .line 153
    invoke-direct {p0}, Lru/cn/view/VolumeControl;->updateView()V

    .line 154
    return-void

    .end local v0    # "volume":I
    :cond_0
    move v1, v2

    .line 142
    goto :goto_0

    .line 148
    :cond_1
    invoke-direct {p0}, Lru/cn/view/VolumeControl;->getSavedVolume()I

    move-result v0

    .line 149
    .restart local v0    # "volume":I
    invoke-direct {p0, v0}, Lru/cn/view/VolumeControl;->setVolumeLevel(I)V

    goto :goto_1
.end method

.method public volumeUp()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 109
    iget-object v2, p0, Lru/cn/view/VolumeControl;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, v4}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    .line 110
    .local v1, "volume":I
    add-int/lit8 v1, v1, 0x1

    .line 111
    iget-object v2, p0, Lru/cn/view/VolumeControl;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, v4}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    .line 113
    .local v0, "maxVolume":I
    if-gt v1, v0, :cond_0

    .line 114
    invoke-direct {p0, v3}, Lru/cn/view/VolumeControl;->setMute(Z)V

    .line 115
    invoke-direct {p0, v1}, Lru/cn/view/VolumeControl;->setVolumeLevel(I)V

    .line 118
    :cond_0
    iget-boolean v2, p0, Lru/cn/view/VolumeControl;->volumeMuted:Z

    if-eqz v2, :cond_1

    .line 119
    iput-boolean v3, p0, Lru/cn/view/VolumeControl;->volumeMuted:Z

    .line 120
    invoke-direct {p0, v3}, Lru/cn/view/VolumeControl;->saveMute(Z)V

    .line 123
    :cond_1
    invoke-direct {p0}, Lru/cn/view/VolumeControl;->updateView()V

    .line 124
    return-void
.end method
