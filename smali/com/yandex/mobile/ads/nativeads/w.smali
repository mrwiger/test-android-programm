.class Lcom/yandex/mobile/ads/nativeads/w;
.super Lcom/yandex/mobile/ads/nativeads/j;
.source "SourceFile"

# interfaces
.implements Lcom/yandex/mobile/ads/nativeads/x;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/yandex/mobile/ads/nativeads/o;Lcom/yandex/mobile/ads/nativeads/d;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Lcom/yandex/mobile/ads/nativeads/j;-><init>(Landroid/content/Context;Lcom/yandex/mobile/ads/nativeads/o;Lcom/yandex/mobile/ads/nativeads/d;)V

    .line 29
    return-void
.end method


# virtual methods
.method protected a()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 35
    const-string v1, "body"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    const-string v1, "domain"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37
    const-string v1, "sponsored"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    const-string v1, "title"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    return-object v0
.end method

.method public bindContentAd(Lcom/yandex/mobile/ads/nativeads/NativeContentAdView;)V
    .locals 2
    .param p1, "contentAdView"    # Lcom/yandex/mobile/ads/nativeads/NativeContentAdView;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/yandex/mobile/ads/nativeads/NativeAdException;
        }
    .end annotation

    .prologue
    .line 51
    if-eqz p1, :cond_0

    .line 52
    new-instance v0, Lcom/yandex/mobile/ads/nativeads/y;

    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/w;->a:Lcom/yandex/mobile/ads/nativeads/d;

    invoke-direct {v0, p1, v1}, Lcom/yandex/mobile/ads/nativeads/y;-><init>(Lcom/yandex/mobile/ads/nativeads/NativeContentAdView;Lcom/yandex/mobile/ads/nativeads/d;)V

    invoke-virtual {p0, v0}, Lcom/yandex/mobile/ads/nativeads/w;->a(Lcom/yandex/mobile/ads/nativeads/q;)V

    .line 53
    invoke-virtual {p1, p0}, Lcom/yandex/mobile/ads/nativeads/NativeContentAdView;->a(Lcom/yandex/mobile/ads/nativeads/j;)V

    .line 55
    :cond_0
    return-void
.end method
