.class Lru/cn/tv/mobile/authorization/SignInFragment$2;
.super Landroid/os/AsyncTask;
.source "SignInFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/mobile/authorization/SignInFragment;->proceedAuthorization(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/mobile/authorization/SignInFragment;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lru/cn/tv/mobile/authorization/SignInFragment;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/mobile/authorization/SignInFragment;

    .prologue
    .line 86
    iput-object p1, p0, Lru/cn/tv/mobile/authorization/SignInFragment$2;->this$0:Lru/cn/tv/mobile/authorization/SignInFragment;

    iput-object p2, p0, Lru/cn/tv/mobile/authorization/SignInFragment$2;->val$url:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 89
    iget-object v0, p0, Lru/cn/tv/mobile/authorization/SignInFragment$2;->this$0:Lru/cn/tv/mobile/authorization/SignInFragment;

    invoke-static {v0}, Lru/cn/tv/mobile/authorization/SignInFragment;->access$200(Lru/cn/tv/mobile/authorization/SignInFragment;)Lru/cn/api/authorization/CodeAuthorizationChallenge;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/mobile/authorization/SignInFragment$2;->val$url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lru/cn/api/authorization/CodeAuthorizationChallenge;->proceedAuthorization(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 86
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lru/cn/tv/mobile/authorization/SignInFragment$2;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "aBoolean"    # Ljava/lang/Boolean;

    .prologue
    .line 94
    iget-object v1, p0, Lru/cn/tv/mobile/authorization/SignInFragment$2;->this$0:Lru/cn/tv/mobile/authorization/SignInFragment;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lru/cn/tv/mobile/authorization/SignInFragment;->access$300(Lru/cn/tv/mobile/authorization/SignInFragment;I)V

    .line 95
    return-void

    .line 94
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 86
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lru/cn/tv/mobile/authorization/SignInFragment$2;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
