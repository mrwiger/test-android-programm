.class public Lru/cn/api/registry/retrofit/SupportedOfficeIdiomsDeserializer;
.super Ljava/lang/Object;
.source "SupportedOfficeIdiomsDeserializer.java"

# interfaces
.implements Lcom/google/gson/JsonDeserializer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gson/JsonDeserializer",
        "<",
        "Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/gson/JsonParseException;
        }
    .end annotation

    .prologue
    .line 12
    invoke-virtual {p0, p1, p2, p3}, Lru/cn/api/registry/retrofit/SupportedOfficeIdiomsDeserializer;->deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

    move-result-object v0

    return-object v0
.end method

.method public deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;
    .locals 6
    .param p1, "json"    # Lcom/google/gson/JsonElement;
    .param p2, "typeOfT"    # Ljava/lang/reflect/Type;
    .param p3, "context"    # Lcom/google/gson/JsonDeserializationContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/gson/JsonParseException;
        }
    .end annotation

    .prologue
    .line 17
    invoke-static {}, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;->values()[Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

    move-result-object v1

    .line 18
    .local v1, "types":[Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;
    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, v1, v2

    .line 19
    .local v0, "supportedOfficeIdioms":Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;
    invoke-virtual {v0}, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;->getValue()I

    move-result v4

    invoke-virtual {p1}, Lcom/google/gson/JsonElement;->getAsInt()I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 23
    .end local v0    # "supportedOfficeIdioms":Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;
    :goto_1
    return-object v0

    .line 18
    .restart local v0    # "supportedOfficeIdioms":Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 23
    .end local v0    # "supportedOfficeIdioms":Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
