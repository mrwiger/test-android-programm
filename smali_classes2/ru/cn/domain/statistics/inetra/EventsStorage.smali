.class interface abstract Lru/cn/domain/statistics/inetra/EventsStorage;
.super Ljava/lang/Object;
.source "EventsStorage.java"


# virtual methods
.method public abstract addEvent(Lru/cn/domain/statistics/inetra/TrackingEvent;)V
.end method

.method public abstract allEvents()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lru/cn/domain/statistics/inetra/TrackingEvent;",
            ">;"
        }
    .end annotation
.end method

.method public abstract contains(Lru/cn/domain/statistics/inetra/TrackingEvent;)Z
.end method

.method public abstract removeEvent(Lru/cn/domain/statistics/inetra/TrackingEvent;)V
.end method
