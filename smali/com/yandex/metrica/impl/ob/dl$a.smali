.class Lcom/yandex/metrica/impl/ob/dl$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/impl/ob/dl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field a:Lcom/yandex/metrica/impl/ob/dl;

.field private b:Lcom/yandex/metrica/impl/utils/h;


# direct methods
.method private constructor <init>(Lcom/yandex/metrica/impl/ob/dl;)V
    .locals 2

    .prologue
    .line 323
    new-instance v0, Lcom/yandex/metrica/impl/utils/h;

    const-string v1, "com.yandex.metrica.synchronization.deviceid"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/utils/h;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1, v0}, Lcom/yandex/metrica/impl/ob/dl$a;-><init>(Lcom/yandex/metrica/impl/ob/dl;Lcom/yandex/metrica/impl/utils/h;)V

    .line 324
    return-void
.end method

.method synthetic constructor <init>(Lcom/yandex/metrica/impl/ob/dl;B)V
    .locals 0

    .prologue
    .line 315
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/dl$a;-><init>(Lcom/yandex/metrica/impl/ob/dl;)V

    return-void
.end method

.method constructor <init>(Lcom/yandex/metrica/impl/ob/dl;Lcom/yandex/metrica/impl/utils/h;)V
    .locals 0

    .prologue
    .line 327
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 328
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/dl$a;->a:Lcom/yandex/metrica/impl/ob/dl;

    .line 329
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/dl$a;->b:Lcom/yandex/metrica/impl/utils/h;

    .line 330
    return-void
.end method


# virtual methods
.method a()Lcom/yandex/metrica/impl/ob/dl;
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/dl$a;->a:Lcom/yandex/metrica/impl/ob/dl;

    return-object v0
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 342
    const/4 v0, 0x0

    .line 343
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 347
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/dl$a;->a()Lcom/yandex/metrica/impl/ob/dl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/dl;->f()Lcom/yandex/metrica/impl/ob/dn;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/yandex/metrica/impl/ob/dn;->a(Landroid/content/Context;)Ljava/lang/String;

    .line 351
    :try_start_0
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/dl$a;->b:Lcom/yandex/metrica/impl/utils/h;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/utils/h;->c()V

    .line 353
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/dl$a;->a()Lcom/yandex/metrica/impl/ob/dl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/dl;->f()Lcom/yandex/metrica/impl/ob/dn;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/yandex/metrica/impl/ob/dn;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 355
    invoke-virtual {p0, p1, p2, v1}, Lcom/yandex/metrica/impl/ob/dl$a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 357
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/dl$a;->b:Lcom/yandex/metrica/impl/utils/h;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/utils/h;->b()V
    :try_end_0
    .catch Lcom/yandex/metrica/impl/utils/h$a; {:try_start_0 .. :try_end_0} :catch_0

    .line 361
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 366
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 367
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 368
    const/4 p3, 0x0

    .line 395
    :goto_0
    return-object p3

    .line 371
    :cond_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/dl$a;->a()Lcom/yandex/metrica/impl/ob/dl;

    move-result-object v0

    invoke-virtual {v0, p1, p3}, Lcom/yandex/metrica/impl/ob/dl;->f(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 376
    :cond_1
    invoke-virtual {p2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 377
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/dl$a;->a()Lcom/yandex/metrica/impl/ob/dl;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/yandex/metrica/impl/ob/dl;->f(Landroid/content/Context;Ljava/lang/String;)V

    .line 1022
    const-string v0, "20799a27-fa80-4b36-b2db-0f8141f24180"

    invoke-static {p1, v0}, Lcom/yandex/metrica/YandexMetrica;->getReporter(Landroid/content/Context;Ljava/lang/String;)Lcom/yandex/metrica/IReporter;

    move-result-object v0

    .line 378
    const-string v1, "update_snapshot"

    new-instance v2, Lcom/yandex/metrica/impl/ob/dl$c;

    invoke-direct {v2, p1, p3, p2}, Lcom/yandex/metrica/impl/ob/dl$c;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    invoke-interface {v0, v1, v2}, Lcom/yandex/metrica/IReporter;->reportEvent(Ljava/lang/String;Ljava/util/Map;)V

    move-object p3, p2

    .line 380
    goto :goto_0

    .line 382
    :cond_2
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 384
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/dl$a;->a()Lcom/yandex/metrica/impl/ob/dl;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/yandex/metrica/impl/ob/dl;->f(Landroid/content/Context;Ljava/lang/String;)V

    .line 2022
    const-string v0, "20799a27-fa80-4b36-b2db-0f8141f24180"

    invoke-static {p1, v0}, Lcom/yandex/metrica/YandexMetrica;->getReporter(Landroid/content/Context;Ljava/lang/String;)Lcom/yandex/metrica/IReporter;

    move-result-object v0

    .line 386
    const-string v1, "wtf_situation. App has id and elector hasn\'t"

    new-instance v2, Lcom/yandex/metrica/impl/ob/dl$c;

    invoke-direct {v2, p1, p3, p2}, Lcom/yandex/metrica/impl/ob/dl$c;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    invoke-interface {v0, v1, v2}, Lcom/yandex/metrica/IReporter;->reportEvent(Ljava/lang/String;Ljava/util/Map;)V

    move-object p3, p2

    .line 388
    goto :goto_0

    .line 391
    :cond_3
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/dl$a;->a()Lcom/yandex/metrica/impl/ob/dl;

    move-result-object v0

    invoke-virtual {v0, p1, p3}, Lcom/yandex/metrica/impl/ob/dl;->f(Landroid/content/Context;Ljava/lang/String;)V

    .line 3022
    const-string v0, "20799a27-fa80-4b36-b2db-0f8141f24180"

    invoke-static {p1, v0}, Lcom/yandex/metrica/YandexMetrica;->getReporter(Landroid/content/Context;Ljava/lang/String;)Lcom/yandex/metrica/IReporter;

    move-result-object v0

    .line 393
    const-string v1, "overlapping_device_id"

    new-instance v2, Lcom/yandex/metrica/impl/ob/dl$c;

    invoke-direct {v2, p1, p3, p2}, Lcom/yandex/metrica/impl/ob/dl$c;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    invoke-interface {v0, v1, v2}, Lcom/yandex/metrica/IReporter;->reportEvent(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0
.end method
