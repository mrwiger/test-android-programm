.class public Lru/cn/api/authorization/Authorization;
.super Ljava/lang/Object;
.source "Authorization.java"


# direct methods
.method private static anonymousToken(Lru/cn/api/authorization/retrofit/AuthApi;)Lru/cn/api/authorization/replies/TokenReply;
    .locals 3
    .param p0, "authApi"    # Lru/cn/api/authorization/retrofit/AuthApi;

    .prologue
    .line 258
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 259
    .local v0, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "grant_type"

    const-string v2, "inetra:anonymous"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    invoke-static {p0, v0}, Lru/cn/api/authorization/Authorization;->token(Lru/cn/api/authorization/retrofit/AuthApi;Ljava/util/Map;)Lru/cn/api/authorization/replies/TokenReply;

    move-result-object v1

    return-object v1
.end method

.method private static authorizeUri(Lru/cn/api/registry/replies/Service;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 4
    .param p0, "service"    # Lru/cn/api/registry/replies/Service;
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "redirectUri"    # Ljava/lang/String;
    .param p3, "accessToken"    # Ljava/lang/String;
    .param p4, "state"    # Ljava/lang/String;

    .prologue
    .line 317
    invoke-virtual {p0}, Lru/cn/api/registry/replies/Service;->getLocation()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "authorize"

    .line 318
    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "response_type"

    .line 319
    invoke-virtual {v1, v2, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "client_id"

    const-string v3, "29783051"

    .line 320
    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 322
    .local v0, "b":Landroid/net/Uri$Builder;
    if-eqz p2, :cond_0

    .line 323
    const-string v1, "redirect_uri"

    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 326
    :cond_0
    if-eqz p3, :cond_1

    .line 327
    const-string v1, "access_token"

    invoke-virtual {v0, v1, p3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 330
    :cond_1
    if-eqz p4, :cond_2

    .line 331
    const-string v1, "state"

    invoke-virtual {v0, v1, p4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 334
    :cond_2
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static clientId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    const-string v0, "29783051"

    return-object v0
.end method

.method public static codeChallenge(Landroid/content/Context;JLjava/lang/String;)Lru/cn/api/authorization/CodeAuthorizationChallenge;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contractorId"    # J
    .param p3, "redirectUri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 44
    sget-object v1, Lru/cn/api/registry/replies/Service$Type;->auth:Lru/cn/api/registry/replies/Service$Type;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {p0, v1, v2}, Lru/cn/api/ServiceLocator;->getRegistryService(Landroid/content/Context;Lru/cn/api/registry/replies/Service$Type;Ljava/lang/Long;)Lru/cn/api/registry/replies/Service;

    move-result-object v0

    .line 45
    .local v0, "service":Lru/cn/api/registry/replies/Service;
    const-string v1, "code"

    invoke-static {v0, v1, p3, v4, v4}, Lru/cn/api/authorization/Authorization;->authorizeUri(Lru/cn/api/registry/replies/Service;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 47
    .local v6, "authorizeUri":Landroid/net/Uri;
    invoke-static {p0, p1, p2}, Lru/cn/api/ServiceLocator;->auth(Landroid/content/Context;J)Lru/cn/api/authorization/retrofit/AuthApi;

    move-result-object v3

    .line 48
    .local v3, "auth":Lru/cn/api/authorization/retrofit/AuthApi;
    new-instance v1, Lru/cn/api/authorization/CodeAuthorizationChallenge;

    move-object v2, p0

    move-wide v4, p1

    move-object v7, p3

    invoke-direct/range {v1 .. v7}, Lru/cn/api/authorization/CodeAuthorizationChallenge;-><init>(Landroid/content/Context;Lru/cn/api/authorization/retrofit/AuthApi;JLandroid/net/Uri;Ljava/lang/String;)V

    return-object v1
.end method

.method private static codeToken(Lru/cn/api/authorization/retrofit/AuthApi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lru/cn/api/authorization/replies/TokenReply;
    .locals 3
    .param p0, "authApi"    # Lru/cn/api/authorization/retrofit/AuthApi;
    .param p1, "code"    # Ljava/lang/String;
    .param p2, "redirectUri"    # Ljava/lang/String;
    .param p3, "anonymousToken"    # Ljava/lang/String;

    .prologue
    .line 274
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 275
    .local v0, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "grant_type"

    const-string v2, "authorization_code"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    const-string v1, "code"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    if-eqz p2, :cond_0

    .line 278
    const-string v1, "redirect_uri"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    :cond_0
    if-eqz p3, :cond_1

    .line 282
    const-string v1, "anonymous_token"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    :cond_1
    invoke-static {p0, v0}, Lru/cn/api/authorization/Authorization;->token(Lru/cn/api/authorization/retrofit/AuthApi;Ljava/util/Map;)Lru/cn/api/authorization/replies/TokenReply;

    move-result-object v1

    return-object v1
.end method

.method static completeCodeToken(Landroid/content/Context;Lru/cn/api/authorization/retrofit/AuthApi;Ljava/lang/String;Ljava/lang/String;J)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "authApi"    # Lru/cn/api/authorization/retrofit/AuthApi;
    .param p2, "code"    # Ljava/lang/String;
    .param p3, "redirectUri"    # Ljava/lang/String;
    .param p4, "contractorId"    # J

    .prologue
    .line 77
    const/4 v3, 0x0

    .line 79
    .local v3, "anonymousToken":Ljava/lang/String;
    invoke-static {p0, p4, p5}, Lru/cn/api/authorization/AccountStorage;->getAccount(Landroid/content/Context;J)Lru/cn/api/authorization/Account;

    move-result-object v1

    .line 80
    .local v1, "account":Lru/cn/api/authorization/Account;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lru/cn/api/authorization/Account;->getToken()Lru/cn/api/authorization/Account$Token;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-virtual {v1}, Lru/cn/api/authorization/Account;->getLogin()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_0

    .line 81
    invoke-virtual {v1}, Lru/cn/api/authorization/Account;->getToken()Lru/cn/api/authorization/Account$Token;

    move-result-object v8

    iget-object v3, v8, Lru/cn/api/authorization/Account$Token;->accessToken:Ljava/lang/String;

    .line 86
    :cond_0
    :try_start_0
    invoke-static {p1, p2, p3, v3}, Lru/cn/api/authorization/Authorization;->codeToken(Lru/cn/api/authorization/retrofit/AuthApi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lru/cn/api/authorization/replies/TokenReply;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 92
    .local v6, "reply":Lru/cn/api/authorization/replies/TokenReply;
    invoke-virtual {v6}, Lru/cn/api/authorization/replies/TokenReply;->token()Lru/cn/api/authorization/Account$Token;

    move-result-object v7

    .line 93
    .local v7, "token":Lru/cn/api/authorization/Account$Token;
    if-eqz v7, :cond_2

    .line 94
    const/4 v5, 0x0

    .line 96
    .local v5, "info":Lru/cn/api/authorization/replies/AccountInfo;
    :try_start_1
    iget-object v8, v7, Lru/cn/api/authorization/Account$Token;->accessToken:Ljava/lang/String;

    invoke-interface {p1, v8}, Lru/cn/api/authorization/retrofit/AuthApi;->account(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v8

    invoke-virtual {v8}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Lru/cn/api/authorization/replies/AccountInfo;

    move-object v5, v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 101
    :goto_0
    const-string v2, "unknown"

    .line 102
    .local v2, "accountName":Ljava/lang/String;
    if-eqz v5, :cond_1

    .line 103
    iget-object v2, v5, Lru/cn/api/authorization/replies/AccountInfo;->accountName:Ljava/lang/String;

    .line 106
    :cond_1
    new-instance v1, Lru/cn/api/authorization/Account;

    .end local v1    # "account":Lru/cn/api/authorization/Account;
    invoke-direct {v1, v2, v7}, Lru/cn/api/authorization/Account;-><init>(Ljava/lang/String;Lru/cn/api/authorization/Account$Token;)V

    .line 107
    .restart local v1    # "account":Lru/cn/api/authorization/Account;
    invoke-static {p0, v1, p4, p5}, Lru/cn/api/authorization/AccountStorage;->saveAccount(Landroid/content/Context;Lru/cn/api/authorization/Account;J)V

    .line 109
    invoke-static {p0}, Lru/cn/api/authorization/Authorization;->resetCache(Landroid/content/Context;)V

    .line 112
    if-eqz v5, :cond_2

    .line 113
    invoke-static {p0, v5, p4, p5}, Lru/cn/api/authorization/Authorization;->trackLogin(Landroid/content/Context;Lru/cn/api/authorization/replies/AccountInfo;J)V

    .line 116
    .end local v2    # "accountName":Ljava/lang/String;
    .end local v5    # "info":Lru/cn/api/authorization/replies/AccountInfo;
    :cond_2
    const/4 v8, 0x1

    .end local v6    # "reply":Lru/cn/api/authorization/replies/TokenReply;
    .end local v7    # "token":Lru/cn/api/authorization/Account$Token;
    :goto_1
    return v8

    .line 87
    :catch_0
    move-exception v4

    .line 88
    .local v4, "e":Ljava/lang/Exception;
    const-string v8, "Authorization"

    const-string v9, "Unable to retrieve token by code"

    invoke-static {v8, v9, v4}, Lru/cn/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 89
    const/4 v8, 0x0

    goto :goto_1

    .line 97
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v5    # "info":Lru/cn/api/authorization/replies/AccountInfo;
    .restart local v6    # "reply":Lru/cn/api/authorization/replies/TokenReply;
    .restart local v7    # "token":Lru/cn/api/authorization/Account$Token;
    :catch_1
    move-exception v4

    .line 98
    .restart local v4    # "e":Ljava/lang/Exception;
    const-string v8, "Authorization"

    const-string v9, "Unable to get account name"

    invoke-static {v8, v9, v4}, Lru/cn/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static completeDeviceCode(Landroid/content/Context;Ljava/lang/String;J)Z
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "deviceCode"    # Ljava/lang/String;
    .param p2, "contractorId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 202
    const/4 v3, 0x0

    .line 204
    .local v3, "anonymousToken":Ljava/lang/String;
    invoke-static {p0, p2, p3}, Lru/cn/api/authorization/AccountStorage;->getAccount(Landroid/content/Context;J)Lru/cn/api/authorization/Account;

    move-result-object v1

    .line 205
    .local v1, "account":Lru/cn/api/authorization/Account;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lru/cn/api/authorization/Account;->getToken()Lru/cn/api/authorization/Account$Token;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-virtual {v1}, Lru/cn/api/authorization/Account;->getLogin()Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_0

    .line 206
    invoke-virtual {v1}, Lru/cn/api/authorization/Account;->getToken()Lru/cn/api/authorization/Account$Token;

    move-result-object v9

    iget-object v3, v9, Lru/cn/api/authorization/Account$Token;->accessToken:Ljava/lang/String;

    .line 209
    :cond_0
    invoke-static {p0, p2, p3}, Lru/cn/api/ServiceLocator;->auth(Landroid/content/Context;J)Lru/cn/api/authorization/retrofit/AuthApi;

    move-result-object v4

    .line 213
    .local v4, "authApi":Lru/cn/api/authorization/retrofit/AuthApi;
    :try_start_0
    invoke-static {v4, p1, v3}, Lru/cn/api/authorization/Authorization;->deviceToken(Lru/cn/api/authorization/retrofit/AuthApi;Ljava/lang/String;Ljava/lang/String;)Lru/cn/api/authorization/replies/TokenReply;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 218
    .local v7, "reply":Lru/cn/api/authorization/replies/TokenReply;
    invoke-virtual {v7}, Lru/cn/api/authorization/replies/TokenReply;->token()Lru/cn/api/authorization/Account$Token;

    move-result-object v8

    .line 219
    .local v8, "token":Lru/cn/api/authorization/Account$Token;
    if-eqz v8, :cond_2

    .line 220
    const/4 v6, 0x0

    .line 222
    .local v6, "info":Lru/cn/api/authorization/replies/AccountInfo;
    :try_start_1
    iget-object v9, v8, Lru/cn/api/authorization/Account$Token;->accessToken:Ljava/lang/String;

    invoke-interface {v4, v9}, Lru/cn/api/authorization/retrofit/AuthApi;->account(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v9

    invoke-virtual {v9}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Lru/cn/api/authorization/replies/AccountInfo;

    move-object v6, v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 227
    :goto_0
    const-string v2, "unknown"

    .line 228
    .local v2, "accountName":Ljava/lang/String;
    if-eqz v6, :cond_1

    .line 229
    iget-object v2, v6, Lru/cn/api/authorization/replies/AccountInfo;->accountName:Ljava/lang/String;

    .line 232
    :cond_1
    new-instance v1, Lru/cn/api/authorization/Account;

    .end local v1    # "account":Lru/cn/api/authorization/Account;
    invoke-direct {v1, v2, v8}, Lru/cn/api/authorization/Account;-><init>(Ljava/lang/String;Lru/cn/api/authorization/Account$Token;)V

    .line 233
    .restart local v1    # "account":Lru/cn/api/authorization/Account;
    invoke-static {p0, v1, p2, p3}, Lru/cn/api/authorization/AccountStorage;->saveAccount(Landroid/content/Context;Lru/cn/api/authorization/Account;J)V

    .line 235
    invoke-static {p0}, Lru/cn/api/authorization/Authorization;->resetCache(Landroid/content/Context;)V

    .line 238
    if-eqz v6, :cond_2

    .line 239
    invoke-static {p0, v6, p2, p3}, Lru/cn/api/authorization/Authorization;->trackLogin(Landroid/content/Context;Lru/cn/api/authorization/replies/AccountInfo;J)V

    .line 242
    .end local v2    # "accountName":Ljava/lang/String;
    .end local v6    # "info":Lru/cn/api/authorization/replies/AccountInfo;
    :cond_2
    const/4 v9, 0x1

    .end local v7    # "reply":Lru/cn/api/authorization/replies/TokenReply;
    .end local v8    # "token":Lru/cn/api/authorization/Account$Token;
    :goto_1
    return v9

    .line 214
    :catch_0
    move-exception v5

    .line 215
    .local v5, "e":Ljava/lang/Exception;
    const/4 v9, 0x0

    goto :goto_1

    .line 223
    .end local v5    # "e":Ljava/lang/Exception;
    .restart local v6    # "info":Lru/cn/api/authorization/replies/AccountInfo;
    .restart local v7    # "reply":Lru/cn/api/authorization/replies/TokenReply;
    .restart local v8    # "token":Lru/cn/api/authorization/Account$Token;
    :catch_1
    move-exception v5

    .line 224
    .restart local v5    # "e":Ljava/lang/Exception;
    const-string v9, "Authorization"

    const-string v10, "Unable to get account name"

    invoke-static {v9, v10, v5}, Lru/cn/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static deviceCode(Landroid/content/Context;J)Lio/reactivex/Single;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contractorId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "J)",
            "Lio/reactivex/Single",
            "<",
            "Lru/cn/api/authorization/replies/DeviceCodeReply;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    :try_start_0
    invoke-static {p0, p1, p2}, Lru/cn/api/ServiceLocator;->auth(Landroid/content/Context;J)Lru/cn/api/authorization/retrofit/AuthApi;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 198
    .local v0, "authApi":Lru/cn/api/authorization/retrofit/AuthApi;
    const-string v2, "29783051"

    invoke-interface {v0, v2}, Lru/cn/api/authorization/retrofit/AuthApi;->deviceCode(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v2

    .end local v0    # "authApi":Lru/cn/api/authorization/retrofit/AuthApi;
    :goto_0
    return-object v2

    .line 194
    :catch_0
    move-exception v1

    .line 195
    .local v1, "e":Ljava/lang/Exception;
    invoke-static {v1}, Lio/reactivex/Single;->error(Ljava/lang/Throwable;)Lio/reactivex/Single;

    move-result-object v2

    goto :goto_0
.end method

.method private static deviceToken(Lru/cn/api/authorization/retrofit/AuthApi;Ljava/lang/String;Ljava/lang/String;)Lru/cn/api/authorization/replies/TokenReply;
    .locals 3
    .param p0, "authApi"    # Lru/cn/api/authorization/retrofit/AuthApi;
    .param p1, "deviceCode"    # Ljava/lang/String;
    .param p2, "anonymousToken"    # Ljava/lang/String;

    .prologue
    .line 289
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 290
    .local v0, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "grant_type"

    const-string v2, "inetra:device_code"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    const-string v1, "device_code"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    if-eqz p2, :cond_0

    .line 294
    const-string v1, "anonymous_token"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    :cond_0
    invoke-static {p0, v0}, Lru/cn/api/authorization/Authorization;->token(Lru/cn/api/authorization/retrofit/AuthApi;Ljava/util/Map;)Lru/cn/api/authorization/replies/TokenReply;

    move-result-object v1

    return-object v1
.end method

.method public static declared-synchronized getAccount(Landroid/content/Context;Lru/cn/api/authorization/retrofit/AuthApi;J)Lru/cn/api/authorization/Account;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "authApi"    # Lru/cn/api/authorization/retrofit/AuthApi;
    .param p2, "contractorId"    # J

    .prologue
    .line 120
    const-class v5, Lru/cn/api/authorization/Authorization;

    monitor-enter v5

    :try_start_0
    invoke-static {p0, p2, p3}, Lru/cn/api/authorization/AccountStorage;->getAccount(Landroid/content/Context;J)Lru/cn/api/authorization/Account;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 121
    .local v0, "account":Lru/cn/api/authorization/Account;
    if-nez v0, :cond_1

    .line 123
    const/4 v2, 0x0

    .line 125
    .local v2, "reply":Lru/cn/api/authorization/replies/TokenReply;
    :try_start_1
    invoke-static {p1}, Lru/cn/api/authorization/Authorization;->anonymousToken(Lru/cn/api/authorization/retrofit/AuthApi;)Lru/cn/api/authorization/replies/TokenReply;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 131
    :goto_0
    if-eqz v2, :cond_0

    :try_start_2
    iget-object v4, v2, Lru/cn/api/authorization/replies/TokenReply;->error:Ljava/lang/String;

    if-nez v4, :cond_0

    .line 132
    invoke-virtual {v2}, Lru/cn/api/authorization/replies/TokenReply;->token()Lru/cn/api/authorization/Account$Token;

    move-result-object v3

    .line 133
    .local v3, "token":Lru/cn/api/authorization/Account$Token;
    new-instance v0, Lru/cn/api/authorization/Account;

    .end local v0    # "account":Lru/cn/api/authorization/Account;
    const/4 v4, 0x0

    invoke-direct {v0, v4, v3}, Lru/cn/api/authorization/Account;-><init>(Ljava/lang/String;Lru/cn/api/authorization/Account$Token;)V

    .line 134
    .restart local v0    # "account":Lru/cn/api/authorization/Account;
    invoke-static {p0, v0, p2, p3}, Lru/cn/api/authorization/AccountStorage;->saveAccount(Landroid/content/Context;Lru/cn/api/authorization/Account;J)V

    .line 137
    iget-object v4, v3, Lru/cn/api/authorization/Account$Token;->accessToken:Ljava/lang/String;

    invoke-static {p0, p1, v4, p2, p3}, Lru/cn/api/authorization/Authorization;->trackLogin(Landroid/content/Context;Lru/cn/api/authorization/retrofit/AuthApi;Ljava/lang/String;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 160
    .end local v2    # "reply":Lru/cn/api/authorization/replies/TokenReply;
    .end local v3    # "token":Lru/cn/api/authorization/Account$Token;
    :cond_0
    :goto_1
    monitor-exit v5

    return-object v0

    .line 126
    .restart local v2    # "reply":Lru/cn/api/authorization/replies/TokenReply;
    :catch_0
    move-exception v1

    .line 127
    .local v1, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v4, "Authorization"

    const-string v6, "Unable to retrieve token"

    invoke-static {v4, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 128
    invoke-static {v1}, Lru/cn/utils/Logger;->logException(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 120
    .end local v0    # "account":Lru/cn/api/authorization/Account;
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "reply":Lru/cn/api/authorization/replies/TokenReply;
    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4

    .line 140
    .restart local v0    # "account":Lru/cn/api/authorization/Account;
    :cond_1
    :try_start_4
    invoke-virtual {v0}, Lru/cn/api/authorization/Account;->getToken()Lru/cn/api/authorization/Account$Token;

    move-result-object v3

    .line 143
    .restart local v3    # "token":Lru/cn/api/authorization/Account$Token;
    if-eqz v3, :cond_0

    iget-object v4, v3, Lru/cn/api/authorization/Account$Token;->refreshToken:Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-nez v4, :cond_0

    .line 144
    const/4 v2, 0x0

    .line 146
    .restart local v2    # "reply":Lru/cn/api/authorization/replies/TokenReply;
    :try_start_5
    iget-object v4, v3, Lru/cn/api/authorization/Account$Token;->accessToken:Ljava/lang/String;

    invoke-static {p1, v4}, Lru/cn/api/authorization/Authorization;->renewToken(Lru/cn/api/authorization/retrofit/AuthApi;Ljava/lang/String;)Lru/cn/api/authorization/replies/TokenReply;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v2

    .line 151
    :goto_2
    if-eqz v2, :cond_0

    :try_start_6
    iget-object v4, v2, Lru/cn/api/authorization/replies/TokenReply;->error:Ljava/lang/String;

    if-nez v4, :cond_0

    .line 152
    invoke-virtual {v2}, Lru/cn/api/authorization/replies/TokenReply;->token()Lru/cn/api/authorization/Account$Token;

    move-result-object v3

    .line 153
    invoke-virtual {v0, v3}, Lru/cn/api/authorization/Account;->setToken(Lru/cn/api/authorization/Account$Token;)V

    .line 155
    invoke-static {p0, v0, p2, p3}, Lru/cn/api/authorization/AccountStorage;->saveAccount(Landroid/content/Context;Lru/cn/api/authorization/Account;J)V

    goto :goto_1

    .line 147
    :catch_1
    move-exception v1

    .line 148
    .restart local v1    # "e":Ljava/lang/Exception;
    const-string v4, "Authorization"

    const-string v6, "Unable to migrate to v2"

    invoke-static {v4, v6, v1}, Lru/cn/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2
.end method

.method public static getAuthToken(Landroid/content/Context;J)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contractorId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 61
    invoke-static {p0, p1, p2}, Lru/cn/api/ServiceLocator;->auth(Landroid/content/Context;J)Lru/cn/api/authorization/retrofit/AuthApi;

    move-result-object v1

    .line 62
    .local v1, "auth":Lru/cn/api/authorization/retrofit/AuthApi;
    invoke-static {p0, v1, p1, p2}, Lru/cn/api/authorization/Authorization;->getAccount(Landroid/content/Context;Lru/cn/api/authorization/retrofit/AuthApi;J)Lru/cn/api/authorization/Account;

    move-result-object v0

    .line 64
    .local v0, "account":Lru/cn/api/authorization/Account;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lru/cn/api/authorization/Account;->getToken()Lru/cn/api/authorization/Account$Token;

    move-result-object v2

    if-nez v2, :cond_1

    .line 65
    :cond_0
    const/4 v2, 0x0

    .line 67
    :goto_0
    return-object v2

    :cond_1
    invoke-virtual {v0}, Lru/cn/api/authorization/Account;->getToken()Lru/cn/api/authorization/Account$Token;

    move-result-object v2

    iget-object v2, v2, Lru/cn/api/authorization/Account$Token;->accessToken:Ljava/lang/String;

    goto :goto_0
.end method

.method public static logout(Landroid/content/Context;J)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contractorId"    # J

    .prologue
    .line 71
    invoke-static {p0, p1, p2}, Lru/cn/api/authorization/AccountStorage;->removeAccount(Landroid/content/Context;J)V

    .line 73
    invoke-static {p0}, Lru/cn/api/authorization/Authorization;->resetCache(Landroid/content/Context;)V

    .line 74
    return-void
.end method

.method public static pinAuthorizationUri(Landroid/content/Context;JLjava/lang/String;)Landroid/net/Uri;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contractorId"    # J
    .param p3, "redirectUri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 54
    sget-object v2, Lru/cn/api/registry/replies/Service$Type;->auth:Lru/cn/api/registry/replies/Service$Type;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {p0, v2, v3}, Lru/cn/api/ServiceLocator;->getRegistryService(Landroid/content/Context;Lru/cn/api/registry/replies/Service$Type;Ljava/lang/Long;)Lru/cn/api/registry/replies/Service;

    move-result-object v1

    .line 55
    .local v1, "service":Lru/cn/api/registry/replies/Service;
    invoke-static {p0, p1, p2}, Lru/cn/api/authorization/Authorization;->getAuthToken(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 57
    .local v0, "authToken":Ljava/lang/String;
    const-string v2, "pin_code"

    const/4 v3, 0x0

    invoke-static {v1, v2, p3, v0, v3}, Lru/cn/api/authorization/Authorization;->authorizeUri(Lru/cn/api/registry/replies/Service;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    return-object v2
.end method

.method static declared-synchronized renewToken(Landroid/content/Context;Lru/cn/api/authorization/retrofit/AuthApi;Lru/cn/api/authorization/Account;J)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "authApi"    # Lru/cn/api/authorization/retrofit/AuthApi;
    .param p2, "account"    # Lru/cn/api/authorization/Account;
    .param p3, "contractorId"    # J

    .prologue
    const/4 v4, 0x0

    .line 164
    const-class v5, Lru/cn/api/authorization/Authorization;

    monitor-enter v5

    :try_start_0
    invoke-virtual {p2}, Lru/cn/api/authorization/Account;->getToken()Lru/cn/api/authorization/Account$Token;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 168
    .local v3, "token":Lru/cn/api/authorization/Account$Token;
    :try_start_1
    iget-object v6, v3, Lru/cn/api/authorization/Account$Token;->refreshToken:Ljava/lang/String;

    invoke-static {p1, v6}, Lru/cn/api/authorization/Authorization;->renewToken(Lru/cn/api/authorization/retrofit/AuthApi;Ljava/lang/String;)Lru/cn/api/authorization/replies/TokenReply;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 175
    .local v2, "reply":Lru/cn/api/authorization/replies/TokenReply;
    :try_start_2
    invoke-virtual {v2}, Lru/cn/api/authorization/replies/TokenReply;->token()Lru/cn/api/authorization/Account$Token;

    move-result-object v3

    .line 176
    if-eqz v3, :cond_0

    .line 177
    invoke-virtual {p2, v3}, Lru/cn/api/authorization/Account;->setToken(Lru/cn/api/authorization/Account$Token;)V

    .line 179
    invoke-static {p0, p2, p3, p4}, Lru/cn/api/authorization/AccountStorage;->saveAccount(Landroid/content/Context;Lru/cn/api/authorization/Account;J)V

    .line 182
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->channels()Landroid/net/Uri;

    move-result-object v0

    .line 183
    .local v0, "channels":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v4, v0, v6, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 184
    iget-object v4, v3, Lru/cn/api/authorization/Account$Token;->accessToken:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 187
    .end local v0    # "channels":Landroid/net/Uri;
    .end local v2    # "reply":Lru/cn/api/authorization/replies/TokenReply;
    :cond_0
    :goto_0
    monitor-exit v5

    return-object v4

    .line 169
    :catch_0
    move-exception v1

    .line 170
    .local v1, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v6, "Authorization"

    const-string v7, "Unable to renew token"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 171
    invoke-static {v1}, Lru/cn/utils/Logger;->logException(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 164
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v3    # "token":Lru/cn/api/authorization/Account$Token;
    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4
.end method

.method private static renewToken(Lru/cn/api/authorization/retrofit/AuthApi;Ljava/lang/String;)Lru/cn/api/authorization/replies/TokenReply;
    .locals 3
    .param p0, "authApi"    # Lru/cn/api/authorization/retrofit/AuthApi;
    .param p1, "refreshToken"    # Ljava/lang/String;

    .prologue
    .line 265
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 266
    .local v0, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "grant_type"

    const-string v2, "refresh_token"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    const-string v1, "refresh_token"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    invoke-static {p0, v0}, Lru/cn/api/authorization/Authorization;->token(Lru/cn/api/authorization/retrofit/AuthApi;Ljava/util/Map;)Lru/cn/api/authorization/replies/TokenReply;

    move-result-object v1

    return-object v1
.end method

.method private static resetCache(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 248
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "content"

    .line 249
    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "ru.cn.api.tv"

    .line 250
    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "clear_cache"

    .line 251
    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 252
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 254
    .local v0, "reloadUri":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 255
    return-void
.end method

.method private static token(Lru/cn/api/authorization/retrofit/AuthApi;Ljava/util/Map;)Lru/cn/api/authorization/replies/TokenReply;
    .locals 3
    .param p0, "authApi"    # Lru/cn/api/authorization/retrofit/AuthApi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/api/authorization/retrofit/AuthApi;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lru/cn/api/authorization/replies/TokenReply;"
        }
    .end annotation

    .prologue
    .line 301
    .local p1, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {}, Lru/cn/utils/Utils;->getSignature()Ljava/lang/String;

    move-result-object v0

    .line 302
    .local v0, "signature":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 303
    const-string v1, "signature"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    :cond_0
    const-string v1, "client_id"

    const-string v2, "29783051"

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    const-string v1, "client_secret"

    const-string v2, "b4d4eb438d760da95f0acb5bc6b5c760"

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    invoke-interface {p0, p1}, Lru/cn/api/authorization/retrofit/AuthApi;->token(Ljava/util/Map;)Lio/reactivex/Single;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/authorization/replies/TokenReply;

    return-object v1
.end method

.method private static trackLogin(Landroid/content/Context;Lru/cn/api/authorization/replies/AccountInfo;J)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountInfo"    # Lru/cn/api/authorization/replies/AccountInfo;
    .param p2, "contractorId"    # J

    .prologue
    .line 339
    const-wide/16 v2, 0x2

    cmp-long v2, p2, v2

    if-eqz v2, :cond_0

    .line 349
    :goto_0
    return-void

    .line 343
    :cond_0
    :try_start_0
    invoke-static {p0}, Lru/cn/api/ServiceLocator;->getWhereAmI(Landroid/content/Context;)Lru/cn/api/registry/replies/WhereAmI;

    move-result-object v1

    .line 345
    .local v1, "whereAmI":Lru/cn/api/registry/replies/WhereAmI;
    iget-object v2, v1, Lru/cn/api/registry/replies/WhereAmI;->ipAddress:Ljava/lang/String;

    iget-wide v4, p1, Lru/cn/api/authorization/replies/AccountInfo;->accountId:J

    invoke-static {p0, v2, v4, v5}, Lru/cn/domain/statistics/AnalyticsManager;->trackLogin(Landroid/content/Context;Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 346
    .end local v1    # "whereAmI":Lru/cn/api/registry/replies/WhereAmI;
    :catch_0
    move-exception v0

    .line 347
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "Authorization"

    const-string v3, "Unable to get account info"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static trackLogin(Landroid/content/Context;Lru/cn/api/authorization/retrofit/AuthApi;Ljava/lang/String;J)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "authApi"    # Lru/cn/api/authorization/retrofit/AuthApi;
    .param p2, "accessToken"    # Ljava/lang/String;
    .param p3, "contractorId"    # J

    .prologue
    .line 353
    const-wide/16 v2, 0x2

    cmp-long v2, p3, v2

    if-eqz v2, :cond_0

    .line 363
    :goto_0
    return-void

    .line 357
    :cond_0
    :try_start_0
    invoke-interface {p1, p2}, Lru/cn/api/authorization/retrofit/AuthApi;->account(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v2

    invoke-virtual {v2}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/authorization/replies/AccountInfo;

    .line 359
    .local v0, "accountInfo":Lru/cn/api/authorization/replies/AccountInfo;
    invoke-static {p0, v0, p3, p4}, Lru/cn/api/authorization/Authorization;->trackLogin(Landroid/content/Context;Lru/cn/api/authorization/replies/AccountInfo;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 360
    .end local v0    # "accountInfo":Lru/cn/api/authorization/replies/AccountInfo;
    :catch_0
    move-exception v1

    .line 361
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "Authorization"

    const-string v3, "Unable to get ipv4"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
