.class public Lcom/my/target/bp;
.super Lcom/my/target/bm;
.source "NetworkInfoDataProvider.java"


# instance fields
.field private hK:Z

.field private hL:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/my/target/bm;-><init>()V

    .line 28
    return-void
.end method

.method private a(Landroid/net/NetworkInfo;)V
    .locals 2

    .prologue
    .line 72
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    if-nez v0, :cond_1

    .line 74
    const-string v1, "connection_type"

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/my/target/bp;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 80
    :goto_1
    return-void

    .line 74
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 78
    :cond_1
    const-string v1, "connection_type"

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {p0, v1, v0}, Lcom/my/target/bp;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_1

    :cond_2
    const-string v0, ""

    goto :goto_2
.end method


# virtual methods
.method public aS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/my/target/bp;->hL:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized collectData(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingPermission"
        }
    .end annotation

    .prologue
    .line 45
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/my/target/bp;->removeAll()V

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/bp;->hK:Z

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/bp;->hL:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    const/4 v1, 0x0

    .line 51
    :try_start_1
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 52
    if-eqz v0, :cond_1

    .line 54
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 56
    :goto_0
    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    iput-boolean v1, p0, Lcom/my/target/bp;->hK:Z

    .line 59
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/my/target/bp;->hL:Ljava/lang/String;

    .line 60
    const-string v1, "connection"

    iget-object v2, p0, Lcom/my/target/bp;->hL:Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, Lcom/my/target/bp;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 61
    invoke-direct {p0, v0}, Lcom/my/target/bp;->a(Landroid/net/NetworkInfo;)V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 68
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 64
    :catch_0
    move-exception v0

    .line 66
    :try_start_2
    const-string v0, "No permissions for access to network state"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/my/target/bp;->hK:Z

    return v0
.end method
