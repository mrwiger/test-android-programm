.class final Lru/cn/notification/commands/CommandWatchFilm;
.super Ljava/lang/Object;
.source "CommandWatchFilm.java"

# interfaces
.implements Lru/cn/notification/commands/Command;


# instance fields
.field private final filmId:J


# direct methods
.method constructor <init>(J)V
    .locals 1
    .param p1, "filmId"    # J

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-wide p1, p0, Lru/cn/notification/commands/CommandWatchFilm;->filmId:J

    .line 15
    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 20
    .local v1, "manager":Landroid/content/pm/PackageManager;
    const-string v2, "ru.cn.peers"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 22
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 23
    const-string v2, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const v3, 0x10008000

    .line 24
    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "_id"

    iget-wide v4, p0, Lru/cn/notification/commands/CommandWatchFilm;->filmId:J

    .line 25
    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 27
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 29
    :cond_0
    return-void
.end method
