.class Lcom/samsung/multiscreen/Service$4;
.super Ljava/lang/Object;
.source "Service.java"

# interfaces
.implements Lcom/samsung/multiscreen/Result;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/multiscreen/Service;->isSecurityModeSupported(Lcom/samsung/multiscreen/Result;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/samsung/multiscreen/Result",
        "<",
        "Lcom/samsung/multiscreen/Device;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/multiscreen/Service;

.field final synthetic val$result:Lcom/samsung/multiscreen/Result;


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/Service;Lcom/samsung/multiscreen/Result;)V
    .locals 0
    .param p1, "this$0"    # Lcom/samsung/multiscreen/Service;

    .prologue
    .line 315
    iput-object p1, p0, Lcom/samsung/multiscreen/Service$4;->this$0:Lcom/samsung/multiscreen/Service;

    iput-object p2, p0, Lcom/samsung/multiscreen/Service$4;->val$result:Lcom/samsung/multiscreen/Result;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/samsung/multiscreen/Error;)V
    .locals 1
    .param p1, "error"    # Lcom/samsung/multiscreen/Error;

    .prologue
    .line 338
    iget-object v0, p0, Lcom/samsung/multiscreen/Service$4;->val$result:Lcom/samsung/multiscreen/Result;

    invoke-interface {v0, p1}, Lcom/samsung/multiscreen/Result;->onError(Lcom/samsung/multiscreen/Error;)V

    .line 339
    return-void
.end method

.method public onSuccess(Lcom/samsung/multiscreen/Device;)V
    .locals 7
    .param p1, "device"    # Lcom/samsung/multiscreen/Device;

    .prologue
    const/16 v6, 0xf

    const/4 v3, 0x0

    .line 319
    if-eqz p1, :cond_1

    .line 320
    invoke-virtual {p1}, Lcom/samsung/multiscreen/Device;->getModel()Ljava/lang/String;

    move-result-object v2

    .line 321
    .local v2, "model":Ljava/lang/String;
    const/4 v0, 0x0

    .line 323
    .local v0, "TVYear":I
    const/4 v4, 0x0

    const/4 v5, 0x2

    :try_start_0
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 327
    :goto_0
    if-lt v0, v6, :cond_2

    .line 328
    iget-object v4, p0, Lcom/samsung/multiscreen/Service$4;->this$0:Lcom/samsung/multiscreen/Service;

    sget-object v5, Lcom/samsung/multiscreen/Service$SecureModeState;->Supported:Lcom/samsung/multiscreen/Service$SecureModeState;

    invoke-static {v4, v5}, Lcom/samsung/multiscreen/Service;->access$102(Lcom/samsung/multiscreen/Service;Lcom/samsung/multiscreen/Service$SecureModeState;)Lcom/samsung/multiscreen/Service$SecureModeState;

    .line 332
    :goto_1
    iget-object v4, p0, Lcom/samsung/multiscreen/Service$4;->val$result:Lcom/samsung/multiscreen/Result;

    if-lt v0, v6, :cond_0

    const/4 v3, 0x1

    :cond_0
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v4, v3}, Lcom/samsung/multiscreen/Result;->onSuccess(Ljava/lang/Object;)V

    .line 334
    .end local v0    # "TVYear":I
    .end local v2    # "model":Ljava/lang/String;
    :cond_1
    return-void

    .line 324
    .restart local v0    # "TVYear":I
    .restart local v2    # "model":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 325
    .local v1, "ex":Ljava/lang/NumberFormatException;
    iget-object v4, p0, Lcom/samsung/multiscreen/Service$4;->this$0:Lcom/samsung/multiscreen/Service;

    sget-object v5, Lcom/samsung/multiscreen/Service$SecureModeState;->NotSupported:Lcom/samsung/multiscreen/Service$SecureModeState;

    invoke-static {v4, v5}, Lcom/samsung/multiscreen/Service;->access$102(Lcom/samsung/multiscreen/Service;Lcom/samsung/multiscreen/Service$SecureModeState;)Lcom/samsung/multiscreen/Service$SecureModeState;

    goto :goto_0

    .line 330
    .end local v1    # "ex":Ljava/lang/NumberFormatException;
    :cond_2
    iget-object v4, p0, Lcom/samsung/multiscreen/Service$4;->this$0:Lcom/samsung/multiscreen/Service;

    sget-object v5, Lcom/samsung/multiscreen/Service$SecureModeState;->NotSupported:Lcom/samsung/multiscreen/Service$SecureModeState;

    invoke-static {v4, v5}, Lcom/samsung/multiscreen/Service;->access$102(Lcom/samsung/multiscreen/Service;Lcom/samsung/multiscreen/Service$SecureModeState;)Lcom/samsung/multiscreen/Service$SecureModeState;

    goto :goto_1
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 315
    check-cast p1, Lcom/samsung/multiscreen/Device;

    invoke-virtual {p0, p1}, Lcom/samsung/multiscreen/Service$4;->onSuccess(Lcom/samsung/multiscreen/Device;)V

    return-void
.end method
