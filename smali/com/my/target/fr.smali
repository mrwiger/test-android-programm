.class public final Lcom/my/target/fr;
.super Landroid/widget/RelativeLayout;
.source "InstreamAdVideoPlayer.java"

# interfaces
.implements Lcom/my/target/cc$a;
.implements Lcom/my/target/instreamads/InstreamAdPlayer;


# instance fields
.field private final J:Lcom/my/target/cc;

.field private K:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

.field private L:I

.field private M:I

.field private N:Z

.field private O:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/my/target/cc;

    invoke-direct {v0, p1}, Lcom/my/target/cc;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, v0}, Lcom/my/target/fr;-><init>(Landroid/content/Context;Lcom/my/target/cc;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;B)V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/my/target/fr;-><init>(Landroid/content/Context;C)V

    .line 60
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;C)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/my/target/fr;-><init>(Landroid/content/Context;)V

    .line 65
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/my/target/cc;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 42
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    iput-object p2, p0, Lcom/my/target/fr;->J:Lcom/my/target/cc;

    .line 44
    invoke-virtual {p2, p0}, Lcom/my/target/cc;->setVideoListener(Lcom/my/target/cc$a;)V

    .line 45
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 47
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 48
    invoke-virtual {p0, p2, v0}, Lcom/my/target/fr;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 49
    return-void
.end method


# virtual methods
.method public final L(Ljava/lang/String;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 214
    iget-object v0, p0, Lcom/my/target/fr;->K:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/my/target/fr;->K:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    invoke-interface {v0, p1}, Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;->onAdVideoError(Ljava/lang/String;)V

    .line 218
    :cond_0
    return-void
.end method

.method public final b(FF)V
    .locals 0

    .prologue
    .line 209
    return-void
.end method

.method public final bq()V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/my/target/fr;->K:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/my/target/fr;->K:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;->onAdVideoStopped()V

    .line 156
    :cond_0
    return-void
.end method

.method public final br()V
    .locals 1

    .prologue
    .line 170
    iget-boolean v0, p0, Lcom/my/target/fr;->N:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/my/target/fr;->K:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/my/target/fr;->K:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;->onAdVideoStarted()V

    .line 173
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/fr;->N:Z

    .line 175
    :cond_0
    return-void
.end method

.method public final bs()V
    .locals 1

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/my/target/fr;->O:Z

    if-eqz v0, :cond_1

    .line 182
    iget-object v0, p0, Lcom/my/target/fr;->K:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/my/target/fr;->K:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;->onAdVideoResumed()V

    .line 186
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/fr;->O:Z

    .line 188
    :cond_1
    return-void
.end method

.method public final bt()V
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/fr;->O:Z

    .line 194
    iget-object v0, p0, Lcom/my/target/fr;->K:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/my/target/fr;->K:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;->onAdVideoPaused()V

    .line 198
    :cond_0
    return-void
.end method

.method public final bu()V
    .locals 0

    .prologue
    .line 204
    return-void
.end method

.method public final bv()V
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/my/target/fr;->K:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/my/target/fr;->K:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;->onAdVideoCompleted()V

    .line 227
    :cond_0
    return-void
.end method

.method public final destroy()V
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/my/target/fr;->J:Lcom/my/target/cc;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/my/target/cc;->k(Z)V

    .line 141
    return-void
.end method

.method public final e(F)V
    .locals 1
    .param p1, "volume"    # F

    .prologue
    .line 161
    iget-object v0, p0, Lcom/my/target/fr;->K:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/my/target/fr;->K:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    invoke-interface {v0, p1}, Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;->onVolumeChanged(F)V

    .line 165
    :cond_0
    return-void
.end method

.method public final getAdPlayerListener()Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/my/target/fr;->K:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    return-object v0
.end method

.method public final getAdVideoDuration()F
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/my/target/fr;->J:Lcom/my/target/cc;

    invoke-virtual {v0}, Lcom/my/target/cc;->getVideoDuration()F

    move-result v0

    return v0
.end method

.method public final getAdVideoPosition()F
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/my/target/fr;->J:Lcom/my/target/cc;

    invoke-virtual {v0}, Lcom/my/target/cc;->getTimeElapsed()F

    move-result v0

    return v0
.end method

.method final getPlaceholderHeight()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/my/target/fr;->M:I

    return v0
.end method

.method final getPlaceholderWidth()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/my/target/fr;->L:I

    return v0
.end method

.method public final getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 94
    return-object p0
.end method

.method protected final onMeasure(II)V
    .locals 10
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v3, 0x0

    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v1, -0x80000000

    .line 232
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 233
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 234
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 235
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 237
    if-nez v5, :cond_d

    move v6, v1

    .line 242
    :goto_0
    if-nez v0, :cond_0

    move v0, v1

    .line 247
    :cond_0
    iget v5, p0, Lcom/my/target/fr;->M:I

    if-eqz v5, :cond_1

    iget v5, p0, Lcom/my/target/fr;->L:I

    if-nez v5, :cond_2

    .line 249
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 336
    :goto_1
    return-void

    .line 253
    :cond_2
    iget v5, p0, Lcom/my/target/fr;->L:I

    int-to-float v5, v5

    iget v7, p0, Lcom/my/target/fr;->M:I

    int-to-float v7, v7

    div-float v7, v5, v7

    .line 255
    const/4 v5, 0x0

    .line 256
    if-eqz v4, :cond_3

    .line 258
    int-to-float v5, v2

    int-to-float v8, v4

    div-float/2addr v5, v8

    .line 264
    :cond_3
    if-ne v6, v9, :cond_4

    if-ne v0, v9, :cond_4

    move v0, v2

    move v3, v4

    .line 333
    :goto_2
    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 334
    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 335
    invoke-super {p0, v0, v1}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    goto :goto_1

    .line 269
    :cond_4
    if-ne v6, v1, :cond_8

    if-ne v0, v1, :cond_8

    .line 271
    cmpg-float v0, v7, v5

    if-gez v0, :cond_6

    .line 273
    int-to-float v0, v4

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 275
    if-lez v2, :cond_5

    if-le v0, v2, :cond_5

    .line 277
    int-to-float v0, v2

    div-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    move v0, v2

    .line 278
    goto :goto_2

    :cond_5
    move v3, v4

    .line 285
    goto :goto_2

    .line 288
    :cond_6
    int-to-float v0, v2

    div-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 290
    if-lez v4, :cond_7

    if-le v3, v4, :cond_7

    .line 292
    int-to-float v0, v4

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    move v3, v4

    .line 293
    goto :goto_2

    :cond_7
    move v0, v2

    .line 300
    goto :goto_2

    .line 302
    :cond_8
    if-ne v6, v1, :cond_a

    if-ne v0, v9, :cond_a

    .line 304
    int-to-float v0, v4

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 306
    if-lez v2, :cond_9

    if-le v0, v2, :cond_9

    .line 308
    int-to-float v0, v2

    div-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    move v0, v2

    .line 309
    goto :goto_2

    :cond_9
    move v3, v4

    .line 316
    goto :goto_2

    .line 317
    :cond_a
    if-ne v6, v9, :cond_c

    if-ne v0, v1, :cond_c

    .line 319
    int-to-float v0, v2

    div-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 321
    if-lez v4, :cond_b

    if-le v3, v4, :cond_b

    .line 323
    int-to-float v0, v4

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    move v3, v4

    .line 324
    goto :goto_2

    :cond_b
    move v0, v2

    .line 329
    goto :goto_2

    :cond_c
    move v0, v3

    goto :goto_2

    :cond_d
    move v6, v5

    goto/16 :goto_0
.end method

.method public final pauseAdVideo()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/my/target/fr;->J:Lcom/my/target/cc;

    invoke-virtual {v0}, Lcom/my/target/cc;->pause()V

    .line 123
    return-void
.end method

.method public final playAdVideo(Landroid/net/Uri;II)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 106
    iput p2, p0, Lcom/my/target/fr;->L:I

    .line 107
    iput p3, p0, Lcom/my/target/fr;->M:I

    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/fr;->N:Z

    .line 109
    iget-object v0, p0, Lcom/my/target/fr;->J:Lcom/my/target/cc;

    invoke-virtual {v0, p1}, Lcom/my/target/cc;->c(Landroid/net/Uri;)V

    .line 110
    return-void
.end method

.method public final playAdVideo(Landroid/net/Uri;IIF)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "position"    # F

    .prologue
    .line 115
    invoke-virtual {p0, p1, p2, p3}, Lcom/my/target/fr;->playAdVideo(Landroid/net/Uri;II)V

    .line 116
    iget-object v0, p0, Lcom/my/target/fr;->J:Lcom/my/target/cc;

    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v1, p4

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/my/target/cc;->seekTo(I)V

    .line 117
    return-void
.end method

.method public final resumeAdVideo()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/my/target/fr;->J:Lcom/my/target/cc;

    invoke-virtual {v0}, Lcom/my/target/cc;->resume()V

    .line 129
    return-void
.end method

.method public final setAdPlayerListener(Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/my/target/fr;->K:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    .line 101
    return-void
.end method

.method public final setVolume(F)V
    .locals 1
    .param p1, "volume"    # F

    .prologue
    .line 146
    iget-object v0, p0, Lcom/my/target/fr;->J:Lcom/my/target/cc;

    invoke-virtual {v0, p1}, Lcom/my/target/cc;->setVolume(F)V

    .line 147
    return-void
.end method

.method public final stopAdVideo()V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/my/target/fr;->J:Lcom/my/target/cc;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/my/target/cc;->k(Z)V

    .line 135
    return-void
.end method
