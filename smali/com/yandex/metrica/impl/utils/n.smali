.class public Lcom/yandex/metrica/impl/utils/n;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/utils/n$a;
    }
.end annotation


# instance fields
.field private volatile a:J

.field private b:Lcom/yandex/metrica/impl/ob/df;

.field private c:Lcom/yandex/metrica/impl/utils/q;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/yandex/metrica/impl/utils/n;-><init>()V

    return-void
.end method

.method public static a()Lcom/yandex/metrica/impl/utils/n;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/yandex/metrica/impl/utils/n$a;->a:Lcom/yandex/metrica/impl/utils/n;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized a(JLjava/lang/Long;)V
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 49
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/yandex/metrica/impl/utils/n;->c:Lcom/yandex/metrica/impl/utils/q;

    invoke-interface {v2}, Lcom/yandex/metrica/impl/utils/q;->a()J

    move-result-wide v2

    sub-long v2, p1, v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    iput-wide v2, p0, Lcom/yandex/metrica/impl/utils/n;->a:J

    .line 51
    iget-object v2, p0, Lcom/yandex/metrica/impl/utils/n;->b:Lcom/yandex/metrica/impl/ob/df;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/yandex/metrica/impl/ob/df;->b(Z)Z

    move-result v2

    .line 52
    if-eqz v2, :cond_0

    .line 53
    if-eqz p3, :cond_2

    .line 54
    iget-object v2, p0, Lcom/yandex/metrica/impl/utils/n;->c:Lcom/yandex/metrica/impl/utils/q;

    invoke-interface {v2}, Lcom/yandex/metrica/impl/utils/q;->a()J

    move-result-wide v2

    sub-long v2, p1, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    .line 55
    iget-object v4, p0, Lcom/yandex/metrica/impl/utils/n;->b:Lcom/yandex/metrica/impl/ob/df;

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    cmp-long v2, v2, v6

    if-lez v2, :cond_1

    :goto_0
    invoke-virtual {v4, v0}, Lcom/yandex/metrica/impl/ob/df;->c(Z)Lcom/yandex/metrica/impl/ob/df;

    .line 60
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/utils/n;->b:Lcom/yandex/metrica/impl/ob/df;

    iget-wide v2, p0, Lcom/yandex/metrica/impl/utils/n;->a:J

    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/df;->a(J)Lcom/yandex/metrica/impl/ob/df;

    .line 61
    iget-object v0, p0, Lcom/yandex/metrica/impl/utils/n;->b:Lcom/yandex/metrica/impl/ob/df;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/df;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    monitor-exit p0

    return-void

    :cond_1
    move v0, v1

    .line 55
    goto :goto_0

    .line 57
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/utils/n;->b:Lcom/yandex/metrica/impl/ob/df;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/df;->c(Z)Lcom/yandex/metrica/impl/ob/df;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 43
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/cq;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/cq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/cq;->b()Lcom/yandex/metrica/impl/ob/cr;

    move-result-object v0

    .line 44
    new-instance v1, Lcom/yandex/metrica/impl/ob/df;

    invoke-direct {v1, v0}, Lcom/yandex/metrica/impl/ob/df;-><init>(Lcom/yandex/metrica/impl/ob/cr;)V

    .line 45
    new-instance v0, Lcom/yandex/metrica/impl/utils/p;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/utils/p;-><init>()V

    invoke-virtual {p0, v1, v0}, Lcom/yandex/metrica/impl/utils/n;->a(Lcom/yandex/metrica/impl/ob/df;Lcom/yandex/metrica/impl/utils/q;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 46
    monitor-exit p0

    return-void

    .line 43
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/yandex/metrica/impl/ob/df;Lcom/yandex/metrica/impl/utils/q;)V
    .locals 2

    .prologue
    .line 81
    iput-object p1, p0, Lcom/yandex/metrica/impl/utils/n;->b:Lcom/yandex/metrica/impl/ob/df;

    .line 82
    iget-object v0, p0, Lcom/yandex/metrica/impl/utils/n;->b:Lcom/yandex/metrica/impl/ob/df;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/df;->c(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/yandex/metrica/impl/utils/n;->a:J

    .line 83
    iput-object p2, p0, Lcom/yandex/metrica/impl/utils/n;->c:Lcom/yandex/metrica/impl/utils/q;

    .line 86
    return-void
.end method

.method public declared-synchronized b()J
    .locals 2

    .prologue
    .line 39
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/yandex/metrica/impl/utils/n;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()V
    .locals 2

    .prologue
    .line 67
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/utils/n;->b:Lcom/yandex/metrica/impl/ob/df;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/df;->c(Z)Lcom/yandex/metrica/impl/ob/df;

    .line 68
    iget-object v0, p0, Lcom/yandex/metrica/impl/utils/n;->b:Lcom/yandex/metrica/impl/ob/df;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/df;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    monitor-exit p0

    return-void

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()Z
    .locals 2

    .prologue
    .line 76
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/utils/n;->b:Lcom/yandex/metrica/impl/ob/df;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/df;->b(Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
