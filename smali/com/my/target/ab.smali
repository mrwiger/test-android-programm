.class public Lcom/my/target/ab;
.super Ljava/lang/Object;
.source "MraidJsCommand.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/ab$a;
    }
.end annotation


# static fields
.field public static final au:Ljava/lang/String; = "expand"

.field public static final bA:Ljava/lang/String; = "playheadEvent"

.field public static final bB:Ljava/lang/String; = ""

.field public static final bo:Ljava/lang/String; = "close"

.field public static final bp:Ljava/lang/String; = "useCustomClose"

.field public static final bq:Ljava/lang/String; = "open"

.field public static final br:Ljava/lang/String; = "resize"

.field public static final bs:Ljava/lang/String; = "setResizeProperties"

.field public static final bt:Ljava/lang/String; = "setExpandProperties"

.field public static final bu:Ljava/lang/String; = "setOrientationProperties"

.field public static final bv:Ljava/lang/String; = "playVideo"

.field public static final bw:Ljava/lang/String; = "storePicture"

.field public static final bx:Ljava/lang/String; = "createCalendarEvent"

.field public static final by:Ljava/lang/String; = "vpaidInit"

.field public static final bz:Ljava/lang/String; = "vpaidEvent"


# instance fields
.field public final bC:Z

.field private final bD:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 59
    iput-boolean v1, p0, Lcom/my/target/ab;->bC:Z

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/ab;->bD:Ljava/lang/String;

    .line 63
    :goto_1
    return-void

    .line 33
    :sswitch_0
    const-string v3, "close"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v3, "useCustomClose"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v2

    goto :goto_0

    :sswitch_2
    const-string v3, "setOrientationProperties"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v3, "setResizeProperties"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v3, "setExpandProperties"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v3, "vpaidInit"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string v3, "vpaidEvent"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x6

    goto :goto_0

    :sswitch_7
    const-string v3, "playheadEvent"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x7

    goto :goto_0

    :sswitch_8
    const-string v3, "open"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0x8

    goto :goto_0

    :sswitch_9
    const-string v3, "resize"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0x9

    goto :goto_0

    :sswitch_a
    const-string v3, "storePicture"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0xa

    goto :goto_0

    :sswitch_b
    const-string v3, "createCalendarEvent"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0xb

    goto/16 :goto_0

    :sswitch_c
    const-string v3, "expand"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v3, "playVideo"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0xd

    goto/16 :goto_0

    .line 43
    :pswitch_0
    iput-boolean v1, p0, Lcom/my/target/ab;->bC:Z

    .line 44
    iput-object p1, p0, Lcom/my/target/ab;->bD:Ljava/lang/String;

    goto/16 :goto_1

    .line 50
    :pswitch_1
    iput-boolean v2, p0, Lcom/my/target/ab;->bC:Z

    .line 51
    iput-object p1, p0, Lcom/my/target/ab;->bD:Ljava/lang/String;

    goto/16 :goto_1

    .line 55
    :pswitch_2
    const-string v0, "inline"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/my/target/ab;->bC:Z

    .line 56
    iput-object p1, p0, Lcom/my/target/ab;->bD:Ljava/lang/String;

    goto/16 :goto_1

    .line 33
    :sswitch_data_0
    .sparse-switch
        -0x71e3df8e -> :sswitch_5
        -0x706c8659 -> :sswitch_d
        -0x4cd72166 -> :sswitch_c
        -0x37b2634c -> :sswitch_9
        -0x2bba19a0 -> :sswitch_b
        0x34264a -> :sswitch_8
        0x5a5ddf8 -> :sswitch_0
        0x7f3dfe1 -> :sswitch_2
        0x1b5f6cdd -> :sswitch_a
        0x253cb189 -> :sswitch_3
        0x35332378 -> :sswitch_6
        0x51334bef -> :sswitch_4
        0x6037d900 -> :sswitch_1
        0x6b2b2fe6 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/my/target/ab;->bD:Ljava/lang/String;

    return-object v0
.end method
