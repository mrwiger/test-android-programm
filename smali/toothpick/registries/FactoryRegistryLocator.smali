.class public Ltoothpick/registries/FactoryRegistryLocator;
.super Ljava/lang/Object;
.source "FactoryRegistryLocator.java"


# static fields
.field private static rootRegistry:Ltoothpick/registries/FactoryRegistry;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static getFactory(Ljava/lang/Class;)Ltoothpick/Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ltoothpick/Factory",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 38
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    sget-object v0, Ltoothpick/configuration/ConfigurationHolder;->configuration:Ltoothpick/configuration/Configuration;

    invoke-virtual {v0, p0}, Ltoothpick/configuration/Configuration;->getFactory(Ljava/lang/Class;)Ltoothpick/Factory;

    move-result-object v0

    return-object v0
.end method

.method public static getFactoryUsingReflection(Ljava/lang/Class;)Ltoothpick/Factory;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ltoothpick/Factory",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 54
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "$$Factory"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 55
    .local v1, "factoryClass":Ljava/lang/Class;, "Ljava/lang/Class<+Ltoothpick/Factory<TT;>;>;"
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ltoothpick/Factory;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 56
    .end local v1    # "factoryClass":Ljava/lang/Class;, "Ljava/lang/Class<+Ltoothpick/Factory<TT;>;>;"
    :catch_0
    move-exception v0

    .line 57
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ltoothpick/registries/NoFactoryFoundException;

    invoke-direct {v2, p0, v0}, Ltoothpick/registries/NoFactoryFoundException;-><init>(Ljava/lang/Class;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static getFactoryUsingRegistries(Ljava/lang/Class;)Ltoothpick/Factory;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ltoothpick/Factory",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 43
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    sget-object v1, Ltoothpick/registries/FactoryRegistryLocator;->rootRegistry:Ltoothpick/registries/FactoryRegistry;

    if-eqz v1, :cond_0

    .line 44
    sget-object v1, Ltoothpick/registries/FactoryRegistryLocator;->rootRegistry:Ltoothpick/registries/FactoryRegistry;

    invoke-interface {v1, p0}, Ltoothpick/registries/FactoryRegistry;->getFactory(Ljava/lang/Class;)Ltoothpick/Factory;

    move-result-object v0

    .line 45
    .local v0, "factory":Ltoothpick/Factory;, "Ltoothpick/Factory<TT;>;"
    if-eqz v0, :cond_0

    .line 46
    return-object v0

    .line 49
    .end local v0    # "factory":Ltoothpick/Factory;, "Ltoothpick/Factory<TT;>;"
    :cond_0
    new-instance v1, Ltoothpick/registries/NoFactoryFoundException;

    invoke-direct {v1, p0}, Ltoothpick/registries/NoFactoryFoundException;-><init>(Ljava/lang/Class;)V

    throw v1
.end method

.method public static setRootRegistry(Ltoothpick/registries/FactoryRegistry;)V
    .locals 0
    .param p0, "rootRegistry"    # Ltoothpick/registries/FactoryRegistry;

    .prologue
    .line 34
    sput-object p0, Ltoothpick/registries/FactoryRegistryLocator;->rootRegistry:Ltoothpick/registries/FactoryRegistry;

    .line 35
    return-void
.end method
