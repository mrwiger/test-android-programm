.class public final Lcom/my/target/am;
.super Ljava/lang/Object;
.source "VideoSettings.java"


# static fields
.field public static final DEFAULT_ALLOW_BACK_BUTTON:Z = true

.field public static final DEFAULT_ALLOW_REPLAY:Z = true

.field public static final cP:F = 0.0f

.field public static final dc:Ljava/lang/String; = "Close"

.field public static final dd:Ljava/lang/String; = "Replay"

.field public static final de:Ljava/lang/String; = "Ad can be skipped after %ds"

.field public static final df:Z = false

.field public static final dg:Z = true

.field public static final dh:Z = true


# instance fields
.field private allowBackButton:Z

.field private allowClose:Z

.field private allowCloseDelay:F

.field private allowReplay:Z

.field private autoMute:Z

.field private closeActionText:Ljava/lang/String;

.field private closeDelayActionText:Ljava/lang/String;

.field private replayActionText:Ljava/lang/String;

.field private showPlayerControls:Z


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const-string v0, "Close"

    iput-object v0, p0, Lcom/my/target/am;->closeActionText:Ljava/lang/String;

    .line 29
    const-string v0, "Replay"

    iput-object v0, p0, Lcom/my/target/am;->replayActionText:Ljava/lang/String;

    .line 30
    const-string v0, "Ad can be skipped after %ds"

    iput-object v0, p0, Lcom/my/target/am;->closeDelayActionText:Ljava/lang/String;

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/am;->autoMute:Z

    .line 32
    iput-boolean v1, p0, Lcom/my/target/am;->showPlayerControls:Z

    .line 33
    iput-boolean v1, p0, Lcom/my/target/am;->allowClose:Z

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/my/target/am;->allowCloseDelay:F

    .line 35
    iput-boolean v1, p0, Lcom/my/target/am;->allowBackButton:Z

    .line 36
    iput-boolean v1, p0, Lcom/my/target/am;->allowReplay:Z

    .line 41
    return-void
.end method

.method public static W()Lcom/my/target/am;
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/my/target/am;

    invoke-direct {v0}, Lcom/my/target/am;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getAllowCloseDelay()F
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/my/target/am;->allowCloseDelay:F

    return v0
.end method

.method public getCloseActionText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/my/target/am;->closeActionText:Ljava/lang/String;

    return-object v0
.end method

.method public getCloseDelayActionText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/my/target/am;->closeDelayActionText:Ljava/lang/String;

    return-object v0
.end method

.method public getReplayActionText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/my/target/am;->replayActionText:Ljava/lang/String;

    return-object v0
.end method

.method public isAllowBackButton()Z
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/my/target/am;->allowBackButton:Z

    return v0
.end method

.method public isAllowClose()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/my/target/am;->allowClose:Z

    return v0
.end method

.method public isAllowReplay()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/my/target/am;->allowReplay:Z

    return v0
.end method

.method public isAutoMute()Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/my/target/am;->autoMute:Z

    return v0
.end method

.method public isShowPlayerControls()Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/my/target/am;->showPlayerControls:Z

    return v0
.end method

.method public setAllowBackButton(Z)V
    .locals 0
    .param p1, "allowBackButton"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/my/target/am;->allowBackButton:Z

    .line 106
    return-void
.end method

.method public setAllowClose(Z)V
    .locals 0
    .param p1, "allowClose"    # Z

    .prologue
    .line 90
    iput-boolean p1, p0, Lcom/my/target/am;->allowClose:Z

    .line 91
    return-void
.end method

.method public setAllowCloseDelay(F)V
    .locals 0
    .param p1, "allowCloseDelay"    # F

    .prologue
    .line 95
    iput p1, p0, Lcom/my/target/am;->allowCloseDelay:F

    .line 96
    return-void
.end method

.method public setAllowReplay(Z)V
    .locals 0
    .param p1, "allowReplay"    # Z

    .prologue
    .line 115
    iput-boolean p1, p0, Lcom/my/target/am;->allowReplay:Z

    .line 116
    return-void
.end method

.method public setAutoMute(Z)V
    .locals 0
    .param p1, "autoMute"    # Z

    .prologue
    .line 80
    iput-boolean p1, p0, Lcom/my/target/am;->autoMute:Z

    .line 81
    return-void
.end method

.method public setCloseActionText(Ljava/lang/String;)V
    .locals 0
    .param p1, "closeActionText"    # Ljava/lang/String;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/my/target/am;->closeActionText:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public setCloseDelayActionText(Ljava/lang/String;)V
    .locals 0
    .param p1, "closeDelayActionText"    # Ljava/lang/String;

    .prologue
    .line 130
    iput-object p1, p0, Lcom/my/target/am;->closeDelayActionText:Ljava/lang/String;

    .line 131
    return-void
.end method

.method public setReplayActionText(Ljava/lang/String;)V
    .locals 0
    .param p1, "replayActionText"    # Ljava/lang/String;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/my/target/am;->replayActionText:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public setShowPlayerControls(Z)V
    .locals 0
    .param p1, "showPlayerControls"    # Z

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/my/target/am;->showPlayerControls:Z

    .line 86
    return-void
.end method
