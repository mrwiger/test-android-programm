.class public final Lru/cn/tv/stb/parental/PasswordDialog;
.super Landroid/support/v7/app/AlertDialog$Builder;
.source "PasswordDialog.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lru/cn/tv/stb/parental/PasswordDialogPresenter$PasswordDialogListener;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dialogListener"    # Lru/cn/tv/stb/parental/PasswordDialogPresenter$PasswordDialogListener;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 17
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 18
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f0c0025

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 19
    .local v3, "view":Landroid/view/View;
    const v4, 0x7f0e0093

    invoke-virtual {p0, v4}, Lru/cn/tv/stb/parental/PasswordDialog;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 20
    invoke-virtual {p0, v3}, Lru/cn/tv/stb/parental/PasswordDialog;->setView(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 22
    new-instance v2, Lru/cn/tv/stb/parental/PasswordDialogPresenter;

    invoke-direct {v2, p1, p2}, Lru/cn/tv/stb/parental/PasswordDialogPresenter;-><init>(Landroid/content/Context;Lru/cn/tv/stb/parental/PasswordDialogPresenter$PasswordDialogListener;)V

    .line 23
    .local v2, "presenter":Lru/cn/tv/stb/parental/PasswordDialogPresenter;
    const v4, 0x7f09015f

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 24
    .local v1, "passwordEditText":Landroid/widget/EditText;
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 26
    const v4, 0x7f0e0036

    invoke-virtual {p0, v4, v2}, Lru/cn/tv/stb/parental/PasswordDialog;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 27
    const v4, 0x7f0e0032

    invoke-virtual {p0, v4, v2}, Lru/cn/tv/stb/parental/PasswordDialog;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 28
    return-void
.end method
