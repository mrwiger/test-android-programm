.class public Lru/cn/domain/statistics/AnalyticsManager;
.super Ljava/lang/Object;
.source "AnalyticsManager.java"


# static fields
.field private static amberDataTracker:Lru/cn/domain/statistics/AmberDataTracker;


# direct methods
.method static synthetic access$000(Lcom/samsung/multiscreen/Device;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/Device;

    .prologue
    .line 47
    invoke-static {p0}, Lru/cn/domain/statistics/AnalyticsManager;->trackSmartViewDevice(Lcom/samsung/multiscreen/Device;)V

    return-void
.end method

.method public static future_telecast_selected()V
    .locals 1

    .prologue
    .line 139
    const-string v0, "future_telecast_selected"

    invoke-static {v0}, Lcom/flurry/android/FlurryAgent;->logEvent(Ljava/lang/String;)Lcom/flurry/android/FlurryEventRecordStatus;

    .line 140
    return-void
.end method

.method public static initialize(Landroid/app/Application;Z)V
    .locals 10
    .param p0, "application"    # Landroid/app/Application;
    .param p1, "isMainProcess"    # Z

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 56
    const-string v6, "2e0cf8b6-8ca7-4e28-94f4-2c425590efba"

    invoke-static {v6}, Lcom/yandex/metrica/YandexMetricaConfig;->newConfigBuilder(Ljava/lang/String;)Lcom/yandex/metrica/YandexMetricaConfig$Builder;

    move-result-object v6

    .line 57
    invoke-virtual {v6, v8}, Lcom/yandex/metrica/YandexMetricaConfig$Builder;->setReportCrashesEnabled(Z)Lcom/yandex/metrica/YandexMetricaConfig$Builder;

    move-result-object v6

    .line 58
    invoke-virtual {v6}, Lcom/yandex/metrica/YandexMetricaConfig$Builder;->build()Lcom/yandex/metrica/YandexMetricaConfig;

    move-result-object v0

    .line 59
    .local v0, "config":Lcom/yandex/metrica/YandexMetricaConfig;
    invoke-static {p0, v0}, Lcom/yandex/metrica/YandexMetrica;->activate(Landroid/content/Context;Lcom/yandex/metrica/YandexMetricaConfig;)V

    .line 60
    invoke-static {p0}, Lcom/yandex/metrica/YandexMetrica;->enableActivityAutoTracking(Landroid/app/Application;)V

    .line 63
    invoke-static {p0}, Lru/cn/utils/Utils;->getUUID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 66
    .local v5, "userId":Ljava/lang/String;
    const/4 v2, 0x0

    .line 68
    .local v2, "error":Ljava/lang/LinkageError;
    const/4 v3, 0x0

    .line 70
    .local v3, "kits":[Lio/fabric/sdk/android/Kit;
    :try_start_0
    new-instance v4, Lcom/crashlytics/android/ndk/CrashlyticsNdk;

    invoke-direct {v4}, Lcom/crashlytics/android/ndk/CrashlyticsNdk;-><init>()V

    .line 71
    .local v4, "ndkKit":Lio/fabric/sdk/android/Kit;
    const/4 v6, 0x2

    new-array v3, v6, [Lio/fabric/sdk/android/Kit;

    .end local v3    # "kits":[Lio/fabric/sdk/android/Kit;
    const/4 v6, 0x0

    new-instance v7, Lcom/crashlytics/android/Crashlytics;

    invoke-direct {v7}, Lcom/crashlytics/android/Crashlytics;-><init>()V

    aput-object v7, v3, v6

    const/4 v6, 0x1

    aput-object v4, v3, v6
    :try_end_0
    .catch Ljava/lang/LinkageError; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    .end local v4    # "ndkKit":Lio/fabric/sdk/android/Kit;
    .restart local v3    # "kits":[Lio/fabric/sdk/android/Kit;
    :goto_0
    invoke-static {p0, v3}, Lio/fabric/sdk/android/Fabric;->with(Landroid/content/Context;[Lio/fabric/sdk/android/Kit;)Lio/fabric/sdk/android/Fabric;

    .line 80
    invoke-static {v5}, Lcom/crashlytics/android/Crashlytics;->setUserIdentifier(Ljava/lang/String;)V

    .line 82
    if-eqz v2, :cond_0

    .line 83
    invoke-static {v2}, Lru/cn/utils/Logger;->logException(Ljava/lang/Throwable;)V

    .line 87
    :cond_0
    if-eqz p1, :cond_1

    .line 88
    invoke-static {p0}, Lru/cn/domain/statistics/inetra/InetraTracker;->singletonize(Landroid/content/Context;)V

    .line 91
    :try_start_1
    invoke-static {p0}, Lcom/facebook/appevents/AppEventsLogger;->activateApp(Landroid/app/Application;)V
    :try_end_1
    .catch Lcom/facebook/FacebookException; {:try_start_1 .. :try_end_1} :catch_1

    .line 98
    :goto_1
    new-instance v6, Lcom/flurry/android/FlurryAgent$Builder;

    invoke-direct {v6}, Lcom/flurry/android/FlurryAgent$Builder;-><init>()V

    .line 99
    invoke-virtual {v6, v8}, Lcom/flurry/android/FlurryAgent$Builder;->withCaptureUncaughtExceptions(Z)Lcom/flurry/android/FlurryAgent$Builder;

    move-result-object v6

    .line 100
    invoke-virtual {v6, v9}, Lcom/flurry/android/FlurryAgent$Builder;->withLogEnabled(Z)Lcom/flurry/android/FlurryAgent$Builder;

    move-result-object v6

    const-string v7, "XC9FYHD7BH3QFYB2VR33"

    .line 101
    invoke-virtual {v6, p0, v7}, Lcom/flurry/android/FlurryAgent$Builder;->build(Landroid/content/Context;Ljava/lang/String;)V

    .line 103
    invoke-static {v5}, Lcom/flurry/android/FlurryAgent;->setUserId(Ljava/lang/String;)V

    .line 106
    new-instance v6, Lru/cn/domain/statistics/AmberDataTracker;

    invoke-static {}, Lru/cn/api/authorization/Authorization;->clientId()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, p0, v7}, Lru/cn/domain/statistics/AmberDataTracker;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v6, Lru/cn/domain/statistics/AnalyticsManager;->amberDataTracker:Lru/cn/domain/statistics/AmberDataTracker;

    .line 108
    invoke-static {p0}, Lru/cn/domain/statistics/AnalyticsManager;->trackSamsungMultiscreen(Landroid/content/Context;)V

    .line 110
    :cond_1
    return-void

    .line 72
    .end local v3    # "kits":[Lio/fabric/sdk/android/Kit;
    :catch_0
    move-exception v1

    .line 73
    .local v1, "e":Ljava/lang/LinkageError;
    move-object v2, v1

    .line 75
    new-array v3, v9, [Lio/fabric/sdk/android/Kit;

    new-instance v6, Lcom/crashlytics/android/Crashlytics;

    invoke-direct {v6}, Lcom/crashlytics/android/Crashlytics;-><init>()V

    aput-object v6, v3, v8

    .restart local v3    # "kits":[Lio/fabric/sdk/android/Kit;
    goto :goto_0

    .line 92
    .end local v1    # "e":Ljava/lang/LinkageError;
    :catch_1
    move-exception v1

    .line 94
    .local v1, "e":Lcom/facebook/FacebookException;
    const-string v6, "Analytics"

    const-string v7, "unable to initialize Facebook SDK"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method static final synthetic lambda$trackSamsungMultiscreen$0$AnalyticsManager(Lcom/samsung/multiscreen/Service;)V
    .locals 1
    .param p0, "service"    # Lcom/samsung/multiscreen/Service;

    .prologue
    .line 277
    new-instance v0, Lru/cn/domain/statistics/AnalyticsManager$1;

    invoke-direct {v0}, Lru/cn/domain/statistics/AnalyticsManager$1;-><init>()V

    invoke-virtual {p0, v0}, Lcom/samsung/multiscreen/Service;->getDeviceInfo(Lcom/samsung/multiscreen/Result;)V

    return-void
.end method

.method public static peerstv_plus()V
    .locals 2

    .prologue
    .line 146
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 147
    .local v0, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "peerstv plus"

    invoke-static {v1, v0}, Lcom/flurry/android/FlurryAgent;->logEvent(Ljava/lang/String;Ljava/util/Map;)Lcom/flurry/android/FlurryEventRecordStatus;

    .line 148
    return-void
.end method

.method public static player_minimize()V
    .locals 1

    .prologue
    .line 223
    const-string v0, "player/minimize"

    invoke-static {v0}, Lcom/flurry/android/FlurryAgent;->logEvent(Ljava/lang/String;)Lcom/flurry/android/FlurryEventRecordStatus;

    .line 224
    return-void
.end method

.method public static player_option(Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;)V
    .locals 3
    .param p0, "optionType"    # Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    .prologue
    .line 230
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 231
    .local v0, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "option"

    invoke-virtual {p0}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    const-string v1, "player_option"

    invoke-static {v1, v0}, Lcom/flurry/android/FlurryAgent;->logEvent(Ljava/lang/String;Ljava/util/Map;)Lcom/flurry/android/FlurryEventRecordStatus;

    .line 233
    return-void
.end method

.method public static purchase_complete(Ljava/lang/String;I)V
    .locals 2
    .param p0, "productId"    # Ljava/lang/String;
    .param p1, "result"    # I

    .prologue
    .line 175
    if-eqz p1, :cond_0

    .line 184
    :goto_0
    return-void

    .line 178
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 179
    .local v0, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p0, :cond_1

    .line 180
    const-string v1, "product_id"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    :cond_1
    const-string v1, "purchase complete"

    invoke-static {v1, v0}, Lcom/flurry/android/FlurryAgent;->logEvent(Ljava/lang/String;Ljava/util/Map;)Lcom/flurry/android/FlurryEventRecordStatus;

    goto :goto_0
.end method

.method public static purchase_overlay(JLjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "contractorId"    # J
    .param p2, "channel"    # Ljava/lang/String;
    .param p3, "mode"    # Ljava/lang/String;

    .prologue
    .line 190
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 191
    .local v0, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "channel"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    const-string v1, "contractor"

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    const-string v1, "content"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    const-string v1, "purchase_overlay"

    invoke-static {v1, v0}, Lcom/flurry/android/FlurryAgent;->logEvent(Ljava/lang/String;Ljava/util/Map;)Lcom/flurry/android/FlurryEventRecordStatus;

    .line 196
    return-void
.end method

.method public static purchase_overlay_lk(JLjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "contractorId"    # J
    .param p2, "channel"    # Ljava/lang/String;
    .param p3, "mode"    # Ljava/lang/String;

    .prologue
    .line 202
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 203
    .local v0, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "channel"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    const-string v1, "contractor"

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    const-string v1, "content"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    const-string v1, "purchase_overlay_lk"

    invoke-static {v1, v0}, Lcom/flurry/android/FlurryAgent;->logEvent(Ljava/lang/String;Ljava/util/Map;)Lcom/flurry/android/FlurryEventRecordStatus;

    .line 208
    return-void
.end method

.method public static purchase_start(Ljava/lang/String;)V
    .locals 2
    .param p0, "productId"    # Ljava/lang/String;

    .prologue
    .line 164
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 165
    .local v0, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p0, :cond_0

    .line 166
    const-string v1, "product_id"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    :cond_0
    const-string v1, "purchase start"

    invoke-static {v1, v0}, Lcom/flurry/android/FlurryAgent;->logEvent(Ljava/lang/String;Ljava/util/Map;)Lcom/flurry/android/FlurryEventRecordStatus;

    .line 169
    return-void
.end method

.method public static recommend_next(Ljava/lang/String;)V
    .locals 2
    .param p0, "action"    # Ljava/lang/String;

    .prologue
    .line 214
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 215
    .local v0, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "action"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    const-string v1, "recommend_next"

    invoke-static {v1, v0}, Lcom/flurry/android/FlurryAgent;->logEvent(Ljava/lang/String;Ljava/util/Map;)Lcom/flurry/android/FlurryEventRecordStatus;

    .line 217
    return-void
.end method

.method public static start_search()V
    .locals 1

    .prologue
    .line 116
    const-string v0, "start_search"

    invoke-static {v0}, Lcom/flurry/android/FlurryAgent;->logEvent(Ljava/lang/String;)Lcom/flurry/android/FlurryEventRecordStatus;

    .line 117
    return-void
.end method

.method public static store_open(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "contractor"    # Ljava/lang/String;
    .param p1, "from"    # Ljava/lang/String;

    .prologue
    .line 154
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 155
    .local v0, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "contractor"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    const-string v1, "from"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    const-string v1, "store open"

    invoke-static {v1, v0}, Lcom/flurry/android/FlurryAgent;->logEvent(Ljava/lang/String;Ljava/util/Map;)Lcom/flurry/android/FlurryEventRecordStatus;

    .line 158
    return-void
.end method

.method public static trackGeoLocation(Landroid/content/Context;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 236
    const-string v3, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {p0, v3}, Landroid/support/v4/app/ActivityCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "android.permission.ACCESS_COARSE_LOCATION"

    .line 237
    invoke-static {p0, v3}, Landroid/support/v4/app/ActivityCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_1

    .line 253
    :cond_0
    :goto_0
    return-void

    .line 241
    :cond_1
    const-string v3, "location"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 242
    .local v0, "locManager":Landroid/location/LocationManager;
    const-string v3, "network"

    invoke-virtual {v0, v3}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    .line 243
    .local v1, "location":Landroid/location/Location;
    if-eqz v1, :cond_0

    .line 246
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 247
    .local v2, "message":Ljava/lang/StringBuilder;
    const-string v3, "source="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "android_network"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";lat="

    .line 248
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";lon="

    .line 249
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";precise="

    .line 250
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/location/Location;->getAccuracy()F

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    sget-object v3, Lru/cn/domain/statistics/inetra/ErrorCode;->UNKNOWN_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v4, "GeoLocation"

    const/4 v5, 0x0

    .line 252
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 251
    invoke-static {v3, v4, v5, v6}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0
.end method

.method public static trackInterfaceChange()V
    .locals 4

    .prologue
    .line 257
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "device="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 258
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";manufacturer="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 259
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";board="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->BOARD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 260
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";brand="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 261
    sget-object v1, Lru/cn/domain/statistics/inetra/ErrorCode;->UNKNOWN_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v2, "UserInterfaceChange"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3, v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    .line 263
    return-void
.end method

.method public static trackLogin(Landroid/content/Context;Ljava/lang/String;J)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "ipv4"    # Ljava/lang/String;
    .param p2, "userId"    # J

    .prologue
    .line 266
    invoke-static {}, Lru/cn/utils/http/HttpClient;->defaultUserAgent()Ljava/lang/String;

    move-result-object v5

    .line 267
    .local v5, "userAgent":Ljava/lang/String;
    invoke-static {p0}, Lru/cn/utils/Utils;->getAndroidId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 268
    .local v2, "androidId":Ljava/lang/String;
    invoke-static {p0}, Lru/cn/utils/Utils;->getGadId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 270
    .local v3, "gadId":Ljava/lang/String;
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 271
    .local v1, "userIdValue":Ljava/lang/String;
    sget-object v0, Lru/cn/domain/statistics/AnalyticsManager;->amberDataTracker:Lru/cn/domain/statistics/AmberDataTracker;

    move-object v4, p1

    invoke-virtual/range {v0 .. v5}, Lru/cn/domain/statistics/AmberDataTracker;->trackLogin(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    return-void
.end method

.method private static trackSamsungMultiscreen(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 275
    invoke-static {p0}, Lcom/samsung/multiscreen/Service;->search(Landroid/content/Context;)Lcom/samsung/multiscreen/Search;

    move-result-object v2

    .line 277
    .local v2, "search":Lcom/samsung/multiscreen/Search;
    sget-object v3, Lru/cn/domain/statistics/AnalyticsManager$$Lambda$0;->$instance:Lcom/samsung/multiscreen/Search$OnServiceFoundListener;

    invoke-virtual {v2, v3}, Lcom/samsung/multiscreen/Search;->setOnServiceFoundListener(Lcom/samsung/multiscreen/Search$OnServiceFoundListener;)V

    .line 289
    invoke-static {p0}, Lcom/samsung/multiscreen/MDNSSearchProvider;->create(Landroid/content/Context;)Lcom/samsung/multiscreen/SearchProvider;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/multiscreen/Search;->addProvider(Lcom/samsung/multiscreen/SearchProvider;)V

    .line 290
    invoke-static {p0}, Lcom/samsung/multiscreen/MSFDSearchProvider;->create(Landroid/content/Context;)Lcom/samsung/multiscreen/SearchProvider;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/multiscreen/Search;->addProvider(Lcom/samsung/multiscreen/SearchProvider;)V

    .line 293
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x12

    if-lt v3, v4, :cond_0

    .line 296
    :try_start_0
    invoke-static {p0}, Lcom/samsung/multiscreen/BLESearchProvider;->create(Landroid/content/Context;)Lcom/samsung/multiscreen/SearchProvider;

    move-result-object v0

    .line 297
    .local v0, "bleSearchProvider":Lcom/samsung/multiscreen/SearchProvider;
    invoke-virtual {v2, v0}, Lcom/samsung/multiscreen/Search;->addProvider(Lcom/samsung/multiscreen/SearchProvider;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 303
    .end local v0    # "bleSearchProvider":Lcom/samsung/multiscreen/SearchProvider;
    :cond_0
    :goto_0
    invoke-virtual {v2}, Lcom/samsung/multiscreen/Search;->start()Z

    .line 304
    return-void

    .line 298
    :catch_0
    move-exception v1

    .line 299
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "SMF"

    const-string v4, "BLE not supported"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static trackSmartViewDevice(Lcom/samsung/multiscreen/Device;)V
    .locals 4
    .param p0, "device"    # Lcom/samsung/multiscreen/Device;

    .prologue
    .line 307
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "duid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Device;->getDuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 308
    .local v0, "message":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Device;->getModel()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 309
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";model="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Device;->getModel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 312
    :cond_0
    sget-object v1, Lru/cn/domain/statistics/inetra/ErrorCode;->UNKNOWN_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v2, "SmartViewDeviceFound"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3, v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    .line 313
    return-void
.end method

.method public static use_search()V
    .locals 1

    .prologue
    .line 123
    const-string v0, "use_search"

    invoke-static {v0}, Lcom/flurry/android/FlurryAgent;->logEvent(Ljava/lang/String;)Lcom/flurry/android/FlurryEventRecordStatus;

    .line 124
    return-void
.end method

.method public static view_search(Ljava/lang/String;)V
    .locals 2
    .param p0, "clickFrom"    # Ljava/lang/String;

    .prologue
    .line 130
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 131
    .local v0, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "click_from"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    const-string v1, "view_search"

    invoke-static {v1, v0}, Lcom/flurry/android/FlurryAgent;->logEvent(Ljava/lang/String;Ljava/util/Map;)Lcom/flurry/android/FlurryEventRecordStatus;

    .line 133
    return-void
.end method
