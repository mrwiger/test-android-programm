.class public final Lcom/my/target/ads/MyTargetView;
.super Landroid/widget/RelativeLayout;
.source "MyTargetView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/ads/MyTargetView$MyTargetViewListener;,
        Lcom/my/target/ads/MyTargetView$AdSize;
    }
.end annotation


# instance fields
.field private adConfig:Lcom/my/target/b;

.field private engine:Lcom/my/target/core/engines/b;

.field private listener:Lcom/my/target/ads/MyTargetView$MyTargetViewListener;

.field private trackingEnvironmentEnabled:Z

.field private trackingLocationEnabled:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    .line 92
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 40
    iput-boolean v0, p0, Lcom/my/target/ads/MyTargetView;->trackingEnvironmentEnabled:Z

    .line 41
    iput-boolean v0, p0, Lcom/my/target/ads/MyTargetView;->trackingLocationEnabled:Z

    .line 93
    const-string v0, "MyTargetView created. Version: 5.1.0"

    invoke-static {v0}, Lcom/my/target/g;->c(Ljava/lang/String;)V

    .line 94
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x1

    .line 98
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    iput-boolean v0, p0, Lcom/my/target/ads/MyTargetView;->trackingEnvironmentEnabled:Z

    .line 41
    iput-boolean v0, p0, Lcom/my/target/ads/MyTargetView;->trackingLocationEnabled:Z

    .line 99
    const-string v0, "MyTargetView created. Version: 5.1.0"

    invoke-static {v0}, Lcom/my/target/g;->c(Ljava/lang/String;)V

    .line 100
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x1

    .line 104
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    iput-boolean v0, p0, Lcom/my/target/ads/MyTargetView;->trackingEnvironmentEnabled:Z

    .line 41
    iput-boolean v0, p0, Lcom/my/target/ads/MyTargetView;->trackingLocationEnabled:Z

    .line 105
    const-string v0, "MyTargetView created. Version: 5.1.0"

    invoke-static {v0}, Lcom/my/target/g;->c(Ljava/lang/String;)V

    .line 106
    return-void
.end method

.method static synthetic access$000(Lcom/my/target/ads/MyTargetView;Lcom/my/target/dh;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/my/target/ads/MyTargetView;
    .param p1, "x1"    # Lcom/my/target/dh;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/my/target/ads/MyTargetView;->handleResult(Lcom/my/target/dh;Ljava/lang/String;)V

    return-void
.end method

.method private handleResult(Lcom/my/target/dh;Ljava/lang/String;)V
    .locals 2
    .param p1, "result"    # Lcom/my/target/dh;
    .param p2, "error"    # Ljava/lang/String;

    .prologue
    .line 211
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->listener:Lcom/my/target/ads/MyTargetView$MyTargetViewListener;

    if-eqz v0, :cond_1

    .line 213
    if-nez p1, :cond_2

    .line 215
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->listener:Lcom/my/target/ads/MyTargetView$MyTargetViewListener;

    if-nez p2, :cond_0

    const-string p2, "no ad"

    .end local p2    # "error":Ljava/lang/String;
    :cond_0
    invoke-interface {v0, p2, p0}, Lcom/my/target/ads/MyTargetView$MyTargetViewListener;->onNoAd(Ljava/lang/String;Lcom/my/target/ads/MyTargetView;)V

    .line 230
    :cond_1
    :goto_0
    return-void

    .line 219
    .restart local p2    # "error":Ljava/lang/String;
    :cond_2
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->adConfig:Lcom/my/target/b;

    if-eqz v0, :cond_3

    .line 221
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->adConfig:Lcom/my/target/b;

    invoke-static {p0, v0}, Lcom/my/target/core/engines/b;->a(Lcom/my/target/ads/MyTargetView;Lcom/my/target/b;)Lcom/my/target/core/engines/b;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/ads/MyTargetView;->engine:Lcom/my/target/core/engines/b;

    .line 222
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->engine:Lcom/my/target/core/engines/b;

    invoke-virtual {v0, p1}, Lcom/my/target/core/engines/b;->a(Lcom/my/target/dh;)V

    goto :goto_0

    .line 226
    :cond_3
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->listener:Lcom/my/target/ads/MyTargetView$MyTargetViewListener;

    const-string v1, "no ad"

    invoke-interface {v0, v1, p0}, Lcom/my/target/ads/MyTargetView$MyTargetViewListener;->onNoAd(Ljava/lang/String;Lcom/my/target/ads/MyTargetView;)V

    goto :goto_0
.end method

.method public static setDebugMode(Z)V
    .locals 1
    .param p0, "debugMode"    # Z

    .prologue
    .line 30
    sput-boolean p0, Lcom/my/target/g;->enabled:Z

    .line 31
    if-eqz p0, :cond_0

    .line 33
    const-string v0, "Debug mode enabled"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 35
    :cond_0
    return-void
.end method


# virtual methods
.method public final destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 201
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->engine:Lcom/my/target/core/engines/b;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->engine:Lcom/my/target/core/engines/b;

    invoke-virtual {v0}, Lcom/my/target/core/engines/b;->destroy()V

    .line 204
    iput-object v1, p0, Lcom/my/target/ads/MyTargetView;->engine:Lcom/my/target/core/engines/b;

    .line 206
    :cond_0
    iput-object v1, p0, Lcom/my/target/ads/MyTargetView;->listener:Lcom/my/target/ads/MyTargetView$MyTargetViewListener;

    .line 207
    return-void
.end method

.method public final getCustomParams()Lcom/my/target/common/CustomParams;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->adConfig:Lcom/my/target/b;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0}, Lcom/my/target/b;->getCustomParams()Lcom/my/target/common/CustomParams;

    move-result-object v0

    .line 87
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getListener()Lcom/my/target/ads/MyTargetView$MyTargetViewListener;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->listener:Lcom/my/target/ads/MyTargetView$MyTargetViewListener;

    return-object v0
.end method

.method public final init(I)V
    .locals 1
    .param p1, "slotId"    # I

    .prologue
    .line 110
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/my/target/ads/MyTargetView;->init(IZ)V

    .line 111
    return-void
.end method

.method public final init(II)V
    .locals 1
    .param p1, "slotId"    # I
    .param p2, "adSize"    # I

    .prologue
    .line 120
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/my/target/ads/MyTargetView;->init(IIZ)V

    .line 121
    return-void
.end method

.method public final init(IIZ)V
    .locals 2
    .param p1, "slotId"    # I
    .param p2, "adSize"    # I
    .param p3, "isRefreshAd"    # Z

    .prologue
    .line 125
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->adConfig:Lcom/my/target/b;

    if-eqz v0, :cond_0

    .line 146
    :goto_0
    return-void

    .line 130
    :cond_0
    const-string v0, "standard_320x50"

    .line 131
    const/4 v1, 0x1

    if-ne p2, v1, :cond_2

    .line 133
    const-string v0, "standard_300x250"

    .line 140
    :cond_1
    :goto_1
    invoke-static {p1, v0}, Lcom/my/target/b;->newConfig(ILjava/lang/String;)Lcom/my/target/b;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/ads/MyTargetView;->adConfig:Lcom/my/target/b;

    .line 141
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->adConfig:Lcom/my/target/b;

    iget-boolean v1, p0, Lcom/my/target/ads/MyTargetView;->trackingEnvironmentEnabled:Z

    invoke-virtual {v0, v1}, Lcom/my/target/b;->setTrackingEnvironmentEnabled(Z)V

    .line 142
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->adConfig:Lcom/my/target/b;

    iget-boolean v1, p0, Lcom/my/target/ads/MyTargetView;->trackingLocationEnabled:Z

    invoke-virtual {v0, v1}, Lcom/my/target/b;->setTrackingLocationEnabled(Z)V

    .line 143
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0, p3}, Lcom/my/target/b;->setRefreshAd(Z)V

    .line 145
    const-string v0, "MyTargetView initialized"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 135
    :cond_2
    const/4 v1, 0x2

    if-ne p2, v1, :cond_1

    .line 137
    const-string v0, "standard_728x90"

    goto :goto_1
.end method

.method public final init(IZ)V
    .locals 1
    .param p1, "slotId"    # I
    .param p2, "isRefreshAd"    # Z

    .prologue
    .line 115
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/my/target/ads/MyTargetView;->init(IIZ)V

    .line 116
    return-void
.end method

.method public final isTrackingEnvironmentEnabled()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/my/target/ads/MyTargetView;->trackingEnvironmentEnabled:Z

    return v0
.end method

.method public final isTrackingLocationEnabled()Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/my/target/ads/MyTargetView;->trackingLocationEnabled:Z

    return v0
.end method

.method public final load()V
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->adConfig:Lcom/my/target/b;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->adConfig:Lcom/my/target/b;

    invoke-static {v0}, Lcom/my/target/db;->newFactory(Lcom/my/target/b;)Lcom/my/target/c;

    move-result-object v0

    new-instance v1, Lcom/my/target/ads/MyTargetView$1;

    invoke-direct {v1, p0}, Lcom/my/target/ads/MyTargetView$1;-><init>(Lcom/my/target/ads/MyTargetView;)V

    invoke-virtual {v0, v1}, Lcom/my/target/c;->a(Lcom/my/target/c$b;)Lcom/my/target/c;

    move-result-object v0

    .line 159
    invoke-virtual {p0}, Lcom/my/target/ads/MyTargetView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/c;->a(Landroid/content/Context;)Lcom/my/target/c;

    .line 165
    :goto_0
    return-void

    .line 163
    :cond_0
    const-string v0, "MyTargetView not initialized"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final pause()V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->engine:Lcom/my/target/core/engines/b;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->engine:Lcom/my/target/core/engines/b;

    invoke-virtual {v0}, Lcom/my/target/core/engines/b;->pause()V

    .line 189
    :cond_0
    return-void
.end method

.method public final resume()V
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->engine:Lcom/my/target/core/engines/b;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->engine:Lcom/my/target/core/engines/b;

    invoke-virtual {v0}, Lcom/my/target/core/engines/b;->resume()V

    .line 197
    :cond_0
    return-void
.end method

.method public final setListener(Lcom/my/target/ads/MyTargetView$MyTargetViewListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/my/target/ads/MyTargetView$MyTargetViewListener;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/my/target/ads/MyTargetView;->listener:Lcom/my/target/ads/MyTargetView$MyTargetViewListener;

    .line 51
    return-void
.end method

.method public final setTrackingEnvironmentEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/my/target/ads/MyTargetView;->trackingEnvironmentEnabled:Z

    .line 61
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->adConfig:Lcom/my/target/b;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0, p1}, Lcom/my/target/b;->setTrackingEnvironmentEnabled(Z)V

    .line 65
    :cond_0
    return-void
.end method

.method public final setTrackingLocationEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/my/target/ads/MyTargetView;->trackingLocationEnabled:Z

    .line 70
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->adConfig:Lcom/my/target/b;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0, p1}, Lcom/my/target/b;->setTrackingLocationEnabled(Z)V

    .line 74
    :cond_0
    return-void
.end method

.method public final start()V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->engine:Lcom/my/target/core/engines/b;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->engine:Lcom/my/target/core/engines/b;

    invoke-virtual {v0}, Lcom/my/target/core/engines/b;->start()V

    .line 173
    :cond_0
    return-void
.end method

.method public final stop()V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->engine:Lcom/my/target/core/engines/b;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/my/target/ads/MyTargetView;->engine:Lcom/my/target/core/engines/b;

    invoke-virtual {v0}, Lcom/my/target/core/engines/b;->stop()V

    .line 181
    :cond_0
    return-void
.end method
