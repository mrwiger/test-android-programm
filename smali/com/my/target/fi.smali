.class public final Lcom/my/target/fi;
.super Ljava/lang/Object;
.source "NotificationHandler.java"


# instance fields
.field private final adConfig:Lcom/my/target/b;

.field private final section:Lcom/my/target/fg;


# direct methods
.method private constructor <init>(Lcom/my/target/fg;Lcom/my/target/b;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/my/target/fi;->section:Lcom/my/target/fg;

    .line 40
    iput-object p2, p0, Lcom/my/target/fi;->adConfig:Lcom/my/target/b;

    .line 41
    return-void
.end method

.method static synthetic a(Lcom/my/target/fi;)Lcom/my/target/b;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/my/target/fi;->adConfig:Lcom/my/target/b;

    return-object v0
.end method

.method public static a(Lcom/my/target/fg;Lcom/my/target/b;)Lcom/my/target/fi;
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/my/target/fi;

    invoke-direct {v0, p0, p1}, Lcom/my/target/fi;-><init>(Lcom/my/target/fg;Lcom/my/target/b;)V

    return-object v0
.end method

.method static synthetic a(Lcom/my/target/fi;Lcom/my/target/core/models/banners/i;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/my/target/fi;->b(Lcom/my/target/core/models/banners/i;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/my/target/core/models/banners/i;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 74
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->getId()Ljava/lang/String;

    move-result-object v3

    .line 77
    :try_start_0
    iget-object v0, p0, Lcom/my/target/fi;->section:Lcom/my/target/fg;

    invoke-virtual {v0}, Lcom/my/target/fg;->getRawData()Lorg/json/JSONObject;

    move-result-object v4

    .line 78
    if-nez v4, :cond_0

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "unable to change cached notification for banner "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": no raw data in section"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    move-object v0, v1

    .line 117
    :goto_0
    return-object v0

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/my/target/fi;->section:Lcom/my/target/fg;

    invoke-virtual {v0}, Lcom/my/target/fg;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 86
    if-nez v0, :cond_1

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "unable to change cached notification for banner "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": no section object in raw data"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    move-object v0, v1

    .line 89
    goto :goto_0

    .line 92
    :cond_1
    const-string v2, "banners"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 94
    if-nez v5, :cond_2

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "unable to change cached notification for banner "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": no banners array in section object"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    move-object v0, v1

    .line 97
    goto :goto_0

    .line 100
    :cond_2
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    .line 101
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v6, :cond_4

    .line 103
    invoke-virtual {v5, v2}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 104
    const-string v7, "bannerID"

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 105
    if-eqz v7, :cond_3

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 107
    const-string v2, "hasNotification"

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->isHasNotification()Z

    move-result v5

    invoke-virtual {v0, v2, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 108
    const-string v0, "notification changed in raw data for banner "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 109
    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 101
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 113
    :catch_0
    move-exception v0

    .line 115
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "error updating cached notification for section "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/my/target/fi;->section:Lcom/my/target/fg;

    invoke-virtual {v4}, Lcom/my/target/fg;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " and banner "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    :cond_4
    move-object v0, v1

    .line 117
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lcom/my/target/core/models/banners/i;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 45
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->isHasNotification()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/my/target/core/models/banners/i;->setHasNotification(Z)V

    .line 48
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 49
    new-instance v1, Lcom/my/target/fi$1;

    invoke-direct {v1, p0, p1, v0}, Lcom/my/target/fi$1;-><init>(Lcom/my/target/fi;Lcom/my/target/core/models/banners/i;Landroid/content/Context;)V

    invoke-static {v1}, Lcom/my/target/h;->a(Ljava/lang/Runnable;)V

    .line 70
    :cond_0
    return-void
.end method
