.class public Lru/cn/tv/mobile/authorization/SignInActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "SignInActivity.java"

# interfaces
.implements Lru/cn/tv/WebviewFragment$WebviewFragmentListener;


# instance fields
.field private fragment:Lru/cn/tv/mobile/authorization/SignInFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public handleUrl(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 52
    const-string v0, "ptv29783051://callback"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lru/cn/tv/mobile/authorization/SignInActivity;->fragment:Lru/cn/tv/mobile/authorization/SignInFragment;

    invoke-virtual {v0, p1}, Lru/cn/tv/mobile/authorization/SignInFragment;->proceedAuthorization(Ljava/lang/String;)V

    .line 55
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 18
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 19
    const v3, 0x7f0c0093

    invoke-virtual {p0, v3}, Lru/cn/tv/mobile/authorization/SignInActivity;->setContentView(I)V

    .line 21
    const v3, 0x7f0901e2

    invoke-virtual {p0, v3}, Lru/cn/tv/mobile/authorization/SignInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/Toolbar;

    .line 22
    .local v2, "toolbar":Landroid/support/v7/widget/Toolbar;
    invoke-virtual {p0, v2}, Lru/cn/tv/mobile/authorization/SignInActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 24
    invoke-virtual {p0}, Lru/cn/tv/mobile/authorization/SignInActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 25
    invoke-virtual {p0}, Lru/cn/tv/mobile/authorization/SignInActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/support/v7/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 27
    invoke-virtual {p0}, Lru/cn/tv/mobile/authorization/SignInActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "contractor"

    const-wide/16 v6, 0x0

    invoke-virtual {v3, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 29
    .local v0, "contractorId":J
    if-nez p1, :cond_0

    .line 30
    invoke-static {v0, v1}, Lru/cn/tv/mobile/authorization/SignInFragment;->newInstance(J)Lru/cn/tv/mobile/authorization/SignInFragment;

    move-result-object v3

    iput-object v3, p0, Lru/cn/tv/mobile/authorization/SignInActivity;->fragment:Lru/cn/tv/mobile/authorization/SignInFragment;

    .line 31
    invoke-virtual {p0}, Lru/cn/tv/mobile/authorization/SignInActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    const v4, 0x7f090085

    iget-object v5, p0, Lru/cn/tv/mobile/authorization/SignInActivity;->fragment:Lru/cn/tv/mobile/authorization/SignInFragment;

    const-string v6, "fragment"

    .line 32
    invoke-virtual {v3, v4, v5, v6}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 33
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 38
    :goto_0
    iget-object v3, p0, Lru/cn/tv/mobile/authorization/SignInActivity;->fragment:Lru/cn/tv/mobile/authorization/SignInFragment;

    invoke-virtual {v3, p0}, Lru/cn/tv/mobile/authorization/SignInFragment;->setListener(Lru/cn/tv/WebviewFragment$WebviewFragmentListener;)V

    .line 39
    return-void

    .line 35
    :cond_0
    invoke-virtual {p0}, Lru/cn/tv/mobile/authorization/SignInActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "fragment"

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    check-cast v3, Lru/cn/tv/mobile/authorization/SignInFragment;

    iput-object v3, p0, Lru/cn/tv/mobile/authorization/SignInActivity;->fragment:Lru/cn/tv/mobile/authorization/SignInFragment;

    goto :goto_0
.end method

.method public onError(ILjava/lang/String;)V
    .locals 1
    .param p1, "code"    # I
    .param p2, "description"    # Ljava/lang/String;

    .prologue
    .line 68
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/authorization/SignInActivity;->setResult(I)V

    .line 69
    invoke-virtual {p0}, Lru/cn/tv/mobile/authorization/SignInActivity;->finish()V

    .line 70
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 43
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 44
    invoke-virtual {p0}, Lru/cn/tv/mobile/authorization/SignInActivity;->onBackPressed()V

    .line 45
    const/4 v0, 0x1

    .line 47
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPageFinishLoading(Landroid/webkit/WebView;)V
    .locals 0
    .param p1, "webView"    # Landroid/webkit/WebView;

    .prologue
    .line 64
    return-void
.end method

.method public onReceivedTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-virtual {p0}, Lru/cn/tv/mobile/authorization/SignInActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 60
    return-void
.end method
