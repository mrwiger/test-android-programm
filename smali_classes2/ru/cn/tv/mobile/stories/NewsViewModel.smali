.class Lru/cn/tv/mobile/stories/NewsViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "NewsViewModel.java"


# instance fields
.field private final loader:Lru/cn/mvvm/RxLoader;

.field private final news:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lru/cn/mvvm/RxLoader;)V
    .locals 1
    .param p1, "loader"    # Lru/cn/mvvm/RxLoader;

    .prologue
    .line 26
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 27
    iput-object p1, p0, Lru/cn/tv/mobile/stories/NewsViewModel;->loader:Lru/cn/mvvm/RxLoader;

    .line 29
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/mobile/stories/NewsViewModel;->news:Landroid/arch/lifecycle/MutableLiveData;

    .line 30
    return-void
.end method

.method private mapNewsCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 13
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 51
    const/4 v2, 0x0

    .line 53
    .local v2, "rubrics":Landroid/database/MatrixCursor;
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 54
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 55
    const-string v4, "data"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 56
    .local v0, "jsonRubric":Ljava/lang/String;
    invoke-static {v0}, Lru/cn/api/catalogue/replies/Rubric;->fromJson(Ljava/lang/String;)Lru/cn/api/catalogue/replies/Rubric;

    move-result-object v1

    .line 57
    .local v1, "rootRubric":Lru/cn/api/catalogue/replies/Rubric;
    iget-object v4, v1, Lru/cn/api/catalogue/replies/Rubric;->subitems:Ljava/util/List;

    if-eqz v4, :cond_0

    .line 58
    new-instance v2, Landroid/database/MatrixCursor;

    .end local v2    # "rubrics":Landroid/database/MatrixCursor;
    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v4, v8

    const-string v5, "title"

    aput-object v5, v4, v9

    const-string v5, "description"

    aput-object v5, v4, v10

    const-string v5, "image_resource"

    aput-object v5, v4, v11

    const-string v5, "image_url"

    aput-object v5, v4, v12

    const/4 v5, 0x5

    const-string v6, "ui_hint"

    aput-object v6, v4, v5

    invoke-direct {v2, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 60
    .restart local v2    # "rubrics":Landroid/database/MatrixCursor;
    iget-object v4, v1, Lru/cn/api/catalogue/replies/Rubric;->subitems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lru/cn/api/catalogue/replies/Rubric;

    .line 61
    .local v3, "sub":Lru/cn/api/catalogue/replies/Rubric;
    const/4 v5, 0x6

    new-array v5, v5, [Ljava/lang/Object;

    iget-wide v6, v3, Lru/cn/api/catalogue/replies/Rubric;->id:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v8

    iget-object v6, v3, Lru/cn/api/catalogue/replies/Rubric;->title:Ljava/lang/String;

    aput-object v6, v5, v9

    iget-object v6, v3, Lru/cn/api/catalogue/replies/Rubric;->description:Ljava/lang/String;

    aput-object v6, v5, v10

    .line 62
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v11

    const/4 v6, 0x0

    aput-object v6, v5, v12

    const/4 v6, 0x5

    iget-object v7, v1, Lru/cn/api/catalogue/replies/Rubric;->uiHint:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    invoke-virtual {v7}, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->getValue()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    .line 61
    invoke-virtual {v2, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0

    .line 67
    .end local v0    # "jsonRubric":Ljava/lang/String;
    .end local v1    # "rootRubric":Lru/cn/api/catalogue/replies/Rubric;
    .end local v3    # "sub":Lru/cn/api/catalogue/replies/Rubric;
    :cond_0
    return-object v2
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$NewsViewModel(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1

    invoke-direct {p0, p1}, Lru/cn/tv/mobile/stories/NewsViewModel;->mapNewsCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method final synthetic lambda$setRubricId$0$NewsViewModel(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "it"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lru/cn/tv/mobile/stories/NewsViewModel;->news:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v0, p1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method final synthetic lambda$setRubricId$1$NewsViewModel(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "throwable"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lru/cn/tv/mobile/stories/NewsViewModel;->news:Landroid/arch/lifecycle/MutableLiveData;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method public news()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lru/cn/tv/mobile/stories/NewsViewModel;->news:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method public setRubricId(J)V
    .locals 5
    .param p1, "rubricId"    # J

    .prologue
    .line 37
    invoke-virtual {p0}, Lru/cn/tv/mobile/stories/NewsViewModel;->unbindAll()V

    .line 39
    invoke-static {p1, p2}, Lru/cn/api/provider/TvContentProviderContract;->rubricatorUri(J)Landroid/net/Uri;

    move-result-object v0

    .line 41
    .local v0, "rubricUri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/mobile/stories/NewsViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v1

    .line 42
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 43
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lru/cn/tv/mobile/stories/NewsViewModel$$Lambda$0;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/stories/NewsViewModel$$Lambda$0;-><init>(Lru/cn/tv/mobile/stories/NewsViewModel;)V

    .line 44
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lru/cn/tv/mobile/stories/NewsViewModel$$Lambda$1;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/stories/NewsViewModel$$Lambda$1;-><init>(Lru/cn/tv/mobile/stories/NewsViewModel;)V

    new-instance v3, Lru/cn/tv/mobile/stories/NewsViewModel$$Lambda$2;

    invoke-direct {v3, p0}, Lru/cn/tv/mobile/stories/NewsViewModel$$Lambda$2;-><init>(Lru/cn/tv/mobile/stories/NewsViewModel;)V

    .line 45
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 41
    invoke-virtual {p0, v1}, Lru/cn/tv/mobile/stories/NewsViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 48
    return-void
.end method
