.class public Lru/cn/tv/billing/BillingFragment;
.super Lru/cn/tv/WebviewFragment;
.source "BillingFragment.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "SetJavaScriptEnabled"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/billing/BillingFragment$StoreJsObject;,
        Lru/cn/tv/billing/BillingFragment$BillingFragmentListener;
    }
.end annotation


# instance fields
.field private listener:Lru/cn/tv/billing/BillingFragment$BillingFragmentListener;

.field private needCallOnStoreReady:Z

.field private needReloadCacheOnStop:Z

.field private viewModel:Lru/cn/tv/billing/BillingViewModel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lru/cn/tv/WebviewFragment;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/tv/billing/BillingFragment;->needReloadCacheOnStop:Z

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/billing/BillingFragment;)Lru/cn/tv/billing/BillingFragment$BillingFragmentListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/billing/BillingFragment;

    .prologue
    .line 17
    iget-object v0, p0, Lru/cn/tv/billing/BillingFragment;->listener:Lru/cn/tv/billing/BillingFragment$BillingFragmentListener;

    return-object v0
.end method

.method public static newInstance(JJ)Lru/cn/tv/billing/BillingFragment;
    .locals 4
    .param p0, "contractorId"    # J
    .param p2, "channelId"    # J

    .prologue
    .line 46
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 47
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "contractor_id"

    invoke-virtual {v0, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 48
    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-lez v2, :cond_0

    .line 49
    const-string v2, "channel_id"

    invoke-virtual {v0, v2, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 52
    :cond_0
    new-instance v1, Lru/cn/tv/billing/BillingFragment;

    invoke-direct {v1}, Lru/cn/tv/billing/BillingFragment;-><init>()V

    .line 53
    .local v1, "fragment":Lru/cn/tv/billing/BillingFragment;
    invoke-virtual {v1, v0}, Lru/cn/tv/billing/BillingFragment;->setArguments(Landroid/os/Bundle;)V

    .line 54
    return-object v1
.end method

.method public static newInstance(Z)Lru/cn/tv/billing/BillingFragment;
    .locals 6
    .param p0, "peersTVPlus"    # Z

    .prologue
    .line 58
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 59
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "contractor_id"

    const-wide/16 v4, 0x2

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 60
    const-string v2, "peerstv_plus"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 62
    new-instance v1, Lru/cn/tv/billing/BillingFragment;

    invoke-direct {v1}, Lru/cn/tv/billing/BillingFragment;-><init>()V

    .line 63
    .local v1, "fragment":Lru/cn/tv/billing/BillingFragment;
    invoke-virtual {v1, v0}, Lru/cn/tv/billing/BillingFragment;->setArguments(Landroid/os/Bundle;)V

    .line 64
    return-object v1
.end method

.method private setPrivateOfficeURL(Ljava/lang/String;)V
    .locals 4
    .param p1, "url"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "AddJavascriptInterface"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 161
    iput-boolean v3, p0, Lru/cn/tv/billing/BillingFragment;->needCallOnStoreReady:Z

    .line 163
    iget-object v0, p0, Lru/cn/tv/billing/BillingFragment;->webView:Landroid/webkit/WebView;

    new-instance v1, Lru/cn/tv/billing/BillingFragment$StoreJsObject;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lru/cn/tv/billing/BillingFragment$StoreJsObject;-><init>(Lru/cn/tv/billing/BillingFragment;Lru/cn/tv/billing/BillingFragment$1;)V

    const-string v2, "store"

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    invoke-virtual {p0, p1, v3}, Lru/cn/tv/billing/BillingFragment;->load(Ljava/lang/String;Z)V

    .line 165
    return-void
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$BillingFragment(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/billing/BillingFragment;->setPrivateOfficeURL(Ljava/lang/String;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 69
    invoke-super {p0, p1}, Lru/cn/tv/WebviewFragment;->onCreate(Landroid/os/Bundle;)V

    .line 71
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v1

    const-class v7, Lru/cn/tv/billing/BillingViewModel;

    invoke-static {p0, v1, v7}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v1

    check-cast v1, Lru/cn/tv/billing/BillingViewModel;

    iput-object v1, p0, Lru/cn/tv/billing/BillingFragment;->viewModel:Lru/cn/tv/billing/BillingViewModel;

    .line 72
    iget-object v1, p0, Lru/cn/tv/billing/BillingFragment;->viewModel:Lru/cn/tv/billing/BillingViewModel;

    invoke-virtual {v1}, Lru/cn/tv/billing/BillingViewModel;->privateOfficeUrl()Landroid/arch/lifecycle/LiveData;

    move-result-object v1

    new-instance v7, Lru/cn/tv/billing/BillingFragment$$Lambda$0;

    invoke-direct {v7, p0}, Lru/cn/tv/billing/BillingFragment$$Lambda$0;-><init>(Lru/cn/tv/billing/BillingFragment;)V

    invoke-virtual {v1, p0, v7}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 74
    invoke-virtual {p0}, Lru/cn/tv/billing/BillingFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 75
    .local v0, "arguments":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 76
    const/4 v1, 0x1

    iput-boolean v1, p0, Lru/cn/tv/billing/BillingFragment;->needReloadCacheOnStop:Z

    .line 78
    const-string v1, "contractor_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 79
    .local v2, "contractorId":J
    const-string v1, "channel_id"

    const-wide/16 v8, 0x0

    invoke-virtual {v0, v1, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 80
    .local v4, "channelId":J
    const-string v1, "peerstv_plus"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    .line 82
    .local v6, "peersTVPlus":Z
    iget-object v1, p0, Lru/cn/tv/billing/BillingFragment;->viewModel:Lru/cn/tv/billing/BillingViewModel;

    const/4 v7, 0x0

    const-string v8, "http://close/"

    invoke-virtual/range {v1 .. v8}, Lru/cn/tv/billing/BillingViewModel;->setBillingParams(JJZZLjava/lang/String;)V

    .line 84
    .end local v2    # "contractorId":J
    .end local v4    # "channelId":J
    .end local v6    # "peersTVPlus":Z
    :cond_0
    return-void
.end method

.method protected onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 96
    invoke-super {p0, p1, p2}, Lru/cn/tv/WebviewFragment;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 97
    const-string v0, "BillingFragment"

    const-string v1, "onPageFinished"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget-boolean v0, p0, Lru/cn/tv/billing/BillingFragment;->needCallOnStoreReady:Z

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lru/cn/tv/billing/BillingFragment;->webView:Landroid/webkit/WebView;

    const-string v1, "javascript:readyEvent = document.createEvent(\'Events\'); readyEvent.initEvent(\'onStoreReady\');readyEvent.store = store;document.dispatchEvent(readyEvent);"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 105
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/tv/billing/BillingFragment;->needCallOnStoreReady:Z

    .line 106
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 110
    const-string v0, "BillingFragment"

    const-string v1, "billing stop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    iget-boolean v0, p0, Lru/cn/tv/billing/BillingFragment;->needReloadCacheOnStop:Z

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lru/cn/tv/billing/BillingFragment;->listener:Lru/cn/tv/billing/BillingFragment$BillingFragmentListener;

    invoke-interface {v0}, Lru/cn/tv/billing/BillingFragment$BillingFragmentListener;->onBillingFragmentStop()V

    .line 114
    :cond_0
    invoke-super {p0}, Lru/cn/tv/WebviewFragment;->onStop()V

    .line 115
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 88
    invoke-super {p0, p1, p2}, Lru/cn/tv/WebviewFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 91
    iget-object v0, p0, Lru/cn/tv/billing/BillingFragment;->webView:Landroid/webkit/WebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 92
    return-void
.end method

.method public pinAuthorize(JLjava/lang/String;)V
    .locals 9
    .param p1, "contractorId"    # J
    .param p3, "callback"    # Ljava/lang/String;

    .prologue
    .line 126
    iget-object v1, p0, Lru/cn/tv/billing/BillingFragment;->viewModel:Lru/cn/tv/billing/BillingViewModel;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-wide v2, p1

    move-object v8, p3

    invoke-virtual/range {v1 .. v8}, Lru/cn/tv/billing/BillingViewModel;->setBillingParams(JJZZLjava/lang/String;)V

    .line 127
    return-void
.end method

.method public purchaseOptions(JJ)V
    .locals 9
    .param p1, "contractorId"    # J
    .param p3, "channelId"    # J

    .prologue
    const/4 v6, 0x0

    .line 122
    iget-object v1, p0, Lru/cn/tv/billing/BillingFragment;->viewModel:Lru/cn/tv/billing/BillingViewModel;

    const-string v8, "http://close/"

    move-wide v2, p1

    move-wide v4, p3

    move v7, v6

    invoke-virtual/range {v1 .. v8}, Lru/cn/tv/billing/BillingViewModel;->setBillingParams(JJZZLjava/lang/String;)V

    .line 123
    return-void
.end method

.method public setListener(Lru/cn/tv/billing/BillingFragment$BillingFragmentListener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/tv/billing/BillingFragment$BillingFragmentListener;

    .prologue
    .line 118
    iput-object p1, p0, Lru/cn/tv/billing/BillingFragment;->listener:Lru/cn/tv/billing/BillingFragment$BillingFragmentListener;

    .line 119
    return-void
.end method
