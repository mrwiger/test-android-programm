.class Lru/inetra/bytefog/service/BytefogService$NetworkChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BytefogService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/inetra/bytefog/service/BytefogService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NetworkChangeReceiver"
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private isRegistered:Z

.field final synthetic this$0:Lru/inetra/bytefog/service/BytefogService;


# direct methods
.method public constructor <init>(Lru/inetra/bytefog/service/BytefogService;Landroid/content/Context;)V
    .locals 1
    .param p2, "c"    # Landroid/content/Context;

    .prologue
    .line 106
    iput-object p1, p0, Lru/inetra/bytefog/service/BytefogService$NetworkChangeReceiver;->this$0:Lru/inetra/bytefog/service/BytefogService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 104
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/inetra/bytefog/service/BytefogService$NetworkChangeReceiver;->isRegistered:Z

    .line 107
    iput-object p2, p0, Lru/inetra/bytefog/service/BytefogService$NetworkChangeReceiver;->context:Landroid/content/Context;

    .line 108
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 112
    iget-object v0, p0, Lru/inetra/bytefog/service/BytefogService$NetworkChangeReceiver;->this$0:Lru/inetra/bytefog/service/BytefogService;

    invoke-static {v0}, Lru/inetra/bytefog/service/BytefogService;->access$100(Lru/inetra/bytefog/service/BytefogService;)Lru/inetra/bytefog/service/BytefogLibrary;

    move-result-object v0

    invoke-virtual {v0}, Lru/inetra/bytefog/service/BytefogLibrary;->notifyNetworkChanged()V

    .line 113
    return-void
.end method

.method public register()V
    .locals 2

    .prologue
    .line 116
    iget-boolean v1, p0, Lru/inetra/bytefog/service/BytefogService$NetworkChangeReceiver;->isRegistered:Z

    if-nez v1, :cond_0

    .line 117
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 118
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 119
    iget-object v1, p0, Lru/inetra/bytefog/service/BytefogService$NetworkChangeReceiver;->context:Landroid/content/Context;

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 120
    const/4 v1, 0x1

    iput-boolean v1, p0, Lru/inetra/bytefog/service/BytefogService$NetworkChangeReceiver;->isRegistered:Z

    .line 122
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method public unregister()V
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lru/inetra/bytefog/service/BytefogService$NetworkChangeReceiver;->isRegistered:Z

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lru/inetra/bytefog/service/BytefogService$NetworkChangeReceiver;->context:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 127
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/inetra/bytefog/service/BytefogService$NetworkChangeReceiver;->isRegistered:Z

    .line 129
    :cond_0
    return-void
.end method
