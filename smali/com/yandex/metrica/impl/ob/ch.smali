.class public Lcom/yandex/metrica/impl/ob/ch;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/yandex/metrica/impl/ob/ck;

.field public final b:Lcom/yandex/metrica/impl/ob/cg;

.field public final c:Ljava/lang/String;

.field public final d:Z


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 36
    const-string v0, "package_name"

    .line 37
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "clte"

    .line 38
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    const-string v0, "flcc"

    .line 39
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    .line 1079
    if-nez v4, :cond_0

    move-object v0, v1

    .line 39
    :goto_0
    const-string v4, "blcc"

    .line 41
    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    .line 1084
    if-nez v4, :cond_1

    .line 36
    :goto_1
    invoke-direct {p0, v2, v3, v0, v1}, Lcom/yandex/metrica/impl/ob/ch;-><init>(Ljava/lang/String;ZLcom/yandex/metrica/impl/ob/ck;Lcom/yandex/metrica/impl/ob/cg;)V

    .line 44
    return-void

    .line 1079
    :cond_0
    new-instance v0, Lcom/yandex/metrica/impl/ob/ck;

    invoke-direct {v0, v4}, Lcom/yandex/metrica/impl/ob/ck;-><init>(Landroid/os/Bundle;)V

    goto :goto_0

    .line 1084
    :cond_1
    new-instance v1, Lcom/yandex/metrica/impl/ob/cg;

    invoke-direct {v1, v4}, Lcom/yandex/metrica/impl/ob/cg;-><init>(Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;ZLcom/yandex/metrica/impl/ob/ck;Lcom/yandex/metrica/impl/ob/cg;)V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/ch;->c:Ljava/lang/String;

    .line 121
    iput-boolean p2, p0, Lcom/yandex/metrica/impl/ob/ch;->d:Z

    .line 122
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/ch;->a:Lcom/yandex/metrica/impl/ob/ck;

    .line 123
    iput-object p4, p0, Lcom/yandex/metrica/impl/ob/ch;->b:Lcom/yandex/metrica/impl/ob/cg;

    .line 124
    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 47
    const-string v0, "package_name"

    .line 48
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "clte"

    const/4 v3, 0x1

    .line 49
    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    const-string v0, "flcc"

    .line 50
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 1090
    if-eqz v4, :cond_1

    .line 1091
    new-instance v0, Lcom/yandex/metrica/impl/ob/ck;

    invoke-direct {v0, v4}, Lcom/yandex/metrica/impl/ob/ck;-><init>(Lorg/json/JSONObject;)V

    .line 50
    :goto_0
    const-string v4, "blcc"

    .line 52
    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 1099
    if-eqz v4, :cond_0

    .line 1100
    new-instance v1, Lcom/yandex/metrica/impl/ob/cg;

    invoke-direct {v1, v4}, Lcom/yandex/metrica/impl/ob/cg;-><init>(Lorg/json/JSONObject;)V

    .line 47
    :cond_0
    invoke-direct {p0, v2, v3, v0, v1}, Lcom/yandex/metrica/impl/ob/ch;-><init>(Ljava/lang/String;ZLcom/yandex/metrica/impl/ob/ck;Lcom/yandex/metrica/impl/ob/cg;)V

    .line 55
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 59
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 61
    :try_start_0
    const-string v1, "package_name"

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ch;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 62
    const-string v1, "clte"

    iget-boolean v2, p0, Lcom/yandex/metrica/impl/ob/ch;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 63
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ch;->b:Lcom/yandex/metrica/impl/ob/cg;

    if-eqz v1, :cond_0

    .line 64
    const-string v1, "blcc"

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ch;->b:Lcom/yandex/metrica/impl/ob/cg;

    .line 65
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/cg;->a()Lorg/json/JSONObject;

    move-result-object v2

    .line 64
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 67
    :cond_0
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ch;->a:Lcom/yandex/metrica/impl/ob/ck;

    if-eqz v1, :cond_1

    .line 68
    const-string v1, "flcc"

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ch;->a:Lcom/yandex/metrica/impl/ob/ck;

    .line 69
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/ck;->a()Lorg/json/JSONObject;

    move-result-object v2

    .line 68
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :cond_1
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ClientConfig{foregroundConfig="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ch;->a:Lcom/yandex/metrica/impl/ob/ck;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", backgroundConfig="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ch;->b:Lcom/yandex/metrica/impl/ob/cg;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", packageName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ch;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLocationTrackingEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/yandex/metrica/impl/ob/ch;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
