.class final Lcom/samsung/multiscreen/Service$3;
.super Ljava/lang/Object;
.source "Service.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/multiscreen/Service;->getById(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/multiscreen/Result;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$result:Lcom/samsung/multiscreen/Result;

.field final synthetic val$results:Ljava/util/List;

.field final synthetic val$threads:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;Ljava/util/List;Lcom/samsung/multiscreen/Result;)V
    .locals 0

    .prologue
    .line 268
    iput-object p1, p0, Lcom/samsung/multiscreen/Service$3;->val$threads:Ljava/util/List;

    iput-object p2, p0, Lcom/samsung/multiscreen/Service$3;->val$results:Ljava/util/List;

    iput-object p3, p0, Lcom/samsung/multiscreen/Service$3;->val$result:Lcom/samsung/multiscreen/Result;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private chooseResult()V
    .locals 5

    .prologue
    .line 285
    const/4 v0, 0x0

    .line 289
    .local v0, "error":Lcom/samsung/multiscreen/Error;
    iget-object v3, p0, Lcom/samsung/multiscreen/Service$3;->val$results:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 290
    .local v1, "obj":Ljava/lang/Object;
    instance-of v4, v1, Lcom/samsung/multiscreen/Service;

    if-eqz v4, :cond_2

    move-object v2, v1

    .line 291
    check-cast v2, Lcom/samsung/multiscreen/Service;

    .line 292
    .local v2, "service":Lcom/samsung/multiscreen/Service;
    iget-object v3, p0, Lcom/samsung/multiscreen/Service$3;->val$result:Lcom/samsung/multiscreen/Result;

    invoke-interface {v3, v2}, Lcom/samsung/multiscreen/Result;->onSuccess(Ljava/lang/Object;)V

    .line 302
    .end local v1    # "obj":Ljava/lang/Object;
    .end local v2    # "service":Lcom/samsung/multiscreen/Service;
    :cond_1
    :goto_1
    return-void

    .line 294
    .restart local v1    # "obj":Ljava/lang/Object;
    :cond_2
    if-nez v0, :cond_0

    instance-of v4, v1, Lcom/samsung/multiscreen/Error;

    if-eqz v4, :cond_0

    move-object v0, v1

    .line 295
    check-cast v0, Lcom/samsung/multiscreen/Error;

    goto :goto_0

    .line 299
    .end local v1    # "obj":Ljava/lang/Object;
    :cond_3
    if-eqz v0, :cond_1

    .line 300
    iget-object v3, p0, Lcom/samsung/multiscreen/Service$3;->val$result:Lcom/samsung/multiscreen/Result;

    invoke-interface {v3, v0}, Lcom/samsung/multiscreen/Result;->onError(Lcom/samsung/multiscreen/Error;)V

    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 272
    iget-object v2, p0, Lcom/samsung/multiscreen/Service$3;->val$threads:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/multiscreen/ProviderThread;

    .line 274
    .local v1, "thread":Lcom/samsung/multiscreen/ProviderThread;
    :try_start_0
    invoke-virtual {v1}, Lcom/samsung/multiscreen/ProviderThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 275
    :catch_0
    move-exception v0

    .line 276
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 280
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v1    # "thread":Lcom/samsung/multiscreen/ProviderThread;
    :cond_0
    invoke-direct {p0}, Lcom/samsung/multiscreen/Service$3;->chooseResult()V

    .line 281
    return-void
.end method
