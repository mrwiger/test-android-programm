.class final Lru/cn/ad/AdMobManager$1;
.super Landroid/os/AsyncTask;
.source "AdMobManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/ad/AdMobManager;->loadBanner(Landroid/content/Context;Lcom/google/android/gms/ads/AdView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$adView:Lcom/google/android/gms/ads/AdView;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/AdView;)V
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lru/cn/ad/AdMobManager$1;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lru/cn/ad/AdMobManager$1;->val$adView:Lcom/google/android/gms/ads/AdView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lru/cn/ad/AdMobManager$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 7
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    const/4 v1, 0x0

    .line 25
    iget-object v4, p0, Lru/cn/ad/AdMobManager$1;->val$context:Landroid/content/Context;

    const-string v5, "Vq0Ps6K9"

    invoke-static {v4, v5}, Lru/cn/ad/AdsManager;->getPlace(Landroid/content/Context;Ljava/lang/String;)Lru/cn/api/money_miner/replies/AdPlace;

    move-result-object v0

    .line 26
    .local v0, "adPlace":Lru/cn/api/money_miner/replies/AdPlace;
    if-nez v0, :cond_1

    .line 44
    :cond_0
    :goto_0
    return-object v1

    .line 30
    :cond_1
    iget-object v4, v0, Lru/cn/api/money_miner/replies/AdPlace;->networks:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lru/cn/api/money_miner/replies/AdSystem;

    .line 31
    .local v3, "system":Lru/cn/api/money_miner/replies/AdSystem;
    iget v5, v3, Lru/cn/api/money_miner/replies/AdSystem;->type:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_2

    .line 32
    const-string v4, "banner_id"

    invoke-virtual {v3, v4}, Lru/cn/api/money_miner/replies/AdSystem;->getParam(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 33
    .local v1, "bannerId":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 34
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "network_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v3, Lru/cn/api/money_miner/replies/AdSystem;->id:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 35
    .local v2, "message":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ";place_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lru/cn/api/money_miner/replies/AdPlace;->placeId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 36
    sget-object v4, Lru/cn/domain/statistics/inetra/ErrorCode;->UNKNOWN_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v5, "MoneyMinerErrorDomain"

    const/16 v6, 0x66

    invoke-static {v4, v5, v6, v2}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lru/cn/ad/AdMobManager$1;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 5
    .param p1, "bannerId"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 50
    if-nez p1, :cond_0

    .line 66
    :goto_0
    return-void

    .line 53
    :cond_0
    iget-object v3, p0, Lru/cn/ad/AdMobManager$1;->val$adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v3, p1}, Lcom/google/android/gms/ads/AdView;->setAdUnitId(Ljava/lang/String;)V

    .line 55
    new-instance v1, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v1}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    .line 56
    .local v1, "adRequestBuilder":Lcom/google/android/gms/ads/AdRequest$Builder;
    iget-object v3, p0, Lru/cn/ad/AdMobManager$1;->val$context:Landroid/content/Context;

    invoke-static {v3}, Lru/cn/ad/AdsManager;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 57
    iget-object v3, p0, Lru/cn/ad/AdMobManager$1;->val$context:Landroid/content/Context;

    invoke-static {v3}, Lru/cn/ad/AdsManager;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/gms/ads/AdRequest$Builder;->addTestDevice(Ljava/lang/String;)Lcom/google/android/gms/ads/AdRequest$Builder;

    .line 59
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/gms/ads/AdRequest$Builder;->build()Lcom/google/android/gms/ads/AdRequest;

    move-result-object v0

    .line 62
    .local v0, "adRequest":Lcom/google/android/gms/ads/AdRequest;
    :try_start_0
    iget-object v3, p0, Lru/cn/ad/AdMobManager$1;->val$adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/ads/AdView;->loadAd(Lcom/google/android/gms/ads/AdRequest;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 63
    :catch_0
    move-exception v2

    .line 64
    .local v2, "e":Ljava/lang/Exception;
    invoke-static {}, Lru/cn/ad/AdMobManager;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Unable to load request"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
