.class public interface abstract Lcom/my/target/bd$a;
.super Ljava/lang/Object;
.source "AdditionalDataParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/bd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# static fields
.field public static final ID:Ljava/lang/String; = "id"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final URL:Ljava/lang/String; = "url"

.field public static final eA:Ljava/lang/String; = "hasPause"

.field public static final ep:Ljava/lang/String; = "doAfter"

.field public static final eq:Ljava/lang/String; = "doOnEmptyResponseFromId"

.field public static final er:Ljava/lang/String; = "isMidrollPoint"

.field public static final es:Ljava/lang/String; = "serviceStatistics"

.field public static final et:Ljava/lang/String; = "pointP"

.field public static final eu:Ljava/lang/String; = "point"

.field public static final ev:Ljava/lang/String; = "allowClose"

.field public static final ew:Ljava/lang/String; = "allowCloseDelay"

.field public static final ex:Ljava/lang/String; = "allowSeek"

.field public static final ey:Ljava/lang/String; = "allowSkip"

.field public static final ez:Ljava/lang/String; = "allowTrackChange"
