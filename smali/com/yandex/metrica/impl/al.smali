.class public Lcom/yandex/metrica/impl/al;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/yandex/metrica/impl/ak;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 28
    .line 36
    :try_start_0
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ak;->e()V

    .line 38
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ak;->d()Lcom/yandex/metrica/impl/ob/es;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/es;->a()Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 40
    const/4 v0, 0x2

    :try_start_1
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ak;->i()I

    move-result v2

    if-ne v0, v2, :cond_1

    .line 41
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ak;->j()[B

    move-result-object v3

    .line 43
    if-eqz v3, :cond_1

    array-length v0, v3

    if-lez v0, :cond_1

    .line 44
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ak;->m()Ljava/lang/String;

    move-result-object v0

    .line 1087
    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 1088
    const-string v2, "Accept-Encoding"

    invoke-virtual {v4, v2, v0}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1089
    const-string v2, "Content-Encoding"

    invoke-virtual {v4, v2, v0}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 47
    :try_start_2
    new-instance v2, Ljava/io/BufferedOutputStream;

    array-length v3, v3

    invoke-direct {v2, v0, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 49
    :try_start_3
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ak;->j()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write([B)V

    .line 50
    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    .line 52
    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->a(Ljava/io/Closeable;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-object v3, v0

    move-object v5, v2

    .line 56
    :goto_0
    :try_start_4
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    .line 57
    invoke-virtual {p1, v0}, Lcom/yandex/metrica/impl/ak;->a(I)V

    .line 58
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/yandex/metrica/impl/ak;->a(Ljava/util/Map;)V

    .line 60
    invoke-virtual {p1, v0}, Lcom/yandex/metrica/impl/ak;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    move-result-object v0

    .line 63
    :try_start_5
    new-instance v2, Ljava/io/BufferedInputStream;

    const/16 v6, 0x1f40

    invoke-direct {v2, v0, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 64
    :try_start_6
    invoke-static {v2}, Lcom/yandex/metrica/impl/s;->b(Ljava/io/InputStream;)[B

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/yandex/metrica/impl/ak;->b([B)V

    .line 65
    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->a(Ljava/io/Closeable;)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    move-object v1, v2

    .line 76
    :goto_1
    invoke-static {v5}, Lcom/yandex/metrica/impl/bl;->a(Ljava/io/Closeable;)V

    .line 77
    invoke-static {v1}, Lcom/yandex/metrica/impl/bl;->a(Ljava/io/Closeable;)V

    .line 78
    invoke-static {v3}, Lcom/yandex/metrica/impl/bl;->a(Ljava/io/Closeable;)V

    .line 79
    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->a(Ljava/io/Closeable;)V

    .line 80
    invoke-static {v4}, Lcom/yandex/metrica/impl/bl;->a(Ljava/net/HttpURLConnection;)V

    .line 81
    :goto_2
    return-void

    .line 67
    :cond_0
    :try_start_7
    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v2

    const/16 v6, 0x1f40

    invoke-direct {v0, v2, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_1

    .line 74
    :catch_0
    move-exception v0

    move-object v0, v1

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    :goto_3
    :try_start_8
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ak;->g()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_7

    .line 76
    invoke-static {v3}, Lcom/yandex/metrica/impl/bl;->a(Ljava/io/Closeable;)V

    .line 77
    invoke-static {v4}, Lcom/yandex/metrica/impl/bl;->a(Ljava/io/Closeable;)V

    .line 78
    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->a(Ljava/io/Closeable;)V

    .line 79
    invoke-static {v1}, Lcom/yandex/metrica/impl/bl;->a(Ljava/io/Closeable;)V

    .line 80
    invoke-static {v2}, Lcom/yandex/metrica/impl/bl;->a(Ljava/net/HttpURLConnection;)V

    goto :goto_2

    .line 76
    :catchall_0
    move-exception v0

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v2, v1

    :goto_4
    invoke-static {v5}, Lcom/yandex/metrica/impl/bl;->a(Ljava/io/Closeable;)V

    .line 77
    invoke-static {v2}, Lcom/yandex/metrica/impl/bl;->a(Ljava/io/Closeable;)V

    .line 78
    invoke-static {v3}, Lcom/yandex/metrica/impl/bl;->a(Ljava/io/Closeable;)V

    .line 79
    invoke-static {v1}, Lcom/yandex/metrica/impl/bl;->a(Ljava/io/Closeable;)V

    .line 80
    invoke-static {v4}, Lcom/yandex/metrica/impl/bl;->a(Ljava/net/HttpURLConnection;)V

    .line 81
    throw v0

    .line 76
    :catchall_1
    move-exception v0

    move-object v3, v1

    move-object v5, v1

    move-object v2, v1

    goto :goto_4

    :catchall_2
    move-exception v2

    move-object v3, v0

    move-object v5, v1

    move-object v0, v2

    move-object v2, v1

    goto :goto_4

    :catchall_3
    move-exception v3

    move-object v5, v2

    move-object v2, v1

    move-object v7, v3

    move-object v3, v0

    move-object v0, v7

    goto :goto_4

    :catchall_4
    move-exception v0

    move-object v2, v1

    goto :goto_4

    :catchall_5
    move-exception v2

    move-object v7, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_4

    :catchall_6
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_4

    :catchall_7
    move-exception v5

    move-object v7, v5

    move-object v5, v3

    move-object v3, v0

    move-object v0, v7

    move-object v8, v2

    move-object v2, v4

    move-object v4, v8

    goto :goto_4

    .line 74
    :catch_1
    move-exception v0

    move-object v0, v1

    move-object v2, v4

    move-object v3, v1

    move-object v4, v1

    goto :goto_3

    :catch_2
    move-exception v2

    move-object v2, v4

    move-object v3, v1

    move-object v4, v1

    goto :goto_3

    :catch_3
    move-exception v3

    move-object v3, v2

    move-object v2, v4

    move-object v4, v1

    goto :goto_3

    :catch_4
    move-exception v0

    move-object v0, v3

    move-object v2, v4

    move-object v3, v5

    move-object v4, v1

    goto :goto_3

    :catch_5
    move-exception v2

    move-object v2, v4

    move-object v4, v1

    move-object v1, v0

    move-object v0, v3

    move-object v3, v5

    goto :goto_3

    :catch_6
    move-exception v1

    move-object v1, v0

    move-object v0, v3

    move-object v3, v5

    move-object v7, v4

    move-object v4, v2

    move-object v2, v7

    goto :goto_3

    :cond_1
    move-object v3, v1

    move-object v5, v1

    goto/16 :goto_0
.end method
