.class final Lcom/google/obf/bd$b;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/bd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation


# instance fields
.field public final a:I

.field public b:I

.field public c:I

.field public d:J

.field private final e:Z

.field private final f:Lcom/google/obf/dw;

.field private final g:Lcom/google/obf/dw;

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>(Lcom/google/obf/dw;Lcom/google/obf/dw;Z)V
    .locals 3

    .prologue
    const/16 v2, 0xc

    const/4 v0, 0x1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/obf/bd$b;->g:Lcom/google/obf/dw;

    .line 3
    iput-object p2, p0, Lcom/google/obf/bd$b;->f:Lcom/google/obf/dw;

    .line 4
    iput-boolean p3, p0, Lcom/google/obf/bd$b;->e:Z

    .line 5
    invoke-virtual {p2, v2}, Lcom/google/obf/dw;->c(I)V

    .line 6
    invoke-virtual {p2}, Lcom/google/obf/dw;->s()I

    move-result v1

    iput v1, p0, Lcom/google/obf/bd$b;->a:I

    .line 7
    invoke-virtual {p1, v2}, Lcom/google/obf/dw;->c(I)V

    .line 8
    invoke-virtual {p1}, Lcom/google/obf/dw;->s()I

    move-result v1

    iput v1, p0, Lcom/google/obf/bd$b;->i:I

    .line 9
    invoke-virtual {p1}, Lcom/google/obf/dw;->m()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    const-string v1, "first_chunk must be 1"

    invoke-static {v0, v1}, Lcom/google/obf/dl;->b(ZLjava/lang/Object;)V

    .line 10
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/obf/bd$b;->b:I

    .line 11
    return-void

    .line 9
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Z
    .locals 2

    .prologue
    .line 12
    iget v0, p0, Lcom/google/obf/bd$b;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/bd$b;->b:I

    iget v1, p0, Lcom/google/obf/bd$b;->a:I

    if-ne v0, v1, :cond_0

    .line 13
    const/4 v0, 0x0

    .line 21
    :goto_0
    return v0

    .line 14
    :cond_0
    iget-boolean v0, p0, Lcom/google/obf/bd$b;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/obf/bd$b;->f:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->u()J

    move-result-wide v0

    .line 15
    :goto_1
    iput-wide v0, p0, Lcom/google/obf/bd$b;->d:J

    .line 16
    iget v0, p0, Lcom/google/obf/bd$b;->b:I

    iget v1, p0, Lcom/google/obf/bd$b;->h:I

    if-ne v0, v1, :cond_1

    .line 17
    iget-object v0, p0, Lcom/google/obf/bd$b;->g:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->s()I

    move-result v0

    iput v0, p0, Lcom/google/obf/bd$b;->c:I

    .line 18
    iget-object v0, p0, Lcom/google/obf/bd$b;->g:Lcom/google/obf/dw;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/obf/dw;->d(I)V

    .line 19
    iget v0, p0, Lcom/google/obf/bd$b;->i:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/obf/bd$b;->i:I

    if-lez v0, :cond_3

    .line 20
    iget-object v0, p0, Lcom/google/obf/bd$b;->g:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->s()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_2
    iput v0, p0, Lcom/google/obf/bd$b;->h:I

    .line 21
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 15
    :cond_2
    iget-object v0, p0, Lcom/google/obf/bd$b;->f:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->k()J

    move-result-wide v0

    goto :goto_1

    .line 20
    :cond_3
    const/4 v0, -0x1

    goto :goto_2
.end method
