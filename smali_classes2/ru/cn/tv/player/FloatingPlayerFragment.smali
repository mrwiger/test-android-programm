.class public Lru/cn/tv/player/FloatingPlayerFragment;
.super Landroid/support/v4/app/Fragment;
.source "FloatingPlayerFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/player/FloatingPlayerFragment$Listener;
    }
.end annotation


# instance fields
.field private draggableView:Lru/cn/draggableview/DraggableView;

.field private isTablet:Z

.field private listener:Lru/cn/tv/player/FloatingPlayerFragment$Listener;

.field private player:Lru/cn/tv/player/SimplePlayerFragmentEx;

.field private secondFragment:Landroid/support/v4/app/Fragment;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 31
    new-instance v0, Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-direct {v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;-><init>()V

    iput-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->player:Lru/cn/tv/player/SimplePlayerFragmentEx;

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->isTablet:Z

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/player/FloatingPlayerFragment;)Lru/cn/tv/player/SimplePlayerFragmentEx;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/FloatingPlayerFragment;

    .prologue
    .line 23
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->player:Lru/cn/tv/player/SimplePlayerFragmentEx;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/tv/player/FloatingPlayerFragment;)Lru/cn/tv/player/FloatingPlayerFragment$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/FloatingPlayerFragment;

    .prologue
    .line 23
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->listener:Lru/cn/tv/player/FloatingPlayerFragment$Listener;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/tv/player/FloatingPlayerFragment;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/FloatingPlayerFragment;

    .prologue
    .line 23
    iget-boolean v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->isTablet:Z

    return v0
.end method

.method static synthetic access$300(Lru/cn/tv/player/FloatingPlayerFragment;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/FloatingPlayerFragment;

    .prologue
    .line 23
    invoke-direct {p0}, Lru/cn/tv/player/FloatingPlayerFragment;->isHorizontal()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lru/cn/tv/player/FloatingPlayerFragment;)Landroid/support/v4/app/Fragment;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/FloatingPlayerFragment;

    .prologue
    .line 23
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->secondFragment:Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method static synthetic access$500(Lru/cn/tv/player/FloatingPlayerFragment;)Lru/cn/draggableview/DraggableView;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/FloatingPlayerFragment;

    .prologue
    .line 23
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->draggableView:Lru/cn/draggableview/DraggableView;

    return-object v0
.end method

.method private instantiateTelecastInfoFragment()V
    .locals 2

    .prologue
    .line 352
    iget-object v1, p0, Lru/cn/tv/player/FloatingPlayerFragment;->secondFragment:Landroid/support/v4/app/Fragment;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lru/cn/tv/player/FloatingPlayerFragment;->secondFragment:Landroid/support/v4/app/Fragment;

    instance-of v1, v1, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;

    if-nez v1, :cond_1

    .line 354
    :cond_0
    new-instance v0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;

    invoke-direct {v0}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;-><init>()V

    .line 355
    .local v0, "fragment":Lru/cn/tv/mobile/telecast/TelecastInfoFragment;
    new-instance v1, Lru/cn/tv/player/FloatingPlayerFragment$4;

    invoke-direct {v1, p0, v0}, Lru/cn/tv/player/FloatingPlayerFragment$4;-><init>(Lru/cn/tv/player/FloatingPlayerFragment;Lru/cn/tv/mobile/telecast/TelecastInfoFragment;)V

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->setListener(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)V

    .line 369
    invoke-direct {p0, v0}, Lru/cn/tv/player/FloatingPlayerFragment;->setSecondFragment(Landroid/support/v4/app/Fragment;)V

    .line 373
    .end local v0    # "fragment":Lru/cn/tv/mobile/telecast/TelecastInfoFragment;
    :goto_0
    return-void

    .line 371
    :cond_1
    iget-object v1, p0, Lru/cn/tv/player/FloatingPlayerFragment;->secondFragment:Landroid/support/v4/app/Fragment;

    check-cast v1, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;

    invoke-virtual {v1}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->hideNoScheduleText()V

    goto :goto_0
.end method

.method private isHorizontal()Z
    .locals 3

    .prologue
    .line 307
    invoke-virtual {p0}, Lru/cn/tv/player/FloatingPlayerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 308
    .local v0, "config":Landroid/content/res/Configuration;
    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setSecondFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1, "f"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 347
    iput-object p1, p0, Lru/cn/tv/player/FloatingPlayerFragment;->secondFragment:Landroid/support/v4/app/Fragment;

    .line 348
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->draggableView:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v0, p1}, Lru/cn/draggableview/DraggableView;->addSecondFragment(Landroid/support/v4/app/Fragment;)V

    .line 349
    return-void
.end method


# virtual methods
.method public close(Z)V
    .locals 1
    .param p1, "animate"    # Z

    .prologue
    .line 333
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->draggableView:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v0}, Lru/cn/draggableview/DraggableView;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 334
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->draggableView:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v0, p1}, Lru/cn/draggableview/DraggableView;->close(Z)V

    .line 336
    :cond_0
    return-void
.end method

.method public fullScreen(Z)V
    .locals 1
    .param p1, "fullScreen"    # Z

    .prologue
    .line 295
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->player:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-virtual {v0, p1}, Lru/cn/tv/player/SimplePlayerFragmentEx;->fullScreen(Z)V

    .line 296
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->draggableView:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v0, p1}, Lru/cn/draggableview/DraggableView;->lockInMax(Z)V

    .line 297
    invoke-virtual {p0}, Lru/cn/tv/player/FloatingPlayerFragment;->updateDraggableViewOrientation()V

    .line 298
    return-void
.end method

.method public hideLockWrapper()V
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->player:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->hideLockOverlay()V

    .line 340
    return-void
.end method

.method public isClosed()Z
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->draggableView:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v0}, Lru/cn/draggableview/DraggableView;->isClosed()Z

    move-result v0

    return v0
.end method

.method public isMaximized()Z
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->draggableView:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v0}, Lru/cn/draggableview/DraggableView;->isMaximized()Z

    move-result v0

    return v0
.end method

.method public isMinimized()Z
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->draggableView:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v0}, Lru/cn/draggableview/DraggableView;->isMinimized()Z

    move-result v0

    return v0
.end method

.method public maximize(Z)V
    .locals 2
    .param p1, "animate"    # Z

    .prologue
    .line 267
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->player:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 269
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->draggableView:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v0, p1}, Lru/cn/draggableview/DraggableView;->maximize(Z)V

    .line 270
    return-void
.end method

.method public minimize(Z)V
    .locals 2
    .param p1, "animate"    # Z

    .prologue
    .line 273
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->player:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->canMinimize()Z

    move-result v0

    if-nez v0, :cond_1

    .line 274
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lru/cn/tv/player/FloatingPlayerFragment;->close(Z)V

    .line 276
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->listener:Lru/cn/tv/player/FloatingPlayerFragment$Listener;

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->listener:Lru/cn/tv/player/FloatingPlayerFragment$Listener;

    invoke-interface {v0}, Lru/cn/tv/player/FloatingPlayerFragment$Listener;->onMinimize()V

    .line 284
    :cond_0
    :goto_0
    return-void

    .line 281
    :cond_1
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->player:Lru/cn/tv/player/SimplePlayerFragmentEx;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lru/cn/tv/player/SimplePlayerFragmentEx;->mustHideControls(Z)V

    .line 283
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->draggableView:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v0, p1}, Lru/cn/draggableview/DraggableView;->minimize(Z)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 302
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 303
    invoke-virtual {p0}, Lru/cn/tv/player/FloatingPlayerFragment;->updateDraggableViewOrientation()V

    .line 304
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 41
    invoke-virtual {p0}, Lru/cn/tv/player/FloatingPlayerFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lru/cn/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->isTablet:Z

    .line 42
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 48
    const v0, 0x7f0c009e

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 53
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 55
    const v0, 0x7f0900a2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lru/cn/draggableview/DraggableView;

    iput-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->draggableView:Lru/cn/draggableview/DraggableView;

    .line 56
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->draggableView:Lru/cn/draggableview/DraggableView;

    new-instance v1, Lru/cn/tv/player/FloatingPlayerFragment$1;

    invoke-direct {v1, p0}, Lru/cn/tv/player/FloatingPlayerFragment$1;-><init>(Lru/cn/tv/player/FloatingPlayerFragment;)V

    invoke-virtual {v0, v1}, Lru/cn/draggableview/DraggableView;->setListener(Lru/cn/draggableview/DraggableView$DraggableViewListener;)V

    .line 136
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->player:Lru/cn/tv/player/SimplePlayerFragmentEx;

    new-instance v1, Lru/cn/tv/player/FloatingPlayerFragment$2;

    invoke-direct {v1, p0}, Lru/cn/tv/player/FloatingPlayerFragment$2;-><init>(Lru/cn/tv/player/FloatingPlayerFragment;)V

    invoke-virtual {v0, v1}, Lru/cn/tv/player/SimplePlayerFragmentEx;->setListener(Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;)V

    .line 210
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->draggableView:Lru/cn/draggableview/DraggableView;

    invoke-virtual {p0}, Lru/cn/tv/player/FloatingPlayerFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/cn/draggableview/DraggableView;->setFragmentManager(Landroid/support/v4/app/FragmentManager;)V

    .line 211
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->draggableView:Lru/cn/draggableview/DraggableView;

    iget-object v1, p0, Lru/cn/tv/player/FloatingPlayerFragment;->player:Lru/cn/tv/player/SimplePlayerFragmentEx;

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v1, v2}, Lru/cn/draggableview/DraggableView;->addFirstFragment(Landroid/support/v4/app/Fragment;I)V

    .line 212
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->draggableView:Lru/cn/draggableview/DraggableView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lru/cn/draggableview/DraggableView;->close(Z)V

    .line 214
    invoke-virtual {p0}, Lru/cn/tv/player/FloatingPlayerFragment;->updateDraggableViewOrientation()V

    .line 215
    return-void
.end method

.method public playChannel(J)V
    .locals 1
    .param p1, "channelId"    # J

    .prologue
    .line 223
    invoke-direct {p0}, Lru/cn/tv/player/FloatingPlayerFragment;->instantiateTelecastInfoFragment()V

    .line 224
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->player:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-virtual {v0, p1, p2}, Lru/cn/tv/player/SimplePlayerFragmentEx;->playChannel(J)V

    .line 225
    return-void
.end method

.method public playStory(JJ)V
    .locals 3
    .param p1, "telecastId"    # J
    .param p3, "rubricId"    # J

    .prologue
    .line 233
    new-instance v0, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;

    invoke-direct {v0}, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;-><init>()V

    .line 235
    .local v0, "simpleRubricFragment":Lru/cn/tv/mobile/rubric/SimpleRubricFragment;
    invoke-virtual {v0, p3, p4}, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->setRubricId(J)Lru/cn/tv/mobile/rubric/RubricFragment;

    move-result-object v1

    new-instance v2, Lru/cn/tv/player/FloatingPlayerFragment$3;

    invoke-direct {v2, p0, p3, p4}, Lru/cn/tv/player/FloatingPlayerFragment$3;-><init>(Lru/cn/tv/player/FloatingPlayerFragment;J)V

    .line 236
    invoke-virtual {v1, v2}, Lru/cn/tv/mobile/rubric/RubricFragment;->setListener(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)Lru/cn/tv/mobile/rubric/RubricFragment;

    .line 248
    invoke-direct {p0, v0}, Lru/cn/tv/player/FloatingPlayerFragment;->setSecondFragment(Landroid/support/v4/app/Fragment;)V

    .line 250
    iget-object v1, p0, Lru/cn/tv/player/FloatingPlayerFragment;->player:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-virtual {v1, p1, p2}, Lru/cn/tv/player/SimplePlayerFragmentEx;->playTelecast(J)V

    .line 251
    return-void
.end method

.method public playTelecast(J)V
    .locals 1
    .param p1, "telecastId"    # J

    .prologue
    .line 228
    invoke-direct {p0}, Lru/cn/tv/player/FloatingPlayerFragment;->instantiateTelecastInfoFragment()V

    .line 229
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->player:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-virtual {v0, p1, p2}, Lru/cn/tv/player/SimplePlayerFragmentEx;->playTelecast(J)V

    .line 230
    return-void
.end method

.method public restartCurrentLocation()V
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->player:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->restartCurrentLocation()V

    .line 344
    return-void
.end method

.method public setListener(Lru/cn/tv/player/FloatingPlayerFragment$Listener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/tv/player/FloatingPlayerFragment$Listener;

    .prologue
    .line 218
    iput-object p1, p0, Lru/cn/tv/player/FloatingPlayerFragment;->listener:Lru/cn/tv/player/FloatingPlayerFragment$Listener;

    .line 219
    return-void
.end method

.method public setSubscribeListener(Lru/cn/tv/player/SimplePlayerFragmentEx$SubscribeListener;)V
    .locals 1
    .param p1, "listener"    # Lru/cn/tv/player/SimplePlayerFragmentEx$SubscribeListener;

    .prologue
    .line 329
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->player:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-virtual {v0, p1}, Lru/cn/tv/player/SimplePlayerFragmentEx;->setSubscribeListener(Lru/cn/tv/player/SimplePlayerFragmentEx$SubscribeListener;)V

    .line 330
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->player:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->stop()V

    .line 288
    return-void
.end method

.method public updateDraggableViewOrientation()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 313
    invoke-direct {p0}, Lru/cn/tv/player/FloatingPlayerFragment;->isHorizontal()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 314
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->draggableView:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v0, v1}, Lru/cn/draggableview/DraggableView;->setSecondViewVisible(Z)V

    .line 315
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->draggableView:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v0, v2}, Lru/cn/draggableview/DraggableView;->setThirdViewVisible(Z)V

    .line 316
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->draggableView:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v0, v2}, Lru/cn/draggableview/DraggableView;->Swap(Z)V

    .line 317
    iget-boolean v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->isTablet:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->player:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->isFullScreen()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 318
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->draggableView:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v0, v1}, Lru/cn/draggableview/DraggableView;->setSecondViewVisible(Z)V

    .line 319
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->draggableView:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v0, v1}, Lru/cn/draggableview/DraggableView;->setThirdViewVisible(Z)V

    .line 326
    :cond_1
    :goto_0
    return-void

    .line 322
    :cond_2
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->draggableView:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v0, v2}, Lru/cn/draggableview/DraggableView;->setSecondViewVisible(Z)V

    .line 323
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->draggableView:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v0, v1}, Lru/cn/draggableview/DraggableView;->setThirdViewVisible(Z)V

    .line 324
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment;->draggableView:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v0, v1}, Lru/cn/draggableview/DraggableView;->Swap(Z)V

    goto :goto_0
.end method
