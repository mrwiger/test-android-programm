.class public Lcom/yandex/metrica/impl/z;
.super Lcom/yandex/metrica/impl/b;
.source "SourceFile"

# interfaces
.implements Lcom/yandex/metrica/impl/ac;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/yandex/metrica/e;Lcom/yandex/metrica/impl/az;)V
    .locals 3

    .prologue
    .line 34
    invoke-virtual {p2}, Lcom/yandex/metrica/e;->getApiKey()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/yandex/metrica/impl/ax;

    invoke-direct {v1}, Lcom/yandex/metrica/impl/ax;-><init>()V

    invoke-direct {p0, p1, v0, p3, v1}, Lcom/yandex/metrica/impl/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/yandex/metrica/impl/az;Lcom/yandex/metrica/impl/ax;)V

    .line 37
    iget-object v0, p0, Lcom/yandex/metrica/impl/z;->a:Lcom/yandex/metrica/impl/ax;

    new-instance v1, Lcom/yandex/metrica/impl/ao;

    invoke-virtual {p2}, Lcom/yandex/metrica/e;->getPreloadInfo()Lcom/yandex/metrica/PreloadInfo;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/yandex/metrica/impl/ao;-><init>(Lcom/yandex/metrica/PreloadInfo;)V

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ax;->a(Lcom/yandex/metrica/impl/ao;)V

    .line 38
    return-void
.end method

.method private static c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Event received: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 69
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 71
    const-string v1, ". With value: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    :cond_0
    invoke-static {}, Lcom/yandex/metrica/impl/utils/l;->f()Lcom/yandex/metrica/impl/utils/l;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/utils/l;->a(Ljava/lang/String;)V

    .line 75
    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 84
    if-eqz p1, :cond_1

    .line 85
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 86
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    .line 88
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 89
    iget-object v1, p0, Lcom/yandex/metrica/impl/z;->b:Lcom/yandex/metrica/impl/az;

    invoke-static {v0}, Lcom/yandex/metrica/impl/q;->e(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    iget-object v2, p0, Lcom/yandex/metrica/impl/z;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v1, v0, v2}, Lcom/yandex/metrica/impl/az;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)V

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    invoke-static {}, Lcom/yandex/metrica/impl/utils/l;->f()Lcom/yandex/metrica/impl/utils/l;

    move-result-object v0

    const-string v1, "Null activity parameter for reportAppOpen(Activity)"

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/utils/l;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Landroid/app/Application;)V
    .locals 2

    .prologue
    .line 114
    const-string v0, "Application"

    invoke-static {p1, v0}, Lcom/yandex/metrica/impl/bl;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 116
    invoke-static {}, Lcom/yandex/metrica/impl/utils/l;->f()Lcom/yandex/metrica/impl/utils/l;

    move-result-object v0

    const-string v1, "Enable activity auto tracking"

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/utils/l;->a(Ljava/lang/String;)V

    .line 1125
    new-instance v0, Lcom/yandex/metrica/impl/n;

    invoke-direct {v0, p0}, Lcom/yandex/metrica/impl/n;-><init>(Lcom/yandex/metrica/impl/z;)V

    invoke-virtual {p1, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 121
    :goto_0
    return-void

    .line 119
    :cond_0
    invoke-static {}, Lcom/yandex/metrica/impl/utils/l;->f()Lcom/yandex/metrica/impl/utils/l;

    move-result-object v0

    const-string v1, "Could not enable activity auto tracking. API level should be more than 14 (ICE_CREAM_SANDWICH)"

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/utils/l;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Landroid/location/Location;)V
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/yandex/metrica/impl/z;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ax;->b()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/CounterConfiguration;->a(Landroid/location/Location;)V

    .line 168
    return-void
.end method

.method a(Lcom/yandex/metrica/e;Z)V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/yandex/metrica/impl/z;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ax;->b()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/CounterConfiguration;->a(Lcom/yandex/metrica/e;)V

    .line 146
    iget-object v0, p0, Lcom/yandex/metrica/impl/z;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ax;->b()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->l()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/z;->d(Z)V

    .line 148
    if-eqz p2, :cond_0

    .line 149
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/z;->b()V

    .line 151
    :cond_0
    invoke-virtual {p1}, Lcom/yandex/metrica/e;->j()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/z;->b(Ljava/util/Map;)V

    .line 152
    invoke-virtual {p1}, Lcom/yandex/metrica/e;->getErrorEnvironment()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/z;->a(Ljava/util/Map;)V

    .line 153
    return-void
.end method

.method a(Lcom/yandex/metrica/impl/k;)V
    .locals 0

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/yandex/metrica/impl/b;->a(Lcom/yandex/metrica/impl/k;)V

    .line 48
    return-void
.end method

.method a(Lcom/yandex/metrica/impl/ob/fz;)V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/yandex/metrica/impl/b;->a(Lcom/yandex/metrica/impl/ob/fz;)V

    .line 43
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 187
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    invoke-static {}, Lcom/yandex/metrica/impl/utils/l;->f()Lcom/yandex/metrica/impl/utils/l;

    move-result-object v0

    const-string v1, "Invalid Error Environment (key,value) pair: (%s,%s)."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/yandex/metrica/impl/utils/l;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 192
    :goto_0
    return-void

    .line 190
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/yandex/metrica/impl/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 196
    iget-object v0, p0, Lcom/yandex/metrica/impl/z;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ax;->b()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/CounterConfiguration;->d(Z)V

    .line 197
    iget-object v0, p0, Lcom/yandex/metrica/impl/z;->b:Lcom/yandex/metrica/impl/az;

    invoke-static {}, Lcom/yandex/metrica/impl/q;->a()Lcom/yandex/metrica/impl/i;

    move-result-object v1

    iget-object v2, p0, Lcom/yandex/metrica/impl/z;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0, v1, v2}, Lcom/yandex/metrica/impl/az;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)V

    .line 198
    return-void
.end method

.method public b(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/z;->d(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/z;->b(Ljava/lang/String;)V

    .line 130
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 177
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    invoke-static {}, Lcom/yandex/metrica/impl/utils/l;->f()Lcom/yandex/metrica/impl/utils/l;

    move-result-object v0

    const-string v1, "Invalid App Environment (key,value) pair: (%s,%s)."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    .line 179
    invoke-virtual {v0, v1, v2}, Lcom/yandex/metrica/impl/utils/l;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 183
    :goto_0
    return-void

    .line 181
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/yandex/metrica/impl/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/yandex/metrica/impl/z;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ax;->b()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/CounterConfiguration;->c(Z)V

    .line 173
    return-void
.end method

.method public c(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 133
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/z;->d(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/z;->c(Ljava/lang/String;)V

    .line 134
    return-void
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/yandex/metrica/impl/z;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ax;->b()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/CounterConfiguration;->a(Z)V

    .line 158
    return-void
.end method

.method d(Landroid/app/Activity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x0

    .line 138
    if-eqz p1, :cond_0

    .line 139
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 141
    :cond_0
    return-object v0
.end method

.method public d(Z)V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/yandex/metrica/impl/z;->b:Lcom/yandex/metrica/impl/az;

    iget-object v1, p0, Lcom/yandex/metrica/impl/z;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0, p1, v1}, Lcom/yandex/metrica/impl/az;->a(ZLcom/yandex/metrica/impl/ax;)V

    .line 163
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 98
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    invoke-static {}, Lcom/yandex/metrica/impl/utils/l;->f()Lcom/yandex/metrica/impl/utils/l;

    move-result-object v0

    const-string v1, "Null or empty deeplink value for reportAppOpen(String) was ignored."

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/utils/l;->b(Ljava/lang/String;)V

    .line 103
    :goto_0
    return-void

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/z;->b:Lcom/yandex/metrica/impl/az;

    invoke-static {p1}, Lcom/yandex/metrica/impl/q;->e(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v1

    iget-object v2, p0, Lcom/yandex/metrica/impl/z;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0, v1, v2}, Lcom/yandex/metrica/impl/az;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)V

    goto :goto_0
.end method

.method public f(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 106
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    invoke-static {}, Lcom/yandex/metrica/impl/utils/l;->f()Lcom/yandex/metrica/impl/utils/l;

    move-result-object v0

    const-string v1, "Null or empty referral url value for reportReferralUrl(String) was ignored"

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/utils/l;->b(Ljava/lang/String;)V

    .line 111
    :goto_0
    return-void

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/z;->b:Lcom/yandex/metrica/impl/az;

    invoke-static {p1}, Lcom/yandex/metrica/impl/q;->f(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v1

    iget-object v2, p0, Lcom/yandex/metrica/impl/z;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0, v1, v2}, Lcom/yandex/metrica/impl/az;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)V

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/yandex/metrica/impl/z;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ax;->b()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->k()Z

    move-result v0

    return v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/yandex/metrica/impl/z;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ax;->b()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->q()Z

    move-result v0

    return v0
.end method

.method public reportError(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 4
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "error"    # Ljava/lang/Throwable;

    .prologue
    .line 79
    invoke-super {p0, p1, p2}, Lcom/yandex/metrica/impl/b;->reportError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 80
    invoke-static {}, Lcom/yandex/metrica/impl/utils/l;->f()Lcom/yandex/metrica/impl/utils/l;

    move-result-object v0

    const-string v1, "Error received: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/yandex/metrica/impl/utils/l;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    return-void
.end method

.method public reportEvent(Ljava/lang/String;)V
    .locals 0
    .param p1, "eventName"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-super {p0, p1}, Lcom/yandex/metrica/impl/b;->reportEvent(Ljava/lang/String;)V

    .line 53
    return-void
.end method

.method public reportEvent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "eventName"    # Ljava/lang/String;
    .param p2, "jsonValue"    # Ljava/lang/String;

    .prologue
    .line 57
    invoke-super {p0, p1, p2}, Lcom/yandex/metrica/impl/b;->reportEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-static {p1, p2}, Lcom/yandex/metrica/impl/z;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method public reportEvent(Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .param p1, "eventName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    .local p2, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-super {p0, p1, p2}, Lcom/yandex/metrica/impl/b;->reportEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 64
    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {p1, v0}, Lcom/yandex/metrica/impl/z;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    return-void

    .line 64
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
