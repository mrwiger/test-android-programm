.class public final Lcom/my/target/eu;
.super Lcom/my/target/et;
.source "RecyclerHorizontalView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/eu$b;,
        Lcom/my/target/eu$a;
    }
.end annotation


# instance fields
.field private final dm:Landroid/view/View$OnClickListener;

.field private final du:Lcom/my/target/es;

.field private dv:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/my/target/eu;-><init>(Landroid/content/Context;B)V

    .line 50
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/my/target/eu;-><init>(Landroid/content/Context;C)V

    .line 55
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;C)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/my/target/et;-><init>(Landroid/content/Context;)V

    .line 27
    new-instance v0, Lcom/my/target/eu$1;

    invoke-direct {v0, p0}, Lcom/my/target/eu$1;-><init>(Lcom/my/target/eu;)V

    iput-object v0, p0, Lcom/my/target/eu;->dm:Landroid/view/View$OnClickListener;

    .line 60
    new-instance v0, Lcom/my/target/es;

    invoke-direct {v0, p1}, Lcom/my/target/es;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/eu;->du:Lcom/my/target/es;

    .line 62
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 63
    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 85
    return-void
.end method

.method public final c(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 73
    iput-object p1, p0, Lcom/my/target/eu;->dp:Ljava/util/List;

    .line 74
    new-instance v0, Lcom/my/target/eu$b;

    iget v1, p0, Lcom/my/target/eu;->dv:I

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/my/target/eu$b;-><init>(Ljava/util/List;ILandroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/eu;->dr:Lcom/my/target/er;

    .line 75
    iget-object v0, p0, Lcom/my/target/eu;->dr:Lcom/my/target/er;

    iget-object v1, p0, Lcom/my/target/eu;->dl:Landroid/view/View$OnClickListener;

    .line 1069
    iput-object v1, v0, Lcom/my/target/er;->dl:Landroid/view/View$OnClickListener;

    .line 76
    iget-object v0, p0, Lcom/my/target/eu;->dr:Lcom/my/target/er;

    iget-object v1, p0, Lcom/my/target/eu;->dm:Landroid/view/View$OnClickListener;

    .line 2058
    iput-object v1, v0, Lcom/my/target/er;->dm:Landroid/view/View$OnClickListener;

    .line 77
    iget-object v0, p0, Lcom/my/target/eu;->du:Lcom/my/target/es;

    invoke-virtual {p0, v0}, Lcom/my/target/eu;->setCardLayoutManager(Lcom/my/target/es;)V

    .line 78
    iget-object v0, p0, Lcom/my/target/eu;->dr:Lcom/my/target/er;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 79
    return-void
.end method

.method protected final getCardLayoutManager()Lcom/my/target/es;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/my/target/eu;->du:Lcom/my/target/es;

    return-object v0
.end method

.method public final setSideSlidesMargins(I)V
    .locals 0
    .param p1, "sideSlidesMargins"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/my/target/eu;->dv:I

    .line 68
    return-void
.end method
