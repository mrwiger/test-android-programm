.class public final Lru/cn/tv/mobile/channels/ChannelsAdapter;
.super Landroid/support/v4/widget/SimpleCursorAdapter;
.source "ChannelsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private final maskId:I

.field private final maskedPreloader:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 28
    const/4 v3, 0x0

    new-array v4, v2, [Ljava/lang/String;

    new-array v5, v2, [I

    const/4 v6, 0x2

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 31
    const v0, 0x7f080096

    iput v0, p0, Lru/cn/tv/mobile/channels/ChannelsAdapter;->maskId:I

    .line 32
    const v0, 0x7f080095

    iget v1, p0, Lru/cn/tv/mobile/channels/ChannelsAdapter;->maskId:I

    invoke-static {p1, v0, v1}, Lru/cn/utils/Utils;->maskedDrawable(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/mobile/channels/ChannelsAdapter;->maskedPreloader:Landroid/graphics/drawable/Drawable;

    .line 33
    return-void
.end method

.method static synthetic access$100(Lru/cn/tv/mobile/channels/ChannelsAdapter;Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;JJ)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/mobile/channels/ChannelsAdapter;
    .param p1, "x1"    # Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;
    .param p2, "x2"    # J
    .param p4, "x3"    # J

    .prologue
    .line 22
    invoke-direct/range {p0 .. p5}, Lru/cn/tv/mobile/channels/ChannelsAdapter;->setProgress(Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;JJ)V

    return-void
.end method

.method private setProgress(Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;JJ)V
    .locals 8
    .param p1, "holder"    # Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;
    .param p2, "time"    # J
    .param p4, "duration"    # J

    .prologue
    .line 118
    const/4 v1, 0x0

    .line 119
    .local v1, "p":I
    const-wide/16 v4, 0x0

    cmp-long v4, p4, v4

    if-lez v4, :cond_0

    .line 120
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long v2, v4, v6

    .line 121
    .local v2, "timeSec":J
    const-wide/16 v4, 0x64

    sub-long v6, v2, p2

    mul-long/2addr v4, v6

    div-long/2addr v4, p4

    long-to-int v1, v4

    .line 124
    .end local v2    # "timeSec":J
    :cond_0
    iget-object v4, p1, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->currentTelecastProgressIndicator:Landroid/view/View;

    .line 125
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 126
    .local v0, "lp":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v4, p1, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->currentTelecastProgressWrapper:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    mul-int/2addr v4, v1

    div-int/lit8 v4, v4, 0x64

    iput v4, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 127
    iget-object v4, p1, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->currentTelecastProgressIndicator:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 128
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 18
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 68
    move-object/from16 v8, p3

    check-cast v8, Lru/cn/api/provider/cursor/ChannelCursor;

    .line 69
    .local v8, "c":Lru/cn/api/provider/cursor/ChannelCursor;
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;

    .line 71
    .local v3, "holder":Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;
    invoke-virtual {v8}, Lru/cn/api/provider/cursor/ChannelCursor;->getFavourite()I

    move-result v9

    .line 72
    .local v9, "favourite":I
    invoke-virtual {v8}, Lru/cn/api/provider/cursor/ChannelCursor;->getTitle()Ljava/lang/String;

    move-result-object v17

    .line 73
    .local v17, "title":Ljava/lang/String;
    invoke-virtual {v8}, Lru/cn/api/provider/cursor/ChannelCursor;->getRecordable()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_1

    const/16 v16, 0x1

    .line 74
    .local v16, "recordable":Z
    :goto_0
    invoke-virtual {v8}, Lru/cn/api/provider/cursor/ChannelCursor;->getHasSchedule()I

    move-result v12

    .line 75
    .local v12, "hasSchedule":I
    invoke-virtual {v8}, Lru/cn/api/provider/cursor/ChannelCursor;->getChannelId()J

    move-result-wide v10

    .line 76
    .local v10, "cnId":J
    invoke-virtual {v8}, Lru/cn/api/provider/cursor/ChannelCursor;->getIsDenied()Z

    move-result v14

    .line 77
    .local v14, "isDenied":Z
    invoke-virtual {v8}, Lru/cn/api/provider/cursor/ChannelCursor;->getIsIntersections()I

    move-result v15

    .line 78
    .local v15, "isIntersections":I
    invoke-virtual {v8}, Lru/cn/api/provider/cursor/ChannelCursor;->getImage()Ljava/lang/String;

    move-result-object v13

    .line 80
    .local v13, "image":Ljava/lang/String;
    iget-object v4, v3, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->archiveIndicator:Landroid/view/View;

    if-eqz v16, :cond_2

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 81
    iget-object v4, v3, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->accessIndicator:Landroid/view/View;

    if-eqz v14, :cond_3

    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 83
    iget-object v2, v3, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->favouriteIndicator:Lru/cn/tv/FavouriteStar;

    if-eqz v2, :cond_0

    .line 84
    if-eqz v14, :cond_4

    .line 85
    iget-object v2, v3, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->favouriteIndicator:Lru/cn/tv/FavouriteStar;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Lru/cn/tv/FavouriteStar;->setVisibility(I)V

    .line 94
    :cond_0
    :goto_3
    iget-object v2, v3, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    if-eqz v13, :cond_7

    .line 96
    invoke-static/range {p2 .. p2}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v2

    .line 97
    invoke-virtual {v2, v13}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    .line 98
    invoke-virtual {v2}, Lcom/squareup/picasso/RequestCreator;->fit()Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lru/cn/tv/mobile/channels/ChannelsAdapter;->maskedPreloader:Landroid/graphics/drawable/Drawable;

    .line 99
    invoke-virtual {v2, v4}, Lcom/squareup/picasso/RequestCreator;->placeholder(Landroid/graphics/drawable/Drawable;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    new-instance v4, Lru/cn/utils/MaskTransformation;

    move-object/from16 v0, p0

    iget v5, v0, Lru/cn/tv/mobile/channels/ChannelsAdapter;->maskId:I

    move-object/from16 v0, p2

    invoke-direct {v4, v0, v5}, Lru/cn/utils/MaskTransformation;-><init>(Landroid/content/Context;I)V

    .line 100
    invoke-virtual {v2, v4}, Lcom/squareup/picasso/RequestCreator;->transform(Lcom/squareup/picasso/Transformation;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    iget-object v4, v3, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 108
    :goto_4
    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lru/cn/tv/mobile/channels/ChannelsAdapter;->setProgress(Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;JJ)V

    .line 109
    const-wide/16 v4, 0x0

    cmp-long v2, v10, v4

    if-lez v2, :cond_8

    const/4 v2, 0x1

    if-ne v12, v2, :cond_8

    .line 110
    iget-object v2, v3, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->telecastLoader:Lru/cn/tv/mobile/channels/CurrentTelecastLoader;

    invoke-virtual {v2, v10, v11}, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->setChannelId(J)V

    .line 115
    :goto_5
    return-void

    .line 73
    .end local v10    # "cnId":J
    .end local v12    # "hasSchedule":I
    .end local v13    # "image":Ljava/lang/String;
    .end local v14    # "isDenied":Z
    .end local v15    # "isIntersections":I
    .end local v16    # "recordable":Z
    :cond_1
    const/16 v16, 0x0

    goto :goto_0

    .line 80
    .restart local v10    # "cnId":J
    .restart local v12    # "hasSchedule":I
    .restart local v13    # "image":Ljava/lang/String;
    .restart local v14    # "isDenied":Z
    .restart local v15    # "isIntersections":I
    .restart local v16    # "recordable":Z
    :cond_2
    const/16 v2, 0x8

    goto :goto_1

    .line 81
    :cond_3
    const/16 v2, 0x8

    goto :goto_2

    .line 86
    :cond_4
    const/4 v2, 0x1

    if-ne v15, v2, :cond_5

    .line 87
    iget-object v2, v3, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->favouriteIndicator:Lru/cn/tv/FavouriteStar;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Lru/cn/tv/FavouriteStar;->setVisibility(I)V

    goto :goto_3

    .line 89
    :cond_5
    iget-object v2, v3, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->favouriteIndicator:Lru/cn/tv/FavouriteStar;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lru/cn/tv/FavouriteStar;->setVisibility(I)V

    .line 90
    iget-object v4, v3, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->favouriteIndicator:Lru/cn/tv/FavouriteStar;

    const/4 v2, 0x1

    if-ne v9, v2, :cond_6

    const/4 v2, 0x1

    :goto_6
    invoke-virtual {v4, v10, v11, v2}, Lru/cn/tv/FavouriteStar;->favourite(JZ)V

    goto :goto_3

    :cond_6
    const/4 v2, 0x0

    goto :goto_6

    .line 102
    :cond_7
    invoke-static/range {p2 .. p2}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v2

    const v4, 0x7f080094

    .line 103
    invoke-virtual {v2, v4}, Lcom/squareup/picasso/Picasso;->load(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    new-instance v4, Lru/cn/utils/MaskTransformation;

    move-object/from16 v0, p0

    iget v5, v0, Lru/cn/tv/mobile/channels/ChannelsAdapter;->maskId:I

    move-object/from16 v0, p2

    invoke-direct {v4, v0, v5}, Lru/cn/utils/MaskTransformation;-><init>(Landroid/content/Context;I)V

    .line 104
    invoke-virtual {v2, v4}, Lcom/squareup/picasso/RequestCreator;->transform(Lcom/squareup/picasso/Transformation;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    iget-object v4, v3, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    .line 105
    invoke-virtual {v2, v4}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    goto :goto_4

    .line 112
    :cond_8
    iget-object v2, v3, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->telecastLoader:Lru/cn/tv/mobile/channels/CurrentTelecastLoader;

    invoke-virtual {v2}, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->reset()V

    .line 113
    iget-object v2, v3, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->currentTelecast:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 37
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 38
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f0c0099

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 40
    .local v2, "view":Landroid/view/View;
    new-instance v0, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;

    invoke-direct {v0, v5}, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;-><init>(Lru/cn/tv/mobile/channels/ChannelsAdapter$1;)V

    .line 41
    .local v0, "holder":Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;
    const v3, 0x7f0901d7

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 42
    const v3, 0x7f0900f3

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v0, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    .line 43
    const v3, 0x7f090031

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v0, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->archiveIndicator:Landroid/view/View;

    .line 44
    const v3, 0x7f0900d2

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lru/cn/tv/FavouriteStar;

    iput-object v3, v0, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->favouriteIndicator:Lru/cn/tv/FavouriteStar;

    .line 45
    const v3, 0x7f090006

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v0, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->accessIndicator:Landroid/view/View;

    .line 46
    const v3, 0x7f09008e

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->currentTelecast:Landroid/widget/TextView;

    .line 47
    const v3, 0x7f090090

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v0, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->currentTelecastProgressWrapper:Landroid/view/View;

    .line 48
    const v3, 0x7f09008f

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v0, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->currentTelecastProgressIndicator:Landroid/view/View;

    .line 50
    new-instance v3, Lru/cn/tv/mobile/channels/ChannelsAdapter$1;

    invoke-direct {v3, p0, p1, v0}, Lru/cn/tv/mobile/channels/ChannelsAdapter$1;-><init>(Lru/cn/tv/mobile/channels/ChannelsAdapter;Landroid/content/Context;Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;)V

    iput-object v3, v0, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;->telecastLoader:Lru/cn/tv/mobile/channels/CurrentTelecastLoader;

    .line 61
    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 63
    return-object v2
.end method
