.class public final Lcom/my/target/core/engines/h;
.super Ljava/lang/Object;
.source "NativeAppwallAdEngine.java"

# interfaces
.implements Lcom/my/target/br$a;
.implements Lcom/my/target/common/MyTargetActivity$ActivityEngine;


# instance fields
.field private final a:Lcom/my/target/nativeads/NativeAppwallAd;

.field private b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/my/target/common/MyTargetActivity;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/my/target/br;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/my/target/nativeads/NativeAppwallAd;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/my/target/core/engines/h;->a:Lcom/my/target/nativeads/NativeAppwallAd;

    .line 56
    return-void
.end method

.method public static a(Lcom/my/target/nativeads/NativeAppwallAd;)Lcom/my/target/core/engines/h;
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lcom/my/target/core/engines/h;

    invoke-direct {v0, p0}, Lcom/my/target/core/engines/h;-><init>(Lcom/my/target/nativeads/NativeAppwallAd;)V

    return-object v0
.end method

.method private static a(Landroid/app/ActionBar;I)V
    .locals 5

    .prologue
    .line 269
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Landroid/app/ActionBar;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 270
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, p1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 271
    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/app/ActionBar;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    const/16 v4, 0x12

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 272
    invoke-virtual {p0, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 273
    return-void
.end method

.method private a(Landroid/view/ViewGroup;)V
    .locals 4

    .prologue
    .line 277
    new-instance v0, Lcom/my/target/fh;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/my/target/fh;-><init>(Landroid/content/Context;)V

    .line 278
    iget-object v1, p0, Lcom/my/target/core/engines/h;->a:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-virtual {v1}, Lcom/my/target/nativeads/NativeAppwallAd;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/fh;->setTitle(Ljava/lang/String;)V

    .line 279
    iget-object v1, p0, Lcom/my/target/core/engines/h;->a:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-virtual {v1}, Lcom/my/target/nativeads/NativeAppwallAd;->getTitleSupplementaryColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/fh;->setStripeColor(I)V

    .line 280
    iget-object v1, p0, Lcom/my/target/core/engines/h;->a:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-virtual {v1}, Lcom/my/target/nativeads/NativeAppwallAd;->getTitleBackgroundColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/fh;->setMainColor(I)V

    .line 281
    iget-object v1, p0, Lcom/my/target/core/engines/h;->a:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-virtual {v1}, Lcom/my/target/nativeads/NativeAppwallAd;->getTitleTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/fh;->setTitleColor(I)V

    .line 282
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v1

    const/16 v2, 0x34

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    .line 283
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v2, v3, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 284
    invoke-virtual {v0, v2}, Lcom/my/target/fh;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 285
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 287
    new-instance v1, Lcom/my/target/core/engines/h$1;

    invoke-direct {v1, p0}, Lcom/my/target/core/engines/h$1;-><init>(Lcom/my/target/core/engines/h;)V

    invoke-virtual {v0, v1}, Lcom/my/target/fh;->setOnCloseClickListener(Lcom/my/target/fh$a;)V

    .line 295
    return-void
.end method

.method private b(Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 299
    iget-object v0, p0, Lcom/my/target/core/engines/h;->a:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/my/target/nativeads/factories/NativeAppwallViewsFactory;->getAppwallView(Lcom/my/target/nativeads/NativeAppwallAd;Landroid/content/Context;)Lcom/my/target/nativeads/views/AppwallAdView;

    move-result-object v0

    .line 300
    iget-object v1, p0, Lcom/my/target/core/engines/h;->a:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-virtual {v1, v0}, Lcom/my/target/nativeads/NativeAppwallAd;->registerAppwallAdView(Lcom/my/target/nativeads/views/AppwallAdView;)V

    .line 301
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/my/target/nativeads/views/AppwallAdView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 302
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 303
    return-void
.end method


# virtual methods
.method public final a(Lcom/my/target/br;Landroid/widget/FrameLayout;)V
    .locals 3
    .param p1, "dialog"    # Lcom/my/target/br;
    .param p2, "rootLayout"    # Landroid/widget/FrameLayout;

    .prologue
    const/4 v2, -0x1

    .line 191
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/my/target/core/engines/h;->c:Ljava/lang/ref/WeakReference;

    .line 192
    iget-object v0, p0, Lcom/my/target/core/engines/h;->a:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-virtual {v0}, Lcom/my/target/nativeads/NativeAppwallAd;->isHideStatusBarInDialog()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    invoke-virtual {p1}, Lcom/my/target/br;->aT()V

    .line 197
    :cond_0
    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 199
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 200
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 201
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 202
    invoke-virtual {p2, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 204
    invoke-direct {p0, v1}, Lcom/my/target/core/engines/h;->a(Landroid/view/ViewGroup;)V

    .line 205
    invoke-direct {p0, v1}, Lcom/my/target/core/engines/h;->b(Landroid/view/ViewGroup;)V

    .line 206
    iget-object v0, p0, Lcom/my/target/core/engines/h;->a:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-virtual {v0}, Lcom/my/target/nativeads/NativeAppwallAd;->getListener()Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;

    move-result-object v0

    .line 207
    if-eqz v0, :cond_1

    .line 209
    iget-object v1, p0, Lcom/my/target/core/engines/h;->a:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-interface {v0, v1}, Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;->onDisplay(Lcom/my/target/nativeads/NativeAppwallAd;)V

    .line 211
    :cond_1
    return-void
.end method

.method public final aU()V
    .locals 2

    .prologue
    .line 222
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/core/engines/h;->c:Ljava/lang/ref/WeakReference;

    .line 223
    iget-object v0, p0, Lcom/my/target/core/engines/h;->a:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-virtual {v0}, Lcom/my/target/nativeads/NativeAppwallAd;->getListener()Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;

    move-result-object v0

    .line 224
    if-eqz v0, :cond_0

    .line 226
    iget-object v1, p0, Lcom/my/target/core/engines/h;->a:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-interface {v0, v1}, Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;->onDismiss(Lcom/my/target/nativeads/NativeAppwallAd;)V

    .line 228
    :cond_0
    return-void
.end method

.method public final destroy()V
    .locals 0

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/my/target/core/engines/h;->dismiss()V

    .line 101
    return-void
.end method

.method public final dismiss()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 84
    iget-object v0, p0, Lcom/my/target/core/engines/h;->b:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 85
    :goto_0
    if-eqz v0, :cond_2

    .line 87
    invoke-virtual {v0}, Lcom/my/target/common/MyTargetActivity;->finish()V

    .line 96
    :cond_0
    :goto_1
    return-void

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/engines/h;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/common/MyTargetActivity;

    goto :goto_0

    .line 91
    :cond_2
    iget-object v0, p0, Lcom/my/target/core/engines/h;->c:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_3

    move-object v0, v1

    .line 92
    :goto_2
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/my/target/br;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    invoke-virtual {v0}, Lcom/my/target/br;->dismiss()V

    goto :goto_1

    .line 91
    :cond_3
    iget-object v0, p0, Lcom/my/target/core/engines/h;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/br;

    goto :goto_2
.end method

.method public final i(Z)V
    .locals 0

    .prologue
    .line 217
    return-void
.end method

.method public final onActivityBackPressed()Z
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x1

    return v0
.end method

.method public final onActivityCreate(Lcom/my/target/common/MyTargetActivity;Landroid/content/Intent;Landroid/widget/FrameLayout;)V
    .locals 6
    .param p1, "activity"    # Lcom/my/target/common/MyTargetActivity;
    .param p3, "rootLayout"    # Landroid/widget/FrameLayout;

    .prologue
    const v3, 0x106000d

    const/4 v5, -0x1

    const/4 v4, 0x1

    .line 106
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/my/target/core/engines/h;->b:Ljava/lang/ref/WeakReference;

    .line 1232
    invoke-virtual {p1}, Lcom/my/target/common/MyTargetActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 1233
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_3

    .line 1235
    const/high16 v1, -0x80000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 1236
    const v1, 0x1030238

    invoke-virtual {p1, v1}, Lcom/my/target/common/MyTargetActivity;->setTheme(I)V

    .line 1237
    invoke-virtual {p1}, Lcom/my/target/common/MyTargetActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 1238
    if-eqz v1, :cond_0

    .line 1240
    iget-object v2, p0, Lcom/my/target/core/engines/h;->a:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-virtual {v2}, Lcom/my/target/nativeads/NativeAppwallAd;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 1241
    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setIcon(I)V

    .line 1242
    invoke-virtual {v1, v4}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 1243
    invoke-virtual {v1, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 1244
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    iget-object v3, p0, Lcom/my/target/core/engines/h;->a:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-virtual {v3}, Lcom/my/target/nativeads/NativeAppwallAd;->getTitleBackgroundColor()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1245
    iget-object v2, p0, Lcom/my/target/core/engines/h;->a:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-virtual {v2}, Lcom/my/target/nativeads/NativeAppwallAd;->getTitleTextColor()I

    move-result v2

    invoke-static {v1, v2}, Lcom/my/target/core/engines/h;->a(Landroid/app/ActionBar;I)V

    .line 1246
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setElevation(F)V

    .line 1248
    :cond_0
    iget-object v1, p0, Lcom/my/target/core/engines/h;->a:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-virtual {v1}, Lcom/my/target/nativeads/NativeAppwallAd;->getTitleSupplementaryColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 108
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/my/target/common/MyTargetActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-nez v0, :cond_4

    .line 110
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 111
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 112
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 113
    invoke-virtual {p3, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 114
    invoke-direct {p0, v0}, Lcom/my/target/core/engines/h;->a(Landroid/view/ViewGroup;)V

    .line 115
    invoke-direct {p0, v0}, Lcom/my/target/core/engines/h;->b(Landroid/view/ViewGroup;)V

    .line 122
    :goto_1
    iget-object v0, p0, Lcom/my/target/core/engines/h;->a:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-virtual {v0}, Lcom/my/target/nativeads/NativeAppwallAd;->getListener()Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;

    move-result-object v0

    .line 123
    if-eqz v0, :cond_2

    .line 125
    iget-object v1, p0, Lcom/my/target/core/engines/h;->a:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-interface {v0, v1}, Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;->onDisplay(Lcom/my/target/nativeads/NativeAppwallAd;)V

    .line 127
    :cond_2
    return-void

    .line 1250
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    .line 1252
    const v0, 0x1030119

    invoke-virtual {p1, v0}, Lcom/my/target/common/MyTargetActivity;->setTheme(I)V

    .line 1253
    invoke-virtual {p1}, Lcom/my/target/common/MyTargetActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 1254
    if-eqz v0, :cond_1

    .line 1256
    iget-object v1, p0, Lcom/my/target/core/engines/h;->a:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-virtual {v1}, Lcom/my/target/nativeads/NativeAppwallAd;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 1257
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget-object v2, p0, Lcom/my/target/core/engines/h;->a:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-virtual {v2}, Lcom/my/target/nativeads/NativeAppwallAd;->getTitleBackgroundColor()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1258
    iget-object v1, p0, Lcom/my/target/core/engines/h;->a:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-virtual {v1}, Lcom/my/target/nativeads/NativeAppwallAd;->getTitleTextColor()I

    move-result v1

    invoke-static {v0, v1}, Lcom/my/target/core/engines/h;->a(Landroid/app/ActionBar;I)V

    .line 1259
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setIcon(I)V

    .line 1260
    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 1261
    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    goto :goto_0

    .line 119
    :cond_4
    invoke-direct {p0, p3}, Lcom/my/target/core/engines/h;->b(Landroid/view/ViewGroup;)V

    goto :goto_1
.end method

.method public final onActivityDestroy()V
    .locals 2

    .prologue
    .line 156
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/core/engines/h;->b:Ljava/lang/ref/WeakReference;

    .line 157
    iget-object v0, p0, Lcom/my/target/core/engines/h;->a:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-virtual {v0}, Lcom/my/target/nativeads/NativeAppwallAd;->getListener()Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;

    move-result-object v0

    .line 158
    if-eqz v0, :cond_0

    .line 160
    iget-object v1, p0, Lcom/my/target/core/engines/h;->a:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-interface {v0, v1}, Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;->onDismiss(Lcom/my/target/nativeads/NativeAppwallAd;)V

    .line 162
    :cond_0
    return-void
.end method

.method public final onActivityOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 173
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 175
    iget-object v0, p0, Lcom/my/target/core/engines/h;->b:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/my/target/core/engines/h;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/common/MyTargetActivity;

    .line 178
    if-eqz v0, :cond_0

    .line 180
    invoke-virtual {v0}, Lcom/my/target/common/MyTargetActivity;->finish()V

    .line 181
    const/4 v0, 0x1

    .line 185
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActivityPause()V
    .locals 0

    .prologue
    .line 145
    return-void
.end method

.method public final onActivityResume()V
    .locals 0

    .prologue
    .line 151
    return-void
.end method

.method public final onActivityStart()V
    .locals 0

    .prologue
    .line 133
    return-void
.end method

.method public final onActivityStop()V
    .locals 0

    .prologue
    .line 139
    return-void
.end method

.method public final showDialog(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/my/target/core/engines/h;->c:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 72
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/my/target/br;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    const-string v0, "NativeAppwallAdEngine.showDialog: dialog already showing"

    invoke-static {v0}, Lcom/my/target/g;->c(Ljava/lang/String;)V

    .line 80
    :goto_1
    return-void

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/engines/h;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/br;

    goto :goto_0

    .line 78
    :cond_1
    invoke-static {p0, p1}, Lcom/my/target/br;->a(Lcom/my/target/br$a;Landroid/content/Context;)Lcom/my/target/br;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Lcom/my/target/br;->show()V

    goto :goto_1
.end method
