.class public final Lcom/my/target/g;
.super Ljava/lang/Object;
.source "Tracer.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "[myTarget]"

.field public static enabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    sput-boolean v0, Lcom/my/target/g;->enabled:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 25
    sget-boolean v0, Lcom/my/target/g;->enabled:Z

    if-eqz v0, :cond_0

    .line 27
    const-string v0, "[myTarget]"

    if-eqz p0, :cond_1

    :goto_0
    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    :cond_0
    return-void

    .line 27
    :cond_1
    const-string p0, "null"

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 33
    sget-boolean v0, Lcom/my/target/g;->enabled:Z

    if-eqz v0, :cond_0

    .line 35
    const-string v0, "[myTarget]"

    if-eqz p0, :cond_1

    :goto_0
    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    :cond_0
    return-void

    .line 35
    :cond_1
    const-string p0, "null"

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 41
    const-string v0, "[myTarget]"

    if-eqz p0, :cond_0

    :goto_0
    invoke-static {v0, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    return-void

    .line 41
    :cond_0
    const-string p0, "null"

    goto :goto_0
.end method
