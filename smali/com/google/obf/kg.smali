.class public Lcom/google/obf/kg;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/kg;->a:I

    .line 3
    return-void
.end method

.method private b(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/util/Comparator",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 24
    instance-of v0, p1, [J

    if-eqz v0, :cond_0

    .line 25
    check-cast p1, [J

    check-cast p1, [J

    check-cast p2, [J

    check-cast p2, [J

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/kg;->a([J[J)Lcom/google/obf/kg;

    .line 41
    :goto_0
    return-void

    .line 26
    :cond_0
    instance-of v0, p1, [I

    if-eqz v0, :cond_1

    .line 27
    check-cast p1, [I

    check-cast p1, [I

    check-cast p2, [I

    check-cast p2, [I

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/kg;->a([I[I)Lcom/google/obf/kg;

    goto :goto_0

    .line 28
    :cond_1
    instance-of v0, p1, [S

    if-eqz v0, :cond_2

    .line 29
    check-cast p1, [S

    check-cast p1, [S

    check-cast p2, [S

    check-cast p2, [S

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/kg;->a([S[S)Lcom/google/obf/kg;

    goto :goto_0

    .line 30
    :cond_2
    instance-of v0, p1, [C

    if-eqz v0, :cond_3

    .line 31
    check-cast p1, [C

    check-cast p1, [C

    check-cast p2, [C

    check-cast p2, [C

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/kg;->a([C[C)Lcom/google/obf/kg;

    goto :goto_0

    .line 32
    :cond_3
    instance-of v0, p1, [B

    if-eqz v0, :cond_4

    .line 33
    check-cast p1, [B

    check-cast p1, [B

    check-cast p2, [B

    check-cast p2, [B

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/kg;->a([B[B)Lcom/google/obf/kg;

    goto :goto_0

    .line 34
    :cond_4
    instance-of v0, p1, [D

    if-eqz v0, :cond_5

    .line 35
    check-cast p1, [D

    check-cast p1, [D

    check-cast p2, [D

    check-cast p2, [D

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/kg;->a([D[D)Lcom/google/obf/kg;

    goto :goto_0

    .line 36
    :cond_5
    instance-of v0, p1, [F

    if-eqz v0, :cond_6

    .line 37
    check-cast p1, [F

    check-cast p1, [F

    check-cast p2, [F

    check-cast p2, [F

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/kg;->a([F[F)Lcom/google/obf/kg;

    goto :goto_0

    .line 38
    :cond_6
    instance-of v0, p1, [Z

    if-eqz v0, :cond_7

    .line 39
    check-cast p1, [Z

    check-cast p1, [Z

    check-cast p2, [Z

    check-cast p2, [Z

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/kg;->a([Z[Z)Lcom/google/obf/kg;

    goto :goto_0

    .line 40
    :cond_7
    check-cast p1, [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Object;

    check-cast p2, [Ljava/lang/Object;

    check-cast p2, [Ljava/lang/Object;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/obf/kg;->a([Ljava/lang/Object;[Ljava/lang/Object;Ljava/util/Comparator;)Lcom/google/obf/kg;

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 231
    iget v0, p0, Lcom/google/obf/kg;->a:I

    return v0
.end method

.method public a(BB)Lcom/google/obf/kg;
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/google/obf/kg;->a:I

    if-eqz v0, :cond_0

    .line 61
    :goto_0
    return-object p0

    .line 60
    :cond_0
    if-ge p1, p2, :cond_1

    const/4 v0, -0x1

    :goto_1
    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    :cond_1
    if-le p1, p2, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(CC)Lcom/google/obf/kg;
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/google/obf/kg;->a:I

    if-eqz v0, :cond_0

    .line 57
    :goto_0
    return-object p0

    .line 56
    :cond_0
    if-ge p1, p2, :cond_1

    const/4 v0, -0x1

    :goto_1
    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    :cond_1
    if-le p1, p2, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(DD)Lcom/google/obf/kg;
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/google/obf/kg;->a:I

    if-eqz v0, :cond_0

    .line 65
    :goto_0
    return-object p0

    .line 64
    :cond_0
    invoke-static {p1, p2, p3, p4}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0
.end method

.method public a(FF)Lcom/google/obf/kg;
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/google/obf/kg;->a:I

    if-eqz v0, :cond_0

    .line 69
    :goto_0
    return-object p0

    .line 68
    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0
.end method

.method public a(II)Lcom/google/obf/kg;
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/google/obf/kg;->a:I

    if-eqz v0, :cond_0

    .line 49
    :goto_0
    return-object p0

    .line 48
    :cond_0
    if-ge p1, p2, :cond_1

    const/4 v0, -0x1

    :goto_1
    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    :cond_1
    if-le p1, p2, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(JJ)Lcom/google/obf/kg;
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/google/obf/kg;->a:I

    if-eqz v0, :cond_0

    .line 45
    :goto_0
    return-object p0

    .line 44
    :cond_0
    cmp-long v0, p1, p3

    if-gez v0, :cond_1

    const/4 v0, -0x1

    :goto_1
    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    :cond_1
    cmp-long v0, p1, p3

    if-lez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/obf/kg;
    .locals 1

    .prologue
    .line 4
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/obf/kg;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lcom/google/obf/kg;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lcom/google/obf/kg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/util/Comparator",
            "<*>;)",
            "Lcom/google/obf/kg;"
        }
    .end annotation

    .prologue
    .line 5
    iget v0, p0, Lcom/google/obf/kg;->a:I

    if-eqz v0, :cond_1

    .line 23
    :cond_0
    :goto_0
    return-object p0

    .line 7
    :cond_1
    if-eq p1, p2, :cond_0

    .line 9
    if-nez p1, :cond_2

    .line 10
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    .line 12
    :cond_2
    if-nez p2, :cond_3

    .line 13
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    .line 15
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 16
    invoke-direct {p0, p1, p2, p3}, Lcom/google/obf/kg;->b(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)V

    goto :goto_0

    .line 17
    :cond_4
    if-nez p3, :cond_5

    .line 18
    check-cast p1, Ljava/lang/Comparable;

    .line 19
    invoke-interface {p1, p2}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    .line 22
    :cond_5
    invoke-interface {p3, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0
.end method

.method public a(SS)Lcom/google/obf/kg;
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/google/obf/kg;->a:I

    if-eqz v0, :cond_0

    .line 53
    :goto_0
    return-object p0

    .line 52
    :cond_0
    if-ge p1, p2, :cond_1

    const/4 v0, -0x1

    :goto_1
    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    :cond_1
    if-le p1, p2, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(ZZ)Lcom/google/obf/kg;
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/google/obf/kg;->a:I

    if-eqz v0, :cond_1

    .line 77
    :cond_0
    :goto_0
    return-object p0

    .line 72
    :cond_1
    if-eq p1, p2, :cond_0

    .line 74
    if-nez p1, :cond_2

    .line 75
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    .line 76
    :cond_2
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0
.end method

.method public a([B[B)Lcom/google/obf/kg;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 163
    iget v2, p0, Lcom/google/obf/kg;->a:I

    if-eqz v2, :cond_1

    .line 179
    :cond_0
    :goto_0
    return-object p0

    .line 165
    :cond_1
    if-eq p1, p2, :cond_0

    .line 167
    if-nez p1, :cond_2

    .line 168
    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    .line 170
    :cond_2
    if-nez p2, :cond_3

    .line 171
    iput v1, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    .line 173
    :cond_3
    array-length v2, p1

    array-length v3, p2

    if-eq v2, v3, :cond_5

    .line 174
    array-length v2, p1

    array-length v3, p2

    if-ge v2, v3, :cond_4

    :goto_1
    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 176
    :cond_5
    const/4 v0, 0x0

    :goto_2
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget v1, p0, Lcom/google/obf/kg;->a:I

    if-nez v1, :cond_0

    .line 177
    aget-byte v1, p1, v0

    aget-byte v2, p2, v0

    invoke-virtual {p0, v1, v2}, Lcom/google/obf/kg;->a(BB)Lcom/google/obf/kg;

    .line 178
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public a([C[C)Lcom/google/obf/kg;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 146
    iget v2, p0, Lcom/google/obf/kg;->a:I

    if-eqz v2, :cond_1

    .line 162
    :cond_0
    :goto_0
    return-object p0

    .line 148
    :cond_1
    if-eq p1, p2, :cond_0

    .line 150
    if-nez p1, :cond_2

    .line 151
    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    .line 153
    :cond_2
    if-nez p2, :cond_3

    .line 154
    iput v1, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    .line 156
    :cond_3
    array-length v2, p1

    array-length v3, p2

    if-eq v2, v3, :cond_5

    .line 157
    array-length v2, p1

    array-length v3, p2

    if-ge v2, v3, :cond_4

    :goto_1
    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 159
    :cond_5
    const/4 v0, 0x0

    :goto_2
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget v1, p0, Lcom/google/obf/kg;->a:I

    if-nez v1, :cond_0

    .line 160
    aget-char v1, p1, v0

    aget-char v2, p2, v0

    invoke-virtual {p0, v1, v2}, Lcom/google/obf/kg;->a(CC)Lcom/google/obf/kg;

    .line 161
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public a([D[D)Lcom/google/obf/kg;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 180
    iget v2, p0, Lcom/google/obf/kg;->a:I

    if-eqz v2, :cond_1

    .line 196
    :cond_0
    :goto_0
    return-object p0

    .line 182
    :cond_1
    if-eq p1, p2, :cond_0

    .line 184
    if-nez p1, :cond_2

    .line 185
    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    .line 187
    :cond_2
    if-nez p2, :cond_3

    .line 188
    iput v1, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    .line 190
    :cond_3
    array-length v2, p1

    array-length v3, p2

    if-eq v2, v3, :cond_5

    .line 191
    array-length v2, p1

    array-length v3, p2

    if-ge v2, v3, :cond_4

    :goto_1
    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 193
    :cond_5
    const/4 v0, 0x0

    :goto_2
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget v1, p0, Lcom/google/obf/kg;->a:I

    if-nez v1, :cond_0

    .line 194
    aget-wide v2, p1, v0

    aget-wide v4, p2, v0

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/google/obf/kg;->a(DD)Lcom/google/obf/kg;

    .line 195
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public a([F[F)Lcom/google/obf/kg;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 197
    iget v2, p0, Lcom/google/obf/kg;->a:I

    if-eqz v2, :cond_1

    .line 213
    :cond_0
    :goto_0
    return-object p0

    .line 199
    :cond_1
    if-eq p1, p2, :cond_0

    .line 201
    if-nez p1, :cond_2

    .line 202
    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    .line 204
    :cond_2
    if-nez p2, :cond_3

    .line 205
    iput v1, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    .line 207
    :cond_3
    array-length v2, p1

    array-length v3, p2

    if-eq v2, v3, :cond_5

    .line 208
    array-length v2, p1

    array-length v3, p2

    if-ge v2, v3, :cond_4

    :goto_1
    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 210
    :cond_5
    const/4 v0, 0x0

    :goto_2
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget v1, p0, Lcom/google/obf/kg;->a:I

    if-nez v1, :cond_0

    .line 211
    aget v1, p1, v0

    aget v2, p2, v0

    invoke-virtual {p0, v1, v2}, Lcom/google/obf/kg;->a(FF)Lcom/google/obf/kg;

    .line 212
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public a([I[I)Lcom/google/obf/kg;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 112
    iget v2, p0, Lcom/google/obf/kg;->a:I

    if-eqz v2, :cond_1

    .line 128
    :cond_0
    :goto_0
    return-object p0

    .line 114
    :cond_1
    if-eq p1, p2, :cond_0

    .line 116
    if-nez p1, :cond_2

    .line 117
    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    .line 119
    :cond_2
    if-nez p2, :cond_3

    .line 120
    iput v1, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    .line 122
    :cond_3
    array-length v2, p1

    array-length v3, p2

    if-eq v2, v3, :cond_5

    .line 123
    array-length v2, p1

    array-length v3, p2

    if-ge v2, v3, :cond_4

    :goto_1
    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 125
    :cond_5
    const/4 v0, 0x0

    :goto_2
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget v1, p0, Lcom/google/obf/kg;->a:I

    if-nez v1, :cond_0

    .line 126
    aget v1, p1, v0

    aget v2, p2, v0

    invoke-virtual {p0, v1, v2}, Lcom/google/obf/kg;->a(II)Lcom/google/obf/kg;

    .line 127
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public a([J[J)Lcom/google/obf/kg;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 95
    iget v2, p0, Lcom/google/obf/kg;->a:I

    if-eqz v2, :cond_1

    .line 111
    :cond_0
    :goto_0
    return-object p0

    .line 97
    :cond_1
    if-eq p1, p2, :cond_0

    .line 99
    if-nez p1, :cond_2

    .line 100
    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    .line 102
    :cond_2
    if-nez p2, :cond_3

    .line 103
    iput v1, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    .line 105
    :cond_3
    array-length v2, p1

    array-length v3, p2

    if-eq v2, v3, :cond_5

    .line 106
    array-length v2, p1

    array-length v3, p2

    if-ge v2, v3, :cond_4

    :goto_1
    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 108
    :cond_5
    const/4 v0, 0x0

    :goto_2
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget v1, p0, Lcom/google/obf/kg;->a:I

    if-nez v1, :cond_0

    .line 109
    aget-wide v2, p1, v0

    aget-wide v4, p2, v0

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/google/obf/kg;->a(JJ)Lcom/google/obf/kg;

    .line 110
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public a([Ljava/lang/Object;[Ljava/lang/Object;Ljava/util/Comparator;)Lcom/google/obf/kg;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            "[",
            "Ljava/lang/Object;",
            "Ljava/util/Comparator",
            "<*>;)",
            "Lcom/google/obf/kg;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 78
    iget v2, p0, Lcom/google/obf/kg;->a:I

    if-eqz v2, :cond_1

    .line 94
    :cond_0
    :goto_0
    return-object p0

    .line 80
    :cond_1
    if-eq p1, p2, :cond_0

    .line 82
    if-nez p1, :cond_2

    .line 83
    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    .line 85
    :cond_2
    if-nez p2, :cond_3

    .line 86
    iput v1, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    .line 88
    :cond_3
    array-length v2, p1

    array-length v3, p2

    if-eq v2, v3, :cond_5

    .line 89
    array-length v2, p1

    array-length v3, p2

    if-ge v2, v3, :cond_4

    :goto_1
    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 91
    :cond_5
    const/4 v0, 0x0

    :goto_2
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget v1, p0, Lcom/google/obf/kg;->a:I

    if-nez v1, :cond_0

    .line 92
    aget-object v1, p1, v0

    aget-object v2, p2, v0

    invoke-virtual {p0, v1, v2, p3}, Lcom/google/obf/kg;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lcom/google/obf/kg;

    .line 93
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public a([S[S)Lcom/google/obf/kg;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 129
    iget v2, p0, Lcom/google/obf/kg;->a:I

    if-eqz v2, :cond_1

    .line 145
    :cond_0
    :goto_0
    return-object p0

    .line 131
    :cond_1
    if-eq p1, p2, :cond_0

    .line 133
    if-nez p1, :cond_2

    .line 134
    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    .line 136
    :cond_2
    if-nez p2, :cond_3

    .line 137
    iput v1, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    .line 139
    :cond_3
    array-length v2, p1

    array-length v3, p2

    if-eq v2, v3, :cond_5

    .line 140
    array-length v2, p1

    array-length v3, p2

    if-ge v2, v3, :cond_4

    :goto_1
    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 142
    :cond_5
    const/4 v0, 0x0

    :goto_2
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget v1, p0, Lcom/google/obf/kg;->a:I

    if-nez v1, :cond_0

    .line 143
    aget-short v1, p1, v0

    aget-short v2, p2, v0

    invoke-virtual {p0, v1, v2}, Lcom/google/obf/kg;->a(SS)Lcom/google/obf/kg;

    .line 144
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public a([Z[Z)Lcom/google/obf/kg;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 214
    iget v2, p0, Lcom/google/obf/kg;->a:I

    if-eqz v2, :cond_1

    .line 230
    :cond_0
    :goto_0
    return-object p0

    .line 216
    :cond_1
    if-eq p1, p2, :cond_0

    .line 218
    if-nez p1, :cond_2

    .line 219
    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    .line 221
    :cond_2
    if-nez p2, :cond_3

    .line 222
    iput v1, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    .line 224
    :cond_3
    array-length v2, p1

    array-length v3, p2

    if-eq v2, v3, :cond_5

    .line 225
    array-length v2, p1

    array-length v3, p2

    if-ge v2, v3, :cond_4

    :goto_1
    iput v0, p0, Lcom/google/obf/kg;->a:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 227
    :cond_5
    const/4 v0, 0x0

    :goto_2
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget v1, p0, Lcom/google/obf/kg;->a:I

    if-nez v1, :cond_0

    .line 228
    aget-boolean v1, p1, v0

    aget-boolean v2, p2, v0

    invoke-virtual {p0, v1, v2}, Lcom/google/obf/kg;->a(ZZ)Lcom/google/obf/kg;

    .line 229
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method
