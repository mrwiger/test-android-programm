.class final Lcom/google/obf/ga;
.super Lcom/google/obf/ew;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/obf/ew",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/obf/eg;

.field private final b:Lcom/google/obf/ew;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/ew",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/reflect/Type;


# direct methods
.method constructor <init>(Lcom/google/obf/eg;Lcom/google/obf/ew;Ljava/lang/reflect/Type;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/eg;",
            "Lcom/google/obf/ew",
            "<TT;>;",
            "Ljava/lang/reflect/Type;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/obf/ew;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/obf/ga;->a:Lcom/google/obf/eg;

    .line 3
    iput-object p2, p0, Lcom/google/obf/ga;->b:Lcom/google/obf/ew;

    .line 4
    iput-object p3, p0, Lcom/google/obf/ga;->c:Ljava/lang/reflect/Type;

    .line 5
    return-void
.end method

.method private a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/reflect/Type;
    .locals 1

    .prologue
    .line 18
    if-eqz p2, :cond_1

    const-class v0, Ljava/lang/Object;

    if-eq p1, v0, :cond_0

    instance-of v0, p1, Ljava/lang/reflect/TypeVariable;

    if-nez v0, :cond_0

    instance-of v0, p1, Ljava/lang/Class;

    if-eqz v0, :cond_1

    .line 19
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    .line 20
    :cond_1
    return-object p1
.end method


# virtual methods
.method public read(Lcom/google/obf/ge;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/ge;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6
    iget-object v0, p0, Lcom/google/obf/ga;->b:Lcom/google/obf/ew;

    invoke-virtual {v0, p1}, Lcom/google/obf/ew;->read(Lcom/google/obf/ge;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public write(Lcom/google/obf/gg;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/gg;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7
    iget-object v0, p0, Lcom/google/obf/ga;->b:Lcom/google/obf/ew;

    .line 8
    iget-object v1, p0, Lcom/google/obf/ga;->c:Ljava/lang/reflect/Type;

    invoke-direct {p0, v1, p2}, Lcom/google/obf/ga;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/reflect/Type;

    move-result-object v1

    .line 9
    iget-object v2, p0, Lcom/google/obf/ga;->c:Ljava/lang/reflect/Type;

    if-eq v1, v2, :cond_0

    .line 10
    iget-object v0, p0, Lcom/google/obf/ga;->a:Lcom/google/obf/eg;

    invoke-static {v1}, Lcom/google/obf/gd;->a(Ljava/lang/reflect/Type;)Lcom/google/obf/gd;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/obf/eg;->a(Lcom/google/obf/gd;)Lcom/google/obf/ew;

    move-result-object v0

    .line 11
    instance-of v1, v0, Lcom/google/obf/fw$a;

    if-nez v1, :cond_1

    .line 16
    :cond_0
    :goto_0
    invoke-virtual {v0, p1, p2}, Lcom/google/obf/ew;->write(Lcom/google/obf/gg;Ljava/lang/Object;)V

    .line 17
    return-void

    .line 13
    :cond_1
    iget-object v1, p0, Lcom/google/obf/ga;->b:Lcom/google/obf/ew;

    instance-of v1, v1, Lcom/google/obf/fw$a;

    if-nez v1, :cond_0

    .line 14
    iget-object v0, p0, Lcom/google/obf/ga;->b:Lcom/google/obf/ew;

    goto :goto_0
.end method
