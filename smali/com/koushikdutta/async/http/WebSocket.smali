.class public interface abstract Lcom/koushikdutta/async/http/WebSocket;
.super Ljava/lang/Object;
.source "WebSocket.java"

# interfaces
.implements Lcom/koushikdutta/async/AsyncSocket;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/koushikdutta/async/http/WebSocket$PongCallback;,
        Lcom/koushikdutta/async/http/WebSocket$StringCallback;
    }
.end annotation


# virtual methods
.method public abstract send(Ljava/lang/String;)V
.end method

.method public abstract send([B)V
.end method

.method public abstract setStringCallback(Lcom/koushikdutta/async/http/WebSocket$StringCallback;)V
.end method
