.class final Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;
.super Ljava/lang/Object;
.source "BytefogSession.java"

# interfaces
.implements Lru/inetra/bytefog/service/InstanceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/utils/bytefog/BytefogSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "BytefogWrapper"
.end annotation


# instance fields
.field private errorListener:Lru/cn/utils/bytefog/BytefogSession$OnErrorListener;

.field private final eventHandler:Landroid/os/Handler;

.field private magnetUri:Landroid/net/Uri;

.field private readyListener:Lru/cn/utils/bytefog/BytefogSession$OnReadyListener;


# direct methods
.method public constructor <init>(Landroid/net/Uri;)V
    .locals 1
    .param p1, "magnetUri"    # Landroid/net/Uri;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;->eventHandler:Landroid/os/Handler;

    .line 63
    iput-object p1, p0, Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;->magnetUri:Landroid/net/Uri;

    .line 64
    return-void
.end method

.method static synthetic access$000(Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;)Lru/cn/utils/bytefog/BytefogSession$OnErrorListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;

    .prologue
    .line 54
    iget-object v0, p0, Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;->errorListener:Lru/cn/utils/bytefog/BytefogSession$OnErrorListener;

    return-object v0
.end method

.method private getLocalUri()Ljava/lang/String;
    .locals 3

    .prologue
    .line 85
    invoke-static {}, Lru/inetra/bytefog/service/Bytefog;->instance()Lru/inetra/bytefog/service/Instance;

    move-result-object v1

    iget-object v2, p0, Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;->magnetUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lru/inetra/bytefog/service/Instance;->convertBytefogMagnetToHlsPlaylistUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 86
    .local v0, "localUri":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 87
    iget-object v1, p0, Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;->errorListener:Lru/cn/utils/bytefog/BytefogSession$OnErrorListener;

    if-eqz v1, :cond_0

    .line 88
    iget-object v1, p0, Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;->errorListener:Lru/cn/utils/bytefog/BytefogSession$OnErrorListener;

    const/16 v2, -0x4652

    invoke-interface {v1, v2}, Lru/cn/utils/bytefog/BytefogSession$OnErrorListener;->onError(I)V

    .line 91
    :cond_0
    return-object v0
.end method


# virtual methods
.method public onBytefogServiceDown()Z
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;->errorListener:Lru/cn/utils/bytefog/BytefogSession$OnErrorListener;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;->errorListener:Lru/cn/utils/bytefog/BytefogSession$OnErrorListener;

    const/16 v1, -0x4651

    invoke-interface {v0, v1}, Lru/cn/utils/bytefog/BytefogSession$OnErrorListener;->onError(I)V

    .line 125
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onBytefogServiceReady()V
    .locals 2

    .prologue
    .line 96
    iget-object v1, p0, Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;->readyListener:Lru/cn/utils/bytefog/BytefogSession$OnReadyListener;

    if-eqz v1, :cond_0

    .line 97
    invoke-direct {p0}, Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;->getLocalUri()Ljava/lang/String;

    move-result-object v0

    .line 98
    .local v0, "localUri":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 104
    .end local v0    # "localUri":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 101
    .restart local v0    # "localUri":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;->readyListener:Lru/cn/utils/bytefog/BytefogSession$OnReadyListener;

    invoke-interface {v1, v0}, Lru/cn/utils/bytefog/BytefogSession$OnReadyListener;->onReady(Ljava/lang/String;)V

    .line 102
    const/4 v1, 0x0

    iput-object v1, p0, Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;->readyListener:Lru/cn/utils/bytefog/BytefogSession$OnReadyListener;

    goto :goto_0
.end method

.method public onStreamError(I)V
    .locals 2
    .param p1, "code"    # I

    .prologue
    .line 108
    iget-object v0, p0, Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;->eventHandler:Landroid/os/Handler;

    new-instance v1, Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper$1;

    invoke-direct {v1, p0, p1}, Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper$1;-><init>(Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 116
    return-void
.end method

.method setOnErrorListener(Lru/cn/utils/bytefog/BytefogSession$OnErrorListener;)V
    .locals 0
    .param p1, "errorListener"    # Lru/cn/utils/bytefog/BytefogSession$OnErrorListener;

    .prologue
    .line 67
    iput-object p1, p0, Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;->errorListener:Lru/cn/utils/bytefog/BytefogSession$OnErrorListener;

    .line 68
    return-void
.end method

.method waitReady(Lru/cn/utils/bytefog/BytefogSession$OnReadyListener;)V
    .locals 2
    .param p1, "readyListener"    # Lru/cn/utils/bytefog/BytefogSession$OnReadyListener;

    .prologue
    .line 71
    invoke-static {}, Lru/inetra/bytefog/service/Bytefog;->instance()Lru/inetra/bytefog/service/Instance;

    move-result-object v1

    invoke-interface {v1}, Lru/inetra/bytefog/service/Instance;->isReady()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 72
    invoke-direct {p0}, Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;->getLocalUri()Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, "localUri":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 82
    .end local v0    # "localUri":Ljava/lang/String;
    :goto_0
    return-void

    .line 76
    .restart local v0    # "localUri":Ljava/lang/String;
    :cond_0
    invoke-interface {p1, v0}, Lru/cn/utils/bytefog/BytefogSession$OnReadyListener;->onReady(Ljava/lang/String;)V

    goto :goto_0

    .line 78
    .end local v0    # "localUri":Ljava/lang/String;
    :cond_1
    iput-object p1, p0, Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;->readyListener:Lru/cn/utils/bytefog/BytefogSession$OnReadyListener;

    .line 80
    invoke-static {}, Lru/inetra/bytefog/service/Bytefog;->instance()Lru/inetra/bytefog/service/Instance;

    move-result-object v1

    invoke-interface {v1}, Lru/inetra/bytefog/service/Instance;->start()V

    goto :goto_0
.end method
