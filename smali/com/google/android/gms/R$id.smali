.class public final Lcom/google/android/gms/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final ad_container:I = 0x7f09001b

.field public static final ad_image_view:I = 0x7f09001e

.field public static final ad_in_progress_label:I = 0x7f09001f

.field public static final ad_label:I = 0x7f090020

.field public static final adjust_height:I = 0x7f090028

.field public static final adjust_width:I = 0x7f090029

.field public static final audio_list_view:I = 0x7f090034

.field public static final auto:I = 0x7f090036

.field public static final background_image_view:I = 0x7f090037

.field public static final background_place_holder_image_view:I = 0x7f090038

.field public static final blurred_background_image_view:I = 0x7f09003e

.field public static final button:I = 0x7f090046

.field public static final button_0:I = 0x7f090048

.field public static final button_1:I = 0x7f090049

.field public static final button_2:I = 0x7f09004a

.field public static final button_3:I = 0x7f09004b

.field public static final button_play_pause_toggle:I = 0x7f09004e

.field public static final cast_button_type_closed_caption:I = 0x7f090053

.field public static final cast_button_type_custom:I = 0x7f090054

.field public static final cast_button_type_empty:I = 0x7f090055

.field public static final cast_button_type_forward_30_seconds:I = 0x7f090056

.field public static final cast_button_type_mute_toggle:I = 0x7f090057

.field public static final cast_button_type_play_pause_toggle:I = 0x7f090058

.field public static final cast_button_type_rewind_30_seconds:I = 0x7f090059

.field public static final cast_button_type_skip_next:I = 0x7f09005a

.field public static final cast_button_type_skip_previous:I = 0x7f09005b

.field public static final cast_featurehighlight_help_text_body_view:I = 0x7f09005e

.field public static final cast_featurehighlight_help_text_header_view:I = 0x7f09005f

.field public static final cast_featurehighlight_view:I = 0x7f090060

.field public static final cast_notification_id:I = 0x7f090061

.field public static final center:I = 0x7f090066

.field public static final container_all:I = 0x7f090083

.field public static final container_current:I = 0x7f090084

.field public static final controllers:I = 0x7f09008a

.field public static final dark:I = 0x7f090094

.field public static final date:I = 0x7f090095

.field public static final end_text:I = 0x7f0900b7

.field public static final expanded_controller_layout:I = 0x7f0900d0

.field public static final icon_only:I = 0x7f0900f0

.field public static final icon_view:I = 0x7f0900f1

.field public static final light:I = 0x7f090101

.field public static final live_stream_indicator:I = 0x7f09010a

.field public static final live_stream_seek_bar:I = 0x7f09010b

.field public static final loading_indicator:I = 0x7f090113

.field public static final none:I = 0x7f09014d

.field public static final normal:I = 0x7f09014e

.field public static final progressBar:I = 0x7f090176

.field public static final radio:I = 0x7f09017a

.field public static final seek_bar:I = 0x7f09019b

.field public static final seek_bar_controls:I = 0x7f09019c

.field public static final standard:I = 0x7f0901b3

.field public static final start_text:I = 0x7f0901b5

.field public static final status_text:I = 0x7f0901b7

.field public static final subtitle_view:I = 0x7f0901be

.field public static final tab_host:I = 0x7f0901c3

.field public static final text:I = 0x7f0901ca

.field public static final text2:I = 0x7f0901cb

.field public static final textTitle:I = 0x7f0901ce

.field public static final text_list_view:I = 0x7f0901d0

.field public static final title_view:I = 0x7f0901db

.field public static final toolbar:I = 0x7f0901dc

.field public static final wide:I = 0x7f0901f9

.field public static final wrap_content:I = 0x7f0901fb
