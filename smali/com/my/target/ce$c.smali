.class Lcom/my/target/ce$c;
.super Lcom/my/target/ce$d;
.source "ClickHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/ce;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/my/target/ah;)V
    .locals 1

    .prologue
    .line 252
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/my/target/ce$d;-><init>(Ljava/lang/String;Lcom/my/target/ah;Lcom/my/target/ce$1;)V

    .line 253
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcom/my/target/ah;Lcom/my/target/ce$1;)V
    .locals 0

    .prologue
    .line 248
    invoke-direct {p0, p1, p2}, Lcom/my/target/ce$c;-><init>(Ljava/lang/String;Lcom/my/target/ah;)V

    return-void
.end method

.method private j(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 277
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 278
    instance-of v1, p2, Landroid/app/Activity;

    if-nez v1, :cond_0

    .line 280
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 282
    :cond_0
    invoke-virtual {p2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 283
    const/4 v0, 0x1

    .line 288
    :goto_0
    return v0

    .line 285
    :catch_0
    move-exception v0

    .line 288
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 4

    .prologue
    .line 295
    :try_start_0
    iget-object v0, p0, Lcom/my/target/ce$c;->jK:Lcom/my/target/ah;

    invoke-virtual {v0}, Lcom/my/target/ah;->isUsePlayStoreAction()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 297
    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 298
    if-eqz v0, :cond_1

    .line 300
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.android.vending"

    const-string v3, "com.google.android.finsky.activities.LaunchUrlHandlerActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 302
    instance-of v1, p2, Landroid/app/Activity;

    if-nez v1, :cond_0

    .line 304
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 306
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 307
    invoke-virtual {p2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 319
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 324
    :goto_1
    return v0

    .line 312
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 313
    instance-of v1, p2, Landroid/app/Activity;

    if-nez v1, :cond_3

    .line 315
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 317
    :cond_3
    invoke-virtual {p2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 321
    :catch_0
    move-exception v0

    .line 324
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected r(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 258
    iget-object v1, p0, Lcom/my/target/ce$c;->url:Ljava/lang/String;

    invoke-static {v1}, Lcom/my/target/cn;->T(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 260
    iget-object v1, p0, Lcom/my/target/ce$c;->url:Ljava/lang/String;

    invoke-direct {p0, v1, p1}, Lcom/my/target/ce$c;->j(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 270
    :cond_0
    :goto_0
    return v0

    .line 265
    :cond_1
    iget-object v1, p0, Lcom/my/target/ce$c;->url:Ljava/lang/String;

    invoke-direct {p0, v1, p1}, Lcom/my/target/ce$c;->k(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 270
    :cond_2
    invoke-super {p0, p1}, Lcom/my/target/ce$d;->r(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method
