.class public Lcom/yandex/metrica/impl/ob/dy;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/yandex/metrica/impl/bn;

.field private final b:Lcom/yandex/metrica/impl/ob/gi;

.field private final c:Lcom/yandex/metrica/impl/ob/en;

.field private final d:Lcom/yandex/metrica/impl/ob/dz;

.field private final e:Lcom/yandex/metrica/impl/ob/dr;

.field private final f:Lcom/yandex/metrica/impl/utils/p;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/yandex/metrica/impl/bn;Lcom/yandex/metrica/impl/ob/gi;Lcom/yandex/metrica/impl/ob/en;Lcom/yandex/metrica/impl/ob/dr;)V
    .locals 7

    .prologue
    .line 44
    new-instance v4, Lcom/yandex/metrica/impl/ob/dz;

    invoke-direct {v4, p1}, Lcom/yandex/metrica/impl/ob/dz;-><init>(Landroid/content/Context;)V

    new-instance v5, Lcom/yandex/metrica/impl/utils/p;

    invoke-direct {v5}, Lcom/yandex/metrica/impl/utils/p;-><init>()V

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/yandex/metrica/impl/ob/dy;-><init>(Lcom/yandex/metrica/impl/bn;Lcom/yandex/metrica/impl/ob/gi;Lcom/yandex/metrica/impl/ob/en;Lcom/yandex/metrica/impl/ob/dz;Lcom/yandex/metrica/impl/utils/p;Lcom/yandex/metrica/impl/ob/dr;)V

    .line 45
    return-void
.end method

.method constructor <init>(Lcom/yandex/metrica/impl/bn;Lcom/yandex/metrica/impl/ob/gi;Lcom/yandex/metrica/impl/ob/en;Lcom/yandex/metrica/impl/ob/dz;Lcom/yandex/metrica/impl/utils/p;Lcom/yandex/metrica/impl/ob/dr;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/dy;->a:Lcom/yandex/metrica/impl/bn;

    .line 69
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/dy;->b:Lcom/yandex/metrica/impl/ob/gi;

    .line 70
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/dy;->c:Lcom/yandex/metrica/impl/ob/en;

    .line 71
    iput-object p4, p0, Lcom/yandex/metrica/impl/ob/dy;->d:Lcom/yandex/metrica/impl/ob/dz;

    .line 72
    iput-object p5, p0, Lcom/yandex/metrica/impl/ob/dy;->f:Lcom/yandex/metrica/impl/utils/p;

    .line 73
    iput-object p6, p0, Lcom/yandex/metrica/impl/ob/dy;->e:Lcom/yandex/metrica/impl/ob/dr;

    .line 74
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 48
    new-instance v0, Lcom/yandex/metrica/impl/ob/ea;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/ob/ea;-><init>()V

    .line 49
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/dy;->f:Lcom/yandex/metrica/impl/utils/p;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/utils/p;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/ea;->a(J)V

    .line 50
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/dy;->a:Lcom/yandex/metrica/impl/bn;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bn;->a()Lorg/json/JSONArray;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/ea;->a(Lorg/json/JSONArray;)V

    .line 51
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/dy;->b:Lcom/yandex/metrica/impl/ob/gi;

    new-instance v2, Lcom/yandex/metrica/impl/ob/dy$1;

    invoke-direct {v2, v0}, Lcom/yandex/metrica/impl/ob/dy$1;-><init>(Lcom/yandex/metrica/impl/ob/ea;)V

    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/ob/gi;->a(Lcom/yandex/metrica/impl/ob/gd;)V

    .line 56
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/dy;->d:Lcom/yandex/metrica/impl/ob/dz;

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/ob/dz;->a(Lcom/yandex/metrica/impl/ob/ea;)V

    .line 57
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/dy;->c:Lcom/yandex/metrica/impl/ob/en;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/en;->a()V

    .line 58
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/dy;->e:Lcom/yandex/metrica/impl/ob/dr;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dr;->a()V

    .line 59
    return-void
.end method
