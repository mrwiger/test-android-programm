.class public Lcom/yandex/mobile/ads/ar;
.super Lcom/yandex/mobile/ads/w;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/mobile/ads/ar$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/yandex/mobile/ads/w",
        "<",
        "Lcom/yandex/mobile/ads/nativeads/k;",
        ">;"
    }
.end annotation


# instance fields
.field private final h:Lcom/yandex/mobile/ads/nativeads/NativeAdLoaderConfiguration;

.field private final i:Lcom/yandex/mobile/ads/ao;

.field private final j:Lcom/yandex/mobile/ads/ar$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/yandex/mobile/ads/nativeads/NativeAdLoaderConfiguration;Lcom/yandex/mobile/ads/ar$a;)V
    .locals 2

    .prologue
    .line 53
    sget-object v0, Lcom/yandex/mobile/ads/r;->c:Lcom/yandex/mobile/ads/r;

    invoke-direct {p0, p1, v0}, Lcom/yandex/mobile/ads/w;-><init>(Landroid/content/Context;Lcom/yandex/mobile/ads/r;)V

    .line 54
    invoke-virtual {p2}, Lcom/yandex/mobile/ads/nativeads/NativeAdLoaderConfiguration;->getBlockId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/mobile/ads/ar;->b(Ljava/lang/String;)V

    .line 55
    invoke-virtual {p2}, Lcom/yandex/mobile/ads/nativeads/NativeAdLoaderConfiguration;->getImageSizes()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/mobile/ads/ar;->g:Lcom/yandex/mobile/ads/e;

    invoke-virtual {v1, v0}, Lcom/yandex/mobile/ads/e;->a([Ljava/lang/String;)V

    .line 57
    iput-object p2, p0, Lcom/yandex/mobile/ads/ar;->h:Lcom/yandex/mobile/ads/nativeads/NativeAdLoaderConfiguration;

    .line 58
    iput-object p3, p0, Lcom/yandex/mobile/ads/ar;->j:Lcom/yandex/mobile/ads/ar$a;

    .line 59
    new-instance v0, Lcom/yandex/mobile/ads/ao;

    invoke-direct {v0, p1}, Lcom/yandex/mobile/ads/ao;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/yandex/mobile/ads/ar;->i:Lcom/yandex/mobile/ads/ao;

    .line 60
    return-void
.end method

.method static synthetic a(Lcom/yandex/mobile/ads/ar;Lcom/yandex/mobile/ads/nativeads/o;Lcom/yandex/mobile/ads/nativeads/d;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/yandex/mobile/ads/ar;->b(Lcom/yandex/mobile/ads/nativeads/o;Lcom/yandex/mobile/ads/nativeads/d;)V

    return-void
.end method

.method private b(Lcom/yandex/mobile/ads/AdRequestError;)V
    .locals 1

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/yandex/mobile/ads/ar;->I()V

    .line 156
    iget-object v0, p0, Lcom/yandex/mobile/ads/ar;->j:Lcom/yandex/mobile/ads/ar$a;

    invoke-interface {v0, p1}, Lcom/yandex/mobile/ads/ar$a;->a(Lcom/yandex/mobile/ads/AdRequestError;)V

    .line 157
    return-void
.end method

.method private b(Lcom/yandex/mobile/ads/nativeads/o;Lcom/yandex/mobile/ads/nativeads/d;)V
    .locals 1

    .prologue
    .line 178
    sget-object v0, Lcom/yandex/mobile/ads/k;->c:Lcom/yandex/mobile/ads/k;

    invoke-virtual {p0, v0}, Lcom/yandex/mobile/ads/ar;->a(Lcom/yandex/mobile/ads/k;)V

    .line 179
    invoke-virtual {p0}, Lcom/yandex/mobile/ads/ar;->G()V

    .line 181
    iget-object v0, p0, Lcom/yandex/mobile/ads/ar;->j:Lcom/yandex/mobile/ads/ar$a;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/yandex/mobile/ads/ar;->j:Lcom/yandex/mobile/ads/ar$a;

    invoke-interface {v0, p1, p2}, Lcom/yandex/mobile/ads/ar$a;->a(Lcom/yandex/mobile/ads/nativeads/o;Lcom/yandex/mobile/ads/nativeads/d;)V

    .line 184
    :cond_0
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/mobile/ads/network/request/b;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/yandex/mobile/ads/network/request/b",
            "<",
            "Lcom/yandex/mobile/ads/nativeads/k;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    new-instance v0, Lcom/yandex/mobile/ads/network/request/d;

    iget-object v1, p0, Lcom/yandex/mobile/ads/ar;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/yandex/mobile/ads/ar;->g:Lcom/yandex/mobile/ads/e;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/yandex/mobile/ads/network/request/d;-><init>(Landroid/content/Context;Lcom/yandex/mobile/ads/e;Ljava/lang/String;Ljava/lang/String;Lcom/yandex/mobile/ads/network/request/c$a;)V

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/yandex/mobile/ads/ar;->c:Lcom/yandex/mobile/ads/h;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/h;->a()V

    .line 193
    sget-object v0, Lcom/yandex/mobile/ads/k;->a:Lcom/yandex/mobile/ads/k;

    invoke-virtual {p0, v0}, Lcom/yandex/mobile/ads/ar;->a(Lcom/yandex/mobile/ads/k;)V

    .line 194
    return-void
.end method

.method public declared-synchronized a(Lcom/yandex/mobile/ads/AdRequest;)V
    .locals 1

    .prologue
    .line 161
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/yandex/mobile/ads/ar;->b(Lcom/yandex/mobile/ads/AdRequest;)V

    .line 162
    invoke-virtual {p0}, Lcom/yandex/mobile/ads/ar;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    sget-object v0, Lcom/yandex/mobile/ads/k;->b:Lcom/yandex/mobile/ads/k;

    invoke-virtual {p0, v0}, Lcom/yandex/mobile/ads/ar;->a(Lcom/yandex/mobile/ads/k;)V

    .line 164
    invoke-virtual {p0}, Lcom/yandex/mobile/ads/ar;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    :cond_0
    monitor-exit p0

    return-void

    .line 161
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method a(Lcom/yandex/mobile/ads/AdRequestError;)V
    .locals 2

    .prologue
    .line 143
    invoke-virtual {p1}, Lcom/yandex/mobile/ads/AdRequestError;->getDescription()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/yandex/mobile/ads/utils/logger/b;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 144
    sget-object v0, Lcom/yandex/mobile/ads/k;->d:Lcom/yandex/mobile/ads/k;

    invoke-virtual {p0, v0}, Lcom/yandex/mobile/ads/ar;->a(Lcom/yandex/mobile/ads/k;)V

    .line 146
    invoke-direct {p0, p1}, Lcom/yandex/mobile/ads/ar;->b(Lcom/yandex/mobile/ads/AdRequestError;)V

    .line 147
    return-void
.end method

.method public a(Lcom/yandex/mobile/ads/n;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/yandex/mobile/ads/n",
            "<",
            "Lcom/yandex/mobile/ads/nativeads/k;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    :try_start_0
    invoke-virtual {p0}, Lcom/yandex/mobile/ads/ar;->D()Lcom/yandex/mobile/ads/e;

    move-result-object v1

    .line 71
    invoke-virtual {p1}, Lcom/yandex/mobile/ads/n;->m()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/k;

    .line 72
    new-instance v2, Lcom/yandex/mobile/ads/nativeads/o;

    invoke-direct {v2, v0, p1, v1}, Lcom/yandex/mobile/ads/nativeads/o;-><init>(Lcom/yandex/mobile/ads/nativeads/k;Lcom/yandex/mobile/ads/n;Lcom/yandex/mobile/ads/e;)V

    .line 74
    iget-object v0, p0, Lcom/yandex/mobile/ads/ar;->h:Lcom/yandex/mobile/ads/nativeads/NativeAdLoaderConfiguration;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/NativeAdLoaderConfiguration;->shouldLoadImagesAutomatically()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    new-instance v0, Lcom/yandex/mobile/ads/nativeads/aa;

    invoke-direct {v0}, Lcom/yandex/mobile/ads/nativeads/aa;-><init>()V

    .line 76
    invoke-virtual {p0, v2, v0}, Lcom/yandex/mobile/ads/ar;->a(Lcom/yandex/mobile/ads/nativeads/o;Lcom/yandex/mobile/ads/nativeads/d;)V

    .line 87
    :goto_0
    return-void

    .line 78
    :cond_0
    new-instance v0, Lcom/yandex/mobile/ads/nativeads/c;

    invoke-virtual {p0}, Lcom/yandex/mobile/ads/ar;->o()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/yandex/mobile/ads/nativeads/c;-><init>(Landroid/content/Context;Lcom/yandex/mobile/ads/nativeads/o;)V

    .line 79
    invoke-direct {p0, v2, v0}, Lcom/yandex/mobile/ads/ar;->b(Lcom/yandex/mobile/ads/nativeads/o;Lcom/yandex/mobile/ads/nativeads/d;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 82
    :catch_0
    move-exception v0

    sget-object v0, Lcom/yandex/mobile/ads/AdRequestError;->d:Lcom/yandex/mobile/ads/AdRequestError;

    sget-object v1, Lcom/yandex/mobile/ads/k;->d:Lcom/yandex/mobile/ads/k;

    invoke-virtual {p0, v1}, Lcom/yandex/mobile/ads/ar;->a(Lcom/yandex/mobile/ads/k;)V

    iget-object v1, p0, Lcom/yandex/mobile/ads/ar;->j:Lcom/yandex/mobile/ads/ar$a;

    invoke-interface {v1, v0}, Lcom/yandex/mobile/ads/ar$a;->a(Lcom/yandex/mobile/ads/AdRequestError;)V

    .line 85
    sget-object v0, Lcom/yandex/mobile/ads/AdRequestError;->d:Lcom/yandex/mobile/ads/AdRequestError;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/AdRequestError;->getDescription()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/yandex/mobile/ads/utils/logger/b;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method a(Lcom/yandex/mobile/ads/nativeads/o;Lcom/yandex/mobile/ads/nativeads/d;)V
    .locals 3

    .prologue
    .line 96
    invoke-virtual {p1}, Lcom/yandex/mobile/ads/nativeads/o;->d()Lcom/yandex/mobile/ads/nativeads/h;

    move-result-object v0

    .line 97
    iget-object v1, p0, Lcom/yandex/mobile/ads/ar;->i:Lcom/yandex/mobile/ads/ao;

    new-instance v2, Lcom/yandex/mobile/ads/ar$1;

    invoke-direct {v2, p0, v0, p2, p1}, Lcom/yandex/mobile/ads/ar$1;-><init>(Lcom/yandex/mobile/ads/ar;Lcom/yandex/mobile/ads/nativeads/h;Lcom/yandex/mobile/ads/nativeads/d;Lcom/yandex/mobile/ads/nativeads/o;)V

    invoke-virtual {v1, v0, v2}, Lcom/yandex/mobile/ads/ao;->a(Lcom/yandex/mobile/ads/nativeads/h;Lcom/yandex/mobile/ads/nativeads/f;)V

    .line 138
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 38
    check-cast p1, Lcom/yandex/mobile/ads/n;

    invoke-virtual {p0, p1}, Lcom/yandex/mobile/ads/ar;->a(Lcom/yandex/mobile/ads/n;)V

    return-void
.end method

.method c(Lcom/yandex/mobile/ads/AdRequest;)Z
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x1

    return v0
.end method

.method public onAdFailedToLoad(Lcom/yandex/mobile/ads/AdRequestError;)V
    .locals 0
    .param p1, "error"    # Lcom/yandex/mobile/ads/AdRequestError;

    .prologue
    .line 151
    invoke-direct {p0, p1}, Lcom/yandex/mobile/ads/ar;->b(Lcom/yandex/mobile/ads/AdRequestError;)V

    .line 152
    return-void
.end method

.method p()Z
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/yandex/mobile/ads/ar;->q()Z

    move-result v0

    return v0
.end method
