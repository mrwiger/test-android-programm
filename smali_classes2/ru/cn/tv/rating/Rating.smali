.class public Lru/cn/tv/rating/Rating;
.super Ljava/lang/Object;
.source "Rating.java"


# instance fields
.field private context:Landroid/content/Context;

.field private deltaDayMillsec:J

.field private sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-string v1, "rating"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lru/cn/tv/rating/Rating;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 31
    invoke-static {}, Lru/cn/utils/Utils;->getCalendar()Ljava/util/Calendar;

    move-result-object v0

    .line 32
    .local v0, "yesterday":Ljava/util/Calendar;
    const/4 v1, 0x7

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 33
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 34
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lru/cn/tv/rating/Rating;->deltaDayMillsec:J

    .line 35
    iput-object p1, p0, Lru/cn/tv/rating/Rating;->context:Landroid/content/Context;

    .line 36
    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/rating/Rating;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/rating/Rating;

    .prologue
    .line 15
    iget-object v0, p0, Lru/cn/tv/rating/Rating;->sharedPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/tv/rating/Rating;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/rating/Rating;

    .prologue
    .line 15
    iget-object v0, p0, Lru/cn/tv/rating/Rating;->context:Landroid/content/Context;

    return-object v0
.end method

.method private isMarketInstalled()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 64
    iget-object v3, p0, Lru/cn/tv/rating/Rating;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 66
    .local v1, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v3, "com.android.vending"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    :goto_0
    return v2

    .line 68
    :catch_0
    move-exception v0

    .line 69
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static saveStartCount(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 54
    const-string v2, "rating"

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 56
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v2, "started_counter"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 58
    .local v0, "count":I
    const/16 v2, 0x8

    if-ge v0, v2, :cond_0

    .line 59
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "started_counter"

    add-int/lit8 v4, v0, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 61
    :cond_0
    return-void
.end method

.method private showRecommendsPopup()V
    .locals 4

    .prologue
    .line 75
    new-instance v0, Landroid/widget/CheckBox;

    iget-object v2, p0, Lru/cn/tv/rating/Rating;->context:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    .line 76
    .local v0, "dontShowAgain":Landroid/widget/CheckBox;
    const v2, 0x7f0e008c

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setText(I)V

    .line 78
    new-instance v1, Landroid/support/v7/app/AlertDialog$Builder;

    iget-object v2, p0, Lru/cn/tv/rating/Rating;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 79
    .local v1, "rateMe":Landroid/support/v7/app/AlertDialog$Builder;
    const v2, 0x7f0e0121

    invoke-virtual {v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 80
    const v2, 0x7f0e011e

    invoke-virtual {v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 81
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    .line 82
    invoke-virtual {v1, v0}, Landroid/support/v7/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 83
    const v2, 0x7f0e0120

    new-instance v3, Lru/cn/tv/rating/Rating$1;

    invoke-direct {v3, p0}, Lru/cn/tv/rating/Rating$1;-><init>(Lru/cn/tv/rating/Rating;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 96
    const v2, 0x7f0e011f

    new-instance v3, Lru/cn/tv/rating/Rating$2;

    invoke-direct {v3, p0, v0}, Lru/cn/tv/rating/Rating$2;-><init>(Lru/cn/tv/rating/Rating;Landroid/widget/CheckBox;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 106
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 107
    return-void
.end method


# virtual methods
.method public runRatingRequestIfNeeded()V
    .locals 10

    .prologue
    .line 39
    iget-object v6, p0, Lru/cn/tv/rating/Rating;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v7, "needRate"

    const/4 v8, 0x1

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 40
    .local v3, "requestNeeded":Z
    if-eqz v3, :cond_0

    invoke-direct {p0}, Lru/cn/tv/rating/Rating;->isMarketInstalled()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 41
    iget-object v6, p0, Lru/cn/tv/rating/Rating;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v7, "started_counter"

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 42
    .local v2, "count":I
    iget-object v6, p0, Lru/cn/tv/rating/Rating;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v7, "lastAsked"

    const-wide/16 v8, 0x0

    invoke-interface {v6, v7, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 43
    .local v0, "asked":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 44
    .local v4, "nowMillsecs":J
    const/16 v6, 0x8

    if-lt v2, v6, :cond_0

    sub-long v6, v4, v0

    iget-wide v8, p0, Lru/cn/tv/rating/Rating;->deltaDayMillsec:J

    cmp-long v6, v6, v8

    if-ltz v6, :cond_0

    .line 46
    invoke-direct {p0}, Lru/cn/tv/rating/Rating;->showRecommendsPopup()V

    .line 47
    iget-object v6, p0, Lru/cn/tv/rating/Rating;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "lastAsked"

    invoke-interface {v6, v7, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    .line 48
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 51
    .end local v0    # "asked":J
    .end local v2    # "count":I
    .end local v4    # "nowMillsecs":J
    :cond_0
    return-void
.end method
