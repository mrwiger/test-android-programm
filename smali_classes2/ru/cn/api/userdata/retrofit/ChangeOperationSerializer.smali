.class public Lru/cn/api/userdata/retrofit/ChangeOperationSerializer;
.super Ljava/lang/Object;
.source "ChangeOperationSerializer.java"

# interfaces
.implements Lcom/google/gson/JsonSerializer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gson/JsonSerializer",
        "<",
        "Lru/cn/api/userdata/replies/ChangeOperation;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/gson/JsonSerializationContext;)Lcom/google/gson/JsonElement;
    .locals 1

    .prologue
    .line 10
    check-cast p1, Lru/cn/api/userdata/replies/ChangeOperation;

    invoke-virtual {p0, p1, p2, p3}, Lru/cn/api/userdata/retrofit/ChangeOperationSerializer;->serialize(Lru/cn/api/userdata/replies/ChangeOperation;Ljava/lang/reflect/Type;Lcom/google/gson/JsonSerializationContext;)Lcom/google/gson/JsonElement;

    move-result-object v0

    return-object v0
.end method

.method public serialize(Lru/cn/api/userdata/replies/ChangeOperation;Ljava/lang/reflect/Type;Lcom/google/gson/JsonSerializationContext;)Lcom/google/gson/JsonElement;
    .locals 2
    .param p1, "src"    # Lru/cn/api/userdata/replies/ChangeOperation;
    .param p2, "type"    # Ljava/lang/reflect/Type;
    .param p3, "arg2"    # Lcom/google/gson/JsonSerializationContext;

    .prologue
    .line 15
    new-instance v0, Lcom/google/gson/JsonPrimitive;

    invoke-virtual {p1}, Lru/cn/api/userdata/replies/ChangeOperation;->getValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/gson/JsonPrimitive;-><init>(Ljava/lang/Number;)V

    return-object v0
.end method
