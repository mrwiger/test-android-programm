.class Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "TelecastInfoViewModel.java"


# instance fields
.field private final contractor:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private contractorDisposable:Lio/reactivex/disposables/Disposable;

.field private currentContractor:J

.field private final loader:Lru/cn/mvvm/RxLoader;

.field private final relatedRubric:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final telecast:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private telecastDisposable:Lio/reactivex/disposables/Disposable;


# direct methods
.method constructor <init>(Lru/cn/mvvm/RxLoader;)V
    .locals 1
    .param p1, "loader"    # Lru/cn/mvvm/RxLoader;

    .prologue
    .line 33
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 34
    iput-object p1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->loader:Lru/cn/mvvm/RxLoader;

    .line 36
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->telecast:Landroid/arch/lifecycle/MutableLiveData;

    .line 37
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->contractor:Landroid/arch/lifecycle/MutableLiveData;

    .line 38
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->relatedRubric:Landroid/arch/lifecycle/MutableLiveData;

    .line 39
    return-void
.end method


# virtual methods
.method public contractor()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->contractor:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method final synthetic lambda$setContractor$0$TelecastInfoViewModel(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "it"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->contractor:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v0, p1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method final synthetic lambda$setTelecast$1$TelecastInfoViewModel(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "it"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->telecast:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v0, p1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method final synthetic lambda$setTelecast$2$TelecastInfoViewModel(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "it"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->relatedRubric:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v0, p1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method public relatedRubric()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->relatedRubric:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method public setContractor(J)V
    .locals 5
    .param p1, "contractorId"    # J

    .prologue
    .line 54
    iget-wide v2, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->currentContractor:J

    cmp-long v1, p1, v2

    if-nez v1, :cond_1

    .line 75
    :cond_0
    :goto_0
    return-void

    .line 57
    :cond_1
    iput-wide p1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->currentContractor:J

    .line 59
    iget-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->contractorDisposable:Lio/reactivex/disposables/Disposable;

    if-eqz v1, :cond_2

    .line 60
    iget-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->contractorDisposable:Lio/reactivex/disposables/Disposable;

    invoke-virtual {p0, v1}, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->unbind(Lio/reactivex/disposables/Disposable;)V

    .line 61
    const/4 v1, 0x0

    iput-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->contractorDisposable:Lio/reactivex/disposables/Disposable;

    .line 64
    :cond_2
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-lez v1, :cond_0

    .line 67
    invoke-static {p1, p2}, Lru/cn/api/provider/TvContentProviderContract;->contractor(J)Landroid/net/Uri;

    move-result-object v0

    .line 69
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v1

    .line 70
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 71
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel$$Lambda$0;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel$$Lambda$0;-><init>(Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;)V

    .line 72
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    iput-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->contractorDisposable:Lio/reactivex/disposables/Disposable;

    .line 74
    iget-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->contractorDisposable:Lio/reactivex/disposables/Disposable;

    invoke-virtual {p0, v1}, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    goto :goto_0
.end method

.method public setTelecast(J)V
    .locals 7
    .param p1, "telecastId"    # J

    .prologue
    .line 78
    iget-object v4, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->telecastDisposable:Lio/reactivex/disposables/Disposable;

    if-eqz v4, :cond_0

    .line 79
    iget-object v4, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->telecastDisposable:Lio/reactivex/disposables/Disposable;

    invoke-virtual {p0, v4}, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->unbind(Lio/reactivex/disposables/Disposable;)V

    .line 80
    const/4 v4, 0x0

    iput-object v4, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->telecastDisposable:Lio/reactivex/disposables/Disposable;

    .line 83
    :cond_0
    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-gtz v4, :cond_1

    .line 108
    :goto_0
    return-void

    .line 86
    :cond_1
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    .line 88
    .local v0, "compositeDisposable":Lio/reactivex/disposables/CompositeDisposable;
    invoke-static {p1, p2}, Lru/cn/api/provider/TvContentProviderContract;->telecastUri(J)Landroid/net/Uri;

    move-result-object v3

    .line 90
    .local v3, "uri":Landroid/net/Uri;
    iget-object v4, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v4, v3}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v4

    .line 91
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v5

    invoke-virtual {v4, v5}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v4

    .line 92
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v5

    invoke-virtual {v4, v5}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v4

    new-instance v5, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel$$Lambda$1;

    invoke-direct {v5, p0}, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel$$Lambda$1;-><init>(Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;)V

    .line 93
    invoke-virtual {v4, v5}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 95
    .local v1, "disposable":Lio/reactivex/disposables/Disposable;
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 97
    invoke-static {p1, p2}, Lru/cn/api/provider/TvContentProviderContract;->rubricRelated(J)Landroid/net/Uri;

    move-result-object v2

    .line 99
    .local v2, "relatedRubricUri":Landroid/net/Uri;
    iget-object v4, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v4, v2}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v4

    .line 100
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v5

    invoke-virtual {v4, v5}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v4

    .line 101
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v5

    invoke-virtual {v4, v5}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v4

    new-instance v5, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel$$Lambda$2;

    invoke-direct {v5, p0}, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel$$Lambda$2;-><init>(Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;)V

    .line 102
    invoke-virtual {v4, v5}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 104
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 106
    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 107
    iput-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->telecastDisposable:Lio/reactivex/disposables/Disposable;

    goto :goto_0
.end method

.method public telecast()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->telecast:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method
