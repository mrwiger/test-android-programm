.class Lcom/samsung/multiscreen/Player$1;
.super Ljava/lang/Object;
.source "Player.java"

# interfaces
.implements Lcom/samsung/multiscreen/util/HttpUtil$ResultCreator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/multiscreen/Player;->getDMPStatus(Lcom/samsung/multiscreen/Result;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/samsung/multiscreen/util/HttpUtil$ResultCreator",
        "<",
        "Lcom/samsung/multiscreen/Player$DMPStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/multiscreen/Player;


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/Player;)V
    .locals 0
    .param p1, "this$0"    # Lcom/samsung/multiscreen/Player;

    .prologue
    .line 323
    iput-object p1, p0, Lcom/samsung/multiscreen/Player$1;->this$0:Lcom/samsung/multiscreen/Player;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createResult(Ljava/util/Map;)Lcom/samsung/multiscreen/Player$DMPStatus;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/samsung/multiscreen/Player$DMPStatus;"
        }
    .end annotation

    .prologue
    .line 326
    .local p1, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v1, Lcom/samsung/multiscreen/Player$DMPStatus;

    iget-object v2, p0, Lcom/samsung/multiscreen/Player$1;->this$0:Lcom/samsung/multiscreen/Player;

    invoke-direct {v1, v2}, Lcom/samsung/multiscreen/Player$DMPStatus;-><init>(Lcom/samsung/multiscreen/Player;)V

    .line 327
    .local v1, "status":Lcom/samsung/multiscreen/Player$DMPStatus;
    if-eqz p1, :cond_4

    .line 328
    const-string v2, "id"

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 329
    .local v0, "id":Ljava/lang/String;
    const-string v2, "appName"

    invoke-interface {p1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 330
    const-string v2, "appName"

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, v1, Lcom/samsung/multiscreen/Player$DMPStatus;->mAppName:Ljava/lang/String;

    .line 332
    :cond_0
    const-string v2, "visible"

    invoke-interface {p1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 333
    const-string v2, "visible"

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    iput-object v2, v1, Lcom/samsung/multiscreen/Player$DMPStatus;->mVisible:Ljava/lang/Boolean;

    .line 335
    :cond_1
    const-string v2, "media_player"

    invoke-interface {p1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 336
    const-string v2, "media_player"

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    iput-object v2, v1, Lcom/samsung/multiscreen/Player$DMPStatus;->mDMPRunning:Ljava/lang/Boolean;

    .line 338
    :cond_2
    const-string v2, "running"

    invoke-interface {p1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 339
    const-string v2, "running"

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    iput-object v2, v1, Lcom/samsung/multiscreen/Player$DMPStatus;->mRunning:Ljava/lang/Boolean;

    .line 341
    :cond_3
    if-eqz v0, :cond_4

    const-string v2, "3201412000694"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 345
    .end local v0    # "id":Ljava/lang/String;
    .end local v1    # "status":Lcom/samsung/multiscreen/Player$DMPStatus;
    :goto_0
    return-object v1

    .restart local v1    # "status":Lcom/samsung/multiscreen/Player$DMPStatus;
    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic createResult(Ljava/util/Map;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 323
    invoke-virtual {p0, p1}, Lcom/samsung/multiscreen/Player$1;->createResult(Ljava/util/Map;)Lcom/samsung/multiscreen/Player$DMPStatus;

    move-result-object v0

    return-object v0
.end method
