.class Lru/cn/tv/mobile/NewActivity$1;
.super Ljava/lang/Object;
.source "NewActivity.java"

# interfaces
.implements Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/mobile/NewActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/mobile/NewActivity;


# direct methods
.method constructor <init>(Lru/cn/tv/mobile/NewActivity;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/mobile/NewActivity;

    .prologue
    .line 120
    iput-object p1, p0, Lru/cn/tv/mobile/NewActivity$1;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackStackChanged()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 123
    iget-object v4, p0, Lru/cn/tv/mobile/NewActivity$1;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-virtual {v4}, Lru/cn/tv/mobile/NewActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 124
    .local v2, "fm":Landroid/support/v4/app/FragmentManager;
    iget-object v4, p0, Lru/cn/tv/mobile/NewActivity$1;->this$0:Lru/cn/tv/mobile/NewActivity;

    const-string v5, "current_fragment"

    invoke-virtual {v2, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v5

    invoke-static {v4, v5}, Lru/cn/tv/mobile/NewActivity;->access$002(Lru/cn/tv/mobile/NewActivity;Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/Fragment;

    .line 126
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 127
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v2, v4}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryAt(I)Landroid/support/v4/app/FragmentManager$BackStackEntry;

    move-result-object v0

    .line 128
    .local v0, "bse":Landroid/support/v4/app/FragmentManager$BackStackEntry;
    invoke-interface {v0}, Landroid/support/v4/app/FragmentManager$BackStackEntry;->getBreadCrumbTitle()Ljava/lang/CharSequence;

    move-result-object v3

    .line 129
    .local v3, "title":Ljava/lang/CharSequence;
    iget-object v4, p0, Lru/cn/tv/mobile/NewActivity$1;->this$0:Lru/cn/tv/mobile/NewActivity;

    check-cast v3, Ljava/lang/String;

    .end local v3    # "title":Ljava/lang/CharSequence;
    invoke-virtual {v4, v3}, Lru/cn/tv/mobile/NewActivity;->setDefaultTitle(Ljava/lang/String;)V

    .line 130
    iget-object v4, p0, Lru/cn/tv/mobile/NewActivity$1;->this$0:Lru/cn/tv/mobile/NewActivity;

    iget-object v4, v4, Lru/cn/tv/mobile/NewActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v4, v6}, Landroid/support/v7/app/ActionBarDrawerToggle;->setDrawerIndicatorEnabled(Z)V

    .line 143
    .end local v0    # "bse":Landroid/support/v4/app/FragmentManager$BackStackEntry;
    :goto_0
    return-void

    .line 132
    :cond_0
    iget-object v4, p0, Lru/cn/tv/mobile/NewActivity$1;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-static {v4}, Lru/cn/tv/mobile/NewActivity;->access$000(Lru/cn/tv/mobile/NewActivity;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    instance-of v4, v4, Lru/cn/tv/mobile/ChannelsScheduleFragment;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lru/cn/tv/mobile/NewActivity$1;->this$0:Lru/cn/tv/mobile/NewActivity;

    .line 133
    invoke-static {v4}, Lru/cn/tv/mobile/NewActivity;->access$000(Lru/cn/tv/mobile/NewActivity;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Lru/cn/tv/mobile/ChannelsScheduleFragment;

    invoke-virtual {v4}, Lru/cn/tv/mobile/ChannelsScheduleFragment;->canGoBack()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 134
    iget-object v4, p0, Lru/cn/tv/mobile/NewActivity$1;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-static {v4}, Lru/cn/tv/mobile/NewActivity;->access$000(Lru/cn/tv/mobile/NewActivity;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Lru/cn/tv/mobile/ChannelsScheduleFragment;

    invoke-virtual {v4}, Lru/cn/tv/mobile/ChannelsScheduleFragment;->getChannelTitle()Ljava/lang/String;

    move-result-object v1

    .line 135
    .local v1, "channelTitle":Ljava/lang/String;
    iget-object v4, p0, Lru/cn/tv/mobile/NewActivity$1;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-virtual {v4, v1}, Lru/cn/tv/mobile/NewActivity;->setDefaultTitle(Ljava/lang/String;)V

    .line 136
    iget-object v4, p0, Lru/cn/tv/mobile/NewActivity$1;->this$0:Lru/cn/tv/mobile/NewActivity;

    iget-object v4, v4, Lru/cn/tv/mobile/NewActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v4, v6}, Landroid/support/v7/app/ActionBarDrawerToggle;->setDrawerIndicatorEnabled(Z)V

    goto :goto_0

    .line 139
    .end local v1    # "channelTitle":Ljava/lang/String;
    :cond_1
    iget-object v4, p0, Lru/cn/tv/mobile/NewActivity$1;->this$0:Lru/cn/tv/mobile/NewActivity;

    iget-object v5, p0, Lru/cn/tv/mobile/NewActivity$1;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-static {v5}, Lru/cn/tv/mobile/NewActivity;->access$100(Lru/cn/tv/mobile/NewActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lru/cn/tv/mobile/NewActivity;->setDefaultTitle(Ljava/lang/String;)V

    .line 140
    iget-object v4, p0, Lru/cn/tv/mobile/NewActivity$1;->this$0:Lru/cn/tv/mobile/NewActivity;

    iget-object v4, v4, Lru/cn/tv/mobile/NewActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/support/v7/app/ActionBarDrawerToggle;->setDrawerIndicatorEnabled(Z)V

    goto :goto_0
.end method
