.class public final Lcom/yandex/metrica/c$c$e;
.super Lcom/yandex/metrica/impl/ob/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/c$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "e"
.end annotation


# static fields
.field private static volatile g:[Lcom/yandex/metrica/c$c$e;


# instance fields
.field public b:I

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2186
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/d;-><init>()V

    .line 2187
    invoke-virtual {p0}, Lcom/yandex/metrica/c$c$e;->e()Lcom/yandex/metrica/c$c$e;

    .line 2188
    return-void
.end method

.method public static d()[Lcom/yandex/metrica/c$c$e;
    .locals 2

    .prologue
    .line 2158
    sget-object v0, Lcom/yandex/metrica/c$c$e;->g:[Lcom/yandex/metrica/c$c$e;

    if-nez v0, :cond_1

    .line 2159
    sget-object v1, Lcom/yandex/metrica/impl/ob/c;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2161
    :try_start_0
    sget-object v0, Lcom/yandex/metrica/c$c$e;->g:[Lcom/yandex/metrica/c$c$e;

    if-nez v0, :cond_0

    .line 2162
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/yandex/metrica/c$c$e;

    sput-object v0, Lcom/yandex/metrica/c$c$e;->g:[Lcom/yandex/metrica/c$c$e;

    .line 2164
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2166
    :cond_1
    sget-object v0, Lcom/yandex/metrica/c$c$e;->g:[Lcom/yandex/metrica/c$c$e;

    return-object v0

    .line 2164
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(Lcom/yandex/metrica/impl/ob/b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2203
    iget v0, p0, Lcom/yandex/metrica/c$c$e;->b:I

    if-eqz v0, :cond_0

    .line 2204
    const/4 v0, 0x1

    iget v1, p0, Lcom/yandex/metrica/c$c$e;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->b(II)V

    .line 2206
    :cond_0
    iget v0, p0, Lcom/yandex/metrica/c$c$e;->c:I

    if-eqz v0, :cond_1

    .line 2207
    const/4 v0, 0x2

    iget v1, p0, Lcom/yandex/metrica/c$c$e;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->b(II)V

    .line 2209
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/c$c$e;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2210
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/yandex/metrica/c$c$e;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(ILjava/lang/String;)V

    .line 2212
    :cond_2
    iget-boolean v0, p0, Lcom/yandex/metrica/c$c$e;->e:Z

    if-eqz v0, :cond_3

    .line 2213
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/yandex/metrica/c$c$e;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(IZ)V

    .line 2215
    :cond_3
    iget-object v0, p0, Lcom/yandex/metrica/c$c$e;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2216
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/yandex/metrica/c$c$e;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(ILjava/lang/String;)V

    .line 2218
    :cond_4
    invoke-super {p0, p1}, Lcom/yandex/metrica/impl/ob/d;->a(Lcom/yandex/metrica/impl/ob/b;)V

    .line 2219
    return-void
.end method

.method protected c()I
    .locals 3

    .prologue
    .line 2223
    invoke-super {p0}, Lcom/yandex/metrica/impl/ob/d;->c()I

    move-result v0

    .line 2224
    iget v1, p0, Lcom/yandex/metrica/c$c$e;->b:I

    if-eqz v1, :cond_0

    .line 2225
    const/4 v1, 0x1

    iget v2, p0, Lcom/yandex/metrica/c$c$e;->b:I

    .line 2226
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2228
    :cond_0
    iget v1, p0, Lcom/yandex/metrica/c$c$e;->c:I

    if-eqz v1, :cond_1

    .line 2229
    const/4 v1, 0x2

    iget v2, p0, Lcom/yandex/metrica/c$c$e;->c:I

    .line 2230
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2232
    :cond_1
    iget-object v1, p0, Lcom/yandex/metrica/c$c$e;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2233
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/yandex/metrica/c$c$e;->d:Ljava/lang/String;

    .line 2234
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2236
    :cond_2
    iget-boolean v1, p0, Lcom/yandex/metrica/c$c$e;->e:Z

    if-eqz v1, :cond_3

    .line 2237
    const/4 v1, 0x4

    .line 2238
    invoke-static {v1}, Lcom/yandex/metrica/impl/ob/b;->e(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 2240
    :cond_3
    iget-object v1, p0, Lcom/yandex/metrica/c$c$e;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2241
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/yandex/metrica/c$c$e;->f:Ljava/lang/String;

    .line 2242
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2244
    :cond_4
    return v0
.end method

.method public e()Lcom/yandex/metrica/c$c$e;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2191
    iput v1, p0, Lcom/yandex/metrica/c$c$e;->b:I

    .line 2192
    iput v1, p0, Lcom/yandex/metrica/c$c$e;->c:I

    .line 2193
    const-string v0, ""

    iput-object v0, p0, Lcom/yandex/metrica/c$c$e;->d:Ljava/lang/String;

    .line 2194
    iput-boolean v1, p0, Lcom/yandex/metrica/c$c$e;->e:Z

    .line 2195
    const-string v0, ""

    iput-object v0, p0, Lcom/yandex/metrica/c$c$e;->f:Ljava/lang/String;

    .line 2196
    const/4 v0, -0x1

    iput v0, p0, Lcom/yandex/metrica/c$c$e;->a:I

    .line 2197
    return-object p0
.end method
