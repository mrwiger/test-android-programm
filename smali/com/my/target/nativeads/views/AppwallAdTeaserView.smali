.class public Lcom/my/target/nativeads/views/AppwallAdTeaserView;
.super Landroid/widget/RelativeLayout;
.source "AppwallAdTeaserView.java"


# instance fields
.field private banner:Lcom/my/target/nativeads/banners/NativeAppwallBanner;

.field private final bannerIcon:Lcom/my/target/bv;

.field private final coinsBgShape:Landroid/graphics/drawable/ShapeDrawable;

.field private final coinsCountView:Landroid/widget/TextView;

.field private final coinsIconView:Lcom/my/target/bv;

.field private final coinsLayout:Landroid/widget/LinearLayout;

.field private final descrView:Landroid/widget/TextView;

.field private final notificationView:Lcom/my/target/bv;

.field private final openImageView:Lcom/my/target/bv;

.field private final starsRatingView:Lcom/my/target/ca;

.field private final statusIconView:Lcom/my/target/bv;

.field private final textColor:I

.field private final titleView:Landroid/widget/TextView;

.field private final uiUtils:Lcom/my/target/cm;

.field private viewed:Z

.field private final votesCountView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x6

    const/4 v5, 0x0

    const/16 v0, 0x24

    .line 55
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 37
    invoke-static {v0, v0, v0}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->textColor:I

    .line 51
    iput-boolean v5, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->viewed:Z

    .line 56
    new-instance v0, Lcom/my/target/bv;

    invoke-virtual {p0}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/my/target/bv;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->bannerIcon:Lcom/my/target/bv;

    .line 57
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsLayout:Landroid/widget/LinearLayout;

    .line 58
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsCountView:Landroid/widget/TextView;

    .line 59
    new-instance v0, Lcom/my/target/bv;

    invoke-virtual {p0}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/my/target/bv;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsIconView:Lcom/my/target/bv;

    .line 60
    new-instance v0, Lcom/my/target/bv;

    invoke-virtual {p0}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/my/target/bv;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->openImageView:Lcom/my/target/bv;

    .line 61
    new-instance v0, Lcom/my/target/bv;

    invoke-virtual {p0}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/my/target/bv;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->statusIconView:Lcom/my/target/bv;

    .line 62
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->titleView:Landroid/widget/TextView;

    .line 63
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->descrView:Landroid/widget/TextView;

    .line 64
    new-instance v0, Lcom/my/target/ca;

    invoke-virtual {p0}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/my/target/ca;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->starsRatingView:Lcom/my/target/ca;

    .line 65
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->votesCountView:Landroid/widget/TextView;

    .line 66
    new-instance v0, Lcom/my/target/bv;

    invoke-virtual {p0}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/my/target/bv;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->notificationView:Lcom/my/target/bv;

    .line 67
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    .line 68
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v0, v6}, Lcom/my/target/cm;->n(I)I

    move-result v0

    .line 69
    new-instance v1, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v2, Landroid/graphics/drawable/shapes/RoundRectShape;

    const/16 v3, 0x8

    new-array v3, v3, [F

    int-to-float v4, v0

    aput v4, v3, v5

    const/4 v4, 0x1

    int-to-float v5, v0

    aput v5, v3, v4

    const/4 v4, 0x2

    int-to-float v5, v0

    aput v5, v3, v4

    const/4 v4, 0x3

    int-to-float v5, v0

    aput v5, v3, v4

    const/4 v4, 0x4

    int-to-float v5, v0

    aput v5, v3, v4

    const/4 v4, 0x5

    int-to-float v5, v0

    aput v5, v3, v4

    int-to-float v4, v0

    aput v4, v3, v6

    const/4 v4, 0x7

    int-to-float v0, v0

    aput v0, v3, v4

    invoke-direct {v2, v3, v7, v7}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    iput-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsBgShape:Landroid/graphics/drawable/ShapeDrawable;

    .line 70
    invoke-direct {p0}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->H()V

    .line 71
    return-void
.end method

.method private H()V
    .locals 14

    .prologue
    const/16 v13, 0x14

    const/4 v12, -0x1

    const/4 v11, 0x2

    const/4 v10, -0x2

    const/4 v9, 0x0

    .line 228
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Lcom/my/target/cm;->n(I)I

    move-result v0

    .line 229
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    .line 230
    iget-object v2, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    const/16 v3, 0x35

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    .line 232
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v3

    .line 233
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v4

    .line 234
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v5

    .line 236
    invoke-virtual {p0, v12}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->setBackgroundColor(I)V

    .line 238
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    add-int v7, v2, v1

    add-int/2addr v7, v1

    add-int v8, v2, v0

    add-int/2addr v8, v0

    invoke-direct {v6, v7, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 240
    iget-object v7, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->bannerIcon:Lcom/my/target/bv;

    invoke-virtual {v7, v1, v0, v1, v0}, Lcom/my/target/bv;->setPadding(IIII)V

    .line 241
    iget-object v7, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->bannerIcon:Lcom/my/target/bv;

    invoke-virtual {p0, v7, v6}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 243
    iget-object v6, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v6, v13}, Lcom/my/target/cm;->n(I)I

    move-result v6

    .line 245
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v7, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 246
    iget-object v6, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    const/16 v8, 0x39

    invoke-virtual {v6, v8}, Lcom/my/target/cm;->n(I)I

    move-result v6

    iput v6, v7, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 247
    iget-object v6, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    const/16 v8, 0xa

    invoke-virtual {v6, v8}, Lcom/my/target/cm;->n(I)I

    move-result v6

    iput v6, v7, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 248
    iget-object v6, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->notificationView:Lcom/my/target/bv;

    invoke-virtual {v6, v7}, Lcom/my/target/bv;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 249
    iget-object v6, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->notificationView:Lcom/my/target/bv;

    invoke-virtual {p0, v6}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->addView(Landroid/view/View;)V

    .line 251
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v6, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 252
    const/16 v7, 0xb

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 253
    iput v1, v6, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 254
    iput v0, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 256
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsBgShape:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 257
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 258
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0, v6}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 260
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v12, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 261
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsCountView:Landroid/widget/TextView;

    sget-object v6, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 262
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsCountView:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    const/16 v7, 0xa

    invoke-virtual {v6, v7}, Lcom/my/target/cm;->n(I)I

    move-result v6

    iget-object v7, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v7, v11}, Lcom/my/target/cm;->n(I)I

    move-result v7

    invoke-virtual {v1, v9, v6, v9, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 263
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsCountView:Landroid/widget/TextView;

    const/high16 v6, 0x41500000    # 13.0f

    invoke-virtual {v1, v11, v6}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 264
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsCountView:Landroid/widget/TextView;

    const/16 v6, 0x31

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 265
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsLayout:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsCountView:Landroid/widget/TextView;

    invoke-virtual {v1, v6, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 267
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v1, v13}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v6, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v6, v13}, Lcom/my/target/cm;->n(I)I

    move-result v6

    invoke-direct {v0, v1, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 268
    const/4 v1, 0x1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 269
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsLayout:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsIconView:Lcom/my/target/bv;

    invoke-virtual {v1, v6, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 271
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    const/16 v6, 0x13

    invoke-virtual {v1, v6}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-direct {v0, v1, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 272
    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 273
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 274
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    const/16 v6, 0x1e

    invoke-virtual {v1, v6}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 276
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->openImageView:Lcom/my/target/bv;

    invoke-virtual {p0, v1, v0}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 278
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 279
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 280
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 281
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->statusIconView:Lcom/my/target/bv;

    invoke-virtual {p0, v1, v0}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 283
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->titleView:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 284
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->titleView:Landroid/widget/TextView;

    const/high16 v1, 0x41900000    # 18.0f

    invoke-virtual {v0, v11, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 285
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->titleView:Landroid/widget/TextView;

    iget v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->textColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 286
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->titleView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0x43

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v9, v9, v1, v9}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 287
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setId(I)V

    .line 289
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v12, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 290
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0x5b

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 291
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0xf

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 292
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 294
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 295
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->titleView:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->addView(Landroid/view/View;)V

    .line 297
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->descrView:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 298
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->descrView:Landroid/widget/TextView;

    const/high16 v1, 0x41500000    # 13.0f

    invoke-virtual {v0, v11, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 299
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->descrView:Landroid/widget/TextView;

    iget v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->textColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 301
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v12, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 302
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0x5b

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 303
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 304
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->descrView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setId(I)V

    .line 306
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->descrView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 307
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->descrView:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->addView(Landroid/view/View;)V

    .line 309
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v10, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 310
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 311
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0x5b

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 312
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 314
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->starsRatingView:Lcom/my/target/ca;

    iget-object v2, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v2, v13}, Lcom/my/target/cm;->n(I)I

    move-result v2

    invoke-virtual {v1, v9, v9, v9, v2}, Lcom/my/target/ca;->setPadding(IIII)V

    .line 315
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->starsRatingView:Lcom/my/target/ca;

    iget-object v2, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v2, v11}, Lcom/my/target/cm;->n(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/my/target/ca;->setStarsPadding(F)V

    .line 316
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->starsRatingView:Lcom/my/target/ca;

    iget-object v2, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    const/16 v5, 0xc

    invoke-virtual {v2, v5}, Lcom/my/target/cm;->n(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/my/target/ca;->setStarSize(I)V

    .line 317
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->starsRatingView:Lcom/my/target/ca;

    invoke-virtual {v1, v4}, Lcom/my/target/ca;->setId(I)V

    .line 318
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->starsRatingView:Lcom/my/target/ca;

    invoke-virtual {p0, v1, v0}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 320
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v10, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 321
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 322
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 323
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 325
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->votesCountView:Landroid/widget/TextView;

    sget-object v2, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 326
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->votesCountView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v2, v11}, Lcom/my/target/cm;->n(I)I

    move-result v2

    invoke-virtual {v1, v9, v2, v9, v9}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 327
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->votesCountView:Landroid/widget/TextView;

    const/high16 v2, 0x41500000    # 13.0f

    invoke-virtual {v1, v11, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 328
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->votesCountView:Landroid/widget/TextView;

    iget v2, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->textColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 329
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->votesCountView:Landroid/widget/TextView;

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 330
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->votesCountView:Landroid/widget/TextView;

    invoke-virtual {p0, v1, v0}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 331
    return-void
.end method


# virtual methods
.method public getBanner()Lcom/my/target/nativeads/banners/NativeAppwallBanner;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->banner:Lcom/my/target/nativeads/banners/NativeAppwallBanner;

    return-object v0
.end method

.method public getBannerIconImageView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->bannerIcon:Lcom/my/target/bv;

    return-object v0
.end method

.method public getCoinsCountTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsCountView:Landroid/widget/TextView;

    return-object v0
.end method

.method public getCoinsIconImageView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsIconView:Lcom/my/target/bv;

    return-object v0
.end method

.method public getDescriptionTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->descrView:Landroid/widget/TextView;

    return-object v0
.end method

.method public getNotificationImageView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->notificationView:Lcom/my/target/bv;

    return-object v0
.end method

.method public getOpenImageView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->openImageView:Lcom/my/target/bv;

    return-object v0
.end method

.method public getStarsRatingView()Lcom/my/target/ca;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->starsRatingView:Lcom/my/target/ca;

    return-object v0
.end method

.method public getStatusIconImageView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->statusIconView:Lcom/my/target/bv;

    return-object v0
.end method

.method public getTitleTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->titleView:Landroid/widget/TextView;

    return-object v0
.end method

.method public getVotesCountTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->votesCountView:Landroid/widget/TextView;

    return-object v0
.end method

.method public isViewed()Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->viewed:Z

    return v0
.end method

.method protected removeNotification()V
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->notificationView:Lcom/my/target/bv;

    invoke-virtual {p0, v0}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->removeView(Landroid/view/View;)V

    .line 221
    return-void
.end method

.method public setNativeAppwallBanner(Lcom/my/target/nativeads/banners/NativeAppwallBanner;)V
    .locals 9
    .param p1, "banner"    # Lcom/my/target/nativeads/banners/NativeAppwallBanner;

    .prologue
    const/16 v8, 0x14

    const/4 v7, 0x1

    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 75
    iput-object p1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->banner:Lcom/my/target/nativeads/banners/NativeAppwallBanner;

    .line 76
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->bannerIcon:Lcom/my/target/bv;

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->getIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setImageData(Lcom/my/target/common/models/ImageData;)V

    .line 77
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->getBubbleIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 78
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->notificationView:Lcom/my/target/bv;

    invoke-virtual {v1, v0}, Lcom/my/target/bv;->setImageData(Lcom/my/target/common/models/ImageData;)V

    .line 80
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->getDescription()Ljava/lang/String;

    move-result-object v1

    .line 81
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 83
    iget-object v3, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v2, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->descrView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->isHasNotification()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 88
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->notificationView:Lcom/my/target/bv;

    invoke-virtual {v1, v5}, Lcom/my/target/bv;->setVisibility(I)V

    .line 89
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->notificationView:Lcom/my/target/bv;

    invoke-virtual {v1, v0}, Lcom/my/target/bv;->setImageData(Lcom/my/target/common/models/ImageData;)V

    .line 96
    :goto_0
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->getCoins()I

    move-result v0

    if-lez v0, :cond_3

    .line 98
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsIconView:Lcom/my/target/bv;

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->getCoinsIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setImageData(Lcom/my/target/common/models/ImageData;)V

    .line 100
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsCountView:Landroid/widget/TextView;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const-string v2, "%d"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->getCoins()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsCountView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->getCoinsIconTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 102
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsBgShape:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->getCoinsIconBgColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 103
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->openImageView:Lcom/my/target/bv;

    invoke-virtual {v0, v6}, Lcom/my/target/bv;->setVisibility(I)V

    .line 117
    :goto_1
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->getStatusIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 118
    if-eqz v0, :cond_5

    .line 120
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->statusIconView:Lcom/my/target/bv;

    invoke-virtual {v1, v5}, Lcom/my/target/bv;->setVisibility(I)V

    .line 121
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->statusIconView:Lcom/my/target/bv;

    invoke-virtual {v1, v0}, Lcom/my/target/bv;->setImageData(Lcom/my/target/common/models/ImageData;)V

    .line 128
    :goto_2
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->getCoins()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->isAppInstalled()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->descrView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0x46

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v5, v5, v1, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 137
    :cond_1
    :goto_3
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->getRating()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_7

    .line 139
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->starsRatingView:Lcom/my/target/ca;

    invoke-virtual {v0, v5}, Lcom/my/target/ca;->setVisibility(I)V

    .line 140
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->votesCountView:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 142
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->starsRatingView:Lcom/my/target/ca;

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->getRating()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/ca;->setRating(F)V

    .line 143
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->votesCountView:Landroid/widget/TextView;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const-string v2, "%d"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->getVotes()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    :goto_4
    return-void

    .line 93
    :cond_2
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->notificationView:Lcom/my/target/bv;

    invoke-virtual {v0, v6}, Lcom/my/target/bv;->setVisibility(I)V

    goto/16 :goto_0

    .line 105
    :cond_3
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->isAppInstalled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 107
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->openImageView:Lcom/my/target/bv;

    invoke-virtual {v0, v5}, Lcom/my/target/bv;->setVisibility(I)V

    .line 109
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->openImageView:Lcom/my/target/bv;

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->getGotoAppIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setImageData(Lcom/my/target/common/models/ImageData;)V

    goto/16 :goto_1

    .line 113
    :cond_4
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->coinsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 114
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->openImageView:Lcom/my/target/bv;

    invoke-virtual {v0, v6}, Lcom/my/target/bv;->setVisibility(I)V

    goto/16 :goto_1

    .line 125
    :cond_5
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->statusIconView:Lcom/my/target/bv;

    invoke-virtual {v1, v6}, Lcom/my/target/bv;->setVisibility(I)V

    goto/16 :goto_2

    .line 132
    :cond_6
    if-eqz v0, :cond_1

    .line 134
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->descrView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v1, v8}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v5, v5, v1, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_3

    .line 147
    :cond_7
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->starsRatingView:Lcom/my/target/ca;

    invoke-virtual {v0, v6}, Lcom/my/target/ca;->setVisibility(I)V

    .line 148
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->votesCountView:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 149
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->descrView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->descrView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->descrView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->descrView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v4, v8}, Lcom/my/target/cm;->n(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_4
.end method

.method public setViewed(Z)V
    .locals 0
    .param p1, "viewed"    # Z

    .prologue
    .line 165
    iput-boolean p1, p0, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->viewed:Z

    .line 166
    return-void
.end method
