.class final Lru/cn/ad/preloader/LoadingEntry;
.super Ljava/lang/Object;
.source "LoadingEntry.java"


# instance fields
.field private adapter:Lru/cn/ad/AdAdapter;

.field private loadTime:J

.field final loader:Lru/cn/ad/AdapterLoader;

.field reloadIntervalMs:J


# direct methods
.method constructor <init>(Lru/cn/ad/AdapterLoader;)V
    .locals 2
    .param p1, "loader"    # Lru/cn/ad/AdapterLoader;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lru/cn/ad/preloader/LoadingEntry;->loader:Lru/cn/ad/AdapterLoader;

    .line 21
    const-wide/32 v0, 0x493e0

    iput-wide v0, p0, Lru/cn/ad/preloader/LoadingEntry;->reloadIntervalMs:J

    .line 22
    return-void
.end method


# virtual methods
.method adapter()Lru/cn/ad/AdAdapter;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lru/cn/ad/preloader/LoadingEntry;->adapter:Lru/cn/ad/AdAdapter;

    return-object v0
.end method

.method expired()Z
    .locals 4

    .prologue
    .line 34
    iget-wide v0, p0, Lru/cn/ad/preloader/LoadingEntry;->loadTime:J

    const-wide/32 v2, 0x36ee80

    add-long/2addr v0, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setAdapter(Lru/cn/ad/AdAdapter;)V
    .locals 2
    .param p1, "adapter"    # Lru/cn/ad/AdAdapter;

    .prologue
    .line 29
    iput-object p1, p0, Lru/cn/ad/preloader/LoadingEntry;->adapter:Lru/cn/ad/AdAdapter;

    .line 30
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lru/cn/ad/preloader/LoadingEntry;->loadTime:J

    .line 31
    return-void
.end method
