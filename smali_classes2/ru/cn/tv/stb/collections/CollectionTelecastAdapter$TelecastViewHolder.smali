.class final Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "CollectionTelecastAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/stb/collections/CollectionTelecastAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "TelecastViewHolder"
.end annotation


# instance fields
.field airTelecastText:Landroid/widget/TextView;

.field channelTitle:Landroid/widget/TextView;

.field paidIndicator:Landroid/widget/ImageView;

.field relatedTelecastWrapper:Landroid/view/View;

.field telecastDate:Landroid/widget/TextView;

.field telecastImage:Landroid/widget/ImageView;

.field telecastTitle:Landroid/widget/TextView;

.field viewsCountContainer:Landroid/view/View;

.field viewsCountText:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    .line 115
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 116
    const v0, 0x7f0901b8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->relatedTelecastWrapper:Landroid/view/View;

    .line 117
    const v0, 0x7f0900f4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->telecastImage:Landroid/widget/ImageView;

    .line 118
    const v0, 0x7f09002d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->airTelecastText:Landroid/widget/TextView;

    .line 119
    const v0, 0x7f0901ef

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->viewsCountContainer:Landroid/view/View;

    .line 120
    const v0, 0x7f0901f0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->viewsCountText:Landroid/widget/TextView;

    .line 121
    const v0, 0x7f0901d9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->telecastTitle:Landroid/widget/TextView;

    .line 122
    const v0, 0x7f09006c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->channelTitle:Landroid/widget/TextView;

    .line 123
    const v0, 0x7f090097

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->telecastDate:Landroid/widget/TextView;

    .line 124
    const v0, 0x7f09015b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter$TelecastViewHolder;->paidIndicator:Landroid/widget/ImageView;

    .line 125
    return-void
.end method
