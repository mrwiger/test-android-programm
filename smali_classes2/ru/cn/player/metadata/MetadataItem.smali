.class public final Lru/cn/player/metadata/MetadataItem;
.super Ljava/lang/Object;
.source "MetadataItem.java"


# instance fields
.field private final attributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final classLabel:Ljava/lang/String;

.field public final endTimeMs:J

.field public final startTimeMs:J

.field public final uniqueId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JJLjava/util/Map;)V
    .locals 1
    .param p1, "classLabel"    # Ljava/lang/String;
    .param p2, "uniqueId"    # Ljava/lang/String;
    .param p3, "startTimeUs"    # J
    .param p5, "endTimeUs"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JJ",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p7, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lru/cn/player/metadata/MetadataItem;->classLabel:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lru/cn/player/metadata/MetadataItem;->uniqueId:Ljava/lang/String;

    .line 26
    iput-wide p3, p0, Lru/cn/player/metadata/MetadataItem;->startTimeMs:J

    .line 27
    iput-wide p5, p0, Lru/cn/player/metadata/MetadataItem;->endTimeMs:J

    .line 28
    iput-object p7, p0, Lru/cn/player/metadata/MetadataItem;->attributes:Ljava/util/Map;

    .line 29
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58
    if-ne p0, p1, :cond_1

    move v2, v1

    .line 71
    :cond_0
    :goto_0
    return v2

    .line 59
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-ne v3, v4, :cond_0

    move-object v0, p1

    .line 61
    check-cast v0, Lru/cn/player/metadata/MetadataItem;

    .line 63
    .local v0, "that":Lru/cn/player/metadata/MetadataItem;
    iget-wide v4, p0, Lru/cn/player/metadata/MetadataItem;->startTimeMs:J

    iget-wide v6, v0, Lru/cn/player/metadata/MetadataItem;->startTimeMs:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 64
    iget-wide v4, p0, Lru/cn/player/metadata/MetadataItem;->endTimeMs:J

    iget-wide v6, v0, Lru/cn/player/metadata/MetadataItem;->endTimeMs:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 65
    iget-object v3, p0, Lru/cn/player/metadata/MetadataItem;->uniqueId:Ljava/lang/String;

    iget-object v4, v0, Lru/cn/player/metadata/MetadataItem;->uniqueId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 68
    iget-object v3, p0, Lru/cn/player/metadata/MetadataItem;->classLabel:Ljava/lang/String;

    iget-object v4, v0, Lru/cn/player/metadata/MetadataItem;->classLabel:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 71
    iget-object v3, p0, Lru/cn/player/metadata/MetadataItem;->attributes:Ljava/util/Map;

    if-eqz v3, :cond_3

    iget-object v1, p0, Lru/cn/player/metadata/MetadataItem;->attributes:Ljava/util/Map;

    iget-object v2, v0, Lru/cn/player/metadata/MetadataItem;->attributes:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v1

    :cond_2
    :goto_1
    move v2, v1

    goto :goto_0

    :cond_3
    iget-object v3, v0, Lru/cn/player/metadata/MetadataItem;->attributes:Ljava/util/Map;

    if-eqz v3, :cond_2

    move v1, v2

    goto :goto_1
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 76
    iget-object v1, p0, Lru/cn/player/metadata/MetadataItem;->uniqueId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 77
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lru/cn/player/metadata/MetadataItem;->classLabel:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 78
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lru/cn/player/metadata/MetadataItem;->startTimeMs:J

    iget-wide v4, p0, Lru/cn/player/metadata/MetadataItem;->startTimeMs:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 79
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lru/cn/player/metadata/MetadataItem;->endTimeMs:J

    iget-wide v4, p0, Lru/cn/player/metadata/MetadataItem;->endTimeMs:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 80
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lru/cn/player/metadata/MetadataItem;->attributes:Ljava/util/Map;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lru/cn/player/metadata/MetadataItem;->attributes:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->hashCode()I

    move-result v1

    :goto_0
    add-int v0, v2, v1

    .line 81
    return v0

    .line 80
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final stringValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "attributeKey"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 43
    iget-object v2, p0, Lru/cn/player/metadata/MetadataItem;->attributes:Ljava/util/Map;

    if-nez v2, :cond_0

    move-object v0, v1

    .line 53
    :goto_0
    return-object v0

    .line 46
    :cond_0
    iget-object v2, p0, Lru/cn/player/metadata/MetadataItem;->attributes:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 47
    .local v0, "value":Ljava/lang/Object;
    if-nez v0, :cond_1

    move-object v0, v1

    .line 48
    goto :goto_0

    .line 50
    :cond_1
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_2

    .line 51
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Attribute value is not a string for key "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 53
    :cond_2
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method
