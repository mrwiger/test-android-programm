.class public Lru/cn/guides/GuidesFragment;
.super Landroid/support/v4/app/Fragment;
.source "GuidesFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/guides/GuidesFragment$PagerAdapter;
    }
.end annotation


# instance fields
.field items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/guides/GuidManager$GuidItem;",
            ">;"
        }
    .end annotation
.end field

.field private nextButton:Landroid/widget/Button;

.field private pager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/guides/GuidesFragment;->items:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lru/cn/guides/GuidesFragment;)Landroid/support/v4/view/ViewPager;
    .locals 1
    .param p0, "x0"    # Lru/cn/guides/GuidesFragment;

    .prologue
    .line 17
    iget-object v0, p0, Lru/cn/guides/GuidesFragment;->pager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/guides/GuidesFragment;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lru/cn/guides/GuidesFragment;

    .prologue
    .line 17
    iget-object v0, p0, Lru/cn/guides/GuidesFragment;->nextButton:Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    sget v0, Lru/cn/guides/R$layout;->guides_fragment:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 53
    sget-object v3, Lru/cn/guides/GuidManager;->groups:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/guides/GuidManager$GuidGroup;

    .line 54
    .local v2, "group":Lru/cn/guides/GuidManager$GuidGroup;
    iget-object v4, p0, Lru/cn/guides/GuidesFragment;->items:Ljava/util/List;

    iget-object v5, v2, Lru/cn/guides/GuidManager$GuidGroup;->items:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 57
    .end local v2    # "group":Lru/cn/guides/GuidManager$GuidGroup;
    :cond_0
    sget v3, Lru/cn/guides/R$id;->pager:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v4/view/ViewPager;

    iput-object v3, p0, Lru/cn/guides/GuidesFragment;->pager:Landroid/support/v4/view/ViewPager;

    .line 59
    new-instance v0, Lru/cn/guides/GuidesFragment$PagerAdapter;

    invoke-virtual {p0}, Lru/cn/guides/GuidesFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-direct {v0, p0, v3}, Lru/cn/guides/GuidesFragment$PagerAdapter;-><init>(Lru/cn/guides/GuidesFragment;Landroid/support/v4/app/FragmentManager;)V

    .line 60
    .local v0, "adapter":Lru/cn/guides/GuidesFragment$PagerAdapter;
    iget-object v3, p0, Lru/cn/guides/GuidesFragment;->pager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 62
    sget v3, Lru/cn/guides/R$id;->close:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 63
    .local v1, "closeButton":Landroid/widget/Button;
    new-instance v3, Lru/cn/guides/GuidesFragment$1;

    invoke-direct {v3, p0}, Lru/cn/guides/GuidesFragment$1;-><init>(Lru/cn/guides/GuidesFragment;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    sget v3, Lru/cn/guides/R$id;->next:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lru/cn/guides/GuidesFragment;->nextButton:Landroid/widget/Button;

    .line 71
    iget-object v3, p0, Lru/cn/guides/GuidesFragment;->nextButton:Landroid/widget/Button;

    new-instance v4, Lru/cn/guides/GuidesFragment$2;

    invoke-direct {v4, p0}, Lru/cn/guides/GuidesFragment$2;-><init>(Lru/cn/guides/GuidesFragment;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    iget-object v3, p0, Lru/cn/guides/GuidesFragment;->pager:Landroid/support/v4/view/ViewPager;

    new-instance v4, Lru/cn/guides/GuidesFragment$3;

    invoke-direct {v4, p0}, Lru/cn/guides/GuidesFragment$3;-><init>(Lru/cn/guides/GuidesFragment;)V

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 100
    return-void
.end method
