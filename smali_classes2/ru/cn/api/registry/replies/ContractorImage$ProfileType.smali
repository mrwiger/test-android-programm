.class public final enum Lru/cn/api/registry/replies/ContractorImage$ProfileType;
.super Ljava/lang/Enum;
.source "ContractorImage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/api/registry/replies/ContractorImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ProfileType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/api/registry/replies/ContractorImage$ProfileType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/api/registry/replies/ContractorImage$ProfileType;

.field public static final enum LAUNCHER:Lru/cn/api/registry/replies/ContractorImage$ProfileType;

.field public static final enum LAUNCHER_KIDS:Lru/cn/api/registry/replies/ContractorImage$ProfileType;

.field public static final enum SQUARE_SOLID:Lru/cn/api/registry/replies/ContractorImage$ProfileType;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 8
    new-instance v0, Lru/cn/api/registry/replies/ContractorImage$ProfileType;

    const-string v1, "SQUARE_SOLID"

    invoke-direct {v0, v1, v4, v2}, Lru/cn/api/registry/replies/ContractorImage$ProfileType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/registry/replies/ContractorImage$ProfileType;->SQUARE_SOLID:Lru/cn/api/registry/replies/ContractorImage$ProfileType;

    new-instance v0, Lru/cn/api/registry/replies/ContractorImage$ProfileType;

    const-string v1, "LAUNCHER"

    invoke-direct {v0, v1, v2, v3}, Lru/cn/api/registry/replies/ContractorImage$ProfileType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/registry/replies/ContractorImage$ProfileType;->LAUNCHER:Lru/cn/api/registry/replies/ContractorImage$ProfileType;

    new-instance v0, Lru/cn/api/registry/replies/ContractorImage$ProfileType;

    const-string v1, "LAUNCHER_KIDS"

    invoke-direct {v0, v1, v3, v5}, Lru/cn/api/registry/replies/ContractorImage$ProfileType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/registry/replies/ContractorImage$ProfileType;->LAUNCHER_KIDS:Lru/cn/api/registry/replies/ContractorImage$ProfileType;

    .line 7
    new-array v0, v5, [Lru/cn/api/registry/replies/ContractorImage$ProfileType;

    sget-object v1, Lru/cn/api/registry/replies/ContractorImage$ProfileType;->SQUARE_SOLID:Lru/cn/api/registry/replies/ContractorImage$ProfileType;

    aput-object v1, v0, v4

    sget-object v1, Lru/cn/api/registry/replies/ContractorImage$ProfileType;->LAUNCHER:Lru/cn/api/registry/replies/ContractorImage$ProfileType;

    aput-object v1, v0, v2

    sget-object v1, Lru/cn/api/registry/replies/ContractorImage$ProfileType;->LAUNCHER_KIDS:Lru/cn/api/registry/replies/ContractorImage$ProfileType;

    aput-object v1, v0, v3

    sput-object v0, Lru/cn/api/registry/replies/ContractorImage$ProfileType;->$VALUES:[Lru/cn/api/registry/replies/ContractorImage$ProfileType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 13
    iput p3, p0, Lru/cn/api/registry/replies/ContractorImage$ProfileType;->value:I

    .line 14
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/api/registry/replies/ContractorImage$ProfileType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lru/cn/api/registry/replies/ContractorImage$ProfileType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/api/registry/replies/ContractorImage$ProfileType;

    return-object v0
.end method

.method public static values()[Lru/cn/api/registry/replies/ContractorImage$ProfileType;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lru/cn/api/registry/replies/ContractorImage$ProfileType;->$VALUES:[Lru/cn/api/registry/replies/ContractorImage$ProfileType;

    invoke-virtual {v0}, [Lru/cn/api/registry/replies/ContractorImage$ProfileType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/api/registry/replies/ContractorImage$ProfileType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lru/cn/api/registry/replies/ContractorImage$ProfileType;->value:I

    return v0
.end method
