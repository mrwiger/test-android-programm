.class final Lru/cn/player/exoplayer/SpleenyTrackSelector;
.super Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector;
.source "SpleenyTrackSelector.java"


# direct methods
.method constructor <init>(Lcom/google/android/exoplayer2/trackselection/TrackSelection$Factory;)V
    .locals 0
    .param p1, "factory"    # Lcom/google/android/exoplayer2/trackselection/TrackSelection$Factory;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector;-><init>(Lcom/google/android/exoplayer2/trackselection/TrackSelection$Factory;)V

    .line 21
    return-void
.end method

.method private findUnsupportedMIME([Lcom/google/android/exoplayer2/RendererCapabilities;[Lcom/google/android/exoplayer2/source/TrackGroupArray;[Lcom/google/android/exoplayer2/trackselection/TrackSelection;I)Ljava/lang/String;
    .locals 9
    .param p1, "rendererCapabilities"    # [Lcom/google/android/exoplayer2/RendererCapabilities;
    .param p2, "rendererTrackGroupArrays"    # [Lcom/google/android/exoplayer2/source/TrackGroupArray;
    .param p3, "selections"    # [Lcom/google/android/exoplayer2/trackselection/TrackSelection;
    .param p4, "trackType"    # I

    .prologue
    .line 78
    const/4 v5, 0x0

    .local v5, "index":I
    :goto_0
    array-length v8, p1

    if-ge v5, v8, :cond_3

    .line 79
    aget-object v0, p1, v5

    .line 80
    .local v0, "caps":Lcom/google/android/exoplayer2/RendererCapabilities;
    invoke-interface {v0}, Lcom/google/android/exoplayer2/RendererCapabilities;->getTrackType()I

    move-result v8

    if-eq v8, p4, :cond_1

    .line 78
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 83
    :cond_1
    aget-object v6, p3, v5

    .line 84
    .local v6, "selection":Lcom/google/android/exoplayer2/trackselection/TrackSelection;
    if-nez v6, :cond_0

    .line 88
    aget-object v4, p2, v5

    .line 89
    .local v4, "groups":Lcom/google/android/exoplayer2/source/TrackGroupArray;
    const/4 v3, 0x0

    .local v3, "groupIndex":I
    :goto_1
    iget v8, v4, Lcom/google/android/exoplayer2/source/TrackGroupArray;->length:I

    if-ge v3, v8, :cond_0

    .line 90
    invoke-virtual {v4, v3}, Lcom/google/android/exoplayer2/source/TrackGroupArray;->get(I)Lcom/google/android/exoplayer2/source/TrackGroup;

    move-result-object v2

    .line 91
    .local v2, "group":Lcom/google/android/exoplayer2/source/TrackGroup;
    const/4 v7, 0x0

    .local v7, "trackIndex":I
    iget v8, v2, Lcom/google/android/exoplayer2/source/TrackGroup;->length:I

    if-ge v7, v8, :cond_2

    .line 92
    invoke-virtual {v2, v7}, Lcom/google/android/exoplayer2/source/TrackGroup;->getFormat(I)Lcom/google/android/exoplayer2/Format;

    move-result-object v1

    .line 93
    .local v1, "format":Lcom/google/android/exoplayer2/Format;
    iget-object v8, v1, Lcom/google/android/exoplayer2/Format;->sampleMimeType:Ljava/lang/String;

    .line 97
    .end local v0    # "caps":Lcom/google/android/exoplayer2/RendererCapabilities;
    .end local v1    # "format":Lcom/google/android/exoplayer2/Format;
    .end local v2    # "group":Lcom/google/android/exoplayer2/source/TrackGroup;
    .end local v3    # "groupIndex":I
    .end local v4    # "groups":Lcom/google/android/exoplayer2/source/TrackGroupArray;
    .end local v6    # "selection":Lcom/google/android/exoplayer2/trackselection/TrackSelection;
    .end local v7    # "trackIndex":I
    :goto_2
    return-object v8

    .line 89
    .restart local v0    # "caps":Lcom/google/android/exoplayer2/RendererCapabilities;
    .restart local v2    # "group":Lcom/google/android/exoplayer2/source/TrackGroup;
    .restart local v3    # "groupIndex":I
    .restart local v4    # "groups":Lcom/google/android/exoplayer2/source/TrackGroupArray;
    .restart local v6    # "selection":Lcom/google/android/exoplayer2/trackselection/TrackSelection;
    .restart local v7    # "trackIndex":I
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 97
    .end local v0    # "caps":Lcom/google/android/exoplayer2/RendererCapabilities;
    .end local v2    # "group":Lcom/google/android/exoplayer2/source/TrackGroup;
    .end local v3    # "groupIndex":I
    .end local v4    # "groups":Lcom/google/android/exoplayer2/source/TrackGroupArray;
    .end local v6    # "selection":Lcom/google/android/exoplayer2/trackselection/TrackSelection;
    .end local v7    # "trackIndex":I
    :cond_3
    const/4 v8, 0x0

    goto :goto_2
.end method


# virtual methods
.method getGroupArrayByTrackType(Lcom/google/android/exoplayer2/SimpleExoPlayer;I)Lcom/google/android/exoplayer2/source/TrackGroupArray;
    .locals 3
    .param p1, "player"    # Lcom/google/android/exoplayer2/SimpleExoPlayer;
    .param p2, "trackType"    # I

    .prologue
    const/4 v2, 0x0

    .line 117
    invoke-virtual {p0}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->getCurrentMappedTrackInfo()Lcom/google/android/exoplayer2/trackselection/MappingTrackSelector$MappedTrackInfo;

    move-result-object v0

    .line 118
    .local v0, "mappedTrackInfo":Lcom/google/android/exoplayer2/trackselection/MappingTrackSelector$MappedTrackInfo;
    if-nez v0, :cond_1

    .line 125
    :cond_0
    :goto_0
    return-object v2

    .line 121
    :cond_1
    invoke-virtual {p0, p1, p2}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->getRendererIndex(Lcom/google/android/exoplayer2/SimpleExoPlayer;I)I

    move-result v1

    .line 122
    .local v1, "rendererIndex":I
    if-ltz v1, :cond_0

    .line 125
    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/trackselection/MappingTrackSelector$MappedTrackInfo;->getTrackGroups(I)Lcom/google/android/exoplayer2/source/TrackGroupArray;

    move-result-object v2

    goto :goto_0
.end method

.method getRendererIndex(Lcom/google/android/exoplayer2/SimpleExoPlayer;I)I
    .locals 5
    .param p1, "player"    # Lcom/google/android/exoplayer2/SimpleExoPlayer;
    .param p2, "trackType"    # I

    .prologue
    const/4 v3, -0x1

    .line 101
    invoke-virtual {p0}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->getCurrentMappedTrackInfo()Lcom/google/android/exoplayer2/trackselection/MappingTrackSelector$MappedTrackInfo;

    move-result-object v1

    .line 102
    .local v1, "mappedTrackInfo":Lcom/google/android/exoplayer2/trackselection/MappingTrackSelector$MappedTrackInfo;
    if-nez v1, :cond_1

    move v0, v3

    .line 112
    :cond_0
    :goto_0
    return v0

    .line 106
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v4, v1, Lcom/google/android/exoplayer2/trackselection/MappingTrackSelector$MappedTrackInfo;->length:I

    if-ge v0, v4, :cond_3

    .line 107
    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer2/trackselection/MappingTrackSelector$MappedTrackInfo;->getTrackGroups(I)Lcom/google/android/exoplayer2/source/TrackGroupArray;

    move-result-object v2

    .line 108
    .local v2, "trackGroups":Lcom/google/android/exoplayer2/source/TrackGroupArray;
    iget v4, v2, Lcom/google/android/exoplayer2/source/TrackGroupArray;->length:I

    if-eqz v4, :cond_2

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->getRendererType(I)I

    move-result v4

    if-eq v4, p2, :cond_0

    .line 106
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v2    # "trackGroups":Lcom/google/android/exoplayer2/source/TrackGroupArray;
    :cond_3
    move v0, v3

    .line 112
    goto :goto_0
.end method

.method getSelectionOverride(Lcom/google/android/exoplayer2/SimpleExoPlayer;I)Lcom/google/android/exoplayer2/trackselection/MappingTrackSelector$SelectionOverride;
    .locals 3
    .param p1, "player"    # Lcom/google/android/exoplayer2/SimpleExoPlayer;
    .param p2, "trackType"    # I

    .prologue
    const/4 v2, 0x0

    .line 130
    invoke-virtual {p0, p1, p2}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->getRendererIndex(Lcom/google/android/exoplayer2/SimpleExoPlayer;I)I

    move-result v1

    .line 131
    .local v1, "rendererIndex":I
    if-gez v1, :cond_1

    .line 140
    :cond_0
    :goto_0
    return-object v2

    .line 135
    :cond_1
    invoke-virtual {p0, p1, p2}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->getGroupArrayByTrackType(Lcom/google/android/exoplayer2/SimpleExoPlayer;I)Lcom/google/android/exoplayer2/source/TrackGroupArray;

    move-result-object v0

    .line 136
    .local v0, "groups":Lcom/google/android/exoplayer2/source/TrackGroupArray;
    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {p0, v1, v0}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->getSelectionOverride(ILcom/google/android/exoplayer2/source/TrackGroupArray;)Lcom/google/android/exoplayer2/trackselection/MappingTrackSelector$SelectionOverride;

    move-result-object v2

    goto :goto_0
.end method

.method protected selectTracks([Lcom/google/android/exoplayer2/RendererCapabilities;[Lcom/google/android/exoplayer2/source/TrackGroupArray;[[[I)[Lcom/google/android/exoplayer2/trackselection/TrackSelection;
    .locals 12
    .param p1, "rendererCapabilities"    # [Lcom/google/android/exoplayer2/RendererCapabilities;
    .param p2, "rendererTrackGroupArrays"    # [Lcom/google/android/exoplayer2/source/TrackGroupArray;
    .param p3, "rendererFormatSupports"    # [[[I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/ExoPlaybackException;
        }
    .end annotation

    .prologue
    .line 27
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector;->selectTracks([Lcom/google/android/exoplayer2/RendererCapabilities;[Lcom/google/android/exoplayer2/source/TrackGroupArray;[[[I)[Lcom/google/android/exoplayer2/trackselection/TrackSelection;

    move-result-object v7

    .line 31
    .local v7, "selections":[Lcom/google/android/exoplayer2/trackselection/TrackSelection;
    const/4 v5, 0x0

    .local v5, "requireVideo":Z
    const/4 v4, 0x0

    .line 32
    .local v4, "requireAudio":Z
    const/4 v1, 0x0

    .local v1, "hasVideo":Z
    const/4 v0, 0x0

    .line 33
    .local v0, "hasAudio":Z
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_0
    array-length v10, v7

    if-ge v2, v10, :cond_4

    .line 34
    aget-object v6, v7, v2

    .line 35
    .local v6, "selection":Lcom/google/android/exoplayer2/trackselection/TrackSelection;
    aget-object v8, p2, v2

    .line 37
    .local v8, "trackGroupArray":Lcom/google/android/exoplayer2/source/TrackGroupArray;
    aget-object v10, p1, v2

    invoke-interface {v10}, Lcom/google/android/exoplayer2/RendererCapabilities;->getTrackType()I

    move-result v9

    .line 38
    .local v9, "type":I
    const/4 v10, 0x1

    if-ne v9, v10, :cond_2

    .line 39
    iget v10, v8, Lcom/google/android/exoplayer2/source/TrackGroupArray;->length:I

    if-lez v10, :cond_0

    .line 40
    const/4 v4, 0x1

    .line 43
    :cond_0
    if-eqz v6, :cond_1

    .line 44
    const/4 v0, 0x1

    .line 33
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 47
    :cond_2
    const/4 v10, 0x2

    if-ne v9, v10, :cond_1

    .line 48
    iget v10, v8, Lcom/google/android/exoplayer2/source/TrackGroupArray;->length:I

    if-lez v10, :cond_3

    .line 49
    const/4 v5, 0x1

    .line 52
    :cond_3
    if-eqz v6, :cond_1

    .line 53
    const/4 v1, 0x1

    goto :goto_1

    .line 58
    .end local v6    # "selection":Lcom/google/android/exoplayer2/trackselection/TrackSelection;
    .end local v8    # "trackGroupArray":Lcom/google/android/exoplayer2/source/TrackGroupArray;
    .end local v9    # "type":I
    :cond_4
    if-nez v0, :cond_5

    if-eqz v4, :cond_5

    .line 59
    const/4 v10, 0x1

    invoke-direct {p0, p1, p2, v7, v10}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->findUnsupportedMIME([Lcom/google/android/exoplayer2/RendererCapabilities;[Lcom/google/android/exoplayer2/source/TrackGroupArray;[Lcom/google/android/exoplayer2/trackselection/TrackSelection;I)Ljava/lang/String;

    move-result-object v3

    .line 61
    .local v3, "mime":Ljava/lang/String;
    new-instance v10, Lru/cn/player/exoplayer/ExoPlayerUtils$UnsupportedCodecException;

    const-string v11, "Unable to find codec"

    invoke-direct {v10, v11, v3}, Lru/cn/player/exoplayer/ExoPlayerUtils$UnsupportedCodecException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v11, -0x1

    invoke-static {v10, v11}, Lcom/google/android/exoplayer2/ExoPlaybackException;->createForRenderer(Ljava/lang/Exception;I)Lcom/google/android/exoplayer2/ExoPlaybackException;

    move-result-object v10

    throw v10

    .line 64
    .end local v3    # "mime":Ljava/lang/String;
    :cond_5
    if-nez v1, :cond_6

    if-eqz v5, :cond_6

    .line 65
    const/4 v10, 0x2

    invoke-direct {p0, p1, p2, v7, v10}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->findUnsupportedMIME([Lcom/google/android/exoplayer2/RendererCapabilities;[Lcom/google/android/exoplayer2/source/TrackGroupArray;[Lcom/google/android/exoplayer2/trackselection/TrackSelection;I)Ljava/lang/String;

    move-result-object v3

    .line 67
    .restart local v3    # "mime":Ljava/lang/String;
    new-instance v10, Lru/cn/player/exoplayer/ExoPlayerUtils$UnsupportedCodecException;

    const-string v11, "Unable to find codec"

    invoke-direct {v10, v11, v3}, Lru/cn/player/exoplayer/ExoPlayerUtils$UnsupportedCodecException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v11, -0x1

    invoke-static {v10, v11}, Lcom/google/android/exoplayer2/ExoPlaybackException;->createForRenderer(Ljava/lang/Exception;I)Lcom/google/android/exoplayer2/ExoPlaybackException;

    move-result-object v10

    throw v10

    .line 70
    .end local v3    # "mime":Ljava/lang/String;
    :cond_6
    return-object v7
.end method
