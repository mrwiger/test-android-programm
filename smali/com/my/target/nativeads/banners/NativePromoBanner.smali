.class public Lcom/my/target/nativeads/banners/NativePromoBanner;
.super Ljava/lang/Object;
.source "NativePromoBanner.java"


# instance fields
.field private final advertisingLabel:Ljava/lang/String;

.field private final ageRestrictions:Ljava/lang/String;

.field private final category:Ljava/lang/String;

.field private final ctaText:Ljava/lang/String;

.field private final description:Ljava/lang/String;

.field private final disclaimer:Ljava/lang/String;

.field private final domain:Ljava/lang/String;

.field private final hasVideo:Z

.field private final icon:Lcom/my/target/common/models/ImageData;

.field private final image:Lcom/my/target/common/models/ImageData;

.field private final nativePromoCards:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/nativeads/banners/NativePromoCard;",
            ">;"
        }
    .end annotation
.end field

.field private final navigationType:Ljava/lang/String;

.field private final rating:F

.field private final subCategory:Ljava/lang/String;

.field private final title:Ljava/lang/String;

.field private final votes:I


# direct methods
.method private constructor <init>(Lcom/my/target/core/models/banners/a;)V
    .locals 3
    .param p1, "nativeAdBanner"    # Lcom/my/target/core/models/banners/a;

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->nativePromoCards:Ljava/util/ArrayList;

    .line 41
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getNavigationType()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->navigationType:Ljava/lang/String;

    .line 42
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getRating()F

    move-result v0

    iput v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->rating:F

    .line 43
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getVotes()I

    move-result v0

    iput v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->votes:I

    .line 44
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getVideoBanner()Lcom/my/target/aj;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->hasVideo:Z

    .line 45
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 46
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    :goto_1
    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->title:Ljava/lang/String;

    .line 47
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getDescription()Ljava/lang/String;

    move-result-object v0

    .line 48
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    :goto_2
    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->description:Ljava/lang/String;

    .line 49
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getCtaText()Ljava/lang/String;

    move-result-object v0

    .line 50
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    :goto_3
    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->ctaText:Ljava/lang/String;

    .line 51
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getDisclaimer()Ljava/lang/String;

    move-result-object v0

    .line 52
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    :goto_4
    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->disclaimer:Ljava/lang/String;

    .line 53
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getAgeRestrictions()Ljava/lang/String;

    move-result-object v0

    .line 54
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    :goto_5
    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->ageRestrictions:Ljava/lang/String;

    .line 55
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getCategory()Ljava/lang/String;

    move-result-object v0

    .line 56
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    :goto_6
    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->category:Ljava/lang/String;

    .line 57
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getSubCategory()Ljava/lang/String;

    move-result-object v0

    .line 58
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    :goto_7
    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->subCategory:Ljava/lang/String;

    .line 59
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getDomain()Ljava/lang/String;

    move-result-object v0

    .line 60
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    :goto_8
    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->domain:Ljava/lang/String;

    .line 61
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getAdvertisingLabel()Ljava/lang/String;

    move-result-object v0

    .line 62
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v1, v0

    :cond_0
    iput-object v1, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->advertisingLabel:Ljava/lang/String;

    .line 63
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->image:Lcom/my/target/common/models/ImageData;

    .line 64
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->icon:Lcom/my/target/common/models/ImageData;

    .line 65
    invoke-direct {p0, p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->processCards(Lcom/my/target/core/models/banners/a;)V

    .line 66
    return-void

    .line 44
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 46
    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 48
    goto :goto_2

    :cond_4
    move-object v0, v1

    .line 50
    goto :goto_3

    :cond_5
    move-object v0, v1

    .line 52
    goto :goto_4

    :cond_6
    move-object v0, v1

    .line 54
    goto :goto_5

    :cond_7
    move-object v0, v1

    .line 56
    goto :goto_6

    :cond_8
    move-object v0, v1

    .line 58
    goto :goto_7

    :cond_9
    move-object v0, v1

    .line 60
    goto :goto_8
.end method

.method public static newBanner(Lcom/my/target/core/models/banners/a;)Lcom/my/target/nativeads/banners/NativePromoBanner;
    .locals 1
    .param p0, "nativeAdBanner"    # Lcom/my/target/core/models/banners/a;

    .prologue
    .line 18
    new-instance v0, Lcom/my/target/nativeads/banners/NativePromoBanner;

    invoke-direct {v0, p0}, Lcom/my/target/nativeads/banners/NativePromoBanner;-><init>(Lcom/my/target/core/models/banners/a;)V

    return-object v0
.end method

.method private processCards(Lcom/my/target/core/models/banners/a;)V
    .locals 3
    .param p1, "nativeAdBanner"    # Lcom/my/target/core/models/banners/a;

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->hasVideo:Z

    if-nez v0, :cond_0

    .line 72
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getNativeAdCards()Ljava/util/List;

    move-result-object v0

    .line 73
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 75
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/b;

    .line 77
    iget-object v2, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->nativePromoCards:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/my/target/nativeads/banners/NativePromoCard;->newCard(Lcom/my/target/core/models/banners/b;)Lcom/my/target/nativeads/banners/NativePromoCard;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 81
    :cond_0
    return-void
.end method


# virtual methods
.method public getAdvertisingLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->advertisingLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getAgeRestrictions()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->ageRestrictions:Ljava/lang/String;

    return-object v0
.end method

.method public getCards()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/nativeads/banners/NativePromoCard;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->nativePromoCards:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->category:Ljava/lang/String;

    return-object v0
.end method

.method public getCtaText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->ctaText:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getDisclaimer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->disclaimer:Ljava/lang/String;

    return-object v0
.end method

.method public getDomain()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->domain:Ljava/lang/String;

    return-object v0
.end method

.method public getIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->icon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public getImage()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->image:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public getNavigationType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->navigationType:Ljava/lang/String;

    return-object v0
.end method

.method public getRating()F
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->rating:F

    return v0
.end method

.method public getSubCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->subCategory:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getVotes()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->votes:I

    return v0
.end method

.method public hasVideo()Z
    .locals 1

    .prologue
    .line 155
    iget-boolean v0, p0, Lcom/my/target/nativeads/banners/NativePromoBanner;->hasVideo:Z

    return v0
.end method
