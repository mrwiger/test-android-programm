.class Lru/cn/tv/player/SimplePlayerFragmentEx$9;
.super Ljava/lang/Object;
.source "SimplePlayerFragmentEx.java"

# interfaces
.implements Lcom/google/android/gms/cast/framework/SessionManagerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/player/SimplePlayerFragmentEx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/cast/framework/SessionManagerListener",
        "<",
        "Lcom/google/android/gms/cast/framework/CastSession;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;


# direct methods
.method constructor <init>(Lru/cn/tv/player/SimplePlayerFragmentEx;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/player/SimplePlayerFragmentEx;

    .prologue
    .line 804
    iput-object p1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$9;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSessionEnded(Lcom/google/android/gms/cast/framework/CastSession;I)V
    .locals 2
    .param p1, "castSession"    # Lcom/google/android/gms/cast/framework/CastSession;
    .param p2, "errorCode"    # I

    .prologue
    .line 829
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$9;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lru/cn/tv/player/SimplePlayerFragmentEx;->setCastSession(Lcom/google/android/gms/cast/framework/CastSession;)V

    .line 830
    return-void
.end method

.method public bridge synthetic onSessionEnded(Lcom/google/android/gms/cast/framework/Session;I)V
    .locals 0

    .prologue
    .line 804
    check-cast p1, Lcom/google/android/gms/cast/framework/CastSession;

    invoke-virtual {p0, p1, p2}, Lru/cn/tv/player/SimplePlayerFragmentEx$9;->onSessionEnded(Lcom/google/android/gms/cast/framework/CastSession;I)V

    return-void
.end method

.method public onSessionEnding(Lcom/google/android/gms/cast/framework/CastSession;)V
    .locals 1
    .param p1, "castSession"    # Lcom/google/android/gms/cast/framework/CastSession;

    .prologue
    .line 824
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$9;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->onCastDisconnect()V

    .line 825
    return-void
.end method

.method public bridge synthetic onSessionEnding(Lcom/google/android/gms/cast/framework/Session;)V
    .locals 0

    .prologue
    .line 804
    check-cast p1, Lcom/google/android/gms/cast/framework/CastSession;

    invoke-virtual {p0, p1}, Lru/cn/tv/player/SimplePlayerFragmentEx$9;->onSessionEnding(Lcom/google/android/gms/cast/framework/CastSession;)V

    return-void
.end method

.method public onSessionResumeFailed(Lcom/google/android/gms/cast/framework/CastSession;I)V
    .locals 0
    .param p1, "castSession"    # Lcom/google/android/gms/cast/framework/CastSession;
    .param p2, "i"    # I

    .prologue
    .line 843
    return-void
.end method

.method public bridge synthetic onSessionResumeFailed(Lcom/google/android/gms/cast/framework/Session;I)V
    .locals 0

    .prologue
    .line 804
    check-cast p1, Lcom/google/android/gms/cast/framework/CastSession;

    invoke-virtual {p0, p1, p2}, Lru/cn/tv/player/SimplePlayerFragmentEx$9;->onSessionResumeFailed(Lcom/google/android/gms/cast/framework/CastSession;I)V

    return-void
.end method

.method public onSessionResumed(Lcom/google/android/gms/cast/framework/CastSession;Z)V
    .locals 1
    .param p1, "castSession"    # Lcom/google/android/gms/cast/framework/CastSession;
    .param p2, "wasSuspended"    # Z

    .prologue
    .line 838
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$9;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-virtual {v0, p1}, Lru/cn/tv/player/SimplePlayerFragmentEx;->setCastSession(Lcom/google/android/gms/cast/framework/CastSession;)V

    .line 839
    return-void
.end method

.method public bridge synthetic onSessionResumed(Lcom/google/android/gms/cast/framework/Session;Z)V
    .locals 0

    .prologue
    .line 804
    check-cast p1, Lcom/google/android/gms/cast/framework/CastSession;

    invoke-virtual {p0, p1, p2}, Lru/cn/tv/player/SimplePlayerFragmentEx$9;->onSessionResumed(Lcom/google/android/gms/cast/framework/CastSession;Z)V

    return-void
.end method

.method public onSessionResuming(Lcom/google/android/gms/cast/framework/CastSession;Ljava/lang/String;)V
    .locals 0
    .param p1, "castSession"    # Lcom/google/android/gms/cast/framework/CastSession;
    .param p2, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 834
    return-void
.end method

.method public bridge synthetic onSessionResuming(Lcom/google/android/gms/cast/framework/Session;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 804
    check-cast p1, Lcom/google/android/gms/cast/framework/CastSession;

    invoke-virtual {p0, p1, p2}, Lru/cn/tv/player/SimplePlayerFragmentEx$9;->onSessionResuming(Lcom/google/android/gms/cast/framework/CastSession;Ljava/lang/String;)V

    return-void
.end method

.method public onSessionStartFailed(Lcom/google/android/gms/cast/framework/CastSession;I)V
    .locals 0
    .param p1, "castSession"    # Lcom/google/android/gms/cast/framework/CastSession;
    .param p2, "errorCode"    # I

    .prologue
    .line 820
    return-void
.end method

.method public bridge synthetic onSessionStartFailed(Lcom/google/android/gms/cast/framework/Session;I)V
    .locals 0

    .prologue
    .line 804
    check-cast p1, Lcom/google/android/gms/cast/framework/CastSession;

    invoke-virtual {p0, p1, p2}, Lru/cn/tv/player/SimplePlayerFragmentEx$9;->onSessionStartFailed(Lcom/google/android/gms/cast/framework/CastSession;I)V

    return-void
.end method

.method public onSessionStarted(Lcom/google/android/gms/cast/framework/CastSession;Ljava/lang/String;)V
    .locals 2
    .param p1, "castSession"    # Lcom/google/android/gms/cast/framework/CastSession;
    .param p2, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 814
    invoke-virtual {p1}, Lcom/google/android/gms/cast/framework/CastSession;->getCastDevice()Lcom/google/android/gms/cast/CastDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/cast/CastDevice;->getFriendlyName()Ljava/lang/String;

    move-result-object v0

    .line 815
    .local v0, "deviceName":Ljava/lang/String;
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$9;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-virtual {v1, v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->onCastConnectionSuccess(Ljava/lang/String;)V

    .line 816
    return-void
.end method

.method public bridge synthetic onSessionStarted(Lcom/google/android/gms/cast/framework/Session;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 804
    check-cast p1, Lcom/google/android/gms/cast/framework/CastSession;

    invoke-virtual {p0, p1, p2}, Lru/cn/tv/player/SimplePlayerFragmentEx$9;->onSessionStarted(Lcom/google/android/gms/cast/framework/CastSession;Ljava/lang/String;)V

    return-void
.end method

.method public onSessionStarting(Lcom/google/android/gms/cast/framework/CastSession;)V
    .locals 1
    .param p1, "castSession"    # Lcom/google/android/gms/cast/framework/CastSession;

    .prologue
    .line 807
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$9;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-virtual {v0, p1}, Lru/cn/tv/player/SimplePlayerFragmentEx;->setCastSession(Lcom/google/android/gms/cast/framework/CastSession;)V

    .line 809
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$9;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->onCastConnectionInProgress()V

    .line 810
    return-void
.end method

.method public bridge synthetic onSessionStarting(Lcom/google/android/gms/cast/framework/Session;)V
    .locals 0

    .prologue
    .line 804
    check-cast p1, Lcom/google/android/gms/cast/framework/CastSession;

    invoke-virtual {p0, p1}, Lru/cn/tv/player/SimplePlayerFragmentEx$9;->onSessionStarting(Lcom/google/android/gms/cast/framework/CastSession;)V

    return-void
.end method

.method public onSessionSuspended(Lcom/google/android/gms/cast/framework/CastSession;I)V
    .locals 0
    .param p1, "castSession"    # Lcom/google/android/gms/cast/framework/CastSession;
    .param p2, "i"    # I

    .prologue
    .line 847
    return-void
.end method

.method public bridge synthetic onSessionSuspended(Lcom/google/android/gms/cast/framework/Session;I)V
    .locals 0

    .prologue
    .line 804
    check-cast p1, Lcom/google/android/gms/cast/framework/CastSession;

    invoke-virtual {p0, p1, p2}, Lru/cn/tv/player/SimplePlayerFragmentEx$9;->onSessionSuspended(Lcom/google/android/gms/cast/framework/CastSession;I)V

    return-void
.end method
