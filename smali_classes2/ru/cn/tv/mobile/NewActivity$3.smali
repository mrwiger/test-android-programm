.class Lru/cn/tv/mobile/NewActivity$3;
.super Ljava/lang/Object;
.source "NewActivity.java"

# interfaces
.implements Landroid/support/design/widget/NavigationView$OnNavigationItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/mobile/NewActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/mobile/NewActivity;


# direct methods
.method constructor <init>(Lru/cn/tv/mobile/NewActivity;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/mobile/NewActivity;

    .prologue
    .line 180
    iput-object p1, p0, Lru/cn/tv/mobile/NewActivity$3;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method final synthetic lambda$onNavigationItemSelected$0$NewActivity$3(Lru/cn/tv/mobile/collections/CollectionsFragment;J)V
    .locals 4
    .param p1, "tabRubricFragment"    # Lru/cn/tv/mobile/collections/CollectionsFragment;
    .param p2, "telecastId"    # J

    .prologue
    .line 212
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 215
    invoke-virtual {p1}, Lru/cn/tv/mobile/collections/CollectionsFragment;->getRubricId()J

    move-result-wide v2

    .line 212
    invoke-static {v0, v1, v2, v3}, Lru/cn/domain/statistics/inetra/InetraTracker;->setSessionParams(IIJ)V

    .line 217
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity$3;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-static {v0, p2, p3}, Lru/cn/tv/mobile/NewActivity;->access$1000(Lru/cn/tv/mobile/NewActivity;J)V

    .line 218
    return-void
.end method

.method public onNavigationItemSelected(Landroid/view/MenuItem;)Z
    .locals 12
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 183
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 185
    .local v2, "id":I
    packed-switch v2, :pswitch_data_0

    .line 253
    :goto_0
    :pswitch_0
    iget-object v6, p0, Lru/cn/tv/mobile/NewActivity$3;->this$0:Lru/cn/tv/mobile/NewActivity;

    const v7, 0x7f0900a7

    invoke-virtual {v6, v7}, Lru/cn/tv/mobile/NewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/widget/DrawerLayout;

    .line 254
    .local v1, "drawer":Landroid/support/v4/widget/DrawerLayout;
    if-eqz v1, :cond_0

    .line 255
    const v6, 0x800003

    invoke-virtual {v1, v6}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(I)V

    .line 257
    :cond_0
    return v11

    .line 187
    .end local v1    # "drawer":Landroid/support/v4/widget/DrawerLayout;
    :pswitch_1
    iget-object v6, p0, Lru/cn/tv/mobile/NewActivity$3;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-static {v6, v11, v10}, Lru/cn/tv/mobile/NewActivity;->access$200(Lru/cn/tv/mobile/NewActivity;IZ)V

    goto :goto_0

    .line 191
    :pswitch_2
    iget-object v6, p0, Lru/cn/tv/mobile/NewActivity$3;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-static {v6, v10, v10}, Lru/cn/tv/mobile/NewActivity;->access$200(Lru/cn/tv/mobile/NewActivity;IZ)V

    goto :goto_0

    .line 195
    :pswitch_3
    iget-object v6, p0, Lru/cn/tv/mobile/NewActivity$3;->this$0:Lru/cn/tv/mobile/NewActivity;

    const/4 v7, 0x2

    invoke-static {v6, v7, v10}, Lru/cn/tv/mobile/NewActivity;->access$200(Lru/cn/tv/mobile/NewActivity;IZ)V

    goto :goto_0

    .line 199
    :pswitch_4
    iget-object v6, p0, Lru/cn/tv/mobile/NewActivity$3;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-static {v6}, Lru/cn/tv/mobile/NewActivity;->access$300(Lru/cn/tv/mobile/NewActivity;)V

    goto :goto_0

    .line 203
    :pswitch_5
    iget-object v6, p0, Lru/cn/tv/mobile/NewActivity$3;->this$0:Lru/cn/tv/mobile/NewActivity;

    const v7, 0x7f0900ae

    invoke-virtual {v6, v7}, Lru/cn/tv/mobile/NewActivity;->getRubric(I)Lru/cn/api/catalogue/replies/Rubric;

    move-result-object v5

    .line 204
    .local v5, "topRubric":Lru/cn/api/catalogue/replies/Rubric;
    iget-object v6, p0, Lru/cn/tv/mobile/NewActivity$3;->this$0:Lru/cn/tv/mobile/NewActivity;

    iget-wide v8, v5, Lru/cn/api/catalogue/replies/Rubric;->id:J

    iget-object v7, v5, Lru/cn/api/catalogue/replies/Rubric;->title:Ljava/lang/String;

    invoke-static {v6, v8, v9, v7, v10}, Lru/cn/tv/mobile/NewActivity;->access$400(Lru/cn/tv/mobile/NewActivity;JLjava/lang/String;Z)V

    goto :goto_0

    .line 208
    .end local v5    # "topRubric":Lru/cn/api/catalogue/replies/Rubric;
    :pswitch_6
    iget-object v6, p0, Lru/cn/tv/mobile/NewActivity$3;->this$0:Lru/cn/tv/mobile/NewActivity;

    iget-object v6, v6, Lru/cn/tv/mobile/NewActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v6, v11}, Landroid/support/v7/app/ActionBarDrawerToggle;->setDrawerIndicatorEnabled(Z)V

    .line 209
    new-instance v4, Lru/cn/tv/mobile/collections/CollectionsFragment;

    invoke-direct {v4}, Lru/cn/tv/mobile/collections/CollectionsFragment;-><init>()V

    .line 211
    .local v4, "tabRubricFragment":Lru/cn/tv/mobile/collections/CollectionsFragment;
    new-instance v6, Lru/cn/tv/mobile/NewActivity$3$$Lambda$0;

    invoke-direct {v6, p0, v4}, Lru/cn/tv/mobile/NewActivity$3$$Lambda$0;-><init>(Lru/cn/tv/mobile/NewActivity$3;Lru/cn/tv/mobile/collections/CollectionsFragment;)V

    invoke-virtual {v4, v6}, Lru/cn/tv/mobile/collections/CollectionsFragment;->setListener(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)V

    .line 220
    iget-object v6, p0, Lru/cn/tv/mobile/NewActivity$3;->this$0:Lru/cn/tv/mobile/NewActivity;

    const v7, 0x7f0e0066

    invoke-virtual {v6, v7}, Lru/cn/tv/mobile/NewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 222
    .local v0, "collectionsTitle":Ljava/lang/String;
    iget-object v6, p0, Lru/cn/tv/mobile/NewActivity$3;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-virtual {v6}, Lru/cn/tv/mobile/NewActivity;->getRubricsWithoutHint()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ne v6, v11, :cond_1

    .line 223
    iget-object v6, p0, Lru/cn/tv/mobile/NewActivity$3;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-virtual {v6}, Lru/cn/tv/mobile/NewActivity;->getRubricsWithoutHint()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lru/cn/api/catalogue/replies/Rubric;

    iget-object v0, v6, Lru/cn/api/catalogue/replies/Rubric;->title:Ljava/lang/String;

    .line 226
    :cond_1
    iget-object v6, p0, Lru/cn/tv/mobile/NewActivity$3;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-static {v6, v4, v0, v10}, Lru/cn/tv/mobile/NewActivity;->access$500(Lru/cn/tv/mobile/NewActivity;Landroid/support/v4/app/Fragment;Ljava/lang/String;Z)V

    goto :goto_0

    .line 230
    .end local v0    # "collectionsTitle":Ljava/lang/String;
    .end local v4    # "tabRubricFragment":Lru/cn/tv/mobile/collections/CollectionsFragment;
    :pswitch_7
    iget-object v6, p0, Lru/cn/tv/mobile/NewActivity$3;->this$0:Lru/cn/tv/mobile/NewActivity;

    const v7, 0x7f0900a9

    invoke-virtual {v6, v7}, Lru/cn/tv/mobile/NewActivity;->getRubric(I)Lru/cn/api/catalogue/replies/Rubric;

    move-result-object v3

    .line 231
    .local v3, "newsRubric":Lru/cn/api/catalogue/replies/Rubric;
    invoke-virtual {v3}, Lru/cn/api/catalogue/replies/Rubric;->haveSubrubrics()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 232
    iget-object v6, p0, Lru/cn/tv/mobile/NewActivity$3;->this$0:Lru/cn/tv/mobile/NewActivity;

    iget-wide v8, v3, Lru/cn/api/catalogue/replies/Rubric;->id:J

    iget-object v7, v3, Lru/cn/api/catalogue/replies/Rubric;->title:Ljava/lang/String;

    invoke-static {v6, v8, v9, v7}, Lru/cn/tv/mobile/NewActivity;->access$600(Lru/cn/tv/mobile/NewActivity;JLjava/lang/String;)V

    goto/16 :goto_0

    .line 234
    :cond_2
    iget-object v6, p0, Lru/cn/tv/mobile/NewActivity$3;->this$0:Lru/cn/tv/mobile/NewActivity;

    iget-wide v8, v3, Lru/cn/api/catalogue/replies/Rubric;->id:J

    iget-object v7, v3, Lru/cn/api/catalogue/replies/Rubric;->title:Ljava/lang/String;

    invoke-static {v6, v8, v9, v7}, Lru/cn/tv/mobile/NewActivity;->access$700(Lru/cn/tv/mobile/NewActivity;JLjava/lang/String;)V

    goto/16 :goto_0

    .line 239
    .end local v3    # "newsRubric":Lru/cn/api/catalogue/replies/Rubric;
    :pswitch_8
    iget-object v6, p0, Lru/cn/tv/mobile/NewActivity$3;->this$0:Lru/cn/tv/mobile/NewActivity;

    new-instance v7, Landroid/content/Intent;

    iget-object v8, p0, Lru/cn/tv/mobile/NewActivity$3;->this$0:Lru/cn/tv/mobile/NewActivity;

    const-class v9, Lru/cn/tv/mobile/settings/PreferenceActivity;

    invoke-direct {v7, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v8, 0x160b

    invoke-virtual {v6, v7, v8}, Lru/cn/tv/mobile/NewActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 243
    :pswitch_9
    iget-object v6, p0, Lru/cn/tv/mobile/NewActivity$3;->this$0:Lru/cn/tv/mobile/NewActivity;

    const-wide/16 v8, 0x2

    invoke-static {v6, v8, v9}, Lru/cn/tv/mobile/NewActivity;->access$800(Lru/cn/tv/mobile/NewActivity;J)V

    .line 245
    const-string v6, "\u0418\u043d\u0435\u0442\u0440\u0430, \u041e\u041e\u041e"

    const-string v7, "menu"

    invoke-static {v6, v7}, Lru/cn/domain/statistics/AnalyticsManager;->store_open(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 249
    :pswitch_a
    iget-object v6, p0, Lru/cn/tv/mobile/NewActivity$3;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-static {v6}, Lru/cn/tv/mobile/NewActivity;->access$900(Lru/cn/tv/mobile/NewActivity;)V

    goto/16 :goto_0

    .line 185
    :pswitch_data_0
    .packed-switch 0x7f0900a3
        :pswitch_2
        :pswitch_6
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_7
        :pswitch_0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_5
    .end packed-switch
.end method
