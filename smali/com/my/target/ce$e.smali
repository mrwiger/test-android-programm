.class Lcom/my/target/ce$e;
.super Ljava/lang/Object;
.source "ClickHandler.java"

# interfaces
.implements Lcom/my/target/common/MyTargetActivity$ActivityEngine;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/ce;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "e"
.end annotation


# instance fields
.field private final jM:Ljava/lang/String;

.field private jN:Lcom/my/target/cb;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 416
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 417
    iput-object p1, p0, Lcom/my/target/ce$e;->jM:Ljava/lang/String;

    .line 418
    return-void
.end method

.method public static N(Ljava/lang/String;)Lcom/my/target/ce$e;
    .locals 1

    .prologue
    .line 409
    new-instance v0, Lcom/my/target/ce$e;

    invoke-direct {v0, p0}, Lcom/my/target/ce$e;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public onActivityBackPressed()Z
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/my/target/ce$e;->jN:Lcom/my/target/cb;

    if-eqz v0, :cond_0

    .line 496
    iget-object v0, p0, Lcom/my/target/ce$e;->jN:Lcom/my/target/cb;

    invoke-virtual {v0}, Lcom/my/target/cb;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498
    iget-object v0, p0, Lcom/my/target/ce$e;->jN:Lcom/my/target/cb;

    invoke-virtual {v0}, Lcom/my/target/cb;->goBack()V

    .line 499
    const/4 v0, 0x0

    .line 502
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onActivityCreate(Lcom/my/target/common/MyTargetActivity;Landroid/content/Intent;Landroid/widget/FrameLayout;)V
    .locals 2
    .param p1, "activity"    # Lcom/my/target/common/MyTargetActivity;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "rootLayout"    # Landroid/widget/FrameLayout;

    .prologue
    .line 434
    const v0, 0x103000d

    invoke-virtual {p1, v0}, Lcom/my/target/common/MyTargetActivity;->setTheme(I)V

    .line 435
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 437
    invoke-virtual {p1}, Lcom/my/target/common/MyTargetActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 438
    const/high16 v1, -0x80000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 439
    const v1, -0xbaa59c

    invoke-virtual {v0, v1}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 442
    :cond_0
    new-instance v0, Lcom/my/target/cb;

    invoke-direct {v0, p1}, Lcom/my/target/cb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ce$e;->jN:Lcom/my/target/cb;

    .line 443
    iget-object v0, p0, Lcom/my/target/ce$e;->jN:Lcom/my/target/cb;

    invoke-virtual {p3, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 445
    iget-object v0, p0, Lcom/my/target/ce$e;->jN:Lcom/my/target/cb;

    invoke-virtual {v0}, Lcom/my/target/cb;->be()V

    .line 446
    iget-object v0, p0, Lcom/my/target/ce$e;->jN:Lcom/my/target/cb;

    iget-object v1, p0, Lcom/my/target/ce$e;->jM:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/my/target/cb;->setUrl(Ljava/lang/String;)V

    .line 447
    iget-object v0, p0, Lcom/my/target/ce$e;->jN:Lcom/my/target/cb;

    new-instance v1, Lcom/my/target/ce$e$1;

    invoke-direct {v1, p0, p1}, Lcom/my/target/ce$e$1;-><init>(Lcom/my/target/ce$e;Lcom/my/target/common/MyTargetActivity;)V

    invoke-virtual {v0, v1}, Lcom/my/target/cb;->setListener(Lcom/my/target/cb$b;)V

    .line 455
    return-void
.end method

.method public onActivityDestroy()V
    .locals 1

    .prologue
    .line 484
    iget-object v0, p0, Lcom/my/target/ce$e;->jN:Lcom/my/target/cb;

    if-eqz v0, :cond_0

    .line 486
    iget-object v0, p0, Lcom/my/target/ce$e;->jN:Lcom/my/target/cb;

    invoke-virtual {v0}, Lcom/my/target/cb;->destroy()V

    .line 487
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/ce$e;->jN:Lcom/my/target/cb;

    .line 489
    :cond_0
    return-void
.end method

.method public onActivityOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 508
    const/4 v0, 0x0

    return v0
.end method

.method public onActivityPause()V
    .locals 0

    .prologue
    .line 473
    return-void
.end method

.method public onActivityResume()V
    .locals 0

    .prologue
    .line 479
    return-void
.end method

.method public onActivityStart()V
    .locals 0

    .prologue
    .line 461
    return-void
.end method

.method public onActivityStop()V
    .locals 0

    .prologue
    .line 467
    return-void
.end method

.method public s(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 422
    sput-object p0, Lcom/my/target/common/MyTargetActivity;->activityEngine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

    .line 423
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/my/target/common/MyTargetActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 424
    instance-of v1, p1, Landroid/app/Activity;

    if-nez v1, :cond_0

    .line 426
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 428
    :cond_0
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 429
    return-void
.end method
