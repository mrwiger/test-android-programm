.class public Lcom/yandex/metrica/impl/ob/cj;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/cj;->a:Landroid/content/Context;

    .line 32
    return-void
.end method

.method private a()Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/cj;->b()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    return-object v0
.end method

.method private b()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 85
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cj;->a:Landroid/content/Context;

    const-string v1, "com.yandex.metrica.configuration"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(J)Lcom/yandex/metrica/impl/ob/cj;
    .locals 3

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/cj;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "lwutt"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 48
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/cj;
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/cj;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "client_configurations"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 37
    return-object p0
.end method

.method public a(Z)Lcom/yandex/metrica/impl/ob/cj;
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/cj;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "les"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 58
    return-object p0
.end method

.method public b(J)J
    .locals 3

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/cj;->b()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "lwutt"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 42
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cj;->a:Landroid/content/Context;

    const-string v1, "com.yandex.metrica.configuration"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "client_configurations"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Z)Z
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/cj;->b()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "les"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public c(J)Lcom/yandex/metrica/impl/ob/cj;
    .locals 3

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/cj;->a()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "lci"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 69
    return-object p0
.end method

.method public d(J)J
    .locals 3

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/cj;->b()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "lci"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method
