.class Lru/cn/tv/stb/categories/CategoryFragment$1;
.super Ljava/lang/Object;
.source "CategoryFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/stb/categories/CategoryFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/stb/categories/CategoryFragment;


# direct methods
.method constructor <init>(Lru/cn/tv/stb/categories/CategoryFragment;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/stb/categories/CategoryFragment;

    .prologue
    .line 90
    iput-object p1, p0, Lru/cn/tv/stb/categories/CategoryFragment$1;->this$0:Lru/cn/tv/stb/categories/CategoryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 95
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lru/cn/tv/stb/categories/CategoryFragment$1;->this$0:Lru/cn/tv/stb/categories/CategoryFragment;

    invoke-static {v1}, Lru/cn/tv/stb/categories/CategoryFragment;->access$000(Lru/cn/tv/stb/categories/CategoryFragment;)Lru/cn/domain/tv/CurrentCategory$Type;

    move-result-object v0

    .line 96
    .local v0, "type":Lru/cn/domain/tv/CurrentCategory$Type;
    iget-object v1, p0, Lru/cn/tv/stb/categories/CategoryFragment$1;->this$0:Lru/cn/tv/stb/categories/CategoryFragment;

    invoke-static {v1}, Lru/cn/tv/stb/categories/CategoryFragment;->access$100(Lru/cn/tv/stb/categories/CategoryFragment;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 97
    if-nez p3, :cond_1

    .line 98
    iget-object v1, p0, Lru/cn/tv/stb/categories/CategoryFragment$1;->this$0:Lru/cn/tv/stb/categories/CategoryFragment;

    sget-object v2, Lru/cn/domain/tv/CurrentCategory$Type;->all:Lru/cn/domain/tv/CurrentCategory$Type;

    invoke-virtual {v1, v2}, Lru/cn/tv/stb/categories/CategoryFragment;->getPositionByType(Lru/cn/domain/tv/CurrentCategory$Type;)I

    move-result p3

    .line 99
    iget-object v1, p0, Lru/cn/tv/stb/categories/CategoryFragment$1;->this$0:Lru/cn/tv/stb/categories/CategoryFragment;

    invoke-virtual {v1, p3}, Lru/cn/tv/stb/categories/CategoryFragment;->setSelection(I)V

    .line 100
    sget-object v0, Lru/cn/domain/tv/CurrentCategory$Type;->all:Lru/cn/domain/tv/CurrentCategory$Type;

    .line 106
    :cond_0
    :goto_0
    iget-object v1, p0, Lru/cn/tv/stb/categories/CategoryFragment$1;->this$0:Lru/cn/tv/stb/categories/CategoryFragment;

    invoke-static {v1}, Lru/cn/tv/stb/categories/CategoryFragment;->access$300(Lru/cn/tv/stb/categories/CategoryFragment;)Lru/cn/tv/stb/categories/CategoryViewModel;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/cn/tv/stb/categories/CategoryViewModel;->selectCategory(Lru/cn/domain/tv/CurrentCategory$Type;)V

    .line 107
    return-void

    .line 102
    :cond_1
    iget-object v1, p0, Lru/cn/tv/stb/categories/CategoryFragment$1;->this$0:Lru/cn/tv/stb/categories/CategoryFragment;

    invoke-static {v1, p3}, Lru/cn/tv/stb/categories/CategoryFragment;->access$200(Lru/cn/tv/stb/categories/CategoryFragment;I)Lru/cn/domain/tv/CurrentCategory$Type;

    move-result-object v0

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 111
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
