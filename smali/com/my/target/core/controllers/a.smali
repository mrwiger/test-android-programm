.class public final Lcom/my/target/core/controllers/a;
.super Ljava/lang/Object;
.source "NativeAdVideoController.java"

# interfaces
.implements Lcom/my/target/br$a;
.implements Lcom/my/target/cc$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/core/controllers/a$b;,
        Lcom/my/target/core/controllers/a$a;,
        Lcom/my/target/core/controllers/a$c;
    }
.end annotation


# instance fields
.field private final f:Lcom/my/target/common/models/VideoData;

.field private final g:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private final h:Lcom/my/target/core/models/banners/a;

.field private final i:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/my/target/ap;",
            ">;"
        }
    .end annotation
.end field

.field private j:Landroid/view/View$OnClickListener;

.field private k:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/my/target/nativeads/views/MediaAdView;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcom/my/target/cv;

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Lcom/my/target/core/controllers/a$c;

.field private s:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/my/target/br;",
            ">;"
        }
    .end annotation
.end field

.field private state:I

.field private t:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/my/target/cw;",
            ">;"
        }
    .end annotation
.end field

.field private final videoBanner:Lcom/my/target/aj;


# direct methods
.method public constructor <init>(Lcom/my/target/core/models/banners/a;Lcom/my/target/aj;Lcom/my/target/common/models/VideoData;)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p2, p0, Lcom/my/target/core/controllers/a;->videoBanner:Lcom/my/target/aj;

    .line 63
    iput-object p1, p0, Lcom/my/target/core/controllers/a;->h:Lcom/my/target/core/models/banners/a;

    .line 64
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/my/target/core/controllers/a;->i:Ljava/util/HashSet;

    .line 65
    iput-object p3, p0, Lcom/my/target/core/controllers/a;->f:Lcom/my/target/common/models/VideoData;

    .line 66
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->videoBanner:Lcom/my/target/aj;

    invoke-virtual {v0}, Lcom/my/target/aj;->isAutoPlay()Z

    move-result v0

    iput-boolean v0, p0, Lcom/my/target/core/controllers/a;->n:Z

    .line 67
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->videoBanner:Lcom/my/target/aj;

    invoke-virtual {v0}, Lcom/my/target/aj;->isAutoMute()Z

    move-result v0

    iput-boolean v0, p0, Lcom/my/target/core/controllers/a;->q:Z

    .line 68
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->videoBanner:Lcom/my/target/aj;

    invoke-virtual {v0}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->i:Ljava/util/HashSet;

    invoke-virtual {v0}, Lcom/my/target/ar;->ae()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 70
    new-instance v0, Lcom/my/target/core/controllers/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/my/target/core/controllers/a$a;-><init>(Lcom/my/target/core/controllers/a;B)V

    iput-object v0, p0, Lcom/my/target/core/controllers/a;->g:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 72
    return-void
.end method

.method private a(FLandroid/content/Context;)V
    .locals 3

    .prologue
    .line 666
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->i:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 668
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->i:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 669
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 671
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/ap;

    .line 673
    invoke-virtual {v0}, Lcom/my/target/ap;->Z()F

    move-result v2

    cmpg-float v2, v2, p1

    if-gtz v2, :cond_0

    .line 675
    invoke-static {v0, p2}, Lcom/my/target/cl;->a(Lcom/my/target/aq;Landroid/content/Context;)V

    .line 676
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 680
    :cond_1
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 657
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 658
    if-eqz v0, :cond_0

    .line 660
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->g:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 662
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/my/target/core/controllers/a;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1613
    const/4 v0, 0x0

    .line 1614
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->k:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    .line 1616
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/nativeads/views/MediaAdView;

    .line 1618
    :cond_0
    if-eqz v0, :cond_3

    .line 1623
    iput-boolean v2, p0, Lcom/my/target/core/controllers/a;->o:Z

    .line 1624
    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1626
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    if-nez v1, :cond_1

    .line 1628
    invoke-static {p0, v0}, Lcom/my/target/cv;->b(Lcom/my/target/core/controllers/a;Landroid/content/Context;)Lcom/my/target/cv;

    move-result-object v1

    iput-object v1, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    .line 1630
    :cond_1
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v1, p0}, Lcom/my/target/cv;->setVideoListener(Lcom/my/target/cc$a;)V

    .line 1631
    invoke-direct {p0, v0}, Lcom/my/target/core/controllers/a;->b(Landroid/content/Context;)V

    .line 1633
    iget v1, p0, Lcom/my/target/core/controllers/a;->state:I

    if-ne v1, v2, :cond_2

    .line 1635
    const/4 v1, 0x4

    iput v1, p0, Lcom/my/target/core/controllers/a;->state:I

    .line 1636
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v1}, Lcom/my/target/cv;->bm()V

    .line 1639
    :cond_2
    invoke-static {p0, v0}, Lcom/my/target/br;->a(Lcom/my/target/br$a;Landroid/content/Context;)Lcom/my/target/br;

    move-result-object v0

    .line 1640
    invoke-virtual {v0}, Lcom/my/target/br;->show()V

    .line 34
    :cond_3
    return-void
.end method

.method static synthetic a(Lcom/my/target/core/controllers/a;Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 34
    .line 2713
    iput v2, p0, Lcom/my/target/core/controllers/a;->state:I

    .line 2714
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    if-nez v0, :cond_0

    .line 2716
    invoke-static {p0, p1}, Lcom/my/target/cv;->b(Lcom/my/target/core/controllers/a;Landroid/content/Context;)Lcom/my/target/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    .line 2718
    :cond_0
    invoke-direct {p0, p1}, Lcom/my/target/core/controllers/a;->b(Landroid/content/Context;)V

    .line 2719
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    if-eqz v0, :cond_1

    .line 2721
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    iget-object v1, p0, Lcom/my/target/core/controllers/a;->f:Lcom/my/target/common/models/VideoData;

    invoke-virtual {v0, v1, v2}, Lcom/my/target/cv;->a(Lcom/my/target/common/models/VideoData;Z)V

    .line 2723
    :cond_1
    invoke-virtual {p0}, Lcom/my/target/core/controllers/a;->bu()V

    .line 34
    return-void
.end method

.method static synthetic a(Lcom/my/target/core/controllers/a;Ljava/lang/String;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/my/target/core/controllers/a;->sendStat(Ljava/lang/String;Landroid/content/Context;)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 645
    iget-boolean v0, p0, Lcom/my/target/core/controllers/a;->o:Z

    if-eqz v0, :cond_0

    .line 647
    const/4 v0, 0x2

    iput v0, p0, Lcom/my/target/core/controllers/a;->state:I

    .line 648
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    if-eqz v0, :cond_0

    .line 650
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v0, p1}, Lcom/my/target/cv;->j(Z)V

    .line 653
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/my/target/core/controllers/a;Z)Z
    .locals 0

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/my/target/core/controllers/a;->q:Z

    return p1
.end method

.method private b(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 692
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 693
    if-eqz v0, :cond_0

    .line 695
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->g:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 699
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/my/target/core/controllers/a;)V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/my/target/core/controllers/a;->a(Z)V

    return-void
.end method

.method private bj()V
    .locals 1

    .prologue
    .line 605
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    if-eqz v0, :cond_0

    .line 607
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v0}, Lcom/my/target/cv;->bj()V

    .line 609
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/my/target/core/controllers/a;)Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/my/target/core/controllers/a;->o:Z

    return v0
.end method

.method static synthetic d(Lcom/my/target/core/controllers/a;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/my/target/core/controllers/a;->j()V

    return-void
.end method

.method static synthetic e(Lcom/my/target/core/controllers/a;)V
    .locals 1

    .prologue
    .line 34
    .line 2593
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    if-eqz v0, :cond_0

    .line 2595
    const-string v0, "Audiofocus loss can duck, set volume to 0.3"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 2596
    iget-boolean v0, p0, Lcom/my/target/core/controllers/a;->q:Z

    if-nez v0, :cond_0

    .line 2598
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v0}, Lcom/my/target/cv;->bl()V

    .line 34
    :cond_0
    return-void
.end method

.method static synthetic f(Lcom/my/target/core/controllers/a;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->k:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method static synthetic g(Lcom/my/target/core/controllers/a;)Lcom/my/target/core/controllers/a$c;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->r:Lcom/my/target/core/controllers/a$c;

    return-object v0
.end method

.method static synthetic h(Lcom/my/target/core/controllers/a;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->t:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method static synthetic i(Lcom/my/target/core/controllers/a;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/my/target/core/controllers/a;->state:I

    return v0
.end method

.method static synthetic j(Lcom/my/target/core/controllers/a;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->j:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method private j()V
    .locals 1

    .prologue
    .line 684
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    if-eqz v0, :cond_0

    .line 686
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v0}, Lcom/my/target/cv;->bk()V

    .line 688
    :cond_0
    return-void
.end method

.method static synthetic k(Lcom/my/target/core/controllers/a;)I
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x2

    iput v0, p0, Lcom/my/target/core/controllers/a;->state:I

    return v0
.end method

.method static synthetic l(Lcom/my/target/core/controllers/a;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->s:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method static synthetic m(Lcom/my/target/core/controllers/a;)Lcom/my/target/aj;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->videoBanner:Lcom/my/target/aj;

    return-object v0
.end method

.method static synthetic n(Lcom/my/target/core/controllers/a;)Lcom/my/target/cv;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    return-object v0
.end method

.method private sendStat(Ljava/lang/String;Landroid/content/Context;)V
    .locals 1
    .param p1, "stat"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 703
    if-nez p2, :cond_0

    .line 709
    :goto_0
    return-void

    .line 707
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->videoBanner:Lcom/my/target/aj;

    invoke-virtual {v0}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    .line 708
    invoke-virtual {v0, p1}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method public final L(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 473
    const/4 v0, 0x3

    iput v0, p0, Lcom/my/target/core/controllers/a;->state:I

    .line 474
    invoke-virtual {p0}, Lcom/my/target/core/controllers/a;->bq()V

    .line 475
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/my/target/core/controllers/a;->j:Landroid/view/View$OnClickListener;

    .line 77
    return-void
.end method

.method public final a(Lcom/my/target/br;Landroid/widget/FrameLayout;)V
    .locals 4
    .param p1, "dialog"    # Lcom/my/target/br;
    .param p2, "rootLayout"    # Landroid/widget/FrameLayout;

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 491
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/my/target/core/controllers/a;->s:Ljava/lang/ref/WeakReference;

    .line 493
    new-instance v1, Lcom/my/target/cw;

    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/my/target/cw;-><init>(Landroid/content/Context;)V

    .line 494
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 496
    invoke-virtual {v1, v0}, Lcom/my/target/cw;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 497
    invoke-virtual {p2, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 499
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/my/target/core/controllers/a;->t:Ljava/lang/ref/WeakReference;

    .line 500
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->h:Lcom/my/target/core/models/banners/a;

    iget-object v2, p0, Lcom/my/target/core/controllers/a;->f:Lcom/my/target/common/models/VideoData;

    invoke-virtual {v1, v0, v2}, Lcom/my/target/cw;->a(Lcom/my/target/core/models/banners/a;Lcom/my/target/common/models/VideoData;)V

    .line 501
    new-instance v0, Lcom/my/target/core/controllers/a$b;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/my/target/core/controllers/a$b;-><init>(Lcom/my/target/core/controllers/a;B)V

    invoke-virtual {v1, v0}, Lcom/my/target/cw;->setVideoDialogViewListener(Lcom/my/target/cw$d;)V

    .line 503
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    if-eqz v0, :cond_1

    .line 505
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v0}, Lcom/my/target/cv;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 506
    if-eqz v0, :cond_0

    .line 508
    iget-object v2, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 510
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v1, v0}, Lcom/my/target/cw;->b(Lcom/my/target/cv;)V

    .line 513
    :cond_1
    iget-boolean v0, p0, Lcom/my/target/core/controllers/a;->q:Z

    if-eqz v0, :cond_3

    .line 515
    invoke-direct {p0}, Lcom/my/target/core/controllers/a;->bj()V

    .line 522
    :goto_0
    const-string v0, "fullscreenOn"

    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/my/target/core/controllers/a;->sendStat(Ljava/lang/String;Landroid/content/Context;)V

    .line 523
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    if-eqz v0, :cond_2

    .line 525
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    iget-object v1, p0, Lcom/my/target/core/controllers/a;->f:Lcom/my/target/common/models/VideoData;

    invoke-virtual {v0, v1, v3}, Lcom/my/target/cv;->a(Lcom/my/target/common/models/VideoData;Z)V

    .line 526
    iput v3, p0, Lcom/my/target/core/controllers/a;->state:I

    .line 528
    :cond_2
    return-void

    .line 519
    :cond_3
    invoke-direct {p0}, Lcom/my/target/core/controllers/a;->j()V

    goto :goto_0
.end method

.method public final a(Lcom/my/target/core/controllers/a$c;)V
    .locals 0

    .prologue
    .line 485
    iput-object p1, p0, Lcom/my/target/core/controllers/a;->r:Lcom/my/target/core/controllers/a$c;

    .line 486
    return-void
.end method

.method public final a(Lcom/my/target/nativeads/views/MediaAdView;)V
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/my/target/core/controllers/a;->unregister()V

    .line 130
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/my/target/core/controllers/a;->k:Ljava/lang/ref/WeakReference;

    .line 132
    iget-boolean v0, p0, Lcom/my/target/core/controllers/a;->o:Z

    if-nez v0, :cond_0

    .line 134
    iget-boolean v0, p0, Lcom/my/target/core/controllers/a;->n:Z

    if-eqz v0, :cond_1

    .line 136
    invoke-virtual {p0}, Lcom/my/target/core/controllers/a;->bu()V

    .line 144
    :cond_0
    :goto_0
    new-instance v0, Lcom/my/target/core/controllers/a$1;

    invoke-direct {v0, p0}, Lcom/my/target/core/controllers/a$1;-><init>(Lcom/my/target/core/controllers/a;)V

    invoke-virtual {p1, v0}, Lcom/my/target/nativeads/views/MediaAdView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    return-void

    .line 140
    :cond_1
    invoke-virtual {p0}, Lcom/my/target/core/controllers/a;->bq()V

    goto :goto_0
.end method

.method public final aU()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 539
    const-string v0, "Dismiss dialog"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 541
    iput-object v2, p0, Lcom/my/target/core/controllers/a;->s:Ljava/lang/ref/WeakReference;

    .line 542
    iput-boolean v4, p0, Lcom/my/target/core/controllers/a;->o:Z

    .line 544
    invoke-direct {p0}, Lcom/my/target/core/controllers/a;->bj()V

    .line 546
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    if-eqz v0, :cond_2

    .line 548
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v0}, Lcom/my/target/cv;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/my/target/core/controllers/a;->a(Landroid/content/Context;)V

    .line 549
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v0}, Lcom/my/target/cv;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 551
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->k:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_5

    .line 553
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/my/target/nativeads/views/MediaAdView;

    .line 555
    :goto_0
    if-eqz v0, :cond_0

    if-eq v0, v1, :cond_0

    .line 557
    iget-object v3, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 558
    if-eqz v1, :cond_0

    .line 560
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0, v4, v3}, Lcom/my/target/nativeads/views/MediaAdView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 567
    :cond_0
    iget v0, p0, Lcom/my/target/core/controllers/a;->state:I

    if-ne v0, v5, :cond_3

    .line 569
    invoke-virtual {p0}, Lcom/my/target/core/controllers/a;->br()V

    .line 570
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->videoBanner:Lcom/my/target/aj;

    invoke-virtual {v0}, Lcom/my/target/aj;->isAutoPlay()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 572
    iput-boolean v5, p0, Lcom/my/target/core/controllers/a;->n:Z

    .line 574
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v0}, Lcom/my/target/cv;->bi()V

    .line 586
    :goto_1
    const-string v0, "fullscreenOff"

    iget-object v1, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v1}, Lcom/my/target/cv;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/my/target/core/controllers/a;->sendStat(Ljava/lang/String;Landroid/content/Context;)V

    .line 588
    :cond_2
    iput-object v2, p0, Lcom/my/target/core/controllers/a;->t:Ljava/lang/ref/WeakReference;

    .line 589
    return-void

    .line 576
    :cond_3
    iget v0, p0, Lcom/my/target/core/controllers/a;->state:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 578
    iput-boolean v4, p0, Lcom/my/target/core/controllers/a;->n:Z

    .line 579
    invoke-virtual {p0}, Lcom/my/target/core/controllers/a;->bq()V

    .line 580
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v0, v5}, Lcom/my/target/cv;->k(Z)V

    goto :goto_1

    .line 584
    :cond_4
    iput-boolean v4, p0, Lcom/my/target/core/controllers/a;->n:Z

    goto :goto_1

    :cond_5
    move-object v1, v2

    goto :goto_0
.end method

.method public final b(FF)V
    .locals 7
    .param p1, "position"    # F

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 400
    :goto_0
    invoke-virtual {p0}, Lcom/my/target/core/controllers/a;->br()V

    .line 401
    const/4 v1, 0x0

    .line 402
    iget-object v2, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    if-eqz v2, :cond_9

    .line 404
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v1}, Lcom/my/target/cv;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object v2, v1

    .line 406
    :goto_1
    iget-boolean v1, p0, Lcom/my/target/core/controllers/a;->p:Z

    if-nez v1, :cond_2

    .line 408
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->r:Lcom/my/target/core/controllers/a$c;

    if-eqz v1, :cond_0

    .line 410
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->r:Lcom/my/target/core/controllers/a$c;

    invoke-interface {v1}, Lcom/my/target/core/controllers/a$c;->n()V

    .line 412
    :cond_0
    const-string v1, "playbackStarted"

    invoke-direct {p0, v1, v2}, Lcom/my/target/core/controllers/a;->sendStat(Ljava/lang/String;Landroid/content/Context;)V

    .line 413
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->i:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 414
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->videoBanner:Lcom/my/target/aj;

    invoke-virtual {v1}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v1

    .line 415
    iget-object v3, p0, Lcom/my/target/core/controllers/a;->i:Ljava/util/HashSet;

    invoke-virtual {v1}, Lcom/my/target/ar;->ae()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 417
    if-eqz v2, :cond_1

    .line 419
    invoke-direct {p0, v4, v2}, Lcom/my/target/core/controllers/a;->a(FLandroid/content/Context;)V

    .line 421
    :cond_1
    iput-boolean v6, p0, Lcom/my/target/core/controllers/a;->p:Z

    .line 423
    :cond_2
    iget-boolean v1, p0, Lcom/my/target/core/controllers/a;->m:Z

    if-eqz v1, :cond_3

    .line 425
    cmpl-float v1, p1, p2

    if-eqz v1, :cond_3

    .line 427
    iput-boolean v5, p0, Lcom/my/target/core/controllers/a;->m:Z

    .line 432
    :cond_3
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->videoBanner:Lcom/my/target/aj;

    invoke-virtual {v1}, Lcom/my/target/aj;->getDuration()F

    move-result v0

    .line 434
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->t:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_4

    .line 436
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->t:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/my/target/cw;

    .line 437
    if-eqz v1, :cond_4

    .line 439
    invoke-virtual {v1, p1, v0}, Lcom/my/target/cw;->b(FF)V

    .line 443
    :cond_4
    cmpg-float v1, p1, v0

    if-gtz v1, :cond_8

    .line 445
    cmpl-float v1, p1, v4

    if-eqz v1, :cond_5

    if-eqz v2, :cond_5

    .line 447
    invoke-direct {p0, p1, v2}, Lcom/my/target/core/controllers/a;->a(FLandroid/content/Context;)V

    .line 449
    :cond_5
    cmpl-float v1, p1, v0

    if-nez v1, :cond_7

    .line 451
    invoke-virtual {p0}, Lcom/my/target/core/controllers/a;->bq()V

    .line 452
    const/4 v1, 0x3

    iput v1, p0, Lcom/my/target/core/controllers/a;->state:I

    .line 453
    iput-boolean v5, p0, Lcom/my/target/core/controllers/a;->n:Z

    .line 454
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    if-eqz v1, :cond_6

    .line 456
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v1, v6}, Lcom/my/target/cv;->k(Z)V

    .line 458
    :cond_6
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->r:Lcom/my/target/core/controllers/a$c;

    if-eqz v1, :cond_7

    .line 460
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->r:Lcom/my/target/core/controllers/a$c;

    invoke-interface {v1}, Lcom/my/target/core/controllers/a$c;->p()V

    .line 468
    :cond_7
    return-void

    .end local p1    # "position":F
    .local v0, "position":F
    :cond_8
    move p2, v0

    move p1, v0

    .line 466
    .end local v0    # "position":F
    .restart local p1    # "position":F
    goto/16 :goto_0

    :cond_9
    move-object v2, v1

    goto :goto_1
.end method

.method public final bq()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 230
    iput-boolean v3, p0, Lcom/my/target/core/controllers/a;->p:Z

    .line 233
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->k:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_4

    .line 235
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/nativeads/views/MediaAdView;

    .line 237
    :goto_0
    if-eqz v0, :cond_1

    .line 239
    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getImageView()Landroid/widget/ImageView;

    move-result-object v1

    .line 240
    iget-object v2, p0, Lcom/my/target/core/controllers/a;->videoBanner:Lcom/my/target/aj;

    invoke-virtual {v2}, Lcom/my/target/aj;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v2

    .line 241
    if-eqz v2, :cond_0

    .line 243
    invoke-virtual {v2}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 245
    :cond_0
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 246
    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getPlayButtonView()Landroid/view/View;

    move-result-object v1

    .line 247
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 248
    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getProgressBarView()Landroid/widget/ProgressBar;

    move-result-object v1

    .line 249
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 250
    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    move-object v1, v0

    .line 252
    :cond_1
    iget-boolean v0, p0, Lcom/my/target/core/controllers/a;->o:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/my/target/core/controllers/a;->t:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    .line 254
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->t:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/cw;

    .line 255
    if-eqz v0, :cond_2

    .line 257
    invoke-virtual {v0}, Lcom/my/target/cw;->z()V

    .line 258
    invoke-virtual {v0}, Lcom/my/target/cw;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 261
    :cond_2
    if-eqz v1, :cond_3

    .line 263
    invoke-direct {p0, v1}, Lcom/my/target/core/controllers/a;->a(Landroid/content/Context;)V

    .line 265
    :cond_3
    return-void

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method public final br()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 290
    const/4 v0, 0x0

    .line 291
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->k:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    .line 293
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/nativeads/views/MediaAdView;

    .line 295
    :cond_0
    if-eqz v0, :cond_1

    .line 297
    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getImageView()Landroid/widget/ImageView;

    move-result-object v1

    .line 298
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 299
    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getProgressBarView()Landroid/widget/ProgressBar;

    move-result-object v1

    .line 300
    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 301
    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getPlayButtonView()Landroid/view/View;

    move-result-object v0

    .line 302
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 305
    :cond_1
    iget-boolean v0, p0, Lcom/my/target/core/controllers/a;->o:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/my/target/core/controllers/a;->t:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    .line 307
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->t:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/cw;

    .line 308
    if-eqz v0, :cond_2

    .line 310
    invoke-virtual {v0}, Lcom/my/target/cw;->C()V

    .line 313
    :cond_2
    return-void
.end method

.method public final bs()V
    .locals 0

    .prologue
    .line 319
    return-void
.end method

.method public final bt()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 324
    .line 326
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->k:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_6

    .line 328
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/nativeads/views/MediaAdView;

    move-object v2, v0

    .line 330
    :goto_0
    if-eqz v2, :cond_1

    .line 332
    invoke-virtual {v2}, Lcom/my/target/nativeads/views/MediaAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 333
    invoke-virtual {v2}, Lcom/my/target/nativeads/views/MediaAdView;->getImageView()Landroid/widget/ImageView;

    move-result-object v1

    .line 334
    iget-object v3, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v3}, Lcom/my/target/cv;->getScreenShot()Landroid/graphics/Bitmap;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 336
    iget-object v3, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v3}, Lcom/my/target/cv;->getScreenShot()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 346
    :cond_0
    :goto_1
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 347
    invoke-virtual {v2}, Lcom/my/target/nativeads/views/MediaAdView;->getPlayButtonView()Landroid/view/View;

    move-result-object v1

    .line 348
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 349
    invoke-virtual {v2}, Lcom/my/target/nativeads/views/MediaAdView;->getProgressBarView()Landroid/widget/ProgressBar;

    move-result-object v1

    .line 350
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    move-object v1, v0

    .line 352
    :cond_1
    iget-boolean v0, p0, Lcom/my/target/core/controllers/a;->o:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/my/target/core/controllers/a;->t:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    .line 354
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->t:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/cw;

    .line 355
    if-eqz v0, :cond_2

    .line 357
    invoke-virtual {v0}, Lcom/my/target/cw;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 358
    invoke-virtual {v0}, Lcom/my/target/cw;->B()V

    .line 361
    :cond_2
    if-eqz v2, :cond_3

    .line 363
    invoke-direct {p0, v1}, Lcom/my/target/core/controllers/a;->a(Landroid/content/Context;)V

    .line 365
    :cond_3
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->r:Lcom/my/target/core/controllers/a$c;

    if-eqz v0, :cond_4

    .line 367
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->r:Lcom/my/target/core/controllers/a$c;

    invoke-interface {v0}, Lcom/my/target/core/controllers/a$c;->o()V

    .line 369
    :cond_4
    return-void

    .line 340
    :cond_5
    iget-object v3, p0, Lcom/my/target/core/controllers/a;->videoBanner:Lcom/my/target/aj;

    invoke-virtual {v3}, Lcom/my/target/aj;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v3

    .line 341
    if-eqz v3, :cond_0

    .line 343
    invoke-virtual {v3}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    :cond_6
    move-object v2, v1

    goto :goto_0
.end method

.method public final bu()V
    .locals 3

    .prologue
    .line 374
    const/4 v0, 0x0

    .line 375
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->k:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    .line 377
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/nativeads/views/MediaAdView;

    .line 379
    :cond_0
    if-eqz v0, :cond_1

    .line 381
    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getProgressBarView()Landroid/widget/ProgressBar;

    move-result-object v1

    .line 382
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 383
    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getPlayButtonView()Landroid/view/View;

    move-result-object v0

    .line 384
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 387
    :cond_1
    iget-boolean v0, p0, Lcom/my/target/core/controllers/a;->o:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/my/target/core/controllers/a;->t:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    .line 389
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->t:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/cw;

    .line 390
    if-eqz v0, :cond_2

    .line 392
    invoke-virtual {v0}, Lcom/my/target/cw;->A()V

    .line 395
    :cond_2
    return-void
.end method

.method public final bv()V
    .locals 0

    .prologue
    .line 481
    return-void
.end method

.method public final e(F)V
    .locals 2
    .param p1, "volume"    # F

    .prologue
    .line 270
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->t:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->t:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/cw;

    .line 273
    if-eqz v0, :cond_0

    .line 275
    const/4 v1, 0x0

    cmpl-float v1, p1, v1

    if-lez v1, :cond_1

    .line 277
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/my/target/cw;->c(Z)V

    .line 285
    :cond_0
    :goto_0
    return-void

    .line 281
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/my/target/cw;->c(Z)V

    goto :goto_0
.end method

.method public final g()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 81
    const/4 v0, 0x0

    .line 82
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->k:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    .line 84
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/nativeads/views/MediaAdView;

    .line 86
    :cond_0
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getWindowVisibility()I

    move-result v1

    if-eqz v1, :cond_3

    .line 91
    iget-boolean v0, p0, Lcom/my/target/core/controllers/a;->o:Z

    if-eqz v0, :cond_2

    .line 93
    invoke-direct {p0, v3}, Lcom/my/target/core/controllers/a;->a(Z)V

    .line 125
    :cond_1
    :goto_0
    return-void

    .line 97
    :cond_2
    iput-boolean v3, p0, Lcom/my/target/core/controllers/a;->n:Z

    .line 98
    invoke-virtual {p0}, Lcom/my/target/core/controllers/a;->i()V

    goto :goto_0

    .line 104
    :cond_3
    iget-boolean v1, p0, Lcom/my/target/core/controllers/a;->n:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/my/target/core/controllers/a;->o:Z

    if-nez v1, :cond_1

    iget v1, p0, Lcom/my/target/core/controllers/a;->state:I

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/my/target/core/controllers/a;->state:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_4

    iget v1, p0, Lcom/my/target/core/controllers/a;->state:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    :cond_4
    if-eqz v0, :cond_1

    .line 109
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Handle visible, state = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/my/target/core/controllers/a;->state:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " url = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/my/target/core/controllers/a;->f:Lcom/my/target/common/models/VideoData;

    invoke-virtual {v2}, Lcom/my/target/common/models/VideoData;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 110
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    if-nez v1, :cond_5

    .line 112
    iput-boolean v3, p0, Lcom/my/target/core/controllers/a;->p:Z

    .line 113
    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/my/target/cv;->b(Lcom/my/target/core/controllers/a;Landroid/content/Context;)Lcom/my/target/cv;

    move-result-object v1

    iput-object v1, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    .line 114
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v1, p0}, Lcom/my/target/cv;->setVideoListener(Lcom/my/target/cc$a;)V

    .line 115
    iget-object v1, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v3, v2}, Lcom/my/target/nativeads/views/MediaAdView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 120
    :cond_5
    invoke-direct {p0}, Lcom/my/target/core/controllers/a;->bj()V

    .line 121
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    iget-object v1, p0, Lcom/my/target/core/controllers/a;->f:Lcom/my/target/common/models/VideoData;

    invoke-virtual {v0, v1, v3}, Lcom/my/target/cv;->a(Lcom/my/target/common/models/VideoData;Z)V

    .line 123
    const/4 v0, 0x1

    iput v0, p0, Lcom/my/target/core/controllers/a;->state:I

    goto :goto_0
.end method

.method public final h()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 179
    const-string v0, "Call release texture view on "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 180
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v0, v2}, Lcom/my/target/cv;->setVideoListener(Lcom/my/target/cc$a;)V

    .line 183
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v0}, Lcom/my/target/cv;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v0}, Lcom/my/target/cv;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 188
    :cond_0
    invoke-virtual {p0}, Lcom/my/target/core/controllers/a;->bq()V

    .line 189
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/core/controllers/a;->p:Z

    .line 190
    iput-object v2, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    .line 191
    invoke-static {p0}, Lcom/my/target/cv;->o(Lcom/my/target/core/controllers/a;)V

    .line 192
    return-void
.end method

.method public final i()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x1

    .line 196
    iget-boolean v0, p0, Lcom/my/target/core/controllers/a;->o:Z

    if-nez v0, :cond_0

    .line 198
    iget v0, p0, Lcom/my/target/core/controllers/a;->state:I

    if-ne v0, v1, :cond_2

    .line 200
    iget-boolean v0, p0, Lcom/my/target/core/controllers/a;->n:Z

    if-eqz v0, :cond_1

    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Handle invisible, state = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/my/target/core/controllers/a;->state:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " obj = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 203
    const/4 v0, 0x2

    iput v0, p0, Lcom/my/target/core/controllers/a;->state:I

    .line 204
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v0}, Lcom/my/target/cv;->bm()V

    .line 207
    iput v2, p0, Lcom/my/target/core/controllers/a;->state:I

    .line 225
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    invoke-virtual {p0}, Lcom/my/target/core/controllers/a;->bq()V

    .line 213
    const/4 v0, 0x3

    iput v0, p0, Lcom/my/target/core/controllers/a;->state:I

    .line 214
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    invoke-virtual {v0, v1}, Lcom/my/target/cv;->k(Z)V

    goto :goto_0

    .line 220
    :cond_2
    iget v0, p0, Lcom/my/target/core/controllers/a;->state:I

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/my/target/core/controllers/a;->state:I

    if-eqz v0, :cond_0

    .line 222
    invoke-virtual {p0}, Lcom/my/target/core/controllers/a;->bq()V

    goto :goto_0
.end method

.method public final i(Z)V
    .locals 0

    .prologue
    .line 534
    return-void
.end method

.method public final unregister()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 156
    const-string v0, "unregister from "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 158
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->k:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_3

    .line 160
    iget-object v0, p0, Lcom/my/target/core/controllers/a;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/nativeads/views/MediaAdView;

    .line 162
    :goto_0
    iget-boolean v2, p0, Lcom/my/target/core/controllers/a;->o:Z

    if-nez v2, :cond_2

    .line 164
    invoke-virtual {p0}, Lcom/my/target/core/controllers/a;->i()V

    .line 165
    iget-object v2, p0, Lcom/my/target/core/controllers/a;->l:Lcom/my/target/cv;

    if-eqz v2, :cond_0

    .line 167
    invoke-virtual {p0}, Lcom/my/target/core/controllers/a;->h()V

    .line 169
    :cond_0
    if-eqz v0, :cond_1

    .line 171
    invoke-virtual {v0, v1}, Lcom/my/target/nativeads/views/MediaAdView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 173
    :cond_1
    iput-object v1, p0, Lcom/my/target/core/controllers/a;->k:Ljava/lang/ref/WeakReference;

    .line 175
    :cond_2
    return-void

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method
