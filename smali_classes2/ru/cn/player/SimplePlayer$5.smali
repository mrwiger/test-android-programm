.class Lru/cn/player/SimplePlayer$5;
.super Ljava/lang/Object;
.source "SimplePlayer.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/player/SimplePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/player/SimplePlayer;


# direct methods
.method constructor <init>(Lru/cn/player/SimplePlayer;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/player/SimplePlayer;

    .prologue
    .line 429
    iput-object p1, p0, Lru/cn/player/SimplePlayer$5;->this$0:Lru/cn/player/SimplePlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 431
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 434
    iget-object v0, p0, Lru/cn/player/SimplePlayer$5;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v0, p1}, Lru/cn/player/SimplePlayer;->access$1002(Lru/cn/player/SimplePlayer;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    .line 435
    iget-object v0, p0, Lru/cn/player/SimplePlayer$5;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v0}, Lru/cn/player/SimplePlayer;->access$300(Lru/cn/player/SimplePlayer;)Lru/cn/player/AbstractMediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 436
    iget-object v0, p0, Lru/cn/player/SimplePlayer$5;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v0}, Lru/cn/player/SimplePlayer;->access$300(Lru/cn/player/SimplePlayer;)Lru/cn/player/AbstractMediaPlayer;

    move-result-object v0

    iget-object v1, p0, Lru/cn/player/SimplePlayer$5;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v1}, Lru/cn/player/SimplePlayer;->access$1000(Lru/cn/player/SimplePlayer;)Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/cn/player/AbstractMediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 438
    :cond_0
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    const/4 v1, 0x0

    .line 441
    iget-object v0, p0, Lru/cn/player/SimplePlayer$5;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v0, v1}, Lru/cn/player/SimplePlayer;->access$1002(Lru/cn/player/SimplePlayer;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    .line 442
    iget-object v0, p0, Lru/cn/player/SimplePlayer$5;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v0}, Lru/cn/player/SimplePlayer;->access$300(Lru/cn/player/SimplePlayer;)Lru/cn/player/AbstractMediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 443
    iget-object v0, p0, Lru/cn/player/SimplePlayer$5;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v0}, Lru/cn/player/SimplePlayer;->access$300(Lru/cn/player/SimplePlayer;)Lru/cn/player/AbstractMediaPlayer;

    move-result-object v0

    invoke-virtual {v0, v1}, Lru/cn/player/AbstractMediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 445
    :cond_0
    return-void
.end method
