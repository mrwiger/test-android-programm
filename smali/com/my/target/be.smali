.class public Lcom/my/target/be;
.super Ljava/lang/Object;
.source "CommonBannerParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/be$a;
    }
.end annotation


# instance fields
.field private aB:Ljava/lang/String;

.field private final adConfig:Lcom/my/target/b;

.field private final bJ:Landroid/content/Context;

.field private final eB:Lcom/my/target/ak;

.field private final eC:Lcom/my/target/ae;


# direct methods
.method private constructor <init>(Lcom/my/target/ak;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, Lcom/my/target/be;->eB:Lcom/my/target/ak;

    .line 73
    iput-object p2, p0, Lcom/my/target/be;->eC:Lcom/my/target/ae;

    .line 74
    iput-object p3, p0, Lcom/my/target/be;->adConfig:Lcom/my/target/b;

    .line 75
    iput-object p4, p0, Lcom/my/target/be;->bJ:Landroid/content/Context;

    .line 76
    return-void
.end method

.method private a(Lorg/json/JSONObject;Ljava/lang/String;F)Lcom/my/target/ap;
    .locals 6

    .prologue
    const/high16 v5, 0x42c80000    # 100.0f

    const/4 v4, 0x0

    .line 346
    invoke-static {p2}, Lcom/my/target/ap;->v(Ljava/lang/String;)Lcom/my/target/ap;

    move-result-object v0

    .line 348
    const-string v1, "pvalue"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 350
    const-string v1, "pvalue"

    invoke-virtual {v0}, Lcom/my/target/ap;->aa()F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 352
    cmpl-float v2, v1, v4

    if-ltz v2, :cond_0

    cmpg-float v2, v1, v5

    if-gtz v2, :cond_0

    .line 354
    mul-float/2addr v1, p3

    div-float/2addr v1, v5

    invoke-virtual {v0, v1}, Lcom/my/target/ap;->c(F)V

    .line 355
    const-string v1, ""

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 370
    :goto_0
    return-object v0

    .line 360
    :cond_0
    const-string v1, "value"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 362
    const-string v1, "value"

    invoke-virtual {v0}, Lcom/my/target/ap;->Z()F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 363
    cmpl-float v2, v1, v4

    if-ltz v2, :cond_1

    .line 365
    invoke-virtual {v0, v1}, Lcom/my/target/ap;->c(F)V

    goto :goto_0

    .line 370
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/my/target/ak;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/be;
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/my/target/be;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/my/target/be;-><init>(Lcom/my/target/ak;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V

    return-object v0
.end method

.method private a(Lcom/my/target/ah;Lorg/json/JSONObject;)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 246
    const-string v0, "statistics"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 247
    if-nez v3, :cond_1

    .line 288
    :cond_0
    :goto_0
    return-void

    .line 252
    :cond_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    .line 253
    if-lez v4, :cond_0

    move v2, v1

    .line 257
    :goto_1
    if-ge v2, v4, :cond_0

    .line 259
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 261
    const-string v0, "type"

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 262
    const-string v0, "url"

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 263
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 265
    :cond_2
    const-string v0, "Required field"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "failed to parse stat "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/my/target/be;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 269
    :cond_3
    invoke-virtual {p1}, Lcom/my/target/ah;->getStatHolder()Lcom/my/target/ar;

    move-result-object v8

    .line 270
    invoke-virtual {p1}, Lcom/my/target/ah;->getDuration()F

    move-result v9

    .line 271
    const/4 v0, -0x1

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_4
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 284
    invoke-virtual {v8, v6, v7}, Lcom/my/target/ar;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    :cond_5
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 271
    :sswitch_0
    const-string v10, "playheadReachedValue"

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    move v0, v1

    goto :goto_2

    :sswitch_1
    const-string v10, "playheadViewabilityValue"

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    const/4 v0, 0x1

    goto :goto_2

    .line 274
    :pswitch_0
    invoke-direct {p0, v5, v7, v9}, Lcom/my/target/be;->a(Lorg/json/JSONObject;Ljava/lang/String;F)Lcom/my/target/ap;

    move-result-object v0

    .line 275
    if-eqz v0, :cond_5

    .line 277
    invoke-virtual {v8, v0}, Lcom/my/target/ar;->a(Lcom/my/target/ap;)V

    goto :goto_3

    .line 281
    :pswitch_1
    invoke-direct {p0, v5, v7, v9, v8}, Lcom/my/target/be;->a(Lorg/json/JSONObject;Ljava/lang/String;FLcom/my/target/ar;)V

    goto :goto_3

    .line 271
    :sswitch_data_0
    .sparse-switch
        0x63803cc0 -> :sswitch_1
        0x6a94c473 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Lorg/json/JSONObject;Ljava/lang/String;FLcom/my/target/ar;)V
    .locals 6

    .prologue
    const/high16 v5, 0x42c80000    # 100.0f

    const/4 v4, 0x0

    .line 292
    const-string v0, "viewablePercent"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    .line 293
    if-ltz v0, :cond_0

    const/16 v1, 0x64

    if-le v0, v1, :cond_1

    .line 295
    :cond_0
    const-string v0, "Bad value"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "failed to parse viewAbilityStat "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/my/target/be;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    :goto_0
    return-void

    .line 299
    :cond_1
    const-string v1, "ovv"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 301
    invoke-static {p2}, Lcom/my/target/ao;->u(Ljava/lang/String;)Lcom/my/target/ao;

    move-result-object v1

    .line 302
    invoke-virtual {v1, v0}, Lcom/my/target/ao;->g(I)V

    .line 303
    const-string v0, "ovv"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/my/target/ao;->f(Z)V

    .line 305
    const-string v0, "pvalue"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 307
    const-string v0, "pvalue"

    invoke-virtual {v1}, Lcom/my/target/ao;->aa()F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v2

    double-to-float v0, v2

    .line 309
    cmpl-float v2, v0, v4

    if-ltz v2, :cond_2

    cmpg-float v2, v0, v5

    if-gtz v2, :cond_2

    .line 311
    mul-float/2addr v0, p3

    div-float/2addr v0, v5

    invoke-virtual {v1, v0}, Lcom/my/target/ao;->c(F)V

    .line 312
    invoke-virtual {p4, v1}, Lcom/my/target/ar;->a(Lcom/my/target/ao;)V

    goto :goto_0

    .line 317
    :cond_2
    const-string v0, "value"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 319
    const-string v0, "value"

    invoke-virtual {v1}, Lcom/my/target/ao;->Z()F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v2

    double-to-float v0, v2

    .line 320
    cmpl-float v2, v0, v4

    if-ltz v2, :cond_4

    .line 322
    invoke-virtual {v1, v0}, Lcom/my/target/ao;->c(F)V

    .line 323
    invoke-virtual {p4, v1}, Lcom/my/target/ar;->a(Lcom/my/target/ao;)V

    goto :goto_0

    .line 328
    :cond_3
    const-string v1, "duration"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 330
    invoke-static {p2}, Lcom/my/target/an;->t(Ljava/lang/String;)Lcom/my/target/an;

    move-result-object v1

    .line 331
    invoke-virtual {v1, v0}, Lcom/my/target/an;->g(I)V

    .line 332
    const-string v0, "duration"

    invoke-virtual {v1}, Lcom/my/target/an;->getDuration()F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v2

    double-to-float v0, v2

    .line 333
    cmpl-float v2, v0, v4

    if-ltz v2, :cond_4

    .line 335
    invoke-virtual {v1, v0}, Lcom/my/target/an;->setDuration(F)V

    .line 336
    invoke-virtual {p4, v1}, Lcom/my/target/ar;->a(Lcom/my/target/an;)V

    goto/16 :goto_0

    .line 341
    :cond_4
    const-string v0, "Bad value"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "failed to parse viewAbilityStat "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/my/target/be;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private f(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 375
    invoke-static {p1}, Lcom/my/target/az;->y(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/my/target/az;->z(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/be;->adConfig:Lcom/my/target/b;

    .line 376
    invoke-virtual {v1}, Lcom/my/target/b;->getSlotId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/az;->h(I)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/be;->aB:Ljava/lang/String;

    .line 377
    invoke-virtual {v0, v1}, Lcom/my/target/az;->B(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/be;->eC:Lcom/my/target/ae;

    .line 378
    invoke-virtual {v1}, Lcom/my/target/ae;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/az;->A(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/be;->bJ:Landroid/content/Context;

    .line 379
    invoke-virtual {v0, v1}, Lcom/my/target/az;->e(Landroid/content/Context;)V

    .line 380
    return-void
.end method

.method public static g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 45
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 47
    const-string v1, "<script\\s+[^>]*\\bsrc\\s*=\\s*(\\\\?[\\\"\\\'])mraid\\.js\\1[^>]*>\\s*<\\/script>\\n*"

    .line 48
    const/4 v2, 0x2

    invoke-static {v1, v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 49
    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 50
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 52
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v2

    .line 53
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 54
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<script src=\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\"></script>"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 58
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;Lcom/my/target/ah;Lcom/my/target/af;)V
    .locals 6

    .prologue
    .line 80
    const-string v0, "id"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/be;->aB:Ljava/lang/String;

    .line 81
    iget-object v0, p0, Lcom/my/target/be;->aB:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    const-string v0, "bannerID"

    invoke-virtual {p2}, Lcom/my/target/ah;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/be;->aB:Ljava/lang/String;

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/my/target/be;->aB:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setId(Ljava/lang/String;)V

    .line 87
    const-string v0, "type"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 88
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 90
    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setType(Ljava/lang/String;)V

    .line 93
    :cond_1
    const-string v0, "timeout"

    invoke-virtual {p2}, Lcom/my/target/ah;->getTimeout()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setTimeout(I)V

    .line 94
    const-string v0, "width"

    invoke-virtual {p2}, Lcom/my/target/ah;->getWidth()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setWidth(I)V

    .line 95
    const-string v0, "height"

    invoke-virtual {p2}, Lcom/my/target/ah;->getHeight()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setHeight(I)V

    .line 97
    const-string v0, "ageRestrictions"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 98
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 100
    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setAgeRestrictions(Ljava/lang/String;)V

    .line 103
    :cond_2
    const-string v0, "deeplink"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 104
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 106
    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setDeeplink(Ljava/lang/String;)V

    .line 108
    :cond_3
    const-string v0, "trackingLink"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 109
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 111
    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setTrackingLink(Ljava/lang/String;)V

    .line 113
    :cond_4
    const-string v0, "bundle_id"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 114
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 116
    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setBundleId(Ljava/lang/String;)V

    .line 118
    :cond_5
    const-string v0, "urlscheme"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 119
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 121
    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setUrlscheme(Ljava/lang/String;)V

    .line 124
    :cond_6
    const-string v0, "openInBrowser"

    invoke-virtual {p2}, Lcom/my/target/ah;->isOpenInBrowser()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setOpenInBrowser(Z)V

    .line 125
    const-string v0, "usePlayStoreAction"

    invoke-virtual {p2}, Lcom/my/target/ah;->isUsePlayStoreAction()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setUsePlayStoreAction(Z)V

    .line 126
    const-string v0, "directLink"

    invoke-virtual {p2}, Lcom/my/target/ah;->isDirectLink()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setDirectLink(Z)V

    .line 128
    const-string v0, "navigationType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 129
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 131
    const-string v1, "deeplink"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 133
    const-string v0, "store"

    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setNavigationType(Ljava/lang/String;)V

    .line 140
    :cond_7
    :goto_0
    const-string v0, "title"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 141
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 143
    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setTitle(Ljava/lang/String;)V

    .line 145
    :cond_8
    const-string v0, "description"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 148
    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setDescription(Ljava/lang/String;)V

    .line 150
    :cond_9
    const-string v0, "disclaimer"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 151
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 153
    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setDisclaimer(Ljava/lang/String;)V

    .line 155
    :cond_a
    const-string v0, "votes"

    invoke-virtual {p2}, Lcom/my/target/ah;->getVotes()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setVotes(I)V

    .line 156
    const-string v0, "category"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 157
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 159
    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setCategory(Ljava/lang/String;)V

    .line 161
    :cond_b
    const-string v0, "subcategory"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 162
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 164
    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setSubCategory(Ljava/lang/String;)V

    .line 166
    :cond_c
    const-string v0, "domain"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 167
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 169
    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setDomain(Ljava/lang/String;)V

    .line 171
    :cond_d
    const-string v0, "duration"

    invoke-virtual {p2}, Lcom/my/target/ah;->getDuration()F

    move-result v1

    float-to-double v2, v1

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setDuration(F)V

    .line 172
    iget-object v0, p0, Lcom/my/target/be;->eB:Lcom/my/target/ak;

    invoke-virtual {v0}, Lcom/my/target/ak;->getAdvertisingLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setAdvertisingLabel(Ljava/lang/String;)V

    .line 173
    const-string v0, "rating"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 175
    const-string v0, "rating"

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 176
    float-to-double v2, v0

    const-wide/high16 v4, 0x4014000000000000L    # 5.0

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_12

    float-to-double v2, v0

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_12

    .line 178
    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setRating(F)V

    .line 186
    :cond_e
    :goto_1
    const-string v0, "ctaText"

    invoke-virtual {p2}, Lcom/my/target/ah;->getCtaText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setCtaText(Ljava/lang/String;)V

    .line 188
    const-string v0, "iconLink"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 189
    const-string v1, "iconWidth"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    .line 190
    const-string v2, "iconHeight"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    .line 191
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_f

    .line 193
    invoke-static {v0, v1, v2}, Lcom/my/target/common/models/ImageData;->newImageData(Ljava/lang/String;II)Lcom/my/target/common/models/ImageData;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setIcon(Lcom/my/target/common/models/ImageData;)V

    .line 196
    :cond_f
    const-string v0, "imageLink"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 197
    const-string v1, "imageWidth"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    .line 198
    const-string v2, "imageHeight"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    .line 199
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_10

    .line 201
    invoke-static {v0, v1, v2}, Lcom/my/target/common/models/ImageData;->newImageData(Ljava/lang/String;II)Lcom/my/target/common/models/ImageData;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setImage(Lcom/my/target/common/models/ImageData;)V

    .line 204
    :cond_10
    if-eqz p3, :cond_13

    .line 205
    :goto_2
    const-string v0, "clickArea"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 207
    const-string v0, "clickArea"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    .line 208
    if-gtz v0, :cond_14

    .line 210
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad ClickArea mask "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 211
    const-string v1, "Bad value"

    invoke-direct {p0, v1, v0}, Lcom/my/target/be;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    invoke-virtual {p2, p3}, Lcom/my/target/ah;->setClickArea(Lcom/my/target/af;)V

    .line 225
    :goto_3
    invoke-direct {p0, p2, p1}, Lcom/my/target/be;->a(Lcom/my/target/ah;Lorg/json/JSONObject;)V

    .line 226
    return-void

    .line 137
    :cond_11
    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setNavigationType(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 182
    :cond_12
    const-string v1, "Bad value"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unable to parse rating "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/my/target/be;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 204
    :cond_13
    iget-object v0, p0, Lcom/my/target/be;->eB:Lcom/my/target/ak;

    invoke-virtual {v0}, Lcom/my/target/ak;->getClickArea()Lcom/my/target/af;

    move-result-object p3

    goto :goto_2

    .line 217
    :cond_14
    invoke-static {v0}, Lcom/my/target/af;->d(I)Lcom/my/target/af;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/ah;->setClickArea(Lcom/my/target/af;)V

    goto :goto_3

    .line 222
    :cond_15
    invoke-virtual {p2, p3}, Lcom/my/target/ah;->setClickArea(Lcom/my/target/af;)V

    goto :goto_3
.end method

.method public a(Ljava/lang/String;Lorg/json/JSONObject;)Z
    .locals 2

    .prologue
    .line 230
    const-string v0, "source"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 233
    :try_start_0
    const-string v0, "source"

    invoke-virtual {p2, v0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 234
    const/4 v0, 0x1

    .line 241
    :goto_0
    return v0

    .line 236
    :catch_0
    move-exception v0

    .line 239
    const-string v0, "Json error"

    const-string v1, "Unable to re-encode source of html banner"

    invoke-direct {p0, v0, v1}, Lcom/my/target/be;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    const/4 v0, 0x0

    goto :goto_0
.end method
