.class Lru/cn/tv/mobile/settings/MainPreferenceFragment$7;
.super Ljava/lang/Object;
.source "MainPreferenceFragment.java"

# interfaces
.implements Landroid/support/v7/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/mobile/settings/MainPreferenceFragment;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/mobile/settings/MainPreferenceFragment;

.field final synthetic val$qualityLevel:Landroid/support/v7/preference/ListPreference;


# direct methods
.method constructor <init>(Lru/cn/tv/mobile/settings/MainPreferenceFragment;Landroid/support/v7/preference/ListPreference;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/mobile/settings/MainPreferenceFragment;

    .prologue
    .line 149
    iput-object p1, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment$7;->this$0:Lru/cn/tv/mobile/settings/MainPreferenceFragment;

    iput-object p2, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment$7;->val$qualityLevel:Landroid/support/v7/preference/ListPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/support/v7/preference/Preference;Ljava/lang/Object;)Z
    .locals 3
    .param p1, "preference"    # Landroid/support/v7/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 152
    iget-object v1, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment$7;->val$qualityLevel:Landroid/support/v7/preference/ListPreference;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v0

    .line 153
    .local v0, "level":I
    iget-object v1, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment$7;->this$0:Lru/cn/tv/mobile/settings/MainPreferenceFragment;

    invoke-virtual {v1}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "quality_level"

    invoke-static {v1, v2, v0}, Lru/cn/domain/Preferences;->setInt(Landroid/content/Context;Ljava/lang/String;I)V

    .line 155
    iget-object v1, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment$7;->val$qualityLevel:Landroid/support/v7/preference/ListPreference;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 156
    iget-object v1, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment$7;->val$qualityLevel:Landroid/support/v7/preference/ListPreference;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 158
    const/4 v1, 0x0

    return v1
.end method
