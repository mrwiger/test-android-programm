.class public final Lru/cn/tv/stb/parental/PasswordDialogPresenter;
.super Ljava/lang/Object;
.source "PasswordDialogPresenter.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/stb/parental/PasswordDialogPresenter$PasswordDialogListener;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private final dialogListener:Lru/cn/tv/stb/parental/PasswordDialogPresenter$PasswordDialogListener;

.field private password:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lru/cn/tv/stb/parental/PasswordDialogPresenter$PasswordDialogListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dialogListener"    # Lru/cn/tv/stb/parental/PasswordDialogPresenter$PasswordDialogListener;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p2, p0, Lru/cn/tv/stb/parental/PasswordDialogPresenter;->dialogListener:Lru/cn/tv/stb/parental/PasswordDialogPresenter$PasswordDialogListener;

    .line 17
    iput-object p1, p0, Lru/cn/tv/stb/parental/PasswordDialogPresenter;->context:Landroid/content/Context;

    .line 18
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 41
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/stb/parental/PasswordDialogPresenter;->password:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 52
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 22
    packed-switch p2, :pswitch_data_0

    .line 37
    :goto_0
    :pswitch_0
    return-void

    .line 24
    :pswitch_1
    iget-object v0, p0, Lru/cn/tv/stb/parental/PasswordDialogPresenter;->context:Landroid/content/Context;

    iget-object v1, p0, Lru/cn/tv/stb/parental/PasswordDialogPresenter;->password:Ljava/lang/String;

    invoke-static {v0, v1}, Lru/cn/domain/KidsObject;->checkPassword(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p0, Lru/cn/tv/stb/parental/PasswordDialogPresenter;->dialogListener:Lru/cn/tv/stb/parental/PasswordDialogPresenter$PasswordDialogListener;

    invoke-interface {v0}, Lru/cn/tv/stb/parental/PasswordDialogPresenter$PasswordDialogListener;->onPasswordAccepted()V

    .line 26
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    .line 28
    :cond_0
    iget-object v0, p0, Lru/cn/tv/stb/parental/PasswordDialogPresenter;->dialogListener:Lru/cn/tv/stb/parental/PasswordDialogPresenter$PasswordDialogListener;

    invoke-interface {v0}, Lru/cn/tv/stb/parental/PasswordDialogPresenter$PasswordDialogListener;->onForgotPassword()V

    goto :goto_0

    .line 32
    :pswitch_2
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    .line 22
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 57
    return-void
.end method
