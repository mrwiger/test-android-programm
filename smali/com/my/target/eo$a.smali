.class final Lcom/my/target/eo$a;
.super Lcom/my/target/eq;
.source "CarouselTabletView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/eo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field private dc:F


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 450
    invoke-direct {p0, p1}, Lcom/my/target/eq;-><init>(Landroid/content/Context;)V

    .line 446
    const/high16 v0, 0x40200000    # 2.5f

    iput v0, p0, Lcom/my/target/eo$a;->dc:F

    .line 451
    return-void
.end method


# virtual methods
.method final a(F)V
    .locals 0

    .prologue
    .line 502
    iput p1, p0, Lcom/my/target/eo$a;->dc:F

    .line 503
    return-void
.end method

.method public final measureChildWithMargins(Landroid/view/View;II)V
    .locals 6
    .param p1, "child"    # Landroid/view/View;
    .param p2, "widthUsed"    # I
    .param p3, "heightUsed"    # I

    .prologue
    .line 456
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    .line 458
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->getWidth()I

    move-result v1

    .line 463
    int-to-float v1, v1

    iget v2, p0, Lcom/my/target/eo$a;->dc:F

    div-float/2addr v1, v2

    float-to-int v1, v1

    .line 465
    iget v2, p0, Lcom/my/target/eo$a;->dg:I

    .line 466
    iget v3, p0, Lcom/my/target/eo$a;->dn:I

    if-nez v3, :cond_0

    .line 468
    iput v2, p0, Lcom/my/target/eo$a;->dn:I

    .line 471
    :cond_0
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;->getItemViewType(Landroid/view/View;)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 473
    iget v3, p0, Lcom/my/target/eo$a;->dn:I

    iput v3, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    .line 474
    iput v2, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    .line 487
    :goto_0
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->getWidth()I

    move-result v2

    .line 488
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->getWidthMode()I

    move-result v3

    .line 489
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->getPaddingLeft()I

    move-result v4

    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->getPaddingRight()I

    move-result v5

    add-int/2addr v4, v5

    iget v5, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    add-int/2addr v4, v5

    iget v5, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    add-int/2addr v4, v5

    add-int/2addr v4, p2

    .line 491
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->canScrollHorizontally()Z

    move-result v5

    .line 487
    invoke-static {v2, v3, v4, v1, v5}, Landroid/support/v7/widget/RecyclerView$LayoutManager;->getChildMeasureSpec(IIIIZ)I

    move-result v1

    .line 492
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->getHeight()I

    move-result v2

    .line 493
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->getHeightMode()I

    move-result v3

    .line 494
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->getPaddingTop()I

    move-result v4

    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->getPaddingBottom()I

    move-result v5

    add-int/2addr v4, v5

    iget v5, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->topMargin:I

    add-int/2addr v4, v5

    iget v5, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->bottomMargin:I

    add-int/2addr v4, v5

    add-int/2addr v4, p3

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->height:I

    .line 496
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->canScrollVertically()Z

    move-result v5

    .line 492
    invoke-static {v2, v3, v4, v0, v5}, Landroid/support/v7/widget/RecyclerView$LayoutManager;->getChildMeasureSpec(IIIIZ)I

    move-result v0

    .line 497
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    .line 498
    return-void

    .line 476
    :cond_1
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;->getItemViewType(Landroid/view/View;)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 478
    iget v3, p0, Lcom/my/target/eo$a;->dn:I

    iput v3, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    .line 479
    iput v2, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    goto :goto_0

    .line 483
    :cond_2
    iput v2, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    .line 484
    iput v2, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    goto :goto_0
.end method
