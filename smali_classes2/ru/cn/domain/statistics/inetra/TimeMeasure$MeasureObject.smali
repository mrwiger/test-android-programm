.class final Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;
.super Ljava/lang/Object;
.source "TimeMeasure.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/domain/statistics/inetra/TimeMeasure;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "MeasureObject"
.end annotation


# instance fields
.field private endTime:J

.field final param:Ljava/lang/String;

.field final startTime:J


# direct methods
.method constructor <init>(Ljava/lang/String;J)V
    .locals 0
    .param p1, "param"    # Ljava/lang/String;
    .param p2, "startTime"    # J

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-wide p2, p0, Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;->startTime:J

    .line 15
    iput-object p1, p0, Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;->param:Ljava/lang/String;

    .line 16
    return-void
.end method


# virtual methods
.method setEndTime(J)V
    .locals 1
    .param p1, "endTime"    # J

    .prologue
    .line 23
    iput-wide p1, p0, Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;->endTime:J

    .line 24
    return-void
.end method

.method timeElapsed()J
    .locals 4

    .prologue
    .line 19
    iget-wide v0, p0, Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;->endTime:J

    iget-wide v2, p0, Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;->startTime:J

    sub-long/2addr v0, v2

    return-wide v0
.end method
