.class Lru/cn/ad/natives/adapters/FacebookNativeAdapter$1;
.super Ljava/lang/Object;
.source "FacebookNativeAdapter.java"

# interfaces
.implements Lcom/facebook/ads/AdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->onLoad()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/ad/natives/adapters/FacebookNativeAdapter;


# direct methods
.method constructor <init>(Lru/cn/ad/natives/adapters/FacebookNativeAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/ad/natives/adapters/FacebookNativeAdapter;

    .prologue
    .line 36
    iput-object p1, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$1;->this$0:Lru/cn/ad/natives/adapters/FacebookNativeAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAdClicked(Lcom/facebook/ads/Ad;)V
    .locals 3
    .param p1, "ad"    # Lcom/facebook/ads/Ad;

    .prologue
    .line 52
    iget-object v0, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$1;->this$0:Lru/cn/ad/natives/adapters/FacebookNativeAdapter;

    sget-object v1, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_CLICK:Lru/cn/domain/statistics/inetra/AdvEvent;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/util/Map;)V

    .line 54
    iget-object v0, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$1;->this$0:Lru/cn/ad/natives/adapters/FacebookNativeAdapter;

    invoke-static {v0}, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->access$400(Lru/cn/ad/natives/adapters/FacebookNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$1;->this$0:Lru/cn/ad/natives/adapters/FacebookNativeAdapter;

    invoke-static {v0}, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->access$500(Lru/cn/ad/natives/adapters/FacebookNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdEnded()V

    .line 56
    :cond_0
    return-void
.end method

.method public onAdLoaded(Lcom/facebook/ads/Ad;)V
    .locals 1
    .param p1, "ad"    # Lcom/facebook/ads/Ad;

    .prologue
    .line 45
    iget-object v0, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$1;->this$0:Lru/cn/ad/natives/adapters/FacebookNativeAdapter;

    invoke-static {v0}, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->access$200(Lru/cn/ad/natives/adapters/FacebookNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$1;->this$0:Lru/cn/ad/natives/adapters/FacebookNativeAdapter;

    invoke-static {v0}, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->access$300(Lru/cn/ad/natives/adapters/FacebookNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdLoaded()V

    .line 47
    :cond_0
    return-void
.end method

.method public onError(Lcom/facebook/ads/Ad;Lcom/facebook/ads/AdError;)V
    .locals 1
    .param p1, "ad"    # Lcom/facebook/ads/Ad;
    .param p2, "adError"    # Lcom/facebook/ads/AdError;

    .prologue
    .line 39
    iget-object v0, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$1;->this$0:Lru/cn/ad/natives/adapters/FacebookNativeAdapter;

    invoke-static {v0}, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->access$000(Lru/cn/ad/natives/adapters/FacebookNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$1;->this$0:Lru/cn/ad/natives/adapters/FacebookNativeAdapter;

    invoke-static {v0}, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->access$100(Lru/cn/ad/natives/adapters/FacebookNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onError()V

    .line 41
    :cond_0
    return-void
.end method

.method public onLoggingImpression(Lcom/facebook/ads/Ad;)V
    .locals 0
    .param p1, "ad"    # Lcom/facebook/ads/Ad;

    .prologue
    .line 61
    return-void
.end method
