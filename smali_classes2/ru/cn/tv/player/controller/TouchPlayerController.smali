.class public Lru/cn/tv/player/controller/TouchPlayerController;
.super Lru/cn/tv/player/controller/PlayerController;
.source "TouchPlayerController.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field private audioManager:Landroid/media/AudioManager;

.field private enableTouches:Z

.field private fitToButton:Landroid/widget/ImageButton;

.field private fullScreenButton:Landroid/widget/ImageButton;

.field private gestureDetector:Landroid/view/GestureDetector;

.field private final gestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

.field private handler:Landroid/os/Handler;

.field private isFullscreen:Z

.field public minimizeButton:Landroid/widget/ImageButton;

.field public playerMoreMenu:Landroid/widget/ImageButton;

.field private swipeOverlay:Landroid/widget/TextView;

.field private titleView:Landroid/widget/TextView;

.field private volumeGradesPerHeight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 44
    invoke-direct {p0, p1, p2}, Lru/cn/tv/player/controller/PlayerController;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    iput-boolean v2, p0, Lru/cn/tv/player/controller/TouchPlayerController;->isFullscreen:Z

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->enableTouches:Z

    .line 41
    const/4 v0, 0x6

    iput v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->volumeGradesPerHeight:I

    .line 198
    new-instance v0, Lru/cn/tv/player/controller/TouchPlayerController$3;

    invoke-direct {v0, p0}, Lru/cn/tv/player/controller/TouchPlayerController$3;-><init>(Lru/cn/tv/player/controller/TouchPlayerController;)V

    iput-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->gestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    .line 45
    const/16 v0, 0x1388

    iput v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->hideTimeout:I

    .line 46
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->handler:Landroid/os/Handler;

    .line 48
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->audioManager:Landroid/media/AudioManager;

    .line 49
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->audioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    iput v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->volumeGradesPerHeight:I

    .line 51
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lru/cn/tv/player/controller/TouchPlayerController;->gestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->gestureDetector:Landroid/view/GestureDetector;

    .line 52
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->gestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, v2}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 53
    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/player/controller/TouchPlayerController;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/controller/TouchPlayerController;

    .prologue
    .line 20
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->fullScreenButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/tv/player/controller/TouchPlayerController;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/controller/TouchPlayerController;

    .prologue
    .line 20
    iget-boolean v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->isFullscreen:Z

    return v0
.end method

.method static synthetic access$200(Lru/cn/tv/player/controller/TouchPlayerController;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/player/controller/TouchPlayerController;

    .prologue
    .line 20
    invoke-direct {p0}, Lru/cn/tv/player/controller/TouchPlayerController;->onSwipeRight()V

    return-void
.end method

.method static synthetic access$300(Lru/cn/tv/player/controller/TouchPlayerController;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/player/controller/TouchPlayerController;

    .prologue
    .line 20
    invoke-direct {p0}, Lru/cn/tv/player/controller/TouchPlayerController;->onSwipeLeft()V

    return-void
.end method

.method static synthetic access$400(Lru/cn/tv/player/controller/TouchPlayerController;)I
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/controller/TouchPlayerController;

    .prologue
    .line 20
    iget v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->volumeGradesPerHeight:I

    return v0
.end method

.method static synthetic access$500(Lru/cn/tv/player/controller/TouchPlayerController;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/controller/TouchPlayerController;

    .prologue
    .line 20
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->audioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method private onSwipeLeft()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 339
    iget-object v4, p0, Lru/cn/tv/player/controller/TouchPlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v4}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->getDuration()I

    move-result v4

    int-to-long v0, v4

    .line 340
    .local v0, "duration":J
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-gtz v4, :cond_1

    .line 356
    :cond_0
    :goto_0
    return-void

    .line 343
    :cond_1
    iget-object v4, p0, Lru/cn/tv/player/controller/TouchPlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v4}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->getCurrentPosition()I

    move-result v3

    .line 344
    .local v3, "position":I
    add-int/lit16 v2, v3, -0x3a98

    .line 345
    .local v2, "nextPosition":I
    if-gez v2, :cond_2

    .line 346
    const/4 v2, 0x0

    .line 349
    :cond_2
    iget-object v4, p0, Lru/cn/tv/player/controller/TouchPlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v4, v2}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->seekTo(I)V

    .line 351
    invoke-virtual {p0}, Lru/cn/tv/player/controller/TouchPlayerController;->isShowing()Z

    move-result v4

    if-nez v4, :cond_0

    .line 352
    iget-object v4, p0, Lru/cn/tv/player/controller/TouchPlayerController;->swipeOverlay:Landroid/widget/TextView;

    invoke-virtual {p0}, Lru/cn/tv/player/controller/TouchPlayerController;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0e010f

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v9, [Ljava/lang/Object;

    const/16 v7, 0xf

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 353
    iget-object v4, p0, Lru/cn/tv/player/controller/TouchPlayerController;->swipeOverlay:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 354
    iget-object v4, p0, Lru/cn/tv/player/controller/TouchPlayerController;->handler:Landroid/os/Handler;

    const-wide/16 v6, 0x7d0

    invoke-virtual {v4, v9, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private onSwipeRight()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 359
    iget-object v4, p0, Lru/cn/tv/player/controller/TouchPlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v4}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->getDuration()I

    move-result v4

    int-to-long v0, v4

    .line 360
    .local v0, "duration":J
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-gtz v4, :cond_1

    .line 378
    :cond_0
    :goto_0
    return-void

    .line 364
    :cond_1
    iget-object v4, p0, Lru/cn/tv/player/controller/TouchPlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v4}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->getCurrentPosition()I

    move-result v3

    .line 365
    .local v3, "position":I
    add-int/lit16 v2, v3, 0x7530

    .line 366
    .local v2, "nextPosition":I
    int-to-long v4, v2

    cmp-long v4, v4, v0

    if-gtz v4, :cond_0

    .line 371
    iget-object v4, p0, Lru/cn/tv/player/controller/TouchPlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v4, v2}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->seekTo(I)V

    .line 373
    invoke-virtual {p0}, Lru/cn/tv/player/controller/TouchPlayerController;->isShowing()Z

    move-result v4

    if-nez v4, :cond_0

    .line 374
    iget-object v4, p0, Lru/cn/tv/player/controller/TouchPlayerController;->swipeOverlay:Landroid/widget/TextView;

    invoke-virtual {p0}, Lru/cn/tv/player/controller/TouchPlayerController;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0e0110

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v9, [Ljava/lang/Object;

    const/16 v7, 0x1e

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 375
    iget-object v4, p0, Lru/cn/tv/player/controller/TouchPlayerController;->swipeOverlay:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 376
    iget-object v4, p0, Lru/cn/tv/player/controller/TouchPlayerController;->handler:Landroid/os/Handler;

    const-wide/16 v6, 0x7d0

    invoke-virtual {v4, v9, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private setImmersive(Z)V
    .locals 1
    .param p1, "immersive"    # Z

    .prologue
    .line 332
    invoke-virtual {p0}, Lru/cn/tv/player/controller/TouchPlayerController;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lru/cn/tv/FullScreenActivity;

    .line 333
    .local v0, "activity":Lru/cn/tv/FullScreenActivity;
    if-eqz v0, :cond_0

    .line 334
    invoke-virtual {v0, p1}, Lru/cn/tv/FullScreenActivity;->enableImmersive(Z)V

    .line 336
    :cond_0
    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 100
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 101
    .local v0, "keyCode":I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v3

    if-nez v3, :cond_2

    .line 102
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 103
    .local v1, "uniqueDown":Z
    :goto_0
    const/16 v3, 0x4f

    if-eq v0, v3, :cond_0

    const/16 v3, 0x55

    if-eq v0, v3, :cond_0

    const/16 v3, 0x3e

    if-ne v0, v3, :cond_3

    .line 106
    :cond_0
    if-eqz v1, :cond_1

    .line 107
    invoke-virtual {p0}, Lru/cn/tv/player/controller/TouchPlayerController;->doPauseResume()V

    .line 112
    :cond_1
    :goto_1
    return v2

    .line 102
    .end local v1    # "uniqueDown":Z
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 112
    .restart local v1    # "uniqueDown":Z
    :cond_3
    invoke-super {p0, p1}, Lru/cn/tv/player/controller/PlayerController;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_1
.end method

.method protected getLayout()I
    .locals 1

    .prologue
    .line 139
    const v0, 0x7f0c00aa

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 319
    invoke-virtual {p1}, Landroid/os/Message;->getTarget()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/player/controller/TouchPlayerController;->handler:Landroid/os/Handler;

    if-ne v0, v1, :cond_0

    .line 320
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 327
    :cond_0
    invoke-super {p0, p1}, Lru/cn/tv/player/controller/PlayerController;->handleMessage(Landroid/os/Message;)Z

    move-result v0

    :goto_0
    return v0

    .line 322
    :pswitch_0
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->swipeOverlay:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 323
    const/4 v0, 0x1

    goto :goto_0

    .line 320
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 127
    invoke-super {p0}, Lru/cn/tv/player/controller/PlayerController;->hide()V

    .line 128
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->companion:Lru/cn/tv/player/controller/PlayerController$Companion;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->companion:Lru/cn/tv/player/controller/PlayerController$Companion;

    invoke-interface {v0}, Lru/cn/tv/player/controller/PlayerController$Companion;->show()V

    .line 132
    :cond_0
    iget-boolean v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->isFullscreen:Z

    if-eqz v0, :cond_1

    .line 133
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lru/cn/tv/player/controller/TouchPlayerController;->setImmersive(Z)V

    .line 135
    :cond_1
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 57
    invoke-super {p0}, Lru/cn/tv/player/controller/PlayerController;->onFinishInflate()V

    .line 59
    const v0, 0x7f0900e6

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/TouchPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->fullScreenButton:Landroid/widget/ImageButton;

    .line 60
    const v0, 0x7f0900d8

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/TouchPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->fitToButton:Landroid/widget/ImageButton;

    .line 61
    const v0, 0x7f09016b

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/TouchPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->minimizeButton:Landroid/widget/ImageButton;

    .line 62
    const v0, 0x7f09016c

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/TouchPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->playerMoreMenu:Landroid/widget/ImageButton;

    .line 63
    const v0, 0x7f090091

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/TouchPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->titleView:Landroid/widget/TextView;

    .line 65
    const v0, 0x7f0901c0

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/TouchPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->swipeOverlay:Landroid/widget/TextView;

    .line 67
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->buttonPrev:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->buttonPrev:Landroid/widget/ImageButton;

    new-instance v1, Lru/cn/tv/player/controller/TouchPlayerController$1;

    invoke-direct {v1, p0}, Lru/cn/tv/player/controller/TouchPlayerController$1;-><init>(Lru/cn/tv/player/controller/TouchPlayerController;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 81
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->buttonNext:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->buttonNext:Landroid/widget/ImageButton;

    new-instance v1, Lru/cn/tv/player/controller/TouchPlayerController$2;

    invoke-direct {v1, p0}, Lru/cn/tv/player/controller/TouchPlayerController$2;-><init>(Lru/cn/tv/player/controller/TouchPlayerController;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 95
    :cond_1
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->playerMoreMenu:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 96
    return-void
.end method

.method protected onTelecastChanged(Lru/cn/api/tv/replies/Telecast;)V
    .locals 2
    .param p1, "telecast"    # Lru/cn/api/tv/replies/Telecast;

    .prologue
    .line 149
    invoke-super {p0, p1}, Lru/cn/tv/player/controller/PlayerController;->onTelecastChanged(Lru/cn/api/tv/replies/Telecast;)V

    .line 150
    iget-object v1, p0, Lru/cn/tv/player/controller/TouchPlayerController;->titleView:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lru/cn/api/tv/replies/Telecast;->title:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    return-void

    .line 150
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 306
    iget-boolean v3, p0, Lru/cn/tv/player/controller/TouchPlayerController;->enableTouches:Z

    if-nez v3, :cond_1

    move v1, v2

    .line 314
    :cond_0
    :goto_0
    return v1

    .line 310
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    if-gt v3, v2, :cond_0

    .line 313
    iget-object v3, p0, Lru/cn/tv/player/controller/TouchPlayerController;->gestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v3, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 314
    .local v0, "handled":Z
    if-nez v0, :cond_2

    invoke-super {p0, p1}, Lru/cn/tv/player/controller/PlayerController;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public setFitMode(Lru/cn/player/SimplePlayer$FitMode;)V
    .locals 2
    .param p1, "mode"    # Lru/cn/player/SimplePlayer$FitMode;

    .prologue
    .line 183
    sget-object v0, Lru/cn/tv/player/controller/TouchPlayerController$4;->$SwitchMap$ru$cn$player$SimplePlayer$FitMode:[I

    invoke-virtual {p1}, Lru/cn/player/SimplePlayer$FitMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 192
    :goto_0
    return-void

    .line 185
    :pswitch_0
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->fitToButton:Landroid/widget/ImageButton;

    const v1, 0x7f0802f5

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    .line 189
    :pswitch_1
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->fitToButton:Landroid/widget/ImageButton;

    const v1, 0x7f0802f6

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    .line 183
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setFitModeClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 195
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->fitToButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    return-void
.end method

.method public setFullScreen(Z)V
    .locals 4
    .param p1, "fullScreen"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 167
    iput-boolean p1, p0, Lru/cn/tv/player/controller/TouchPlayerController;->isFullscreen:Z

    .line 168
    invoke-virtual {p0, p1}, Lru/cn/tv/player/controller/TouchPlayerController;->requestDisallowInterceptTouchEvent(Z)V

    .line 170
    iget-boolean v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->isFullscreen:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lru/cn/tv/player/controller/TouchPlayerController;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lru/cn/tv/player/controller/TouchPlayerController;->setImmersive(Z)V

    .line 174
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->fullScreenButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 176
    iget-object v3, p0, Lru/cn/tv/player/controller/TouchPlayerController;->fitToButton:Landroid/widget/ImageButton;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 177
    iget-object v3, p0, Lru/cn/tv/player/controller/TouchPlayerController;->titleView:Landroid/widget/TextView;

    if-eqz p1, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 179
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->minimizeButton:Landroid/widget/ImageButton;

    if-eqz p1, :cond_3

    :goto_2
    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 180
    return-void

    :cond_1
    move v0, v2

    .line 176
    goto :goto_0

    :cond_2
    move v0, v2

    .line 177
    goto :goto_1

    :cond_3
    move v2, v1

    .line 179
    goto :goto_2
.end method

.method public setMoreMenuClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "clickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 154
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->playerMoreMenu:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    return-void
.end method

.method public setOnFullScreenButtonListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 158
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->fullScreenButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    return-void
.end method

.method public setTouchesEnabled(Z)V
    .locals 0
    .param p1, "enableTouches"    # Z

    .prologue
    .line 163
    iput-boolean p1, p0, Lru/cn/tv/player/controller/TouchPlayerController;->enableTouches:Z

    .line 164
    return-void
.end method

.method protected shouldHideRootContainer()Z
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x0

    return v0
.end method

.method public show()V
    .locals 1

    .prologue
    .line 117
    invoke-super {p0}, Lru/cn/tv/player/controller/PlayerController;->show()V

    .line 118
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->companion:Lru/cn/tv/player/controller/PlayerController$Companion;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController;->companion:Lru/cn/tv/player/controller/PlayerController$Companion;

    invoke-interface {v0}, Lru/cn/tv/player/controller/PlayerController$Companion;->hide()V

    .line 122
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lru/cn/tv/player/controller/TouchPlayerController;->setImmersive(Z)V

    .line 123
    return-void
.end method
