.class public Lru/cn/api/authorization/CodeAuthorizationChallenge;
.super Ljava/lang/Object;
.source "CodeAuthorizationChallenge.java"


# instance fields
.field private final authApi:Lru/cn/api/authorization/retrofit/AuthApi;

.field private final authorizeUri:Landroid/net/Uri;

.field private final context:Landroid/content/Context;

.field private final contractorId:J

.field private final redirectUri:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lru/cn/api/authorization/retrofit/AuthApi;JLandroid/net/Uri;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "authApi"    # Lru/cn/api/authorization/retrofit/AuthApi;
    .param p3, "contractorId"    # J
    .param p5, "authorizeUri"    # Landroid/net/Uri;
    .param p6, "redirectUri"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lru/cn/api/authorization/CodeAuthorizationChallenge;->context:Landroid/content/Context;

    .line 21
    iput-object p2, p0, Lru/cn/api/authorization/CodeAuthorizationChallenge;->authApi:Lru/cn/api/authorization/retrofit/AuthApi;

    .line 23
    iput-object p6, p0, Lru/cn/api/authorization/CodeAuthorizationChallenge;->redirectUri:Ljava/lang/String;

    .line 24
    iput-wide p3, p0, Lru/cn/api/authorization/CodeAuthorizationChallenge;->contractorId:J

    .line 25
    iput-object p5, p0, Lru/cn/api/authorization/CodeAuthorizationChallenge;->authorizeUri:Landroid/net/Uri;

    .line 26
    return-void
.end method


# virtual methods
.method public authorizeUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lru/cn/api/authorization/CodeAuthorizationChallenge;->authorizeUri:Landroid/net/Uri;

    return-object v0
.end method

.method public proceedAuthorization(Ljava/lang/String;)Z
    .locals 7
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 33
    iget-object v1, p0, Lru/cn/api/authorization/CodeAuthorizationChallenge;->redirectUri:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 44
    :cond_0
    :goto_0
    return v0

    .line 36
    :cond_1
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 37
    .local v6, "responseUri":Landroid/net/Uri;
    if-eqz v6, :cond_0

    .line 40
    const-string v1, "code"

    invoke-virtual {v6, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 41
    .local v2, "code":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 44
    iget-object v0, p0, Lru/cn/api/authorization/CodeAuthorizationChallenge;->context:Landroid/content/Context;

    iget-object v1, p0, Lru/cn/api/authorization/CodeAuthorizationChallenge;->authApi:Lru/cn/api/authorization/retrofit/AuthApi;

    iget-object v3, p0, Lru/cn/api/authorization/CodeAuthorizationChallenge;->redirectUri:Ljava/lang/String;

    iget-wide v4, p0, Lru/cn/api/authorization/CodeAuthorizationChallenge;->contractorId:J

    invoke-static/range {v0 .. v5}, Lru/cn/api/authorization/Authorization;->completeCodeToken(Landroid/content/Context;Lru/cn/api/authorization/retrofit/AuthApi;Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v0

    goto :goto_0
.end method
