.class public Lcom/yandex/metrica/impl/ba;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/yandex/metrica/impl/ad$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ba$c;,
        Lcom/yandex/metrica/impl/ba$d;,
        Lcom/yandex/metrica/impl/ba$a;,
        Lcom/yandex/metrica/impl/ba$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/yandex/metrica/impl/t;

.field private final b:Lcom/yandex/metrica/impl/ad;

.field private final c:Ljava/lang/Object;

.field private final d:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(Lcom/yandex/metrica/impl/t;)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ba;->c:Ljava/lang/Object;

    .line 39
    new-instance v0, Lcom/yandex/metrica/impl/utils/j;

    const-string v1, "YMM-RS"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/utils/j;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ba;->d:Ljava/util/concurrent/ExecutorService;

    .line 43
    iput-object p1, p0, Lcom/yandex/metrica/impl/ba;->a:Lcom/yandex/metrica/impl/t;

    .line 45
    invoke-interface {p1}, Lcom/yandex/metrica/impl/t;->a()Lcom/yandex/metrica/impl/ad;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ba;->b:Lcom/yandex/metrica/impl/ad;

    .line 46
    iget-object v0, p0, Lcom/yandex/metrica/impl/ba;->b:Lcom/yandex/metrica/impl/ad;

    invoke-virtual {v0, p0}, Lcom/yandex/metrica/impl/ad;->a(Lcom/yandex/metrica/impl/ad$a;)V

    .line 47
    return-void
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/ba;)Lcom/yandex/metrica/impl/ad;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/yandex/metrica/impl/ba;->b:Lcom/yandex/metrica/impl/ad;

    return-object v0
.end method

.method static synthetic b(Lcom/yandex/metrica/impl/ba;)Lcom/yandex/metrica/impl/t;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/yandex/metrica/impl/ba;->a:Lcom/yandex/metrica/impl/t;

    return-object v0
.end method

.method static synthetic c(Lcom/yandex/metrica/impl/ba;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/yandex/metrica/impl/ba;->c:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic d(Lcom/yandex/metrica/impl/ba;)Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/yandex/metrica/impl/ba;->d:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/yandex/metrica/impl/ba$d;)Ljava/util/concurrent/Future;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/yandex/metrica/impl/ba$d;",
            ")",
            "Ljava/util/concurrent/Future",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 50
    iget-object v1, p0, Lcom/yandex/metrica/impl/ba;->d:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ba$d;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/yandex/metrica/impl/ba$a;

    invoke-direct {v0, p0, p1, v2}, Lcom/yandex/metrica/impl/ba$a;-><init>(Lcom/yandex/metrica/impl/ba;Lcom/yandex/metrica/impl/ba$d;B)V

    :goto_0
    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Lcom/yandex/metrica/impl/ba$b;

    invoke-direct {v0, p0, p1, v2}, Lcom/yandex/metrica/impl/ba$b;-><init>(Lcom/yandex/metrica/impl/ba;Lcom/yandex/metrica/impl/ba$d;B)V

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 55
    iget-object v1, p0, Lcom/yandex/metrica/impl/ba;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 56
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ba;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 57
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
