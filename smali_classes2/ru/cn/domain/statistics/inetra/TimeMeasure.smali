.class public final Lru/cn/domain/statistics/inetra/TimeMeasure;
.super Ljava/lang/Object;
.source "TimeMeasure.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;
    }
.end annotation


# instance fields
.field private final measureObjects:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;",
            ">;"
        }
    .end annotation
.end field

.field private totalTime:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/domain/statistics/inetra/TimeMeasure;->measureObjects:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public getMessage(Lru/cn/domain/statistics/inetra/TypeOfMeasure;)Ljava/lang/String;
    .locals 8
    .param p1, "typeOfMeasure"    # Lru/cn/domain/statistics/inetra/TypeOfMeasure;

    .prologue
    .line 64
    const-string v1, ""

    .line 65
    .local v1, "message":Ljava/lang/String;
    const/4 v2, 0x0

    .line 67
    .local v2, "prefix":Ljava/lang/String;
    sget-object v3, Lru/cn/domain/statistics/inetra/TimeMeasure$1;->$SwitchMap$ru$cn$domain$statistics$inetra$TypeOfMeasure:[I

    invoke-virtual {p1}, Lru/cn/domain/statistics/inetra/TypeOfMeasure;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 78
    :goto_0
    if-eqz v2, :cond_1

    .line 79
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 81
    iget-object v3, p0, Lru/cn/domain/statistics/inetra/TimeMeasure;->measureObjects:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;

    .line 82
    .local v0, "measureObject":Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;->param:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 83
    iget-object v4, p0, Lru/cn/domain/statistics/inetra/TimeMeasure;->measureObjects:Ljava/util/ArrayList;

    iget-object v5, p0, Lru/cn/domain/statistics/inetra/TimeMeasure;->measureObjects:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eq v4, v0, :cond_0

    .line 84
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 69
    .end local v0    # "measureObject":Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;
    :pswitch_0
    const-string v2, "content_url="

    .line 70
    goto :goto_0

    .line 74
    :pswitch_1
    const-string v2, "network="

    goto :goto_0

    .line 89
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 90
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 93
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "time="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 94
    iget-object v3, p0, Lru/cn/domain/statistics/inetra/TimeMeasure;->measureObjects:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;

    .line 95
    .restart local v0    # "measureObject":Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;->timeElapsed()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 97
    iget-object v4, p0, Lru/cn/domain/statistics/inetra/TimeMeasure;->measureObjects:Ljava/util/ArrayList;

    iget-object v5, p0, Lru/cn/domain/statistics/inetra/TimeMeasure;->measureObjects:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eq v4, v0, :cond_3

    .line 98
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 102
    .end local v0    # "measureObject":Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;
    :cond_4
    return-object v1

    .line 67
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public getTotalTime()J
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lru/cn/domain/statistics/inetra/TimeMeasure;->totalTime:J

    return-wide v0
.end method

.method public measureContinue(Ljava/lang/String;)V
    .locals 8
    .param p1, "newParam"    # Ljava/lang/String;

    .prologue
    .line 40
    iget-wide v4, p0, Lru/cn/domain/statistics/inetra/TimeMeasure;->totalTime:J

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_0

    .line 41
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v4, "Attempt to continue ended measurement"

    invoke-direct {v1, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 44
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 45
    .local v2, "now":J
    iget-object v1, p0, Lru/cn/domain/statistics/inetra/TimeMeasure;->measureObjects:Ljava/util/ArrayList;

    iget-object v4, p0, Lru/cn/domain/statistics/inetra/TimeMeasure;->measureObjects:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;

    .line 46
    .local v0, "measureObject":Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;
    invoke-virtual {v0, v2, v3}, Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;->setEndTime(J)V

    .line 48
    iget-object v1, p0, Lru/cn/domain/statistics/inetra/TimeMeasure;->measureObjects:Ljava/util/ArrayList;

    new-instance v4, Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;

    invoke-direct {v4, p1, v2, v3}, Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    return-void
.end method

.method public measureEnd()V
    .locals 6

    .prologue
    .line 52
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 53
    .local v2, "now":J
    iget-object v1, p0, Lru/cn/domain/statistics/inetra/TimeMeasure;->measureObjects:Ljava/util/ArrayList;

    iget-object v4, p0, Lru/cn/domain/statistics/inetra/TimeMeasure;->measureObjects:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;

    .line 54
    .local v0, "measureObject":Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;
    invoke-virtual {v0, v2, v3}, Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;->setEndTime(J)V

    .line 56
    iget-object v1, p0, Lru/cn/domain/statistics/inetra/TimeMeasure;->measureObjects:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;

    iget-wide v4, v1, Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;->startTime:J

    sub-long v4, v2, v4

    iput-wide v4, p0, Lru/cn/domain/statistics/inetra/TimeMeasure;->totalTime:J

    .line 57
    return-void
.end method

.method public measureStart(Ljava/lang/String;)V
    .locals 6
    .param p1, "param"    # Ljava/lang/String;

    .prologue
    .line 31
    iget-wide v2, p0, Lru/cn/domain/statistics/inetra/TimeMeasure;->totalTime:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 32
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Attempt to start ended measurement"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 35
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 36
    .local v0, "startTime":J
    iget-object v2, p0, Lru/cn/domain/statistics/inetra/TimeMeasure;->measureObjects:Ljava/util/ArrayList;

    new-instance v3, Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;

    invoke-direct {v3, p1, v0, v1}, Lru/cn/domain/statistics/inetra/TimeMeasure$MeasureObject;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    return-void
.end method
