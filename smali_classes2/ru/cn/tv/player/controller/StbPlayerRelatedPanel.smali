.class public Lru/cn/tv/player/controller/StbPlayerRelatedPanel;
.super Landroid/widget/FrameLayout;
.source "StbPlayerRelatedPanel.java"

# interfaces
.implements Lru/cn/view/CursorRecyclerViewAdapter$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/player/controller/StbPlayerRelatedPanel$Listener;
    }
.end annotation


# instance fields
.field private adapter:Lru/cn/tv/stb/collections/CollectionTelecastAdapter;

.field private listener:Lru/cn/tv/player/controller/StbPlayerRelatedPanel$Listener;

.field private relatedRecycleView:Landroid/support/v7/widget/RecyclerView;

.field private rubricId:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v4, 0x0

    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    const/4 v3, 0x0

    iput-object v3, p0, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;->listener:Lru/cn/tv/player/controller/StbPlayerRelatedPanel$Listener;

    .line 43
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 44
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f0c0087

    invoke-virtual {v0, v3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 46
    const v3, 0x7f09017c

    invoke-virtual {p0, v3}, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/RecyclerView;

    iput-object v3, p0, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;->relatedRecycleView:Landroid/support/v7/widget/RecyclerView;

    .line 48
    new-instance v2, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v2, p1, v4, v4}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 49
    .local v2, "layout":Landroid/support/v7/widget/LinearLayoutManager;
    iget-object v3, p0, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;->relatedRecycleView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 51
    invoke-static {p1}, Lru/cn/domain/KidsObject;->isKidsMode(Landroid/content/Context;)Z

    move-result v1

    .line 52
    .local v1, "kidsMode":Z
    new-instance v3, Lru/cn/tv/stb/collections/CollectionTelecastAdapter;

    const v4, 0x7f0c008f

    invoke-direct {v3, v4, v1}, Lru/cn/tv/stb/collections/CollectionTelecastAdapter;-><init>(IZ)V

    iput-object v3, p0, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;->adapter:Lru/cn/tv/stb/collections/CollectionTelecastAdapter;

    .line 53
    iget-object v3, p0, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;->relatedRecycleView:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, p0, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;->adapter:Lru/cn/tv/stb/collections/CollectionTelecastAdapter;

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 54
    iget-object v3, p0, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;->adapter:Lru/cn/tv/stb/collections/CollectionTelecastAdapter;

    invoke-virtual {v3, p0}, Lru/cn/tv/stb/collections/CollectionTelecastAdapter;->setOnItemClickListener(Lru/cn/view/CursorRecyclerViewAdapter$OnItemClickListener;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;->listener:Lru/cn/tv/player/controller/StbPlayerRelatedPanel$Listener;

    .line 60
    return-void
.end method


# virtual methods
.method public focusSearch(Landroid/view/View;I)Landroid/view/View;
    .locals 2
    .param p1, "focused"    # Landroid/view/View;
    .param p2, "direction"    # I

    .prologue
    .line 98
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;->relatedRecycleView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$Adapter;->getItemCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0x42

    if-ne p2, v0, :cond_0

    .line 102
    .end local p1    # "focused":Landroid/view/View;
    :goto_0
    return-object p1

    .restart local p1    # "focused":Landroid/view/View;
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->focusSearch(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    goto :goto_0
.end method

.method public onItemClick(Landroid/view/View;I)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 73
    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;->adapter:Lru/cn/tv/stb/collections/CollectionTelecastAdapter;

    invoke-virtual {v1, p2}, Lru/cn/tv/stb/collections/CollectionTelecastAdapter;->getItem(I)Landroid/database/Cursor;

    move-result-object v0

    .line 74
    .local v0, "itemCursor":Landroid/database/Cursor;
    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;->listener:Lru/cn/tv/player/controller/StbPlayerRelatedPanel$Listener;

    iget-wide v2, p0, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;->rubricId:J

    invoke-interface {v1, v2, v3, v0}, Lru/cn/tv/player/controller/StbPlayerRelatedPanel$Listener;->onItemSelected(JLandroid/database/Cursor;)V

    .line 75
    return-void
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .locals 3
    .param p1, "direction"    # I
    .param p2, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 88
    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;->relatedRecycleView:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 89
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 90
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    move-result v1

    .line 92
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;->relatedRecycleView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->requestFocus()Z

    move-result v1

    goto :goto_0
.end method

.method public setListener(Lru/cn/tv/player/controller/StbPlayerRelatedPanel$Listener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/tv/player/controller/StbPlayerRelatedPanel$Listener;

    .prologue
    .line 63
    iput-object p1, p0, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;->listener:Lru/cn/tv/player/controller/StbPlayerRelatedPanel$Listener;

    .line 64
    return-void
.end method

.method public setRelatedItems(JLandroid/database/Cursor;)V
    .locals 1
    .param p1, "rubricId"    # J
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 67
    iput-wide p1, p0, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;->rubricId:J

    .line 68
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;->adapter:Lru/cn/tv/stb/collections/CollectionTelecastAdapter;

    invoke-virtual {v0, p3}, Lru/cn/tv/stb/collections/CollectionTelecastAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 69
    return-void
.end method

.method public setVisibility(I)V
    .locals 2
    .param p1, "visibility"    # I

    .prologue
    .line 79
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 81
    const/16 v0, 0x8

    if-ne p1, v0, :cond_0

    .line 82
    iget-object v0, p0, Lru/cn/tv/player/controller/StbPlayerRelatedPanel;->relatedRecycleView:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->scrollToPosition(I)V

    .line 84
    :cond_0
    return-void
.end method
