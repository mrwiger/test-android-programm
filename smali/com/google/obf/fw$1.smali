.class Lcom/google/obf/fw$1;
.super Lcom/google/obf/fw$b;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/obf/fw;->a(Lcom/google/obf/eg;Ljava/lang/reflect/Field;Ljava/lang/String;Lcom/google/obf/gd;ZZ)Lcom/google/obf/fw$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/reflect/Field;

.field final synthetic b:Z

.field final synthetic c:Lcom/google/obf/ew;

.field final synthetic d:Lcom/google/obf/eg;

.field final synthetic e:Lcom/google/obf/gd;

.field final synthetic f:Z

.field final synthetic g:Lcom/google/obf/fw;


# direct methods
.method constructor <init>(Lcom/google/obf/fw;Ljava/lang/String;ZZLjava/lang/reflect/Field;ZLcom/google/obf/ew;Lcom/google/obf/eg;Lcom/google/obf/gd;Z)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/obf/fw$1;->g:Lcom/google/obf/fw;

    iput-object p5, p0, Lcom/google/obf/fw$1;->a:Ljava/lang/reflect/Field;

    iput-boolean p6, p0, Lcom/google/obf/fw$1;->b:Z

    iput-object p7, p0, Lcom/google/obf/fw$1;->c:Lcom/google/obf/ew;

    iput-object p8, p0, Lcom/google/obf/fw$1;->d:Lcom/google/obf/eg;

    iput-object p9, p0, Lcom/google/obf/fw$1;->e:Lcom/google/obf/gd;

    iput-boolean p10, p0, Lcom/google/obf/fw$1;->f:Z

    invoke-direct {p0, p2, p3, p4}, Lcom/google/obf/fw$b;-><init>(Ljava/lang/String;ZZ)V

    return-void
.end method


# virtual methods
.method a(Lcom/google/obf/ge;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalAccessException;
        }
    .end annotation

    .prologue
    .line 7
    iget-object v0, p0, Lcom/google/obf/fw$1;->c:Lcom/google/obf/ew;

    invoke-virtual {v0, p1}, Lcom/google/obf/ew;->read(Lcom/google/obf/ge;)Ljava/lang/Object;

    move-result-object v0

    .line 8
    if-nez v0, :cond_0

    iget-boolean v1, p0, Lcom/google/obf/fw$1;->f:Z

    if-nez v1, :cond_1

    .line 9
    :cond_0
    iget-object v1, p0, Lcom/google/obf/fw$1;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v1, p2, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 10
    :cond_1
    return-void
.end method

.method a(Lcom/google/obf/gg;Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalAccessException;
        }
    .end annotation

    .prologue
    .line 2
    iget-object v0, p0, Lcom/google/obf/fw$1;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 3
    iget-boolean v0, p0, Lcom/google/obf/fw$1;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/fw$1;->c:Lcom/google/obf/ew;

    .line 5
    :goto_0
    invoke-virtual {v0, p1, v1}, Lcom/google/obf/ew;->write(Lcom/google/obf/gg;Ljava/lang/Object;)V

    .line 6
    return-void

    .line 3
    :cond_0
    new-instance v0, Lcom/google/obf/ga;

    iget-object v2, p0, Lcom/google/obf/fw$1;->d:Lcom/google/obf/eg;

    iget-object v3, p0, Lcom/google/obf/fw$1;->c:Lcom/google/obf/ew;

    iget-object v4, p0, Lcom/google/obf/fw$1;->e:Lcom/google/obf/gd;

    .line 4
    invoke-virtual {v4}, Lcom/google/obf/gd;->b()Ljava/lang/reflect/Type;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, Lcom/google/obf/ga;-><init>(Lcom/google/obf/eg;Lcom/google/obf/ew;Ljava/lang/reflect/Type;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalAccessException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 11
    iget-boolean v1, p0, Lcom/google/obf/fw$1;->i:Z

    if-nez v1, :cond_1

    .line 13
    :cond_0
    :goto_0
    return v0

    .line 12
    :cond_1
    iget-object v1, p0, Lcom/google/obf/fw$1;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v1, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 13
    if-eq v1, p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
