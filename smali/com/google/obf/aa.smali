.class public final Lcom/google/obf/aa;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/aa$c;,
        Lcom/google/obf/aa$b;,
        Lcom/google/obf/aa$a;,
        Lcom/google/obf/aa$e;,
        Lcom/google/obf/aa$f;,
        Lcom/google/obf/aa$d;
    }
.end annotation


# static fields
.field public static a:Z

.field public static b:Z


# instance fields
.field private A:I

.field private B:I

.field private C:J

.field private D:J

.field private E:J

.field private F:F

.field private G:[B

.field private H:I

.field private I:I

.field private J:Ljava/nio/ByteBuffer;

.field private K:Z

.field private final c:Lcom/google/obf/z;

.field private final d:Landroid/os/ConditionVariable;

.field private final e:[J

.field private final f:Lcom/google/obf/aa$a;

.field private g:Landroid/media/AudioTrack;

.field private h:Landroid/media/AudioTrack;

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:Z

.field private o:I

.field private p:I

.field private q:J

.field private r:I

.field private s:I

.field private t:J

.field private u:J

.field private v:Z

.field private w:J

.field private x:Ljava/lang/reflect/Method;

.field private y:J

.field private z:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 379
    sput-boolean v0, Lcom/google/obf/aa;->a:Z

    .line 380
    sput-boolean v0, Lcom/google/obf/aa;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lcom/google/obf/aa;-><init>(Lcom/google/obf/z;I)V

    .line 2
    return-void
.end method

.method public constructor <init>(Lcom/google/obf/z;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lcom/google/obf/aa;->c:Lcom/google/obf/z;

    .line 5
    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lcom/google/obf/aa;->d:Landroid/os/ConditionVariable;

    .line 6
    sget v0, Lcom/google/obf/ea;->a:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 7
    :try_start_0
    const-class v1, Landroid/media/AudioTrack;

    const-string v2, "getLatency"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    .line 8
    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/aa;->x:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    :cond_0
    :goto_0
    sget v0, Lcom/google/obf/ea;->a:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1

    .line 12
    new-instance v0, Lcom/google/obf/aa$c;

    invoke-direct {v0}, Lcom/google/obf/aa$c;-><init>()V

    iput-object v0, p0, Lcom/google/obf/aa;->f:Lcom/google/obf/aa$a;

    .line 16
    :goto_1
    const/16 v0, 0xa

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/google/obf/aa;->e:[J

    .line 17
    iput p2, p0, Lcom/google/obf/aa;->k:I

    .line 18
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/obf/aa;->F:F

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/aa;->B:I

    .line 20
    return-void

    .line 13
    :cond_1
    sget v0, Lcom/google/obf/ea;->a:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_2

    .line 14
    new-instance v0, Lcom/google/obf/aa$b;

    invoke-direct {v0}, Lcom/google/obf/aa$b;-><init>()V

    iput-object v0, p0, Lcom/google/obf/aa;->f:Lcom/google/obf/aa$a;

    goto :goto_1

    .line 15
    :cond_2
    new-instance v0, Lcom/google/obf/aa$a;

    invoke-direct {v0, v3}, Lcom/google/obf/aa$a;-><init>(Lcom/google/obf/aa$1;)V

    iput-object v0, p0, Lcom/google/obf/aa;->f:Lcom/google/obf/aa$a;

    goto :goto_1

    .line 10
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static a(ILjava/nio/ByteBuffer;)I
    .locals 3

    .prologue
    .line 366
    const/4 v0, 0x7

    if-eq p0, v0, :cond_0

    const/16 v0, 0x8

    if-ne p0, v0, :cond_1

    .line 367
    :cond_0
    invoke-static {p1}, Lcom/google/obf/dn;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 371
    :goto_0
    return v0

    .line 368
    :cond_1
    const/4 v0, 0x5

    if-ne p0, v0, :cond_2

    .line 369
    invoke-static {}, Lcom/google/obf/dk;->a()I

    move-result v0

    goto :goto_0

    .line 370
    :cond_2
    const/4 v0, 0x6

    if-ne p0, v0, :cond_3

    .line 371
    invoke-static {p1}, Lcom/google/obf/dk;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    goto :goto_0

    .line 372
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const/16 v1, 0x26

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unexpected audio encoding: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Landroid/media/AudioTrack;Ljava/nio/ByteBuffer;I)I
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 373
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Landroid/media/AudioTrack;->write(Ljava/nio/ByteBuffer;II)I

    move-result v0

    return v0
.end method

.method private a(J)J
    .locals 3

    .prologue
    .line 311
    iget v0, p0, Lcom/google/obf/aa;->o:I

    int-to-long v0, v0

    div-long v0, p1, v0

    return-wide v0
.end method

.method static synthetic a(Lcom/google/obf/aa;)Landroid/os/ConditionVariable;
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/google/obf/aa;->d:Landroid/os/ConditionVariable;

    return-object v0
.end method

.method private static a(Ljava/nio/ByteBuffer;IIILjava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 327
    sparse-switch p3, :sswitch_data_0

    .line 334
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 328
    :sswitch_0
    mul-int/lit8 v0, p2, 0x2

    .line 336
    :goto_0
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    if-ge v1, v0, :cond_1

    .line 337
    :cond_0
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object p4

    .line 338
    :cond_1
    invoke-virtual {p4, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 339
    invoke-virtual {p4, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 340
    add-int v0, p1, p2

    .line 341
    sparse-switch p3, :sswitch_data_1

    .line 357
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 330
    :sswitch_1
    div-int/lit8 v0, p2, 0x3

    mul-int/lit8 v0, v0, 0x2

    .line 331
    goto :goto_0

    .line 332
    :sswitch_2
    div-int/lit8 v0, p2, 0x2

    goto :goto_0

    .line 342
    :goto_1
    :sswitch_3
    if-ge p1, v0, :cond_2

    .line 343
    invoke-virtual {p4, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 344
    invoke-virtual {p0, p1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    add-int/lit8 v1, v1, -0x80

    int-to-byte v1, v1

    invoke-virtual {p4, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 345
    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    .line 347
    :goto_2
    :sswitch_4
    if-ge p1, v0, :cond_2

    .line 348
    add-int/lit8 v1, p1, 0x1

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    invoke-virtual {p4, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 349
    add-int/lit8 v1, p1, 0x2

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    invoke-virtual {p4, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 350
    add-int/lit8 p1, p1, 0x3

    goto :goto_2

    .line 352
    :goto_3
    :sswitch_5
    if-ge p1, v0, :cond_2

    .line 353
    add-int/lit8 v1, p1, 0x2

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    invoke-virtual {p4, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 354
    add-int/lit8 v1, p1, 0x3

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    invoke-virtual {p4, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 355
    add-int/lit8 p1, p1, 0x4

    goto :goto_3

    .line 358
    :cond_2
    invoke-virtual {p4, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 359
    return-object p4

    .line 327
    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x3 -> :sswitch_0
        0x40000000 -> :sswitch_2
    .end sparse-switch

    .line 341
    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_4
        0x3 -> :sswitch_3
        0x40000000 -> :sswitch_5
    .end sparse-switch
.end method

.method private static a(Landroid/media/AudioTrack;F)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 374
    invoke-virtual {p0, p1}, Landroid/media/AudioTrack;->setVolume(F)I

    .line 375
    return-void
.end method

.method private static b(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 360
    const/4 v1, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 365
    :goto_1
    return v0

    .line 360
    :sswitch_0
    const-string v2, "audio/ac3"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "audio/eac3"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "audio/vnd.dts"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "audio/vnd.dts.hd"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    .line 361
    :pswitch_0
    const/4 v0, 0x5

    goto :goto_1

    .line 362
    :pswitch_1
    const/4 v0, 0x6

    goto :goto_1

    .line 363
    :pswitch_2
    const/4 v0, 0x7

    goto :goto_1

    .line 364
    :pswitch_3
    const/16 v0, 0x8

    goto :goto_1

    .line 360
    :sswitch_data_0
    .sparse-switch
        -0x41455b98 -> :sswitch_2
        0xb269698 -> :sswitch_0
        0x59ae0c65 -> :sswitch_1
        0x59c2dc42 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b(J)J
    .locals 5

    .prologue
    .line 312
    const-wide/32 v0, 0xf4240

    mul-long/2addr v0, p1

    iget v2, p0, Lcom/google/obf/aa;->i:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    return-wide v0
.end method

.method private static b(Landroid/media/AudioTrack;F)V
    .locals 0

    .prologue
    .line 376
    invoke-virtual {p0, p1, p1}, Landroid/media/AudioTrack;->setStereoVolume(FF)I

    .line 377
    return-void
.end method

.method private c(J)J
    .locals 5

    .prologue
    .line 313
    iget v0, p0, Lcom/google/obf/aa;->i:I

    int-to-long v0, v0

    mul-long/2addr v0, p1

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    return-wide v0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/google/obf/aa;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 220
    :goto_0
    return-void

    .line 217
    :cond_0
    sget v0, Lcom/google/obf/ea;->a:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 218
    iget-object v0, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    iget v1, p0, Lcom/google/obf/aa;->F:F

    invoke-static {v0, v1}, Lcom/google/obf/aa;->a(Landroid/media/AudioTrack;F)V

    goto :goto_0

    .line 219
    :cond_1
    iget-object v0, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    iget v1, p0, Lcom/google/obf/aa;->F:F

    invoke-static {v0, v1}, Lcom/google/obf/aa;->b(Landroid/media/AudioTrack;F)V

    goto :goto_0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/obf/aa;->g:Landroid/media/AudioTrack;

    if-nez v0, :cond_0

    .line 252
    :goto_0
    return-void

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/google/obf/aa;->g:Landroid/media/AudioTrack;

    .line 249
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/obf/aa;->g:Landroid/media/AudioTrack;

    .line 250
    new-instance v1, Lcom/google/obf/aa$2;

    invoke-direct {v1, p0, v0}, Lcom/google/obf/aa$2;-><init>(Lcom/google/obf/aa;Landroid/media/AudioTrack;)V

    .line 251
    invoke-virtual {v1}, Lcom/google/obf/aa$2;->start()V

    goto :goto_0
.end method

.method private n()Z
    .locals 1

    .prologue
    .line 253
    invoke-virtual {p0}, Lcom/google/obf/aa;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/obf/aa;->B:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()V
    .locals 12

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/obf/aa;->f:Lcom/google/obf/aa$a;

    invoke-virtual {v0}, Lcom/google/obf/aa$a;->c()J

    move-result-wide v2

    .line 255
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_1

    .line 299
    :cond_0
    :goto_0
    return-void

    .line 257
    :cond_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    const-wide/16 v4, 0x3e8

    div-long v4, v0, v4

    .line 258
    iget-wide v0, p0, Lcom/google/obf/aa;->u:J

    sub-long v0, v4, v0

    const-wide/16 v6, 0x7530

    cmp-long v0, v0, v6

    if-ltz v0, :cond_3

    .line 259
    iget-object v0, p0, Lcom/google/obf/aa;->e:[J

    iget v1, p0, Lcom/google/obf/aa;->r:I

    sub-long v6, v2, v4

    aput-wide v6, v0, v1

    .line 260
    iget v0, p0, Lcom/google/obf/aa;->r:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0xa

    iput v0, p0, Lcom/google/obf/aa;->r:I

    .line 261
    iget v0, p0, Lcom/google/obf/aa;->s:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_2

    .line 262
    iget v0, p0, Lcom/google/obf/aa;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/aa;->s:I

    .line 263
    :cond_2
    iput-wide v4, p0, Lcom/google/obf/aa;->u:J

    .line 264
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/obf/aa;->t:J

    .line 265
    const/4 v0, 0x0

    :goto_1
    iget v1, p0, Lcom/google/obf/aa;->s:I

    if-ge v0, v1, :cond_3

    .line 266
    iget-wide v6, p0, Lcom/google/obf/aa;->t:J

    iget-object v1, p0, Lcom/google/obf/aa;->e:[J

    aget-wide v8, v1, v0

    iget v1, p0, Lcom/google/obf/aa;->s:I

    int-to-long v10, v1

    div-long/2addr v8, v10

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/google/obf/aa;->t:J

    .line 267
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 268
    :cond_3
    invoke-direct {p0}, Lcom/google/obf/aa;->s()Z

    move-result v0

    if-nez v0, :cond_0

    .line 270
    iget-wide v0, p0, Lcom/google/obf/aa;->w:J

    sub-long v0, v4, v0

    const-wide/32 v6, 0x7a120

    cmp-long v0, v0, v6

    if-ltz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/google/obf/aa;->f:Lcom/google/obf/aa$a;

    invoke-virtual {v0}, Lcom/google/obf/aa$a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/obf/aa;->v:Z

    .line 272
    iget-boolean v0, p0, Lcom/google/obf/aa;->v:Z

    if-eqz v0, :cond_4

    .line 273
    iget-object v0, p0, Lcom/google/obf/aa;->f:Lcom/google/obf/aa$a;

    invoke-virtual {v0}, Lcom/google/obf/aa$a;->e()J

    move-result-wide v0

    const-wide/16 v6, 0x3e8

    div-long/2addr v0, v6

    .line 274
    iget-object v6, p0, Lcom/google/obf/aa;->f:Lcom/google/obf/aa$a;

    invoke-virtual {v6}, Lcom/google/obf/aa$a;->f()J

    move-result-wide v6

    .line 275
    iget-wide v8, p0, Lcom/google/obf/aa;->D:J

    cmp-long v8, v0, v8

    if-gez v8, :cond_6

    .line 276
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/aa;->v:Z

    .line 289
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/google/obf/aa;->x:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/google/obf/aa;->n:Z

    if-nez v0, :cond_5

    .line 290
    :try_start_0
    iget-object v1, p0, Lcom/google/obf/aa;->x:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/obf/aa;->q:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/obf/aa;->E:J

    .line 291
    iget-wide v0, p0, Lcom/google/obf/aa;->E:J

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/obf/aa;->E:J

    .line 292
    iget-wide v0, p0, Lcom/google/obf/aa;->E:J

    const-wide/32 v2, 0x4c4b40

    cmp-long v0, v0, v2

    if-lez v0, :cond_5

    .line 293
    const-string v0, "AudioTrack"

    iget-wide v2, p0, Lcom/google/obf/aa;->E:J

    const/16 v1, 0x3d

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Ignoring impossibly large audio latency: "

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/obf/aa;->E:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 298
    :cond_5
    :goto_3
    iput-wide v4, p0, Lcom/google/obf/aa;->w:J

    goto/16 :goto_0

    .line 277
    :cond_6
    sub-long v8, v0, v4

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(J)J

    move-result-wide v8

    const-wide/32 v10, 0x4c4b40

    cmp-long v8, v8, v10

    if-lez v8, :cond_8

    .line 278
    const/16 v8, 0x88

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Spurious audio timestamp (system clock mismatch): "

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 279
    sget-boolean v1, Lcom/google/obf/aa;->b:Z

    if-eqz v1, :cond_7

    .line 280
    new-instance v1, Lcom/google/obf/aa$e;

    invoke-direct {v1, v0}, Lcom/google/obf/aa$e;-><init>(Ljava/lang/String;)V

    throw v1

    .line 281
    :cond_7
    const-string v1, "AudioTrack"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/aa;->v:Z

    goto/16 :goto_2

    .line 283
    :cond_8
    invoke-direct {p0, v6, v7}, Lcom/google/obf/aa;->b(J)J

    move-result-wide v8

    sub-long/2addr v8, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(J)J

    move-result-wide v8

    const-wide/32 v10, 0x4c4b40

    cmp-long v8, v8, v10

    if-lez v8, :cond_4

    .line 284
    const/16 v8, 0x8a

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Spurious audio timestamp (frame position mismatch): "

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 285
    sget-boolean v1, Lcom/google/obf/aa;->b:Z

    if-eqz v1, :cond_9

    .line 286
    new-instance v1, Lcom/google/obf/aa$e;

    invoke-direct {v1, v0}, Lcom/google/obf/aa$e;-><init>(Ljava/lang/String;)V

    throw v1

    .line 287
    :cond_9
    const-string v1, "AudioTrack"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/aa;->v:Z

    goto/16 :goto_2

    .line 296
    :catch_0
    move-exception v0

    .line 297
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/aa;->x:Ljava/lang/reflect/Method;

    goto/16 :goto_3
.end method

.method private p()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/aa$d;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 300
    iget-object v0, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getState()I

    move-result v0

    .line 301
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 302
    return-void

    .line 303
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    invoke-virtual {v1}, Landroid/media/AudioTrack;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 304
    iput-object v2, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    .line 310
    :goto_0
    new-instance v1, Lcom/google/obf/aa$d;

    iget v2, p0, Lcom/google/obf/aa;->i:I

    iget v3, p0, Lcom/google/obf/aa;->j:I

    iget v4, p0, Lcom/google/obf/aa;->p:I

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/obf/aa$d;-><init>(IIII)V

    throw v1

    .line 306
    :catch_0
    move-exception v1

    .line 307
    iput-object v2, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    goto :goto_0

    .line 309
    :catchall_0
    move-exception v0

    iput-object v2, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    throw v0
.end method

.method private q()J
    .locals 2

    .prologue
    .line 314
    iget-boolean v0, p0, Lcom/google/obf/aa;->n:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/obf/aa;->z:J

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/google/obf/aa;->y:J

    invoke-direct {p0, v0, v1}, Lcom/google/obf/aa;->a(J)J

    move-result-wide v0

    goto :goto_0
.end method

.method private r()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 315
    iput-wide v2, p0, Lcom/google/obf/aa;->t:J

    .line 316
    iput v0, p0, Lcom/google/obf/aa;->s:I

    .line 317
    iput v0, p0, Lcom/google/obf/aa;->r:I

    .line 318
    iput-wide v2, p0, Lcom/google/obf/aa;->u:J

    .line 319
    iput-boolean v0, p0, Lcom/google/obf/aa;->v:Z

    .line 320
    iput-wide v2, p0, Lcom/google/obf/aa;->w:J

    .line 321
    return-void
.end method

.method private s()Z
    .locals 2

    .prologue
    .line 322
    sget v0, Lcom/google/obf/ea;->a:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_1

    iget v0, p0, Lcom/google/obf/aa;->m:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/obf/aa;->m:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private t()Z
    .locals 2

    .prologue
    .line 323
    invoke-direct {p0}, Lcom/google/obf/aa;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    .line 324
    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    .line 325
    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlaybackHeadPosition()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 326
    :goto_0
    return v0

    .line 325
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(I)I
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/aa$d;
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v6, 0x1

    .line 108
    iget-object v0, p0, Lcom/google/obf/aa;->d:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 109
    if-nez p1, :cond_2

    .line 110
    new-instance v0, Landroid/media/AudioTrack;

    iget v1, p0, Lcom/google/obf/aa;->k:I

    iget v2, p0, Lcom/google/obf/aa;->i:I

    iget v3, p0, Lcom/google/obf/aa;->j:I

    iget v4, p0, Lcom/google/obf/aa;->m:I

    iget v5, p0, Lcom/google/obf/aa;->p:I

    invoke-direct/range {v0 .. v6}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    .line 112
    :goto_0
    invoke-direct {p0}, Lcom/google/obf/aa;->p()V

    .line 113
    iget-object v0, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getAudioSessionId()I

    move-result v7

    .line 114
    sget-boolean v0, Lcom/google/obf/aa;->a:Z

    if-eqz v0, :cond_1

    .line 115
    sget v0, Lcom/google/obf/ea;->a:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_1

    .line 116
    iget-object v0, p0, Lcom/google/obf/aa;->g:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/aa;->g:Landroid/media/AudioTrack;

    .line 117
    invoke-virtual {v0}, Landroid/media/AudioTrack;->getAudioSessionId()I

    move-result v0

    if-eq v7, v0, :cond_0

    .line 118
    invoke-direct {p0}, Lcom/google/obf/aa;->m()V

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/google/obf/aa;->g:Landroid/media/AudioTrack;

    if-nez v0, :cond_1

    .line 120
    const/16 v2, 0xfa0

    .line 121
    const/4 v3, 0x4

    .line 124
    new-instance v0, Landroid/media/AudioTrack;

    iget v1, p0, Lcom/google/obf/aa;->k:I

    const/4 v6, 0x0

    move v4, v8

    move v5, v8

    invoke-direct/range {v0 .. v7}, Landroid/media/AudioTrack;-><init>(IIIIIII)V

    iput-object v0, p0, Lcom/google/obf/aa;->g:Landroid/media/AudioTrack;

    .line 125
    :cond_1
    iget-object v0, p0, Lcom/google/obf/aa;->f:Lcom/google/obf/aa$a;

    iget-object v1, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    invoke-direct {p0}, Lcom/google/obf/aa;->s()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/obf/aa$a;->a(Landroid/media/AudioTrack;Z)V

    .line 126
    invoke-direct {p0}, Lcom/google/obf/aa;->l()V

    .line 127
    return v7

    .line 111
    :cond_2
    new-instance v0, Landroid/media/AudioTrack;

    iget v1, p0, Lcom/google/obf/aa;->k:I

    iget v2, p0, Lcom/google/obf/aa;->i:I

    iget v3, p0, Lcom/google/obf/aa;->j:I

    iget v4, p0, Lcom/google/obf/aa;->m:I

    iget v5, p0, Lcom/google/obf/aa;->p:I

    move v7, p1

    invoke-direct/range {v0 .. v7}, Landroid/media/AudioTrack;-><init>(IIIIIII)V

    iput-object v0, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    goto :goto_0
.end method

.method public a(Ljava/nio/ByteBuffer;IIJ)I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/aa$f;
        }
    .end annotation

    .prologue
    .line 137
    invoke-direct {p0}, Lcom/google/obf/aa;->s()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 138
    iget-object v0, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 139
    const/4 v0, 0x0

    .line 197
    :cond_0
    :goto_0
    return v0

    .line 140
    :cond_1
    iget-object v0, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/obf/aa;->f:Lcom/google/obf/aa$a;

    .line 141
    invoke-virtual {v0}, Lcom/google/obf/aa$a;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 142
    const/4 v0, 0x0

    goto :goto_0

    .line 143
    :cond_2
    const/4 v1, 0x0

    .line 144
    iget v0, p0, Lcom/google/obf/aa;->I:I

    if-nez v0, :cond_14

    .line 145
    if-nez p3, :cond_3

    .line 146
    const/4 v0, 0x2

    goto :goto_0

    .line 147
    :cond_3
    iget v0, p0, Lcom/google/obf/aa;->m:I

    iget v2, p0, Lcom/google/obf/aa;->l:I

    if-eq v0, v2, :cond_a

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/obf/aa;->K:Z

    .line 148
    iget-boolean v0, p0, Lcom/google/obf/aa;->K:Z

    if-eqz v0, :cond_4

    .line 149
    iget v0, p0, Lcom/google/obf/aa;->m:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_b

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 150
    iget v0, p0, Lcom/google/obf/aa;->l:I

    iget-object v2, p0, Lcom/google/obf/aa;->J:Ljava/nio/ByteBuffer;

    invoke-static {p1, p2, p3, v0, v2}, Lcom/google/obf/aa;->a(Ljava/nio/ByteBuffer;IIILjava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/aa;->J:Ljava/nio/ByteBuffer;

    .line 151
    iget-object p1, p0, Lcom/google/obf/aa;->J:Ljava/nio/ByteBuffer;

    .line 152
    iget-object v0, p0, Lcom/google/obf/aa;->J:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result p2

    .line 153
    iget-object v0, p0, Lcom/google/obf/aa;->J:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result p3

    .line 154
    :cond_4
    iput p3, p0, Lcom/google/obf/aa;->I:I

    .line 155
    invoke-virtual {p1, p2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 156
    iget-boolean v0, p0, Lcom/google/obf/aa;->n:Z

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/google/obf/aa;->A:I

    if-nez v0, :cond_5

    .line 157
    iget v0, p0, Lcom/google/obf/aa;->m:I

    invoke-static {v0, p1}, Lcom/google/obf/aa;->a(ILjava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Lcom/google/obf/aa;->A:I

    .line 158
    :cond_5
    iget v0, p0, Lcom/google/obf/aa;->B:I

    if-nez v0, :cond_c

    .line 159
    const-wide/16 v2, 0x0

    invoke-static {v2, v3, p4, p5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/obf/aa;->C:J

    .line 160
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/obf/aa;->B:I

    move v0, v1

    .line 170
    :goto_3
    sget v1, Lcom/google/obf/ea;->a:I

    const/16 v2, 0x15

    if-ge v1, v2, :cond_8

    .line 171
    iget-object v1, p0, Lcom/google/obf/aa;->G:[B

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/obf/aa;->G:[B

    array-length v1, v1

    if-ge v1, p3, :cond_7

    .line 172
    :cond_6
    new-array v1, p3, [B

    iput-object v1, p0, Lcom/google/obf/aa;->G:[B

    .line 173
    :cond_7
    iget-object v1, p0, Lcom/google/obf/aa;->G:[B

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, p3}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 174
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/obf/aa;->H:I

    .line 175
    :cond_8
    :goto_4
    const/4 v1, 0x0

    .line 176
    sget v2, Lcom/google/obf/ea;->a:I

    const/16 v3, 0x15

    if-ge v2, v3, :cond_e

    .line 177
    iget-wide v2, p0, Lcom/google/obf/aa;->y:J

    iget-object v4, p0, Lcom/google/obf/aa;->f:Lcom/google/obf/aa$a;

    .line 178
    invoke-virtual {v4}, Lcom/google/obf/aa$a;->b()J

    move-result-wide v4

    iget v6, p0, Lcom/google/obf/aa;->o:I

    int-to-long v6, v6

    mul-long/2addr v4, v6

    sub-long/2addr v2, v4

    long-to-int v2, v2

    .line 179
    iget v3, p0, Lcom/google/obf/aa;->p:I

    sub-int v2, v3, v2

    .line 180
    if-lez v2, :cond_9

    .line 181
    iget v1, p0, Lcom/google/obf/aa;->I:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 182
    iget-object v2, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    iget-object v3, p0, Lcom/google/obf/aa;->G:[B

    iget v4, p0, Lcom/google/obf/aa;->H:I

    invoke-virtual {v2, v3, v4, v1}, Landroid/media/AudioTrack;->write([BII)I

    move-result v1

    .line 183
    if-ltz v1, :cond_9

    .line 184
    iget v2, p0, Lcom/google/obf/aa;->H:I

    add-int/2addr v2, v1

    iput v2, p0, Lcom/google/obf/aa;->H:I

    .line 188
    :cond_9
    :goto_5
    if-gez v1, :cond_10

    .line 189
    new-instance v0, Lcom/google/obf/aa$f;

    invoke-direct {v0, v1}, Lcom/google/obf/aa$f;-><init>(I)V

    throw v0

    .line 147
    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 149
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 161
    :cond_c
    iget-wide v2, p0, Lcom/google/obf/aa;->C:J

    invoke-direct {p0}, Lcom/google/obf/aa;->q()J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/google/obf/aa;->b(J)J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 162
    iget v0, p0, Lcom/google/obf/aa;->B:I

    const/4 v4, 0x1

    if-ne v0, v4, :cond_d

    sub-long v4, v2, p4

    .line 163
    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    const-wide/32 v6, 0x30d40

    cmp-long v0, v4, v6

    if-lez v0, :cond_d

    .line 164
    const-string v0, "AudioTrack"

    const/16 v4, 0x50

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Discontinuity detected [expected "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", got "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/obf/aa;->B:I

    .line 166
    :cond_d
    iget v0, p0, Lcom/google/obf/aa;->B:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_13

    .line 167
    iget-wide v0, p0, Lcom/google/obf/aa;->C:J

    sub-long v2, p4, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/obf/aa;->C:J

    .line 168
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/obf/aa;->B:I

    .line 169
    const/4 v0, 0x1

    goto/16 :goto_3

    .line 186
    :cond_e
    iget-boolean v1, p0, Lcom/google/obf/aa;->K:Z

    if-eqz v1, :cond_f

    iget-object p1, p0, Lcom/google/obf/aa;->J:Ljava/nio/ByteBuffer;

    .line 187
    :cond_f
    iget-object v1, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    iget v2, p0, Lcom/google/obf/aa;->I:I

    invoke-static {v1, p1, v2}, Lcom/google/obf/aa;->a(Landroid/media/AudioTrack;Ljava/nio/ByteBuffer;I)I

    move-result v1

    goto :goto_5

    .line 190
    :cond_10
    iget v2, p0, Lcom/google/obf/aa;->I:I

    sub-int/2addr v2, v1

    iput v2, p0, Lcom/google/obf/aa;->I:I

    .line 191
    iget-boolean v2, p0, Lcom/google/obf/aa;->n:Z

    if-nez v2, :cond_11

    .line 192
    iget-wide v2, p0, Lcom/google/obf/aa;->y:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/obf/aa;->y:J

    .line 193
    :cond_11
    iget v1, p0, Lcom/google/obf/aa;->I:I

    if-nez v1, :cond_0

    .line 194
    iget-boolean v1, p0, Lcom/google/obf/aa;->n:Z

    if-eqz v1, :cond_12

    .line 195
    iget-wide v2, p0, Lcom/google/obf/aa;->z:J

    iget v1, p0, Lcom/google/obf/aa;->A:I

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/obf/aa;->z:J

    .line 196
    :cond_12
    or-int/lit8 v0, v0, 0x2

    goto/16 :goto_0

    :cond_13
    move v0, v1

    goto/16 :goto_3

    :cond_14
    move v0, v1

    goto/16 :goto_4
.end method

.method public a(Z)J
    .locals 6

    .prologue
    const-wide/16 v4, 0x3e8

    .line 25
    invoke-direct {p0}, Lcom/google/obf/aa;->n()Z

    move-result v0

    if-nez v0, :cond_1

    .line 26
    const-wide/high16 v0, -0x8000000000000000L

    .line 43
    :cond_0
    :goto_0
    return-wide v0

    .line 27
    :cond_1
    iget-object v0, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 28
    invoke-direct {p0}, Lcom/google/obf/aa;->o()V

    .line 29
    :cond_2
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    div-long/2addr v0, v4

    .line 30
    iget-boolean v2, p0, Lcom/google/obf/aa;->v:Z

    if-eqz v2, :cond_3

    .line 31
    iget-object v2, p0, Lcom/google/obf/aa;->f:Lcom/google/obf/aa$a;

    invoke-virtual {v2}, Lcom/google/obf/aa$a;->e()J

    move-result-wide v2

    div-long/2addr v2, v4

    sub-long/2addr v0, v2

    .line 32
    long-to-float v0, v0

    iget-object v1, p0, Lcom/google/obf/aa;->f:Lcom/google/obf/aa$a;

    .line 33
    invoke-virtual {v1}, Lcom/google/obf/aa$a;->g()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-long v0, v0

    .line 34
    invoke-direct {p0, v0, v1}, Lcom/google/obf/aa;->c(J)J

    move-result-wide v0

    .line 35
    iget-object v2, p0, Lcom/google/obf/aa;->f:Lcom/google/obf/aa$a;

    invoke-virtual {v2}, Lcom/google/obf/aa$a;->f()J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 36
    invoke-direct {p0, v0, v1}, Lcom/google/obf/aa;->b(J)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/obf/aa;->C:J

    add-long/2addr v0, v2

    .line 37
    goto :goto_0

    .line 38
    :cond_3
    iget v2, p0, Lcom/google/obf/aa;->s:I

    if-nez v2, :cond_4

    .line 39
    iget-object v0, p0, Lcom/google/obf/aa;->f:Lcom/google/obf/aa$a;

    invoke-virtual {v0}, Lcom/google/obf/aa$a;->c()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/obf/aa;->C:J

    add-long/2addr v0, v2

    .line 41
    :goto_1
    if-nez p1, :cond_0

    .line 42
    iget-wide v2, p0, Lcom/google/obf/aa;->E:J

    sub-long/2addr v0, v2

    goto :goto_0

    .line 40
    :cond_4
    iget-wide v2, p0, Lcom/google/obf/aa;->t:J

    add-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/obf/aa;->C:J

    add-long/2addr v0, v2

    goto :goto_1
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 212
    iget v0, p0, Lcom/google/obf/aa;->F:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 213
    iput p1, p0, Lcom/google/obf/aa;->F:F

    .line 214
    invoke-direct {p0}, Lcom/google/obf/aa;->l()V

    .line 215
    :cond_0
    return-void
.end method

.method public a(Landroid/media/PlaybackParams;)V
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/obf/aa;->f:Lcom/google/obf/aa$a;

    invoke-virtual {v0, p1}, Lcom/google/obf/aa$a;->a(Landroid/media/PlaybackParams;)V

    .line 206
    return-void
.end method

.method public a(Ljava/lang/String;III)V
    .locals 6

    .prologue
    .line 44
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/obf/aa;->a(Ljava/lang/String;IIII)V

    .line 45
    return-void
.end method

.method public a(Ljava/lang/String;IIII)V
    .locals 10

    .prologue
    const/16 v2, 0xfc

    const/16 v1, 0xc

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 46
    packed-switch p2, :pswitch_data_0

    .line 63
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x26

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unsupported channel count: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :pswitch_0
    const/4 v0, 0x4

    .line 64
    :goto_0
    sget v6, Lcom/google/obf/ea;->a:I

    const/16 v7, 0x17

    if-gt v6, v7, :cond_0

    const-string v6, "foster"

    sget-object v7, Lcom/google/obf/ea;->b:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "NVIDIA"

    sget-object v7, Lcom/google/obf/ea;->c:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 65
    packed-switch p2, :pswitch_data_1

    .line 70
    :cond_0
    :goto_1
    :pswitch_1
    const-string v2, "audio/raw"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v3

    .line 71
    :goto_2
    sget v6, Lcom/google/obf/ea;->a:I

    const/16 v7, 0x19

    if-gt v6, v7, :cond_1

    const-string v6, "fugu"

    sget-object v7, Lcom/google/obf/ea;->b:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz v2, :cond_1

    if-ne p2, v3, :cond_1

    move v0, v1

    .line 73
    :cond_1
    if-eqz v2, :cond_4

    .line 74
    invoke-static {p1}, Lcom/google/obf/aa;->b(Ljava/lang/String;)I

    move-result p4

    .line 78
    :cond_2
    invoke-virtual {p0}, Lcom/google/obf/aa;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    iget v1, p0, Lcom/google/obf/aa;->l:I

    if-ne v1, p4, :cond_5

    iget v1, p0, Lcom/google/obf/aa;->i:I

    if-ne v1, p3, :cond_5

    iget v1, p0, Lcom/google/obf/aa;->j:I

    if-ne v1, v0, :cond_5

    .line 106
    :goto_3
    return-void

    :pswitch_2
    move v0, v1

    .line 50
    goto :goto_0

    .line 51
    :pswitch_3
    const/16 v0, 0x1c

    .line 52
    goto :goto_0

    .line 53
    :pswitch_4
    const/16 v0, 0xcc

    .line 54
    goto :goto_0

    .line 55
    :pswitch_5
    const/16 v0, 0xdc

    .line 56
    goto :goto_0

    :pswitch_6
    move v0, v2

    .line 58
    goto :goto_0

    .line 59
    :pswitch_7
    const/16 v0, 0x4fc

    .line 60
    goto :goto_0

    .line 61
    :pswitch_8
    sget v0, Lcom/google/obf/b;->a:I

    goto :goto_0

    .line 66
    :pswitch_9
    sget v0, Lcom/google/obf/b;->a:I

    goto :goto_1

    :pswitch_a
    move v0, v2

    .line 69
    goto :goto_1

    :cond_3
    move v2, v4

    .line 70
    goto :goto_2

    .line 75
    :cond_4
    const/4 v1, 0x3

    if-eq p4, v1, :cond_2

    if-eq p4, v5, :cond_2

    const/high16 v1, -0x80000000

    if-eq p4, v1, :cond_2

    const/high16 v1, 0x40000000    # 2.0f

    if-eq p4, v1, :cond_2

    .line 77
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/16 v1, 0x25

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unsupported PCM encoding: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_5
    invoke-virtual {p0}, Lcom/google/obf/aa;->j()V

    .line 81
    iput p4, p0, Lcom/google/obf/aa;->l:I

    .line 82
    iput-boolean v2, p0, Lcom/google/obf/aa;->n:Z

    .line 83
    iput p3, p0, Lcom/google/obf/aa;->i:I

    .line 84
    iput v0, p0, Lcom/google/obf/aa;->j:I

    .line 85
    if-eqz v2, :cond_6

    :goto_4
    iput p4, p0, Lcom/google/obf/aa;->m:I

    .line 86
    mul-int/lit8 v1, p2, 0x2

    iput v1, p0, Lcom/google/obf/aa;->o:I

    .line 87
    if-eqz p5, :cond_7

    .line 88
    iput p5, p0, Lcom/google/obf/aa;->p:I

    .line 104
    :goto_5
    if-eqz v2, :cond_e

    const-wide/16 v0, -0x1

    .line 105
    :goto_6
    iput-wide v0, p0, Lcom/google/obf/aa;->q:J

    goto :goto_3

    :cond_6
    move p4, v5

    .line 85
    goto :goto_4

    .line 89
    :cond_7
    if-eqz v2, :cond_a

    .line 90
    iget v0, p0, Lcom/google/obf/aa;->m:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_8

    iget v0, p0, Lcom/google/obf/aa;->m:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_9

    .line 91
    :cond_8
    const/16 v0, 0x5000

    iput v0, p0, Lcom/google/obf/aa;->p:I

    goto :goto_5

    .line 92
    :cond_9
    const v0, 0xc000

    iput v0, p0, Lcom/google/obf/aa;->p:I

    goto :goto_5

    .line 93
    :cond_a
    iget v1, p0, Lcom/google/obf/aa;->m:I

    .line 94
    invoke-static {p3, v0, v1}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v1

    .line 95
    const/4 v0, -0x2

    if-eq v1, v0, :cond_b

    :goto_7
    invoke-static {v3}, Lcom/google/obf/dl;->b(Z)V

    .line 96
    mul-int/lit8 v3, v1, 0x4

    .line 97
    const-wide/32 v4, 0x3d090

    invoke-direct {p0, v4, v5}, Lcom/google/obf/aa;->c(J)J

    move-result-wide v4

    long-to-int v0, v4

    iget v4, p0, Lcom/google/obf/aa;->o:I

    mul-int/2addr v0, v4

    .line 98
    int-to-long v4, v1

    const-wide/32 v6, 0xb71b0

    .line 99
    invoke-direct {p0, v6, v7}, Lcom/google/obf/aa;->c(J)J

    move-result-wide v6

    iget v1, p0, Lcom/google/obf/aa;->o:I

    int-to-long v8, v1

    mul-long/2addr v6, v8

    .line 100
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    long-to-int v1, v4

    .line 101
    if-ge v3, v0, :cond_c

    .line 103
    :goto_8
    iput v0, p0, Lcom/google/obf/aa;->p:I

    goto :goto_5

    :cond_b
    move v3, v4

    .line 95
    goto :goto_7

    .line 102
    :cond_c
    if-le v3, v1, :cond_d

    move v0, v1

    goto :goto_8

    :cond_d
    move v0, v3

    .line 103
    goto :goto_8

    .line 105
    :cond_e
    iget v0, p0, Lcom/google/obf/aa;->p:I

    int-to-long v0, v0

    invoke-direct {p0, v0, v1}, Lcom/google/obf/aa;->a(J)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/obf/aa;->b(J)J

    move-result-wide v0

    goto :goto_6

    .line 46
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 65
    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_a
        :pswitch_1
        :pswitch_a
        :pswitch_1
        :pswitch_9
    .end packed-switch
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/obf/aa;->c:Lcom/google/obf/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/aa;->c:Lcom/google/obf/z;

    .line 22
    invoke-static {p1}, Lcom/google/obf/aa;->b(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/obf/z;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 23
    :goto_0
    return v0

    .line 22
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/aa$d;
        }
    .end annotation

    .prologue
    .line 107
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/obf/aa;->a(I)I

    move-result v0

    return v0
.end method

.method public b(I)Z
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lcom/google/obf/aa;->k:I

    if-ne v0, p1, :cond_0

    .line 208
    const/4 v0, 0x0

    .line 211
    :goto_0
    return v0

    .line 209
    :cond_0
    iput p1, p0, Lcom/google/obf/aa;->k:I

    .line 210
    invoke-virtual {p0}, Lcom/google/obf/aa;->j()V

    .line 211
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/google/obf/aa;->p:I

    return v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 129
    iget-wide v0, p0, Lcom/google/obf/aa;->q:J

    return-wide v0
.end method

.method public e()V
    .locals 4

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/google/obf/aa;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/obf/aa;->D:J

    .line 132
    iget-object v0, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->play()V

    .line 133
    :cond_0
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 134
    iget v0, p0, Lcom/google/obf/aa;->B:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 135
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/obf/aa;->B:I

    .line 136
    :cond_0
    return-void
.end method

.method public g()V
    .locals 4

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/google/obf/aa;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/google/obf/aa;->f:Lcom/google/obf/aa$a;

    invoke-direct {p0}, Lcom/google/obf/aa;->q()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/obf/aa$a;->a(J)V

    .line 200
    :cond_0
    return-void
.end method

.method public h()Z
    .locals 4

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/google/obf/aa;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 202
    invoke-direct {p0}, Lcom/google/obf/aa;->q()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/obf/aa;->f:Lcom/google/obf/aa$a;

    invoke-virtual {v2}, Lcom/google/obf/aa$a;->b()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 203
    invoke-direct {p0}, Lcom/google/obf/aa;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 204
    :goto_0
    return v0

    .line 203
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()V
    .locals 1

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/google/obf/aa;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    invoke-direct {p0}, Lcom/google/obf/aa;->r()V

    .line 223
    iget-object v0, p0, Lcom/google/obf/aa;->f:Lcom/google/obf/aa$a;

    invoke-virtual {v0}, Lcom/google/obf/aa$a;->a()V

    .line 224
    :cond_0
    return-void
.end method

.method public j()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 225
    invoke-virtual {p0}, Lcom/google/obf/aa;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 226
    iput-wide v4, p0, Lcom/google/obf/aa;->y:J

    .line 227
    iput-wide v4, p0, Lcom/google/obf/aa;->z:J

    .line 228
    iput v2, p0, Lcom/google/obf/aa;->A:I

    .line 229
    iput v2, p0, Lcom/google/obf/aa;->I:I

    .line 230
    iput v2, p0, Lcom/google/obf/aa;->B:I

    .line 231
    iput-wide v4, p0, Lcom/google/obf/aa;->E:J

    .line 232
    invoke-direct {p0}, Lcom/google/obf/aa;->r()V

    .line 233
    iget-object v0, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    .line 234
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 235
    iget-object v0, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->pause()V

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    .line 237
    iput-object v3, p0, Lcom/google/obf/aa;->h:Landroid/media/AudioTrack;

    .line 238
    iget-object v1, p0, Lcom/google/obf/aa;->f:Lcom/google/obf/aa$a;

    invoke-virtual {v1, v3, v2}, Lcom/google/obf/aa$a;->a(Landroid/media/AudioTrack;Z)V

    .line 239
    iget-object v1, p0, Lcom/google/obf/aa;->d:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->close()V

    .line 240
    new-instance v1, Lcom/google/obf/aa$1;

    invoke-direct {v1, p0, v0}, Lcom/google/obf/aa$1;-><init>(Lcom/google/obf/aa;Landroid/media/AudioTrack;)V

    .line 241
    invoke-virtual {v1}, Lcom/google/obf/aa$1;->start()V

    .line 242
    :cond_1
    return-void
.end method

.method public k()V
    .locals 0

    .prologue
    .line 243
    invoke-virtual {p0}, Lcom/google/obf/aa;->j()V

    .line 244
    invoke-direct {p0}, Lcom/google/obf/aa;->m()V

    .line 245
    return-void
.end method
