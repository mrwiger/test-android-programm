.class Ltoothpick/configuration/ReflectionOffConfiguration;
.super Ljava/lang/Object;
.source "ReflectionOffConfiguration.java"

# interfaces
.implements Ltoothpick/configuration/ReflectionConfiguration;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFactory(Ljava/lang/Class;)Ltoothpick/Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ltoothpick/Factory",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 11
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-static {p1}, Ltoothpick/registries/FactoryRegistryLocator;->getFactoryUsingRegistries(Ljava/lang/Class;)Ltoothpick/Factory;

    move-result-object v0

    return-object v0
.end method

.method public getMemberInjector(Ljava/lang/Class;)Ltoothpick/MemberInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ltoothpick/MemberInjector",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 16
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-static {p1}, Ltoothpick/registries/MemberInjectorRegistryLocator;->getMemberInjectorUsingRegistries(Ljava/lang/Class;)Ltoothpick/MemberInjector;

    move-result-object v0

    return-object v0
.end method
