.class final Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;
.super Ljava/lang/Object;
.source "MyTargetVideoPresenter.java"

# interfaces
.implements Lcom/my/target/instreamads/InstreamAdPlayer;
.implements Lru/cn/player/SimplePlayer$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "MTGVideoPlayer"
.end annotation


# instance fields
.field private adPlayerListener:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

.field private destroyed:Z

.field private final player:Lru/cn/player/SimplePlayer;


# direct methods
.method constructor <init>(Lru/cn/player/SimplePlayer;)V
    .locals 1
    .param p1, "player"    # Lru/cn/player/SimplePlayer;

    .prologue
    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->destroyed:Z

    .line 171
    iput-object p1, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    .line 172
    invoke-virtual {p1, p0}, Lru/cn/player/SimplePlayer;->addListener(Lru/cn/player/SimplePlayer$Listener;)V

    .line 173
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 231
    iget-boolean v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->destroyed:Z

    if-eqz v0, :cond_0

    .line 237
    :goto_0
    return-void

    .line 234
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->destroyed:Z

    .line 235
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0, p0}, Lru/cn/player/SimplePlayer;->removeListener(Lru/cn/player/SimplePlayer$Listener;)V

    .line 236
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->stop()V

    goto :goto_0
.end method

.method public endBuffering()V
    .locals 0

    .prologue
    .line 302
    return-void
.end method

.method public getAdPlayerListener()Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->adPlayerListener:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    return-object v0
.end method

.method public getAdVideoDuration()F
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getDuration()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public getAdVideoPosition()F
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getCurrentPosition()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    return-object v0
.end method

.method public onComplete()V
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->adPlayerListener:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->adPlayerListener:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;->onAdVideoCompleted()V

    .line 276
    :cond_0
    return-void
.end method

.method public onError(II)V
    .locals 2
    .param p1, "playerType"    # I
    .param p2, "code"    # I

    .prologue
    .line 280
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->adPlayerListener:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->adPlayerListener:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;->onAdVideoError(Ljava/lang/String;)V

    .line 282
    :cond_0
    return-void
.end method

.method public onMetadata(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/player/metadata/MetadataItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 287
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lru/cn/player/metadata/MetadataItem;>;"
    return-void
.end method

.method public pauseAdVideo()V
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->pause()V

    .line 214
    return-void
.end method

.method public playAdVideo(Landroid/net/Uri;II)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "i"    # I
    .param p3, "i1"    # I

    .prologue
    .line 198
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0, p1}, Lru/cn/player/SimplePlayer;->play(Landroid/net/Uri;)V

    .line 199
    return-void
.end method

.method public playAdVideo(Landroid/net/Uri;IIF)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "i"    # I
    .param p3, "i1"    # I
    .param p4, "v"    # F

    .prologue
    .line 208
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0, p1}, Lru/cn/player/SimplePlayer;->play(Landroid/net/Uri;)V

    .line 209
    return-void
.end method

.method public qualityChanged(I)V
    .locals 0
    .param p1, "bitrate"    # I

    .prologue
    .line 307
    return-void
.end method

.method public resumeAdVideo()V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->resume()V

    .line 219
    return-void
.end method

.method public setAdPlayerListener(Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;)V
    .locals 0
    .param p1, "adPlayerListener"    # Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    .prologue
    .line 193
    iput-object p1, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->adPlayerListener:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    .line 194
    return-void
.end method

.method public setVolume(F)V
    .locals 1
    .param p1, "volume"    # F

    .prologue
    .line 241
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0, p1}, Lru/cn/player/SimplePlayer;->setVolume(F)V

    .line 243
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->adPlayerListener:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->adPlayerListener:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    invoke-interface {v0, p1}, Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;->onVolumeChanged(F)V

    .line 245
    :cond_0
    return-void
.end method

.method public startBuffering()V
    .locals 0

    .prologue
    .line 297
    return-void
.end method

.method public stateChanged(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V
    .locals 2
    .param p1, "state"    # Lru/cn/player/AbstractMediaPlayer$PlayerState;

    .prologue
    .line 249
    sget-object v0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$1;->$SwitchMap$ru$cn$player$AbstractMediaPlayer$PlayerState:[I

    invoke-virtual {p1}, Lru/cn/player/AbstractMediaPlayer$PlayerState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 270
    :cond_0
    :goto_0
    return-void

    .line 251
    :pswitch_0
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->adPlayerListener:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->adPlayerListener:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;->onAdVideoStarted()V

    goto :goto_0

    .line 256
    :pswitch_1
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->adPlayerListener:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->adPlayerListener:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;->onAdVideoResumed()V

    goto :goto_0

    .line 261
    :pswitch_2
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->adPlayerListener:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->adPlayerListener:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;->onAdVideoPaused()V

    goto :goto_0

    .line 266
    :pswitch_3
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->adPlayerListener:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->adPlayerListener:Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;->onAdVideoStopped()V

    goto :goto_0

    .line 249
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public stopAdVideo()V
    .locals 1

    .prologue
    .line 223
    iget-boolean v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->destroyed:Z

    if-eqz v0, :cond_0

    .line 227
    :goto_0
    return-void

    .line 226
    :cond_0
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->stop()V

    goto :goto_0
.end method

.method public videoSizeChanged(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 292
    return-void
.end method
