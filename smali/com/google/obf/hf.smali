.class public Lcom/google/obf/hf;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/ho;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/hf$a;,
        Lcom/google/obf/hf$c;,
        Lcom/google/obf/hf$e;,
        Lcom/google/obf/hf$d;,
        Lcom/google/obf/hf$b;,
        Lcom/google/obf/hf$f;,
        Lcom/google/obf/hf$g;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/obf/h;

.field private final b:Landroid/view/SurfaceView;

.field private final c:Lcom/google/obf/a;

.field private final d:Landroid/widget/FrameLayout;

.field private final e:Landroid/view/ViewGroup;

.field private final f:Landroid/content/Context;

.field private final g:Landroid/os/Handler;

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/google/obf/hf$b;

.field private final j:Lcom/google/obf/hf$e;

.field private final k:Lcom/google/obf/hf$c;

.field private final l:Lcom/google/obf/hf$a;

.field private m:Lcom/google/obf/hf$f;

.field private n:Z

.field private o:Lcom/google/obf/x;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/obf/h$b;->a(I)Lcom/google/obf/h;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/obf/hf;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/google/obf/h;)V

    .line 2
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/google/obf/h;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, -0x1

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lcom/google/obf/hf;->f:Landroid/content/Context;

    .line 5
    iput-object p2, p0, Lcom/google/obf/hf;->e:Landroid/view/ViewGroup;

    .line 6
    iput-object p3, p0, Lcom/google/obf/hf;->a:Lcom/google/obf/h;

    .line 7
    new-instance v0, Lcom/google/obf/hf$b;

    invoke-direct {v0, p0}, Lcom/google/obf/hf$b;-><init>(Lcom/google/obf/hf;)V

    iput-object v0, p0, Lcom/google/obf/hf;->i:Lcom/google/obf/hf$b;

    .line 8
    new-instance v0, Lcom/google/obf/hf$c;

    invoke-direct {v0, p0}, Lcom/google/obf/hf$c;-><init>(Lcom/google/obf/hf;)V

    iput-object v0, p0, Lcom/google/obf/hf;->k:Lcom/google/obf/hf$c;

    .line 9
    new-instance v0, Lcom/google/obf/hf$e;

    invoke-direct {v0, p0}, Lcom/google/obf/hf$e;-><init>(Lcom/google/obf/hf;)V

    iput-object v0, p0, Lcom/google/obf/hf;->j:Lcom/google/obf/hf$e;

    .line 10
    new-instance v0, Lcom/google/obf/hf$a;

    invoke-direct {v0, p0}, Lcom/google/obf/hf$a;-><init>(Lcom/google/obf/hf;)V

    iput-object v0, p0, Lcom/google/obf/hf;->l:Lcom/google/obf/hf$a;

    .line 11
    iget-object v0, p0, Lcom/google/obf/hf;->l:Lcom/google/obf/hf$a;

    invoke-interface {p3, v0}, Lcom/google/obf/h;->a(Lcom/google/obf/h$c;)V

    .line 12
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/obf/hf;->g:Landroid/os/Handler;

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/hf;->h:Ljava/util/List;

    .line 14
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/obf/hf;->d:Landroid/widget/FrameLayout;

    .line 15
    iget-object v0, p0, Lcom/google/obf/hf;->d:Landroid/widget/FrameLayout;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 16
    new-instance v0, Lcom/google/obf/a;

    invoke-direct {v0, p1}, Lcom/google/obf/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/obf/hf;->c:Lcom/google/obf/a;

    .line 17
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 18
    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 19
    iget-object v1, p0, Lcom/google/obf/hf;->c:Lcom/google/obf/a;

    invoke-virtual {v1, v0}, Lcom/google/obf/a;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 20
    sget-object v0, Lcom/google/obf/hf$f;->a:Lcom/google/obf/hf$f;

    iput-object v0, p0, Lcom/google/obf/hf;->m:Lcom/google/obf/hf$f;

    .line 21
    new-instance v0, Landroid/view/SurfaceView;

    invoke-direct {v0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/obf/hf;->b:Landroid/view/SurfaceView;

    .line 22
    iget-object v0, p0, Lcom/google/obf/hf;->b:Landroid/view/SurfaceView;

    invoke-virtual {v0, v2}, Landroid/view/SurfaceView;->setZOrderMediaOverlay(Z)V

    .line 23
    iget-object v0, p0, Lcom/google/obf/hf;->b:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    new-instance v1, Lcom/google/obf/hf$1;

    invoke-direct {v1, p0, p3}, Lcom/google/obf/hf$1;-><init>(Lcom/google/obf/hf;Lcom/google/obf/h;)V

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 24
    iget-object v0, p0, Lcom/google/obf/hf;->c:Lcom/google/obf/a;

    iget-object v1, p0, Lcom/google/obf/hf;->b:Landroid/view/SurfaceView;

    invoke-virtual {v0, v1}, Lcom/google/obf/a;->addView(Landroid/view/View;)V

    .line 25
    iget-object v0, p0, Lcom/google/obf/hf;->d:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/obf/hf;->c:Lcom/google/obf/a;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 26
    iget-object v0, p0, Lcom/google/obf/hf;->e:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/obf/hf;->d:Landroid/widget/FrameLayout;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 27
    return-void
.end method

.method static synthetic a(Lcom/google/obf/hf;)Lcom/google/obf/hf$f;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/obf/hf;->m:Lcom/google/obf/hf$f;

    return-object v0
.end method

.method private a(Landroid/view/Surface;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 76
    iget-object v0, p0, Lcom/google/obf/hf;->a:Lcom/google/obf/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/hf;->o:Lcom/google/obf/x;

    if-nez v0, :cond_1

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    if-eqz p2, :cond_2

    .line 79
    iget-object v0, p0, Lcom/google/obf/hf;->a:Lcom/google/obf/h;

    iget-object v1, p0, Lcom/google/obf/hf;->o:Lcom/google/obf/x;

    invoke-interface {v0, v1, v2, p1}, Lcom/google/obf/h;->b(Lcom/google/obf/h$a;ILjava/lang/Object;)V

    goto :goto_0

    .line 80
    :cond_2
    iget-object v0, p0, Lcom/google/obf/hf;->a:Lcom/google/obf/h;

    iget-object v1, p0, Lcom/google/obf/hf;->o:Lcom/google/obf/x;

    invoke-interface {v0, v1, v2, p1}, Lcom/google/obf/h;->a(Lcom/google/obf/h$a;ILjava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/obf/hf;Landroid/view/Surface;Z)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0, p1, p2}, Lcom/google/obf/hf;->a(Landroid/view/Surface;Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/obf/hf;Z)Z
    .locals 0

    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/google/obf/hf;->n:Z

    return p1
.end method

.method static synthetic b(Lcom/google/obf/hf;)V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/google/obf/hf;->g()V

    return-void
.end method

.method static synthetic c(Lcom/google/obf/hf;)Lcom/google/obf/a;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/obf/hf;->c:Lcom/google/obf/a;

    return-object v0
.end method

.method static synthetic d(Lcom/google/obf/hf;)Ljava/util/List;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/obf/hf;->h:Ljava/util/List;

    return-object v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/obf/hf;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;

    .line 93
    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;->onError()V

    goto :goto_0

    .line 95
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 82
    iget-object v0, p0, Lcom/google/obf/hf;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 83
    iget-object v0, p0, Lcom/google/obf/hf;->b:Landroid/view/SurfaceView;

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setVisibility(I)V

    .line 84
    return-void
.end method

.method public addCallback(Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;)V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/obf/hf;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/obf/hf;->d:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 86
    iget-object v0, p0, Lcom/google/obf/hf;->b:Landroid/view/SurfaceView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setVisibility(I)V

    .line 87
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/obf/hf;->a:Lcom/google/obf/h;

    iget-object v1, p0, Lcom/google/obf/hf;->l:Lcom/google/obf/hf$a;

    invoke-interface {v0, v1}, Lcom/google/obf/h;->b(Lcom/google/obf/h$c;)V

    .line 89
    iget-object v0, p0, Lcom/google/obf/hf;->a:Lcom/google/obf/h;

    invoke-interface {v0}, Lcom/google/obf/h;->c()V

    .line 90
    iget-object v0, p0, Lcom/google/obf/hf;->e:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/obf/hf;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 91
    return-void
.end method

.method public d()Lcom/google/obf/hf$b;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/obf/hf;->i:Lcom/google/obf/hf$b;

    return-object v0
.end method

.method public e()Lcom/google/obf/hf$e;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/obf/hf;->j:Lcom/google/obf/hf$e;

    return-object v0
.end method

.method public f()Lcom/google/obf/hf$c;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/obf/hf;->k:Lcom/google/obf/hf$c;

    return-object v0
.end method

.method public getAdProgress()Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;
    .locals 6

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/obf/hf;->a:Lcom/google/obf/h;

    invoke-interface {v0}, Lcom/google/obf/h;->a()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/obf/hf;->a:Lcom/google/obf/h;

    .line 72
    invoke-interface {v0}, Lcom/google/obf/h;->a()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/obf/hf;->a:Lcom/google/obf/h;

    .line 73
    invoke-interface {v0}, Lcom/google/obf/h;->d()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_2

    .line 74
    :cond_1
    sget-object v0, Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;->VIDEO_TIME_NOT_READY:Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;

    .line 75
    :goto_0
    return-object v0

    :cond_2
    new-instance v0, Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;

    iget-object v1, p0, Lcom/google/obf/hf;->a:Lcom/google/obf/h;

    invoke-interface {v1}, Lcom/google/obf/h;->e()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/obf/hf;->a:Lcom/google/obf/h;

    invoke-interface {v1}, Lcom/google/obf/h;->d()J

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;-><init>(JJ)V

    goto :goto_0
.end method

.method public loadAd(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/obf/hf;->a:Lcom/google/obf/h;

    invoke-interface {v0}, Lcom/google/obf/h;->b()V

    .line 46
    iget-object v0, p0, Lcom/google/obf/hf;->a:Lcom/google/obf/h;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v2, v3}, Lcom/google/obf/h;->a(J)V

    .line 47
    new-instance v0, Lcom/google/obf/hu;

    iget-object v1, p0, Lcom/google/obf/hf;->f:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/obf/hf;->f:Landroid/content/Context;

    const-string v3, "IMA SDK ExoPlayer"

    .line 48
    invoke-static {v2, v3}, Lcom/google/obf/ea;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 49
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/obf/hu;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;)V

    .line 50
    iget-object v1, p0, Lcom/google/obf/hf;->g:Landroid/os/Handler;

    invoke-virtual {v0, p0, v1}, Lcom/google/obf/hu;->a(Lcom/google/obf/hf;Landroid/os/Handler;)[Lcom/google/obf/x;

    move-result-object v0

    .line 51
    sget-object v1, Lcom/google/obf/hf$g;->a:Lcom/google/obf/hf$g;

    invoke-virtual {v1}, Lcom/google/obf/hf$g;->a()I

    move-result v1

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/google/obf/hf;->o:Lcom/google/obf/x;

    .line 52
    iget-object v1, p0, Lcom/google/obf/hf;->a:Lcom/google/obf/h;

    invoke-interface {v1, v0}, Lcom/google/obf/h;->a([Lcom/google/obf/x;)V

    .line 53
    sget-object v0, Lcom/google/obf/hf$f;->b:Lcom/google/obf/hf$f;

    iput-object v0, p0, Lcom/google/obf/hf;->m:Lcom/google/obf/hf$f;

    .line 54
    return-void
.end method

.method public pauseAd()V
    .locals 2

    .prologue
    .line 59
    sget-object v0, Lcom/google/obf/hf$f;->d:Lcom/google/obf/hf$f;

    iput-object v0, p0, Lcom/google/obf/hf;->m:Lcom/google/obf/hf$f;

    .line 60
    iget-object v0, p0, Lcom/google/obf/hf;->a:Lcom/google/obf/h;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/obf/h;->a(Z)V

    .line 61
    iget-object v0, p0, Lcom/google/obf/hf;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;

    .line 62
    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;->onPause()V

    goto :goto_0

    .line 64
    :cond_0
    return-void
.end method

.method public playAd()V
    .locals 4

    .prologue
    .line 28
    sget-object v0, Lcom/google/obf/hf$2;->a:[I

    iget-object v1, p0, Lcom/google/obf/hf;->m:Lcom/google/obf/hf$f;

    invoke-virtual {v1}, Lcom/google/obf/hf$f;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 39
    const-string v0, "IMA SDK"

    iget-object v1, p0, Lcom/google/obf/hf;->m:Lcom/google/obf/hf$f;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x35

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Ignoring call to playAd during invalid player state: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 29
    :pswitch_1
    iget-object v0, p0, Lcom/google/obf/hf;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;

    .line 30
    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;->onPlay()V

    goto :goto_1

    .line 32
    :cond_1
    iget-object v0, p0, Lcom/google/obf/hf;->b:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/obf/hf;->a(Landroid/view/Surface;Z)V

    .line 41
    :cond_2
    sget-object v0, Lcom/google/obf/hf$f;->c:Lcom/google/obf/hf$f;

    iput-object v0, p0, Lcom/google/obf/hf;->m:Lcom/google/obf/hf$f;

    .line 42
    iget-boolean v0, p0, Lcom/google/obf/hf;->n:Z

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/google/obf/hf;->a:Lcom/google/obf/h;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/obf/h;->a(Z)V

    goto :goto_0

    .line 34
    :pswitch_2
    iget-object v0, p0, Lcom/google/obf/hf;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;

    .line 35
    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;->onResume()V

    goto :goto_2

    .line 28
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public removeCallback(Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;)V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/obf/hf;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 70
    return-void
.end method

.method public resumeAd()V
    .locals 0

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/google/obf/hf;->playAd()V

    .line 66
    return-void
.end method

.method public stopAd()V
    .locals 2

    .prologue
    .line 55
    sget-object v0, Lcom/google/obf/hf$f;->a:Lcom/google/obf/hf$f;

    iput-object v0, p0, Lcom/google/obf/hf;->m:Lcom/google/obf/hf$f;

    .line 56
    iget-object v0, p0, Lcom/google/obf/hf;->a:Lcom/google/obf/h;

    invoke-interface {v0}, Lcom/google/obf/h;->b()V

    .line 57
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/obf/hf;->a(Landroid/view/Surface;Z)V

    .line 58
    return-void
.end method
