.class public final Lcom/my/target/cv;
.super Lcom/my/target/cc;
.source "NativeAdVideoTextureView.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/cv$a;
    }
.end annotation


# static fields
.field private static V:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Lcom/my/target/cv;",
            ">;"
        }
    .end annotation
.end field

.field private static W:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/my/target/core/controllers/a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a_:Ljava/lang/Runnable;

.field private final b_:Lcom/my/target/ck;

.field private c_:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/my/target/cc;-><init>(Landroid/content/Context;)V

    .line 79
    const/16 v0, 0xc8

    invoke-static {v0}, Lcom/my/target/ck;->k(I)Lcom/my/target/ck;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/cv;->b_:Lcom/my/target/ck;

    .line 80
    new-instance v0, Lcom/my/target/cv$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/my/target/cv$a;-><init>(Lcom/my/target/cv;B)V

    iput-object v0, p0, Lcom/my/target/cv;->a_:Ljava/lang/Runnable;

    .line 81
    iget-object v0, p0, Lcom/my/target/cv;->b_:Lcom/my/target/ck;

    iget-object v1, p0, Lcom/my/target/cv;->a_:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->d(Ljava/lang/Runnable;)V

    .line 82
    return-void
.end method

.method static synthetic a(Lcom/my/target/cv;)V
    .locals 0

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/my/target/cv;->bn()V

    return-void
.end method

.method public static b(Lcom/my/target/core/controllers/a;Landroid/content/Context;)Lcom/my/target/cv;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 28
    .line 29
    sget-object v0, Lcom/my/target/cv;->V:Ljava/lang/ref/SoftReference;

    if-eqz v0, :cond_3

    .line 31
    sget-object v0, Lcom/my/target/cv;->V:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/cv;

    .line 32
    if-eqz v0, :cond_0

    .line 34
    invoke-virtual {v0}, Lcom/my/target/cv;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eq v1, p1, :cond_0

    .line 36
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/my/target/cv;->k(Z)V

    .line 40
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 42
    new-instance v0, Lcom/my/target/cv;

    invoke-direct {v0, p1}, Lcom/my/target/cv;-><init>(Landroid/content/Context;)V

    move-object v1, v0

    .line 44
    :goto_1
    new-instance v0, Ljava/lang/ref/SoftReference;

    invoke-direct {v0, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/my/target/cv;->V:Ljava/lang/ref/SoftReference;

    .line 47
    sget-object v0, Lcom/my/target/cv;->W:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    .line 49
    sget-object v0, Lcom/my/target/cv;->W:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/controllers/a;

    .line 50
    sget-object v3, Lcom/my/target/cv;->W:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->clear()V

    .line 51
    sput-object v2, Lcom/my/target/cv;->W:Ljava/lang/ref/WeakReference;

    .line 52
    if-eqz v0, :cond_1

    .line 54
    invoke-virtual {v0}, Lcom/my/target/core/controllers/a;->h()V

    .line 58
    :cond_1
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/my/target/cv;->W:Ljava/lang/ref/WeakReference;

    .line 60
    return-object v1

    :cond_2
    move-object v1, v0

    goto :goto_1

    :cond_3
    move-object v0, v2

    goto :goto_0
.end method

.method public static o(Lcom/my/target/core/controllers/a;)V
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/my/target/cv;->W:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/my/target/cv;->W:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 67
    sget-object v0, Lcom/my/target/cv;->W:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    .line 68
    const/4 v0, 0x0

    sput-object v0, Lcom/my/target/cv;->W:Ljava/lang/ref/WeakReference;

    .line 70
    :cond_0
    return-void
.end method

.method private x()V
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/cv;->c_:Z

    .line 132
    iget-object v0, p0, Lcom/my/target/cv;->a_:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/my/target/cv;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 133
    return-void
.end method

.method private y()V
    .locals 2

    .prologue
    .line 137
    iget-boolean v0, p0, Lcom/my/target/cv;->c_:Z

    if-nez v0, :cond_0

    .line 139
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/cv;->c_:Z

    .line 140
    iget-object v0, p0, Lcom/my/target/cv;->b_:Lcom/my/target/ck;

    iget-object v1, p0, Lcom/my/target/cv;->a_:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->d(Ljava/lang/Runnable;)V

    .line 142
    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(Landroid/media/MediaPlayer;)V
    .locals 0
    .param p1, "mediaPlayer"    # Landroid/media/MediaPlayer;

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/my/target/cv;->y()V

    .line 126
    invoke-super {p0, p1}, Lcom/my/target/cc;->a(Landroid/media/MediaPlayer;)V

    .line 127
    return-void
.end method

.method protected final a(Landroid/view/Surface;)V
    .locals 0
    .param p1, "s"    # Landroid/view/Surface;

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/my/target/cv;->y()V

    .line 109
    invoke-super {p0, p1}, Lcom/my/target/cc;->a(Landroid/view/Surface;)V

    .line 110
    return-void
.end method

.method protected final a(Landroid/view/Surface;Landroid/net/Uri;)V
    .locals 0
    .param p1, "s"    # Landroid/view/Surface;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 115
    if-eqz p1, :cond_0

    .line 117
    invoke-direct {p0}, Lcom/my/target/cv;->y()V

    .line 119
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/my/target/cc;->a(Landroid/view/Surface;Landroid/net/Uri;)V

    .line 120
    return-void
.end method

.method public final bm()V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/my/target/cv;->x()V

    .line 95
    invoke-super {p0}, Lcom/my/target/cc;->bm()V

    .line 96
    return-void
.end method

.method public final j(Z)V
    .locals 0
    .param p1, "updateScreenshot"    # Z

    .prologue
    .line 100
    invoke-super {p0, p1}, Lcom/my/target/cc;->j(Z)V

    .line 101
    invoke-direct {p0}, Lcom/my/target/cv;->x()V

    .line 102
    invoke-virtual {p0}, Lcom/my/target/cv;->pause()V

    .line 103
    return-void
.end method

.method public final k(Z)V
    .locals 0
    .param p1, "notifyListener"    # Z

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/my/target/cv;->x()V

    .line 88
    invoke-super {p0, p1}, Lcom/my/target/cc;->k(Z)V

    .line 89
    return-void
.end method
