.class public Lcom/my/target/nativeads/views/MediaAdView;
.super Landroid/widget/RelativeLayout;
.source "MediaAdView.java"


# static fields
.field private static final BUTTON_ID:I

.field public static final COLOR_PLACEHOLDER_GRAY:I = -0x111112

.field private static final IMAGE_ID:I

.field private static final PROGRESS_ID:I


# instance fields
.field private final imageView:Lcom/my/target/bv;

.field private placeholderHeight:I

.field private placeholderWidth:I

.field private final playButton:Lcom/my/target/bv;

.field private final progressBar:Landroid/widget/ProgressBar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/MediaAdView;->IMAGE_ID:I

    .line 25
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/MediaAdView;->BUTTON_ID:I

    .line 26
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/MediaAdView;->PROGRESS_ID:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 37
    new-instance v0, Lcom/my/target/bv;

    invoke-direct {v0, p1}, Lcom/my/target/bv;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/MediaAdView;->imageView:Lcom/my/target/bv;

    .line 38
    new-instance v0, Lcom/my/target/bv;

    invoke-direct {v0, p1}, Lcom/my/target/bv;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/MediaAdView;->playButton:Lcom/my/target/bv;

    .line 39
    new-instance v0, Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    const v2, 0x1010077

    invoke-direct {v0, p1, v1, v2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/MediaAdView;->progressBar:Landroid/widget/ProgressBar;

    .line 40
    invoke-direct {p0, p1}, Lcom/my/target/nativeads/views/MediaAdView;->initViews(Landroid/content/Context;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    new-instance v0, Lcom/my/target/bv;

    invoke-direct {v0, p1}, Lcom/my/target/bv;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/MediaAdView;->imageView:Lcom/my/target/bv;

    .line 47
    new-instance v0, Lcom/my/target/bv;

    invoke-direct {v0, p1}, Lcom/my/target/bv;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/MediaAdView;->playButton:Lcom/my/target/bv;

    .line 48
    new-instance v0, Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    const v2, 0x1010077

    invoke-direct {v0, p1, v1, v2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/MediaAdView;->progressBar:Landroid/widget/ProgressBar;

    .line 49
    invoke-direct {p0, p1}, Lcom/my/target/nativeads/views/MediaAdView;->initViews(Landroid/content/Context;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    new-instance v0, Lcom/my/target/bv;

    invoke-direct {v0, p1}, Lcom/my/target/bv;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/MediaAdView;->imageView:Lcom/my/target/bv;

    .line 56
    new-instance v0, Lcom/my/target/bv;

    invoke-direct {v0, p1}, Lcom/my/target/bv;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/MediaAdView;->playButton:Lcom/my/target/bv;

    .line 57
    new-instance v0, Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    const v2, 0x1010077

    invoke-direct {v0, p1, v1, v2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/MediaAdView;->progressBar:Landroid/widget/ProgressBar;

    .line 58
    invoke-direct {p0, p1}, Lcom/my/target/nativeads/views/MediaAdView;->initViews(Landroid/content/Context;)V

    .line 59
    return-void
.end method

.method private initViews(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v7, 0xd

    const/16 v6, 0x8

    const/4 v5, -0x1

    const/4 v4, -0x2

    .line 201
    iget-object v0, p0, Lcom/my/target/nativeads/views/MediaAdView;->imageView:Lcom/my/target/bv;

    const-string v1, "media_image"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, Lcom/my/target/nativeads/views/MediaAdView;->imageView:Lcom/my/target/bv;

    sget v1, Lcom/my/target/nativeads/views/MediaAdView;->IMAGE_ID:I

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setId(I)V

    .line 203
    iget-object v0, p0, Lcom/my/target/nativeads/views/MediaAdView;->progressBar:Landroid/widget/ProgressBar;

    const-string v1, "progress_bar"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 204
    iget-object v0, p0, Lcom/my/target/nativeads/views/MediaAdView;->progressBar:Landroid/widget/ProgressBar;

    sget v1, Lcom/my/target/nativeads/views/MediaAdView;->PROGRESS_ID:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setId(I)V

    .line 205
    iget-object v0, p0, Lcom/my/target/nativeads/views/MediaAdView;->playButton:Lcom/my/target/bv;

    const-string v1, "play_button"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Lcom/my/target/nativeads/views/MediaAdView;->playButton:Lcom/my/target/bv;

    sget v1, Lcom/my/target/nativeads/views/MediaAdView;->BUTTON_ID:I

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setId(I)V

    .line 208
    const v0, -0x111112

    invoke-virtual {p0, v0}, Lcom/my/target/nativeads/views/MediaAdView;->setBackgroundColor(I)V

    .line 209
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 211
    iget-object v1, p0, Lcom/my/target/nativeads/views/MediaAdView;->imageView:Lcom/my/target/bv;

    invoke-virtual {p0, v1, v0}, Lcom/my/target/nativeads/views/MediaAdView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 212
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 214
    invoke-virtual {v0, v7, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 215
    iget-object v1, p0, Lcom/my/target/nativeads/views/MediaAdView;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 216
    iget-object v1, p0, Lcom/my/target/nativeads/views/MediaAdView;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 217
    const v2, -0xff540e

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 219
    iget-object v1, p0, Lcom/my/target/nativeads/views/MediaAdView;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v1, v0}, Lcom/my/target/nativeads/views/MediaAdView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 220
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    .line 222
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 224
    invoke-virtual {v1, v7, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 225
    iget-object v2, p0, Lcom/my/target/nativeads/views/MediaAdView;->playButton:Lcom/my/target/bv;

    const/16 v3, 0x40

    invoke-virtual {v0, v3}, Lcom/my/target/cm;->n(I)I

    move-result v0

    invoke-static {v0}, Lcom/my/target/core/resources/a;->getPlayIcon(I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/my/target/bv;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 226
    iget-object v0, p0, Lcom/my/target/nativeads/views/MediaAdView;->playButton:Lcom/my/target/bv;

    invoke-virtual {v0, v6}, Lcom/my/target/bv;->setVisibility(I)V

    .line 227
    iget-object v0, p0, Lcom/my/target/nativeads/views/MediaAdView;->playButton:Lcom/my/target/bv;

    invoke-virtual {p0, v0, v1}, Lcom/my/target/nativeads/views/MediaAdView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 228
    return-void
.end method


# virtual methods
.method public getImageView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/my/target/nativeads/views/MediaAdView;->imageView:Lcom/my/target/bv;

    return-object v0
.end method

.method public getPlayButtonView()Landroid/view/View;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/my/target/nativeads/views/MediaAdView;->playButton:Lcom/my/target/bv;

    return-object v0
.end method

.method public getProgressBarView()Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/my/target/nativeads/views/MediaAdView;->progressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v3, 0x0

    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v1, -0x80000000

    .line 93
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 94
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 95
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 96
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 98
    if-nez v5, :cond_d

    move v6, v1

    .line 103
    :goto_0
    if-nez v0, :cond_0

    move v0, v1

    .line 108
    :cond_0
    iget v5, p0, Lcom/my/target/nativeads/views/MediaAdView;->placeholderHeight:I

    if-eqz v5, :cond_1

    iget v5, p0, Lcom/my/target/nativeads/views/MediaAdView;->placeholderWidth:I

    if-nez v5, :cond_2

    .line 110
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 197
    :goto_1
    return-void

    .line 114
    :cond_2
    iget v5, p0, Lcom/my/target/nativeads/views/MediaAdView;->placeholderWidth:I

    int-to-float v5, v5

    iget v7, p0, Lcom/my/target/nativeads/views/MediaAdView;->placeholderHeight:I

    int-to-float v7, v7

    div-float v7, v5, v7

    .line 116
    const/4 v5, 0x0

    .line 117
    if-eqz v4, :cond_3

    .line 119
    int-to-float v5, v2

    int-to-float v8, v4

    div-float/2addr v5, v8

    .line 125
    :cond_3
    if-ne v6, v9, :cond_4

    if-ne v0, v9, :cond_4

    move v0, v2

    move v3, v4

    .line 194
    :goto_2
    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 195
    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 196
    invoke-super {p0, v0, v1}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    goto :goto_1

    .line 130
    :cond_4
    if-ne v6, v1, :cond_8

    if-ne v0, v1, :cond_8

    .line 132
    cmpg-float v0, v7, v5

    if-gez v0, :cond_6

    .line 134
    int-to-float v0, v4

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 136
    if-lez v2, :cond_5

    if-le v0, v2, :cond_5

    .line 138
    int-to-float v0, v2

    div-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    move v0, v2

    .line 139
    goto :goto_2

    :cond_5
    move v3, v4

    .line 146
    goto :goto_2

    .line 149
    :cond_6
    int-to-float v0, v2

    div-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 151
    if-lez v4, :cond_7

    if-le v3, v4, :cond_7

    .line 153
    int-to-float v0, v4

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    move v3, v4

    .line 154
    goto :goto_2

    :cond_7
    move v0, v2

    .line 161
    goto :goto_2

    .line 163
    :cond_8
    if-ne v6, v1, :cond_a

    if-ne v0, v9, :cond_a

    .line 165
    int-to-float v0, v4

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 167
    if-lez v2, :cond_9

    if-le v0, v2, :cond_9

    .line 169
    int-to-float v0, v2

    div-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    move v0, v2

    .line 170
    goto :goto_2

    :cond_9
    move v3, v4

    .line 177
    goto :goto_2

    .line 178
    :cond_a
    if-ne v6, v9, :cond_c

    if-ne v0, v1, :cond_c

    .line 180
    int-to-float v0, v2

    div-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 182
    if-lez v4, :cond_b

    if-le v3, v4, :cond_b

    .line 184
    int-to-float v0, v4

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    move v3, v4

    .line 185
    goto :goto_2

    :cond_b
    move v0, v2

    .line 190
    goto :goto_2

    :cond_c
    move v0, v3

    goto :goto_2

    :cond_d
    move v6, v5

    goto/16 :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .prologue
    .line 80
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 81
    const/4 v0, 0x1

    return v0
.end method

.method public setPlaceHolderDimension(II)V
    .locals 0
    .param p1, "placeholderWidth"    # I
    .param p2, "placeholderHeight"    # I

    .prologue
    .line 86
    iput p1, p0, Lcom/my/target/nativeads/views/MediaAdView;->placeholderWidth:I

    .line 87
    iput p2, p0, Lcom/my/target/nativeads/views/MediaAdView;->placeholderHeight:I

    .line 88
    return-void
.end method
