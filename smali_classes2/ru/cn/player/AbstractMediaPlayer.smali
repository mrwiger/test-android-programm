.class public abstract Lru/cn/player/AbstractMediaPlayer;
.super Ljava/lang/Object;
.source "AbstractMediaPlayer.java"

# interfaces
.implements Lru/cn/player/HeadsetReceiver$AudioBecomingNoisyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/player/AbstractMediaPlayer$Accessibility;,
        Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;,
        Lru/cn/player/AbstractMediaPlayer$PlayerState;
    }
.end annotation


# instance fields
.field protected final LOG_TAG:Ljava/lang/String;

.field protected final accessibility:Lru/cn/player/AbstractMediaPlayer$Accessibility;

.field protected final audioFocus:Lru/cn/player/AudioFocus;

.field protected final context:Landroid/content/Context;

.field private currentState:Lru/cn/player/AbstractMediaPlayer$PlayerState;

.field private isBuffering:Z

.field protected listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

.field private mBufferingListener:Lru/cn/player/BufferingListener;

.field protected mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private metadataListener:Lru/cn/player/metadata/MetadataListener;

.field private pauseOnHeadsetUnplug:Z

.field protected final playerTimeoutHandler:Lru/cn/player/TimeoutHandler;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lru/cn/player/AbstractMediaPlayer;->LOG_TAG:Ljava/lang/String;

    .line 58
    const/4 v2, 0x1

    iput-boolean v2, p0, Lru/cn/player/AbstractMediaPlayer;->pauseOnHeadsetUnplug:Z

    .line 60
    sget-object v2, Lru/cn/player/AbstractMediaPlayer$PlayerState;->STOPPED:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    iput-object v2, p0, Lru/cn/player/AbstractMediaPlayer;->currentState:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    .line 63
    iput-object p1, p0, Lru/cn/player/AbstractMediaPlayer;->context:Landroid/content/Context;

    .line 64
    new-instance v2, Lru/cn/player/AbstractMediaPlayer$Accessibility;

    invoke-direct {v2}, Lru/cn/player/AbstractMediaPlayer$Accessibility;-><init>()V

    iput-object v2, p0, Lru/cn/player/AbstractMediaPlayer;->accessibility:Lru/cn/player/AbstractMediaPlayer$Accessibility;

    .line 66
    new-instance v0, Lru/cn/player/HeadsetReceiver;

    invoke-direct {v0}, Lru/cn/player/HeadsetReceiver;-><init>()V

    .line 67
    .local v0, "headsetReceiver":Lru/cn/player/HeadsetReceiver;
    invoke-virtual {v0, p0}, Lru/cn/player/HeadsetReceiver;->setAudioBecomingNoisyListener(Lru/cn/player/HeadsetReceiver$AudioBecomingNoisyListener;)V

    .line 69
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v1

    .line 70
    .local v1, "pauseOnTransientLoss":Z
    new-instance v2, Lru/cn/player/AudioFocus;

    invoke-direct {v2, p1, p0, v0, v1}, Lru/cn/player/AudioFocus;-><init>(Landroid/content/Context;Lru/cn/player/AbstractMediaPlayer;Lru/cn/player/HeadsetReceiver;Z)V

    iput-object v2, p0, Lru/cn/player/AbstractMediaPlayer;->audioFocus:Lru/cn/player/AudioFocus;

    .line 72
    new-instance v2, Lru/cn/player/TimeoutHandler;

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    invoke-direct {v2, v3}, Lru/cn/player/TimeoutHandler;-><init>(Landroid/os/Handler;)V

    iput-object v2, p0, Lru/cn/player/AbstractMediaPlayer;->playerTimeoutHandler:Lru/cn/player/TimeoutHandler;

    .line 73
    return-void
.end method


# virtual methods
.method protected clearSurface()V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lru/cn/player/AbstractMediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-nez v0, :cond_1

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    iget-object v0, p0, Lru/cn/player/AbstractMediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v1, -0x2

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 107
    iget-object v0, p0, Lru/cn/player/AbstractMediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lru/cn/player/AbstractMediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v1, -0x1

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    goto :goto_0
.end method

.method public abstract destroy()V
.end method

.method public getAudioTrackProvider()Lru/cn/player/AudioTrackSelector;
    .locals 1

    .prologue
    .line 194
    const/4 v0, 0x0

    return-object v0
.end method

.method public getBufferPosition()J
    .locals 2

    .prologue
    .line 171
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public abstract getCurrentPosition()I
.end method

.method public getDateTime()J
    .locals 2

    .prologue
    .line 179
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public abstract getDuration()I
.end method

.method public getState()Lru/cn/player/AbstractMediaPlayer$PlayerState;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lru/cn/player/AbstractMediaPlayer;->currentState:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    return-object v0
.end method

.method public getSubtitleTracksProvider()Lru/cn/player/SubtitleTrackSelector;
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSubtitleView()Landroid/view/View;
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x0

    return-object v0
.end method

.method public getVideoTracksProvider()Lru/cn/player/VideoTrackSelector;
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final notifyBuffering(Z)V
    .locals 1
    .param p1, "isBuffering"    # Z

    .prologue
    .line 145
    iget-boolean v0, p0, Lru/cn/player/AbstractMediaPlayer;->isBuffering:Z

    if-ne v0, p1, :cond_1

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    iput-boolean p1, p0, Lru/cn/player/AbstractMediaPlayer;->isBuffering:Z

    .line 150
    iget-object v0, p0, Lru/cn/player/AbstractMediaPlayer;->mBufferingListener:Lru/cn/player/BufferingListener;

    if-eqz v0, :cond_0

    .line 151
    if-eqz p1, :cond_2

    .line 152
    iget-object v0, p0, Lru/cn/player/AbstractMediaPlayer;->mBufferingListener:Lru/cn/player/BufferingListener;

    invoke-interface {v0}, Lru/cn/player/BufferingListener;->startBuffering()V

    goto :goto_0

    .line 154
    :cond_2
    iget-object v0, p0, Lru/cn/player/AbstractMediaPlayer;->mBufferingListener:Lru/cn/player/BufferingListener;

    invoke-interface {v0}, Lru/cn/player/BufferingListener;->endBuffering()V

    goto :goto_0
.end method

.method public onAudioBecomingNoisy()V
    .locals 1

    .prologue
    .line 204
    iget-boolean v0, p0, Lru/cn/player/AbstractMediaPlayer;->pauseOnHeadsetUnplug:Z

    if-nez v0, :cond_0

    .line 209
    :goto_0
    return-void

    .line 208
    :cond_0
    invoke-virtual {p0}, Lru/cn/player/AbstractMediaPlayer;->pause()V

    goto :goto_0
.end method

.method protected final onMetadataItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/player/metadata/MetadataItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 164
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lru/cn/player/metadata/MetadataItem;>;"
    iget-object v0, p0, Lru/cn/player/AbstractMediaPlayer;->metadataListener:Lru/cn/player/metadata/MetadataListener;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lru/cn/player/AbstractMediaPlayer;->metadataListener:Lru/cn/player/metadata/MetadataListener;

    invoke-interface {v0, p1}, Lru/cn/player/metadata/MetadataListener;->onMetadataItems(Ljava/util/List;)V

    .line 167
    :cond_0
    return-void
.end method

.method public abstract pause()V
.end method

.method public abstract play(Landroid/net/Uri;)V
.end method

.method public abstract resume()V
.end method

.method public abstract seekTo(I)V
.end method

.method final setBufferingListener(Lru/cn/player/BufferingListener;)V
    .locals 0
    .param p1, "bufferingListener"    # Lru/cn/player/BufferingListener;

    .prologue
    .line 141
    iput-object p1, p0, Lru/cn/player/AbstractMediaPlayer;->mBufferingListener:Lru/cn/player/BufferingListener;

    .line 142
    return-void
.end method

.method public abstract setChangeQualityListener(Lru/cn/player/ChangeQualityListener;)V
.end method

.method public setDisplay(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1, "surfaceHolder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 117
    iput-object p1, p0, Lru/cn/player/AbstractMediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 118
    return-void
.end method

.method public setListener(Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;)V
    .locals 0
    .param p1, "l"    # Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    .prologue
    .line 76
    iput-object p1, p0, Lru/cn/player/AbstractMediaPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    .line 77
    return-void
.end method

.method final setMetadataListener(Lru/cn/player/metadata/MetadataListener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/player/metadata/MetadataListener;

    .prologue
    .line 160
    iput-object p1, p0, Lru/cn/player/AbstractMediaPlayer;->metadataListener:Lru/cn/player/metadata/MetadataListener;

    .line 161
    return-void
.end method

.method setNeedHeadsetUnpluggedHandle(Z)V
    .locals 0
    .param p1, "needHandle"    # Z

    .prologue
    .line 95
    iput-boolean p1, p0, Lru/cn/player/AbstractMediaPlayer;->pauseOnHeadsetUnplug:Z

    .line 96
    return-void
.end method

.method protected setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V
    .locals 3
    .param p1, "state"    # Lru/cn/player/AbstractMediaPlayer$PlayerState;

    .prologue
    .line 81
    iget-object v0, p0, Lru/cn/player/AbstractMediaPlayer;->currentState:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    if-eq v0, p1, :cond_0

    .line 82
    iput-object p1, p0, Lru/cn/player/AbstractMediaPlayer;->currentState:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    .line 84
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lru/cn/player/AbstractMediaPlayer;->notifyBuffering(Z)V

    .line 86
    iget-object v0, p0, Lru/cn/player/AbstractMediaPlayer;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "State changed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lru/cn/player/AbstractMediaPlayer;->currentState:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lru/cn/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lru/cn/player/AbstractMediaPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lru/cn/player/AbstractMediaPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    iget-object v1, p0, Lru/cn/player/AbstractMediaPlayer;->currentState:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    invoke-interface {v0, v1}, Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;->onStateChanged(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    .line 92
    :cond_0
    return-void
.end method

.method public abstract setVolume(F)V
.end method

.method public abstract stop()V
.end method
