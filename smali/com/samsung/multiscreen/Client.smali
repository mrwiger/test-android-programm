.class public Lcom/samsung/multiscreen/Client;
.super Ljava/lang/Object;
.source "Client.java"


# static fields
.field private static final ATTRIBUTES_KEY:Ljava/lang/String; = "attributes"

.field private static final CONNECT_TIME_KEY:Ljava/lang/String; = "connectTime"

.field private static final ID_KEY:Ljava/lang/String; = "id"

.field private static final IS_HOST_KEY:Ljava/lang/String; = "isHost"


# instance fields
.field private final attributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final channel:Lcom/samsung/multiscreen/Channel;

.field private final connectTime:J

.field private final host:Z

.field private final id:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/samsung/multiscreen/Channel;Ljava/lang/String;ZJLjava/util/Map;)V
    .locals 0
    .param p1, "channel"    # Lcom/samsung/multiscreen/Channel;
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "host"    # Z
    .param p4, "connectTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/multiscreen/Channel;",
            "Ljava/lang/String;",
            "ZJ",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p6, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/samsung/multiscreen/Client;->channel:Lcom/samsung/multiscreen/Channel;

    iput-object p2, p0, Lcom/samsung/multiscreen/Client;->id:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/samsung/multiscreen/Client;->host:Z

    iput-wide p4, p0, Lcom/samsung/multiscreen/Client;->connectTime:J

    iput-object p6, p0, Lcom/samsung/multiscreen/Client;->attributes:Ljava/util/Map;

    return-void
.end method

.method protected static create(Lcom/samsung/multiscreen/Channel;Ljava/util/Map;)Lcom/samsung/multiscreen/Client;
    .locals 9
    .param p0, "channel"    # Lcom/samsung/multiscreen/Channel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/multiscreen/Channel;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/samsung/multiscreen/Client;"
        }
    .end annotation

    .prologue
    .line 80
    .local p1, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "id"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 81
    .local v2, "id":Ljava/lang/String;
    const-string v0, "isHost"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    .line 82
    .local v8, "host":Ljava/lang/Boolean;
    const-string v0, "connectTime"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    .line 83
    .local v7, "connectTime":Ljava/lang/Long;
    const-string v0, "attributes"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v6

    .line 85
    .local v6, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Lcom/samsung/multiscreen/Client;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/multiscreen/Client;-><init>(Lcom/samsung/multiscreen/Channel;Ljava/lang/String;ZJLjava/util/Map;)V

    return-object v0
.end method


# virtual methods
.method protected canEqual(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 45
    instance-of v0, p1, Lcom/samsung/multiscreen/Client;

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 45
    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    instance-of v5, p1, Lcom/samsung/multiscreen/Client;

    if-nez v5, :cond_2

    move v3, v4

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/samsung/multiscreen/Client;

    .local v0, "other":Lcom/samsung/multiscreen/Client;
    invoke-virtual {v0, p0}, Lcom/samsung/multiscreen/Client;->canEqual(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    move v3, v4

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Client;->getId()Ljava/lang/String;

    move-result-object v2

    .local v2, "this$id":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/samsung/multiscreen/Client;->getId()Ljava/lang/String;

    move-result-object v1

    .local v1, "other$id":Ljava/lang/String;
    if-nez v2, :cond_4

    if-eqz v1, :cond_0

    :goto_1
    move v3, v4

    goto :goto_0

    :cond_4
    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    goto :goto_1
.end method

.method public getAttributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/multiscreen/Client;->attributes:Ljava/util/Map;

    return-object v0
.end method

.method public getChannel()Lcom/samsung/multiscreen/Channel;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/multiscreen/Client;->channel:Lcom/samsung/multiscreen/Channel;

    return-object v0
.end method

.method public getConnectTime()J
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/samsung/multiscreen/Client;->connectTime:J

    return-wide v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/multiscreen/Client;->id:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 45
    const/16 v1, 0x3b

    .local v1, "PRIME":I
    const/4 v2, 0x1

    .local v2, "result":I
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Client;->getId()Ljava/lang/String;

    move-result-object v0

    .local v0, "$id":Ljava/lang/String;
    if-nez v0, :cond_0

    const/4 v3, 0x0

    :goto_0
    add-int/lit8 v2, v3, 0x3b

    return v2

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    goto :goto_0
.end method

.method public isHost()Z
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/samsung/multiscreen/Client;->host:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Client(id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Client;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", host="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Client;->isHost()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", connectTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Client;->getConnectTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", attributes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Client;->getAttributes()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
