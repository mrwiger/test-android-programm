.class Lru/cn/tv/stb/StbActivity$37$1;
.super Ljava/lang/Object;
.source "StbActivity.java"

# interfaces
.implements Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$PinCodeDialogCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/stb/StbActivity$37;->userPinCodeExist()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lru/cn/tv/stb/StbActivity$37;


# direct methods
.method constructor <init>(Lru/cn/tv/stb/StbActivity$37;)V
    .locals 0
    .param p1, "this$1"    # Lru/cn/tv/stb/StbActivity$37;

    .prologue
    .line 2491
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity$37$1;->this$1:Lru/cn/tv/stb/StbActivity$37;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public noButtonClick()V
    .locals 1

    .prologue
    .line 2502
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$37$1;->this$1:Lru/cn/tv/stb/StbActivity$37;

    iget-object v0, v0, Lru/cn/tv/stb/StbActivity$37;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$6800(Lru/cn/tv/stb/StbActivity;)V

    .line 2503
    return-void
.end method

.method public okButtonClickWithValidData()V
    .locals 4

    .prologue
    .line 2494
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$37$1;->this$1:Lru/cn/tv/stb/StbActivity$37;

    iget-object v1, v1, Lru/cn/tv/stb/StbActivity$37;->this$0:Lru/cn/tv/stb/StbActivity;

    const-string v2, "pin_disabled"

    invoke-static {v1, v2}, Lru/cn/domain/Preferences;->getBoolean(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 2495
    .local v0, "isPinDisabled":Z
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$37$1;->this$1:Lru/cn/tv/stb/StbActivity$37;

    iget-object v2, v1, Lru/cn/tv/stb/StbActivity$37;->this$0:Lru/cn/tv/stb/StbActivity;

    const-string v3, "pin_disabled"

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v2, v3, v1}, Lru/cn/domain/Preferences;->setBoolean(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 2497
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$37$1;->this$1:Lru/cn/tv/stb/StbActivity$37;

    iget-object v1, v1, Lru/cn/tv/stb/StbActivity$37;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$7000(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/settings/SettingFragment;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/tv/stb/settings/SettingFragment;->reloadData()V

    .line 2498
    return-void

    .line 2495
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
