.class public Lcom/yandex/metrica/impl/ob/by;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field public static volatile a:Lcom/yandex/metrica/impl/ob/by;

.field private static final b:Ljava/lang/Object;


# instance fields
.field private c:Landroid/content/Context;

.field private d:Landroid/net/LocalServerSocket;

.field private e:Z

.field private f:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/yandex/metrica/impl/ob/by;->b:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/by;->c:Landroid/content/Context;

    .line 62
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/by;
    .locals 3

    .prologue
    .line 38
    sget-object v0, Lcom/yandex/metrica/impl/ob/by;->a:Lcom/yandex/metrica/impl/ob/by;

    if-nez v0, :cond_1

    .line 39
    sget-object v1, Lcom/yandex/metrica/impl/ob/by;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 40
    :try_start_0
    sget-object v0, Lcom/yandex/metrica/impl/ob/by;->a:Lcom/yandex/metrica/impl/ob/by;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/yandex/metrica/impl/ob/by;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/yandex/metrica/impl/ob/by;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/by;->a:Lcom/yandex/metrica/impl/ob/by;

    .line 43
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    :cond_1
    sget-object v0, Lcom/yandex/metrica/impl/ob/by;->a:Lcom/yandex/metrica/impl/ob/by;

    return-object v0

    .line 43
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 66
    iget-boolean v2, p0, Lcom/yandex/metrica/impl/ob/by;->e:Z

    if-eq v2, v0, :cond_0

    .line 67
    iput-boolean v0, p0, Lcom/yandex/metrica/impl/ob/by;->e:Z

    .line 69
    :try_start_0
    new-instance v2, Landroid/net/LocalServerSocket;

    const-string v3, "com.yandex.metrica.configuration.MetricaConfigurationService"

    invoke-direct {v2, v3}, Landroid/net/LocalServerSocket;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/yandex/metrica/impl/ob/by;->d:Landroid/net/LocalServerSocket;

    .line 70
    const-string v2, "YMM-CS"

    invoke-static {v2, p0}, Lcom/yandex/metrica/impl/utils/j;->a(Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Thread;

    move-result-object v2

    iput-object v2, p0, Lcom/yandex/metrica/impl/ob/by;->f:Ljava/lang/Thread;

    .line 71
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/by;->f:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :goto_0
    return v0

    :catch_0
    move-exception v0

    .line 78
    :cond_0
    iput-boolean v1, p0, Lcom/yandex/metrica/impl/ob/by;->e:Z

    move v0, v1

    .line 79
    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/yandex/metrica/impl/ob/by;->e:Z

    .line 85
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/by;->f:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/by;->f:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 87
    iput-object v1, p0, Lcom/yandex/metrica/impl/ob/by;->f:Ljava/lang/Thread;

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/by;->d:Landroid/net/LocalServerSocket;

    if-eqz v0, :cond_1

    .line 91
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/by;->d:Landroid/net/LocalServerSocket;

    invoke-virtual {v0}, Landroid/net/LocalServerSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :goto_0
    iput-object v1, p0, Lcom/yandex/metrica/impl/ob/by;->d:Landroid/net/LocalServerSocket;

    .line 98
    :cond_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public run()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 103
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/ob/by;->e:Z

    if-eqz v0, :cond_4

    .line 107
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/by;->d:Landroid/net/LocalServerSocket;

    if-eqz v0, :cond_5

    .line 108
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/by;->d:Landroid/net/LocalServerSocket;

    invoke-virtual {v0}, Landroid/net/LocalServerSocket;->accept()Landroid/net/LocalSocket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v3

    .line 1128
    :try_start_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/by;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v2

    .line 1131
    :try_start_2
    new-instance v0, Ljava/io/BufferedOutputStream;

    invoke-virtual {v3}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1132
    :try_start_3
    const-string v4, "UTF-8"

    invoke-virtual {v2, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/OutputStream;->write([B)V

    .line 1133
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 1140
    :try_start_4
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 115
    :cond_1
    :goto_1
    if-eqz v3, :cond_0

    .line 117
    :try_start_5
    invoke-virtual {v3}, Landroid/net/LocalSocket;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_0

    .line 120
    :catch_0
    move-exception v0

    goto :goto_0

    .line 1138
    :catch_1
    move-exception v0

    move-object v0, v1

    :goto_2
    if-eqz v0, :cond_1

    .line 1140
    :try_start_6
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    goto :goto_1

    .line 1143
    :catch_2
    move-exception v0

    goto :goto_1

    .line 1138
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_3
    if-eqz v2, :cond_2

    .line 1140
    :try_start_7
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1145
    :cond_2
    :goto_4
    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 115
    :catch_3
    move-exception v0

    move-object v0, v3

    :goto_5
    if-eqz v0, :cond_0

    .line 117
    :try_start_9
    invoke-virtual {v0}, Landroid/net/LocalSocket;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    goto :goto_0

    .line 120
    :catch_4
    move-exception v0

    goto :goto_0

    .line 115
    :catchall_1
    move-exception v0

    move-object v3, v1

    :goto_6
    if-eqz v3, :cond_3

    .line 117
    :try_start_a
    invoke-virtual {v3}, Landroid/net/LocalSocket;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    .line 122
    :cond_3
    :goto_7
    throw v0

    .line 1143
    :catch_5
    move-exception v0

    goto :goto_1

    :catch_6
    move-exception v2

    goto :goto_4

    :catch_7
    move-exception v1

    goto :goto_7

    .line 124
    :cond_4
    return-void

    .line 115
    :catchall_2
    move-exception v0

    goto :goto_6

    :catch_8
    move-exception v0

    move-object v0, v1

    goto :goto_5

    .line 1138
    :catchall_3
    move-exception v2

    move-object v5, v2

    move-object v2, v0

    move-object v0, v5

    goto :goto_3

    :catch_9
    move-exception v2

    goto :goto_2

    :cond_5
    move-object v3, v1

    goto :goto_1
.end method
