.class public final Lcom/my/target/core/parsers/b;
.super Ljava/lang/Object;
.source "NativeAdSectionParser.java"


# instance fields
.field private final S:Lcom/my/target/ae;

.field private final adConfig:Lcom/my/target/b;

.field private final context:Landroid/content/Context;


# direct methods
.method private constructor <init>(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/my/target/core/parsers/b;->S:Lcom/my/target/ae;

    .line 28
    iput-object p2, p0, Lcom/my/target/core/parsers/b;->adConfig:Lcom/my/target/b;

    .line 29
    iput-object p3, p0, Lcom/my/target/core/parsers/b;->context:Landroid/content/Context;

    .line 30
    return-void
.end method

.method public static a(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/b;
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/my/target/core/parsers/b;

    invoke-direct {v0, p0, p1, p2}, Lcom/my/target/core/parsers/b;-><init>(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;Lcom/my/target/cs;)V
    .locals 4

    .prologue
    .line 34
    iget-object v0, p0, Lcom/my/target/core/parsers/b;->S:Lcom/my/target/ae;

    iget-object v1, p0, Lcom/my/target/core/parsers/b;->adConfig:Lcom/my/target/b;

    iget-object v2, p0, Lcom/my/target/core/parsers/b;->context:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/my/target/bf;->b(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/bf;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/my/target/bf;->a(Lorg/json/JSONObject;Lcom/my/target/ak;)V

    .line 35
    const-string v0, "settings"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 36
    if-eqz v0, :cond_0

    .line 1044
    const-string v1, "video"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 1045
    if-eqz v0, :cond_0

    .line 1047
    iget-object v1, p0, Lcom/my/target/core/parsers/b;->S:Lcom/my/target/ae;

    iget-object v2, p0, Lcom/my/target/core/parsers/b;->adConfig:Lcom/my/target/b;

    iget-object v3, p0, Lcom/my/target/core/parsers/b;->context:Landroid/content/Context;

    invoke-static {p2, v1, v2, v3}, Lcom/my/target/bg;->b(Lcom/my/target/ak;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/bg;

    move-result-object v1

    invoke-virtual {p2}, Lcom/my/target/cs;->getVideoSettings()Lcom/my/target/am;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/my/target/bg;->a(Lorg/json/JSONObject;Lcom/my/target/am;)V

    .line 40
    :cond_0
    return-void
.end method
