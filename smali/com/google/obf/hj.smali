.class public Lcom/google/obf/hj;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/hk$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/hj$e;,
        Lcom/google/obf/hj$b;,
        Lcom/google/obf/hj$a;,
        Lcom/google/obf/hj$d;,
        Lcom/google/obf/hj$c;
    }
.end annotation


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/obf/hj$d;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/obf/hj$a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/obf/hj$b;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/obf/hj$e;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/obf/ht;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Landroid/content/Context;

.field private final i:Lcom/google/obf/hk;

.field private j:Lcom/google/obf/hl;

.field private k:Z

.field private l:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/obf/hi;",
            ">;"
        }
    .end annotation
.end field

.field private m:J

.field private n:Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;

.field private o:Ljava/lang/String;

.field private p:Lcom/google/ads/interactivemedia/v3/api/AdsRenderingSettings;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/obf/hj;->a:Ljava/util/Map;

    .line 17
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/obf/hj;->b:Ljava/util/Map;

    .line 18
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/obf/hj;->c:Ljava/util/Set;

    .line 19
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/obf/hj;->d:Ljava/util/Map;

    .line 20
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/obf/hj;->e:Ljava/util/Map;

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/obf/hj;->f:Ljava/util/Map;

    .line 22
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/obf/hj;->g:Ljava/util/Map;

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/hj;->k:Z

    .line 24
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/obf/hj;->l:Ljava/util/Queue;

    .line 25
    iput-object p1, p0, Lcom/google/obf/hj;->h:Landroid/content/Context;

    .line 26
    iput-object p4, p0, Lcom/google/obf/hj;->n:Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;

    .line 27
    new-instance v0, Lcom/google/obf/hk;

    invoke-direct {v0, p1, p0}, Lcom/google/obf/hk;-><init>(Landroid/content/Context;Lcom/google/obf/hk$a;)V

    iput-object v0, p0, Lcom/google/obf/hj;->i:Lcom/google/obf/hk;

    .line 28
    invoke-virtual {p0, p2, p3}, Lcom/google/obf/hj;->a(Landroid/net/Uri;Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/hj;->o:Ljava/lang/String;

    .line 29
    return-void
.end method

.method static synthetic a(Lcom/google/obf/hj;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/google/obf/hj;->h:Landroid/content/Context;

    return-object v0
.end method

.method private a(Lcom/google/obf/gx;Ljava/util/Set;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/gx;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    new-instance v2, Ljava/util/HashMap;

    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 122
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 123
    invoke-virtual {p1}, Lcom/google/obf/gx;->a()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ads/interactivemedia/v3/api/CompanionAdSlot;

    .line 124
    invoke-interface {v1}, Lcom/google/ads/interactivemedia/v3/api/CompanionAdSlot;->getContainer()Landroid/view/ViewGroup;

    move-result-object v4

    if-nez v4, :cond_0

    .line 125
    const/4 v0, 0x0

    .line 128
    :goto_1
    return-object v0

    .line 126
    :cond_0
    invoke-interface {v1}, Lcom/google/ads/interactivemedia/v3/api/CompanionAdSlot;->getContainer()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    move-object v0, v2

    .line 128
    goto :goto_1
.end method

.method private a(JLjava/lang/String;)V
    .locals 5

    .prologue
    .line 88
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 89
    const-string v1, "webViewLoadingTime"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    new-instance v1, Lcom/google/obf/hi;

    sget-object v2, Lcom/google/obf/hi$b;->webViewLoaded:Lcom/google/obf/hi$b;

    sget-object v3, Lcom/google/obf/hi$c;->csi:Lcom/google/obf/hi$c;

    invoke-direct {v1, v2, v3, p3, v0}, Lcom/google/obf/hi;-><init>(Lcom/google/obf/hi$b;Lcom/google/obf/hi$c;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0, v1}, Lcom/google/obf/hj;->b(Lcom/google/obf/hi;)V

    .line 91
    return-void
.end method

.method private a(Landroid/view/ViewGroup;Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/api/CompanionAdSlot;)V
    .locals 4

    .prologue
    .line 282
    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 283
    const/4 v0, 0x0

    .line 284
    check-cast p4, Lcom/google/obf/gz;

    .line 285
    invoke-virtual {p4}, Lcom/google/obf/gz;->a()Ljava/util/List;

    move-result-object v1

    .line 286
    sget-object v2, Lcom/google/obf/hj$2;->c:[I

    invoke-virtual {p2}, Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;->type()Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData$a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData$a;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 290
    :goto_0
    invoke-virtual {v0, p3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 291
    invoke-virtual {p4, p3}, Lcom/google/obf/gz;->a(Ljava/lang/String;)V

    .line 292
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 293
    return-void

    .line 287
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0, p2, v1}, Lcom/google/obf/hj;->a(Landroid/content/Context;Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;Ljava/util/List;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 289
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3, v1}, Lcom/google/obf/hj;->a(Landroid/content/Context;Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;Ljava/lang/String;Ljava/util/List;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 286
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Lcom/google/obf/hi$c;Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/impl/data/l;)V
    .locals 5

    .prologue
    .line 57
    sget-object v0, Lcom/google/obf/hj$2;->b:[I

    invoke-virtual {p1}, Lcom/google/obf/hi$c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 86
    const-string v0, "other"

    invoke-direct {p0, v0, p1}, Lcom/google/obf/hj;->a(Ljava/lang/String;Lcom/google/obf/hi$c;)V

    .line 87
    :goto_0
    return-void

    .line 58
    :pswitch_0
    sget-object v0, Lcom/google/obf/hi$a;->a:Lcom/google/obf/hi$a;

    .line 59
    :try_start_0
    iget-object v1, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->adUiStyle:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 60
    iget-object v1, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->adUiStyle:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/obf/hi$a;->valueOf(Ljava/lang/String;)Lcom/google/obf/hi$a;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 63
    :cond_0
    :goto_1
    new-instance v1, Lcom/google/obf/hl;

    iget-wide v2, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->adTimeUpdateMs:J

    invoke-direct {v1, v2, v3, v0}, Lcom/google/obf/hl;-><init>(JLcom/google/obf/hi$a;)V

    iput-object v1, p0, Lcom/google/obf/hj;->j:Lcom/google/obf/hl;

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/hj;->k:Z

    .line 65
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/obf/hj;->m:J

    sub-long/2addr v0, v2

    invoke-direct {p0, v0, v1, p2}, Lcom/google/obf/hj;->a(JLjava/lang/String;)V

    .line 66
    invoke-direct {p0}, Lcom/google/obf/hj;->d()V

    goto :goto_0

    .line 68
    :pswitch_1
    iget-object v0, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->ln:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->n:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->m:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 69
    :cond_1
    const-string v0, "IMASDK"

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1e

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid logging message data: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 71
    :cond_2
    const-string v1, "SDK_LOG:"

    iget-object v0, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->n:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 72
    :goto_2
    iget-object v1, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->ln:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 83
    const-string v2, "IMASDK"

    const-string v3, "Unrecognized log level: "

    iget-object v1, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->ln:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    iget-object v1, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->m:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 71
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 73
    :sswitch_0
    iget-object v1, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->m:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 75
    :sswitch_1
    iget-object v1, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->m:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 77
    :sswitch_2
    iget-object v1, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->m:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 79
    :sswitch_3
    iget-object v1, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->m:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 81
    :sswitch_4
    iget-object v1, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->m:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 83
    :cond_4
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 62
    :catch_0
    move-exception v1

    goto/16 :goto_1

    .line 57
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 72
    :sswitch_data_0
    .sparse-switch
        0x44 -> :sswitch_0
        0x45 -> :sswitch_1
        0x49 -> :sswitch_2
        0x53 -> :sswitch_1
        0x56 -> :sswitch_3
        0x57 -> :sswitch_4
    .end sparse-switch
.end method

.method private a(Ljava/lang/String;Lcom/google/obf/hi$c;)V
    .locals 4

    .prologue
    .line 246
    const-string v0, "IMASDK"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2b

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Illegal message type "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " received for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " channel"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 248
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 250
    :cond_0
    :goto_0
    return-object p1

    :cond_1
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0xc

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Caused by: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private b(Lcom/google/obf/hi$c;Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/impl/data/l;)V
    .locals 7

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/obf/hj;->g:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/gx;

    .line 93
    iget-object v1, p0, Lcom/google/obf/hj;->a:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/obf/hj$d;

    .line 94
    iget-object v2, p0, Lcom/google/obf/hj;->f:Ljava/util/Map;

    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/obf/ht;

    .line 95
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-nez v2, :cond_2

    .line 96
    :cond_0
    const-string v0, "IMASDK"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x3c

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Received displayContainer message: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for invalid session id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    :cond_1
    :goto_0
    return-void

    .line 98
    :cond_2
    invoke-interface {v2, p1, p3}, Lcom/google/obf/ht;->b(Lcom/google/obf/hi$c;Lcom/google/ads/interactivemedia/v3/impl/data/l;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 99
    sget-object v2, Lcom/google/obf/hj$2;->b:[I

    invoke-virtual {p1}, Lcom/google/obf/hi$c;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 115
    sget-object v0, Lcom/google/obf/hi$b;->displayContainer:Lcom/google/obf/hi$b;

    invoke-virtual {v0}, Lcom/google/obf/hi$b;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/obf/hj;->a(Ljava/lang/String;Lcom/google/obf/hi$c;)V

    goto :goto_0

    .line 100
    :pswitch_0
    if-eqz p3, :cond_3

    iget-object v2, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->companions:Ljava/util/Map;

    if-nez v2, :cond_4

    .line 101
    :cond_3
    sget-object v0, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;->LOAD:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;

    sget-object v2, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;->INTERNAL_ERROR:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;

    const-string v3, "Display companions message requires companions in data."

    invoke-interface {v1, v0, v2, v3}, Lcom/google/obf/hj$d;->a(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V

    goto :goto_0

    .line 103
    :cond_4
    iget-object v2, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->companions:Ljava/util/Map;

    .line 104
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/google/obf/hj;->a(Lcom/google/obf/gx;Ljava/util/Set;)Ljava/util/Map;

    move-result-object v4

    .line 105
    iget-object v2, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->companions:Ljava/util/Map;

    invoke-interface {v1, v2}, Lcom/google/obf/hj$d;->a(Ljava/util/Map;)V

    .line 106
    if-nez v4, :cond_5

    .line 107
    sget-object v0, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;->LOAD:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;

    sget-object v2, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;->INTERNAL_ERROR:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;

    const-string v3, "Display requested for invalid companion slot."

    invoke-interface {v1, v0, v2, v3}, Lcom/google/obf/hj$d;->a(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V

    goto :goto_0

    .line 108
    :cond_5
    iget-object v1, p0, Lcom/google/obf/hj;->p:Lcom/google/ads/interactivemedia/v3/api/AdsRenderingSettings;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/obf/hj;->p:Lcom/google/ads/interactivemedia/v3/api/AdsRenderingSettings;

    invoke-interface {v1}, Lcom/google/ads/interactivemedia/v3/api/AdsRenderingSettings;->isRenderCompanions()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 109
    :cond_6
    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 110
    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iget-object v3, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->companions:Ljava/util/Map;

    .line 111
    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;

    .line 112
    invoke-virtual {v0}, Lcom/google/obf/gx;->a()Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ads/interactivemedia/v3/api/CompanionAdSlot;

    .line 113
    invoke-direct {p0, v2, v3, p2, v1}, Lcom/google/obf/hj;->a(Landroid/view/ViewGroup;Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/api/CompanionAdSlot;)V

    goto :goto_1

    .line 99
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method private c(Lcom/google/obf/hi$c;Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/impl/data/l;)V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/obf/hj;->e:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/hj$e;

    .line 118
    if-eqz v0, :cond_0

    .line 119
    iget-object v1, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->translation:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lcom/google/obf/hj$e;->a(Lcom/google/obf/hi$c;Ljava/lang/String;)V

    .line 120
    :cond_0
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 275
    :goto_0
    iget-boolean v0, p0, Lcom/google/obf/hj;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/hj;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 276
    iget-object v1, p0, Lcom/google/obf/hj;->i:Lcom/google/obf/hk;

    iget-object v0, p0, Lcom/google/obf/hj;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/hi;

    invoke-virtual {v1, v0}, Lcom/google/obf/hk;->a(Lcom/google/obf/hi;)V

    goto :goto_0

    .line 277
    :cond_0
    return-void
.end method

.method private d(Lcom/google/obf/hi$c;Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/impl/data/l;)V
    .locals 6

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/obf/hj;->d:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/hj$b;

    .line 130
    if-nez v0, :cond_0

    .line 131
    const-string v0, "IMASDK"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x33

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Received request message: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for invalid session id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    :goto_0
    return-void

    .line 133
    :cond_0
    sget-object v1, Lcom/google/obf/hj$2;->b:[I

    invoke-virtual {p1}, Lcom/google/obf/hi$c;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 145
    sget-object v0, Lcom/google/obf/hi$b;->adsLoader:Lcom/google/obf/hi$b;

    invoke-virtual {v0}, Lcom/google/obf/hi$b;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/obf/hj;->a(Ljava/lang/String;Lcom/google/obf/hi$c;)V

    goto :goto_0

    .line 134
    :pswitch_0
    if-nez p3, :cond_1

    .line 135
    sget-object v1, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;->LOAD:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;

    sget-object v2, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;->INTERNAL_ERROR:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;

    const-string v3, "adsLoaded message did not contain cue points."

    invoke-interface {v0, p2, v1, v2, v3}, Lcom/google/obf/hj$b;->a(Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V

    goto :goto_0

    .line 136
    :cond_1
    iget-object v2, p0, Lcom/google/obf/hj;->j:Lcom/google/obf/hl;

    iget-object v3, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->adCuePoints:Ljava/util/List;

    iget-object v4, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->internalCuePoints:Ljava/util/SortedSet;

    iget-boolean v5, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->monitorAppLifecycle:Z

    move-object v1, p2

    invoke-interface/range {v0 .. v5}, Lcom/google/obf/hj$b;->a(Ljava/lang/String;Lcom/google/obf/hl;Ljava/util/List;Ljava/util/SortedSet;Z)V

    goto :goto_0

    .line 138
    :pswitch_1
    iget-object v1, p0, Lcom/google/obf/hj;->j:Lcom/google/obf/hl;

    iget-object v2, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->streamId:Ljava/lang/String;

    iget-boolean v3, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->monitorAppLifecycle:Z

    invoke-interface {v0, p2, v1, v2, v3}, Lcom/google/obf/hj$b;->a(Ljava/lang/String;Lcom/google/obf/hl;Ljava/lang/String;Z)V

    .line 139
    const-string v1, "IMASDK"

    const-string v2, "Stream initialized with streamId: "

    iget-object v0, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->streamId:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 141
    :pswitch_2
    sget-object v1, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;->LOAD:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;

    iget v2, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->errorCode:I

    iget-object v3, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->errorMessage:Ljava/lang/String;

    iget-object v4, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->innerError:Ljava/lang/String;

    .line 142
    invoke-direct {p0, v3, v4}, Lcom/google/obf/hj;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 143
    invoke-interface {v0, p2, v1, v2, v3}, Lcom/google/obf/hj$b;->a(Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;ILjava/lang/String;)V

    goto :goto_0

    .line 133
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private e(Lcom/google/obf/hi$c;Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/impl/data/l;)V
    .locals 4

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/obf/hj;->f:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/ht;

    .line 148
    if-nez v0, :cond_0

    .line 149
    const-string v0, "IMASDK"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x38

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Received videoDisplay message: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for invalid session id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    :goto_0
    return-void

    .line 151
    :cond_0
    invoke-interface {v0, p1, p3}, Lcom/google/obf/ht;->a(Lcom/google/obf/hi$c;Lcom/google/ads/interactivemedia/v3/impl/data/l;)Z

    goto :goto_0
.end method

.method private f(Lcom/google/obf/hi$c;Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/impl/data/l;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 153
    iget-object v0, p0, Lcom/google/obf/hj;->a:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/hj$d;

    .line 154
    if-nez v0, :cond_0

    .line 155
    const-string v0, "IMASDK"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x33

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Received manager message: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for invalid session id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    :goto_0
    :pswitch_0
    return-void

    .line 158
    :cond_0
    if-eqz p3, :cond_3

    iget-object v1, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->adData:Lcom/google/ads/interactivemedia/v3/impl/data/b;

    if-eqz v1, :cond_3

    .line 159
    iget-object v1, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->adData:Lcom/google/ads/interactivemedia/v3/impl/data/b;

    .line 160
    :goto_1
    sget-object v3, Lcom/google/obf/hj$2;->b:[I

    invoke-virtual {p1}, Lcom/google/obf/hi$c;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 228
    :pswitch_1
    sget-object v0, Lcom/google/obf/hi$b;->adsManager:Lcom/google/obf/hi$b;

    invoke-virtual {v0}, Lcom/google/obf/hi$b;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/obf/hj;->a(Ljava/lang/String;Lcom/google/obf/hi$c;)V

    goto :goto_0

    .line 162
    :pswitch_2
    if-eqz v1, :cond_1

    .line 163
    new-instance v2, Lcom/google/obf/hj$c;

    sget-object v3, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->LOADED:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    invoke-direct {v2, v3, v1}, Lcom/google/obf/hj$c;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    invoke-interface {v0, v2}, Lcom/google/obf/hj$d;->a(Lcom/google/obf/hj$c;)V

    goto :goto_0

    .line 164
    :cond_1
    const-string v1, "IMASDK"

    const-string v2, "Ad loaded message requires adData"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    sget-object v1, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;->LOAD:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;

    sget-object v2, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;->INTERNAL_ERROR:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;

    const-string v3, "Ad loaded message did not contain adData."

    invoke-interface {v0, v1, v2, v3}, Lcom/google/obf/hj$d;->a(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V

    goto :goto_0

    .line 167
    :pswitch_3
    new-instance v1, Lcom/google/obf/hj$c;

    sget-object v3, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->CONTENT_PAUSE_REQUESTED:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    invoke-direct {v1, v3, v2}, Lcom/google/obf/hj$c;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    invoke-interface {v0, v1}, Lcom/google/obf/hj$d;->a(Lcom/google/obf/hj$c;)V

    goto :goto_0

    .line 169
    :pswitch_4
    new-instance v1, Lcom/google/obf/hj$c;

    sget-object v3, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->CONTENT_RESUME_REQUESTED:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    invoke-direct {v1, v3, v2}, Lcom/google/obf/hj$c;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    invoke-interface {v0, v1}, Lcom/google/obf/hj$d;->a(Lcom/google/obf/hj$c;)V

    goto :goto_0

    .line 171
    :pswitch_5
    new-instance v2, Lcom/google/obf/hj$c;

    sget-object v3, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->COMPLETED:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    invoke-direct {v2, v3, v1}, Lcom/google/obf/hj$c;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    invoke-interface {v0, v2}, Lcom/google/obf/hj$d;->a(Lcom/google/obf/hj$c;)V

    goto :goto_0

    .line 173
    :pswitch_6
    new-instance v1, Lcom/google/obf/hj$c;

    sget-object v3, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->ALL_ADS_COMPLETED:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    invoke-direct {v1, v3, v2}, Lcom/google/obf/hj$c;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    invoke-interface {v0, v1}, Lcom/google/obf/hj$d;->a(Lcom/google/obf/hj$c;)V

    goto :goto_0

    .line 175
    :pswitch_7
    new-instance v7, Lcom/google/obf/hj$c;

    sget-object v1, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->CUEPOINTS_CHANGED:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    invoke-direct {v7, v1, v2}, Lcom/google/obf/hj$c;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    .line 176
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v7, Lcom/google/obf/hj$c;->d:Ljava/util/List;

    .line 177
    iget-object v1, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->cuepoints:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/google/ads/interactivemedia/v3/impl/data/j;

    .line 178
    iget-object v9, v7, Lcom/google/obf/hj$c;->d:Ljava/util/List;

    new-instance v1, Lcom/google/obf/hd;

    .line 179
    invoke-virtual {v6}, Lcom/google/ads/interactivemedia/v3/impl/data/j;->start()D

    move-result-wide v2

    invoke-virtual {v6}, Lcom/google/ads/interactivemedia/v3/impl/data/j;->end()D

    move-result-wide v4

    invoke-virtual {v6}, Lcom/google/ads/interactivemedia/v3/impl/data/j;->played()Z

    move-result v6

    invoke-direct/range {v1 .. v6}, Lcom/google/obf/hd;-><init>(DDZ)V

    .line 180
    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 182
    :cond_2
    invoke-interface {v0, v7}, Lcom/google/obf/hj$d;->a(Lcom/google/obf/hj$c;)V

    goto/16 :goto_0

    .line 184
    :pswitch_8
    new-instance v2, Lcom/google/obf/hj$c;

    sget-object v3, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->SKIPPED:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    invoke-direct {v2, v3, v1}, Lcom/google/obf/hj$c;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    invoke-interface {v0, v2}, Lcom/google/obf/hj$d;->a(Lcom/google/obf/hj$c;)V

    goto/16 :goto_0

    .line 186
    :pswitch_9
    new-instance v2, Lcom/google/obf/hj$c;

    sget-object v3, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->STARTED:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    invoke-direct {v2, v3, v1}, Lcom/google/obf/hj$c;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    invoke-interface {v0, v2}, Lcom/google/obf/hj$d;->a(Lcom/google/obf/hj$c;)V

    goto/16 :goto_0

    .line 188
    :pswitch_a
    new-instance v2, Lcom/google/obf/hj$c;

    sget-object v3, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->PAUSED:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    invoke-direct {v2, v3, v1}, Lcom/google/obf/hj$c;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    invoke-interface {v0, v2}, Lcom/google/obf/hj$d;->a(Lcom/google/obf/hj$c;)V

    goto/16 :goto_0

    .line 190
    :pswitch_b
    new-instance v2, Lcom/google/obf/hj$c;

    sget-object v3, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->RESUMED:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    invoke-direct {v2, v3, v1}, Lcom/google/obf/hj$c;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    invoke-interface {v0, v2}, Lcom/google/obf/hj$d;->a(Lcom/google/obf/hj$c;)V

    goto/16 :goto_0

    .line 192
    :pswitch_c
    new-instance v2, Lcom/google/obf/hj$c;

    sget-object v3, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->FIRST_QUARTILE:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    invoke-direct {v2, v3, v1}, Lcom/google/obf/hj$c;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    invoke-interface {v0, v2}, Lcom/google/obf/hj$d;->a(Lcom/google/obf/hj$c;)V

    goto/16 :goto_0

    .line 194
    :pswitch_d
    new-instance v2, Lcom/google/obf/hj$c;

    sget-object v3, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->MIDPOINT:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    invoke-direct {v2, v3, v1}, Lcom/google/obf/hj$c;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    invoke-interface {v0, v2}, Lcom/google/obf/hj$d;->a(Lcom/google/obf/hj$c;)V

    goto/16 :goto_0

    .line 196
    :pswitch_e
    new-instance v2, Lcom/google/obf/hj$c;

    sget-object v3, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->THIRD_QUARTILE:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    invoke-direct {v2, v3, v1}, Lcom/google/obf/hj$c;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    invoke-interface {v0, v2}, Lcom/google/obf/hj$d;->a(Lcom/google/obf/hj$c;)V

    goto/16 :goto_0

    .line 198
    :pswitch_f
    new-instance v2, Lcom/google/obf/hj$c;

    sget-object v3, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->CLICKED:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    invoke-direct {v2, v3, v1}, Lcom/google/obf/hj$c;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    invoke-interface {v0, v2}, Lcom/google/obf/hj$d;->a(Lcom/google/obf/hj$c;)V

    goto/16 :goto_0

    .line 200
    :pswitch_10
    sget-object v1, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;->PLAY:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;

    iget v2, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->errorCode:I

    iget-object v3, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->errorMessage:Ljava/lang/String;

    iget-object v4, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->innerError:Ljava/lang/String;

    .line 201
    invoke-direct {p0, v3, v4}, Lcom/google/obf/hj;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 202
    invoke-interface {v0, v1, v2, v3}, Lcom/google/obf/hj$d;->a(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 204
    :pswitch_11
    new-instance v2, Lcom/google/obf/hj$c;

    sget-object v3, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->LOG:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    invoke-direct {v2, v3, v1}, Lcom/google/obf/hj$c;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    .line 205
    iget-object v1, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->logData:Lcom/google/ads/interactivemedia/v3/impl/data/l$a;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/v3/impl/data/l$a;->constructMap()Ljava/util/Map;

    move-result-object v1

    iput-object v1, v2, Lcom/google/obf/hj$c;->c:Ljava/util/Map;

    .line 206
    invoke-interface {v0, v2}, Lcom/google/obf/hj$d;->a(Lcom/google/obf/hj$c;)V

    goto/16 :goto_0

    .line 209
    :pswitch_12
    new-instance v2, Lcom/google/obf/hj$c;

    sget-object v3, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->TAPPED:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    invoke-direct {v2, v3, v1}, Lcom/google/obf/hj$c;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    invoke-interface {v0, v2}, Lcom/google/obf/hj$d;->a(Lcom/google/obf/hj$c;)V

    goto/16 :goto_0

    .line 211
    :pswitch_13
    new-instance v1, Lcom/google/obf/hj$c;

    sget-object v3, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->ICON_TAPPED:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    invoke-direct {v1, v3, v2}, Lcom/google/obf/hj$c;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    .line 212
    iget-object v2, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->clickThroughUrl:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/obf/hj$c;->f:Ljava/lang/String;

    .line 213
    invoke-interface {v0, v1}, Lcom/google/obf/hj$d;->a(Lcom/google/obf/hj$c;)V

    goto/16 :goto_0

    .line 215
    :pswitch_14
    new-instance v10, Lcom/google/obf/hj$c;

    sget-object v2, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->AD_PROGRESS:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    invoke-direct {v10, v2, v1}, Lcom/google/obf/hj$c;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    .line 216
    new-instance v1, Lcom/google/obf/gn;

    iget-wide v2, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->currentTime:D

    iget-wide v4, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->duration:D

    iget v6, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->adPosition:I

    iget v7, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->totalAds:I

    iget-wide v8, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->adBreakDuration:D

    invoke-direct/range {v1 .. v9}, Lcom/google/obf/gn;-><init>(DDIID)V

    iput-object v1, v10, Lcom/google/obf/hj$c;->e:Lcom/google/ads/interactivemedia/v3/api/AdProgressInfo;

    .line 217
    invoke-interface {v0, v10}, Lcom/google/obf/hj$d;->a(Lcom/google/obf/hj$c;)V

    goto/16 :goto_0

    .line 219
    :pswitch_15
    new-instance v1, Lcom/google/obf/hj$c;

    sget-object v3, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->AD_BREAK_READY:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    invoke-direct {v1, v3, v2}, Lcom/google/obf/hj$c;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    .line 220
    new-instance v2, Landroid/support/v4/util/ArrayMap;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/support/v4/util/ArrayMap;-><init>(I)V

    iput-object v2, v1, Lcom/google/obf/hj$c;->c:Ljava/util/Map;

    .line 221
    iget-object v2, v1, Lcom/google/obf/hj$c;->c:Ljava/util/Map;

    const-string v3, "adBreakTime"

    iget-object v4, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->adBreakTime:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    invoke-interface {v0, v1}, Lcom/google/obf/hj$d;->a(Lcom/google/obf/hj$c;)V

    goto/16 :goto_0

    .line 224
    :pswitch_16
    new-instance v2, Lcom/google/obf/hj$c;

    sget-object v3, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->AD_BREAK_STARTED:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    invoke-direct {v2, v3, v1}, Lcom/google/obf/hj$c;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    invoke-interface {v0, v2}, Lcom/google/obf/hj$d;->a(Lcom/google/obf/hj$c;)V

    goto/16 :goto_0

    .line 226
    :pswitch_17
    new-instance v2, Lcom/google/obf/hj$c;

    sget-object v3, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->AD_BREAK_ENDED:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    invoke-direct {v2, v3, v1}, Lcom/google/obf/hj$c;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    invoke-interface {v0, v2}, Lcom/google/obf/hj$d;->a(Lcom/google/obf/hj$c;)V

    goto/16 :goto_0

    :cond_3
    move-object v1, v2

    goto/16 :goto_1

    .line 160
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_11
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_10
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_0
        :pswitch_0
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
    .end packed-switch
.end method

.method private g(Lcom/google/obf/hi$c;Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/impl/data/l;)V
    .locals 4

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/obf/hj;->c:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    :goto_0
    return-void

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/google/obf/hj;->b:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/hj$a;

    .line 233
    if-nez v0, :cond_1

    .line 234
    const-string v0, "IMASDK"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x33

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Received monitor message: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for invalid session id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 236
    :cond_1
    if-nez p3, :cond_2

    .line 237
    const-string v0, "IMASDK"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x38

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Received monitor message: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for session id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with no data"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 239
    :cond_2
    sget-object v1, Lcom/google/obf/hj$2;->b:[I

    invoke-virtual {p1}, Lcom/google/obf/hi$c;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 244
    sget-object v0, Lcom/google/obf/hi$b;->activityMonitor:Lcom/google/obf/hi$b;

    invoke-virtual {v0}, Lcom/google/obf/hi$b;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/obf/hj;->a(Ljava/lang/String;Lcom/google/obf/hi$c;)V

    goto/16 :goto_0

    .line 240
    :pswitch_0
    iget-object v1, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->queryId:Ljava/lang/String;

    iget-object v2, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->eventId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/obf/hj$a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 242
    :pswitch_1
    iget-object v1, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->queryId:Ljava/lang/String;

    iget-object v2, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->eventId:Ljava/lang/String;

    iget-object v3, p3, Lcom/google/ads/interactivemedia/v3/impl/data/l;->vastEvent:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/obf/hj$a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 239
    :pswitch_data_0
    .packed-switch 0x1e
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected a(Landroid/net/Uri;Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 1
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "sdk_version"

    const-string v2, "a.3.7.4"

    .line 2
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "hl"

    .line 3
    invoke-virtual {p2}, Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "wvr"

    const-string v2, "2"

    .line 4
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "app"

    iget-object v2, p0, Lcom/google/obf/hj;->h:Landroid/content/Context;

    .line 5
    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 6
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/google/obf/hj;->n:Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;

    if-eqz v1, :cond_0

    .line 8
    new-instance v1, Lcom/google/obf/eh;

    invoke-direct {v1}, Lcom/google/obf/eh;-><init>()V

    new-instance v2, Lcom/google/obf/it;

    invoke-direct {v2}, Lcom/google/obf/it;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/obf/eh;->a(Lcom/google/obf/ex;)Lcom/google/obf/eh;

    move-result-object v1

    new-instance v2, Lcom/google/obf/is;

    invoke-direct {v2}, Lcom/google/obf/is;-><init>()V

    .line 9
    invoke-virtual {v1, v2}, Lcom/google/obf/eh;->a(Lcom/google/obf/ec;)Lcom/google/obf/eh;

    move-result-object v1

    .line 10
    invoke-virtual {v1}, Lcom/google/obf/eh;->a()Lcom/google/obf/eg;

    move-result-object v1

    .line 11
    const-string v2, "tcnfp"

    iget-object v3, p0, Lcom/google/obf/hj;->n:Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;

    .line 12
    invoke-virtual {v1, v3}, Lcom/google/obf/eg;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 13
    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 14
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/content/Context;Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;Ljava/lang/String;Ljava/util/List;)Landroid/view/View;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/ads/interactivemedia/v3/api/CompanionAdSlot$ClickListener;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 300
    new-instance v0, Lcom/google/obf/hh;

    move-object v1, p1

    move-object v2, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/obf/hh;-><init>(Landroid/content/Context;Lcom/google/obf/hj;Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;Ljava/lang/String;Ljava/util/List;)V

    .line 301
    invoke-virtual {v0}, Lcom/google/obf/hh;->a()V

    .line 302
    return-object v0
.end method

.method protected a(Landroid/content/Context;Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;Ljava/util/List;)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;",
            "Ljava/util/List",
            "<",
            "Lcom/google/ads/interactivemedia/v3/api/CompanionAdSlot$ClickListener;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 299
    new-instance v0, Lcom/google/obf/ha;

    invoke-direct {v0, p1, p0, p2, p3}, Lcom/google/obf/ha;-><init>(Landroid/content/Context;Lcom/google/obf/hj;Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;Ljava/util/List;)V

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 30
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/obf/hj;->m:J

    .line 31
    iget-object v0, p0, Lcom/google/obf/hj;->i:Lcom/google/obf/hk;

    iget-object v1, p0, Lcom/google/obf/hj;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/obf/hk;->a(Ljava/lang/String;)V

    .line 32
    return-void
.end method

.method public a(Lcom/google/ads/interactivemedia/v3/api/AdsRenderingSettings;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/google/obf/hj;->p:Lcom/google/ads/interactivemedia/v3/api/AdsRenderingSettings;

    .line 34
    return-void
.end method

.method public a(Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/obf/hj;->g:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    return-void
.end method

.method public a(Lcom/google/obf/hi;)V
    .locals 5

    .prologue
    .line 37
    invoke-virtual {p1}, Lcom/google/obf/hi;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/v3/impl/data/l;

    .line 38
    invoke-virtual {p1}, Lcom/google/obf/hi;->d()Ljava/lang/String;

    move-result-object v1

    .line 39
    invoke-virtual {p1}, Lcom/google/obf/hi;->b()Lcom/google/obf/hi$c;

    move-result-object v2

    .line 40
    sget-object v3, Lcom/google/obf/hj$2;->a:[I

    invoke-virtual {p1}, Lcom/google/obf/hi;->a()Lcom/google/obf/hi$b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/obf/hi$b;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 55
    const-string v0, "IMASDK"

    invoke-virtual {p1}, Lcom/google/obf/hi;->a()Lcom/google/obf/hi$b;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x19

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown message channel: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    :goto_0
    return-void

    .line 41
    :pswitch_0
    invoke-direct {p0, v2, v1, v0}, Lcom/google/obf/hj;->f(Lcom/google/obf/hi$c;Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/impl/data/l;)V

    goto :goto_0

    .line 43
    :pswitch_1
    invoke-direct {p0, v2, v1, v0}, Lcom/google/obf/hj;->g(Lcom/google/obf/hi$c;Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/impl/data/l;)V

    goto :goto_0

    .line 45
    :pswitch_2
    invoke-direct {p0, v2, v1, v0}, Lcom/google/obf/hj;->e(Lcom/google/obf/hi$c;Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/impl/data/l;)V

    goto :goto_0

    .line 47
    :pswitch_3
    invoke-direct {p0, v2, v1, v0}, Lcom/google/obf/hj;->d(Lcom/google/obf/hi$c;Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/impl/data/l;)V

    goto :goto_0

    .line 49
    :pswitch_4
    invoke-direct {p0, v2, v1, v0}, Lcom/google/obf/hj;->b(Lcom/google/obf/hi$c;Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/impl/data/l;)V

    goto :goto_0

    .line 51
    :pswitch_5
    invoke-direct {p0, v2, v1, v0}, Lcom/google/obf/hj;->c(Lcom/google/obf/hi$c;Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/impl/data/l;)V

    goto :goto_0

    .line 53
    :pswitch_6
    invoke-direct {p0, v2, v1, v0}, Lcom/google/obf/hj;->a(Lcom/google/obf/hi$c;Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/impl/data/l;)V

    goto :goto_0

    .line 40
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
    .end packed-switch
.end method

.method public a(Lcom/google/obf/hj$a;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/obf/hj;->b:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    return-void
.end method

.method public a(Lcom/google/obf/hj$b;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/obf/hj;->d:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    return-void
.end method

.method public a(Lcom/google/obf/hj$d;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/obf/hj;->a:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    return-void
.end method

.method public a(Lcom/google/obf/hj$e;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/obf/hj;->e:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    return-void
.end method

.method public a(Lcom/google/obf/ht;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/obf/hj;->f:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/obf/hj;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 294
    invoke-static {p1}, Lcom/google/obf/jb;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Lcom/google/obf/jb;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 295
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 296
    const-string v1, "companionId"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    new-instance v1, Lcom/google/obf/hi;

    sget-object v2, Lcom/google/obf/hi$b;->displayContainer:Lcom/google/obf/hi$b;

    sget-object v3, Lcom/google/obf/hi$c;->companionView:Lcom/google/obf/hi$c;

    invoke-direct {v1, v2, v3, p2, v0}, Lcom/google/obf/hi;-><init>(Lcom/google/obf/hi$b;Lcom/google/obf/hi$c;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0, v1}, Lcom/google/obf/hj;->b(Lcom/google/obf/hi;)V

    .line 298
    :cond_0
    return-void
.end method

.method public b()Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/obf/hj;->i:Lcom/google/obf/hk;

    invoke-virtual {v0}, Lcom/google/obf/hk;->a()Landroid/webkit/WebView;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/obf/hi;)V
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/obf/hj;->l:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 273
    invoke-direct {p0}, Lcom/google/obf/hj;->d()V

    .line 274
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/obf/hj;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    iget-object v0, p0, Lcom/google/obf/hj;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 263
    return-void
.end method

.method public c()Lcom/google/obf/hl;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/obf/hj;->j:Lcom/google/obf/hl;

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/obf/hj;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    iget-object v0, p0, Lcom/google/obf/hj;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    iget-object v0, p0, Lcom/google/obf/hj;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 278
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 279
    new-instance v0, Lcom/google/obf/hj$1;

    invoke-direct {v0, p0, p1}, Lcom/google/obf/hj$1;-><init>(Lcom/google/obf/hj;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 280
    invoke-virtual {v0, v1}, Lcom/google/obf/hj$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 281
    :cond_0
    return-void
.end method
