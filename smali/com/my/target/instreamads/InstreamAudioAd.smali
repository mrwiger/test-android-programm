.class public final Lcom/my/target/instreamads/InstreamAudioAd;
.super Lcom/my/target/common/BaseAd;
.source "InstreamAudioAd.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;,
        Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;,
        Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_LOADING_TIMEOUT_SECONDS:I = 0xa

.field private static final MIN_LOADING_TIMEOUT_SECONDS:I = 0x5


# instance fields
.field private audioDuration:F

.field private final context:Landroid/content/Context;

.field private engine:Lcom/my/target/core/engines/i;

.field private listener:Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;

.field private final loadOnce:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private loadingTimeoutSeconds:I

.field private midpoints:[F

.field private player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

.field private section:Lcom/my/target/fm;

.field private userMidpoints:[F

.field private volume:F


# direct methods
.method public constructor <init>(ILandroid/content/Context;)V
    .locals 1
    .param p1, "slotId"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    const-string v0, "instreamaudioads"

    invoke-direct {p0, p1, v0}, Lcom/my/target/common/BaseAd;-><init>(ILjava/lang/String;)V

    .line 40
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->loadOnce:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 45
    const/16 v0, 0xa

    iput v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->loadingTimeoutSeconds:I

    .line 46
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->volume:F

    .line 54
    iput-object p2, p0, Lcom/my/target/instreamads/InstreamAudioAd;->context:Landroid/content/Context;

    .line 55
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/my/target/instreamads/InstreamAudioAd;->setTrackingEnvironmentEnabled(Z)V

    .line 56
    const-string v0, "InstreamAudioAd created. Version: 5.1.0"

    invoke-static {v0}, Lcom/my/target/g;->c(Ljava/lang/String;)V

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/my/target/instreamads/InstreamAudioAd;Lcom/my/target/fm;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/my/target/instreamads/InstreamAudioAd;
    .param p1, "x1"    # Lcom/my/target/fm;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/my/target/instreamads/InstreamAudioAd;->handleResult(Lcom/my/target/fm;Ljava/lang/String;)V

    return-void
.end method

.method private handleResult(Lcom/my/target/fm;Ljava/lang/String;)V
    .locals 2
    .param p1, "result"    # Lcom/my/target/fm;
    .param p2, "error"    # Ljava/lang/String;

    .prologue
    .line 305
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->listener:Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;

    if-eqz v0, :cond_2

    .line 307
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/my/target/fm;->hasContent()Z

    move-result v0

    if-nez v0, :cond_3

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->listener:Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;

    if-nez p2, :cond_1

    const-string p2, "no ad"

    .end local p2    # "error":Ljava/lang/String;
    :cond_1
    invoke-interface {v0, p2, p0}, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;->onNoAd(Ljava/lang/String;Lcom/my/target/instreamads/InstreamAudioAd;)V

    .line 325
    :cond_2
    :goto_0
    return-void

    .line 313
    .restart local p2    # "error":Ljava/lang/String;
    :cond_3
    iput-object p1, p0, Lcom/my/target/instreamads/InstreamAudioAd;->section:Lcom/my/target/fm;

    .line 314
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->section:Lcom/my/target/fm;

    iget-object v1, p0, Lcom/my/target/instreamads/InstreamAudioAd;->adConfig:Lcom/my/target/b;

    invoke-static {p0, v0, v1}, Lcom/my/target/core/engines/i;->a(Lcom/my/target/instreamads/InstreamAudioAd;Lcom/my/target/fm;Lcom/my/target/b;)Lcom/my/target/core/engines/i;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    .line 315
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    iget v1, p0, Lcom/my/target/instreamads/InstreamAudioAd;->loadingTimeoutSeconds:I

    invoke-virtual {v0, v1}, Lcom/my/target/core/engines/i;->setLoadingTimeoutSeconds(I)V

    .line 316
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    iget v1, p0, Lcom/my/target/instreamads/InstreamAudioAd;->volume:F

    invoke-virtual {v0, v1}, Lcom/my/target/core/engines/i;->setVolume(F)V

    .line 317
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    if-eqz v0, :cond_4

    .line 319
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    iget-object v1, p0, Lcom/my/target/instreamads/InstreamAudioAd;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    invoke-virtual {v0, v1}, Lcom/my/target/core/engines/i;->setPlayer(Lcom/my/target/instreamads/InstreamAudioAdPlayer;)V

    .line 321
    :cond_4
    iget v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->audioDuration:F

    iget-object v1, p0, Lcom/my/target/instreamads/InstreamAudioAd;->userMidpoints:[F

    invoke-virtual {p0, v0, v1}, Lcom/my/target/instreamads/InstreamAudioAd;->configureMidpoints(F[F)V

    .line 322
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->listener:Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;

    invoke-interface {v0, p0}, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;->onLoad(Lcom/my/target/instreamads/InstreamAudioAd;)V

    goto :goto_0
.end method

.method private start(Ljava/lang/String;)V
    .locals 1
    .param p1, "sectionName"    # Ljava/lang/String;

    .prologue
    .line 329
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    if-nez v0, :cond_0

    .line 331
    const-string v0, "Unable to start ad: not loaded yet"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 341
    :goto_0
    return-void

    .line 334
    :cond_0
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    invoke-virtual {v0}, Lcom/my/target/core/engines/i;->getPlayer()Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    move-result-object v0

    if-nez v0, :cond_1

    .line 336
    const-string v0, "Unable to start ad: player has not set"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 340
    :cond_1
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    invoke-virtual {v0, p1}, Lcom/my/target/core/engines/i;->start(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final configureMidpoints(F)V
    .locals 1
    .param p1, "audioDuration"    # F

    .prologue
    .line 158
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/my/target/instreamads/InstreamAudioAd;->configureMidpoints(F[F)V

    .line 159
    return-void
.end method

.method public final configureMidpoints(F[F)V
    .locals 2
    .param p1, "audioDuration"    # F
    .param p2, "midpoints"    # [F

    .prologue
    .line 163
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_1

    .line 165
    const-string v0, "midpoints are not configured, duration is not set or <= zero"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 168
    :cond_1
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->midpoints:[F

    if-eqz v0, :cond_2

    .line 170
    const-string v0, "midpoints already configured"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 173
    :cond_2
    iput-object p2, p0, Lcom/my/target/instreamads/InstreamAudioAd;->userMidpoints:[F

    .line 174
    iput p1, p0, Lcom/my/target/instreamads/InstreamAudioAd;->audioDuration:F

    .line 175
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->section:Lcom/my/target/fm;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->section:Lcom/my/target/fm;

    const-string v1, "midroll"

    invoke-virtual {v0, v1}, Lcom/my/target/fm;->a(Ljava/lang/String;)Lcom/my/target/al;

    move-result-object v0

    .line 178
    if-eqz v0, :cond_0

    .line 180
    iget-object v1, p0, Lcom/my/target/instreamads/InstreamAudioAd;->userMidpoints:[F

    invoke-static {v0, v1, p1}, Lcom/my/target/ci;->a(Lcom/my/target/al;[FF)[F

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->midpoints:[F

    .line 181
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    iget-object v1, p0, Lcom/my/target/instreamads/InstreamAudioAd;->midpoints:[F

    invoke-virtual {v0, v1}, Lcom/my/target/core/engines/i;->setMidpoints([F)V

    goto :goto_0
.end method

.method public final configureMidpointsPercents(F[F)V
    .locals 1
    .param p1, "audioDuration"    # F
    .param p2, "midpointsPercents"    # [F

    .prologue
    .line 191
    if-nez p2, :cond_0

    .line 193
    invoke-virtual {p0, p1}, Lcom/my/target/instreamads/InstreamAudioAd;->configureMidpoints(F)V

    .line 197
    :goto_0
    return-void

    .line 196
    :cond_0
    invoke-static {p1, p2}, Lcom/my/target/ci;->a(F[F)[F

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/my/target/instreamads/InstreamAudioAd;->configureMidpoints(F[F)V

    goto :goto_0
.end method

.method public final destroy()V
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->listener:Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;

    .line 242
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    invoke-virtual {v0}, Lcom/my/target/core/engines/i;->destroy()V

    .line 246
    :cond_0
    return-void
.end method

.method public final getListener()Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->listener:Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;

    return-object v0
.end method

.method public final getLoadingTimeout()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->loadingTimeoutSeconds:I

    return v0
.end method

.method public final getMidPoints()[F
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->midpoints:[F

    if-nez v0, :cond_0

    .line 133
    const/4 v0, 0x0

    new-array v0, v0, [F

    .line 135
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->midpoints:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    goto :goto_0
.end method

.method public final getPlayer()Lcom/my/target/instreamads/InstreamAudioAdPlayer;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    return-object v0
.end method

.method public final getVolume()F
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    invoke-virtual {v0}, Lcom/my/target/core/engines/i;->getVolume()F

    move-result v0

    .line 112
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->volume:F

    goto :goto_0
.end method

.method public final handleCompanionClick(Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;)V
    .locals 1
    .param p1, "instreamAdCompanionBanner"    # Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;

    .prologue
    .line 281
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    invoke-virtual {v0, p1}, Lcom/my/target/core/engines/i;->handleCompanionClick(Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;)V

    .line 285
    :cond_0
    return-void
.end method

.method public final handleCompanionClick(Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;Landroid/content/Context;)V
    .locals 1
    .param p1, "instreamAdCompanionBanner"    # Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 289
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    invoke-virtual {v0, p1, p2}, Lcom/my/target/core/engines/i;->handleCompanionClick(Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;Landroid/content/Context;)V

    .line 293
    :cond_0
    return-void
.end method

.method public final handleCompanionShow(Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;)V
    .locals 1
    .param p1, "instreamAdCompanionBanner"    # Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;

    .prologue
    .line 297
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    invoke-virtual {v0, p1}, Lcom/my/target/core/engines/i;->handleCompanionShow(Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;)V

    .line 301
    :cond_0
    return-void
.end method

.method public final load()V
    .locals 3

    .prologue
    .line 140
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->loadOnce:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 142
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " instance just loaded once, don\'t call load() more than one time per instance"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->b(Ljava/lang/String;)V

    .line 154
    :goto_0
    return-void

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->adConfig:Lcom/my/target/b;

    iget v1, p0, Lcom/my/target/instreamads/InstreamAudioAd;->loadingTimeoutSeconds:I

    invoke-static {v0, v1}, Lcom/my/target/fj;->newFactory(Lcom/my/target/b;I)Lcom/my/target/c;

    move-result-object v0

    new-instance v1, Lcom/my/target/instreamads/InstreamAudioAd$1;

    invoke-direct {v1, p0}, Lcom/my/target/instreamads/InstreamAudioAd$1;-><init>(Lcom/my/target/instreamads/InstreamAudioAd;)V

    invoke-virtual {v0, v1}, Lcom/my/target/c;->a(Lcom/my/target/c$b;)Lcom/my/target/c;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/instreamads/InstreamAudioAd;->context:Landroid/content/Context;

    .line 153
    invoke-virtual {v0, v1}, Lcom/my/target/c;->a(Landroid/content/Context;)Lcom/my/target/c;

    goto :goto_0
.end method

.method public final pause()V
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    invoke-virtual {v0}, Lcom/my/target/core/engines/i;->pause()V

    .line 205
    :cond_0
    return-void
.end method

.method public final resume()V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    invoke-virtual {v0}, Lcom/my/target/core/engines/i;->resume()V

    .line 213
    :cond_0
    return-void
.end method

.method public final setListener(Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/my/target/instreamads/InstreamAudioAd;->listener:Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;

    .line 67
    return-void
.end method

.method public final setLoadingTimeout(I)V
    .locals 2
    .param p1, "loadingTimeout"    # I

    .prologue
    const/4 v1, 0x5

    .line 76
    if-ge p1, v1, :cond_1

    .line 78
    const-string v0, "unable to set ad loading timeout < 5, set to 5 seconds"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 79
    iput v1, p0, Lcom/my/target/instreamads/InstreamAudioAd;->loadingTimeoutSeconds:I

    .line 86
    :goto_0
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    iget v1, p0, Lcom/my/target/instreamads/InstreamAudioAd;->loadingTimeoutSeconds:I

    invoke-virtual {v0, v1}, Lcom/my/target/core/engines/i;->setLoadingTimeoutSeconds(I)V

    .line 90
    :cond_0
    return-void

    .line 83
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ad loading timeout set to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " seconds"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 84
    iput p1, p0, Lcom/my/target/instreamads/InstreamAudioAd;->loadingTimeoutSeconds:I

    goto :goto_0
.end method

.method public final setPlayer(Lcom/my/target/instreamads/InstreamAudioAdPlayer;)V
    .locals 1
    .param p1, "player"    # Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/my/target/instreamads/InstreamAudioAd;->player:Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    .line 100
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    invoke-virtual {v0, p1}, Lcom/my/target/core/engines/i;->setPlayer(Lcom/my/target/instreamads/InstreamAudioAdPlayer;)V

    .line 104
    :cond_0
    return-void
.end method

.method public final setVolume(F)V
    .locals 2
    .param p1, "volume"    # F

    .prologue
    .line 117
    const/4 v0, 0x0

    invoke-static {p1, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p1, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-lez v0, :cond_2

    .line 119
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "unable to set volume"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", volume must be in range [0..1]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 127
    :cond_1
    :goto_0
    return-void

    .line 122
    :cond_2
    iput p1, p0, Lcom/my/target/instreamads/InstreamAudioAd;->volume:F

    .line 123
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    if-eqz v0, :cond_1

    .line 125
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    invoke-virtual {v0, p1}, Lcom/my/target/core/engines/i;->setVolume(F)V

    goto :goto_0
.end method

.method public final skip()V
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    invoke-virtual {v0}, Lcom/my/target/core/engines/i;->skip()V

    .line 229
    :cond_0
    return-void
.end method

.method public final skipBanner()V
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    invoke-virtual {v0}, Lcom/my/target/core/engines/i;->skipBanner()V

    .line 237
    :cond_0
    return-void
.end method

.method public final startMidroll(F)V
    .locals 1
    .param p1, "point"    # F

    .prologue
    .line 265
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    if-nez v0, :cond_0

    .line 267
    const-string v0, "Unable to start ad: not loaded yet"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 277
    :goto_0
    return-void

    .line 270
    :cond_0
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    invoke-virtual {v0}, Lcom/my/target/core/engines/i;->getPlayer()Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    move-result-object v0

    if-nez v0, :cond_1

    .line 272
    const-string v0, "Unable to start ad: player has not set"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 276
    :cond_1
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    invoke-virtual {v0, p1}, Lcom/my/target/core/engines/i;->startMidroll(F)V

    goto :goto_0
.end method

.method public final startPauseroll()V
    .locals 1

    .prologue
    .line 260
    const-string v0, "pauseroll"

    invoke-direct {p0, v0}, Lcom/my/target/instreamads/InstreamAudioAd;->start(Ljava/lang/String;)V

    .line 261
    return-void
.end method

.method public final startPostroll()V
    .locals 1

    .prologue
    .line 255
    const-string v0, "postroll"

    invoke-direct {p0, v0}, Lcom/my/target/instreamads/InstreamAudioAd;->start(Ljava/lang/String;)V

    .line 256
    return-void
.end method

.method public final startPreroll()V
    .locals 1

    .prologue
    .line 250
    const-string v0, "preroll"

    invoke-direct {p0, v0}, Lcom/my/target/instreamads/InstreamAudioAd;->start(Ljava/lang/String;)V

    .line 251
    return-void
.end method

.method public final stop()V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAudioAd;->engine:Lcom/my/target/core/engines/i;

    invoke-virtual {v0}, Lcom/my/target/core/engines/i;->stop()V

    .line 221
    :cond_0
    return-void
.end method
