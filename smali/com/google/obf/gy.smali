.class abstract Lcom/google/obf/gy;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/ads/interactivemedia/v3/api/BaseManager;
.implements Lcom/google/obf/hj$d;


# instance fields
.field protected final a:Lcom/google/obf/hj;

.field protected final b:Ljava/lang/String;

.field protected c:Lcom/google/obf/ht;

.field protected d:Lcom/google/obf/hb;

.field protected e:Lcom/google/obf/hc;

.field protected f:Z

.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventListener;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/google/obf/he;

.field private i:Lcom/google/ads/interactivemedia/v3/impl/data/b;

.field private j:Lcom/google/obf/gi;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Lcom/google/obf/hj;Lcom/google/obf/hl;Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;Lcom/google/obf/gi;Landroid/content/Context;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/ads/interactivemedia/v3/api/AdError;
        }
    .end annotation

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/gy;->f:Z

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/gy;->g:Ljava/util/List;

    .line 4
    new-instance v0, Lcom/google/obf/he;

    invoke-direct {v0}, Lcom/google/obf/he;-><init>()V

    iput-object v0, p0, Lcom/google/obf/gy;->h:Lcom/google/obf/he;

    .line 5
    iput-object p1, p0, Lcom/google/obf/gy;->b:Ljava/lang/String;

    .line 6
    iput-object p2, p0, Lcom/google/obf/gy;->a:Lcom/google/obf/hj;

    .line 7
    if-eqz p5, :cond_0

    .line 8
    iput-object p5, p0, Lcom/google/obf/gy;->j:Lcom/google/obf/gi;

    .line 10
    :goto_0
    iget-object v0, p0, Lcom/google/obf/gy;->j:Lcom/google/obf/gi;

    invoke-virtual {v0, p7}, Lcom/google/obf/gi;->a(Z)V

    .line 11
    invoke-virtual {p2, p0, p1}, Lcom/google/obf/hj;->a(Lcom/google/obf/hj$d;Ljava/lang/String;)V

    .line 12
    iget-object v0, p0, Lcom/google/obf/gy;->j:Lcom/google/obf/gi;

    invoke-virtual {v0}, Lcom/google/obf/gi;->a()V

    .line 13
    return-void

    .line 9
    :cond_0
    new-instance v0, Lcom/google/obf/gi;

    invoke-interface {p4}, Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;->getAdContainer()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-direct {v0, p1, p2, v1}, Lcom/google/obf/gi;-><init>(Ljava/lang/String;Lcom/google/obf/hj;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/obf/gy;->j:Lcom/google/obf/gi;

    goto :goto_0
.end method

.method private a(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;)V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/obf/gy;->a(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Ljava/util/Map;)V

    .line 54
    return-void
.end method

.method private a(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    new-instance v1, Lcom/google/obf/gl;

    iget-object v0, p0, Lcom/google/obf/gy;->i:Lcom/google/ads/interactivemedia/v3/impl/data/b;

    invoke-direct {v1, p1, v0, p2}, Lcom/google/obf/gl;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Lcom/google/ads/interactivemedia/v3/api/Ad;Ljava/util/Map;)V

    .line 56
    iget-object v0, p0, Lcom/google/obf/gy;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventListener;

    .line 57
    invoke-interface {v0, v1}, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventListener;->onAdEvent(Lcom/google/ads/interactivemedia/v3/api/AdEvent;)V

    goto :goto_0

    .line 59
    :cond_0
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/obf/gy;->c:Lcom/google/obf/ht;

    invoke-interface {v0}, Lcom/google/obf/ht;->d()V

    .line 45
    iget-object v0, p0, Lcom/google/obf/gy;->e:Lcom/google/obf/hc;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/google/obf/gy;->e:Lcom/google/obf/hc;

    invoke-virtual {v0}, Lcom/google/obf/hc;->c()V

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/google/obf/gy;->j:Lcom/google/obf/gi;

    invoke-virtual {v0}, Lcom/google/obf/gi;->b()V

    .line 48
    iget-object v0, p0, Lcom/google/obf/gy;->a:Lcom/google/obf/hj;

    iget-object v1, p0, Lcom/google/obf/gy;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/obf/hj;->c(Ljava/lang/String;)V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/gy;->i:Lcom/google/ads/interactivemedia/v3/impl/data/b;

    .line 50
    return-void
.end method

.method public a(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 94
    new-instance v0, Lcom/google/obf/gk;

    new-instance v1, Lcom/google/ads/interactivemedia/v3/api/AdError;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/ads/interactivemedia/v3/api/AdError;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;ILjava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/google/obf/gk;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError;)V

    .line 95
    invoke-virtual {p0, v0}, Lcom/google/obf/gy;->a(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent;)V

    .line 96
    return-void
.end method

.method public a(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 97
    new-instance v0, Lcom/google/obf/gk;

    new-instance v1, Lcom/google/ads/interactivemedia/v3/api/AdError;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/ads/interactivemedia/v3/api/AdError;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/google/obf/gk;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError;)V

    .line 98
    invoke-virtual {p0, v0}, Lcom/google/obf/gy;->a(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent;)V

    .line 99
    return-void
.end method

.method a(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent;)V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/obf/gy;->h:Lcom/google/obf/he;

    invoke-virtual {v0, p1}, Lcom/google/obf/he;->a(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent;)V

    .line 61
    return-void
.end method

.method a(Lcom/google/ads/interactivemedia/v3/impl/data/b;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/obf/gy;->i:Lcom/google/ads/interactivemedia/v3/impl/data/b;

    .line 63
    return-void
.end method

.method protected a(Lcom/google/obf/hi$c;)V
    .locals 4

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/obf/gy;->a:Lcom/google/obf/hj;

    new-instance v1, Lcom/google/obf/hi;

    sget-object v2, Lcom/google/obf/hi$b;->adsManager:Lcom/google/obf/hi$b;

    iget-object v3, p0, Lcom/google/obf/gy;->b:Ljava/lang/String;

    invoke-direct {v1, v2, p1, v3}, Lcom/google/obf/hi;-><init>(Lcom/google/obf/hi$b;Lcom/google/obf/hi$c;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/obf/hj;->b(Lcom/google/obf/hi;)V

    .line 52
    return-void
.end method

.method public a(Lcom/google/obf/hj$c;)V
    .locals 4

    .prologue
    .line 64
    iget-object v0, p1, Lcom/google/obf/hj$c;->a:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    .line 65
    iget-object v1, p1, Lcom/google/obf/hj$c;->b:Lcom/google/ads/interactivemedia/v3/impl/data/b;

    .line 66
    sget-object v2, Lcom/google/obf/gy$1;->a:[I

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 88
    :cond_0
    :goto_0
    iget-object v1, p1, Lcom/google/obf/hj$c;->c:Ljava/util/Map;

    if-eqz v1, :cond_6

    .line 89
    iget-object v1, p1, Lcom/google/obf/hj$c;->c:Ljava/util/Map;

    invoke-direct {p0, v0, v1}, Lcom/google/obf/gy;->a(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;Ljava/util/Map;)V

    .line 91
    :goto_1
    sget-object v1, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->COMPLETED:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->SKIPPED:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    if-ne v0, v1, :cond_2

    .line 92
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/obf/gy;->a(Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    .line 93
    :cond_2
    return-void

    .line 67
    :pswitch_0
    invoke-virtual {p0, v1}, Lcom/google/obf/gy;->a(Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    goto :goto_0

    .line 69
    :pswitch_1
    if-eqz v1, :cond_3

    .line 70
    invoke-virtual {p0, v1}, Lcom/google/obf/gy;->a(Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    .line 71
    :cond_3
    iget-object v2, p0, Lcom/google/obf/gy;->c:Lcom/google/obf/ht;

    invoke-interface {v2, v1}, Lcom/google/obf/ht;->a(Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    goto :goto_0

    .line 73
    :pswitch_2
    iget-object v1, p0, Lcom/google/obf/gy;->c:Lcom/google/obf/ht;

    invoke-interface {v1}, Lcom/google/obf/ht;->a()V

    goto :goto_0

    .line 75
    :pswitch_3
    iget-object v1, p0, Lcom/google/obf/gy;->e:Lcom/google/obf/hc;

    if-eqz v1, :cond_4

    .line 76
    iget-object v1, p0, Lcom/google/obf/gy;->e:Lcom/google/obf/hc;

    invoke-virtual {v1}, Lcom/google/obf/hc;->c()V

    .line 77
    :cond_4
    iget-object v1, p0, Lcom/google/obf/gy;->j:Lcom/google/obf/gi;

    invoke-virtual {v1}, Lcom/google/obf/gi;->c()V

    goto :goto_0

    .line 79
    :pswitch_4
    iget-object v1, p0, Lcom/google/obf/gy;->e:Lcom/google/obf/hc;

    if-eqz v1, :cond_5

    .line 80
    iget-object v1, p0, Lcom/google/obf/gy;->e:Lcom/google/obf/hc;

    invoke-virtual {v1}, Lcom/google/obf/hc;->b()V

    .line 81
    :cond_5
    iget-object v1, p0, Lcom/google/obf/gy;->j:Lcom/google/obf/gi;

    invoke-virtual {v1}, Lcom/google/obf/gi;->d()V

    goto :goto_0

    .line 83
    :pswitch_5
    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/v3/impl/data/b;->getClickThruUrl()Ljava/lang/String;

    move-result-object v1

    .line 84
    invoke-static {v1}, Lcom/google/obf/ic;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 85
    iget-object v2, p0, Lcom/google/obf/gy;->a:Lcom/google/obf/hj;

    invoke-virtual {v2, v1}, Lcom/google/obf/hj;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 86
    :pswitch_6
    iget-object v1, p1, Lcom/google/obf/hj$c;->f:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/obf/ic;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 87
    iget-object v1, p0, Lcom/google/obf/gy;->a:Lcom/google/obf/hj;

    iget-object v2, p1, Lcom/google/obf/hj$c;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/obf/hj;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 90
    :cond_6
    invoke-direct {p0, v0}, Lcom/google/obf/gy;->a(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;)V

    goto :goto_1

    .line 66
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public a(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 100
    return-void
.end method

.method public addAdErrorListener(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent$AdErrorListener;)V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/obf/gy;->h:Lcom/google/obf/he;

    invoke-virtual {v0, p1}, Lcom/google/obf/he;->a(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent$AdErrorListener;)V

    .line 37
    return-void
.end method

.method public addAdEventListener(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventListener;)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/obf/gy;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    return-void
.end method

.method public getAdProgress()Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/obf/gy;->f:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;->VIDEO_TIME_NOT_READY:Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;

    .line 34
    :goto_0
    return-object v0

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/google/obf/gy;->c:Lcom/google/obf/ht;

    invoke-interface {v0}, Lcom/google/obf/ht;->getAdProgress()Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;

    move-result-object v0

    goto :goto_0
.end method

.method public getCurrentAd()Lcom/google/ads/interactivemedia/v3/api/Ad;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/obf/gy;->i:Lcom/google/ads/interactivemedia/v3/impl/data/b;

    return-object v0
.end method

.method public init(Lcom/google/ads/interactivemedia/v3/api/AdsRenderingSettings;)V
    .locals 6

    .prologue
    .line 16
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 17
    const-string v2, "adsRenderingSettings"

    .line 18
    if-nez p1, :cond_2

    new-instance v0, Lcom/google/obf/gt;

    invoke-direct {v0}, Lcom/google/obf/gt;-><init>()V

    .line 19
    :goto_0
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    iget-object v0, p0, Lcom/google/obf/gy;->e:Lcom/google/obf/hc;

    if-eqz v0, :cond_0

    .line 21
    iget-object v0, p0, Lcom/google/obf/gy;->e:Lcom/google/obf/hc;

    invoke-virtual {v0}, Lcom/google/obf/hc;->a()Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;

    move-result-object v0

    .line 22
    sget-object v2, Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;->VIDEO_TIME_NOT_READY:Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;

    invoke-virtual {v0, v2}, Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 23
    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;->getCurrentTime()F

    move-result v0

    float-to-double v2, v0

    .line 24
    const-string v0, "IMASDK"

    const/16 v4, 0x44

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "AdsManager.init -> Setting contentStartTime "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    const-string v0, "contentStartTime"

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    :cond_0
    invoke-virtual {p0}, Lcom/google/obf/gy;->isCustomPlaybackUsed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 27
    const-string v0, "sdkOwnedPlayer"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    :cond_1
    iget-object v2, p0, Lcom/google/obf/gy;->c:Lcom/google/obf/ht;

    if-eqz p1, :cond_3

    invoke-interface {p1}, Lcom/google/ads/interactivemedia/v3/api/AdsRenderingSettings;->getDisableUi()Z

    move-result v0

    :goto_1
    invoke-interface {v2, v0}, Lcom/google/obf/ht;->a(Z)V

    .line 29
    iget-object v0, p0, Lcom/google/obf/gy;->a:Lcom/google/obf/hj;

    invoke-virtual {v0, p1}, Lcom/google/obf/hj;->a(Lcom/google/ads/interactivemedia/v3/api/AdsRenderingSettings;)V

    .line 30
    iget-object v0, p0, Lcom/google/obf/gy;->a:Lcom/google/obf/hj;

    new-instance v2, Lcom/google/obf/hi;

    sget-object v3, Lcom/google/obf/hi$b;->adsManager:Lcom/google/obf/hi$b;

    sget-object v4, Lcom/google/obf/hi$c;->init:Lcom/google/obf/hi$c;

    iget-object v5, p0, Lcom/google/obf/gy;->b:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v5, v1}, Lcom/google/obf/hi;-><init>(Lcom/google/obf/hi$b;Lcom/google/obf/hi$c;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Lcom/google/obf/hj;->b(Lcom/google/obf/hi;)V

    .line 31
    return-void

    :cond_2
    move-object v0, p1

    .line 18
    goto :goto_0

    .line 28
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public abstract isCustomPlaybackUsed()Z
.end method

.method public removeAdErrorListener(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent$AdErrorListener;)V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/obf/gy;->h:Lcom/google/obf/he;

    invoke-virtual {v0, p1}, Lcom/google/obf/he;->b(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent$AdErrorListener;)V

    .line 39
    return-void
.end method

.method public removeAdEventListener(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventListener;)V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/obf/gy;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 43
    return-void
.end method
