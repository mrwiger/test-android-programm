.class public final Lcom/my/target/fn;
.super Lcom/my/target/c;
.source "InstreamAdFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/fn$a;,
        Lcom/my/target/fn$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/my/target/c",
        "<",
        "Lcom/my/target/fq;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/my/target/ae;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/my/target/ck;

.field private c:Ljava/lang/Runnable;


# direct methods
.method private constructor <init>(Lcom/my/target/b;I)V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lcom/my/target/fn;-><init>(Ljava/util/List;Lcom/my/target/b;I)V

    .line 52
    return-void
.end method

.method private constructor <init>(Ljava/util/List;Lcom/my/target/b;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/ae;",
            ">;",
            "Lcom/my/target/b;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 56
    new-instance v0, Lcom/my/target/fn$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/my/target/fn$a;-><init>(B)V

    invoke-direct {p0, v0, p2}, Lcom/my/target/c;-><init>(Lcom/my/target/c$a;Lcom/my/target/b;)V

    .line 57
    iput-object p1, p0, Lcom/my/target/fn;->a:Ljava/util/List;

    .line 58
    mul-int/lit16 v0, p3, 0x3e8

    invoke-static {v0}, Lcom/my/target/ck;->k(I)Lcom/my/target/ck;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/fn;->b:Lcom/my/target/ck;

    .line 59
    return-void
.end method

.method static synthetic a(Lcom/my/target/fn;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/my/target/fn;->c:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic b(Lcom/my/target/fn;)Lcom/my/target/ck;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/my/target/fn;->b:Lcom/my/target/ck;

    return-object v0
.end method

.method public static newFactory(Lcom/my/target/b;I)Lcom/my/target/c;
    .locals 1
    .param p0, "adConfig"    # Lcom/my/target/b;
    .param p1, "loadingTimeoutSeconds"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/b;",
            "I)",
            "Lcom/my/target/c",
            "<",
            "Lcom/my/target/fq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    new-instance v0, Lcom/my/target/fn;

    invoke-direct {v0, p0, p1}, Lcom/my/target/fn;-><init>(Lcom/my/target/b;I)V

    return-object v0
.end method

.method public static newFactoryForAdService(Lcom/my/target/ae;Lcom/my/target/b;I)Lcom/my/target/c;
    .locals 2
    .param p0, "adService"    # Lcom/my/target/ae;
    .param p1, "adConfig"    # Lcom/my/target/b;
    .param p2, "loadingTimeoutSeconds"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/ae;",
            "Lcom/my/target/b;",
            "I)",
            "Lcom/my/target/c",
            "<",
            "Lcom/my/target/fq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 34
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    new-instance v1, Lcom/my/target/fn;

    invoke-direct {v1, v0, p1, p2}, Lcom/my/target/fn;-><init>(Ljava/util/List;Lcom/my/target/b;I)V

    return-object v1
.end method

.method public static newFactoryForAdServices(Ljava/util/List;Lcom/my/target/b;I)Lcom/my/target/c;
    .locals 1
    .param p1, "adConfig"    # Lcom/my/target/b;
    .param p2, "loadingTimeoutSeconds"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/ae;",
            ">;",
            "Lcom/my/target/b;",
            "I)",
            "Lcom/my/target/c",
            "<",
            "Lcom/my/target/fq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    .local p0, "adServices":Ljava/util/List;, "Ljava/util/List<Lcom/my/target/ae;>;"
    new-instance v0, Lcom/my/target/fn;

    invoke-direct {v0, p0, p1, p2}, Lcom/my/target/fn;-><init>(Ljava/util/List;Lcom/my/target/b;I)V

    return-object v0
.end method


# virtual methods
.method protected final synthetic c(Landroid/content/Context;)Lcom/my/target/ak;
    .locals 6

    .prologue
    .line 22
    .line 1064
    iget-object v0, p0, Lcom/my/target/fn;->c:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 1066
    new-instance v0, Lcom/my/target/fn$1;

    invoke-direct {v0, p0}, Lcom/my/target/fn$1;-><init>(Lcom/my/target/fn;)V

    iput-object v0, p0, Lcom/my/target/fn;->c:Ljava/lang/Runnable;

    .line 1076
    :cond_0
    iget-object v0, p0, Lcom/my/target/fn;->b:Lcom/my/target/ck;

    iget-object v1, p0, Lcom/my/target/fn;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->d(Ljava/lang/Runnable;)V

    .line 1077
    iget-object v0, p0, Lcom/my/target/fn;->a:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 1079
    invoke-static {}, Lcom/my/target/at;->ah()Lcom/my/target/at;

    move-result-object v4

    .line 1080
    iget-object v1, p0, Lcom/my/target/fn;->a:Ljava/util/List;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/my/target/fn;->r:Lcom/my/target/c$a;

    invoke-interface {v0}, Lcom/my/target/c$a;->b()Lcom/my/target/d;

    move-result-object v3

    move-object v0, p0

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/my/target/fn;->a(Ljava/util/List;Lcom/my/target/ak;Lcom/my/target/d;Lcom/my/target/at;Landroid/content/Context;)Lcom/my/target/ak;

    move-result-object v0

    check-cast v0, Lcom/my/target/fq;

    .line 1081
    if-eqz v0, :cond_1

    .line 1083
    iget-object v1, p0, Lcom/my/target/fn;->r:Lcom/my/target/c$a;

    invoke-interface {v1}, Lcom/my/target/c$a;->c()Lcom/my/target/e;

    move-result-object v1

    .line 1084
    if-eqz v1, :cond_1

    .line 1086
    iget-object v2, p0, Lcom/my/target/fn;->adConfig:Lcom/my/target/b;

    invoke-virtual {v1, v0, v2, p1}, Lcom/my/target/e;->a(Lcom/my/target/ak;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/ak;

    move-result-object v0

    check-cast v0, Lcom/my/target/fq;

    .line 1089
    :cond_1
    :goto_0
    return-object v0

    .line 1092
    :cond_2
    invoke-super {p0, p1}, Lcom/my/target/c;->c(Landroid/content/Context;)Lcom/my/target/ak;

    move-result-object v0

    check-cast v0, Lcom/my/target/fq;

    goto :goto_0
.end method
