.class public Lru/cn/domain/PurchaseManager;
.super Ljava/lang/Object;
.source "PurchaseManager.java"


# static fields
.field private static final INSTANCE:Lru/cn/domain/PurchaseManager;


# instance fields
.field private store:Lru/cn/domain/stores/PurchaseStore;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lru/cn/domain/PurchaseManager;

    invoke-direct {v0}, Lru/cn/domain/PurchaseManager;-><init>()V

    sput-object v0, Lru/cn/domain/PurchaseManager;->INSTANCE:Lru/cn/domain/PurchaseManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static Init(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    sget-object v0, Lru/cn/domain/PurchaseManager;->INSTANCE:Lru/cn/domain/PurchaseManager;

    invoke-static {p0}, Lru/cn/domain/stores/PurchaseStoreFactory;->createStore(Landroid/content/Context;)Lru/cn/domain/stores/PurchaseStore;

    move-result-object v1

    iput-object v1, v0, Lru/cn/domain/PurchaseManager;->store:Lru/cn/domain/stores/PurchaseStore;

    .line 19
    return-void
.end method

.method public static externalStoreName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lru/cn/domain/PurchaseManager;->INSTANCE:Lru/cn/domain/PurchaseManager;

    iget-object v0, v0, Lru/cn/domain/PurchaseManager;->store:Lru/cn/domain/stores/PurchaseStore;

    if-nez v0, :cond_0

    .line 30
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lru/cn/domain/PurchaseManager;->INSTANCE:Lru/cn/domain/PurchaseManager;

    iget-object v0, v0, Lru/cn/domain/PurchaseManager;->store:Lru/cn/domain/stores/PurchaseStore;

    invoke-virtual {v0}, Lru/cn/domain/stores/PurchaseStore;->storeName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static handleActivityResult(IILandroid/content/Intent;)Z
    .locals 1
    .param p0, "requestCode"    # I
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 46
    sget-object v0, Lru/cn/domain/PurchaseManager;->INSTANCE:Lru/cn/domain/PurchaseManager;

    iget-object v0, v0, Lru/cn/domain/PurchaseManager;->store:Lru/cn/domain/stores/PurchaseStore;

    if-nez v0, :cond_0

    .line 47
    const/4 v0, 0x0

    .line 49
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lru/cn/domain/PurchaseManager;->INSTANCE:Lru/cn/domain/PurchaseManager;

    iget-object v0, v0, Lru/cn/domain/PurchaseManager;->store:Lru/cn/domain/stores/PurchaseStore;

    invoke-virtual {v0, p0, p1, p2}, Lru/cn/domain/stores/PurchaseStore;->handleResult(IILandroid/content/Intent;)Z

    move-result v0

    goto :goto_0
.end method

.method public static isAdDisabled()Z
    .locals 2

    .prologue
    .line 54
    sget-object v0, Lru/cn/domain/PurchaseManager;->INSTANCE:Lru/cn/domain/PurchaseManager;

    iget-object v0, v0, Lru/cn/domain/PurchaseManager;->store:Lru/cn/domain/stores/PurchaseStore;

    if-eqz v0, :cond_0

    sget-object v0, Lru/cn/domain/PurchaseManager;->INSTANCE:Lru/cn/domain/PurchaseManager;

    iget-object v0, v0, Lru/cn/domain/PurchaseManager;->store:Lru/cn/domain/stores/PurchaseStore;

    const-string v1, "month_disable_ads"

    invoke-virtual {v0, v1}, Lru/cn/domain/stores/PurchaseStore;->hasPurchase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static makePurchase(Landroid/app/Activity;Ljava/lang/String;Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "productId"    # Ljava/lang/String;
    .param p2, "listener"    # Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;

    .prologue
    .line 37
    sget-object v0, Lru/cn/domain/PurchaseManager;->INSTANCE:Lru/cn/domain/PurchaseManager;

    iget-object v0, v0, Lru/cn/domain/PurchaseManager;->store:Lru/cn/domain/stores/PurchaseStore;

    if-nez v0, :cond_0

    .line 38
    const/16 v0, -0x3f1

    const/4 v1, 0x0

    invoke-interface {p2, v0, v1}, Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;->onPurchaseCompleted(ILjava/lang/String;)V

    .line 43
    :goto_0
    return-void

    .line 42
    :cond_0
    sget-object v0, Lru/cn/domain/PurchaseManager;->INSTANCE:Lru/cn/domain/PurchaseManager;

    iget-object v0, v0, Lru/cn/domain/PurchaseManager;->store:Lru/cn/domain/stores/PurchaseStore;

    invoke-virtual {v0, p0, p1, p2}, Lru/cn/domain/stores/PurchaseStore;->purchase(Landroid/app/Activity;Ljava/lang/String;Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;)V

    goto :goto_0
.end method
