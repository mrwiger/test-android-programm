.class Lcom/google/obf/jv$a;
.super Lcom/google/obf/jn;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/jv;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/obf/jn",
        "<",
        "Ljava/util/Map$Entry",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field private final transient a:Lcom/google/obf/jm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/jm",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private final transient b:[Ljava/lang/Object;

.field private final transient c:I

.field private final transient d:I


# direct methods
.method constructor <init>(Lcom/google/obf/jm;[Ljava/lang/Object;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/jm",
            "<TK;TV;>;[",
            "Ljava/lang/Object;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/obf/jn;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/obf/jv$a;->a:Lcom/google/obf/jm;

    .line 3
    iput-object p2, p0, Lcom/google/obf/jv$a;->b:[Ljava/lang/Object;

    .line 4
    iput p3, p0, Lcom/google/obf/jv$a;->c:I

    .line 5
    iput p4, p0, Lcom/google/obf/jv$a;->d:I

    .line 6
    return-void
.end method

.method static synthetic a(Lcom/google/obf/jv$a;)I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcom/google/obf/jv$a;->d:I

    return v0
.end method

.method static synthetic b(Lcom/google/obf/jv$a;)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/obf/jv$a;->b:[Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(Lcom/google/obf/jv$a;)I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/google/obf/jv$a;->c:I

    return v0
.end method


# virtual methods
.method public a()Lcom/google/obf/jy;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/obf/jy",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 7
    invoke-virtual {p0}, Lcom/google/obf/jv$a;->b()Lcom/google/obf/jl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/obf/jl;->a()Lcom/google/obf/jy;

    move-result-object v0

    return-object v0
.end method

.method c()Z
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x1

    return v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 9
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-eqz v1, :cond_0

    .line 10
    check-cast p1, Ljava/util/Map$Entry;

    .line 11
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 12
    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    .line 13
    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/google/obf/jv$a;->a:Lcom/google/obf/jm;

    invoke-virtual {v3, v1}, Lcom/google/obf/jm;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 14
    :cond_0
    return v0
.end method

.method e()Lcom/google/obf/jl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/obf/jl",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 8
    new-instance v0, Lcom/google/obf/jv$a$1;

    invoke-direct {v0, p0}, Lcom/google/obf/jv$a$1;-><init>(Lcom/google/obf/jv$a;)V

    return-object v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/google/obf/jv$a;->a()Lcom/google/obf/jy;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lcom/google/obf/jv$a;->d:I

    return v0
.end method
