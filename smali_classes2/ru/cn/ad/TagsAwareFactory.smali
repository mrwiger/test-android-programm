.class final Lru/cn/ad/TagsAwareFactory;
.super Ljava/lang/Object;
.source "TagsAwareFactory.java"

# interfaces
.implements Lru/cn/ad/AdAdapter$Factory;


# instance fields
.field private final factory:Lru/cn/ad/AdAdapter$Factory;

.field private tags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lru/cn/ad/AdAdapter$Factory;)V
    .locals 0
    .param p1, "factory"    # Lru/cn/ad/AdAdapter$Factory;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lru/cn/ad/TagsAwareFactory;->factory:Lru/cn/ad/AdAdapter$Factory;

    .line 15
    return-void
.end method


# virtual methods
.method public create(Lru/cn/api/money_miner/replies/AdSystem;)Lru/cn/ad/AdAdapter;
    .locals 2
    .param p1, "system"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 23
    iget-object v0, p0, Lru/cn/ad/TagsAwareFactory;->tags:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/ad/TagsAwareFactory;->tags:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lru/cn/api/money_miner/replies/AdSystem;->predicate:Lru/cn/api/money_miner/replies/AdPredicate;

    if-nez v0, :cond_1

    .line 24
    :cond_0
    iget-object v0, p0, Lru/cn/ad/TagsAwareFactory;->factory:Lru/cn/ad/AdAdapter$Factory;

    invoke-interface {v0, p1}, Lru/cn/ad/AdAdapter$Factory;->create(Lru/cn/api/money_miner/replies/AdSystem;)Lru/cn/ad/AdAdapter;

    move-result-object v0

    .line 29
    :goto_0
    return-object v0

    .line 26
    :cond_1
    iget-object v0, p1, Lru/cn/api/money_miner/replies/AdSystem;->predicate:Lru/cn/api/money_miner/replies/AdPredicate;

    iget-object v1, p0, Lru/cn/ad/TagsAwareFactory;->tags:Ljava/util/List;

    invoke-virtual {v0, v1}, Lru/cn/api/money_miner/replies/AdPredicate;->matches(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 27
    iget-object v0, p0, Lru/cn/ad/TagsAwareFactory;->factory:Lru/cn/ad/AdAdapter$Factory;

    invoke-interface {v0, p1}, Lru/cn/ad/AdAdapter$Factory;->create(Lru/cn/api/money_miner/replies/AdSystem;)Lru/cn/ad/AdAdapter;

    move-result-object v0

    goto :goto_0

    .line 29
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setTags(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p1, "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    iput-object p1, p0, Lru/cn/ad/TagsAwareFactory;->tags:Ljava/util/List;

    .line 19
    return-void
.end method
