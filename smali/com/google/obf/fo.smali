.class public final Lcom/google/obf/fo;
.super Lcom/google/obf/ew;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/obf/ew",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/google/obf/ex;


# instance fields
.field private final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/obf/ew;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/ew",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/google/obf/fo$1;

    invoke-direct {v0}, Lcom/google/obf/fo$1;-><init>()V

    sput-object v0, Lcom/google/obf/fo;->a:Lcom/google/obf/ex;

    return-void
.end method

.method public constructor <init>(Lcom/google/obf/eg;Lcom/google/obf/ew;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/eg;",
            "Lcom/google/obf/ew",
            "<TE;>;",
            "Ljava/lang/Class",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/obf/ew;-><init>()V

    .line 2
    new-instance v0, Lcom/google/obf/ga;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/obf/ga;-><init>(Lcom/google/obf/eg;Lcom/google/obf/ew;Ljava/lang/reflect/Type;)V

    iput-object v0, p0, Lcom/google/obf/fo;->c:Lcom/google/obf/ew;

    .line 3
    iput-object p3, p0, Lcom/google/obf/fo;->b:Ljava/lang/Class;

    .line 4
    return-void
.end method


# virtual methods
.method public read(Lcom/google/obf/ge;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p1}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    move-result-object v0

    sget-object v1, Lcom/google/obf/gf;->i:Lcom/google/obf/gf;

    if-ne v0, v1, :cond_0

    .line 6
    invoke-virtual {p1}, Lcom/google/obf/ge;->j()V

    .line 7
    const/4 v0, 0x0

    .line 19
    :goto_0
    return-object v0

    .line 8
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 9
    invoke-virtual {p1}, Lcom/google/obf/ge;->a()V

    .line 10
    :goto_1
    invoke-virtual {p1}, Lcom/google/obf/ge;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 11
    iget-object v0, p0, Lcom/google/obf/fo;->c:Lcom/google/obf/ew;

    invoke-virtual {v0, p1}, Lcom/google/obf/ew;->read(Lcom/google/obf/ge;)Ljava/lang/Object;

    move-result-object v0

    .line 12
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 14
    :cond_1
    invoke-virtual {p1}, Lcom/google/obf/ge;->b()V

    .line 15
    iget-object v0, p0, Lcom/google/obf/fo;->b:Ljava/lang/Class;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v1

    .line 16
    const/4 v0, 0x0

    :goto_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 17
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v1, v0, v3}, Ljava/lang/reflect/Array;->set(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 18
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move-object v0, v1

    .line 19
    goto :goto_0
.end method

.method public write(Lcom/google/obf/gg;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    if-nez p2, :cond_0

    .line 21
    invoke-virtual {p1}, Lcom/google/obf/gg;->f()Lcom/google/obf/gg;

    .line 29
    :goto_0
    return-void

    .line 23
    :cond_0
    invoke-virtual {p1}, Lcom/google/obf/gg;->b()Lcom/google/obf/gg;

    .line 24
    const/4 v0, 0x0

    invoke-static {p2}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v1

    :goto_1
    if-ge v0, v1, :cond_1

    .line 25
    invoke-static {p2, v0}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v2

    .line 26
    iget-object v3, p0, Lcom/google/obf/fo;->c:Lcom/google/obf/ew;

    invoke-virtual {v3, p1, v2}, Lcom/google/obf/ew;->write(Lcom/google/obf/gg;Ljava/lang/Object;)V

    .line 27
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 28
    :cond_1
    invoke-virtual {p1}, Lcom/google/obf/gg;->c()Lcom/google/obf/gg;

    goto :goto_0
.end method
