.class final Lcom/my/target/core/controllers/c$a;
.super Ljava/lang/Object;
.source "InstreamAdAudioController.java"

# interfaces
.implements Lcom/my/target/instreamads/InstreamAudioAdPlayer$AdPlayerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/core/controllers/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic p:Lcom/my/target/core/controllers/c;

.field private volume:F


# direct methods
.method private constructor <init>(Lcom/my/target/core/controllers/c;)V
    .locals 1

    .prologue
    .line 319
    iput-object p1, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 321
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/my/target/core/controllers/c$a;->volume:F

    return-void
.end method

.method synthetic constructor <init>(Lcom/my/target/core/controllers/c;B)V
    .locals 0

    .prologue
    .line 319
    invoke-direct {p0, p1}, Lcom/my/target/core/controllers/c$a;-><init>(Lcom/my/target/core/controllers/c;)V

    return-void
.end method


# virtual methods
.method public final onAdAudioCompleted()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 391
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v0}, Lcom/my/target/core/controllers/c;->f(Lcom/my/target/core/controllers/c;)I

    move-result v0

    if-eq v0, v3, :cond_1

    .line 393
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v0}, Lcom/my/target/core/controllers/c;->e(Lcom/my/target/core/controllers/c;)Lcom/my/target/aj;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v0}, Lcom/my/target/core/controllers/c;->g(Lcom/my/target/core/controllers/c;)Lcom/my/target/core/controllers/c$b;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 395
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v0}, Lcom/my/target/core/controllers/c;->h(Lcom/my/target/core/controllers/c;)V

    .line 396
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v0}, Lcom/my/target/core/controllers/c;->e(Lcom/my/target/core/controllers/c;)Lcom/my/target/aj;

    move-result-object v0

    .line 397
    iget-object v1, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v1}, Lcom/my/target/core/controllers/c;->i(Lcom/my/target/core/controllers/c;)Lcom/my/target/aj;

    .line 398
    if-eqz v0, :cond_0

    .line 400
    iget-object v1, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-virtual {v0}, Lcom/my/target/aj;->getDuration()F

    move-result v2

    invoke-static {v1, v2}, Lcom/my/target/core/controllers/c;->b(Lcom/my/target/core/controllers/c;F)V

    .line 401
    iget-object v1, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v1}, Lcom/my/target/core/controllers/c;->g(Lcom/my/target/core/controllers/c;)Lcom/my/target/core/controllers/c$b;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/my/target/core/controllers/c$b;->onBannerCompleted(Lcom/my/target/aj;)V

    .line 404
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v0, v3}, Lcom/my/target/core/controllers/c;->a(Lcom/my/target/core/controllers/c;I)I

    .line 406
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v0}, Lcom/my/target/core/controllers/c;->d(Lcom/my/target/core/controllers/c;)Lcom/my/target/ck;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v1}, Lcom/my/target/core/controllers/c;->c(Lcom/my/target/core/controllers/c;)Lcom/my/target/core/controllers/c$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    .line 407
    return-void
.end method

.method public final onAdAudioError(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 381
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v0}, Lcom/my/target/core/controllers/c;->e(Lcom/my/target/core/controllers/c;)Lcom/my/target/aj;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v0}, Lcom/my/target/core/controllers/c;->g(Lcom/my/target/core/controllers/c;)Lcom/my/target/core/controllers/c$b;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v0}, Lcom/my/target/core/controllers/c;->g(Lcom/my/target/core/controllers/c;)Lcom/my/target/core/controllers/c$b;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v1}, Lcom/my/target/core/controllers/c;->e(Lcom/my/target/core/controllers/c;)Lcom/my/target/aj;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/my/target/core/controllers/c$b;->onBannerError(Ljava/lang/String;Lcom/my/target/aj;)V

    .line 385
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v0}, Lcom/my/target/core/controllers/c;->d(Lcom/my/target/core/controllers/c;)Lcom/my/target/ck;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v1}, Lcom/my/target/core/controllers/c;->c(Lcom/my/target/core/controllers/c;)Lcom/my/target/core/controllers/c$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    .line 386
    return-void
.end method

.method public final onAdAudioPaused()V
    .locals 3

    .prologue
    .line 340
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/c;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 341
    iget-object v1, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v1}, Lcom/my/target/core/controllers/c;->e(Lcom/my/target/core/controllers/c;)Lcom/my/target/aj;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 343
    iget-object v1, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v1}, Lcom/my/target/core/controllers/c;->e(Lcom/my/target/core/controllers/c;)Lcom/my/target/aj;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v1

    const-string v2, "playbackPaused"

    invoke-virtual {v1, v2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v0}, Lcom/my/target/core/controllers/c;->d(Lcom/my/target/core/controllers/c;)Lcom/my/target/ck;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v1}, Lcom/my/target/core/controllers/c;->c(Lcom/my/target/core/controllers/c;)Lcom/my/target/core/controllers/c$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    .line 346
    return-void
.end method

.method public final onAdAudioResumed()V
    .locals 3

    .prologue
    .line 351
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/c;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 352
    iget-object v1, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v1}, Lcom/my/target/core/controllers/c;->e(Lcom/my/target/core/controllers/c;)Lcom/my/target/aj;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 354
    iget-object v1, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v1}, Lcom/my/target/core/controllers/c;->e(Lcom/my/target/core/controllers/c;)Lcom/my/target/aj;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v1

    const-string v2, "playbackResumed"

    invoke-virtual {v1, v2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 356
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v0}, Lcom/my/target/core/controllers/c;->d(Lcom/my/target/core/controllers/c;)Lcom/my/target/ck;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v1}, Lcom/my/target/core/controllers/c;->c(Lcom/my/target/core/controllers/c;)Lcom/my/target/core/controllers/c$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->d(Ljava/lang/Runnable;)V

    .line 357
    return-void
.end method

.method public final onAdAudioStarted()V
    .locals 2

    .prologue
    .line 326
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/my/target/core/controllers/c;->a(Lcom/my/target/core/controllers/c;I)I

    .line 327
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v0}, Lcom/my/target/core/controllers/c;->a(Lcom/my/target/core/controllers/c;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v0}, Lcom/my/target/core/controllers/c;->b(Lcom/my/target/core/controllers/c;)Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    iget-object v1, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v1}, Lcom/my/target/core/controllers/c;->b(Lcom/my/target/core/controllers/c;)Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    move-result-object v1

    invoke-interface {v1}, Lcom/my/target/instreamads/InstreamAudioAdPlayer;->getAdAudioDuration()F

    move-result v1

    invoke-static {v0, v1}, Lcom/my/target/core/controllers/c;->a(Lcom/my/target/core/controllers/c;F)V

    .line 334
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v0}, Lcom/my/target/core/controllers/c;->d(Lcom/my/target/core/controllers/c;)Lcom/my/target/ck;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v1}, Lcom/my/target/core/controllers/c;->c(Lcom/my/target/core/controllers/c;)Lcom/my/target/core/controllers/c$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->d(Ljava/lang/Runnable;)V

    .line 335
    return-void
.end method

.method public final onAdAudioStopped()V
    .locals 3

    .prologue
    .line 362
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v0}, Lcom/my/target/core/controllers/c;->f(Lcom/my/target/core/controllers/c;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 364
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v0}, Lcom/my/target/core/controllers/c;->e(Lcom/my/target/core/controllers/c;)Lcom/my/target/aj;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v0}, Lcom/my/target/core/controllers/c;->g(Lcom/my/target/core/controllers/c;)Lcom/my/target/core/controllers/c$b;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 366
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/c;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 367
    if-eqz v0, :cond_0

    .line 369
    iget-object v1, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v1}, Lcom/my/target/core/controllers/c;->e(Lcom/my/target/core/controllers/c;)Lcom/my/target/aj;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v1

    const-string v2, "playbackStopped"

    invoke-virtual {v1, v2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v0}, Lcom/my/target/core/controllers/c;->g(Lcom/my/target/core/controllers/c;)Lcom/my/target/core/controllers/c$b;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v1}, Lcom/my/target/core/controllers/c;->e(Lcom/my/target/core/controllers/c;)Lcom/my/target/aj;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/my/target/core/controllers/c$b;->onBannerStopped(Lcom/my/target/aj;)V

    .line 373
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/my/target/core/controllers/c;->a(Lcom/my/target/core/controllers/c;I)I

    .line 375
    :cond_2
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v0}, Lcom/my/target/core/controllers/c;->d(Lcom/my/target/core/controllers/c;)Lcom/my/target/ck;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v1}, Lcom/my/target/core/controllers/c;->c(Lcom/my/target/core/controllers/c;)Lcom/my/target/core/controllers/c$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    .line 376
    return-void
.end method

.method public final onVolumeChanged(F)V
    .locals 3
    .param p1, "volume"    # F

    .prologue
    const/4 v1, 0x0

    .line 412
    iget v0, p0, Lcom/my/target/core/controllers/c$a;->volume:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    .line 437
    :cond_0
    :goto_0
    return-void

    .line 416
    :cond_1
    iget v0, p0, Lcom/my/target/core/controllers/c$a;->volume:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    cmpg-float v0, p1, v1

    if-gtz v0, :cond_2

    .line 418
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/c;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 419
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v1}, Lcom/my/target/core/controllers/c;->e(Lcom/my/target/core/controllers/c;)Lcom/my/target/aj;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 421
    iget-object v1, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v1}, Lcom/my/target/core/controllers/c;->e(Lcom/my/target/core/controllers/c;)Lcom/my/target/aj;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v1

    const-string v2, "volumeOff"

    invoke-virtual {v1, v2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 422
    iput p1, p0, Lcom/my/target/core/controllers/c$a;->volume:F

    .line 423
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v0, p1}, Lcom/my/target/core/controllers/c;->c(Lcom/my/target/core/controllers/c;F)F

    goto :goto_0

    .line 427
    :cond_2
    iget v0, p0, Lcom/my/target/core/controllers/c$a;->volume:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    cmpl-float v0, p1, v1

    if-lez v0, :cond_0

    .line 429
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/c;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 430
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v1}, Lcom/my/target/core/controllers/c;->e(Lcom/my/target/core/controllers/c;)Lcom/my/target/aj;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 432
    iget-object v1, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v1}, Lcom/my/target/core/controllers/c;->e(Lcom/my/target/core/controllers/c;)Lcom/my/target/aj;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v1

    const-string v2, "volumeOn"

    invoke-virtual {v1, v2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 433
    iput p1, p0, Lcom/my/target/core/controllers/c$a;->volume:F

    .line 434
    iget-object v0, p0, Lcom/my/target/core/controllers/c$a;->p:Lcom/my/target/core/controllers/c;

    invoke-static {v0, p1}, Lcom/my/target/core/controllers/c;->c(Lcom/my/target/core/controllers/c;F)F

    goto :goto_0
.end method
