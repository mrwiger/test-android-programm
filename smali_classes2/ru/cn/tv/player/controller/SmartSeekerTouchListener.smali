.class Lru/cn/tv/player/controller/SmartSeekerTouchListener;
.super Ljava/lang/Object;
.source "SmartSeekerTouchListener.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private handler:Landroid/os/Handler;

.field private isSeeking:Z

.field private mAction:Ljava/lang/Runnable;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    new-instance v0, Lru/cn/tv/player/controller/SmartSeekerTouchListener$1;

    invoke-direct {v0, p0}, Lru/cn/tv/player/controller/SmartSeekerTouchListener$1;-><init>(Lru/cn/tv/player/controller/SmartSeekerTouchListener;)V

    iput-object v0, p0, Lru/cn/tv/player/controller/SmartSeekerTouchListener;->mAction:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$002(Lru/cn/tv/player/controller/SmartSeekerTouchListener;Z)Z
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/player/controller/SmartSeekerTouchListener;
    .param p1, "x1"    # Z

    .prologue
    .line 7
    iput-boolean p1, p0, Lru/cn/tv/player/controller/SmartSeekerTouchListener;->isSeeking:Z

    return p1
.end method

.method static synthetic access$100(Lru/cn/tv/player/controller/SmartSeekerTouchListener;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/controller/SmartSeekerTouchListener;

    .prologue
    .line 7
    iget-object v0, p0, Lru/cn/tv/player/controller/SmartSeekerTouchListener;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/tv/player/controller/SmartSeekerTouchListener;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/controller/SmartSeekerTouchListener;

    .prologue
    .line 7
    iget-object v0, p0, Lru/cn/tv/player/controller/SmartSeekerTouchListener;->mAction:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method protected onClick()V
    .locals 0

    .prologue
    .line 60
    return-void
.end method

.method protected onSeeking()V
    .locals 0

    .prologue
    .line 56
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 18
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 51
    :cond_0
    :goto_0
    :pswitch_0
    return v4

    .line 20
    :pswitch_1
    invoke-virtual {p1, v4}, Landroid/view/View;->setPressed(Z)V

    .line 21
    iget-object v0, p0, Lru/cn/tv/player/controller/SmartSeekerTouchListener;->handler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 22
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lru/cn/tv/player/controller/SmartSeekerTouchListener;->handler:Landroid/os/Handler;

    .line 23
    iget-object v0, p0, Lru/cn/tv/player/controller/SmartSeekerTouchListener;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lru/cn/tv/player/controller/SmartSeekerTouchListener;->mAction:Ljava/lang/Runnable;

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 28
    :pswitch_2
    invoke-virtual {p1, v1}, Landroid/view/View;->setPressed(Z)V

    .line 29
    iget-boolean v0, p0, Lru/cn/tv/player/controller/SmartSeekerTouchListener;->isSeeking:Z

    if-nez v0, :cond_1

    .line 30
    invoke-virtual {p0}, Lru/cn/tv/player/controller/SmartSeekerTouchListener;->onClick()V

    .line 33
    :cond_1
    iput-boolean v1, p0, Lru/cn/tv/player/controller/SmartSeekerTouchListener;->isSeeking:Z

    .line 34
    iget-object v0, p0, Lru/cn/tv/player/controller/SmartSeekerTouchListener;->handler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lru/cn/tv/player/controller/SmartSeekerTouchListener;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lru/cn/tv/player/controller/SmartSeekerTouchListener;->mAction:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 36
    iput-object v2, p0, Lru/cn/tv/player/controller/SmartSeekerTouchListener;->handler:Landroid/os/Handler;

    goto :goto_0

    .line 41
    :pswitch_3
    invoke-virtual {p1, v1}, Landroid/view/View;->setPressed(Z)V

    .line 43
    iput-boolean v1, p0, Lru/cn/tv/player/controller/SmartSeekerTouchListener;->isSeeking:Z

    .line 44
    iget-object v0, p0, Lru/cn/tv/player/controller/SmartSeekerTouchListener;->handler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lru/cn/tv/player/controller/SmartSeekerTouchListener;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lru/cn/tv/player/controller/SmartSeekerTouchListener;->mAction:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 46
    iput-object v2, p0, Lru/cn/tv/player/controller/SmartSeekerTouchListener;->handler:Landroid/os/Handler;

    goto :goto_0

    .line 18
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
