.class public interface abstract Lru/cn/api/medialocator/retrofit/MediaLocatorApi;
.super Ljava/lang/Object;
.source "MediaLocatorApi.java"


# virtual methods
.method public abstract records(Ljava/lang/String;)Lio/reactivex/Single;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Query;
            value = "id"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lru/cn/api/medialocator/replies/SourcesReply;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "sources.json"
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Client-Capabilities: paid_content"
        }
    .end annotation
.end method

.method public abstract timeshift(JI)Lretrofit2/Call;
    .param p1    # J
        .annotation runtime Lretrofit2/http/Query;
            value = "stream_id"
        .end annotation
    .end param
    .param p3    # I
        .annotation runtime Lretrofit2/http/Query;
            value = "offset"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI)",
            "Lretrofit2/Call",
            "<",
            "Lru/cn/api/medialocator/replies/TimeshiftReply;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "timeshift.json"
    .end annotation
.end method

.method public abstract timeshift(JZ)Lretrofit2/Call;
    .param p1    # J
        .annotation runtime Lretrofit2/http/Query;
            value = "stream_id"
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Lretrofit2/http/Query;
            value = "start_over"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZ)",
            "Lretrofit2/Call",
            "<",
            "Lru/cn/api/medialocator/replies/TimeshiftReply;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "timeshift.json"
    .end annotation
.end method
