.class Lcom/samsung/multiscreen/Channel$3;
.super Ljava/lang/Object;
.source "Channel.java"

# interfaces
.implements Lcom/koushikdutta/async/http/AsyncHttpClient$WebSocketConnectCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/multiscreen/Channel;->connect(Landroid/net/Uri;Ljava/util/Map;Lcom/samsung/multiscreen/Result;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/multiscreen/Channel;

.field final synthetic val$id:Ljava/lang/String;

.field final synthetic val$result:Lcom/samsung/multiscreen/Result;


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/Channel;Ljava/lang/String;Lcom/samsung/multiscreen/Result;)V
    .locals 0
    .param p1, "this$0"    # Lcom/samsung/multiscreen/Channel;

    .prologue
    .line 484
    iput-object p1, p0, Lcom/samsung/multiscreen/Channel$3;->this$0:Lcom/samsung/multiscreen/Channel;

    iput-object p2, p0, Lcom/samsung/multiscreen/Channel$3;->val$id:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsung/multiscreen/Channel$3;->val$result:Lcom/samsung/multiscreen/Result;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Ljava/lang/Exception;Lcom/koushikdutta/async/http/WebSocket;)V
    .locals 7
    .param p1, "exception"    # Ljava/lang/Exception;
    .param p2, "socket"    # Lcom/koushikdutta/async/http/WebSocket;

    .prologue
    .line 487
    iget-object v1, p0, Lcom/samsung/multiscreen/Channel$3;->this$0:Lcom/samsung/multiscreen/Channel;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/Channel;->isDebug()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 488
    const-string v1, "Channel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Connect completed socket "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    :cond_0
    if-nez p2, :cond_1

    .line 492
    new-instance v0, Lcom/samsung/multiscreen/ErrorCode;

    const-string v1, "ERROR_CONNECT_FAILED"

    invoke-direct {v0, v1}, Lcom/samsung/multiscreen/ErrorCode;-><init>(Ljava/lang/String;)V

    .line 493
    .local v0, "error":Lcom/samsung/multiscreen/ErrorCode;
    iget-object v1, p0, Lcom/samsung/multiscreen/Channel$3;->this$0:Lcom/samsung/multiscreen/Channel;

    iget-object v2, p0, Lcom/samsung/multiscreen/Channel$3;->val$id:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/samsung/multiscreen/ErrorCode;->value()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v0}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v3

    const-string v6, "Connect failed"

    invoke-static {v4, v5, v3, v6}, Lcom/samsung/multiscreen/Error;->create(JLjava/lang/String;Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/multiscreen/Channel;->handleError(Ljava/lang/String;Lcom/samsung/multiscreen/Error;)V

    .line 546
    .end local v0    # "error":Lcom/samsung/multiscreen/ErrorCode;
    :goto_0
    return-void

    .line 497
    :cond_1
    iget-object v1, p0, Lcom/samsung/multiscreen/Channel$3;->this$0:Lcom/samsung/multiscreen/Channel;

    invoke-static {v1, p2}, Lcom/samsung/multiscreen/Channel;->access$202(Lcom/samsung/multiscreen/Channel;Lcom/koushikdutta/async/http/WebSocket;)Lcom/koushikdutta/async/http/WebSocket;

    .line 500
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/samsung/multiscreen/Channel$3;->val$result:Lcom/samsung/multiscreen/Result;

    if-eqz v1, :cond_2

    .line 501
    iget-object v1, p0, Lcom/samsung/multiscreen/Channel$3;->this$0:Lcom/samsung/multiscreen/Channel;

    iget-object v2, p0, Lcom/samsung/multiscreen/Channel$3;->val$id:Ljava/lang/String;

    invoke-static {p1}, Lcom/samsung/multiscreen/Error;->create(Ljava/lang/Exception;)Lcom/samsung/multiscreen/Error;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/multiscreen/Channel;->handleError(Ljava/lang/String;Lcom/samsung/multiscreen/Error;)V

    goto :goto_0

    .line 506
    :cond_2
    new-instance v1, Lcom/samsung/multiscreen/Channel$3$1;

    invoke-direct {v1, p0}, Lcom/samsung/multiscreen/Channel$3$1;-><init>(Lcom/samsung/multiscreen/Channel$3;)V

    invoke-interface {p2, v1}, Lcom/koushikdutta/async/http/WebSocket;->setClosedCallback(Lcom/koushikdutta/async/callback/CompletedCallback;)V

    .line 514
    new-instance v1, Lcom/samsung/multiscreen/Channel$3$2;

    invoke-direct {v1, p0}, Lcom/samsung/multiscreen/Channel$3$2;-><init>(Lcom/samsung/multiscreen/Channel$3;)V

    invoke-interface {p2, v1}, Lcom/koushikdutta/async/http/WebSocket;->setStringCallback(Lcom/koushikdutta/async/http/WebSocket$StringCallback;)V

    .line 539
    new-instance v1, Lcom/samsung/multiscreen/Channel$3$3;

    invoke-direct {v1, p0}, Lcom/samsung/multiscreen/Channel$3$3;-><init>(Lcom/samsung/multiscreen/Channel$3;)V

    invoke-interface {p2, v1}, Lcom/koushikdutta/async/http/WebSocket;->setDataCallback(Lcom/koushikdutta/async/callback/DataCallback;)V

    goto :goto_0
.end method
