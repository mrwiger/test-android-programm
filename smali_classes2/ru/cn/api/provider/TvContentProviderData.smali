.class public Lru/cn/api/provider/TvContentProviderData;
.super Ljava/lang/Object;
.source "TvContentProviderData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;
    }
.end annotation


# static fields
.field private static LOGTAG:Ljava/lang/String;


# instance fields
.field private age:I

.field private final allowedChannels:Lru/cn/api/allowed/AllowedChannels;

.field private blockedIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private cacheUtils:Lru/cn/api/provider/CacheUtils;

.field private channelsInfoMap:Landroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Lru/cn/api/tv/replies/ChannelInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private channelsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/provider/Channel;",
            ">;"
        }
    .end annotation
.end field

.field private channelsMap:Landroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Lru/cn/api/provider/Channel;",
            ">;"
        }
    .end annotation
.end field

.field private channelsNumberMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lru/cn/api/provider/Channel;",
            ">;"
        }
    .end annotation
.end field

.field private final context:Landroid/content/Context;

.field private favouriteChannelsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/provider/Channel;",
            ">;"
        }
    .end annotation
.end field

.field private favouriteObjects:Landroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Lru/cn/api/userdata/elementclasses/CatalogueItemClass;",
            ">;"
        }
    .end annotation
.end field

.field private favouritesIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private hdChannelsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/provider/Channel;",
            ">;"
        }
    .end annotation
.end field

.field private ignoreServerCache:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private intersectionsChannelsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/provider/Channel;",
            ">;"
        }
    .end annotation
.end field

.field private intersectionsNumberMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lru/cn/api/provider/Channel;",
            ">;"
        }
    .end annotation
.end field

.field private iptvLocations:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lru/cn/api/registry/replies/Service;",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;>;"
        }
    .end annotation
.end field

.field private pinCodeCache:Ljava/lang/String;

.field private pornoChannelsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/provider/Channel;",
            ">;"
        }
    .end annotation
.end field

.field private pornoNumberMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lru/cn/api/provider/Channel;",
            ">;"
        }
    .end annotation
.end field

.field private relatedRubricCache:Landroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;>;"
        }
    .end annotation
.end field

.field private rubricCache:Landroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;"
        }
    .end annotation
.end field

.field private rubricOptionTelecastsMap:Landroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;",
            ">;>;"
        }
    .end annotation
.end field

.field private rubricatorCache:Lru/cn/api/catalogue/replies/Rubricator;

.field private scheduleCache:Landroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Lru/cn/api/provider/Schedule;",
            ">;>;"
        }
    .end annotation
.end field

.field private telecastLocations:Landroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Lru/cn/api/provider/TelecastLocationInfo;",
            ">;"
        }
    .end annotation
.end field

.field private telecastsMap:Landroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Lru/cn/api/tv/replies/Telecast;",
            ">;"
        }
    .end annotation
.end field

.field private timezones:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private titleToIdMapping:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private userPlaylistsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    const-string v0, "TvContentProviderData"

    sput-object v0, Lru/cn/api/provider/TvContentProviderData;->LOGTAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v3, Lru/cn/api/provider/CacheUtils;

    invoke-direct {v3}, Lru/cn/api/provider/CacheUtils;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->cacheUtils:Lru/cn/api/provider/CacheUtils;

    .line 81
    new-instance v3, Ljava/util/LinkedHashSet;

    invoke-direct {v3}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->favouritesIds:Ljava/util/Set;

    .line 82
    new-instance v3, Ljava/util/LinkedHashSet;

    invoke-direct {v3}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->blockedIds:Ljava/util/Set;

    .line 83
    new-instance v3, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v3}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->favouriteObjects:Landroid/support/v4/util/LongSparseArray;

    .line 85
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->titleToIdMapping:Ljava/util/Map;

    .line 86
    new-instance v3, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v3}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->channelsMap:Landroid/support/v4/util/LongSparseArray;

    .line 88
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->channelsList:Ljava/util/List;

    .line 89
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->pornoChannelsList:Ljava/util/List;

    .line 90
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->favouriteChannelsList:Ljava/util/List;

    .line 91
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->hdChannelsList:Ljava/util/List;

    .line 92
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->intersectionsChannelsList:Ljava/util/List;

    .line 93
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->iptvLocations:Ljava/util/Map;

    .line 95
    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->channelsNumberMap:Landroid/util/SparseArray;

    .line 96
    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->intersectionsNumberMap:Landroid/util/SparseArray;

    .line 97
    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->pornoNumberMap:Landroid/util/SparseArray;

    .line 99
    iput-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->rubricatorCache:Lru/cn/api/catalogue/replies/Rubricator;

    .line 100
    new-instance v3, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v3}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->rubricCache:Landroid/support/v4/util/LongSparseArray;

    .line 101
    new-instance v3, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v3}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->relatedRubricCache:Landroid/support/v4/util/LongSparseArray;

    .line 104
    new-instance v3, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v3}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->channelsInfoMap:Landroid/support/v4/util/LongSparseArray;

    .line 105
    new-instance v3, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v3}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->scheduleCache:Landroid/support/v4/util/LongSparseArray;

    .line 108
    new-instance v3, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v3}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->telecastsMap:Landroid/support/v4/util/LongSparseArray;

    .line 109
    new-instance v3, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v3}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->rubricOptionTelecastsMap:Landroid/support/v4/util/LongSparseArray;

    .line 111
    new-instance v3, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v3}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->telecastLocations:Landroid/support/v4/util/LongSparseArray;

    .line 113
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->timezones:Ljava/util/Map;

    .line 116
    iput-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->userPlaylistsMap:Ljava/util/Map;

    .line 117
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->ignoreServerCache:Ljava/util/Set;

    .line 119
    iput-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->pinCodeCache:Ljava/lang/String;

    .line 121
    const/4 v3, -0x1

    iput v3, p0, Lru/cn/api/provider/TvContentProviderData;->age:I

    .line 126
    iput-object p1, p0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    .line 127
    new-instance v3, Lru/cn/api/allowed/AllowedChannels;

    invoke-direct {v3, p1}, Lru/cn/api/allowed/AllowedChannels;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->allowedChannels:Lru/cn/api/allowed/AllowedChannels;

    .line 129
    invoke-static {}, Lru/cn/api/parental/BlockedChannelProviderContract;->blockedChannelsUri()Landroid/net/Uri;

    move-result-object v1

    .line 130
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .local v0, "resolver":Landroid/content/ContentResolver;
    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 132
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 133
    .local v6, "cursor":Landroid/database/Cursor;
    invoke-direct {p0, v6}, Lru/cn/api/provider/TvContentProviderData;->blockedChannels(Landroid/database/Cursor;)V

    .line 135
    invoke-static {}, Lru/cn/api/parental/BlockedChannelProviderContract;->blockedChannelsUri()Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x1

    new-instance v4, Lru/cn/api/provider/TvContentProviderData$1;

    new-instance v5, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v7

    invoke-direct {v5, v7}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v4, p0, v5, v0, v1}, Lru/cn/api/provider/TvContentProviderData$1;-><init>(Lru/cn/api/provider/TvContentProviderData;Landroid/os/Handler;Landroid/content/ContentResolver;Landroid/net/Uri;)V

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 143
    return-void
.end method

.method private _saveChannelInfo(Lru/cn/api/tv/replies/ChannelInfo;J)V
    .locals 4
    .param p1, "info"    # Lru/cn/api/tv/replies/ChannelInfo;
    .param p2, "territoryId"    # J

    .prologue
    .line 365
    iget-object v1, p0, Lru/cn/api/provider/TvContentProviderData;->channelsInfoMap:Landroid/support/v4/util/LongSparseArray;

    iget-wide v2, p1, Lru/cn/api/tv/replies/ChannelInfo;->channelId:J

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/util/LongSparseArray;

    .line 366
    .local v0, "map":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Lru/cn/api/tv/replies/ChannelInfo;>;"
    if-nez v0, :cond_0

    .line 367
    new-instance v0, Landroid/support/v4/util/LongSparseArray;

    .end local v0    # "map":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Lru/cn/api/tv/replies/ChannelInfo;>;"
    invoke-direct {v0}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    .line 368
    .restart local v0    # "map":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Lru/cn/api/tv/replies/ChannelInfo;>;"
    iget-object v1, p0, Lru/cn/api/provider/TvContentProviderData;->channelsInfoMap:Landroid/support/v4/util/LongSparseArray;

    iget-wide v2, p1, Lru/cn/api/tv/replies/ChannelInfo;->channelId:J

    invoke-virtual {v1, v2, v3, v0}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 371
    :cond_0
    invoke-virtual {v0, p2, p3, p1}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 372
    return-void
.end method

.method static synthetic access$000(Lru/cn/api/provider/TvContentProviderData;Landroid/database/Cursor;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/api/provider/TvContentProviderData;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lru/cn/api/provider/TvContentProviderData;->blockedChannels(Landroid/database/Cursor;)V

    return-void
.end method

.method private declared-synchronized blockedChannels(Landroid/database/Cursor;)V
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 727
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->blockedIds:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 728
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 729
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_0

    .line 730
    const-string v2, "cnId"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 731
    .local v0, "channelId":J
    iget-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->blockedIds:Ljava/util/Set;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 732
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 727
    .end local v0    # "channelId":J
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 734
    :cond_0
    :try_start_1
    iget-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->cacheUtils:Lru/cn/api/provider/CacheUtils;

    sget-object v3, Lru/cn/api/provider/CacheUtils$CacheName;->channels:Lru/cn/api/provider/CacheUtils$CacheName;

    invoke-virtual {v2, v3}, Lru/cn/api/provider/CacheUtils;->deleteCacheExpireTime(Lru/cn/api/provider/CacheUtils$CacheName;)V

    .line 735
    iget-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->channels()Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 736
    iget-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Lru/cn/api/provider/TvContentProviderContract;->lastChannelUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 737
    monitor-exit p0

    return-void
.end method

.method private buildFavouriteChannelsList()V
    .locals 6

    .prologue
    .line 915
    iget-object v1, p0, Lru/cn/api/provider/TvContentProviderData;->favouriteChannelsList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 918
    iget-object v1, p0, Lru/cn/api/provider/TvContentProviderData;->channelsList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/Channel;

    .line 919
    .local v0, "channel":Lru/cn/api/provider/Channel;
    iget-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->favouritesIds:Ljava/util/Set;

    iget-wide v4, v0, Lru/cn/api/provider/Channel;->channelId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 920
    iget-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->favouriteChannelsList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 923
    .end local v0    # "channel":Lru/cn/api/provider/Channel;
    :cond_1
    return-void
.end method

.method private cacheKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1, "optionValue"    # Ljava/lang/String;

    .prologue
    .line 1609
    if-nez p1, :cond_0

    .line 1610
    const-string p1, ""

    .line 1612
    .end local p1    # "optionValue":Ljava/lang/String;
    :cond_0
    return-object p1
.end method

.method private channelIdOfLocation(Lru/cn/api/iptv/replies/MediaLocation;)J
    .locals 8
    .param p1, "location"    # Lru/cn/api/iptv/replies/MediaLocation;

    .prologue
    .line 893
    iget-wide v4, p1, Lru/cn/api/iptv/replies/MediaLocation;->channelId:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 894
    iget-wide v4, p1, Lru/cn/api/iptv/replies/MediaLocation;->channelId:J

    .line 911
    :goto_0
    return-wide v4

    .line 896
    :cond_0
    iget-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->titleToIdMapping:Ljava/util/Map;

    iget-object v4, p1, Lru/cn/api/iptv/replies/MediaLocation;->title:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 897
    .local v1, "id":Ljava/lang/Long;
    if-eqz v1, :cond_1

    .line 898
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    goto :goto_0

    .line 901
    :cond_1
    iget-object v2, p1, Lru/cn/api/iptv/replies/MediaLocation;->title:Ljava/lang/String;

    .line 902
    .local v2, "title":Ljava/lang/String;
    if-nez v2, :cond_2

    .line 903
    iget-object v2, p1, Lru/cn/api/iptv/replies/MediaLocation;->location:Ljava/lang/String;

    .line 906
    :cond_2
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 907
    .local v0, "hash":I
    if-lez v0, :cond_3

    .line 908
    neg-int v0, v0

    .line 911
    :cond_3
    int-to-long v4, v0

    goto :goto_0
.end method

.method private channels(Ljava/util/List;JLru/cn/api/tv/retrofit/MediaGuideApi;Ljava/util/Set;)Ljava/util/List;
    .locals 20
    .param p2, "registryTerritoryId"    # J
    .param p4, "tvAPI"    # Lru/cn/api/tv/retrofit/MediaGuideApi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/registry/replies/Service;",
            ">;J",
            "Lru/cn/api/tv/retrofit/MediaGuideApi;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/provider/Channel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 571
    .local p1, "services":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/registry/replies/Service;>;"
    .local p5, "contentTerritories":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-static {v5}, Lru/cn/utils/Utils;->isSupportMulticast(Landroid/content/Context;)Z

    move-result v7

    .line 573
    .local v7, "isSupportMulticast":Z
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 574
    .local v11, "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    new-instance v14, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v14}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    .line 576
    .local v14, "channelsMap":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Lru/cn/api/provider/Channel;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_0
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lru/cn/api/registry/replies/Service;

    .line 577
    .local v6, "iptvService":Lru/cn/api/registry/replies/Service;
    invoke-virtual {v6}, Lru/cn/api/registry/replies/Service;->getContractorId()J

    move-result-wide v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v9}, Lru/cn/api/provider/TvContentProviderData;->getContractor(J)Lru/cn/api/registry/replies/Contractor;

    move-result-object v15

    .line 578
    .local v15, "contractor":Lru/cn/api/registry/replies/Contractor;
    if-eqz v15, :cond_3

    const-string v5, "no_ads"

    invoke-virtual {v15, v5}, Lru/cn/api/registry/replies/Contractor;->hasPartnerTag(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v10, 0x1

    .local v10, "noAdsPartner":Z
    :goto_0
    move-object/from16 v5, p0

    move-wide/from16 v8, p2

    .line 580
    invoke-direct/range {v5 .. v10}, Lru/cn/api/provider/TvContentProviderData;->getLocations(Lru/cn/api/registry/replies/Service;ZJZ)Ljava/util/List;

    move-result-object v18

    .line 582
    .local v18, "locations":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/iptv/replies/MediaLocation;>;"
    if-eqz v18, :cond_0

    .line 585
    invoke-static/range {v18 .. v18}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v5

    sget-object v8, Lru/cn/api/provider/TvContentProviderData$$Lambda$4;->$instance:Lcom/annimon/stream/function/Predicate;

    .line 586
    invoke-virtual {v5, v8}, Lcom/annimon/stream/Stream;->filter(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;

    move-result-object v5

    new-instance v8, Lru/cn/api/provider/TvContentProviderData$$Lambda$5;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Lru/cn/api/provider/TvContentProviderData$$Lambda$5;-><init>(Lru/cn/api/provider/TvContentProviderData;)V

    .line 587
    invoke-virtual {v5, v8}, Lcom/annimon/stream/Stream;->filter(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;

    move-result-object v5

    sget-object v8, Lru/cn/api/provider/TvContentProviderData$$Lambda$6;->$instance:Lcom/annimon/stream/function/Function;

    .line 588
    invoke-virtual {v5, v8}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v5

    .line 589
    invoke-virtual {v5}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object v16

    .line 591
    .local v16, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    move-object/from16 v0, p5

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 593
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lru/cn/api/provider/TvContentProviderData;->updateTitleMapping(Lru/cn/api/tv/retrofit/MediaGuideApi;Ljava/util/List;)V

    .line 595
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lru/cn/api/iptv/replies/MediaLocation;

    .line 596
    .local v17, "location":Lru/cn/api/iptv/replies/MediaLocation;
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lru/cn/api/provider/TvContentProviderData;->channelIdOfLocation(Lru/cn/api/iptv/replies/MediaLocation;)J

    move-result-wide v12

    .line 598
    .local v12, "channelId":J
    invoke-virtual {v14, v12, v13}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/api/provider/Channel;

    .line 599
    .local v4, "channel":Lru/cn/api/provider/Channel;
    if-nez v4, :cond_4

    .line 600
    new-instance v4, Lru/cn/api/provider/Channel;

    .end local v4    # "channel":Lru/cn/api/provider/Channel;
    invoke-direct {v4}, Lru/cn/api/provider/Channel;-><init>()V

    .line 601
    .restart local v4    # "channel":Lru/cn/api/provider/Channel;
    iput-wide v12, v4, Lru/cn/api/provider/Channel;->channelId:J

    .line 602
    move-object/from16 v0, v17

    iget-object v8, v0, Lru/cn/api/iptv/replies/MediaLocation;->title:Ljava/lang/String;

    iput-object v8, v4, Lru/cn/api/provider/Channel;->title:Ljava/lang/String;

    .line 603
    move-object/from16 v0, v17

    iget-object v8, v0, Lru/cn/api/iptv/replies/MediaLocation;->groupTitle:Ljava/lang/String;

    iput-object v8, v4, Lru/cn/api/provider/Channel;->groupTitle:Ljava/lang/String;

    .line 604
    iget-object v8, v4, Lru/cn/api/provider/Channel;->title:Ljava/lang/String;

    if-nez v8, :cond_1

    .line 605
    move-object/from16 v0, v17

    iget-object v8, v0, Lru/cn/api/iptv/replies/MediaLocation;->location:Ljava/lang/String;

    iput-object v8, v4, Lru/cn/api/provider/Channel;->title:Ljava/lang/String;

    .line 608
    :cond_1
    invoke-interface {v11, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 609
    iget-wide v8, v4, Lru/cn/api/provider/Channel;->channelId:J

    invoke-virtual {v14, v8, v9, v4}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 615
    :cond_2
    :goto_2
    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Lru/cn/api/provider/Channel;->addLocation(Lru/cn/api/iptv/replies/MediaLocation;)V

    goto :goto_1

    .line 578
    .end local v4    # "channel":Lru/cn/api/provider/Channel;
    .end local v10    # "noAdsPartner":Z
    .end local v12    # "channelId":J
    .end local v16    # "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v17    # "location":Lru/cn/api/iptv/replies/MediaLocation;
    .end local v18    # "locations":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/iptv/replies/MediaLocation;>;"
    :cond_3
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 611
    .restart local v4    # "channel":Lru/cn/api/provider/Channel;
    .restart local v10    # "noAdsPartner":Z
    .restart local v12    # "channelId":J
    .restart local v16    # "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v17    # "location":Lru/cn/api/iptv/replies/MediaLocation;
    .restart local v18    # "locations":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/iptv/replies/MediaLocation;>;"
    :cond_4
    iget-object v8, v4, Lru/cn/api/provider/Channel;->groupTitle:Ljava/lang/String;

    if-nez v8, :cond_2

    .line 612
    move-object/from16 v0, v17

    iget-object v8, v0, Lru/cn/api/iptv/replies/MediaLocation;->groupTitle:Ljava/lang/String;

    iput-object v8, v4, Lru/cn/api/provider/Channel;->groupTitle:Ljava/lang/String;

    goto :goto_2

    .line 619
    .end local v4    # "channel":Lru/cn/api/provider/Channel;
    .end local v6    # "iptvService":Lru/cn/api/registry/replies/Service;
    .end local v10    # "noAdsPartner":Z
    .end local v12    # "channelId":J
    .end local v15    # "contractor":Lru/cn/api/registry/replies/Contractor;
    .end local v16    # "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v17    # "location":Lru/cn/api/iptv/replies/MediaLocation;
    .end local v18    # "locations":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/iptv/replies/MediaLocation;>;"
    :cond_5
    return-object v11
.end method

.method private combineSparseArrays(Landroid/support/v4/util/LongSparseArray;Landroid/support/v4/util/LongSparseArray;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/support/v4/util/LongSparseArray",
            "<TK;>;",
            "Landroid/support/v4/util/LongSparseArray",
            "<TK;>;)V"
        }
    .end annotation

    .prologue
    .line 1910
    .local p1, "ret":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<TK;>;"
    .local p2, "arg":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<TK;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p2}, Landroid/support/v4/util/LongSparseArray;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 1911
    invoke-virtual {p2, v0}, Landroid/support/v4/util/LongSparseArray;->keyAt(I)J

    move-result-wide v2

    .line 1912
    .local v2, "key":J
    invoke-virtual {p2, v2, v3}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v1

    .line 1913
    .local v1, "info":Ljava/lang/Object;, "TK;"
    invoke-virtual {p1, v2, v3, v1}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 1910
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1915
    .end local v1    # "info":Ljava/lang/Object;, "TK;"
    .end local v2    # "key":J
    :cond_0
    return-void
.end method

.method private defaultComparator(JJJ)Ljava/util/Comparator;
    .locals 5
    .param p1, "contractorId"    # J
    .param p3, "territoryId"    # J
    .param p5, "paidPartnerId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJJ)",
            "Ljava/util/Comparator",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 741
    new-instance v0, Lru/cn/domain/CompositeComparator;

    invoke-direct {v0}, Lru/cn/domain/CompositeComparator;-><init>()V

    .line 742
    .local v0, "composite":Lru/cn/domain/CompositeComparator;, "Lru/cn/domain/CompositeComparator<Lru/cn/api/iptv/replies/MediaLocation;>;"
    invoke-static {}, Lru/cn/domain/TrackPriorities;->purchased()Ljava/util/Comparator;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/cn/domain/CompositeComparator;->add(Ljava/util/Comparator;)V

    .line 743
    invoke-static {p3, p4}, Lru/cn/domain/TrackPriorities;->byTerritory(J)Ljava/util/Comparator;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/cn/domain/CompositeComparator;->add(Ljava/util/Comparator;)V

    .line 745
    iget-object v1, p0, Lru/cn/api/provider/TvContentProviderData;->timezones:Ljava/util/Map;

    invoke-static {v1, p3, p4}, Lru/cn/domain/TrackPriorities;->byTimezones(Ljava/util/Map;J)Ljava/util/Comparator;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/cn/domain/CompositeComparator;->add(Ljava/util/Comparator;)V

    .line 747
    cmp-long v1, p5, v2

    if-lez v1, :cond_0

    .line 748
    invoke-static {p5, p6}, Lru/cn/domain/TrackPriorities;->paidPartner(J)Ljava/util/Comparator;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/cn/domain/CompositeComparator;->add(Ljava/util/Comparator;)V

    .line 751
    :cond_0
    invoke-static {}, Lru/cn/domain/TrackPriorities;->allowed()Ljava/util/Comparator;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/cn/domain/CompositeComparator;->add(Ljava/util/Comparator;)V

    .line 752
    invoke-static {}, Lru/cn/domain/TrackPriorities;->userStreams()Ljava/util/Comparator;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/cn/domain/CompositeComparator;->add(Ljava/util/Comparator;)V

    .line 754
    cmp-long v1, p1, v2

    if-lez v1, :cond_1

    .line 755
    invoke-static {p1, p2}, Lru/cn/domain/TrackPriorities;->byContractor(J)Ljava/util/Comparator;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/cn/domain/CompositeComparator;->add(Ljava/util/Comparator;)V

    .line 758
    :cond_1
    invoke-static {}, Lru/cn/domain/TrackPriorities;->udpTransport()Ljava/util/Comparator;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/cn/domain/CompositeComparator;->add(Ljava/util/Comparator;)V

    .line 759
    invoke-static {}, Lru/cn/domain/TrackPriorities;->p2pTransport()Ljava/util/Comparator;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/cn/domain/CompositeComparator;->add(Ljava/util/Comparator;)V

    .line 761
    return-object v0
.end method

.method private filterChannels(Ljava/util/List;I)Ljava/util/List;
    .locals 2
    .param p2, "age"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/provider/Channel;",
            ">;I)",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/provider/Channel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1989
    .local p1, "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1993
    .end local p1    # "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    :goto_0
    return-object p1

    .restart local p1    # "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    :cond_0
    invoke-static {p1}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v0

    .line 1994
    invoke-virtual {v0}, Lcom/annimon/stream/Stream;->withoutNulls()Lcom/annimon/stream/Stream;

    move-result-object v0

    new-instance v1, Lru/cn/api/provider/TvContentProviderData$$Lambda$16;

    invoke-direct {v1, p0}, Lru/cn/api/provider/TvContentProviderData$$Lambda$16;-><init>(Lru/cn/api/provider/TvContentProviderData;)V

    .line 1995
    invoke-virtual {v0, v1}, Lcom/annimon/stream/Stream;->filterNot(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;

    move-result-object v0

    new-instance v1, Lru/cn/api/provider/TvContentProviderData$$Lambda$17;

    invoke-direct {v1, p0, p2}, Lru/cn/api/provider/TvContentProviderData$$Lambda$17;-><init>(Lru/cn/api/provider/TvContentProviderData;I)V

    .line 1996
    invoke-virtual {v0, v1}, Lcom/annimon/stream/Stream;->filterNot(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;

    move-result-object v0

    .line 2002
    invoke-virtual {v0}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object p1

    goto :goto_0
.end method

.method private getIptvServices(J)Ljava/util/List;
    .locals 15
    .param p1, "currentContractorId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/registry/replies/Service;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 926
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 928
    .local v6, "ret":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/registry/replies/Service;>;"
    iget-object v10, p0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    sget-object v11, Lru/cn/api/registry/replies/Service$Type;->iptv:Lru/cn/api/registry/replies/Service$Type;

    invoke-static {v10, v11}, Lru/cn/api/ServiceLocator;->getRegistryServices(Landroid/content/Context;Lru/cn/api/registry/replies/Service$Type;)Ljava/util/List;

    move-result-object v8

    .line 931
    .local v8, "services":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/registry/replies/Service;>;"
    const/4 v0, 0x0

    .line 932
    .local v0, "contractorPlaylistIndex":I
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lru/cn/api/registry/replies/Service;

    .line 933
    .local v7, "service":Lru/cn/api/registry/replies/Service;
    invoke-virtual {v7}, Lru/cn/api/registry/replies/Service;->getLocation()Ljava/lang/String;

    move-result-object v5

    .line 935
    .local v5, "location":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 936
    const-wide/16 v12, 0x0

    cmp-long v11, p1, v12

    if-lez v11, :cond_1

    invoke-virtual {v7}, Lru/cn/api/registry/replies/Service;->getContractorId()J

    move-result-wide v12

    cmp-long v11, v12, p1

    if-nez v11, :cond_1

    .line 937
    invoke-interface {v6, v0, v7}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 938
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 940
    :cond_1
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 946
    .end local v5    # "location":Ljava/lang/String;
    .end local v7    # "service":Lru/cn/api/registry/replies/Service;
    :cond_2
    iget-object v10, p0, Lru/cn/api/provider/TvContentProviderData;->userPlaylistsMap:Ljava/util/Map;

    if-nez v10, :cond_3

    .line 947
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateUserPlaylists()V

    .line 950
    :cond_3
    iget-object v10, p0, Lru/cn/api/provider/TvContentProviderData;->userPlaylistsMap:Ljava/util/Map;

    invoke-interface {v10}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v9

    .line 951
    .local v9, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 952
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v4, Lru/cn/api/registry/replies/Service;

    invoke-direct {v4}, Lru/cn/api/registry/replies/Service;-><init>()V

    .line 953
    .local v4, "fakeService":Lru/cn/api/registry/replies/Service;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    iput-object v10, v4, Lru/cn/api/registry/replies/Service;->name:Ljava/lang/String;

    .line 954
    sget-object v10, Lru/cn/api/registry/replies/Service$Type;->iptv:Lru/cn/api/registry/replies/Service$Type;

    iput-object v10, v4, Lru/cn/api/registry/replies/Service;->serviceType:Lru/cn/api/registry/replies/Service$Type;

    .line 955
    new-instance v2, Lru/cn/api/registry/replies/APIVersion;

    invoke-direct {v2}, Lru/cn/api/registry/replies/APIVersion;-><init>()V

    .line 956
    .local v2, "fakeApiVersion":Lru/cn/api/registry/replies/APIVersion;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    iput-object v10, v2, Lru/cn/api/registry/replies/APIVersion;->location:Ljava/lang/String;

    .line 957
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 958
    .local v3, "fakeListApiVersion":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/registry/replies/APIVersion;>;"
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 959
    iput-object v3, v4, Lru/cn/api/registry/replies/Service;->versions:Ljava/util/List;

    .line 960
    const/4 v10, 0x0

    invoke-interface {v6, v10, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1

    .line 964
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "fakeApiVersion":Lru/cn/api/registry/replies/APIVersion;
    .end local v3    # "fakeListApiVersion":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/registry/replies/APIVersion;>;"
    .end local v4    # "fakeService":Lru/cn/api/registry/replies/Service;
    :cond_4
    return-object v6
.end method

.method private getLocations(Lru/cn/api/registry/replies/Service;ZJZ)Ljava/util/List;
    .locals 9
    .param p1, "iptvService"    # Lru/cn/api/registry/replies/Service;
    .param p2, "isSupportMulticast"    # Z
    .param p3, "territoryId"    # J
    .param p5, "isNoAdsPartner"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/api/registry/replies/Service;",
            "ZJZ)",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 784
    invoke-direct {p0, p1}, Lru/cn/api/provider/TvContentProviderData;->safeLoadLocations(Lru/cn/api/registry/replies/Service;)Ljava/util/List;

    move-result-object v7

    .line 787
    .local v7, "locations":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/iptv/replies/MediaLocation;>;"
    if-nez v7, :cond_0

    .line 788
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->iptvLocations:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "locations":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/iptv/replies/MediaLocation;>;"
    check-cast v7, Ljava/util/List;

    .line 791
    .restart local v7    # "locations":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/iptv/replies/MediaLocation;>;"
    :cond_0
    if-nez v7, :cond_1

    .line 792
    const/4 v0, 0x0

    .line 797
    :goto_0
    return-object v0

    .line 794
    :cond_1
    invoke-virtual {p1}, Lru/cn/api/registry/replies/Service;->getLocation()Ljava/lang/String;

    move-result-object v1

    .line 795
    .local v1, "sourceUri":Ljava/lang/String;
    invoke-virtual {p1}, Lru/cn/api/registry/replies/Service;->getContractorId()J

    move-result-wide v2

    .line 797
    .local v2, "contractorId":J
    invoke-static {v7}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v0

    new-instance v4, Lru/cn/api/provider/TvContentProviderData$$Lambda$10;

    invoke-direct {v4, p2}, Lru/cn/api/provider/TvContentProviderData$$Lambda$10;-><init>(Z)V

    .line 798
    invoke-virtual {v0, v4}, Lcom/annimon/stream/Stream;->filter(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;

    move-result-object v8

    new-instance v0, Lru/cn/api/provider/TvContentProviderData$$Lambda$11;

    move v4, p5

    move-wide v5, p3

    invoke-direct/range {v0 .. v6}, Lru/cn/api/provider/TvContentProviderData$$Lambda$11;-><init>(Ljava/lang/String;JZJ)V

    .line 809
    invoke-virtual {v8, v0}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v0

    .line 820
    invoke-virtual {v0}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private getSchedule(JJ)Lru/cn/api/provider/Schedule;
    .locals 3
    .param p1, "channelId"    # J
    .param p3, "territoryId"    # J

    .prologue
    .line 1338
    iget-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->scheduleCache:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v2, p1, p2}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/util/LongSparseArray;

    .line 1339
    .local v0, "cacheByTerritory":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Lru/cn/api/provider/Schedule;>;"
    if-nez v0, :cond_0

    .line 1340
    new-instance v0, Landroid/support/v4/util/LongSparseArray;

    .end local v0    # "cacheByTerritory":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Lru/cn/api/provider/Schedule;>;"
    invoke-direct {v0}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    .line 1341
    .restart local v0    # "cacheByTerritory":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Lru/cn/api/provider/Schedule;>;"
    iget-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->scheduleCache:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v2, p1, p2, v0}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 1344
    :cond_0
    invoke-virtual {v0, p3, p4}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/provider/Schedule;

    .line 1345
    .local v1, "schedule":Lru/cn/api/provider/Schedule;
    if-nez v1, :cond_1

    .line 1346
    new-instance v1, Lru/cn/api/provider/Schedule;

    .end local v1    # "schedule":Lru/cn/api/provider/Schedule;
    invoke-direct {v1}, Lru/cn/api/provider/Schedule;-><init>()V

    .line 1347
    .restart local v1    # "schedule":Lru/cn/api/provider/Schedule;
    invoke-virtual {v0, p3, p4, v1}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 1350
    :cond_1
    return-object v1
.end method

.method private getSchedule(JJLjava/util/Calendar;)Lru/cn/api/provider/Schedule;
    .locals 15
    .param p1, "channelId"    # J
    .param p3, "territoryId"    # J
    .param p5, "calendar"    # Ljava/util/Calendar;

    .prologue
    .line 1354
    invoke-direct/range {p0 .. p4}, Lru/cn/api/provider/TvContentProviderData;->getSchedule(JJ)Lru/cn/api/provider/Schedule;

    move-result-object v10

    .line 1355
    .local v10, "schedule":Lru/cn/api/provider/Schedule;
    move-object/from16 v0, p5

    invoke-virtual {v10, v0}, Lru/cn/api/provider/Schedule;->containsDate(Ljava/util/Calendar;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1388
    .end local v10    # "schedule":Lru/cn/api/provider/Schedule;
    :goto_0
    return-object v10

    .line 1359
    .restart local v10    # "schedule":Lru/cn/api/provider/Schedule;
    :cond_0
    :try_start_0
    iget-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-static {v2}, Lru/cn/api/ServiceLocator;->mediaGuide(Landroid/content/Context;)Lru/cn/api/tv/retrofit/MediaGuideApi;

    move-result-object v1

    .line 1362
    .local v1, "tvAPI":Lru/cn/api/tv/retrofit/MediaGuideApi;
    const-string v2, "yyyy-MM-dd"

    move-object/from16 v0, p5

    invoke-static {v0, v2}, Lru/cn/utils/Utils;->format(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 1363
    .local v12, "today":Ljava/lang/String;
    const/4 v2, 0x5

    const/4 v3, -0x1

    move-object/from16 v0, p5

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 1364
    const-string v2, "yyyy-MM-dd"

    move-object/from16 v0, p5

    invoke-static {v0, v2}, Lru/cn/utils/Utils;->format(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 1365
    .local v14, "yesterday":Ljava/lang/String;
    const/4 v2, 0x5

    const/4 v3, 0x2

    move-object/from16 v0, p5

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 1366
    const-string v2, "yyyy-MM-dd"

    move-object/from16 v0, p5

    invoke-static {v0, v2}, Lru/cn/utils/Utils;->format(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1367
    .local v13, "tomorrow":Ljava/lang/String;
    const/4 v2, 0x5

    const/4 v3, -0x1

    move-object/from16 v0, p5

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 1369
    const-string v2, ","

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v14, v3, v5

    const/4 v5, 0x1

    aput-object v12, v3, v5

    const/4 v5, 0x2

    aput-object v13, v3, v5

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    .local v4, "dates":Ljava/lang/String;
    move-wide/from16 v2, p1

    move-wide/from16 v5, p3

    .line 1371
    invoke-interface/range {v1 .. v6}, Lru/cn/api/tv/retrofit/MediaGuideApi;->schedule(JLjava/lang/String;J)Lio/reactivex/Single;

    move-result-object v2

    .line 1372
    invoke-virtual {v2}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/api/tv/replies/TelecastsResponse;

    iget-object v11, v2, Lru/cn/api/tv/replies/TelecastsResponse;->telecasts:Ljava/util/ArrayList;

    .line 1375
    .local v11, "telecasts":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    if-nez v11, :cond_1

    .line 1376
    const/4 v10, 0x0

    goto :goto_0

    .line 1378
    :cond_1
    invoke-virtual {v10, v11}, Lru/cn/api/provider/Schedule;->add(Ljava/util/List;)I

    move-result v9

    .line 1380
    .local v9, "inserted":I
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v2

    sub-int v8, v2, v9

    .line 1381
    .local v8, "ignored":I
    const-string v2, "Schedule"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Schedule add: ignored = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1384
    .end local v1    # "tvAPI":Lru/cn/api/tv/retrofit/MediaGuideApi;
    .end local v4    # "dates":Ljava/lang/String;
    .end local v8    # "ignored":I
    .end local v9    # "inserted":I
    .end local v11    # "telecasts":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    .end local v12    # "today":Ljava/lang/String;
    .end local v13    # "tomorrow":Ljava/lang/String;
    .end local v14    # "yesterday":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 1385
    .local v7, "e":Ljava/lang/Exception;
    invoke-static {v7}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .line 1388
    const/4 v10, 0x0

    goto/16 :goto_0
.end method

.method private groupUpChannels(Ljava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/provider/Channel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 624
    .local p1, "allChannels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 625
    .local v5, "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/provider/Channel;

    .line 626
    .local v1, "channel":Lru/cn/api/provider/Channel;
    iget-wide v10, v1, Lru/cn/api/provider/Channel;->channelId:J

    iget-object v7, v1, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    const/4 v9, 0x0

    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lru/cn/api/iptv/replies/MediaLocation;

    iget-wide v12, v7, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    invoke-virtual {p0, v10, v11, v12, v13}, Lru/cn/api/provider/TvContentProviderData;->getStoredChannelInfo(JJ)Lru/cn/api/tv/replies/ChannelInfo;

    move-result-object v4

    .line 629
    .local v4, "channelInfo":Lru/cn/api/tv/replies/ChannelInfo;
    const/4 v6, 0x0

    .line 630
    .local v6, "type":Lru/cn/api/tv/replies/ChannelInfo$CategoryType;
    if-eqz v4, :cond_3

    iget-object v7, v1, Lru/cn/api/provider/Channel;->groupTitle:Ljava/lang/String;

    if-eqz v7, :cond_3

    .line 633
    iget-object v7, v1, Lru/cn/api/provider/Channel;->groupTitle:Ljava/lang/String;

    const-string v9, "\u041f\u0435\u0440\u0435\u043a\u0440\u0435\u0441\u0442\u043a\u0438"

    invoke-virtual {v7, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 634
    sget-object v6, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->INTERSECTIONS:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    .line 637
    :cond_1
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 638
    iget-object v7, v1, Lru/cn/api/provider/Channel;->groupTitle:Ljava/lang/String;

    const-string v9, "HD"

    invoke-virtual {v7, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 639
    sget-object v6, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->HD:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    .line 643
    :cond_2
    if-eqz v6, :cond_3

    iget-object v7, v4, Lru/cn/api/tv/replies/ChannelInfo;->categories:Ljava/util/List;

    if-nez v7, :cond_3

    .line 644
    new-instance v0, Lru/cn/api/tv/replies/ChannelInfo$Category;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, v4}, Lru/cn/api/tv/replies/ChannelInfo$Category;-><init>(Lru/cn/api/tv/replies/ChannelInfo;)V

    .line 645
    .local v0, "category":Lru/cn/api/tv/replies/ChannelInfo$Category;
    iget-object v7, v1, Lru/cn/api/provider/Channel;->groupTitle:Ljava/lang/String;

    iput-object v7, v0, Lru/cn/api/tv/replies/ChannelInfo$Category;->title:Ljava/lang/String;

    .line 646
    iput-object v6, v0, Lru/cn/api/tv/replies/ChannelInfo$Category;->type:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    .line 647
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, v4, Lru/cn/api/tv/replies/ChannelInfo;->categories:Ljava/util/List;

    .line 648
    iget-object v7, v4, Lru/cn/api/tv/replies/ChannelInfo;->categories:Ljava/util/List;

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 652
    .end local v0    # "category":Lru/cn/api/tv/replies/ChannelInfo$Category;
    :cond_3
    if-eqz v4, :cond_4

    iget-object v7, v4, Lru/cn/api/tv/replies/ChannelInfo;->categories:Ljava/util/List;

    if-nez v7, :cond_5

    .line 653
    :cond_4
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 657
    :cond_5
    iget-object v2, v4, Lru/cn/api/tv/replies/ChannelInfo;->categories:Ljava/util/List;

    .line 660
    .local v2, "channelCategories":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/ChannelInfo$Category;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lru/cn/api/tv/replies/ChannelInfo$Category;

    .line 661
    .local v3, "channelCategory":Lru/cn/api/tv/replies/ChannelInfo$Category;
    sget-object v9, Lru/cn/api/provider/TvContentProviderData$2;->$SwitchMap$ru$cn$api$tv$replies$ChannelInfo$CategoryType:[I

    iget-object v10, v3, Lru/cn/api/tv/replies/ChannelInfo$Category;->type:Lru/cn/api/tv/replies/ChannelInfo$CategoryType;

    invoke-virtual {v10}, Lru/cn/api/tv/replies/ChannelInfo$CategoryType;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    goto :goto_1

    .line 663
    :pswitch_0
    iget-object v7, p0, Lru/cn/api/provider/TvContentProviderData;->pornoChannelsList:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 664
    const/4 v7, 0x1

    iput-boolean v7, v1, Lru/cn/api/provider/Channel;->isPornoChannel:Z

    .line 682
    .end local v3    # "channelCategory":Lru/cn/api/tv/replies/ChannelInfo$Category;
    :cond_6
    :goto_2
    iget-boolean v7, v1, Lru/cn/api/provider/Channel;->isPornoChannel:Z

    if-nez v7, :cond_0

    iget-boolean v7, v1, Lru/cn/api/provider/Channel;->isIntersectionChannel:Z

    if-nez v7, :cond_0

    .line 683
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 668
    .restart local v3    # "channelCategory":Lru/cn/api/tv/replies/ChannelInfo$Category;
    :pswitch_1
    iget-object v7, p0, Lru/cn/api/provider/TvContentProviderData;->hdChannelsList:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 672
    :pswitch_2
    iget-object v7, p0, Lru/cn/api/provider/TvContentProviderData;->intersectionsChannelsList:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 673
    const/4 v7, 0x1

    iput-boolean v7, v1, Lru/cn/api/provider/Channel;->isIntersectionChannel:Z

    goto :goto_2

    .line 687
    .end local v1    # "channel":Lru/cn/api/provider/Channel;
    .end local v2    # "channelCategories":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/ChannelInfo$Category;>;"
    .end local v3    # "channelCategory":Lru/cn/api/tv/replies/ChannelInfo$Category;
    .end local v4    # "channelInfo":Lru/cn/api/tv/replies/ChannelInfo;
    .end local v6    # "type":Lru/cn/api/tv/replies/ChannelInfo$CategoryType;
    :cond_7
    iput-object v5, p0, Lru/cn/api/provider/TvContentProviderData;->channelsList:Ljava/util/List;

    .line 688
    return-void

    .line 661
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static final synthetic lambda$channels$3$TvContentProviderData(Lru/cn/api/iptv/replies/MediaLocation;)Z
    .locals 4
    .param p0, "location"    # Lru/cn/api/iptv/replies/MediaLocation;

    .prologue
    .line 586
    iget-wide v0, p0, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static final synthetic lambda$channels$5$TvContentProviderData(Lru/cn/api/iptv/replies/MediaLocation;)Ljava/lang/Long;
    .locals 2
    .param p0, "location"    # Lru/cn/api/iptv/replies/MediaLocation;

    .prologue
    .line 588
    iget-wide v0, p0, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method static final synthetic lambda$getLocations$10$TvContentProviderData(Ljava/lang/String;JZJLru/cn/api/iptv/replies/MediaLocation;)Lru/cn/api/iptv/replies/MediaLocation;
    .locals 4
    .param p0, "sourceUri"    # Ljava/lang/String;
    .param p1, "contractorId"    # J
    .param p3, "isNoAdsPartner"    # Z
    .param p4, "territoryId"    # J
    .param p6, "location"    # Lru/cn/api/iptv/replies/MediaLocation;

    .prologue
    .line 810
    iput-object p0, p6, Lru/cn/api/iptv/replies/MediaLocation;->sourceUri:Ljava/lang/String;

    .line 811
    iput-wide p1, p6, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    .line 812
    iput-boolean p3, p6, Lru/cn/api/iptv/replies/MediaLocation;->noAdsPartner:Z

    .line 814
    iget-wide v0, p6, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 815
    iput-wide p4, p6, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    .line 818
    :cond_0
    return-object p6
.end method

.method static final synthetic lambda$getLocations$9$TvContentProviderData(ZLru/cn/api/iptv/replies/MediaLocation;)Z
    .locals 3
    .param p0, "isSupportMulticast"    # Z
    .param p1, "location"    # Lru/cn/api/iptv/replies/MediaLocation;

    .prologue
    const/4 v0, 0x0

    .line 799
    iget-object v1, p1, Lru/cn/api/iptv/replies/MediaLocation;->type:Lru/cn/api/iptv/replies/MediaLocation$Type;

    sget-object v2, Lru/cn/api/iptv/replies/MediaLocation$Type;->undefined:Lru/cn/api/iptv/replies/MediaLocation$Type;

    if-ne v1, v2, :cond_1

    .line 807
    :cond_0
    :goto_0
    return v0

    .line 803
    :cond_1
    if-nez p0, :cond_2

    iget-object v1, p1, Lru/cn/api/iptv/replies/MediaLocation;->type:Lru/cn/api/iptv/replies/MediaLocation$Type;

    sget-object v2, Lru/cn/api/iptv/replies/MediaLocation$Type;->mcastudp:Lru/cn/api/iptv/replies/MediaLocation$Type;

    if-eq v1, v2, :cond_0

    .line 807
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static final synthetic lambda$preloadTimezones$6$TvContentProviderData(Lru/cn/api/registry/replies/Territory;)Z
    .locals 1
    .param p0, "territory"    # Lru/cn/api/registry/replies/Territory;

    .prologue
    .line 774
    iget-object v0, p0, Lru/cn/api/registry/replies/Territory;->timezone:Ljava/lang/Long;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static final synthetic lambda$preloadTimezones$7$TvContentProviderData(Lru/cn/api/registry/replies/Territory;)Ljava/lang/Long;
    .locals 2
    .param p0, "territory"    # Lru/cn/api/registry/replies/Territory;

    .prologue
    .line 776
    iget-wide v0, p0, Lru/cn/api/registry/replies/Territory;->territoryId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method static final synthetic lambda$preloadTimezones$8$TvContentProviderData(Lru/cn/api/registry/replies/Territory;)Ljava/lang/Long;
    .locals 1
    .param p0, "territory"    # Lru/cn/api/registry/replies/Territory;

    .prologue
    .line 777
    iget-object v0, p0, Lru/cn/api/registry/replies/Territory;->timezone:Ljava/lang/Long;

    return-object v0
.end method

.method static final synthetic lambda$updateTitleMapping$11$TvContentProviderData(Lru/cn/api/iptv/replies/MediaLocation;)Z
    .locals 4
    .param p0, "location"    # Lru/cn/api/iptv/replies/MediaLocation;

    .prologue
    .line 866
    iget-wide v0, p0, Lru/cn/api/iptv/replies/MediaLocation;->channelId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lru/cn/api/iptv/replies/MediaLocation;->title:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static final synthetic lambda$updateTitleMapping$12$TvContentProviderData(Lru/cn/api/iptv/replies/MediaLocation;)Ljava/lang/String;
    .locals 1
    .param p0, "location"    # Lru/cn/api/iptv/replies/MediaLocation;

    .prologue
    .line 867
    iget-object v0, p0, Lru/cn/api/iptv/replies/MediaLocation;->title:Ljava/lang/String;

    return-object v0
.end method

.method private loadRecords(JLjava/util/List;)Landroid/support/v4/util/LongSparseArray;
    .locals 19
    .param p1, "contractorId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Lru/cn/api/provider/TelecastLocationInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1432
    .local p3, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    new-instance v12, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v12}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    .line 1433
    .local v12, "ret":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Lru/cn/api/provider/TelecastLocationInfo;>;"
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v15

    if-nez v15, :cond_1

    .line 1479
    :cond_0
    :goto_0
    return-object v12

    .line 1438
    :cond_1
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    move-wide/from16 v0, p1

    invoke-static {v15, v0, v1}, Lru/cn/api/ServiceLocator;->mediaLocator(Landroid/content/Context;J)Lru/cn/api/medialocator/retrofit/MediaLocatorApi;

    move-result-object v9

    .line 1439
    .local v9, "locatorApi":Lru/cn/api/medialocator/retrofit/MediaLocatorApi;
    if-eqz v9, :cond_0

    .line 1443
    const-string v15, ","

    move-object/from16 v0, p3

    invoke-static {v15, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v15

    invoke-interface {v9, v15}, Lru/cn/api/medialocator/retrofit/MediaLocatorApi;->records(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v15

    .line 1444
    invoke-virtual {v15}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lru/cn/api/medialocator/replies/SourcesReply;

    .line 1446
    .local v10, "records":Lru/cn/api/medialocator/replies/SourcesReply;
    if-eqz v10, :cond_0

    .line 1449
    invoke-virtual/range {p0 .. p2}, Lru/cn/api/provider/TvContentProviderData;->getContractor(J)Lru/cn/api/registry/replies/Contractor;

    move-result-object v2

    .line 1450
    .local v2, "contractor":Lru/cn/api/registry/replies/Contractor;
    const-string v15, "no_ads"

    invoke-virtual {v2, v15}, Lru/cn/api/registry/replies/Contractor;->hasPartnerTag(Ljava/lang/String;)Z

    move-result v7

    .line 1452
    .local v7, "isNoAdsPartner":Z
    iget-object v15, v10, Lru/cn/api/medialocator/replies/SourcesReply;->replies:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_2
    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_0

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lru/cn/api/medialocator/replies/SourceReply;

    .line 1453
    .local v11, "reply":Lru/cn/api/medialocator/replies/SourceReply;
    iget-wide v4, v11, Lru/cn/api/medialocator/replies/SourceReply;->catalogue_item_id:J

    .line 1455
    .local v4, "id":J
    iget-object v0, v11, Lru/cn/api/medialocator/replies/SourceReply;->rips:Ljava/util/List;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_3
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_2

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lru/cn/api/medialocator/replies/Rip;

    .line 1456
    .local v13, "rip":Lru/cn/api/medialocator/replies/Rip;
    iget-object v0, v13, Lru/cn/api/medialocator/replies/Rip;->streaming_type:Lru/cn/api/medialocator/replies/Rip$StreamingType;

    move-object/from16 v17, v0

    sget-object v18, Lru/cn/api/medialocator/replies/Rip$StreamingType;->HTTP_LIVE_STREAMING:Lru/cn/api/medialocator/replies/Rip$StreamingType;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_3

    iget-object v0, v13, Lru/cn/api/medialocator/replies/Rip;->parts:Ljava/util/List;

    move-object/from16 v17, v0

    .line 1457
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    if-lez v17, :cond_3

    .line 1458
    iget-object v0, v13, Lru/cn/api/medialocator/replies/Rip;->parts:Ljava/util/List;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-interface/range {v17 .. v18}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lru/cn/api/medialocator/replies/RipPart;

    .line 1459
    .local v14, "ripPart":Lru/cn/api/medialocator/replies/RipPart;
    iget-object v0, v14, Lru/cn/api/medialocator/replies/RipPart;->locations:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    if-lez v17, :cond_3

    .line 1460
    iget-object v0, v14, Lru/cn/api/medialocator/replies/RipPart;->locations:Ljava/util/List;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-interface/range {v16 .. v17}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lru/cn/api/medialocator/replies/Location;

    .line 1462
    .local v8, "l":Lru/cn/api/medialocator/replies/Location;
    new-instance v6, Lru/cn/api/provider/TelecastLocationInfo;

    invoke-direct {v6}, Lru/cn/api/provider/TelecastLocationInfo;-><init>()V

    .line 1463
    .local v6, "info":Lru/cn/api/provider/TelecastLocationInfo;
    iget-object v0, v13, Lru/cn/api/medialocator/replies/Rip;->status:Lru/cn/api/medialocator/replies/Rip$RipStatus;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iput-object v0, v6, Lru/cn/api/provider/TelecastLocationInfo;->status:Lru/cn/api/medialocator/replies/Rip$RipStatus;

    .line 1464
    iput-wide v4, v6, Lru/cn/api/provider/TelecastLocationInfo;->id:J

    .line 1465
    iget-object v0, v8, Lru/cn/api/medialocator/replies/Location;->uri:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iput-object v0, v6, Lru/cn/api/provider/TelecastLocationInfo;->location:Ljava/lang/String;

    .line 1466
    move-wide/from16 v0, p1

    iput-wide v0, v6, Lru/cn/api/provider/TelecastLocationInfo;->contractorId:J

    .line 1467
    iget-boolean v0, v13, Lru/cn/api/medialocator/replies/Rip;->adTargeting:Z

    move/from16 v16, v0

    move/from16 v0, v16

    iput-boolean v0, v6, Lru/cn/api/provider/TelecastLocationInfo;->adTargeting:Z

    .line 1468
    iput-boolean v7, v6, Lru/cn/api/provider/TelecastLocationInfo;->noAdsPartner:Z

    .line 1470
    invoke-virtual {v12, v4, v5, v6}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 1476
    .end local v2    # "contractor":Lru/cn/api/registry/replies/Contractor;
    .end local v4    # "id":J
    .end local v6    # "info":Lru/cn/api/provider/TelecastLocationInfo;
    .end local v7    # "isNoAdsPartner":Z
    .end local v8    # "l":Lru/cn/api/medialocator/replies/Location;
    .end local v9    # "locatorApi":Lru/cn/api/medialocator/retrofit/MediaLocatorApi;
    .end local v10    # "records":Lru/cn/api/medialocator/replies/SourcesReply;
    .end local v11    # "reply":Lru/cn/api/medialocator/replies/SourceReply;
    .end local v13    # "rip":Lru/cn/api/medialocator/replies/Rip;
    .end local v14    # "ripPart":Lru/cn/api/medialocator/replies/RipPart;
    :catch_0
    move-exception v3

    .line 1477
    .local v3, "e":Ljava/lang/Exception;
    invoke-static {v3}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method private loadRecords(Ljava/util/List;)Landroid/support/v4/util/LongSparseArray;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Lru/cn/api/provider/TelecastLocationInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1393
    .local p1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    new-instance v3, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v3}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    .line 1394
    .local v3, "ret":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Lru/cn/api/provider/TelecastLocationInfo;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v10

    if-nez v10, :cond_1

    .line 1427
    :cond_0
    return-object v3

    .line 1398
    :cond_1
    iget-object v10, p0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-static {v10}, Lru/cn/api/ServiceLocator;->getWhereAmI(Landroid/content/Context;)Lru/cn/api/registry/replies/WhereAmI;

    move-result-object v9

    .line 1399
    .local v9, "whereAmI":Lru/cn/api/registry/replies/WhereAmI;
    const-wide/16 v4, 0x0

    .line 1400
    .local v4, "registryContractorId":J
    if-eqz v9, :cond_2

    .line 1401
    iget-object v10, v9, Lru/cn/api/registry/replies/WhereAmI;->contractor:Lru/cn/api/registry/replies/Contractor;

    if-eqz v10, :cond_2

    .line 1402
    iget-object v10, v9, Lru/cn/api/registry/replies/WhereAmI;->contractor:Lru/cn/api/registry/replies/Contractor;

    iget-wide v4, v10, Lru/cn/api/registry/replies/Contractor;->contractorId:J

    .line 1406
    :cond_2
    iget-object v10, p0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    sget-object v11, Lru/cn/api/registry/replies/Service$Type;->media_locator:Lru/cn/api/registry/replies/Service$Type;

    invoke-static {v10, v11}, Lru/cn/api/ServiceLocator;->getRegistryServices(Landroid/content/Context;Lru/cn/api/registry/replies/Service$Type;)Ljava/util/List;

    move-result-object v8

    .line 1408
    .local v8, "t":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/registry/replies/Service;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1409
    .local v7, "services":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/registry/replies/Service;>;"
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_3
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lru/cn/api/registry/replies/Service;

    .line 1410
    .local v6, "service":Lru/cn/api/registry/replies/Service;
    invoke-virtual {v6}, Lru/cn/api/registry/replies/Service;->getContractorId()J

    move-result-wide v0

    .line 1411
    .local v0, "contractorId":J
    cmp-long v11, v0, v4

    if-eqz v11, :cond_3

    .line 1412
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1415
    .end local v0    # "contractorId":J
    .end local v6    # "service":Lru/cn/api/registry/replies/Service;
    :cond_4
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_5
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lru/cn/api/registry/replies/Service;

    .line 1416
    .restart local v6    # "service":Lru/cn/api/registry/replies/Service;
    invoke-virtual {v6}, Lru/cn/api/registry/replies/Service;->getContractorId()J

    move-result-wide v0

    .line 1417
    .restart local v0    # "contractorId":J
    cmp-long v11, v0, v4

    if-nez v11, :cond_5

    .line 1418
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1422
    .end local v0    # "contractorId":J
    .end local v6    # "service":Lru/cn/api/registry/replies/Service;
    :cond_6
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lru/cn/api/registry/replies/Service;

    .line 1423
    .restart local v6    # "service":Lru/cn/api/registry/replies/Service;
    invoke-virtual {v6}, Lru/cn/api/registry/replies/Service;->getContractorId()J

    move-result-wide v12

    invoke-direct {p0, v12, v13, p1}, Lru/cn/api/provider/TvContentProviderData;->loadRecords(JLjava/util/List;)Landroid/support/v4/util/LongSparseArray;

    move-result-object v2

    .line 1425
    .local v2, "records":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Lru/cn/api/provider/TelecastLocationInfo;>;"
    invoke-direct {p0, v3, v2}, Lru/cn/api/provider/TvContentProviderData;->combineSparseArrays(Landroid/support/v4/util/LongSparseArray;Landroid/support/v4/util/LongSparseArray;)V

    goto :goto_2
.end method

.method private preloadChannelInfos(Lru/cn/api/tv/retrofit/MediaGuideApi;Ljava/util/List;)V
    .locals 18
    .param p1, "tvAPI"    # Lru/cn/api/tv/retrofit/MediaGuideApi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/api/tv/retrofit/MediaGuideApi;",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/provider/Channel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 691
    .local p2, "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    new-instance v6, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v6}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    .line 692
    .local v6, "ids":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Ljava/util/List<Ljava/lang/Long;>;>;"
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_0
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lru/cn/api/provider/Channel;

    .line 693
    .local v3, "channel":Lru/cn/api/provider/Channel;
    iget-wide v14, v3, Lru/cn/api/provider/Channel;->channelId:J

    const-wide/16 v16, 0x0

    cmp-long v12, v14, v16

    if-lez v12, :cond_0

    .line 694
    iget-object v12, v3, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    const/4 v14, 0x0

    invoke-interface {v12, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lru/cn/api/iptv/replies/MediaLocation;

    iget-wide v10, v12, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    .line 696
    .local v10, "territoryId":J
    invoke-virtual {v6, v10, v11}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    .line 697
    .local v8, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    if-nez v8, :cond_1

    .line 698
    new-instance v8, Ljava/util/ArrayList;

    .end local v8    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 699
    .restart local v8    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-virtual {v6, v10, v11, v8}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 701
    :cond_1
    iget-wide v14, v3, Lru/cn/api/provider/Channel;->channelId:J

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15, v10, v11}, Lru/cn/api/provider/TvContentProviderData;->getStoredChannelInfo(JJ)Lru/cn/api/tv/replies/ChannelInfo;

    move-result-object v7

    .line 703
    .local v7, "info":Lru/cn/api/tv/replies/ChannelInfo;
    if-nez v7, :cond_0

    .line 704
    iget-wide v14, v3, Lru/cn/api/provider/Channel;->channelId:J

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-interface {v8, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 709
    .end local v3    # "channel":Lru/cn/api/provider/Channel;
    .end local v7    # "info":Lru/cn/api/tv/replies/ChannelInfo;
    .end local v8    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v10    # "territoryId":J
    :cond_2
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    invoke-virtual {v6}, Landroid/support/v4/util/LongSparseArray;->size()I

    move-result v12

    if-ge v5, v12, :cond_4

    .line 710
    invoke-virtual {v6, v5}, Landroid/support/v4/util/LongSparseArray;->keyAt(I)J

    move-result-wide v10

    .line 711
    .restart local v10    # "territoryId":J
    invoke-virtual {v6, v5}, Landroid/support/v4/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 713
    .local v4, "channelIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v12

    if-lez v12, :cond_3

    .line 714
    const-string v12, ","

    invoke-static {v12, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "categories,title,logoURL,hasSchedule,ageRestriction"

    .line 715
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    .line 714
    move-object/from16 v0, p1

    invoke-interface {v0, v12, v13, v14}, Lru/cn/api/tv/retrofit/MediaGuideApi;->channelsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Lio/reactivex/Single;

    move-result-object v12

    .line 716
    invoke-virtual {v12}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lru/cn/api/tv/replies/ChannelInfoResponse;

    iget-object v9, v12, Lru/cn/api/tv/replies/ChannelInfoResponse;->list:Ljava/util/List;

    .line 719
    .local v9, "list":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/ChannelInfo;>;"
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/api/tv/replies/ChannelInfo;

    .line 720
    .local v2, "c":Lru/cn/api/tv/replies/ChannelInfo;
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v10, v11}, Lru/cn/api/provider/TvContentProviderData;->_saveChannelInfo(Lru/cn/api/tv/replies/ChannelInfo;J)V

    goto :goto_2

    .line 709
    .end local v2    # "c":Lru/cn/api/tv/replies/ChannelInfo;
    .end local v9    # "list":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/ChannelInfo;>;"
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 724
    .end local v4    # "channelIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v10    # "territoryId":J
    :cond_4
    return-void
.end method

.method private preloadTimezones(Ljava/util/Set;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 765
    .local p1, "contentTerritories":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 780
    :goto_0
    return-void

    .line 768
    :cond_0
    invoke-static {}, Lru/cn/api/ServiceLocator;->registry()Lru/cn/api/registry/retrofit/RegistryApi;

    move-result-object v1

    .line 769
    .local v1, "registry":Lru/cn/api/registry/retrofit/RegistryApi;
    const-string v3, ","

    invoke-static {v3, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Lru/cn/api/registry/retrofit/RegistryApi;->territories(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v3

    .line 770
    invoke-virtual {v3}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lru/cn/api/registry/replies/TerritoriesList;

    iget-object v2, v3, Lru/cn/api/registry/replies/TerritoriesList;->territories:Ljava/util/List;

    .line 773
    .local v2, "territories":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/registry/replies/Territory;>;"
    invoke-static {v2}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v3

    sget-object v4, Lru/cn/api/provider/TvContentProviderData$$Lambda$7;->$instance:Lcom/annimon/stream/function/Predicate;

    .line 774
    invoke-virtual {v3, v4}, Lcom/annimon/stream/Stream;->filter(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;

    move-result-object v3

    sget-object v4, Lru/cn/api/provider/TvContentProviderData$$Lambda$8;->$instance:Lcom/annimon/stream/function/Function;

    sget-object v5, Lru/cn/api/provider/TvContentProviderData$$Lambda$9;->$instance:Lcom/annimon/stream/function/Function;

    .line 775
    invoke-static {v4, v5}, Lcom/annimon/stream/Collectors;->toMap(Lcom/annimon/stream/function/Function;Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Collector;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/annimon/stream/Stream;->collect(Lcom/annimon/stream/Collector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 779
    .local v0, "addition":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Long;>;"
    iget-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->timezones:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto :goto_0
.end method

.method private safeLoadLocations(Lru/cn/api/registry/replies/Service;)Ljava/util/List;
    .locals 6
    .param p1, "iptvService"    # Lru/cn/api/registry/replies/Service;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/api/registry/replies/Service;",
            ")",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 841
    :try_start_0
    iget-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->ignoreServerCache:Ljava/util/Set;

    invoke-virtual {p1}, Lru/cn/api/registry/replies/Service;->getContractorId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    .line 842
    .local v2, "shouldIgnoreServerCache":Z
    invoke-static {p1, v2}, Lru/cn/api/iptv/IptvLoader;->getChannels(Lru/cn/api/registry/replies/Service;Z)Ljava/util/List;

    move-result-object v1

    .line 843
    .local v1, "locations":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/iptv/replies/MediaLocation;>;"
    if-eqz v2, :cond_0

    .line 844
    iget-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->ignoreServerCache:Ljava/util/Set;

    invoke-virtual {p1}, Lru/cn/api/registry/replies/Service;->getContractorId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 848
    :cond_0
    if-eqz v1, :cond_1

    .line 849
    iget-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->iptvLocations:Ljava/util/Map;

    invoke-interface {v3, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 858
    .end local v1    # "locations":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/iptv/replies/MediaLocation;>;"
    .end local v2    # "shouldIgnoreServerCache":Z
    :cond_1
    :goto_0
    return-object v1

    .line 854
    :catch_0
    move-exception v0

    .line 855
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .line 858
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private declared-synchronized updateChannelsIfExpire()V
    .locals 23
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 457
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/api/provider/TvContentProviderData;->cacheUtils:Lru/cn/api/provider/CacheUtils;

    sget-object v9, Lru/cn/api/provider/CacheUtils$CacheName;->channels:Lru/cn/api/provider/CacheUtils$CacheName;

    invoke-virtual {v2, v9}, Lru/cn/api/provider/CacheUtils;->isCacheExpire(Lru/cn/api/provider/CacheUtils$CacheName;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 562
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 460
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-static {v2}, Lru/cn/api/experiments/Experiments;->initialize(Landroid/content/Context;)V

    .line 462
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/api/provider/TvContentProviderData;->channelsMap:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v2}, Landroid/support/v4/util/LongSparseArray;->clear()V

    .line 463
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/api/provider/TvContentProviderData;->channelsList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 464
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/api/provider/TvContentProviderData;->channelsNumberMap:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->clear()V

    .line 465
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/api/provider/TvContentProviderData;->favouriteChannelsList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 466
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/api/provider/TvContentProviderData;->pornoChannelsList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 467
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/api/provider/TvContentProviderData;->hdChannelsList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 468
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/api/provider/TvContentProviderData;->intersectionsChannelsList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 470
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-static {v2}, Lru/cn/api/ServiceLocator;->getWhereAmI(Landroid/content/Context;)Lru/cn/api/registry/replies/WhereAmI;

    move-result-object v22

    .line 471
    .local v22, "whereAmI":Lru/cn/api/registry/replies/WhereAmI;
    const-wide/16 v10, 0x0

    .line 472
    .local v10, "registryContractorId":J
    const-wide/16 v4, 0x0

    .line 473
    .local v4, "registryTerritoryId":J
    const/16 v19, 0x0

    .line 474
    .local v19, "contractorIsPartner":Z
    if-eqz v22, :cond_3

    .line 475
    move-object/from16 v0, v22

    iget-object v2, v0, Lru/cn/api/registry/replies/WhereAmI;->contractor:Lru/cn/api/registry/replies/Contractor;

    if-eqz v2, :cond_2

    .line 476
    move-object/from16 v0, v22

    iget-object v2, v0, Lru/cn/api/registry/replies/WhereAmI;->contractor:Lru/cn/api/registry/replies/Contractor;

    iget-wide v10, v2, Lru/cn/api/registry/replies/Contractor;->contractorId:J

    .line 477
    move-object/from16 v0, v22

    iget-object v2, v0, Lru/cn/api/registry/replies/WhereAmI;->contractor:Lru/cn/api/registry/replies/Contractor;

    const-string v9, "payment"

    invoke-virtual {v2, v9}, Lru/cn/api/registry/replies/Contractor;->hasPartnerTag(Ljava/lang/String;)Z

    move-result v19

    .line 480
    :cond_2
    move-object/from16 v0, v22

    iget-object v2, v0, Lru/cn/api/registry/replies/WhereAmI;->territories:Ljava/util/List;

    if-eqz v2, :cond_3

    move-object/from16 v0, v22

    iget-object v2, v0, Lru/cn/api/registry/replies/WhereAmI;->territories:Ljava/util/List;

    .line 481
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 482
    move-object/from16 v0, v22

    iget-object v2, v0, Lru/cn/api/registry/replies/WhereAmI;->territories:Ljava/util/List;

    const/4 v9, 0x0

    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lru/cn/api/registry/replies/Territory;

    .line 483
    .local v21, "t":Lru/cn/api/registry/replies/Territory;
    if-eqz v21, :cond_3

    .line 484
    move-object/from16 v0, v21

    iget-wide v4, v0, Lru/cn/api/registry/replies/Territory;->territoryId:J

    .line 489
    .end local v21    # "t":Lru/cn/api/registry/replies/Territory;
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-static {v2}, Lru/cn/api/ServiceLocator;->mediaGuide(Landroid/content/Context;)Lru/cn/api/tv/retrofit/MediaGuideApi;

    move-result-object v6

    .line 490
    .local v6, "tvAPI":Lru/cn/api/tv/retrofit/MediaGuideApi;
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 492
    .local v7, "contentTerritories":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lru/cn/api/provider/TvContentProviderData;->getIptvServices(J)Ljava/util/List;

    move-result-object v3

    .local v3, "services":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/registry/replies/Service;>;"
    move-object/from16 v2, p0

    .line 493
    invoke-direct/range {v2 .. v7}, Lru/cn/api/provider/TvContentProviderData;->channels(Ljava/util/List;JLru/cn/api/tv/retrofit/MediaGuideApi;Ljava/util/Set;)Ljava/util/List;

    move-result-object v17

    .line 497
    .local v17, "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_4

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_4

    .line 498
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v9, "There is no channels loaded \u2014 abort"

    invoke-direct {v2, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 457
    .end local v3    # "services":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/registry/replies/Service;>;"
    .end local v4    # "registryTerritoryId":J
    .end local v6    # "tvAPI":Lru/cn/api/tv/retrofit/MediaGuideApi;
    .end local v7    # "contentTerritories":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v10    # "registryContractorId":J
    .end local v17    # "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    .end local v19    # "contractorIsPartner":Z
    .end local v22    # "whereAmI":Lru/cn/api/registry/replies/WhereAmI;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 501
    .restart local v3    # "services":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/registry/replies/Service;>;"
    .restart local v4    # "registryTerritoryId":J
    .restart local v6    # "tvAPI":Lru/cn/api/tv/retrofit/MediaGuideApi;
    .restart local v7    # "contentTerritories":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .restart local v10    # "registryContractorId":J
    .restart local v17    # "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    .restart local v19    # "contractorIsPartner":Z
    .restart local v22    # "whereAmI":Lru/cn/api/registry/replies/WhereAmI;
    :cond_4
    :try_start_2
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v6, v1}, Lru/cn/api/provider/TvContentProviderData;->preloadChannelInfos(Lru/cn/api/tv/retrofit/MediaGuideApi;Ljava/util/List;)V

    .line 504
    const-wide/16 v12, 0x0

    cmp-long v2, v4, v12

    if-lez v2, :cond_5

    .line 505
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v7, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 508
    :cond_5
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lru/cn/api/provider/TvContentProviderData;->preloadTimezones(Ljava/util/Set;)V

    .line 510
    const-wide/16 v14, 0x0

    .line 511
    .local v14, "paidPartnerId":J
    if-eqz v19, :cond_6

    .line 512
    move-wide v14, v10

    :cond_6
    move-object/from16 v9, p0

    move-wide v12, v4

    .line 515
    invoke-direct/range {v9 .. v15}, Lru/cn/api/provider/TvContentProviderData;->defaultComparator(JJJ)Ljava/util/Comparator;

    move-result-object v18

    .line 516
    .local v18, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lru/cn/api/iptv/replies/MediaLocation;>;"
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lru/cn/api/provider/Channel;

    .line 518
    .local v16, "channel":Lru/cn/api/provider/Channel;
    move-object/from16 v0, v16

    iget-object v9, v0, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    move-object/from16 v0, v18

    invoke-static {v9, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 519
    invoke-virtual/range {v16 .. v16}, Lru/cn/api/provider/Channel;->deleteDoubles()V

    .line 522
    move-object/from16 v0, v16

    iget-object v9, v0, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    invoke-static {v9}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v9

    new-instance v12, Lru/cn/api/provider/TvContentProviderData$$Lambda$2;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lru/cn/api/provider/TvContentProviderData$$Lambda$2;-><init>(Lru/cn/api/provider/TvContentProviderData;)V

    .line 523
    invoke-virtual {v9, v12}, Lcom/annimon/stream/Stream;->forEach(Lcom/annimon/stream/function/Consumer;)V

    goto :goto_1

    .line 527
    .end local v16    # "channel":Lru/cn/api/provider/Channel;
    :cond_7
    move-object/from16 v0, p0

    iget v2, v0, Lru/cn/api/provider/TvContentProviderData;->age:I

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v2}, Lru/cn/api/provider/TvContentProviderData;->filterChannels(Ljava/util/List;I)Ljava/util/List;

    move-result-object v17

    .line 528
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lru/cn/api/provider/TvContentProviderData;->groupUpChannels(Ljava/util/List;)V

    .line 530
    invoke-static/range {v17 .. v17}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v2

    new-instance v9, Lru/cn/api/provider/TvContentProviderData$$Lambda$3;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lru/cn/api/provider/TvContentProviderData$$Lambda$3;-><init>(Lru/cn/api/provider/TvContentProviderData;)V

    .line 531
    invoke-virtual {v2, v9}, Lcom/annimon/stream/Stream;->forEach(Lcom/annimon/stream/function/Consumer;)V

    .line 534
    const/16 v20, 0x1

    .line 535
    .local v20, "number":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/api/provider/TvContentProviderData;->channelsList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lru/cn/api/provider/Channel;

    .line 536
    .local v8, "c":Lru/cn/api/provider/Channel;
    move/from16 v0, v20

    iput v0, v8, Lru/cn/api/provider/Channel;->number:I

    .line 537
    move-object/from16 v0, p0

    iget-object v9, v0, Lru/cn/api/provider/TvContentProviderData;->channelsNumberMap:Landroid/util/SparseArray;

    iget v12, v8, Lru/cn/api/provider/Channel;->number:I

    invoke-virtual {v9, v12, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 538
    add-int/lit8 v20, v20, 0x1

    .line 539
    goto :goto_2

    .line 541
    .end local v8    # "c":Lru/cn/api/provider/Channel;
    :cond_8
    const/16 v20, 0x1

    .line 542
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/api/provider/TvContentProviderData;->pornoChannelsList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lru/cn/api/provider/Channel;

    .line 543
    .restart local v8    # "c":Lru/cn/api/provider/Channel;
    move/from16 v0, v20

    iput v0, v8, Lru/cn/api/provider/Channel;->number:I

    .line 544
    move-object/from16 v0, p0

    iget-object v9, v0, Lru/cn/api/provider/TvContentProviderData;->pornoNumberMap:Landroid/util/SparseArray;

    iget v12, v8, Lru/cn/api/provider/Channel;->number:I

    invoke-virtual {v9, v12, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 545
    add-int/lit8 v20, v20, 0x1

    .line 546
    goto :goto_3

    .line 549
    .end local v8    # "c":Lru/cn/api/provider/Channel;
    :cond_9
    const/16 v20, 0x1

    .line 550
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/api/provider/TvContentProviderData;->intersectionsChannelsList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lru/cn/api/provider/Channel;

    .line 551
    .restart local v8    # "c":Lru/cn/api/provider/Channel;
    move/from16 v0, v20

    iput v0, v8, Lru/cn/api/provider/Channel;->number:I

    .line 552
    move-object/from16 v0, p0

    iget-object v9, v0, Lru/cn/api/provider/TvContentProviderData;->intersectionsNumberMap:Landroid/util/SparseArray;

    iget v12, v8, Lru/cn/api/provider/Channel;->number:I

    invoke-virtual {v9, v12, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 553
    add-int/lit8 v20, v20, 0x1

    .line 554
    goto :goto_4

    .line 556
    .end local v8    # "c":Lru/cn/api/provider/Channel;
    :cond_a
    invoke-direct/range {p0 .. p0}, Lru/cn/api/provider/TvContentProviderData;->buildFavouriteChannelsList()V

    .line 559
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/api/provider/TvContentProviderData;->channelsList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 560
    move-object/from16 v0, p0

    iget-object v2, v0, Lru/cn/api/provider/TvContentProviderData;->cacheUtils:Lru/cn/api/provider/CacheUtils;

    sget-object v9, Lru/cn/api/provider/CacheUtils$CacheName;->channels:Lru/cn/api/provider/CacheUtils$CacheName;

    invoke-virtual {v2, v9}, Lru/cn/api/provider/CacheUtils;->updateCacheExpireTime(Lru/cn/api/provider/CacheUtils$CacheName;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method private updateCurrentTelecastIfNeed(JJ)V
    .locals 9
    .param p1, "channelId"    # J
    .param p3, "territoryId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 378
    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-gtz v4, :cond_1

    .line 405
    :cond_0
    :goto_0
    return-void

    .line 381
    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lru/cn/api/provider/TvContentProviderData;->getSchedule(JJ)Lru/cn/api/provider/Schedule;

    move-result-object v2

    .line 382
    .local v2, "schedule":Lru/cn/api/provider/Schedule;
    invoke-virtual {v2}, Lru/cn/api/provider/Schedule;->getCurrentTelecast()Lru/cn/api/tv/replies/Telecast;

    move-result-object v3

    .line 383
    .local v3, "telecast":Lru/cn/api/tv/replies/Telecast;
    if-nez v3, :cond_0

    .line 386
    iget-object v4, p0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-static {v4}, Lru/cn/api/ServiceLocator;->mediaGuide(Landroid/content/Context;)Lru/cn/api/tv/retrofit/MediaGuideApi;

    move-result-object v1

    .line 387
    .local v1, "mediaGuideApi":Lru/cn/api/tv/retrofit/MediaGuideApi;
    if-eqz v1, :cond_0

    .line 388
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    const-string v5, "currentTelecast"

    .line 389
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 388
    invoke-interface {v1, v4, v5, v6}, Lru/cn/api/tv/retrofit/MediaGuideApi;->channelsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Lio/reactivex/Single;

    move-result-object v4

    .line 390
    invoke-virtual {v4}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/api/tv/replies/ChannelInfoResponse;

    iget-object v0, v4, Lru/cn/api/tv/replies/ChannelInfoResponse;->list:Ljava/util/List;

    .line 393
    .local v0, "infos":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/ChannelInfo;>;"
    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_3

    const/4 v4, 0x0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/api/tv/replies/ChannelInfo;

    iget-object v3, v4, Lru/cn/api/tv/replies/ChannelInfo;->currentTelecast:Lru/cn/api/tv/replies/Telecast;

    .line 394
    :goto_1
    if-eqz v3, :cond_0

    .line 395
    iget-object v4, v3, Lru/cn/api/tv/replies/Telecast;->channel:Lru/cn/api/tv/replies/Telecast$Channel;

    if-nez v4, :cond_2

    .line 396
    new-instance v4, Lru/cn/api/tv/replies/Telecast$Channel;

    invoke-direct {v4}, Lru/cn/api/tv/replies/Telecast$Channel;-><init>()V

    iput-object v4, v3, Lru/cn/api/tv/replies/Telecast;->channel:Lru/cn/api/tv/replies/Telecast$Channel;

    .line 397
    iget-object v4, v3, Lru/cn/api/tv/replies/Telecast;->channel:Lru/cn/api/tv/replies/Telecast$Channel;

    iput-wide p1, v4, Lru/cn/api/tv/replies/Telecast$Channel;->channelId:J

    .line 400
    :cond_2
    invoke-virtual {v2, v3}, Lru/cn/api/provider/Schedule;->setCurrentTelecast(Lru/cn/api/tv/replies/Telecast;)V

    .line 401
    iget-object v4, p0, Lru/cn/api/provider/TvContentProviderData;->telecastsMap:Landroid/support/v4/util/LongSparseArray;

    iget-wide v6, v3, Lru/cn/api/tv/replies/Telecast;->id:J

    invoke-virtual {v4, v6, v7, v3}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    goto :goto_0

    .line 393
    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private declared-synchronized updateFavouriteChannelsIfExpire()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 415
    monitor-enter p0

    :try_start_0
    iget-object v7, p0, Lru/cn/api/provider/TvContentProviderData;->cacheUtils:Lru/cn/api/provider/CacheUtils;

    sget-object v8, Lru/cn/api/provider/CacheUtils$CacheName;->user_data:Lru/cn/api/provider/CacheUtils$CacheName;

    invoke-virtual {v7, v8}, Lru/cn/api/provider/CacheUtils;->isCacheExpire(Lru/cn/api/provider/CacheUtils$CacheName;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-nez v7, :cond_1

    .line 454
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 418
    :cond_1
    :try_start_1
    iget-object v7, p0, Lru/cn/api/provider/TvContentProviderData;->favouritesIds:Ljava/util/Set;

    invoke-interface {v7}, Ljava/util/Set;->clear()V

    .line 419
    iget-object v7, p0, Lru/cn/api/provider/TvContentProviderData;->favouriteObjects:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v7}, Landroid/support/v4/util/LongSparseArray;->clear()V

    .line 421
    iget-object v7, p0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-static {v7}, Lru/cn/api/userdata/UserData;->getFavoriteChannels(Landroid/content/Context;)Ljava/util/List;

    move-result-object v4

    .line 424
    .local v4, "favoriteChannels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/userdata/elementclasses/CatalogueItemClass;>;"
    if-eqz v4, :cond_0

    .line 428
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lru/cn/api/userdata/elementclasses/CatalogueItemClass;

    .line 429
    .local v5, "favoriteItem":Lru/cn/api/userdata/elementclasses/CatalogueItemClass;
    iget-object v6, v5, Lru/cn/api/userdata/elementclasses/CatalogueItemClass;->catalogue_item_id:Ljava/lang/String;

    .line 431
    .local v6, "objectId":Ljava/lang/String;
    const-wide/16 v2, 0x0

    .line 432
    .local v2, "channelId":J
    iget-object v8, v5, Lru/cn/api/userdata/elementclasses/CatalogueItemClass;->catalogue_id:Ljava/lang/String;

    const-string v9, "channels"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v8

    if-eqz v8, :cond_4

    .line 434
    :try_start_2
    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v2

    .line 446
    :cond_3
    :goto_2
    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-eqz v8, :cond_2

    .line 447
    :try_start_3
    iget-object v8, p0, Lru/cn/api/provider/TvContentProviderData;->favouritesIds:Ljava/util/Set;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 448
    iget-object v8, p0, Lru/cn/api/provider/TvContentProviderData;->favouriteObjects:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v8, v2, v3, v5}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 415
    .end local v2    # "channelId":J
    .end local v4    # "favoriteChannels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/userdata/elementclasses/CatalogueItemClass;>;"
    .end local v5    # "favoriteItem":Lru/cn/api/userdata/elementclasses/CatalogueItemClass;
    .end local v6    # "objectId":Ljava/lang/String;
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    .line 435
    .restart local v2    # "channelId":J
    .restart local v4    # "favoriteChannels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/userdata/elementclasses/CatalogueItemClass;>;"
    .restart local v5    # "favoriteItem":Lru/cn/api/userdata/elementclasses/CatalogueItemClass;
    .restart local v6    # "objectId":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 436
    .local v1, "e":Ljava/lang/NumberFormatException;
    :try_start_4
    invoke-static {v1}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 438
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    :cond_4
    iget-object v8, v5, Lru/cn/api/userdata/elementclasses/CatalogueItemClass;->catalogue_id:Ljava/lang/String;

    const-string v9, "channels-by-titles"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 439
    iget-object v8, p0, Lru/cn/api/provider/TvContentProviderData;->channelsList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_5
    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/Channel;

    .line 440
    .local v0, "channel":Lru/cn/api/provider/Channel;
    iget-object v9, v0, Lru/cn/api/provider/Channel;->title:Ljava/lang/String;

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 441
    iget-wide v2, v0, Lru/cn/api/provider/Channel;->channelId:J

    goto :goto_3

    .line 452
    .end local v0    # "channel":Lru/cn/api/provider/Channel;
    .end local v2    # "channelId":J
    .end local v5    # "favoriteItem":Lru/cn/api/userdata/elementclasses/CatalogueItemClass;
    .end local v6    # "objectId":Ljava/lang/String;
    :cond_6
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->buildFavouriteChannelsList()V

    .line 453
    iget-object v7, p0, Lru/cn/api/provider/TvContentProviderData;->cacheUtils:Lru/cn/api/provider/CacheUtils;

    sget-object v8, Lru/cn/api/provider/CacheUtils$CacheName;->user_data:Lru/cn/api/provider/CacheUtils$CacheName;

    invoke-virtual {v7, v8}, Lru/cn/api/provider/CacheUtils;->updateCacheExpireTime(Lru/cn/api/provider/CacheUtils$CacheName;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method

.method private updateRubricatorCacheIfNeed()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1858
    iget-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->cacheUtils:Lru/cn/api/provider/CacheUtils;

    sget-object v4, Lru/cn/api/provider/CacheUtils$CacheName;->rubricator:Lru/cn/api/provider/CacheUtils$CacheName;

    invoke-virtual {v3, v4}, Lru/cn/api/provider/CacheUtils;->isCacheExpire(Lru/cn/api/provider/CacheUtils$CacheName;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1859
    iput-object v5, p0, Lru/cn/api/provider/TvContentProviderData;->rubricatorCache:Lru/cn/api/catalogue/replies/Rubricator;

    .line 1862
    :cond_0
    iget-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->rubricatorCache:Lru/cn/api/catalogue/replies/Rubricator;

    if-nez v3, :cond_3

    .line 1863
    iget-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->rubricCache:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v3}, Landroid/support/v4/util/LongSparseArray;->clear()V

    .line 1865
    iget-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-static {v3}, Lru/cn/api/ServiceLocator;->catalogue(Landroid/content/Context;)Lru/cn/api/catalogue/retrofit/CatalogueApi;

    move-result-object v0

    .line 1866
    .local v0, "catalogue":Lru/cn/api/catalogue/retrofit/CatalogueApi;
    invoke-interface {v0, v5}, Lru/cn/api/catalogue/retrofit/CatalogueApi;->rubricator(Ljava/lang/Long;)Lio/reactivex/Single;

    move-result-object v3

    .line 1867
    invoke-virtual {v3}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lru/cn/api/catalogue/replies/Rubricator;

    iput-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->rubricatorCache:Lru/cn/api/catalogue/replies/Rubricator;

    .line 1869
    iget-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->rubricatorCache:Lru/cn/api/catalogue/replies/Rubricator;

    iget-object v3, v3, Lru/cn/api/catalogue/replies/Rubricator;->rubrics:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/catalogue/replies/Rubric;

    .line 1870
    .local v1, "rubric":Lru/cn/api/catalogue/replies/Rubric;
    iget-wide v4, v1, Lru/cn/api/catalogue/replies/Rubric;->id:J

    const-string v6, "android"

    invoke-interface {v0, v4, v5, v6}, Lru/cn/api/catalogue/retrofit/CatalogueApi;->logo(JLjava/lang/String;)Lretrofit2/Call;

    move-result-object v4

    invoke-interface {v4}, Lretrofit2/Call;->request()Lokhttp3/Request;

    move-result-object v4

    invoke-virtual {v4}, Lokhttp3/Request;->url()Lokhttp3/HttpUrl;

    move-result-object v4

    invoke-virtual {v4}, Lokhttp3/HttpUrl;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lru/cn/api/catalogue/replies/Rubric;->logoUrl:Ljava/lang/String;

    .line 1872
    iget-object v4, p0, Lru/cn/api/provider/TvContentProviderData;->rubricCache:Landroid/support/v4/util/LongSparseArray;

    iget-wide v6, v1, Lru/cn/api/catalogue/replies/Rubric;->id:J

    invoke-virtual {v4, v6, v7, v1}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 1873
    iget-object v4, v1, Lru/cn/api/catalogue/replies/Rubric;->subitems:Ljava/util/List;

    if-eqz v4, :cond_1

    iget-object v4, v1, Lru/cn/api/catalogue/replies/Rubric;->subitems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 1874
    iget-object v4, v1, Lru/cn/api/catalogue/replies/Rubric;->subitems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/api/catalogue/replies/Rubric;

    .line 1875
    .local v2, "sub":Lru/cn/api/catalogue/replies/Rubric;
    iget-object v5, v1, Lru/cn/api/catalogue/replies/Rubric;->uiHint:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    iput-object v5, v2, Lru/cn/api/catalogue/replies/Rubric;->uiHint:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    .line 1876
    iget-object v5, p0, Lru/cn/api/provider/TvContentProviderData;->rubricCache:Landroid/support/v4/util/LongSparseArray;

    iget-wide v6, v2, Lru/cn/api/catalogue/replies/Rubric;->id:J

    invoke-virtual {v5, v6, v7, v2}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    goto :goto_0

    .line 1880
    .end local v1    # "rubric":Lru/cn/api/catalogue/replies/Rubric;
    .end local v2    # "sub":Lru/cn/api/catalogue/replies/Rubric;
    :cond_2
    iget-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->cacheUtils:Lru/cn/api/provider/CacheUtils;

    sget-object v4, Lru/cn/api/provider/CacheUtils$CacheName;->rubricator:Lru/cn/api/provider/CacheUtils$CacheName;

    invoke-virtual {v3, v4}, Lru/cn/api/provider/CacheUtils;->updateCacheExpireTime(Lru/cn/api/provider/CacheUtils$CacheName;)V

    .line 1882
    .end local v0    # "catalogue":Lru/cn/api/catalogue/retrofit/CatalogueApi;
    :cond_3
    return-void
.end method

.method private varargs updateTelecastLocations([Lru/cn/api/tv/replies/Telecast;)V
    .locals 14
    .param p1, "telecasts"    # [Lru/cn/api/tv/replies/Telecast;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1308
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1310
    .local v0, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long v2, v8, v10

    .line 1311
    .local v2, "now":J
    array-length v8, p1

    move v7, v6

    :goto_0
    if-ge v7, v8, :cond_2

    aget-object v5, p1, v7

    .line 1312
    .local v5, "telecast":Lru/cn/api/tv/replies/Telecast;
    if-nez v5, :cond_1

    .line 1311
    :cond_0
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 1315
    :cond_1
    iget-object v9, v5, Lru/cn/api/tv/replies/Telecast;->date:Lru/cn/api/tv/replies/DateTime;

    invoke-virtual {v9}, Lru/cn/api/tv/replies/DateTime;->toSeconds()J

    move-result-wide v10

    iget-wide v12, v5, Lru/cn/api/tv/replies/Telecast;->duration:J

    add-long/2addr v10, v12

    cmp-long v9, v2, v10

    if-lez v9, :cond_0

    .line 1316
    iget-wide v10, v5, Lru/cn/api/tv/replies/Telecast;->id:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1320
    .end local v5    # "telecast":Lru/cn/api/tv/replies/Telecast;
    :cond_2
    invoke-direct {p0, v0}, Lru/cn/api/provider/TvContentProviderData;->loadRecords(Ljava/util/List;)Landroid/support/v4/util/LongSparseArray;

    move-result-object v4

    .line 1321
    .local v4, "records":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Lru/cn/api/provider/TelecastLocationInfo;>;"
    if-eqz v4, :cond_5

    .line 1322
    array-length v7, p1

    :goto_2
    if-ge v6, v7, :cond_5

    aget-object v5, p1, v6

    .line 1323
    .restart local v5    # "telecast":Lru/cn/api/tv/replies/Telecast;
    if-nez v5, :cond_4

    .line 1322
    :cond_3
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 1326
    :cond_4
    iget-wide v8, v5, Lru/cn/api/tv/replies/Telecast;->id:J

    invoke-virtual {v4, v8, v9}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/provider/TelecastLocationInfo;

    .line 1327
    .local v1, "info":Lru/cn/api/provider/TelecastLocationInfo;
    if-eqz v1, :cond_3

    .line 1328
    iget-object v8, p0, Lru/cn/api/provider/TvContentProviderData;->telecastLocations:Landroid/support/v4/util/LongSparseArray;

    iget-wide v10, v5, Lru/cn/api/tv/replies/Telecast;->id:J

    invoke-virtual {v8, v10, v11, v1}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    goto :goto_3

    .line 1332
    .end local v1    # "info":Lru/cn/api/provider/TelecastLocationInfo;
    .end local v5    # "telecast":Lru/cn/api/tv/replies/Telecast;
    :cond_5
    return-void
.end method

.method private updateTitleMapping(Lru/cn/api/tv/retrofit/MediaGuideApi;Ljava/util/List;)V
    .locals 5
    .param p1, "tvAPI"    # Lru/cn/api/tv/retrofit/MediaGuideApi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/api/tv/retrofit/MediaGuideApi;",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 862
    .local p2, "locations":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/iptv/replies/MediaLocation;>;"
    if-nez p1, :cond_1

    .line 890
    :cond_0
    :goto_0
    return-void

    .line 865
    :cond_1
    invoke-static {p2}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v3

    sget-object v4, Lru/cn/api/provider/TvContentProviderData$$Lambda$12;->$instance:Lcom/annimon/stream/function/Predicate;

    .line 866
    invoke-virtual {v3, v4}, Lcom/annimon/stream/Stream;->filter(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;

    move-result-object v3

    sget-object v4, Lru/cn/api/provider/TvContentProviderData$$Lambda$13;->$instance:Lcom/annimon/stream/function/Function;

    .line 867
    invoke-virtual {v3, v4}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v3

    new-instance v4, Lru/cn/api/provider/TvContentProviderData$$Lambda$14;

    invoke-direct {v4, p0}, Lru/cn/api/provider/TvContentProviderData$$Lambda$14;-><init>(Lru/cn/api/provider/TvContentProviderData;)V

    .line 868
    invoke-virtual {v3, v4}, Lcom/annimon/stream/Stream;->filter(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;

    move-result-object v3

    .line 869
    invoke-virtual {v3}, Lcom/annimon/stream/Stream;->distinct()Lcom/annimon/stream/Stream;

    move-result-object v3

    .line 870
    invoke-virtual {v3}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object v2

    .line 872
    .local v2, "titles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 876
    :try_start_0
    new-instance v3, Lru/cn/api/tv/TitlesRequest;

    invoke-direct {v3, v2}, Lru/cn/api/tv/TitlesRequest;-><init>(Ljava/util/List;)V

    invoke-interface {p1, v3}, Lru/cn/api/tv/retrofit/MediaGuideApi;->infosByTitles(Lru/cn/api/tv/TitlesRequest;)Lio/reactivex/Single;

    move-result-object v3

    .line 877
    invoke-virtual {v3}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lru/cn/api/tv/replies/ChannelInfoResponse;

    iget-object v1, v3, Lru/cn/api/tv/replies/ChannelInfoResponse;->list:Ljava/util/List;

    .line 880
    .local v1, "infoList":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/ChannelInfo;>;"
    invoke-static {v1}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v3

    new-instance v4, Lru/cn/api/provider/TvContentProviderData$$Lambda$15;

    invoke-direct {v4, p0, v2}, Lru/cn/api/provider/TvContentProviderData$$Lambda$15;-><init>(Lru/cn/api/provider/TvContentProviderData;Ljava/util/List;)V

    .line 881
    invoke-virtual {v3, v4}, Lcom/annimon/stream/Stream;->forEachIndexed(Lcom/annimon/stream/function/IndexedConsumer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 887
    .end local v1    # "infoList":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/ChannelInfo;>;"
    :catch_0
    move-exception v0

    .line 888
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private updateUserPlaylists()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1792
    iget-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->userPlaylistsMap:Ljava/util/Map;

    if-nez v2, :cond_1

    .line 1793
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->userPlaylistsMap:Ljava/util/Map;

    .line 1799
    :goto_0
    iget-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-static {v2}, Lru/cn/api/userdata/UserData;->getUserPlaylists(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 1800
    .local v1, "bookmarks":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/userdata/elementclasses/BookmarkClass;>;"
    if-nez v1, :cond_2

    .line 1807
    :cond_0
    return-void

    .line 1795
    .end local v1    # "bookmarks":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/userdata/elementclasses/BookmarkClass;>;"
    :cond_1
    iget-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->userPlaylistsMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    goto :goto_0

    .line 1804
    .restart local v1    # "bookmarks":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/userdata/elementclasses/BookmarkClass;>;"
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/userdata/elementclasses/BookmarkClass;

    .line 1805
    .local v0, "bookmark":Lru/cn/api/userdata/elementclasses/BookmarkClass;
    iget-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->userPlaylistsMap:Ljava/util/Map;

    iget-object v4, v0, Lru/cn/api/userdata/elementclasses/BookmarkClass;->bookmark_url:Ljava/lang/String;

    iget-object v5, v0, Lru/cn/api/userdata/elementclasses/BookmarkClass;->bookmark_title:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method


# virtual methods
.method public addAllowedChannel(Landroid/content/ContentValues;)Z
    .locals 3
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 163
    const-string v2, "id"

    invoke-virtual {p1, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 164
    .local v0, "id":J
    iget-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->allowedChannels:Lru/cn/api/allowed/AllowedChannels;

    invoke-virtual {v2, v0, v1}, Lru/cn/api/allowed/AllowedChannels;->insert(J)Z

    move-result v2

    return v2
.end method

.method public addFavouriteChannel(J)Z
    .locals 7
    .param p1, "channelId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 1484
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateFavouriteChannelsIfExpire()V

    .line 1485
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateChannelsIfExpire()V

    .line 1487
    iget-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->channelsMap:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v2, p1, p2}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/Channel;

    .line 1489
    .local v0, "channel":Lru/cn/api/provider/Channel;
    new-instance v1, Lru/cn/api/userdata/elementclasses/CatalogueItemClass;

    invoke-direct {v1}, Lru/cn/api/userdata/elementclasses/CatalogueItemClass;-><init>()V

    .line 1490
    .local v1, "item":Lru/cn/api/userdata/elementclasses/CatalogueItemClass;
    cmp-long v2, p1, v4

    if-lez v2, :cond_0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    :goto_0
    iput-object v2, v1, Lru/cn/api/userdata/elementclasses/CatalogueItemClass;->catalogue_item_id:Ljava/lang/String;

    .line 1491
    cmp-long v2, p1, v4

    if-lez v2, :cond_1

    const-string v2, "channels"

    :goto_1
    iput-object v2, v1, Lru/cn/api/userdata/elementclasses/CatalogueItemClass;->catalogue_id:Ljava/lang/String;

    .line 1493
    iget-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-static {v2, v1}, Lru/cn/api/userdata/UserData;->addFavoriteChannel(Landroid/content/Context;Lru/cn/api/userdata/elementclasses/CatalogueItemClass;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1494
    iget-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->favouritesIds:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1495
    iget-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->favouriteObjects:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v2, p1, p2, v1}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 1496
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->buildFavouriteChannelsList()V

    .line 1497
    const/4 v2, 0x1

    .line 1500
    :goto_2
    return v2

    .line 1490
    :cond_0
    iget-object v2, v0, Lru/cn/api/provider/Channel;->title:Ljava/lang/String;

    goto :goto_0

    .line 1491
    :cond_1
    const-string v2, "channels-by-titles"

    goto :goto_1

    .line 1500
    :cond_2
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public addPinCode(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pincode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1521
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-static {v0}, Lru/cn/api/userdata/UserData;->clearPin(Landroid/content/Context;)V

    .line 1522
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-static {v0, p1}, Lru/cn/api/userdata/UserData;->addPinCode(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1523
    iput-object p1, p0, Lru/cn/api/provider/TvContentProviderData;->pinCodeCache:Ljava/lang/String;

    .line 1524
    const/4 v0, 0x1

    .line 1527
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addUserPlaylist(Landroid/content/ContentValues;)Z
    .locals 6
    .param p1, "values"    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1810
    const-string v3, "title"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1811
    .local v2, "title":Ljava/lang/String;
    const-string v3, "location"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1813
    .local v0, "location":Ljava/lang/String;
    iget-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-static {v3, v2, v0}, Lru/cn/api/userdata/UserData;->addUserPlaylist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 1814
    .local v1, "playlistAdded":Z
    if-nez v1, :cond_0

    .line 1815
    const/4 v3, 0x0

    .line 1822
    :goto_0
    return v3

    .line 1817
    :cond_0
    iget-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->userPlaylistsMap:Ljava/util/Map;

    if-nez v3, :cond_1

    .line 1818
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateUserPlaylists()V

    .line 1821
    :cond_1
    iget-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->userPlaylistsMap:Ljava/util/Map;

    const-string v4, "location"

    invoke-virtual {p1, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "title"

    invoke-virtual {p1, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1822
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public allowPurchases(J)Z
    .locals 5
    .param p1, "contractorId"    # J

    .prologue
    .line 824
    const/4 v0, 0x0

    .line 825
    .local v0, "allowPurchases":Z
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-lez v2, :cond_0

    .line 826
    invoke-static {p1, p2}, Lru/cn/api/ServiceLocator;->getContractor(J)Lru/cn/api/registry/replies/Contractor;

    move-result-object v1

    .line 827
    .local v1, "contractor":Lru/cn/api/registry/replies/Contractor;
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 828
    iget-object v2, v1, Lru/cn/api/registry/replies/Contractor;->supportedOfficeIdioms:Ljava/util/List;

    sget-object v3, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;->TV:Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

    .line 829
    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 836
    .end local v1    # "contractor":Lru/cn/api/registry/replies/Contractor;
    :cond_0
    :goto_0
    return v0

    .line 831
    .restart local v1    # "contractor":Lru/cn/api/registry/replies/Contractor;
    :cond_1
    iget-object v2, v1, Lru/cn/api/registry/replies/Contractor;->supportedOfficeIdioms:Ljava/util/List;

    sget-object v3, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;->MOBILE:Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

    .line 832
    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public delAllowedChannel([Ljava/lang/String;)Z
    .locals 3
    .param p1, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 168
    if-nez p1, :cond_0

    .line 169
    const/4 v0, 0x0

    .line 172
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->allowedChannels:Lru/cn/api/allowed/AllowedChannels;

    invoke-static {p1}, Lcom/annimon/stream/Stream;->of([Ljava/lang/Object;)Lcom/annimon/stream/Stream;

    move-result-object v1

    sget-object v2, Lru/cn/api/provider/TvContentProviderData$$Lambda$1;->$instance:Lcom/annimon/stream/function/Function;

    .line 173
    invoke-virtual {v1, v2}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v1

    .line 174
    invoke-virtual {v1}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object v1

    .line 172
    invoke-virtual {v0, v1}, Lru/cn/api/allowed/AllowedChannels;->removeAll(Ljava/util/List;)Z

    move-result v0

    goto :goto_0
.end method

.method public delFavouriteChannel(J)Z
    .locals 3
    .param p1, "channelId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1505
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateFavouriteChannelsIfExpire()V

    .line 1506
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateChannelsIfExpire()V

    .line 1508
    iget-object v1, p0, Lru/cn/api/provider/TvContentProviderData;->favouriteObjects:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v1, p1, p2}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/userdata/elementclasses/CatalogueItemClass;

    .line 1509
    .local v0, "item":Lru/cn/api/userdata/elementclasses/CatalogueItemClass;
    iget-object v1, p0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-static {v1, v0}, Lru/cn/api/userdata/UserData;->delFavoriteChannel(Landroid/content/Context;Lru/cn/api/userdata/elementclasses/CatalogueItemClass;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1510
    iget-object v1, p0, Lru/cn/api/provider/TvContentProviderData;->favouritesIds:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1511
    iget-object v1, p0, Lru/cn/api/provider/TvContentProviderData;->favouriteObjects:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v1, p1, p2}, Landroid/support/v4/util/LongSparseArray;->remove(J)V

    .line 1513
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->buildFavouriteChannelsList()V

    .line 1514
    const/4 v1, 0x1

    .line 1517
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public delUserPlaylist(Ljava/lang/String;)Z
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1826
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-static {v0, p1}, Lru/cn/api/userdata/UserData;->delUserPlaylist(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1828
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->userPlaylistsMap:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 1829
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateUserPlaylists()V

    .line 1832
    :cond_0
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->userPlaylistsMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1833
    const/4 v0, 0x1

    .line 1836
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAllowedChannels()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/provider/Channel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->allowedChannels:Lru/cn/api/allowed/AllowedChannels;

    invoke-virtual {v0}, Lru/cn/api/allowed/AllowedChannels;->ids()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v0

    new-instance v1, Lru/cn/api/provider/TvContentProviderData$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/api/provider/TvContentProviderData$$Lambda$0;-><init>(Lru/cn/api/provider/TvContentProviderData;)V

    .line 151
    invoke-virtual {v0, v1}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v0

    .line 158
    invoke-virtual {v0}, Lcom/annimon/stream/Stream;->withoutNulls()Lcom/annimon/stream/Stream;

    move-result-object v0

    .line 159
    invoke-virtual {v0}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object v0

    .line 150
    return-object v0
.end method

.method public varargs getCachedLocations([J)Landroid/support/v4/util/LongSparseArray;
    .locals 7
    .param p1, "telecastIds"    # [J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([J)",
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Lru/cn/api/provider/TelecastLocationInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1292
    new-instance v1, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v1}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    .line 1293
    .local v1, "locations":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Lru/cn/api/provider/TelecastLocationInfo;>;"
    array-length v5, p1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_1

    aget-wide v2, p1, v4

    .line 1294
    .local v2, "telecastId":J
    iget-object v6, p0, Lru/cn/api/provider/TvContentProviderData;->telecastLocations:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v6, v2, v3}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/TelecastLocationInfo;

    .line 1295
    .local v0, "info":Lru/cn/api/provider/TelecastLocationInfo;
    if-eqz v0, :cond_0

    .line 1296
    invoke-virtual {v1, v2, v3, v0}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 1293
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1300
    .end local v0    # "info":Lru/cn/api/provider/TelecastLocationInfo;
    .end local v2    # "telecastId":J
    :cond_1
    return-object v1
.end method

.method public getChannel(J)Lru/cn/api/provider/Channel;
    .locals 1
    .param p1, "cnId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 226
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateChannelsIfExpire()V

    .line 227
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateFavouriteChannelsIfExpire()V

    .line 229
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->channelsMap:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/Channel;

    return-object v0
.end method

.method public getChannelByNumber(Ljava/lang/Integer;Ljava/lang/String;)Lru/cn/api/provider/Channel;
    .locals 2
    .param p1, "number"    # Ljava/lang/Integer;
    .param p2, "selection"    # Ljava/lang/String;

    .prologue
    .line 1543
    const-string v0, "intersections"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1544
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->intersectionsNumberMap:Landroid/util/SparseArray;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/Channel;

    .line 1551
    :goto_0
    return-object v0

    .line 1547
    :cond_0
    const-string v0, "porno"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1548
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->pornoNumberMap:Landroid/util/SparseArray;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/Channel;

    goto :goto_0

    .line 1551
    :cond_1
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->channelsNumberMap:Landroid/util/SparseArray;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/Channel;

    goto :goto_0
.end method

.method public getChannelDates(J)Ljava/util/List;
    .locals 13
    .param p1, "channelId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/tv/replies/DateTime;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1117
    invoke-virtual {p0, p1, p2}, Lru/cn/api/provider/TvContentProviderData;->getChannel(J)Lru/cn/api/provider/Channel;

    move-result-object v0

    .line 1118
    .local v0, "channel":Lru/cn/api/provider/Channel;
    iget-object v11, v0, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    const/4 v12, 0x0

    invoke-interface {v11, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lru/cn/api/iptv/replies/MediaLocation;

    iget-wide v8, v11, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    .line 1120
    .local v8, "territoryId":J
    invoke-virtual {p0, p1, p2, v8, v9}, Lru/cn/api/provider/TvContentProviderData;->getStoredChannelInfo(JJ)Lru/cn/api/tv/replies/ChannelInfo;

    move-result-object v6

    .line 1121
    .local v6, "info":Lru/cn/api/tv/replies/ChannelInfo;
    if-nez v6, :cond_0

    .line 1122
    invoke-virtual {p0, p1, p2, v8, v9}, Lru/cn/api/provider/TvContentProviderData;->getChannelInfo(JJ)Lru/cn/api/tv/replies/ChannelInfo;

    move-result-object v6

    .line 1125
    :cond_0
    if-eqz v6, :cond_1

    iget v11, v6, Lru/cn/api/tv/replies/ChannelInfo;->hasSchedule:I

    if-nez v11, :cond_3

    .line 1126
    :cond_1
    const/4 v3, 0x0

    .line 1149
    :cond_2
    return-object v3

    .line 1129
    :cond_3
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v11

    div-int/lit16 v10, v11, 0x3e8

    .line 1131
    .local v10, "timezoneOffset":I
    iget-object v11, p0, Lru/cn/api/provider/TvContentProviderData;->intersectionsChannelsList:Ljava/util/List;

    invoke-interface {v11, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    .line 1132
    .local v7, "isCamera":Z
    if-eqz v7, :cond_4

    const/4 v4, 0x1

    .line 1135
    .local v4, "futureDaysCount":I
    :goto_0
    invoke-static {}, Lru/cn/utils/Utils;->getCalendar()Ljava/util/Calendar;

    move-result-object v2

    .line 1136
    .local v2, "dateCalendar":Ljava/util/Calendar;
    const/4 v11, 0x5

    const/4 v12, -0x7

    invoke-virtual {v2, v11, v12}, Ljava/util/Calendar;->add(II)V

    .line 1137
    new-instance v3, Ljava/util/ArrayList;

    add-int/lit8 v11, v4, 0x8

    invoke-direct {v3, v11}, Ljava/util/ArrayList;-><init>(I)V

    .line 1138
    .local v3, "dates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lru/cn/api/tv/replies/DateTime;>;"
    const/4 v5, 0x0

    .local v5, "index":I
    :goto_1
    add-int/lit8 v11, v4, 0x8

    if-ge v5, v11, :cond_2

    .line 1139
    new-instance v1, Lru/cn/api/tv/replies/DateTime;

    invoke-direct {v1}, Lru/cn/api/tv/replies/DateTime;-><init>()V

    .line 1140
    .local v1, "date":Lru/cn/api/tv/replies/DateTime;
    const/4 v11, 0x1

    invoke-virtual {v2, v11}, Ljava/util/Calendar;->get(I)I

    move-result v11

    iput v11, v1, Lru/cn/api/tv/replies/DateTime;->year:I

    .line 1141
    const/4 v11, 0x2

    invoke-virtual {v2, v11}, Ljava/util/Calendar;->get(I)I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    iput v11, v1, Lru/cn/api/tv/replies/DateTime;->month:I

    .line 1142
    const/4 v11, 0x5

    invoke-virtual {v2, v11}, Ljava/util/Calendar;->get(I)I

    move-result v11

    iput v11, v1, Lru/cn/api/tv/replies/DateTime;->day:I

    .line 1143
    iput v10, v1, Lru/cn/api/tv/replies/DateTime;->timezone:I

    .line 1144
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1146
    const/4 v11, 0x5

    const/4 v12, 0x1

    invoke-virtual {v2, v11, v12}, Ljava/util/Calendar;->add(II)V

    .line 1138
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 1132
    .end local v1    # "date":Lru/cn/api/tv/replies/DateTime;
    .end local v2    # "dateCalendar":Ljava/util/Calendar;
    .end local v3    # "dates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lru/cn/api/tv/replies/DateTime;>;"
    .end local v4    # "futureDaysCount":I
    .end local v5    # "index":I
    :cond_4
    const/4 v4, 0x3

    goto :goto_0
.end method

.method public getChannelFromCache(J)Lru/cn/api/provider/Channel;
    .locals 1
    .param p1, "cnId"    # J

    .prologue
    .line 242
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->channelsMap:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/Channel;

    return-object v0
.end method

.method public getChannelInfo(JJ)Lru/cn/api/tv/replies/ChannelInfo;
    .locals 5
    .param p1, "cnId"    # J
    .param p3, "territoryId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 271
    const-wide/16 v2, 0x0

    cmp-long v1, p3, v2

    if-nez v1, :cond_0

    .line 272
    invoke-virtual {p0, p1, p2}, Lru/cn/api/provider/TvContentProviderData;->getChannel(J)Lru/cn/api/provider/Channel;

    move-result-object v0

    .line 273
    .local v0, "channel":Lru/cn/api/provider/Channel;
    iget-object v1, v0, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/iptv/replies/MediaLocation;

    iget-wide p3, v1, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    .line 276
    .end local v0    # "channel":Lru/cn/api/provider/Channel;
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lru/cn/api/provider/TvContentProviderData;->getStoredChannelInfo(JJ)Lru/cn/api/tv/replies/ChannelInfo;

    move-result-object v1

    return-object v1
.end method

.method public getChannelLocations(J)Ljava/util/List;
    .locals 11
    .param p1, "channelId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 246
    invoke-virtual {p0, p1, p2}, Lru/cn/api/provider/TvContentProviderData;->getChannel(J)Lru/cn/api/provider/Channel;

    move-result-object v1

    .line 247
    .local v1, "channel":Lru/cn/api/provider/Channel;
    if-nez v1, :cond_0

    .line 248
    const/4 v4, 0x0

    .line 265
    :goto_0
    return-object v4

    .line 251
    :cond_0
    iget-object v4, v1, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/api/iptv/replies/MediaLocation;

    .line 252
    .local v2, "location":Lru/cn/api/iptv/replies/MediaLocation;
    iget-wide v6, v2, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_1

    .line 253
    iget-object v5, p0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    iget-wide v6, v2, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    invoke-static {v5, v6, v7}, Lru/cn/api/authorization/AccountStorage;->getAccount(Landroid/content/Context;J)Lru/cn/api/authorization/Account;

    move-result-object v0

    .line 254
    .local v0, "account":Lru/cn/api/authorization/Account;
    if-eqz v0, :cond_1

    .line 255
    invoke-virtual {v0}, Lru/cn/api/authorization/Account;->getToken()Lru/cn/api/authorization/Account$Token;

    move-result-object v3

    .line 256
    .local v3, "token":Lru/cn/api/authorization/Account$Token;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lru/cn/api/authorization/Account$Token;->isExpired()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 257
    iget-object v4, p0, Lru/cn/api/provider/TvContentProviderData;->cacheUtils:Lru/cn/api/provider/CacheUtils;

    sget-object v5, Lru/cn/api/provider/CacheUtils$CacheName;->channels:Lru/cn/api/provider/CacheUtils$CacheName;

    invoke-virtual {v4, v5}, Lru/cn/api/provider/CacheUtils;->deleteCacheExpireTime(Lru/cn/api/provider/CacheUtils$CacheName;)V

    .line 258
    invoke-virtual {p0, p1, p2}, Lru/cn/api/provider/TvContentProviderData;->getChannel(J)Lru/cn/api/provider/Channel;

    move-result-object v1

    .line 265
    .end local v0    # "account":Lru/cn/api/authorization/Account;
    .end local v2    # "location":Lru/cn/api/iptv/replies/MediaLocation;
    .end local v3    # "token":Lru/cn/api/authorization/Account$Token;
    :cond_2
    iget-object v4, v1, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    goto :goto_0
.end method

.method public getChannelSchedule(JLjava/util/Calendar;J)Ljava/util/List;
    .locals 30
    .param p1, "cnId"    # J
    .param p3, "calendar"    # Ljava/util/Calendar;
    .param p4, "territoryId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Calendar;",
            "J)",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/tv/replies/Telecast;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1154
    invoke-virtual/range {p0 .. p2}, Lru/cn/api/provider/TvContentProviderData;->getChannel(J)Lru/cn/api/provider/Channel;

    move-result-object v13

    .line 1155
    .local v13, "channel":Lru/cn/api/provider/Channel;
    const-wide/16 v8, 0x0

    cmp-long v7, p4, v8

    if-nez v7, :cond_0

    .line 1156
    iget-object v7, v13, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    const/4 v8, 0x0

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lru/cn/api/iptv/replies/MediaLocation;

    iget-wide v0, v7, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    move-wide/from16 p4, v0

    .line 1159
    :cond_0
    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    move-wide/from16 v3, p4

    invoke-virtual {v0, v1, v2, v3, v4}, Lru/cn/api/provider/TvContentProviderData;->getChannelInfo(JJ)Lru/cn/api/tv/replies/ChannelInfo;

    move-result-object v14

    .line 1160
    .local v14, "channelInfo":Lru/cn/api/tv/replies/ChannelInfo;
    if-eqz v14, :cond_1

    iget v7, v14, Lru/cn/api/tv/replies/ChannelInfo;->hasSchedule:I

    if-nez v7, :cond_3

    .line 1161
    :cond_1
    const/16 v28, 0x0

    .line 1214
    :cond_2
    :goto_0
    return-object v28

    :cond_3
    move-object/from16 v7, p0

    move-wide/from16 v8, p1

    move-wide/from16 v10, p4

    move-object/from16 v12, p3

    .line 1164
    invoke-direct/range {v7 .. v12}, Lru/cn/api/provider/TvContentProviderData;->getSchedule(JJLjava/util/Calendar;)Lru/cn/api/provider/Schedule;

    move-result-object v26

    .line 1165
    .local v26, "schedule":Lru/cn/api/provider/Schedule;
    if-nez v26, :cond_4

    .line 1166
    const/16 v28, 0x0

    goto :goto_0

    .line 1168
    :cond_4
    move-object/from16 v0, v26

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lru/cn/api/provider/Schedule;->getTelecasts(Ljava/util/Calendar;)Ljava/util/List;

    move-result-object v28

    .line 1169
    .local v28, "telecasts":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    if-eqz v28, :cond_5

    invoke-interface/range {v28 .. v28}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1170
    :cond_5
    const/16 v28, 0x0

    goto :goto_0

    .line 1173
    :cond_6
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 1174
    .local v20, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long v24, v8, v10

    .line 1175
    .local v24, "nowSeconds":J
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_9

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lru/cn/api/tv/replies/Telecast;

    .line 1176
    .local v27, "telecast":Lru/cn/api/tv/replies/Telecast;
    move-object/from16 v0, v27

    iget-object v8, v0, Lru/cn/api/tv/replies/Telecast;->date:Lru/cn/api/tv/replies/DateTime;

    invoke-virtual {v8}, Lru/cn/api/tv/replies/DateTime;->toSeconds()J

    move-result-wide v8

    long-to-int v0, v8

    move/from16 v29, v0

    .line 1177
    .local v29, "time":I
    move-object/from16 v0, v27

    iget-wide v0, v0, Lru/cn/api/tv/replies/Telecast;->duration:J

    move-wide/from16 v18, v0

    .line 1180
    .local v18, "duration":J
    move/from16 v0, v29

    int-to-long v8, v0

    add-long v8, v8, v18

    cmp-long v8, v8, v24

    if-gez v8, :cond_7

    .line 1181
    move-object/from16 v0, v27

    iget-wide v8, v0, Lru/cn/api/tv/replies/Telecast;->id:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, v20

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184
    :cond_7
    move-object/from16 v0, v27

    iget-object v8, v0, Lru/cn/api/tv/replies/Telecast;->channel:Lru/cn/api/tv/replies/Telecast$Channel;

    if-nez v8, :cond_8

    .line 1185
    new-instance v8, Lru/cn/api/tv/replies/Telecast$Channel;

    invoke-direct {v8}, Lru/cn/api/tv/replies/Telecast$Channel;-><init>()V

    move-object/from16 v0, v27

    iput-object v8, v0, Lru/cn/api/tv/replies/Telecast;->channel:Lru/cn/api/tv/replies/Telecast$Channel;

    .line 1186
    move-object/from16 v0, v27

    iget-object v8, v0, Lru/cn/api/tv/replies/Telecast;->channel:Lru/cn/api/tv/replies/Telecast$Channel;

    move-wide/from16 v0, p1

    iput-wide v0, v8, Lru/cn/api/tv/replies/Telecast$Channel;->channelId:J

    .line 1189
    :cond_8
    move-object/from16 v0, p0

    iget-object v8, v0, Lru/cn/api/provider/TvContentProviderData;->telecastsMap:Landroid/support/v4/util/LongSparseArray;

    move-object/from16 v0, v27

    iget-wide v10, v0, Lru/cn/api/tv/replies/Telecast;->id:J

    move-object/from16 v0, v27

    invoke-virtual {v8, v10, v11, v0}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    goto :goto_1

    .line 1192
    .end local v18    # "duration":J
    .end local v27    # "telecast":Lru/cn/api/tv/replies/Telecast;
    .end local v29    # "time":I
    :cond_9
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_2

    .line 1193
    new-instance v23, Landroid/support/v4/util/LongSparseArray;

    invoke-direct/range {v23 .. v23}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    .line 1195
    .local v23, "records":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Lru/cn/api/provider/TelecastLocationInfo;>;"
    new-instance v15, Ljava/util/HashSet;

    invoke-direct {v15}, Ljava/util/HashSet;-><init>()V

    .line 1196
    .local v15, "contractorIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    iget-object v7, v13, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_a
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_b

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lru/cn/api/iptv/replies/MediaLocation;

    .line 1197
    .local v22, "location":Lru/cn/api/iptv/replies/MediaLocation;
    move-object/from16 v0, v22

    iget-boolean v8, v0, Lru/cn/api/iptv/replies/MediaLocation;->hasRecords:Z

    if-eqz v8, :cond_a

    .line 1198
    move-object/from16 v0, v22

    iget-wide v8, v0, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v15, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1202
    .end local v22    # "location":Lru/cn/api/iptv/replies/MediaLocation;
    :cond_b
    invoke-interface {v15}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_c

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 1203
    .local v16, "contId":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    move-object/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lru/cn/api/provider/TvContentProviderData;->loadRecords(JLjava/util/List;)Landroid/support/v4/util/LongSparseArray;

    move-result-object v6

    .line 1204
    .local v6, "arg":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Lru/cn/api/provider/TelecastLocationInfo;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v6}, Lru/cn/api/provider/TvContentProviderData;->combineSparseArrays(Landroid/support/v4/util/LongSparseArray;Landroid/support/v4/util/LongSparseArray;)V

    goto :goto_3

    .line 1207
    .end local v6    # "arg":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Lru/cn/api/provider/TelecastLocationInfo;>;"
    .end local v16    # "contId":J
    :cond_c
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_d
    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lru/cn/api/tv/replies/Telecast;

    .line 1208
    .restart local v27    # "telecast":Lru/cn/api/tv/replies/Telecast;
    move-object/from16 v0, v27

    iget-wide v8, v0, Lru/cn/api/tv/replies/Telecast;->id:J

    move-object/from16 v0, v23

    invoke-virtual {v0, v8, v9}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lru/cn/api/provider/TelecastLocationInfo;

    .line 1209
    .local v21, "info":Lru/cn/api/provider/TelecastLocationInfo;
    if-eqz v21, :cond_d

    .line 1210
    move-object/from16 v0, p0

    iget-object v8, v0, Lru/cn/api/provider/TvContentProviderData;->telecastLocations:Landroid/support/v4/util/LongSparseArray;

    move-object/from16 v0, v27

    iget-wide v10, v0, Lru/cn/api/tv/replies/Telecast;->id:J

    move-object/from16 v0, v21

    invoke-virtual {v8, v10, v11, v0}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    goto :goto_4
.end method

.method public getChannelTitle(J)Ljava/lang/String;
    .locals 5
    .param p1, "channelId"    # J

    .prologue
    .line 234
    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, p2, v2, v3}, Lru/cn/api/provider/TvContentProviderData;->getStoredChannelInfo(JJ)Lru/cn/api/tv/replies/ChannelInfo;

    move-result-object v0

    .line 235
    .local v0, "info":Lru/cn/api/tv/replies/ChannelInfo;
    if-nez v0, :cond_0

    .line 236
    const/4 v1, 0x0

    .line 238
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Lru/cn/api/tv/replies/ChannelInfo;->title:Ljava/lang/String;

    goto :goto_0
.end method

.method public getChannels()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/provider/Channel;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 193
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateChannelsIfExpire()V

    .line 194
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateFavouriteChannelsIfExpire()V

    .line 195
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lru/cn/api/provider/TvContentProviderData;->channelsList:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method getChannelsCount()I
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->channelsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getContractor(J)Lru/cn/api/registry/replies/Contractor;
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 1906
    invoke-static {p1, p2}, Lru/cn/api/ServiceLocator;->getContractor(J)Lru/cn/api/registry/replies/Contractor;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentTelecast(JJ)Lru/cn/api/tv/replies/Telecast;
    .locals 7
    .param p1, "channelId"    # J
    .param p3, "territoryId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 343
    const-wide/16 v2, 0x0

    cmp-long v1, p3, v2

    if-nez v1, :cond_0

    .line 344
    invoke-virtual {p0, p1, p2}, Lru/cn/api/provider/TvContentProviderData;->getChannel(J)Lru/cn/api/provider/Channel;

    move-result-object v0

    .line 345
    .local v0, "channel":Lru/cn/api/provider/Channel;
    iget-object v1, v0, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/iptv/replies/MediaLocation;

    iget-wide p3, v1, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    .line 348
    .end local v0    # "channel":Lru/cn/api/provider/Channel;
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lru/cn/api/provider/TvContentProviderData;->updateCurrentTelecastIfNeed(JJ)V

    .line 350
    const/4 v6, 0x1

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v1 .. v6}, Lru/cn/api/provider/TvContentProviderData;->getStoredCurrentTelecast(JJZ)Lru/cn/api/tv/replies/Telecast;

    move-result-object v1

    return-object v1
.end method

.method public getCurrentTelecasts(Ljava/util/List;J)Ljava/util/List;
    .locals 10
    .param p2, "territoryId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;J)",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/tv/replies/Telecast;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 301
    .local p1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 303
    .local v4, "telecasts":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    iget-object v6, p0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-static {v6}, Lru/cn/api/ServiceLocator;->mediaGuide(Landroid/content/Context;)Lru/cn/api/tv/retrofit/MediaGuideApi;

    move-result-object v5

    .line 304
    .local v5, "tvAPI":Lru/cn/api/tv/retrofit/MediaGuideApi;
    if-nez v5, :cond_1

    .line 329
    :cond_0
    return-object v4

    .line 307
    :cond_1
    const-string v6, ","

    invoke-static {v6, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "currentTelecast"

    .line 308
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 307
    invoke-interface {v5, v6, v7, v8}, Lru/cn/api/tv/retrofit/MediaGuideApi;->channelsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Lio/reactivex/Single;

    move-result-object v6

    .line 309
    invoke-virtual {v6}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lru/cn/api/tv/replies/ChannelInfoResponse;

    iget-object v2, v6, Lru/cn/api/tv/replies/ChannelInfoResponse;->list:Ljava/util/List;

    .line 312
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/ChannelInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/tv/replies/ChannelInfo;

    .line 313
    .local v1, "info":Lru/cn/api/tv/replies/ChannelInfo;
    iget-object v0, v1, Lru/cn/api/tv/replies/ChannelInfo;->currentTelecast:Lru/cn/api/tv/replies/Telecast;

    .line 314
    .local v0, "currentTelecast":Lru/cn/api/tv/replies/Telecast;
    if-eqz v0, :cond_2

    .line 315
    iget-object v7, v0, Lru/cn/api/tv/replies/Telecast;->channel:Lru/cn/api/tv/replies/Telecast$Channel;

    if-nez v7, :cond_3

    .line 316
    new-instance v7, Lru/cn/api/tv/replies/Telecast$Channel;

    invoke-direct {v7}, Lru/cn/api/tv/replies/Telecast$Channel;-><init>()V

    iput-object v7, v0, Lru/cn/api/tv/replies/Telecast;->channel:Lru/cn/api/tv/replies/Telecast$Channel;

    .line 317
    iget-object v7, v0, Lru/cn/api/tv/replies/Telecast;->channel:Lru/cn/api/tv/replies/Telecast$Channel;

    iget-wide v8, v1, Lru/cn/api/tv/replies/ChannelInfo;->channelId:J

    iput-wide v8, v7, Lru/cn/api/tv/replies/Telecast$Channel;->channelId:J

    .line 320
    :cond_3
    iget-object v7, p0, Lru/cn/api/provider/TvContentProviderData;->telecastsMap:Landroid/support/v4/util/LongSparseArray;

    iget-wide v8, v0, Lru/cn/api/tv/replies/Telecast;->id:J

    invoke-virtual {v7, v8, v9, v0}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 322
    iget-wide v8, v1, Lru/cn/api/tv/replies/ChannelInfo;->channelId:J

    invoke-direct {p0, v8, v9, p2, p3}, Lru/cn/api/provider/TvContentProviderData;->getSchedule(JJ)Lru/cn/api/provider/Schedule;

    move-result-object v3

    .line 323
    .local v3, "schedule":Lru/cn/api/provider/Schedule;
    invoke-virtual {v3, v0}, Lru/cn/api/provider/Schedule;->setCurrentTelecast(Lru/cn/api/tv/replies/Telecast;)V

    .line 325
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getFavouriteChannels()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/provider/Channel;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 206
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateChannelsIfExpire()V

    .line 207
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateFavouriteChannelsIfExpire()V

    .line 208
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lru/cn/api/provider/TvContentProviderData;->favouriteChannelsList:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getHdChannels()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/provider/Channel;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 199
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateChannelsIfExpire()V

    .line 200
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateFavouriteChannelsIfExpire()V

    .line 202
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lru/cn/api/provider/TvContentProviderData;->hdChannelsList:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getIntersectionsChannels()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/provider/Channel;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 219
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateChannelsIfExpire()V

    .line 220
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateFavouriteChannelsIfExpire()V

    .line 222
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lru/cn/api/provider/TvContentProviderData;->intersectionsChannelsList:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getMoreRubricItems(JLjava/lang/String;)Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;
    .locals 1
    .param p1, "rubricId"    # J
    .param p3, "optionValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1616
    const/16 v0, 0x23

    invoke-virtual {p0, p1, p2, p3, v0}, Lru/cn/api/provider/TvContentProviderData;->getMoreRubricItems(JLjava/lang/String;I)Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;

    move-result-object v0

    return-object v0
.end method

.method public getMoreRubricItems(JLjava/lang/String;I)Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;
    .locals 7
    .param p1, "rubricId"    # J
    .param p3, "optionValue"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1620
    const/4 v5, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move v6, p4

    invoke-virtual/range {v1 .. v6}, Lru/cn/api/provider/TvContentProviderData;->getMoreRubricTelecasts(JLjava/lang/String;ZI)Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;

    move-result-object v0

    return-object v0
.end method

.method public getMoreRubricTelecasts(JLjava/lang/String;ZI)Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;
    .locals 35
    .param p1, "rubricId"    # J
    .param p3, "optionValue"    # Ljava/lang/String;
    .param p4, "needClearCache"    # Z
    .param p5, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1625
    move-object/from16 v0, p0

    iget-object v6, v0, Lru/cn/api/provider/TvContentProviderData;->rubricOptionTelecastsMap:Landroid/support/v4/util/LongSparseArray;

    move-wide/from16 v0, p1

    invoke-virtual {v6, v0, v1}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/util/HashMap;

    .line 1627
    .local v28, "telecastsOptionMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;>;"
    if-nez v28, :cond_0

    .line 1628
    new-instance v28, Ljava/util/HashMap;

    .end local v28    # "telecastsOptionMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;>;"
    invoke-direct/range {v28 .. v28}, Ljava/util/HashMap;-><init>()V

    .line 1631
    .restart local v28    # "telecastsOptionMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;>;"
    :cond_0
    const/16 v27, 0x0

    .line 1633
    .local v27, "telecasts":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lru/cn/api/provider/TvContentProviderData;->cacheKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 1634
    .local v22, "optionsCacheKey":Ljava/lang/String;
    move-object/from16 v0, v28

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;

    .line 1635
    .local v4, "cacheObj":Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;
    if-eqz v4, :cond_5

    .line 1636
    move-object/from16 v0, v28

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;

    iget-object v0, v6, Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;->telecasts:Ljava/util/List;

    move-object/from16 v27, v0

    .line 1641
    :goto_0
    const/4 v9, 0x0

    .line 1642
    .local v9, "skip":I
    if-eqz v27, :cond_1

    if-eqz p4, :cond_6

    .line 1643
    :cond_1
    new-instance v27, Ljava/util/ArrayList;

    .end local v27    # "telecasts":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    .line 1644
    .restart local v27    # "telecasts":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    move-object/from16 v0, v27

    iput-object v0, v4, Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;->telecasts:Ljava/util/List;

    .line 1645
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v32, 0x3e8

    div-long v6, v6, v32

    iput-wide v6, v4, Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;->cacheExpireTime:J

    .line 1646
    move-object/from16 v0, v28

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1647
    move-object/from16 v0, p0

    iget-object v6, v0, Lru/cn/api/provider/TvContentProviderData;->rubricOptionTelecastsMap:Landroid/support/v4/util/LongSparseArray;

    move-wide/from16 v0, p1

    move-object/from16 v2, v28

    invoke-virtual {v6, v0, v1, v2}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 1653
    :goto_1
    :try_start_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-static {v6}, Lru/cn/api/ServiceLocator;->catalogue(Landroid/content/Context;)Lru/cn/api/catalogue/retrofit/CatalogueApi;

    move-result-object v5

    .local v5, "catalogue":Lru/cn/api/catalogue/retrofit/CatalogueApi;
    move-wide/from16 v6, p1

    move/from16 v8, p5

    move-object/from16 v10, p3

    .line 1654
    invoke-interface/range {v5 .. v10}, Lru/cn/api/catalogue/retrofit/CatalogueApi;->items(JIILjava/lang/String;)Lio/reactivex/Single;

    move-result-object v6

    .line 1655
    invoke-virtual {v6}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lru/cn/api/catalogue/replies/SelectionPage;

    .line 1657
    .local v24, "selectionPage":Lru/cn/api/catalogue/replies/SelectionPage;
    move-object/from16 v0, v24

    iget v6, v0, Lru/cn/api/catalogue/replies/SelectionPage;->itemsSkipped:I

    add-int v6, v6, p5

    move-object/from16 v0, v24

    iget v7, v0, Lru/cn/api/catalogue/replies/SelectionPage;->totalCount:I

    if-lt v6, v7, :cond_2

    .line 1658
    const/4 v6, 0x1

    iput-boolean v6, v4, Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;->noMoreData:Z

    .line 1661
    :cond_2
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    .line 1662
    .local v26, "telecastIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 1663
    .local v13, "channelIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    new-instance v31, Ljava/util/HashMap;

    invoke-direct/range {v31 .. v31}, Ljava/util/HashMap;-><init>()V

    .line 1665
    .local v31, "viewsCountMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Integer;>;"
    move-object/from16 v0, v24

    iget-object v6, v0, Lru/cn/api/catalogue/replies/SelectionPage;->items:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lru/cn/api/catalogue/replies/CatalogueItem;

    .line 1666
    .local v19, "item":Lru/cn/api/catalogue/replies/CatalogueItem;
    move-object/from16 v0, v19

    iget-object v7, v0, Lru/cn/api/catalogue/replies/CatalogueItem;->guideType:Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;

    sget-object v8, Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;->FRAGMENT:Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;

    if-ne v7, v8, :cond_7

    .line 1667
    move-object/from16 v0, v19

    iget-wide v0, v0, Lru/cn/api/catalogue/replies/CatalogueItem;->id:J

    move-wide/from16 v32, v0

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object/from16 v0, v26

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1668
    move-object/from16 v0, v19

    iget-object v7, v0, Lru/cn/api/catalogue/replies/CatalogueItem;->selectionAttributes:Lru/cn/api/catalogue/replies/CatalogueItemSelectionAttributes;

    if-eqz v7, :cond_3

    .line 1669
    move-object/from16 v0, v19

    iget-wide v0, v0, Lru/cn/api/catalogue/replies/CatalogueItem;->id:J

    move-wide/from16 v32, v0

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object/from16 v0, v19

    iget-object v8, v0, Lru/cn/api/catalogue/replies/CatalogueItem;->selectionAttributes:Lru/cn/api/catalogue/replies/CatalogueItemSelectionAttributes;

    iget v8, v8, Lru/cn/api/catalogue/replies/CatalogueItemSelectionAttributes;->viewsCount:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object/from16 v0, v31

    invoke-interface {v0, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 1753
    .end local v5    # "catalogue":Lru/cn/api/catalogue/retrofit/CatalogueApi;
    .end local v13    # "channelIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v19    # "item":Lru/cn/api/catalogue/replies/CatalogueItem;
    .end local v24    # "selectionPage":Lru/cn/api/catalogue/replies/SelectionPage;
    .end local v26    # "telecastIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v31    # "viewsCountMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Integer;>;"
    :catch_0
    move-exception v17

    .line 1754
    .local v17, "e":Ljava/lang/Exception;
    invoke-static/range {v17 .. v17}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .line 1756
    .end local v17    # "e":Ljava/lang/Exception;
    :cond_4
    :goto_3
    return-object v4

    .line 1638
    .end local v9    # "skip":I
    :cond_5
    new-instance v4, Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;

    .end local v4    # "cacheObj":Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;
    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;-><init>(Lru/cn/api/provider/TvContentProviderData;)V

    .restart local v4    # "cacheObj":Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;
    goto/16 :goto_0

    .line 1649
    .restart local v9    # "skip":I
    :cond_6
    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v9

    goto/16 :goto_1

    .line 1671
    .restart local v5    # "catalogue":Lru/cn/api/catalogue/retrofit/CatalogueApi;
    .restart local v13    # "channelIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v19    # "item":Lru/cn/api/catalogue/replies/CatalogueItem;
    .restart local v24    # "selectionPage":Lru/cn/api/catalogue/replies/SelectionPage;
    .restart local v26    # "telecastIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v31    # "viewsCountMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Integer;>;"
    :cond_7
    :try_start_1
    move-object/from16 v0, v19

    iget-object v7, v0, Lru/cn/api/catalogue/replies/CatalogueItem;->guideType:Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;

    sget-object v8, Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;->CHANNEL:Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;

    if-ne v7, v8, :cond_3

    .line 1672
    move-object/from16 v0, v19

    iget-wide v0, v0, Lru/cn/api/catalogue/replies/CatalogueItem;->id:J

    move-wide/from16 v32, v0

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v13, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1677
    .end local v19    # "item":Lru/cn/api/catalogue/replies/CatalogueItem;
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProviderData;->getChannels()Ljava/util/List;

    move-result-object v16

    .line 1678
    .local v16, "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_9
    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_a

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lru/cn/api/provider/Channel;

    .line 1679
    .local v12, "channel":Lru/cn/api/provider/Channel;
    iget-wide v0, v12, Lru/cn/api/provider/Channel;->channelId:J

    move-wide/from16 v32, v0

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v13, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 1680
    iget-object v7, v4, Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;->channels:Ljava/util/List;

    invoke-interface {v7, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1685
    .end local v12    # "channel":Lru/cn/api/provider/Channel;
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProviderData;->getIntersectionsChannels()Ljava/util/List;

    move-result-object v11

    .line 1686
    .local v11, "cameras":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_b
    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_c

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lru/cn/api/provider/Channel;

    .line 1687
    .restart local v12    # "channel":Lru/cn/api/provider/Channel;
    iget-wide v0, v12, Lru/cn/api/provider/Channel;->channelId:J

    move-wide/from16 v32, v0

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v13, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 1688
    iget-object v7, v4, Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;->channels:Ljava/util/List;

    invoke-interface {v7, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 1692
    .end local v12    # "channel":Lru/cn/api/provider/Channel;
    :cond_c
    invoke-interface/range {v26 .. v26}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_4

    .line 1696
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lru/cn/api/provider/TvContentProviderData;->loadRecords(Ljava/util/List;)Landroid/support/v4/util/LongSparseArray;

    move-result-object v23

    .line 1699
    .local v23, "records":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Lru/cn/api/provider/TelecastLocationInfo;>;"
    move-object/from16 v0, p0

    iget-object v6, v0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-static {v6}, Lru/cn/api/ServiceLocator;->mediaGuide(Landroid/content/Context;)Lru/cn/api/tv/retrofit/MediaGuideApi;

    move-result-object v30

    .line 1700
    .local v30, "tvAPI":Lru/cn/api/tv/retrofit/MediaGuideApi;
    const-string v6, ","

    move-object/from16 v0, v26

    invoke-static {v6, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v30

    invoke-interface {v0, v6}, Lru/cn/api/tv/retrofit/MediaGuideApi;->telecasts(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v6

    .line 1701
    invoke-virtual {v6}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lru/cn/api/tv/replies/TelecastsResponse;

    iget-object v0, v6, Lru/cn/api/tv/replies/TelecastsResponse;->telecasts:Ljava/util/ArrayList;

    move-object/from16 v29, v0

    .line 1704
    .local v29, "telecastsTmp":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    if-eqz v29, :cond_4

    .line 1708
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 1709
    .local v21, "missingChannels":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-interface/range {v29 .. v29}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_d
    :goto_6
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_11

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lru/cn/api/tv/replies/Telecast;

    .line 1710
    .local v25, "telecast":Lru/cn/api/tv/replies/Telecast;
    move-object/from16 v0, v25

    iget-wide v0, v0, Lru/cn/api/tv/replies/Telecast;->id:J

    move-wide/from16 v32, v0

    move-object/from16 v0, v23

    move-wide/from16 v1, v32

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lru/cn/api/provider/TelecastLocationInfo;

    .line 1711
    .local v18, "info":Lru/cn/api/provider/TelecastLocationInfo;
    if-eqz v18, :cond_e

    .line 1712
    move-object/from16 v0, p0

    iget-object v6, v0, Lru/cn/api/provider/TvContentProviderData;->telecastLocations:Landroid/support/v4/util/LongSparseArray;

    move-object/from16 v0, v25

    iget-wide v0, v0, Lru/cn/api/tv/replies/Telecast;->id:J

    move-wide/from16 v32, v0

    move-wide/from16 v0, v32

    move-object/from16 v2, v18

    invoke-virtual {v6, v0, v1, v2}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 1714
    invoke-interface/range {v31 .. v31}, Ljava/util/Map;->size()I

    move-result v6

    if-lez v6, :cond_10

    .line 1715
    move-object/from16 v0, v25

    iget-wide v0, v0, Lru/cn/api/tv/replies/Telecast;->id:J

    move-wide/from16 v32, v0

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v31

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    move-object/from16 v0, v25

    iput v6, v0, Lru/cn/api/tv/replies/Telecast;->viewsCount:I

    .line 1722
    :cond_e
    :goto_7
    const/4 v14, 0x0

    .line 1723
    .local v14, "channelInfo":Lru/cn/api/tv/replies/ChannelInfo;
    move-object/from16 v0, p0

    iget-object v6, v0, Lru/cn/api/provider/TvContentProviderData;->channelsInfoMap:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v6}, Landroid/support/v4/util/LongSparseArray;->size()I

    move-result v6

    if-lez v6, :cond_d

    .line 1724
    move-object/from16 v0, p0

    iget-object v6, v0, Lru/cn/api/provider/TvContentProviderData;->channelsInfoMap:Landroid/support/v4/util/LongSparseArray;

    move-object/from16 v0, v25

    iget-object v8, v0, Lru/cn/api/tv/replies/Telecast;->channel:Lru/cn/api/tv/replies/Telecast$Channel;

    iget-wide v0, v8, Lru/cn/api/tv/replies/Telecast$Channel;->channelId:J

    move-wide/from16 v32, v0

    move-wide/from16 v0, v32

    invoke-virtual {v6, v0, v1}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/support/v4/util/LongSparseArray;

    .line 1725
    .local v20, "mapByTerritory":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Lru/cn/api/tv/replies/ChannelInfo;>;"
    if-eqz v20, :cond_f

    .line 1726
    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Landroid/support/v4/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "channelInfo":Lru/cn/api/tv/replies/ChannelInfo;
    check-cast v14, Lru/cn/api/tv/replies/ChannelInfo;

    .line 1727
    .restart local v14    # "channelInfo":Lru/cn/api/tv/replies/ChannelInfo;
    if-nez v14, :cond_f

    invoke-virtual/range {v20 .. v20}, Landroid/support/v4/util/LongSparseArray;->size()I

    move-result v6

    const/4 v8, 0x1

    if-le v6, v8, :cond_f

    .line 1728
    const/4 v6, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Landroid/support/v4/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "channelInfo":Lru/cn/api/tv/replies/ChannelInfo;
    check-cast v14, Lru/cn/api/tv/replies/ChannelInfo;

    .line 1732
    .restart local v14    # "channelInfo":Lru/cn/api/tv/replies/ChannelInfo;
    :cond_f
    if-nez v14, :cond_d

    .line 1733
    move-object/from16 v0, v25

    iget-object v6, v0, Lru/cn/api/tv/replies/Telecast;->channel:Lru/cn/api/tv/replies/Telecast$Channel;

    iget-wide v0, v6, Lru/cn/api/tv/replies/Telecast$Channel;->channelId:J

    move-wide/from16 v32, v0

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v21

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    .line 1717
    .end local v14    # "channelInfo":Lru/cn/api/tv/replies/ChannelInfo;
    .end local v20    # "mapByTerritory":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Lru/cn/api/tv/replies/ChannelInfo;>;"
    :cond_10
    const/4 v6, 0x0

    move-object/from16 v0, v25

    iput v6, v0, Lru/cn/api/tv/replies/Telecast;->viewsCount:I

    goto :goto_7

    .line 1738
    .end local v18    # "info":Lru/cn/api/provider/TelecastLocationInfo;
    .end local v25    # "telecast":Lru/cn/api/tv/replies/Telecast;
    :cond_11
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v6

    if-lez v6, :cond_12

    .line 1740
    :try_start_2
    const-string v6, ","

    move-object/from16 v0, v21

    invoke-static {v6, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "categories,title,logoURL,hasSchedule,ageRestriction"

    const/4 v8, 0x0

    move-object/from16 v0, v30

    invoke-interface {v0, v6, v7, v8}, Lru/cn/api/tv/retrofit/MediaGuideApi;->channelsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Lio/reactivex/Single;

    move-result-object v6

    .line 1742
    invoke-virtual {v6}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lru/cn/api/tv/replies/ChannelInfoResponse;

    iget-object v15, v6, Lru/cn/api/tv/replies/ChannelInfoResponse;->list:Ljava/util/List;

    .line 1745
    .local v15, "channelInfos":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/ChannelInfo;>;"
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_8
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_12

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lru/cn/api/tv/replies/ChannelInfo;

    .line 1746
    .restart local v14    # "channelInfo":Lru/cn/api/tv/replies/ChannelInfo;
    const-wide/16 v32, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, v32

    invoke-direct {v0, v14, v1, v2}, Lru/cn/api/provider/TvContentProviderData;->_saveChannelInfo(Lru/cn/api/tv/replies/ChannelInfo;J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_8

    .line 1748
    .end local v14    # "channelInfo":Lru/cn/api/tv/replies/ChannelInfo;
    .end local v15    # "channelInfos":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/ChannelInfo;>;"
    :catch_1
    move-exception v6

    .line 1752
    :cond_12
    :try_start_3
    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_3
.end method

.method public getNavigationItems(JJ)[Lru/cn/api/tv/replies/Telecast;
    .locals 29
    .param p1, "collectionId"    # J
    .param p3, "telecastId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1919
    const/16 v18, 0x0

    .line 1920
    .local v18, "prevTelecast":Lru/cn/api/tv/replies/Telecast;
    const/4 v14, 0x0

    .line 1922
    .local v14, "nextTelecast":Lru/cn/api/tv/replies/Telecast;
    invoke-virtual/range {p0 .. p2}, Lru/cn/api/provider/TvContentProviderData;->getChannel(J)Lru/cn/api/provider/Channel;

    move-result-object v11

    .line 1923
    .local v11, "channel":Lru/cn/api/provider/Channel;
    if-eqz v11, :cond_3

    .line 1924
    iget-object v5, v11, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lru/cn/api/iptv/replies/MediaLocation;

    iget-wide v8, v5, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    .line 1927
    .local v8, "territoryId":J
    move-object/from16 v0, p0

    move-wide/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Lru/cn/api/provider/TvContentProviderData;->getTelecastItem(J)Lru/cn/api/tv/replies/Telecast;

    move-result-object v24

    .line 1929
    .local v24, "telecast":Lru/cn/api/tv/replies/Telecast;
    move-object/from16 v0, v24

    iget-object v5, v0, Lru/cn/api/tv/replies/Telecast;->date:Lru/cn/api/tv/replies/DateTime;

    invoke-virtual {v5}, Lru/cn/api/tv/replies/DateTime;->toCalendar()Ljava/util/Calendar;

    move-result-object v10

    move-object/from16 v5, p0

    move-wide/from16 v6, p1

    invoke-direct/range {v5 .. v10}, Lru/cn/api/provider/TvContentProviderData;->getSchedule(JJLjava/util/Calendar;)Lru/cn/api/provider/Schedule;

    move-result-object v20

    .line 1932
    .local v20, "schedule":Lru/cn/api/provider/Schedule;
    move-object/from16 v0, v24

    iget-object v5, v0, Lru/cn/api/tv/replies/Telecast;->date:Lru/cn/api/tv/replies/DateTime;

    invoke-virtual {v5}, Lru/cn/api/tv/replies/DateTime;->toSeconds()J

    move-result-wide v6

    const-wide/16 v26, 0x5

    sub-long v6, v6, v26

    move-object/from16 v0, v20

    invoke-virtual {v0, v6, v7}, Lru/cn/api/provider/Schedule;->find(J)Lru/cn/api/tv/replies/Telecast;

    move-result-object v18

    .line 1933
    move-object/from16 v0, v24

    iget-object v5, v0, Lru/cn/api/tv/replies/Telecast;->date:Lru/cn/api/tv/replies/DateTime;

    invoke-virtual {v5}, Lru/cn/api/tv/replies/DateTime;->toSeconds()J

    move-result-wide v6

    move-object/from16 v0, v24

    iget-wide v0, v0, Lru/cn/api/tv/replies/Telecast;->duration:J

    move-wide/from16 v26, v0

    add-long v6, v6, v26

    move-object/from16 v0, v20

    invoke-virtual {v0, v6, v7}, Lru/cn/api/provider/Schedule;->find(J)Lru/cn/api/tv/replies/Telecast;

    move-result-object v14

    .line 1936
    const/4 v5, 0x2

    new-array v5, v5, [Lru/cn/api/tv/replies/Telecast;

    const/4 v6, 0x0

    aput-object v18, v5, v6

    const/4 v6, 0x1

    aput-object v14, v5, v6

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lru/cn/api/provider/TvContentProviderData;->updateTelecastLocations([Lru/cn/api/tv/replies/Telecast;)V

    .line 1972
    .end local v8    # "territoryId":J
    .end local v20    # "schedule":Lru/cn/api/provider/Schedule;
    .end local v24    # "telecast":Lru/cn/api/tv/replies/Telecast;
    :cond_0
    :goto_0
    if-eqz v18, :cond_1

    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProviderData;->telecastLocations:Landroid/support/v4/util/LongSparseArray;

    move-object/from16 v0, v18

    iget-wide v6, v0, Lru/cn/api/tv/replies/Telecast;->id:J

    invoke-virtual {v5, v6, v7}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_1

    .line 1973
    const/16 v18, 0x0

    .line 1976
    :cond_1
    if-eqz v14, :cond_2

    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProviderData;->telecastLocations:Landroid/support/v4/util/LongSparseArray;

    iget-wide v6, v14, Lru/cn/api/tv/replies/Telecast;->id:J

    invoke-virtual {v5, v6, v7}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_2

    .line 1977
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v26, 0x3e8

    div-long v16, v6, v26

    .line 1978
    .local v16, "nowSec":J
    iget-object v5, v14, Lru/cn/api/tv/replies/Telecast;->date:Lru/cn/api/tv/replies/DateTime;

    invoke-virtual {v5}, Lru/cn/api/tv/replies/DateTime;->toSeconds()J

    move-result-wide v22

    .line 1979
    .local v22, "startTimeSec":J
    cmp-long v5, v22, v16

    if-gez v5, :cond_8

    iget-wide v6, v14, Lru/cn/api/tv/replies/Telecast;->duration:J

    add-long v6, v6, v22

    cmp-long v5, v6, v16

    if-lez v5, :cond_8

    const/4 v15, 0x1

    .line 1980
    .local v15, "onAir":Z
    :goto_1
    if-nez v15, :cond_2

    .line 1981
    const/4 v14, 0x0

    .line 1985
    .end local v15    # "onAir":Z
    .end local v16    # "nowSec":J
    .end local v22    # "startTimeSec":J
    :cond_2
    const/4 v5, 0x2

    new-array v5, v5, [Lru/cn/api/tv/replies/Telecast;

    const/4 v6, 0x0

    aput-object v18, v5, v6

    const/4 v6, 0x1

    aput-object v14, v5, v6

    return-object v5

    .line 1939
    :cond_3
    invoke-virtual/range {p0 .. p2}, Lru/cn/api/provider/TvContentProviderData;->getRubricInfo(J)Lru/cn/api/catalogue/replies/Rubric;

    move-result-object v19

    .line 1940
    .local v19, "rubric":Lru/cn/api/catalogue/replies/Rubric;
    if-eqz v19, :cond_0

    move-object/from16 v0, v19

    iget-object v5, v0, Lru/cn/api/catalogue/replies/Rubric;->uiHint:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    if-eqz v5, :cond_0

    move-object/from16 v0, v19

    iget-object v5, v0, Lru/cn/api/catalogue/replies/Rubric;->uiHint:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    sget-object v6, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->STORIES:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    if-ne v5, v6, :cond_0

    .line 1942
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProviderData;->rubricOptionTelecastsMap:Landroid/support/v4/util/LongSparseArray;

    move-wide/from16 v0, p1

    invoke-virtual {v5, v0, v1}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/util/HashMap;

    .line 1943
    .local v25, "telecastsOptionMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;>;"
    if-eqz v25, :cond_0

    .line 1944
    invoke-virtual/range {v25 .. v25}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;

    .line 1946
    .local v4, "cacheObj":Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;
    const/4 v12, 0x0

    .line 1947
    .local v12, "lastTelecast":Lru/cn/api/tv/replies/Telecast;
    const/16 v21, 0x0

    .line 1949
    .local v21, "searchRight":Z
    iget-object v5, v4, Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;->telecasts:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lru/cn/api/tv/replies/Telecast;

    .line 1950
    .restart local v24    # "telecast":Lru/cn/api/tv/replies/Telecast;
    if-eqz v21, :cond_6

    if-eqz v12, :cond_6

    .line 1951
    move-object v14, v12

    .line 1965
    .end local v24    # "telecast":Lru/cn/api/tv/replies/Telecast;
    :cond_5
    if-eqz v21, :cond_0

    if-eqz v12, :cond_0

    .line 1966
    move-object v14, v12

    goto/16 :goto_0

    .line 1955
    .restart local v24    # "telecast":Lru/cn/api/tv/replies/Telecast;
    :cond_6
    move-object/from16 v0, p0

    iget-object v6, v0, Lru/cn/api/provider/TvContentProviderData;->telecastLocations:Landroid/support/v4/util/LongSparseArray;

    move-object/from16 v0, v24

    iget-wide v0, v0, Lru/cn/api/tv/replies/Telecast;->id:J

    move-wide/from16 v26, v0

    move-wide/from16 v0, v26

    invoke-virtual {v6, v0, v1}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lru/cn/api/provider/TelecastLocationInfo;

    .line 1956
    .local v13, "location":Lru/cn/api/provider/TelecastLocationInfo;
    move-object/from16 v0, v24

    iget-wide v6, v0, Lru/cn/api/tv/replies/Telecast;->id:J

    cmp-long v6, v6, p3

    if-nez v6, :cond_7

    .line 1957
    move-object/from16 v18, v12

    .line 1958
    const/16 v21, 0x1

    .line 1959
    const/4 v12, 0x0

    goto :goto_2

    .line 1960
    :cond_7
    if-eqz v13, :cond_4

    .line 1961
    move-object/from16 v12, v24

    goto :goto_2

    .line 1979
    .end local v4    # "cacheObj":Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;
    .end local v12    # "lastTelecast":Lru/cn/api/tv/replies/Telecast;
    .end local v13    # "location":Lru/cn/api/provider/TelecastLocationInfo;
    .end local v19    # "rubric":Lru/cn/api/catalogue/replies/Rubric;
    .end local v21    # "searchRight":Z
    .end local v24    # "telecast":Lru/cn/api/tv/replies/Telecast;
    .end local v25    # "telecastsOptionMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;>;"
    .restart local v16    # "nowSec":J
    .restart local v22    # "startTimeSec":J
    :cond_8
    const/4 v15, 0x0

    goto/16 :goto_1
.end method

.method public getNextChannel(JZ)Lru/cn/api/provider/Channel;
    .locals 11
    .param p1, "channelId"    # J
    .param p3, "favourite"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 969
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateChannelsIfExpire()V

    .line 970
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateFavouriteChannelsIfExpire()V

    .line 973
    if-eqz p3, :cond_0

    .line 974
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProviderData;->getFavouriteChannels()Ljava/util/List;

    move-result-object v2

    .line 979
    .local v2, "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v8

    if-nez v8, :cond_1

    .line 980
    const/4 v8, 0x0

    .line 1034
    :goto_1
    return-object v8

    .line 976
    .end local v2    # "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    :cond_0
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProviderData;->getChannels()Ljava/util/List;

    move-result-object v2

    .restart local v2    # "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    goto :goto_0

    .line 983
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 984
    .local v3, "count":I
    const-wide/16 v8, 0x0

    cmp-long v8, p1, v8

    if-nez v8, :cond_2

    .line 985
    const/4 v8, 0x0

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lru/cn/api/provider/Channel;

    goto :goto_1

    .line 989
    :cond_2
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    if-ge v4, v3, :cond_3

    .line 990
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/Channel;

    .line 991
    .local v0, "c":Lru/cn/api/provider/Channel;
    iget-wide v8, v0, Lru/cn/api/provider/Channel;->channelId:J

    cmp-long v8, v8, p1

    if-nez v8, :cond_6

    .line 996
    .end local v0    # "c":Lru/cn/api/provider/Channel;
    :cond_3
    add-int/lit8 v8, v4, 0x1

    rem-int v4, v8, v3

    .line 998
    const/4 v5, 0x0

    .line 999
    .local v5, "isDenied":Z
    const/4 v1, 0x0

    .line 1000
    .local v1, "channel":Lru/cn/api/provider/Channel;
    move v6, v4

    .local v6, "it":I
    :goto_3
    if-ge v6, v3, :cond_7

    .line 1001
    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "channel":Lru/cn/api/provider/Channel;
    check-cast v1, Lru/cn/api/provider/Channel;

    .line 1002
    .restart local v1    # "channel":Lru/cn/api/provider/Channel;
    iget-object v8, v1, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lru/cn/api/iptv/replies/MediaLocation;

    .line 1003
    .local v7, "l":Lru/cn/api/iptv/replies/MediaLocation;
    iget-object v9, v7, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;

    sget-object v10, Lru/cn/api/iptv/replies/MediaLocation$Access;->denied:Lru/cn/api/iptv/replies/MediaLocation$Access;

    if-ne v9, v10, :cond_4

    .line 1004
    const/4 v5, 0x1

    .line 1008
    .end local v7    # "l":Lru/cn/api/iptv/replies/MediaLocation;
    :cond_5
    if-eqz v5, :cond_7

    .line 1009
    const/4 v5, 0x0

    .line 1010
    const/4 v1, 0x0

    .line 1000
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 989
    .end local v1    # "channel":Lru/cn/api/provider/Channel;
    .end local v5    # "isDenied":Z
    .end local v6    # "it":I
    .restart local v0    # "c":Lru/cn/api/provider/Channel;
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1016
    .end local v0    # "c":Lru/cn/api/provider/Channel;
    .restart local v1    # "channel":Lru/cn/api/provider/Channel;
    .restart local v5    # "isDenied":Z
    .restart local v6    # "it":I
    :cond_7
    if-nez v1, :cond_a

    .line 1017
    const/4 v6, 0x0

    :goto_4
    if-ge v6, v3, :cond_a

    .line 1018
    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "channel":Lru/cn/api/provider/Channel;
    check-cast v1, Lru/cn/api/provider/Channel;

    .line 1019
    .restart local v1    # "channel":Lru/cn/api/provider/Channel;
    iget-object v8, v1, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_8
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lru/cn/api/iptv/replies/MediaLocation;

    .line 1020
    .restart local v7    # "l":Lru/cn/api/iptv/replies/MediaLocation;
    iget-object v9, v7, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;

    sget-object v10, Lru/cn/api/iptv/replies/MediaLocation$Access;->denied:Lru/cn/api/iptv/replies/MediaLocation$Access;

    if-ne v9, v10, :cond_8

    .line 1021
    const/4 v5, 0x1

    .line 1025
    .end local v7    # "l":Lru/cn/api/iptv/replies/MediaLocation;
    :cond_9
    if-eqz v5, :cond_a

    .line 1026
    const/4 v5, 0x0

    .line 1027
    const/4 v1, 0x0

    .line 1017
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_a
    move-object v8, v1

    .line 1034
    goto/16 :goto_1
.end method

.method public getPinCode()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1531
    iget-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->pinCodeCache:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 1532
    iget-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-static {v2}, Lru/cn/api/userdata/UserData;->getPinCode(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 1533
    .local v0, "catalogueItemClassList":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/userdata/elementclasses/CatalogueItemClass;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 1534
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/userdata/elementclasses/CatalogueItemClass;

    .line 1535
    .local v1, "pinCode":Lru/cn/api/userdata/elementclasses/CatalogueItemClass;
    iget-object v2, v1, Lru/cn/api/userdata/elementclasses/CatalogueItemClass;->catalogue_item_id:Ljava/lang/String;

    iput-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->pinCodeCache:Ljava/lang/String;

    .line 1539
    .end local v0    # "catalogueItemClassList":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/userdata/elementclasses/CatalogueItemClass;>;"
    .end local v1    # "pinCode":Lru/cn/api/userdata/elementclasses/CatalogueItemClass;
    :cond_0
    iget-object v2, p0, Lru/cn/api/provider/TvContentProviderData;->pinCodeCache:Ljava/lang/String;

    return-object v2
.end method

.method public getPornoChannels()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/provider/Channel;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 212
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateChannelsIfExpire()V

    .line 213
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateFavouriteChannelsIfExpire()V

    .line 215
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lru/cn/api/provider/TvContentProviderData;->pornoChannelsList:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getPrevChannel(JZ)Lru/cn/api/provider/Channel;
    .locals 11
    .param p1, "channelId"    # J
    .param p3, "favourite"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1039
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateChannelsIfExpire()V

    .line 1040
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateFavouriteChannelsIfExpire()V

    .line 1043
    if-eqz p3, :cond_0

    .line 1044
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProviderData;->getFavouriteChannels()Ljava/util/List;

    move-result-object v2

    .line 1049
    .local v2, "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v8

    if-nez v8, :cond_1

    .line 1050
    const/4 v8, 0x0

    .line 1109
    :goto_1
    return-object v8

    .line 1046
    .end local v2    # "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    :cond_0
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProviderData;->getChannels()Ljava/util/List;

    move-result-object v2

    .restart local v2    # "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    goto :goto_0

    .line 1053
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 1054
    .local v3, "count":I
    const-wide/16 v8, 0x0

    cmp-long v8, p1, v8

    if-nez v8, :cond_2

    .line 1055
    add-int/lit8 v8, v3, -0x1

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lru/cn/api/provider/Channel;

    goto :goto_1

    .line 1059
    :cond_2
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    if-ge v4, v3, :cond_3

    .line 1060
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/Channel;

    .line 1061
    .local v0, "c":Lru/cn/api/provider/Channel;
    iget-wide v8, v0, Lru/cn/api/provider/Channel;->channelId:J

    cmp-long v8, v8, p1

    if-nez v8, :cond_4

    .line 1066
    .end local v0    # "c":Lru/cn/api/provider/Channel;
    :cond_3
    if-ne v4, v3, :cond_5

    .line 1067
    add-int/lit8 v8, v3, -0x1

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lru/cn/api/provider/Channel;

    goto :goto_1

    .line 1059
    .restart local v0    # "c":Lru/cn/api/provider/Channel;
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1070
    .end local v0    # "c":Lru/cn/api/provider/Channel;
    :cond_5
    add-int/lit8 v8, v4, -0x1

    add-int/2addr v8, v3

    rem-int v4, v8, v3

    .line 1073
    const/4 v5, 0x0

    .line 1074
    .local v5, "isDenied":Z
    const/4 v1, 0x0

    .line 1075
    .local v1, "channel":Lru/cn/api/provider/Channel;
    move v6, v4

    .local v6, "it":I
    :goto_3
    if-ltz v6, :cond_8

    .line 1076
    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "channel":Lru/cn/api/provider/Channel;
    check-cast v1, Lru/cn/api/provider/Channel;

    .line 1077
    .restart local v1    # "channel":Lru/cn/api/provider/Channel;
    iget-object v8, v1, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_6
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lru/cn/api/iptv/replies/MediaLocation;

    .line 1078
    .local v7, "l":Lru/cn/api/iptv/replies/MediaLocation;
    iget-object v9, v7, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;

    sget-object v10, Lru/cn/api/iptv/replies/MediaLocation$Access;->denied:Lru/cn/api/iptv/replies/MediaLocation$Access;

    if-ne v9, v10, :cond_6

    .line 1079
    const/4 v5, 0x1

    .line 1083
    .end local v7    # "l":Lru/cn/api/iptv/replies/MediaLocation;
    :cond_7
    if-eqz v5, :cond_8

    .line 1084
    const/4 v5, 0x0

    .line 1085
    const/4 v1, 0x0

    .line 1075
    add-int/lit8 v6, v6, -0x1

    goto :goto_3

    .line 1091
    :cond_8
    if-nez v1, :cond_b

    .line 1092
    add-int/lit8 v6, v3, -0x1

    :goto_4
    if-ltz v6, :cond_b

    .line 1093
    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "channel":Lru/cn/api/provider/Channel;
    check-cast v1, Lru/cn/api/provider/Channel;

    .line 1094
    .restart local v1    # "channel":Lru/cn/api/provider/Channel;
    iget-object v8, v1, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_9
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_a

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lru/cn/api/iptv/replies/MediaLocation;

    .line 1095
    .restart local v7    # "l":Lru/cn/api/iptv/replies/MediaLocation;
    iget-object v9, v7, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;

    sget-object v10, Lru/cn/api/iptv/replies/MediaLocation$Access;->denied:Lru/cn/api/iptv/replies/MediaLocation$Access;

    if-ne v9, v10, :cond_9

    .line 1096
    const/4 v5, 0x1

    .line 1100
    .end local v7    # "l":Lru/cn/api/iptv/replies/MediaLocation;
    :cond_a
    if-eqz v5, :cond_b

    .line 1101
    const/4 v5, 0x0

    .line 1102
    const/4 v1, 0x0

    .line 1092
    add-int/lit8 v6, v6, -0x1

    goto :goto_4

    :cond_b
    move-object v8, v1

    .line 1109
    goto/16 :goto_1
.end method

.method public getRelatedRubrics(J)Ljava/util/List;
    .locals 9
    .param p1, "catalogueId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1760
    iget-object v4, p0, Lru/cn/api/provider/TvContentProviderData;->relatedRubricCache:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v4, p1, p2}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 1761
    .local v3, "rubrics":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/catalogue/replies/Rubric;>;"
    if-eqz v3, :cond_0

    .line 1781
    .end local v3    # "rubrics":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/catalogue/replies/Rubric;>;"
    :goto_0
    return-object v3

    .line 1764
    .restart local v3    # "rubrics":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/catalogue/replies/Rubric;>;"
    :cond_0
    const-wide/16 v6, 0x0

    cmp-long v4, p1, v6

    if-gtz v4, :cond_1

    move-object v3, v5

    .line 1765
    goto :goto_0

    .line 1767
    :cond_1
    iget-object v4, p0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-static {v4}, Lru/cn/api/ServiceLocator;->catalogue(Landroid/content/Context;)Lru/cn/api/catalogue/retrofit/CatalogueApi;

    move-result-object v0

    .line 1769
    .local v0, "catalogue":Lru/cn/api/catalogue/retrofit/CatalogueApi;
    const-string v4, "id,guide_type,selection_attributes,related_rubrics"

    invoke-interface {v0, p1, p2, v4}, Lru/cn/api/catalogue/retrofit/CatalogueApi;->items(JLjava/lang/String;)Lio/reactivex/Single;

    move-result-object v4

    .line 1770
    invoke-virtual {v4}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/api/catalogue/replies/SelectionPage;

    .line 1772
    .local v2, "response":Lru/cn/api/catalogue/replies/SelectionPage;
    iget-object v4, v2, Lru/cn/api/catalogue/replies/SelectionPage;->items:Ljava/util/List;

    if-eqz v4, :cond_2

    iget-object v4, v2, Lru/cn/api/catalogue/replies/SelectionPage;->items:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, v2, Lru/cn/api/catalogue/replies/SelectionPage;->items:Ljava/util/List;

    const/4 v6, 0x0

    .line 1773
    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/api/catalogue/replies/CatalogueItem;

    move-object v1, v4

    .line 1776
    .local v1, "item":Lru/cn/api/catalogue/replies/CatalogueItem;
    :goto_1
    if-nez v1, :cond_3

    move-object v3, v5

    .line 1777
    goto :goto_0

    .end local v1    # "item":Lru/cn/api/catalogue/replies/CatalogueItem;
    :cond_2
    move-object v1, v5

    .line 1773
    goto :goto_1

    .line 1779
    .restart local v1    # "item":Lru/cn/api/catalogue/replies/CatalogueItem;
    :cond_3
    iget-object v4, p0, Lru/cn/api/provider/TvContentProviderData;->relatedRubricCache:Landroid/support/v4/util/LongSparseArray;

    iget-object v5, v1, Lru/cn/api/catalogue/replies/CatalogueItem;->relatedRubrics:Ljava/util/List;

    invoke-virtual {v4, p1, p2, v5}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 1781
    iget-object v3, v1, Lru/cn/api/catalogue/replies/CatalogueItem;->relatedRubrics:Ljava/util/List;

    goto :goto_0
.end method

.method public getRubricInfo(J)Lru/cn/api/catalogue/replies/Rubric;
    .locals 1
    .param p1, "id"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1568
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateRubricatorCacheIfNeed()V

    .line 1569
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->rubricCache:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/catalogue/replies/Rubric;

    return-object v0
.end method

.method public getRubricItems(JLjava/lang/String;)Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;
    .locals 1
    .param p1, "rubricId"    # J
    .param p3, "optionValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1586
    const/16 v0, 0x23

    invoke-virtual {p0, p1, p2, p3, v0}, Lru/cn/api/provider/TvContentProviderData;->getRubricItems(JLjava/lang/String;I)Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;

    move-result-object v0

    return-object v0
.end method

.method public getRubricItems(JLjava/lang/String;I)Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;
    .locals 11
    .param p1, "rubricId"    # J
    .param p3, "optionValue"    # Ljava/lang/String;
    .param p4, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1592
    iget-object v1, p0, Lru/cn/api/provider/TvContentProviderData;->rubricOptionTelecastsMap:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v1, p1, p2}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map;

    .line 1594
    .local v7, "telecastsOptionMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;>;"
    if-nez v7, :cond_1

    .line 1595
    invoke-virtual {p0, p1, p2, p3, p4}, Lru/cn/api/provider/TvContentProviderData;->getMoreRubricItems(JLjava/lang/String;I)Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;

    move-result-object v0

    .line 1605
    :cond_0
    :goto_0
    return-object v0

    .line 1598
    :cond_1
    invoke-direct {p0, p3}, Lru/cn/api/provider/TvContentProviderData;->cacheKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v7, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;

    .line 1599
    .local v0, "cacheObj":Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long v8, v2, v4

    .line 1600
    .local v8, "timeNow":J
    if-eqz v0, :cond_2

    iget-object v1, v0, Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;->telecasts:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-wide v2, v0, Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;->cacheExpireTime:J

    sub-long v2, v8, v2

    const-wide/16 v4, 0x4b0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 1602
    :cond_2
    const/4 v5, 0x1

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move v6, p4

    invoke-virtual/range {v1 .. v6}, Lru/cn/api/provider/TvContentProviderData;->getMoreRubricTelecasts(JLjava/lang/String;ZI)Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;

    move-result-object v0

    goto :goto_0
.end method

.method public getRubricator()Lru/cn/api/catalogue/replies/Rubricator;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1555
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateRubricatorCacheIfNeed()V

    .line 1556
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->rubricatorCache:Lru/cn/api/catalogue/replies/Rubricator;

    return-object v0
.end method

.method public getStoredChannelInfo(JJ)Lru/cn/api/tv/replies/ChannelInfo;
    .locals 5
    .param p1, "cnId"    # J
    .param p3, "territoryId"    # J

    .prologue
    .line 281
    iget-object v1, p0, Lru/cn/api/provider/TvContentProviderData;->channelsInfoMap:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v1, p1, p2}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/util/LongSparseArray;

    .line 282
    .local v0, "map":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Lru/cn/api/tv/replies/ChannelInfo;>;"
    if-eqz v0, :cond_1

    .line 283
    const-wide/16 v2, 0x0

    cmp-long v1, p3, v2

    if-nez v1, :cond_0

    .line 284
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/tv/replies/ChannelInfo;

    .line 289
    :goto_0
    return-object v1

    .line 286
    :cond_0
    invoke-virtual {v0, p3, p4}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/tv/replies/ChannelInfo;

    goto :goto_0

    .line 289
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getStoredCurrentTelecast(JJZ)Lru/cn/api/tv/replies/Telecast;
    .locals 3
    .param p1, "channelId"    # J
    .param p3, "territoryId"    # J
    .param p5, "allowExpired"    # Z

    .prologue
    .line 360
    invoke-direct {p0, p1, p2, p3, p4}, Lru/cn/api/provider/TvContentProviderData;->getSchedule(JJ)Lru/cn/api/provider/Schedule;

    move-result-object v0

    .line 361
    .local v0, "schedule":Lru/cn/api/provider/Schedule;
    invoke-virtual {v0, p5}, Lru/cn/api/provider/Schedule;->getCurrentTelecast(Z)Lru/cn/api/tv/replies/Telecast;

    move-result-object v1

    return-object v1
.end method

.method public getTelecastItem(J)Lru/cn/api/tv/replies/Telecast;
    .locals 11
    .param p1, "telecastId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 1218
    const-wide/16 v8, 0x0

    cmp-long v6, p1, v8

    if-gtz v6, :cond_1

    move-object v4, v7

    .line 1246
    :cond_0
    :goto_0
    return-object v4

    .line 1221
    :cond_1
    iget-object v6, p0, Lru/cn/api/provider/TvContentProviderData;->telecastsMap:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v6, p1, p2}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/api/tv/replies/Telecast;

    .line 1224
    .local v4, "telecast":Lru/cn/api/tv/replies/Telecast;
    if-nez v4, :cond_0

    .line 1225
    :try_start_0
    iget-object v6, p0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-static {v6}, Lru/cn/api/ServiceLocator;->mediaGuide(Landroid/content/Context;)Lru/cn/api/tv/retrofit/MediaGuideApi;

    move-result-object v5

    .line 1226
    .local v5, "tvAPI":Lru/cn/api/tv/retrofit/MediaGuideApi;
    if-nez v5, :cond_2

    move-object v4, v7

    .line 1227
    goto :goto_0

    .line 1229
    :cond_2
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lru/cn/api/tv/retrofit/MediaGuideApi;->telecasts(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v6

    .line 1230
    invoke-virtual {v6}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lru/cn/api/tv/replies/TelecastsResponse;

    .line 1232
    .local v3, "response":Lru/cn/api/tv/replies/TelecastsResponse;
    iget-object v6, v3, Lru/cn/api/tv/replies/TelecastsResponse;->telecasts:Ljava/util/ArrayList;

    if-eqz v6, :cond_3

    iget-object v6, v3, Lru/cn/api/tv/replies/TelecastsResponse;->telecasts:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    .line 1233
    iget-object v6, v3, Lru/cn/api/tv/replies/TelecastsResponse;->telecasts:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lru/cn/api/tv/replies/Telecast;

    move-object v4, v0

    .line 1236
    :cond_3
    if-eqz v4, :cond_0

    .line 1237
    iget-object v6, p0, Lru/cn/api/provider/TvContentProviderData;->telecastsMap:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v6, p1, p2, v4}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V
    :try_end_0
    .catch Lru/cn/utils/http/NetworkException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lru/cn/api/BaseAPI$ParseException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1242
    .end local v3    # "response":Lru/cn/api/tv/replies/TelecastsResponse;
    .end local v5    # "tvAPI":Lru/cn/api/tv/retrofit/MediaGuideApi;
    :catch_0
    move-exception v2

    .line 1243
    .local v2, "e":Ljava/lang/Exception;
    :goto_1
    invoke-static {v2}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    move-object v4, v7

    .line 1246
    goto :goto_0

    .line 1242
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method public getTelecastItemByTime(JJJ)Lru/cn/api/tv/replies/Telecast;
    .locals 9
    .param p1, "channelId"    # J
    .param p3, "timeInSec"    # J
    .param p5, "territoryId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1251
    const-wide/16 v2, 0x0

    cmp-long v1, p5, v2

    if-nez v1, :cond_0

    .line 1252
    invoke-virtual {p0, p1, p2}, Lru/cn/api/provider/TvContentProviderData;->getChannel(J)Lru/cn/api/provider/Channel;

    move-result-object v7

    .line 1253
    .local v7, "channel":Lru/cn/api/provider/Channel;
    iget-object v1, v7, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/iptv/replies/MediaLocation;

    iget-wide p5, v1, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    .line 1256
    .end local v7    # "channel":Lru/cn/api/provider/Channel;
    :cond_0
    invoke-direct {p0, p1, p2, p5, p6}, Lru/cn/api/provider/TvContentProviderData;->getSchedule(JJ)Lru/cn/api/provider/Schedule;

    move-result-object v8

    .line 1257
    .local v8, "schedule":Lru/cn/api/provider/Schedule;
    invoke-virtual {v8, p3, p4}, Lru/cn/api/provider/Schedule;->find(J)Lru/cn/api/tv/replies/Telecast;

    move-result-object v0

    .line 1258
    .local v0, "cachedTelecast":Lru/cn/api/tv/replies/Telecast;
    if-eqz v0, :cond_1

    .line 1269
    .end local v0    # "cachedTelecast":Lru/cn/api/tv/replies/Telecast;
    :goto_0
    return-object v0

    .line 1261
    .restart local v0    # "cachedTelecast":Lru/cn/api/tv/replies/Telecast;
    :cond_1
    invoke-static {}, Lru/cn/utils/Utils;->getCalendar()Ljava/util/Calendar;

    move-result-object v6

    .line 1262
    .local v6, "calendar":Ljava/util/Calendar;
    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p3

    invoke-virtual {v6, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p5

    .line 1265
    invoke-direct/range {v1 .. v6}, Lru/cn/api/provider/TvContentProviderData;->getSchedule(JJLjava/util/Calendar;)Lru/cn/api/provider/Schedule;

    move-result-object v8

    .line 1266
    if-nez v8, :cond_2

    .line 1267
    const/4 v0, 0x0

    goto :goto_0

    .line 1269
    :cond_2
    invoke-virtual {v8, p3, p4}, Lru/cn/api/provider/Schedule;->find(J)Lru/cn/api/tv/replies/Telecast;

    move-result-object v0

    goto :goto_0
.end method

.method public getTelecastLocations(J)Lru/cn/api/provider/TelecastLocationInfo;
    .locals 1
    .param p1, "telecastId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1304
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lru/cn/api/provider/TvContentProviderData;->getTelecastLocations(JZ)Lru/cn/api/provider/TelecastLocationInfo;

    move-result-object v0

    return-object v0
.end method

.method public getTelecastLocations(JZ)Lru/cn/api/provider/TelecastLocationInfo;
    .locals 7
    .param p1, "telecastId"    # J
    .param p3, "cacheOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1273
    if-eqz p3, :cond_0

    .line 1274
    iget-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->telecastLocations:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v3, p1, p2}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lru/cn/api/provider/TelecastLocationInfo;

    .line 1285
    :goto_0
    return-object v3

    .line 1276
    :cond_0
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Long;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 1277
    .local v0, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-direct {p0, v0}, Lru/cn/api/provider/TvContentProviderData;->loadRecords(Ljava/util/List;)Landroid/support/v4/util/LongSparseArray;

    move-result-object v2

    .line 1278
    .local v2, "records":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Lru/cn/api/provider/TelecastLocationInfo;>;"
    if-eqz v2, :cond_1

    invoke-virtual {v2, p1, p2}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1279
    invoke-virtual {v2, p1, p2}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/provider/TelecastLocationInfo;

    .line 1280
    .local v1, "info":Lru/cn/api/provider/TelecastLocationInfo;
    if-eqz v1, :cond_1

    .line 1281
    iget-object v3, p0, Lru/cn/api/provider/TvContentProviderData;->telecastLocations:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v3, p1, p2, v1}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    move-object v3, v1

    .line 1282
    goto :goto_0

    .line 1285
    .end local v1    # "info":Lru/cn/api/provider/TelecastLocationInfo;
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getUserPlayists()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1785
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->userPlaylistsMap:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 1786
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateUserPlaylists()V

    .line 1788
    :cond_0
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->userPlaylistsMap:Ljava/util/Map;

    return-object v0
.end method

.method public invalidate(Z)V
    .locals 2
    .param p1, "includeUserData"    # Z

    .prologue
    .line 1885
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->cacheUtils:Lru/cn/api/provider/CacheUtils;

    sget-object v1, Lru/cn/api/provider/CacheUtils$CacheName;->channels:Lru/cn/api/provider/CacheUtils$CacheName;

    invoke-virtual {v0, v1}, Lru/cn/api/provider/CacheUtils;->deleteCacheExpireTime(Lru/cn/api/provider/CacheUtils$CacheName;)V

    .line 1886
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->cacheUtils:Lru/cn/api/provider/CacheUtils;

    sget-object v1, Lru/cn/api/provider/CacheUtils$CacheName;->rubricator:Lru/cn/api/provider/CacheUtils$CacheName;

    invoke-virtual {v0, v1}, Lru/cn/api/provider/CacheUtils;->deleteCacheExpireTime(Lru/cn/api/provider/CacheUtils$CacheName;)V

    .line 1888
    if-eqz p1, :cond_0

    .line 1889
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->cacheUtils:Lru/cn/api/provider/CacheUtils;

    sget-object v1, Lru/cn/api/provider/CacheUtils$CacheName;->user_data:Lru/cn/api/provider/CacheUtils$CacheName;

    invoke-virtual {v0, v1}, Lru/cn/api/provider/CacheUtils;->deleteCacheExpireTime(Lru/cn/api/provider/CacheUtils$CacheName;)V

    .line 1890
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->favouriteChannelsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1891
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->favouritesIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1892
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->favouriteObjects:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/support/v4/util/LongSparseArray;->clear()V

    .line 1893
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->userPlaylistsMap:Ljava/util/Map;

    .line 1896
    :cond_0
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->hdChannelsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1897
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->intersectionsChannelsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1898
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->rubricOptionTelecastsMap:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/support/v4/util/LongSparseArray;->clear()V

    .line 1899
    return-void
.end method

.method public isFavourite(J)Z
    .locals 3
    .param p1, "cnId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 408
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateFavouriteChannelsIfExpire()V

    .line 410
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->favouritesIds:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final synthetic lambda$channels$4$TvContentProviderData(Lru/cn/api/iptv/replies/MediaLocation;)Z
    .locals 4
    .param p1, "location"    # Lru/cn/api/iptv/replies/MediaLocation;

    .prologue
    .line 587
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->timezones:Ljava/util/Map;

    iget-wide v2, p1, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final synthetic lambda$filterChannels$15$TvContentProviderData(Lru/cn/api/provider/Channel;)Z
    .locals 4
    .param p1, "channel"    # Lru/cn/api/provider/Channel;

    .prologue
    .line 1995
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->blockedIds:Ljava/util/Set;

    iget-wide v2, p1, Lru/cn/api/provider/Channel;->channelId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final synthetic lambda$filterChannels$18$TvContentProviderData(ILru/cn/api/provider/Channel;)Z
    .locals 2
    .param p1, "age"    # I
    .param p2, "channel"    # Lru/cn/api/provider/Channel;

    .prologue
    .line 1997
    iget-object v0, p2, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    invoke-static {v0}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v0

    new-instance v1, Lru/cn/api/provider/TvContentProviderData$$Lambda$18;

    invoke-direct {v1, p0, p2}, Lru/cn/api/provider/TvContentProviderData$$Lambda$18;-><init>(Lru/cn/api/provider/TvContentProviderData;Lru/cn/api/provider/Channel;)V

    .line 1998
    invoke-virtual {v0, v1}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v0

    .line 1999
    invoke-virtual {v0}, Lcom/annimon/stream/Stream;->withoutNulls()Lcom/annimon/stream/Stream;

    move-result-object v0

    new-instance v1, Lru/cn/api/provider/TvContentProviderData$$Lambda$19;

    invoke-direct {v1, p0, p1, p2}, Lru/cn/api/provider/TvContentProviderData$$Lambda$19;-><init>(Lru/cn/api/provider/TvContentProviderData;ILru/cn/api/provider/Channel;)V

    .line 2000
    invoke-virtual {v0, v1}, Lcom/annimon/stream/Stream;->anyMatch(Lcom/annimon/stream/function/Predicate;)Z

    move-result v0

    .line 1997
    return v0
.end method

.method final synthetic lambda$getAllowedChannels$0$TvContentProviderData(Ljava/lang/Long;)Lru/cn/api/provider/Channel;
    .locals 4
    .param p1, "id"    # Ljava/lang/Long;

    .prologue
    .line 153
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lru/cn/api/provider/TvContentProviderData;->getChannel(J)Lru/cn/api/provider/Channel;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 155
    :goto_0
    return-object v1

    .line 154
    :catch_0
    move-exception v0

    .line 155
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method final synthetic lambda$null$16$TvContentProviderData(Lru/cn/api/provider/Channel;Lru/cn/api/iptv/replies/MediaLocation;)Lru/cn/api/tv/replies/ChannelInfo;
    .locals 4
    .param p1, "channel"    # Lru/cn/api/provider/Channel;
    .param p2, "location"    # Lru/cn/api/iptv/replies/MediaLocation;

    .prologue
    .line 1998
    iget-wide v0, p1, Lru/cn/api/provider/Channel;->channelId:J

    iget-wide v2, p2, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    invoke-virtual {p0, v0, v1, v2, v3}, Lru/cn/api/provider/TvContentProviderData;->getStoredChannelInfo(JJ)Lru/cn/api/tv/replies/ChannelInfo;

    move-result-object v0

    return-object v0
.end method

.method final synthetic lambda$null$17$TvContentProviderData(ILru/cn/api/provider/Channel;Lru/cn/api/tv/replies/ChannelInfo;)Z
    .locals 4
    .param p1, "age"    # I
    .param p2, "channel"    # Lru/cn/api/provider/Channel;
    .param p3, "info"    # Lru/cn/api/tv/replies/ChannelInfo;

    .prologue
    .line 2000
    iget v0, p3, Lru/cn/api/tv/replies/ChannelInfo;->ageRestriction:I

    if-le v0, p1, :cond_0

    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->allowedChannels:Lru/cn/api/allowed/AllowedChannels;

    iget-wide v2, p2, Lru/cn/api/provider/Channel;->channelId:J

    .line 2001
    invoke-virtual {v0, v2, v3}, Lru/cn/api/allowed/AllowedChannels;->contains(J)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 2000
    :goto_0
    return v0

    .line 2001
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final synthetic lambda$updateChannelsIfExpire$1$TvContentProviderData(Lru/cn/api/iptv/replies/MediaLocation;)V
    .locals 4
    .param p1, "location"    # Lru/cn/api/iptv/replies/MediaLocation;

    .prologue
    .line 523
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->timezones:Ljava/util/Map;

    iget-wide v2, p1, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iput-object v0, p1, Lru/cn/api/iptv/replies/MediaLocation;->timezoneOffset:Ljava/lang/Long;

    return-void
.end method

.method final synthetic lambda$updateChannelsIfExpire$2$TvContentProviderData(Lru/cn/api/provider/Channel;)V
    .locals 4
    .param p1, "channel"    # Lru/cn/api/provider/Channel;

    .prologue
    .line 531
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->channelsMap:Landroid/support/v4/util/LongSparseArray;

    iget-wide v2, p1, Lru/cn/api/provider/Channel;->channelId:J

    invoke-virtual {v0, v2, v3, p1}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    return-void
.end method

.method final synthetic lambda$updateTitleMapping$13$TvContentProviderData(Ljava/lang/String;)Z
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 868
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->titleToIdMapping:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final synthetic lambda$updateTitleMapping$14$TvContentProviderData(Ljava/util/List;ILru/cn/api/tv/replies/ChannelInfo;)V
    .locals 6
    .param p1, "titles"    # Ljava/util/List;
    .param p2, "index"    # I
    .param p3, "channelInfo"    # Lru/cn/api/tv/replies/ChannelInfo;

    .prologue
    .line 882
    iget-wide v2, p3, Lru/cn/api/tv/replies/ChannelInfo;->channelId:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 883
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 884
    .local v0, "title":Ljava/lang/String;
    iget-object v1, p0, Lru/cn/api/provider/TvContentProviderData;->titleToIdMapping:Ljava/util/Map;

    iget-wide v2, p3, Lru/cn/api/tv/replies/ChannelInfo;->channelId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 886
    .end local v0    # "title":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method preventServerCacheOnce(J)V
    .locals 3
    .param p1, "contractorId"    # J

    .prologue
    .line 1902
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->ignoreServerCache:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1903
    return-void
.end method

.method public rubricatorIsValid()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1560
    iget-object v1, p0, Lru/cn/api/provider/TvContentProviderData;->rubricatorCache:Lru/cn/api/catalogue/replies/Rubricator;

    if-nez v1, :cond_1

    .line 1563
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lru/cn/api/provider/TvContentProviderData;->cacheUtils:Lru/cn/api/provider/CacheUtils;

    sget-object v2, Lru/cn/api/provider/CacheUtils$CacheName;->rubricator:Lru/cn/api/provider/CacheUtils$CacheName;

    invoke-virtual {v1, v2}, Lru/cn/api/provider/CacheUtils;->isCacheExpire(Lru/cn/api/provider/CacheUtils$CacheName;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setAge(I)V
    .locals 2
    .param p1, "age"    # I

    .prologue
    .line 178
    iget v0, p0, Lru/cn/api/provider/TvContentProviderData;->age:I

    if-eq v0, p1, :cond_0

    .line 179
    iput p1, p0, Lru/cn/api/provider/TvContentProviderData;->age:I

    .line 180
    iget-object v0, p0, Lru/cn/api/provider/TvContentProviderData;->cacheUtils:Lru/cn/api/provider/CacheUtils;

    sget-object v1, Lru/cn/api/provider/CacheUtils$CacheName;->channels:Lru/cn/api/provider/CacheUtils$CacheName;

    invoke-virtual {v0, v1}, Lru/cn/api/provider/CacheUtils;->deleteCacheExpireTime(Lru/cn/api/provider/CacheUtils$CacheName;)V

    .line 182
    :cond_0
    return-void
.end method

.method public updateUserPlaylist(Ljava/lang/String;Landroid/content/ContentValues;)Z
    .locals 2
    .param p1, "location"    # Ljava/lang/String;
    .param p2, "values"    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1842
    const-string v1, "title"

    invoke-virtual {p2, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1843
    .local v0, "title":Ljava/lang/String;
    iget-object v1, p0, Lru/cn/api/provider/TvContentProviderData;->context:Landroid/content/Context;

    invoke-static {v1, v0, p1}, Lru/cn/api/userdata/UserData;->updateUserPlaylist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1846
    iget-object v1, p0, Lru/cn/api/provider/TvContentProviderData;->userPlaylistsMap:Ljava/util/Map;

    if-nez v1, :cond_0

    .line 1847
    invoke-direct {p0}, Lru/cn/api/provider/TvContentProviderData;->updateUserPlaylists()V

    .line 1850
    :cond_0
    iget-object v1, p0, Lru/cn/api/provider/TvContentProviderData;->userPlaylistsMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1851
    const/4 v1, 0x1

    .line 1854
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
