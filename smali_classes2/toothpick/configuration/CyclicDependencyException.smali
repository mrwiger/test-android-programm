.class public Ltoothpick/configuration/CyclicDependencyException;
.super Ljava/lang/RuntimeException;
.source "CyclicDependencyException.java"


# static fields
.field private static final LINE_SEPARATOR:Ljava/lang/String;

.field private static final MARGIN_SIZE:I = 0x3


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ltoothpick/configuration/CyclicDependencyException;->LINE_SEPARATOR:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/RuntimeException;-><init>()V

    .line 11
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 15
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;ZZ)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "cause"    # Ljava/lang/Throwable;
    .param p3, "enableSuppression"    # Z
    .param p4, "writableStackTrace"    # Z

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3, p4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;ZZ)V

    .line 27
    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/Class;)V
    .locals 4
    .param p2, "startClass"    # Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class;",
            ">;",
            "Ljava/lang/Class;",
            ")V"
        }
    .end annotation

    .prologue
    .line 30
    .local p1, "path":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Class;>;"
    const-string v0, "Class %s creates a cycle:%n%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p1, p2}, Ltoothpick/configuration/CyclicDependencyException;->format(Ljava/util/List;Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ltoothpick/configuration/CyclicDependencyException;-><init>(Ljava/lang/String;)V

    .line 31
    return-void
.end method

.method private static addHorizontalLine(Ljava/lang/StringBuilder;II)V
    .locals 2
    .param p0, "builder"    # Ljava/lang/StringBuilder;
    .param p1, "middleWordPos"    # I
    .param p2, "loopLinePosition"    # I

    .prologue
    .line 64
    const/16 v0, 0x20

    invoke-static {v0, p1}, Ltoothpick/configuration/CyclicDependencyException;->repeat(CI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    const/16 v0, 0x3d

    sub-int v1, p2, p1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Ltoothpick/configuration/CyclicDependencyException;->repeat(CI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    sget-object v0, Ltoothpick/configuration/CyclicDependencyException;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    return-void
.end method

.method private static addLine(Ljava/lang/StringBuilder;Ljava/lang/String;II)V
    .locals 5
    .param p0, "builder"    # Ljava/lang/StringBuilder;
    .param p1, "content"    # Ljava/lang/String;
    .param p2, "middleWordPos"    # I
    .param p3, "loopLinePosition"    # I

    .prologue
    const/16 v4, 0x20

    .line 70
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int v0, p2, v2

    .line 71
    .local v0, "leftMarginSize":I
    sub-int v2, p3, v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    sub-int v1, v2, v3

    .line 72
    .local v1, "rightMarginSize":I
    invoke-static {v4, v0}, Ltoothpick/configuration/CyclicDependencyException;->repeat(CI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    invoke-static {v4, v1}, Ltoothpick/configuration/CyclicDependencyException;->repeat(CI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    const-string v2, "||"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    sget-object v2, Ltoothpick/configuration/CyclicDependencyException;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    return-void
.end method

.method private static addTopLines(Ljava/lang/StringBuilder;II)V
    .locals 1
    .param p0, "builder"    # Ljava/lang/StringBuilder;
    .param p1, "middleWordPos"    # I
    .param p2, "loopLinePosition"    # I

    .prologue
    .line 57
    sget-object v0, Ltoothpick/configuration/CyclicDependencyException;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    invoke-static {p0, p1, p2}, Ltoothpick/configuration/CyclicDependencyException;->addHorizontalLine(Ljava/lang/StringBuilder;II)V

    .line 59
    const-string v0, "||"

    invoke-static {p0, v0, p1, p2}, Ltoothpick/configuration/CyclicDependencyException;->addLine(Ljava/lang/StringBuilder;Ljava/lang/String;II)V

    .line 60
    const-string v0, "\\/"

    invoke-static {p0, v0, p1, p2}, Ltoothpick/configuration/CyclicDependencyException;->addLine(Ljava/lang/StringBuilder;Ljava/lang/String;II)V

    .line 61
    return-void
.end method

.method private static findLongestClassNameLength(Ljava/util/List;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 80
    .local p0, "path":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Class;>;"
    const/4 v2, 0x0

    .line 81
    .local v2, "max":I
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 82
    .local v0, "clazz":Ljava/lang/Class;
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    .line 83
    .local v1, "length":I
    if-le v1, v2, :cond_0

    .line 84
    move v2, v1

    goto :goto_0

    .line 87
    .end local v0    # "clazz":Ljava/lang/Class;
    .end local v1    # "length":I
    :cond_1
    return v2
.end method

.method private static format(Ljava/util/List;Ljava/lang/Class;)Ljava/lang/String;
    .locals 8
    .param p1, "startClass"    # Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class;",
            ">;",
            "Ljava/lang/Class;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 34
    .local p0, "path":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Class;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_0

    .line 35
    new-instance v6, Ljava/lang/IllegalArgumentException;

    invoke-direct {v6}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v6

    .line 38
    :cond_0
    invoke-interface {p0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v6

    const/4 v7, 0x0

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 39
    .local v1, "classPosition":I
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v6

    invoke-interface {p0, v1, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p0

    .line 41
    invoke-static {p0}, Ltoothpick/configuration/CyclicDependencyException;->findLongestClassNameLength(Ljava/util/List;)I

    move-result v4

    .line 42
    .local v4, "maxWordLength":I
    div-int/lit8 v6, v4, 0x2

    add-int/lit8 v5, v6, 0x3

    .line 43
    .local v5, "middleWordPos":I
    add-int/lit8 v3, v4, 0x6

    .line 44
    .local v3, "loopLinePosition":I
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-static {v0, v5, v3}, Ltoothpick/configuration/CyclicDependencyException;->addTopLines(Ljava/lang/StringBuilder;II)V

    .line 47
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;

    .line 48
    .local v2, "clazz":Ljava/lang/Class;
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7, v5, v3}, Ltoothpick/configuration/CyclicDependencyException;->addLine(Ljava/lang/StringBuilder;Ljava/lang/String;II)V

    .line 49
    const-string v7, "||"

    invoke-static {v0, v7, v5, v3}, Ltoothpick/configuration/CyclicDependencyException;->addLine(Ljava/lang/StringBuilder;Ljava/lang/String;II)V

    goto :goto_0

    .line 51
    .end local v2    # "clazz":Ljava/lang/Class;
    :cond_1
    invoke-static {v0, v5, v3}, Ltoothpick/configuration/CyclicDependencyException;->addHorizontalLine(Ljava/lang/StringBuilder;II)V

    .line 53
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method private static repeat(CI)Ljava/lang/String;
    .locals 2
    .param p0, "c"    # C
    .param p1, "n"    # I

    .prologue
    .line 91
    new-instance v0, Ljava/lang/String;

    new-array v1, p1, [C

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p0}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
