.class public Lcom/samsung/multiscreen/ApplicationInfo;
.super Ljava/lang/Object;
.source "ApplicationInfo.java"


# static fields
.field private static final PROPERTY_ID:Ljava/lang/String; = "id"

.field private static final PROPERTY_NAME:Ljava/lang/String; = "name"

.field private static final PROPERTY_STATE:Ljava/lang/String; = "running"

.field private static final PROPERTY_VERSION:Ljava/lang/String; = "version"


# instance fields
.field private final id:Ljava/lang/String;
    .annotation build Llombok/NonNull;
    .end annotation
.end field

.field private final name:Ljava/lang/String;
    .annotation build Llombok/NonNull;
    .end annotation
.end field

.field private final running:Z

.field private final version:Ljava/lang/String;
    .annotation build Llombok/NonNull;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
        .annotation build Llombok/NonNull;
        .end annotation
    .end param
    .param p2, "running"    # Z
    .param p3, "name"    # Ljava/lang/String;
        .annotation build Llombok/NonNull;
        .end annotation
    .end param
    .param p4, "version"    # Ljava/lang/String;
        .annotation build Llombok/NonNull;
        .end annotation
    .end param

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "id"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "name"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p4, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "version"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iput-object p1, p0, Lcom/samsung/multiscreen/ApplicationInfo;->id:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/samsung/multiscreen/ApplicationInfo;->running:Z

    iput-object p3, p0, Lcom/samsung/multiscreen/ApplicationInfo;->name:Ljava/lang/String;

    iput-object p4, p0, Lcom/samsung/multiscreen/ApplicationInfo;->version:Ljava/lang/String;

    return-void
.end method

.method static create(Ljava/util/Map;)Lcom/samsung/multiscreen/ApplicationInfo;
    .locals 6
    .param p0    # Ljava/util/Map;
        .annotation build Llombok/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/samsung/multiscreen/ApplicationInfo;"
        }
    .end annotation

    .prologue
    .line 77
    .local p0, "info":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-nez p0, :cond_0

    new-instance v4, Ljava/lang/NullPointerException;

    const-string v5, "info"

    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 78
    :cond_0
    const-string v4, "id"

    invoke-interface {p0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 79
    .local v0, "id":Ljava/lang/String;
    const-string v4, "running"

    invoke-interface {p0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 80
    .local v2, "running":Z
    const-string v4, "name"

    invoke-interface {p0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 81
    .local v1, "name":Ljava/lang/String;
    const-string v4, "version"

    invoke-interface {p0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 82
    .local v3, "version":Ljava/lang/String;
    new-instance v4, Lcom/samsung/multiscreen/ApplicationInfo;

    invoke-direct {v4, v0, v2, v1, v3}, Lcom/samsung/multiscreen/ApplicationInfo;-><init>(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v4
.end method


# virtual methods
.method protected canEqual(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 37
    instance-of v0, p1, Lcom/samsung/multiscreen/ApplicationInfo;

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 11
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 37
    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v7

    :cond_1
    instance-of v9, p1, Lcom/samsung/multiscreen/ApplicationInfo;

    if-nez v9, :cond_2

    move v7, v8

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/samsung/multiscreen/ApplicationInfo;

    .local v0, "other":Lcom/samsung/multiscreen/ApplicationInfo;
    invoke-virtual {v0, p0}, Lcom/samsung/multiscreen/ApplicationInfo;->canEqual(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    move v7, v8

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/samsung/multiscreen/ApplicationInfo;->getId()Ljava/lang/String;

    move-result-object v4

    .local v4, "this$id":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/samsung/multiscreen/ApplicationInfo;->getId()Ljava/lang/String;

    move-result-object v1

    .local v1, "other$id":Ljava/lang/String;
    if-nez v4, :cond_5

    if-eqz v1, :cond_6

    :cond_4
    move v7, v8

    goto :goto_0

    :cond_5
    invoke-virtual {v4, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    :cond_6
    invoke-virtual {p0}, Lcom/samsung/multiscreen/ApplicationInfo;->isRunning()Z

    move-result v9

    invoke-virtual {v0}, Lcom/samsung/multiscreen/ApplicationInfo;->isRunning()Z

    move-result v10

    if-eq v9, v10, :cond_7

    move v7, v8

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/samsung/multiscreen/ApplicationInfo;->getName()Ljava/lang/String;

    move-result-object v5

    .local v5, "this$name":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/samsung/multiscreen/ApplicationInfo;->getName()Ljava/lang/String;

    move-result-object v2

    .local v2, "other$name":Ljava/lang/String;
    if-nez v5, :cond_9

    if-eqz v2, :cond_a

    :cond_8
    move v7, v8

    goto :goto_0

    :cond_9
    invoke-virtual {v5, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    :cond_a
    invoke-virtual {p0}, Lcom/samsung/multiscreen/ApplicationInfo;->getVersion()Ljava/lang/String;

    move-result-object v6

    .local v6, "this$version":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/samsung/multiscreen/ApplicationInfo;->getVersion()Ljava/lang/String;

    move-result-object v3

    .local v3, "other$version":Ljava/lang/String;
    if-nez v6, :cond_b

    if-eqz v3, :cond_0

    :goto_1
    move v7, v8

    goto :goto_0

    :cond_b
    invoke-virtual {v6, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    goto :goto_1
.end method

.method public getId()Ljava/lang/String;
    .locals 1
    .annotation build Llombok/NonNull;
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/multiscreen/ApplicationInfo;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1
    .annotation build Llombok/NonNull;
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/multiscreen/ApplicationInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1
    .annotation build Llombok/NonNull;
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/multiscreen/ApplicationInfo;->version:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 37
    const/16 v3, 0x3b

    .local v3, "PRIME":I
    const/4 v4, 0x1

    .local v4, "result":I
    invoke-virtual {p0}, Lcom/samsung/multiscreen/ApplicationInfo;->getId()Ljava/lang/String;

    move-result-object v0

    .local v0, "$id":Ljava/lang/String;
    if-nez v0, :cond_0

    move v5, v6

    :goto_0
    add-int/lit8 v4, v5, 0x3b

    mul-int/lit8 v7, v4, 0x3b

    invoke-virtual {p0}, Lcom/samsung/multiscreen/ApplicationInfo;->isRunning()Z

    move-result v5

    if-eqz v5, :cond_1

    const/16 v5, 0x4f

    :goto_1
    add-int v4, v7, v5

    invoke-virtual {p0}, Lcom/samsung/multiscreen/ApplicationInfo;->getName()Ljava/lang/String;

    move-result-object v1

    .local v1, "$name":Ljava/lang/String;
    mul-int/lit8 v7, v4, 0x3b

    if-nez v1, :cond_2

    move v5, v6

    :goto_2
    add-int v4, v7, v5

    invoke-virtual {p0}, Lcom/samsung/multiscreen/ApplicationInfo;->getVersion()Ljava/lang/String;

    move-result-object v2

    .local v2, "$version":Ljava/lang/String;
    mul-int/lit8 v5, v4, 0x3b

    if-nez v2, :cond_3

    :goto_3
    add-int v4, v5, v6

    return v4

    .end local v1    # "$name":Ljava/lang/String;
    .end local v2    # "$version":Ljava/lang/String;
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v5

    goto :goto_0

    :cond_1
    const/16 v5, 0x61

    goto :goto_1

    .restart local v1    # "$name":Ljava/lang/String;
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v5

    goto :goto_2

    .restart local v2    # "$version":Ljava/lang/String;
    :cond_3
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v6

    goto :goto_3
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/samsung/multiscreen/ApplicationInfo;->running:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ApplicationInfo(id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/ApplicationInfo;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", running="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/ApplicationInfo;->isRunning()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/ApplicationInfo;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/ApplicationInfo;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
