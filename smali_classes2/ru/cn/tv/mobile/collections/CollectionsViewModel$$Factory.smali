.class public final Lru/cn/tv/mobile/collections/CollectionsViewModel$$Factory;
.super Ljava/lang/Object;
.source "CollectionsViewModel$$Factory.java"

# interfaces
.implements Ltoothpick/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ltoothpick/Factory",
        "<",
        "Lru/cn/tv/mobile/collections/CollectionsViewModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic createInstance(Ltoothpick/Scope;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lru/cn/tv/mobile/collections/CollectionsViewModel$$Factory;->createInstance(Ltoothpick/Scope;)Lru/cn/tv/mobile/collections/CollectionsViewModel;

    move-result-object v0

    return-object v0
.end method

.method public createInstance(Ltoothpick/Scope;)Lru/cn/tv/mobile/collections/CollectionsViewModel;
    .locals 3
    .param p1, "scope"    # Ltoothpick/Scope;

    .prologue
    .line 11
    invoke-virtual {p0, p1}, Lru/cn/tv/mobile/collections/CollectionsViewModel$$Factory;->getTargetScope(Ltoothpick/Scope;)Ltoothpick/Scope;

    move-result-object p1

    .line 12
    const-class v2, Lru/cn/domain/Collections;

    invoke-interface {p1, v2}, Ltoothpick/Scope;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/domain/Collections;

    .line 13
    .local v1, "param1":Lru/cn/domain/Collections;
    new-instance v0, Lru/cn/tv/mobile/collections/CollectionsViewModel;

    invoke-direct {v0, v1}, Lru/cn/tv/mobile/collections/CollectionsViewModel;-><init>(Lru/cn/domain/Collections;)V

    .line 14
    .local v0, "collectionsViewModel":Lru/cn/tv/mobile/collections/CollectionsViewModel;
    return-object v0
.end method

.method public getTargetScope(Ltoothpick/Scope;)Ltoothpick/Scope;
    .locals 0
    .param p1, "scope"    # Ltoothpick/Scope;

    .prologue
    .line 19
    return-object p1
.end method

.method public hasProvidesSingletonInScopeAnnotation()Z
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    return v0
.end method

.method public hasScopeAnnotation()Z
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    return v0
.end method
