.class Lru/cn/tv/playlists/UserPlaylistsViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "UserPlaylistsViewModel.java"


# instance fields
.field private final userPlaylists:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lru/cn/mvvm/RxLoader;)V
    .locals 5
    .param p1, "loader"    # Lru/cn/mvvm/RxLoader;

    .prologue
    .line 22
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 23
    new-instance v2, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v2}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v2, p0, Lru/cn/tv/playlists/UserPlaylistsViewModel;->userPlaylists:Landroid/arch/lifecycle/MutableLiveData;

    .line 25
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->playlistUri()Landroid/net/Uri;

    move-result-object v1

    .line 27
    .local v1, "playlistUri":Landroid/net/Uri;
    const-string v2, "contractor_id=0"

    .line 28
    invoke-virtual {p1, v1, v2}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v2

    .line 29
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    .line 30
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    iget-object v3, p0, Lru/cn/tv/playlists/UserPlaylistsViewModel;->userPlaylists:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v3}, Lru/cn/tv/playlists/UserPlaylistsViewModel$$Lambda$0;->get$Lambda(Landroid/arch/lifecycle/MutableLiveData;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    new-instance v4, Lru/cn/tv/playlists/UserPlaylistsViewModel$$Lambda$1;

    invoke-direct {v4, p0}, Lru/cn/tv/playlists/UserPlaylistsViewModel$$Lambda$1;-><init>(Lru/cn/tv/playlists/UserPlaylistsViewModel;)V

    .line 31
    invoke-virtual {v2, v3, v4}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 34
    .local v0, "disposable":Lio/reactivex/disposables/Disposable;
    invoke-virtual {p0, v0}, Lru/cn/tv/playlists/UserPlaylistsViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 35
    return-void
.end method


# virtual methods
.method final synthetic lambda$new$0$UserPlaylistsViewModel(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "throwable"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lru/cn/tv/playlists/UserPlaylistsViewModel;->userPlaylists:Landroid/arch/lifecycle/MutableLiveData;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method public userPlaylists()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lru/cn/tv/playlists/UserPlaylistsViewModel;->userPlaylists:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method
