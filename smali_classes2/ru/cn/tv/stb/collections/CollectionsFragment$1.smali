.class Lru/cn/tv/stb/collections/CollectionsFragment$1;
.super Ljava/lang/Object;
.source "CollectionsFragment.java"

# interfaces
.implements Lru/cn/view/CursorRecyclerViewAdapter$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/stb/collections/CollectionsFragment;->createCollectionView(Lru/cn/tv/stb/collections/CollectionInfo;)Lru/cn/tv/stb/collections/StbCollectionView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/stb/collections/CollectionsFragment;

.field final synthetic val$adapter:Lru/cn/tv/stb/collections/CollectionTelecastAdapter;

.field final synthetic val$rubric:Lru/cn/api/catalogue/replies/Rubric;


# direct methods
.method constructor <init>(Lru/cn/tv/stb/collections/CollectionsFragment;Lru/cn/tv/stb/collections/CollectionTelecastAdapter;Lru/cn/api/catalogue/replies/Rubric;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/stb/collections/CollectionsFragment;

    .prologue
    .line 146
    iput-object p1, p0, Lru/cn/tv/stb/collections/CollectionsFragment$1;->this$0:Lru/cn/tv/stb/collections/CollectionsFragment;

    iput-object p2, p0, Lru/cn/tv/stb/collections/CollectionsFragment$1;->val$adapter:Lru/cn/tv/stb/collections/CollectionTelecastAdapter;

    iput-object p3, p0, Lru/cn/tv/stb/collections/CollectionsFragment$1;->val$rubric:Lru/cn/api/catalogue/replies/Rubric;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/view/View;I)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 149
    iget-object v1, p0, Lru/cn/tv/stb/collections/CollectionsFragment$1;->val$adapter:Lru/cn/tv/stb/collections/CollectionTelecastAdapter;

    invoke-virtual {v1, p2}, Lru/cn/tv/stb/collections/CollectionTelecastAdapter;->getItem(I)Landroid/database/Cursor;

    move-result-object v0

    .line 150
    .local v0, "item":Landroid/database/Cursor;
    iget-object v1, p0, Lru/cn/tv/stb/collections/CollectionsFragment$1;->this$0:Lru/cn/tv/stb/collections/CollectionsFragment;

    invoke-static {v1}, Lru/cn/tv/stb/collections/CollectionsFragment;->access$000(Lru/cn/tv/stb/collections/CollectionsFragment;)Lru/cn/tv/stb/collections/CollectionsFragment$CollectionListener;

    move-result-object v1

    iget-object v2, p0, Lru/cn/tv/stb/collections/CollectionsFragment$1;->val$rubric:Lru/cn/api/catalogue/replies/Rubric;

    iget-wide v2, v2, Lru/cn/api/catalogue/replies/Rubric;->id:J

    invoke-interface {v1, v2, v3, v0}, Lru/cn/tv/stb/collections/CollectionsFragment$CollectionListener;->onClick(JLandroid/database/Cursor;)V

    .line 151
    return-void
.end method
