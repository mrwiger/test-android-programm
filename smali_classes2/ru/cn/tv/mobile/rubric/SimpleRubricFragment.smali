.class public Lru/cn/tv/mobile/rubric/SimpleRubricFragment;
.super Lru/cn/tv/mobile/rubric/RubricFragment;
.source "SimpleRubricFragment.java"


# instance fields
.field private currentTelecastId:J

.field protected list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lru/cn/tv/mobile/rubric/RubricFragment;-><init>()V

    .line 27
    return-void
.end method


# virtual methods
.method public checkTelecast(J)V
    .locals 5
    .param p1, "telecastId"    # J

    .prologue
    .line 71
    iget-wide v0, p0, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->currentTelecastId:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    .line 72
    iput-wide p1, p0, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->currentTelecastId:J

    .line 73
    iget-wide v0, p0, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->currentTelecastId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    invoke-virtual {v0, p1, p2}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->checkItem(J)V

    .line 77
    :cond_0
    return-void
.end method

.method protected layout()I
    .locals 1

    .prologue
    .line 22
    const v0, 0x7f0c00b1

    return v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 48
    iget-object v0, p0, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    if-eqz v0, :cond_0

    .line 49
    invoke-virtual {p0}, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "list"

    iget-object v2, p0, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    invoke-virtual {v0, p1, v1, v2}, Landroid/support/v4/app/FragmentManager;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 51
    :cond_0
    invoke-super {p0, p1}, Lru/cn/tv/mobile/rubric/RubricFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 52
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    invoke-super {p0, p1, p2}, Lru/cn/tv/mobile/rubric/RubricFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 41
    if-eqz p2, :cond_0

    .line 42
    invoke-virtual {p0}, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "list"

    invoke-virtual {v0, p2, v1}, Landroid/support/v4/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    iput-object v0, p0, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    .line 44
    :cond_0
    return-void
.end method

.method protected rubricInfoLoaded(Lru/cn/api/catalogue/replies/Rubric;)V
    .locals 6
    .param p1, "rubric"    # Lru/cn/api/catalogue/replies/Rubric;

    .prologue
    .line 56
    iget-wide v2, p1, Lru/cn/api/catalogue/replies/Rubric;->id:J

    const/4 v1, 0x0

    invoke-static {v2, v3, v1}, Lru/cn/api/provider/TvContentProviderContract;->rubricItemsUri(JLandroid/support/v4/util/LongSparseArray;)Landroid/net/Uri;

    move-result-object v0

    .line 57
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    if-nez v1, :cond_0

    .line 58
    invoke-static {v0}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->newInstance(Landroid/net/Uri;)Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    move-result-object v1

    iput-object v1, p0, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    .line 59
    invoke-virtual {p0}, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f090085

    iget-object v3, p0, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    .line 60
    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 63
    :cond_0
    iget-object v1, p0, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    iget-object v2, p0, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->listener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    invoke-virtual {v1, v2}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->setOnSelectedListener(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)V

    .line 65
    iget-wide v2, p0, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->currentTelecastId:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 66
    iget-object v1, p0, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    iget-wide v2, p0, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->currentTelecastId:J

    invoke-virtual {v1, v2, v3}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->checkItem(J)V

    .line 68
    :cond_1
    return-void
.end method

.method public bridge synthetic setListener(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)Lru/cn/tv/mobile/rubric/RubricFragment;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->setListener(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)Lru/cn/tv/mobile/rubric/SimpleRubricFragment;

    move-result-object v0

    return-object v0
.end method

.method public setListener(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)Lru/cn/tv/mobile/rubric/SimpleRubricFragment;
    .locals 2
    .param p1, "l"    # Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    .prologue
    .line 31
    invoke-super {p0, p1}, Lru/cn/tv/mobile/rubric/RubricFragment;->setListener(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)Lru/cn/tv/mobile/rubric/RubricFragment;

    .line 32
    iget-object v0, p0, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    iget-object v1, p0, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->listener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->setOnSelectedListener(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)V

    .line 35
    :cond_0
    return-object p0
.end method
