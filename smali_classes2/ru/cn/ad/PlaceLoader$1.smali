.class Lru/cn/ad/PlaceLoader$1;
.super Landroid/os/AsyncTask;
.source "PlaceLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/ad/PlaceLoader;->load()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lru/cn/api/money_miner/replies/AdPlace;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/ad/PlaceLoader;


# direct methods
.method constructor <init>(Lru/cn/ad/PlaceLoader;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/ad/PlaceLoader;

    .prologue
    .line 33
    iput-object p1, p0, Lru/cn/ad/PlaceLoader$1;->this$0:Lru/cn/ad/PlaceLoader;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 33
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lru/cn/ad/PlaceLoader$1;->doInBackground([Ljava/lang/Void;)Lru/cn/api/money_miner/replies/AdPlace;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Lru/cn/api/money_miner/replies/AdPlace;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 36
    iget-object v0, p0, Lru/cn/ad/PlaceLoader$1;->this$0:Lru/cn/ad/PlaceLoader;

    invoke-static {v0}, Lru/cn/ad/PlaceLoader;->access$000(Lru/cn/ad/PlaceLoader;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lru/cn/ad/PlaceLoader$1;->this$0:Lru/cn/ad/PlaceLoader;

    invoke-static {v1}, Lru/cn/ad/PlaceLoader;->access$100(Lru/cn/ad/PlaceLoader;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lru/cn/ad/AdsManager;->getPlace(Landroid/content/Context;Ljava/lang/String;)Lru/cn/api/money_miner/replies/AdPlace;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 33
    check-cast p1, Lru/cn/api/money_miner/replies/AdPlace;

    invoke-virtual {p0, p1}, Lru/cn/ad/PlaceLoader$1;->onPostExecute(Lru/cn/api/money_miner/replies/AdPlace;)V

    return-void
.end method

.method protected onPostExecute(Lru/cn/api/money_miner/replies/AdPlace;)V
    .locals 2
    .param p1, "adPlace"    # Lru/cn/api/money_miner/replies/AdPlace;

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-virtual {p0}, Lru/cn/ad/PlaceLoader$1;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 45
    :cond_1
    if-eqz p1, :cond_2

    iget-object v0, p1, Lru/cn/api/money_miner/replies/AdPlace;->networks:Ljava/util/List;

    if-nez v0, :cond_3

    .line 46
    :cond_2
    iget-object v0, p0, Lru/cn/ad/PlaceLoader$1;->this$0:Lru/cn/ad/PlaceLoader;

    invoke-static {v0, v1}, Lru/cn/ad/PlaceLoader;->access$202(Lru/cn/ad/PlaceLoader;Z)Z

    .line 47
    iget-object v0, p0, Lru/cn/ad/PlaceLoader$1;->this$0:Lru/cn/ad/PlaceLoader;

    invoke-static {v0}, Lru/cn/ad/PlaceLoader;->access$300(Lru/cn/ad/PlaceLoader;)Lru/cn/ad/AdapterLoader$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lru/cn/ad/PlaceLoader$1;->this$0:Lru/cn/ad/PlaceLoader;

    invoke-static {v0}, Lru/cn/ad/PlaceLoader;->access$300(Lru/cn/ad/PlaceLoader;)Lru/cn/ad/AdapterLoader$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdapterLoader$Listener;->onError()V

    goto :goto_0

    .line 52
    :cond_3
    iget-object v0, p0, Lru/cn/ad/PlaceLoader$1;->this$0:Lru/cn/ad/PlaceLoader;

    invoke-static {v0, p1}, Lru/cn/ad/PlaceLoader;->access$400(Lru/cn/ad/PlaceLoader;Lru/cn/api/money_miner/replies/AdPlace;)V

    .line 53
    iget-object v0, p0, Lru/cn/ad/PlaceLoader$1;->this$0:Lru/cn/ad/PlaceLoader;

    invoke-static {v0, v1}, Lru/cn/ad/PlaceLoader;->access$202(Lru/cn/ad/PlaceLoader;Z)Z

    goto :goto_0
.end method
