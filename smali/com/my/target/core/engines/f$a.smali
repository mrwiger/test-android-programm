.class final Lcom/my/target/core/engines/f$a;
.super Ljava/lang/Object;
.source "InterstitialAdPromoEngine.java"

# interfaces
.implements Lcom/my/target/core/presenters/f$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/core/engines/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic l:Lcom/my/target/core/engines/f;


# direct methods
.method private constructor <init>(Lcom/my/target/core/engines/f;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/my/target/core/engines/f$a;->l:Lcom/my/target/core/engines/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/my/target/core/engines/f;B)V
    .locals 0

    .prologue
    .line 190
    invoke-direct {p0, p1}, Lcom/my/target/core/engines/f$a;-><init>(Lcom/my/target/core/engines/f;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/my/target/core/models/banners/e;)V
    .locals 3

    .prologue
    .line 223
    const/4 v0, 0x0

    .line 224
    iget-object v1, p0, Lcom/my/target/core/engines/f$a;->l:Lcom/my/target/core/engines/f;

    invoke-static {v1}, Lcom/my/target/core/engines/f;->a(Lcom/my/target/core/engines/f;)Ljava/lang/ref/WeakReference;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 226
    iget-object v0, p0, Lcom/my/target/core/engines/f$a;->l:Lcom/my/target/core/engines/f;

    invoke-static {v0}, Lcom/my/target/core/engines/f;->a(Lcom/my/target/core/engines/f;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/presenters/f;

    .line 228
    :cond_0
    if-nez v0, :cond_2

    .line 247
    :cond_1
    :goto_0
    return-void

    .line 232
    :cond_2
    invoke-virtual {v0}, Lcom/my/target/core/presenters/f;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 234
    invoke-static {}, Lcom/my/target/ce;->bw()Lcom/my/target/ce;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/my/target/ce;->a(Lcom/my/target/ah;Landroid/content/Context;)V

    .line 235
    iget-object v1, p0, Lcom/my/target/core/engines/f$a;->l:Lcom/my/target/core/engines/f;

    invoke-static {v1}, Lcom/my/target/core/engines/f;->c(Lcom/my/target/core/engines/f;)Lcom/my/target/core/models/banners/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/core/models/banners/h;->getStatHolder()Lcom/my/target/ar;

    move-result-object v1

    const-string v2, "click"

    invoke-virtual {v1, v2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 237
    iget-object v0, p0, Lcom/my/target/core/engines/f$a;->l:Lcom/my/target/core/engines/f;

    iget-object v0, v0, Lcom/my/target/core/engines/f;->a:Lcom/my/target/ads/InterstitialAd;

    invoke-virtual {v0}, Lcom/my/target/ads/InterstitialAd;->getListener()Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;

    move-result-object v0

    .line 238
    if-eqz v0, :cond_3

    .line 240
    iget-object v1, p0, Lcom/my/target/core/engines/f$a;->l:Lcom/my/target/core/engines/f;

    iget-object v1, v1, Lcom/my/target/core/engines/f;->a:Lcom/my/target/ads/InterstitialAd;

    invoke-interface {v0, v1}, Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;->onClick(Lcom/my/target/ads/InterstitialAd;)V

    .line 242
    :cond_3
    iget-object v0, p0, Lcom/my/target/core/engines/f$a;->l:Lcom/my/target/core/engines/f;

    invoke-static {v0}, Lcom/my/target/core/engines/f;->c(Lcom/my/target/core/engines/f;)Lcom/my/target/core/models/banners/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/core/models/banners/h;->getVideoBanner()Lcom/my/target/aj;

    move-result-object v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/my/target/core/engines/f$a;->l:Lcom/my/target/core/engines/f;

    invoke-static {v0}, Lcom/my/target/core/engines/f;->b(Lcom/my/target/core/engines/f;)Lcom/my/target/dv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/dv;->k()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    .line 243
    :goto_1
    if-eqz v0, :cond_1

    .line 245
    iget-object v0, p0, Lcom/my/target/core/engines/f$a;->l:Lcom/my/target/core/engines/f;

    invoke-virtual {v0}, Lcom/my/target/core/engines/f;->dismiss()V

    goto :goto_0

    .line 242
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/my/target/core/models/banners/h;)V
    .locals 3

    .prologue
    .line 195
    const/4 v0, 0x0

    .line 196
    iget-object v1, p0, Lcom/my/target/core/engines/f$a;->l:Lcom/my/target/core/engines/f;

    invoke-static {v1}, Lcom/my/target/core/engines/f;->a(Lcom/my/target/core/engines/f;)Ljava/lang/ref/WeakReference;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 198
    iget-object v0, p0, Lcom/my/target/core/engines/f$a;->l:Lcom/my/target/core/engines/f;

    invoke-static {v0}, Lcom/my/target/core/engines/f;->a(Lcom/my/target/core/engines/f;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/presenters/f;

    .line 200
    :cond_0
    if-nez v0, :cond_2

    .line 218
    :cond_1
    :goto_0
    return-void

    .line 204
    :cond_2
    invoke-virtual {v0}, Lcom/my/target/core/presenters/f;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 206
    invoke-static {}, Lcom/my/target/ce;->bw()Lcom/my/target/ce;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/my/target/ce;->a(Lcom/my/target/ah;Landroid/content/Context;)V

    .line 208
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getVideoBanner()Lcom/my/target/aj;

    move-result-object v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/my/target/core/engines/f$a;->l:Lcom/my/target/core/engines/f;

    invoke-static {v0}, Lcom/my/target/core/engines/f;->b(Lcom/my/target/core/engines/f;)Lcom/my/target/dv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/dv;->k()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    .line 209
    :goto_1
    iget-object v1, p0, Lcom/my/target/core/engines/f$a;->l:Lcom/my/target/core/engines/f;

    iget-object v1, v1, Lcom/my/target/core/engines/f;->a:Lcom/my/target/ads/InterstitialAd;

    invoke-virtual {v1}, Lcom/my/target/ads/InterstitialAd;->getListener()Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;

    move-result-object v1

    .line 210
    if-eqz v1, :cond_3

    .line 212
    iget-object v2, p0, Lcom/my/target/core/engines/f$a;->l:Lcom/my/target/core/engines/f;

    iget-object v2, v2, Lcom/my/target/core/engines/f;->a:Lcom/my/target/ads/InterstitialAd;

    invoke-interface {v1, v2}, Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;->onClick(Lcom/my/target/ads/InterstitialAd;)V

    .line 214
    :cond_3
    if-eqz v0, :cond_1

    .line 216
    iget-object v0, p0, Lcom/my/target/core/engines/f$a;->l:Lcom/my/target/core/engines/f;

    invoke-virtual {v0}, Lcom/my/target/core/engines/f;->dismiss()V

    goto :goto_0

    .line 208
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final bh()V
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/my/target/core/engines/f$a;->l:Lcom/my/target/core/engines/f;

    invoke-virtual {v0}, Lcom/my/target/core/engines/f;->dismiss()V

    .line 263
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, Lcom/my/target/core/engines/f$a;->l:Lcom/my/target/core/engines/f;

    iget-object v0, v0, Lcom/my/target/core/engines/f;->a:Lcom/my/target/ads/InterstitialAd;

    invoke-virtual {v0}, Lcom/my/target/ads/InterstitialAd;->getListener()Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;

    move-result-object v0

    .line 253
    if-eqz v0, :cond_0

    .line 255
    iget-object v1, p0, Lcom/my/target/core/engines/f$a;->l:Lcom/my/target/core/engines/f;

    iget-object v1, v1, Lcom/my/target/core/engines/f;->a:Lcom/my/target/ads/InterstitialAd;

    invoke-interface {v0, v1}, Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;->onVideoCompleted(Lcom/my/target/ads/InterstitialAd;)V

    .line 257
    :cond_0
    return-void
.end method
