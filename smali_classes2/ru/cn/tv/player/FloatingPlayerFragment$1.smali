.class Lru/cn/tv/player/FloatingPlayerFragment$1;
.super Ljava/lang/Object;
.source "FloatingPlayerFragment.java"

# interfaces
.implements Lru/cn/draggableview/DraggableView$DraggableViewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/player/FloatingPlayerFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field lastOffsetX:F

.field final synthetic this$0:Lru/cn/tv/player/FloatingPlayerFragment;


# direct methods
.method constructor <init>(Lru/cn/tv/player/FloatingPlayerFragment;)V
    .locals 1
    .param p1, "this$0"    # Lru/cn/tv/player/FloatingPlayerFragment;

    .prologue
    .line 56
    iput-object p1, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput v0, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->lastOffsetX:F

    return-void
.end method


# virtual methods
.method public closed()V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->access$000(Lru/cn/tv/player/FloatingPlayerFragment;)Lru/cn/tv/player/SimplePlayerFragmentEx;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->access$000(Lru/cn/tv/player/FloatingPlayerFragment;)Lru/cn/tv/player/SimplePlayerFragmentEx;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->stop()V

    .line 114
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->access$000(Lru/cn/tv/player/FloatingPlayerFragment;)Lru/cn/tv/player/SimplePlayerFragmentEx;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->onMinimize()V

    .line 115
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->access$400(Lru/cn/tv/player/FloatingPlayerFragment;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->access$400(Lru/cn/tv/player/FloatingPlayerFragment;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->hideDescription(I)V

    .line 119
    :cond_0
    return-void
.end method

.method public dragViewClicked()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 123
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->isMinimized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v0, v1}, Lru/cn/tv/player/FloatingPlayerFragment;->maximize(Z)V

    .line 127
    :cond_0
    return v1
.end method

.method public dragViewDoubleClicked()Z
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    return v0
.end method

.method public maximized()V
    .locals 2

    .prologue
    .line 94
    iget-object v1, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v1}, Lru/cn/tv/player/FloatingPlayerFragment;->access$000(Lru/cn/tv/player/FloatingPlayerFragment;)Lru/cn/tv/player/SimplePlayerFragmentEx;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/tv/player/SimplePlayerFragmentEx;->onMaximize()V

    .line 96
    iget-object v1, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v1}, Lru/cn/tv/player/FloatingPlayerFragment;->access$200(Lru/cn/tv/player/FloatingPlayerFragment;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v1}, Lru/cn/tv/player/FloatingPlayerFragment;->access$300(Lru/cn/tv/player/FloatingPlayerFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 97
    iget-object v1, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v1}, Lru/cn/tv/player/FloatingPlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lru/cn/tv/FullScreenActivity;

    .line 98
    .local v0, "f":Lru/cn/tv/FullScreenActivity;
    invoke-virtual {v0}, Lru/cn/tv/FullScreenActivity;->isFullScreen()Z

    move-result v1

    if-nez v1, :cond_0

    .line 99
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lru/cn/tv/FullScreenActivity;->fullScreen(Z)V

    .line 103
    .end local v0    # "f":Lru/cn/tv/FullScreenActivity;
    :cond_0
    iget-object v1, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v1}, Lru/cn/tv/player/FloatingPlayerFragment;->access$100(Lru/cn/tv/player/FloatingPlayerFragment;)Lru/cn/tv/player/FloatingPlayerFragment$Listener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 104
    iget-object v1, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v1}, Lru/cn/tv/player/FloatingPlayerFragment;->access$100(Lru/cn/tv/player/FloatingPlayerFragment;)Lru/cn/tv/player/FloatingPlayerFragment$Listener;

    move-result-object v1

    invoke-interface {v1}, Lru/cn/tv/player/FloatingPlayerFragment$Listener;->onMaximize()V

    .line 105
    :cond_1
    return-void
.end method

.method public minimized()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->access$000(Lru/cn/tv/player/FloatingPlayerFragment;)Lru/cn/tv/player/SimplePlayerFragmentEx;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->onMinimize()V

    .line 88
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->access$100(Lru/cn/tv/player/FloatingPlayerFragment;)Lru/cn/tv/player/FloatingPlayerFragment$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->access$100(Lru/cn/tv/player/FloatingPlayerFragment;)Lru/cn/tv/player/FloatingPlayerFragment$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/tv/player/FloatingPlayerFragment$Listener;->onMinimize()V

    .line 90
    :cond_0
    return-void
.end method

.method public offsetChanged(FF)V
    .locals 4
    .param p1, "offsetX"    # F
    .param p2, "offsetY"    # F

    .prologue
    const/4 v3, 0x0

    .line 62
    iget-object v1, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v1}, Lru/cn/tv/player/FloatingPlayerFragment;->access$000(Lru/cn/tv/player/FloatingPlayerFragment;)Lru/cn/tv/player/SimplePlayerFragmentEx;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getView()Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    .line 82
    :goto_0
    return-void

    .line 67
    :cond_0
    const/4 v0, 0x0

    .line 68
    .local v0, "requestedVisibility":I
    cmpl-float v1, p1, v3

    if-eqz v1, :cond_1

    .line 69
    const/16 v0, 0x8

    .line 72
    :cond_1
    iget-object v1, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v1}, Lru/cn/tv/player/FloatingPlayerFragment;->access$000(Lru/cn/tv/player/FloatingPlayerFragment;)Lru/cn/tv/player/SimplePlayerFragmentEx;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 73
    iget-object v1, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v1}, Lru/cn/tv/player/FloatingPlayerFragment;->access$000(Lru/cn/tv/player/FloatingPlayerFragment;)Lru/cn/tv/player/SimplePlayerFragmentEx;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 77
    :cond_2
    iget v1, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->lastOffsetX:F

    cmpl-float v1, v1, p1

    if-eqz v1, :cond_3

    .line 78
    iget-object v1, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v1}, Lru/cn/tv/player/FloatingPlayerFragment;->access$000(Lru/cn/tv/player/FloatingPlayerFragment;)Lru/cn/tv/player/SimplePlayerFragmentEx;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v2, p1

    invoke-virtual {v1, v2}, Lru/cn/tv/player/SimplePlayerFragmentEx;->setVolume(F)V

    .line 81
    :cond_3
    iget-object v1, p0, Lru/cn/tv/player/FloatingPlayerFragment$1;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v1}, Lru/cn/tv/player/FloatingPlayerFragment;->access$000(Lru/cn/tv/player/FloatingPlayerFragment;)Lru/cn/tv/player/SimplePlayerFragmentEx;

    move-result-object v2

    cmpl-float v1, p2, v3

    if-lez v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v2, v1}, Lru/cn/tv/player/SimplePlayerFragmentEx;->mustHideControls(Z)V

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method
