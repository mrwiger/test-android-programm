.class public Lcom/yandex/metrica/impl/bh;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/bh$a;
    }
.end annotation


# direct methods
.method public static a([B)Lcom/yandex/metrica/impl/bh$a;
    .locals 5

    .prologue
    .line 420
    new-instance v1, Lcom/yandex/metrica/impl/bh$a;

    invoke-direct {v1}, Lcom/yandex/metrica/impl/bh$a;-><init>()V

    .line 423
    :try_start_0
    new-instance v2, Lcom/yandex/metrica/impl/utils/g$a;

    new-instance v0, Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-direct {v0, p0, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-direct {v2, v0}, Lcom/yandex/metrica/impl/utils/g$a;-><init>(Ljava/lang/String;)V

    .line 426
    const-string v0, "device_id"

    invoke-static {v2, v0}, Lcom/yandex/metrica/impl/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/bh$a;->d(Ljava/lang/String;)V

    .line 429
    const-string v0, "uuid"

    invoke-static {v2, v0}, Lcom/yandex/metrica/impl/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/bh$a;->e(Ljava/lang/String;)V

    .line 1539
    const-string v0, "query_hosts"

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {v2, v0, v3}, Lcom/yandex/metrica/impl/utils/g$a;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 1540
    const-string v3, "list"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1541
    const-string v3, "list"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 1543
    const-string v3, "get_ad"

    invoke-static {v0, v3}, Lcom/yandex/metrica/impl/bh;->b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1544
    invoke-static {v3}, Lcom/yandex/metrica/impl/bh;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1545
    invoke-virtual {v1, v3}, Lcom/yandex/metrica/impl/bh$a;->a(Ljava/lang/String;)V

    .line 1548
    :cond_0
    const-string v3, "report"

    invoke-static {v0, v3}, Lcom/yandex/metrica/impl/bh;->c(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 1549
    invoke-static {v3}, Lcom/yandex/metrica/impl/bh;->a(Ljava/util/List;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1550
    invoke-virtual {v1, v3}, Lcom/yandex/metrica/impl/bh$a;->b(Ljava/util/List;)V

    .line 1553
    :cond_1
    const-string v3, "report_ad"

    invoke-static {v0, v3}, Lcom/yandex/metrica/impl/bh;->b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1554
    invoke-static {v3}, Lcom/yandex/metrica/impl/bh;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1555
    invoke-virtual {v1, v3}, Lcom/yandex/metrica/impl/bh$a;->b(Ljava/lang/String;)V

    .line 1558
    :cond_2
    const-string v3, "ssl_pinning"

    invoke-static {v0, v3}, Lcom/yandex/metrica/impl/bh;->b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1559
    invoke-static {v3}, Lcom/yandex/metrica/impl/bh;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1560
    invoke-virtual {v1, v3}, Lcom/yandex/metrica/impl/bh$a;->c(Ljava/lang/String;)V

    .line 1563
    :cond_3
    const-string v3, "bind_id"

    invoke-static {v0, v3}, Lcom/yandex/metrica/impl/bh;->b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1564
    invoke-static {v3}, Lcom/yandex/metrica/impl/bh;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1565
    invoke-virtual {v1, v3}, Lcom/yandex/metrica/impl/bh$a;->g(Ljava/lang/String;)V

    .line 1568
    :cond_4
    const-string v3, "location"

    invoke-static {v0, v3}, Lcom/yandex/metrica/impl/bh;->c(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 1569
    invoke-static {v3}, Lcom/yandex/metrica/impl/bh;->a(Ljava/util/List;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1570
    invoke-virtual {v1, v3}, Lcom/yandex/metrica/impl/bh$a;->c(Ljava/util/List;)V

    .line 1573
    :cond_5
    const-string v3, "startup"

    invoke-static {v0, v3}, Lcom/yandex/metrica/impl/bh;->c(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 1574
    invoke-static {v0}, Lcom/yandex/metrica/impl/bh;->a(Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1575
    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/bh$a;->a(Ljava/util/List;)V

    .line 1589
    :cond_6
    const-string v0, "distribution_customization"

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {v2, v0, v3}, Lcom/yandex/metrica/impl/utils/g$a;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 1591
    const-string v3, "clids"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 1592
    if-eqz v0, :cond_7

    .line 1593
    invoke-static {v1, v0}, Lcom/yandex/metrica/impl/bh;->a(Lcom/yandex/metrica/impl/bh$a;Lorg/json/JSONObject;)V

    .line 2456
    :cond_7
    const-string v0, "features"

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {v2, v0, v3}, Lcom/yandex/metrica/impl/utils/g$a;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 2457
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/yandex/metrica/impl/bh$a;->b(Z)V

    .line 2458
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/yandex/metrica/impl/bh$a;->c(Z)V

    .line 2459
    const-string v3, "list"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 2460
    const-string v3, "list"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 2462
    const-string v3, "easy_collecting"

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lcom/yandex/metrica/impl/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/yandex/metrica/impl/bh$a;->b(Z)V

    .line 2465
    const-string v3, "package_info"

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lcom/yandex/metrica/impl/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/yandex/metrica/impl/bh$a;->c(Z)V

    .line 2468
    const-string v3, "socket"

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lcom/yandex/metrica/impl/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/yandex/metrica/impl/bh$a;->f(Z)V

    .line 2470
    const-string v3, "permissions_collecting"

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lcom/yandex/metrica/impl/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/yandex/metrica/impl/bh$a;->g(Z)V

    .line 2473
    const-string v3, "features_collecting"

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lcom/yandex/metrica/impl/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/yandex/metrica/impl/bh$a;->h(Z)V

    .line 2476
    const-string v3, "foreground_location_collection"

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lcom/yandex/metrica/impl/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/yandex/metrica/impl/bh$a;->d(Z)V

    .line 2479
    const-string v3, "background_location_collection"

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lcom/yandex/metrica/impl/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/yandex/metrica/impl/bh$a;->e(Z)V

    .line 2482
    const-string v3, "telephony_restricted_to_location_tracking"

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lcom/yandex/metrica/impl/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/bh$a;->a(Z)V

    .line 434
    :cond_8
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/bh;->a(Lcom/yandex/metrica/impl/bh$a;Lcom/yandex/metrica/impl/utils/g$a;)V

    .line 435
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bh$a;->r()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 436
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/bh;->b(Lcom/yandex/metrica/impl/bh$a;Lcom/yandex/metrica/impl/utils/g$a;)V

    .line 439
    :cond_9
    new-instance v0, Lcom/yandex/metrica/impl/bh$a$b;

    const-string v3, "foreground_location_collection"

    .line 440
    invoke-virtual {v2, v3}, Lcom/yandex/metrica/impl/utils/g$a;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/yandex/metrica/impl/bh$a$b;-><init>(Lorg/json/JSONObject;)V

    .line 439
    invoke-static {v1, v0}, Lcom/yandex/metrica/impl/bh$a;->a(Lcom/yandex/metrica/impl/bh$a;Lcom/yandex/metrica/impl/bh$a$b;)Lcom/yandex/metrica/impl/bh$a$b;

    .line 441
    new-instance v0, Lcom/yandex/metrica/impl/bh$a$a;

    const-string v3, "background_location_collection"

    invoke-virtual {v2, v3}, Lcom/yandex/metrica/impl/utils/g$a;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/yandex/metrica/impl/bh$a$a;-><init>(Lorg/json/JSONObject;)V

    invoke-static {v1, v0}, Lcom/yandex/metrica/impl/bh$a;->a(Lcom/yandex/metrica/impl/bh$a;Lcom/yandex/metrica/impl/bh$a$a;)Lcom/yandex/metrica/impl/bh$a$a;

    .line 2613
    const-string v0, "time"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2614
    if-eqz v0, :cond_a

    .line 2616
    :try_start_1
    const-string v2, "max_valid_difference_seconds"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2617
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/bh$a;->a(Ljava/lang/Long;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 451
    :cond_a
    :goto_0
    sget-object v0, Lcom/yandex/metrica/impl/bh$a$c;->b:Lcom/yandex/metrica/impl/bh$a$c;

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/bh$a;->a(Lcom/yandex/metrica/impl/bh$a$c;)V

    move-object v0, v1

    .line 452
    :goto_1
    return-object v0

    .line 447
    :catch_0
    move-exception v0

    new-instance v0, Lcom/yandex/metrica/impl/bh$a;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/bh$a;-><init>()V

    .line 448
    sget-object v1, Lcom/yandex/metrica/impl/bh$a$c;->a:Lcom/yandex/metrica/impl/bh$a$c;

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/bh$a;->a(Lcom/yandex/metrica/impl/bh$a$c;)V

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public static a(Ljava/util/Map;)Ljava/lang/Long;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)",
            "Ljava/lang/Long;"
        }
    .end annotation

    .prologue
    .line 625
    const/4 v1, 0x0

    .line 627
    invoke-static {p0}, Lcom/yandex/metrica/impl/bl;->a(Ljava/util/Map;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 628
    const-string v0, "Date"

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 630
    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->a(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 632
    const/4 v2, 0x0

    :try_start_0
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 633
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "E, d MMM yyyy HH:mm:ss z"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 635
    invoke-virtual {v2, v0}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 641
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method private static a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 393
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "value"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 395
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, ""

    goto :goto_0
.end method

.method private static a(Lcom/yandex/metrica/impl/bh$a;Lcom/yandex/metrica/impl/utils/g$a;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 497
    const-string v0, "browsers"

    invoke-virtual {p1, v0}, Lcom/yandex/metrica/impl/utils/g$a;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 498
    if-eqz v0, :cond_2

    .line 499
    const-string v1, "list"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 500
    if-eqz v1, :cond_2

    .line 501
    new-instance v2, Lcom/yandex/metrica/impl/ob/fw;

    invoke-direct {v2}, Lcom/yandex/metrica/impl/ob/fw;-><init>()V

    .line 502
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 503
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 504
    const-string v4, "package_id"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 505
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 506
    const-string v5, "min_interval_seconds"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    .line 507
    invoke-virtual {v2, v4, v3}, Lcom/yandex/metrica/impl/ob/fw;->a(Ljava/lang/String;I)V

    .line 502
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 510
    :cond_1
    invoke-virtual {p0, v2}, Lcom/yandex/metrica/impl/bh$a;->a(Lcom/yandex/metrica/impl/ob/fw;)V

    .line 513
    :cond_2
    return-void
.end method

.method private static a(Lcom/yandex/metrica/impl/bh$a;Lorg/json/JSONObject;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 598
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 600
    invoke-virtual {p1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v2

    .line 601
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 602
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 603
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 605
    if-eqz v3, :cond_0

    const-string v4, "value"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 606
    const-string v4, "value"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 609
    :cond_1
    invoke-static {v1}, Lcom/yandex/metrica/impl/utils/o;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bh$a;->f(Ljava/lang/String;)V

    .line 610
    return-void
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 581
    invoke-static {p0}, Lcom/yandex/metrica/impl/bj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 585
    invoke-static {p0}, Lcom/yandex/metrica/impl/bl;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lorg/json/JSONObject;Ljava/lang/String;Z)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 488
    const/4 v0, 0x0

    .line 489
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 490
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 491
    const-string v1, "enabled"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 493
    :cond_0
    return v0
.end method

.method private static b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 401
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "urls"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 404
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, ""

    goto :goto_0
.end method

.method private static b(Lcom/yandex/metrica/impl/bh$a;Lcom/yandex/metrica/impl/utils/g$a;)V
    .locals 8

    .prologue
    .line 516
    const-string v0, "socket"

    invoke-virtual {p1, v0}, Lcom/yandex/metrica/impl/utils/g$a;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 517
    if-eqz v0, :cond_2

    .line 518
    const-string v1, "seconds_to_live"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 519
    const-string v1, "token"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 520
    const-string v4, "ports"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 521
    const-wide/16 v6, 0x0

    cmp-long v0, v2, v6

    if-lez v0, :cond_2

    invoke-static {v1}, Lcom/yandex/metrica/impl/bh;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 522
    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 523
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v0, v6, :cond_1

    .line 524
    invoke-virtual {v4, v0}, Lorg/json/JSONArray;->optInt(I)I

    move-result v6

    .line 525
    if-eqz v6, :cond_0

    .line 526
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 523
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 529
    :cond_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 530
    new-instance v0, Lcom/yandex/metrica/impl/ob/fv;

    invoke-direct {v0, v2, v3, v1, v5}, Lcom/yandex/metrica/impl/ob/fv;-><init>(JLjava/lang/String;Ljava/util/List;)V

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bh$a;->a(Lcom/yandex/metrica/impl/ob/fv;)V

    .line 534
    :cond_2
    return-void
.end method

.method private static c(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 409
    .line 411
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "urls"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 1262
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 1263
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1264
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 1265
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1264
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_0
    move-object v0, v1

    .line 415
    :cond_1
    return-object v0
.end method
