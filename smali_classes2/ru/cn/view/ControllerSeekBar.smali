.class public Lru/cn/view/ControllerSeekBar;
.super Landroid/support/v7/widget/AppCompatSeekBar;
.source "ControllerSeekBar.java"


# instance fields
.field private livePosition:I

.field private livePx:I

.field private final liveThumbHeight:I

.field private final liveThumbWidth:I

.field private final paint:Landroid/graphics/Paint;

.field private thumbDrawable:Landroid/graphics/drawable/Drawable;

.field private thumbHeight:I

.field private thumbWidth:I

.field private touchingInLiveRange:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lru/cn/view/ControllerSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    const v0, 0x101007b

    invoke-direct {p0, p1, p2, v0}, Lru/cn/view/ControllerSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v1, -0x1

    .line 39
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/AppCompatSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    iput v1, p0, Lru/cn/view/ControllerSeekBar;->livePosition:I

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/view/ControllerSeekBar;->touchingInLiveRange:Z

    .line 41
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lru/cn/view/ControllerSeekBar;->paint:Landroid/graphics/Paint;

    .line 42
    iget-object v0, p0, Lru/cn/view/ControllerSeekBar;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 43
    invoke-virtual {p0}, Lru/cn/view/ControllerSeekBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lru/cn/utils/Utils;->dpToPx(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lru/cn/view/ControllerSeekBar;->liveThumbWidth:I

    .line 44
    invoke-virtual {p0}, Lru/cn/view/ControllerSeekBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lru/cn/utils/Utils;->dpToPx(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lru/cn/view/ControllerSeekBar;->liveThumbHeight:I

    .line 45
    return-void
.end method

.method private isInThumbRange(I)Z
    .locals 6
    .param p1, "thumbX"    # I

    .prologue
    .line 96
    iget v4, p0, Lru/cn/view/ControllerSeekBar;->thumbWidth:I

    div-int/lit8 v4, v4, 0x2

    sub-int v2, p1, v4

    .line 97
    .local v2, "thumbLeft":I
    iget v4, p0, Lru/cn/view/ControllerSeekBar;->thumbWidth:I

    add-int v3, v2, v4

    .line 98
    .local v3, "thumbRight":I
    iget v4, p0, Lru/cn/view/ControllerSeekBar;->livePx:I

    iget v5, p0, Lru/cn/view/ControllerSeekBar;->liveThumbWidth:I

    div-int/lit8 v5, v5, 0x2

    sub-int v0, v4, v5

    .line 99
    .local v0, "liveLeft":I
    iget v4, p0, Lru/cn/view/ControllerSeekBar;->livePx:I

    iget v5, p0, Lru/cn/view/ControllerSeekBar;->liveThumbWidth:I

    div-int/lit8 v5, v5, 0x2

    add-int v1, v4, v5

    .line 102
    .local v1, "liveRight":I
    if-lt v1, v2, :cond_0

    if-gt v0, v3, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected declared-synchronized onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 82
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Landroid/support/v7/widget/AppCompatSeekBar;->onDraw(Landroid/graphics/Canvas;)V

    .line 84
    iget v0, p0, Lru/cn/view/ControllerSeekBar;->livePosition:I

    if-ltz v0, :cond_0

    iget-boolean v0, p0, Lru/cn/view/ControllerSeekBar;->touchingInLiveRange:Z

    if-nez v0, :cond_0

    .line 85
    invoke-virtual {p0}, Lru/cn/view/ControllerSeekBar;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lru/cn/view/ControllerSeekBar;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lru/cn/view/ControllerSeekBar;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lru/cn/view/ControllerSeekBar;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lru/cn/view/ControllerSeekBar;->getProgress()I

    move-result v2

    mul-int/2addr v1, v2

    invoke-virtual {p0}, Lru/cn/view/ControllerSeekBar;->getMax()I

    move-result v2

    div-int/2addr v1, v2

    add-int v6, v0, v1

    .line 86
    .local v6, "progressX":I
    invoke-direct {p0, v6}, Lru/cn/view/ControllerSeekBar;->isInThumbRange(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    invoke-virtual {p0}, Lru/cn/view/ControllerSeekBar;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lru/cn/view/ControllerSeekBar;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Lru/cn/view/ControllerSeekBar;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lru/cn/view/ControllerSeekBar;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int v7, v0, v1

    .line 88
    .local v7, "topY":I
    iget v0, p0, Lru/cn/view/ControllerSeekBar;->livePx:I

    iget v1, p0, Lru/cn/view/ControllerSeekBar;->liveThumbWidth:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    int-to-float v1, v0

    iget v0, p0, Lru/cn/view/ControllerSeekBar;->liveThumbHeight:I

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v7, v0

    int-to-float v2, v0

    iget v0, p0, Lru/cn/view/ControllerSeekBar;->livePx:I

    iget v3, p0, Lru/cn/view/ControllerSeekBar;->liveThumbWidth:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v0, v3

    int-to-float v3, v0

    iget v0, p0, Lru/cn/view/ControllerSeekBar;->liveThumbHeight:I

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v7

    int-to-float v4, v0

    iget-object v5, p0, Lru/cn/view/ControllerSeekBar;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    .end local v6    # "progressX":I
    .end local v7    # "topY":I
    :cond_0
    monitor-exit p0

    return-void

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 107
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-direct {p0, v0}, Lru/cn/view/ControllerSeekBar;->isInThumbRange(I)Z

    move-result v0

    iput-boolean v0, p0, Lru/cn/view/ControllerSeekBar;->touchingInLiveRange:Z

    .line 108
    iget v0, p0, Lru/cn/view/ControllerSeekBar;->livePosition:I

    if-ltz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v1, p0, Lru/cn/view/ControllerSeekBar;->livePx:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 109
    iget v0, p0, Lru/cn/view/ControllerSeekBar;->livePx:I

    invoke-virtual {p0}, Lru/cn/view/ControllerSeekBar;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 110
    invoke-super {p0, p1}, Landroid/support/v7/widget/AppCompatSeekBar;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 113
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/widget/AppCompatSeekBar;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setLivePosition(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 72
    iget v0, p0, Lru/cn/view/ControllerSeekBar;->livePosition:I

    if-ne p1, v0, :cond_0

    .line 78
    :goto_0
    return-void

    .line 75
    :cond_0
    iput p1, p0, Lru/cn/view/ControllerSeekBar;->livePosition:I

    .line 76
    invoke-virtual {p0}, Lru/cn/view/ControllerSeekBar;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lru/cn/view/ControllerSeekBar;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lru/cn/view/ControllerSeekBar;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lru/cn/view/ControllerSeekBar;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    mul-int/2addr v1, p1

    invoke-virtual {p0}, Lru/cn/view/ControllerSeekBar;->getMax()I

    move-result v2

    div-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lru/cn/view/ControllerSeekBar;->livePx:I

    .line 77
    invoke-virtual {p0}, Lru/cn/view/ControllerSeekBar;->invalidate()V

    goto :goto_0
.end method

.method public setThumb(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "thumb"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 49
    invoke-super {p0, p1}, Landroid/support/v7/widget/AppCompatSeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 51
    iput-object p1, p0, Lru/cn/view/ControllerSeekBar;->thumbDrawable:Landroid/graphics/drawable/Drawable;

    .line 53
    if-eqz p1, :cond_0

    .line 54
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iput v0, p0, Lru/cn/view/ControllerSeekBar;->thumbHeight:I

    .line 55
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iput v0, p0, Lru/cn/view/ControllerSeekBar;->thumbWidth:I

    .line 57
    :cond_0
    return-void
.end method

.method public setThumbVisible(Z)V
    .locals 8
    .param p1, "visible"    # Z

    .prologue
    const/4 v1, 0x0

    .line 60
    iget-object v2, p0, Lru/cn/view/ControllerSeekBar;->thumbDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_1

    .line 61
    iget-object v2, p0, Lru/cn/view/ControllerSeekBar;->thumbDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 63
    .local v0, "bounds":Landroid/graphics/Rect;
    iget-object v2, p0, Lru/cn/view/ControllerSeekBar;->thumbDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    new-instance v4, Landroid/graphics/Rect;

    iget v5, v0, Landroid/graphics/Rect;->left:I

    iget v6, v0, Landroid/graphics/Rect;->top:I

    if-eqz p1, :cond_2

    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v7, p0, Lru/cn/view/ControllerSeekBar;->thumbWidth:I

    add-int/2addr v2, v7

    :goto_0
    if-eqz p1, :cond_0

    iget v1, v0, Landroid/graphics/Rect;->top:I

    iget v7, p0, Lru/cn/view/ControllerSeekBar;->thumbHeight:I

    add-int/2addr v1, v7

    :cond_0
    invoke-direct {v4, v5, v6, v2, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 68
    .end local v0    # "bounds":Landroid/graphics/Rect;
    :cond_1
    invoke-virtual {p0, p1}, Lru/cn/view/ControllerSeekBar;->setEnabled(Z)V

    .line 69
    return-void

    .restart local v0    # "bounds":Landroid/graphics/Rect;
    :cond_2
    move v2, v1

    .line 63
    goto :goto_0
.end method
