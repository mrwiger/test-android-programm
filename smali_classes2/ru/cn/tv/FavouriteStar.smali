.class public Lru/cn/tv/FavouriteStar;
.super Landroid/support/v7/widget/AppCompatImageView;
.source "FavouriteStar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/Checkable;


# static fields
.field private static final CheckedStateSet:[I


# instance fields
.field private channelId:J

.field private isFavourite:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 65
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, Lru/cn/tv/FavouriteStar;->CheckedStateSet:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/AppCompatImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/tv/FavouriteStar;->isFavourite:Z

    .line 14
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lru/cn/tv/FavouriteStar;->channelId:J

    .line 18
    invoke-virtual {p0, p0}, Lru/cn/tv/FavouriteStar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 19
    return-void
.end method


# virtual methods
.method public favourite(JZ)V
    .locals 7
    .param p1, "id"    # J
    .param p3, "favourite"    # Z

    .prologue
    .line 22
    iput-wide p1, p0, Lru/cn/tv/FavouriteStar;->channelId:J

    .line 23
    invoke-virtual {p0, p3}, Lru/cn/tv/FavouriteStar;->setChecked(Z)V

    .line 25
    iget-wide v2, p0, Lru/cn/tv/FavouriteStar;->channelId:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 26
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lru/cn/tv/FavouriteStar;->setEnabled(Z)V

    .line 29
    if-eqz p3, :cond_0

    .line 30
    invoke-virtual {p0}, Lru/cn/tv/FavouriteStar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e008b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, "contentDescription":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0, v0}, Lru/cn/tv/FavouriteStar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 40
    .end local v0    # "contentDescription":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0}, Lru/cn/tv/FavouriteStar;->refreshDrawableState()V

    .line 41
    return-void

    .line 33
    :cond_0
    invoke-virtual {p0}, Lru/cn/tv/FavouriteStar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0026

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "contentDescription":Ljava/lang/String;
    goto :goto_0

    .line 38
    .end local v0    # "contentDescription":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lru/cn/tv/FavouriteStar;->setEnabled(Z)V

    goto :goto_1
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lru/cn/tv/FavouriteStar;->isFavourite:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lru/cn/tv/FavouriteStar;->setEnabled(Z)V

    .line 47
    new-instance v0, Lru/cn/tv/FavouriteWorker;

    iget-wide v2, p0, Lru/cn/tv/FavouriteStar;->channelId:J

    iget-boolean v1, p0, Lru/cn/tv/FavouriteStar;->isFavourite:Z

    invoke-virtual {p0}, Lru/cn/tv/FavouriteStar;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v2, v3, v1, v4}, Lru/cn/tv/FavouriteWorker;-><init>(JZLandroid/content/Context;)V

    invoke-virtual {v0}, Lru/cn/tv/FavouriteWorker;->toggleFavourite()V

    .line 48
    return-void
.end method

.method public onCreateDrawableState(I)[I
    .locals 2
    .param p1, "extraSpace"    # I

    .prologue
    .line 69
    add-int/lit8 v1, p1, 0x1

    invoke-super {p0, v1}, Landroid/support/v7/widget/AppCompatImageView;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 70
    .local v0, "drawableState":[I
    invoke-virtual {p0}, Lru/cn/tv/FavouriteStar;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    sget-object v1, Lru/cn/tv/FavouriteStar;->CheckedStateSet:[I

    invoke-static {v0, v1}, Lru/cn/tv/FavouriteStar;->mergeDrawableStates([I[I)[I

    .line 73
    :cond_0
    return-object v0
.end method

.method public setChecked(Z)V
    .locals 0
    .param p1, "checked"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lru/cn/tv/FavouriteStar;->isFavourite:Z

    .line 53
    return-void
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lru/cn/tv/FavouriteStar;->isFavourite:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lru/cn/tv/FavouriteStar;->setChecked(Z)V

    .line 63
    return-void

    .line 62
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
