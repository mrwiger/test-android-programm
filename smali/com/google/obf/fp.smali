.class public final Lcom/google/obf/fp;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/ex;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/fp$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/obf/ff;


# direct methods
.method public constructor <init>(Lcom/google/obf/ff;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/obf/fp;->a:Lcom/google/obf/ff;

    .line 3
    return-void
.end method


# virtual methods
.method public a(Lcom/google/obf/eg;Lcom/google/obf/gd;)Lcom/google/obf/ew;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/obf/eg;",
            "Lcom/google/obf/gd",
            "<TT;>;)",
            "Lcom/google/obf/ew",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 4
    invoke-virtual {p2}, Lcom/google/obf/gd;->b()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 5
    invoke-virtual {p2}, Lcom/google/obf/gd;->a()Ljava/lang/Class;

    move-result-object v1

    .line 6
    const-class v2, Ljava/util/Collection;

    invoke-virtual {v2, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 7
    const/4 v0, 0x0

    .line 12
    :goto_0
    return-object v0

    .line 8
    :cond_0
    invoke-static {v0, v1}, Lcom/google/obf/fe;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;)Ljava/lang/reflect/Type;

    move-result-object v1

    .line 9
    invoke-static {v1}, Lcom/google/obf/gd;->a(Ljava/lang/reflect/Type;)Lcom/google/obf/gd;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/obf/eg;->a(Lcom/google/obf/gd;)Lcom/google/obf/ew;

    move-result-object v2

    .line 10
    iget-object v0, p0, Lcom/google/obf/fp;->a:Lcom/google/obf/ff;

    invoke-virtual {v0, p2}, Lcom/google/obf/ff;->a(Lcom/google/obf/gd;)Lcom/google/obf/fk;

    move-result-object v3

    .line 11
    new-instance v0, Lcom/google/obf/fp$a;

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/obf/fp$a;-><init>(Lcom/google/obf/eg;Ljava/lang/reflect/Type;Lcom/google/obf/ew;Lcom/google/obf/fk;)V

    goto :goto_0
.end method
