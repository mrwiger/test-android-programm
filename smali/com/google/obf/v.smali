.class public abstract Lcom/google/obf/v;
.super Lcom/google/obf/x;
.source "IMASDK"


# instance fields
.field private final a:[Lcom/google/obf/u$a;

.field private b:[I

.field private c:[I

.field private d:Lcom/google/obf/u$a;

.field private e:I

.field private f:J


# direct methods
.method public varargs constructor <init>([Lcom/google/obf/u;)V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/obf/x;-><init>()V

    .line 2
    array-length v0, p1

    new-array v0, v0, [Lcom/google/obf/u$a;

    iput-object v0, p0, Lcom/google/obf/v;->a:[Lcom/google/obf/u$a;

    .line 3
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 4
    iget-object v1, p0, Lcom/google/obf/v;->a:[Lcom/google/obf/u$a;

    aget-object v2, p1, v0

    invoke-interface {v2}, Lcom/google/obf/u;->a()Lcom/google/obf/u$a;

    move-result-object v2

    aput-object v2, v1, v0

    .line 5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6
    :cond_0
    return-void
.end method

.method private a(Lcom/google/obf/u$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 89
    :try_start_0
    invoke-interface {p1}, Lcom/google/obf/u$a;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    return-void

    .line 91
    :catch_0
    move-exception v0

    .line 92
    new-instance v1, Lcom/google/obf/g;

    invoke-direct {v1, v0}, Lcom/google/obf/g;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private b(J)J
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/obf/v;->d:Lcom/google/obf/u$a;

    iget v1, p0, Lcom/google/obf/v;->e:I

    invoke-interface {v0, v1}, Lcom/google/obf/u$a;->b(I)J

    move-result-wide v0

    .line 85
    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 86
    invoke-virtual {p0, v0, v1}, Lcom/google/obf/v;->a(J)V

    move-wide p1, v0

    .line 88
    :cond_0
    return-wide p1
.end method


# virtual methods
.method protected final a(JLcom/google/obf/r;Lcom/google/obf/t;)I
    .locals 7

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/obf/v;->d:Lcom/google/obf/u$a;

    iget v1, p0, Lcom/google/obf/v;->e:I

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/google/obf/u$a;->a(IJLcom/google/obf/r;Lcom/google/obf/t;)I

    move-result v0

    return v0
.end method

.method protected a(IJZ)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 47
    invoke-virtual {p0, p2, p3}, Lcom/google/obf/v;->e(J)J

    move-result-wide v0

    .line 48
    iget-object v2, p0, Lcom/google/obf/v;->a:[Lcom/google/obf/u$a;

    iget-object v3, p0, Lcom/google/obf/v;->b:[I

    aget v3, v3, p1

    aget-object v2, v2, v3

    iput-object v2, p0, Lcom/google/obf/v;->d:Lcom/google/obf/u$a;

    .line 49
    iget-object v2, p0, Lcom/google/obf/v;->c:[I

    aget v2, v2, p1

    iput v2, p0, Lcom/google/obf/v;->e:I

    .line 50
    iget-object v2, p0, Lcom/google/obf/v;->d:Lcom/google/obf/u$a;

    iget v3, p0, Lcom/google/obf/v;->e:I

    invoke-interface {v2, v3, v0, v1}, Lcom/google/obf/u$a;->a(IJ)V

    .line 51
    invoke-virtual {p0, v0, v1}, Lcom/google/obf/v;->a(J)V

    .line 52
    return-void
.end method

.method protected abstract a(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation
.end method

.method protected abstract a(JJZ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation
.end method

.method protected abstract a(Lcom/google/obf/q;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/o$b;
        }
    .end annotation
.end method

.method protected final b(I)Lcom/google/obf/q;
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/obf/v;->a:[Lcom/google/obf/u$a;

    iget-object v1, p0, Lcom/google/obf/v;->b:[I

    aget v1, v1, p1

    aget-object v0, v0, v1

    .line 81
    iget-object v1, p0, Lcom/google/obf/v;->c:[I

    aget v1, v1, p1

    invoke-interface {v0, v1}, Lcom/google/obf/u$a;->a(I)Lcom/google/obf/q;

    move-result-object v0

    return-object v0
.end method

.method protected final b(JJ)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 57
    invoke-virtual {p0, p1, p2}, Lcom/google/obf/v;->e(J)J

    move-result-wide v0

    .line 58
    iget-object v2, p0, Lcom/google/obf/v;->d:Lcom/google/obf/u$a;

    iget v3, p0, Lcom/google/obf/v;->e:I

    invoke-interface {v2, v3, v0, v1}, Lcom/google/obf/u$a;->b(IJ)Z

    move-result v6

    .line 59
    invoke-direct {p0, v0, v1}, Lcom/google/obf/v;->b(J)J

    move-result-wide v2

    move-object v1, p0

    move-wide v4, p3

    .line 60
    invoke-virtual/range {v1 .. v6}, Lcom/google/obf/v;->a(JJZ)V

    .line 61
    return-void
.end method

.method protected final c(J)Z
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 7
    const/4 v3, 0x1

    .line 8
    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/v;->a:[Lcom/google/obf/u$a;

    array-length v4, v4

    if-ge v2, v4, :cond_0

    .line 9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/v;->a:[Lcom/google/obf/u$a;

    aget-object v4, v4, v2

    move-wide/from16 v0, p1

    invoke-interface {v4, v0, v1}, Lcom/google/obf/u$a;->a(J)Z

    move-result v4

    and-int/2addr v3, v4

    .line 10
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 11
    :cond_0
    if-nez v3, :cond_1

    .line 12
    const/4 v2, 0x0

    .line 46
    :goto_1
    return v2

    .line 13
    :cond_1
    const/4 v3, 0x0

    .line 14
    const/4 v2, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/v;->a:[Lcom/google/obf/u$a;

    array-length v4, v4

    if-ge v2, v4, :cond_2

    .line 15
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/v;->a:[Lcom/google/obf/u$a;

    aget-object v4, v4, v2

    invoke-interface {v4}, Lcom/google/obf/u$a;->c()I

    move-result v4

    add-int/2addr v3, v4

    .line 16
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 17
    :cond_2
    const-wide/16 v6, 0x0

    .line 18
    const/4 v4, 0x0

    .line 19
    new-array v8, v3, [I

    .line 20
    new-array v9, v3, [I

    .line 21
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/v;->a:[Lcom/google/obf/u$a;

    array-length v10, v2

    .line 22
    const/4 v2, 0x0

    move v3, v4

    move-wide v4, v6

    move v6, v2

    :goto_3
    if-ge v6, v10, :cond_7

    .line 23
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/v;->a:[Lcom/google/obf/u$a;

    aget-object v7, v2, v6

    .line 24
    invoke-interface {v7}, Lcom/google/obf/u$a;->c()I

    move-result v11

    .line 25
    const/4 v2, 0x0

    move/from16 v16, v2

    move v2, v3

    move/from16 v3, v16

    :goto_4
    if-ge v3, v11, :cond_6

    .line 26
    invoke-interface {v7, v3}, Lcom/google/obf/u$a;->a(I)Lcom/google/obf/q;

    move-result-object v12

    .line 27
    :try_start_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/google/obf/v;->a(Lcom/google/obf/q;)Z
    :try_end_0
    .catch Lcom/google/obf/o$b; {:try_start_0 .. :try_end_0} :catch_0

    move-result v13

    .line 31
    if-eqz v13, :cond_3

    .line 32
    aput v6, v8, v2

    .line 33
    aput v3, v9, v2

    .line 34
    add-int/lit8 v2, v2, 0x1

    .line 35
    const-wide/16 v14, -0x1

    cmp-long v13, v4, v14

    if-nez v13, :cond_4

    .line 41
    :cond_3
    :goto_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 29
    :catch_0
    move-exception v2

    .line 30
    new-instance v3, Lcom/google/obf/g;

    invoke-direct {v3, v2}, Lcom/google/obf/g;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 36
    :cond_4
    iget-wide v12, v12, Lcom/google/obf/q;->e:J

    .line 37
    const-wide/16 v14, -0x1

    cmp-long v14, v12, v14

    if-nez v14, :cond_5

    .line 38
    const-wide/16 v4, -0x1

    goto :goto_5

    .line 39
    :cond_5
    const-wide/16 v14, -0x2

    cmp-long v14, v12, v14

    if-eqz v14, :cond_3

    .line 40
    invoke-static {v4, v5, v12, v13}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    goto :goto_5

    .line 42
    :cond_6
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move v3, v2

    goto :goto_3

    .line 43
    :cond_7
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/google/obf/v;->f:J

    .line 44
    invoke-static {v8, v3}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/obf/v;->b:[I

    .line 45
    invoke-static {v9, v3}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/obf/v;->c:[I

    .line 46
    const/4 v2, 0x1

    goto/16 :goto_1
.end method

.method protected d(J)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p0, p1, p2}, Lcom/google/obf/v;->e(J)J

    move-result-wide v0

    .line 54
    iget-object v2, p0, Lcom/google/obf/v;->d:Lcom/google/obf/u$a;

    invoke-interface {v2, v0, v1}, Lcom/google/obf/u$a;->b(J)V

    .line 55
    invoke-direct {p0, v0, v1}, Lcom/google/obf/v;->b(J)J

    .line 56
    return-void
.end method

.method protected e(J)J
    .locals 1

    .prologue
    .line 82
    return-wide p1
.end method

.method protected g()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/obf/v;->d:Lcom/google/obf/u$a;

    iget v1, p0, Lcom/google/obf/v;->e:I

    invoke-interface {v0, v1}, Lcom/google/obf/u$a;->c(I)V

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/v;->d:Lcom/google/obf/u$a;

    .line 73
    return-void
.end method

.method protected q()J
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/obf/v;->d:Lcom/google/obf/u$a;

    invoke-interface {v0}, Lcom/google/obf/u$a;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method protected r()J
    .locals 2

    .prologue
    .line 63
    iget-wide v0, p0, Lcom/google/obf/v;->f:J

    return-wide v0
.end method

.method protected s()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/obf/v;->d:Lcom/google/obf/u$a;

    if-eqz v0, :cond_1

    .line 65
    iget-object v0, p0, Lcom/google/obf/v;->d:Lcom/google/obf/u$a;

    invoke-direct {p0, v0}, Lcom/google/obf/v;->a(Lcom/google/obf/u$a;)V

    .line 70
    :cond_0
    return-void

    .line 66
    :cond_1
    iget-object v0, p0, Lcom/google/obf/v;->a:[Lcom/google/obf/u$a;

    array-length v1, v0

    .line 67
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 68
    iget-object v2, p0, Lcom/google/obf/v;->a:[Lcom/google/obf/u$a;

    aget-object v2, v2, v0

    invoke-direct {p0, v2}, Lcom/google/obf/v;->a(Lcom/google/obf/u$a;)V

    .line 69
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected t()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/obf/v;->a:[Lcom/google/obf/u$a;

    array-length v1, v0

    .line 75
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 76
    iget-object v2, p0, Lcom/google/obf/v;->a:[Lcom/google/obf/u$a;

    aget-object v2, v2, v0

    invoke-interface {v2}, Lcom/google/obf/u$a;->e()V

    .line 77
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 78
    :cond_0
    return-void
.end method

.method protected final u()I
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/obf/v;->c:[I

    array-length v0, v0

    return v0
.end method
