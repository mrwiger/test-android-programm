.class public abstract Ltoothpick/registries/factory/AbstractFactoryRegistry;
.super Ljava/lang/Object;
.source "AbstractFactoryRegistry.java"

# interfaces
.implements Ltoothpick/registries/FactoryRegistry;


# instance fields
.field private childrenRegistries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltoothpick/registries/FactoryRegistry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ltoothpick/registries/factory/AbstractFactoryRegistry;->childrenRegistries:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addChildRegistry(Ltoothpick/registries/FactoryRegistry;)V
    .locals 1
    .param p1, "childRegistry"    # Ltoothpick/registries/FactoryRegistry;

    .prologue
    .line 20
    iget-object v0, p0, Ltoothpick/registries/factory/AbstractFactoryRegistry;->childrenRegistries:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 21
    return-void
.end method

.method protected getFactoryInChildrenRegistries(Ljava/lang/Class;)Ltoothpick/Factory;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ltoothpick/Factory",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 34
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    iget-object v2, p0, Ltoothpick/registries/factory/AbstractFactoryRegistry;->childrenRegistries:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltoothpick/registries/FactoryRegistry;

    .line 35
    .local v1, "registry":Ltoothpick/registries/FactoryRegistry;
    invoke-interface {v1, p1}, Ltoothpick/registries/FactoryRegistry;->getFactory(Ljava/lang/Class;)Ltoothpick/Factory;

    move-result-object v0

    .line 36
    .local v0, "factory":Ltoothpick/Factory;, "Ltoothpick/Factory<TT;>;"
    if-eqz v0, :cond_0

    .line 40
    .end local v0    # "factory":Ltoothpick/Factory;, "Ltoothpick/Factory<TT;>;"
    .end local v1    # "registry":Ltoothpick/registries/FactoryRegistry;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
