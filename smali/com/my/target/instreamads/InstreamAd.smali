.class public final Lcom/my/target/instreamads/InstreamAd;
.super Lcom/my/target/common/BaseAd;
.source "InstreamAd.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;,
        Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_LOADING_TIMEOUT_SECONDS:I = 0xa

.field private static final MIN_LOADING_TIMEOUT_SECONDS:I = 0x5


# instance fields
.field private final context:Landroid/content/Context;

.field private engine:Lcom/my/target/core/engines/j;

.field private isFullscreen:Z

.field private listener:Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;

.field private final loadOnce:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private loadingTimeoutSeconds:I

.field private midpoints:[F

.field private player:Lcom/my/target/instreamads/InstreamAdPlayer;

.field private section:Lcom/my/target/fq;

.field private userMidpoints:[F

.field private videoDuration:F

.field private volume:F


# direct methods
.method public constructor <init>(ILandroid/content/Context;)V
    .locals 1
    .param p1, "slotId"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    const-string v0, "instreamads"

    invoke-direct {p0, p1, v0}, Lcom/my/target/common/BaseAd;-><init>(ILjava/lang/String;)V

    .line 37
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->loadOnce:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 42
    const/16 v0, 0xa

    iput v0, p0, Lcom/my/target/instreamads/InstreamAd;->loadingTimeoutSeconds:I

    .line 47
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/my/target/instreamads/InstreamAd;->volume:F

    .line 52
    iput-object p2, p0, Lcom/my/target/instreamads/InstreamAd;->context:Landroid/content/Context;

    .line 53
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/my/target/instreamads/InstreamAd;->setTrackingEnvironmentEnabled(Z)V

    .line 54
    const-string v0, "InstreamAd created. Version: 5.1.0"

    invoke-static {v0}, Lcom/my/target/g;->c(Ljava/lang/String;)V

    .line 55
    return-void
.end method

.method static synthetic access$000(Lcom/my/target/instreamads/InstreamAd;Lcom/my/target/fq;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/my/target/instreamads/InstreamAd;
    .param p1, "x1"    # Lcom/my/target/fq;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/my/target/instreamads/InstreamAd;->handleResult(Lcom/my/target/fq;Ljava/lang/String;)V

    return-void
.end method

.method private handleResult(Lcom/my/target/fq;Ljava/lang/String;)V
    .locals 2
    .param p1, "result"    # Lcom/my/target/fq;
    .param p2, "error"    # Ljava/lang/String;

    .prologue
    .line 324
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->listener:Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;

    if-eqz v0, :cond_2

    .line 326
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/my/target/fq;->hasContent()Z

    move-result v0

    if-nez v0, :cond_3

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->listener:Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;

    if-nez p2, :cond_1

    const-string p2, "no ad"

    .end local p2    # "error":Ljava/lang/String;
    :cond_1
    invoke-interface {v0, p2, p0}, Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;->onNoAd(Ljava/lang/String;Lcom/my/target/instreamads/InstreamAd;)V

    .line 344
    :cond_2
    :goto_0
    return-void

    .line 332
    .restart local p2    # "error":Ljava/lang/String;
    :cond_3
    iput-object p1, p0, Lcom/my/target/instreamads/InstreamAd;->section:Lcom/my/target/fq;

    .line 333
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->section:Lcom/my/target/fq;

    iget-object v1, p0, Lcom/my/target/instreamads/InstreamAd;->adConfig:Lcom/my/target/b;

    invoke-static {p0, v0, v1}, Lcom/my/target/core/engines/j;->a(Lcom/my/target/instreamads/InstreamAd;Lcom/my/target/fq;Lcom/my/target/b;)Lcom/my/target/core/engines/j;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    .line 334
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    iget v1, p0, Lcom/my/target/instreamads/InstreamAd;->loadingTimeoutSeconds:I

    invoke-virtual {v0, v1}, Lcom/my/target/core/engines/j;->setLoadingTimeoutSeconds(I)V

    .line 335
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    iget v1, p0, Lcom/my/target/instreamads/InstreamAd;->volume:F

    invoke-virtual {v0, v1}, Lcom/my/target/core/engines/j;->setVolume(F)V

    .line 336
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    if-eqz v0, :cond_4

    .line 338
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    iget-object v1, p0, Lcom/my/target/instreamads/InstreamAd;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    invoke-virtual {v0, v1}, Lcom/my/target/core/engines/j;->setPlayer(Lcom/my/target/instreamads/InstreamAdPlayer;)V

    .line 340
    :cond_4
    iget v0, p0, Lcom/my/target/instreamads/InstreamAd;->videoDuration:F

    iget-object v1, p0, Lcom/my/target/instreamads/InstreamAd;->userMidpoints:[F

    invoke-virtual {p0, v0, v1}, Lcom/my/target/instreamads/InstreamAd;->configureMidpoints(F[F)V

    .line 341
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->listener:Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;

    invoke-interface {v0, p0}, Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;->onLoad(Lcom/my/target/instreamads/InstreamAd;)V

    goto :goto_0
.end method

.method private start(Ljava/lang/String;)V
    .locals 1
    .param p1, "sectionName"    # Ljava/lang/String;

    .prologue
    .line 348
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    if-nez v0, :cond_0

    .line 350
    const-string v0, "Unable to start ad: not loaded yet"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 359
    :goto_0
    return-void

    .line 353
    :cond_0
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    invoke-virtual {v0}, Lcom/my/target/core/engines/j;->getPlayer()Lcom/my/target/instreamads/InstreamAdPlayer;

    move-result-object v0

    if-nez v0, :cond_1

    .line 355
    const-string v0, "Unable to start ad: player has not set"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 358
    :cond_1
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    invoke-virtual {v0, p1}, Lcom/my/target/core/engines/j;->start(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final configureMidpoints(F)V
    .locals 1
    .param p1, "videoDuration"    # F

    .prologue
    .line 189
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/my/target/instreamads/InstreamAd;->configureMidpoints(F[F)V

    .line 190
    return-void
.end method

.method public final configureMidpoints(F[F)V
    .locals 2
    .param p1, "videoDuration"    # F
    .param p2, "midpoints"    # [F

    .prologue
    .line 194
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_1

    .line 196
    const-string v0, "midpoints are not configured, duration is not set or <= zero"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->midpoints:[F

    if-eqz v0, :cond_2

    .line 201
    const-string v0, "midpoints already configured"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 204
    :cond_2
    iput-object p2, p0, Lcom/my/target/instreamads/InstreamAd;->userMidpoints:[F

    .line 205
    iput p1, p0, Lcom/my/target/instreamads/InstreamAd;->videoDuration:F

    .line 206
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->section:Lcom/my/target/fq;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->section:Lcom/my/target/fq;

    const-string v1, "midroll"

    invoke-virtual {v0, v1}, Lcom/my/target/fq;->a(Ljava/lang/String;)Lcom/my/target/al;

    move-result-object v0

    .line 209
    if-eqz v0, :cond_0

    .line 211
    iget-object v1, p0, Lcom/my/target/instreamads/InstreamAd;->userMidpoints:[F

    invoke-static {v0, v1, p1}, Lcom/my/target/ci;->a(Lcom/my/target/al;[FF)[F

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->midpoints:[F

    .line 212
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    iget-object v1, p0, Lcom/my/target/instreamads/InstreamAd;->midpoints:[F

    invoke-virtual {v0, v1}, Lcom/my/target/core/engines/j;->setMidpoints([F)V

    goto :goto_0
.end method

.method public final configureMidpointsPercents(F[F)V
    .locals 1
    .param p1, "videoDuration"    # F
    .param p2, "midpointsPercents"    # [F

    .prologue
    .line 222
    if-nez p2, :cond_0

    .line 224
    invoke-virtual {p0, p1}, Lcom/my/target/instreamads/InstreamAd;->configureMidpoints(F)V

    .line 228
    :goto_0
    return-void

    .line 227
    :cond_0
    invoke-static {p1, p2}, Lcom/my/target/ci;->a(F[F)[F

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/my/target/instreamads/InstreamAd;->configureMidpoints(F[F)V

    goto :goto_0
.end method

.method public final destroy()V
    .locals 1

    .prologue
    .line 285
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->listener:Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;

    .line 286
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    invoke-virtual {v0}, Lcom/my/target/core/engines/j;->destroy()V

    .line 290
    :cond_0
    return-void
.end method

.method public final getListener()Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->listener:Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;

    return-object v0
.end method

.method public final getLoadingTimeout()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/my/target/instreamads/InstreamAd;->loadingTimeoutSeconds:I

    return v0
.end method

.method public final getMidPoints()[F
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->midpoints:[F

    if-nez v0, :cond_0

    .line 164
    const/4 v0, 0x0

    new-array v0, v0, [F

    .line 166
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->midpoints:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    goto :goto_0
.end method

.method public final getPlayer()Lcom/my/target/instreamads/InstreamAdPlayer;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    return-object v0
.end method

.method public final getVideoQuality()I
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0}, Lcom/my/target/b;->getVideoQuality()I

    move-result v0

    return v0
.end method

.method public final getVolume()F
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    invoke-virtual {v0}, Lcom/my/target/core/engines/j;->getVolume()F

    move-result v0

    .line 143
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/my/target/instreamads/InstreamAd;->volume:F

    goto :goto_0
.end method

.method public final handleClick()V
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    invoke-virtual {v0}, Lcom/my/target/core/engines/j;->handleClick()V

    .line 281
    :cond_0
    return-void
.end method

.method public final isFullscreen()Z
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/my/target/instreamads/InstreamAd;->isFullscreen:Z

    return v0
.end method

.method public final load()V
    .locals 3

    .prologue
    .line 171
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->loadOnce:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 173
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " instance just loaded once, don\'t call load() more than one time per instance"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->b(Ljava/lang/String;)V

    .line 185
    :goto_0
    return-void

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->adConfig:Lcom/my/target/b;

    iget v1, p0, Lcom/my/target/instreamads/InstreamAd;->loadingTimeoutSeconds:I

    invoke-static {v0, v1}, Lcom/my/target/fn;->newFactory(Lcom/my/target/b;I)Lcom/my/target/c;

    move-result-object v0

    new-instance v1, Lcom/my/target/instreamads/InstreamAd$1;

    invoke-direct {v1, p0}, Lcom/my/target/instreamads/InstreamAd$1;-><init>(Lcom/my/target/instreamads/InstreamAd;)V

    invoke-virtual {v0, v1}, Lcom/my/target/c;->a(Lcom/my/target/c$b;)Lcom/my/target/c;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/instreamads/InstreamAd;->context:Landroid/content/Context;

    .line 184
    invoke-virtual {v0, v1}, Lcom/my/target/c;->a(Landroid/content/Context;)Lcom/my/target/c;

    goto :goto_0
.end method

.method public final pause()V
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    invoke-virtual {v0}, Lcom/my/target/core/engines/j;->pause()V

    .line 241
    :cond_0
    return-void
.end method

.method public final resume()V
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    invoke-virtual {v0}, Lcom/my/target/core/engines/j;->resume()V

    .line 249
    :cond_0
    return-void
.end method

.method public final setFullscreen(Z)V
    .locals 1
    .param p1, "fullscreen"    # Z

    .prologue
    .line 130
    iput-boolean p1, p0, Lcom/my/target/instreamads/InstreamAd;->isFullscreen:Z

    .line 131
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    invoke-virtual {v0, p1}, Lcom/my/target/core/engines/j;->setFullscreen(Z)V

    .line 135
    :cond_0
    return-void
.end method

.method public final setListener(Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/my/target/instreamads/InstreamAd;->listener:Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;

    .line 65
    return-void
.end method

.method public final setLoadingTimeout(I)V
    .locals 2
    .param p1, "loadingTimeout"    # I

    .prologue
    const/4 v1, 0x5

    .line 74
    if-ge p1, v1, :cond_1

    .line 76
    const-string v0, "unable to set ad loading timeout < 5, set to 5 seconds"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 77
    iput v1, p0, Lcom/my/target/instreamads/InstreamAd;->loadingTimeoutSeconds:I

    .line 84
    :goto_0
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    iget v1, p0, Lcom/my/target/instreamads/InstreamAd;->loadingTimeoutSeconds:I

    invoke-virtual {v0, v1}, Lcom/my/target/core/engines/j;->setLoadingTimeoutSeconds(I)V

    .line 88
    :cond_0
    return-void

    .line 81
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ad loading timeout set to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " seconds"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 82
    iput p1, p0, Lcom/my/target/instreamads/InstreamAd;->loadingTimeoutSeconds:I

    goto :goto_0
.end method

.method public final setPlayer(Lcom/my/target/instreamads/InstreamAdPlayer;)V
    .locals 1
    .param p1, "player"    # Lcom/my/target/instreamads/InstreamAdPlayer;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/my/target/instreamads/InstreamAd;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    .line 108
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    invoke-virtual {v0, p1}, Lcom/my/target/core/engines/j;->setPlayer(Lcom/my/target/instreamads/InstreamAdPlayer;)V

    .line 112
    :cond_0
    return-void
.end method

.method public final setVideoQuality(I)V
    .locals 1
    .param p1, "videoQuality"    # I

    .prologue
    .line 97
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0, p1}, Lcom/my/target/b;->setVideoQuality(I)V

    .line 98
    return-void
.end method

.method public final setVolume(F)V
    .locals 2
    .param p1, "volume"    # F

    .prologue
    .line 148
    const/4 v0, 0x0

    invoke-static {p1, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p1, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-lez v0, :cond_2

    .line 150
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "unable to set volume"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", volume must be in range [0..1]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 158
    :cond_1
    :goto_0
    return-void

    .line 153
    :cond_2
    iput p1, p0, Lcom/my/target/instreamads/InstreamAd;->volume:F

    .line 154
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    if-eqz v0, :cond_1

    .line 156
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    invoke-virtual {v0, p1}, Lcom/my/target/core/engines/j;->setVolume(F)V

    goto :goto_0
.end method

.method public final skip()V
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    invoke-virtual {v0}, Lcom/my/target/core/engines/j;->skip()V

    .line 265
    :cond_0
    return-void
.end method

.method public final skipBanner()V
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    invoke-virtual {v0}, Lcom/my/target/core/engines/j;->skipBanner()V

    .line 273
    :cond_0
    return-void
.end method

.method public final startMidroll(F)V
    .locals 1
    .param p1, "point"    # F

    .prologue
    .line 309
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    if-nez v0, :cond_0

    .line 311
    const-string v0, "Unable to start ad: not loaded yet"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 320
    :goto_0
    return-void

    .line 314
    :cond_0
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    invoke-virtual {v0}, Lcom/my/target/core/engines/j;->getPlayer()Lcom/my/target/instreamads/InstreamAdPlayer;

    move-result-object v0

    if-nez v0, :cond_1

    .line 316
    const-string v0, "Unable to start ad: player has not set"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 319
    :cond_1
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    invoke-virtual {v0, p1}, Lcom/my/target/core/engines/j;->startMidroll(F)V

    goto :goto_0
.end method

.method public final startPauseroll()V
    .locals 1

    .prologue
    .line 304
    const-string v0, "pauseroll"

    invoke-direct {p0, v0}, Lcom/my/target/instreamads/InstreamAd;->start(Ljava/lang/String;)V

    .line 305
    return-void
.end method

.method public final startPostroll()V
    .locals 1

    .prologue
    .line 299
    const-string v0, "postroll"

    invoke-direct {p0, v0}, Lcom/my/target/instreamads/InstreamAd;->start(Ljava/lang/String;)V

    .line 300
    return-void
.end method

.method public final startPreroll()V
    .locals 1

    .prologue
    .line 294
    const-string v0, "preroll"

    invoke-direct {p0, v0}, Lcom/my/target/instreamads/InstreamAd;->start(Ljava/lang/String;)V

    .line 295
    return-void
.end method

.method public final stop()V
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    invoke-virtual {v0}, Lcom/my/target/core/engines/j;->stop()V

    .line 257
    :cond_0
    return-void
.end method

.method public final swapPlayer(Lcom/my/target/instreamads/InstreamAdPlayer;)V
    .locals 1
    .param p1, "player"    # Lcom/my/target/instreamads/InstreamAdPlayer;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/my/target/instreamads/InstreamAd;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    .line 117
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/my/target/instreamads/InstreamAd;->engine:Lcom/my/target/core/engines/j;

    invoke-virtual {v0, p1}, Lcom/my/target/core/engines/j;->swapPlayer(Lcom/my/target/instreamads/InstreamAdPlayer;)V

    .line 121
    :cond_0
    return-void
.end method

.method public final useDefaultPlayer()V
    .locals 3

    .prologue
    .line 232
    new-instance v0, Lcom/my/target/fr;

    iget-object v1, p0, Lcom/my/target/instreamads/InstreamAd;->context:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/my/target/fr;-><init>(Landroid/content/Context;B)V

    invoke-virtual {p0, v0}, Lcom/my/target/instreamads/InstreamAd;->setPlayer(Lcom/my/target/instreamads/InstreamAdPlayer;)V

    .line 233
    return-void
.end method
