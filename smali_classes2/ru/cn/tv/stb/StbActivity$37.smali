.class Lru/cn/tv/stb/StbActivity$37;
.super Ljava/lang/Object;
.source "StbActivity.java"

# interfaces
.implements Lru/cn/domain/PinCode$PinCodeCheckCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/stb/StbActivity;->disablePin()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/stb/StbActivity;


# direct methods
.method constructor <init>(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 2476
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity$37;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public userPinCodeExist()V
    .locals 3

    .prologue
    .line 2490
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$37;->this$0:Lru/cn/tv/stb/StbActivity;

    sget-object v1, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;->enterPin:Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    new-instance v2, Lru/cn/tv/stb/StbActivity$37$1;

    invoke-direct {v2, p0}, Lru/cn/tv/stb/StbActivity$37$1;-><init>(Lru/cn/tv/stb/StbActivity$37;)V

    invoke-static {v0, v1, v2}, Lru/cn/tv/stb/StbActivity;->access$3400(Lru/cn/tv/stb/StbActivity;Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$PinCodeDialogCallbacks;)V

    .line 2505
    return-void
.end method

.method public userPinCodeNotExists()V
    .locals 2

    .prologue
    .line 2480
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$37;->this$0:Lru/cn/tv/stb/StbActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lru/cn/tv/stb/StbActivity;->access$7202(Lru/cn/tv/stb/StbActivity;I)I

    .line 2481
    const-string v0, "StbActivity"

    const-string v1, "Pin code not exists"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2483
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$37;->this$0:Lru/cn/tv/stb/StbActivity;

    const-string v1, "http://closedisableenterpin/"

    invoke-static {v0, v1}, Lru/cn/tv/stb/StbActivity;->access$3100(Lru/cn/tv/stb/StbActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2484
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$37;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$7300(Lru/cn/tv/stb/StbActivity;)V

    .line 2486
    :cond_0
    return-void
.end method
