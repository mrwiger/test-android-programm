.class public Lru/cn/utils/http/HttpException;
.super Lru/cn/utils/http/NetworkException;
.source "HttpException.java"


# static fields
.field private static final serialVersionUID:J = -0x679a2c829e53de71L


# instance fields
.field private statusCode:I


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1, "statusCode"    # I

    .prologue
    .line 9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Status code is: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lru/cn/utils/http/NetworkException;-><init>(Ljava/lang/String;)V

    .line 10
    iput p1, p0, Lru/cn/utils/http/HttpException;->statusCode:I

    .line 11
    return-void
.end method


# virtual methods
.method public getStatusCode()I
    .locals 1

    .prologue
    .line 14
    iget v0, p0, Lru/cn/utils/http/HttpException;->statusCode:I

    return v0
.end method
