.class public Lru/cn/notification/NotificationAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "NotificationAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lru/cn/notification/view/BaseNotificationsHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final CONTENT_TYPE:I

.field private final listener:Lru/cn/notification/NotificationButtonClickListener;

.field private notifications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/notifications/entity/INotification;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lru/cn/notification/NotificationButtonClickListener;)V
    .locals 1
    .param p1, "listener"    # Lru/cn/notification/NotificationButtonClickListener;

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 18
    const/4 v0, 0x1

    iput v0, p0, Lru/cn/notification/NotificationAdapter;->CONTENT_TYPE:I

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/notification/NotificationAdapter;->notifications:Ljava/util/List;

    .line 23
    iput-object p1, p0, Lru/cn/notification/NotificationAdapter;->listener:Lru/cn/notification/NotificationButtonClickListener;

    .line 24
    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lru/cn/notification/NotificationAdapter;->notifications:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 50
    iget-object v0, p0, Lru/cn/notification/NotificationAdapter;->notifications:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lru/cn/notifications/entity/INotification$NewFilmMessage;

    if-nez v0, :cond_0

    iget-object v0, p0, Lru/cn/notification/NotificationAdapter;->notifications:Ljava/util/List;

    .line 51
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lru/cn/notifications/entity/INotification$NewChannelMessage;

    if-nez v0, :cond_0

    iget-object v0, p0, Lru/cn/notification/NotificationAdapter;->notifications:Ljava/util/List;

    .line 52
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lru/cn/notifications/entity/INotification$NewTelecastMessage;

    if-nez v0, :cond_0

    iget-object v0, p0, Lru/cn/notification/NotificationAdapter;->notifications:Ljava/util/List;

    .line 53
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lru/cn/notifications/entity/INotification$LaunchAppSection;

    if-eqz v0, :cond_1

    .line 54
    :cond_0
    const/4 v0, 0x1

    .line 56
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lru/cn/notification/view/BaseNotificationsHolder;

    invoke-virtual {p0, p1, p2}, Lru/cn/notification/NotificationAdapter;->onBindViewHolder(Lru/cn/notification/view/BaseNotificationsHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lru/cn/notification/view/BaseNotificationsHolder;I)V
    .locals 1
    .param p1, "holder"    # Lru/cn/notification/view/BaseNotificationsHolder;
    .param p2, "position"    # I

    .prologue
    .line 61
    iget-object v0, p0, Lru/cn/notification/NotificationAdapter;->notifications:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/notifications/entity/INotification;

    invoke-virtual {p1, v0}, Lru/cn/notification/view/BaseNotificationsHolder;->set(Lru/cn/notifications/entity/INotification;)V

    .line 62
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0, p1, p2}, Lru/cn/notification/NotificationAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lru/cn/notification/view/BaseNotificationsHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lru/cn/notification/view/BaseNotificationsHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    const/4 v3, 0x0

    .line 38
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 39
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const/4 v2, 0x1

    if-ne p2, v2, :cond_0

    .line 40
    const v2, 0x7f0c003d

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 41
    .local v1, "view":Landroid/view/View;
    new-instance v2, Lru/cn/notification/view/NotificationContentHolder;

    iget-object v3, p0, Lru/cn/notification/NotificationAdapter;->listener:Lru/cn/notification/NotificationButtonClickListener;

    invoke-direct {v2, v1, v3}, Lru/cn/notification/view/NotificationContentHolder;-><init>(Landroid/view/View;Lru/cn/notification/NotificationButtonClickListener;)V

    .line 44
    :goto_0
    return-object v2

    .line 43
    .end local v1    # "view":Landroid/view/View;
    :cond_0
    const v2, 0x7f0c003e

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 44
    .restart local v1    # "view":Landroid/view/View;
    new-instance v2, Lru/cn/notification/view/NotificationHolder;

    iget-object v3, p0, Lru/cn/notification/NotificationAdapter;->listener:Lru/cn/notification/NotificationButtonClickListener;

    invoke-direct {v2, v1, v3}, Lru/cn/notification/view/NotificationHolder;-><init>(Landroid/view/View;Lru/cn/notification/NotificationButtonClickListener;)V

    goto :goto_0
.end method

.method public replace(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/notifications/entity/INotification;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p1, "notifications":Ljava/util/List;, "Ljava/util/List<Lru/cn/notifications/entity/INotification;>;"
    iput-object p1, p0, Lru/cn/notification/NotificationAdapter;->notifications:Ljava/util/List;

    .line 28
    invoke-virtual {p0}, Lru/cn/notification/NotificationAdapter;->notifyDataSetChanged()V

    .line 29
    return-void
.end method
