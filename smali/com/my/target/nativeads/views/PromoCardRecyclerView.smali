.class public Lcom/my/target/nativeads/views/PromoCardRecyclerView;
.super Landroid/support/v7/widget/RecyclerView;
.source "PromoCardRecyclerView.java"

# interfaces
.implements Lcom/my/target/da;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardViewHolder;,
        Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;
    }
.end annotation


# instance fields
.field private final cardClickListener:Landroid/view/View$OnClickListener;

.field private cards:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/my/target/nativeads/banners/NativePromoCard;",
            ">;"
        }
    .end annotation
.end field

.field private displayedCardNum:I

.field private final layoutManager:Lcom/my/target/ct;

.field private moving:Z

.field private onPromoCardSliderListener:Lcom/my/target/da$a;

.field private promoCardAdapter:Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;

.field private final snapHelper:Landroid/support/v7/widget/PagerSnapHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    .line 30
    new-instance v0, Lcom/my/target/nativeads/views/PromoCardRecyclerView$1;

    invoke-direct {v0, p0}, Lcom/my/target/nativeads/views/PromoCardRecyclerView$1;-><init>(Lcom/my/target/nativeads/views/PromoCardRecyclerView;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->cardClickListener:Landroid/view/View$OnClickListener;

    .line 68
    const/4 v0, -0x1

    iput v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->displayedCardNum:I

    .line 74
    new-instance v0, Lcom/my/target/ct;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/my/target/ct;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->layoutManager:Lcom/my/target/ct;

    .line 75
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 76
    new-instance v0, Landroid/support/v7/widget/PagerSnapHelper;

    invoke-direct {v0}, Landroid/support/v7/widget/PagerSnapHelper;-><init>()V

    iput-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->snapHelper:Landroid/support/v7/widget/PagerSnapHelper;

    .line 77
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->snapHelper:Landroid/support/v7/widget/PagerSnapHelper;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/PagerSnapHelper;->attachToRecyclerView(Landroid/support/v7/widget/RecyclerView;)V

    .line 79
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    new-instance v0, Lcom/my/target/nativeads/views/PromoCardRecyclerView$1;

    invoke-direct {v0, p0}, Lcom/my/target/nativeads/views/PromoCardRecyclerView$1;-><init>(Lcom/my/target/nativeads/views/PromoCardRecyclerView;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->cardClickListener:Landroid/view/View$OnClickListener;

    .line 68
    const/4 v0, -0x1

    iput v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->displayedCardNum:I

    .line 84
    new-instance v0, Lcom/my/target/ct;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/my/target/ct;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->layoutManager:Lcom/my/target/ct;

    .line 85
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 86
    new-instance v0, Landroid/support/v7/widget/PagerSnapHelper;

    invoke-direct {v0}, Landroid/support/v7/widget/PagerSnapHelper;-><init>()V

    iput-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->snapHelper:Landroid/support/v7/widget/PagerSnapHelper;

    .line 87
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->snapHelper:Landroid/support/v7/widget/PagerSnapHelper;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/PagerSnapHelper;->attachToRecyclerView(Landroid/support/v7/widget/RecyclerView;)V

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 92
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    new-instance v0, Lcom/my/target/nativeads/views/PromoCardRecyclerView$1;

    invoke-direct {v0, p0}, Lcom/my/target/nativeads/views/PromoCardRecyclerView$1;-><init>(Lcom/my/target/nativeads/views/PromoCardRecyclerView;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->cardClickListener:Landroid/view/View$OnClickListener;

    .line 68
    const/4 v0, -0x1

    iput v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->displayedCardNum:I

    .line 93
    new-instance v0, Lcom/my/target/ct;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/my/target/ct;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->layoutManager:Lcom/my/target/ct;

    .line 94
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 95
    new-instance v0, Landroid/support/v7/widget/PagerSnapHelper;

    invoke-direct {v0}, Landroid/support/v7/widget/PagerSnapHelper;-><init>()V

    iput-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->snapHelper:Landroid/support/v7/widget/PagerSnapHelper;

    .line 96
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->snapHelper:Landroid/support/v7/widget/PagerSnapHelper;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/PagerSnapHelper;->attachToRecyclerView(Landroid/support/v7/widget/RecyclerView;)V

    .line 97
    return-void
.end method

.method static synthetic access$000(Lcom/my/target/nativeads/views/PromoCardRecyclerView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/my/target/nativeads/views/PromoCardRecyclerView;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->moving:Z

    return v0
.end method

.method static synthetic access$100(Lcom/my/target/nativeads/views/PromoCardRecyclerView;)Lcom/my/target/ct;
    .locals 1
    .param p0, "x0"    # Lcom/my/target/nativeads/views/PromoCardRecyclerView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->layoutManager:Lcom/my/target/ct;

    return-object v0
.end method

.method static synthetic access$200(Lcom/my/target/nativeads/views/PromoCardRecyclerView;)Lcom/my/target/da$a;
    .locals 1
    .param p0, "x0"    # Lcom/my/target/nativeads/views/PromoCardRecyclerView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->onPromoCardSliderListener:Lcom/my/target/da$a;

    return-object v0
.end method

.method static synthetic access$300(Lcom/my/target/nativeads/views/PromoCardRecyclerView;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/my/target/nativeads/views/PromoCardRecyclerView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->cards:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$400(Lcom/my/target/nativeads/views/PromoCardRecyclerView;)Landroid/support/v7/widget/PagerSnapHelper;
    .locals 1
    .param p0, "x0"    # Lcom/my/target/nativeads/views/PromoCardRecyclerView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->snapHelper:Landroid/support/v7/widget/PagerSnapHelper;

    return-object v0
.end method

.method private checkCardChanged()V
    .locals 5

    .prologue
    .line 189
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->layoutManager:Lcom/my/target/ct;

    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager;->findFirstCompletelyVisibleItemPosition()I

    move-result v0

    .line 190
    if-gez v0, :cond_1

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    iget v1, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->displayedCardNum:I

    if-eq v1, v0, :cond_0

    .line 196
    iput v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->displayedCardNum:I

    .line 197
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->onPromoCardSliderListener:Lcom/my/target/da$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->cards:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->layoutManager:Lcom/my/target/ct;

    iget v1, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->displayedCardNum:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/LinearLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v0

    .line 201
    if-eqz v0, :cond_0

    .line 203
    iget-object v1, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->onPromoCardSliderListener:Lcom/my/target/da$a;

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v3, 0x0

    iget v4, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->displayedCardNum:I

    aput v4, v2, v3

    invoke-interface {v1, v0, v2}, Lcom/my/target/da$a;->a(Landroid/view/View;[I)V

    goto :goto_0
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->promoCardAdapter:Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->promoCardAdapter:Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;

    invoke-virtual {v0}, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->dispose()V

    .line 160
    :cond_0
    return-void
.end method

.method public getState()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->layoutManager:Lcom/my/target/ct;

    invoke-virtual {v0}, Lcom/my/target/ct;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public getVisibleCardNumbers()[I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 102
    iget-object v1, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->layoutManager:Lcom/my/target/ct;

    invoke-virtual {v1}, Landroid/support/v7/widget/LinearLayoutManager;->findFirstCompletelyVisibleItemPosition()I

    move-result v1

    .line 103
    iget-object v2, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->layoutManager:Lcom/my/target/ct;

    invoke-virtual {v2}, Landroid/support/v7/widget/LinearLayoutManager;->findLastCompletelyVisibleItemPosition()I

    move-result v2

    .line 105
    iget-object v3, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->cards:Ljava/util/List;

    if-eqz v3, :cond_0

    if-gt v1, v2, :cond_0

    if-ltz v1, :cond_0

    iget-object v3, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->cards:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v2, v3, :cond_1

    .line 107
    :cond_0
    new-array v0, v0, [I

    .line 117
    :goto_0
    return-object v0

    .line 110
    :cond_1
    sub-int/2addr v2, v1

    add-int/lit8 v2, v2, 0x1

    new-array v2, v2, [I

    .line 111
    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_2

    .line 113
    aput v1, v2, v0

    .line 114
    add-int/lit8 v1, v1, 0x1

    .line 111
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 117
    goto :goto_0
.end method

.method public onScrollStateChanged(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 177
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->onScrollStateChanged(I)V

    .line 179
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->moving:Z

    .line 181
    iget-boolean v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->moving:Z

    if-nez v0, :cond_0

    .line 183
    invoke-direct {p0}, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->checkCardChanged()V

    .line 185
    :cond_0
    return-void

    .line 179
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public restoreState(Landroid/os/Parcelable;)V
    .locals 1
    .param p1, "position"    # Landroid/os/Parcelable;

    .prologue
    .line 171
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->layoutManager:Lcom/my/target/ct;

    invoke-virtual {v0, p1}, Lcom/my/target/ct;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 172
    return-void
.end method

.method public setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V
    .locals 1
    .param p1, "adapter"    # Landroid/support/v7/widget/RecyclerView$Adapter;

    .prologue
    .line 122
    instance-of v0, p1, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;

    if-eqz v0, :cond_0

    .line 124
    check-cast p1, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;

    .end local p1    # "adapter":Landroid/support/v7/widget/RecyclerView$Adapter;
    invoke-virtual {p0, p1}, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->setPromoCardAdapter(Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;)V

    .line 132
    :goto_0
    return-void

    .line 128
    .restart local p1    # "adapter":Landroid/support/v7/widget/RecyclerView$Adapter;
    :cond_0
    const-string v0, "You must use setPromoCardAdapter(PromoCardAdapter) method with custom CardRecyclerView"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setPromoCardAdapter(Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;

    .prologue
    .line 136
    if-nez p1, :cond_0

    .line 145
    :goto_0
    return-void

    .line 140
    :cond_0
    invoke-virtual {p1}, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->getNativePromoCards()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->cards:Ljava/util/List;

    .line 141
    iput-object p1, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->promoCardAdapter:Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;

    .line 142
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->promoCardAdapter:Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;

    iget-object v1, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->cardClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;->setClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->layoutManager:Lcom/my/target/ct;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 144
    iget-object v0, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->promoCardAdapter:Lcom/my/target/nativeads/views/PromoCardRecyclerView$PromoCardAdapter;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Landroid/support/v7/widget/RecyclerView;->swapAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;Z)V

    goto :goto_0
.end method

.method public setPromoCardSliderListener(Lcom/my/target/da$a;)V
    .locals 0
    .param p1, "promoClickListener"    # Lcom/my/target/da$a;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/my/target/nativeads/views/PromoCardRecyclerView;->onPromoCardSliderListener:Lcom/my/target/da$a;

    .line 151
    return-void
.end method
