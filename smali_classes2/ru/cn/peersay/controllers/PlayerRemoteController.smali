.class public Lru/cn/peersay/controllers/PlayerRemoteController;
.super Ljava/lang/Object;
.source "PlayerRemoteController.java"

# interfaces
.implements Lru/cn/peersay/controllers/RemoteIntentController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;
    }
.end annotation


# instance fields
.field private currentState:Ljava/lang/String;

.field private player:Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const-string v0, "stopped"

    iput-object v0, p0, Lru/cn/peersay/controllers/PlayerRemoteController;->currentState:Ljava/lang/String;

    return-void
.end method

.method private static convertState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)Ljava/lang/String;
    .locals 2
    .param p0, "state"    # Lru/cn/player/AbstractMediaPlayer$PlayerState;

    .prologue
    .line 115
    sget-object v0, Lru/cn/peersay/controllers/PlayerRemoteController$1;->$SwitchMap$ru$cn$player$AbstractMediaPlayer$PlayerState:[I

    invoke-virtual {p0}, Lru/cn/player/AbstractMediaPlayer$PlayerState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 127
    const-string v0, "stopped"

    :goto_0
    return-object v0

    .line 118
    :pswitch_0
    const-string v0, "loading"

    goto :goto_0

    .line 121
    :pswitch_1
    const-string v0, "playing"

    goto :goto_0

    .line 124
    :pswitch_2
    const-string v0, "paused"

    goto :goto_0

    .line 115
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public handleMessage(Landroid/content/Context;Lru/cn/peersay/IntentMessage;)Z
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "message"    # Lru/cn/peersay/IntentMessage;

    .prologue
    const-wide/16 v10, -0x1

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 64
    iget-object v7, p0, Lru/cn/peersay/controllers/PlayerRemoteController;->player:Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;

    if-nez v7, :cond_1

    .line 111
    :cond_0
    :goto_0
    return v6

    .line 67
    :cond_1
    iget-object v7, p2, Lru/cn/peersay/IntentMessage;->extras:Landroid/os/Bundle;

    const-string v9, "command"

    invoke-virtual {v7, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 68
    .local v0, "command":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 71
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 72
    .local v4, "result":Landroid/os/Bundle;
    const/4 v7, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_2
    :goto_1
    packed-switch v7, :pswitch_data_0

    goto :goto_0

    .line 74
    :pswitch_0
    iget-object v6, p0, Lru/cn/peersay/controllers/PlayerRemoteController;->player:Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;

    invoke-interface {v6}, Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;->pause()V

    .line 110
    :cond_3
    :goto_2
    invoke-virtual {p2, v4}, Lru/cn/peersay/IntentMessage;->setResult(Landroid/os/Bundle;)V

    move v6, v8

    .line 111
    goto :goto_0

    .line 72
    :sswitch_0
    const-string v9, "tv.playback.pause"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    move v7, v6

    goto :goto_1

    :sswitch_1
    const-string v9, "tv.playback.play"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    move v7, v8

    goto :goto_1

    :sswitch_2
    const-string v9, "tv.playback.position"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v7, 0x2

    goto :goto_1

    :sswitch_3
    const-string v9, "tv.playback.duration"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v7, 0x3

    goto :goto_1

    :sswitch_4
    const-string v9, "tv.playback.seek"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v7, 0x4

    goto :goto_1

    :sswitch_5
    const-string v9, "tv.playback.state"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v7, 0x5

    goto :goto_1

    .line 78
    :pswitch_1
    iget-object v6, p0, Lru/cn/peersay/controllers/PlayerRemoteController;->player:Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;

    invoke-interface {v6}, Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;->play()V

    goto :goto_2

    .line 82
    :pswitch_2
    const-string v6, "playingPosition"

    iget-object v7, p0, Lru/cn/peersay/controllers/PlayerRemoteController;->player:Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;

    invoke-interface {v7}, Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;->getCurrentPosition()I

    move-result v7

    int-to-long v10, v7

    invoke-virtual {v4, v6, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 83
    iget-object v6, p0, Lru/cn/peersay/controllers/PlayerRemoteController;->player:Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;

    invoke-interface {v6}, Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;->getLivePosition()I

    move-result v1

    .line 84
    .local v1, "livePosition":I
    if-lez v1, :cond_3

    .line 85
    const-string v6, "airPosition"

    iget-object v7, p0, Lru/cn/peersay/controllers/PlayerRemoteController;->player:Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;

    invoke-interface {v7}, Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;->getLivePosition()I

    move-result v7

    int-to-long v10, v7

    invoke-virtual {v4, v6, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_2

    .line 90
    .end local v1    # "livePosition":I
    :pswitch_3
    const-string v9, "duration"

    iget-object v6, p0, Lru/cn/peersay/controllers/PlayerRemoteController;->player:Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;

    invoke-interface {v6}, Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;->isSeekable()Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lru/cn/peersay/controllers/PlayerRemoteController;->player:Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;

    invoke-interface {v6}, Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;->getDuration()I

    move-result v6

    int-to-long v6, v6

    :goto_3
    invoke-virtual {v4, v9, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_2

    :cond_4
    const-wide/16 v6, 0x0

    goto :goto_3

    .line 94
    :pswitch_4
    iget-object v7, p2, Lru/cn/peersay/IntentMessage;->extras:Landroid/os/Bundle;

    const-string v9, "position"

    invoke-virtual {v7, v9, v10, v11}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 95
    .local v2, "position":J
    cmp-long v7, v2, v10

    if-eqz v7, :cond_0

    .line 98
    iget-object v6, p0, Lru/cn/peersay/controllers/PlayerRemoteController;->player:Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;

    long-to-int v7, v2

    invoke-interface {v6, v7}, Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;->seekTo(I)V

    goto/16 :goto_2

    .line 102
    .end local v2    # "position":J
    :pswitch_5
    iget-object v6, p0, Lru/cn/peersay/controllers/PlayerRemoteController;->player:Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;

    invoke-interface {v6}, Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;->getState()Lru/cn/player/AbstractMediaPlayer$PlayerState;

    move-result-object v6

    invoke-static {v6}, Lru/cn/peersay/controllers/PlayerRemoteController;->convertState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)Ljava/lang/String;

    move-result-object v5

    .line 103
    .local v5, "state":Ljava/lang/String;
    const-string v6, "state"

    invoke-virtual {v4, v6, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 72
    :sswitch_data_0
    .sparse-switch
        -0x4a7562b1 -> :sswitch_0
        -0x4a42c416 -> :sswitch_5
        -0x266bb45 -> :sswitch_1
        -0x2657801 -> :sswitch_4
        0xe613730 -> :sswitch_2
        0x6b12eb9b -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onStateChanged(Landroid/content/Context;Lru/cn/player/AbstractMediaPlayer$PlayerState;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "state"    # Lru/cn/player/AbstractMediaPlayer$PlayerState;

    .prologue
    .line 45
    invoke-static {p2}, Lru/cn/peersay/controllers/PlayerRemoteController;->convertState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, "convertedState":Ljava/lang/String;
    iget-object v3, p0, Lru/cn/peersay/controllers/PlayerRemoteController;->currentState:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 58
    :goto_0
    return-void

    .line 49
    :cond_0
    iput-object v0, p0, Lru/cn/peersay/controllers/PlayerRemoteController;->currentState:Ljava/lang/String;

    .line 50
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 51
    .local v2, "info":Landroid/os/Bundle;
    const-string v3, "event"

    const-string v4, "tv.playback.state_changed"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const-string v3, "state"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    invoke-static {v2}, Lru/cn/peersay/IntentMessage;->createEventIntent(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v1

    .line 55
    .local v1, "event":Landroid/content/Intent;
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 57
    const-string v3, "PlayerRemoteController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "event sent "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setPlayer(Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;)V
    .locals 0
    .param p1, "player"    # Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;

    .prologue
    .line 39
    iput-object p1, p0, Lru/cn/peersay/controllers/PlayerRemoteController;->player:Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;

    .line 40
    return-void
.end method
