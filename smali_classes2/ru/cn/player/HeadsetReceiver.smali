.class public Lru/cn/player/HeadsetReceiver;
.super Landroid/content/BroadcastReceiver;
.source "HeadsetReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/player/HeadsetReceiver$AudioBecomingNoisyListener;
    }
.end annotation


# instance fields
.field private audioBecomingNoisyListener:Lru/cn/player/HeadsetReceiver$AudioBecomingNoisyListener;

.field private final headsetReceiverFilter:Landroid/content/IntentFilter;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 9
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 12
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.media.AUDIO_BECOMING_NOISY"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lru/cn/player/HeadsetReceiver;->headsetReceiverFilter:Landroid/content/IntentFilter;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 24
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p0, Lru/cn/player/HeadsetReceiver;->audioBecomingNoisyListener:Lru/cn/player/HeadsetReceiver$AudioBecomingNoisyListener;

    invoke-interface {v0}, Lru/cn/player/HeadsetReceiver$AudioBecomingNoisyListener;->onAudioBecomingNoisy()V

    .line 27
    :cond_0
    return-void
.end method

.method public register(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    iget-object v0, p0, Lru/cn/player/HeadsetReceiver;->headsetReceiverFilter:Landroid/content/IntentFilter;

    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 31
    return-void
.end method

.method public setAudioBecomingNoisyListener(Lru/cn/player/HeadsetReceiver$AudioBecomingNoisyListener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/player/HeadsetReceiver$AudioBecomingNoisyListener;

    .prologue
    .line 19
    iput-object p1, p0, Lru/cn/player/HeadsetReceiver;->audioBecomingNoisyListener:Lru/cn/player/HeadsetReceiver$AudioBecomingNoisyListener;

    .line 20
    return-void
.end method

.method public unregister(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    invoke-virtual {p1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 35
    return-void
.end method
