.class public Lru/cn/tv/mobile/ChannelsScheduleFragment;
.super Landroid/support/v4/app/Fragment;
.source "ChannelsScheduleFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/mobile/ChannelsScheduleFragment$ChannelsScheduleFragmentListener;
    }
.end annotation


# instance fields
.field private channelsFragment:Lru/cn/tv/mobile/channels/ChannelsFragment;

.field private layout12:Lru/cn/view/OneTwoFragmentLayout;

.field private listener:Lru/cn/tv/mobile/ChannelsScheduleFragment$ChannelsScheduleFragmentListener;

.field private page:I

.field private scheduleFragment:Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;

.field private scheduleListener:Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;

.field private scheduleOpenEventListener:Ljava/lang/Object;

.field private viewMode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 212
    new-instance v0, Lru/cn/tv/mobile/ChannelsScheduleFragment$3;

    invoke-direct {v0, p0}, Lru/cn/tv/mobile/ChannelsScheduleFragment$3;-><init>(Lru/cn/tv/mobile/ChannelsScheduleFragment;)V

    iput-object v0, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->scheduleOpenEventListener:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/mobile/ChannelsScheduleFragment;J)V
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/ChannelsScheduleFragment;
    .param p1, "x1"    # J

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lru/cn/tv/mobile/ChannelsScheduleFragment;->openSchedule(J)V

    return-void
.end method

.method static synthetic access$100(Lru/cn/tv/mobile/ChannelsScheduleFragment;J)V
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/ChannelsScheduleFragment;
    .param p1, "x1"    # J

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lru/cn/tv/mobile/ChannelsScheduleFragment;->openScheduleImpl(J)V

    return-void
.end method

.method private isHorizontal()Z
    .locals 3

    .prologue
    .line 146
    invoke-virtual {p0}, Lru/cn/tv/mobile/ChannelsScheduleFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 147
    .local v0, "config":Landroid/content/res/Configuration;
    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private openChannels()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 165
    iput v1, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->page:I

    .line 166
    iget-object v0, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->layout12:Lru/cn/view/OneTwoFragmentLayout;

    invoke-virtual {v0, v1}, Lru/cn/view/OneTwoFragmentLayout;->setCurrentPage(I)V

    .line 167
    iget-object v0, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->layout12:Lru/cn/view/OneTwoFragmentLayout;

    invoke-virtual {v0, v1}, Lru/cn/view/OneTwoFragmentLayout;->showSecondFragment(Z)V

    .line 168
    iget-object v0, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->channelsFragment:Lru/cn/tv/mobile/channels/ChannelsFragment;

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/channels/ChannelsFragment;->checkCurrentChannel(Z)V

    .line 170
    iget-object v0, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->listener:Lru/cn/tv/mobile/ChannelsScheduleFragment$ChannelsScheduleFragmentListener;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->listener:Lru/cn/tv/mobile/ChannelsScheduleFragment$ChannelsScheduleFragmentListener;

    invoke-interface {v0}, Lru/cn/tv/mobile/ChannelsScheduleFragment$ChannelsScheduleFragmentListener;->onListOpened()V

    .line 173
    :cond_0
    return-void
.end method

.method private openSchedule(J)V
    .locals 5
    .param p1, "cnId"    # J

    .prologue
    const/4 v2, 0x0

    .line 176
    invoke-virtual {p0}, Lru/cn/tv/mobile/ChannelsScheduleFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lru/cn/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v1, v2

    .line 177
    .local v1, "placeId":Ljava/lang/String;
    :goto_0
    if-eqz v1, :cond_1

    .line 178
    invoke-static {v1}, Lru/cn/ad/AdsManager;->hasOpportunity(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 180
    sget-object v3, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_OPPORTUNITY:Lru/cn/domain/statistics/inetra/AdvEvent;

    invoke-static {v3, v1, v2, v2}, Lru/cn/domain/statistics/inetra/InetraTracker;->advEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/lang/String;Lru/cn/api/money_miner/replies/AdSystem;Ljava/util/Map;)V

    .line 181
    invoke-static {v1}, Lru/cn/ad/AdsManager;->consumeOpportunity(Ljava/lang/String;)V

    .line 183
    new-instance v0, Lru/cn/ad/interstitial/InterstitialBanner;

    invoke-virtual {p0}, Lru/cn/tv/mobile/ChannelsScheduleFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lru/cn/ad/interstitial/InterstitialBanner;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 184
    .local v0, "banner":Lru/cn/ad/interstitial/InterstitialBanner;
    invoke-virtual {v0}, Lru/cn/ad/interstitial/InterstitialBanner;->isReady()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 185
    new-instance v2, Lru/cn/tv/mobile/ChannelsScheduleFragment$2;

    invoke-direct {v2, p0, p1, p2}, Lru/cn/tv/mobile/ChannelsScheduleFragment$2;-><init>(Lru/cn/tv/mobile/ChannelsScheduleFragment;J)V

    invoke-virtual {v0, v2}, Lru/cn/ad/interstitial/InterstitialBanner;->setListener(Lru/cn/ad/interstitial/InterstitialBanner$Listener;)V

    .line 203
    invoke-virtual {v0}, Lru/cn/ad/interstitial/InterstitialBanner;->show()V

    .line 210
    .end local v0    # "banner":Lru/cn/ad/interstitial/InterstitialBanner;
    :goto_1
    return-void

    .line 176
    .end local v1    # "placeId":Ljava/lang/String;
    :cond_0
    const-string v1, "QDhB8u"

    goto :goto_0

    .line 209
    .restart local v1    # "placeId":Ljava/lang/String;
    :cond_1
    invoke-direct {p0, p1, p2}, Lru/cn/tv/mobile/ChannelsScheduleFragment;->openScheduleImpl(J)V

    goto :goto_1
.end method

.method private openScheduleImpl(J)V
    .locals 3
    .param p1, "channelId"    # J

    .prologue
    const/4 v2, 0x1

    .line 220
    iget-object v1, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->channelsFragment:Lru/cn/tv/mobile/channels/ChannelsFragment;

    invoke-virtual {v1, p1, p2}, Lru/cn/tv/mobile/channels/ChannelsFragment;->setCurrentChannel(J)V

    .line 221
    iget-object v1, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->channelsFragment:Lru/cn/tv/mobile/channels/ChannelsFragment;

    invoke-virtual {v1, v2}, Lru/cn/tv/mobile/channels/ChannelsFragment;->checkCurrentChannel(Z)V

    .line 222
    iget-object v1, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->scheduleFragment:Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;

    invoke-virtual {v1, p1, p2}, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->setChannelId(J)V

    .line 224
    iput v2, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->page:I

    .line 225
    iget-object v1, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->layout12:Lru/cn/view/OneTwoFragmentLayout;

    invoke-virtual {v1, v2}, Lru/cn/view/OneTwoFragmentLayout;->setCurrentPage(I)V

    .line 226
    iget-object v1, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->layout12:Lru/cn/view/OneTwoFragmentLayout;

    invoke-virtual {v1, v2}, Lru/cn/view/OneTwoFragmentLayout;->showSecondFragment(Z)V

    .line 228
    iget-object v1, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->listener:Lru/cn/tv/mobile/ChannelsScheduleFragment$ChannelsScheduleFragmentListener;

    if-eqz v1, :cond_0

    .line 229
    iget-object v1, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->channelsFragment:Lru/cn/tv/mobile/channels/ChannelsFragment;

    invoke-virtual {v1, p1, p2}, Lru/cn/tv/mobile/channels/ChannelsFragment;->getChannelTitle(J)Ljava/lang/String;

    move-result-object v0

    .line 230
    .local v0, "channelTitle":Ljava/lang/String;
    iget-object v1, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->listener:Lru/cn/tv/mobile/ChannelsScheduleFragment$ChannelsScheduleFragmentListener;

    invoke-interface {v1, v0}, Lru/cn/tv/mobile/ChannelsScheduleFragment$ChannelsScheduleFragmentListener;->onScheduleOpened(Ljava/lang/String;)V

    .line 232
    .end local v0    # "channelTitle":Ljava/lang/String;
    :cond_0
    return-void
.end method


# virtual methods
.method public backPressed()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 125
    iget v1, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->page:I

    if-ne v1, v0, :cond_0

    .line 126
    invoke-direct {p0}, Lru/cn/tv/mobile/ChannelsScheduleFragment;->openChannels()V

    .line 129
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canGoBack()Z
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->page:I

    if-nez v0, :cond_0

    .line 134
    const/4 v0, 0x0

    .line 136
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getChannelTitle()Ljava/lang/String;
    .locals 3

    .prologue
    .line 116
    iget-object v2, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->channelsFragment:Lru/cn/tv/mobile/channels/ChannelsFragment;

    invoke-virtual {v2}, Lru/cn/tv/mobile/channels/ChannelsFragment;->getCurrentChannel()J

    move-result-wide v0

    .line 117
    .local v0, "channelId":J
    iget-object v2, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->channelsFragment:Lru/cn/tv/mobile/channels/ChannelsFragment;

    invoke-virtual {v2, v0, v1}, Lru/cn/tv/mobile/channels/ChannelsFragment;->getChannelTitle(J)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public getViewMode()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->viewMode:I

    return v0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 88
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 90
    invoke-static {}, Lorg/greenrobot/eventbus/EventBus;->getDefault()Lorg/greenrobot/eventbus/EventBus;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->scheduleOpenEventListener:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lorg/greenrobot/eventbus/EventBus;->register(Ljava/lang/Object;)V

    .line 91
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 141
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 142
    invoke-virtual {p0}, Lru/cn/tv/mobile/ChannelsScheduleFragment;->updateViewOrientation()V

    .line 143
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 50
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 52
    new-instance v0, Lru/cn/tv/mobile/channels/ChannelsFragment;

    invoke-direct {v0}, Lru/cn/tv/mobile/channels/ChannelsFragment;-><init>()V

    iput-object v0, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->channelsFragment:Lru/cn/tv/mobile/channels/ChannelsFragment;

    .line 53
    new-instance v0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;

    invoke-direct {v0}, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;-><init>()V

    iput-object v0, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->scheduleFragment:Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;

    .line 54
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 60
    const v0, 0x7f0c009b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 95
    invoke-static {}, Lorg/greenrobot/eventbus/EventBus;->getDefault()Lorg/greenrobot/eventbus/EventBus;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->scheduleOpenEventListener:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lorg/greenrobot/eventbus/EventBus;->unregister(Ljava/lang/Object;)V

    .line 97
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 98
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 64
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 66
    const v0, 0x7f09015a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lru/cn/view/OneTwoFragmentLayout;

    iput-object v0, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->layout12:Lru/cn/view/OneTwoFragmentLayout;

    .line 67
    iget-object v0, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->layout12:Lru/cn/view/OneTwoFragmentLayout;

    invoke-virtual {p0}, Lru/cn/tv/mobile/ChannelsScheduleFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/cn/view/OneTwoFragmentLayout;->setFragmentManager(Landroid/support/v4/app/FragmentManager;)V

    .line 69
    iget-object v0, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->channelsFragment:Lru/cn/tv/mobile/channels/ChannelsFragment;

    iget v1, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->viewMode:I

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/channels/ChannelsFragment;->setViewMode(I)V

    .line 70
    iget-object v0, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->channelsFragment:Lru/cn/tv/mobile/channels/ChannelsFragment;

    new-instance v1, Lru/cn/tv/mobile/ChannelsScheduleFragment$1;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/ChannelsScheduleFragment$1;-><init>(Lru/cn/tv/mobile/ChannelsScheduleFragment;)V

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/channels/ChannelsFragment;->setListener(Lru/cn/tv/mobile/channels/ChannelsFragment$ChannelsFragmentListener;)V

    .line 77
    iget-object v0, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->layout12:Lru/cn/view/OneTwoFragmentLayout;

    iget-object v1, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->channelsFragment:Lru/cn/tv/mobile/channels/ChannelsFragment;

    invoke-virtual {v0, v1}, Lru/cn/view/OneTwoFragmentLayout;->addFirstFragment(Landroid/support/v4/app/Fragment;)V

    .line 80
    iget-object v0, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->scheduleFragment:Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;

    iget-object v1, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->scheduleListener:Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->setListener(Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;)V

    .line 81
    iget-object v0, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->layout12:Lru/cn/view/OneTwoFragmentLayout;

    iget-object v1, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->scheduleFragment:Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;

    invoke-virtual {v0, v1}, Lru/cn/view/OneTwoFragmentLayout;->addSecondFragment(Landroid/support/v4/app/Fragment;)V

    .line 82
    iget-object v0, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->layout12:Lru/cn/view/OneTwoFragmentLayout;

    iget v1, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->page:I

    invoke-virtual {v0, v1}, Lru/cn/view/OneTwoFragmentLayout;->setCurrentPage(I)V

    .line 83
    invoke-virtual {p0}, Lru/cn/tv/mobile/ChannelsScheduleFragment;->updateViewOrientation()V

    .line 84
    return-void
.end method

.method public setListener(Lru/cn/tv/mobile/ChannelsScheduleFragment$ChannelsScheduleFragmentListener;)V
    .locals 0
    .param p1, "l"    # Lru/cn/tv/mobile/ChannelsScheduleFragment$ChannelsScheduleFragmentListener;

    .prologue
    .line 101
    iput-object p1, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->listener:Lru/cn/tv/mobile/ChannelsScheduleFragment$ChannelsScheduleFragmentListener;

    .line 102
    return-void
.end method

.method public setScheduleListener(Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;

    .prologue
    .line 105
    iput-object p1, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->scheduleListener:Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;

    .line 106
    return-void
.end method

.method public setViewMode(I)V
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 109
    iput p1, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->viewMode:I

    .line 110
    iget-object v0, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->channelsFragment:Lru/cn/tv/mobile/channels/ChannelsFragment;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->channelsFragment:Lru/cn/tv/mobile/channels/ChannelsFragment;

    invoke-virtual {v0, p1}, Lru/cn/tv/mobile/channels/ChannelsFragment;->setViewMode(I)V

    .line 113
    :cond_0
    return-void
.end method

.method public updateViewOrientation()V
    .locals 3

    .prologue
    .line 152
    invoke-virtual {p0}, Lru/cn/tv/mobile/ChannelsScheduleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 153
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_0

    .line 162
    :goto_0
    return-void

    .line 156
    :cond_0
    invoke-static {v0}, Lru/cn/utils/Utils;->isXLarge(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 157
    invoke-static {v0}, Lru/cn/utils/Utils;->isLarge(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lru/cn/tv/mobile/ChannelsScheduleFragment;->isHorizontal()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 158
    :cond_1
    iget-object v1, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->layout12:Lru/cn/view/OneTwoFragmentLayout;

    sget-object v2, Lru/cn/view/OneTwoFragmentLayout$Mode;->MODE_2:Lru/cn/view/OneTwoFragmentLayout$Mode;

    invoke-virtual {v1, v2}, Lru/cn/view/OneTwoFragmentLayout;->setMode(Lru/cn/view/OneTwoFragmentLayout$Mode;)V

    goto :goto_0

    .line 160
    :cond_2
    iget-object v1, p0, Lru/cn/tv/mobile/ChannelsScheduleFragment;->layout12:Lru/cn/view/OneTwoFragmentLayout;

    sget-object v2, Lru/cn/view/OneTwoFragmentLayout$Mode;->MODE_1:Lru/cn/view/OneTwoFragmentLayout$Mode;

    invoke-virtual {v1, v2}, Lru/cn/view/OneTwoFragmentLayout;->setMode(Lru/cn/view/OneTwoFragmentLayout$Mode;)V

    goto :goto_0
.end method
