.class public final Lcom/my/target/dc;
.super Lcom/my/target/d;
.source "StandardAdResponseParser.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/my/target/d",
        "<",
        "Lcom/my/target/dh;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/my/target/d;-><init>()V

    .line 36
    return-void
.end method

.method public static newParser()Lcom/my/target/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/my/target/d",
            "<",
            "Lcom/my/target/dh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    new-instance v0, Lcom/my/target/dc;

    invoke-direct {v0}, Lcom/my/target/dc;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;Lcom/my/target/ae;Lcom/my/target/ak;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/ak;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 26
    check-cast p3, Lcom/my/target/dh;

    .line 1045
    invoke-virtual {p0, p1, p5}, Lcom/my/target/dc;->a(Ljava/lang/String;Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object v0

    .line 1046
    if-eqz v0, :cond_6

    .line 1051
    invoke-virtual {p4}, Lcom/my/target/b;->getFormat()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 1053
    if-eqz v2, :cond_6

    .line 1058
    const-string v3, "banners"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 1060
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-gtz v4, :cond_1

    :cond_0
    move-object p3, v1

    .line 1107
    :goto_0
    return-object p3

    .line 1065
    :cond_1
    if-nez p3, :cond_2

    .line 1067
    invoke-static {}, Lcom/my/target/dh;->v()Lcom/my/target/dh;

    move-result-object p3

    .line 1070
    :cond_2
    invoke-static {p2, p4, p5}, Lcom/my/target/df;->a(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/df;

    move-result-object v4

    invoke-virtual {v4, v2, p3}, Lcom/my/target/df;->a(Lorg/json/JSONObject;Lcom/my/target/dh;)V

    .line 1071
    const-string v2, "html"

    invoke-virtual {p3}, Lcom/my/target/dh;->w()Lcom/my/target/dg;

    move-result-object v4

    invoke-virtual {v4}, Lcom/my/target/dg;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1073
    const-string v2, "html_wrapper"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1075
    const-string v2, "html_wrapper"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Lcom/my/target/dh;->setHtml(Ljava/lang/String;)V

    .line 1076
    const-string v2, "html_wrapper"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 1077
    invoke-virtual {p3, v0}, Lcom/my/target/dh;->setRawData(Lorg/json/JSONObject;)V

    .line 1091
    :cond_3
    invoke-static {p3, p2, p4, p5}, Lcom/my/target/de;->a(Lcom/my/target/dh;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/de;

    move-result-object v2

    .line 1092
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v0, v4, :cond_7

    .line 1094
    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 1095
    if-eqz v4, :cond_4

    .line 1097
    invoke-static {}, Lcom/my/target/core/models/banners/c;->newBanner()Lcom/my/target/core/models/banners/c;

    move-result-object v5

    .line 1098
    invoke-virtual {v2, v4, v5}, Lcom/my/target/de;->a(Lorg/json/JSONObject;Lcom/my/target/core/models/banners/c;)Z

    move-result v4

    .line 1099
    if-eqz v4, :cond_4

    .line 1101
    invoke-virtual {p3, v5}, Lcom/my/target/dh;->a(Lcom/my/target/core/models/banners/c;)V

    .line 1092
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1081
    :cond_5
    const-string v0, "Section has no HTML_WRAPPER field, required for viewType = html"

    .line 1083
    const-string v2, "Required field"

    invoke-static {v2}, Lcom/my/target/az;->y(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/my/target/az;->z(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    .line 1084
    invoke-virtual {p4}, Lcom/my/target/b;->getSlotId()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/my/target/az;->h(I)Lcom/my/target/az;

    move-result-object v0

    .line 1085
    invoke-virtual {p2}, Lcom/my/target/ae;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/my/target/az;->A(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    .line 1086
    invoke-virtual {v0, p5}, Lcom/my/target/az;->e(Landroid/content/Context;)V

    :cond_6
    move-object p3, v1

    .line 26
    goto :goto_0

    .line 1105
    :cond_7
    invoke-virtual {p3}, Lcom/my/target/dh;->getBannersCount()I

    move-result v0

    if-lez v0, :cond_6

    goto :goto_0
.end method
