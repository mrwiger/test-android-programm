.class Lru/cn/tv/mobile/BaseDrawerActivity$1;
.super Landroid/support/v7/app/ActionBarDrawerToggle;
.source "BaseDrawerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/mobile/BaseDrawerActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/mobile/BaseDrawerActivity;


# direct methods
.method constructor <init>(Lru/cn/tv/mobile/BaseDrawerActivity;Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;Landroid/support/v7/widget/Toolbar;II)V
    .locals 6
    .param p1, "this$0"    # Lru/cn/tv/mobile/BaseDrawerActivity;
    .param p2, "arg0"    # Landroid/app/Activity;
    .param p3, "arg1"    # Landroid/support/v4/widget/DrawerLayout;
    .param p4, "arg2"    # Landroid/support/v7/widget/Toolbar;
    .param p5, "arg3"    # I
    .param p6, "arg4"    # I

    .prologue
    .line 91
    iput-object p1, p0, Lru/cn/tv/mobile/BaseDrawerActivity$1;->this$0:Lru/cn/tv/mobile/BaseDrawerActivity;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/app/ActionBarDrawerToggle;-><init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;Landroid/support/v7/widget/Toolbar;II)V

    return-void
.end method


# virtual methods
.method public onDrawerClosed(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 94
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarDrawerToggle;->onDrawerClosed(Landroid/view/View;)V

    .line 95
    iget-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity$1;->this$0:Lru/cn/tv/mobile/BaseDrawerActivity;

    invoke-virtual {v0}, Lru/cn/tv/mobile/BaseDrawerActivity;->drawerClosed()V

    .line 96
    return-void
.end method

.method public onDrawerOpened(Landroid/view/View;)V
    .locals 1
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    .line 100
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarDrawerToggle;->onDrawerOpened(Landroid/view/View;)V

    .line 101
    iget-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity$1;->this$0:Lru/cn/tv/mobile/BaseDrawerActivity;

    invoke-virtual {v0}, Lru/cn/tv/mobile/BaseDrawerActivity;->drawerOpened()V

    .line 102
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 106
    iget-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity$1;->this$0:Lru/cn/tv/mobile/BaseDrawerActivity;

    iget-object v0, v0, Lru/cn/tv/mobile/BaseDrawerActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBarDrawerToggle;->isDrawerIndicatorEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarDrawerToggle;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 110
    :goto_0
    return v0

    .line 109
    :cond_0
    iget-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity$1;->this$0:Lru/cn/tv/mobile/BaseDrawerActivity;

    invoke-virtual {v0}, Lru/cn/tv/mobile/BaseDrawerActivity;->onBackPressed()V

    .line 110
    const/4 v0, 0x1

    goto :goto_0
.end method
