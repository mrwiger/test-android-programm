.class final synthetic Lru/cn/player/AudioTrackSelector$$Lambda$0;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/annimon/stream/function/Function;


# instance fields
.field private final arg$1:Lru/cn/player/ITrackSelector$TrackNameGenerator;


# direct methods
.method private constructor <init>(Lru/cn/player/ITrackSelector$TrackNameGenerator;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lru/cn/player/AudioTrackSelector$$Lambda$0;->arg$1:Lru/cn/player/ITrackSelector$TrackNameGenerator;

    return-void
.end method

.method static get$Lambda(Lru/cn/player/ITrackSelector$TrackNameGenerator;)Lcom/annimon/stream/function/Function;
    .locals 1

    new-instance v0, Lru/cn/player/AudioTrackSelector$$Lambda$0;

    invoke-direct {v0, p0}, Lru/cn/player/AudioTrackSelector$$Lambda$0;-><init>(Lru/cn/player/ITrackSelector$TrackNameGenerator;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lru/cn/player/AudioTrackSelector$$Lambda$0;->arg$1:Lru/cn/player/ITrackSelector$TrackNameGenerator;

    check-cast p1, Lru/cn/player/TrackInfo;

    invoke-interface {v0, p1}, Lru/cn/player/ITrackSelector$TrackNameGenerator;->getName(Lru/cn/player/TrackInfo;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
