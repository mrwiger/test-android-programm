.class Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;
.super Ljava/lang/Object;
.source "MyTargetVideoPresenter.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lru/cn/ad/video/ui/MediaControl$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;,
        Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$PresenterListener;
    }
.end annotation


# instance fields
.field private final adContainer:Landroid/view/ViewGroup;

.field private adStarted:Z

.field private final context:Landroid/content/Context;

.field private firstQuartileReady:Z

.field private final handler:Landroid/os/Handler;

.field private final instreamAd:Lcom/my/target/instreamads/InstreamAd;

.field private listener:Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$PresenterListener;

.field private mediaControl:Lru/cn/ad/video/ui/MediaControl;

.field private midpointReady:Z

.field private final player:Lru/cn/player/SimplePlayer;

.field private thirdQuartileReady:Z

.field private final tracker:Lru/cn/ad/video/mytarget/Tracker;


# direct methods
.method public constructor <init>(Lru/cn/ad/video/RenderingSettings;Lcom/my/target/instreamads/InstreamAd;Lru/cn/ad/video/mytarget/Tracker;)V
    .locals 2
    .param p1, "settings"    # Lru/cn/ad/video/RenderingSettings;
    .param p2, "instreamAd"    # Lcom/my/target/instreamads/InstreamAd;
    .param p3, "tracker"    # Lru/cn/ad/video/mytarget/Tracker;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iget-object v1, p1, Lru/cn/ad/video/RenderingSettings;->player:Lru/cn/player/SimplePlayer;

    iput-object v1, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->player:Lru/cn/player/SimplePlayer;

    .line 52
    iget-object v1, p1, Lru/cn/ad/video/RenderingSettings;->container:Landroid/view/ViewGroup;

    iput-object v1, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->adContainer:Landroid/view/ViewGroup;

    .line 53
    iput-object p3, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->tracker:Lru/cn/ad/video/mytarget/Tracker;

    .line 54
    iget-object v1, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v1}, Lru/cn/player/SimplePlayer;->getContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->context:Landroid/content/Context;

    .line 55
    iput-object p2, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->instreamAd:Lcom/my/target/instreamads/InstreamAd;

    .line 58
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->handler:Landroid/os/Handler;

    .line 60
    new-instance v0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;

    iget-object v1, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->player:Lru/cn/player/SimplePlayer;

    invoke-direct {v0, v1}, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$MTGVideoPlayer;-><init>(Lru/cn/player/SimplePlayer;)V

    .line 61
    .local v0, "adPlayer":Lcom/my/target/instreamads/InstreamAdPlayer;
    invoke-virtual {p2, v0}, Lcom/my/target/instreamads/InstreamAd;->setPlayer(Lcom/my/target/instreamads/InstreamAdPlayer;)V

    .line 62
    return-void
.end method

.method private trackEvent(Lru/cn/domain/statistics/inetra/AdvEvent;)V
    .locals 1
    .param p1, "eventName"    # Lru/cn/domain/statistics/inetra/AdvEvent;

    .prologue
    .line 311
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->tracker:Lru/cn/ad/video/mytarget/Tracker;

    invoke-interface {v0, p1}, Lru/cn/ad/video/mytarget/Tracker;->trackEvent(Lru/cn/domain/statistics/inetra/AdvEvent;)V

    .line 312
    return-void
.end method

.method private updateProgress()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 83
    iget-object v4, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v4}, Lru/cn/player/SimplePlayer;->getCurrentPosition()I

    move-result v1

    .line 84
    .local v1, "position":I
    iget-object v4, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v4}, Lru/cn/player/SimplePlayer;->getDuration()I

    move-result v0

    .line 85
    .local v0, "duration":I
    if-gtz v0, :cond_0

    .line 110
    :goto_0
    return-void

    .line 88
    :cond_0
    mul-int/lit8 v4, v1, 0x64

    div-int/2addr v4, v0

    int-to-double v2, v4

    .line 89
    .local v2, "progress":D
    iget-boolean v4, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->adStarted:Z

    if-nez v4, :cond_1

    .line 90
    iput-boolean v6, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->adStarted:Z

    .line 91
    sget-object v4, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_START:Lru/cn/domain/statistics/inetra/AdvEvent;

    invoke-direct {p0, v4}, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->trackEvent(Lru/cn/domain/statistics/inetra/AdvEvent;)V

    .line 94
    :cond_1
    const-wide/high16 v4, 0x4039000000000000L    # 25.0

    cmpl-double v4, v2, v4

    if-ltz v4, :cond_2

    iget-boolean v4, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->firstQuartileReady:Z

    if-nez v4, :cond_2

    .line 95
    iput-boolean v6, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->firstQuartileReady:Z

    .line 96
    sget-object v4, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_FIRST_QUARTILE:Lru/cn/domain/statistics/inetra/AdvEvent;

    invoke-direct {p0, v4}, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->trackEvent(Lru/cn/domain/statistics/inetra/AdvEvent;)V

    .line 99
    :cond_2
    const-wide/high16 v4, 0x4049000000000000L    # 50.0

    cmpl-double v4, v2, v4

    if-ltz v4, :cond_3

    iget-boolean v4, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->midpointReady:Z

    if-nez v4, :cond_3

    .line 100
    iput-boolean v6, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->midpointReady:Z

    .line 101
    sget-object v4, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_MIDPOINT:Lru/cn/domain/statistics/inetra/AdvEvent;

    invoke-direct {p0, v4}, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->trackEvent(Lru/cn/domain/statistics/inetra/AdvEvent;)V

    .line 104
    :cond_3
    const-wide v4, 0x4052c00000000000L    # 75.0

    cmpl-double v4, v2, v4

    if-ltz v4, :cond_4

    iget-boolean v4, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->thirdQuartileReady:Z

    if-nez v4, :cond_4

    .line 105
    iput-boolean v6, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->thirdQuartileReady:Z

    .line 106
    sget-object v4, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_THIRD_QUARTILE:Lru/cn/domain/statistics/inetra/AdvEvent;

    invoke-direct {p0, v4}, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->trackEvent(Lru/cn/domain/statistics/inetra/AdvEvent;)V

    .line 109
    :cond_4
    iget-object v4, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->mediaControl:Lru/cn/ad/video/ui/MediaControl;

    int-to-long v6, v1

    invoke-interface {v4, v6, v7}, Lru/cn/ad/video/ui/MediaControl;->setPosition(J)V

    goto :goto_0
.end method


# virtual methods
.method public destroy()V
    .locals 4

    .prologue
    .line 73
    iget-object v2, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->mediaControl:Lru/cn/ad/video/ui/MediaControl;

    if-eqz v2, :cond_0

    .line 74
    iget-object v2, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->mediaControl:Lru/cn/ad/video/ui/MediaControl;

    invoke-interface {v2}, Lru/cn/ad/video/ui/MediaControl;->getView()Landroid/view/View;

    move-result-object v0

    .line 75
    .local v0, "adWrapper":Landroid/view/View;
    iget-object v2, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->adContainer:Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    iget-object v1, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->adContainer:Landroid/view/ViewGroup;

    .line 76
    .local v1, "parentLayout":Landroid/view/ViewGroup;
    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 79
    .end local v0    # "adWrapper":Landroid/view/View;
    .end local v1    # "parentLayout":Landroid/view/ViewGroup;
    :cond_0
    iget-object v2, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->handler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 80
    return-void

    .line 75
    .restart local v0    # "adWrapper":Landroid/view/View;
    :cond_1
    iget-object v2, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v2}, Lru/cn/player/SimplePlayer;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    move-object v1, v2

    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v0, 0x1

    .line 153
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 161
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 155
    :pswitch_0
    invoke-direct {p0}, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->updateProgress()V

    .line 156
    iget-object v1, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->handler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 153
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onClicked()V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->tracker:Lru/cn/ad/video/mytarget/Tracker;

    sget-object v1, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_CLICK:Lru/cn/domain/statistics/inetra/AdvEvent;

    invoke-interface {v0, v1}, Lru/cn/ad/video/mytarget/Tracker;->trackEvent(Lru/cn/domain/statistics/inetra/AdvEvent;)V

    .line 126
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->instreamAd:Lcom/my/target/instreamads/InstreamAd;

    invoke-virtual {v0}, Lcom/my/target/instreamads/InstreamAd;->handleClick()V

    .line 128
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->listener:Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$PresenterListener;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->listener:Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$PresenterListener;

    invoke-interface {v0}, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$PresenterListener;->onComplete()V

    .line 130
    :cond_0
    return-void
.end method

.method public onSkipped()V
    .locals 1

    .prologue
    .line 114
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_SKIP:Lru/cn/domain/statistics/inetra/AdvEvent;

    invoke-direct {p0, v0}, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->trackEvent(Lru/cn/domain/statistics/inetra/AdvEvent;)V

    .line 116
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->instreamAd:Lcom/my/target/instreamads/InstreamAd;

    invoke-virtual {v0}, Lcom/my/target/instreamads/InstreamAd;->skip()V

    .line 118
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->listener:Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$PresenterListener;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->listener:Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$PresenterListener;

    invoke-interface {v0}, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$PresenterListener;->onComplete()V

    .line 120
    :cond_0
    return-void
.end method

.method public play()V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->instreamAd:Lcom/my/target/instreamads/InstreamAd;

    invoke-virtual {v0}, Lcom/my/target/instreamads/InstreamAd;->startPreroll()V

    .line 70
    return-void
.end method

.method setBanner(Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;)V
    .locals 9
    .param p1, "instreamAdBanner"    # Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;

    .prologue
    const/4 v8, 0x1

    .line 133
    new-instance v4, Lru/cn/ad/video/ui/MobileVideoMediaControl;

    iget-object v5, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->context:Landroid/content/Context;

    invoke-direct {v4, v5}, Lru/cn/ad/video/ui/MobileVideoMediaControl;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->mediaControl:Lru/cn/ad/video/ui/MediaControl;

    .line 134
    iget-object v4, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->mediaControl:Lru/cn/ad/video/ui/MediaControl;

    invoke-interface {v4, v8}, Lru/cn/ad/video/ui/MediaControl;->setClickable(Z)V

    .line 135
    iget-object v4, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->mediaControl:Lru/cn/ad/video/ui/MediaControl;

    invoke-interface {v4, p0}, Lru/cn/ad/video/ui/MediaControl;->setListener(Lru/cn/ad/video/ui/MediaControl$Listener;)V

    .line 137
    iget-object v4, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->mediaControl:Lru/cn/ad/video/ui/MediaControl;

    invoke-interface {v4}, Lru/cn/ad/video/ui/MediaControl;->getView()Landroid/view/View;

    move-result-object v0

    .line 138
    .local v0, "adWrapper":Landroid/view/View;
    iget-object v4, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->adContainer:Landroid/view/ViewGroup;

    if-eqz v4, :cond_1

    iget-object v1, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->adContainer:Landroid/view/ViewGroup;

    .line 139
    .local v1, "parentLayout":Landroid/view/ViewGroup;
    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 141
    iget-object v4, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v4}, Lru/cn/player/SimplePlayer;->getDuration()I

    move-result v4

    int-to-long v2, v4

    .line 142
    .local v2, "duration":J
    iget-boolean v4, p1, Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;->allowClose:Z

    if-eqz v4, :cond_0

    .line 143
    iget-object v4, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->mediaControl:Lru/cn/ad/video/ui/MediaControl;

    iget v5, p1, Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;->allowCloseDelay:F

    const/high16 v6, 0x447a0000    # 1000.0f

    mul-float/2addr v5, v6

    float-to-long v6, v5

    invoke-interface {v4, v6, v7}, Lru/cn/ad/video/ui/MediaControl;->setSkipPositionMs(J)V

    .line 146
    :cond_0
    iget-object v4, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->mediaControl:Lru/cn/ad/video/ui/MediaControl;

    invoke-interface {v4, v2, v3}, Lru/cn/ad/video/ui/MediaControl;->setDuration(J)V

    .line 148
    iget-object v4, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->handler:Landroid/os/Handler;

    invoke-virtual {v4, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 149
    return-void

    .line 138
    .end local v1    # "parentLayout":Landroid/view/ViewGroup;
    .end local v2    # "duration":J
    :cond_1
    iget-object v4, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v4}, Lru/cn/player/SimplePlayer;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    move-object v1, v4

    goto :goto_0
.end method

.method public setListener(Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$PresenterListener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$PresenterListener;

    .prologue
    .line 65
    iput-object p1, p0, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->listener:Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$PresenterListener;

    .line 66
    return-void
.end method
