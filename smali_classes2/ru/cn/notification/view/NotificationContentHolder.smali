.class public Lru/cn/notification/view/NotificationContentHolder;
.super Lru/cn/notification/view/BaseNotificationsHolder;
.source "NotificationContentHolder.java"


# instance fields
.field private close:Landroid/widget/TextView;

.field private listener:Lru/cn/notification/NotificationButtonClickListener;

.field private message:Landroid/widget/TextView;

.field private positive:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;Lru/cn/notification/NotificationButtonClickListener;)V
    .locals 1
    .param p1, "itemView"    # Landroid/view/View;
    .param p2, "listener"    # Lru/cn/notification/NotificationButtonClickListener;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lru/cn/notification/view/BaseNotificationsHolder;-><init>(Landroid/view/View;)V

    .line 18
    iput-object p2, p0, Lru/cn/notification/view/NotificationContentHolder;->listener:Lru/cn/notification/NotificationButtonClickListener;

    .line 19
    const v0, 0x7f090153

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/notification/view/NotificationContentHolder;->message:Landroid/widget/TextView;

    .line 20
    const v0, 0x7f09004d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/notification/view/NotificationContentHolder;->positive:Landroid/widget/TextView;

    .line 21
    const v0, 0x7f09004c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/notification/view/NotificationContentHolder;->close:Landroid/widget/TextView;

    .line 22
    return-void
.end method


# virtual methods
.method final synthetic lambda$set$0$NotificationContentHolder(Lru/cn/notifications/entity/INotification;Landroid/view/View;)V
    .locals 1
    .param p1, "notification"    # Lru/cn/notifications/entity/INotification;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 26
    iget-object v0, p0, Lru/cn/notification/view/NotificationContentHolder;->listener:Lru/cn/notification/NotificationButtonClickListener;

    invoke-interface {v0, p1}, Lru/cn/notification/NotificationButtonClickListener;->pressPositive(Lru/cn/notifications/entity/INotification;)V

    return-void
.end method

.method final synthetic lambda$set$1$NotificationContentHolder(Lru/cn/notifications/entity/INotification;Landroid/view/View;)V
    .locals 1
    .param p1, "notification"    # Lru/cn/notifications/entity/INotification;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/notification/view/NotificationContentHolder;->listener:Lru/cn/notification/NotificationButtonClickListener;

    invoke-interface {v0, p1}, Lru/cn/notification/NotificationButtonClickListener;->pressClosed(Lru/cn/notifications/entity/INotification;)V

    return-void
.end method

.method public set(Lru/cn/notifications/entity/INotification;)V
    .locals 2
    .param p1, "notification"    # Lru/cn/notifications/entity/INotification;

    .prologue
    .line 25
    iget-object v0, p0, Lru/cn/notification/view/NotificationContentHolder;->message:Landroid/widget/TextView;

    invoke-interface {p1}, Lru/cn/notifications/entity/INotification;->message()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 26
    iget-object v0, p0, Lru/cn/notification/view/NotificationContentHolder;->positive:Landroid/widget/TextView;

    new-instance v1, Lru/cn/notification/view/NotificationContentHolder$$Lambda$0;

    invoke-direct {v1, p0, p1}, Lru/cn/notification/view/NotificationContentHolder$$Lambda$0;-><init>(Lru/cn/notification/view/NotificationContentHolder;Lru/cn/notifications/entity/INotification;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 27
    iget-object v0, p0, Lru/cn/notification/view/NotificationContentHolder;->close:Landroid/widget/TextView;

    new-instance v1, Lru/cn/notification/view/NotificationContentHolder$$Lambda$1;

    invoke-direct {v1, p0, p1}, Lru/cn/notification/view/NotificationContentHolder$$Lambda$1;-><init>(Lru/cn/notification/view/NotificationContentHolder;Lru/cn/notifications/entity/INotification;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 28
    return-void
.end method
