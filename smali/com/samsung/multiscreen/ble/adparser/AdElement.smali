.class public abstract Lcom/samsung/multiscreen/ble/adparser/AdElement;
.super Ljava/lang/Object;
.source "AdElement.java"


# static fields
.field private static hexDigits:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    const-string v0, "0123456789ABCDEF"

    sput-object v0, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigits:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static hex16(I)Ljava/lang/String;
    .locals 2
    .param p0, "v"    # I

    .prologue
    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0xc

    invoke-static {p0, v1}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x8

    .line 40
    invoke-static {p0, v1}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x4

    .line 41
    invoke-static {p0, v1}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    .line 42
    invoke-static {p0, v1}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static hex32(I)Ljava/lang/String;
    .locals 2
    .param p0, "v"    # I

    .prologue
    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x1c

    invoke-static {p0, v1}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x18

    .line 47
    invoke-static {p0, v1}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x14

    .line 48
    invoke-static {p0, v1}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x10

    .line 49
    invoke-static {p0, v1}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0xc

    .line 50
    invoke-static {p0, v1}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x8

    .line 51
    invoke-static {p0, v1}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x4

    .line 52
    invoke-static {p0, v1}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    .line 53
    invoke-static {p0, v1}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static hex8(I)Ljava/lang/String;
    .locals 2
    .param p0, "v"    # I

    .prologue
    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x4

    .line 34
    invoke-static {p0, v1}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    .line 35
    invoke-static {p0, v1}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static hexDigit(II)C
    .locals 2
    .param p0, "v"    # I
    .param p1, "shift"    # I

    .prologue
    .line 28
    shr-int v1, p0, p1

    and-int/lit8 v0, v1, 0xf

    .line 29
    .local v0, "v1":I
    sget-object v1, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigits:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    return v1
.end method

.method public static uuid128(IIII)Ljava/lang/String;
    .locals 9
    .param p0, "v1"    # I
    .param p1, "v2"    # I
    .param p2, "v3"    # I
    .param p3, "v4"    # I

    .prologue
    const/16 v8, 0xc

    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x0

    const v4, 0xffff

    .line 57
    shr-int/lit8 v3, p1, 0x10

    and-int v0, v3, v4

    .line 58
    .local v0, "v2h":I
    and-int v1, p1, v4

    .line 59
    .local v1, "v2l":I
    shr-int/lit8 v3, p2, 0x10

    and-int v2, v3, v4

    .line 60
    .local v2, "v3h":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 61
    invoke-static {p0}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hex32(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 63
    invoke-static {v0}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hex16(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 65
    invoke-static {v1}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hex16(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 67
    invoke-static {v2}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hex16(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 69
    invoke-static {p2, v8}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 70
    invoke-static {p2, v7}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 71
    invoke-static {p2, v6}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 72
    invoke-static {p2, v5}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x1c

    .line 73
    invoke-static {p3, v4}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x18

    .line 74
    invoke-static {p3, v4}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x14

    .line 75
    invoke-static {p3, v4}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x10

    .line 76
    invoke-static {p3, v4}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 77
    invoke-static {p3, v8}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 78
    invoke-static {p3, v7}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 79
    invoke-static {p3, v6}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 80
    invoke-static {p3, v5}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->hexDigit(II)C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method


# virtual methods
.method public abstract toString()Ljava/lang/String;
.end method
