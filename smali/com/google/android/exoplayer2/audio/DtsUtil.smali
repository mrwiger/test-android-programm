.class public final Lcom/google/android/exoplayer2/audio/DtsUtil;
.super Ljava/lang/Object;
.source "DtsUtil.java"


# static fields
.field private static final CHANNELS_BY_AMODE:[I

.field private static final SAMPLE_RATE_BY_SFREQ:[I

.field private static final TWICE_BITRATE_KBPS_BY_RATE:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 42
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/exoplayer2/audio/DtsUtil;->CHANNELS_BY_AMODE:[I

    .line 48
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/exoplayer2/audio/DtsUtil;->SAMPLE_RATE_BY_SFREQ:[I

    .line 54
    const/16 v0, 0x1d

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/exoplayer2/audio/DtsUtil;->TWICE_BITRATE_KBPS_BY_RATE:[I

    return-void

    .line 42
    :array_0
    .array-data 4
        0x1
        0x2
        0x2
        0x2
        0x2
        0x3
        0x3
        0x4
        0x4
        0x5
        0x6
        0x6
        0x6
        0x7
        0x8
        0x8
    .end array-data

    .line 48
    :array_1
    .array-data 4
        -0x1
        0x1f40
        0x3e80
        0x7d00
        -0x1
        -0x1
        0x2b11
        0x5622
        0xac44
        -0x1
        -0x1
        0x2ee0
        0x5dc0
        0xbb80
        -0x1
        -0x1
    .end array-data

    .line 54
    :array_2
    .array-data 4
        0x40
        0x70
        0x80
        0xc0
        0xe0
        0x100
        0x180
        0x1c0
        0x200
        0x280
        0x300
        0x380
        0x400
        0x480
        0x500
        0x600
        0x780
        0x800
        0x900
        0xa00
        0xa80
        0xb00
        0xb07
        0xb80
        0xc00
        0xf00
        0x1000
        0x1800
        0x1e00
    .end array-data
.end method

.method public static getDtsFrameSize([B)I
    .locals 6
    .param p0, "data"    # [B

    .prologue
    const/4 v5, 0x7

    const/4 v4, 0x6

    .line 160
    const/4 v1, 0x0

    .line 161
    .local v1, "uses14BitPerWord":Z
    const/4 v2, 0x0

    aget-byte v2, p0, v2

    sparse-switch v2, :sswitch_data_0

    .line 175
    const/4 v2, 0x5

    aget-byte v2, p0, v2

    and-int/lit8 v2, v2, 0x3

    shl-int/lit8 v2, v2, 0xc

    aget-byte v3, p0, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x4

    or-int/2addr v2, v3

    aget-byte v3, p0, v5

    and-int/lit16 v3, v3, 0xf0

    shr-int/lit8 v3, v3, 0x4

    or-int/2addr v2, v3

    add-int/lit8 v0, v2, 0x1

    .line 179
    .local v0, "fsize":I
    :goto_0
    if-eqz v1, :cond_0

    mul-int/lit8 v2, v0, 0x10

    div-int/lit8 v0, v2, 0xe

    .end local v0    # "fsize":I
    :cond_0
    return v0

    .line 163
    :sswitch_0
    aget-byte v2, p0, v4

    and-int/lit8 v2, v2, 0x3

    shl-int/lit8 v2, v2, 0xc

    aget-byte v3, p0, v5

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x4

    or-int/2addr v2, v3

    const/16 v3, 0x8

    aget-byte v3, p0, v3

    and-int/lit8 v3, v3, 0x3c

    shr-int/lit8 v3, v3, 0x2

    or-int/2addr v2, v3

    add-int/lit8 v0, v2, 0x1

    .line 164
    .restart local v0    # "fsize":I
    const/4 v1, 0x1

    .line 165
    goto :goto_0

    .line 167
    .end local v0    # "fsize":I
    :sswitch_1
    const/4 v2, 0x4

    aget-byte v2, p0, v2

    and-int/lit8 v2, v2, 0x3

    shl-int/lit8 v2, v2, 0xc

    aget-byte v3, p0, v5

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x4

    or-int/2addr v2, v3

    aget-byte v3, p0, v4

    and-int/lit16 v3, v3, 0xf0

    shr-int/lit8 v3, v3, 0x4

    or-int/2addr v2, v3

    add-int/lit8 v0, v2, 0x1

    .line 168
    .restart local v0    # "fsize":I
    goto :goto_0

    .line 170
    .end local v0    # "fsize":I
    :sswitch_2
    aget-byte v2, p0, v5

    and-int/lit8 v2, v2, 0x3

    shl-int/lit8 v2, v2, 0xc

    aget-byte v3, p0, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x4

    or-int/2addr v2, v3

    const/16 v3, 0x9

    aget-byte v3, p0, v3

    and-int/lit8 v3, v3, 0x3c

    shr-int/lit8 v3, v3, 0x2

    or-int/2addr v2, v3

    add-int/lit8 v0, v2, 0x1

    .line 171
    .restart local v0    # "fsize":I
    const/4 v1, 0x1

    .line 172
    goto :goto_0

    .line 161
    nop

    :sswitch_data_0
    .sparse-switch
        -0x2 -> :sswitch_1
        -0x1 -> :sswitch_2
        0x1f -> :sswitch_0
    .end sparse-switch
.end method

.method private static getNormalizedFrameHeader([B)Lcom/google/android/exoplayer2/util/ParsableBitArray;
    .locals 8
    .param p0, "frameHeader"    # [B

    .prologue
    const/16 v7, 0xe

    const/4 v6, 0x0

    .line 183
    aget-byte v4, p0, v6

    const/16 v5, 0x7f

    if-ne v4, v5, :cond_0

    .line 185
    new-instance v0, Lcom/google/android/exoplayer2/util/ParsableBitArray;

    invoke-direct {v0, p0}, Lcom/google/android/exoplayer2/util/ParsableBitArray;-><init>([B)V

    .line 207
    :goto_0
    return-object v0

    .line 188
    :cond_0
    array-length v4, p0

    invoke-static {p0, v4}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object p0

    .line 189
    invoke-static {p0}, Lcom/google/android/exoplayer2/audio/DtsUtil;->isLittleEndianFrameHeader([B)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 191
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v4, p0

    add-int/lit8 v4, v4, -0x1

    if-ge v1, v4, :cond_1

    .line 192
    aget-byte v3, p0, v1

    .line 193
    .local v3, "temp":B
    add-int/lit8 v4, v1, 0x1

    aget-byte v4, p0, v4

    aput-byte v4, p0, v1

    .line 194
    add-int/lit8 v4, v1, 0x1

    aput-byte v3, p0, v4

    .line 191
    add-int/lit8 v1, v1, 0x2

    goto :goto_1

    .line 197
    .end local v1    # "i":I
    .end local v3    # "temp":B
    :cond_1
    new-instance v0, Lcom/google/android/exoplayer2/util/ParsableBitArray;

    invoke-direct {v0, p0}, Lcom/google/android/exoplayer2/util/ParsableBitArray;-><init>([B)V

    .line 198
    .local v0, "frameBits":Lcom/google/android/exoplayer2/util/ParsableBitArray;
    aget-byte v4, p0, v6

    const/16 v5, 0x1f

    if-ne v4, v5, :cond_2

    .line 200
    new-instance v2, Lcom/google/android/exoplayer2/util/ParsableBitArray;

    invoke-direct {v2, p0}, Lcom/google/android/exoplayer2/util/ParsableBitArray;-><init>([B)V

    .line 201
    .local v2, "scratchBits":Lcom/google/android/exoplayer2/util/ParsableBitArray;
    :goto_2
    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/ParsableBitArray;->bitsLeft()I

    move-result v4

    const/16 v5, 0x10

    if-lt v4, v5, :cond_2

    .line 202
    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Lcom/google/android/exoplayer2/util/ParsableBitArray;->skipBits(I)V

    .line 203
    invoke-virtual {v2, v7}, Lcom/google/android/exoplayer2/util/ParsableBitArray;->readBits(I)I

    move-result v4

    invoke-virtual {v0, v4, v7}, Lcom/google/android/exoplayer2/util/ParsableBitArray;->putInt(II)V

    goto :goto_2

    .line 206
    .end local v2    # "scratchBits":Lcom/google/android/exoplayer2/util/ParsableBitArray;
    :cond_2
    invoke-virtual {v0, p0}, Lcom/google/android/exoplayer2/util/ParsableBitArray;->reset([B)V

    goto :goto_0
.end method

.method private static isLittleEndianFrameHeader([B)Z
    .locals 3
    .param p0, "frameHeader"    # [B

    .prologue
    const/4 v0, 0x0

    .line 211
    aget-byte v1, p0, v0

    const/4 v2, -0x2

    if-eq v1, v2, :cond_0

    aget-byte v1, p0, v0

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public static isSyncWord(I)Z
    .locals 1
    .param p0, "word"    # I

    .prologue
    .line 66
    const v0, 0x7ffe8001

    if-eq p0, v0, :cond_0

    const v0, -0x180fe80

    if-eq p0, v0, :cond_0

    const v0, 0x1fffe800

    if-eq p0, v0, :cond_0

    const v0, -0xe0ff18

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDtsAudioSampleCount(Ljava/nio/ByteBuffer;)I
    .locals 4
    .param p0, "buffer"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 133
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    .line 135
    .local v1, "position":I
    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 147
    add-int/lit8 v2, v1, 0x4

    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    and-int/lit8 v2, v2, 0x1

    shl-int/lit8 v2, v2, 0x6

    add-int/lit8 v3, v1, 0x5

    invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v3

    and-int/lit16 v3, v3, 0xfc

    shr-int/lit8 v3, v3, 0x2

    or-int v0, v2, v3

    .line 149
    .local v0, "nblks":I
    :goto_0
    add-int/lit8 v2, v0, 0x1

    mul-int/lit8 v2, v2, 0x20

    return v2

    .line 137
    .end local v0    # "nblks":I
    :sswitch_0
    add-int/lit8 v2, v1, 0x5

    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    and-int/lit8 v2, v2, 0x1

    shl-int/lit8 v2, v2, 0x6

    add-int/lit8 v3, v1, 0x4

    invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v3

    and-int/lit16 v3, v3, 0xfc

    shr-int/lit8 v3, v3, 0x2

    or-int v0, v2, v3

    .line 138
    .restart local v0    # "nblks":I
    goto :goto_0

    .line 140
    .end local v0    # "nblks":I
    :sswitch_1
    add-int/lit8 v2, v1, 0x4

    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    and-int/lit8 v2, v2, 0x7

    shl-int/lit8 v2, v2, 0x4

    add-int/lit8 v3, v1, 0x7

    invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v3

    and-int/lit8 v3, v3, 0x3c

    shr-int/lit8 v3, v3, 0x2

    or-int v0, v2, v3

    .line 141
    .restart local v0    # "nblks":I
    goto :goto_0

    .line 143
    .end local v0    # "nblks":I
    :sswitch_2
    add-int/lit8 v2, v1, 0x5

    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    and-int/lit8 v2, v2, 0x7

    shl-int/lit8 v2, v2, 0x4

    add-int/lit8 v3, v1, 0x6

    invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v3

    and-int/lit8 v3, v3, 0x3c

    shr-int/lit8 v3, v3, 0x2

    or-int v0, v2, v3

    .line 144
    .restart local v0    # "nblks":I
    goto :goto_0

    .line 135
    nop

    :sswitch_data_0
    .sparse-switch
        -0x2 -> :sswitch_0
        -0x1 -> :sswitch_1
        0x1f -> :sswitch_2
    .end sparse-switch
.end method

.method public static parseDtsAudioSampleCount([B)I
    .locals 4
    .param p0, "data"    # [B

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x4

    .line 107
    const/4 v1, 0x0

    aget-byte v1, p0, v1

    sparse-switch v1, :sswitch_data_0

    .line 119
    aget-byte v1, p0, v2

    and-int/lit8 v1, v1, 0x1

    shl-int/lit8 v1, v1, 0x6

    aget-byte v2, p0, v3

    and-int/lit16 v2, v2, 0xfc

    shr-int/lit8 v2, v2, 0x2

    or-int v0, v1, v2

    .line 121
    .local v0, "nblks":I
    :goto_0
    add-int/lit8 v1, v0, 0x1

    mul-int/lit8 v1, v1, 0x20

    return v1

    .line 109
    .end local v0    # "nblks":I
    :sswitch_0
    aget-byte v1, p0, v3

    and-int/lit8 v1, v1, 0x1

    shl-int/lit8 v1, v1, 0x6

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xfc

    shr-int/lit8 v2, v2, 0x2

    or-int v0, v1, v2

    .line 110
    .restart local v0    # "nblks":I
    goto :goto_0

    .line 112
    .end local v0    # "nblks":I
    :sswitch_1
    aget-byte v1, p0, v2

    and-int/lit8 v1, v1, 0x7

    shl-int/lit8 v1, v1, 0x4

    const/4 v2, 0x7

    aget-byte v2, p0, v2

    and-int/lit8 v2, v2, 0x3c

    shr-int/lit8 v2, v2, 0x2

    or-int v0, v1, v2

    .line 113
    .restart local v0    # "nblks":I
    goto :goto_0

    .line 115
    .end local v0    # "nblks":I
    :sswitch_2
    aget-byte v1, p0, v3

    and-int/lit8 v1, v1, 0x7

    shl-int/lit8 v1, v1, 0x4

    const/4 v2, 0x6

    aget-byte v2, p0, v2

    and-int/lit8 v2, v2, 0x3c

    shr-int/lit8 v2, v2, 0x2

    or-int v0, v1, v2

    .line 116
    .restart local v0    # "nblks":I
    goto :goto_0

    .line 107
    :sswitch_data_0
    .sparse-switch
        -0x2 -> :sswitch_0
        -0x1 -> :sswitch_1
        0x1f -> :sswitch_2
    .end sparse-switch
.end method

.method public static parseDtsFormat([BLjava/lang/String;Ljava/lang/String;Lcom/google/android/exoplayer2/drm/DrmInitData;)Lcom/google/android/exoplayer2/Format;
    .locals 15
    .param p0, "frame"    # [B
    .param p1, "trackId"    # Ljava/lang/String;
    .param p2, "language"    # Ljava/lang/String;
    .param p3, "drmInitData"    # Lcom/google/android/exoplayer2/drm/DrmInitData;

    .prologue
    .line 84
    invoke-static {p0}, Lcom/google/android/exoplayer2/audio/DtsUtil;->getNormalizedFrameHeader([B)Lcom/google/android/exoplayer2/util/ParsableBitArray;

    move-result-object v12

    .line 85
    .local v12, "frameBits":Lcom/google/android/exoplayer2/util/ParsableBitArray;
    const/16 v0, 0x3c

    invoke-virtual {v12, v0}, Lcom/google/android/exoplayer2/util/ParsableBitArray;->skipBits(I)V

    .line 86
    const/4 v0, 0x6

    invoke-virtual {v12, v0}, Lcom/google/android/exoplayer2/util/ParsableBitArray;->readBits(I)I

    move-result v11

    .line 87
    .local v11, "amode":I
    sget-object v0, Lcom/google/android/exoplayer2/audio/DtsUtil;->CHANNELS_BY_AMODE:[I

    aget v5, v0, v11

    .line 88
    .local v5, "channelCount":I
    const/4 v0, 0x4

    invoke-virtual {v12, v0}, Lcom/google/android/exoplayer2/util/ParsableBitArray;->readBits(I)I

    move-result v14

    .line 89
    .local v14, "sfreq":I
    sget-object v0, Lcom/google/android/exoplayer2/audio/DtsUtil;->SAMPLE_RATE_BY_SFREQ:[I

    aget v6, v0, v14

    .line 90
    .local v6, "sampleRate":I
    const/4 v0, 0x5

    invoke-virtual {v12, v0}, Lcom/google/android/exoplayer2/util/ParsableBitArray;->readBits(I)I

    move-result v13

    .line 91
    .local v13, "rate":I
    sget-object v0, Lcom/google/android/exoplayer2/audio/DtsUtil;->TWICE_BITRATE_KBPS_BY_RATE:[I

    array-length v0, v0

    if-lt v13, v0, :cond_0

    const/4 v3, -0x1

    .line 93
    .local v3, "bitrate":I
    :goto_0
    const/16 v0, 0xa

    invoke-virtual {v12, v0}, Lcom/google/android/exoplayer2/util/ParsableBitArray;->skipBits(I)V

    .line 94
    const/4 v0, 0x2

    invoke-virtual {v12, v0}, Lcom/google/android/exoplayer2/util/ParsableBitArray;->readBits(I)I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    add-int/2addr v5, v0

    .line 95
    const-string v1, "audio/vnd.dts"

    const/4 v2, 0x0

    const/4 v4, -0x1

    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p1

    move-object/from16 v8, p3

    move-object/from16 v10, p2

    invoke-static/range {v0 .. v10}, Lcom/google/android/exoplayer2/Format;->createAudioSampleFormat(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIILjava/util/List;Lcom/google/android/exoplayer2/drm/DrmInitData;ILjava/lang/String;)Lcom/google/android/exoplayer2/Format;

    move-result-object v0

    return-object v0

    .line 91
    .end local v3    # "bitrate":I
    :cond_0
    sget-object v0, Lcom/google/android/exoplayer2/audio/DtsUtil;->TWICE_BITRATE_KBPS_BY_RATE:[I

    aget v0, v0, v13

    mul-int/lit16 v0, v0, 0x3e8

    div-int/lit8 v3, v0, 0x2

    goto :goto_0

    .line 94
    .restart local v3    # "bitrate":I
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
