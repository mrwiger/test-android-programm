.class public final Lcom/google/obf/cu;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/aj;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/cu$b;,
        Lcom/google/obf/cu$a;
    }
.end annotation


# static fields
.field private static final a:[B

.field private static final b:[B

.field private static final c:Ljava/util/UUID;


# instance fields
.field private A:J

.field private B:Z

.field private C:J

.field private D:J

.field private E:J

.field private F:Lcom/google/obf/dr;

.field private G:Lcom/google/obf/dr;

.field private H:Z

.field private I:I

.field private J:J

.field private K:J

.field private L:I

.field private M:I

.field private N:[I

.field private O:I

.field private P:I

.field private Q:I

.field private R:I

.field private S:Z

.field private T:Z

.field private U:Z

.field private V:Z

.field private W:B

.field private X:I

.field private Y:I

.field private Z:I

.field private aa:Z

.field private ab:Z

.field private ac:Lcom/google/obf/al;

.field private final d:Lcom/google/obf/cq;

.field private final e:Lcom/google/obf/ct;

.field private final f:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/obf/cu$b;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Z

.field private final h:Lcom/google/obf/dw;

.field private final i:Lcom/google/obf/dw;

.field private final j:Lcom/google/obf/dw;

.field private final k:Lcom/google/obf/dw;

.field private final l:Lcom/google/obf/dw;

.field private final m:Lcom/google/obf/dw;

.field private final n:Lcom/google/obf/dw;

.field private final o:Lcom/google/obf/dw;

.field private final p:Lcom/google/obf/dw;

.field private q:Ljava/nio/ByteBuffer;

.field private r:J

.field private s:J

.field private t:J

.field private u:J

.field private v:J

.field private w:Lcom/google/obf/cu$b;

.field private x:Z

.field private y:Z

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 599
    const/16 v0, 0x20

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/obf/cu;->a:[B

    .line 600
    const/16 v0, 0xc

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/obf/cu;->b:[B

    .line 601
    new-instance v0, Ljava/util/UUID;

    const-wide v2, 0x100000000001000L

    const-wide v4, -0x7fffff55ffc7648fL    # -3.607411173533E-312

    invoke-direct {v0, v2, v3, v4, v5}, Ljava/util/UUID;-><init>(JJ)V

    sput-object v0, Lcom/google/obf/cu;->c:Ljava/util/UUID;

    return-void

    .line 599
    :array_0
    .array-data 1
        0x31t
        0xat
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x2ct
        0x30t
        0x30t
        0x30t
        0x20t
        0x2dt
        0x2dt
        0x3et
        0x20t
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x3at
        0x30t
        0x30t
        0x2ct
        0x30t
        0x30t
        0x30t
        0xat
    .end array-data

    .line 600
    :array_1
    .array-data 1
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1
    new-instance v0, Lcom/google/obf/cp;

    invoke-direct {v0}, Lcom/google/obf/cp;-><init>()V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/obf/cu;-><init>(Lcom/google/obf/cq;I)V

    .line 2
    return-void
.end method

.method constructor <init>(Lcom/google/obf/cq;I)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const-wide/16 v0, -0x1

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-wide v0, p0, Lcom/google/obf/cu;->r:J

    .line 5
    iput-wide v0, p0, Lcom/google/obf/cu;->s:J

    .line 6
    iput-wide v0, p0, Lcom/google/obf/cu;->t:J

    .line 7
    iput-wide v0, p0, Lcom/google/obf/cu;->u:J

    .line 8
    iput-wide v0, p0, Lcom/google/obf/cu;->v:J

    .line 9
    iput-wide v0, p0, Lcom/google/obf/cu;->C:J

    .line 10
    iput-wide v0, p0, Lcom/google/obf/cu;->D:J

    .line 11
    iput-wide v0, p0, Lcom/google/obf/cu;->E:J

    .line 12
    iput-object p1, p0, Lcom/google/obf/cu;->d:Lcom/google/obf/cq;

    .line 13
    iget-object v0, p0, Lcom/google/obf/cu;->d:Lcom/google/obf/cq;

    new-instance v1, Lcom/google/obf/cu$a;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/obf/cu$a;-><init>(Lcom/google/obf/cu;Lcom/google/obf/cu$1;)V

    invoke-interface {v0, v1}, Lcom/google/obf/cq;->a(Lcom/google/obf/cr;)V

    .line 14
    and-int/lit8 v0, p2, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/obf/cu;->g:Z

    .line 15
    new-instance v0, Lcom/google/obf/ct;

    invoke-direct {v0}, Lcom/google/obf/ct;-><init>()V

    iput-object v0, p0, Lcom/google/obf/cu;->e:Lcom/google/obf/ct;

    .line 16
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/obf/cu;->f:Landroid/util/SparseArray;

    .line 17
    new-instance v0, Lcom/google/obf/dw;

    invoke-direct {v0, v3}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    .line 18
    new-instance v0, Lcom/google/obf/dw;

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>([B)V

    iput-object v0, p0, Lcom/google/obf/cu;->k:Lcom/google/obf/dw;

    .line 19
    new-instance v0, Lcom/google/obf/dw;

    invoke-direct {v0, v3}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/cu;->l:Lcom/google/obf/dw;

    .line 20
    new-instance v0, Lcom/google/obf/dw;

    sget-object v1, Lcom/google/obf/du;->a:[B

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>([B)V

    iput-object v0, p0, Lcom/google/obf/cu;->h:Lcom/google/obf/dw;

    .line 21
    new-instance v0, Lcom/google/obf/dw;

    invoke-direct {v0, v3}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/cu;->i:Lcom/google/obf/dw;

    .line 22
    new-instance v0, Lcom/google/obf/dw;

    invoke-direct {v0}, Lcom/google/obf/dw;-><init>()V

    iput-object v0, p0, Lcom/google/obf/cu;->m:Lcom/google/obf/dw;

    .line 23
    new-instance v0, Lcom/google/obf/dw;

    invoke-direct {v0}, Lcom/google/obf/dw;-><init>()V

    iput-object v0, p0, Lcom/google/obf/cu;->n:Lcom/google/obf/dw;

    .line 24
    new-instance v0, Lcom/google/obf/dw;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/cu;->o:Lcom/google/obf/dw;

    .line 25
    new-instance v0, Lcom/google/obf/dw;

    invoke-direct {v0}, Lcom/google/obf/dw;-><init>()V

    iput-object v0, p0, Lcom/google/obf/cu;->p:Lcom/google/obf/dw;

    .line 26
    return-void

    .line 14
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/obf/ak;Lcom/google/obf/ar;I)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 522
    iget-object v0, p0, Lcom/google/obf/cu;->m:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->b()I

    move-result v0

    .line 523
    if-lez v0, :cond_0

    .line 524
    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 525
    iget-object v1, p0, Lcom/google/obf/cu;->m:Lcom/google/obf/dw;

    invoke-interface {p2, v1, v0}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 527
    :goto_0
    iget v1, p0, Lcom/google/obf/cu;->R:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/obf/cu;->R:I

    .line 528
    iget v1, p0, Lcom/google/obf/cu;->Z:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/obf/cu;->Z:I

    .line 529
    return v0

    .line 526
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p2, p1, p3, v0}, Lcom/google/obf/ar;->a(Lcom/google/obf/ak;IZ)I

    move-result v0

    goto :goto_0
.end method

.method private a(J)J
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    .line 564
    iget-wide v0, p0, Lcom/google/obf/cu;->t:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 565
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Can\'t scale timecode prior to timecodeScale being set."

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 566
    :cond_0
    iget-wide v2, p0, Lcom/google/obf/cu;->t:J

    const-wide/16 v4, 0x3e8

    move-wide v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/obf/ea;->a(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic a()Ljava/util/UUID;
    .locals 1

    .prologue
    .line 598
    sget-object v0, Lcom/google/obf/cu;->c:Ljava/util/UUID;

    return-object v0
.end method

.method private a(Lcom/google/obf/ak;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->c()I

    move-result v0

    if-lt v0, p2, :cond_0

    .line 396
    :goto_0
    return-void

    .line 390
    :cond_0
    iget-object v0, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->e()I

    move-result v0

    if-ge v0, p2, :cond_1

    .line 391
    iget-object v0, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    iget-object v1, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    iget-object v1, v1, Lcom/google/obf/dw;->a:[B

    iget-object v2, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    iget-object v2, v2, Lcom/google/obf/dw;->a:[B

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v2, p2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    iget-object v2, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    .line 392
    invoke-virtual {v2}, Lcom/google/obf/dw;->c()I

    move-result v2

    .line 393
    invoke-virtual {v0, v1, v2}, Lcom/google/obf/dw;->a([BI)V

    .line 394
    :cond_1
    iget-object v0, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    iget-object v1, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    invoke-virtual {v1}, Lcom/google/obf/dw;->c()I

    move-result v1

    iget-object v2, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    invoke-virtual {v2}, Lcom/google/obf/dw;->c()I

    move-result v2

    sub-int v2, p2, v2

    invoke-interface {p1, v0, v1, v2}, Lcom/google/obf/ak;->b([BII)V

    .line 395
    iget-object v0, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    invoke-virtual {v0, p2}, Lcom/google/obf/dw;->b(I)V

    goto :goto_0
.end method

.method private a(Lcom/google/obf/ak;Lcom/google/obf/cu$b;I)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x8

    const/4 v9, 0x4

    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 397
    const-string v0, "S_TEXT/UTF8"

    iget-object v3, p2, Lcom/google/obf/cu$b;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 398
    sget-object v0, Lcom/google/obf/cu;->a:[B

    array-length v0, v0

    add-int/2addr v0, p3

    .line 399
    iget-object v1, p0, Lcom/google/obf/cu;->n:Lcom/google/obf/dw;

    invoke-virtual {v1}, Lcom/google/obf/dw;->e()I

    move-result v1

    if-ge v1, v0, :cond_0

    .line 400
    iget-object v1, p0, Lcom/google/obf/cu;->n:Lcom/google/obf/dw;

    sget-object v3, Lcom/google/obf/cu;->a:[B

    add-int v4, v0, p3

    invoke-static {v3, v4}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v3

    iput-object v3, v1, Lcom/google/obf/dw;->a:[B

    .line 401
    :cond_0
    iget-object v1, p0, Lcom/google/obf/cu;->n:Lcom/google/obf/dw;

    iget-object v1, v1, Lcom/google/obf/dw;->a:[B

    sget-object v3, Lcom/google/obf/cu;->a:[B

    array-length v3, v3

    invoke-interface {p1, v1, v3, p3}, Lcom/google/obf/ak;->b([BII)V

    .line 402
    iget-object v1, p0, Lcom/google/obf/cu;->n:Lcom/google/obf/dw;

    invoke-virtual {v1, v2}, Lcom/google/obf/dw;->c(I)V

    .line 403
    iget-object v1, p0, Lcom/google/obf/cu;->n:Lcom/google/obf/dw;

    invoke-virtual {v1, v0}, Lcom/google/obf/dw;->b(I)V

    .line 496
    :cond_1
    :goto_0
    return-void

    .line 405
    :cond_2
    iget-object v5, p2, Lcom/google/obf/cu$b;->K:Lcom/google/obf/ar;

    .line 406
    iget-boolean v0, p0, Lcom/google/obf/cu;->S:Z

    if-nez v0, :cond_10

    .line 407
    iget-boolean v0, p2, Lcom/google/obf/cu$b;->e:Z

    if-eqz v0, :cond_13

    .line 408
    iget v0, p0, Lcom/google/obf/cu;->Q:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/obf/cu;->Q:I

    .line 409
    iget-boolean v0, p0, Lcom/google/obf/cu;->T:Z

    if-nez v0, :cond_4

    .line 410
    iget-object v0, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    invoke-interface {p1, v0, v2, v1}, Lcom/google/obf/ak;->b([BII)V

    .line 411
    iget v0, p0, Lcom/google/obf/cu;->R:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/cu;->R:I

    .line 412
    iget-object v0, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_3

    .line 413
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Extension bit is set in signal byte"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 414
    :cond_3
    iget-object v0, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    aget-byte v0, v0, v2

    iput-byte v0, p0, Lcom/google/obf/cu;->W:B

    .line 415
    iput-boolean v1, p0, Lcom/google/obf/cu;->T:Z

    .line 416
    :cond_4
    iget-byte v0, p0, Lcom/google/obf/cu;->W:B

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_a

    move v0, v1

    .line 417
    :goto_1
    if-eqz v0, :cond_f

    .line 418
    iget-byte v0, p0, Lcom/google/obf/cu;->W:B

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v8, :cond_b

    move v0, v1

    .line 419
    :goto_2
    iget v3, p0, Lcom/google/obf/cu;->Q:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/obf/cu;->Q:I

    .line 420
    iget-boolean v3, p0, Lcom/google/obf/cu;->U:Z

    if-nez v3, :cond_5

    .line 421
    iget-object v3, p0, Lcom/google/obf/cu;->o:Lcom/google/obf/dw;

    iget-object v3, v3, Lcom/google/obf/dw;->a:[B

    invoke-interface {p1, v3, v2, v6}, Lcom/google/obf/ak;->b([BII)V

    .line 422
    iget v3, p0, Lcom/google/obf/cu;->R:I

    add-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/obf/cu;->R:I

    .line 423
    iput-boolean v1, p0, Lcom/google/obf/cu;->U:Z

    .line 424
    iget-object v3, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    iget-object v4, v3, Lcom/google/obf/dw;->a:[B

    if-eqz v0, :cond_c

    const/16 v3, 0x80

    :goto_3
    or-int/lit8 v3, v3, 0x8

    int-to-byte v3, v3

    aput-byte v3, v4, v2

    .line 425
    iget-object v3, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    invoke-virtual {v3, v2}, Lcom/google/obf/dw;->c(I)V

    .line 426
    iget-object v3, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    invoke-interface {v5, v3, v1}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 427
    iget v3, p0, Lcom/google/obf/cu;->Z:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/obf/cu;->Z:I

    .line 428
    iget-object v3, p0, Lcom/google/obf/cu;->o:Lcom/google/obf/dw;

    invoke-virtual {v3, v2}, Lcom/google/obf/dw;->c(I)V

    .line 429
    iget-object v3, p0, Lcom/google/obf/cu;->o:Lcom/google/obf/dw;

    invoke-interface {v5, v3, v6}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 430
    iget v3, p0, Lcom/google/obf/cu;->Z:I

    add-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/obf/cu;->Z:I

    .line 431
    :cond_5
    if-eqz v0, :cond_f

    .line 432
    iget-boolean v0, p0, Lcom/google/obf/cu;->V:Z

    if-nez v0, :cond_6

    .line 433
    iget-object v0, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    invoke-interface {p1, v0, v2, v1}, Lcom/google/obf/ak;->b([BII)V

    .line 434
    iget v0, p0, Lcom/google/obf/cu;->R:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/cu;->R:I

    .line 435
    iget-object v0, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    invoke-virtual {v0, v2}, Lcom/google/obf/dw;->c(I)V

    .line 436
    iget-object v0, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->f()I

    move-result v0

    iput v0, p0, Lcom/google/obf/cu;->X:I

    .line 437
    iput-boolean v1, p0, Lcom/google/obf/cu;->V:Z

    .line 438
    :cond_6
    iget v0, p0, Lcom/google/obf/cu;->X:I

    mul-int/lit8 v0, v0, 0x4

    .line 439
    iget-object v3, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    invoke-virtual {v3}, Lcom/google/obf/dw;->c()I

    move-result v3

    if-ge v3, v0, :cond_7

    .line 440
    iget-object v3, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    new-array v4, v0, [B

    invoke-virtual {v3, v4, v0}, Lcom/google/obf/dw;->a([BI)V

    .line 441
    :cond_7
    iget-object v3, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    iget-object v3, v3, Lcom/google/obf/dw;->a:[B

    invoke-interface {p1, v3, v2, v0}, Lcom/google/obf/ak;->b([BII)V

    .line 442
    iget v3, p0, Lcom/google/obf/cu;->R:I

    add-int/2addr v3, v0

    iput v3, p0, Lcom/google/obf/cu;->R:I

    .line 443
    iget-object v3, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    invoke-virtual {v3, v2}, Lcom/google/obf/dw;->c(I)V

    .line 444
    iget-object v3, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    invoke-virtual {v3, v0}, Lcom/google/obf/dw;->b(I)V

    .line 445
    iget v0, p0, Lcom/google/obf/cu;->X:I

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    int-to-short v0, v0

    .line 446
    mul-int/lit8 v3, v0, 0x6

    add-int/lit8 v6, v3, 0x2

    .line 447
    iget-object v3, p0, Lcom/google/obf/cu;->q:Ljava/nio/ByteBuffer;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/obf/cu;->q:Ljava/nio/ByteBuffer;

    .line 448
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    if-ge v3, v6, :cond_9

    .line 449
    :cond_8
    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/obf/cu;->q:Ljava/nio/ByteBuffer;

    .line 450
    :cond_9
    iget-object v3, p0, Lcom/google/obf/cu;->q:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 451
    iget-object v3, p0, Lcom/google/obf/cu;->q:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    move v0, v2

    move v3, v2

    .line 453
    :goto_4
    iget v4, p0, Lcom/google/obf/cu;->X:I

    if-ge v0, v4, :cond_e

    .line 455
    iget-object v4, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    invoke-virtual {v4}, Lcom/google/obf/dw;->s()I

    move-result v4

    .line 456
    rem-int/lit8 v7, v0, 0x2

    if-nez v7, :cond_d

    .line 457
    iget-object v7, p0, Lcom/google/obf/cu;->q:Ljava/nio/ByteBuffer;

    sub-int v3, v4, v3

    int-to-short v3, v3

    invoke-virtual {v7, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 459
    :goto_5
    add-int/lit8 v0, v0, 0x1

    move v3, v4

    goto :goto_4

    :cond_a
    move v0, v2

    .line 416
    goto/16 :goto_1

    :cond_b
    move v0, v2

    .line 418
    goto/16 :goto_2

    :cond_c
    move v3, v2

    .line 424
    goto/16 :goto_3

    .line 458
    :cond_d
    iget-object v7, p0, Lcom/google/obf/cu;->q:Ljava/nio/ByteBuffer;

    sub-int v3, v4, v3

    invoke-virtual {v7, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_5

    .line 460
    :cond_e
    iget v0, p0, Lcom/google/obf/cu;->R:I

    sub-int v0, p3, v0

    sub-int/2addr v0, v3

    .line 461
    iget v3, p0, Lcom/google/obf/cu;->X:I

    rem-int/lit8 v3, v3, 0x2

    if-ne v3, v1, :cond_12

    .line 462
    iget-object v3, p0, Lcom/google/obf/cu;->q:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 465
    :goto_6
    iget-object v0, p0, Lcom/google/obf/cu;->p:Lcom/google/obf/dw;

    iget-object v3, p0, Lcom/google/obf/cu;->q:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-virtual {v0, v3, v6}, Lcom/google/obf/dw;->a([BI)V

    .line 466
    iget-object v0, p0, Lcom/google/obf/cu;->p:Lcom/google/obf/dw;

    invoke-interface {v5, v0, v6}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 467
    iget v0, p0, Lcom/google/obf/cu;->Z:I

    add-int/2addr v0, v6

    iput v0, p0, Lcom/google/obf/cu;->Z:I

    .line 470
    :cond_f
    :goto_7
    iput-boolean v1, p0, Lcom/google/obf/cu;->S:Z

    .line 471
    :cond_10
    iget-object v0, p0, Lcom/google/obf/cu;->m:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->c()I

    move-result v0

    add-int/2addr v0, p3

    .line 472
    const-string v3, "V_MPEG4/ISO/AVC"

    iget-object v4, p2, Lcom/google/obf/cu$b;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    const-string v3, "V_MPEGH/ISO/HEVC"

    iget-object v4, p2, Lcom/google/obf/cu$b;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 473
    :cond_11
    iget-object v3, p0, Lcom/google/obf/cu;->i:Lcom/google/obf/dw;

    iget-object v3, v3, Lcom/google/obf/dw;->a:[B

    .line 474
    aput-byte v2, v3, v2

    .line 475
    aput-byte v2, v3, v1

    .line 476
    aput-byte v2, v3, v8

    .line 477
    iget v1, p2, Lcom/google/obf/cu$b;->L:I

    .line 478
    iget v4, p2, Lcom/google/obf/cu$b;->L:I

    rsub-int/lit8 v4, v4, 0x4

    .line 479
    :goto_8
    iget v6, p0, Lcom/google/obf/cu;->R:I

    if-ge v6, v0, :cond_16

    .line 480
    iget v6, p0, Lcom/google/obf/cu;->Y:I

    if-nez v6, :cond_14

    .line 481
    invoke-direct {p0, p1, v3, v4, v1}, Lcom/google/obf/cu;->a(Lcom/google/obf/ak;[BII)V

    .line 482
    iget-object v6, p0, Lcom/google/obf/cu;->i:Lcom/google/obf/dw;

    invoke-virtual {v6, v2}, Lcom/google/obf/dw;->c(I)V

    .line 483
    iget-object v6, p0, Lcom/google/obf/cu;->i:Lcom/google/obf/dw;

    invoke-virtual {v6}, Lcom/google/obf/dw;->s()I

    move-result v6

    iput v6, p0, Lcom/google/obf/cu;->Y:I

    .line 484
    iget-object v6, p0, Lcom/google/obf/cu;->h:Lcom/google/obf/dw;

    invoke-virtual {v6, v2}, Lcom/google/obf/dw;->c(I)V

    .line 485
    iget-object v6, p0, Lcom/google/obf/cu;->h:Lcom/google/obf/dw;

    invoke-interface {v5, v6, v9}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 486
    iget v6, p0, Lcom/google/obf/cu;->Z:I

    add-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/obf/cu;->Z:I

    goto :goto_8

    .line 463
    :cond_12
    iget-object v3, p0, Lcom/google/obf/cu;->q:Ljava/nio/ByteBuffer;

    int-to-short v0, v0

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 464
    iget-object v0, p0, Lcom/google/obf/cu;->q:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_6

    .line 468
    :cond_13
    iget-object v0, p2, Lcom/google/obf/cu$b;->f:[B

    if-eqz v0, :cond_f

    .line 469
    iget-object v0, p0, Lcom/google/obf/cu;->m:Lcom/google/obf/dw;

    iget-object v3, p2, Lcom/google/obf/cu$b;->f:[B

    iget-object v4, p2, Lcom/google/obf/cu$b;->f:[B

    array-length v4, v4

    invoke-virtual {v0, v3, v4}, Lcom/google/obf/dw;->a([BI)V

    goto :goto_7

    .line 487
    :cond_14
    iget v6, p0, Lcom/google/obf/cu;->Y:I

    iget v7, p0, Lcom/google/obf/cu;->Y:I

    .line 488
    invoke-direct {p0, p1, v5, v7}, Lcom/google/obf/cu;->a(Lcom/google/obf/ak;Lcom/google/obf/ar;I)I

    move-result v7

    sub-int/2addr v6, v7

    iput v6, p0, Lcom/google/obf/cu;->Y:I

    goto :goto_8

    .line 490
    :cond_15
    :goto_9
    iget v1, p0, Lcom/google/obf/cu;->R:I

    if-ge v1, v0, :cond_16

    .line 491
    iget v1, p0, Lcom/google/obf/cu;->R:I

    sub-int v1, v0, v1

    invoke-direct {p0, p1, v5, v1}, Lcom/google/obf/cu;->a(Lcom/google/obf/ak;Lcom/google/obf/ar;I)I

    goto :goto_9

    .line 492
    :cond_16
    const-string v0, "A_VORBIS"

    iget-object v1, p2, Lcom/google/obf/cu$b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 493
    iget-object v0, p0, Lcom/google/obf/cu;->k:Lcom/google/obf/dw;

    invoke-virtual {v0, v2}, Lcom/google/obf/dw;->c(I)V

    .line 494
    iget-object v0, p0, Lcom/google/obf/cu;->k:Lcom/google/obf/dw;

    invoke-interface {v5, v0, v9}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 495
    iget v0, p0, Lcom/google/obf/cu;->Z:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/obf/cu;->Z:I

    goto/16 :goto_0
.end method

.method private a(Lcom/google/obf/ak;[BII)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 516
    iget-object v0, p0, Lcom/google/obf/cu;->m:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->b()I

    move-result v0

    invoke-static {p4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 517
    add-int v1, p3, v0

    sub-int v2, p4, v0

    invoke-interface {p1, p2, v1, v2}, Lcom/google/obf/ak;->b([BII)V

    .line 518
    if-lez v0, :cond_0

    .line 519
    iget-object v1, p0, Lcom/google/obf/cu;->m:Lcom/google/obf/dw;

    invoke-virtual {v1, p2, p3, v0}, Lcom/google/obf/dw;->a([BII)V

    .line 520
    :cond_0
    iget v0, p0, Lcom/google/obf/cu;->R:I

    add-int/2addr v0, p4

    iput v0, p0, Lcom/google/obf/cu;->R:I

    .line 521
    return-void
.end method

.method private a(Lcom/google/obf/cu$b;)V
    .locals 4

    .prologue
    .line 497
    iget-object v0, p0, Lcom/google/obf/cu;->n:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    iget-wide v2, p0, Lcom/google/obf/cu;->K:J

    invoke-static {v0, v2, v3}, Lcom/google/obf/cu;->a([BJ)V

    .line 498
    iget-object v0, p1, Lcom/google/obf/cu$b;->K:Lcom/google/obf/ar;

    iget-object v1, p0, Lcom/google/obf/cu;->n:Lcom/google/obf/dw;

    iget-object v2, p0, Lcom/google/obf/cu;->n:Lcom/google/obf/dw;

    invoke-virtual {v2}, Lcom/google/obf/dw;->c()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 499
    iget v0, p0, Lcom/google/obf/cu;->Z:I

    iget-object v1, p0, Lcom/google/obf/cu;->n:Lcom/google/obf/dw;

    invoke-virtual {v1}, Lcom/google/obf/dw;->c()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/cu;->Z:I

    .line 500
    return-void
.end method

.method private a(Lcom/google/obf/cu$b;J)V
    .locals 8

    .prologue
    .line 371
    const-string v0, "S_TEXT/UTF8"

    iget-object v1, p1, Lcom/google/obf/cu$b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    invoke-direct {p0, p1}, Lcom/google/obf/cu;->a(Lcom/google/obf/cu$b;)V

    .line 373
    :cond_0
    iget-object v1, p1, Lcom/google/obf/cu$b;->K:Lcom/google/obf/ar;

    iget v4, p0, Lcom/google/obf/cu;->Q:I

    iget v5, p0, Lcom/google/obf/cu;->Z:I

    const/4 v6, 0x0

    iget-object v7, p1, Lcom/google/obf/cu$b;->g:[B

    move-wide v2, p2

    invoke-interface/range {v1 .. v7}, Lcom/google/obf/ar;->a(JIII[B)V

    .line 374
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/cu;->aa:Z

    .line 375
    invoke-direct {p0}, Lcom/google/obf/cu;->d()V

    .line 376
    return-void
.end method

.method private static a([BJ)V
    .locals 9

    .prologue
    const-wide v4, 0xd693a400L

    const/4 v8, 0x0

    .line 501
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 502
    sget-object v0, Lcom/google/obf/cu;->b:[B

    .line 514
    :goto_0
    const/16 v1, 0x13

    const/16 v2, 0xc

    invoke-static {v0, v8, p0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 515
    return-void

    .line 503
    :cond_0
    div-long v0, p1, v4

    long-to-int v0, v0

    .line 504
    int-to-long v2, v0

    mul-long/2addr v2, v4

    sub-long v2, p1, v2

    .line 505
    const-wide/32 v4, 0x3938700

    div-long v4, v2, v4

    long-to-int v1, v4

    .line 506
    const v4, 0x3938700

    mul-int/2addr v4, v1

    int-to-long v4, v4

    sub-long/2addr v2, v4

    .line 507
    const-wide/32 v4, 0xf4240

    div-long v4, v2, v4

    long-to-int v4, v4

    .line 508
    const v5, 0xf4240

    mul-int/2addr v5, v4

    int-to-long v6, v5

    sub-long/2addr v2, v6

    .line 509
    const-wide/16 v6, 0x3e8

    div-long/2addr v2, v6

    long-to-int v2, v2

    .line 510
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%02d:%02d:%02d,%03d"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    .line 511
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v8

    const/4 v0, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v0

    const/4 v0, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v0

    const/4 v0, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v0

    .line 512
    invoke-static {v3, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 513
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/google/obf/ao;J)Z
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 554
    iget-boolean v2, p0, Lcom/google/obf/cu;->B:Z

    if-eqz v2, :cond_0

    .line 555
    iput-wide p2, p0, Lcom/google/obf/cu;->D:J

    .line 556
    iget-wide v2, p0, Lcom/google/obf/cu;->C:J

    iput-wide v2, p1, Lcom/google/obf/ao;->a:J

    .line 557
    iput-boolean v1, p0, Lcom/google/obf/cu;->B:Z

    .line 563
    :goto_0
    return v0

    .line 559
    :cond_0
    iget-boolean v2, p0, Lcom/google/obf/cu;->y:Z

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lcom/google/obf/cu;->D:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 560
    iget-wide v2, p0, Lcom/google/obf/cu;->D:J

    iput-wide v2, p1, Lcom/google/obf/ao;->a:J

    .line 561
    iput-wide v4, p0, Lcom/google/obf/cu;->D:J

    goto :goto_0

    :cond_1
    move v0, v1

    .line 563
    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 567
    const-string v0, "V_VP8"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_VP9"

    .line 568
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEG2"

    .line 569
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEG4/ISO/SP"

    .line 570
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEG4/ISO/ASP"

    .line 571
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEG4/ISO/AP"

    .line 572
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEG4/ISO/AVC"

    .line 573
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MPEGH/ISO/HEVC"

    .line 574
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "V_MS/VFW/FOURCC"

    .line 575
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_OPUS"

    .line 576
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_VORBIS"

    .line 577
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_AAC"

    .line 578
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_MPEG/L3"

    .line 579
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_AC3"

    .line 580
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_EAC3"

    .line 581
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_TRUEHD"

    .line 582
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_DTS"

    .line 583
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_DTS/EXPRESS"

    .line 584
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_DTS/LOSSLESS"

    .line 585
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_FLAC"

    .line 586
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_MS/ACM"

    .line 587
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "A_PCM/INT/LIT"

    .line 588
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "S_TEXT/UTF8"

    .line 589
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "S_VOBSUB"

    .line 590
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "S_HDMV/PGS"

    .line 591
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 592
    :goto_0
    return v0

    .line 591
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a([II)[I
    .locals 1

    .prologue
    .line 593
    if-nez p0, :cond_1

    .line 594
    new-array p0, p1, [I

    .line 597
    :cond_0
    :goto_0
    return-object p0

    .line 595
    :cond_1
    array-length v0, p0

    if-ge v0, p1, :cond_0

    .line 597
    array-length v0, p0

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    new-array p0, v0, [I

    goto :goto_0
.end method

.method private d()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 377
    iput v0, p0, Lcom/google/obf/cu;->R:I

    .line 378
    iput v0, p0, Lcom/google/obf/cu;->Z:I

    .line 379
    iput v0, p0, Lcom/google/obf/cu;->Y:I

    .line 380
    iput-boolean v0, p0, Lcom/google/obf/cu;->S:Z

    .line 381
    iput-boolean v0, p0, Lcom/google/obf/cu;->T:Z

    .line 382
    iput-boolean v0, p0, Lcom/google/obf/cu;->V:Z

    .line 383
    iput v0, p0, Lcom/google/obf/cu;->X:I

    .line 384
    iput-byte v0, p0, Lcom/google/obf/cu;->W:B

    .line 385
    iput-boolean v0, p0, Lcom/google/obf/cu;->U:Z

    .line 386
    iget-object v0, p0, Lcom/google/obf/cu;->m:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->a()V

    .line 387
    return-void
.end method

.method private e()Lcom/google/obf/aq;
    .locals 13

    .prologue
    const-wide/16 v4, -0x1

    const/4 v0, 0x0

    const/4 v12, 0x0

    .line 530
    iget-wide v2, p0, Lcom/google/obf/cu;->r:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/google/obf/cu;->v:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/obf/cu;->F:Lcom/google/obf/dr;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/obf/cu;->F:Lcom/google/obf/dr;

    .line 531
    invoke-virtual {v1}, Lcom/google/obf/dr;->a()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/obf/cu;->G:Lcom/google/obf/dr;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/obf/cu;->G:Lcom/google/obf/dr;

    .line 532
    invoke-virtual {v1}, Lcom/google/obf/dr;->a()I

    move-result v1

    iget-object v2, p0, Lcom/google/obf/cu;->F:Lcom/google/obf/dr;

    invoke-virtual {v2}, Lcom/google/obf/dr;->a()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 533
    :cond_0
    iput-object v12, p0, Lcom/google/obf/cu;->F:Lcom/google/obf/dr;

    .line 534
    iput-object v12, p0, Lcom/google/obf/cu;->G:Lcom/google/obf/dr;

    .line 535
    sget-object v0, Lcom/google/obf/aq;->f:Lcom/google/obf/aq;

    .line 553
    :goto_0
    return-object v0

    .line 536
    :cond_1
    iget-object v1, p0, Lcom/google/obf/cu;->F:Lcom/google/obf/dr;

    invoke-virtual {v1}, Lcom/google/obf/dr;->a()I

    move-result v2

    .line 537
    new-array v3, v2, [I

    .line 538
    new-array v4, v2, [J

    .line 539
    new-array v5, v2, [J

    .line 540
    new-array v6, v2, [J

    move v1, v0

    .line 541
    :goto_1
    if-ge v1, v2, :cond_2

    .line 542
    iget-object v7, p0, Lcom/google/obf/cu;->F:Lcom/google/obf/dr;

    invoke-virtual {v7, v1}, Lcom/google/obf/dr;->a(I)J

    move-result-wide v8

    aput-wide v8, v6, v1

    .line 543
    iget-wide v8, p0, Lcom/google/obf/cu;->r:J

    iget-object v7, p0, Lcom/google/obf/cu;->G:Lcom/google/obf/dr;

    invoke-virtual {v7, v1}, Lcom/google/obf/dr;->a(I)J

    move-result-wide v10

    add-long/2addr v8, v10

    aput-wide v8, v4, v1

    .line 544
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 545
    :cond_2
    :goto_2
    add-int/lit8 v1, v2, -0x1

    if-ge v0, v1, :cond_3

    .line 546
    add-int/lit8 v1, v0, 0x1

    aget-wide v8, v4, v1

    aget-wide v10, v4, v0

    sub-long/2addr v8, v10

    long-to-int v1, v8

    aput v1, v3, v0

    .line 547
    add-int/lit8 v1, v0, 0x1

    aget-wide v8, v6, v1

    aget-wide v10, v6, v0

    sub-long/2addr v8, v10

    aput-wide v8, v5, v0

    .line 548
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 549
    :cond_3
    add-int/lit8 v0, v2, -0x1

    iget-wide v8, p0, Lcom/google/obf/cu;->r:J

    iget-wide v10, p0, Lcom/google/obf/cu;->s:J

    add-long/2addr v8, v10

    add-int/lit8 v1, v2, -0x1

    aget-wide v10, v4, v1

    sub-long/2addr v8, v10

    long-to-int v1, v8

    aput v1, v3, v0

    .line 550
    add-int/lit8 v0, v2, -0x1

    iget-wide v8, p0, Lcom/google/obf/cu;->v:J

    add-int/lit8 v1, v2, -0x1

    aget-wide v10, v6, v1

    sub-long/2addr v8, v10

    aput-wide v8, v5, v0

    .line 551
    iput-object v12, p0, Lcom/google/obf/cu;->F:Lcom/google/obf/dr;

    .line 552
    iput-object v12, p0, Lcom/google/obf/cu;->G:Lcom/google/obf/dr;

    .line 553
    new-instance v0, Lcom/google/obf/af;

    invoke-direct {v0, v3, v4, v5, v6}, Lcom/google/obf/af;-><init>([I[J[J[J)V

    goto :goto_0
.end method


# virtual methods
.method a(I)I
    .locals 1

    .prologue
    .line 44
    sparse-switch p1, :sswitch_data_0

    .line 50
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 45
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 46
    :sswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 47
    :sswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 48
    :sswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 49
    :sswitch_4
    const/4 v0, 0x5

    goto :goto_0

    .line 44
    nop

    :sswitch_data_0
    .sparse-switch
        0x83 -> :sswitch_1
        0x86 -> :sswitch_2
        0x9b -> :sswitch_1
        0x9f -> :sswitch_1
        0xa0 -> :sswitch_0
        0xa1 -> :sswitch_3
        0xa3 -> :sswitch_3
        0xae -> :sswitch_0
        0xb0 -> :sswitch_1
        0xb3 -> :sswitch_1
        0xb5 -> :sswitch_4
        0xb7 -> :sswitch_0
        0xba -> :sswitch_1
        0xbb -> :sswitch_0
        0xd7 -> :sswitch_1
        0xe0 -> :sswitch_0
        0xe1 -> :sswitch_0
        0xe7 -> :sswitch_1
        0xf1 -> :sswitch_1
        0xfb -> :sswitch_1
        0x4254 -> :sswitch_1
        0x4255 -> :sswitch_3
        0x4282 -> :sswitch_2
        0x4285 -> :sswitch_1
        0x42f7 -> :sswitch_1
        0x4489 -> :sswitch_4
        0x47e1 -> :sswitch_1
        0x47e2 -> :sswitch_3
        0x47e7 -> :sswitch_0
        0x47e8 -> :sswitch_1
        0x4dbb -> :sswitch_0
        0x5031 -> :sswitch_1
        0x5032 -> :sswitch_1
        0x5034 -> :sswitch_0
        0x5035 -> :sswitch_0
        0x53ab -> :sswitch_3
        0x53ac -> :sswitch_1
        0x53b8 -> :sswitch_1
        0x54b0 -> :sswitch_1
        0x54b2 -> :sswitch_1
        0x54ba -> :sswitch_1
        0x55b0 -> :sswitch_0
        0x55b9 -> :sswitch_1
        0x55ba -> :sswitch_1
        0x55bb -> :sswitch_1
        0x55bc -> :sswitch_1
        0x55bd -> :sswitch_1
        0x55d0 -> :sswitch_0
        0x55d1 -> :sswitch_4
        0x55d2 -> :sswitch_4
        0x55d3 -> :sswitch_4
        0x55d4 -> :sswitch_4
        0x55d5 -> :sswitch_4
        0x55d6 -> :sswitch_4
        0x55d7 -> :sswitch_4
        0x55d8 -> :sswitch_4
        0x55d9 -> :sswitch_4
        0x55da -> :sswitch_4
        0x56aa -> :sswitch_1
        0x56bb -> :sswitch_1
        0x6240 -> :sswitch_0
        0x6264 -> :sswitch_1
        0x63a2 -> :sswitch_3
        0x6d80 -> :sswitch_0
        0x7670 -> :sswitch_0
        0x7672 -> :sswitch_3
        0x22b59c -> :sswitch_2
        0x23e383 -> :sswitch_1
        0x2ad7b1 -> :sswitch_1
        0x114d9b74 -> :sswitch_0
        0x1549a966 -> :sswitch_0
        0x1654ae6b -> :sswitch_0
        0x18538067 -> :sswitch_0
        0x1a45dfa3 -> :sswitch_0
        0x1c53bb6b -> :sswitch_0
        0x1f43b675 -> :sswitch_0
    .end sparse-switch
.end method

.method public a(Lcom/google/obf/ak;Lcom/google/obf/ao;)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 37
    iput-boolean v0, p0, Lcom/google/obf/cu;->aa:Z

    move v2, v1

    .line 39
    :cond_0
    if-eqz v2, :cond_2

    iget-boolean v3, p0, Lcom/google/obf/cu;->aa:Z

    if-nez v3, :cond_2

    .line 40
    iget-object v2, p0, Lcom/google/obf/cu;->d:Lcom/google/obf/cq;

    invoke-interface {v2, p1}, Lcom/google/obf/cq;->a(Lcom/google/obf/ak;)Z

    move-result v2

    .line 41
    if-eqz v2, :cond_0

    invoke-interface {p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v4

    invoke-direct {p0, p2, v4, v5}, Lcom/google/obf/cu;->a(Lcom/google/obf/ao;J)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    .line 43
    :cond_1
    :goto_0
    return v0

    :cond_2
    if-nez v2, :cond_1

    const/4 v0, -0x1

    goto :goto_0
.end method

.method a(ID)V
    .locals 2

    .prologue
    .line 227
    sparse-switch p1, :sswitch_data_0

    .line 253
    :goto_0
    return-void

    .line 228
    :sswitch_0
    double-to-long v0, p2

    iput-wide v0, p0, Lcom/google/obf/cu;->u:J

    goto :goto_0

    .line 230
    :sswitch_1
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    double-to-int v1, p2

    iput v1, v0, Lcom/google/obf/cu$b;->H:I

    goto :goto_0

    .line 232
    :sswitch_2
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    double-to-float v1, p2

    iput v1, v0, Lcom/google/obf/cu$b;->v:F

    goto :goto_0

    .line 234
    :sswitch_3
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    double-to-float v1, p2

    iput v1, v0, Lcom/google/obf/cu$b;->w:F

    goto :goto_0

    .line 236
    :sswitch_4
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    double-to-float v1, p2

    iput v1, v0, Lcom/google/obf/cu$b;->x:F

    goto :goto_0

    .line 238
    :sswitch_5
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    double-to-float v1, p2

    iput v1, v0, Lcom/google/obf/cu$b;->y:F

    goto :goto_0

    .line 240
    :sswitch_6
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    double-to-float v1, p2

    iput v1, v0, Lcom/google/obf/cu$b;->z:F

    goto :goto_0

    .line 242
    :sswitch_7
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    double-to-float v1, p2

    iput v1, v0, Lcom/google/obf/cu$b;->A:F

    goto :goto_0

    .line 244
    :sswitch_8
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    double-to-float v1, p2

    iput v1, v0, Lcom/google/obf/cu$b;->B:F

    goto :goto_0

    .line 246
    :sswitch_9
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    double-to-float v1, p2

    iput v1, v0, Lcom/google/obf/cu$b;->C:F

    goto :goto_0

    .line 248
    :sswitch_a
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    double-to-float v1, p2

    iput v1, v0, Lcom/google/obf/cu$b;->D:F

    goto :goto_0

    .line 250
    :sswitch_b
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    double-to-float v1, p2

    iput v1, v0, Lcom/google/obf/cu$b;->E:F

    goto :goto_0

    .line 227
    :sswitch_data_0
    .sparse-switch
        0xb5 -> :sswitch_1
        0x4489 -> :sswitch_0
        0x55d1 -> :sswitch_2
        0x55d2 -> :sswitch_3
        0x55d3 -> :sswitch_4
        0x55d4 -> :sswitch_5
        0x55d5 -> :sswitch_6
        0x55d6 -> :sswitch_7
        0x55d7 -> :sswitch_8
        0x55d8 -> :sswitch_9
        0x55d9 -> :sswitch_a
        0x55da -> :sswitch_b
    .end sparse-switch
.end method

.method a(IILcom/google/obf/ak;)V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 263
    sparse-switch p1, :sswitch_data_0

    .line 370
    new-instance v2, Lcom/google/obf/s;

    const/16 v3, 0x1a

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unexpected id: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v2

    .line 264
    :sswitch_0
    iget-object v2, p0, Lcom/google/obf/cu;->l:Lcom/google/obf/dw;

    iget-object v2, v2, Lcom/google/obf/dw;->a:[B

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([BB)V

    .line 265
    iget-object v2, p0, Lcom/google/obf/cu;->l:Lcom/google/obf/dw;

    iget-object v2, v2, Lcom/google/obf/dw;->a:[B

    rsub-int/lit8 v3, p2, 0x4

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v2, v3, v1}, Lcom/google/obf/ak;->b([BII)V

    .line 266
    iget-object v2, p0, Lcom/google/obf/cu;->l:Lcom/google/obf/dw;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/obf/dw;->c(I)V

    .line 267
    iget-object v2, p0, Lcom/google/obf/cu;->l:Lcom/google/obf/dw;

    invoke-virtual {v2}, Lcom/google/obf/dw;->k()J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, p0, Lcom/google/obf/cu;->z:I

    .line 369
    :goto_0
    return-void

    .line 269
    :sswitch_1
    iget-object v2, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    move/from16 v0, p2

    new-array v3, v0, [B

    iput-object v3, v2, Lcom/google/obf/cu$b;->h:[B

    .line 270
    iget-object v2, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iget-object v2, v2, Lcom/google/obf/cu$b;->h:[B

    const/4 v3, 0x0

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v2, v3, v1}, Lcom/google/obf/ak;->b([BII)V

    goto :goto_0

    .line 272
    :sswitch_2
    iget-object v2, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    move/from16 v0, p2

    new-array v3, v0, [B

    iput-object v3, v2, Lcom/google/obf/cu$b;->n:[B

    .line 273
    iget-object v2, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iget-object v2, v2, Lcom/google/obf/cu$b;->n:[B

    const/4 v3, 0x0

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v2, v3, v1}, Lcom/google/obf/ak;->b([BII)V

    goto :goto_0

    .line 275
    :sswitch_3
    iget-object v2, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    move/from16 v0, p2

    new-array v3, v0, [B

    iput-object v3, v2, Lcom/google/obf/cu$b;->f:[B

    .line 276
    iget-object v2, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iget-object v2, v2, Lcom/google/obf/cu$b;->f:[B

    const/4 v3, 0x0

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v2, v3, v1}, Lcom/google/obf/ak;->b([BII)V

    goto :goto_0

    .line 278
    :sswitch_4
    iget-object v2, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    move/from16 v0, p2

    new-array v3, v0, [B

    iput-object v3, v2, Lcom/google/obf/cu$b;->g:[B

    .line 279
    iget-object v2, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iget-object v2, v2, Lcom/google/obf/cu$b;->g:[B

    const/4 v3, 0x0

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v2, v3, v1}, Lcom/google/obf/ak;->b([BII)V

    goto :goto_0

    .line 281
    :sswitch_5
    iget v2, p0, Lcom/google/obf/cu;->I:I

    if-nez v2, :cond_0

    .line 282
    iget-object v2, p0, Lcom/google/obf/cu;->e:Lcom/google/obf/ct;

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/16 v5, 0x8

    move-object/from16 v0, p3

    invoke-virtual {v2, v0, v3, v4, v5}, Lcom/google/obf/ct;->a(Lcom/google/obf/ak;ZZI)J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, p0, Lcom/google/obf/cu;->O:I

    .line 283
    iget-object v2, p0, Lcom/google/obf/cu;->e:Lcom/google/obf/ct;

    invoke-virtual {v2}, Lcom/google/obf/ct;->b()I

    move-result v2

    iput v2, p0, Lcom/google/obf/cu;->P:I

    .line 284
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/obf/cu;->K:J

    .line 285
    const/4 v2, 0x1

    iput v2, p0, Lcom/google/obf/cu;->I:I

    .line 286
    iget-object v2, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    invoke-virtual {v2}, Lcom/google/obf/dw;->a()V

    .line 287
    :cond_0
    iget-object v2, p0, Lcom/google/obf/cu;->f:Landroid/util/SparseArray;

    iget v3, p0, Lcom/google/obf/cu;->O:I

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/obf/cu$b;

    .line 288
    if-nez v2, :cond_1

    .line 289
    iget v2, p0, Lcom/google/obf/cu;->P:I

    sub-int v2, p2, v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Lcom/google/obf/ak;->b(I)V

    .line 290
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/obf/cu;->I:I

    goto/16 :goto_0

    .line 292
    :cond_1
    iget v3, p0, Lcom/google/obf/cu;->I:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 293
    const/4 v3, 0x3

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v3}, Lcom/google/obf/cu;->a(Lcom/google/obf/ak;I)V

    .line 294
    iget-object v3, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    iget-object v3, v3, Lcom/google/obf/dw;->a:[B

    const/4 v4, 0x2

    aget-byte v3, v3, v4

    and-int/lit8 v3, v3, 0x6

    shr-int/lit8 v3, v3, 0x1

    .line 295
    if-nez v3, :cond_4

    .line 296
    const/4 v3, 0x1

    iput v3, p0, Lcom/google/obf/cu;->M:I

    .line 297
    iget-object v3, p0, Lcom/google/obf/cu;->N:[I

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/google/obf/cu;->a([II)[I

    move-result-object v3

    iput-object v3, p0, Lcom/google/obf/cu;->N:[I

    .line 298
    iget-object v3, p0, Lcom/google/obf/cu;->N:[I

    const/4 v4, 0x0

    iget v5, p0, Lcom/google/obf/cu;->P:I

    sub-int v5, p2, v5

    add-int/lit8 v5, v5, -0x3

    aput v5, v3, v4

    .line 352
    :goto_1
    iget-object v3, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    iget-object v3, v3, Lcom/google/obf/dw;->a:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    shl-int/lit8 v3, v3, 0x8

    iget-object v4, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    iget-object v4, v4, Lcom/google/obf/dw;->a:[B

    const/4 v5, 0x1

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v3, v4

    .line 353
    iget-wide v4, p0, Lcom/google/obf/cu;->E:J

    int-to-long v6, v3

    invoke-direct {p0, v6, v7}, Lcom/google/obf/cu;->a(J)J

    move-result-wide v6

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/obf/cu;->J:J

    .line 354
    iget-object v3, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    iget-object v3, v3, Lcom/google/obf/dw;->a:[B

    const/4 v4, 0x2

    aget-byte v3, v3, v4

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_13

    const/4 v3, 0x1

    .line 355
    :goto_2
    iget v4, v2, Lcom/google/obf/cu$b;->c:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_2

    const/16 v4, 0xa3

    move/from16 v0, p1

    if-ne v0, v4, :cond_14

    iget-object v4, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    iget-object v4, v4, Lcom/google/obf/dw;->a:[B

    const/4 v5, 0x2

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_14

    :cond_2
    const/4 v4, 0x1

    .line 356
    :goto_3
    if-eqz v4, :cond_15

    const/4 v4, 0x1

    .line 357
    :goto_4
    if-eqz v3, :cond_16

    const/high16 v3, 0x8000000

    :goto_5
    or-int/2addr v3, v4

    iput v3, p0, Lcom/google/obf/cu;->Q:I

    .line 358
    const/4 v3, 0x2

    iput v3, p0, Lcom/google/obf/cu;->I:I

    .line 359
    const/4 v3, 0x0

    iput v3, p0, Lcom/google/obf/cu;->L:I

    .line 360
    :cond_3
    const/16 v3, 0xa3

    move/from16 v0, p1

    if-ne v0, v3, :cond_18

    .line 361
    :goto_6
    iget v3, p0, Lcom/google/obf/cu;->L:I

    iget v4, p0, Lcom/google/obf/cu;->M:I

    if-ge v3, v4, :cond_17

    .line 362
    iget-object v3, p0, Lcom/google/obf/cu;->N:[I

    iget v4, p0, Lcom/google/obf/cu;->L:I

    aget v3, v3, v4

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v2, v3}, Lcom/google/obf/cu;->a(Lcom/google/obf/ak;Lcom/google/obf/cu$b;I)V

    .line 363
    iget-wide v4, p0, Lcom/google/obf/cu;->J:J

    iget v3, p0, Lcom/google/obf/cu;->L:I

    iget v6, v2, Lcom/google/obf/cu$b;->d:I

    mul-int/2addr v3, v6

    div-int/lit16 v3, v3, 0x3e8

    int-to-long v6, v3

    add-long/2addr v4, v6

    .line 364
    invoke-direct {p0, v2, v4, v5}, Lcom/google/obf/cu;->a(Lcom/google/obf/cu$b;J)V

    .line 365
    iget v3, p0, Lcom/google/obf/cu;->L:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/obf/cu;->L:I

    goto :goto_6

    .line 299
    :cond_4
    const/16 v4, 0xa3

    move/from16 v0, p1

    if-eq v0, v4, :cond_5

    .line 300
    new-instance v2, Lcom/google/obf/s;

    const-string v3, "Lacing only supported in SimpleBlocks."

    invoke-direct {v2, v3}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v2

    .line 301
    :cond_5
    const/4 v4, 0x4

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v4}, Lcom/google/obf/cu;->a(Lcom/google/obf/ak;I)V

    .line 302
    iget-object v4, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    iget-object v4, v4, Lcom/google/obf/dw;->a:[B

    const/4 v5, 0x3

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/obf/cu;->M:I

    .line 303
    iget-object v4, p0, Lcom/google/obf/cu;->N:[I

    iget v5, p0, Lcom/google/obf/cu;->M:I

    .line 304
    invoke-static {v4, v5}, Lcom/google/obf/cu;->a([II)[I

    move-result-object v4

    iput-object v4, p0, Lcom/google/obf/cu;->N:[I

    .line 305
    const/4 v4, 0x2

    if-ne v3, v4, :cond_6

    .line 306
    iget v3, p0, Lcom/google/obf/cu;->P:I

    sub-int v3, p2, v3

    add-int/lit8 v3, v3, -0x4

    iget v4, p0, Lcom/google/obf/cu;->M:I

    div-int/2addr v3, v4

    .line 307
    iget-object v4, p0, Lcom/google/obf/cu;->N:[I

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/obf/cu;->M:I

    invoke-static {v4, v5, v6, v3}, Ljava/util/Arrays;->fill([IIII)V

    goto/16 :goto_1

    .line 308
    :cond_6
    const/4 v4, 0x1

    if-ne v3, v4, :cond_9

    .line 309
    const/4 v5, 0x0

    .line 310
    const/4 v4, 0x4

    .line 311
    const/4 v3, 0x0

    :goto_7
    iget v6, p0, Lcom/google/obf/cu;->M:I

    add-int/lit8 v6, v6, -0x1

    if-ge v3, v6, :cond_8

    .line 312
    iget-object v6, p0, Lcom/google/obf/cu;->N:[I

    const/4 v7, 0x0

    aput v7, v6, v3

    .line 313
    :cond_7
    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v4}, Lcom/google/obf/cu;->a(Lcom/google/obf/ak;I)V

    .line 314
    iget-object v6, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    iget-object v6, v6, Lcom/google/obf/dw;->a:[B

    add-int/lit8 v7, v4, -0x1

    aget-byte v6, v6, v7

    and-int/lit16 v6, v6, 0xff

    .line 315
    iget-object v7, p0, Lcom/google/obf/cu;->N:[I

    aget v8, v7, v3

    add-int/2addr v8, v6

    aput v8, v7, v3

    .line 316
    const/16 v7, 0xff

    if-eq v6, v7, :cond_7

    .line 317
    iget-object v6, p0, Lcom/google/obf/cu;->N:[I

    aget v6, v6, v3

    add-int/2addr v5, v6

    .line 318
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 319
    :cond_8
    iget-object v3, p0, Lcom/google/obf/cu;->N:[I

    iget v6, p0, Lcom/google/obf/cu;->M:I

    add-int/lit8 v6, v6, -0x1

    iget v7, p0, Lcom/google/obf/cu;->P:I

    sub-int v7, p2, v7

    sub-int v4, v7, v4

    sub-int/2addr v4, v5

    aput v4, v3, v6

    goto/16 :goto_1

    .line 320
    :cond_9
    const/4 v4, 0x3

    if-ne v3, v4, :cond_12

    .line 321
    const/4 v5, 0x0

    .line 322
    const/4 v4, 0x4

    .line 323
    const/4 v3, 0x0

    :goto_8
    iget v6, p0, Lcom/google/obf/cu;->M:I

    add-int/lit8 v6, v6, -0x1

    if-ge v3, v6, :cond_11

    .line 324
    iget-object v6, p0, Lcom/google/obf/cu;->N:[I

    const/4 v7, 0x0

    aput v7, v6, v3

    .line 325
    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v4}, Lcom/google/obf/cu;->a(Lcom/google/obf/ak;I)V

    .line 326
    iget-object v6, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    iget-object v6, v6, Lcom/google/obf/dw;->a:[B

    add-int/lit8 v7, v4, -0x1

    aget-byte v6, v6, v7

    if-nez v6, :cond_a

    .line 327
    new-instance v2, Lcom/google/obf/s;

    const-string v3, "No valid varint length mask found"

    invoke-direct {v2, v3}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v2

    .line 328
    :cond_a
    const-wide/16 v6, 0x0

    .line 329
    const/4 v8, 0x0

    move v10, v8

    :goto_9
    const/16 v8, 0x8

    if-ge v10, v8, :cond_c

    .line 330
    const/4 v8, 0x1

    rsub-int/lit8 v9, v10, 0x7

    shl-int/2addr v8, v9

    .line 331
    iget-object v9, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    iget-object v9, v9, Lcom/google/obf/dw;->a:[B

    add-int/lit8 v11, v4, -0x1

    aget-byte v9, v9, v11

    and-int/2addr v9, v8

    if-eqz v9, :cond_e

    .line 332
    add-int/lit8 v7, v4, -0x1

    .line 333
    add-int/2addr v4, v10

    .line 334
    move-object/from16 v0, p3

    invoke-direct {p0, v0, v4}, Lcom/google/obf/cu;->a(Lcom/google/obf/ak;I)V

    .line 335
    iget-object v6, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    iget-object v9, v6, Lcom/google/obf/dw;->a:[B

    add-int/lit8 v6, v7, 0x1

    aget-byte v7, v9, v7

    and-int/lit16 v7, v7, 0xff

    xor-int/lit8 v8, v8, -0x1

    and-int/2addr v7, v8

    int-to-long v8, v7

    move v14, v6

    move-wide v6, v8

    move v8, v14

    .line 336
    :goto_a
    if-ge v8, v4, :cond_b

    .line 337
    const/16 v9, 0x8

    shl-long v12, v6, v9

    .line 338
    iget-object v6, p0, Lcom/google/obf/cu;->j:Lcom/google/obf/dw;

    iget-object v7, v6, Lcom/google/obf/dw;->a:[B

    add-int/lit8 v6, v8, 0x1

    aget-byte v7, v7, v8

    and-int/lit16 v7, v7, 0xff

    int-to-long v8, v7

    or-long/2addr v8, v12

    move v14, v6

    move-wide v6, v8

    move v8, v14

    goto :goto_a

    .line 339
    :cond_b
    if-lez v3, :cond_c

    .line 340
    const-wide/16 v8, 0x1

    mul-int/lit8 v10, v10, 0x7

    add-int/lit8 v10, v10, 0x6

    shl-long/2addr v8, v10

    const-wide/16 v10, 0x1

    sub-long/2addr v8, v10

    sub-long/2addr v6, v8

    .line 342
    :cond_c
    const-wide/32 v8, -0x80000000

    cmp-long v8, v6, v8

    if-ltz v8, :cond_d

    const-wide/32 v8, 0x7fffffff

    cmp-long v8, v6, v8

    if-lez v8, :cond_f

    .line 343
    :cond_d
    new-instance v2, Lcom/google/obf/s;

    const-string v3, "EBML lacing sample size out of range."

    invoke-direct {v2, v3}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v2

    .line 341
    :cond_e
    add-int/lit8 v8, v10, 0x1

    move v10, v8

    goto :goto_9

    .line 344
    :cond_f
    long-to-int v6, v6

    .line 345
    iget-object v7, p0, Lcom/google/obf/cu;->N:[I

    if-nez v3, :cond_10

    .line 346
    :goto_b
    aput v6, v7, v3

    .line 347
    iget-object v6, p0, Lcom/google/obf/cu;->N:[I

    aget v6, v6, v3

    add-int/2addr v5, v6

    .line 348
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    .line 346
    :cond_10
    iget-object v8, p0, Lcom/google/obf/cu;->N:[I

    add-int/lit8 v9, v3, -0x1

    aget v8, v8, v9

    add-int/2addr v6, v8

    goto :goto_b

    .line 349
    :cond_11
    iget-object v3, p0, Lcom/google/obf/cu;->N:[I

    iget v6, p0, Lcom/google/obf/cu;->M:I

    add-int/lit8 v6, v6, -0x1

    iget v7, p0, Lcom/google/obf/cu;->P:I

    sub-int v7, p2, v7

    sub-int v4, v7, v4

    sub-int/2addr v4, v5

    aput v4, v3, v6

    goto/16 :goto_1

    .line 351
    :cond_12
    new-instance v2, Lcom/google/obf/s;

    const/16 v4, 0x24

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unexpected lacing value: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v2

    .line 354
    :cond_13
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 355
    :cond_14
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 356
    :cond_15
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 357
    :cond_16
    const/4 v3, 0x0

    goto/16 :goto_5

    .line 367
    :cond_17
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/obf/cu;->I:I

    goto/16 :goto_0

    .line 368
    :cond_18
    iget-object v3, p0, Lcom/google/obf/cu;->N:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p3

    invoke-direct {p0, v0, v2, v3}, Lcom/google/obf/cu;->a(Lcom/google/obf/ak;Lcom/google/obf/cu$b;I)V

    goto/16 :goto_0

    .line 263
    :sswitch_data_0
    .sparse-switch
        0xa1 -> :sswitch_5
        0xa3 -> :sswitch_5
        0x4255 -> :sswitch_3
        0x47e2 -> :sswitch_4
        0x53ab -> :sswitch_0
        0x63a2 -> :sswitch_1
        0x7672 -> :sswitch_2
    .end sparse-switch
.end method

.method a(IJ)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const-wide/16 v2, 0x1

    const/4 v1, 0x1

    .line 125
    sparse-switch p1, :sswitch_data_0

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 126
    :sswitch_0
    cmp-long v0, p2, v2

    if-eqz v0, :cond_0

    .line 127
    new-instance v0, Lcom/google/obf/s;

    const/16 v1, 0x32

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "EBMLReadVersion "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129
    :sswitch_1
    cmp-long v0, p2, v2

    if-ltz v0, :cond_1

    const-wide/16 v0, 0x2

    cmp-long v0, p2, v0

    if-lez v0, :cond_0

    .line 130
    :cond_1
    new-instance v0, Lcom/google/obf/s;

    const/16 v1, 0x35

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "DocTypeReadVersion "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 132
    :sswitch_2
    iget-wide v0, p0, Lcom/google/obf/cu;->r:J

    add-long/2addr v0, p2

    iput-wide v0, p0, Lcom/google/obf/cu;->A:J

    goto :goto_0

    .line 134
    :sswitch_3
    iput-wide p2, p0, Lcom/google/obf/cu;->t:J

    goto :goto_0

    .line 136
    :sswitch_4
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/obf/cu$b;->i:I

    goto :goto_0

    .line 138
    :sswitch_5
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/obf/cu$b;->j:I

    goto :goto_0

    .line 140
    :sswitch_6
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/obf/cu$b;->k:I

    goto :goto_0

    .line 142
    :sswitch_7
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/obf/cu$b;->l:I

    goto :goto_0

    .line 144
    :sswitch_8
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/obf/cu$b;->m:I

    goto :goto_0

    .line 146
    :sswitch_9
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/obf/cu$b;->b:I

    goto :goto_0

    .line 148
    :sswitch_a
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/obf/cu$b;->c:I

    goto/16 :goto_0

    .line 150
    :sswitch_b
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/obf/cu$b;->d:I

    goto/16 :goto_0

    .line 152
    :sswitch_c
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iput-wide p2, v0, Lcom/google/obf/cu$b;->I:J

    goto/16 :goto_0

    .line 154
    :sswitch_d
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iput-wide p2, v0, Lcom/google/obf/cu$b;->J:J

    goto/16 :goto_0

    .line 156
    :sswitch_e
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/obf/cu$b;->F:I

    goto/16 :goto_0

    .line 158
    :sswitch_f
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/obf/cu$b;->G:I

    goto/16 :goto_0

    .line 160
    :sswitch_10
    iput-boolean v1, p0, Lcom/google/obf/cu;->ab:Z

    goto/16 :goto_0

    .line 162
    :sswitch_11
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    .line 163
    new-instance v0, Lcom/google/obf/s;

    const/16 v1, 0x37

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "ContentEncodingOrder "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 165
    :sswitch_12
    cmp-long v0, p2, v2

    if-eqz v0, :cond_0

    .line 166
    new-instance v0, Lcom/google/obf/s;

    const/16 v1, 0x37

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "ContentEncodingScope "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 168
    :sswitch_13
    const-wide/16 v0, 0x3

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    .line 169
    new-instance v0, Lcom/google/obf/s;

    const/16 v1, 0x32

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "ContentCompAlgo "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 171
    :sswitch_14
    const-wide/16 v0, 0x5

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    .line 172
    new-instance v0, Lcom/google/obf/s;

    const/16 v1, 0x31

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "ContentEncAlgo "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 174
    :sswitch_15
    cmp-long v0, p2, v2

    if-eqz v0, :cond_0

    .line 175
    new-instance v0, Lcom/google/obf/s;

    const/16 v1, 0x38

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "AESSettingsCipherMode "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 177
    :sswitch_16
    iget-object v0, p0, Lcom/google/obf/cu;->F:Lcom/google/obf/dr;

    invoke-direct {p0, p2, p3}, Lcom/google/obf/cu;->a(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/obf/dr;->a(J)V

    goto/16 :goto_0

    .line 179
    :sswitch_17
    iget-boolean v0, p0, Lcom/google/obf/cu;->H:Z

    if-nez v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/google/obf/cu;->G:Lcom/google/obf/dr;

    invoke-virtual {v0, p2, p3}, Lcom/google/obf/dr;->a(J)V

    .line 181
    iput-boolean v1, p0, Lcom/google/obf/cu;->H:Z

    goto/16 :goto_0

    .line 183
    :sswitch_18
    invoke-direct {p0, p2, p3}, Lcom/google/obf/cu;->a(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/obf/cu;->E:J

    goto/16 :goto_0

    .line 185
    :sswitch_19
    invoke-direct {p0, p2, p3}, Lcom/google/obf/cu;->a(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/obf/cu;->K:J

    goto/16 :goto_0

    .line 187
    :sswitch_1a
    long-to-int v0, p2

    .line 188
    sparse-switch v0, :sswitch_data_1

    goto/16 :goto_0

    .line 189
    :sswitch_1b
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    const/4 v1, 0x0

    iput v1, v0, Lcom/google/obf/cu$b;->o:I

    goto/16 :goto_0

    .line 191
    :sswitch_1c
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iput v4, v0, Lcom/google/obf/cu$b;->o:I

    goto/16 :goto_0

    .line 193
    :sswitch_1d
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iput v1, v0, Lcom/google/obf/cu$b;->o:I

    goto/16 :goto_0

    .line 195
    :sswitch_1e
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iput v5, v0, Lcom/google/obf/cu$b;->o:I

    goto/16 :goto_0

    .line 198
    :sswitch_1f
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iput-boolean v1, v0, Lcom/google/obf/cu$b;->p:Z

    .line 199
    long-to-int v0, p2

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_0

    .line 200
    :pswitch_1
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iput v1, v0, Lcom/google/obf/cu$b;->q:I

    goto/16 :goto_0

    .line 202
    :pswitch_2
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iput v4, v0, Lcom/google/obf/cu$b;->q:I

    goto/16 :goto_0

    .line 204
    :pswitch_3
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iput v6, v0, Lcom/google/obf/cu$b;->q:I

    goto/16 :goto_0

    .line 207
    :sswitch_20
    long-to-int v0, p2

    sparse-switch v0, :sswitch_data_2

    goto/16 :goto_0

    .line 208
    :sswitch_21
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iput v5, v0, Lcom/google/obf/cu$b;->r:I

    goto/16 :goto_0

    .line 210
    :sswitch_22
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iput v6, v0, Lcom/google/obf/cu$b;->r:I

    goto/16 :goto_0

    .line 212
    :sswitch_23
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    const/4 v1, 0x7

    iput v1, v0, Lcom/google/obf/cu$b;->r:I

    goto/16 :goto_0

    .line 215
    :sswitch_24
    long-to-int v0, p2

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_0

    .line 216
    :pswitch_4
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iput v4, v0, Lcom/google/obf/cu$b;->s:I

    goto/16 :goto_0

    .line 218
    :pswitch_5
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iput v1, v0, Lcom/google/obf/cu$b;->s:I

    goto/16 :goto_0

    .line 221
    :sswitch_25
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/obf/cu$b;->t:I

    goto/16 :goto_0

    .line 223
    :sswitch_26
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    long-to-int v1, p2

    iput v1, v0, Lcom/google/obf/cu$b;->u:I

    goto/16 :goto_0

    .line 125
    :sswitch_data_0
    .sparse-switch
        0x83 -> :sswitch_a
        0x9b -> :sswitch_19
        0x9f -> :sswitch_e
        0xb0 -> :sswitch_4
        0xb3 -> :sswitch_16
        0xba -> :sswitch_5
        0xd7 -> :sswitch_9
        0xe7 -> :sswitch_18
        0xf1 -> :sswitch_17
        0xfb -> :sswitch_10
        0x4254 -> :sswitch_13
        0x4285 -> :sswitch_1
        0x42f7 -> :sswitch_0
        0x47e1 -> :sswitch_14
        0x47e8 -> :sswitch_15
        0x5031 -> :sswitch_11
        0x5032 -> :sswitch_12
        0x53ac -> :sswitch_2
        0x53b8 -> :sswitch_1a
        0x54b0 -> :sswitch_6
        0x54b2 -> :sswitch_8
        0x54ba -> :sswitch_7
        0x55b9 -> :sswitch_24
        0x55ba -> :sswitch_20
        0x55bb -> :sswitch_1f
        0x55bc -> :sswitch_25
        0x55bd -> :sswitch_26
        0x56aa -> :sswitch_c
        0x56bb -> :sswitch_d
        0x6264 -> :sswitch_f
        0x23e383 -> :sswitch_b
        0x2ad7b1 -> :sswitch_3
    .end sparse-switch

    .line 188
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_1b
        0x1 -> :sswitch_1c
        0x3 -> :sswitch_1d
        0xf -> :sswitch_1e
    .end sparse-switch

    .line 199
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 207
    :sswitch_data_2
    .sparse-switch
        0x1 -> :sswitch_21
        0x6 -> :sswitch_21
        0x7 -> :sswitch_21
        0x10 -> :sswitch_22
        0x12 -> :sswitch_23
    .end sparse-switch

    .line 215
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method a(IJJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const-wide/16 v4, -0x1

    const/4 v2, 0x1

    .line 52
    sparse-switch p1, :sswitch_data_0

    .line 82
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 53
    :sswitch_1
    iget-wide v0, p0, Lcom/google/obf/cu;->r:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/google/obf/cu;->r:J

    cmp-long v0, v0, p2

    if-eqz v0, :cond_1

    .line 54
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Multiple Segment elements not supported"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_1
    iput-wide p2, p0, Lcom/google/obf/cu;->r:J

    .line 56
    iput-wide p4, p0, Lcom/google/obf/cu;->s:J

    goto :goto_0

    .line 58
    :sswitch_2
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/obf/cu;->z:I

    .line 59
    iput-wide v4, p0, Lcom/google/obf/cu;->A:J

    goto :goto_0

    .line 61
    :sswitch_3
    new-instance v0, Lcom/google/obf/dr;

    invoke-direct {v0}, Lcom/google/obf/dr;-><init>()V

    iput-object v0, p0, Lcom/google/obf/cu;->F:Lcom/google/obf/dr;

    .line 62
    new-instance v0, Lcom/google/obf/dr;

    invoke-direct {v0}, Lcom/google/obf/dr;-><init>()V

    iput-object v0, p0, Lcom/google/obf/cu;->G:Lcom/google/obf/dr;

    goto :goto_0

    .line 64
    :sswitch_4
    iput-boolean v0, p0, Lcom/google/obf/cu;->H:Z

    goto :goto_0

    .line 66
    :sswitch_5
    iget-boolean v0, p0, Lcom/google/obf/cu;->y:Z

    if-nez v0, :cond_0

    .line 67
    iget-boolean v0, p0, Lcom/google/obf/cu;->g:Z

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lcom/google/obf/cu;->C:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 68
    iput-boolean v2, p0, Lcom/google/obf/cu;->B:Z

    goto :goto_0

    .line 69
    :cond_2
    iget-object v0, p0, Lcom/google/obf/cu;->ac:Lcom/google/obf/al;

    sget-object v1, Lcom/google/obf/aq;->f:Lcom/google/obf/aq;

    invoke-interface {v0, v1}, Lcom/google/obf/al;->a(Lcom/google/obf/aq;)V

    .line 70
    iput-boolean v2, p0, Lcom/google/obf/cu;->y:Z

    goto :goto_0

    .line 72
    :sswitch_6
    iput-boolean v0, p0, Lcom/google/obf/cu;->ab:Z

    goto :goto_0

    .line 75
    :sswitch_7
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iput-boolean v2, v0, Lcom/google/obf/cu$b;->e:Z

    goto :goto_0

    .line 77
    :sswitch_8
    new-instance v0, Lcom/google/obf/cu$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/obf/cu$b;-><init>(Lcom/google/obf/cu$1;)V

    iput-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    goto :goto_0

    .line 79
    :sswitch_9
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iput-boolean v2, v0, Lcom/google/obf/cu$b;->p:Z

    goto :goto_0

    .line 52
    :sswitch_data_0
    .sparse-switch
        0xa0 -> :sswitch_6
        0xae -> :sswitch_8
        0xbb -> :sswitch_4
        0x4dbb -> :sswitch_2
        0x5035 -> :sswitch_7
        0x55d0 -> :sswitch_9
        0x6240 -> :sswitch_0
        0x18538067 -> :sswitch_1
        0x1c53bb6b -> :sswitch_3
        0x1f43b675 -> :sswitch_5
    .end sparse-switch
.end method

.method a(ILjava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    .line 254
    sparse-switch p1, :sswitch_data_0

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 255
    :sswitch_0
    const-string v0, "webm"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "matroska"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 256
    new-instance v0, Lcom/google/obf/s;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x16

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "DocType "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 258
    :sswitch_1
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iput-object p2, v0, Lcom/google/obf/cu$b;->a:Ljava/lang/String;

    goto :goto_0

    .line 260
    :sswitch_2
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    invoke-static {v0, p2}, Lcom/google/obf/cu$b;->a(Lcom/google/obf/cu$b;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 254
    :sswitch_data_0
    .sparse-switch
        0x86 -> :sswitch_1
        0x4282 -> :sswitch_0
        0x22b59c -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Lcom/google/obf/al;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/obf/cu;->ac:Lcom/google/obf/al;

    .line 29
    return-void
.end method

.method public a(Lcom/google/obf/ak;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 27
    new-instance v0, Lcom/google/obf/cs;

    invoke-direct {v0}, Lcom/google/obf/cs;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/obf/cs;->a(Lcom/google/obf/ak;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 30
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/obf/cu;->E:J

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/cu;->I:I

    .line 32
    iget-object v0, p0, Lcom/google/obf/cu;->d:Lcom/google/obf/cq;

    invoke-interface {v0}, Lcom/google/obf/cq;->a()V

    .line 33
    iget-object v0, p0, Lcom/google/obf/cu;->e:Lcom/google/obf/ct;

    invoke-virtual {v0}, Lcom/google/obf/ct;->a()V

    .line 34
    invoke-direct {p0}, Lcom/google/obf/cu;->d()V

    .line 35
    return-void
.end method

.method b(I)Z
    .locals 1

    .prologue
    .line 51
    const v0, 0x1549a966

    if-eq p1, v0, :cond_0

    const v0, 0x1f43b675

    if-eq p1, v0, :cond_0

    const v0, 0x1c53bb6b

    if-eq p1, v0, :cond_0

    const v0, 0x1654ae6b

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 36
    return-void
.end method

.method c(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const-wide/16 v2, -0x1

    .line 83
    sparse-switch p1, :sswitch_data_0

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 84
    :sswitch_0
    iget-wide v0, p0, Lcom/google/obf/cu;->t:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 85
    const-wide/32 v0, 0xf4240

    iput-wide v0, p0, Lcom/google/obf/cu;->t:J

    .line 86
    :cond_1
    iget-wide v0, p0, Lcom/google/obf/cu;->u:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 87
    iget-wide v0, p0, Lcom/google/obf/cu;->u:J

    invoke-direct {p0, v0, v1}, Lcom/google/obf/cu;->a(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/obf/cu;->v:J

    goto :goto_0

    .line 89
    :sswitch_1
    iget v0, p0, Lcom/google/obf/cu;->z:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget-wide v0, p0, Lcom/google/obf/cu;->A:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    .line 90
    :cond_2
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Mandatory element SeekID or SeekPosition not found"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_3
    iget v0, p0, Lcom/google/obf/cu;->z:I

    const v1, 0x1c53bb6b

    if-ne v0, v1, :cond_0

    .line 92
    iget-wide v0, p0, Lcom/google/obf/cu;->A:J

    iput-wide v0, p0, Lcom/google/obf/cu;->C:J

    goto :goto_0

    .line 94
    :sswitch_2
    iget-boolean v0, p0, Lcom/google/obf/cu;->y:Z

    if-nez v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/google/obf/cu;->ac:Lcom/google/obf/al;

    invoke-direct {p0}, Lcom/google/obf/cu;->e()Lcom/google/obf/aq;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/obf/al;->a(Lcom/google/obf/aq;)V

    .line 96
    iput-boolean v5, p0, Lcom/google/obf/cu;->y:Z

    goto :goto_0

    .line 98
    :sswitch_3
    iget v0, p0, Lcom/google/obf/cu;->I:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 100
    iget-boolean v0, p0, Lcom/google/obf/cu;->ab:Z

    if-nez v0, :cond_4

    .line 101
    iget v0, p0, Lcom/google/obf/cu;->Q:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/cu;->Q:I

    .line 102
    :cond_4
    iget-object v0, p0, Lcom/google/obf/cu;->f:Landroid/util/SparseArray;

    iget v1, p0, Lcom/google/obf/cu;->O:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/cu$b;

    iget-wide v2, p0, Lcom/google/obf/cu;->J:J

    invoke-direct {p0, v0, v2, v3}, Lcom/google/obf/cu;->a(Lcom/google/obf/cu$b;J)V

    .line 103
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/cu;->I:I

    goto :goto_0

    .line 105
    :sswitch_4
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iget-boolean v0, v0, Lcom/google/obf/cu$b;->e:Z

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iget-object v0, v0, Lcom/google/obf/cu$b;->g:[B

    if-nez v0, :cond_5

    .line 107
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Encrypted Track found but ContentEncKeyID was not found"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_5
    iget-boolean v0, p0, Lcom/google/obf/cu;->x:Z

    if-nez v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/google/obf/cu;->ac:Lcom/google/obf/al;

    new-instance v1, Lcom/google/obf/ab$c;

    new-instance v2, Lcom/google/obf/ab$b;

    const-string v3, "video/webm"

    iget-object v4, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iget-object v4, v4, Lcom/google/obf/cu$b;->g:[B

    invoke-direct {v2, v3, v4}, Lcom/google/obf/ab$b;-><init>(Ljava/lang/String;[B)V

    invoke-direct {v1, v2}, Lcom/google/obf/ab$c;-><init>(Lcom/google/obf/ab$b;)V

    invoke-interface {v0, v1}, Lcom/google/obf/al;->a(Lcom/google/obf/ab;)V

    .line 110
    iput-boolean v5, p0, Lcom/google/obf/cu;->x:Z

    goto/16 :goto_0

    .line 112
    :sswitch_5
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iget-boolean v0, v0, Lcom/google/obf/cu$b;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iget-object v0, v0, Lcom/google/obf/cu$b;->f:[B

    if-eqz v0, :cond_0

    .line 113
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Combining encryption and compression is not supported"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :sswitch_6
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iget-object v0, v0, Lcom/google/obf/cu$b;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/obf/cu;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 116
    iget-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iget-object v1, p0, Lcom/google/obf/cu;->ac:Lcom/google/obf/al;

    iget-object v2, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iget v2, v2, Lcom/google/obf/cu$b;->b:I

    iget-wide v4, p0, Lcom/google/obf/cu;->v:J

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/google/obf/cu$b;->a(Lcom/google/obf/al;IJ)V

    .line 117
    iget-object v0, p0, Lcom/google/obf/cu;->f:Landroid/util/SparseArray;

    iget-object v1, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    iget v1, v1, Lcom/google/obf/cu$b;->b:I

    iget-object v2, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 118
    :cond_6
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/cu;->w:Lcom/google/obf/cu$b;

    goto/16 :goto_0

    .line 120
    :sswitch_7
    iget-object v0, p0, Lcom/google/obf/cu;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_7

    .line 121
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "No valid tracks were found"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :cond_7
    iget-object v0, p0, Lcom/google/obf/cu;->ac:Lcom/google/obf/al;

    invoke-interface {v0}, Lcom/google/obf/al;->f()V

    goto/16 :goto_0

    .line 83
    nop

    :sswitch_data_0
    .sparse-switch
        0xa0 -> :sswitch_3
        0xae -> :sswitch_6
        0x4dbb -> :sswitch_1
        0x6240 -> :sswitch_4
        0x6d80 -> :sswitch_5
        0x1549a966 -> :sswitch_0
        0x1654ae6b -> :sswitch_7
        0x1c53bb6b -> :sswitch_2
    .end sparse-switch
.end method
