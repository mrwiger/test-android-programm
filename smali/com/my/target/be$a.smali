.class public interface abstract Lcom/my/target/be$a;
.super Ljava/lang/Object;
.source "CommonBannerParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/be;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# static fields
.field public static final CATEGORY:Ljava/lang/String; = "category"

.field public static final DEEPLINK:Ljava/lang/String; = "deeplink"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final DURATION:Ljava/lang/String; = "duration"

.field public static final HEIGHT:Ljava/lang/String; = "height"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final URL:Ljava/lang/String; = "url"

.field public static final VALUE:Ljava/lang/String; = "value"

.field public static final WIDTH:Ljava/lang/String; = "width"

.field public static final cN:Ljava/lang/String; = "statistics"

.field public static final eD:Ljava/lang/String; = "timeout"

.field public static final eE:Ljava/lang/String; = "trackingLink"

.field public static final eF:Ljava/lang/String; = "disclaimer"

.field public static final eG:Ljava/lang/String; = "votes"

.field public static final eH:Ljava/lang/String; = "rating"

.field public static final eI:Ljava/lang/String; = "domain"

.field public static final eJ:Ljava/lang/String; = "subcategory"

.field public static final eK:Ljava/lang/String; = "urlscheme"

.field public static final eL:Ljava/lang/String; = "ageRestrictions"

.field public static final eM:Ljava/lang/String; = "bundle_id"

.field public static final eN:Ljava/lang/String; = "openInBrowser"

.field public static final eO:Ljava/lang/String; = "usePlayStoreAction"

.field public static final eP:Ljava/lang/String; = "directLink"

.field public static final eQ:Ljava/lang/String; = "navigationType"

.field public static final eR:Ljava/lang/String; = "ctaText"

.field public static final eS:Ljava/lang/String; = "iconLink"

.field public static final eT:Ljava/lang/String; = "iconWidth"

.field public static final eU:Ljava/lang/String; = "iconHeight"

.field public static final eV:Ljava/lang/String; = "imageLink"

.field public static final eW:Ljava/lang/String; = "imageWidth"

.field public static final eX:Ljava/lang/String; = "imageHeight"

.field public static final eY:Ljava/lang/String; = "pvalue"

.field public static final eZ:Ljava/lang/String; = "viewablePercent"

.field public static final eg:Ljava/lang/String; = "bannerID"

.field public static final fa:Ljava/lang/String; = "ovv"

.field public static final fb:Ljava/lang/String; = "source"

.field public static final fc:Ljava/lang/String; = "clickArea"

.field public static final fd:Ljava/lang/String; = "extendedClickArea"
