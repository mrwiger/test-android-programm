.class final Landroid/support/v7/preference/CollapsiblePreferenceGroupController;
.super Ljava/lang/Object;
.source "CollapsiblePreferenceGroupController.java"

# interfaces
.implements Landroid/support/v7/preference/PreferenceGroup$PreferenceInstanceStateCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v7/preference/CollapsiblePreferenceGroupController$SavedState;,
        Landroid/support/v7/preference/CollapsiblePreferenceGroupController$ExpandButton;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mMaxPreferenceToShow:I

.field private final mPreferenceGroupAdapter:Landroid/support/v7/preference/PreferenceGroupAdapter;


# direct methods
.method constructor <init>(Landroid/support/v7/preference/PreferenceGroup;Landroid/support/v7/preference/PreferenceGroupAdapter;)V
    .locals 1
    .param p1, "preferenceGroup"    # Landroid/support/v7/preference/PreferenceGroup;
    .param p2, "preferenceGroupAdapter"    # Landroid/support/v7/preference/PreferenceGroupAdapter;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p2, p0, Landroid/support/v7/preference/CollapsiblePreferenceGroupController;->mPreferenceGroupAdapter:Landroid/support/v7/preference/PreferenceGroupAdapter;

    .line 40
    invoke-virtual {p1}, Landroid/support/v7/preference/PreferenceGroup;->getInitialExpandedChildrenCount()I

    move-result v0

    iput v0, p0, Landroid/support/v7/preference/CollapsiblePreferenceGroupController;->mMaxPreferenceToShow:I

    .line 41
    invoke-virtual {p1}, Landroid/support/v7/preference/PreferenceGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/preference/CollapsiblePreferenceGroupController;->mContext:Landroid/content/Context;

    .line 42
    invoke-virtual {p1, p0}, Landroid/support/v7/preference/PreferenceGroup;->setPreferenceInstanceStateCallback(Landroid/support/v7/preference/PreferenceGroup$PreferenceInstanceStateCallback;)V

    .line 43
    return-void
.end method

.method static synthetic access$002(Landroid/support/v7/preference/CollapsiblePreferenceGroupController;I)I
    .locals 0
    .param p0, "x0"    # Landroid/support/v7/preference/CollapsiblePreferenceGroupController;
    .param p1, "x1"    # I

    .prologue
    .line 30
    iput p1, p0, Landroid/support/v7/preference/CollapsiblePreferenceGroupController;->mMaxPreferenceToShow:I

    return p1
.end method

.method static synthetic access$100(Landroid/support/v7/preference/CollapsiblePreferenceGroupController;)Landroid/support/v7/preference/PreferenceGroupAdapter;
    .locals 1
    .param p0, "x0"    # Landroid/support/v7/preference/CollapsiblePreferenceGroupController;

    .prologue
    .line 30
    iget-object v0, p0, Landroid/support/v7/preference/CollapsiblePreferenceGroupController;->mPreferenceGroupAdapter:Landroid/support/v7/preference/PreferenceGroupAdapter;

    return-object v0
.end method

.method private createExpandButton(Ljava/util/List;Ljava/util/List;)Landroid/support/v7/preference/CollapsiblePreferenceGroupController$ExpandButton;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/support/v7/preference/Preference;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/support/v7/preference/Preference;",
            ">;)",
            "Landroid/support/v7/preference/CollapsiblePreferenceGroupController$ExpandButton;"
        }
    .end annotation

    .prologue
    .line 120
    .local p1, "visiblePreferenceList":Ljava/util/List;, "Ljava/util/List<Landroid/support/v7/preference/Preference;>;"
    .local p2, "flattenedPreferenceList":Ljava/util/List;, "Ljava/util/List<Landroid/support/v7/preference/Preference;>;"
    new-instance v0, Landroid/support/v7/preference/CollapsiblePreferenceGroupController$ExpandButton;

    iget-object v1, p0, Landroid/support/v7/preference/CollapsiblePreferenceGroupController;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1, p2}, Landroid/support/v7/preference/CollapsiblePreferenceGroupController$ExpandButton;-><init>(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V

    .line 122
    .local v0, "preference":Landroid/support/v7/preference/CollapsiblePreferenceGroupController$ExpandButton;
    new-instance v1, Landroid/support/v7/preference/CollapsiblePreferenceGroupController$1;

    invoke-direct {v1, p0}, Landroid/support/v7/preference/CollapsiblePreferenceGroupController$1;-><init>(Landroid/support/v7/preference/CollapsiblePreferenceGroupController;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/CollapsiblePreferenceGroupController$ExpandButton;->setOnPreferenceClickListener(Landroid/support/v7/preference/Preference$OnPreferenceClickListener;)V

    .line 130
    return-object v0
.end method

.method private showLimitedChildren()Z
    .locals 2

    .prologue
    .line 134
    iget v0, p0, Landroid/support/v7/preference/CollapsiblePreferenceGroupController;->mMaxPreferenceToShow:I

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public createVisiblePreferencesList(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/support/v7/preference/Preference;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/support/v7/preference/Preference;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    .local p1, "flattenedPreferenceList":Ljava/util/List;, "Ljava/util/List<Landroid/support/v7/preference/Preference;>;"
    const/4 v2, 0x0

    .line 53
    .local v2, "visiblePreferenceCount":I
    new-instance v3, Ljava/util/ArrayList;

    .line 54
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 56
    .local v3, "visiblePreferenceList":Ljava/util/List;, "Ljava/util/List<Landroid/support/v7/preference/Preference;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/preference/Preference;

    .line 57
    .local v1, "preference":Landroid/support/v7/preference/Preference;
    invoke-virtual {v1}, Landroid/support/v7/preference/Preference;->isVisible()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 58
    iget v5, p0, Landroid/support/v7/preference/CollapsiblePreferenceGroupController;->mMaxPreferenceToShow:I

    if-ge v2, v5, :cond_1

    .line 59
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    :cond_1
    instance-of v5, v1, Landroid/support/v7/preference/PreferenceGroup;

    if-nez v5, :cond_0

    .line 64
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 71
    .end local v1    # "preference":Landroid/support/v7/preference/Preference;
    :cond_2
    invoke-direct {p0}, Landroid/support/v7/preference/CollapsiblePreferenceGroupController;->showLimitedChildren()Z

    move-result v4

    if-eqz v4, :cond_3

    iget v4, p0, Landroid/support/v7/preference/CollapsiblePreferenceGroupController;->mMaxPreferenceToShow:I

    if-le v2, v4, :cond_3

    .line 72
    invoke-direct {p0, v3, p1}, Landroid/support/v7/preference/CollapsiblePreferenceGroupController;->createExpandButton(Ljava/util/List;Ljava/util/List;)Landroid/support/v7/preference/CollapsiblePreferenceGroupController$ExpandButton;

    move-result-object v0

    .line 74
    .local v0, "expandButton":Landroid/support/v7/preference/CollapsiblePreferenceGroupController$ExpandButton;
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    .end local v0    # "expandButton":Landroid/support/v7/preference/CollapsiblePreferenceGroupController$ExpandButton;
    :cond_3
    return-object v3
.end method

.method public onPreferenceVisibilityChange(Landroid/support/v7/preference/Preference;)Z
    .locals 1
    .param p1, "preference"    # Landroid/support/v7/preference/Preference;

    .prologue
    .line 86
    invoke-direct {p0}, Landroid/support/v7/preference/CollapsiblePreferenceGroupController;->showLimitedChildren()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Landroid/support/v7/preference/CollapsiblePreferenceGroupController;->mPreferenceGroupAdapter:Landroid/support/v7/preference/PreferenceGroupAdapter;

    invoke-virtual {v0, p1}, Landroid/support/v7/preference/PreferenceGroupAdapter;->onPreferenceHierarchyChange(Landroid/support/v7/preference/Preference;)V

    .line 91
    const/4 v0, 0x1

    .line 93
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public restoreInstanceState(Landroid/os/Parcelable;)Landroid/os/Parcelable;
    .locals 4
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 105
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Landroid/support/v7/preference/CollapsiblePreferenceGroupController$SavedState;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 115
    .end local p1    # "state":Landroid/os/Parcelable;
    :cond_0
    :goto_0
    return-object p1

    .restart local p1    # "state":Landroid/os/Parcelable;
    :cond_1
    move-object v0, p1

    .line 109
    check-cast v0, Landroid/support/v7/preference/CollapsiblePreferenceGroupController$SavedState;

    .line 110
    .local v0, "myState":Landroid/support/v7/preference/CollapsiblePreferenceGroupController$SavedState;
    iget v1, v0, Landroid/support/v7/preference/CollapsiblePreferenceGroupController$SavedState;->mMaxPreferenceToShow:I

    .line 111
    .local v1, "restoredMaxToShow":I
    iget v2, p0, Landroid/support/v7/preference/CollapsiblePreferenceGroupController;->mMaxPreferenceToShow:I

    if-eq v2, v1, :cond_2

    .line 112
    iput v1, p0, Landroid/support/v7/preference/CollapsiblePreferenceGroupController;->mMaxPreferenceToShow:I

    .line 113
    iget-object v2, p0, Landroid/support/v7/preference/CollapsiblePreferenceGroupController;->mPreferenceGroupAdapter:Landroid/support/v7/preference/PreferenceGroupAdapter;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/support/v7/preference/PreferenceGroupAdapter;->onPreferenceHierarchyChange(Landroid/support/v7/preference/Preference;)V

    .line 115
    :cond_2
    invoke-virtual {v0}, Landroid/support/v7/preference/CollapsiblePreferenceGroupController$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object p1

    goto :goto_0
.end method

.method public saveInstanceState(Landroid/os/Parcelable;)Landroid/os/Parcelable;
    .locals 2
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 98
    new-instance v0, Landroid/support/v7/preference/CollapsiblePreferenceGroupController$SavedState;

    invoke-direct {v0, p1}, Landroid/support/v7/preference/CollapsiblePreferenceGroupController$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 99
    .local v0, "myState":Landroid/support/v7/preference/CollapsiblePreferenceGroupController$SavedState;
    iget v1, p0, Landroid/support/v7/preference/CollapsiblePreferenceGroupController;->mMaxPreferenceToShow:I

    iput v1, v0, Landroid/support/v7/preference/CollapsiblePreferenceGroupController$SavedState;->mMaxPreferenceToShow:I

    .line 100
    return-object v0
.end method
