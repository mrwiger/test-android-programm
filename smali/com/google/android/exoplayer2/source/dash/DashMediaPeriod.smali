.class final Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;
.super Ljava/lang/Object;
.source "DashMediaPeriod.java"

# interfaces
.implements Lcom/google/android/exoplayer2/source/MediaPeriod;
.implements Lcom/google/android/exoplayer2/source/SequenceableLoader$Callback;
.implements Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream$ReleaseCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/exoplayer2/source/MediaPeriod;",
        "Lcom/google/android/exoplayer2/source/SequenceableLoader$Callback",
        "<",
        "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream",
        "<",
        "Lcom/google/android/exoplayer2/source/dash/DashChunkSource;",
        ">;>;",
        "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream$ReleaseCallback",
        "<",
        "Lcom/google/android/exoplayer2/source/dash/DashChunkSource;",
        ">;"
    }
.end annotation


# instance fields
.field private final allocator:Lcom/google/android/exoplayer2/upstream/Allocator;

.field private callback:Lcom/google/android/exoplayer2/source/MediaPeriod$Callback;

.field private final chunkSourceFactory:Lcom/google/android/exoplayer2/source/dash/DashChunkSource$Factory;

.field private compositeSequenceableLoader:Lcom/google/android/exoplayer2/source/SequenceableLoader;

.field private final compositeSequenceableLoaderFactory:Lcom/google/android/exoplayer2/source/CompositeSequenceableLoaderFactory;

.field private final elapsedRealtimeOffset:J

.field private final eventDispatcher:Lcom/google/android/exoplayer2/source/MediaSourceEventListener$EventDispatcher;

.field private eventSampleStreams:[Lcom/google/android/exoplayer2/source/dash/EventSampleStream;

.field private eventStreams:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;",
            ">;"
        }
    .end annotation
.end field

.field final id:I

.field private manifest:Lcom/google/android/exoplayer2/source/dash/manifest/DashManifest;

.field private final manifestLoaderErrorThrower:Lcom/google/android/exoplayer2/upstream/LoaderErrorThrower;

.field private final minLoadableRetryCount:I

.field private periodIndex:I

.field private final playerEmsgHandler:Lcom/google/android/exoplayer2/source/dash/PlayerEmsgHandler;

.field private sampleStreams:[Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream",
            "<",
            "Lcom/google/android/exoplayer2/source/dash/DashChunkSource;",
            ">;"
        }
    .end annotation
.end field

.field private final trackEmsgHandlerBySampleStream:Ljava/util/IdentityHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/IdentityHashMap",
            "<",
            "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream",
            "<",
            "Lcom/google/android/exoplayer2/source/dash/DashChunkSource;",
            ">;",
            "Lcom/google/android/exoplayer2/source/dash/PlayerEmsgHandler$PlayerTrackEmsgHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final trackGroupInfos:[Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;

.field private final trackGroups:Lcom/google/android/exoplayer2/source/TrackGroupArray;


# direct methods
.method public constructor <init>(ILcom/google/android/exoplayer2/source/dash/manifest/DashManifest;ILcom/google/android/exoplayer2/source/dash/DashChunkSource$Factory;ILcom/google/android/exoplayer2/source/MediaSourceEventListener$EventDispatcher;JLcom/google/android/exoplayer2/upstream/LoaderErrorThrower;Lcom/google/android/exoplayer2/upstream/Allocator;Lcom/google/android/exoplayer2/source/CompositeSequenceableLoaderFactory;Lcom/google/android/exoplayer2/source/dash/PlayerEmsgHandler$PlayerEmsgCallback;)V
    .locals 5
    .param p1, "id"    # I
    .param p2, "manifest"    # Lcom/google/android/exoplayer2/source/dash/manifest/DashManifest;
    .param p3, "periodIndex"    # I
    .param p4, "chunkSourceFactory"    # Lcom/google/android/exoplayer2/source/dash/DashChunkSource$Factory;
    .param p5, "minLoadableRetryCount"    # I
    .param p6, "eventDispatcher"    # Lcom/google/android/exoplayer2/source/MediaSourceEventListener$EventDispatcher;
    .param p7, "elapsedRealtimeOffset"    # J
    .param p9, "manifestLoaderErrorThrower"    # Lcom/google/android/exoplayer2/upstream/LoaderErrorThrower;
    .param p10, "allocator"    # Lcom/google/android/exoplayer2/upstream/Allocator;
    .param p11, "compositeSequenceableLoaderFactory"    # Lcom/google/android/exoplayer2/source/CompositeSequenceableLoaderFactory;
    .param p12, "playerEmsgCallback"    # Lcom/google/android/exoplayer2/source/dash/PlayerEmsgHandler$PlayerEmsgCallback;

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput p1, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->id:I

    .line 96
    iput-object p2, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->manifest:Lcom/google/android/exoplayer2/source/dash/manifest/DashManifest;

    .line 97
    iput p3, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->periodIndex:I

    .line 98
    iput-object p4, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->chunkSourceFactory:Lcom/google/android/exoplayer2/source/dash/DashChunkSource$Factory;

    .line 99
    iput p5, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->minLoadableRetryCount:I

    .line 100
    iput-object p6, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->eventDispatcher:Lcom/google/android/exoplayer2/source/MediaSourceEventListener$EventDispatcher;

    .line 101
    iput-wide p7, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->elapsedRealtimeOffset:J

    .line 102
    iput-object p9, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->manifestLoaderErrorThrower:Lcom/google/android/exoplayer2/upstream/LoaderErrorThrower;

    .line 103
    iput-object p10, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->allocator:Lcom/google/android/exoplayer2/upstream/Allocator;

    .line 104
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->compositeSequenceableLoaderFactory:Lcom/google/android/exoplayer2/source/CompositeSequenceableLoaderFactory;

    .line 105
    new-instance v3, Lcom/google/android/exoplayer2/source/dash/PlayerEmsgHandler;

    move-object/from16 v0, p12

    invoke-direct {v3, p2, v0, p10}, Lcom/google/android/exoplayer2/source/dash/PlayerEmsgHandler;-><init>(Lcom/google/android/exoplayer2/source/dash/manifest/DashManifest;Lcom/google/android/exoplayer2/source/dash/PlayerEmsgHandler$PlayerEmsgCallback;Lcom/google/android/exoplayer2/upstream/Allocator;)V

    iput-object v3, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->playerEmsgHandler:Lcom/google/android/exoplayer2/source/dash/PlayerEmsgHandler;

    .line 106
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->newSampleStreamArray(I)[Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->sampleStreams:[Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;

    .line 107
    const/4 v3, 0x0

    new-array v3, v3, [Lcom/google/android/exoplayer2/source/dash/EventSampleStream;

    iput-object v3, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->eventSampleStreams:[Lcom/google/android/exoplayer2/source/dash/EventSampleStream;

    .line 108
    new-instance v3, Ljava/util/IdentityHashMap;

    invoke-direct {v3}, Ljava/util/IdentityHashMap;-><init>()V

    iput-object v3, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->trackEmsgHandlerBySampleStream:Ljava/util/IdentityHashMap;

    .line 109
    iget-object v3, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->sampleStreams:[Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;

    .line 110
    move-object/from16 v0, p11

    invoke-interface {v0, v3}, Lcom/google/android/exoplayer2/source/CompositeSequenceableLoaderFactory;->createCompositeSequenceableLoader([Lcom/google/android/exoplayer2/source/SequenceableLoader;)Lcom/google/android/exoplayer2/source/SequenceableLoader;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->compositeSequenceableLoader:Lcom/google/android/exoplayer2/source/SequenceableLoader;

    .line 111
    invoke-virtual {p2, p3}, Lcom/google/android/exoplayer2/source/dash/manifest/DashManifest;->getPeriod(I)Lcom/google/android/exoplayer2/source/dash/manifest/Period;

    move-result-object v1

    .line 112
    .local v1, "period":Lcom/google/android/exoplayer2/source/dash/manifest/Period;
    iget-object v3, v1, Lcom/google/android/exoplayer2/source/dash/manifest/Period;->eventStreams:Ljava/util/List;

    iput-object v3, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->eventStreams:Ljava/util/List;

    .line 113
    iget-object v3, v1, Lcom/google/android/exoplayer2/source/dash/manifest/Period;->adaptationSets:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->eventStreams:Ljava/util/List;

    invoke-static {v3, v4}, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->buildTrackGroups(Ljava/util/List;Ljava/util/List;)Landroid/util/Pair;

    move-result-object v2

    .line 115
    .local v2, "result":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/exoplayer2/source/TrackGroupArray;[Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;>;"
    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Lcom/google/android/exoplayer2/source/TrackGroupArray;

    iput-object v3, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->trackGroups:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    .line 116
    iget-object v3, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, [Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;

    iput-object v3, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->trackGroupInfos:[Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;

    .line 117
    return-void
.end method

.method private static buildManifestEventTrackGroupInfos(Ljava/util/List;[Lcom/google/android/exoplayer2/source/TrackGroup;[Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;I)V
    .locals 8
    .param p1, "trackGroups"    # [Lcom/google/android/exoplayer2/source/TrackGroup;
    .param p2, "trackGroupInfos"    # [Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;
    .param p3, "existingTrackGroupCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;",
            ">;[",
            "Lcom/google/android/exoplayer2/source/TrackGroup;",
            "[",
            "Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;",
            "I)V"
        }
    .end annotation

    .prologue
    .local p0, "eventStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;>;"
    const/4 v7, 0x0

    .line 510
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 511
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;

    .line 512
    .local v0, "eventStream":Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;->id()Ljava/lang/String;

    move-result-object v4

    const-string v5, "application/x-emsg"

    const/4 v6, -0x1

    invoke-static {v4, v5, v7, v6, v7}, Lcom/google/android/exoplayer2/Format;->createSampleFormat(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/exoplayer2/drm/DrmInitData;)Lcom/google/android/exoplayer2/Format;

    move-result-object v2

    .line 514
    .local v2, "format":Lcom/google/android/exoplayer2/Format;
    new-instance v4, Lcom/google/android/exoplayer2/source/TrackGroup;

    const/4 v5, 0x1

    new-array v5, v5, [Lcom/google/android/exoplayer2/Format;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-direct {v4, v5}, Lcom/google/android/exoplayer2/source/TrackGroup;-><init>([Lcom/google/android/exoplayer2/Format;)V

    aput-object v4, p1, p3

    .line 515
    add-int/lit8 v1, p3, 0x1

    .end local p3    # "existingTrackGroupCount":I
    .local v1, "existingTrackGroupCount":I
    invoke-static {v3}, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;->mpdEventTrack(I)Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;

    move-result-object v4

    aput-object v4, p2, p3

    .line 510
    add-int/lit8 v3, v3, 0x1

    move p3, v1

    .end local v1    # "existingTrackGroupCount":I
    .restart local p3    # "existingTrackGroupCount":I
    goto :goto_0

    .line 517
    .end local v0    # "eventStream":Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;
    .end local v2    # "format":Lcom/google/android/exoplayer2/Format;
    :cond_0
    return-void
.end method

.method private static buildPrimaryAndEmbeddedTrackGroupInfos(Ljava/util/List;[[II[Z[Z[Lcom/google/android/exoplayer2/source/TrackGroup;[Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;)I
    .locals 19
    .param p1, "groupedAdaptationSetIndices"    # [[I
    .param p2, "primaryGroupCount"    # I
    .param p3, "primaryGroupHasEventMessageTrackFlags"    # [Z
    .param p4, "primaryGroupHasCea608TrackFlags"    # [Z
    .param p5, "trackGroups"    # [Lcom/google/android/exoplayer2/source/TrackGroup;
    .param p6, "trackGroupInfos"    # [Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;",
            ">;[[II[Z[Z[",
            "Lcom/google/android/exoplayer2/source/TrackGroup;",
            "[",
            "Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;",
            ")I"
        }
    .end annotation

    .prologue
    .line 463
    .local p0, "adaptationSets":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;>;"
    const/4 v12, 0x0

    .line 464
    .local v12, "trackGroupCount":I
    const/4 v8, 0x0

    .local v8, "i":I
    move v13, v12

    .end local v12    # "trackGroupCount":I
    .local v13, "trackGroupCount":I
    :goto_0
    move/from16 v0, p2

    if-ge v8, v0, :cond_6

    .line 465
    aget-object v2, p1, v8

    .line 466
    .local v2, "adaptationSetIndices":[I
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 467
    .local v11, "representations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/exoplayer2/source/dash/manifest/Representation;>;"
    array-length v0, v2

    move/from16 v16, v0

    const/4 v14, 0x0

    move v15, v14

    :goto_1
    move/from16 v0, v16

    if-ge v15, v0, :cond_0

    aget v1, v2, v15

    .line 468
    .local v1, "adaptationSetIndex":I
    move-object/from16 v0, p0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;

    iget-object v14, v14, Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;->representations:Ljava/util/List;

    invoke-interface {v11, v14}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 467
    add-int/lit8 v14, v15, 0x1

    move v15, v14

    goto :goto_1

    .line 470
    .end local v1    # "adaptationSetIndex":I
    :cond_0
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v14

    new-array v7, v14, [Lcom/google/android/exoplayer2/Format;

    .line 471
    .local v7, "formats":[Lcom/google/android/exoplayer2/Format;
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_2
    array-length v14, v7

    if-ge v9, v14, :cond_1

    .line 472
    invoke-interface {v11, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/exoplayer2/source/dash/manifest/Representation;

    iget-object v14, v14, Lcom/google/android/exoplayer2/source/dash/manifest/Representation;->format:Lcom/google/android/exoplayer2/Format;

    aput-object v14, v7, v9

    .line 471
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 475
    :cond_1
    const/4 v14, 0x0

    aget v14, v2, v14

    move-object/from16 v0, p0

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;

    .line 476
    .local v5, "firstAdaptationSet":Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;
    add-int/lit8 v12, v13, 0x1

    .end local v13    # "trackGroupCount":I
    .restart local v12    # "trackGroupCount":I
    move v10, v13

    .line 477
    .local v10, "primaryTrackGroupIndex":I
    aget-boolean v14, p3, v8

    if-eqz v14, :cond_4

    add-int/lit8 v13, v12, 0x1

    .end local v12    # "trackGroupCount":I
    .restart local v13    # "trackGroupCount":I
    move v4, v12

    move v12, v13

    .line 479
    .end local v13    # "trackGroupCount":I
    .local v4, "eventMessageTrackGroupIndex":I
    .restart local v12    # "trackGroupCount":I
    :goto_3
    aget-boolean v14, p4, v8

    if-eqz v14, :cond_5

    add-int/lit8 v13, v12, 0x1

    .end local v12    # "trackGroupCount":I
    .restart local v13    # "trackGroupCount":I
    move v3, v12

    move v12, v13

    .line 482
    .end local v13    # "trackGroupCount":I
    .local v3, "cea608TrackGroupIndex":I
    .restart local v12    # "trackGroupCount":I
    :goto_4
    new-instance v14, Lcom/google/android/exoplayer2/source/TrackGroup;

    invoke-direct {v14, v7}, Lcom/google/android/exoplayer2/source/TrackGroup;-><init>([Lcom/google/android/exoplayer2/Format;)V

    aput-object v14, p5, v10

    .line 483
    iget v14, v5, Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;->type:I

    .line 484
    invoke-static {v14, v2, v10, v4, v3}, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;->primaryTrack(I[IIII)Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;

    move-result-object v14

    aput-object v14, p6, v10

    .line 490
    const/4 v14, -0x1

    if-eq v4, v14, :cond_2

    .line 491
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    iget v15, v5, Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;->id:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ":emsg"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    const-string v15, "application/x-emsg"

    const/16 v16, 0x0

    const/16 v17, -0x1

    const/16 v18, 0x0

    invoke-static/range {v14 .. v18}, Lcom/google/android/exoplayer2/Format;->createSampleFormat(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/exoplayer2/drm/DrmInitData;)Lcom/google/android/exoplayer2/Format;

    move-result-object v6

    .line 493
    .local v6, "format":Lcom/google/android/exoplayer2/Format;
    new-instance v14, Lcom/google/android/exoplayer2/source/TrackGroup;

    const/4 v15, 0x1

    new-array v15, v15, [Lcom/google/android/exoplayer2/Format;

    const/16 v16, 0x0

    aput-object v6, v15, v16

    invoke-direct {v14, v15}, Lcom/google/android/exoplayer2/source/TrackGroup;-><init>([Lcom/google/android/exoplayer2/Format;)V

    aput-object v14, p5, v4

    .line 495
    invoke-static {v2, v10}, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;->embeddedEmsgTrack([II)Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;

    move-result-object v14

    aput-object v14, p6, v4

    .line 497
    .end local v6    # "format":Lcom/google/android/exoplayer2/Format;
    :cond_2
    const/4 v14, -0x1

    if-eq v3, v14, :cond_3

    .line 498
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    iget v15, v5, Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;->id:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ":cea608"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    const-string v15, "application/cea-608"

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-static/range {v14 .. v17}, Lcom/google/android/exoplayer2/Format;->createTextSampleFormat(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/google/android/exoplayer2/Format;

    move-result-object v6

    .line 500
    .restart local v6    # "format":Lcom/google/android/exoplayer2/Format;
    new-instance v14, Lcom/google/android/exoplayer2/source/TrackGroup;

    const/4 v15, 0x1

    new-array v15, v15, [Lcom/google/android/exoplayer2/Format;

    const/16 v16, 0x0

    aput-object v6, v15, v16

    invoke-direct {v14, v15}, Lcom/google/android/exoplayer2/source/TrackGroup;-><init>([Lcom/google/android/exoplayer2/Format;)V

    aput-object v14, p5, v3

    .line 502
    invoke-static {v2, v10}, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;->embeddedCea608Track([II)Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;

    move-result-object v14

    aput-object v14, p6, v3

    .line 464
    .end local v6    # "format":Lcom/google/android/exoplayer2/Format;
    :cond_3
    add-int/lit8 v8, v8, 0x1

    move v13, v12

    .end local v12    # "trackGroupCount":I
    .restart local v13    # "trackGroupCount":I
    goto/16 :goto_0

    .line 477
    .end local v3    # "cea608TrackGroupIndex":I
    .end local v4    # "eventMessageTrackGroupIndex":I
    .end local v13    # "trackGroupCount":I
    .restart local v12    # "trackGroupCount":I
    :cond_4
    const/4 v4, -0x1

    goto/16 :goto_3

    .line 479
    .restart local v4    # "eventMessageTrackGroupIndex":I
    :cond_5
    const/4 v3, -0x1

    goto/16 :goto_4

    .line 505
    .end local v2    # "adaptationSetIndices":[I
    .end local v4    # "eventMessageTrackGroupIndex":I
    .end local v5    # "firstAdaptationSet":Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;
    .end local v7    # "formats":[Lcom/google/android/exoplayer2/Format;
    .end local v9    # "j":I
    .end local v10    # "primaryTrackGroupIndex":I
    .end local v11    # "representations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/exoplayer2/source/dash/manifest/Representation;>;"
    .end local v12    # "trackGroupCount":I
    .restart local v13    # "trackGroupCount":I
    :cond_6
    return v13
.end method

.method private buildSampleStream(Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;Lcom/google/android/exoplayer2/trackselection/TrackSelection;J)Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;
    .locals 27
    .param p1, "trackGroupInfo"    # Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;
    .param p2, "selection"    # Lcom/google/android/exoplayer2/trackselection/TrackSelection;
    .param p3, "positionUs"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;",
            "Lcom/google/android/exoplayer2/trackselection/TrackSelection;",
            "J)",
            "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream",
            "<",
            "Lcom/google/android/exoplayer2/source/dash/DashChunkSource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 521
    const/4 v2, 0x0

    .line 522
    .local v2, "embeddedTrackCount":I
    const/4 v3, 0x2

    new-array v0, v3, [I

    move-object/from16 v17, v0

    .line 523
    .local v17, "embeddedTrackTypes":[I
    const/4 v3, 0x2

    new-array v0, v3, [Lcom/google/android/exoplayer2/Format;

    move-object/from16 v18, v0

    .line 524
    .local v18, "embeddedTrackFormats":[Lcom/google/android/exoplayer2/Format;
    move-object/from16 v0, p1

    iget v3, v0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;->embeddedEventMessageTrackGroupIndex:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v12, 0x1

    .line 526
    .local v12, "enableEventMessageTrack":Z
    :goto_0
    if-eqz v12, :cond_0

    .line 527
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->trackGroups:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;->embeddedEventMessageTrackGroupIndex:I

    .line 528
    invoke-virtual {v3, v4}, Lcom/google/android/exoplayer2/source/TrackGroupArray;->get(I)Lcom/google/android/exoplayer2/source/TrackGroup;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/exoplayer2/source/TrackGroup;->getFormat(I)Lcom/google/android/exoplayer2/Format;

    move-result-object v3

    aput-object v3, v18, v2

    .line 529
    add-int/lit8 v26, v2, 0x1

    .end local v2    # "embeddedTrackCount":I
    .local v26, "embeddedTrackCount":I
    const/4 v3, 0x4

    aput v3, v17, v2

    move/from16 v2, v26

    .line 531
    .end local v26    # "embeddedTrackCount":I
    .restart local v2    # "embeddedTrackCount":I
    :cond_0
    move-object/from16 v0, p1

    iget v3, v0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;->embeddedCea608TrackGroupIndex:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v13, 0x1

    .line 532
    .local v13, "enableCea608Track":Z
    :goto_1
    if-eqz v13, :cond_1

    .line 533
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->trackGroups:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;->embeddedCea608TrackGroupIndex:I

    .line 534
    invoke-virtual {v3, v4}, Lcom/google/android/exoplayer2/source/TrackGroupArray;->get(I)Lcom/google/android/exoplayer2/source/TrackGroup;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/exoplayer2/source/TrackGroup;->getFormat(I)Lcom/google/android/exoplayer2/Format;

    move-result-object v3

    aput-object v3, v18, v2

    .line 535
    add-int/lit8 v26, v2, 0x1

    .end local v2    # "embeddedTrackCount":I
    .restart local v26    # "embeddedTrackCount":I
    const/4 v3, 0x3

    aput v3, v17, v2

    move/from16 v2, v26

    .line 537
    .end local v26    # "embeddedTrackCount":I
    .restart local v2    # "embeddedTrackCount":I
    :cond_1
    move-object/from16 v0, v17

    array-length v3, v0

    if-ge v2, v3, :cond_2

    .line 538
    move-object/from16 v0, v18

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v18

    .end local v18    # "embeddedTrackFormats":[Lcom/google/android/exoplayer2/Format;
    check-cast v18, [Lcom/google/android/exoplayer2/Format;

    .line 539
    .restart local v18    # "embeddedTrackFormats":[Lcom/google/android/exoplayer2/Format;
    move-object/from16 v0, v17

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v17

    .line 541
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->manifest:Lcom/google/android/exoplayer2/source/dash/manifest/DashManifest;

    iget-boolean v3, v3, Lcom/google/android/exoplayer2/source/dash/manifest/DashManifest;->dynamic:Z

    if-eqz v3, :cond_5

    if-eqz v12, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->playerEmsgHandler:Lcom/google/android/exoplayer2/source/dash/PlayerEmsgHandler;

    .line 543
    invoke-virtual {v3}, Lcom/google/android/exoplayer2/source/dash/PlayerEmsgHandler;->newPlayerTrackEmsgHandler()Lcom/google/android/exoplayer2/source/dash/PlayerEmsgHandler$PlayerTrackEmsgHandler;

    move-result-object v14

    .line 545
    .local v14, "trackPlayerEmsgHandler":Lcom/google/android/exoplayer2/source/dash/PlayerEmsgHandler$PlayerTrackEmsgHandler;
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->chunkSourceFactory:Lcom/google/android/exoplayer2/source/dash/DashChunkSource$Factory;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->manifestLoaderErrorThrower:Lcom/google/android/exoplayer2/upstream/LoaderErrorThrower;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->manifest:Lcom/google/android/exoplayer2/source/dash/manifest/DashManifest;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->periodIndex:I

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;->adaptationSetIndices:[I

    move-object/from16 v0, p1

    iget v9, v0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;->trackType:I

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->elapsedRealtimeOffset:J

    move-object/from16 v8, p2

    .line 546
    invoke-interface/range {v3 .. v14}, Lcom/google/android/exoplayer2/source/dash/DashChunkSource$Factory;->createDashChunkSource(Lcom/google/android/exoplayer2/upstream/LoaderErrorThrower;Lcom/google/android/exoplayer2/source/dash/manifest/DashManifest;I[ILcom/google/android/exoplayer2/trackselection/TrackSelection;IJZZLcom/google/android/exoplayer2/source/dash/PlayerEmsgHandler$PlayerTrackEmsgHandler;)Lcom/google/android/exoplayer2/source/dash/DashChunkSource;

    move-result-object v19

    .line 557
    .local v19, "chunkSource":Lcom/google/android/exoplayer2/source/dash/DashChunkSource;
    new-instance v15, Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;->trackType:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->allocator:Lcom/google/android/exoplayer2/upstream/Allocator;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->minLoadableRetryCount:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->eventDispatcher:Lcom/google/android/exoplayer2/source/MediaSourceEventListener$EventDispatcher;

    move-object/from16 v25, v0

    move-object/from16 v20, p0

    move-wide/from16 v22, p3

    invoke-direct/range {v15 .. v25}, Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;-><init>(I[I[Lcom/google/android/exoplayer2/Format;Lcom/google/android/exoplayer2/source/chunk/ChunkSource;Lcom/google/android/exoplayer2/source/SequenceableLoader$Callback;Lcom/google/android/exoplayer2/upstream/Allocator;JILcom/google/android/exoplayer2/source/MediaSourceEventListener$EventDispatcher;)V

    .line 568
    .local v15, "stream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;"
    monitor-enter p0

    .line 570
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->trackEmsgHandlerBySampleStream:Ljava/util/IdentityHashMap;

    invoke-virtual {v3, v15, v14}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 571
    monitor-exit p0

    .line 572
    return-object v15

    .line 524
    .end local v12    # "enableEventMessageTrack":Z
    .end local v13    # "enableCea608Track":Z
    .end local v14    # "trackPlayerEmsgHandler":Lcom/google/android/exoplayer2/source/dash/PlayerEmsgHandler$PlayerTrackEmsgHandler;
    .end local v15    # "stream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;"
    .end local v19    # "chunkSource":Lcom/google/android/exoplayer2/source/dash/DashChunkSource;
    :cond_3
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 531
    .restart local v12    # "enableEventMessageTrack":Z
    :cond_4
    const/4 v13, 0x0

    goto/16 :goto_1

    .line 543
    .restart local v13    # "enableCea608Track":Z
    :cond_5
    const/4 v14, 0x0

    goto :goto_2

    .line 571
    .restart local v14    # "trackPlayerEmsgHandler":Lcom/google/android/exoplayer2/source/dash/PlayerEmsgHandler$PlayerTrackEmsgHandler;
    .restart local v15    # "stream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;"
    .restart local v19    # "chunkSource":Lcom/google/android/exoplayer2/source/dash/DashChunkSource;
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method private static buildTrackGroups(Ljava/util/List;Ljava/util/List;)Landroid/util/Pair;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;",
            ">;)",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/exoplayer2/source/TrackGroupArray;",
            "[",
            "Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 368
    .local p0, "adaptationSets":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;>;"
    .local p1, "eventStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;>;"
    invoke-static {p0}, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->getGroupedAdaptationSetIndices(Ljava/util/List;)[[I

    move-result-object v1

    .line 370
    .local v1, "groupedAdaptationSetIndices":[[I
    array-length v2, v1

    .line 371
    .local v2, "primaryGroupCount":I
    new-array v3, v2, [Z

    .line 372
    .local v3, "primaryGroupHasEventMessageTrackFlags":[Z
    new-array v4, v2, [Z

    .line 373
    .local v4, "primaryGroupHasCea608TrackFlags":[Z
    invoke-static {v2, p0, v1, v3, v4}, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->identifyEmbeddedTracks(ILjava/util/List;[[I[Z[Z)I

    move-result v7

    .line 377
    .local v7, "totalEmbeddedTrackGroupCount":I
    add-int v0, v2, v7

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v10

    add-int v8, v0, v10

    .line 378
    .local v8, "totalGroupCount":I
    new-array v5, v8, [Lcom/google/android/exoplayer2/source/TrackGroup;

    .line 379
    .local v5, "trackGroups":[Lcom/google/android/exoplayer2/source/TrackGroup;
    new-array v6, v8, [Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;

    .local v6, "trackGroupInfos":[Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;
    move-object v0, p0

    .line 381
    invoke-static/range {v0 .. v6}, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->buildPrimaryAndEmbeddedTrackGroupInfos(Ljava/util/List;[[II[Z[Z[Lcom/google/android/exoplayer2/source/TrackGroup;[Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;)I

    move-result v9

    .line 385
    .local v9, "trackGroupCount":I
    invoke-static {p1, v5, v6, v9}, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->buildManifestEventTrackGroupInfos(Ljava/util/List;[Lcom/google/android/exoplayer2/source/TrackGroup;[Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;I)V

    .line 387
    new-instance v0, Lcom/google/android/exoplayer2/source/TrackGroupArray;

    invoke-direct {v0, v5}, Lcom/google/android/exoplayer2/source/TrackGroupArray;-><init>([Lcom/google/android/exoplayer2/source/TrackGroup;)V

    invoke-static {v0, v6}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method private static findAdaptationSetSwitchingProperty(Ljava/util/List;)Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;",
            ">;)",
            "Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;"
        }
    .end annotation

    .prologue
    .line 576
    .local p0, "descriptors":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 577
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;

    .line 578
    .local v0, "descriptor":Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;
    const-string v2, "urn:mpeg:dash:adaptation-set-switching:2016"

    iget-object v3, v0, Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;->schemeIdUri:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 582
    .end local v0    # "descriptor":Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;
    :goto_1
    return-object v0

    .line 576
    .restart local v0    # "descriptor":Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 582
    .end local v0    # "descriptor":Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static getGroupedAdaptationSetIndices(Ljava/util/List;)[[I
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;",
            ">;)[[I"
        }
    .end annotation

    .prologue
    .line 391
    .local p0, "adaptationSets":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    .line 392
    .local v0, "adaptationSetCount":I
    new-instance v10, Landroid/util/SparseIntArray;

    invoke-direct {v10, v0}, Landroid/util/SparseIntArray;-><init>(I)V

    .line 393
    .local v10, "idToIndexMap":Landroid/util/SparseIntArray;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-ge v9, v0, :cond_0

    .line 394
    invoke-interface {p0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;

    iget v12, v12, Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;->id:I

    invoke-virtual {v10, v12, v9}, Landroid/util/SparseIntArray;->put(II)V

    .line 393
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 397
    :cond_0
    new-array v8, v0, [[I

    .line 398
    .local v8, "groupedAdaptationSetIndices":[[I
    new-array v3, v0, [Z

    .line 400
    .local v3, "adaptationSetUsedFlags":[Z
    const/4 v6, 0x0

    .line 401
    .local v6, "groupCount":I
    const/4 v9, 0x0

    move v7, v6

    .end local v6    # "groupCount":I
    .local v7, "groupCount":I
    :goto_1
    if-ge v9, v0, :cond_4

    .line 402
    aget-boolean v12, v3, v9

    if-eqz v12, :cond_1

    move v6, v7

    .line 401
    .end local v7    # "groupCount":I
    .restart local v6    # "groupCount":I
    :goto_2
    add-int/lit8 v9, v9, 0x1

    move v7, v6

    .end local v6    # "groupCount":I
    .restart local v7    # "groupCount":I
    goto :goto_1

    .line 406
    :cond_1
    const/4 v12, 0x1

    aput-boolean v12, v3, v9

    .line 408
    invoke-interface {p0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;

    iget-object v12, v12, Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;->supplementalProperties:Ljava/util/List;

    .line 407
    invoke-static {v12}, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->findAdaptationSetSwitchingProperty(Ljava/util/List;)Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;

    move-result-object v2

    .line 409
    .local v2, "adaptationSetSwitchingProperty":Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;
    if-nez v2, :cond_2

    .line 410
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "groupCount":I
    .restart local v6    # "groupCount":I
    const/4 v12, 0x1

    new-array v12, v12, [I

    const/4 v13, 0x0

    aput v9, v12, v13

    aput-object v12, v8, v7

    goto :goto_2

    .line 412
    .end local v6    # "groupCount":I
    .restart local v7    # "groupCount":I
    :cond_2
    iget-object v12, v2, Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;->value:Ljava/lang/String;

    const-string v13, ","

    invoke-virtual {v12, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 413
    .local v4, "extraAdaptationSetIds":[Ljava/lang/String;
    array-length v12, v4

    add-int/lit8 v12, v12, 0x1

    new-array v1, v12, [I

    .line 414
    .local v1, "adaptationSetIndices":[I
    const/4 v12, 0x0

    aput v9, v1, v12

    .line 415
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_3
    array-length v12, v4

    if-ge v11, v12, :cond_3

    .line 416
    aget-object v12, v4, v11

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    invoke-virtual {v10, v12}, Landroid/util/SparseIntArray;->get(I)I

    move-result v5

    .line 417
    .local v5, "extraIndex":I
    const/4 v12, 0x1

    aput-boolean v12, v3, v5

    .line 418
    add-int/lit8 v12, v11, 0x1

    aput v5, v1, v12

    .line 415
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 420
    .end local v5    # "extraIndex":I
    :cond_3
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "groupCount":I
    .restart local v6    # "groupCount":I
    aput-object v1, v8, v7

    goto :goto_2

    .line 424
    .end local v1    # "adaptationSetIndices":[I
    .end local v2    # "adaptationSetSwitchingProperty":Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;
    .end local v4    # "extraAdaptationSetIds":[Ljava/lang/String;
    .end local v6    # "groupCount":I
    .end local v11    # "j":I
    .restart local v7    # "groupCount":I
    :cond_4
    if-ge v7, v0, :cond_5

    .line 425
    invoke-static {v8, v7}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v12

    check-cast v12, [[I

    .line 424
    :goto_4
    return-object v12

    :cond_5
    move-object v12, v8

    .line 425
    goto :goto_4
.end method

.method private static hasCea608Track(Ljava/util/List;[I)Z
    .locals 9
    .param p1, "adaptationSetIndices"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;",
            ">;[I)Z"
        }
    .end annotation

    .prologue
    .local p0, "adaptationSets":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;>;"
    const/4 v5, 0x0

    .line 601
    array-length v7, p1

    move v6, v5

    :goto_0
    if-ge v6, v7, :cond_2

    aget v2, p1, v6

    .line 602
    .local v2, "i":I
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;

    iget-object v1, v4, Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;->accessibilityDescriptors:Ljava/util/List;

    .line 603
    .local v1, "descriptors":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;>;"
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 604
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;

    .line 605
    .local v0, "descriptor":Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;
    const-string v4, "urn:scte:dash:cc:cea-608:2015"

    iget-object v8, v0, Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;->schemeIdUri:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 606
    const/4 v4, 0x1

    .line 610
    .end local v0    # "descriptor":Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;
    .end local v1    # "descriptors":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;>;"
    .end local v2    # "i":I
    .end local v3    # "j":I
    :goto_2
    return v4

    .line 603
    .restart local v0    # "descriptor":Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;
    .restart local v1    # "descriptors":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;>;"
    .restart local v2    # "i":I
    .restart local v3    # "j":I
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 601
    .end local v0    # "descriptor":Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;
    :cond_1
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_0

    .end local v1    # "descriptors":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/exoplayer2/source/dash/manifest/Descriptor;>;"
    .end local v2    # "i":I
    .end local v3    # "j":I
    :cond_2
    move v4, v5

    .line 610
    goto :goto_2
.end method

.method private static hasEventMessageTrack(Ljava/util/List;[I)Z
    .locals 8
    .param p1, "adaptationSetIndices"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;",
            ">;[I)Z"
        }
    .end annotation

    .prologue
    .local p0, "adaptationSets":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;>;"
    const/4 v5, 0x0

    .line 587
    array-length v7, p1

    move v6, v5

    :goto_0
    if-ge v6, v7, :cond_2

    aget v0, p1, v6

    .line 588
    .local v0, "i":I
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;

    iget-object v3, v4, Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;->representations:Ljava/util/List;

    .line 589
    .local v3, "representations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/exoplayer2/source/dash/manifest/Representation;>;"
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 590
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer2/source/dash/manifest/Representation;

    .line 591
    .local v2, "representation":Lcom/google/android/exoplayer2/source/dash/manifest/Representation;
    iget-object v4, v2, Lcom/google/android/exoplayer2/source/dash/manifest/Representation;->inbandEventStreams:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 592
    const/4 v4, 0x1

    .line 596
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v2    # "representation":Lcom/google/android/exoplayer2/source/dash/manifest/Representation;
    .end local v3    # "representations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/exoplayer2/source/dash/manifest/Representation;>;"
    :goto_2
    return v4

    .line 589
    .restart local v0    # "i":I
    .restart local v1    # "j":I
    .restart local v2    # "representation":Lcom/google/android/exoplayer2/source/dash/manifest/Representation;
    .restart local v3    # "representations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/exoplayer2/source/dash/manifest/Representation;>;"
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 587
    .end local v2    # "representation":Lcom/google/android/exoplayer2/source/dash/manifest/Representation;
    :cond_1
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_0

    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v3    # "representations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/exoplayer2/source/dash/manifest/Representation;>;"
    :cond_2
    move v4, v5

    .line 596
    goto :goto_2
.end method

.method private static identifyEmbeddedTracks(ILjava/util/List;[[I[Z[Z)I
    .locals 4
    .param p0, "primaryGroupCount"    # I
    .param p2, "groupedAdaptationSetIndices"    # [[I
    .param p3, "primaryGroupHasEventMessageTrackFlags"    # [Z
    .param p4, "primaryGroupHasCea608TrackFlags"    # [Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;",
            ">;[[I[Z[Z)I"
        }
    .end annotation

    .prologue
    .local p1, "adaptationSets":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;>;"
    const/4 v3, 0x1

    .line 445
    const/4 v1, 0x0

    .line 446
    .local v1, "numEmbeddedTrack":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p0, :cond_2

    .line 447
    aget-object v2, p2, v0

    invoke-static {p1, v2}, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->hasEventMessageTrack(Ljava/util/List;[I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 448
    aput-boolean v3, p3, v0

    .line 449
    add-int/lit8 v1, v1, 0x1

    .line 451
    :cond_0
    aget-object v2, p2, v0

    invoke-static {p1, v2}, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->hasCea608Track(Ljava/util/List;[I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 452
    aput-boolean v3, p4, v0

    .line 453
    add-int/lit8 v1, v1, 0x1

    .line 446
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 456
    :cond_2
    return v1
.end method

.method private static newSampleStreamArray(I)[Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;
    .locals 1
    .param p0, "length"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)[",
            "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream",
            "<",
            "Lcom/google/android/exoplayer2/source/dash/DashChunkSource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 615
    new-array v0, p0, [Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;

    return-object v0
.end method

.method private static releaseIfEmbeddedSampleStream(Lcom/google/android/exoplayer2/source/SampleStream;)V
    .locals 1
    .param p0, "sampleStream"    # Lcom/google/android/exoplayer2/source/SampleStream;

    .prologue
    .line 619
    instance-of v0, p0, Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream$EmbeddedSampleStream;

    if-eqz v0, :cond_0

    .line 620
    check-cast p0, Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream$EmbeddedSampleStream;

    .end local p0    # "sampleStream":Lcom/google/android/exoplayer2/source/SampleStream;
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream$EmbeddedSampleStream;->release()V

    .line 622
    :cond_0
    return-void
.end method

.method private selectEmbeddedSampleStreams([Lcom/google/android/exoplayer2/trackselection/TrackSelection;[Z[Lcom/google/android/exoplayer2/source/SampleStream;[ZJLandroid/util/SparseArray;)V
    .locals 11
    .param p1, "selections"    # [Lcom/google/android/exoplayer2/trackselection/TrackSelection;
    .param p2, "mayRetainStreamFlags"    # [Z
    .param p3, "streams"    # [Lcom/google/android/exoplayer2/source/SampleStream;
    .param p4, "streamResetFlags"    # [Z
    .param p5, "positionUs"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/android/exoplayer2/trackselection/TrackSelection;",
            "[Z[",
            "Lcom/google/android/exoplayer2/source/SampleStream;",
            "[ZJ",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream",
            "<",
            "Lcom/google/android/exoplayer2/source/dash/DashChunkSource;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 274
    .local p7, "primarySampleStreams":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v8, p1

    if-ge v2, v8, :cond_7

    .line 275
    aget-object v8, p3, v2

    instance-of v8, v8, Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream$EmbeddedSampleStream;

    if-nez v8, :cond_0

    aget-object v8, p3, v2

    instance-of v8, v8, Lcom/google/android/exoplayer2/source/EmptySampleStream;

    if-eqz v8, :cond_2

    :cond_0
    aget-object v8, p1, v2

    if-eqz v8, :cond_1

    aget-boolean v8, p2, v2

    if-nez v8, :cond_2

    .line 278
    :cond_1
    aget-object v8, p3, v2

    invoke-static {v8}, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->releaseIfEmbeddedSampleStream(Lcom/google/android/exoplayer2/source/SampleStream;)V

    .line 279
    const/4 v8, 0x0

    aput-object v8, p3, v2

    .line 283
    :cond_2
    aget-object v8, p1, v2

    if-eqz v8, :cond_3

    .line 284
    iget-object v8, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->trackGroups:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    aget-object v9, p1, v2

    invoke-interface {v9}, Lcom/google/android/exoplayer2/trackselection/TrackSelection;->getTrackGroup()Lcom/google/android/exoplayer2/source/TrackGroup;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/exoplayer2/source/TrackGroupArray;->indexOf(Lcom/google/android/exoplayer2/source/TrackGroup;)I

    move-result v6

    .line 285
    .local v6, "trackGroupIndex":I
    iget-object v8, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->trackGroupInfos:[Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;

    aget-object v7, v8, v6

    .line 286
    .local v7, "trackGroupInfo":Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;
    iget v8, v7, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;->trackGroupCategory:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_3

    .line 287
    iget v8, v7, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;->primaryTrackGroupIndex:I

    move-object/from16 v0, p7

    invoke-virtual {v0, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;

    .line 289
    .local v4, "primaryStream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<*>;"
    aget-object v5, p3, v2

    .line 290
    .local v5, "stream":Lcom/google/android/exoplayer2/source/SampleStream;
    if-nez v4, :cond_4

    instance-of v3, v5, Lcom/google/android/exoplayer2/source/EmptySampleStream;

    .line 293
    .local v3, "mayRetainStream":Z
    :goto_1
    if-nez v3, :cond_3

    .line 294
    invoke-static {v5}, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->releaseIfEmbeddedSampleStream(Lcom/google/android/exoplayer2/source/SampleStream;)V

    .line 295
    if-nez v4, :cond_6

    new-instance v8, Lcom/google/android/exoplayer2/source/EmptySampleStream;

    invoke-direct {v8}, Lcom/google/android/exoplayer2/source/EmptySampleStream;-><init>()V

    .line 296
    :goto_2
    aput-object v8, p3, v2

    .line 297
    const/4 v8, 0x1

    aput-boolean v8, p4, v2

    .line 274
    .end local v3    # "mayRetainStream":Z
    .end local v4    # "primaryStream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<*>;"
    .end local v5    # "stream":Lcom/google/android/exoplayer2/source/SampleStream;
    .end local v6    # "trackGroupIndex":I
    .end local v7    # "trackGroupInfo":Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 290
    .restart local v4    # "primaryStream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<*>;"
    .restart local v5    # "stream":Lcom/google/android/exoplayer2/source/SampleStream;
    .restart local v6    # "trackGroupIndex":I
    .restart local v7    # "trackGroupInfo":Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;
    :cond_4
    instance-of v8, v5, Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream$EmbeddedSampleStream;

    if-eqz v8, :cond_5

    move-object v8, v5

    check-cast v8, Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream$EmbeddedSampleStream;

    iget-object v8, v8, Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream$EmbeddedSampleStream;->parent:Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;

    if-ne v8, v4, :cond_5

    const/4 v3, 0x1

    goto :goto_1

    :cond_5
    const/4 v3, 0x0

    goto :goto_1

    .line 295
    .restart local v3    # "mayRetainStream":Z
    :cond_6
    iget v8, v7, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;->trackType:I

    .line 296
    move-wide/from16 v0, p5

    invoke-virtual {v4, v0, v1, v8}, Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;->selectEmbeddedTrack(JI)Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream$EmbeddedSampleStream;

    move-result-object v8

    goto :goto_2

    .line 302
    .end local v3    # "mayRetainStream":Z
    .end local v4    # "primaryStream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<*>;"
    .end local v5    # "stream":Lcom/google/android/exoplayer2/source/SampleStream;
    .end local v6    # "trackGroupIndex":I
    .end local v7    # "trackGroupInfo":Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;
    :cond_7
    return-void
.end method

.method private selectEventSampleStreams([Lcom/google/android/exoplayer2/trackselection/TrackSelection;[Z[Lcom/google/android/exoplayer2/source/SampleStream;[ZLjava/util/List;)V
    .locals 8
    .param p1, "selections"    # [Lcom/google/android/exoplayer2/trackselection/TrackSelection;
    .param p2, "mayRetainStreamFlags"    # [Z
    .param p3, "streams"    # [Lcom/google/android/exoplayer2/source/SampleStream;
    .param p4, "streamResetFlags"    # [Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/android/exoplayer2/trackselection/TrackSelection;",
            "[Z[",
            "Lcom/google/android/exoplayer2/source/SampleStream;",
            "[Z",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer2/source/dash/EventSampleStream;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 242
    .local p5, "eventSampleStreamsList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/exoplayer2/source/dash/EventSampleStream;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v6, p1

    if-ge v2, v6, :cond_4

    .line 243
    aget-object v6, p3, v2

    instance-of v6, v6, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;

    if-eqz v6, :cond_1

    .line 244
    aget-object v3, p3, v2

    check-cast v3, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;

    .line 245
    .local v3, "stream":Lcom/google/android/exoplayer2/source/dash/EventSampleStream;
    aget-object v6, p1, v2

    if-eqz v6, :cond_0

    aget-boolean v6, p2, v2

    if-nez v6, :cond_3

    .line 246
    :cond_0
    const/4 v6, 0x0

    aput-object v6, p3, v2

    .line 252
    .end local v3    # "stream":Lcom/google/android/exoplayer2/source/dash/EventSampleStream;
    :cond_1
    :goto_1
    aget-object v6, p3, v2

    if-nez v6, :cond_2

    aget-object v6, p1, v2

    if-eqz v6, :cond_2

    .line 253
    iget-object v6, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->trackGroups:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    aget-object v7, p1, v2

    invoke-interface {v7}, Lcom/google/android/exoplayer2/trackselection/TrackSelection;->getTrackGroup()Lcom/google/android/exoplayer2/source/TrackGroup;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/exoplayer2/source/TrackGroupArray;->indexOf(Lcom/google/android/exoplayer2/source/TrackGroup;)I

    move-result v4

    .line 254
    .local v4, "trackGroupIndex":I
    iget-object v6, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->trackGroupInfos:[Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;

    aget-object v5, v6, v4

    .line 255
    .local v5, "trackGroupInfo":Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;
    iget v6, v5, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;->trackGroupCategory:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_2

    .line 256
    iget-object v6, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->eventStreams:Ljava/util/List;

    iget v7, v5, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;->eventStreamGroupIndex:I

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;

    .line 257
    .local v0, "eventStream":Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;
    aget-object v6, p1, v2

    invoke-interface {v6}, Lcom/google/android/exoplayer2/trackselection/TrackSelection;->getTrackGroup()Lcom/google/android/exoplayer2/source/TrackGroup;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/google/android/exoplayer2/source/TrackGroup;->getFormat(I)Lcom/google/android/exoplayer2/Format;

    move-result-object v1

    .line 258
    .local v1, "format":Lcom/google/android/exoplayer2/Format;
    new-instance v3, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;

    iget-object v6, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->manifest:Lcom/google/android/exoplayer2/source/dash/manifest/DashManifest;

    iget-boolean v6, v6, Lcom/google/android/exoplayer2/source/dash/manifest/DashManifest;->dynamic:Z

    invoke-direct {v3, v0, v1, v6}, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;-><init>(Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;Lcom/google/android/exoplayer2/Format;Z)V

    .line 259
    .restart local v3    # "stream":Lcom/google/android/exoplayer2/source/dash/EventSampleStream;
    aput-object v3, p3, v2

    .line 260
    const/4 v6, 0x1

    aput-boolean v6, p4, v2

    .line 261
    invoke-interface {p5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 242
    .end local v0    # "eventStream":Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;
    .end local v1    # "format":Lcom/google/android/exoplayer2/Format;
    .end local v3    # "stream":Lcom/google/android/exoplayer2/source/dash/EventSampleStream;
    .end local v4    # "trackGroupIndex":I
    .end local v5    # "trackGroupInfo":Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 248
    .restart local v3    # "stream":Lcom/google/android/exoplayer2/source/dash/EventSampleStream;
    :cond_3
    invoke-interface {p5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 265
    .end local v3    # "stream":Lcom/google/android/exoplayer2/source/dash/EventSampleStream;
    :cond_4
    return-void
.end method

.method private selectPrimarySampleStreams([Lcom/google/android/exoplayer2/trackselection/TrackSelection;[Z[Lcom/google/android/exoplayer2/source/SampleStream;[ZJLandroid/util/SparseArray;)V
    .locals 7
    .param p1, "selections"    # [Lcom/google/android/exoplayer2/trackselection/TrackSelection;
    .param p2, "mayRetainStreamFlags"    # [Z
    .param p3, "streams"    # [Lcom/google/android/exoplayer2/source/SampleStream;
    .param p4, "streamResetFlags"    # [Z
    .param p5, "positionUs"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/android/exoplayer2/trackselection/TrackSelection;",
            "[Z[",
            "Lcom/google/android/exoplayer2/source/SampleStream;",
            "[ZJ",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream",
            "<",
            "Lcom/google/android/exoplayer2/source/dash/DashChunkSource;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 212
    .local p7, "primarySampleStreams":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, p1

    if-ge v0, v4, :cond_4

    .line 213
    aget-object v4, p3, v0

    instance-of v4, v4, Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;

    if-eqz v4, :cond_1

    .line 215
    aget-object v1, p3, v0

    check-cast v1, Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;

    .line 216
    .local v1, "stream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;"
    aget-object v4, p1, v0

    if-eqz v4, :cond_0

    aget-boolean v4, p2, v0

    if-nez v4, :cond_3

    .line 217
    :cond_0
    invoke-virtual {v1, p0}, Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;->release(Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream$ReleaseCallback;)V

    .line 218
    const/4 v4, 0x0

    aput-object v4, p3, v0

    .line 225
    .end local v1    # "stream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;"
    :cond_1
    :goto_1
    aget-object v4, p3, v0

    if-nez v4, :cond_2

    aget-object v4, p1, v0

    if-eqz v4, :cond_2

    .line 226
    iget-object v4, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->trackGroups:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    aget-object v5, p1, v0

    invoke-interface {v5}, Lcom/google/android/exoplayer2/trackselection/TrackSelection;->getTrackGroup()Lcom/google/android/exoplayer2/source/TrackGroup;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/exoplayer2/source/TrackGroupArray;->indexOf(Lcom/google/android/exoplayer2/source/TrackGroup;)I

    move-result v2

    .line 227
    .local v2, "trackGroupIndex":I
    iget-object v4, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->trackGroupInfos:[Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;

    aget-object v3, v4, v2

    .line 228
    .local v3, "trackGroupInfo":Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;
    iget v4, v3, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;->trackGroupCategory:I

    if-nez v4, :cond_2

    .line 229
    aget-object v4, p1, v0

    invoke-direct {p0, v3, v4, p5, p6}, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->buildSampleStream(Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;Lcom/google/android/exoplayer2/trackselection/TrackSelection;J)Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;

    move-result-object v1

    .line 231
    .restart local v1    # "stream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;"
    invoke-virtual {p7, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 232
    aput-object v1, p3, v0

    .line 233
    const/4 v4, 0x1

    aput-boolean v4, p4, v0

    .line 212
    .end local v1    # "stream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;"
    .end local v2    # "trackGroupIndex":I
    .end local v3    # "trackGroupInfo":Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod$TrackGroupInfo;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 220
    .restart local v1    # "stream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;"
    :cond_3
    iget-object v4, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->trackGroups:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    aget-object v5, p1, v0

    invoke-interface {v5}, Lcom/google/android/exoplayer2/trackselection/TrackSelection;->getTrackGroup()Lcom/google/android/exoplayer2/source/TrackGroup;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/exoplayer2/source/TrackGroupArray;->indexOf(Lcom/google/android/exoplayer2/source/TrackGroup;)I

    move-result v2

    .line 221
    .restart local v2    # "trackGroupIndex":I
    invoke-virtual {p7, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_1

    .line 237
    .end local v1    # "stream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;"
    .end local v2    # "trackGroupIndex":I
    :cond_4
    return-void
.end method


# virtual methods
.method public continueLoading(J)Z
    .locals 1
    .param p1, "positionUs"    # J

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->compositeSequenceableLoader:Lcom/google/android/exoplayer2/source/SequenceableLoader;

    invoke-interface {v0, p1, p2}, Lcom/google/android/exoplayer2/source/SequenceableLoader;->continueLoading(J)Z

    move-result v0

    return v0
.end method

.method public discardBuffer(JZ)V
    .locals 5
    .param p1, "positionUs"    # J
    .param p3, "toKeyframe"    # Z

    .prologue
    .line 306
    iget-object v2, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->sampleStreams:[Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 307
    .local v0, "sampleStream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;"
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;->discardBuffer(JZ)V

    .line 306
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 309
    .end local v0    # "sampleStream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;"
    :cond_0
    return-void
.end method

.method public getAdjustedSeekPositionUs(JLcom/google/android/exoplayer2/SeekParameters;)J
    .locals 7
    .param p1, "positionUs"    # J
    .param p3, "seekParameters"    # Lcom/google/android/exoplayer2/SeekParameters;

    .prologue
    .line 349
    iget-object v2, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->sampleStreams:[Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 350
    .local v0, "sampleStream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;"
    iget v4, v0, Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;->primaryTrackType:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 351
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;->getAdjustedSeekPositionUs(JLcom/google/android/exoplayer2/SeekParameters;)J

    move-result-wide p1

    .line 354
    .end local v0    # "sampleStream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;"
    .end local p1    # "positionUs":J
    :cond_0
    return-wide p1

    .line 349
    .restart local v0    # "sampleStream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;"
    .restart local p1    # "positionUs":J
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getBufferedPositionUs()J
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->compositeSequenceableLoader:Lcom/google/android/exoplayer2/source/SequenceableLoader;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/SequenceableLoader;->getBufferedPositionUs()J

    move-result-wide v0

    return-wide v0
.end method

.method public getNextLoadPositionUs()J
    .locals 2

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->compositeSequenceableLoader:Lcom/google/android/exoplayer2/source/SequenceableLoader;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/source/SequenceableLoader;->getNextLoadPositionUs()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTrackGroups()Lcom/google/android/exoplayer2/source/TrackGroupArray;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->trackGroups:Lcom/google/android/exoplayer2/source/TrackGroupArray;

    return-object v0
.end method

.method public maybeThrowPrepareError()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->manifestLoaderErrorThrower:Lcom/google/android/exoplayer2/upstream/LoaderErrorThrower;

    invoke-interface {v0}, Lcom/google/android/exoplayer2/upstream/LoaderErrorThrower;->maybeThrowError()V

    .line 174
    return-void
.end method

.method public bridge synthetic onContinueLoadingRequested(Lcom/google/android/exoplayer2/source/SequenceableLoader;)V
    .locals 0

    .prologue
    .line 56
    check-cast p1, Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;

    invoke-virtual {p0, p1}, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->onContinueLoadingRequested(Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;)V

    return-void
.end method

.method public onContinueLoadingRequested(Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream",
            "<",
            "Lcom/google/android/exoplayer2/source/dash/DashChunkSource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 361
    .local p1, "sampleStream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;"
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->callback:Lcom/google/android/exoplayer2/source/MediaPeriod$Callback;

    invoke-interface {v0, p0}, Lcom/google/android/exoplayer2/source/MediaPeriod$Callback;->onContinueLoadingRequested(Lcom/google/android/exoplayer2/source/SequenceableLoader;)V

    .line 362
    return-void
.end method

.method public declared-synchronized onSampleStreamReleased(Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream",
            "<",
            "Lcom/google/android/exoplayer2/source/dash/DashChunkSource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 157
    .local p1, "stream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;"
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->trackEmsgHandlerBySampleStream:Ljava/util/IdentityHashMap;

    invoke-virtual {v1, p1}, Ljava/util/IdentityHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/source/dash/PlayerEmsgHandler$PlayerTrackEmsgHandler;

    .line 158
    .local v0, "trackEmsgHandler":Lcom/google/android/exoplayer2/source/dash/PlayerEmsgHandler$PlayerTrackEmsgHandler;
    if-eqz v0, :cond_0

    .line 159
    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/dash/PlayerEmsgHandler$PlayerTrackEmsgHandler;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    :cond_0
    monitor-exit p0

    return-void

    .line 157
    .end local v0    # "trackEmsgHandler":Lcom/google/android/exoplayer2/source/dash/PlayerEmsgHandler$PlayerTrackEmsgHandler;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public prepare(Lcom/google/android/exoplayer2/source/MediaPeriod$Callback;J)V
    .locals 0
    .param p1, "callback"    # Lcom/google/android/exoplayer2/source/MediaPeriod$Callback;
    .param p2, "positionUs"    # J

    .prologue
    .line 167
    iput-object p1, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->callback:Lcom/google/android/exoplayer2/source/MediaPeriod$Callback;

    .line 168
    invoke-interface {p1, p0}, Lcom/google/android/exoplayer2/source/MediaPeriod$Callback;->onPrepared(Lcom/google/android/exoplayer2/source/MediaPeriod;)V

    .line 169
    return-void
.end method

.method public readDiscontinuity()J
    .locals 2

    .prologue
    .line 328
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    return-wide v0
.end method

.method public reevaluateBuffer(J)V
    .locals 1
    .param p1, "positionUs"    # J

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->compositeSequenceableLoader:Lcom/google/android/exoplayer2/source/SequenceableLoader;

    invoke-interface {v0, p1, p2}, Lcom/google/android/exoplayer2/source/SequenceableLoader;->reevaluateBuffer(J)V

    .line 314
    return-void
.end method

.method public release()V
    .locals 4

    .prologue
    .line 147
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->playerEmsgHandler:Lcom/google/android/exoplayer2/source/dash/PlayerEmsgHandler;

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/source/dash/PlayerEmsgHandler;->release()V

    .line 148
    iget-object v2, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->sampleStreams:[Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 149
    .local v0, "sampleStream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;"
    invoke-virtual {v0, p0}, Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;->release(Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream$ReleaseCallback;)V

    .line 148
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 151
    .end local v0    # "sampleStream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;"
    :cond_0
    return-void
.end method

.method public seekToUs(J)J
    .locals 7
    .param p1, "positionUs"    # J

    .prologue
    const/4 v2, 0x0

    .line 338
    iget-object v4, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->sampleStreams:[Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v1, v4, v3

    .line 339
    .local v1, "sampleStream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;"
    invoke-virtual {v1, p1, p2}, Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;->seekToUs(J)V

    .line 338
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 341
    .end local v1    # "sampleStream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;"
    :cond_0
    iget-object v3, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->eventSampleStreams:[Lcom/google/android/exoplayer2/source/dash/EventSampleStream;

    array-length v4, v3

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 342
    .local v0, "sampleStream":Lcom/google/android/exoplayer2/source/dash/EventSampleStream;
    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->seekToUs(J)V

    .line 341
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 344
    .end local v0    # "sampleStream":Lcom/google/android/exoplayer2/source/dash/EventSampleStream;
    :cond_1
    return-wide p1
.end method

.method public selectTracks([Lcom/google/android/exoplayer2/trackselection/TrackSelection;[Z[Lcom/google/android/exoplayer2/source/SampleStream;[ZJ)J
    .locals 11
    .param p1, "selections"    # [Lcom/google/android/exoplayer2/trackselection/TrackSelection;
    .param p2, "mayRetainStreamFlags"    # [Z
    .param p3, "streams"    # [Lcom/google/android/exoplayer2/source/SampleStream;
    .param p4, "streamResetFlags"    # [Z
    .param p5, "positionUs"    # J

    .prologue
    .line 184
    new-instance v8, Landroid/util/SparseArray;

    invoke-direct {v8}, Landroid/util/SparseArray;-><init>()V

    .line 185
    .local v8, "primarySampleStreams":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .local v9, "eventSampleStreamList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/exoplayer2/source/dash/EventSampleStream;>;"
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide/from16 v6, p5

    .line 187
    invoke-direct/range {v1 .. v8}, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->selectPrimarySampleStreams([Lcom/google/android/exoplayer2/trackselection/TrackSelection;[Z[Lcom/google/android/exoplayer2/source/SampleStream;[ZJLandroid/util/SparseArray;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, v9

    .line 189
    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->selectEventSampleStreams([Lcom/google/android/exoplayer2/trackselection/TrackSelection;[Z[Lcom/google/android/exoplayer2/source/SampleStream;[ZLjava/util/List;)V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide/from16 v6, p5

    .line 191
    invoke-direct/range {v1 .. v8}, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->selectEmbeddedSampleStreams([Lcom/google/android/exoplayer2/trackselection/TrackSelection;[Z[Lcom/google/android/exoplayer2/source/SampleStream;[ZJLandroid/util/SparseArray;)V

    .line 194
    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->newSampleStreamArray(I)[Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->sampleStreams:[Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;

    .line 195
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->sampleStreams:[Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;

    array-length v0, v0

    if-ge v10, v0, :cond_0

    .line 196
    iget-object v1, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->sampleStreams:[Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;

    invoke-virtual {v8, v10}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;

    aput-object v0, v1, v10

    .line 195
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 198
    :cond_0
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/exoplayer2/source/dash/EventSampleStream;

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->eventSampleStreams:[Lcom/google/android/exoplayer2/source/dash/EventSampleStream;

    .line 199
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->eventSampleStreams:[Lcom/google/android/exoplayer2/source/dash/EventSampleStream;

    invoke-interface {v9, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 200
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->compositeSequenceableLoaderFactory:Lcom/google/android/exoplayer2/source/CompositeSequenceableLoaderFactory;

    iget-object v1, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->sampleStreams:[Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;

    .line 201
    invoke-interface {v0, v1}, Lcom/google/android/exoplayer2/source/CompositeSequenceableLoaderFactory;->createCompositeSequenceableLoader([Lcom/google/android/exoplayer2/source/SequenceableLoader;)Lcom/google/android/exoplayer2/source/SequenceableLoader;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->compositeSequenceableLoader:Lcom/google/android/exoplayer2/source/SequenceableLoader;

    .line 202
    return-wide p5
.end method

.method public updateManifest(Lcom/google/android/exoplayer2/source/dash/manifest/DashManifest;I)V
    .locals 9
    .param p1, "manifest"    # Lcom/google/android/exoplayer2/source/dash/manifest/DashManifest;
    .param p2, "periodIndex"    # I

    .prologue
    const/4 v4, 0x0

    .line 126
    iput-object p1, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->manifest:Lcom/google/android/exoplayer2/source/dash/manifest/DashManifest;

    .line 127
    iput p2, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->periodIndex:I

    .line 128
    iget-object v3, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->playerEmsgHandler:Lcom/google/android/exoplayer2/source/dash/PlayerEmsgHandler;

    invoke-virtual {v3, p1}, Lcom/google/android/exoplayer2/source/dash/PlayerEmsgHandler;->updateManifest(Lcom/google/android/exoplayer2/source/dash/manifest/DashManifest;)V

    .line 129
    iget-object v3, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->sampleStreams:[Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;

    if-eqz v3, :cond_1

    .line 130
    iget-object v6, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->sampleStreams:[Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;

    array-length v7, v6

    move v5, v4

    :goto_0
    if-ge v5, v7, :cond_0

    aget-object v2, v6, v5

    .line 131
    .local v2, "sampleStream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;"
    invoke-virtual {v2}, Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;->getChunkSource()Lcom/google/android/exoplayer2/source/chunk/ChunkSource;

    move-result-object v3

    check-cast v3, Lcom/google/android/exoplayer2/source/dash/DashChunkSource;

    invoke-interface {v3, p1, p2}, Lcom/google/android/exoplayer2/source/dash/DashChunkSource;->updateManifest(Lcom/google/android/exoplayer2/source/dash/manifest/DashManifest;I)V

    .line 130
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0

    .line 133
    .end local v2    # "sampleStream":Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream;, "Lcom/google/android/exoplayer2/source/chunk/ChunkSampleStream<Lcom/google/android/exoplayer2/source/dash/DashChunkSource;>;"
    :cond_0
    iget-object v3, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->callback:Lcom/google/android/exoplayer2/source/MediaPeriod$Callback;

    invoke-interface {v3, p0}, Lcom/google/android/exoplayer2/source/MediaPeriod$Callback;->onContinueLoadingRequested(Lcom/google/android/exoplayer2/source/SequenceableLoader;)V

    .line 135
    :cond_1
    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer2/source/dash/manifest/DashManifest;->getPeriod(I)Lcom/google/android/exoplayer2/source/dash/manifest/Period;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/exoplayer2/source/dash/manifest/Period;->eventStreams:Ljava/util/List;

    iput-object v3, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->eventStreams:Ljava/util/List;

    .line 136
    iget-object v5, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->eventSampleStreams:[Lcom/google/android/exoplayer2/source/dash/EventSampleStream;

    array-length v6, v5

    move v3, v4

    :goto_1
    if-ge v3, v6, :cond_4

    aget-object v0, v5, v3

    .line 137
    .local v0, "eventSampleStream":Lcom/google/android/exoplayer2/source/dash/EventSampleStream;
    iget-object v4, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaPeriod;->eventStreams:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;

    .line 138
    .local v1, "eventStream":Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;->id()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->eventStreamId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 139
    iget-boolean v4, p1, Lcom/google/android/exoplayer2/source/dash/manifest/DashManifest;->dynamic:Z

    invoke-virtual {v0, v1, v4}, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->updateEventStream(Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;Z)V

    .line 136
    .end local v1    # "eventStream":Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 144
    .end local v0    # "eventSampleStream":Lcom/google/android/exoplayer2/source/dash/EventSampleStream;
    :cond_4
    return-void
.end method
