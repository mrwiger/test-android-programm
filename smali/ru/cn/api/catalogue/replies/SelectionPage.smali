.class public Lru/cn/api/catalogue/replies/SelectionPage;
.super Ljava/lang/Object;
.source "SelectionPage.java"


# instance fields
.field public items:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "items"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/catalogue/replies/CatalogueItem;",
            ">;"
        }
    .end annotation
.end field

.field public itemsSkipped:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "items_skipped"
    .end annotation
.end field

.field public totalCount:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "total_count"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
