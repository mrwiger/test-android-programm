.class public Lru/cn/notifications/mapper/NotificationMapper;
.super Ljava/lang/Object;
.source "NotificationMapper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getLong(Landroid/database/Cursor;Ljava/lang/String;)J
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    const-wide/16 v2, 0x0

    .line 74
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 75
    .local v0, "position":I
    const/4 v4, -0x1

    if-ne v0, v4, :cond_1

    .line 79
    :cond_0
    :goto_0
    return-wide v2

    .line 76
    :cond_1
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 77
    .local v1, "value":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 78
    const-string v4, "\\d*"

    invoke-virtual {v1, v4}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 79
    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto :goto_0
.end method

.method private getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 84
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 85
    .local v0, "position":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x0

    .line 86
    :goto_0
    return-object v1

    :cond_0
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public map(Landroid/database/Cursor;)Ljava/util/List;
    .locals 21
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lru/cn/notifications/entity/INotification;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 44
    .local v20, "notifications":Ljava/util/List;, "Ljava/util/List<Lru/cn/notifications/entity/INotification;>;"
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 45
    :goto_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_4

    .line 46
    sget-object v2, Lru/cn/notifications/NotificationContract;->messageKey:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lru/cn/notifications/mapper/NotificationMapper;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 47
    .local v3, "message":Ljava/lang/String;
    sget-object v2, Lru/cn/notifications/NotificationContract;->filmIdKey:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lru/cn/notifications/mapper/NotificationMapper;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v16

    .line 48
    .local v16, "filmId":J
    sget-object v2, Lru/cn/notifications/NotificationContract;->channelIdKey:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lru/cn/notifications/mapper/NotificationMapper;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v10

    .line 49
    .local v10, "channelId":J
    sget-object v2, Lru/cn/notifications/NotificationContract;->telecastIdKey:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lru/cn/notifications/mapper/NotificationMapper;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v12

    .line 50
    .local v12, "telecastId":J
    sget-object v2, Lru/cn/notifications/NotificationContract;->createTimeKey:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lru/cn/notifications/mapper/NotificationMapper;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v6

    .line 51
    .local v6, "createTime":J
    sget-object v2, Lru/cn/notifications/NotificationContract;->deepLinkSchemeKey:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lru/cn/notifications/mapper/NotificationMapper;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 52
    .local v4, "scheme":Ljava/lang/String;
    sget-object v2, Lru/cn/notifications/NotificationContract;->deepLinkHostKey:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lru/cn/notifications/mapper/NotificationMapper;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 54
    .local v5, "host":Ljava/lang/String;
    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    .line 57
    new-instance v2, Lru/cn/notifications/entity/INotification$LaunchAppSection;

    invoke-direct/range {v2 .. v7}, Lru/cn/notifications/entity/INotification$LaunchAppSection;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    .end local v16    # "filmId":J
    :goto_1
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 58
    .restart local v16    # "filmId":J
    :cond_0
    const-wide/16 v8, 0x0

    cmp-long v2, v12, v8

    if-eqz v2, :cond_1

    .line 59
    new-instance v8, Lru/cn/notifications/entity/INotification$NewTelecastMessage;

    move-object v9, v3

    move-wide v14, v6

    invoke-direct/range {v8 .. v15}, Lru/cn/notifications/entity/INotification$NewTelecastMessage;-><init>(Ljava/lang/String;JJJ)V

    move-object/from16 v0, v20

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 60
    :cond_1
    const-wide/16 v8, 0x0

    cmp-long v2, v10, v8

    if-eqz v2, :cond_2

    .line 61
    new-instance v14, Lru/cn/notifications/entity/INotification$NewChannelMessage;

    move-object v15, v3

    move-wide/from16 v16, v10

    move-wide/from16 v18, v6

    invoke-direct/range {v14 .. v19}, Lru/cn/notifications/entity/INotification$NewChannelMessage;-><init>(Ljava/lang/String;JJ)V

    .end local v16    # "filmId":J
    move-object/from16 v0, v20

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 62
    .restart local v16    # "filmId":J
    :cond_2
    const-wide/16 v8, 0x0

    cmp-long v2, v16, v8

    if-eqz v2, :cond_3

    .line 63
    new-instance v14, Lru/cn/notifications/entity/INotification$NewFilmMessage;

    move-object v15, v3

    move-wide/from16 v18, v6

    invoke-direct/range {v14 .. v19}, Lru/cn/notifications/entity/INotification$NewFilmMessage;-><init>(Ljava/lang/String;JJ)V

    move-object/from16 v0, v20

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 65
    :cond_3
    new-instance v2, Lru/cn/notifications/entity/INotification$Message;

    invoke-direct {v2, v3, v6, v7}, Lru/cn/notifications/entity/INotification$Message;-><init>(Ljava/lang/String;J)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 69
    .end local v3    # "message":Ljava/lang/String;
    .end local v4    # "scheme":Ljava/lang/String;
    .end local v5    # "host":Ljava/lang/String;
    .end local v6    # "createTime":J
    .end local v10    # "channelId":J
    .end local v12    # "telecastId":J
    .end local v16    # "filmId":J
    :cond_4
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    .line 70
    return-object v20
.end method
