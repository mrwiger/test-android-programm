.class Lru/cn/player/timeshift/TimeshiftTask;
.super Landroid/os/AsyncTask;
.source "TimeshiftTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/player/timeshift/TimeshiftTask$OnTaskCompletedListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field protected final callback:Lru/cn/player/timeshift/TimeshiftTask$OnTaskCompletedListener;

.field protected final mLocation:Lru/cn/api/iptv/replies/MediaLocation;

.field protected offset:I

.field protected startOver:Z


# direct methods
.method constructor <init>(Lru/cn/api/iptv/replies/MediaLocation;Lru/cn/player/timeshift/TimeshiftTask$OnTaskCompletedListener;)V
    .locals 1
    .param p1, "location"    # Lru/cn/api/iptv/replies/MediaLocation;
    .param p2, "callback"    # Lru/cn/player/timeshift/TimeshiftTask$OnTaskCompletedListener;

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 24
    iput-boolean v0, p0, Lru/cn/player/timeshift/TimeshiftTask;->startOver:Z

    .line 25
    iput v0, p0, Lru/cn/player/timeshift/TimeshiftTask;->offset:I

    .line 28
    iput-object p2, p0, Lru/cn/player/timeshift/TimeshiftTask;->callback:Lru/cn/player/timeshift/TimeshiftTask$OnTaskCompletedListener;

    .line 29
    iput-object p1, p0, Lru/cn/player/timeshift/TimeshiftTask;->mLocation:Lru/cn/api/iptv/replies/MediaLocation;

    .line 30
    return-void
.end method

.method private static getHeaderValue(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 6
    .param p0, "param"    # Ljava/lang/String;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "escaped"    # Z

    .prologue
    const/4 v5, 0x2

    .line 122
    const-string v2, "(.*)"

    .line 123
    .local v2, "valueTemplate":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 124
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 127
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 128
    .local v1, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 129
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v3

    if-eq v3, v5, :cond_2

    .line 130
    :cond_1
    const/4 v3, 0x0

    .line 133
    :goto_0
    return-object v3

    :cond_2
    invoke-virtual {v0, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private resolveBaseUri()Z
    .locals 13

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 89
    iget-object v9, p0, Lru/cn/player/timeshift/TimeshiftTask;->mLocation:Lru/cn/api/iptv/replies/MediaLocation;

    iget-object v9, v9, Lru/cn/api/iptv/replies/MediaLocation;->timeshiftBaseUri:Ljava/lang/String;

    if-eqz v9, :cond_1

    .line 118
    :cond_0
    :goto_0
    return v7

    .line 92
    :cond_1
    new-instance v1, Lru/cn/utils/http/HttpClient;

    invoke-direct {v1}, Lru/cn/utils/http/HttpClient;-><init>()V

    .line 94
    .local v1, "client":Lru/cn/utils/http/HttpClient;
    :try_start_0
    iget-object v9, p0, Lru/cn/player/timeshift/TimeshiftTask;->mLocation:Lru/cn/api/iptv/replies/MediaLocation;

    iget-object v9, v9, Lru/cn/api/iptv/replies/MediaLocation;->location:Ljava/lang/String;

    sget-object v10, Lru/cn/utils/http/HttpClient$Method;->HEAD:Lru/cn/utils/http/HttpClient$Method;

    const/4 v11, 0x0

    invoke-virtual {v1, v9, v10, v11}, Lru/cn/utils/http/HttpClient;->sendRequest(Ljava/lang/String;Lru/cn/utils/http/HttpClient$Method;Ljava/lang/String;)V

    .line 96
    invoke-virtual {v1}, Lru/cn/utils/http/HttpClient;->getStatusCode()I

    move-result v6

    .line 97
    .local v6, "statusCode":I
    const/16 v9, 0xc8

    if-eq v6, v9, :cond_2

    .line 98
    sget-object v7, Lru/cn/domain/statistics/inetra/ErrorCode;->UNKNOWN_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v9, "TimeshiftError"

    const/4 v10, 0x1

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "code="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v7, v9, v10, v11}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    move v7, v8

    .line 100
    goto :goto_0

    .line 103
    :cond_2
    const-string v9, "Inetra-Timeshift"

    invoke-virtual {v1, v9}, Lru/cn/utils/http/HttpClient;->getHeader(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 104
    .local v3, "header":Ljava/lang/String;
    if-eqz v3, :cond_4

    .line 105
    const-string v9, ";"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 106
    .local v4, "m":[Ljava/lang/String;
    array-length v10, v4

    move v9, v8

    :goto_1
    if-ge v9, v10, :cond_4

    aget-object v5, v4, v9

    .line 107
    .local v5, "param":Ljava/lang/String;
    const-string v11, "base-uri"

    const/4 v12, 0x1

    invoke-static {v5, v11, v12}, Lru/cn/player/timeshift/TimeshiftTask;->getHeaderValue(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 108
    .local v0, "baseUri":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 109
    iget-object v11, p0, Lru/cn/player/timeshift/TimeshiftTask;->mLocation:Lru/cn/api/iptv/replies/MediaLocation;

    iput-object v0, v11, Lru/cn/api/iptv/replies/MediaLocation;->timeshiftBaseUri:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 113
    .end local v0    # "baseUri":Ljava/lang/String;
    .end local v3    # "header":Ljava/lang/String;
    .end local v4    # "m":[Ljava/lang/String;
    .end local v5    # "param":Ljava/lang/String;
    .end local v6    # "statusCode":I
    :catch_0
    move-exception v2

    .line 114
    .local v2, "e":Ljava/lang/Exception;
    invoke-static {v2}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    move v7, v8

    .line 115
    goto :goto_0

    .line 118
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v3    # "header":Ljava/lang/String;
    .restart local v6    # "statusCode":I
    :cond_4
    iget-object v9, p0, Lru/cn/player/timeshift/TimeshiftTask;->mLocation:Lru/cn/api/iptv/replies/MediaLocation;

    iget-object v9, v9, Lru/cn/api/iptv/replies/MediaLocation;->timeshiftBaseUri:Ljava/lang/String;

    if-nez v9, :cond_0

    move v7, v8

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 8
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v4, 0x0

    .line 48
    invoke-direct {p0}, Lru/cn/player/timeshift/TimeshiftTask;->resolveBaseUri()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 50
    iget-object v5, p0, Lru/cn/player/timeshift/TimeshiftTask;->mLocation:Lru/cn/api/iptv/replies/MediaLocation;

    iget-object v5, v5, Lru/cn/api/iptv/replies/MediaLocation;->timeshiftBaseUri:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    .line 51
    .local v3, "uriBuilder":Landroid/net/Uri$Builder;
    iget-boolean v5, p0, Lru/cn/player/timeshift/TimeshiftTask;->startOver:Z

    if-eqz v5, :cond_2

    .line 52
    const-string v5, "start_over"

    const-string v6, "true"

    invoke-virtual {v3, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 57
    :cond_0
    :goto_0
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 59
    .local v2, "uri":Landroid/net/Uri;
    new-instance v0, Lru/cn/utils/http/HttpClient;

    invoke-direct {v0}, Lru/cn/utils/http/HttpClient;-><init>()V

    .line 61
    .local v0, "client":Lru/cn/utils/http/HttpClient;
    :try_start_0
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lru/cn/utils/http/HttpClient$Method;->HEAD:Lru/cn/utils/http/HttpClient$Method;

    const/4 v7, 0x0

    invoke-virtual {v0, v5, v6, v7}, Lru/cn/utils/http/HttpClient;->sendRequest(Ljava/lang/String;Lru/cn/utils/http/HttpClient$Method;Ljava/lang/String;)V

    .line 62
    const-string v5, "Inetra-Timeshift-Offset"

    invoke-virtual {v0, v5}, Lru/cn/utils/http/HttpClient;->getHeader(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 64
    .local v1, "offset":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_1

    .line 65
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 71
    .end local v0    # "client":Lru/cn/utils/http/HttpClient;
    .end local v1    # "offset":Ljava/lang/String;
    .end local v2    # "uri":Landroid/net/Uri;
    .end local v3    # "uriBuilder":Landroid/net/Uri$Builder;
    :cond_1
    :goto_1
    return-object v4

    .line 53
    .restart local v3    # "uriBuilder":Landroid/net/Uri$Builder;
    :cond_2
    iget v5, p0, Lru/cn/player/timeshift/TimeshiftTask;->offset:I

    if-lez v5, :cond_0

    .line 54
    const-string v5, "offset"

    iget v6, p0, Lru/cn/player/timeshift/TimeshiftTask;->offset:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .line 67
    .restart local v0    # "client":Lru/cn/utils/http/HttpClient;
    .restart local v2    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v5

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lru/cn/player/timeshift/TimeshiftTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method final isStartOver()Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lru/cn/player/timeshift/TimeshiftTask;->startOver:Z

    return v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 4
    .param p1, "offset"    # Ljava/lang/Integer;

    .prologue
    .line 76
    const/4 v0, 0x0

    .line 77
    .local v0, "playbackUrl":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 78
    iget-object v1, p0, Lru/cn/player/timeshift/TimeshiftTask;->mLocation:Lru/cn/api/iptv/replies/MediaLocation;

    iget-object v1, v1, Lru/cn/api/iptv/replies/MediaLocation;->timeshiftBaseUri:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 79
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "offset"

    .line 80
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 81
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 82
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 85
    :cond_0
    iget-object v1, p0, Lru/cn/player/timeshift/TimeshiftTask;->callback:Lru/cn/player/timeshift/TimeshiftTask$OnTaskCompletedListener;

    invoke-interface {v1, p0, v0, p1}, Lru/cn/player/timeshift/TimeshiftTask$OnTaskCompletedListener;->onCompleted(Lru/cn/player/timeshift/TimeshiftTask;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 86
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 15
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lru/cn/player/timeshift/TimeshiftTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method final seekTo(I)V
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 33
    iput p1, p0, Lru/cn/player/timeshift/TimeshiftTask;->offset:I

    .line 34
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p0, v0}, Lru/cn/player/timeshift/TimeshiftTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 35
    return-void
.end method

.method final startOver()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/player/timeshift/TimeshiftTask;->startOver:Z

    .line 39
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p0, v0}, Lru/cn/player/timeshift/TimeshiftTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 40
    return-void
.end method
