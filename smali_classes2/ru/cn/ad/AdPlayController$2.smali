.class Lru/cn/ad/AdPlayController$2;
.super Ljava/lang/Object;
.source "AdPlayController.java"

# interfaces
.implements Lru/cn/ad/PreRollBanner$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/ad/AdPlayController;->startPreRoll()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/ad/AdPlayController;

.field final synthetic val$placeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lru/cn/ad/AdPlayController;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/ad/AdPlayController;

    .prologue
    .line 165
    iput-object p1, p0, Lru/cn/ad/AdPlayController$2;->this$0:Lru/cn/ad/AdPlayController;

    iput-object p2, p0, Lru/cn/ad/AdPlayController$2;->val$placeId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public adCompleted()V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lru/cn/ad/AdPlayController$2;->this$0:Lru/cn/ad/AdPlayController;

    invoke-static {v0}, Lru/cn/ad/AdPlayController;->access$200(Lru/cn/ad/AdPlayController;)Landroid/view/ViewGroup;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 185
    iget-object v0, p0, Lru/cn/ad/AdPlayController$2;->this$0:Lru/cn/ad/AdPlayController;

    invoke-static {v0}, Lru/cn/ad/AdPlayController;->access$000(Lru/cn/ad/AdPlayController;)Lru/cn/ad/AdPlayController$Listener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lru/cn/ad/AdPlayController$Listener;->onAdBreakEnded(Z)V

    .line 188
    iget-object v0, p0, Lru/cn/ad/AdPlayController$2;->val$placeId:Ljava/lang/String;

    invoke-static {v0}, Lru/cn/ad/AdsManager;->consumeOpportunity(Ljava/lang/String;)V

    .line 189
    return-void
.end method

.method public adLoadingFinished(Z)V
    .locals 2
    .param p1, "hasAds"    # Z

    .prologue
    .line 168
    if-eqz p1, :cond_0

    .line 169
    iget-object v0, p0, Lru/cn/ad/AdPlayController$2;->this$0:Lru/cn/ad/AdPlayController;

    invoke-static {v0}, Lru/cn/ad/AdPlayController;->access$000(Lru/cn/ad/AdPlayController;)Lru/cn/ad/AdPlayController$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdPlayController$Listener;->onAdBreak()V

    .line 170
    iget-object v0, p0, Lru/cn/ad/AdPlayController$2;->this$0:Lru/cn/ad/AdPlayController;

    invoke-static {v0}, Lru/cn/ad/AdPlayController;->access$100(Lru/cn/ad/AdPlayController;)V

    .line 174
    :goto_0
    return-void

    .line 172
    :cond_0
    iget-object v0, p0, Lru/cn/ad/AdPlayController$2;->this$0:Lru/cn/ad/AdPlayController;

    invoke-static {v0}, Lru/cn/ad/AdPlayController;->access$000(Lru/cn/ad/AdPlayController;)Lru/cn/ad/AdPlayController$Listener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lru/cn/ad/AdPlayController$Listener;->onAdBreakEnded(Z)V

    goto :goto_0
.end method

.method public adStarted()V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lru/cn/ad/AdPlayController$2;->this$0:Lru/cn/ad/AdPlayController;

    invoke-static {v0}, Lru/cn/ad/AdPlayController;->access$200(Lru/cn/ad/AdPlayController;)Landroid/view/ViewGroup;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 179
    iget-object v0, p0, Lru/cn/ad/AdPlayController$2;->this$0:Lru/cn/ad/AdPlayController;

    invoke-static {v0}, Lru/cn/ad/AdPlayController;->access$000(Lru/cn/ad/AdPlayController;)Lru/cn/ad/AdPlayController$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdPlayController$Listener;->onAdStart()V

    .line 180
    return-void
.end method
