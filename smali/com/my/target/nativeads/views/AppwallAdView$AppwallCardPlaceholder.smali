.class public Lcom/my/target/nativeads/views/AppwallAdView$AppwallCardPlaceholder;
.super Landroid/widget/FrameLayout;
.source "AppwallAdView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/nativeads/views/AppwallAdView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AppwallCardPlaceholder"
.end annotation


# instance fields
.field private final rootLayout:Landroid/widget/LinearLayout;

.field private final view:Lcom/my/target/nativeads/views/AppwallAdTeaserView;


# direct methods
.method public constructor <init>(Lcom/my/target/nativeads/views/AppwallAdTeaserView;Landroid/content/Context;)V
    .locals 13
    .param p1, "view"    # Lcom/my/target/nativeads/views/AppwallAdTeaserView;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const v12, -0x333334

    const/4 v11, -0x2

    const/4 v10, 0x1

    const/4 v9, 0x2

    const/4 v8, 0x0

    .line 199
    invoke-direct {p0, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 200
    invoke-static {p2}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    .line 201
    iput-object p1, p0, Lcom/my/target/nativeads/views/AppwallAdView$AppwallCardPlaceholder;->view:Lcom/my/target/nativeads/views/AppwallAdTeaserView;

    .line 203
    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/my/target/cm;->n(I)I

    move-result v1

    .line 204
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/my/target/cm;->n(I)I

    move-result v2

    .line 205
    invoke-virtual {v0, v9}, Lcom/my/target/cm;->n(I)I

    move-result v0

    .line 207
    new-instance v3, Landroid/widget/LinearLayout;

    invoke-direct {v3, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/my/target/nativeads/views/AppwallAdView$AppwallCardPlaceholder;->rootLayout:Landroid/widget/LinearLayout;

    .line 208
    iget-object v3, p0, Lcom/my/target/nativeads/views/AppwallAdView$AppwallCardPlaceholder;->rootLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v10}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 209
    iget-object v3, p0, Lcom/my/target/nativeads/views/AppwallAdView$AppwallCardPlaceholder;->rootLayout:Landroid/widget/LinearLayout;

    const v4, -0x111112

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 211
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    invoke-direct {v3, v4, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 212
    invoke-virtual {v3, v1, v2, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 213
    invoke-virtual {p1, v3}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 214
    iget-object v4, p0, Lcom/my/target/nativeads/views/AppwallAdView$AppwallCardPlaceholder;->rootLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 216
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x15

    if-lt v4, v5, :cond_0

    .line 218
    int-to-float v0, v0

    invoke-virtual {p1, v0}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->setElevation(F)V

    .line 220
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v2, v9, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    .line 222
    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    sget-object v2, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v3, v9, [I

    fill-array-data v3, :array_1

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    .line 226
    new-instance v2, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 227
    new-array v3, v10, [I

    const v4, 0x10100a7

    aput v4, v3, v8

    invoke-virtual {v2, v3, v1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 228
    sget-object v1, Landroid/util/StateSet;->WILD_CARD:[I

    invoke-virtual {v2, v1, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 229
    invoke-virtual {p1, v2}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 261
    :goto_0
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdView$AppwallCardPlaceholder;->rootLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0, v11, v11}, Lcom/my/target/nativeads/views/AppwallAdView$AppwallCardPlaceholder;->addView(Landroid/view/View;II)V

    .line 262
    return-void

    .line 233
    :cond_0
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 234
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    invoke-direct {v4, v5, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 236
    new-array v5, v9, [I

    fill-array-data v5, :array_2

    .line 237
    new-instance v6, Landroid/graphics/drawable/GradientDrawable;

    sget-object v7, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    invoke-direct {v6, v7, v5}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    .line 238
    invoke-virtual {v0, v6}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 239
    invoke-virtual {v0, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 240
    iget-object v4, p0, Lcom/my/target/nativeads/views/AppwallAdView$AppwallCardPlaceholder;->rootLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 242
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v4, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v5, v9, [I

    fill-array-data v5, :array_3

    invoke-direct {v0, v4, v5}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    .line 244
    invoke-virtual {v0, v10, v12}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 246
    new-instance v4, Landroid/graphics/drawable/GradientDrawable;

    sget-object v5, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v6, v9, [I

    fill-array-data v6, :array_4

    invoke-direct {v4, v5, v6}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    .line 249
    invoke-virtual {v4, v10, v12}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 251
    new-instance v5, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v5}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 252
    new-array v6, v10, [I

    const v7, 0x10100a7

    aput v7, v6, v8

    invoke-virtual {v5, v6, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 253
    sget-object v4, Landroid/util/StateSet;->WILD_CARD:[I

    invoke-virtual {v5, v4, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 255
    invoke-virtual {p1, v5}, Lcom/my/target/nativeads/views/AppwallAdTeaserView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 257
    invoke-virtual {v3, v8, v2, v8, v8}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 258
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdView$AppwallCardPlaceholder;->rootLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v8, v1, v8}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    goto :goto_0

    .line 220
    nop

    :array_0
    .array-data 4
        -0x1
        -0x1
    .end array-data

    .line 222
    :array_1
    .array-data 4
        -0x111112
        -0x111112
    .end array-data

    .line 236
    :array_2
    .array-data 4
        -0x333334
        -0x111112
    .end array-data

    .line 242
    :array_3
    .array-data 4
        -0x1
        -0x1
    .end array-data

    .line 246
    :array_4
    .array-data 4
        -0x111112
        -0x111112
    .end array-data
.end method


# virtual methods
.method public getView()Lcom/my/target/nativeads/views/AppwallAdTeaserView;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdView$AppwallCardPlaceholder;->view:Lcom/my/target/nativeads/views/AppwallAdTeaserView;

    return-object v0
.end method
