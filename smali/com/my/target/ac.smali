.class public Lcom/my/target/ac;
.super Ljava/lang/Object;
.source "MraidOrientation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/ac$a;
    }
.end annotation


# static fields
.field public static final bE:Ljava/lang/String; = "none"

.field public static final bF:Ljava/lang/String; = "portrait"

.field public static final bG:Ljava/lang/String; = "landscape"


# instance fields
.field private final bH:I

.field private final bI:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lcom/my/target/ac;->bH:I

    .line 49
    const-string v0, "none"

    iput-object v0, p0, Lcom/my/target/ac;->bI:Ljava/lang/String;

    .line 50
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/my/target/ac;->bI:Ljava/lang/String;

    .line 55
    iput p2, p0, Lcom/my/target/ac;->bH:I

    .line 56
    return-void
.end method

.method public static l(Ljava/lang/String;)Lcom/my/target/ac;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 20
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    move v3, v0

    :goto_0
    packed-switch v3, :pswitch_data_0

    .line 33
    const/4 v0, 0x0

    .line 35
    :goto_1
    return-object v0

    .line 20
    :sswitch_0
    const-string v3, "none"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v2

    goto :goto_0

    :sswitch_1
    const-string v3, "portrait"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v1

    goto :goto_0

    :sswitch_2
    const-string v3, "landscape"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x2

    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 35
    :goto_2
    :pswitch_1
    new-instance v1, Lcom/my/target/ac;

    invoke-direct {v1, p0, v0}, Lcom/my/target/ac;-><init>(Ljava/lang/String;I)V

    move-object v0, v1

    goto :goto_1

    :pswitch_2
    move v0, v2

    .line 31
    goto :goto_2

    .line 20
    :sswitch_data_0
    .sparse-switch
        0x33af38 -> :sswitch_0
        0x2b77bb9b -> :sswitch_1
        0x5545f2bb -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static s()Lcom/my/target/ac;
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/my/target/ac;

    invoke-direct {v0}, Lcom/my/target/ac;-><init>()V

    return-object v0
.end method


# virtual methods
.method public t()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/my/target/ac;->bH:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/my/target/ac;->bI:Ljava/lang/String;

    return-object v0
.end method
