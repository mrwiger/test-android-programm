.class public final Lcom/yandex/metrica/impl/bp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/yandex/metrica/impl/k$a;


# static fields
.field private static a:Lcom/yandex/metrica/impl/bp;

.field private static b:Lcom/yandex/metrica/impl/o;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/yandex/metrica/impl/ay;

.field private e:Lcom/yandex/metrica/impl/z;

.field private f:Lcom/yandex/metrica/impl/ah;

.field private final g:Ljava/util/concurrent/ExecutorService;

.field private final h:Lcom/yandex/metrica/impl/ob/fy;

.field private final i:Lcom/yandex/metrica/impl/as;

.field private j:Lcom/yandex/metrica/impl/h;

.field private k:Lcom/yandex/metrica/impl/j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lcom/yandex/metrica/impl/o;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/o;-><init>()V

    sput-object v0, Lcom/yandex/metrica/impl/bp;->b:Lcom/yandex/metrica/impl/o;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lcom/yandex/metrica/impl/utils/j;

    const-string v1, "YMM-RH"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/utils/j;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/bp;->g:Ljava/util/concurrent/ExecutorService;

    .line 1088
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Initializing of Metrica"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1089
    const-string v1, ", Release type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1090
    const-string v1, ", Version 2.80"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1091
    const-string v1, ", API Level 64"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1092
    const-string v1, ", Dated 27.12.2017."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1093
    invoke-static {}, Lcom/yandex/metrica/impl/utils/l;->f()Lcom/yandex/metrica/impl/utils/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/utils/l;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/yandex/metrica/impl/utils/l;->a(Landroid/content/Context;)V

    .line 67
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/bp;->c:Landroid/content/Context;

    .line 2061
    sget-object v0, Lcom/yandex/metrica/impl/GoogleAdvertisingIdGetter$c;->a:Lcom/yandex/metrica/impl/GoogleAdvertisingIdGetter;

    .line 69
    iget-object v1, p0, Lcom/yandex/metrica/impl/bp;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/GoogleAdvertisingIdGetter;->a(Landroid/content/Context;)V

    .line 71
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 73
    new-instance v1, Lcom/yandex/metrica/impl/az;

    iget-object v2, p0, Lcom/yandex/metrica/impl/bp;->g:Ljava/util/concurrent/ExecutorService;

    iget-object v3, p0, Lcom/yandex/metrica/impl/bp;->c:Landroid/content/Context;

    invoke-direct {v1, v2, v3, v0}, Lcom/yandex/metrica/impl/az;-><init>(Ljava/util/concurrent/ExecutorService;Landroid/content/Context;Landroid/os/Handler;)V

    .line 75
    iget-object v2, p0, Lcom/yandex/metrica/impl/bp;->c:Landroid/content/Context;

    invoke-static {v2}, Lcom/yandex/metrica/impl/ob/cq;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/cq;

    move-result-object v2

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/cq;->e()Lcom/yandex/metrica/impl/ob/cr;

    move-result-object v2

    .line 76
    new-instance v3, Lcom/yandex/metrica/impl/ob/dc;

    invoke-direct {v3, v2}, Lcom/yandex/metrica/impl/ob/dc;-><init>(Lcom/yandex/metrica/impl/ob/cr;)V

    .line 78
    new-instance v2, Lcom/yandex/metrica/impl/g;

    invoke-direct {v2, v3}, Lcom/yandex/metrica/impl/g;-><init>(Lcom/yandex/metrica/impl/ob/dc;)V

    iget-object v4, p0, Lcom/yandex/metrica/impl/bp;->c:Landroid/content/Context;

    invoke-virtual {v2, v4}, Lcom/yandex/metrica/impl/g;->a(Landroid/content/Context;)V

    .line 80
    new-instance v2, Lcom/yandex/metrica/impl/ob/fy;

    invoke-direct {v2, v1, p2, v3}, Lcom/yandex/metrica/impl/ob/fy;-><init>(Lcom/yandex/metrica/impl/az;Ljava/lang/String;Lcom/yandex/metrica/impl/ob/dc;)V

    iput-object v2, p0, Lcom/yandex/metrica/impl/bp;->h:Lcom/yandex/metrica/impl/ob/fy;

    .line 81
    iget-object v2, p0, Lcom/yandex/metrica/impl/bp;->h:Lcom/yandex/metrica/impl/ob/fy;

    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/az;->a(Lcom/yandex/metrica/impl/ob/fz;)V

    .line 83
    new-instance v2, Lcom/yandex/metrica/impl/as;

    invoke-direct {v2, v1, v3}, Lcom/yandex/metrica/impl/as;-><init>(Lcom/yandex/metrica/impl/az;Lcom/yandex/metrica/impl/ob/dc;)V

    iput-object v2, p0, Lcom/yandex/metrica/impl/bp;->i:Lcom/yandex/metrica/impl/as;

    .line 85
    new-instance v2, Lcom/yandex/metrica/impl/k;

    invoke-direct {v2, v0}, Lcom/yandex/metrica/impl/k;-><init>(Landroid/os/Handler;)V

    .line 86
    invoke-virtual {v2, p0}, Lcom/yandex/metrica/impl/k;->a(Lcom/yandex/metrica/impl/k$a;)V

    .line 87
    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/az;->a(Lcom/yandex/metrica/impl/k;)V

    .line 89
    new-instance v4, Lcom/yandex/metrica/impl/ay$a;

    invoke-direct {v4}, Lcom/yandex/metrica/impl/ay$a;-><init>()V

    iget-object v5, p0, Lcom/yandex/metrica/impl/bp;->c:Landroid/content/Context;

    .line 90
    invoke-virtual {v4, v5}, Lcom/yandex/metrica/impl/ay$a;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ay$a;

    move-result-object v4

    iget-object v5, p0, Lcom/yandex/metrica/impl/bp;->h:Lcom/yandex/metrica/impl/ob/fy;

    .line 91
    invoke-virtual {v4, v5}, Lcom/yandex/metrica/impl/ay$a;->a(Lcom/yandex/metrica/impl/ob/fz;)Lcom/yandex/metrica/impl/ay$a;

    move-result-object v4

    .line 92
    invoke-virtual {v4, v1}, Lcom/yandex/metrica/impl/ay$a;->a(Lcom/yandex/metrica/impl/az;)Lcom/yandex/metrica/impl/ay$a;

    move-result-object v1

    .line 93
    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/ay$a;->a(Landroid/os/Handler;)Lcom/yandex/metrica/impl/ay$a;

    move-result-object v0

    .line 94
    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ay$a;->a(Lcom/yandex/metrica/impl/k;)Lcom/yandex/metrica/impl/ay$a;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ay$a;->a()Lcom/yandex/metrica/impl/ay;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/bp;->d:Lcom/yandex/metrica/impl/ay;

    .line 97
    invoke-static {}, Lcom/yandex/metrica/impl/bd;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    new-instance v0, Lcom/yandex/metrica/impl/h;

    new-instance v1, Lcom/yandex/metrica/impl/d;

    iget-object v2, p0, Lcom/yandex/metrica/impl/bp;->c:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/yandex/metrica/impl/d;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/yandex/metrica/impl/bp;->g:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v0, v3, v1, v2}, Lcom/yandex/metrica/impl/h;-><init>(Lcom/yandex/metrica/impl/ob/dc;Lcom/yandex/metrica/impl/d;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/bp;->j:Lcom/yandex/metrica/impl/h;

    .line 100
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/bp;)Lcom/yandex/metrica/impl/z;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/yandex/metrica/impl/bp;->e:Lcom/yandex/metrica/impl/z;

    return-object v0
.end method

.method public static a(I)V
    .locals 1

    .prologue
    .line 193
    invoke-static {}, Lcom/yandex/metrica/impl/bp;->g()Lcom/yandex/metrica/impl/ac;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/yandex/metrica/impl/ac;->setSessionTimeout(I)V

    .line 194
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 123
    const-class v0, Lcom/yandex/metrica/impl/bp;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0, v1}, Lcom/yandex/metrica/impl/bp;->b(Landroid/content/Context;Lcom/yandex/metrica/e;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    monitor-exit v0

    return-void

    .line 123
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized a(Landroid/content/Context;Lcom/yandex/metrica/e;)V
    .locals 5

    .prologue
    .line 108
    const-class v1, Lcom/yandex/metrica/impl/bp;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/yandex/metrica/impl/bp;->b:Lcom/yandex/metrica/impl/o;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/o;->i()Z

    move-result v0

    .line 109
    sget-object v2, Lcom/yandex/metrica/impl/bp;->b:Lcom/yandex/metrica/impl/o;

    invoke-virtual {v2, p1}, Lcom/yandex/metrica/impl/o;->a(Lcom/yandex/metrica/e;)Lcom/yandex/metrica/e;

    move-result-object v2

    .line 110
    invoke-static {p0, v2}, Lcom/yandex/metrica/impl/bp;->b(Landroid/content/Context;Lcom/yandex/metrica/e;)V

    .line 111
    sget-object v3, Lcom/yandex/metrica/impl/bp;->a:Lcom/yandex/metrica/impl/bp;

    iget-object v3, v3, Lcom/yandex/metrica/impl/bp;->e:Lcom/yandex/metrica/impl/z;

    if-nez v3, :cond_1

    .line 112
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Lcom/yandex/metrica/e;->isLogEnabled()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 113
    invoke-static {}, Lcom/yandex/metrica/impl/utils/l;->f()Lcom/yandex/metrica/impl/utils/l;

    move-result-object v3

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/utils/l;->a()V

    .line 115
    :cond_0
    sget-object v3, Lcom/yandex/metrica/impl/bp;->a:Lcom/yandex/metrica/impl/bp;

    .line 3285
    iget-object v4, v3, Lcom/yandex/metrica/impl/bp;->d:Lcom/yandex/metrica/impl/ay;

    invoke-virtual {v4, v2, v0}, Lcom/yandex/metrica/impl/ay;->a(Lcom/yandex/metrica/e;Z)Lcom/yandex/metrica/impl/z;

    move-result-object v0

    iput-object v0, v3, Lcom/yandex/metrica/impl/bp;->e:Lcom/yandex/metrica/impl/z;

    .line 3286
    iget-object v0, v3, Lcom/yandex/metrica/impl/bp;->e:Lcom/yandex/metrica/impl/z;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/z;->d()Lcom/yandex/metrica/impl/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ax;->b()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->k()Z

    move-result v0

    invoke-static {v0}, Lcom/yandex/metrica/impl/bp;->a(Z)V

    .line 4022
    :goto_0
    const-string v0, "20799a27-fa80-4b36-b2db-0f8141f24180"

    invoke-static {p0, v0}, Lcom/yandex/metrica/YandexMetrica;->getReporter(Landroid/content/Context;Ljava/lang/String;)Lcom/yandex/metrica/IReporter;

    move-result-object v0

    .line 119
    check-cast v0, Lcom/yandex/metrica/b;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Lcom/yandex/metrica/b;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    monitor-exit v1

    return-void

    .line 117
    :cond_1
    :try_start_1
    sget-object v3, Lcom/yandex/metrica/impl/bp;->a:Lcom/yandex/metrica/impl/bp;

    iget-object v3, v3, Lcom/yandex/metrica/impl/bp;->e:Lcom/yandex/metrica/impl/z;

    invoke-virtual {v3, v2, v0}, Lcom/yandex/metrica/impl/z;->a(Lcom/yandex/metrica/e;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 108
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/location/Location;)V
    .locals 1

    .prologue
    .line 225
    invoke-static {}, Lcom/yandex/metrica/impl/bp;->g()Lcom/yandex/metrica/impl/ac;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/yandex/metrica/impl/ac;->a(Landroid/location/Location;)V

    .line 226
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 253
    invoke-static {}, Lcom/yandex/metrica/impl/bp;->g()Lcom/yandex/metrica/impl/ac;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/yandex/metrica/impl/ac;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    return-void
.end method

.method public static a(Z)V
    .locals 4

    .prologue
    .line 197
    invoke-static {}, Lcom/yandex/metrica/impl/bp;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 198
    invoke-static {}, Lcom/yandex/metrica/impl/bp;->b()Lcom/yandex/metrica/impl/bp;

    move-result-object v0

    .line 4205
    if-eqz p0, :cond_1

    .line 4206
    iget-object v1, v0, Lcom/yandex/metrica/impl/bp;->k:Lcom/yandex/metrica/impl/j;

    if-nez v1, :cond_0

    .line 4207
    new-instance v1, Lcom/yandex/metrica/impl/aw;

    iget-object v2, v0, Lcom/yandex/metrica/impl/bp;->e:Lcom/yandex/metrica/impl/z;

    new-instance v3, Lcom/yandex/metrica/impl/bp$1;

    invoke-direct {v3, v0}, Lcom/yandex/metrica/impl/bp$1;-><init>(Lcom/yandex/metrica/impl/bp;)V

    invoke-direct {v1, v2, v3}, Lcom/yandex/metrica/impl/aw;-><init>(Lcom/yandex/metrica/IReporter;Lcom/yandex/metrica/impl/j$a;)V

    iput-object v1, v0, Lcom/yandex/metrica/impl/bp;->k:Lcom/yandex/metrica/impl/j;

    .line 4213
    :cond_0
    iget-object v1, v0, Lcom/yandex/metrica/impl/bp;->f:Lcom/yandex/metrica/impl/ah;

    iget-object v2, v0, Lcom/yandex/metrica/impl/bp;->k:Lcom/yandex/metrica/impl/j;

    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/ah;->a(Lcom/yandex/metrica/impl/j;)V

    .line 4217
    :goto_0
    iget-object v0, v0, Lcom/yandex/metrica/impl/bp;->e:Lcom/yandex/metrica/impl/z;

    invoke-virtual {v0, p0}, Lcom/yandex/metrica/impl/z;->c(Z)V

    .line 202
    :goto_1
    return-void

    .line 4215
    :cond_1
    iget-object v1, v0, Lcom/yandex/metrica/impl/bp;->f:Lcom/yandex/metrica/impl/ah;

    iget-object v2, v0, Lcom/yandex/metrica/impl/bp;->k:Lcom/yandex/metrica/impl/j;

    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/ah;->b(Lcom/yandex/metrica/impl/j;)V

    goto :goto_0

    .line 200
    :cond_2
    sget-object v0, Lcom/yandex/metrica/impl/bp;->b:Lcom/yandex/metrica/impl/o;

    invoke-virtual {v0, p0}, Lcom/yandex/metrica/impl/o;->c(Z)V

    goto :goto_1
.end method

.method public static declared-synchronized b()Lcom/yandex/metrica/impl/bp;
    .locals 2

    .prologue
    .line 137
    const-class v1, Lcom/yandex/metrica/impl/bp;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/yandex/metrica/impl/bp;->a:Lcom/yandex/metrica/impl/bp;

    if-nez v0, :cond_0

    .line 138
    sget-object v0, Lcom/yandex/metrica/impl/bm;->a:Ljava/lang/IllegalStateException;

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 140
    :cond_0
    :try_start_1
    sget-object v0, Lcom/yandex/metrica/impl/bp;->a:Lcom/yandex/metrica/impl/bp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method public static b(Landroid/content/Context;)Lcom/yandex/metrica/impl/bp;
    .locals 1

    .prologue
    .line 146
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/yandex/metrica/impl/bp;->a(Landroid/content/Context;)V

    .line 148
    invoke-static {}, Lcom/yandex/metrica/impl/bp;->b()Lcom/yandex/metrica/impl/bp;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized b(Landroid/content/Context;Lcom/yandex/metrica/e;)V
    .locals 4

    .prologue
    .line 127
    const-class v1, Lcom/yandex/metrica/impl/bp;

    monitor-enter v1

    :try_start_0
    const-string v0, "App Context"

    invoke-static {p0, v0}, Lcom/yandex/metrica/impl/bl;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    sget-object v0, Lcom/yandex/metrica/impl/bp;->a:Lcom/yandex/metrica/impl/bp;

    if-nez v0, :cond_1

    .line 129
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/yandex/metrica/e;->a()Ljava/lang/String;

    move-result-object v0

    .line 130
    :goto_0
    new-instance v2, Lcom/yandex/metrica/impl/bp;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/yandex/metrica/impl/bp;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 131
    sput-object v2, Lcom/yandex/metrica/impl/bp;->a:Lcom/yandex/metrica/impl/bp;

    .line 4164
    iget-object v0, v2, Lcom/yandex/metrica/impl/bp;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/metrica/impl/w;->a(Landroid/content/Context;)V

    .line 4170
    if-eqz p1, :cond_0

    .line 4171
    iget-object v0, v2, Lcom/yandex/metrica/impl/bp;->h:Lcom/yandex/metrica/impl/ob/fy;

    invoke-virtual {p1}, Lcom/yandex/metrica/e;->c()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/yandex/metrica/impl/ob/fy;->a(Ljava/util/List;)V

    .line 4172
    iget-object v0, v2, Lcom/yandex/metrica/impl/bp;->h:Lcom/yandex/metrica/impl/ob/fy;

    invoke-virtual {p1}, Lcom/yandex/metrica/e;->f()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/yandex/metrica/impl/ob/fy;->a(Ljava/util/Map;)V

    .line 4173
    iget-object v0, v2, Lcom/yandex/metrica/impl/bp;->h:Lcom/yandex/metrica/impl/ob/fy;

    invoke-virtual {p1}, Lcom/yandex/metrica/e;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/yandex/metrica/impl/ob/fy;->a(Ljava/lang/String;)V

    .line 4175
    :cond_0
    iget-object v0, v2, Lcom/yandex/metrica/impl/bp;->h:Lcom/yandex/metrica/impl/ob/fy;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fy;->d()V

    .line 4166
    iget-object v0, v2, Lcom/yandex/metrica/impl/bp;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/yandex/metrica/impl/utils/i$a;

    iget-object v2, v2, Lcom/yandex/metrica/impl/bp;->c:Landroid/content/Context;

    invoke-direct {v3, v2}, Lcom/yandex/metrica/impl/utils/i$a;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v3}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 132
    sget-object v0, Lcom/yandex/metrica/impl/bp;->a:Lcom/yandex/metrica/impl/bp;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/bp;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    :cond_1
    monitor-exit v1

    return-void

    .line 129
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b(Z)V
    .locals 1

    .prologue
    .line 221
    invoke-static {}, Lcom/yandex/metrica/impl/bp;->g()Lcom/yandex/metrica/impl/ac;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/yandex/metrica/impl/ac;->d(Z)V

    .line 222
    return-void
.end method

.method public static declared-synchronized c()Lcom/yandex/metrica/impl/z;
    .locals 3

    .prologue
    .line 152
    const-class v1, Lcom/yandex/metrica/impl/bp;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/yandex/metrica/impl/bp;->b()Lcom/yandex/metrica/impl/bp;

    move-result-object v0

    .line 153
    iget-object v2, v0, Lcom/yandex/metrica/impl/bp;->e:Lcom/yandex/metrica/impl/z;

    if-nez v2, :cond_0

    .line 154
    sget-object v0, Lcom/yandex/metrica/impl/bm;->a:Ljava/lang/IllegalStateException;

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 156
    :cond_0
    :try_start_1
    iget-object v0, v0, Lcom/yandex/metrica/impl/bp;->e:Lcom/yandex/metrica/impl/z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method public static c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 233
    invoke-static {}, Lcom/yandex/metrica/impl/bp;->g()Lcom/yandex/metrica/impl/ac;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/yandex/metrica/impl/ac;->a(Ljava/lang/String;)V

    .line 234
    return-void
.end method

.method public static c(Z)V
    .locals 1

    .prologue
    .line 229
    invoke-static {}, Lcom/yandex/metrica/impl/bp;->g()Lcom/yandex/metrica/impl/ac;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/yandex/metrica/impl/ac;->b(Z)V

    .line 230
    return-void
.end method

.method public static d(Z)V
    .locals 1

    .prologue
    .line 237
    invoke-static {}, Lcom/yandex/metrica/impl/bp;->g()Lcom/yandex/metrica/impl/ac;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/yandex/metrica/impl/ac;->a(Z)V

    .line 238
    return-void
.end method

.method static declared-synchronized d()Z
    .locals 2

    .prologue
    .line 160
    const-class v1, Lcom/yandex/metrica/impl/bp;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/yandex/metrica/impl/bp;->a:Lcom/yandex/metrica/impl/bp;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/yandex/metrica/impl/bp;->a:Lcom/yandex/metrica/impl/bp;

    iget-object v0, v0, Lcom/yandex/metrica/impl/bp;->e:Lcom/yandex/metrica/impl/z;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static e()Z
    .locals 1

    .prologue
    .line 241
    invoke-static {}, Lcom/yandex/metrica/impl/bp;->g()Lcom/yandex/metrica/impl/ac;

    move-result-object v0

    invoke-interface {v0}, Lcom/yandex/metrica/impl/ac;->h()Z

    move-result v0

    return v0
.end method

.method private static g()Lcom/yandex/metrica/impl/ac;
    .locals 1

    .prologue
    .line 187
    invoke-static {}, Lcom/yandex/metrica/impl/bp;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    invoke-static {}, Lcom/yandex/metrica/impl/bp;->b()Lcom/yandex/metrica/impl/bp;

    move-result-object v0

    iget-object v0, v0, Lcom/yandex/metrica/impl/bp;->e:Lcom/yandex/metrica/impl/z;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/yandex/metrica/impl/bp;->b:Lcom/yandex/metrica/impl/o;

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/yandex/metrica/b;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/yandex/metrica/impl/bp;->d:Lcom/yandex/metrica/impl/ay;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ay;->a(Ljava/lang/String;)Lcom/yandex/metrica/b;

    move-result-object v0

    return-object v0
.end method

.method a()V
    .locals 4

    .prologue
    .line 103
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    .line 2316
    new-instance v1, Lcom/yandex/metrica/impl/ah;

    invoke-direct {v1, v0}, Lcom/yandex/metrica/impl/ah;-><init>(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 2318
    new-instance v0, Lcom/yandex/metrica/impl/aw;

    iget-object v2, p0, Lcom/yandex/metrica/impl/bp;->d:Lcom/yandex/metrica/impl/ay;

    const-string v3, "20799a27-fa80-4b36-b2db-0f8141f24180"

    invoke-virtual {v2, v3}, Lcom/yandex/metrica/impl/ay;->a(Ljava/lang/String;)Lcom/yandex/metrica/b;

    move-result-object v2

    new-instance v3, Lcom/yandex/metrica/impl/bp$2;

    invoke-direct {v3}, Lcom/yandex/metrica/impl/bp$2;-><init>()V

    invoke-direct {v0, v2, v3}, Lcom/yandex/metrica/impl/aw;-><init>(Lcom/yandex/metrica/IReporter;Lcom/yandex/metrica/impl/j$a;)V

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/ah;->a(Lcom/yandex/metrica/impl/j;)V

    .line 103
    iput-object v1, p0, Lcom/yandex/metrica/impl/bp;->f:Lcom/yandex/metrica/impl/ah;

    .line 104
    iget-object v0, p0, Lcom/yandex/metrica/impl/bp;->f:Lcom/yandex/metrica/impl/ah;

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 105
    return-void
.end method

.method public a(ILandroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 293
    packed-switch p1, :pswitch_data_0

    .line 307
    :cond_0
    :goto_0
    return-void

    .line 295
    :pswitch_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/bp;->h:Lcom/yandex/metrica/impl/ob/fy;

    invoke-virtual {v0, p2}, Lcom/yandex/metrica/impl/ob/fy;->a(Landroid/os/Bundle;)V

    .line 296
    iget-object v0, p0, Lcom/yandex/metrica/impl/bp;->j:Lcom/yandex/metrica/impl/h;

    if-eqz v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/yandex/metrica/impl/bp;->j:Lcom/yandex/metrica/impl/h;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/h;->a()V

    goto :goto_0

    .line 301
    :pswitch_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/bp;->h:Lcom/yandex/metrica/impl/ob/fy;

    invoke-virtual {v0, p2}, Lcom/yandex/metrica/impl/ob/fy;->b(Landroid/os/Bundle;)V

    goto :goto_0

    .line 293
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/yandex/metrica/DeferredDeeplinkParametersListener;)V
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lcom/yandex/metrica/impl/bp;->i:Lcom/yandex/metrica/impl/as;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/as;->a(Lcom/yandex/metrica/DeferredDeeplinkParametersListener;)V

    .line 359
    return-void
.end method

.method public a(Lcom/yandex/metrica/IIdentifierCallback;)V
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/yandex/metrica/impl/bp;->h:Lcom/yandex/metrica/impl/ob/fy;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/fy;->a(Lcom/yandex/metrica/IIdentifierCallback;)V

    .line 282
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/yandex/metrica/impl/bp;->i:Lcom/yandex/metrica/impl/as;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/as;->a(Ljava/lang/String;)V

    .line 184
    return-void
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/yandex/metrica/impl/bp;->h:Lcom/yandex/metrica/impl/ob/fy;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fy;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
