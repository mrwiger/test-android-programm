.class public Ltoothpick/ScopeImpl;
.super Ltoothpick/ScopeNode;
.source "ScopeImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltoothpick/ScopeImpl$ClassNameComparator;
    }
.end annotation


# static fields
.field private static final LINE_SEPARATOR:Ljava/lang/String;

.field private static mapClassesToUnNamedUnBoundProviders:Ljava/util/IdentityHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/IdentityHashMap",
            "<",
            "Ljava/lang/Class;",
            "Ltoothpick/InternalProviderImpl;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private hasTestModules:Z

.field private mapClassesToNamedBoundProviders:Ljava/util/IdentityHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/IdentityHashMap",
            "<",
            "Ljava/lang/Class;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ltoothpick/InternalProviderImpl;",
            ">;>;"
        }
    .end annotation
.end field

.field private mapClassesToUnNamedBoundProviders:Ljava/util/IdentityHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/IdentityHashMap",
            "<",
            "Ljava/lang/Class;",
            "Ltoothpick/InternalProviderImpl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ltoothpick/ScopeImpl;->LINE_SEPARATOR:Ljava/lang/String;

    .line 37
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    sput-object v0, Ltoothpick/ScopeImpl;->mapClassesToUnNamedUnBoundProviders:Ljava/util/IdentityHashMap;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/Object;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Ltoothpick/ScopeNode;-><init>(Ljava/lang/Object;)V

    .line 38
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    iput-object v0, p0, Ltoothpick/ScopeImpl;->mapClassesToNamedBoundProviders:Ljava/util/IdentityHashMap;

    .line 39
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    iput-object v0, p0, Ltoothpick/ScopeImpl;->mapClassesToUnNamedBoundProviders:Ljava/util/IdentityHashMap;

    .line 44
    invoke-direct {p0}, Ltoothpick/ScopeImpl;->installBindingForScope()V

    .line 45
    return-void
.end method

.method private crashIfClosed()V
    .locals 5

    .prologue
    .line 527
    iget-boolean v0, p0, Ltoothpick/ScopeImpl;->isOpen:Z

    if-nez v0, :cond_0

    .line 528
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The scope with name %s has been already closed. It can\'t be used to create new instances."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Ltoothpick/ScopeImpl;->name:Ljava/lang/Object;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 531
    :cond_0
    return-void
.end method

.method private createInternalProvider(Ltoothpick/Scope;Ljava/lang/Class;ZZZZ)Ltoothpick/InternalProviderImpl;
    .locals 6
    .param p1, "scope"    # Ltoothpick/Scope;
    .param p3, "isProviderClass"    # Z
    .param p4, "isCreatingInstancesInScope"    # Z
    .param p5, "isCreatingSingletonInScope"    # Z
    .param p6, "isProvidingInstancesInScope"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ltoothpick/Scope;",
            "Ljava/lang/Class",
            "<*>;ZZZZ)",
            "Ltoothpick/InternalProviderImpl",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 259
    .local p2, "factoryKeyClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-eqz p4, :cond_0

    .line 260
    new-instance v0, Ltoothpick/ScopedProviderImpl;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Ltoothpick/ScopedProviderImpl;-><init>(Ltoothpick/Scope;Ljava/lang/Class;ZZZ)V

    .line 266
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ltoothpick/InternalProviderImpl;

    invoke-direct {v0, p2, p3, p5, p6}, Ltoothpick/InternalProviderImpl;-><init>(Ljava/lang/Class;ZZZ)V

    goto :goto_0
.end method

.method private getBoundProvider(Ljava/lang/Class;Ljava/lang/String;)Ltoothpick/InternalProviderImpl;
    .locals 1
    .param p2, "bindingName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            ")",
            "Ltoothpick/InternalProviderImpl",
            "<+TT;>;"
        }
    .end annotation

    .prologue
    .line 363
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Ltoothpick/ScopeImpl;->getInternalProvider(Ljava/lang/Class;Ljava/lang/String;Z)Ltoothpick/InternalProviderImpl;

    move-result-object v0

    return-object v0
.end method

.method private getInternalProvider(Ljava/lang/Class;Ljava/lang/String;Z)Ltoothpick/InternalProviderImpl;
    .locals 3
    .param p2, "bindingName"    # Ljava/lang/String;
    .param p3, "isBound"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            "Z)",
            "Ltoothpick/InternalProviderImpl",
            "<+TT;>;"
        }
    .end annotation

    .prologue
    .line 396
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    if-nez p2, :cond_1

    .line 397
    if-eqz p3, :cond_0

    .line 398
    iget-object v2, p0, Ltoothpick/ScopeImpl;->mapClassesToUnNamedBoundProviders:Ljava/util/IdentityHashMap;

    monitor-enter v2

    .line 399
    :try_start_0
    iget-object v1, p0, Ltoothpick/ScopeImpl;->mapClassesToUnNamedBoundProviders:Ljava/util/IdentityHashMap;

    invoke-virtual {v1, p1}, Ljava/util/IdentityHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltoothpick/InternalProviderImpl;

    monitor-exit v2

    .line 412
    :goto_0
    return-object v1

    .line 400
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 402
    :cond_0
    sget-object v2, Ltoothpick/ScopeImpl;->mapClassesToUnNamedUnBoundProviders:Ljava/util/IdentityHashMap;

    monitor-enter v2

    .line 403
    :try_start_1
    sget-object v1, Ltoothpick/ScopeImpl;->mapClassesToUnNamedUnBoundProviders:Ljava/util/IdentityHashMap;

    invoke-virtual {v1, p1}, Ljava/util/IdentityHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltoothpick/InternalProviderImpl;

    monitor-exit v2

    goto :goto_0

    .line 404
    :catchall_1
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v1

    .line 407
    :cond_1
    iget-object v2, p0, Ltoothpick/ScopeImpl;->mapClassesToNamedBoundProviders:Ljava/util/IdentityHashMap;

    monitor-enter v2

    .line 408
    :try_start_2
    iget-object v1, p0, Ltoothpick/ScopeImpl;->mapClassesToNamedBoundProviders:Ljava/util/IdentityHashMap;

    invoke-virtual {v1, p1}, Ljava/util/IdentityHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 409
    .local v0, "mapNameToProvider":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ltoothpick/InternalProviderImpl;>;"
    if-nez v0, :cond_2

    .line 410
    const/4 v1, 0x0

    monitor-exit v2

    goto :goto_0

    .line 413
    .end local v0    # "mapNameToProvider":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ltoothpick/InternalProviderImpl;>;"
    :catchall_2
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    throw v1

    .line 412
    .restart local v0    # "mapNameToProvider":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ltoothpick/InternalProviderImpl;>;"
    :cond_2
    :try_start_3
    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltoothpick/InternalProviderImpl;

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_0
.end method

.method private getUnBoundProvider(Ljava/lang/Class;Ljava/lang/String;)Ltoothpick/InternalProviderImpl;
    .locals 1
    .param p2, "bindingName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            ")",
            "Ltoothpick/InternalProviderImpl",
            "<+TT;>;"
        }
    .end annotation

    .prologue
    .line 377
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Ltoothpick/ScopeImpl;->getInternalProvider(Ljava/lang/Class;Ljava/lang/String;Z)Ltoothpick/InternalProviderImpl;

    move-result-object v0

    return-object v0
.end method

.method private installBindingForScope()V
    .locals 4

    .prologue
    .line 555
    const-class v0, Ltoothpick/Scope;

    const/4 v1, 0x0

    new-instance v2, Ltoothpick/InternalProviderImpl;

    invoke-direct {v2, p0}, Ltoothpick/InternalProviderImpl;-><init>(Ljava/lang/Object;)V

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Ltoothpick/ScopeImpl;->installBoundProvider(Ljava/lang/Class;Ljava/lang/String;Ltoothpick/InternalProviderImpl;Z)Ltoothpick/InternalProviderImpl;

    .line 556
    return-void
.end method

.method private installBoundProvider(Ljava/lang/Class;Ljava/lang/String;Ltoothpick/InternalProviderImpl;Z)Ltoothpick/InternalProviderImpl;
    .locals 6
    .param p2, "bindingName"    # Ljava/lang/String;
    .param p4, "isTestProvider"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            "Ltoothpick/InternalProviderImpl",
            "<+TT;>;Z)",
            "Ltoothpick/InternalProviderImpl",
            "<+TT;>;"
        }
    .end annotation

    .prologue
    .line 447
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p3, "internalProvider":Ltoothpick/InternalProviderImpl;, "Ltoothpick/InternalProviderImpl<+TT;>;"
    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Ltoothpick/ScopeImpl;->installInternalProvider(Ljava/lang/Class;Ljava/lang/String;Ltoothpick/InternalProviderImpl;ZZ)Ltoothpick/InternalProviderImpl;

    move-result-object v0

    return-object v0
.end method

.method private installInternalProvider(Ljava/lang/Class;Ljava/lang/String;Ltoothpick/InternalProviderImpl;ZZ)Ltoothpick/InternalProviderImpl;
    .locals 6
    .param p2, "bindingName"    # Ljava/lang/String;
    .param p4, "isBound"    # Z
    .param p5, "isTestProvider"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            "Ltoothpick/InternalProviderImpl",
            "<+TT;>;ZZ)",
            "Ltoothpick/InternalProviderImpl;"
        }
    .end annotation

    .prologue
    .line 481
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p3, "internalProvider":Ltoothpick/InternalProviderImpl;, "Ltoothpick/InternalProviderImpl<+TT;>;"
    if-nez p2, :cond_1

    .line 482
    if-eqz p4, :cond_0

    .line 483
    iget-object v0, p0, Ltoothpick/ScopeImpl;->mapClassesToUnNamedBoundProviders:Ljava/util/IdentityHashMap;

    invoke-direct {p0, v0, p1, p3, p5}, Ltoothpick/ScopeImpl;->installUnNamedProvider(Ljava/util/IdentityHashMap;Ljava/lang/Class;Ltoothpick/InternalProviderImpl;Z)Ltoothpick/InternalProviderImpl;

    move-result-object v0

    .line 488
    :goto_0
    return-object v0

    .line 485
    :cond_0
    sget-object v0, Ltoothpick/ScopeImpl;->mapClassesToUnNamedUnBoundProviders:Ljava/util/IdentityHashMap;

    invoke-direct {p0, v0, p1, p3, p5}, Ltoothpick/ScopeImpl;->installUnNamedProvider(Ljava/util/IdentityHashMap;Ljava/lang/Class;Ltoothpick/InternalProviderImpl;Z)Ltoothpick/InternalProviderImpl;

    move-result-object v0

    goto :goto_0

    .line 488
    :cond_1
    iget-object v1, p0, Ltoothpick/ScopeImpl;->mapClassesToNamedBoundProviders:Ljava/util/IdentityHashMap;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p5

    invoke-direct/range {v0 .. v5}, Ltoothpick/ScopeImpl;->installNamedProvider(Ljava/util/IdentityHashMap;Ljava/lang/Class;Ljava/lang/String;Ltoothpick/InternalProviderImpl;Z)Ltoothpick/InternalProviderImpl;

    move-result-object v0

    goto :goto_0
.end method

.method private installModule(ZLtoothpick/config/Module;)V
    .locals 9
    .param p1, "isTestModule"    # Z
    .param p2, "module"    # Ltoothpick/config/Module;

    .prologue
    .line 186
    invoke-virtual {p2}, Ltoothpick/config/Module;->getBindingSet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltoothpick/config/Binding;

    .line 187
    .local v0, "binding":Ltoothpick/config/Binding;
    if-nez v0, :cond_1

    .line 188
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "A module can\'t have a null binding : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 191
    :cond_1
    invoke-virtual {v0}, Ltoothpick/config/Binding;->getKey()Ljava/lang/Class;

    move-result-object v2

    .line 192
    .local v2, "clazz":Ljava/lang/Class;
    invoke-virtual {v0}, Ltoothpick/config/Binding;->getName()Ljava/lang/String;

    move-result-object v1

    .line 194
    .local v1, "bindingName":Ljava/lang/String;
    if-nez p1, :cond_2

    :try_start_0
    invoke-direct {p0, v2, v1}, Ltoothpick/ScopeImpl;->getBoundProvider(Ljava/lang/Class;Ljava/lang/String;)Ltoothpick/InternalProviderImpl;

    move-result-object v6

    if-nez v6, :cond_0

    .line 195
    :cond_2
    invoke-virtual {p0, v0}, Ltoothpick/ScopeImpl;->toProvider(Ltoothpick/config/Binding;)Ltoothpick/InternalProviderImpl;

    move-result-object v4

    .line 196
    .local v4, "provider":Ltoothpick/InternalProviderImpl;
    invoke-virtual {v0}, Ltoothpick/config/Binding;->isCreatingInstancesInScope()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 197
    check-cast v4, Ltoothpick/ScopedProviderImpl;

    .end local v4    # "provider":Ltoothpick/InternalProviderImpl;
    invoke-direct {p0, v2, v1, v4, p1}, Ltoothpick/ScopeImpl;->installScopedProvider(Ljava/lang/Class;Ljava/lang/String;Ltoothpick/ScopedProviderImpl;Z)Ltoothpick/InternalProviderImpl;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 202
    :catch_0
    move-exception v3

    .line 203
    .local v3, "e":Ljava/lang/Exception;
    new-instance v5, Ltoothpick/configuration/IllegalBindingException;

    const-string v6, "Binding %s couldn\'t be installed"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v3}, Ltoothpick/configuration/IllegalBindingException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 199
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v4    # "provider":Ltoothpick/InternalProviderImpl;
    :cond_3
    :try_start_1
    invoke-direct {p0, v2, v1, v4, p1}, Ltoothpick/ScopeImpl;->installBoundProvider(Ljava/lang/Class;Ljava/lang/String;Ltoothpick/InternalProviderImpl;Z)Ltoothpick/InternalProviderImpl;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 206
    .end local v0    # "binding":Ltoothpick/config/Binding;
    .end local v1    # "bindingName":Ljava/lang/String;
    .end local v2    # "clazz":Ljava/lang/Class;
    .end local v4    # "provider":Ltoothpick/InternalProviderImpl;
    :cond_4
    return-void
.end method

.method private varargs installModules(Z[Ltoothpick/config/Module;)V
    .locals 7
    .param p1, "isTestModule"    # Z
    .param p2, "modules"    # [Ltoothpick/config/Module;

    .prologue
    const/4 v3, 0x0

    .line 176
    array-length v4, p2

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v1, p2, v2

    .line 178
    .local v1, "module":Ltoothpick/config/Module;
    :try_start_0
    invoke-direct {p0, p1, v1}, Ltoothpick/ScopeImpl;->installModule(ZLtoothpick/config/Module;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 179
    :catch_0
    move-exception v0

    .line 180
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v4, "Module %s couldn\'t be installed"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 183
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "module":Ltoothpick/config/Module;
    :cond_0
    return-void
.end method

.method private installNamedProvider(Ljava/util/IdentityHashMap;Ljava/lang/Class;Ljava/lang/String;Ltoothpick/InternalProviderImpl;Z)Ltoothpick/InternalProviderImpl;
    .locals 3
    .param p3, "bindingName"    # Ljava/lang/String;
    .param p5, "isTestProvider"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/IdentityHashMap",
            "<",
            "Ljava/lang/Class;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ltoothpick/InternalProviderImpl;",
            ">;>;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            "Ltoothpick/InternalProviderImpl",
            "<+TT;>;Z)",
            "Ltoothpick/InternalProviderImpl;"
        }
    .end annotation

    .prologue
    .line 494
    .local p1, "mapClassesToNamedBoundProviders":Ljava/util/IdentityHashMap;, "Ljava/util/IdentityHashMap<Ljava/lang/Class;Ljava/util/Map<Ljava/lang/String;Ltoothpick/InternalProviderImpl;>;>;"
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p4, "internalProvider":Ltoothpick/InternalProviderImpl;, "Ltoothpick/InternalProviderImpl<+TT;>;"
    monitor-enter p1

    .line 495
    :try_start_0
    invoke-virtual {p1, p2}, Ljava/util/IdentityHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 496
    .local v0, "mapNameToProvider":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ltoothpick/InternalProviderImpl;>;"
    if-nez v0, :cond_0

    .line 497
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "mapNameToProvider":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ltoothpick/InternalProviderImpl;>;"
    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 498
    .restart local v0    # "mapNameToProvider":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ltoothpick/InternalProviderImpl;>;"
    invoke-virtual {p1, p2, v0}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 499
    invoke-interface {v0, p3, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 500
    monitor-exit p1

    .line 508
    .end local p4    # "internalProvider":Ltoothpick/InternalProviderImpl;, "Ltoothpick/InternalProviderImpl<+TT;>;"
    :goto_0
    return-object p4

    .line 503
    .restart local p4    # "internalProvider":Ltoothpick/InternalProviderImpl;, "Ltoothpick/InternalProviderImpl<+TT;>;"
    :cond_0
    invoke-interface {v0, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltoothpick/InternalProviderImpl;

    .line 504
    .local v1, "previous":Ltoothpick/InternalProviderImpl;
    if-eqz v1, :cond_1

    if-eqz p5, :cond_2

    .line 505
    :cond_1
    invoke-interface {v0, p3, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 506
    monitor-exit p1

    goto :goto_0

    .line 510
    .end local v0    # "mapNameToProvider":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ltoothpick/InternalProviderImpl;>;"
    .end local v1    # "previous":Ltoothpick/InternalProviderImpl;
    :catchall_0
    move-exception v2

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 508
    .restart local v0    # "mapNameToProvider":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ltoothpick/InternalProviderImpl;>;"
    .restart local v1    # "previous":Ltoothpick/InternalProviderImpl;
    :cond_2
    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object p4, v1

    goto :goto_0
.end method

.method private installScopedProvider(Ljava/lang/Class;Ljava/lang/String;Ltoothpick/ScopedProviderImpl;Z)Ltoothpick/InternalProviderImpl;
    .locals 1
    .param p2, "bindingName"    # Ljava/lang/String;
    .param p4, "isTestProvider"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            "Ltoothpick/ScopedProviderImpl",
            "<+TT;>;Z)",
            "Ltoothpick/InternalProviderImpl",
            "<+TT;>;"
        }
    .end annotation

    .prologue
    .line 431
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p3, "scopedProvider":Ltoothpick/ScopedProviderImpl;, "Ltoothpick/ScopedProviderImpl<+TT;>;"
    invoke-direct {p0, p1, p2, p3, p4}, Ltoothpick/ScopeImpl;->installBoundProvider(Ljava/lang/Class;Ljava/lang/String;Ltoothpick/InternalProviderImpl;Z)Ltoothpick/InternalProviderImpl;

    move-result-object v0

    return-object v0
.end method

.method private installUnBoundProvider(Ljava/lang/Class;Ljava/lang/String;Ltoothpick/InternalProviderImpl;)Ltoothpick/InternalProviderImpl;
    .locals 6
    .param p2, "bindingName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            "Ltoothpick/InternalProviderImpl",
            "<+TT;>;)",
            "Ltoothpick/InternalProviderImpl",
            "<+TT;>;"
        }
    .end annotation

    .prologue
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p3, "internalProvider":Ltoothpick/InternalProviderImpl;, "Ltoothpick/InternalProviderImpl<+TT;>;"
    const/4 v4, 0x0

    .line 461
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, v4

    invoke-direct/range {v0 .. v5}, Ltoothpick/ScopeImpl;->installInternalProvider(Ljava/lang/Class;Ljava/lang/String;Ltoothpick/InternalProviderImpl;ZZ)Ltoothpick/InternalProviderImpl;

    move-result-object v0

    return-object v0
.end method

.method private installUnNamedProvider(Ljava/util/IdentityHashMap;Ljava/lang/Class;Ltoothpick/InternalProviderImpl;Z)Ltoothpick/InternalProviderImpl;
    .locals 2
    .param p4, "isTestProvider"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/IdentityHashMap",
            "<",
            "Ljava/lang/Class;",
            "Ltoothpick/InternalProviderImpl;",
            ">;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ltoothpick/InternalProviderImpl",
            "<+TT;>;Z)",
            "Ltoothpick/InternalProviderImpl;"
        }
    .end annotation

    .prologue
    .line 515
    .local p1, "mapClassesToUnNamedProviders":Ljava/util/IdentityHashMap;, "Ljava/util/IdentityHashMap<Ljava/lang/Class;Ltoothpick/InternalProviderImpl;>;"
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p3, "internalProvider":Ltoothpick/InternalProviderImpl;, "Ltoothpick/InternalProviderImpl<+TT;>;"
    monitor-enter p1

    .line 516
    :try_start_0
    invoke-virtual {p1, p2}, Ljava/util/IdentityHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltoothpick/InternalProviderImpl;

    .line 517
    .local v0, "previous":Ltoothpick/InternalProviderImpl;
    if-eqz v0, :cond_0

    if-eqz p4, :cond_1

    .line 518
    :cond_0
    invoke-virtual {p1, p2, p3}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 519
    monitor-exit p1

    move-object v0, p3

    .line 521
    .end local v0    # "previous":Ltoothpick/InternalProviderImpl;
    :goto_0
    return-object v0

    .restart local v0    # "previous":Ltoothpick/InternalProviderImpl;
    :cond_1
    monitor-exit p1

    goto :goto_0

    .line 523
    .end local v0    # "previous":Ltoothpick/InternalProviderImpl;
    :catchall_0
    move-exception v1

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method static resetUnBoundProviders()V
    .locals 1

    .prologue
    .line 534
    sget-object v0, Ltoothpick/ScopeImpl;->mapClassesToUnNamedUnBoundProviders:Ljava/util/IdentityHashMap;

    invoke-virtual {v0}, Ljava/util/IdentityHashMap;->clear()V

    .line 535
    return-void
.end method


# virtual methods
.method public getInstance(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 49
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Ltoothpick/ScopeImpl;->getInstance(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getInstance(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;
    .locals 3
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 54
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-direct {p0}, Ltoothpick/ScopeImpl;->crashIfClosed()V

    .line 55
    sget-object v1, Ltoothpick/configuration/ConfigurationHolder;->configuration:Ltoothpick/configuration/Configuration;

    invoke-virtual {v1, p1, p2}, Ltoothpick/configuration/Configuration;->checkCyclesStart(Ljava/lang/Class;Ljava/lang/String;)V

    .line 58
    :try_start_0
    invoke-virtual {p0, p1, p2}, Ltoothpick/ScopeImpl;->lookupProvider(Ljava/lang/Class;Ljava/lang/String;)Ltoothpick/InternalProviderImpl;

    move-result-object v1

    invoke-virtual {v1, p0}, Ltoothpick/InternalProviderImpl;->get(Ltoothpick/Scope;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 60
    .local v0, "t":Ljava/lang/Object;, "TT;"
    sget-object v1, Ltoothpick/configuration/ConfigurationHolder;->configuration:Ltoothpick/configuration/Configuration;

    invoke-virtual {v1, p1, p2}, Ltoothpick/configuration/Configuration;->checkCyclesEnd(Ljava/lang/Class;Ljava/lang/String;)V

    .line 62
    return-object v0

    .line 60
    .end local v0    # "t":Ljava/lang/Object;, "TT;"
    :catchall_0
    move-exception v1

    sget-object v2, Ltoothpick/configuration/ConfigurationHolder;->configuration:Ltoothpick/configuration/Configuration;

    invoke-virtual {v2, p1, p2}, Ltoothpick/configuration/Configuration;->checkCyclesEnd(Ljava/lang/Class;Ljava/lang/String;)V

    .line 61
    throw v1
.end method

.method public getLazy(Ljava/lang/Class;)Ltoothpick/Lazy;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ltoothpick/Lazy",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 78
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Ltoothpick/ScopeImpl;->getLazy(Ljava/lang/Class;Ljava/lang/String;)Ltoothpick/Lazy;

    move-result-object v0

    return-object v0
.end method

.method public getLazy(Ljava/lang/Class;Ljava/lang/String;)Ltoothpick/Lazy;
    .locals 2
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            ")",
            "Ltoothpick/Lazy",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 83
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-direct {p0}, Ltoothpick/ScopeImpl;->crashIfClosed()V

    .line 84
    new-instance v0, Ltoothpick/ThreadSafeProviderImpl;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, p2, v1}, Ltoothpick/ThreadSafeProviderImpl;-><init>(Ltoothpick/Scope;Ljava/lang/Class;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public getProvider(Ljava/lang/Class;)Ljavax/inject/Provider;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ljavax/inject/Provider",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 67
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Ltoothpick/ScopeImpl;->getProvider(Ljava/lang/Class;Ljava/lang/String;)Ljavax/inject/Provider;

    move-result-object v0

    return-object v0
.end method

.method public getProvider(Ljava/lang/Class;Ljava/lang/String;)Ljavax/inject/Provider;
    .locals 2
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            ")",
            "Ljavax/inject/Provider",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 72
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-direct {p0}, Ltoothpick/ScopeImpl;->crashIfClosed()V

    .line 73
    new-instance v0, Ltoothpick/ThreadSafeProviderImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Ltoothpick/ThreadSafeProviderImpl;-><init>(Ltoothpick/Scope;Ljava/lang/Class;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public varargs installModules([Ltoothpick/config/Module;)V
    .locals 1
    .param p1, "modules"    # [Ltoothpick/config/Module;

    .prologue
    .line 98
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Ltoothpick/ScopeImpl;->installModules(Z[Ltoothpick/config/Module;)V

    .line 99
    return-void
.end method

.method public varargs declared-synchronized installTestModules([Ltoothpick/config/Module;)V
    .locals 2
    .param p1, "modules"    # [Ltoothpick/config/Module;

    .prologue
    .line 89
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Ltoothpick/ScopeImpl;->hasTestModules:Z

    if-eqz v0, :cond_0

    .line 90
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "TestModules can only be installed once per scope."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 92
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    invoke-direct {p0, v0, p1}, Ltoothpick/ScopeImpl;->installModules(Z[Ltoothpick/config/Module;)V

    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Ltoothpick/ScopeImpl;->hasTestModules:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 94
    monitor-exit p0

    return-void
.end method

.method lookupProvider(Ljava/lang/Class;Ljava/lang/String;)Ltoothpick/InternalProviderImpl;
    .locals 18
    .param p2, "bindingName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            ")",
            "Ltoothpick/InternalProviderImpl",
            "<+TT;>;"
        }
    .end annotation

    .prologue
    .line 291
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    if-nez p1, :cond_0

    .line 292
    new-instance v13, Ljava/lang/IllegalArgumentException;

    const-string v14, "TP can\'t get an instance of a null class."

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 294
    :cond_0
    invoke-direct/range {p0 .. p2}, Ltoothpick/ScopeImpl;->getBoundProvider(Ljava/lang/Class;Ljava/lang/String;)Ltoothpick/InternalProviderImpl;

    move-result-object v9

    .line 295
    .local v9, "scopedProvider":Ltoothpick/InternalProviderImpl;, "Ltoothpick/InternalProviderImpl<+TT;>;"
    if-eqz v9, :cond_1

    .line 347
    .end local v9    # "scopedProvider":Ltoothpick/InternalProviderImpl;, "Ltoothpick/InternalProviderImpl<+TT;>;"
    :goto_0
    return-object v9

    .line 298
    .restart local v9    # "scopedProvider":Ltoothpick/InternalProviderImpl;, "Ltoothpick/InternalProviderImpl<+TT;>;"
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Ltoothpick/ScopeImpl;->parentScopes:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 299
    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ltoothpick/ScopeNode;>;"
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_3

    .line 300
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ltoothpick/Scope;

    .local v6, "parentScope":Ltoothpick/Scope;
    move-object v7, v6

    .line 301
    check-cast v7, Ltoothpick/ScopeImpl;

    .line 302
    .local v7, "parentScopeImpl":Ltoothpick/ScopeImpl;
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v7, v0, v1}, Ltoothpick/ScopeImpl;->getBoundProvider(Ljava/lang/Class;Ljava/lang/String;)Ltoothpick/InternalProviderImpl;

    move-result-object v8

    .line 303
    .local v8, "parentScopedProvider":Ltoothpick/InternalProviderImpl;, "Ltoothpick/InternalProviderImpl<+TT;>;"
    if-eqz v8, :cond_2

    move-object v9, v8

    .line 304
    goto :goto_0

    .line 311
    .end local v6    # "parentScope":Ltoothpick/Scope;
    .end local v7    # "parentScopeImpl":Ltoothpick/ScopeImpl;
    .end local v8    # "parentScopedProvider":Ltoothpick/InternalProviderImpl;, "Ltoothpick/InternalProviderImpl<+TT;>;"
    :cond_3
    if-eqz p2, :cond_4

    .line 312
    new-instance v13, Ljava/lang/RuntimeException;

    const-string v14, "No binding was defined for class %s and name %s in scope %s and its parents %s"

    const/4 v15, 0x4

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    .line 313
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    aput-object p2, v15, v16

    const/16 v16, 0x2

    invoke-virtual/range {p0 .. p0}, Ltoothpick/ScopeImpl;->getName()Ljava/lang/Object;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x3

    invoke-virtual/range {p0 .. p0}, Ltoothpick/ScopeImpl;->getParentScopesNames()Ljava/util/List;

    move-result-object v17

    aput-object v17, v15, v16

    .line 312
    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 320
    :cond_4
    const/4 v13, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13}, Ltoothpick/ScopeImpl;->getUnBoundProvider(Ljava/lang/Class;Ljava/lang/String;)Ltoothpick/InternalProviderImpl;

    move-result-object v12

    .line 321
    .local v12, "unScopedProviderInPool":Ltoothpick/InternalProviderImpl;
    if-eqz v12, :cond_5

    move-object v9, v12

    .line 322
    goto :goto_0

    .line 329
    :cond_5
    invoke-static/range {p1 .. p1}, Ltoothpick/registries/FactoryRegistryLocator;->getFactory(Ljava/lang/Class;)Ltoothpick/Factory;

    move-result-object v2

    .line 331
    .local v2, "factory":Ltoothpick/Factory;, "Ltoothpick/Factory<TT;>;"
    invoke-interface {v2}, Ltoothpick/Factory;->hasScopeAnnotation()Z

    move-result v13

    if-eqz v13, :cond_6

    .line 333
    move-object/from16 v0, p0

    invoke-interface {v2, v0}, Ltoothpick/Factory;->getTargetScope(Ltoothpick/Scope;)Ltoothpick/Scope;

    move-result-object v10

    .line 334
    .local v10, "targetScope":Ltoothpick/Scope;
    new-instance v5, Ltoothpick/ScopedProviderImpl;

    const/4 v13, 0x0

    invoke-direct {v5, v10, v2, v13}, Ltoothpick/ScopedProviderImpl;-><init>(Ltoothpick/Scope;Ltoothpick/Factory;Z)V

    .local v5, "newProvider":Ltoothpick/ScopedProviderImpl;, "Ltoothpick/ScopedProviderImpl<+TT;>;"
    move-object v11, v10

    .line 338
    check-cast v11, Ltoothpick/ScopeImpl;

    .line 339
    .local v11, "targetScopeImpl":Ltoothpick/ScopeImpl;
    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-direct {v11, v0, v13, v5, v14}, Ltoothpick/ScopeImpl;->installScopedProvider(Ljava/lang/Class;Ljava/lang/String;Ltoothpick/ScopedProviderImpl;Z)Ltoothpick/InternalProviderImpl;

    move-result-object v9

    goto :goto_0

    .line 342
    .end local v5    # "newProvider":Ltoothpick/ScopedProviderImpl;, "Ltoothpick/ScopedProviderImpl<+TT;>;"
    .end local v10    # "targetScope":Ltoothpick/Scope;
    .end local v11    # "targetScopeImpl":Ltoothpick/ScopeImpl;
    :cond_6
    new-instance v4, Ltoothpick/InternalProviderImpl;

    const/4 v13, 0x0

    invoke-direct {v4, v2, v13}, Ltoothpick/InternalProviderImpl;-><init>(Ltoothpick/Factory;Z)V

    .line 347
    .local v4, "newProvider":Ltoothpick/InternalProviderImpl;, "Ltoothpick/InternalProviderImpl<TT;>;"
    const/4 v13, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13, v4}, Ltoothpick/ScopeImpl;->installUnBoundProvider(Ljava/lang/Class;Ljava/lang/String;Ltoothpick/InternalProviderImpl;)Ltoothpick/InternalProviderImpl;

    move-result-object v9

    goto/16 :goto_0
.end method

.method protected reset()V
    .locals 1

    .prologue
    .line 543
    invoke-super {p0}, Ltoothpick/ScopeNode;->reset()V

    .line 544
    iget-object v0, p0, Ltoothpick/ScopeImpl;->mapClassesToNamedBoundProviders:Ljava/util/IdentityHashMap;

    invoke-virtual {v0}, Ljava/util/IdentityHashMap;->clear()V

    .line 545
    iget-object v0, p0, Ltoothpick/ScopeImpl;->mapClassesToUnNamedBoundProviders:Ljava/util/IdentityHashMap;

    invoke-virtual {v0}, Ljava/util/IdentityHashMap;->clear()V

    .line 546
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltoothpick/ScopeImpl;->hasTestModules:Z

    .line 547
    invoke-direct {p0}, Ltoothpick/ScopeImpl;->installBindingForScope()V

    .line 548
    return-void
.end method

.method toProvider(Ltoothpick/config/Binding;)Ltoothpick/InternalProviderImpl;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ltoothpick/config/Binding",
            "<TT;>;)",
            "Ltoothpick/InternalProviderImpl",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .local p1, "binding":Ltoothpick/config/Binding;, "Ltoothpick/config/Binding<TT;>;"
    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 214
    if-nez p1, :cond_0

    .line 215
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "null binding are not allowed. Should not happen unless getBindingSet is overridden."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 217
    :cond_0
    sget-object v0, Ltoothpick/configuration/ConfigurationHolder;->configuration:Ltoothpick/configuration/Configuration;

    invoke-virtual {v0, p1, p0}, Ltoothpick/configuration/Configuration;->checkIllegalBinding(Ltoothpick/config/Binding;Ltoothpick/Scope;)V

    .line 219
    sget-object v0, Ltoothpick/ScopeImpl$1;->$SwitchMap$toothpick$config$Binding$Mode:[I

    invoke-virtual {p1}, Ltoothpick/config/Binding;->getMode()Ltoothpick/config/Binding$Mode;

    move-result-object v1

    invoke-virtual {v1}, Ltoothpick/config/Binding$Mode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 249
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "mode is not handled: %s. This should not happen."

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Ltoothpick/config/Binding;->getMode()Ltoothpick/config/Binding$Mode;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 222
    :pswitch_0
    invoke-virtual {p1}, Ltoothpick/config/Binding;->getKey()Ljava/lang/Class;

    move-result-object v2

    .line 224
    invoke-virtual {p1}, Ltoothpick/config/Binding;->isCreatingInstancesInScope()Z

    move-result v4

    .line 225
    invoke-virtual {p1}, Ltoothpick/config/Binding;->isCreatingSingletonInScope()Z

    move-result v5

    move-object v0, p0

    move-object v1, p0

    move v6, v3

    .line 221
    invoke-direct/range {v0 .. v6}, Ltoothpick/ScopeImpl;->createInternalProvider(Ltoothpick/Scope;Ljava/lang/Class;ZZZZ)Ltoothpick/InternalProviderImpl;

    move-result-object v0

    .line 241
    :goto_0
    return-object v0

    .line 229
    :pswitch_1
    invoke-virtual {p1}, Ltoothpick/config/Binding;->getImplementationClass()Ljava/lang/Class;

    move-result-object v2

    .line 231
    invoke-virtual {p1}, Ltoothpick/config/Binding;->isCreatingInstancesInScope()Z

    move-result v4

    .line 232
    invoke-virtual {p1}, Ltoothpick/config/Binding;->isCreatingSingletonInScope()Z

    move-result v5

    move-object v0, p0

    move-object v1, p0

    move v6, v3

    .line 228
    invoke-direct/range {v0 .. v6}, Ltoothpick/ScopeImpl;->createInternalProvider(Ltoothpick/Scope;Ljava/lang/Class;ZZZZ)Ltoothpick/InternalProviderImpl;

    move-result-object v0

    goto :goto_0

    .line 235
    :pswitch_2
    new-instance v0, Ltoothpick/InternalProviderImpl;

    invoke-virtual {p1}, Ltoothpick/config/Binding;->getInstance()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ltoothpick/InternalProviderImpl;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    .line 239
    :pswitch_3
    new-instance v0, Ltoothpick/InternalProviderImpl;

    invoke-virtual {p1}, Ltoothpick/config/Binding;->getProviderInstance()Ljavax/inject/Provider;

    move-result-object v1

    invoke-virtual {p1}, Ltoothpick/config/Binding;->isProvidingSingletonInScope()Z

    move-result v2

    invoke-direct {v0, v1, v2}, Ltoothpick/InternalProviderImpl;-><init>(Ljavax/inject/Provider;Z)V

    goto :goto_0

    .line 242
    :pswitch_4
    invoke-virtual {p1}, Ltoothpick/config/Binding;->getProviderClass()Ljava/lang/Class;

    move-result-object v2

    .line 244
    invoke-virtual {p1}, Ltoothpick/config/Binding;->isCreatingInstancesInScope()Z

    move-result v4

    .line 245
    invoke-virtual {p1}, Ltoothpick/config/Binding;->isCreatingSingletonInScope()Z

    move-result v5

    .line 246
    invoke-virtual {p1}, Ltoothpick/config/Binding;->isProvidingSingletonInScope()Z

    move-result v6

    move-object v0, p0

    move-object v1, p0

    move v3, v7

    .line 241
    invoke-direct/range {v0 .. v6}, Ltoothpick/ScopeImpl;->createInternalProvider(Ltoothpick/Scope;Ljava/lang/Class;ZZZZ)Ltoothpick/InternalProviderImpl;

    move-result-object v0

    goto :goto_0

    .line 219
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 19

    .prologue
    .line 103
    const-string v3, "---"

    .line 104
    .local v3, "branch":Ljava/lang/String;
    const/16 v11, 0x5c

    .line 105
    .local v11, "lastNode":C
    const/16 v12, 0x2b

    .line 106
    .local v12, "node":C
    const-string v8, "    "

    .line 108
    .local v8, "indent":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 109
    .local v4, "builder":Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v0, v0, Ltoothpick/ScopeImpl;->name:Ljava/lang/Object;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 110
    const/16 v17, 0x3a

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 111
    invoke-static/range {p0 .. p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 112
    sget-object v17, Ltoothpick/ScopeImpl;->LINE_SEPARATOR:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    const-string v17, "Providers: ["

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    move-object/from16 v0, p0

    iget-object v0, v0, Ltoothpick/ScopeImpl;->mapClassesToNamedBoundProviders:Ljava/util/IdentityHashMap;

    move-object/from16 v18, v0

    monitor-enter v18

    .line 117
    :try_start_0
    new-instance v14, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v0, v0, Ltoothpick/ScopeImpl;->mapClassesToNamedBoundProviders:Ljava/util/IdentityHashMap;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/IdentityHashMap;->keySet()Ljava/util/Set;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v14, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 118
    .local v14, "sortedBoundProviderClassesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Class;>;"
    monitor-exit v18
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    move-object/from16 v0, p0

    iget-object v0, v0, Ltoothpick/ScopeImpl;->mapClassesToUnNamedBoundProviders:Ljava/util/IdentityHashMap;

    move-object/from16 v18, v0

    monitor-enter v18

    .line 120
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Ltoothpick/ScopeImpl;->mapClassesToUnNamedBoundProviders:Ljava/util/IdentityHashMap;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/IdentityHashMap;->keySet()Ljava/util/Set;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 121
    monitor-exit v18
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 122
    new-instance v17, Ltoothpick/ScopeImpl$ClassNameComparator;

    const/16 v18, 0x0

    invoke-direct/range {v17 .. v18}, Ltoothpick/ScopeImpl$ClassNameComparator;-><init>(Ltoothpick/ScopeImpl$1;)V

    move-object/from16 v0, v17

    invoke-static {v14, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 123
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_0
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_0

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;

    .line 124
    .local v2, "aClass":Ljava/lang/Class;
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    const/16 v18, 0x2c

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 118
    .end local v2    # "aClass":Ljava/lang/Class;
    .end local v14    # "sortedBoundProviderClassesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Class;>;"
    :catchall_0
    move-exception v17

    :try_start_2
    monitor-exit v18
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v17

    .line 121
    .restart local v14    # "sortedBoundProviderClassesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Class;>;"
    :catchall_1
    move-exception v17

    :try_start_3
    monitor-exit v18
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v17

    .line 128
    :cond_0
    invoke-virtual {v14}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v17

    if-nez v17, :cond_1

    .line 129
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v17

    add-int/lit8 v17, v17, -0x1

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 132
    :cond_1
    const/16 v17, 0x5d

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 133
    sget-object v17, Ltoothpick/ScopeImpl;->LINE_SEPARATOR:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    move-object/from16 v0, p0

    iget-object v0, v0, Ltoothpick/ScopeImpl;->childrenScopes:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .line 136
    .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ltoothpick/ScopeNode;>;"
    :cond_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_6

    .line 137
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ltoothpick/Scope;

    .line 138
    .local v13, "scope":Ltoothpick/Scope;
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-nez v17, :cond_4

    const/4 v9, 0x1

    .line 139
    .local v9, "isLast":Z
    :goto_1
    if-eqz v9, :cond_5

    const/16 v17, 0x5c

    :goto_2
    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 140
    const-string v17, "---"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 142
    .local v6, "childString":Ljava/lang/String;
    sget-object v17, Ltoothpick/ScopeImpl;->LINE_SEPARATOR:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    .line 143
    .local v16, "split":[Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_3
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v7, v0, :cond_2

    .line 144
    aget-object v5, v16, v7

    .line 145
    .local v5, "childLine":Ljava/lang/String;
    if-eqz v7, :cond_3

    .line 146
    const-string v17, "    "

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    :cond_3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    sget-object v17, Ltoothpick/ScopeImpl;->LINE_SEPARATOR:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 138
    .end local v5    # "childLine":Ljava/lang/String;
    .end local v6    # "childString":Ljava/lang/String;
    .end local v7    # "i":I
    .end local v9    # "isLast":Z
    .end local v16    # "split":[Ljava/lang/String;
    :cond_4
    const/4 v9, 0x0

    goto :goto_1

    .line 139
    .restart local v9    # "isLast":Z
    :cond_5
    const/16 v17, 0x2b

    goto :goto_2

    .line 153
    .end local v9    # "isLast":Z
    .end local v13    # "scope":Ltoothpick/Scope;
    :cond_6
    invoke-virtual/range {p0 .. p0}, Ltoothpick/ScopeImpl;->getRootScope()Ltoothpick/ScopeNode;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    if-ne v0, v1, :cond_9

    .line 154
    const-string v17, "Unbound providers: ["

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    sget-object v18, Ltoothpick/ScopeImpl;->mapClassesToUnNamedUnBoundProviders:Ljava/util/IdentityHashMap;

    monitor-enter v18

    .line 157
    :try_start_4
    new-instance v15, Ljava/util/ArrayList;

    sget-object v17, Ltoothpick/ScopeImpl;->mapClassesToUnNamedUnBoundProviders:Ljava/util/IdentityHashMap;

    invoke-virtual/range {v17 .. v17}, Ljava/util/IdentityHashMap;->keySet()Ljava/util/Set;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v15, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 158
    .local v15, "sortedUnboundProviderClassesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Class;>;"
    monitor-exit v18
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 159
    new-instance v17, Ltoothpick/ScopeImpl$ClassNameComparator;

    const/16 v18, 0x0

    invoke-direct/range {v17 .. v18}, Ltoothpick/ScopeImpl$ClassNameComparator;-><init>(Ltoothpick/ScopeImpl$1;)V

    move-object/from16 v0, v17

    invoke-static {v15, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 161
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_4
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_7

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;

    .line 162
    .restart local v2    # "aClass":Ljava/lang/Class;
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    const/16 v18, 0x2c

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 158
    .end local v2    # "aClass":Ljava/lang/Class;
    .end local v15    # "sortedUnboundProviderClassesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Class;>;"
    :catchall_2
    move-exception v17

    :try_start_5
    monitor-exit v18
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v17

    .line 165
    .restart local v15    # "sortedUnboundProviderClassesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Class;>;"
    :cond_7
    invoke-virtual {v15}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v17

    if-nez v17, :cond_8

    .line 166
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v17

    add-int/lit8 v17, v17, -0x1

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 168
    :cond_8
    const/16 v17, 0x5d

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 169
    sget-object v17, Ltoothpick/ScopeImpl;->LINE_SEPARATOR:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    .end local v15    # "sortedUnboundProviderClassesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Class;>;"
    :cond_9
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    return-object v17
.end method
