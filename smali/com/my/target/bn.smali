.class public final Lcom/my/target/bn;
.super Lcom/my/target/bm;
.source "FingerprintDataProvider.java"


# static fields
.field private static final hF:Lcom/my/target/bn;


# instance fields
.field private final hG:Lcom/my/target/bk;

.field private final hH:Lcom/my/target/bl;

.field private final hI:Lcom/my/target/bo;

.field private final hJ:Lcom/my/target/bp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/my/target/bn;

    invoke-direct {v0}, Lcom/my/target/bn;-><init>()V

    sput-object v0, Lcom/my/target/bn;->hF:Lcom/my/target/bn;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/my/target/bm;-><init>()V

    .line 39
    new-instance v0, Lcom/my/target/bk;

    invoke-direct {v0}, Lcom/my/target/bk;-><init>()V

    iput-object v0, p0, Lcom/my/target/bn;->hG:Lcom/my/target/bk;

    .line 40
    new-instance v0, Lcom/my/target/bl;

    invoke-direct {v0}, Lcom/my/target/bl;-><init>()V

    iput-object v0, p0, Lcom/my/target/bn;->hH:Lcom/my/target/bl;

    .line 41
    new-instance v0, Lcom/my/target/bo;

    invoke-direct {v0}, Lcom/my/target/bo;-><init>()V

    iput-object v0, p0, Lcom/my/target/bn;->hI:Lcom/my/target/bo;

    .line 42
    new-instance v0, Lcom/my/target/bp;

    invoke-direct {v0}, Lcom/my/target/bp;-><init>()V

    iput-object v0, p0, Lcom/my/target/bn;->hJ:Lcom/my/target/bp;

    .line 69
    return-void
.end method

.method public static aN()Lcom/my/target/bn;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/my/target/bn;->hF:Lcom/my/target/bn;

    return-object v0
.end method


# virtual methods
.method public aO()Lcom/my/target/bk;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/my/target/bn;->hG:Lcom/my/target/bk;

    return-object v0
.end method

.method public aP()Lcom/my/target/bl;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/my/target/bn;->hH:Lcom/my/target/bl;

    return-object v0
.end method

.method public aQ()Lcom/my/target/bo;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/my/target/bn;->hI:Lcom/my/target/bo;

    return-object v0
.end method

.method public aR()Lcom/my/target/bp;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/my/target/bn;->hJ:Lcom/my/target/bp;

    return-object v0
.end method

.method public declared-synchronized collectData(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 75
    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 77
    const-string v0, "FingerprintDataProvider: You must not call collectData method from main thread"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    :goto_0
    monitor-exit p0

    return-void

    .line 81
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/my/target/bn;->removeAll()V

    .line 83
    iget-object v0, p0, Lcom/my/target/bn;->hG:Lcom/my/target/bk;

    invoke-virtual {v0, p1}, Lcom/my/target/bk;->collectData(Landroid/content/Context;)V

    .line 84
    iget-object v0, p0, Lcom/my/target/bn;->hH:Lcom/my/target/bl;

    invoke-virtual {v0, p1}, Lcom/my/target/bl;->collectData(Landroid/content/Context;)V

    .line 85
    iget-object v0, p0, Lcom/my/target/bn;->hI:Lcom/my/target/bo;

    invoke-virtual {v0, p1}, Lcom/my/target/bo;->collectData(Landroid/content/Context;)V

    .line 86
    iget-object v0, p0, Lcom/my/target/bn;->hJ:Lcom/my/target/bp;

    invoke-virtual {v0, p1}, Lcom/my/target/bp;->collectData(Landroid/content/Context;)V

    .line 88
    invoke-virtual {p0}, Lcom/my/target/bn;->getMap()Ljava/util/Map;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/my/target/bn;->hG:Lcom/my/target/bk;

    invoke-virtual {v1, v0}, Lcom/my/target/bk;->putDataTo(Ljava/util/Map;)V

    .line 91
    iget-object v1, p0, Lcom/my/target/bn;->hH:Lcom/my/target/bl;

    invoke-virtual {v1, v0}, Lcom/my/target/bl;->putDataTo(Ljava/util/Map;)V

    .line 92
    iget-object v1, p0, Lcom/my/target/bn;->hI:Lcom/my/target/bo;

    invoke-virtual {v1, v0}, Lcom/my/target/bo;->putDataTo(Ljava/util/Map;)V

    .line 93
    iget-object v1, p0, Lcom/my/target/bn;->hJ:Lcom/my/target/bp;

    invoke-virtual {v1, v0}, Lcom/my/target/bp;->putDataTo(Ljava/util/Map;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
