.class Lru/cn/tv/mobile/telecasts/TelecastsViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "TelecastsViewModel.java"


# instance fields
.field private final loader:Lru/cn/mvvm/RxLoader;

.field private final telecasts:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private uri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lru/cn/mvvm/RxLoader;)V
    .locals 1
    .param p1, "loader"    # Lru/cn/mvvm/RxLoader;

    .prologue
    .line 25
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 26
    iput-object p1, p0, Lru/cn/tv/mobile/telecasts/TelecastsViewModel;->loader:Lru/cn/mvvm/RxLoader;

    .line 28
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/mobile/telecasts/TelecastsViewModel;->telecasts:Landroid/arch/lifecycle/MutableLiveData;

    .line 29
    return-void
.end method

.method private load(Z)V
    .locals 4
    .param p1, "more"    # Z

    .prologue
    .line 51
    invoke-virtual {p0}, Lru/cn/tv/mobile/telecasts/TelecastsViewModel;->unbindAll()V

    .line 53
    const/4 v1, 0x0

    .line 54
    .local v1, "selection":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 55
    const-string v1, "more"

    .line 58
    :cond_0
    iget-object v2, p0, Lru/cn/tv/mobile/telecasts/TelecastsViewModel;->loader:Lru/cn/mvvm/RxLoader;

    iget-object v3, p0, Lru/cn/tv/mobile/telecasts/TelecastsViewModel;->uri:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v2

    .line 59
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    .line 60
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Lru/cn/tv/mobile/telecasts/TelecastsViewModel$$Lambda$0;

    invoke-direct {v3, p0}, Lru/cn/tv/mobile/telecasts/TelecastsViewModel$$Lambda$0;-><init>(Lru/cn/tv/mobile/telecasts/TelecastsViewModel;)V

    .line 61
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 63
    .local v0, "disposable":Lio/reactivex/disposables/Disposable;
    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/telecasts/TelecastsViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 64
    return-void
.end method


# virtual methods
.method final synthetic lambda$load$0$TelecastsViewModel(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "it"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lru/cn/tv/mobile/telecasts/TelecastsViewModel;->telecasts:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v0, p1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method public prefetch()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lru/cn/tv/mobile/telecasts/TelecastsViewModel;->load(Z)V

    .line 48
    return-void
.end method

.method public setUri(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 36
    invoke-virtual {p0}, Lru/cn/tv/mobile/telecasts/TelecastsViewModel;->unbindAll()V

    .line 38
    iput-object p1, p0, Lru/cn/tv/mobile/telecasts/TelecastsViewModel;->uri:Landroid/net/Uri;

    .line 39
    if-nez p1, :cond_0

    .line 44
    :goto_0
    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lru/cn/tv/mobile/telecasts/TelecastsViewModel;->load(Z)V

    goto :goto_0
.end method

.method public telecasts()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lru/cn/tv/mobile/telecasts/TelecastsViewModel;->telecasts:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method
