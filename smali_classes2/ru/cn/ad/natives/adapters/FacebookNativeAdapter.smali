.class public Lru/cn/ad/natives/adapters/FacebookNativeAdapter;
.super Lru/cn/ad/natives/adapters/NativeAdAdapter;
.source "FacebookNativeAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/ad/natives/adapters/FacebookNativeAdapter$FacebookBinder;
    }
.end annotation


# instance fields
.field private final placementId:Ljava/lang/String;

.field private viewBinder:Lru/cn/ad/natives/adapters/FacebookNativeAdapter$FacebookBinder;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "system"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lru/cn/ad/natives/adapters/NativeAdAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    .line 29
    const-string v0, "placement_id"

    invoke-virtual {p2, v0}, Lru/cn/api/money_miner/replies/AdSystem;->getParamOrThrow(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->placementId:Ljava/lang/String;

    .line 30
    return-void
.end method

.method static synthetic access$000(Lru/cn/ad/natives/adapters/FacebookNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/FacebookNativeAdapter;

    .prologue
    .line 21
    iget-object v0, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/ad/natives/adapters/FacebookNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/FacebookNativeAdapter;

    .prologue
    .line 21
    iget-object v0, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/ad/natives/adapters/FacebookNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/FacebookNativeAdapter;

    .prologue
    .line 21
    iget-object v0, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/ad/natives/adapters/FacebookNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/FacebookNativeAdapter;

    .prologue
    .line 21
    iget-object v0, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$400(Lru/cn/ad/natives/adapters/FacebookNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/FacebookNativeAdapter;

    .prologue
    .line 21
    iget-object v0, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$500(Lru/cn/ad/natives/adapters/FacebookNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/FacebookNativeAdapter;

    .prologue
    .line 21
    iget-object v0, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$600(Lru/cn/ad/natives/adapters/FacebookNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/FacebookNativeAdapter;

    .prologue
    .line 21
    iget-object v0, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 90
    invoke-virtual {p0}, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->getPresenter()Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    move-result-object v0

    .line 91
    .local v0, "presenter":Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;
    if-eqz v0, :cond_0

    .line 92
    invoke-interface {v0}, Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;->destroy()V

    .line 95
    :cond_0
    iget-object v1, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->viewBinder:Lru/cn/ad/natives/adapters/FacebookNativeAdapter$FacebookBinder;

    if-eqz v1, :cond_1

    .line 96
    iget-object v1, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->viewBinder:Lru/cn/ad/natives/adapters/FacebookNativeAdapter$FacebookBinder;

    invoke-virtual {v1}, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$FacebookBinder;->destroy()V

    .line 97
    const/4 v1, 0x0

    iput-object v1, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->viewBinder:Lru/cn/ad/natives/adapters/FacebookNativeAdapter$FacebookBinder;

    .line 99
    :cond_1
    return-void
.end method

.method protected onLoad()V
    .locals 4

    .prologue
    .line 34
    new-instance v1, Lcom/facebook/ads/NativeAd;

    iget-object v2, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->context:Landroid/content/Context;

    iget-object v3, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->placementId:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/facebook/ads/NativeAd;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 36
    .local v1, "nativeAd":Lcom/facebook/ads/NativeAd;
    new-instance v2, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$1;

    invoke-direct {v2, p0}, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$1;-><init>(Lru/cn/ad/natives/adapters/FacebookNativeAdapter;)V

    invoke-virtual {v1, v2}, Lcom/facebook/ads/NativeAd;->setAdListener(Lcom/facebook/ads/AdListener;)V

    .line 63
    new-instance v2, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$FacebookBinder;

    invoke-direct {v2, v1}, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$FacebookBinder;-><init>(Lcom/facebook/ads/NativeAd;)V

    iput-object v2, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->viewBinder:Lru/cn/ad/natives/adapters/FacebookNativeAdapter$FacebookBinder;

    .line 65
    sget-object v2, Lcom/facebook/ads/NativeAd$MediaCacheFlag;->ICON:Lcom/facebook/ads/NativeAd$MediaCacheFlag;

    sget-object v3, Lcom/facebook/ads/NativeAd$MediaCacheFlag;->IMAGE:Lcom/facebook/ads/NativeAd$MediaCacheFlag;

    invoke-static {v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    .line 68
    .local v0, "loadFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/facebook/ads/NativeAd$MediaCacheFlag;>;"
    invoke-virtual {v1, v0}, Lcom/facebook/ads/NativeAd;->loadAd(Ljava/util/EnumSet;)V

    .line 69
    return-void
.end method

.method public show()V
    .locals 3

    .prologue
    .line 73
    invoke-virtual {p0}, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->getPresenter()Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    move-result-object v0

    .line 74
    .local v0, "presenter":Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;
    iget-object v1, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->viewBinder:Lru/cn/ad/natives/adapters/FacebookNativeAdapter$FacebookBinder;

    new-instance v2, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$2;

    invoke-direct {v2, p0}, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$2;-><init>(Lru/cn/ad/natives/adapters/FacebookNativeAdapter;)V

    invoke-interface {v0, v1, v2}, Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;->show(Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;Ljava/lang/Runnable;)V

    .line 81
    iget-object v1, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    if-eqz v1, :cond_0

    .line 82
    iget-object v1, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    invoke-interface {v1}, Lru/cn/ad/AdAdapter$Listener;->onAdStarted()V

    .line 85
    :cond_0
    sget-object v1, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_START:Lru/cn/domain/statistics/inetra/AdvEvent;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;->reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/util/Map;)V

    .line 86
    return-void
.end method
