.class public interface abstract Lru/inetra/bytefog/service/IService;
.super Ljava/lang/Object;
.source "IService.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/inetra/bytefog/service/IService$Stub;
    }
.end annotation


# virtual methods
.method public abstract cancelAllRequests()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract convertBytefogMagnetToHlsPlaylistUri(Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract setListener(Lru/inetra/bytefog/service/IServiceListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
