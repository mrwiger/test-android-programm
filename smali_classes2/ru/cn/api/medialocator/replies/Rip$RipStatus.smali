.class public final enum Lru/cn/api/medialocator/replies/Rip$RipStatus;
.super Ljava/lang/Enum;
.source "Rip.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/api/medialocator/replies/Rip;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RipStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/api/medialocator/replies/Rip$RipStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/api/medialocator/replies/Rip$RipStatus;

.field public static final enum ACCESS_ALLOWED:Lru/cn/api/medialocator/replies/Rip$RipStatus;

.field public static final enum ACCESS_DENIED:Lru/cn/api/medialocator/replies/Rip$RipStatus;

.field public static final enum ACCESS_PURCHASED:Lru/cn/api/medialocator/replies/Rip$RipStatus;

.field public static final enum PAYMENT_REQUIRED:Lru/cn/api/medialocator/replies/Rip$RipStatus;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 16
    new-instance v0, Lru/cn/api/medialocator/replies/Rip$RipStatus;

    const-string v1, "ACCESS_ALLOWED"

    const/16 v2, 0xc8

    invoke-direct {v0, v1, v3, v2}, Lru/cn/api/medialocator/replies/Rip$RipStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/medialocator/replies/Rip$RipStatus;->ACCESS_ALLOWED:Lru/cn/api/medialocator/replies/Rip$RipStatus;

    new-instance v0, Lru/cn/api/medialocator/replies/Rip$RipStatus;

    const-string v1, "PAYMENT_REQUIRED"

    const/16 v2, 0x192

    invoke-direct {v0, v1, v4, v2}, Lru/cn/api/medialocator/replies/Rip$RipStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/medialocator/replies/Rip$RipStatus;->PAYMENT_REQUIRED:Lru/cn/api/medialocator/replies/Rip$RipStatus;

    new-instance v0, Lru/cn/api/medialocator/replies/Rip$RipStatus;

    const-string v1, "ACCESS_DENIED"

    const/16 v2, 0x193

    invoke-direct {v0, v1, v5, v2}, Lru/cn/api/medialocator/replies/Rip$RipStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/medialocator/replies/Rip$RipStatus;->ACCESS_DENIED:Lru/cn/api/medialocator/replies/Rip$RipStatus;

    new-instance v0, Lru/cn/api/medialocator/replies/Rip$RipStatus;

    const-string v1, "ACCESS_PURCHASED"

    const/16 v2, 0xdd

    invoke-direct {v0, v1, v6, v2}, Lru/cn/api/medialocator/replies/Rip$RipStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/medialocator/replies/Rip$RipStatus;->ACCESS_PURCHASED:Lru/cn/api/medialocator/replies/Rip$RipStatus;

    .line 15
    const/4 v0, 0x4

    new-array v0, v0, [Lru/cn/api/medialocator/replies/Rip$RipStatus;

    sget-object v1, Lru/cn/api/medialocator/replies/Rip$RipStatus;->ACCESS_ALLOWED:Lru/cn/api/medialocator/replies/Rip$RipStatus;

    aput-object v1, v0, v3

    sget-object v1, Lru/cn/api/medialocator/replies/Rip$RipStatus;->PAYMENT_REQUIRED:Lru/cn/api/medialocator/replies/Rip$RipStatus;

    aput-object v1, v0, v4

    sget-object v1, Lru/cn/api/medialocator/replies/Rip$RipStatus;->ACCESS_DENIED:Lru/cn/api/medialocator/replies/Rip$RipStatus;

    aput-object v1, v0, v5

    sget-object v1, Lru/cn/api/medialocator/replies/Rip$RipStatus;->ACCESS_PURCHASED:Lru/cn/api/medialocator/replies/Rip$RipStatus;

    aput-object v1, v0, v6

    sput-object v0, Lru/cn/api/medialocator/replies/Rip$RipStatus;->$VALUES:[Lru/cn/api/medialocator/replies/Rip$RipStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 21
    iput p3, p0, Lru/cn/api/medialocator/replies/Rip$RipStatus;->value:I

    .line 22
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/api/medialocator/replies/Rip$RipStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 15
    const-class v0, Lru/cn/api/medialocator/replies/Rip$RipStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/api/medialocator/replies/Rip$RipStatus;

    return-object v0
.end method

.method public static values()[Lru/cn/api/medialocator/replies/Rip$RipStatus;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lru/cn/api/medialocator/replies/Rip$RipStatus;->$VALUES:[Lru/cn/api/medialocator/replies/Rip$RipStatus;

    invoke-virtual {v0}, [Lru/cn/api/medialocator/replies/Rip$RipStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/api/medialocator/replies/Rip$RipStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lru/cn/api/medialocator/replies/Rip$RipStatus;->value:I

    return v0
.end method
