.class Lru/cn/player/androidplayer/AndroidMediaPlayer$5;
.super Ljava/lang/Object;
.source "AndroidMediaPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/player/androidplayer/AndroidMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;


# direct methods
.method constructor <init>(Lru/cn/player/androidplayer/AndroidMediaPlayer;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/player/androidplayer/AndroidMediaPlayer;

    .prologue
    .line 118
    iput-object p1, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$5;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 5
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 121
    iget-object v2, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$5;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    invoke-static {v2}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->access$1100(Lru/cn/player/androidplayer/AndroidMediaPlayer;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Info ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    const/16 v2, 0x2bd

    if-ne p2, v2, :cond_0

    .line 125
    iget-object v1, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$5;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    invoke-static {v1, v0}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->access$1200(Lru/cn/player/androidplayer/AndroidMediaPlayer;Z)V

    .line 134
    :goto_0
    return v0

    .line 128
    :cond_0
    const/16 v2, 0x2be

    if-ne p2, v2, :cond_1

    .line 129
    iget-object v2, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$5;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    invoke-static {v2, v1}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->access$1300(Lru/cn/player/androidplayer/AndroidMediaPlayer;Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 134
    goto :goto_0
.end method
