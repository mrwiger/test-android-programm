.class Lcom/samsung/multiscreen/StandbyDeviceList$3;
.super Ljava/lang/Object;
.source "StandbyDeviceList.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/multiscreen/StandbyDeviceList;->clear()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/multiscreen/StandbyDeviceList;


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/StandbyDeviceList;)V
    .locals 0
    .param p1, "this$0"    # Lcom/samsung/multiscreen/StandbyDeviceList;

    .prologue
    .line 325
    iput-object p1, p0, Lcom/samsung/multiscreen/StandbyDeviceList$3;->this$0:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 328
    iget-object v3, p0, Lcom/samsung/multiscreen/StandbyDeviceList$3;->this$0:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-static {v3}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$900(Lcom/samsung/multiscreen/StandbyDeviceList;)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/multiscreen/StandbyDeviceList$3;->this$0:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-static {v3}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$900(Lcom/samsung/multiscreen/StandbyDeviceList;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 329
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/multiscreen/StandbyDeviceList$3;->this$0:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-static {v3}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$900(Lcom/samsung/multiscreen/StandbyDeviceList;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 330
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 332
    .local v2, "record":Lorg/json/JSONObject;
    :try_start_0
    invoke-static {}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$1100()Ljava/lang/String;

    move-result-object v4

    iget-object v3, p0, Lcom/samsung/multiscreen/StandbyDeviceList$3;->this$0:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-static {v3}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$900(Lcom/samsung/multiscreen/StandbyDeviceList;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;

    iget-object v3, v3, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->duid:Ljava/lang/String;

    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 333
    invoke-static {}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$1200()Ljava/lang/String;

    move-result-object v4

    iget-object v3, p0, Lcom/samsung/multiscreen/StandbyDeviceList$3;->this$0:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-static {v3}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$900(Lcom/samsung/multiscreen/StandbyDeviceList;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;

    iget-object v3, v3, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->bssid:Ljava/lang/String;

    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 334
    invoke-static {}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$1300()Ljava/lang/String;

    move-result-object v4

    iget-object v3, p0, Lcom/samsung/multiscreen/StandbyDeviceList$3;->this$0:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-static {v3}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$900(Lcom/samsung/multiscreen/StandbyDeviceList;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;

    iget-object v3, v3, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->mac:Ljava/lang/String;

    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 335
    invoke-static {}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$1400()Ljava/lang/String;

    move-result-object v4

    iget-object v3, p0, Lcom/samsung/multiscreen/StandbyDeviceList$3;->this$0:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-static {v3}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$900(Lcom/samsung/multiscreen/StandbyDeviceList;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;

    iget-object v3, v3, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->ip:Ljava/lang/String;

    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 336
    invoke-static {}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$1500()Ljava/lang/String;

    move-result-object v4

    iget-object v3, p0, Lcom/samsung/multiscreen/StandbyDeviceList$3;->this$0:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-static {v3}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$900(Lcom/samsung/multiscreen/StandbyDeviceList;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;

    iget-object v3, v3, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->name:Ljava/lang/String;

    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 341
    iget-object v3, p0, Lcom/samsung/multiscreen/StandbyDeviceList$3;->this$0:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-static {v3}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$000(Lcom/samsung/multiscreen/StandbyDeviceList;)Lcom/samsung/multiscreen/Search$SearchListener;

    move-result-object v3

    invoke-static {v2}, Lcom/samsung/multiscreen/Service;->create(Lorg/json/JSONObject;)Lcom/samsung/multiscreen/Service;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/samsung/multiscreen/Search$SearchListener;->onLost(Lcom/samsung/multiscreen/Service;)V

    .line 342
    iget-object v3, p0, Lcom/samsung/multiscreen/StandbyDeviceList$3;->this$0:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-static {v3}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$900(Lcom/samsung/multiscreen/StandbyDeviceList;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 329
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 337
    :catch_0
    move-exception v0

    .line 338
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$1600()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "clear() Unsuccessful: error : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "i":I
    .end local v2    # "record":Lorg/json/JSONObject;
    :cond_0
    :goto_1
    return-void

    .line 344
    .restart local v1    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/samsung/multiscreen/StandbyDeviceList$3;->this$0:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-static {v3}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$1000(Lcom/samsung/multiscreen/StandbyDeviceList;)V

    goto :goto_1
.end method
