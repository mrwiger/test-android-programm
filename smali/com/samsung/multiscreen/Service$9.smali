.class Lcom/samsung/multiscreen/Service$9;
.super Ljava/lang/Object;
.source "Service.java"

# interfaces
.implements Lcom/samsung/multiscreen/Result;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/multiscreen/Service;->isDMPSupported(Lcom/samsung/multiscreen/Result;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/samsung/multiscreen/Result",
        "<",
        "Lcom/samsung/multiscreen/Device;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/multiscreen/Service;

.field final synthetic val$result:Lcom/samsung/multiscreen/Result;


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/Service;Lcom/samsung/multiscreen/Result;)V
    .locals 0
    .param p1, "this$0"    # Lcom/samsung/multiscreen/Service;

    .prologue
    .line 763
    iput-object p1, p0, Lcom/samsung/multiscreen/Service$9;->this$0:Lcom/samsung/multiscreen/Service;

    iput-object p2, p0, Lcom/samsung/multiscreen/Service$9;->val$result:Lcom/samsung/multiscreen/Result;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/samsung/multiscreen/Error;)V
    .locals 1
    .param p1, "error"    # Lcom/samsung/multiscreen/Error;

    .prologue
    .line 780
    iget-object v0, p0, Lcom/samsung/multiscreen/Service$9;->val$result:Lcom/samsung/multiscreen/Result;

    invoke-interface {v0, p1}, Lcom/samsung/multiscreen/Result;->onError(Lcom/samsung/multiscreen/Error;)V

    .line 781
    return-void
.end method

.method public onSuccess(Lcom/samsung/multiscreen/Device;)V
    .locals 6
    .param p1, "device"    # Lcom/samsung/multiscreen/Device;

    .prologue
    const/4 v3, 0x0

    .line 766
    if-eqz p1, :cond_1

    .line 767
    invoke-virtual {p1}, Lcom/samsung/multiscreen/Device;->getModel()Ljava/lang/String;

    move-result-object v1

    .line 768
    .local v1, "model":Ljava/lang/String;
    const/4 v0, 0x0

    .line 770
    .local v0, "TVYear":I
    const/4 v4, 0x0

    const/4 v5, 0x2

    :try_start_0
    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 775
    :goto_0
    iget-object v4, p0, Lcom/samsung/multiscreen/Service$9;->val$result:Lcom/samsung/multiscreen/Result;

    const/16 v5, 0xf

    if-ne v0, v5, :cond_0

    const/4 v3, 0x1

    :cond_0
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v4, v3}, Lcom/samsung/multiscreen/Result;->onSuccess(Ljava/lang/Object;)V

    .line 777
    .end local v0    # "TVYear":I
    .end local v1    # "model":Ljava/lang/String;
    :cond_1
    return-void

    .line 771
    .restart local v0    # "TVYear":I
    .restart local v1    # "model":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 772
    .local v2, "var5":Ljava/lang/NumberFormatException;
    iget-object v4, p0, Lcom/samsung/multiscreen/Service$9;->val$result:Lcom/samsung/multiscreen/Result;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/samsung/multiscreen/Result;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 763
    check-cast p1, Lcom/samsung/multiscreen/Device;

    invoke-virtual {p0, p1}, Lcom/samsung/multiscreen/Service$9;->onSuccess(Lcom/samsung/multiscreen/Device;)V

    return-void
.end method
