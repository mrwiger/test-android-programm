.class public final Lcom/my/target/core/controllers/d;
.super Ljava/lang/Object;
.source "InstreamAdVideoController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/core/controllers/d$c;,
        Lcom/my/target/core/controllers/d$b;,
        Lcom/my/target/core/controllers/d$a;,
        Lcom/my/target/core/controllers/d$d;
    }
.end annotation


# instance fields
.field private final e:Lcom/my/target/core/controllers/d$a;

.field private final f:Lcom/my/target/ck;

.field private final g:Lcom/my/target/ck;

.field private final h:Lcom/my/target/core/controllers/d$c;

.field private final i:Lcom/my/target/core/controllers/d$b;

.field private final j:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/my/target/ao;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/my/target/ap;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcom/my/target/core/controllers/d$d;

.field private m:Lcom/my/target/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/my/target/aj",
            "<",
            "Lcom/my/target/common/models/VideoData;",
            ">;"
        }
    .end annotation
.end field

.field private n:I

.field private o:F

.field private p:I

.field private player:Lcom/my/target/instreamads/InstreamAdPlayer;

.field private q:Z

.field private r:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/an;",
            ">;"
        }
    .end annotation
.end field

.field private s:I

.field private volume:F


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/my/target/core/controllers/d;->volume:F

    .line 63
    iput v1, p0, Lcom/my/target/core/controllers/d;->s:I

    .line 73
    new-instance v0, Lcom/my/target/core/controllers/d$a;

    invoke-direct {v0, p0, v1}, Lcom/my/target/core/controllers/d$a;-><init>(Lcom/my/target/core/controllers/d;B)V

    iput-object v0, p0, Lcom/my/target/core/controllers/d;->e:Lcom/my/target/core/controllers/d$a;

    .line 74
    const/16 v0, 0xc8

    invoke-static {v0}, Lcom/my/target/ck;->k(I)Lcom/my/target/ck;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/controllers/d;->f:Lcom/my/target/ck;

    .line 75
    sget-object v0, Lcom/my/target/ck;->km:Lcom/my/target/ck;

    iput-object v0, p0, Lcom/my/target/core/controllers/d;->g:Lcom/my/target/ck;

    .line 76
    new-instance v0, Lcom/my/target/core/controllers/d$b;

    invoke-direct {v0, p0, v1}, Lcom/my/target/core/controllers/d$b;-><init>(Lcom/my/target/core/controllers/d;B)V

    iput-object v0, p0, Lcom/my/target/core/controllers/d;->i:Lcom/my/target/core/controllers/d$b;

    .line 77
    new-instance v0, Lcom/my/target/core/controllers/d$c;

    invoke-direct {v0, p0, v1}, Lcom/my/target/core/controllers/d$c;-><init>(Lcom/my/target/core/controllers/d;B)V

    iput-object v0, p0, Lcom/my/target/core/controllers/d;->h:Lcom/my/target/core/controllers/d$c;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/core/controllers/d;->r:Ljava/util/ArrayList;

    .line 79
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/my/target/core/controllers/d;->j:Ljava/util/Stack;

    .line 80
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/my/target/core/controllers/d;->k:Ljava/util/Stack;

    .line 81
    return-void
.end method

.method static synthetic a(Lcom/my/target/core/controllers/d;I)I
    .locals 0

    .prologue
    .line 34
    iput p1, p0, Lcom/my/target/core/controllers/d;->s:I

    return p1
.end method

.method private a(F)V
    .locals 2

    .prologue
    .line 427
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 428
    :goto_0
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->k:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/controllers/d;->k:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/ap;

    invoke-virtual {v0}, Lcom/my/target/ap;->Z()F

    move-result v0

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_0

    .line 430
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->k:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 432
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    if-eqz v0, :cond_1

    .line 434
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAdPlayer;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 436
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/my/target/core/controllers/d;DF)V
    .locals 7

    .prologue
    const v6, 0x7f7fffff    # Float.MAX_VALUE

    .line 34
    .line 4228
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 4229
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->j:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/my/target/core/controllers/d;->j:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/ao;

    invoke-virtual {v0}, Lcom/my/target/ao;->Z()F

    move-result v0

    cmpg-float v0, v0, p3

    if-gtz v0, :cond_3

    .line 4231
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->j:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/ao;

    .line 4232
    invoke-virtual {v0}, Lcom/my/target/ao;->ag()I

    move-result v2

    .line 4233
    invoke-virtual {v0}, Lcom/my/target/ao;->Y()Z

    move-result v3

    .line 4234
    int-to-double v4, v2

    cmpg-double v4, v4, p1

    if-gtz v4, :cond_1

    if-nez v3, :cond_2

    :cond_1
    int-to-double v4, v2

    cmpl-double v2, v4, p1

    if-lez v2, :cond_0

    if-nez v3, :cond_0

    .line 4236
    :cond_2
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 4240
    :cond_3
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 4242
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/an;

    .line 4243
    invoke-virtual {v0}, Lcom/my/target/an;->ag()I

    move-result v3

    .line 4244
    int-to-double v4, v3

    cmpl-double v3, v4, p1

    if-lez v3, :cond_5

    .line 4247
    invoke-virtual {v0, v6}, Lcom/my/target/an;->b(F)V

    goto :goto_1

    .line 4251
    :cond_5
    invoke-virtual {v0}, Lcom/my/target/an;->X()F

    move-result v3

    sub-float v3, p3, v3

    invoke-virtual {v0}, Lcom/my/target/an;->getDuration()F

    move-result v4

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_6

    .line 4253
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4254
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 4258
    :cond_6
    invoke-virtual {v0}, Lcom/my/target/an;->X()F

    move-result v3

    cmpl-float v3, v3, v6

    if-nez v3, :cond_4

    .line 4260
    invoke-virtual {v0, p3}, Lcom/my/target/an;->b(F)V

    goto :goto_1

    .line 4266
    :cond_7
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    if-eqz v0, :cond_8

    .line 4268
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAdPlayer;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 4271
    :cond_8
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->j:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/my/target/core/controllers/d;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 4273
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->g:Lcom/my/target/ck;

    iget-object v1, p0, Lcom/my/target/core/controllers/d;->h:Lcom/my/target/core/controllers/d$c;

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    .line 34
    :cond_9
    return-void
.end method

.method static synthetic a(Lcom/my/target/core/controllers/d;F)V
    .locals 3

    .prologue
    .line 34
    .line 1386
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/controllers/d;->l:Lcom/my/target/core/controllers/d$d;

    if-eqz v0, :cond_0

    .line 1388
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->l:Lcom/my/target/core/controllers/d$d;

    iget-object v1, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    invoke-interface {v0, v1}, Lcom/my/target/core/controllers/d$d;->onBannerStarted(Lcom/my/target/aj;)V

    .line 1390
    :cond_0
    invoke-virtual {p0}, Lcom/my/target/core/controllers/d;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1391
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    if-eqz v1, :cond_1

    .line 1393
    iget-object v1, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    invoke-virtual {v1}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v1

    const-string v2, "playbackStarted"

    invoke-virtual {v1, v2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 1396
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->l:Lcom/my/target/core/controllers/d$d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    if-eqz v0, :cond_2

    .line 1398
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->l:Lcom/my/target/core/controllers/d$d;

    iget-object v1, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    invoke-interface {v0, p1, p1, v1}, Lcom/my/target/core/controllers/d$d;->onBannerProgressChanged(FFLcom/my/target/aj;)V

    .line 1400
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/my/target/core/controllers/d;->a(F)V

    .line 1401
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/core/controllers/d;->q:Z

    .line 34
    return-void
.end method

.method static synthetic a(Lcom/my/target/core/controllers/d;)Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/my/target/core/controllers/d;->q:Z

    return v0
.end method

.method static synthetic b(Lcom/my/target/core/controllers/d;)Lcom/my/target/instreamads/InstreamAdPlayer;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    return-object v0
.end method

.method static synthetic b(Lcom/my/target/core/controllers/d;F)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/my/target/core/controllers/d;->a(F)V

    return-void
.end method

.method static synthetic c(Lcom/my/target/core/controllers/d;F)F
    .locals 0

    .prologue
    .line 34
    iput p1, p0, Lcom/my/target/core/controllers/d;->volume:F

    return p1
.end method

.method static synthetic c(Lcom/my/target/core/controllers/d;)Lcom/my/target/core/controllers/d$b;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->i:Lcom/my/target/core/controllers/d$b;

    return-object v0
.end method

.method static synthetic d(Lcom/my/target/core/controllers/d;)Lcom/my/target/ck;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->f:Lcom/my/target/ck;

    return-object v0
.end method

.method static synthetic e(Lcom/my/target/core/controllers/d;)Ljava/util/Stack;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->j:Ljava/util/Stack;

    return-object v0
.end method

.method public static f()Lcom/my/target/core/controllers/d;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/my/target/core/controllers/d;

    invoke-direct {v0}, Lcom/my/target/core/controllers/d;-><init>()V

    return-object v0
.end method

.method static synthetic f(Lcom/my/target/core/controllers/d;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->r:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic g(Lcom/my/target/core/controllers/d;)Lcom/my/target/core/controllers/d$c;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->h:Lcom/my/target/core/controllers/d$c;

    return-object v0
.end method

.method static synthetic h(Lcom/my/target/core/controllers/d;)Lcom/my/target/ck;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->g:Lcom/my/target/ck;

    return-object v0
.end method

.method static synthetic i(Lcom/my/target/core/controllers/d;)Lcom/my/target/aj;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    return-object v0
.end method

.method static synthetic j(Lcom/my/target/core/controllers/d;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/my/target/core/controllers/d;->s:I

    return v0
.end method

.method static synthetic k(Lcom/my/target/core/controllers/d;)Lcom/my/target/core/controllers/d$d;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->l:Lcom/my/target/core/controllers/d$d;

    return-object v0
.end method

.method static synthetic l(Lcom/my/target/core/controllers/d;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 2303
    .line 2307
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    if-eqz v0, :cond_a

    .line 2309
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    invoke-virtual {v0}, Lcom/my/target/aj;->getDuration()F

    move-result v0

    .line 2311
    :goto_0
    iget-object v2, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    if-nez v2, :cond_1

    .line 2313
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->f:Lcom/my/target/ck;

    iget-object v1, p0, Lcom/my/target/core/controllers/d;->i:Lcom/my/target/core/controllers/d$b;

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    .line 2314
    :cond_0
    :goto_1
    return-void

    .line 2317
    :cond_1
    iget v2, p0, Lcom/my/target/core/controllers/d;->s:I

    if-ne v2, v6, :cond_9

    iget-object v2, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    if-eqz v2, :cond_9

    .line 2319
    iget-object v2, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    invoke-interface {v2}, Lcom/my/target/instreamads/InstreamAdPlayer;->getAdVideoDuration()F

    move-result v2

    .line 2320
    iget-object v3, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    invoke-interface {v3}, Lcom/my/target/instreamads/InstreamAdPlayer;->getAdVideoPosition()F

    move-result v3

    .line 2321
    sub-float v4, v0, v3

    .line 2324
    :goto_2
    iget v5, p0, Lcom/my/target/core/controllers/d;->s:I

    if-ne v5, v6, :cond_8

    iget v5, p0, Lcom/my/target/core/controllers/d;->o:F

    cmpl-float v5, v5, v3

    if-eqz v5, :cond_8

    cmpl-float v2, v2, v1

    if-lez v2, :cond_8

    .line 2341
    const/4 v2, 0x0

    iput v2, p0, Lcom/my/target/core/controllers/d;->n:I

    .line 2342
    iput v3, p0, Lcom/my/target/core/controllers/d;->o:F

    .line 2344
    cmpg-float v2, v3, v0

    if-gez v2, :cond_3

    .line 2346
    invoke-direct {p0, v3}, Lcom/my/target/core/controllers/d;->a(F)V

    .line 2347
    iget-object v1, p0, Lcom/my/target/core/controllers/d;->l:Lcom/my/target/core/controllers/d$d;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    if-eqz v1, :cond_2

    .line 2349
    iget-object v1, p0, Lcom/my/target/core/controllers/d;->l:Lcom/my/target/core/controllers/d$d;

    iget-object v2, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    invoke-interface {v1, v4, v0, v2}, Lcom/my/target/core/controllers/d$d;->onBannerProgressChanged(FFLcom/my/target/aj;)V

    .line 2333
    :cond_2
    :goto_3
    iget v0, p0, Lcom/my/target/core/controllers/d;->n:I

    iget v1, p0, Lcom/my/target/core/controllers/d;->p:I

    mul-int/lit16 v1, v1, 0x3e8

    div-int/lit16 v1, v1, 0xc8

    if-lt v0, v1, :cond_0

    .line 3376
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "video freeze more then "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/my/target/core/controllers/d;->p:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " seconds, stopping"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 3377
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->f:Lcom/my/target/ck;

    iget-object v1, p0, Lcom/my/target/core/controllers/d;->i:Lcom/my/target/core/controllers/d$b;

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    .line 3378
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->l:Lcom/my/target/core/controllers/d$d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    if-eqz v0, :cond_0

    .line 3380
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->l:Lcom/my/target/core/controllers/d$d;

    const-string v1, "Timeout"

    iget-object v2, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    invoke-interface {v0, v1, v2}, Lcom/my/target/core/controllers/d$d;->onBannerError(Ljava/lang/String;Lcom/my/target/aj;)V

    goto :goto_1

    .line 2360
    :cond_3
    invoke-direct {p0, v0}, Lcom/my/target/core/controllers/d;->a(F)V

    .line 2361
    iget-object v2, p0, Lcom/my/target/core/controllers/d;->j:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/my/target/core/controllers/d;->r:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 2363
    :cond_4
    iput v0, p0, Lcom/my/target/core/controllers/d;->o:F

    .line 2365
    iget-object v2, p0, Lcom/my/target/core/controllers/d;->h:Lcom/my/target/core/controllers/d$c;

    invoke-virtual {v2}, Lcom/my/target/core/controllers/d$c;->run()V

    .line 2367
    :cond_5
    iget-object v2, p0, Lcom/my/target/core/controllers/d;->l:Lcom/my/target/core/controllers/d$d;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    if-eqz v2, :cond_6

    .line 2369
    iget-object v2, p0, Lcom/my/target/core/controllers/d;->l:Lcom/my/target/core/controllers/d$d;

    iget-object v3, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    invoke-interface {v2, v1, v0, v3}, Lcom/my/target/core/controllers/d$d;->onBannerProgressChanged(FFLcom/my/target/aj;)V

    .line 2406
    :cond_6
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->f:Lcom/my/target/ck;

    iget-object v1, p0, Lcom/my/target/core/controllers/d;->i:Lcom/my/target/core/controllers/d$b;

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    .line 2407
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->g:Lcom/my/target/ck;

    iget-object v1, p0, Lcom/my/target/core/controllers/d;->h:Lcom/my/target/core/controllers/d$c;

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    .line 2409
    iget v0, p0, Lcom/my/target/core/controllers/d;->s:I

    if-eq v0, v7, :cond_2

    .line 2411
    iput v7, p0, Lcom/my/target/core/controllers/d;->s:I

    .line 2412
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    if-eqz v0, :cond_7

    .line 2414
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAdPlayer;->stopAdVideo()V

    .line 2416
    :cond_7
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/my/target/core/controllers/d;->l:Lcom/my/target/core/controllers/d$d;

    if-eqz v0, :cond_2

    .line 2418
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    .line 2419
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    .line 2420
    iget-object v1, p0, Lcom/my/target/core/controllers/d;->l:Lcom/my/target/core/controllers/d$d;

    invoke-interface {v1, v0}, Lcom/my/target/core/controllers/d$d;->onBannerCompleted(Lcom/my/target/aj;)V

    goto/16 :goto_3

    .line 2330
    :cond_8
    iget v0, p0, Lcom/my/target/core/controllers/d;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/my/target/core/controllers/d;->n:I

    goto/16 :goto_3

    :cond_9
    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_2

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method static synthetic m(Lcom/my/target/core/controllers/d;)Lcom/my/target/aj;
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    return-object v0
.end method

.method static synthetic n(Lcom/my/target/core/controllers/d;)F
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/my/target/core/controllers/d;->o:F

    return v0
.end method


# virtual methods
.method public final a(Lcom/my/target/core/controllers/d$d;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/my/target/core/controllers/d;->l:Lcom/my/target/core/controllers/d$d;

    .line 139
    return-void
.end method

.method public final destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 208
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAdPlayer;->destroy()V

    .line 212
    :cond_0
    iput-object v1, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    .line 213
    iput-object v1, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    .line 214
    return-void
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAdPlayer;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0
.end method

.method public final getPlayer()Lcom/my/target/instreamads/InstreamAdPlayer;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    return-object v0
.end method

.method public final getVolume()F
    .locals 1

    .prologue
    .line 223
    iget v0, p0, Lcom/my/target/core/controllers/d;->volume:F

    return v0
.end method

.method public final pause()V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAdPlayer;->pauseAdVideo()V

    .line 174
    :cond_0
    return-void
.end method

.method public final play(Lcom/my/target/aj;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/aj",
            "<",
            "Lcom/my/target/common/models/VideoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 148
    .local p1, "banner":Lcom/my/target/aj;, "Lcom/my/target/aj<Lcom/my/target/common/models/VideoData;>;"
    iput-object p1, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    .line 149
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/core/controllers/d;->q:Z

    .line 150
    invoke-virtual {p1}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/d;->k:Ljava/util/Stack;

    invoke-virtual {v0, v1}, Lcom/my/target/ar;->a(Ljava/util/Stack;)V

    .line 151
    invoke-virtual {p1}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/controllers/d;->j:Ljava/util/Stack;

    invoke-virtual {v0, v1}, Lcom/my/target/ar;->b(Ljava/util/Stack;)V

    .line 152
    invoke-virtual {p1}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/ar;->ad()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/controllers/d;->r:Ljava/util/ArrayList;

    .line 154
    invoke-virtual {p1}, Lcom/my/target/aj;->getMediaData()Lcom/my/target/ag;

    move-result-object v0

    check-cast v0, Lcom/my/target/common/models/VideoData;

    .line 155
    if-nez v0, :cond_1

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    invoke-virtual {v0}, Lcom/my/target/common/models/VideoData;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 161
    iget-object v2, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    if-eqz v2, :cond_0

    .line 163
    iget-object v2, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    iget v3, p0, Lcom/my/target/core/controllers/d;->volume:F

    invoke-interface {v2, v3}, Lcom/my/target/instreamads/InstreamAdPlayer;->setVolume(F)V

    .line 164
    iget-object v2, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    invoke-virtual {v0}, Lcom/my/target/common/models/VideoData;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Lcom/my/target/common/models/VideoData;->getHeight()I

    move-result v0

    invoke-interface {v2, v1, v3, v0}, Lcom/my/target/instreamads/InstreamAdPlayer;->playAdVideo(Landroid/net/Uri;II)V

    goto :goto_0
.end method

.method public final resume()V
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAdPlayer;->resumeAdVideo()V

    .line 182
    :cond_0
    return-void
.end method

.method public final setConnectionTimeout(I)V
    .locals 0
    .param p1, "connectionTimeout"    # I

    .prologue
    .line 218
    iput p1, p0, Lcom/my/target/core/controllers/d;->p:I

    .line 219
    return-void
.end method

.method public final setPlayer(Lcom/my/target/instreamads/InstreamAdPlayer;)V
    .locals 1
    .param p1, "player"    # Lcom/my/target/instreamads/InstreamAdPlayer;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    .line 100
    if-eqz p1, :cond_0

    .line 102
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->e:Lcom/my/target/core/controllers/d$a;

    invoke-interface {p1, v0}, Lcom/my/target/instreamads/InstreamAdPlayer;->setAdPlayerListener(Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;)V

    .line 104
    :cond_0
    return-void
.end method

.method public final setVolume(F)V
    .locals 1
    .param p1, "volume"    # F

    .prologue
    .line 85
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    invoke-interface {v0, p1}, Lcom/my/target/instreamads/InstreamAdPlayer;->setVolume(F)V

    .line 89
    :cond_0
    iput p1, p0, Lcom/my/target/core/controllers/d;->volume:F

    .line 90
    return-void
.end method

.method public final stop()V
    .locals 3

    .prologue
    .line 186
    iget v0, p0, Lcom/my/target/core/controllers/d;->s:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 188
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/my/target/core/controllers/d;->l:Lcom/my/target/core/controllers/d$d;

    if-eqz v0, :cond_1

    .line 190
    invoke-virtual {p0}, Lcom/my/target/core/controllers/d;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 191
    if-eqz v0, :cond_0

    .line 193
    iget-object v1, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    invoke-virtual {v1}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v1

    const-string v2, "playbackStopped"

    invoke-virtual {v1, v2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->l:Lcom/my/target/core/controllers/d$d;

    iget-object v1, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    invoke-interface {v0, v1}, Lcom/my/target/core/controllers/d$d;->onBannerStopped(Lcom/my/target/aj;)V

    .line 197
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/my/target/core/controllers/d;->s:I

    .line 199
    :cond_2
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    if-eqz v0, :cond_3

    .line 201
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAdPlayer;->stopAdVideo()V

    .line 203
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    .line 204
    return-void
.end method

.method public final swapPlayer(Lcom/my/target/instreamads/InstreamAdPlayer;)V
    .locals 4
    .param p1, "player"    # Lcom/my/target/instreamads/InstreamAdPlayer;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/my/target/instreamads/InstreamAdPlayer;->setAdPlayerListener(Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;)V

    .line 112
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    invoke-interface {v0}, Lcom/my/target/instreamads/InstreamAdPlayer;->stopAdVideo()V

    .line 114
    :cond_0
    iput-object p1, p0, Lcom/my/target/core/controllers/d;->player:Lcom/my/target/instreamads/InstreamAdPlayer;

    .line 115
    if-eqz p1, :cond_1

    .line 117
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->e:Lcom/my/target/core/controllers/d$a;

    invoke-interface {p1, v0}, Lcom/my/target/instreamads/InstreamAdPlayer;->setAdPlayerListener(Lcom/my/target/instreamads/InstreamAdPlayer$AdPlayerListener;)V

    .line 119
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    if-eqz v0, :cond_2

    .line 121
    iget-object v0, p0, Lcom/my/target/core/controllers/d;->m:Lcom/my/target/aj;

    invoke-virtual {v0}, Lcom/my/target/aj;->getMediaData()Lcom/my/target/ag;

    move-result-object v0

    check-cast v0, Lcom/my/target/common/models/VideoData;

    .line 122
    if-nez v0, :cond_3

    .line 134
    :cond_2
    :goto_0
    return-void

    .line 127
    :cond_3
    invoke-virtual {v0}, Lcom/my/target/common/models/VideoData;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 128
    if-eqz p1, :cond_2

    .line 130
    iget v2, p0, Lcom/my/target/core/controllers/d;->volume:F

    invoke-interface {p1, v2}, Lcom/my/target/instreamads/InstreamAdPlayer;->setVolume(F)V

    .line 131
    invoke-virtual {v0}, Lcom/my/target/common/models/VideoData;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Lcom/my/target/common/models/VideoData;->getHeight()I

    move-result v0

    iget v3, p0, Lcom/my/target/core/controllers/d;->o:F

    invoke-interface {p1, v1, v2, v0, v3}, Lcom/my/target/instreamads/InstreamAdPlayer;->playAdVideo(Landroid/net/Uri;IIF)V

    goto :goto_0
.end method
