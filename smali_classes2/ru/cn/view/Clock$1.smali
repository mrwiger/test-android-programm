.class Lru/cn/view/Clock$1;
.super Ljava/lang/Object;
.source "Clock.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/view/Clock;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/view/Clock;


# direct methods
.method constructor <init>(Lru/cn/view/Clock;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/view/Clock;

    .prologue
    .line 85
    iput-object p1, p0, Lru/cn/view/Clock$1;->this$0:Lru/cn/view/Clock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x3e8

    .line 87
    iget-object v4, p0, Lru/cn/view/Clock$1;->this$0:Lru/cn/view/Clock;

    invoke-static {v4}, Lru/cn/view/Clock;->access$000(Lru/cn/view/Clock;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 96
    :goto_0
    return-void

    .line 90
    :cond_0
    iget-object v4, p0, Lru/cn/view/Clock$1;->this$0:Lru/cn/view/Clock;

    invoke-static {v4}, Lru/cn/view/Clock;->access$100(Lru/cn/view/Clock;)V

    .line 92
    iget-object v4, p0, Lru/cn/view/Clock$1;->this$0:Lru/cn/view/Clock;

    invoke-virtual {v4}, Lru/cn/view/Clock;->invalidate()V

    .line 93
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 94
    .local v2, "now":J
    rem-long v4, v2, v6

    sub-long v4, v6, v4

    add-long v0, v2, v4

    .line 95
    .local v0, "next":J
    iget-object v4, p0, Lru/cn/view/Clock$1;->this$0:Lru/cn/view/Clock;

    invoke-static {v4}, Lru/cn/view/Clock;->access$300(Lru/cn/view/Clock;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lru/cn/view/Clock$1;->this$0:Lru/cn/view/Clock;

    invoke-static {v5}, Lru/cn/view/Clock;->access$200(Lru/cn/view/Clock;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-virtual {v4, v5, v0, v1}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
