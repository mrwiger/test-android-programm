.class public Lru/cn/ad/video/VAST/parser/SkipOffset;
.super Ljava/lang/Object;
.source "SkipOffset.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;
    }
.end annotation


# instance fields
.field public final type:Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;

.field public final value:F


# direct methods
.method public constructor <init>(Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;F)V
    .locals 0
    .param p1, "type"    # Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;
    .param p2, "value"    # F

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lru/cn/ad/video/VAST/parser/SkipOffset;->type:Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;

    .line 19
    iput p2, p0, Lru/cn/ad/video/VAST/parser/SkipOffset;->value:F

    .line 20
    return-void
.end method

.method public static parseString(Ljava/lang/String;)Lru/cn/ad/video/VAST/parser/SkipOffset;
    .locals 12
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 24
    :try_start_0
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v7}, Ljava/text/NumberFormat;->getPercentInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v4

    .line 25
    .local v4, "percentFormat":Ljava/text/NumberFormat;
    invoke-virtual {v4, p0}, Ljava/text/NumberFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F

    move-result v0

    .line 26
    .local v0, "floatValue":F
    new-instance v7, Lru/cn/ad/video/VAST/parser/SkipOffset;

    sget-object v9, Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;->SkipTypePercent:Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;

    invoke-direct {v7, v9, v0}, Lru/cn/ad/video/VAST/parser/SkipOffset;-><init>(Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;F)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    .end local v0    # "floatValue":F
    .end local v4    # "percentFormat":Ljava/text/NumberFormat;
    :goto_0
    return-object v7

    .line 27
    :catch_0
    move-exception v7

    .line 31
    const-string v7, ":"

    invoke-virtual {p0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 32
    .local v3, "parts":[Ljava/lang/String;
    array-length v7, v3

    const/4 v9, 0x3

    if-ge v7, v9, :cond_0

    move-object v7, v8

    .line 33
    goto :goto_0

    .line 37
    :cond_0
    :try_start_1
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v7}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v6

    .line 38
    .local v6, "timeFormat":Ljava/text/NumberFormat;
    const/4 v7, 0x0

    aget-object v7, v3, v7

    invoke-virtual {v6, v7}, Ljava/text/NumberFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F

    move-result v1

    .line 39
    .local v1, "hours":F
    const/4 v7, 0x1

    aget-object v7, v3, v7

    invoke-virtual {v6, v7}, Ljava/text/NumberFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F

    move-result v2

    .line 40
    .local v2, "minutes":F
    const/4 v7, 0x2

    aget-object v7, v3, v7

    invoke-virtual {v6, v7}, Ljava/text/NumberFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F

    move-result v5

    .line 41
    .local v5, "seconds":F
    new-instance v7, Lru/cn/ad/video/VAST/parser/SkipOffset;

    sget-object v9, Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;->SkipTypeTime:Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;

    const/high16 v10, 0x45610000    # 3600.0f

    mul-float/2addr v10, v1

    const/high16 v11, 0x42700000    # 60.0f

    mul-float/2addr v11, v2

    add-float/2addr v10, v11

    add-float/2addr v10, v5

    invoke-direct {v7, v9, v10}, Lru/cn/ad/video/VAST/parser/SkipOffset;-><init>(Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;F)V
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 42
    .end local v1    # "hours":F
    .end local v2    # "minutes":F
    .end local v5    # "seconds":F
    .end local v6    # "timeFormat":Ljava/text/NumberFormat;
    :catch_1
    move-exception v7

    move-object v7, v8

    .line 46
    goto :goto_0
.end method
