.class public Lru/cn/ad/video/VAST/VASTUtils;
.super Ljava/lang/Object;
.source "VASTUtils.java"


# direct methods
.method public static resolveErrorUri(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p0, "errorUri"    # Ljava/lang/String;
    .param p1, "errorCode"    # I

    .prologue
    .line 34
    const-string v1, "[ERRORCODE]"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, "resultUri":Ljava/lang/String;
    const-string v1, "\\[.*?\\]"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static resolveVASTAdTagMacros(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "vastAdTag"    # Ljava/lang/String;

    .prologue
    .line 20
    const-string v3, "[CACHEBUSTING]"

    new-instance v4, Ljava/util/Random;

    invoke-direct {v4}, Ljava/util/Random;-><init>()V

    invoke-virtual {v4}, Ljava/util/Random;->nextInt()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 21
    .local v0, "cleanTag":Ljava/lang/String;
    invoke-static {p0}, Lru/cn/utils/Utils;->getGadId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 22
    .local v1, "gadId":Ljava/lang/String;
    const-string v3, "[USER_ID]"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    .line 23
    const-string v3, "[USER_ID]"

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 27
    :cond_0
    invoke-static {p0}, Lru/cn/utils/Utils;->getUUID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 28
    .local v2, "installId":Ljava/lang/String;
    const-string v3, "[INSTALL_ID]"

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 30
    const-string v3, "\\[.*?\\]"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
