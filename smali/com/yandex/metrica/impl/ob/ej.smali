.class Lcom/yandex/metrica/impl/ob/ej;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/yandex/metrica/impl/ob/ef;

.field private final b:Lcom/yandex/metrica/impl/ob/dy;

.field private final c:Lcom/yandex/metrica/impl/ob/en;

.field private final d:Lcom/yandex/metrica/impl/ob/dr;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/os/Looper;Landroid/location/LocationManager;Lcom/yandex/metrica/impl/bn;Lcom/yandex/metrica/impl/ob/gi;Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/en;Lcom/yandex/metrica/impl/ob/dr;)V
    .locals 7

    .prologue
    .line 63
    new-instance v0, Lcom/yandex/metrica/impl/ob/ef;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p6

    move-object v5, p7

    move-object v6, p8

    invoke-direct/range {v0 .. v6}, Lcom/yandex/metrica/impl/ob/ef;-><init>(Landroid/content/Context;Landroid/os/Looper;Landroid/location/LocationManager;Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/en;Lcom/yandex/metrica/impl/ob/dr;)V

    new-instance v1, Lcom/yandex/metrica/impl/ob/dy;

    move-object v2, p1

    move-object v3, p4

    move-object v4, p5

    move-object v5, p7

    move-object v6, p8

    invoke-direct/range {v1 .. v6}, Lcom/yandex/metrica/impl/ob/dy;-><init>(Landroid/content/Context;Lcom/yandex/metrica/impl/bn;Lcom/yandex/metrica/impl/ob/gi;Lcom/yandex/metrica/impl/ob/en;Lcom/yandex/metrica/impl/ob/dr;)V

    invoke-direct {p0, v0, v1, p7, p8}, Lcom/yandex/metrica/impl/ob/ej;-><init>(Lcom/yandex/metrica/impl/ob/ef;Lcom/yandex/metrica/impl/ob/dy;Lcom/yandex/metrica/impl/ob/en;Lcom/yandex/metrica/impl/ob/dr;)V

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/ct;Lcom/yandex/metrica/impl/ob/cs;)V
    .locals 9

    .prologue
    .line 43
    const-string v0, "location"

    .line 46
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/location/LocationManager;

    .line 47
    invoke-static {p1}, Lcom/yandex/metrica/impl/bn;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/bn;

    move-result-object v4

    .line 48
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/gi;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/gi;

    move-result-object v5

    new-instance v7, Lcom/yandex/metrica/impl/ob/en;

    invoke-direct {v7, p1, p3, p4, p5}, Lcom/yandex/metrica/impl/ob/en;-><init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/ct;Lcom/yandex/metrica/impl/ob/cs;)V

    new-instance v8, Lcom/yandex/metrica/impl/ob/dr;

    invoke-direct {v8, p3, p4, p5}, Lcom/yandex/metrica/impl/ob/dr;-><init>(Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/ct;Lcom/yandex/metrica/impl/ob/cs;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    .line 43
    invoke-direct/range {v0 .. v8}, Lcom/yandex/metrica/impl/ob/ej;-><init>(Landroid/content/Context;Landroid/os/Looper;Landroid/location/LocationManager;Lcom/yandex/metrica/impl/bn;Lcom/yandex/metrica/impl/ob/gi;Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/en;Lcom/yandex/metrica/impl/ob/dr;)V

    .line 53
    return-void
.end method

.method constructor <init>(Lcom/yandex/metrica/impl/ob/ef;Lcom/yandex/metrica/impl/ob/dy;Lcom/yandex/metrica/impl/ob/en;Lcom/yandex/metrica/impl/ob/dr;)V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/ej;->a:Lcom/yandex/metrica/impl/ob/ef;

    .line 124
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/ej;->b:Lcom/yandex/metrica/impl/ob/dy;

    .line 125
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/ej;->c:Lcom/yandex/metrica/impl/ob/en;

    .line 126
    iput-object p4, p0, Lcom/yandex/metrica/impl/ob/ej;->d:Lcom/yandex/metrica/impl/ob/dr;

    .line 127
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ej;->a:Lcom/yandex/metrica/impl/ob/ef;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/ef;->a()V

    .line 86
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ej;->b:Lcom/yandex/metrica/impl/ob/dy;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dy;->a()V

    .line 87
    return-void
.end method

.method public a(Lcom/yandex/metrica/impl/ob/dv;)V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ej;->a:Lcom/yandex/metrica/impl/ob/ef;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/ef;->a(Lcom/yandex/metrica/impl/ob/dv;)V

    .line 113
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ej;->c:Lcom/yandex/metrica/impl/ob/en;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/en;->a(Lcom/yandex/metrica/impl/ob/dv;)V

    .line 114
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ej;->d:Lcom/yandex/metrica/impl/ob/dr;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/dr;->a(Lcom/yandex/metrica/impl/ob/dv;)V

    .line 115
    return-void
.end method

.method public b()Landroid/location/Location;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ej;->a:Lcom/yandex/metrica/impl/ob/ef;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/ef;->b()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public c()Landroid/location/Location;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ej;->a:Lcom/yandex/metrica/impl/ob/ef;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/ef;->c()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ej;->c:Lcom/yandex/metrica/impl/ob/en;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/en;->a()V

    .line 101
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ej;->a:Lcom/yandex/metrica/impl/ob/ef;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/ef;->d()V

    .line 105
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ej;->a:Lcom/yandex/metrica/impl/ob/ef;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/ef;->e()V

    .line 109
    return-void
.end method
