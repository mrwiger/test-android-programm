.class public final enum Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;
.super Ljava/lang/Enum;
.source "CatalogueItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/api/catalogue/replies/CatalogueItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MediaGuideType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;

.field public static final enum BROADCAST:Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;

.field public static final enum CHANNEL:Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;

.field public static final enum FRAGMENT:Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 11
    new-instance v0, Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;

    const-string v1, "CHANNEL"

    invoke-direct {v0, v1, v4, v2}, Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;->CHANNEL:Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;

    new-instance v0, Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;

    const-string v1, "BROADCAST"

    invoke-direct {v0, v1, v2, v3}, Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;->BROADCAST:Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;

    new-instance v0, Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;

    const-string v1, "FRAGMENT"

    invoke-direct {v0, v1, v3, v5}, Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;->FRAGMENT:Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;

    .line 10
    new-array v0, v5, [Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;

    sget-object v1, Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;->CHANNEL:Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;

    aput-object v1, v0, v4

    sget-object v1, Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;->BROADCAST:Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;

    aput-object v1, v0, v2

    sget-object v1, Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;->FRAGMENT:Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;

    aput-object v1, v0, v3

    sput-object v0, Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;->$VALUES:[Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 15
    iput p3, p0, Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;->value:I

    .line 16
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;

    return-object v0
.end method

.method public static values()[Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;->$VALUES:[Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;

    invoke-virtual {v0}, [Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;->value:I

    return v0
.end method
