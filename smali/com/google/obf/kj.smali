.class public Lcom/google/obf/kj;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/google/obf/kl;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:I

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 154
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lcom/google/obf/kj;->a:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/kj;->c:I

    .line 49
    const/16 v0, 0x25

    iput v0, p0, Lcom/google/obf/kj;->b:I

    .line 50
    const/16 v0, 0x11

    iput v0, p0, Lcom/google/obf/kj;->c:I

    .line 51
    return-void
.end method

.method public constructor <init>(II)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput v2, p0, Lcom/google/obf/kj;->c:I

    .line 54
    rem-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "HashCodeBuilder requires an odd initial value"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lcom/google/obf/kf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 55
    rem-int/lit8 v0, p2, 0x2

    if-eqz v0, :cond_1

    :goto_1
    const-string v0, "HashCodeBuilder requires an odd multiplier"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/obf/kf;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 56
    iput p2, p0, Lcom/google/obf/kj;->b:I

    .line 57
    iput p1, p0, Lcom/google/obf/kj;->c:I

    .line 58
    return-void

    :cond_0
    move v0, v2

    .line 54
    goto :goto_0

    :cond_1
    move v1, v2

    .line 55
    goto :goto_1
.end method

.method public static varargs a(IILjava/lang/Object;ZLjava/lang/Class;[Ljava/lang/String;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(IITT;Z",
            "Ljava/lang/Class",
            "<-TT;>;[",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    .line 25
    if-nez p2, :cond_0

    .line 26
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The object to build a hash code for must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 27
    :cond_0
    new-instance v1, Lcom/google/obf/kj;

    invoke-direct {v1, p0, p1}, Lcom/google/obf/kj;-><init>(II)V

    .line 28
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 29
    invoke-static {p2, v0, v1, p3, p5}, Lcom/google/obf/kj;->a(Ljava/lang/Object;Ljava/lang/Class;Lcom/google/obf/kj;Z[Ljava/lang/String;)V

    .line 30
    :goto_0
    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v2

    if-eqz v2, :cond_1

    if-eq v0, p4, :cond_1

    .line 31
    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    .line 32
    invoke-static {p2, v0, v1, p3, p5}, Lcom/google/obf/kj;->a(Ljava/lang/Object;Ljava/lang/Class;Lcom/google/obf/kj;Z[Ljava/lang/String;)V

    goto :goto_0

    .line 33
    :cond_1
    invoke-virtual {v1}, Lcom/google/obf/kj;->b()I

    move-result v0

    return v0
.end method

.method public static varargs a(Ljava/lang/Object;[Ljava/lang/String;)I
    .locals 6

    .prologue
    .line 34
    const/16 v0, 0x11

    const/16 v1, 0x25

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/obf/kj;->a(IILjava/lang/Object;ZLjava/lang/Class;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/obf/kl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1
    sget-object v0, Lcom/google/obf/kj;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Class;Lcom/google/obf/kj;Z[Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/google/obf/kj;",
            "Z[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 4
    invoke-static {p0}, Lcom/google/obf/kj;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    :goto_0
    return-void

    .line 6
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/google/obf/kj;->c(Ljava/lang/Object;)V

    .line 7
    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    .line 8
    const/4 v0, 0x1

    invoke-static {v1, v0}, Ljava/lang/reflect/AccessibleObject;->setAccessible([Ljava/lang/reflect/AccessibleObject;Z)V

    .line 9
    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 10
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {p4, v4}, Lcom/google/obf/kd;->b([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 11
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "$"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    if-nez p3, :cond_1

    .line 12
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v4

    invoke-static {v4}, Ljava/lang/reflect/Modifier;->isTransient(I)Z

    move-result v4

    if-nez v4, :cond_2

    .line 13
    :cond_1
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v4

    invoke-static {v4}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v4

    if-nez v4, :cond_2

    const-class v4, Lcom/google/obf/kk;

    .line 14
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->isAnnotationPresent(Ljava/lang/Class;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_2

    .line 15
    :try_start_1
    invoke-virtual {v3, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 16
    invoke-virtual {p2, v3}, Lcom/google/obf/kj;->b(Ljava/lang/Object;)Lcom/google/obf/kj;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 20
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 18
    :catch_0
    move-exception v0

    .line 19
    :try_start_2
    new-instance v0, Ljava/lang/InternalError;

    const-string v1, "Unexpected IllegalAccessException"

    invoke-direct {v0, v1}, Ljava/lang/InternalError;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 23
    :catchall_0
    move-exception v0

    invoke-static {p0}, Lcom/google/obf/kj;->d(Ljava/lang/Object;)V

    throw v0

    .line 21
    :cond_3
    invoke-static {p0}, Lcom/google/obf/kj;->d(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2
    invoke-static {}, Lcom/google/obf/kj;->a()Ljava/util/Set;

    move-result-object v0

    .line 3
    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/obf/kl;

    invoke-direct {v1, p0}, Lcom/google/obf/kl;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 35
    invoke-static {}, Lcom/google/obf/kj;->a()Ljava/util/Set;

    move-result-object v0

    .line 36
    if-nez v0, :cond_0

    .line 37
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 38
    sget-object v1, Lcom/google/obf/kj;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 39
    :cond_0
    new-instance v1, Lcom/google/obf/kl;

    invoke-direct {v1, p0}, Lcom/google/obf/kl;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 40
    return-void
.end method

.method private static d(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 41
    invoke-static {}, Lcom/google/obf/kj;->a()Ljava/util/Set;

    move-result-object v0

    .line 42
    if-eqz v0, :cond_0

    .line 43
    new-instance v1, Lcom/google/obf/kl;

    invoke-direct {v1, p0}, Lcom/google/obf/kl;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 44
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    sget-object v0, Lcom/google/obf/kj;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    .line 46
    :cond_0
    return-void
.end method

.method private e(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 120
    instance-of v0, p1, [J

    if-eqz v0, :cond_0

    .line 121
    check-cast p1, [J

    check-cast p1, [J

    invoke-virtual {p0, p1}, Lcom/google/obf/kj;->a([J)Lcom/google/obf/kj;

    .line 137
    :goto_0
    return-void

    .line 122
    :cond_0
    instance-of v0, p1, [I

    if-eqz v0, :cond_1

    .line 123
    check-cast p1, [I

    check-cast p1, [I

    invoke-virtual {p0, p1}, Lcom/google/obf/kj;->a([I)Lcom/google/obf/kj;

    goto :goto_0

    .line 124
    :cond_1
    instance-of v0, p1, [S

    if-eqz v0, :cond_2

    .line 125
    check-cast p1, [S

    check-cast p1, [S

    invoke-virtual {p0, p1}, Lcom/google/obf/kj;->a([S)Lcom/google/obf/kj;

    goto :goto_0

    .line 126
    :cond_2
    instance-of v0, p1, [C

    if-eqz v0, :cond_3

    .line 127
    check-cast p1, [C

    check-cast p1, [C

    invoke-virtual {p0, p1}, Lcom/google/obf/kj;->a([C)Lcom/google/obf/kj;

    goto :goto_0

    .line 128
    :cond_3
    instance-of v0, p1, [B

    if-eqz v0, :cond_4

    .line 129
    check-cast p1, [B

    check-cast p1, [B

    invoke-virtual {p0, p1}, Lcom/google/obf/kj;->a([B)Lcom/google/obf/kj;

    goto :goto_0

    .line 130
    :cond_4
    instance-of v0, p1, [D

    if-eqz v0, :cond_5

    .line 131
    check-cast p1, [D

    check-cast p1, [D

    invoke-virtual {p0, p1}, Lcom/google/obf/kj;->a([D)Lcom/google/obf/kj;

    goto :goto_0

    .line 132
    :cond_5
    instance-of v0, p1, [F

    if-eqz v0, :cond_6

    .line 133
    check-cast p1, [F

    check-cast p1, [F

    invoke-virtual {p0, p1}, Lcom/google/obf/kj;->a([F)Lcom/google/obf/kj;

    goto :goto_0

    .line 134
    :cond_6
    instance-of v0, p1, [Z

    if-eqz v0, :cond_7

    .line 135
    check-cast p1, [Z

    check-cast p1, [Z

    invoke-virtual {p0, p1}, Lcom/google/obf/kj;->a([Z)Lcom/google/obf/kj;

    goto :goto_0

    .line 136
    :cond_7
    check-cast p1, [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/google/obf/kj;->a([Ljava/lang/Object;)Lcom/google/obf/kj;

    goto :goto_0
.end method


# virtual methods
.method public a(B)Lcom/google/obf/kj;
    .locals 2

    .prologue
    .line 67
    iget v0, p0, Lcom/google/obf/kj;->c:I

    iget v1, p0, Lcom/google/obf/kj;->b:I

    mul-int/2addr v0, v1

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/obf/kj;->c:I

    .line 68
    return-object p0
.end method

.method public a(C)Lcom/google/obf/kj;
    .locals 2

    .prologue
    .line 75
    iget v0, p0, Lcom/google/obf/kj;->c:I

    iget v1, p0, Lcom/google/obf/kj;->b:I

    mul-int/2addr v0, v1

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/obf/kj;->c:I

    .line 76
    return-object p0
.end method

.method public a(D)Lcom/google/obf/kj;
    .locals 3

    .prologue
    .line 83
    invoke-static {p1, p2}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/obf/kj;->a(J)Lcom/google/obf/kj;

    move-result-object v0

    return-object v0
.end method

.method public a(F)Lcom/google/obf/kj;
    .locals 2

    .prologue
    .line 90
    iget v0, p0, Lcom/google/obf/kj;->c:I

    iget v1, p0, Lcom/google/obf/kj;->b:I

    mul-int/2addr v0, v1

    invoke-static {p1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/kj;->c:I

    .line 91
    return-object p0
.end method

.method public a(I)Lcom/google/obf/kj;
    .locals 2

    .prologue
    .line 98
    iget v0, p0, Lcom/google/obf/kj;->c:I

    iget v1, p0, Lcom/google/obf/kj;->b:I

    mul-int/2addr v0, v1

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/obf/kj;->c:I

    .line 99
    return-object p0
.end method

.method public a(J)Lcom/google/obf/kj;
    .locals 5

    .prologue
    .line 106
    iget v0, p0, Lcom/google/obf/kj;->c:I

    iget v1, p0, Lcom/google/obf/kj;->b:I

    mul-int/2addr v0, v1

    const/16 v1, 0x20

    shr-long v2, p1, v1

    xor-long/2addr v2, p1

    long-to-int v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/kj;->c:I

    .line 107
    return-object p0
.end method

.method public a(S)Lcom/google/obf/kj;
    .locals 2

    .prologue
    .line 144
    iget v0, p0, Lcom/google/obf/kj;->c:I

    iget v1, p0, Lcom/google/obf/kj;->b:I

    mul-int/2addr v0, v1

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/obf/kj;->c:I

    .line 145
    return-object p0
.end method

.method public a(Z)Lcom/google/obf/kj;
    .locals 2

    .prologue
    .line 59
    iget v0, p0, Lcom/google/obf/kj;->c:I

    iget v1, p0, Lcom/google/obf/kj;->b:I

    mul-int/2addr v1, v0

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/kj;->c:I

    .line 60
    return-object p0

    .line 59
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a([B)Lcom/google/obf/kj;
    .locals 3

    .prologue
    .line 69
    if-nez p1, :cond_1

    .line 70
    iget v0, p0, Lcom/google/obf/kj;->c:I

    iget v1, p0, Lcom/google/obf/kj;->b:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/kj;->c:I

    .line 74
    :cond_0
    return-object p0

    .line 71
    :cond_1
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-byte v2, p1, v0

    .line 72
    invoke-virtual {p0, v2}, Lcom/google/obf/kj;->a(B)Lcom/google/obf/kj;

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a([C)Lcom/google/obf/kj;
    .locals 3

    .prologue
    .line 77
    if-nez p1, :cond_1

    .line 78
    iget v0, p0, Lcom/google/obf/kj;->c:I

    iget v1, p0, Lcom/google/obf/kj;->b:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/kj;->c:I

    .line 82
    :cond_0
    return-object p0

    .line 79
    :cond_1
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-char v2, p1, v0

    .line 80
    invoke-virtual {p0, v2}, Lcom/google/obf/kj;->a(C)Lcom/google/obf/kj;

    .line 81
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a([D)Lcom/google/obf/kj;
    .locals 4

    .prologue
    .line 84
    if-nez p1, :cond_1

    .line 85
    iget v0, p0, Lcom/google/obf/kj;->c:I

    iget v1, p0, Lcom/google/obf/kj;->b:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/kj;->c:I

    .line 89
    :cond_0
    return-object p0

    .line 86
    :cond_1
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-wide v2, p1, v0

    .line 87
    invoke-virtual {p0, v2, v3}, Lcom/google/obf/kj;->a(D)Lcom/google/obf/kj;

    .line 88
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a([F)Lcom/google/obf/kj;
    .locals 3

    .prologue
    .line 92
    if-nez p1, :cond_1

    .line 93
    iget v0, p0, Lcom/google/obf/kj;->c:I

    iget v1, p0, Lcom/google/obf/kj;->b:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/kj;->c:I

    .line 97
    :cond_0
    return-object p0

    .line 94
    :cond_1
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p1, v0

    .line 95
    invoke-virtual {p0, v2}, Lcom/google/obf/kj;->a(F)Lcom/google/obf/kj;

    .line 96
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a([I)Lcom/google/obf/kj;
    .locals 3

    .prologue
    .line 100
    if-nez p1, :cond_1

    .line 101
    iget v0, p0, Lcom/google/obf/kj;->c:I

    iget v1, p0, Lcom/google/obf/kj;->b:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/kj;->c:I

    .line 105
    :cond_0
    return-object p0

    .line 102
    :cond_1
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p1, v0

    .line 103
    invoke-virtual {p0, v2}, Lcom/google/obf/kj;->a(I)Lcom/google/obf/kj;

    .line 104
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a([J)Lcom/google/obf/kj;
    .locals 4

    .prologue
    .line 108
    if-nez p1, :cond_1

    .line 109
    iget v0, p0, Lcom/google/obf/kj;->c:I

    iget v1, p0, Lcom/google/obf/kj;->b:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/kj;->c:I

    .line 113
    :cond_0
    return-object p0

    .line 110
    :cond_1
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-wide v2, p1, v0

    .line 111
    invoke-virtual {p0, v2, v3}, Lcom/google/obf/kj;->a(J)Lcom/google/obf/kj;

    .line 112
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a([Ljava/lang/Object;)Lcom/google/obf/kj;
    .locals 3

    .prologue
    .line 138
    if-nez p1, :cond_1

    .line 139
    iget v0, p0, Lcom/google/obf/kj;->c:I

    iget v1, p0, Lcom/google/obf/kj;->b:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/kj;->c:I

    .line 143
    :cond_0
    return-object p0

    .line 140
    :cond_1
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 141
    invoke-virtual {p0, v2}, Lcom/google/obf/kj;->b(Ljava/lang/Object;)Lcom/google/obf/kj;

    .line 142
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a([S)Lcom/google/obf/kj;
    .locals 3

    .prologue
    .line 146
    if-nez p1, :cond_1

    .line 147
    iget v0, p0, Lcom/google/obf/kj;->c:I

    iget v1, p0, Lcom/google/obf/kj;->b:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/kj;->c:I

    .line 151
    :cond_0
    return-object p0

    .line 148
    :cond_1
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-short v2, p1, v0

    .line 149
    invoke-virtual {p0, v2}, Lcom/google/obf/kj;->a(S)Lcom/google/obf/kj;

    .line 150
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a([Z)Lcom/google/obf/kj;
    .locals 3

    .prologue
    .line 61
    if-nez p1, :cond_1

    .line 62
    iget v0, p0, Lcom/google/obf/kj;->c:I

    iget v1, p0, Lcom/google/obf/kj;->b:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/kj;->c:I

    .line 66
    :cond_0
    return-object p0

    .line 63
    :cond_1
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-boolean v2, p1, v0

    .line 64
    invoke-virtual {p0, v2}, Lcom/google/obf/kj;->a(Z)Lcom/google/obf/kj;

    .line 65
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Lcom/google/obf/kj;->c:I

    return v0
.end method

.method public b(Ljava/lang/Object;)Lcom/google/obf/kj;
    .locals 2

    .prologue
    .line 114
    if-nez p1, :cond_0

    .line 115
    iget v0, p0, Lcom/google/obf/kj;->c:I

    iget v1, p0, Lcom/google/obf/kj;->b:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/kj;->c:I

    .line 119
    :goto_0
    return-object p0

    .line 116
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 117
    invoke-direct {p0, p1}, Lcom/google/obf/kj;->e(Ljava/lang/Object;)V

    goto :goto_0

    .line 118
    :cond_1
    iget v0, p0, Lcom/google/obf/kj;->c:I

    iget v1, p0, Lcom/google/obf/kj;->b:I

    mul-int/2addr v0, v1

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/kj;->c:I

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/google/obf/kj;->b()I

    move-result v0

    return v0
.end method
