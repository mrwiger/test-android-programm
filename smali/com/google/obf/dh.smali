.class public final Lcom/google/obf/dh;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/dh$b;,
        Lcom/google/obf/dh$a;,
        Lcom/google/obf/dh$c;,
        Lcom/google/obf/dh$d;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field private b:Lcom/google/obf/dh$b;

.field private c:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lcom/google/obf/ea;->a(Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/dh;->a:Ljava/util/concurrent/ExecutorService;

    .line 3
    return-void
.end method

.method static synthetic a(Lcom/google/obf/dh;Lcom/google/obf/dh$b;)Lcom/google/obf/dh$b;
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lcom/google/obf/dh;->b:Lcom/google/obf/dh$b;

    return-object p1
.end method

.method static synthetic a(Lcom/google/obf/dh;Z)Z
    .locals 0

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/google/obf/dh;->c:Z

    return p1
.end method


# virtual methods
.method public a(Landroid/os/Looper;Lcom/google/obf/dh$c;Lcom/google/obf/dh$a;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 8
    iget-boolean v0, p0, Lcom/google/obf/dh;->c:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 9
    iput-boolean v1, p0, Lcom/google/obf/dh;->c:Z

    .line 10
    new-instance v0, Lcom/google/obf/dh$b;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/obf/dh$b;-><init>(Lcom/google/obf/dh;Landroid/os/Looper;Lcom/google/obf/dh$c;Lcom/google/obf/dh$a;)V

    iput-object v0, p0, Lcom/google/obf/dh;->b:Lcom/google/obf/dh$b;

    .line 11
    iget-object v0, p0, Lcom/google/obf/dh;->a:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcom/google/obf/dh;->b:Lcom/google/obf/dh$b;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 12
    return-void

    .line 8
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/obf/dh$c;Lcom/google/obf/dh$a;)V
    .locals 2

    .prologue
    .line 4
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    .line 5
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 6
    invoke-virtual {p0, v1, p1, p2}, Lcom/google/obf/dh;->a(Landroid/os/Looper;Lcom/google/obf/dh$c;Lcom/google/obf/dh$a;)V

    .line 7
    return-void

    .line 5
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/google/obf/dh;->c:Z

    if-eqz v0, :cond_0

    .line 18
    invoke-virtual {p0}, Lcom/google/obf/dh;->b()V

    .line 19
    :cond_0
    if-eqz p1, :cond_1

    .line 20
    iget-object v0, p0, Lcom/google/obf/dh;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 21
    :cond_1
    iget-object v0, p0, Lcom/google/obf/dh;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 22
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 13
    iget-boolean v0, p0, Lcom/google/obf/dh;->c:Z

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 14
    iget-boolean v0, p0, Lcom/google/obf/dh;->c:Z

    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 15
    iget-object v0, p0, Lcom/google/obf/dh;->b:Lcom/google/obf/dh$b;

    invoke-virtual {v0}, Lcom/google/obf/dh$b;->a()V

    .line 16
    return-void
.end method
