.class public Lcom/yandex/metrica/impl/ob/dv;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ob/dv$a;
    }
.end annotation


# static fields
.field public static final e:J

.field public static final f:J


# instance fields
.field public final g:J

.field public final h:F

.field public final i:I

.field public final j:I

.field public final k:J

.field public final l:I

.field public final m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 58
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/yandex/metrica/impl/ob/dv;->e:J

    .line 62
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/yandex/metrica/impl/ob/dv;->f:J

    return-void
.end method

.method public constructor <init>(JFIIJIZ)V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168
    iput-wide p1, p0, Lcom/yandex/metrica/impl/ob/dv;->g:J

    .line 169
    iput p3, p0, Lcom/yandex/metrica/impl/ob/dv;->h:F

    .line 170
    iput p4, p0, Lcom/yandex/metrica/impl/ob/dv;->i:I

    .line 171
    iput p5, p0, Lcom/yandex/metrica/impl/ob/dv;->j:I

    .line 172
    iput-wide p6, p0, Lcom/yandex/metrica/impl/ob/dv;->k:J

    .line 173
    iput p8, p0, Lcom/yandex/metrica/impl/ob/dv;->l:I

    .line 174
    iput-boolean p9, p0, Lcom/yandex/metrica/impl/ob/dv;->m:Z

    .line 175
    return-void
.end method

.method public constructor <init>(Lcom/yandex/metrica/impl/bh$a$b;Z)V
    .locals 10

    .prologue
    .line 76
    iget-object v0, p1, Lcom/yandex/metrica/impl/bh$a$b;->c:Ljava/lang/Long;

    if-eqz v0, :cond_0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p1, Lcom/yandex/metrica/impl/bh$a$b;->c:Ljava/lang/Long;

    .line 77
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    :goto_0
    iget-object v0, p1, Lcom/yandex/metrica/impl/bh$a$b;->d:Ljava/lang/Float;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/yandex/metrica/impl/bh$a$b;->d:Ljava/lang/Float;

    .line 79
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v3

    :goto_1
    iget-object v0, p1, Lcom/yandex/metrica/impl/bh$a$b;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/yandex/metrica/impl/bh$a$b;->e:Ljava/lang/Integer;

    .line 81
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    :goto_2
    iget-object v0, p1, Lcom/yandex/metrica/impl/bh$a$b;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/yandex/metrica/impl/bh$a$b;->f:Ljava/lang/Integer;

    .line 83
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    :goto_3
    iget-object v0, p1, Lcom/yandex/metrica/impl/bh$a$b;->g:Ljava/lang/Long;

    if-eqz v0, :cond_4

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v6, p1, Lcom/yandex/metrica/impl/bh$a$b;->g:Ljava/lang/Long;

    .line 85
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    :goto_4
    iget-object v0, p1, Lcom/yandex/metrica/impl/bh$a$b;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/yandex/metrica/impl/bh$a$b;->h:Ljava/lang/Integer;

    .line 87
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    :goto_5
    move-object v0, p0

    move v9, p2

    .line 76
    invoke-direct/range {v0 .. v9}, Lcom/yandex/metrica/impl/ob/dv;-><init>(JFIIJIZ)V

    .line 91
    return-void

    .line 77
    :cond_0
    sget-wide v1, Lcom/yandex/metrica/impl/ob/dv;->e:J

    goto :goto_0

    .line 79
    :cond_1
    const/high16 v3, 0x41200000    # 10.0f

    goto :goto_1

    .line 81
    :cond_2
    const/16 v4, 0x14

    goto :goto_2

    .line 83
    :cond_3
    const/16 v5, 0xc8

    goto :goto_3

    .line 85
    :cond_4
    sget-wide v6, Lcom/yandex/metrica/impl/ob/dv;->f:J

    goto :goto_4

    .line 87
    :cond_5
    const/16 v8, 0x2710

    goto :goto_5
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 10

    .prologue
    .line 109
    const-string v0, "uti"

    sget-wide v2, Lcom/yandex/metrica/impl/ob/dv;->e:J

    .line 110
    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v1

    const-string v0, "udi"

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    .line 112
    invoke-virtual {p1, v0, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v4

    double-to-float v3, v4

    const-string v0, "rcff"

    const/16 v4, 0x14

    .line 114
    invoke-virtual {p1, v0, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    const-string v0, "mbs"

    const/16 v5, 0xc8

    .line 116
    invoke-virtual {p1, v0, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v5

    const-string v0, "maff"

    sget-wide v6, Lcom/yandex/metrica/impl/ob/dv;->f:J

    .line 117
    invoke-virtual {p1, v0, v6, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v6

    const-string v0, "mrsl"

    const/16 v8, 0x2710

    .line 119
    invoke-virtual {p1, v0, v8}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v8

    const-string v0, "ce"

    const/4 v9, 0x0

    .line 121
    invoke-virtual {p1, v0, v9}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v9

    move-object v0, p0

    .line 109
    invoke-direct/range {v0 .. v9}, Lcom/yandex/metrica/impl/ob/dv;-><init>(JFIIJIZ)V

    .line 124
    return-void
.end method

.method public static b(Lcom/yandex/metrica/impl/ob/dg;)Lcom/yandex/metrica/impl/ob/dv;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 95
    .line 96
    invoke-virtual {p0, v1}, Lcom/yandex/metrica/impl/ob/dg;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 97
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 99
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 100
    new-instance v0, Lcom/yandex/metrica/impl/ob/dv;

    invoke-direct {v0, v2}, Lcom/yandex/metrica/impl/ob/dv;-><init>(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 134
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 135
    const-string v1, "uti"

    iget-wide v2, p0, Lcom/yandex/metrica/impl/ob/dv;->g:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 136
    const-string v1, "udi"

    iget v2, p0, Lcom/yandex/metrica/impl/ob/dv;->h:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 137
    const-string v1, "rcff"

    iget v2, p0, Lcom/yandex/metrica/impl/ob/dv;->i:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 138
    const-string v1, "mbs"

    iget v2, p0, Lcom/yandex/metrica/impl/ob/dv;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 139
    const-string v1, "maff"

    iget-wide v2, p0, Lcom/yandex/metrica/impl/ob/dv;->k:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 140
    const-string v1, "mrsl"

    iget v2, p0, Lcom/yandex/metrica/impl/ob/dv;->l:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 141
    const-string v1, "ce"

    iget-boolean v2, p0, Lcom/yandex/metrica/impl/ob/dv;->m:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 146
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    goto :goto_0
.end method

.method public b()Lcom/yandex/metrica/impl/ob/dv$a;
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lcom/yandex/metrica/impl/ob/dv$a;->a:Lcom/yandex/metrica/impl/ob/dv$a;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ForegroundLocationCollectionConfig{updateTimeInterval="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/yandex/metrica/impl/ob/dv;->g:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", updateDistanceInterval="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/yandex/metrica/impl/ob/dv;->h:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", recordsCountToForceFlush="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/yandex/metrica/impl/ob/dv;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxBatchSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/yandex/metrica/impl/ob/dv;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxAgeToForceFlush="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/yandex/metrica/impl/ob/dv;->k:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxRecordsToStoreLocally="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/yandex/metrica/impl/ob/dv;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", collectionEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/yandex/metrica/impl/ob/dv;->m:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
