.class final Lcom/google/obf/bq;
.super Ljava/lang/Object;
.source "IMASDK"


# instance fields
.field private final a:Lcom/google/obf/bs$b;

.field private final b:Lcom/google/obf/dw;

.field private final c:Lcom/google/obf/bs$a;

.field private d:I

.field private e:J


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/google/obf/bs$b;

    invoke-direct {v0}, Lcom/google/obf/bs$b;-><init>()V

    iput-object v0, p0, Lcom/google/obf/bq;->a:Lcom/google/obf/bs$b;

    .line 3
    new-instance v0, Lcom/google/obf/dw;

    const/16 v1, 0x11a

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/bq;->b:Lcom/google/obf/dw;

    .line 4
    new-instance v0, Lcom/google/obf/bs$a;

    invoke-direct {v0}, Lcom/google/obf/bs$a;-><init>()V

    iput-object v0, p0, Lcom/google/obf/bq;->c:Lcom/google/obf/bs$a;

    .line 5
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/obf/bq;->d:I

    return-void
.end method


# virtual methods
.method public a(Lcom/google/obf/ak;)J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-interface {p1}, Lcom/google/obf/ak;->d()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->a(Z)V

    .line 35
    invoke-static {p1}, Lcom/google/obf/bs;->a(Lcom/google/obf/ak;)V

    .line 36
    iget-object v0, p0, Lcom/google/obf/bq;->a:Lcom/google/obf/bs$b;

    invoke-virtual {v0}, Lcom/google/obf/bs$b;->a()V

    .line 37
    :goto_1
    iget-object v0, p0, Lcom/google/obf/bq;->a:Lcom/google/obf/bs$b;

    iget v0, v0, Lcom/google/obf/bs$b;->b:I

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x4

    if-eq v0, v2, :cond_1

    invoke-interface {p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v2

    invoke-interface {p1}, Lcom/google/obf/ak;->d()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gez v0, :cond_1

    .line 38
    iget-object v0, p0, Lcom/google/obf/bq;->a:Lcom/google/obf/bs$b;

    iget-object v2, p0, Lcom/google/obf/bq;->b:Lcom/google/obf/dw;

    invoke-static {p1, v0, v2, v1}, Lcom/google/obf/bs;->a(Lcom/google/obf/ak;Lcom/google/obf/bs$b;Lcom/google/obf/dw;Z)Z

    .line 39
    iget-object v0, p0, Lcom/google/obf/bq;->a:Lcom/google/obf/bs$b;

    iget v0, v0, Lcom/google/obf/bs$b;->h:I

    iget-object v2, p0, Lcom/google/obf/bq;->a:Lcom/google/obf/bs$b;

    iget v2, v2, Lcom/google/obf/bs$b;->i:I

    add-int/2addr v0, v2

    invoke-interface {p1, v0}, Lcom/google/obf/ak;->b(I)V

    goto :goto_1

    :cond_0
    move v0, v1

    .line 34
    goto :goto_0

    .line 40
    :cond_1
    iget-object v0, p0, Lcom/google/obf/bq;->a:Lcom/google/obf/bs$b;

    iget-wide v0, v0, Lcom/google/obf/bs$b;->c:J

    return-wide v0
.end method

.method public a(Lcom/google/obf/ak;J)J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 41
    invoke-static {p1}, Lcom/google/obf/bs;->a(Lcom/google/obf/ak;)V

    .line 42
    iget-object v0, p0, Lcom/google/obf/bq;->a:Lcom/google/obf/bs$b;

    iget-object v1, p0, Lcom/google/obf/bq;->b:Lcom/google/obf/dw;

    invoke-static {p1, v0, v1, v2}, Lcom/google/obf/bs;->a(Lcom/google/obf/ak;Lcom/google/obf/bs$b;Lcom/google/obf/dw;Z)Z

    .line 43
    :goto_0
    iget-object v0, p0, Lcom/google/obf/bq;->a:Lcom/google/obf/bs$b;

    iget-wide v0, v0, Lcom/google/obf/bs$b;->c:J

    cmp-long v0, v0, p2

    if-gez v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/google/obf/bq;->a:Lcom/google/obf/bs$b;

    iget v0, v0, Lcom/google/obf/bs$b;->h:I

    iget-object v1, p0, Lcom/google/obf/bq;->a:Lcom/google/obf/bs$b;

    iget v1, v1, Lcom/google/obf/bs$b;->i:I

    add-int/2addr v0, v1

    invoke-interface {p1, v0}, Lcom/google/obf/ak;->b(I)V

    .line 45
    iget-object v0, p0, Lcom/google/obf/bq;->a:Lcom/google/obf/bs$b;

    iget-wide v0, v0, Lcom/google/obf/bs$b;->c:J

    iput-wide v0, p0, Lcom/google/obf/bq;->e:J

    .line 46
    iget-object v0, p0, Lcom/google/obf/bq;->a:Lcom/google/obf/bs$b;

    iget-object v1, p0, Lcom/google/obf/bq;->b:Lcom/google/obf/dw;

    invoke-static {p1, v0, v1, v2}, Lcom/google/obf/bs;->a(Lcom/google/obf/ak;Lcom/google/obf/bs$b;Lcom/google/obf/dw;Z)Z

    goto :goto_0

    .line 47
    :cond_0
    iget-wide v0, p0, Lcom/google/obf/bq;->e:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    .line 48
    new-instance v0, Lcom/google/obf/s;

    invoke-direct {v0}, Lcom/google/obf/s;-><init>()V

    throw v0

    .line 49
    :cond_1
    invoke-interface {p1}, Lcom/google/obf/ak;->a()V

    .line 50
    iget-wide v0, p0, Lcom/google/obf/bq;->e:J

    .line 51
    iput-wide v4, p0, Lcom/google/obf/bq;->e:J

    .line 52
    const/4 v2, -0x1

    iput v2, p0, Lcom/google/obf/bq;->d:I

    .line 53
    return-wide v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lcom/google/obf/bq;->a:Lcom/google/obf/bs$b;

    invoke-virtual {v0}, Lcom/google/obf/bs$b;->a()V

    .line 7
    iget-object v0, p0, Lcom/google/obf/bq;->b:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->a()V

    .line 8
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/obf/bq;->d:I

    .line 9
    return-void
.end method

.method public a(Lcom/google/obf/ak;Lcom/google/obf/dw;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 10
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    move v4, v2

    .line 12
    :goto_1
    if-nez v4, :cond_5

    .line 13
    iget v0, p0, Lcom/google/obf/bq;->d:I

    if-gez v0, :cond_2

    .line 14
    iget-object v0, p0, Lcom/google/obf/bq;->a:Lcom/google/obf/bs$b;

    iget-object v3, p0, Lcom/google/obf/bq;->b:Lcom/google/obf/dw;

    invoke-static {p1, v0, v3, v1}, Lcom/google/obf/bs;->a(Lcom/google/obf/ak;Lcom/google/obf/bs$b;Lcom/google/obf/dw;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 33
    :goto_2
    return v2

    :cond_0
    move v0, v2

    .line 10
    goto :goto_0

    .line 17
    :cond_1
    iget-object v0, p0, Lcom/google/obf/bq;->a:Lcom/google/obf/bs$b;

    iget v0, v0, Lcom/google/obf/bs$b;->h:I

    .line 18
    iget-object v3, p0, Lcom/google/obf/bq;->a:Lcom/google/obf/bs$b;

    iget v3, v3, Lcom/google/obf/bs$b;->b:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_7

    invoke-virtual {p2}, Lcom/google/obf/dw;->c()I

    move-result v3

    if-nez v3, :cond_7

    .line 19
    iget-object v3, p0, Lcom/google/obf/bq;->a:Lcom/google/obf/bs$b;

    iget-object v5, p0, Lcom/google/obf/bq;->c:Lcom/google/obf/bs$a;

    invoke-static {v3, v2, v5}, Lcom/google/obf/bs;->a(Lcom/google/obf/bs$b;ILcom/google/obf/bs$a;)V

    .line 20
    iget-object v3, p0, Lcom/google/obf/bq;->c:Lcom/google/obf/bs$a;

    iget v3, v3, Lcom/google/obf/bs$a;->b:I

    add-int/2addr v3, v2

    .line 21
    iget-object v5, p0, Lcom/google/obf/bq;->c:Lcom/google/obf/bs$a;

    iget v5, v5, Lcom/google/obf/bs$a;->a:I

    add-int/2addr v0, v5

    .line 22
    :goto_3
    invoke-interface {p1, v0}, Lcom/google/obf/ak;->b(I)V

    .line 23
    iput v3, p0, Lcom/google/obf/bq;->d:I

    .line 24
    :cond_2
    iget-object v0, p0, Lcom/google/obf/bq;->a:Lcom/google/obf/bs$b;

    iget v3, p0, Lcom/google/obf/bq;->d:I

    iget-object v5, p0, Lcom/google/obf/bq;->c:Lcom/google/obf/bs$a;

    invoke-static {v0, v3, v5}, Lcom/google/obf/bs;->a(Lcom/google/obf/bs$b;ILcom/google/obf/bs$a;)V

    .line 25
    iget v0, p0, Lcom/google/obf/bq;->d:I

    iget-object v3, p0, Lcom/google/obf/bq;->c:Lcom/google/obf/bs$a;

    iget v3, v3, Lcom/google/obf/bs$a;->b:I

    add-int/2addr v3, v0

    .line 26
    iget-object v0, p0, Lcom/google/obf/bq;->c:Lcom/google/obf/bs$a;

    iget v0, v0, Lcom/google/obf/bs$a;->a:I

    if-lez v0, :cond_6

    .line 27
    iget-object v0, p2, Lcom/google/obf/dw;->a:[B

    invoke-virtual {p2}, Lcom/google/obf/dw;->c()I

    move-result v4

    iget-object v5, p0, Lcom/google/obf/bq;->c:Lcom/google/obf/bs$a;

    iget v5, v5, Lcom/google/obf/bs$a;->a:I

    invoke-interface {p1, v0, v4, v5}, Lcom/google/obf/ak;->b([BII)V

    .line 28
    invoke-virtual {p2}, Lcom/google/obf/dw;->c()I

    move-result v0

    iget-object v4, p0, Lcom/google/obf/bq;->c:Lcom/google/obf/bs$a;

    iget v4, v4, Lcom/google/obf/bs$a;->a:I

    add-int/2addr v0, v4

    invoke-virtual {p2, v0}, Lcom/google/obf/dw;->b(I)V

    .line 29
    iget-object v0, p0, Lcom/google/obf/bq;->a:Lcom/google/obf/bs$b;

    iget-object v0, v0, Lcom/google/obf/bs$b;->j:[I

    add-int/lit8 v4, v3, -0x1

    aget v0, v0, v4

    const/16 v4, 0xff

    if-eq v0, v4, :cond_4

    move v0, v1

    .line 30
    :goto_4
    iget-object v4, p0, Lcom/google/obf/bq;->a:Lcom/google/obf/bs$b;

    iget v4, v4, Lcom/google/obf/bs$b;->g:I

    if-ne v3, v4, :cond_3

    const/4 v3, -0x1

    .line 31
    :cond_3
    iput v3, p0, Lcom/google/obf/bq;->d:I

    move v4, v0

    .line 32
    goto/16 :goto_1

    :cond_4
    move v0, v2

    .line 29
    goto :goto_4

    :cond_5
    move v2, v1

    .line 33
    goto :goto_2

    :cond_6
    move v0, v4

    goto :goto_4

    :cond_7
    move v3, v2

    goto :goto_3
.end method
