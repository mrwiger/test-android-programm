.class final Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;
.super Lru/cn/ad/natives/adapters/AdmobNativeAdapter$AdmobBinder;
.source "AdmobNativeAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/ad/natives/adapters/AdmobNativeAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "InstallBinder"
.end annotation


# instance fields
.field private adView:Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;

.field private final installAd:Lcom/google/android/gms/ads/formats/NativeAppInstallAd;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/formats/NativeAppInstallAd;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "installAd"    # Lcom/google/android/gms/ads/formats/NativeAppInstallAd;

    .prologue
    .line 150
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$AdmobBinder;-><init>(Lru/cn/ad/natives/adapters/AdmobNativeAdapter$1;)V

    .line 151
    iput-object p2, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;->installAd:Lcom/google/android/gms/ads/formats/NativeAppInstallAd;

    .line 153
    new-instance v0, Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;

    invoke-direct {v0, p1}, Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;->adView:Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;

    .line 154
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;->adView:Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;->setNativeAd(Lcom/google/android/gms/ads/formats/NativeAd;)V

    .line 155
    return-void
.end method


# virtual methods
.method public bind(Landroid/widget/Button;I)Z
    .locals 1
    .param p1, "view"    # Landroid/widget/Button;
    .param p2, "componentType"    # I

    .prologue
    .line 208
    packed-switch p2, :pswitch_data_0

    .line 214
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 210
    :pswitch_0
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;->adView:Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;->setCallToActionView(Landroid/view/View;)V

    .line 211
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;->installAd:Lcom/google/android/gms/ads/formats/NativeAppInstallAd;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/formats/NativeAppInstallAd;->getCallToAction()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 212
    const/4 v0, 0x1

    goto :goto_0

    .line 208
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public bind(Landroid/widget/ImageView;I)Z
    .locals 3
    .param p1, "view"    # Landroid/widget/ImageView;
    .param p2, "componentType"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 188
    packed-switch p2, :pswitch_data_0

    .line 203
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 190
    :pswitch_1
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;->adView:Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;->setIconView(Landroid/view/View;)V

    .line 191
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;->installAd:Lcom/google/android/gms/ads/formats/NativeAppInstallAd;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/formats/NativeAppInstallAd;->getIcon()Lcom/google/android/gms/ads/formats/NativeAd$Image;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/ads/formats/NativeAd$Image;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move v0, v1

    .line 192
    goto :goto_0

    .line 195
    :pswitch_2
    iget-object v2, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;->installAd:Lcom/google/android/gms/ads/formats/NativeAppInstallAd;

    invoke-virtual {v2}, Lcom/google/android/gms/ads/formats/NativeAppInstallAd;->getImages()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 196
    iget-object v2, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;->adView:Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;

    invoke-virtual {v2, p1}, Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;->setImageView(Landroid/view/View;)V

    .line 197
    iget-object v2, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;->installAd:Lcom/google/android/gms/ads/formats/NativeAppInstallAd;

    invoke-virtual {v2}, Lcom/google/android/gms/ads/formats/NativeAppInstallAd;->getImages()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/formats/NativeAd$Image;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/formats/NativeAd$Image;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move v0, v1

    .line 198
    goto :goto_0

    .line 188
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public bind(Landroid/widget/TextView;I)Z
    .locals 3
    .param p1, "view"    # Landroid/widget/TextView;
    .param p2, "componentType"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 164
    packed-switch p2, :pswitch_data_0

    :pswitch_0
    move v0, v1

    .line 183
    :goto_0
    return v0

    .line 166
    :pswitch_1
    iget-object v1, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;->adView:Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;->setHeadlineView(Landroid/view/View;)V

    .line 167
    iget-object v1, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;->installAd:Lcom/google/android/gms/ads/formats/NativeAppInstallAd;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/formats/NativeAppInstallAd;->getHeadline()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 171
    :pswitch_2
    iget-object v1, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;->adView:Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;->setCallToActionView(Landroid/view/View;)V

    .line 172
    iget-object v1, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;->installAd:Lcom/google/android/gms/ads/formats/NativeAppInstallAd;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/formats/NativeAppInstallAd;->getCallToAction()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 176
    :pswitch_3
    iget-object v2, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;->installAd:Lcom/google/android/gms/ads/formats/NativeAppInstallAd;

    invoke-virtual {v2}, Lcom/google/android/gms/ads/formats/NativeAppInstallAd;->getBody()Ljava/lang/CharSequence;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    .line 177
    goto :goto_0

    .line 179
    :cond_0
    iget-object v1, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;->adView:Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;->setBodyView(Landroid/view/View;)V

    .line 180
    iget-object v1, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;->installAd:Lcom/google/android/gms/ads/formats/NativeAppInstallAd;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/formats/NativeAppInstallAd;->getBody()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 164
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;->installAd:Lcom/google/android/gms/ads/formats/NativeAppInstallAd;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/formats/NativeAppInstallAd;->destroy()V

    .line 226
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;->adView:Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;->removeAllViews()V

    .line 227
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;->adView:Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;->destroy()V

    .line 228
    return-void
.end method

.method public getAdType()I
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x2

    return v0
.end method

.method public registerTemplate(Landroid/view/View;Ljava/util/List;)Landroid/view/View;
    .locals 1
    .param p1, "templateView"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 219
    .local p2, "clickableViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;->adView:Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;->addView(Landroid/view/View;)V

    .line 220
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;->adView:Lcom/google/android/gms/ads/formats/NativeAppInstallAdView;

    return-object v0
.end method
