.class public Lcom/my/target/ad;
.super Ljava/lang/Object;
.source "MraidScreenMetrics.java"


# instance fields
.field private final bJ:Landroid/content/Context;

.field private final bK:Landroid/graphics/Rect;

.field private final bL:Landroid/graphics/Rect;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/ad;->bJ:Landroid/content/Context;

    .line 24
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/my/target/ad;->bK:Landroid/graphics/Rect;

    .line 25
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/my/target/ad;->bL:Landroid/graphics/Rect;

    .line 26
    return-void
.end method

.method private a(Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 5

    .prologue
    .line 57
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/my/target/ad;->bJ:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(ILandroid/content/Context;)I

    move-result v0

    iget v1, p1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/my/target/ad;->bJ:Landroid/content/Context;

    .line 58
    invoke-static {v1, v2}, Lcom/my/target/cm;->a(ILandroid/content/Context;)I

    move-result v1

    iget v2, p1, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/my/target/ad;->bJ:Landroid/content/Context;

    .line 59
    invoke-static {v2, v3}, Lcom/my/target/cm;->a(ILandroid/content/Context;)I

    move-result v2

    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, Lcom/my/target/ad;->bJ:Landroid/content/Context;

    .line 60
    invoke-static {v3, v4}, Lcom/my/target/cm;->a(ILandroid/content/Context;)I

    move-result v3

    .line 57
    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 61
    return-void
.end method

.method public static d(Landroid/content/Context;)Lcom/my/target/ad;
    .locals 1

    .prologue
    .line 13
    new-instance v0, Lcom/my/target/ad;

    invoke-direct {v0, p0}, Lcom/my/target/ad;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public a(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    iget-object v0, p0, Lcom/my/target/ad;->bK:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v1, p1, p2}, Landroid/graphics/Rect;->set(IIII)V

    .line 31
    iget-object v0, p0, Lcom/my/target/ad;->bK:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/my/target/ad;->bL:Landroid/graphics/Rect;

    invoke-direct {p0, v0, v1}, Lcom/my/target/ad;->a(Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 32
    return-void
.end method

.method public u()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/my/target/ad;->bL:Landroid/graphics/Rect;

    return-object v0
.end method

.method public v()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/my/target/ad;->bL:Landroid/graphics/Rect;

    return-object v0
.end method

.method public w()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/my/target/ad;->bL:Landroid/graphics/Rect;

    return-object v0
.end method

.method x()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/my/target/ad;->bL:Landroid/graphics/Rect;

    return-object v0
.end method
