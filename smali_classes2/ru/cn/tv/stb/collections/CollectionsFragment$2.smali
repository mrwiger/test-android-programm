.class Lru/cn/tv/stb/collections/CollectionsFragment$2;
.super Landroid/support/v7/widget/RecyclerView$OnScrollListener;
.source "CollectionsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/stb/collections/CollectionsFragment;->createCollectionView(Lru/cn/tv/stb/collections/CollectionInfo;)Lru/cn/tv/stb/collections/StbCollectionView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/stb/collections/CollectionsFragment;

.field final synthetic val$adapter:Lru/cn/tv/stb/collections/CollectionTelecastAdapter;

.field final synthetic val$collection:Lru/cn/tv/stb/collections/CollectionInfo;

.field final synthetic val$collectionLayout:Landroid/support/v7/widget/LinearLayoutManager;

.field final synthetic val$collectionView:Lru/cn/tv/stb/collections/StbCollectionView;


# direct methods
.method constructor <init>(Lru/cn/tv/stb/collections/CollectionsFragment;Lru/cn/tv/stb/collections/CollectionTelecastAdapter;Landroid/support/v7/widget/LinearLayoutManager;Lru/cn/tv/stb/collections/StbCollectionView;Lru/cn/tv/stb/collections/CollectionInfo;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/stb/collections/CollectionsFragment;

    .prologue
    .line 157
    iput-object p1, p0, Lru/cn/tv/stb/collections/CollectionsFragment$2;->this$0:Lru/cn/tv/stb/collections/CollectionsFragment;

    iput-object p2, p0, Lru/cn/tv/stb/collections/CollectionsFragment$2;->val$adapter:Lru/cn/tv/stb/collections/CollectionTelecastAdapter;

    iput-object p3, p0, Lru/cn/tv/stb/collections/CollectionsFragment$2;->val$collectionLayout:Landroid/support/v7/widget/LinearLayoutManager;

    iput-object p4, p0, Lru/cn/tv/stb/collections/CollectionsFragment$2;->val$collectionView:Lru/cn/tv/stb/collections/StbCollectionView;

    iput-object p5, p0, Lru/cn/tv/stb/collections/CollectionsFragment$2;->val$collection:Lru/cn/tv/stb/collections/CollectionInfo;

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$OnScrollListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrolled(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 4
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "dx"    # I
    .param p3, "dy"    # I

    .prologue
    .line 161
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView$OnScrollListener;->onScrolled(Landroid/support/v7/widget/RecyclerView;II)V

    .line 162
    iget-object v2, p0, Lru/cn/tv/stb/collections/CollectionsFragment$2;->this$0:Lru/cn/tv/stb/collections/CollectionsFragment;

    invoke-static {v2}, Lru/cn/tv/stb/collections/CollectionsFragment;->access$000(Lru/cn/tv/stb/collections/CollectionsFragment;)Lru/cn/tv/stb/collections/CollectionsFragment$CollectionListener;

    move-result-object v2

    invoke-interface {v2}, Lru/cn/tv/stb/collections/CollectionsFragment$CollectionListener;->onScroll()V

    .line 164
    if-lez p2, :cond_0

    .line 165
    iget-object v2, p0, Lru/cn/tv/stb/collections/CollectionsFragment$2;->val$adapter:Lru/cn/tv/stb/collections/CollectionTelecastAdapter;

    invoke-virtual {v2}, Lru/cn/tv/stb/collections/CollectionTelecastAdapter;->getItemCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .line 167
    .local v0, "itemCount":I
    iget-object v2, p0, Lru/cn/tv/stb/collections/CollectionsFragment$2;->val$collectionLayout:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v2}, Landroid/support/v7/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    move-result v1

    .line 168
    .local v1, "lastVisiblePos":I
    add-int/lit8 v2, v0, -0x5

    if-ne v1, v2, :cond_0

    const/16 v2, 0x21

    if-le v0, v2, :cond_0

    .line 169
    iget-object v2, p0, Lru/cn/tv/stb/collections/CollectionsFragment$2;->this$0:Lru/cn/tv/stb/collections/CollectionsFragment;

    invoke-static {v2}, Lru/cn/tv/stb/collections/CollectionsFragment;->access$100(Lru/cn/tv/stb/collections/CollectionsFragment;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 170
    iget-object v2, p0, Lru/cn/tv/stb/collections/CollectionsFragment$2;->this$0:Lru/cn/tv/stb/collections/CollectionsFragment;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lru/cn/tv/stb/collections/CollectionsFragment;->access$102(Lru/cn/tv/stb/collections/CollectionsFragment;Z)Z

    .line 172
    iget-object v2, p0, Lru/cn/tv/stb/collections/CollectionsFragment$2;->val$collectionView:Lru/cn/tv/stb/collections/StbCollectionView;

    invoke-virtual {v2}, Lru/cn/tv/stb/collections/StbCollectionView;->showProgress()V

    .line 174
    iget-object v2, p0, Lru/cn/tv/stb/collections/CollectionsFragment$2;->this$0:Lru/cn/tv/stb/collections/CollectionsFragment;

    invoke-static {v2}, Lru/cn/tv/stb/collections/CollectionsFragment;->access$200(Lru/cn/tv/stb/collections/CollectionsFragment;)Lru/cn/tv/stb/collections/CollectionsViewModel;

    move-result-object v2

    iget-object v3, p0, Lru/cn/tv/stb/collections/CollectionsFragment$2;->val$collection:Lru/cn/tv/stb/collections/CollectionInfo;

    invoke-virtual {v2, v3}, Lru/cn/tv/stb/collections/CollectionsViewModel;->loadCollection(Lru/cn/tv/stb/collections/CollectionInfo;)V

    .line 178
    .end local v0    # "itemCount":I
    .end local v1    # "lastVisiblePos":I
    :cond_0
    return-void
.end method
