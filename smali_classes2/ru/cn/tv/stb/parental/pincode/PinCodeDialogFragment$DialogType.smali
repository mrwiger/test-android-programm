.class public final enum Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;
.super Ljava/lang/Enum;
.source "PinCodeDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DialogType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

.field public static final enum changePin:Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

.field public static final enum enterPin:Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

.field public static final enum error:Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

.field public static final enum showPin:Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    new-instance v0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    const-string v1, "enterPin"

    invoke-direct {v0, v1, v2}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;->enterPin:Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    .line 32
    new-instance v0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    const-string v1, "changePin"

    invoke-direct {v0, v1, v3}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;->changePin:Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    .line 33
    new-instance v0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    const-string v1, "showPin"

    invoke-direct {v0, v1, v4}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;->showPin:Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    .line 34
    new-instance v0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    const-string v1, "error"

    invoke-direct {v0, v1, v5}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;->error:Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    .line 30
    const/4 v0, 0x4

    new-array v0, v0, [Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    sget-object v1, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;->enterPin:Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    aput-object v1, v0, v2

    sget-object v1, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;->changePin:Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    aput-object v1, v0, v3

    sget-object v1, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;->showPin:Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    aput-object v1, v0, v4

    sget-object v1, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;->error:Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    aput-object v1, v0, v5

    sput-object v0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;->$VALUES:[Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 30
    const-class v0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    return-object v0
.end method

.method public static values()[Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;->$VALUES:[Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    invoke-virtual {v0}, [Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    return-object v0
.end method
