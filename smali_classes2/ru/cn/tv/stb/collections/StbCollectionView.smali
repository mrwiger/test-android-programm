.class public Lru/cn/tv/stb/collections/StbCollectionView;
.super Landroid/widget/LinearLayout;
.source "StbCollectionView.java"


# instance fields
.field private collectionDivider:Landroid/view/View;

.field private collectionRecycler:Landroid/support/v7/widget/RecyclerView;

.field private progress:Landroid/widget/ProgressBar;

.field private title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lru/cn/tv/stb/collections/StbCollectionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 36
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f0c007a

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 37
    invoke-virtual {p0}, Lru/cn/tv/stb/collections/StbCollectionView;->onFinishInflate()V

    .line 38
    return-void
.end method


# virtual methods
.method public getCollectionRecycler()Landroid/support/v7/widget/RecyclerView;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lru/cn/tv/stb/collections/StbCollectionView;->collectionRecycler:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method

.method public hideProgress()V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lru/cn/tv/stb/collections/StbCollectionView;->progress:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 94
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 47
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 48
    const v0, 0x7f09007d

    invoke-virtual {p0, v0}, Lru/cn/tv/stb/collections/StbCollectionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/tv/stb/collections/StbCollectionView;->title:Landroid/widget/TextView;

    .line 49
    const v0, 0x7f09007e

    invoke-virtual {p0, v0}, Lru/cn/tv/stb/collections/StbCollectionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lru/cn/tv/stb/collections/StbCollectionView;->progress:Landroid/widget/ProgressBar;

    .line 50
    const v0, 0x7f09007c

    invoke-virtual {p0, v0}, Lru/cn/tv/stb/collections/StbCollectionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lru/cn/tv/stb/collections/StbCollectionView;->collectionRecycler:Landroid/support/v7/widget/RecyclerView;

    .line 51
    const v0, 0x7f090079

    invoke-virtual {p0, v0}, Lru/cn/tv/stb/collections/StbCollectionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/stb/collections/StbCollectionView;->collectionDivider:Landroid/view/View;

    .line 53
    iget-object v0, p0, Lru/cn/tv/stb/collections/StbCollectionView;->collectionRecycler:Landroid/support/v7/widget/RecyclerView;

    const/high16 v1, 0x40000

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setDescendantFocusability(I)V

    .line 54
    return-void
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .locals 3
    .param p1, "direction"    # I
    .param p2, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 76
    iget-object v1, p0, Lru/cn/tv/stb/collections/StbCollectionView;->collectionRecycler:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 77
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    move-result v1

    .line 80
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lru/cn/tv/stb/collections/StbCollectionView;->collectionRecycler:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->requestFocus()Z

    move-result v1

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 57
    iget-object v0, p0, Lru/cn/tv/stb/collections/StbCollectionView;->title:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    return-void
.end method

.method public show()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-virtual {p0, v1}, Lru/cn/tv/stb/collections/StbCollectionView;->setVisibility(I)V

    .line 66
    iget-object v0, p0, Lru/cn/tv/stb/collections/StbCollectionView;->collectionRecycler:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 67
    return-void
.end method

.method public showDivider()V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lru/cn/tv/stb/collections/StbCollectionView;->collectionDivider:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 86
    return-void
.end method

.method public showProgress()V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lru/cn/tv/stb/collections/StbCollectionView;->progress:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 90
    return-void
.end method
