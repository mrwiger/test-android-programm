.class Lcom/samsung/multiscreen/Application$6;
.super Ljava/lang/Object;
.source "Application.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/multiscreen/Application;->doApplicationCallback(Lcom/samsung/multiscreen/Result;Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/multiscreen/Application;

.field final synthetic val$errorMap:Ljava/util/Map;

.field final synthetic val$obj:Ljava/lang/Object;

.field final synthetic val$result:Lcom/samsung/multiscreen/Result;


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/Application;Ljava/util/Map;Lcom/samsung/multiscreen/Result;Ljava/lang/Object;)V
    .locals 0
    .param p1, "this$0"    # Lcom/samsung/multiscreen/Application;

    .prologue
    .line 418
    iput-object p1, p0, Lcom/samsung/multiscreen/Application$6;->this$0:Lcom/samsung/multiscreen/Application;

    iput-object p2, p0, Lcom/samsung/multiscreen/Application$6;->val$errorMap:Ljava/util/Map;

    iput-object p3, p0, Lcom/samsung/multiscreen/Application$6;->val$result:Lcom/samsung/multiscreen/Result;

    iput-object p4, p0, Lcom/samsung/multiscreen/Application$6;->val$obj:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 422
    iget-object v5, p0, Lcom/samsung/multiscreen/Application$6;->val$errorMap:Ljava/util/Map;

    if-eqz v5, :cond_3

    .line 423
    const-wide/16 v0, -0x1

    .line 425
    .local v0, "code":J
    :try_start_0
    iget-object v5, p0, Lcom/samsung/multiscreen/Application$6;->val$errorMap:Ljava/util/Map;

    const-string v6, "code"

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 426
    .local v2, "codeObj":Ljava/lang/Object;
    instance-of v5, v2, Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 427
    check-cast v2, Ljava/lang/String;

    .end local v2    # "codeObj":Ljava/lang/Object;
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v0

    .line 435
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/samsung/multiscreen/Application$6;->val$result:Lcom/samsung/multiscreen/Result;

    iget-object v6, p0, Lcom/samsung/multiscreen/Application$6;->val$errorMap:Ljava/util/Map;

    invoke-static {v0, v1, v6}, Lcom/samsung/multiscreen/Error;->create(JLjava/util/Map;)Lcom/samsung/multiscreen/Error;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/samsung/multiscreen/Result;->onError(Lcom/samsung/multiscreen/Error;)V

    .line 449
    .end local v0    # "code":J
    :goto_1
    return-void

    .line 428
    .restart local v0    # "code":J
    .restart local v2    # "codeObj":Ljava/lang/Object;
    :cond_1
    :try_start_1
    instance-of v5, v2, Ljava/lang/Integer;

    if-eqz v5, :cond_2

    .line 429
    check-cast v2, Ljava/lang/Integer;

    .end local v2    # "codeObj":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-long v0, v5

    goto :goto_0

    .line 430
    .restart local v2    # "codeObj":Ljava/lang/Object;
    :cond_2
    instance-of v5, v2, Ljava/lang/Long;

    if-eqz v5, :cond_0

    .line 431
    check-cast v2, Ljava/lang/Long;

    .end local v2    # "codeObj":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v0

    goto :goto_0

    .line 437
    .end local v0    # "code":J
    :cond_3
    const/4 v4, 0x0

    .line 438
    .local v4, "info":Lcom/samsung/multiscreen/ApplicationInfo;
    iget-object v5, p0, Lcom/samsung/multiscreen/Application$6;->val$obj:Ljava/lang/Object;

    instance-of v5, v5, Ljava/util/Map;

    if-eqz v5, :cond_4

    .line 440
    :try_start_2
    iget-object v5, p0, Lcom/samsung/multiscreen/Application$6;->val$obj:Ljava/lang/Object;

    check-cast v5, Ljava/util/Map;

    invoke-static {v5}, Lcom/samsung/multiscreen/ApplicationInfo;->create(Ljava/util/Map;)Lcom/samsung/multiscreen/ApplicationInfo;

    move-result-object v4

    .line 441
    iget-object v5, p0, Lcom/samsung/multiscreen/Application$6;->val$result:Lcom/samsung/multiscreen/Result;

    invoke-interface {v5, v4}, Lcom/samsung/multiscreen/Result;->onSuccess(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 442
    :catch_0
    move-exception v3

    .line 443
    .local v3, "e":Ljava/lang/NullPointerException;
    iget-object v5, p0, Lcom/samsung/multiscreen/Application$6;->val$result:Lcom/samsung/multiscreen/Result;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unexpected response: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/multiscreen/Application$6;->val$obj:Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/multiscreen/Error;->create(Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/samsung/multiscreen/Result;->onError(Lcom/samsung/multiscreen/Error;)V

    goto :goto_1

    .line 446
    .end local v3    # "e":Ljava/lang/NullPointerException;
    :cond_4
    iget-object v5, p0, Lcom/samsung/multiscreen/Application$6;->val$result:Lcom/samsung/multiscreen/Result;

    iget-object v6, p0, Lcom/samsung/multiscreen/Application$6;->val$obj:Ljava/lang/Object;

    invoke-interface {v5, v6}, Lcom/samsung/multiscreen/Result;->onSuccess(Ljava/lang/Object;)V

    goto :goto_1

    .line 433
    .end local v4    # "info":Lcom/samsung/multiscreen/ApplicationInfo;
    .restart local v0    # "code":J
    :catch_1
    move-exception v5

    goto :goto_0
.end method
