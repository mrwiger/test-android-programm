.class public Lru/cn/api/provider/Schedule;
.super Ljava/lang/Object;
.source "Schedule.java"


# instance fields
.field private currentTelecast:Lru/cn/api/tv/replies/Telecast;

.field private telecasts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/tv/replies/Telecast;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/api/provider/Schedule;->telecasts:Ljava/util/List;

    return-void
.end method

.method private inRangeOfTelecast(Lru/cn/api/tv/replies/Telecast;J)Z
    .locals 8
    .param p1, "telecast"    # Lru/cn/api/tv/replies/Telecast;
    .param p2, "utcSeconds"    # J

    .prologue
    const/4 v4, 0x0

    .line 149
    iget-object v5, p1, Lru/cn/api/tv/replies/Telecast;->date:Lru/cn/api/tv/replies/DateTime;

    invoke-virtual {v5}, Lru/cn/api/tv/replies/DateTime;->toSeconds()J

    move-result-wide v2

    .line 150
    .local v2, "time":J
    iget-wide v0, p1, Lru/cn/api/tv/replies/Telecast;->duration:J

    .line 151
    .local v0, "duration":J
    cmp-long v5, p2, v2

    if-gez v5, :cond_1

    .line 160
    :cond_0
    :goto_0
    return v4

    .line 154
    :cond_1
    cmp-long v5, p2, v2

    if-ltz v5, :cond_0

    .line 155
    add-long v6, v2, v0

    cmp-long v5, p2, v6

    if-gez v5, :cond_0

    .line 156
    const/4 v4, 0x1

    goto :goto_0
.end method

.method private telecastsByDate(Ljava/util/Calendar;)Ljava/util/List;
    .locals 10
    .param p1, "date"    # Ljava/util/Calendar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            ")",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/tv/replies/Telecast;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x3e8

    .line 127
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    div-long v4, v6, v8

    .line 128
    .local v4, "startTimeSec":J
    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 129
    .local v0, "endTimeCalendar":Ljava/util/Calendar;
    const/4 v1, 0x5

    const/4 v6, 0x1

    invoke-virtual {v0, v1, v6}, Ljava/util/Calendar;->add(II)V

    .line 130
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    div-long v2, v6, v8

    .line 132
    .local v2, "endTimeSec":J
    invoke-direct {p0, v4, v5, v2, v3}, Lru/cn/api/provider/Schedule;->telecastsByPeriod(JJ)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method private telecastsByPeriod(JJ)Ljava/util/List;
    .locals 7
    .param p1, "startTimestamp"    # J
    .param p3, "endTimestamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/tv/replies/Telecast;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 137
    .local v1, "thatDays":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    iget-object v4, p0, Lru/cn/api/provider/Schedule;->telecasts:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/tv/replies/Telecast;

    .line 138
    .local v0, "telecast":Lru/cn/api/tv/replies/Telecast;
    iget-object v5, v0, Lru/cn/api/tv/replies/Telecast;->date:Lru/cn/api/tv/replies/DateTime;

    invoke-virtual {v5}, Lru/cn/api/tv/replies/DateTime;->toSeconds()J

    move-result-wide v2

    .line 140
    .local v2, "telecastStartTime":J
    cmp-long v5, v2, p1

    if-ltz v5, :cond_0

    cmp-long v5, v2, p3

    if-gez v5, :cond_0

    .line 141
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 145
    .end local v0    # "telecast":Lru/cn/api/tv/replies/Telecast;
    .end local v2    # "telecastStartTime":J
    :cond_1
    return-object v1
.end method


# virtual methods
.method public add(Ljava/util/List;)I
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/tv/replies/Telecast;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 65
    .local p1, "telecasts":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 66
    const/4 v7, 0x0

    .line 104
    :goto_0
    return v7

    .line 68
    :cond_0
    iget-object v7, p0, Lru/cn/api/provider/Schedule;->telecasts:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 69
    iget-object v7, p0, Lru/cn/api/provider/Schedule;->telecasts:Ljava/util/List;

    invoke-interface {v7, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 70
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    goto :goto_0

    .line 73
    :cond_1
    iget-object v7, p0, Lru/cn/api/provider/Schedule;->telecasts:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    .line 75
    .local v0, "before":I
    const/4 v1, 0x0

    .local v1, "index":I
    const/4 v2, 0x0

    .line 76
    .local v2, "indexInsertion":I
    :cond_2
    iget-object v7, p0, Lru/cn/api/provider/Schedule;->telecasts:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ge v1, v7, :cond_4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    if-ge v2, v7, :cond_4

    .line 77
    iget-object v7, p0, Lru/cn/api/provider/Schedule;->telecasts:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lru/cn/api/tv/replies/Telecast;

    .line 78
    .local v6, "telecast":Lru/cn/api/tv/replies/Telecast;
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lru/cn/api/tv/replies/Telecast;

    .line 80
    .local v3, "insertingTelecast":Lru/cn/api/tv/replies/Telecast;
    iget-wide v10, v6, Lru/cn/api/tv/replies/Telecast;->id:J

    iget-wide v12, v3, Lru/cn/api/tv/replies/Telecast;->id:J

    cmp-long v7, v10, v12

    if-nez v7, :cond_5

    .line 81
    add-int/lit8 v1, v1, 0x1

    .line 82
    add-int/lit8 v2, v2, 0x1

    .line 98
    :cond_3
    :goto_1
    iget-object v7, p0, Lru/cn/api/provider/Schedule;->telecasts:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ne v1, v7, :cond_2

    .line 99
    iget-object v7, p0, Lru/cn/api/provider/Schedule;->telecasts:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v10

    invoke-interface {p1, v2, v10}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v10

    invoke-interface {v7, v1, v10}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 104
    .end local v3    # "insertingTelecast":Lru/cn/api/tv/replies/Telecast;
    .end local v6    # "telecast":Lru/cn/api/tv/replies/Telecast;
    :cond_4
    iget-object v7, p0, Lru/cn/api/provider/Schedule;->telecasts:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    sub-int/2addr v7, v0

    goto :goto_0

    .line 85
    .restart local v3    # "insertingTelecast":Lru/cn/api/tv/replies/Telecast;
    .restart local v6    # "telecast":Lru/cn/api/tv/replies/Telecast;
    :cond_5
    iget-object v7, v6, Lru/cn/api/tv/replies/Telecast;->date:Lru/cn/api/tv/replies/DateTime;

    invoke-virtual {v7}, Lru/cn/api/tv/replies/DateTime;->toSeconds()J

    move-result-wide v8

    .line 86
    .local v8, "telecastStart":J
    iget-object v7, v3, Lru/cn/api/tv/replies/Telecast;->date:Lru/cn/api/tv/replies/DateTime;

    invoke-virtual {v7}, Lru/cn/api/tv/replies/DateTime;->toSeconds()J

    move-result-wide v4

    .line 88
    .local v4, "insertTelecastStart":J
    cmp-long v7, v8, v4

    if-lez v7, :cond_6

    .line 89
    iget-object v7, p0, Lru/cn/api/provider/Schedule;->telecasts:Ljava/util/List;

    invoke-interface {v7, v1, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 90
    add-int/lit8 v1, v1, 0x1

    .line 91
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 93
    :cond_6
    cmp-long v7, v8, v4

    if-gez v7, :cond_3

    .line 94
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public containsDate(Ljava/util/Calendar;)Z
    .locals 12
    .param p1, "date"    # Ljava/util/Calendar;

    .prologue
    .line 35
    iget-object v8, p0, Lru/cn/api/provider/Schedule;->telecasts:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 36
    const/4 v8, 0x0

    .line 56
    :goto_0
    return v8

    .line 38
    :cond_0
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long v6, v8, v10

    .line 39
    .local v6, "startTimeSec":J
    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 40
    .local v0, "endTimeCalendar":Ljava/util/Calendar;
    const/4 v8, 0x5

    const/4 v9, 0x1

    invoke-virtual {v0, v8, v9}, Ljava/util/Calendar;->add(II)V

    .line 41
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long v2, v8, v10

    .line 43
    .local v2, "endTimeSec":J
    invoke-direct {p0, v6, v7, v2, v3}, Lru/cn/api/provider/Schedule;->telecastsByPeriod(JJ)Ljava/util/List;

    move-result-object v5

    .line 44
    .local v5, "telecastByPeriod":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 45
    const/4 v8, 0x0

    goto :goto_0

    .line 48
    :cond_1
    const/4 v8, 0x0

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/tv/replies/Telecast;

    .line 49
    .local v1, "first":Lru/cn/api/tv/replies/Telecast;
    iget-wide v10, v1, Lru/cn/api/tv/replies/Telecast;->id:J

    iget-object v8, p0, Lru/cn/api/provider/Schedule;->telecasts:Ljava/util/List;

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lru/cn/api/tv/replies/Telecast;

    iget-wide v8, v8, Lru/cn/api/tv/replies/Telecast;->id:J

    cmp-long v8, v10, v8

    if-nez v8, :cond_2

    .line 50
    const/4 v8, 0x0

    goto :goto_0

    .line 52
    :cond_2
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/api/tv/replies/Telecast;

    .line 53
    .local v4, "last":Lru/cn/api/tv/replies/Telecast;
    iget-object v8, v4, Lru/cn/api/tv/replies/Telecast;->date:Lru/cn/api/tv/replies/DateTime;

    invoke-virtual {v8}, Lru/cn/api/tv/replies/DateTime;->toSeconds()J

    move-result-wide v8

    iget-wide v10, v4, Lru/cn/api/tv/replies/Telecast;->duration:J

    add-long/2addr v8, v10

    cmp-long v8, v8, v2

    if-gez v8, :cond_3

    .line 54
    const/4 v8, 0x0

    goto :goto_0

    .line 56
    :cond_3
    const/4 v8, 0x1

    goto :goto_0
.end method

.method public find(J)Lru/cn/api/tv/replies/Telecast;
    .locals 3
    .param p1, "utcSeconds"    # J

    .prologue
    .line 23
    iget-object v1, p0, Lru/cn/api/provider/Schedule;->currentTelecast:Lru/cn/api/tv/replies/Telecast;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lru/cn/api/provider/Schedule;->currentTelecast:Lru/cn/api/tv/replies/Telecast;

    invoke-direct {p0, v1, p1, p2}, Lru/cn/api/provider/Schedule;->inRangeOfTelecast(Lru/cn/api/tv/replies/Telecast;J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 24
    iget-object v0, p0, Lru/cn/api/provider/Schedule;->currentTelecast:Lru/cn/api/tv/replies/Telecast;

    .line 31
    :goto_0
    return-object v0

    .line 26
    :cond_0
    iget-object v1, p0, Lru/cn/api/provider/Schedule;->telecasts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/tv/replies/Telecast;

    .line 27
    .local v0, "telecast":Lru/cn/api/tv/replies/Telecast;
    invoke-direct {p0, v0, p1, p2}, Lru/cn/api/provider/Schedule;->inRangeOfTelecast(Lru/cn/api/tv/replies/Telecast;J)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 31
    .end local v0    # "telecast":Lru/cn/api/tv/replies/Telecast;
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentTelecast()Lru/cn/api/tv/replies/Telecast;
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lru/cn/api/provider/Schedule;->getCurrentTelecast(Z)Lru/cn/api/tv/replies/Telecast;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentTelecast(Z)Lru/cn/api/tv/replies/Telecast;
    .locals 6
    .param p1, "allowExpired"    # Z

    .prologue
    .line 112
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {p0, v2, v3}, Lru/cn/api/provider/Schedule;->find(J)Lru/cn/api/tv/replies/Telecast;

    move-result-object v0

    .line 113
    .local v0, "current":Lru/cn/api/tv/replies/Telecast;
    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 114
    iget-object v0, p0, Lru/cn/api/provider/Schedule;->currentTelecast:Lru/cn/api/tv/replies/Telecast;

    .line 116
    .end local v0    # "current":Lru/cn/api/tv/replies/Telecast;
    :cond_0
    return-object v0
.end method

.method public getTelecasts(Ljava/util/Calendar;)Ljava/util/List;
    .locals 1
    .param p1, "date"    # Ljava/util/Calendar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            ")",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/tv/replies/Telecast;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lru/cn/api/provider/Schedule;->telecastsByDate(Ljava/util/Calendar;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public setCurrentTelecast(Lru/cn/api/tv/replies/Telecast;)V
    .locals 2
    .param p1, "telecast"    # Lru/cn/api/tv/replies/Telecast;

    .prologue
    .line 120
    iput-object p1, p0, Lru/cn/api/provider/Schedule;->currentTelecast:Lru/cn/api/tv/replies/Telecast;

    .line 121
    if-eqz p1, :cond_0

    .line 122
    const/4 v0, 0x1

    new-array v0, v0, [Lru/cn/api/tv/replies/Telecast;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lru/cn/api/provider/Schedule;->add(Ljava/util/List;)I

    .line 124
    :cond_0
    return-void
.end method
