.class public Lru/cn/tv/WebviewFragment;
.super Landroid/support/v4/app/Fragment;
.source "WebviewFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/WebviewFragment$WebviewFragmentListener;
    }
.end annotation


# instance fields
.field private errorText:Landroid/widget/TextView;

.field private handledUrls:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private listener:Lru/cn/tv/WebviewFragment$WebviewFragmentListener;

.field private needClearHistory:Z

.field private needRequestFocus:Z

.field private progress:Landroid/view/View;

.field protected webView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 30
    iput-boolean v0, p0, Lru/cn/tv/WebviewFragment;->needRequestFocus:Z

    .line 31
    iput-boolean v0, p0, Lru/cn/tv/WebviewFragment;->needClearHistory:Z

    .line 33
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lru/cn/tv/WebviewFragment;->handledUrls:Ljava/util/Set;

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/WebviewFragment;)Lru/cn/tv/WebviewFragment$WebviewFragmentListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/WebviewFragment;

    .prologue
    .line 24
    iget-object v0, p0, Lru/cn/tv/WebviewFragment;->listener:Lru/cn/tv/WebviewFragment$WebviewFragmentListener;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/tv/WebviewFragment;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/WebviewFragment;

    .prologue
    .line 24
    iget-object v0, p0, Lru/cn/tv/WebviewFragment;->handledUrls:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/tv/WebviewFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/WebviewFragment;

    .prologue
    .line 24
    iget-object v0, p0, Lru/cn/tv/WebviewFragment;->progress:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/tv/WebviewFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/WebviewFragment;

    .prologue
    .line 24
    iget-object v0, p0, Lru/cn/tv/WebviewFragment;->errorText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lru/cn/tv/WebviewFragment;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/WebviewFragment;

    .prologue
    .line 24
    iget-boolean v0, p0, Lru/cn/tv/WebviewFragment;->needRequestFocus:Z

    return v0
.end method

.method static synthetic access$402(Lru/cn/tv/WebviewFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/WebviewFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 24
    iput-boolean p1, p0, Lru/cn/tv/WebviewFragment;->needRequestFocus:Z

    return p1
.end method

.method static synthetic access$500(Lru/cn/tv/WebviewFragment;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/WebviewFragment;

    .prologue
    .line 24
    iget-boolean v0, p0, Lru/cn/tv/WebviewFragment;->needClearHistory:Z

    return v0
.end method

.method static synthetic access$502(Lru/cn/tv/WebviewFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/WebviewFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 24
    iput-boolean p1, p0, Lru/cn/tv/WebviewFragment;->needClearHistory:Z

    return p1
.end method


# virtual methods
.method public addHandledUrl(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 36
    iget-object v0, p0, Lru/cn/tv/WebviewFragment;->handledUrls:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 37
    return-void
.end method

.method public goBack()Z
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lru/cn/tv/WebviewFragment;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lru/cn/tv/WebviewFragment;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 224
    const/4 v0, 0x1

    .line 226
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFocused()Z
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lru/cn/tv/WebviewFragment;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->isFocusable()Z

    move-result v0

    return v0
.end method

.method protected load(Ljava/lang/String;)V
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 208
    iget-object v0, p0, Lru/cn/tv/WebviewFragment;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 209
    return-void
.end method

.method public load(Ljava/lang/String;Z)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "clearHistory"    # Z

    .prologue
    .line 212
    iput-boolean p2, p0, Lru/cn/tv/WebviewFragment;->needClearHistory:Z

    .line 213
    invoke-virtual {p0, p1}, Lru/cn/tv/WebviewFragment;->load(Ljava/lang/String;)V

    .line 214
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 56
    const v0, 0x7f0c00be

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 189
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 190
    invoke-virtual {p0}, Lru/cn/tv/WebviewFragment;->stopLoading()V

    .line 191
    return-void
.end method

.method protected onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 0
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 231
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 61
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 63
    const v0, 0x7f090175

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/WebviewFragment;->progress:Landroid/view/View;

    .line 64
    const v0, 0x7f0900ba

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/tv/WebviewFragment;->errorText:Landroid/widget/TextView;

    .line 66
    const v0, 0x7f0901f8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lru/cn/tv/WebviewFragment;->webView:Landroid/webkit/WebView;

    .line 67
    iget-object v0, p0, Lru/cn/tv/WebviewFragment;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 68
    iget-object v0, p0, Lru/cn/tv/WebviewFragment;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-static {}, Lru/cn/utils/http/HttpClient;->defaultUserAgent()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lru/cn/tv/WebviewFragment;->webView:Landroid/webkit/WebView;

    new-instance v1, Lru/cn/tv/WebviewFragment$1;

    invoke-direct {v1, p0}, Lru/cn/tv/WebviewFragment$1;-><init>(Lru/cn/tv/WebviewFragment;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 123
    iget-object v0, p0, Lru/cn/tv/WebviewFragment;->webView:Landroid/webkit/WebView;

    new-instance v1, Lru/cn/tv/WebviewFragment$2;

    invoke-direct {v1, p0}, Lru/cn/tv/WebviewFragment$2;-><init>(Lru/cn/tv/WebviewFragment;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 185
    return-void
.end method

.method public requestFocus()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 195
    iget-object v1, p0, Lru/cn/tv/WebviewFragment;->webView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->requestFocus()Z

    move-result v1

    if-nez v1, :cond_0

    .line 196
    iput-boolean v0, p0, Lru/cn/tv/WebviewFragment;->needRequestFocus:Z

    .line 197
    const/4 v0, 0x0

    .line 200
    :cond_0
    return v0
.end method

.method public setListener(Lru/cn/tv/WebviewFragment$WebviewFragmentListener;)V
    .locals 0
    .param p1, "l"    # Lru/cn/tv/WebviewFragment$WebviewFragmentListener;

    .prologue
    .line 50
    iput-object p1, p0, Lru/cn/tv/WebviewFragment;->listener:Lru/cn/tv/WebviewFragment$WebviewFragmentListener;

    .line 51
    return-void
.end method

.method public stopLoading()V
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/tv/WebviewFragment;->needRequestFocus:Z

    .line 218
    iget-object v0, p0, Lru/cn/tv/WebviewFragment;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V

    .line 219
    return-void
.end method
