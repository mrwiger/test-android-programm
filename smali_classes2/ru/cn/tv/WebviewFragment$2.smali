.class Lru/cn/tv/WebviewFragment$2;
.super Landroid/webkit/WebViewClient;
.source "WebviewFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/WebviewFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/WebviewFragment;


# direct methods
.method constructor <init>(Lru/cn/tv/WebviewFragment;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/WebviewFragment;

    .prologue
    .line 123
    iput-object p1, p0, Lru/cn/tv/WebviewFragment$2;->this$0:Lru/cn/tv/WebviewFragment;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 155
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lru/cn/tv/WebviewFragment$2;->this$0:Lru/cn/tv/WebviewFragment;

    invoke-static {v0}, Lru/cn/tv/WebviewFragment;->access$000(Lru/cn/tv/WebviewFragment;)Lru/cn/tv/WebviewFragment$WebviewFragmentListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lru/cn/tv/WebviewFragment$2;->this$0:Lru/cn/tv/WebviewFragment;

    invoke-static {v0}, Lru/cn/tv/WebviewFragment;->access$000(Lru/cn/tv/WebviewFragment;)Lru/cn/tv/WebviewFragment$WebviewFragmentListener;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/WebviewFragment$2;->this$0:Lru/cn/tv/WebviewFragment;

    iget-object v1, v1, Lru/cn/tv/WebviewFragment;->webView:Landroid/webkit/WebView;

    invoke-interface {v0, v1}, Lru/cn/tv/WebviewFragment$WebviewFragmentListener;->onPageFinishLoading(Landroid/webkit/WebView;)V

    .line 160
    :cond_0
    iget-object v0, p0, Lru/cn/tv/WebviewFragment$2;->this$0:Lru/cn/tv/WebviewFragment;

    invoke-static {v0}, Lru/cn/tv/WebviewFragment;->access$200(Lru/cn/tv/WebviewFragment;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 161
    iget-object v0, p0, Lru/cn/tv/WebviewFragment$2;->this$0:Lru/cn/tv/WebviewFragment;

    invoke-static {v0}, Lru/cn/tv/WebviewFragment;->access$400(Lru/cn/tv/WebviewFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 162
    iget-object v0, p0, Lru/cn/tv/WebviewFragment$2;->this$0:Lru/cn/tv/WebviewFragment;

    invoke-static {v0, v2}, Lru/cn/tv/WebviewFragment;->access$402(Lru/cn/tv/WebviewFragment;Z)Z

    .line 163
    iget-object v0, p0, Lru/cn/tv/WebviewFragment$2;->this$0:Lru/cn/tv/WebviewFragment;

    iget-object v0, v0, Lru/cn/tv/WebviewFragment;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->requestFocus()Z

    .line 165
    :cond_1
    iget-object v0, p0, Lru/cn/tv/WebviewFragment$2;->this$0:Lru/cn/tv/WebviewFragment;

    invoke-static {v0}, Lru/cn/tv/WebviewFragment;->access$500(Lru/cn/tv/WebviewFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 166
    iget-object v0, p0, Lru/cn/tv/WebviewFragment$2;->this$0:Lru/cn/tv/WebviewFragment;

    invoke-static {v0, v2}, Lru/cn/tv/WebviewFragment;->access$502(Lru/cn/tv/WebviewFragment;Z)Z

    .line 167
    iget-object v0, p0, Lru/cn/tv/WebviewFragment$2;->this$0:Lru/cn/tv/WebviewFragment;

    iget-object v0, v0, Lru/cn/tv/WebviewFragment;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    .line 169
    :cond_2
    iget-object v0, p0, Lru/cn/tv/WebviewFragment$2;->this$0:Lru/cn/tv/WebviewFragment;

    invoke-virtual {v0, p1, p2}, Lru/cn/tv/WebviewFragment;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 170
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "favicon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 141
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 143
    iget-object v1, p0, Lru/cn/tv/WebviewFragment$2;->this$0:Lru/cn/tv/WebviewFragment;

    invoke-static {v1}, Lru/cn/tv/WebviewFragment;->access$100(Lru/cn/tv/WebviewFragment;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 144
    .local v0, "handledUrl":Ljava/lang/String;
    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 151
    .end local v0    # "handledUrl":Ljava/lang/String;
    :goto_0
    return-void

    .line 149
    :cond_1
    iget-object v1, p0, Lru/cn/tv/WebviewFragment$2;->this$0:Lru/cn/tv/WebviewFragment;

    invoke-static {v1}, Lru/cn/tv/WebviewFragment;->access$200(Lru/cn/tv/WebviewFragment;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 150
    iget-object v1, p0, Lru/cn/tv/WebviewFragment$2;->this$0:Lru/cn/tv/WebviewFragment;

    invoke-static {v1}, Lru/cn/tv/WebviewFragment;->access$300(Lru/cn/tv/WebviewFragment;)Landroid/widget/TextView;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 175
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 177
    iget-object v0, p0, Lru/cn/tv/WebviewFragment$2;->this$0:Lru/cn/tv/WebviewFragment;

    invoke-static {v0}, Lru/cn/tv/WebviewFragment;->access$000(Lru/cn/tv/WebviewFragment;)Lru/cn/tv/WebviewFragment$WebviewFragmentListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lru/cn/tv/WebviewFragment$2;->this$0:Lru/cn/tv/WebviewFragment;

    invoke-static {v0}, Lru/cn/tv/WebviewFragment;->access$000(Lru/cn/tv/WebviewFragment;)Lru/cn/tv/WebviewFragment$WebviewFragmentListener;

    move-result-object v0

    invoke-interface {v0, p2, p3}, Lru/cn/tv/WebviewFragment$WebviewFragmentListener;->onError(ILjava/lang/String;)V

    .line 180
    :cond_0
    iget-object v0, p0, Lru/cn/tv/WebviewFragment$2;->this$0:Lru/cn/tv/WebviewFragment;

    invoke-static {v0}, Lru/cn/tv/WebviewFragment;->access$200(Lru/cn/tv/WebviewFragment;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 181
    iget-object v0, p0, Lru/cn/tv/WebviewFragment$2;->this$0:Lru/cn/tv/WebviewFragment;

    invoke-static {v0}, Lru/cn/tv/WebviewFragment;->access$300(Lru/cn/tv/WebviewFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v0, p0, Lru/cn/tv/WebviewFragment$2;->this$0:Lru/cn/tv/WebviewFragment;

    invoke-static {v0}, Lru/cn/tv/WebviewFragment;->access$300(Lru/cn/tv/WebviewFragment;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 183
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 3
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 127
    iget-object v1, p0, Lru/cn/tv/WebviewFragment$2;->this$0:Lru/cn/tv/WebviewFragment;

    invoke-static {v1}, Lru/cn/tv/WebviewFragment;->access$100(Lru/cn/tv/WebviewFragment;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 128
    .local v0, "handledUrl":Ljava/lang/String;
    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 129
    iget-object v1, p0, Lru/cn/tv/WebviewFragment$2;->this$0:Lru/cn/tv/WebviewFragment;

    invoke-static {v1}, Lru/cn/tv/WebviewFragment;->access$000(Lru/cn/tv/WebviewFragment;)Lru/cn/tv/WebviewFragment$WebviewFragmentListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 130
    iget-object v1, p0, Lru/cn/tv/WebviewFragment$2;->this$0:Lru/cn/tv/WebviewFragment;

    invoke-static {v1}, Lru/cn/tv/WebviewFragment;->access$000(Lru/cn/tv/WebviewFragment;)Lru/cn/tv/WebviewFragment$WebviewFragmentListener;

    move-result-object v1

    invoke-interface {v1, p2}, Lru/cn/tv/WebviewFragment$WebviewFragmentListener;->handleUrl(Ljava/lang/String;)V

    .line 132
    :cond_1
    const/4 v1, 0x1

    .line 136
    .end local v0    # "handledUrl":Ljava/lang/String;
    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
