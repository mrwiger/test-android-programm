.class Lru/cn/tv/stb/StbActivity$28;
.super Lru/cn/tv/stb/ListKeyListener;
.source "StbActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/stb/StbActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private moveNextPrev:Z

.field final synthetic this$0:Lru/cn/tv/stb/StbActivity;


# direct methods
.method constructor <init>(Lru/cn/tv/stb/StbActivity;)V
    .locals 1
    .param p1, "this$0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 1806
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-direct {p0}, Lru/cn/tv/stb/ListKeyListener;-><init>()V

    .line 1808
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/tv/stb/StbActivity$28;->moveNextPrev:Z

    return-void
.end method

.method private switchToDate(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 1859
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$800(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/calendar/CalendarFragment;

    move-result-object v1

    invoke-virtual {v1, p1}, Lru/cn/tv/stb/calendar/CalendarFragment;->getDate(I)Ljava/util/Calendar;

    move-result-object v0

    .line 1861
    .local v0, "date":Ljava/util/Calendar;
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$900(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/schedule/ScheduleFragment;

    move-result-object v1

    invoke-virtual {v1, v0}, Lru/cn/tv/stb/schedule/ScheduleFragment;->setDate(Ljava/util/Calendar;)V

    .line 1863
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$800(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/calendar/CalendarFragment;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/tv/stb/calendar/CalendarFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/widget/ListView;->setSelection(I)V

    .line 1864
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$800(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/calendar/CalendarFragment;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/tv/stb/calendar/CalendarFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 1865
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$800(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/calendar/CalendarFragment;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/tv/stb/calendar/CalendarFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    .line 1866
    return-void
.end method


# virtual methods
.method protected keyDown()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1811
    iget-object v2, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v2}, Lru/cn/tv/stb/StbActivity;->access$900(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/schedule/ScheduleFragment;

    move-result-object v2

    invoke-virtual {v2}, Lru/cn/tv/stb/schedule/ScheduleFragment;->selectNext()Z

    .line 1813
    iget-object v2, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v2}, Lru/cn/tv/stb/StbActivity;->access$6500(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v1

    .line 1814
    .local v1, "selectedPosition":I
    iget-object v2, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v2}, Lru/cn/tv/stb/StbActivity;->access$6500(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_2

    .line 1816
    iget-object v2, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v2}, Lru/cn/tv/stb/StbActivity;->access$6400(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v0

    .line 1817
    .local v0, "pos":I
    iget-object v2, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v2}, Lru/cn/tv/stb/StbActivity;->access$6400(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    if-ltz v0, :cond_0

    .line 1818
    iget-boolean v2, p0, Lru/cn/tv/stb/StbActivity$28;->moveNextPrev:Z

    if-nez v2, :cond_1

    .line 1819
    iput-boolean v3, p0, Lru/cn/tv/stb/StbActivity$28;->moveNextPrev:Z

    .line 1830
    .end local v0    # "pos":I
    :cond_0
    :goto_0
    return v3

    .line 1823
    .restart local v0    # "pos":I
    :cond_1
    add-int/lit8 v2, v0, 0x1

    invoke-direct {p0, v2}, Lru/cn/tv/stb/StbActivity$28;->switchToDate(I)V

    .line 1824
    iget-object v2, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v2}, Lru/cn/tv/stb/StbActivity;->access$900(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/schedule/ScheduleFragment;

    move-result-object v2

    invoke-virtual {v2, v3}, Lru/cn/tv/stb/schedule/ScheduleFragment;->requestFocus(I)V

    goto :goto_0

    .line 1827
    .end local v0    # "pos":I
    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, p0, Lru/cn/tv/stb/StbActivity$28;->moveNextPrev:Z

    goto :goto_0
.end method

.method protected keyLeft()Z
    .locals 3

    .prologue
    .line 1869
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$5600(Lru/cn/tv/stb/StbActivity;)V

    .line 1871
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$1800(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$1800(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 1872
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$1800(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    .line 1874
    const/4 v0, 0x1

    return v0
.end method

.method protected keyRight()Z
    .locals 3

    .prologue
    .line 1878
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$900(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/schedule/ScheduleFragment;

    move-result-object v1

    iget-object v2, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v2}, Lru/cn/tv/stb/StbActivity;->access$6500(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lru/cn/tv/stb/schedule/ScheduleFragment;->selectPosition(I)V

    .line 1880
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$6400(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v0

    .line 1882
    .local v0, "checkedPosition":I
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$6400(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 1884
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$6400(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setSelection(I)V

    .line 1885
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$6400(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->requestFocus()Z

    .line 1887
    const/4 v1, 0x1

    return v1
.end method

.method protected keyUp()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1834
    iget-object v2, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v2}, Lru/cn/tv/stb/StbActivity;->access$900(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/schedule/ScheduleFragment;

    move-result-object v2

    invoke-virtual {v2}, Lru/cn/tv/stb/schedule/ScheduleFragment;->selectPrev()Z

    .line 1836
    iget-object v2, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v2}, Lru/cn/tv/stb/StbActivity;->access$6500(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v1

    .line 1837
    .local v1, "selectedPosition":I
    if-nez v1, :cond_2

    .line 1841
    iget-object v2, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v2}, Lru/cn/tv/stb/StbActivity;->access$6400(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v0

    .line 1842
    .local v0, "pos":I
    if-lez v0, :cond_0

    .line 1843
    iget-boolean v2, p0, Lru/cn/tv/stb/StbActivity$28;->moveNextPrev:Z

    if-nez v2, :cond_1

    .line 1844
    iput-boolean v4, p0, Lru/cn/tv/stb/StbActivity$28;->moveNextPrev:Z

    .line 1855
    .end local v0    # "pos":I
    :cond_0
    :goto_0
    return v4

    .line 1848
    .restart local v0    # "pos":I
    :cond_1
    add-int/lit8 v2, v0, -0x1

    invoke-direct {p0, v2}, Lru/cn/tv/stb/StbActivity$28;->switchToDate(I)V

    .line 1849
    iget-object v2, p0, Lru/cn/tv/stb/StbActivity$28;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v2}, Lru/cn/tv/stb/StbActivity;->access$900(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/schedule/ScheduleFragment;

    move-result-object v2

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lru/cn/tv/stb/schedule/ScheduleFragment;->requestFocus(I)V

    goto :goto_0

    .line 1852
    .end local v0    # "pos":I
    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, p0, Lru/cn/tv/stb/StbActivity$28;->moveNextPrev:Z

    goto :goto_0
.end method
