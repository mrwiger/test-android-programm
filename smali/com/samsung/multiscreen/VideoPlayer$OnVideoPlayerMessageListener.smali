.class Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;
.super Ljava/lang/Object;
.source "VideoPlayer.java"

# interfaces
.implements Lcom/samsung/multiscreen/Channel$OnMessageListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/multiscreen/VideoPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnVideoPlayerMessageListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/multiscreen/VideoPlayer;


# direct methods
.method private constructor <init>(Lcom/samsung/multiscreen/VideoPlayer;)V
    .locals 0

    .prologue
    .line 445
    iput-object p1, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/multiscreen/VideoPlayer;Lcom/samsung/multiscreen/VideoPlayer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/multiscreen/VideoPlayer;
    .param p2, "x1"    # Lcom/samsung/multiscreen/VideoPlayer$1;

    .prologue
    .line 445
    invoke-direct {p0, p1}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;-><init>(Lcom/samsung/multiscreen/VideoPlayer;)V

    return-void
.end method

.method private handlePlayerControlResponse(Ljava/lang/String;)V
    .locals 4
    .param p1, "response"    # Ljava/lang/String;

    .prologue
    .line 581
    :try_start_0
    sget-object v1, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->play:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 582
    iget-object v1, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-static {v1}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onPlay()V

    .line 610
    :cond_0
    :goto_0
    return-void

    .line 583
    :cond_1
    sget-object v1, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->pause:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 584
    iget-object v1, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-static {v1}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onPause()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 607
    :catch_0
    move-exception v0

    .line 608
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/VideoPlayer;->isDebug()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "VideoPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error while parsing control response : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 585
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    :try_start_1
    sget-object v1, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->stop:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 586
    iget-object v1, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-static {v1}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onStop()V

    goto :goto_0

    .line 587
    :cond_3
    sget-object v1, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->next:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 588
    iget-object v1, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-static {v1}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onNext()V

    goto :goto_0

    .line 589
    :cond_4
    sget-object v1, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->previous:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 590
    iget-object v1, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-static {v1}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onPrevious()V

    goto :goto_0

    .line 591
    :cond_5
    sget-object v1, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->FF:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 592
    iget-object v1, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-static {v1}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onForward()V

    goto/16 :goto_0

    .line 593
    :cond_6
    sget-object v1, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->RWD:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 594
    iget-object v1, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-static {v1}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onRewind()V

    goto/16 :goto_0

    .line 595
    :cond_7
    sget-object v1, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->repeat:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 596
    sget-object v1, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatAll:Lcom/samsung/multiscreen/Player$RepeatMode;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/Player$RepeatMode;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 597
    iget-object v1, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-static {v1}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v1

    sget-object v2, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatAll:Lcom/samsung/multiscreen/Player$RepeatMode;

    invoke-interface {v1, v2}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onRepeat(Lcom/samsung/multiscreen/Player$RepeatMode;)V

    goto/16 :goto_0

    .line 598
    :cond_8
    sget-object v1, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatSingle:Lcom/samsung/multiscreen/Player$RepeatMode;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/Player$RepeatMode;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 599
    iget-object v1, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-static {v1}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v1

    sget-object v2, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatSingle:Lcom/samsung/multiscreen/Player$RepeatMode;

    invoke-interface {v1, v2}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onRepeat(Lcom/samsung/multiscreen/Player$RepeatMode;)V

    goto/16 :goto_0

    .line 600
    :cond_9
    sget-object v1, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatOff:Lcom/samsung/multiscreen/Player$RepeatMode;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/Player$RepeatMode;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 601
    iget-object v1, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-static {v1}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v1

    sget-object v2, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatOff:Lcom/samsung/multiscreen/Player$RepeatMode;

    invoke-interface {v1, v2}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onRepeat(Lcom/samsung/multiscreen/Player$RepeatMode;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private handlePlayerInternalResponse(Ljava/lang/String;)V
    .locals 6
    .param p1, "response"    # Ljava/lang/String;

    .prologue
    .line 616
    :try_start_0
    sget-object v3, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerInternalEvents;->bufferingstart:Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerInternalEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerInternalEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 617
    iget-object v3, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onBufferingStart()V

    .line 638
    :cond_0
    :goto_0
    return-void

    .line 618
    :cond_1
    sget-object v3, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerInternalEvents;->bufferingcomplete:Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerInternalEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerInternalEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 619
    iget-object v3, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onBufferingComplete()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 635
    :catch_0
    move-exception v0

    .line 636
    .local v0, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/VideoPlayer;->isDebug()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "VideoPlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error while parsing Internal Event response : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 620
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    :try_start_1
    sget-object v3, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerInternalEvents;->bufferingprogress:Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerInternalEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerInternalEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 621
    const-string v3, ":"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 622
    .local v1, "index":I
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 623
    .local v2, "progress":I
    iget-object v3, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onBufferingProgress(I)V

    goto :goto_0

    .line 624
    .end local v1    # "index":I
    .end local v2    # "progress":I
    :cond_3
    sget-object v3, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerInternalEvents;->currentplaytime:Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerInternalEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerInternalEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 625
    const-string v3, ":"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 626
    .restart local v1    # "index":I
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 627
    .restart local v2    # "progress":I
    iget-object v3, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onCurrentPlayTime(I)V

    goto/16 :goto_0

    .line 628
    .end local v1    # "index":I
    .end local v2    # "progress":I
    :cond_4
    sget-object v3, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerInternalEvents;->streamcompleted:Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerInternalEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerInternalEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 629
    iget-object v3, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onStreamCompleted()V

    goto/16 :goto_0

    .line 630
    :cond_5
    sget-object v3, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerInternalEvents;->totalduration:Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerInternalEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerInternalEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 631
    const-string v3, ":"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 632
    .restart local v1    # "index":I
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 633
    .restart local v2    # "progress":I
    iget-object v3, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onStreamingStarted(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private handlePlayerQueueEventResponse(Ljava/lang/String;)V
    .locals 6
    .param p1, "response"    # Ljava/lang/String;

    .prologue
    .line 642
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 645
    .local v2, "jsonObject":Lorg/json/JSONObject;
    const-string v3, "subEvent"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 646
    .local v1, "event":Ljava/lang/String;
    const-string v3, "subEvent"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 648
    if-nez v1, :cond_1

    .line 649
    iget-object v3, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/VideoPlayer;->isDebug()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "VideoPlayer"

    const-string v4, "Sub-Event key missing from message."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 666
    .end local v1    # "event":Ljava/lang/String;
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    return-void

    .line 653
    .restart local v1    # "event":Ljava/lang/String;
    .restart local v2    # "jsonObject":Lorg/json/JSONObject;
    :cond_1
    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->enqueue:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 654
    iget-object v3, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onAddToList(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 663
    .end local v1    # "event":Ljava/lang/String;
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 664
    .local v0, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/VideoPlayer;->isDebug()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "VideoPlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error while parsing list Event response : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 655
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "event":Ljava/lang/String;
    .restart local v2    # "jsonObject":Lorg/json/JSONObject;
    :cond_2
    :try_start_1
    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->dequeue:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 656
    iget-object v3, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onRemoveFromList(Lorg/json/JSONObject;)V

    goto :goto_0

    .line 657
    :cond_3
    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->clear:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 658
    iget-object v3, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onClearList()V

    goto :goto_0

    .line 659
    :cond_4
    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->fetch:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "data"

    .line 660
    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 661
    iget-object v3, p0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v3

    const-string v4, "data"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onGetList(Lorg/json/JSONArray;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method


# virtual methods
.method public onMessage(Lcom/samsung/multiscreen/Message;)V
    .locals 22
    .param p1, "message"    # Lcom/samsung/multiscreen/Message;

    .prologue
    .line 452
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/multiscreen/VideoPlayer;->isDebug()Z

    move-result v17

    if-eqz v17, :cond_0

    const-string v17, "VideoPlayer"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "onMessage : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v17

    if-nez v17, :cond_2

    .line 455
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/multiscreen/VideoPlayer;->isDebug()Z

    move-result v17

    if-eqz v17, :cond_1

    const-string v17, "VideoPlayer"

    const-string v18, "Unregistered PlayerListener."

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    :cond_1
    :goto_0
    return-void

    .line 460
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/multiscreen/Message;->getEvent()Ljava/lang/String;

    move-result-object v17

    const-string v18, "playerNotice"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_3

    .line 461
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/multiscreen/VideoPlayer;->isDebug()Z

    move-result v17

    if-eqz v17, :cond_1

    const-string v17, "VideoPlayer"

    const-string v18, "In-Valid Player Message"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 468
    :cond_3
    :try_start_0
    new-instance v18, Lorg/json/JSONTokener;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/multiscreen/Message;->getData()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/json/JSONObject;

    .line 470
    .local v13, "response":Lorg/json/JSONObject;
    if-nez v13, :cond_4

    .line 471
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/multiscreen/VideoPlayer;->isDebug()Z

    move-result v17

    if-eqz v17, :cond_1

    const-string v17, "VideoPlayer"

    const-string v18, "NULL Response - Unable to parse JSON string."

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 574
    .end local v13    # "response":Lorg/json/JSONObject;
    :catch_0
    move-exception v3

    .line 575
    .local v3, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/multiscreen/VideoPlayer;->isDebug()Z

    move-result v17

    if-eqz v17, :cond_1

    const-string v17, "VideoPlayer"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Error while parsing response : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 480
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v13    # "response":Lorg/json/JSONObject;
    :cond_4
    :try_start_1
    const-string v17, "subEvent"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 481
    const-string v17, "subEvent"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 482
    .local v14, "subEventData":Ljava/lang/String;
    const-string v17, "playerReady"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 484
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer;->mAdditionalData:Lorg/json/JSONObject;

    move-object/from16 v17, v0

    if-eqz v17, :cond_5

    .line 485
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer;->mAdditionalData:Lorg/json/JSONObject;

    move-object/from16 v17, v0

    const-string v18, "playerType"

    sget-object v19, Lcom/samsung/multiscreen/Player$ContentType;->video:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 486
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer;->mAdditionalData:Lorg/json/JSONObject;

    move-object/from16 v17, v0

    const-string v18, "subEvent"

    sget-object v19, Lcom/samsung/multiscreen/Player$PlayerContentSubEvents;->ADDITIONALMEDIAINFO:Lcom/samsung/multiscreen/Player$PlayerContentSubEvents;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/multiscreen/Player$PlayerContentSubEvents;->name()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 487
    sget-object v17, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v18, "playerContentChange"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer;->mAdditionalData:Lorg/json/JSONObject;

    move-object/from16 v19, v0

    invoke-virtual/range {v17 .. v19}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 490
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onPlayerInitialized()V

    goto/16 :goto_0

    .line 495
    :cond_6
    const-string v17, "playerChange"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 501
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v17

    sget-object v18, Lcom/samsung/multiscreen/Player$ContentType;->video:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onPlayerChange(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 503
    .end local v14    # "subEventData":Ljava/lang/String;
    :cond_7
    const-string v17, "playerType"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_c

    .line 504
    const-string v17, "playerType"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 505
    .local v10, "playerType":Ljava/lang/String;
    sget-object v17, Lcom/samsung/multiscreen/Player$ContentType;->video:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 508
    const-string v17, "state"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 509
    const-string v17, "state"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->handlePlayerControlResponse(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 510
    :cond_8
    const-string v17, "Video State"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 511
    const-string v17, "Video State"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->handlePlayerInternalResponse(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 512
    :cond_9
    const-string v17, "queue"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 513
    const-string v17, "queue"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->handlePlayerQueueEventResponse(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 514
    :cond_a
    const-string v17, "currentPlaying"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 515
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v17

    const-string v18, "currentPlaying"

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-interface {v0, v1, v10}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onCurrentPlaying(Lorg/json/JSONObject;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 516
    :cond_b
    const-string v17, "error"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 517
    const-string v17, "error"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v5

    .line 519
    .local v5, "errorMessage":Ljava/lang/String;
    :try_start_2
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 520
    .local v6, "errorValue":I
    new-instance v4, Lcom/samsung/multiscreen/ErrorCode;

    invoke-direct {v4, v6}, Lcom/samsung/multiscreen/ErrorCode;-><init>(I)V

    .line 521
    .local v4, "errorCode":Lcom/samsung/multiscreen/ErrorCode;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v17

    invoke-virtual {v4}, Lcom/samsung/multiscreen/ErrorCode;->value()I

    move-result v18

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    invoke-virtual {v4}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v20

    invoke-virtual {v4}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v18 .. v21}, Lcom/samsung/multiscreen/Error;->create(JLjava/lang/String;Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onError(Lcom/samsung/multiscreen/Error;)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 522
    .end local v4    # "errorCode":Lcom/samsung/multiscreen/ErrorCode;
    .end local v6    # "errorValue":I
    :catch_1
    move-exception v3

    .line 523
    .local v3, "e":Ljava/lang/NumberFormatException;
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v17

    invoke-static {v5}, Lcom/samsung/multiscreen/Error;->create(Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onError(Lcom/samsung/multiscreen/Error;)V

    goto/16 :goto_0

    .line 528
    .end local v3    # "e":Ljava/lang/NumberFormatException;
    .end local v5    # "errorMessage":Ljava/lang/String;
    .end local v10    # "playerType":Ljava/lang/String;
    :cond_c
    const-string v17, "state"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_15

    .line 529
    invoke-virtual {v13}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    .line 530
    .local v2, "data":Ljava/lang/String;
    sget-object v17, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->getControlStatus:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_12

    .line 531
    new-instance v17, Lorg/json/JSONTokener;

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/json/JSONObject;

    .line 532
    .local v8, "jsonData":Lorg/json/JSONObject;
    const/4 v15, 0x0

    .line 533
    .local v15, "volumeLevel":I
    const/16 v17, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 534
    .local v9, "muteStatus":Ljava/lang/Boolean;
    sget-object v11, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatOff:Lcom/samsung/multiscreen/Player$RepeatMode;

    .line 535
    .local v11, "repeatMode":Lcom/samsung/multiscreen/Player$RepeatMode;
    sget-object v17, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->volume:Lcom/samsung/multiscreen/Player$PlayerControlStatus;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->name()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_d

    .line 536
    sget-object v17, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->volume:Lcom/samsung/multiscreen/Player$PlayerControlStatus;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->name()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v15

    .line 538
    :cond_d
    sget-object v17, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->mute:Lcom/samsung/multiscreen/Player$PlayerControlStatus;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->name()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_e

    .line 539
    sget-object v17, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->mute:Lcom/samsung/multiscreen/Player$PlayerControlStatus;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->name()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 541
    :cond_e
    sget-object v17, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->repeat:Lcom/samsung/multiscreen/Player$PlayerControlStatus;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->name()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_f

    .line 542
    sget-object v17, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->repeat:Lcom/samsung/multiscreen/Player$PlayerControlStatus;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->name()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 543
    .local v12, "repeatStr":Ljava/lang/String;
    const/4 v11, 0x0

    .line 544
    sget-object v17, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatAll:Lcom/samsung/multiscreen/Player$RepeatMode;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/multiscreen/Player$RepeatMode;->name()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_10

    .line 545
    sget-object v11, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatAll:Lcom/samsung/multiscreen/Player$RepeatMode;

    .line 555
    .end local v12    # "repeatStr":Ljava/lang/String;
    :cond_f
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v0, v15, v9, v11}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onControlStatus(ILjava/lang/Boolean;Lcom/samsung/multiscreen/Player$RepeatMode;)V

    goto/16 :goto_0

    .line 546
    .restart local v12    # "repeatStr":Ljava/lang/String;
    :cond_10
    sget-object v17, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatSingle:Lcom/samsung/multiscreen/Player$RepeatMode;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/multiscreen/Player$RepeatMode;->name()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_11

    .line 547
    sget-object v11, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatSingle:Lcom/samsung/multiscreen/Player$RepeatMode;

    goto :goto_1

    .line 548
    :cond_11
    sget-object v17, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatOff:Lcom/samsung/multiscreen/Player$RepeatMode;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/multiscreen/Player$RepeatMode;->name()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_f

    .line 549
    sget-object v11, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatOff:Lcom/samsung/multiscreen/Player$RepeatMode;

    goto :goto_1

    .line 556
    .end local v8    # "jsonData":Lorg/json/JSONObject;
    .end local v9    # "muteStatus":Ljava/lang/Boolean;
    .end local v11    # "repeatMode":Lcom/samsung/multiscreen/Player$RepeatMode;
    .end local v12    # "repeatStr":Ljava/lang/String;
    .end local v15    # "volumeLevel":I
    :cond_12
    sget-object v17, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->mute:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_13

    .line 557
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onMute()V

    goto/16 :goto_0

    .line 558
    :cond_13
    sget-object v17, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->unMute:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_14

    .line 559
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onUnMute()V

    goto/16 :goto_0

    .line 560
    :cond_14
    sget-object v17, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->getVolume:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 561
    const-string v17, ":"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    .line 562
    .local v7, "index":I
    add-int/lit8 v17, v7, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v18

    add-int/lit8 v18, v18, -0x2

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v16

    .line 563
    .local v16, "volumeStr":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    .line 564
    .restart local v15    # "volumeLevel":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v0, v15}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onVolumeChange(I)V

    goto/16 :goto_0

    .line 566
    .end local v2    # "data":Ljava/lang/String;
    .end local v7    # "index":I
    .end local v15    # "volumeLevel":I
    .end local v16    # "volumeStr":Ljava/lang/String;
    :cond_15
    const-string v17, "appStatus"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 567
    invoke-virtual {v13}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    .line 568
    .restart local v2    # "data":Ljava/lang/String;
    sget-object v17, Lcom/samsung/multiscreen/Player$PlayerApplicationStatusEvents;->suspend:Lcom/samsung/multiscreen/Player$PlayerApplicationStatusEvents;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/multiscreen/Player$PlayerApplicationStatusEvents;->name()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_16

    .line 569
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onApplicationSuspend()V

    goto/16 :goto_0

    .line 570
    :cond_16
    sget-object v17, Lcom/samsung/multiscreen/Player$PlayerApplicationStatusEvents;->resume:Lcom/samsung/multiscreen/Player$PlayerApplicationStatusEvents;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/multiscreen/Player$PlayerApplicationStatusEvents;->name()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 571
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/multiscreen/VideoPlayer;->access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onApplicationResume()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0
.end method
