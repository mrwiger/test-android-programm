.class public final Lcom/my/target/core/engines/b;
.super Ljava/lang/Object;
.source "StandardAdEngine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/core/engines/b$b;,
        Lcom/my/target/core/engines/b$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/my/target/ads/MyTargetView;

.field private final adConfig:Lcom/my/target/b;

.field private final b:Lcom/my/target/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/my/target/c",
            "<",
            "Lcom/my/target/dh;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/my/target/core/presenters/b$a;

.field private final context:Landroid/content/Context;

.field private final d:Lcom/my/target/core/engines/b$a;

.field private e:Lcom/my/target/core/presenters/b;

.field private f:Z


# direct methods
.method private constructor <init>(Lcom/my/target/ads/MyTargetView;Lcom/my/target/b;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/my/target/core/engines/b;->a:Lcom/my/target/ads/MyTargetView;

    .line 54
    iput-object p2, p0, Lcom/my/target/core/engines/b;->adConfig:Lcom/my/target/b;

    .line 55
    invoke-virtual {p1}, Lcom/my/target/ads/MyTargetView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/engines/b;->context:Landroid/content/Context;

    .line 56
    new-instance v0, Lcom/my/target/core/engines/b$b;

    invoke-direct {v0, p0, v1}, Lcom/my/target/core/engines/b$b;-><init>(Lcom/my/target/core/engines/b;B)V

    iput-object v0, p0, Lcom/my/target/core/engines/b;->c:Lcom/my/target/core/presenters/b$a;

    .line 57
    new-instance v0, Lcom/my/target/core/engines/b$a;

    invoke-direct {v0, p0, v1}, Lcom/my/target/core/engines/b$a;-><init>(Lcom/my/target/core/engines/b;B)V

    iput-object v0, p0, Lcom/my/target/core/engines/b;->d:Lcom/my/target/core/engines/b$a;

    .line 58
    invoke-static {p2}, Lcom/my/target/db;->newFactory(Lcom/my/target/b;)Lcom/my/target/c;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/engines/b;->b:Lcom/my/target/c;

    .line 59
    return-void
.end method

.method static synthetic a(Lcom/my/target/core/engines/b;)Lcom/my/target/ads/MyTargetView;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/my/target/core/engines/b;->a:Lcom/my/target/ads/MyTargetView;

    return-object v0
.end method

.method public static a(Lcom/my/target/ads/MyTargetView;Lcom/my/target/b;)Lcom/my/target/core/engines/b;
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/my/target/core/engines/b;

    invoke-direct {v0, p0, p1}, Lcom/my/target/core/engines/b;-><init>(Lcom/my/target/ads/MyTargetView;Lcom/my/target/b;)V

    return-object v0
.end method

.method private a(Lcom/my/target/di;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, -0x2

    .line 174
    invoke-virtual {p1}, Lcom/my/target/di;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v2

    .line 175
    const/4 v0, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 184
    const/16 v0, 0x140

    invoke-virtual {v2, v0}, Lcom/my/target/cm;->n(I)I

    move-result v0

    const/16 v3, 0x32

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v3

    invoke-virtual {p1, v0, v3}, Lcom/my/target/di;->a(II)V

    .line 185
    invoke-virtual {p1, v1}, Lcom/my/target/di;->setFlexibleWidth(Z)V

    .line 186
    const/16 v0, 0x280

    invoke-virtual {v2, v0}, Lcom/my/target/cm;->n(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/my/target/di;->setMaxWidth(I)V

    .line 190
    :goto_1
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 191
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 192
    invoke-virtual {p1, v0}, Lcom/my/target/di;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 193
    iget-object v0, p0, Lcom/my/target/core/engines/b;->a:Lcom/my/target/ads/MyTargetView;

    invoke-virtual {v0}, Lcom/my/target/ads/MyTargetView;->removeAllViews()V

    .line 194
    iget-object v0, p0, Lcom/my/target/core/engines/b;->a:Lcom/my/target/ads/MyTargetView;

    invoke-virtual {v0, p1}, Lcom/my/target/ads/MyTargetView;->addView(Landroid/view/View;)V

    .line 195
    return-void

    .line 175
    :sswitch_0
    const-string v3, "standard_300x250"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v3, "standard_728x90"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 178
    :pswitch_0
    const/16 v0, 0x12c

    invoke-virtual {v2, v0}, Lcom/my/target/cm;->n(I)I

    move-result v0

    const/16 v1, 0xfa

    invoke-virtual {v2, v1}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/my/target/di;->a(II)V

    goto :goto_1

    .line 181
    :pswitch_1
    const/16 v0, 0x2d8

    invoke-virtual {v2, v0}, Lcom/my/target/cm;->n(I)I

    move-result v0

    const/16 v1, 0x5a

    invoke-virtual {v2, v1}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/my/target/di;->a(II)V

    goto :goto_1

    .line 175
    :sswitch_data_0
    .sparse-switch
        -0x580924ba -> :sswitch_0
        -0x4636608c -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lcom/my/target/core/engines/b;Z)Z
    .locals 0

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/my/target/core/engines/b;->f:Z

    return p1
.end method

.method static synthetic b(Lcom/my/target/core/engines/b;)Lcom/my/target/core/presenters/b;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/my/target/core/engines/b;->e:Lcom/my/target/core/presenters/b;

    return-object v0
.end method

.method static synthetic c(Lcom/my/target/core/engines/b;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/my/target/core/engines/b;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lcom/my/target/core/engines/b;)Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/my/target/core/engines/b;->f:Z

    return v0
.end method

.method static synthetic e(Lcom/my/target/core/engines/b;)Lcom/my/target/b;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/my/target/core/engines/b;->adConfig:Lcom/my/target/b;

    return-object v0
.end method

.method static synthetic f(Lcom/my/target/core/engines/b;)Lcom/my/target/core/engines/b$a;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/my/target/core/engines/b;->d:Lcom/my/target/core/engines/b$a;

    return-object v0
.end method

.method static synthetic g(Lcom/my/target/core/engines/b;)Lcom/my/target/c;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/my/target/core/engines/b;->b:Lcom/my/target/c;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/my/target/dh;)V
    .locals 4

    .prologue
    .line 63
    invoke-virtual {p1}, Lcom/my/target/dh;->y()Z

    move-result v0

    iput-boolean v0, p0, Lcom/my/target/core/engines/b;->f:Z

    .line 64
    const-string v0, "native"

    invoke-virtual {p1}, Lcom/my/target/dh;->w()Lcom/my/target/dg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/dg;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1118
    iget-object v0, p0, Lcom/my/target/core/engines/b;->a:Lcom/my/target/ads/MyTargetView;

    invoke-virtual {v0}, Lcom/my/target/ads/MyTargetView;->getListener()Lcom/my/target/ads/MyTargetView$MyTargetViewListener;

    move-result-object v0

    .line 1119
    if-eqz v0, :cond_0

    .line 1124
    iget-object v1, p0, Lcom/my/target/core/engines/b;->adConfig:Lcom/my/target/b;

    invoke-virtual {v1}, Lcom/my/target/b;->getFormat()Ljava/lang/String;

    move-result-object v1

    .line 1125
    iget-object v2, p0, Lcom/my/target/core/engines/b;->context:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/my/target/core/presenters/a;->a(Ljava/lang/String;Landroid/content/Context;)Lcom/my/target/core/presenters/a;

    move-result-object v2

    iput-object v2, p0, Lcom/my/target/core/engines/b;->e:Lcom/my/target/core/presenters/b;

    .line 1126
    iget-object v2, p0, Lcom/my/target/core/engines/b;->e:Lcom/my/target/core/presenters/b;

    iget-object v3, p0, Lcom/my/target/core/engines/b;->c:Lcom/my/target/core/presenters/b$a;

    invoke-interface {v2, v3}, Lcom/my/target/core/presenters/b;->a(Lcom/my/target/core/presenters/b$a;)V

    .line 1127
    iget-object v2, p0, Lcom/my/target/core/engines/b;->e:Lcom/my/target/core/presenters/b;

    invoke-interface {v2}, Lcom/my/target/core/presenters/b;->C()Lcom/my/target/di;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/my/target/core/engines/b;->a(Lcom/my/target/di;Ljava/lang/String;)V

    .line 1128
    iget-object v1, p0, Lcom/my/target/core/engines/b;->e:Lcom/my/target/core/presenters/b;

    invoke-interface {v1, p1}, Lcom/my/target/core/presenters/b;->b(Lcom/my/target/dh;)V

    .line 1129
    iget-object v1, p0, Lcom/my/target/core/engines/b;->a:Lcom/my/target/ads/MyTargetView;

    invoke-interface {v0, v1}, Lcom/my/target/ads/MyTargetView$MyTargetViewListener;->onLoad(Lcom/my/target/ads/MyTargetView;)V

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 1134
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/engines/b;->a:Lcom/my/target/ads/MyTargetView;

    invoke-virtual {v0}, Lcom/my/target/ads/MyTargetView;->getListener()Lcom/my/target/ads/MyTargetView$MyTargetViewListener;

    move-result-object v0

    .line 1135
    if-eqz v0, :cond_0

    .line 1140
    iget-object v0, p0, Lcom/my/target/core/engines/b;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0}, Lcom/my/target/b;->getFormat()Ljava/lang/String;

    move-result-object v0

    .line 1141
    iget-object v1, p0, Lcom/my/target/core/engines/b;->context:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/my/target/core/presenters/c;->b(Ljava/lang/String;Landroid/content/Context;)Lcom/my/target/core/presenters/c;

    move-result-object v1

    .line 1142
    iput-object v1, p0, Lcom/my/target/core/engines/b;->e:Lcom/my/target/core/presenters/b;

    .line 1144
    iget-object v2, p0, Lcom/my/target/core/engines/b;->c:Lcom/my/target/core/presenters/b$a;

    invoke-virtual {v1, v2}, Lcom/my/target/core/presenters/c;->a(Lcom/my/target/core/presenters/b$a;)V

    .line 1145
    new-instance v2, Lcom/my/target/core/engines/b$1;

    invoke-direct {v2, p0}, Lcom/my/target/core/engines/b$1;-><init>(Lcom/my/target/core/engines/b;)V

    invoke-virtual {v1, v2}, Lcom/my/target/core/presenters/c;->a(Lcom/my/target/core/presenters/c$b;)V

    .line 1168
    invoke-virtual {v1}, Lcom/my/target/core/presenters/c;->C()Lcom/my/target/di;

    move-result-object v2

    invoke-direct {p0, v2, v0}, Lcom/my/target/core/engines/b;->a(Lcom/my/target/di;Ljava/lang/String;)V

    .line 1169
    invoke-virtual {v1, p1}, Lcom/my/target/core/presenters/c;->b(Lcom/my/target/dh;)V

    goto :goto_0
.end method

.method public final destroy()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/my/target/core/engines/b;->a:Lcom/my/target/ads/MyTargetView;

    invoke-virtual {v0}, Lcom/my/target/ads/MyTargetView;->removeAllViews()V

    .line 77
    iget-object v0, p0, Lcom/my/target/core/engines/b;->e:Lcom/my/target/core/presenters/b;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/my/target/core/engines/b;->e:Lcom/my/target/core/presenters/b;

    invoke-interface {v0}, Lcom/my/target/core/presenters/b;->destroy()V

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/core/engines/b;->e:Lcom/my/target/core/presenters/b;

    .line 82
    :cond_0
    return-void
.end method

.method public final pause()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/my/target/core/engines/b;->e:Lcom/my/target/core/presenters/b;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/my/target/core/engines/b;->e:Lcom/my/target/core/presenters/b;

    invoke-interface {v0}, Lcom/my/target/core/presenters/b;->pause()V

    .line 98
    :cond_0
    return-void
.end method

.method public final resume()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/my/target/core/engines/b;->e:Lcom/my/target/core/presenters/b;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/my/target/core/engines/b;->e:Lcom/my/target/core/presenters/b;

    invoke-interface {v0}, Lcom/my/target/core/presenters/b;->resume()V

    .line 90
    :cond_0
    return-void
.end method

.method public final start()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/my/target/core/engines/b;->e:Lcom/my/target/core/presenters/b;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/my/target/core/engines/b;->e:Lcom/my/target/core/presenters/b;

    invoke-interface {v0}, Lcom/my/target/core/presenters/b;->start()V

    .line 114
    :cond_0
    return-void
.end method

.method public final stop()V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/my/target/core/engines/b;->e:Lcom/my/target/core/presenters/b;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/my/target/core/engines/b;->e:Lcom/my/target/core/presenters/b;

    invoke-interface {v0}, Lcom/my/target/core/presenters/b;->stop()V

    .line 106
    :cond_0
    return-void
.end method
