.class Lru/cn/domain/stores/PlayMarketStore$3;
.super Ljava/lang/Object;
.source "PlayMarketStore.java"

# interfaces
.implements Lru/cn/domain/stores/iabhelper/IabHelper$OnIabSetupFinishedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/domain/stores/PlayMarketStore;->purchase(Landroid/app/Activity;Ljava/lang/String;Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/domain/stores/PlayMarketStore;

.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$listener:Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;

.field final synthetic val$productId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lru/cn/domain/stores/PlayMarketStore;Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;Landroid/app/Activity;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/domain/stores/PlayMarketStore;

    .prologue
    .line 76
    iput-object p1, p0, Lru/cn/domain/stores/PlayMarketStore$3;->this$0:Lru/cn/domain/stores/PlayMarketStore;

    iput-object p2, p0, Lru/cn/domain/stores/PlayMarketStore$3;->val$listener:Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;

    iput-object p3, p0, Lru/cn/domain/stores/PlayMarketStore$3;->val$activity:Landroid/app/Activity;

    iput-object p4, p0, Lru/cn/domain/stores/PlayMarketStore$3;->val$productId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onIabSetupFinished(Lru/cn/domain/stores/iabhelper/IabResult;)V
    .locals 5
    .param p1, "result"    # Lru/cn/domain/stores/iabhelper/IabResult;

    .prologue
    .line 79
    invoke-virtual {p1}, Lru/cn/domain/stores/iabhelper/IabResult;->isFailure()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lru/cn/domain/stores/PlayMarketStore$3;->val$listener:Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;

    invoke-virtual {p1}, Lru/cn/domain/stores/iabhelper/IabResult;->getResponse()I

    move-result v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;->onPurchaseCompleted(ILjava/lang/String;)V

    .line 81
    iget-object v0, p0, Lru/cn/domain/stores/PlayMarketStore$3;->this$0:Lru/cn/domain/stores/PlayMarketStore;

    invoke-static {v0}, Lru/cn/domain/stores/PlayMarketStore;->access$100(Lru/cn/domain/stores/PlayMarketStore;)V

    .line 92
    :goto_0
    return-void

    .line 85
    :cond_0
    iget-object v0, p0, Lru/cn/domain/stores/PlayMarketStore$3;->this$0:Lru/cn/domain/stores/PlayMarketStore;

    invoke-static {v0}, Lru/cn/domain/stores/PlayMarketStore;->access$200(Lru/cn/domain/stores/PlayMarketStore;)Lru/cn/domain/stores/iabhelper/IabHelper;

    move-result-object v0

    iget-object v1, p0, Lru/cn/domain/stores/PlayMarketStore$3;->val$activity:Landroid/app/Activity;

    iget-object v2, p0, Lru/cn/domain/stores/PlayMarketStore$3;->val$productId:Ljava/lang/String;

    const/16 v3, 0x6f6

    new-instance v4, Lru/cn/domain/stores/PlayMarketStore$3$1;

    invoke-direct {v4, p0}, Lru/cn/domain/stores/PlayMarketStore$3$1;-><init>(Lru/cn/domain/stores/PlayMarketStore$3;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lru/cn/domain/stores/iabhelper/IabHelper;->launchSubscriptionPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILru/cn/domain/stores/iabhelper/IabHelper$OnIabPurchaseFinishedListener;)V

    goto :goto_0
.end method
