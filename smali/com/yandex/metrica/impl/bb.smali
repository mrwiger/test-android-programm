.class public Lcom/yandex/metrica/impl/bb;
.super Lcom/yandex/metrica/impl/c;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Lcom/yandex/metrica/impl/ob/dq;

.field private k:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/yandex/metrica/impl/c;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/pm/ApplicationInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 326
    iget v0, p0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const-string v0, "1"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method private b(Lcom/yandex/metrica/impl/ob/dg;)V
    .locals 1

    .prologue
    .line 201
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/dg;->j()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->c(Z)V

    .line 202
    return-void
.end method

.method private declared-synchronized b(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/dg;)V
    .locals 4

    .prologue
    .line 237
    monitor-enter p0

    .line 7255
    :try_start_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/bb;->p()Ljava/lang/String;

    move-result-object v0

    .line 7356
    invoke-static {v0}, Lcom/yandex/metrica/impl/bj;->a(Ljava/lang/String;)Z

    move-result v1

    .line 7256
    if-eqz v1, :cond_0

    .line 7257
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->k()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->h()Ljava/lang/String;

    move-result-object v0

    .line 8356
    invoke-static {v0}, Lcom/yandex/metrica/impl/bj;->a(Ljava/lang/String;)Z

    move-result v1

    .line 7258
    if-eqz v1, :cond_0

    .line 7259
    invoke-static {}, Lcom/yandex/metrica/impl/ob/dl;->a()Lcom/yandex/metrica/impl/ob/dl;

    move-result-object v0

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->n()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dl;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 7263
    :cond_0
    invoke-static {}, Lcom/yandex/metrica/impl/ob/g;->a()Lcom/yandex/metrica/impl/ob/g;

    move-result-object v1

    new-instance v2, Lcom/yandex/metrica/impl/ob/n;

    invoke-direct {v2, v0}, Lcom/yandex/metrica/impl/ob/n;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/ob/g;->b(Lcom/yandex/metrica/impl/ob/i;)V

    .line 7264
    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->i(Ljava/lang/String;)V

    .line 9270
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->k()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->g()Ljava/lang/String;

    move-result-object v0

    .line 9356
    invoke-static {v0}, Lcom/yandex/metrica/impl/bj;->a(Ljava/lang/String;)Z

    move-result v1

    .line 9271
    if-eqz v1, :cond_1

    .line 9272
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/bb;->q()Ljava/lang/String;

    move-result-object v0

    .line 10356
    invoke-static {v0}, Lcom/yandex/metrica/impl/bj;->a(Ljava/lang/String;)Z

    move-result v1

    .line 9273
    if-eqz v1, :cond_1

    .line 9274
    const-string v0, ""

    invoke-virtual {p2, v0}, Lcom/yandex/metrica/impl/ob/dg;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 9278
    :cond_1
    invoke-static {}, Lcom/yandex/metrica/impl/ob/g;->a()Lcom/yandex/metrica/impl/ob/g;

    move-result-object v1

    new-instance v2, Lcom/yandex/metrica/impl/ob/r;

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->m()Lcom/yandex/metrica/impl/ob/t;

    move-result-object v3

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/ob/t;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/yandex/metrica/impl/ob/r;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/ob/g;->b(Lcom/yandex/metrica/impl/ob/i;)V

    .line 9279
    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->h(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 239
    monitor-exit p0

    return-void

    .line 237
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private c(Lcom/yandex/metrica/impl/ob/dg;)V
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/yandex/metrica/impl/ob/dg;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/bb;->g:Ljava/lang/String;

    .line 218
    return-void
.end method

.method private d(Lcom/yandex/metrica/impl/ob/dg;)V
    .locals 1

    .prologue
    .line 332
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/dv;->b(Lcom/yandex/metrica/impl/ob/dg;)Lcom/yandex/metrica/impl/ob/dv;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->a(Lcom/yandex/metrica/impl/ob/dv;)V

    .line 333
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/dq;->a(Lcom/yandex/metrica/impl/ob/dg;)Lcom/yandex/metrica/impl/ob/dq;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->a(Lcom/yandex/metrica/impl/ob/dq;)V

    .line 334
    return-void
.end method

.method private f(Lcom/yandex/metrica/impl/ob/v;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 376
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->k()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    .line 379
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->m()Lcom/yandex/metrica/impl/ob/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 380
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->m()Lcom/yandex/metrica/impl/ob/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/t;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 381
    :cond_0
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->F()Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v1

    .line 382
    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->n()Ljava/util/List;

    move-result-object v0

    .line 383
    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->a(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 384
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/bb;->H()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/yandex/metrica/impl/bl;->a(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 385
    invoke-virtual {v1, v3}, Lcom/yandex/metrica/impl/ob/dg;->d(Ljava/util/List;)Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v2

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/dg;->g()V

    .line 386
    invoke-virtual {p0, v3}, Lcom/yandex/metrica/impl/bb;->a(Ljava/util/List;)V

    .line 389
    :cond_1
    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->a(Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 390
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/bb;->H()Ljava/util/List;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/yandex/metrica/impl/bl;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 391
    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->a(Ljava/util/List;)V

    .line 392
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/bb;->H()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/ob/dg;->d(Ljava/util/List;)Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dg;->g()V

    .line 396
    :cond_2
    return-void
.end method


# virtual methods
.method public I()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/yandex/metrica/impl/bb;->b:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized J()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/bb;->a:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public K()Ljava/lang/String;
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/yandex/metrica/impl/bb;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/yandex/metrica/impl/bb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public L()Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/bb;->c:Z

    return v0
.end method

.method public M()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/bb;->d:Z

    return v0
.end method

.method public N()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/bb;->e:Z

    return v0
.end method

.method public O()Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/bb;->f:Z

    return v0
.end method

.method public P()Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/bb;->k:Z

    return v0
.end method

.method public Q()Ljava/lang/String;
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/yandex/metrica/impl/bb;->h:Ljava/lang/String;

    const-string v1, "https://certificate.mobile.yandex.net/api/v1/pins"

    invoke-static {v0, v1}, Lcom/yandex/metrica/impl/bb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized R()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 126
    monitor-enter p0

    const/4 v2, 0x2

    :try_start_0
    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/bb;->q()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/bb;->p()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/yandex/metrica/impl/bj;->a([Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public S()Lcom/yandex/metrica/impl/ob/dq;
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lcom/yandex/metrica/impl/bb;->j:Lcom/yandex/metrica/impl/ob/dq;

    return-object v0
.end method

.method public T()Ljava/lang/String;
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lcom/yandex/metrica/impl/bb;->i:Ljava/lang/String;

    return-object v0
.end method

.method a(Landroid/content/Context;Lcom/yandex/metrica/CounterConfiguration;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    invoke-virtual {p2}, Lcom/yandex/metrica/CounterConfiguration;->e()Lcom/yandex/metrica/a;

    move-result-object v0

    .line 227
    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/bb;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/yandex/metrica/a;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public declared-synchronized a(Lcom/yandex/metrica/impl/bh$a;)V
    .locals 3

    .prologue
    .line 337
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bh$a;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->h(Ljava/lang/String;)V

    .line 338
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bh$a;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->i(Ljava/lang/String;)V

    .line 339
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bh$a;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->n(Ljava/lang/String;)V

    .line 340
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bh$a;->f()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->b(Ljava/util/List;)V

    .line 341
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bh$a;->h()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->c(Ljava/util/List;)V

    .line 342
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bh$a;->k()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->d(Ljava/util/List;)V

    .line 343
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bh$a;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->o(Ljava/lang/String;)V

    .line 344
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bh$a;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->r(Ljava/lang/String;)V

    .line 345
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bh$a;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->q(Ljava/lang/String;)V

    .line 346
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bh$a;->b()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->b(Z)V

    .line 347
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bh$a;->c()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->c(Z)V

    .line 348
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bh$a;->v()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->d(Z)V

    .line 349
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bh$a;->a()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->f(Z)V

    .line 350
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bh$a;->w()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->e(Z)V

    .line 351
    new-instance v0, Lcom/yandex/metrica/impl/ob/dv;

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bh$a;->t()Lcom/yandex/metrica/impl/bh$a$b;

    move-result-object v1

    .line 352
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bh$a;->d()Z

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/dv;-><init>(Lcom/yandex/metrica/impl/bh$a$b;Z)V

    .line 351
    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->a(Lcom/yandex/metrica/impl/ob/dv;)V

    .line 353
    new-instance v0, Lcom/yandex/metrica/impl/ob/dq;

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bh$a;->u()Lcom/yandex/metrica/impl/bh$a$a;

    move-result-object v1

    .line 354
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bh$a;->e()Z

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/dq;-><init>(Lcom/yandex/metrica/impl/bh$a$a;Z)V

    .line 353
    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->a(Lcom/yandex/metrica/impl/ob/dq;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 357
    monitor-exit p0

    return-void

    .line 337
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method a(Lcom/yandex/metrica/impl/ob/dg;)V
    .locals 1

    .prologue
    .line 179
    .line 7197
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/dg;->i()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->b(Z)V

    .line 180
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/bb;->b(Lcom/yandex/metrica/impl/ob/dg;)V

    .line 7205
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/dg;->k()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->d(Z)V

    .line 7209
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/dg;->l()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->e(Z)V

    .line 7213
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/dg;->m()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->f(Z)V

    .line 184
    return-void
.end method

.method public a(Lcom/yandex/metrica/impl/ob/dq;)V
    .locals 0

    .prologue
    .line 403
    iput-object p1, p0, Lcom/yandex/metrica/impl/bb;->j:Lcom/yandex/metrica/impl/ob/dq;

    .line 404
    return-void
.end method

.method public a(Lcom/yandex/metrica/impl/ob/v;)V
    .locals 6

    .prologue
    .line 143
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->n()Landroid/content/Context;

    move-result-object v1

    .line 144
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->m()Lcom/yandex/metrica/impl/ob/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/t;->b()Ljava/lang/String;

    move-result-object v2

    .line 145
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->k()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v3

    .line 146
    invoke-static {v1}, Lcom/yandex/metrica/impl/interact/DeviceInfo;->getInstance(Landroid/content/Context;)Lcom/yandex/metrica/impl/interact/DeviceInfo;

    move-result-object v0

    .line 148
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->F()Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v4

    .line 151
    invoke-static {v1, v3, v2}, Lcom/yandex/metrica/impl/bl;->a(Landroid/content/Context;Lcom/yandex/metrica/CounterConfiguration;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/yandex/metrica/impl/bb;->b:Ljava/lang/String;

    .line 153
    invoke-virtual {p0, v1, v3}, Lcom/yandex/metrica/impl/bb;->a(Landroid/content/Context;Lcom/yandex/metrica/CounterConfiguration;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/yandex/metrica/impl/bb;->m(Ljava/lang/String;)V

    .line 154
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/bd;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/yandex/metrica/impl/bb;->b(Ljava/lang/String;)V

    .line 156
    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->a(Lcom/yandex/metrica/impl/interact/DeviceInfo;)V

    .line 157
    invoke-virtual {p0, p1, v4}, Lcom/yandex/metrica/impl/bb;->a(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/dg;)V

    .line 158
    invoke-direct {p0, p1, v4}, Lcom/yandex/metrica/impl/bb;->b(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/dg;)V

    .line 160
    invoke-direct {p0, v4}, Lcom/yandex/metrica/impl/bb;->c(Lcom/yandex/metrica/impl/ob/dg;)V

    .line 1283
    invoke-virtual {v3}, Lcom/yandex/metrica/CounterConfiguration;->o()Ljava/lang/String;

    move-result-object v0

    .line 1356
    invoke-static {v0}, Lcom/yandex/metrica/impl/bj;->a(Ljava/lang/String;)Z

    move-result v5

    .line 1284
    if-eqz v5, :cond_0

    .line 1285
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/bb;->n()Ljava/lang/String;

    move-result-object v0

    .line 2356
    invoke-static {v0}, Lcom/yandex/metrica/impl/bj;->a(Ljava/lang/String;)Z

    move-result v5

    .line 1286
    if-eqz v5, :cond_0

    .line 1287
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/bl;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1291
    :cond_0
    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->g(Ljava/lang/String;)V

    .line 3295
    invoke-virtual {v3}, Lcom/yandex/metrica/CounterConfiguration;->p()Ljava/lang/String;

    move-result-object v0

    .line 3356
    invoke-static {v0}, Lcom/yandex/metrica/impl/bj;->a(Ljava/lang/String;)Z

    move-result v3

    .line 3296
    if-eqz v3, :cond_1

    .line 3297
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/bb;->m()Ljava/lang/String;

    move-result-object v0

    .line 4356
    invoke-static {v0}, Lcom/yandex/metrica/impl/bj;->a(Ljava/lang/String;)Z

    move-result v3

    .line 3298
    if-eqz v3, :cond_1

    .line 3299
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/bl;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3303
    :cond_1
    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->f(Ljava/lang/String;)V

    .line 5307
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 5309
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/bb;->d(Lcom/yandex/metrica/impl/ob/v;)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    invoke-static {v1}, Lcom/yandex/metrica/impl/bb;->a(Landroid/content/pm/ApplicationInfo;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/yandex/metrica/impl/bb;->i:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    :goto_0
    invoke-direct {p0, v4}, Lcom/yandex/metrica/impl/bb;->d(Lcom/yandex/metrica/impl/ob/dg;)V

    .line 169
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/bb;->e(Lcom/yandex/metrica/impl/ob/v;)V

    .line 170
    return-void

    .line 5311
    :catch_0
    move-exception v1

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->m()Lcom/yandex/metrica/impl/ob/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/t;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5312
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/yandex/metrica/impl/bb;->a(Landroid/content/pm/ApplicationInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/bb;->i:Ljava/lang/String;

    goto :goto_0

    .line 5314
    :cond_2
    const-string v0, "0"

    iput-object v0, p0, Lcom/yandex/metrica/impl/bb;->i:Ljava/lang/String;

    goto :goto_0
.end method

.method a(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/dg;)V
    .locals 1

    .prologue
    .line 174
    .line 6242
    const-string v0, ""

    invoke-virtual {p2, v0}, Lcom/yandex/metrica/impl/ob/dg;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->n(Ljava/lang/String;)V

    .line 6243
    const-string v0, ""

    invoke-virtual {p2, v0}, Lcom/yandex/metrica/impl/ob/dg;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->o(Ljava/lang/String;)V

    .line 6244
    invoke-virtual {p2}, Lcom/yandex/metrica/impl/ob/dg;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->b(Ljava/util/List;)V

    .line 6245
    invoke-virtual {p2}, Lcom/yandex/metrica/impl/ob/dg;->b()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->a(Ljava/util/List;)V

    .line 6246
    invoke-virtual {p2}, Lcom/yandex/metrica/impl/ob/dg;->c()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->c(Ljava/util/List;)V

    .line 6247
    invoke-virtual {p2}, Lcom/yandex/metrica/impl/ob/dg;->d()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->d(Ljava/util/List;)V

    .line 6250
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/bb;->f(Lcom/yandex/metrica/impl/ob/v;)V

    .line 175
    invoke-virtual {p0, p2}, Lcom/yandex/metrica/impl/bb;->a(Lcom/yandex/metrica/impl/ob/dg;)V

    .line 176
    return-void
.end method

.method public declared-synchronized a(J)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 130
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/bb;->R()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 139
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 1029
    :cond_1
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 134
    sub-long/2addr v2, p1

    .line 135
    const-wide/32 v4, 0x15180

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 139
    const/4 v0, 0x1

    goto :goto_0

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method b(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    invoke-static {p1}, Lcom/yandex/metrica/impl/interact/DeviceInfo;->getInstance(Landroid/content/Context;)Lcom/yandex/metrica/impl/interact/DeviceInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/yandex/metrica/impl/interact/DeviceInfo;->deviceType:Ljava/lang/String;

    return-object v0
.end method

.method b(Lcom/yandex/metrica/impl/ob/v;)V
    .locals 1

    .prologue
    .line 187
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->F()Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v0

    .line 188
    invoke-direct {p0, p1, v0}, Lcom/yandex/metrica/impl/bb;->b(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/dg;)V

    .line 190
    invoke-virtual {p0, p1, v0}, Lcom/yandex/metrica/impl/bb;->a(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/dg;)V

    .line 191
    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/bb;->b(Lcom/yandex/metrica/impl/ob/dg;)V

    .line 192
    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/bb;->d(Lcom/yandex/metrica/impl/ob/dg;)V

    .line 193
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/bb;->e(Lcom/yandex/metrica/impl/ob/v;)V

    .line 194
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 81
    iput-boolean p1, p0, Lcom/yandex/metrica/impl/bb;->c:Z

    .line 82
    return-void
.end method

.method public c(Lcom/yandex/metrica/impl/ob/v;)V
    .locals 1

    .prologue
    .line 221
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->n()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/yandex/metrica/impl/interact/DeviceInfo;->getInstance(Landroid/content/Context;)Lcom/yandex/metrica/impl/interact/DeviceInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->a(Lcom/yandex/metrica/impl/interact/DeviceInfo;)V

    .line 222
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->F()Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/bb;->c(Lcom/yandex/metrica/impl/ob/dg;)V

    .line 223
    return-void
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/yandex/metrica/impl/bb;->d:Z

    .line 98
    return-void
.end method

.method d(Lcom/yandex/metrica/impl/ob/v;)Landroid/content/pm/ApplicationInfo;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation

    .prologue
    .line 321
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 322
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->m()Lcom/yandex/metrica/impl/ob/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/t;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 321
    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    return-object v0
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/yandex/metrica/impl/bb;->e:Z

    .line 102
    return-void
.end method

.method public declared-synchronized e(Lcom/yandex/metrica/impl/ob/v;)V
    .locals 2

    .prologue
    .line 360
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->k()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    .line 363
    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/yandex/metrica/impl/bb;->h(Ljava/lang/String;)V

    .line 365
    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/yandex/metrica/impl/bb;->p(Ljava/lang/String;)V

    .line 367
    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/yandex/metrica/impl/bb;->g(Ljava/lang/String;)V

    .line 368
    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/yandex/metrica/impl/bb;->f(Ljava/lang/String;)V

    .line 369
    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->E()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->k(Ljava/lang/String;)V

    .line 11101
    sget-boolean v0, Lcom/yandex/metrica/impl/bd$a;->a:Z

    .line 370
    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bb;->a(Z)V

    .line 372
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/bb;->f(Lcom/yandex/metrica/impl/ob/v;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 373
    monitor-exit p0

    return-void

    .line 360
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e(Z)V
    .locals 0

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/yandex/metrica/impl/bb;->f:Z

    .line 106
    return-void
.end method

.method public f(Z)V
    .locals 3

    .prologue
    .line 113
    iput-boolean p1, p0, Lcom/yandex/metrica/impl/bb;->k:Z

    .line 114
    invoke-static {}, Lcom/yandex/metrica/impl/ob/g;->a()Lcom/yandex/metrica/impl/ob/g;

    move-result-object v0

    new-instance v1, Lcom/yandex/metrica/impl/ob/q;

    iget-boolean v2, p0, Lcom/yandex/metrica/impl/bb;->k:Z

    invoke-direct {v1, v2}, Lcom/yandex/metrica/impl/ob/q;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/g;->b(Lcom/yandex/metrica/impl/ob/i;)V

    .line 115
    return-void
.end method

.method public declared-synchronized p(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 61
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/yandex/metrica/impl/bb;->a:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    monitor-exit p0

    return-void

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public q(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/yandex/metrica/impl/bb;->g:Ljava/lang/String;

    .line 70
    return-void
.end method

.method public r(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/yandex/metrica/impl/bb;->h:Ljava/lang/String;

    .line 123
    return-void
.end method

.method public s(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 411
    iput-object p1, p0, Lcom/yandex/metrica/impl/bb;->i:Ljava/lang/String;

    .line 412
    return-void
.end method
