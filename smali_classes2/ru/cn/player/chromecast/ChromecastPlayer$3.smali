.class Lru/cn/player/chromecast/ChromecastPlayer$3;
.super Ljava/lang/Object;
.source "ChromecastPlayer.java"

# interfaces
.implements Lcom/google/android/gms/common/api/ResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/player/chromecast/ChromecastPlayer;->resume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/api/ResultCallback",
        "<",
        "Lcom/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/player/chromecast/ChromecastPlayer;


# direct methods
.method constructor <init>(Lru/cn/player/chromecast/ChromecastPlayer;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/player/chromecast/ChromecastPlayer;

    .prologue
    .line 136
    iput-object p1, p0, Lru/cn/player/chromecast/ChromecastPlayer$3;->this$0:Lru/cn/player/chromecast/ChromecastPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult;)V
    .locals 2
    .param p1, "rez"    # Lcom/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult;

    .prologue
    .line 139
    invoke-interface {p1}, Lcom/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lru/cn/player/chromecast/ChromecastPlayer$3;->this$0:Lru/cn/player/chromecast/ChromecastPlayer;

    invoke-static {v0}, Lru/cn/player/chromecast/ChromecastPlayer;->access$600(Lru/cn/player/chromecast/ChromecastPlayer;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "play"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    iget-object v0, p0, Lru/cn/player/chromecast/ChromecastPlayer$3;->this$0:Lru/cn/player/chromecast/ChromecastPlayer;

    sget-object v1, Lru/cn/player/AbstractMediaPlayer$PlayerState;->PLAYING:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    invoke-static {v0, v1}, Lru/cn/player/chromecast/ChromecastPlayer;->access$700(Lru/cn/player/chromecast/ChromecastPlayer;Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    .line 143
    :cond_0
    return-void
.end method

.method public bridge synthetic onResult(Lcom/google/android/gms/common/api/Result;)V
    .locals 0

    .prologue
    .line 136
    check-cast p1, Lcom/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult;

    invoke-virtual {p0, p1}, Lru/cn/player/chromecast/ChromecastPlayer$3;->onResult(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult;)V

    return-void
.end method
