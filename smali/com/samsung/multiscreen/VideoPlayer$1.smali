.class Lcom/samsung/multiscreen/VideoPlayer$1;
.super Ljava/lang/Object;
.source "VideoPlayer.java"

# interfaces
.implements Lcom/samsung/multiscreen/Result;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/multiscreen/VideoPlayer;->addToList(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/samsung/multiscreen/Result",
        "<",
        "Lcom/samsung/multiscreen/Player$DMPStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/multiscreen/VideoPlayer;

.field final synthetic val$videoList:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/VideoPlayer;Ljava/util/List;)V
    .locals 0
    .param p1, "this$0"    # Lcom/samsung/multiscreen/VideoPlayer;

    .prologue
    .line 272
    iput-object p1, p0, Lcom/samsung/multiscreen/VideoPlayer$1;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    iput-object p2, p0, Lcom/samsung/multiscreen/VideoPlayer$1;->val$videoList:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/samsung/multiscreen/Error;)V
    .locals 3
    .param p1, "error"    # Lcom/samsung/multiscreen/Error;

    .prologue
    .line 328
    iget-object v0, p0, Lcom/samsung/multiscreen/VideoPlayer$1;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-virtual {v0}, Lcom/samsung/multiscreen/VideoPlayer;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "VideoPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enQueue() Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/multiscreen/Error;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    :cond_0
    return-void
.end method

.method public onSuccess(Lcom/samsung/multiscreen/Player$DMPStatus;)V
    .locals 10
    .param p1, "status"    # Lcom/samsung/multiscreen/Player$DMPStatus;

    .prologue
    .line 275
    if-nez p1, :cond_1

    .line 276
    const-string v7, "VideoPlayer"

    const-string v8, "Error : Something went wrong with Node server!"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    :cond_0
    :goto_0
    return-void

    .line 285
    :cond_1
    iget-object v7, p1, Lcom/samsung/multiscreen/Player$DMPStatus;->mDMPRunning:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_6

    iget-object v7, p1, Lcom/samsung/multiscreen/Player$DMPStatus;->mRunning:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 286
    const/4 v5, 0x0

    .local v5, "it":I
    :goto_1
    iget-object v7, p0, Lcom/samsung/multiscreen/VideoPlayer$1;->val$videoList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ge v5, v7, :cond_0

    .line 287
    const/4 v0, 0x0

    .line 288
    .local v0, "contentThumbUri":Landroid/net/Uri;
    const/4 v1, 0x0

    .line 290
    .local v1, "contentTitle":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/multiscreen/VideoPlayer$1;->val$videoList:Ljava/util/List;

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map;

    .line 292
    .local v6, "item":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v7, "uri"

    invoke-interface {v6, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 293
    const-string v7, "uri"

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 300
    .local v2, "contentUri":Landroid/net/Uri;
    sget-object v7, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->title:Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

    invoke-virtual {v7}, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 301
    sget-object v7, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->title:Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

    invoke-virtual {v7}, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "contentTitle":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 304
    .restart local v1    # "contentTitle":Ljava/lang/String;
    :cond_2
    sget-object v7, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->thumbnailUrl:Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

    invoke-virtual {v7}, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 305
    sget-object v7, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->thumbnailUrl:Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

    invoke-virtual {v7}, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 308
    :cond_3
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 310
    .local v3, "data":Lorg/json/JSONObject;
    :try_start_0
    const-string v7, "subEvent"

    sget-object v8, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->enqueue:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v8}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 311
    const-string v7, "playerType"

    sget-object v8, Lcom/samsung/multiscreen/Player$ContentType;->video:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-virtual {v8}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 312
    const-string v7, "uri"

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 313
    sget-object v7, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->title:Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

    invoke-virtual {v7}, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 314
    if-eqz v0, :cond_4

    .line 315
    sget-object v7, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->thumbnailUrl:Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

    invoke-virtual {v7}, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 319
    :cond_4
    :goto_2
    sget-object v7, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v8, "playerQueueEvent"

    invoke-virtual {v7, v8, v3}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 286
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    .line 295
    .end local v2    # "contentUri":Landroid/net/Uri;
    .end local v3    # "data":Lorg/json/JSONObject;
    :cond_5
    iget-object v7, p0, Lcom/samsung/multiscreen/VideoPlayer$1;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-virtual {v7}, Lcom/samsung/multiscreen/VideoPlayer;->isDebug()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 296
    const-string v7, "VideoPlayer"

    const-string v8, "enQueue(): ContentUrl can not be Optional."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 316
    .restart local v2    # "contentUri":Landroid/net/Uri;
    .restart local v3    # "data":Lorg/json/JSONObject;
    :catch_0
    move-exception v4

    .line 317
    .local v4, "exception":Ljava/lang/Exception;
    const-string v7, "VideoPlayer"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "enQueue(): Error in parsing JSON object: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 322
    .end local v0    # "contentThumbUri":Landroid/net/Uri;
    .end local v1    # "contentTitle":Ljava/lang/String;
    .end local v2    # "contentUri":Landroid/net/Uri;
    .end local v3    # "data":Lorg/json/JSONObject;
    .end local v4    # "exception":Ljava/lang/Exception;
    .end local v5    # "it":I
    .end local v6    # "item":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_6
    iget-object v7, p0, Lcom/samsung/multiscreen/VideoPlayer$1;->this$0:Lcom/samsung/multiscreen/VideoPlayer;

    invoke-virtual {v7}, Lcom/samsung/multiscreen/VideoPlayer;->isDebug()Z

    move-result v7

    if-eqz v7, :cond_0

    const-string v7, "VideoPlayer"

    const-string v8, "enQueue() Error: DMP Un-Initialized!"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 272
    check-cast p1, Lcom/samsung/multiscreen/Player$DMPStatus;

    invoke-virtual {p0, p1}, Lcom/samsung/multiscreen/VideoPlayer$1;->onSuccess(Lcom/samsung/multiscreen/Player$DMPStatus;)V

    return-void
.end method
