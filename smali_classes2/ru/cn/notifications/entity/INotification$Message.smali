.class public Lru/cn/notifications/entity/INotification$Message;
.super Ljava/lang/Object;
.source "INotification.java"

# interfaces
.implements Lru/cn/notifications/entity/INotification;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/notifications/entity/INotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Message"
.end annotation


# instance fields
.field private final createTime:J

.field private final message:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;J)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "createTime"    # J

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lru/cn/notifications/entity/INotification$Message;->message:Ljava/lang/String;

    .line 14
    iput-wide p2, p0, Lru/cn/notifications/entity/INotification$Message;->createTime:J

    .line 15
    return-void
.end method


# virtual methods
.method public createTime()J
    .locals 2

    .prologue
    .line 24
    iget-wide v0, p0, Lru/cn/notifications/entity/INotification$Message;->createTime:J

    return-wide v0
.end method

.method public message()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lru/cn/notifications/entity/INotification$Message;->message:Ljava/lang/String;

    return-object v0
.end method
