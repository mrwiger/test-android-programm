.class public Lru/cn/api/provider/Channel;
.super Ljava/lang/Object;
.source "Channel.java"


# instance fields
.field public channelId:J

.field public groupTitle:Ljava/lang/String;

.field public isIntersectionChannel:Z

.field public isPornoChannel:Z

.field public locations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation
.end field

.field public number:I

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lru/cn/api/provider/Channel;->channelId:J

    .line 12
    iput v2, p0, Lru/cn/api/provider/Channel;->number:I

    .line 13
    iput-boolean v2, p0, Lru/cn/api/provider/Channel;->isPornoChannel:Z

    .line 14
    iput-boolean v2, p0, Lru/cn/api/provider/Channel;->isIntersectionChannel:Z

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/api/provider/Channel;->groupTitle:Ljava/lang/String;

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addLocation(Lru/cn/api/iptv/replies/MediaLocation;)V
    .locals 1
    .param p1, "l"    # Lru/cn/api/iptv/replies/MediaLocation;

    .prologue
    .line 40
    iget-object v0, p0, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    return-void
.end method

.method public deleteDoubles()V
    .locals 7

    .prologue
    .line 20
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 22
    .local v4, "list":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/iptv/replies/MediaLocation;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v5, p0, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_3

    .line 23
    iget-object v5, p0, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lru/cn/api/iptv/replies/MediaLocation;

    .line 24
    .local v3, "l":Lru/cn/api/iptv/replies/MediaLocation;
    const/4 v0, 0x0

    .line 25
    .local v0, "dup":Z
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 26
    iget-object v6, v3, Lru/cn/api/iptv/replies/MediaLocation;->location:Ljava/lang/String;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lru/cn/api/iptv/replies/MediaLocation;

    iget-object v5, v5, Lru/cn/api/iptv/replies/MediaLocation;->location:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 27
    const/4 v0, 0x1

    .line 31
    :cond_0
    if-nez v0, :cond_1

    .line 32
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 22
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 25
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 36
    .end local v0    # "dup":Z
    .end local v2    # "j":I
    .end local v3    # "l":Lru/cn/api/iptv/replies/MediaLocation;
    :cond_3
    iput-object v4, p0, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    .line 37
    return-void
.end method
