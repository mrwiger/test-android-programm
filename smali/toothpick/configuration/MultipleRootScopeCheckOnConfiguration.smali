.class Ltoothpick/configuration/MultipleRootScopeCheckOnConfiguration;
.super Ljava/lang/Object;
.source "MultipleRootScopeCheckOnConfiguration.java"

# interfaces
.implements Ltoothpick/configuration/MultipleRootScopeCheckConfiguration;


# instance fields
.field private rootScope:Ltoothpick/Scope;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized checkMultipleRootScopes(Ltoothpick/Scope;)V
    .locals 1
    .param p1, "scope"    # Ltoothpick/Scope;

    .prologue
    .line 14
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ltoothpick/configuration/MultipleRootScopeCheckOnConfiguration;->rootScope:Ltoothpick/Scope;

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 15
    iput-object p1, p0, Ltoothpick/configuration/MultipleRootScopeCheckOnConfiguration;->rootScope:Ltoothpick/Scope;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 24
    :cond_0
    monitor-exit p0

    return-void

    .line 19
    :cond_1
    :try_start_1
    iget-object v0, p0, Ltoothpick/configuration/MultipleRootScopeCheckOnConfiguration;->rootScope:Ltoothpick/Scope;

    if-eq p1, v0, :cond_0

    .line 23
    invoke-interface {p1}, Ltoothpick/Scope;->getParentScope()Ltoothpick/Scope;

    move-result-object v0

    if-nez v0, :cond_0

    .line 27
    new-instance v0, Ltoothpick/configuration/MultipleRootException;

    invoke-direct {v0, p1}, Ltoothpick/configuration/MultipleRootException;-><init>(Ltoothpick/Scope;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 14
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onScopeForestReset()V
    .locals 1

    .prologue
    .line 31
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Ltoothpick/configuration/MultipleRootScopeCheckOnConfiguration;->rootScope:Ltoothpick/Scope;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 32
    monitor-exit p0

    return-void

    .line 31
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
