.class Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/yandex/mobile/ads/ar$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;


# direct methods
.method constructor <init>(Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$1;->a:Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/yandex/mobile/ads/AdRequestError;)V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$1;->a:Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;

    invoke-static {v0}, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;->a(Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;)Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$OnLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$1;->a:Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;

    invoke-static {v0}, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;->a(Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;)Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$OnLoadListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$OnLoadListener;->onAdFailedToLoad(Lcom/yandex/mobile/ads/AdRequestError;)V

    .line 45
    :cond_0
    return-void
.end method

.method public a(Lcom/yandex/mobile/ads/nativeads/o;Lcom/yandex/mobile/ads/nativeads/d;)V
    .locals 3

    .prologue
    .line 30
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$1;->a:Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;

    invoke-static {v0}, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;->a(Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;)Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$OnLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 31
    invoke-virtual {p1}, Lcom/yandex/mobile/ads/nativeads/o;->d()Lcom/yandex/mobile/ads/nativeads/h;

    move-result-object v0

    .line 32
    sget-object v1, Lcom/yandex/mobile/ads/nativeads/NativeAdType;->CONTENT:Lcom/yandex/mobile/ads/nativeads/NativeAdType;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/h;->b()Lcom/yandex/mobile/ads/nativeads/NativeAdType;

    move-result-object v2

    if-ne v1, v2, :cond_1

    .line 33
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$1;->a:Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;

    invoke-static {v0}, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;->a(Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;)Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$OnLoadListener;

    move-result-object v0

    new-instance v1, Lcom/yandex/mobile/ads/nativeads/w;

    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$1;->a:Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;

    invoke-static {v2}, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;->b(Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;)Lcom/yandex/mobile/ads/ar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/yandex/mobile/ads/ar;->o()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p1, p2}, Lcom/yandex/mobile/ads/nativeads/w;-><init>(Landroid/content/Context;Lcom/yandex/mobile/ads/nativeads/o;Lcom/yandex/mobile/ads/nativeads/d;)V

    invoke-interface {v0, v1}, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$OnLoadListener;->onContentAdLoaded(Lcom/yandex/mobile/ads/nativeads/NativeContentAd;)V

    .line 38
    :cond_0
    :goto_0
    return-void

    .line 34
    :cond_1
    sget-object v1, Lcom/yandex/mobile/ads/nativeads/NativeAdType;->APP_INSTALL:Lcom/yandex/mobile/ads/nativeads/NativeAdType;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/h;->b()Lcom/yandex/mobile/ads/nativeads/NativeAdType;

    move-result-object v0

    if-ne v1, v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$1;->a:Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;

    invoke-static {v0}, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;->a(Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;)Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$OnLoadListener;

    move-result-object v0

    new-instance v1, Lcom/yandex/mobile/ads/nativeads/t;

    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$1;->a:Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;

    invoke-static {v2}, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;->b(Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;)Lcom/yandex/mobile/ads/ar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/yandex/mobile/ads/ar;->o()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p1, p2}, Lcom/yandex/mobile/ads/nativeads/t;-><init>(Landroid/content/Context;Lcom/yandex/mobile/ads/nativeads/o;Lcom/yandex/mobile/ads/nativeads/d;)V

    invoke-interface {v0, v1}, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$OnLoadListener;->onAppInstallAdLoaded(Lcom/yandex/mobile/ads/nativeads/NativeAppInstallAd;)V

    goto :goto_0
.end method
