.class public Lcom/annimon/stream/Stream;
.super Ljava/lang/Object;
.source "Stream.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Closeable;"
    }
.end annotation


# instance fields
.field private final iterator:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<+TT;>;"
        }
    .end annotation
.end field

.field private final params:Lcom/annimon/stream/internal/Params;


# direct methods
.method constructor <init>(Lcom/annimon/stream/internal/Params;Ljava/util/Iterator;)V
    .locals 0
    .param p1, "params"    # Lcom/annimon/stream/internal/Params;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/annimon/stream/internal/Params;",
            "Ljava/util/Iterator",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 460
    .local p0, "this":Lcom/annimon/stream/Stream;, "Lcom/annimon/stream/Stream<TT;>;"
    .local p2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<+TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 461
    iput-object p1, p0, Lcom/annimon/stream/Stream;->params:Lcom/annimon/stream/internal/Params;

    .line 462
    iput-object p2, p0, Lcom/annimon/stream/Stream;->iterator:Ljava/util/Iterator;

    .line 463
    return-void
.end method

.method private constructor <init>(Ljava/lang/Iterable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 453
    .local p0, "this":Lcom/annimon/stream/Stream;, "Lcom/annimon/stream/Stream<TT;>;"
    .local p1, "iterable":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+TT;>;"
    const/4 v0, 0x0

    new-instance v1, Lcom/annimon/stream/iterator/LazyIterator;

    invoke-direct {v1, p1}, Lcom/annimon/stream/iterator/LazyIterator;-><init>(Ljava/lang/Iterable;)V

    invoke-direct {p0, v0, v1}, Lcom/annimon/stream/Stream;-><init>(Lcom/annimon/stream/internal/Params;Ljava/util/Iterator;)V

    .line 454
    return-void
.end method

.method private constructor <init>(Ljava/util/Iterator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 449
    .local p0, "this":Lcom/annimon/stream/Stream;, "Lcom/annimon/stream/Stream<TT;>;"
    .local p1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<+TT;>;"
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/annimon/stream/Stream;-><init>(Lcom/annimon/stream/internal/Params;Ljava/util/Iterator;)V

    .line 450
    return-void
.end method

.method public static empty()Lcom/annimon/stream/Stream;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/annimon/stream/Stream",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 33
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v0

    return-object v0
.end method

.method private match(Lcom/annimon/stream/function/Predicate;I)Z
    .locals 7
    .param p2, "matchKind"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/annimon/stream/function/Predicate",
            "<-TT;>;I)Z"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/annimon/stream/Stream;, "Lcom/annimon/stream/Stream<TT;>;"
    .local p1, "predicate":Lcom/annimon/stream/function/Predicate;, "Lcom/annimon/stream/function/Predicate<-TT;>;"
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 2050
    if-nez p2, :cond_2

    move v1, v4

    .line 2051
    .local v1, "kindAny":Z
    :goto_0
    if-ne p2, v4, :cond_3

    move v0, v4

    .line 2053
    .local v0, "kindAll":Z
    :cond_0
    :goto_1
    iget-object v6, p0, Lcom/annimon/stream/Stream;->iterator:Ljava/util/Iterator;

    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2054
    iget-object v6, p0, Lcom/annimon/stream/Stream;->iterator:Ljava/util/Iterator;

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 2070
    .local v3, "value":Ljava/lang/Object;, "TT;"
    invoke-interface {p1, v3}, Lcom/annimon/stream/function/Predicate;->test(Ljava/lang/Object;)Z

    move-result v2

    .line 2071
    .local v2, "match":Z
    xor-int v6, v2, v0

    if-eqz v6, :cond_0

    .line 2072
    if-eqz v1, :cond_4

    if-eqz v2, :cond_4

    .line 2078
    .end local v2    # "match":Z
    .end local v3    # "value":Ljava/lang/Object;, "TT;"
    :cond_1
    :goto_2
    return v4

    .end local v0    # "kindAll":Z
    .end local v1    # "kindAny":Z
    :cond_2
    move v1, v5

    .line 2050
    goto :goto_0

    .restart local v1    # "kindAny":Z
    :cond_3
    move v0, v5

    .line 2051
    goto :goto_1

    .restart local v0    # "kindAll":Z
    .restart local v2    # "match":Z
    .restart local v3    # "value":Ljava/lang/Object;, "TT;"
    :cond_4
    move v4, v5

    .line 2072
    goto :goto_2

    .line 2078
    .end local v2    # "match":Z
    .end local v3    # "value":Ljava/lang/Object;, "TT;"
    :cond_5
    if-eqz v1, :cond_1

    move v4, v5

    goto :goto_2
.end method

.method public static of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<+TT;>;)",
            "Lcom/annimon/stream/Stream",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 72
    .local p0, "iterable":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+TT;>;"
    invoke-static {p0}, Lcom/annimon/stream/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    new-instance v0, Lcom/annimon/stream/Stream;

    invoke-direct {v0, p0}, Lcom/annimon/stream/Stream;-><init>(Ljava/lang/Iterable;)V

    return-object v0
.end method

.method public static of(Ljava/util/Map;)Lcom/annimon/stream/Stream;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map",
            "<TK;TV;>;)",
            "Lcom/annimon/stream/Stream",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 46
    .local p0, "map":Ljava/util/Map;, "Ljava/util/Map<TK;TV;>;"
    invoke-static {p0}, Lcom/annimon/stream/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    new-instance v0, Lcom/annimon/stream/Stream;

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/annimon/stream/Stream;-><init>(Ljava/lang/Iterable;)V

    return-object v0
.end method

.method public static varargs of([Ljava/lang/Object;)Lcom/annimon/stream/Stream;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)",
            "Lcom/annimon/stream/Stream",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 85
    .local p0, "elements":[Ljava/lang/Object;, "[TT;"
    invoke-static {p0}, Lcom/annimon/stream/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    array-length v0, p0

    if-nez v0, :cond_0

    .line 87
    invoke-static {}, Lcom/annimon/stream/Stream;->empty()Lcom/annimon/stream/Stream;

    move-result-object v0

    .line 89
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/annimon/stream/Stream;

    new-instance v1, Lcom/annimon/stream/operator/ObjArray;

    invoke-direct {v1, p0}, Lcom/annimon/stream/operator/ObjArray;-><init>([Ljava/lang/Object;)V

    invoke-direct {v0, v1}, Lcom/annimon/stream/Stream;-><init>(Ljava/util/Iterator;)V

    goto :goto_0
.end method


# virtual methods
.method public allMatch(Lcom/annimon/stream/function/Predicate;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/annimon/stream/function/Predicate",
            "<-TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 1816
    .local p0, "this":Lcom/annimon/stream/Stream;, "Lcom/annimon/stream/Stream<TT;>;"
    .local p1, "predicate":Lcom/annimon/stream/function/Predicate;, "Lcom/annimon/stream/function/Predicate<-TT;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/annimon/stream/Stream;->match(Lcom/annimon/stream/function/Predicate;I)Z

    move-result v0

    return v0
.end method

.method public anyMatch(Lcom/annimon/stream/function/Predicate;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/annimon/stream/function/Predicate",
            "<-TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 1793
    .local p0, "this":Lcom/annimon/stream/Stream;, "Lcom/annimon/stream/Stream<TT;>;"
    .local p1, "predicate":Lcom/annimon/stream/function/Predicate;, "Lcom/annimon/stream/function/Predicate<-TT;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/annimon/stream/Stream;->match(Lcom/annimon/stream/function/Predicate;I)Z

    move-result v0

    return v0
.end method

.method public close()V
    .locals 2

    .prologue
    .line 2039
    .local p0, "this":Lcom/annimon/stream/Stream;, "Lcom/annimon/stream/Stream<TT;>;"
    iget-object v0, p0, Lcom/annimon/stream/Stream;->params:Lcom/annimon/stream/internal/Params;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/annimon/stream/Stream;->params:Lcom/annimon/stream/internal/Params;

    iget-object v0, v0, Lcom/annimon/stream/internal/Params;->closeHandler:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 2040
    iget-object v0, p0, Lcom/annimon/stream/Stream;->params:Lcom/annimon/stream/internal/Params;

    iget-object v0, v0, Lcom/annimon/stream/internal/Params;->closeHandler:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 2041
    iget-object v0, p0, Lcom/annimon/stream/Stream;->params:Lcom/annimon/stream/internal/Params;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/annimon/stream/internal/Params;->closeHandler:Ljava/lang/Runnable;

    .line 2043
    :cond_0
    return-void
.end method

.method public collect(Lcom/annimon/stream/Collector;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "A:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/annimon/stream/Collector",
            "<-TT;TA;TR;>;)TR;"
        }
    .end annotation

    .prologue
    .line 1709
    .local p0, "this":Lcom/annimon/stream/Stream;, "Lcom/annimon/stream/Stream<TT;>;"
    .local p1, "collector":Lcom/annimon/stream/Collector;, "Lcom/annimon/stream/Collector<-TT;TA;TR;>;"
    invoke-interface {p1}, Lcom/annimon/stream/Collector;->supplier()Lcom/annimon/stream/function/Supplier;

    move-result-object v2

    invoke-interface {v2}, Lcom/annimon/stream/function/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    .line 1710
    .local v0, "container":Ljava/lang/Object;, "TA;"
    :goto_0
    iget-object v2, p0, Lcom/annimon/stream/Stream;->iterator:Ljava/util/Iterator;

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1711
    iget-object v2, p0, Lcom/annimon/stream/Stream;->iterator:Ljava/util/Iterator;

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1712
    .local v1, "value":Ljava/lang/Object;, "TT;"
    invoke-interface {p1}, Lcom/annimon/stream/Collector;->accumulator()Lcom/annimon/stream/function/BiConsumer;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcom/annimon/stream/function/BiConsumer;->accept(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 1714
    .end local v1    # "value":Ljava/lang/Object;, "TT;"
    :cond_0
    invoke-interface {p1}, Lcom/annimon/stream/Collector;->finisher()Lcom/annimon/stream/function/Function;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1715
    invoke-interface {p1}, Lcom/annimon/stream/Collector;->finisher()Lcom/annimon/stream/function/Function;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/annimon/stream/function/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 1716
    :goto_1
    return-object v2

    :cond_1
    invoke-static {}, Lcom/annimon/stream/Collectors;->castIdentity()Lcom/annimon/stream/function/Function;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/annimon/stream/function/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    goto :goto_1
.end method

.method public distinct()Lcom/annimon/stream/Stream;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/annimon/stream/Stream",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 911
    .local p0, "this":Lcom/annimon/stream/Stream;, "Lcom/annimon/stream/Stream<TT;>;"
    new-instance v0, Lcom/annimon/stream/Stream;

    iget-object v1, p0, Lcom/annimon/stream/Stream;->params:Lcom/annimon/stream/internal/Params;

    new-instance v2, Lcom/annimon/stream/operator/ObjDistinct;

    iget-object v3, p0, Lcom/annimon/stream/Stream;->iterator:Ljava/util/Iterator;

    invoke-direct {v2, v3}, Lcom/annimon/stream/operator/ObjDistinct;-><init>(Ljava/util/Iterator;)V

    invoke-direct {v0, v1, v2}, Lcom/annimon/stream/Stream;-><init>(Lcom/annimon/stream/internal/Params;Ljava/util/Iterator;)V

    return-object v0
.end method

.method public distinctBy(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/annimon/stream/function/Function",
            "<-TT;+TK;>;)",
            "Lcom/annimon/stream/Stream",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 933
    .local p0, "this":Lcom/annimon/stream/Stream;, "Lcom/annimon/stream/Stream<TT;>;"
    .local p1, "classifier":Lcom/annimon/stream/function/Function;, "Lcom/annimon/stream/function/Function<-TT;+TK;>;"
    new-instance v0, Lcom/annimon/stream/Stream;

    iget-object v1, p0, Lcom/annimon/stream/Stream;->params:Lcom/annimon/stream/internal/Params;

    new-instance v2, Lcom/annimon/stream/operator/ObjDistinctBy;

    iget-object v3, p0, Lcom/annimon/stream/Stream;->iterator:Ljava/util/Iterator;

    invoke-direct {v2, v3, p1}, Lcom/annimon/stream/operator/ObjDistinctBy;-><init>(Ljava/util/Iterator;Lcom/annimon/stream/function/Function;)V

    invoke-direct {v0, v1, v2}, Lcom/annimon/stream/Stream;-><init>(Lcom/annimon/stream/internal/Params;Ljava/util/Iterator;)V

    return-object v0
.end method

.method public filter(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/annimon/stream/function/Predicate",
            "<-TT;>;)",
            "Lcom/annimon/stream/Stream",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 562
    .local p0, "this":Lcom/annimon/stream/Stream;, "Lcom/annimon/stream/Stream<TT;>;"
    .local p1, "predicate":Lcom/annimon/stream/function/Predicate;, "Lcom/annimon/stream/function/Predicate<-TT;>;"
    new-instance v0, Lcom/annimon/stream/Stream;

    iget-object v1, p0, Lcom/annimon/stream/Stream;->params:Lcom/annimon/stream/internal/Params;

    new-instance v2, Lcom/annimon/stream/operator/ObjFilter;

    iget-object v3, p0, Lcom/annimon/stream/Stream;->iterator:Ljava/util/Iterator;

    invoke-direct {v2, v3, p1}, Lcom/annimon/stream/operator/ObjFilter;-><init>(Ljava/util/Iterator;Lcom/annimon/stream/function/Predicate;)V

    invoke-direct {v0, v1, v2}, Lcom/annimon/stream/Stream;-><init>(Lcom/annimon/stream/internal/Params;Ljava/util/Iterator;)V

    return-object v0
.end method

.method public filterNot(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/annimon/stream/function/Predicate",
            "<-TT;>;)",
            "Lcom/annimon/stream/Stream",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 626
    .local p0, "this":Lcom/annimon/stream/Stream;, "Lcom/annimon/stream/Stream<TT;>;"
    .local p1, "predicate":Lcom/annimon/stream/function/Predicate;, "Lcom/annimon/stream/function/Predicate<-TT;>;"
    invoke-static {p1}, Lcom/annimon/stream/function/Predicate$Util;->negate(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/function/Predicate;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/annimon/stream/Stream;->filter(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;

    move-result-object v0

    return-object v0
.end method

.method public findFirst()Lcom/annimon/stream/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/annimon/stream/Optional",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1909
    .local p0, "this":Lcom/annimon/stream/Stream;, "Lcom/annimon/stream/Stream<TT;>;"
    iget-object v0, p0, Lcom/annimon/stream/Stream;->iterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1910
    iget-object v0, p0, Lcom/annimon/stream/Stream;->iterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/annimon/stream/Optional;->of(Ljava/lang/Object;)Lcom/annimon/stream/Optional;

    move-result-object v0

    .line 1912
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/annimon/stream/Optional;->empty()Lcom/annimon/stream/Optional;

    move-result-object v0

    goto :goto_0
.end method

.method public forEach(Lcom/annimon/stream/function/Consumer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/annimon/stream/function/Consumer",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1478
    .local p0, "this":Lcom/annimon/stream/Stream;, "Lcom/annimon/stream/Stream<TT;>;"
    .local p1, "action":Lcom/annimon/stream/function/Consumer;, "Lcom/annimon/stream/function/Consumer<-TT;>;"
    :goto_0
    iget-object v0, p0, Lcom/annimon/stream/Stream;->iterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1479
    iget-object v0, p0, Lcom/annimon/stream/Stream;->iterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/annimon/stream/function/Consumer;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 1481
    :cond_0
    return-void
.end method

.method public forEachIndexed(IILcom/annimon/stream/function/IndexedConsumer;)V
    .locals 2
    .param p1, "from"    # I
    .param p2, "step"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/annimon/stream/function/IndexedConsumer",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1506
    .local p0, "this":Lcom/annimon/stream/Stream;, "Lcom/annimon/stream/Stream<TT;>;"
    .local p3, "action":Lcom/annimon/stream/function/IndexedConsumer;, "Lcom/annimon/stream/function/IndexedConsumer<-TT;>;"
    move v0, p1

    .line 1507
    .local v0, "index":I
    :goto_0
    iget-object v1, p0, Lcom/annimon/stream/Stream;->iterator:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1508
    iget-object v1, p0, Lcom/annimon/stream/Stream;->iterator:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p3, v0, v1}, Lcom/annimon/stream/function/IndexedConsumer;->accept(ILjava/lang/Object;)V

    .line 1509
    add-int/2addr v0, p2

    goto :goto_0

    .line 1511
    :cond_0
    return-void
.end method

.method public forEachIndexed(Lcom/annimon/stream/function/IndexedConsumer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/annimon/stream/function/IndexedConsumer",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1492
    .local p0, "this":Lcom/annimon/stream/Stream;, "Lcom/annimon/stream/Stream<TT;>;"
    .local p1, "action":Lcom/annimon/stream/function/IndexedConsumer;, "Lcom/annimon/stream/function/IndexedConsumer<-TT;>;"
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1, p1}, Lcom/annimon/stream/Stream;->forEachIndexed(IILcom/annimon/stream/function/IndexedConsumer;)V

    .line 1493
    return-void
.end method

.method public map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/annimon/stream/function/Function",
            "<-TT;+TR;>;)",
            "Lcom/annimon/stream/Stream",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 690
    .local p0, "this":Lcom/annimon/stream/Stream;, "Lcom/annimon/stream/Stream<TT;>;"
    .local p1, "mapper":Lcom/annimon/stream/function/Function;, "Lcom/annimon/stream/function/Function<-TT;+TR;>;"
    new-instance v0, Lcom/annimon/stream/Stream;

    iget-object v1, p0, Lcom/annimon/stream/Stream;->params:Lcom/annimon/stream/internal/Params;

    new-instance v2, Lcom/annimon/stream/operator/ObjMap;

    iget-object v3, p0, Lcom/annimon/stream/Stream;->iterator:Ljava/util/Iterator;

    invoke-direct {v2, v3, p1}, Lcom/annimon/stream/operator/ObjMap;-><init>(Ljava/util/Iterator;Lcom/annimon/stream/function/Function;)V

    invoke-direct {v0, v1, v2}, Lcom/annimon/stream/Stream;-><init>(Lcom/annimon/stream/internal/Params;Ljava/util/Iterator;)V

    return-object v0
.end method

.method public single()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 1959
    .local p0, "this":Lcom/annimon/stream/Stream;, "Lcom/annimon/stream/Stream<TT;>;"
    iget-object v1, p0, Lcom/annimon/stream/Stream;->iterator:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1960
    iget-object v1, p0, Lcom/annimon/stream/Stream;->iterator:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 1961
    .local v0, "singleCandidate":Ljava/lang/Object;, "TT;"
    iget-object v1, p0, Lcom/annimon/stream/Stream;->iterator:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1962
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Stream contains more than one element"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1967
    .end local v0    # "singleCandidate":Ljava/lang/Object;, "TT;"
    :cond_0
    new-instance v1, Ljava/util/NoSuchElementException;

    const-string v2, "Stream contains no element"

    invoke-direct {v1, v2}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1964
    .restart local v0    # "singleCandidate":Ljava/lang/Object;, "TT;"
    :cond_1
    return-object v0
.end method

.method public sortBy(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R::",
            "Ljava/lang/Comparable",
            "<-TR;>;>(",
            "Lcom/annimon/stream/function/Function",
            "<-TT;+TR;>;)",
            "Lcom/annimon/stream/Stream",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1001
    .local p0, "this":Lcom/annimon/stream/Stream;, "Lcom/annimon/stream/Stream<TT;>;"
    .local p1, "f":Lcom/annimon/stream/function/Function;, "Lcom/annimon/stream/function/Function<-TT;+TR;>;"
    invoke-static {p1}, Lcom/annimon/stream/ComparatorCompat;->comparing(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/ComparatorCompat;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/annimon/stream/Stream;->sorted(Ljava/util/Comparator;)Lcom/annimon/stream/Stream;

    move-result-object v0

    return-object v0
.end method

.method public sorted(Ljava/util/Comparator;)Lcom/annimon/stream/Stream;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TT;>;)",
            "Lcom/annimon/stream/Stream",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 980
    .local p0, "this":Lcom/annimon/stream/Stream;, "Lcom/annimon/stream/Stream<TT;>;"
    .local p1, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<-TT;>;"
    new-instance v0, Lcom/annimon/stream/Stream;

    iget-object v1, p0, Lcom/annimon/stream/Stream;->params:Lcom/annimon/stream/internal/Params;

    new-instance v2, Lcom/annimon/stream/operator/ObjSorted;

    iget-object v3, p0, Lcom/annimon/stream/Stream;->iterator:Ljava/util/Iterator;

    invoke-direct {v2, v3, p1}, Lcom/annimon/stream/operator/ObjSorted;-><init>(Ljava/util/Iterator;Ljava/util/Comparator;)V

    invoke-direct {v0, v1, v2}, Lcom/annimon/stream/Stream;-><init>(Lcom/annimon/stream/internal/Params;Ljava/util/Iterator;)V

    return-object v0
.end method

.method public toList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1670
    .local p0, "this":Lcom/annimon/stream/Stream;, "Lcom/annimon/stream/Stream<TT;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1671
    .local v0, "result":Ljava/util/List;, "Ljava/util/List<TT;>;"
    :goto_0
    iget-object v1, p0, Lcom/annimon/stream/Stream;->iterator:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1672
    iget-object v1, p0, Lcom/annimon/stream/Stream;->iterator:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1674
    :cond_0
    return-object v0
.end method

.method public withoutNulls()Lcom/annimon/stream/Stream;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/annimon/stream/Stream",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 658
    .local p0, "this":Lcom/annimon/stream/Stream;, "Lcom/annimon/stream/Stream<TT;>;"
    invoke-static {}, Lcom/annimon/stream/function/Predicate$Util;->notNull()Lcom/annimon/stream/function/Predicate;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/annimon/stream/Stream;->filter(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;

    move-result-object v0

    return-object v0
.end method
