.class public Lcom/google/obf/hh;
.super Landroid/widget/ImageView;
.source "IMASDK"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;

.field private final b:Lcom/google/obf/hj;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/ads/interactivemedia/v3/api/CompanionAdSlot$ClickListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/obf/hj;Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/obf/hj;",
            "Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/ads/interactivemedia/v3/api/CompanionAdSlot$ClickListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 2
    iput-object p2, p0, Lcom/google/obf/hh;->b:Lcom/google/obf/hj;

    .line 3
    iput-object p3, p0, Lcom/google/obf/hh;->a:Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;

    .line 4
    iput-object p4, p0, Lcom/google/obf/hh;->c:Ljava/lang/String;

    .line 5
    iput-object p5, p0, Lcom/google/obf/hh;->d:Ljava/util/List;

    .line 6
    invoke-virtual {p0, p0}, Lcom/google/obf/hh;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 7
    return-void
.end method

.method static synthetic a(Lcom/google/obf/hh;)Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/obf/hh;->a:Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/obf/hh;->b:Lcom/google/obf/hj;

    iget-object v1, p0, Lcom/google/obf/hh;->a:Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;->companionId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/obf/hh;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/obf/hj;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    return-void
.end method

.method static synthetic b(Lcom/google/obf/hh;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/obf/hh;->b()V

    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    .line 9
    invoke-virtual {v0}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 10
    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/obf/hh$1;

    invoke-direct {v0, p0}, Lcom/google/obf/hh$1;-><init>(Lcom/google/obf/hh;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 12
    invoke-virtual {v0, v1}, Lcom/google/obf/hh$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 13
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/obf/hh;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/v3/api/CompanionAdSlot$ClickListener;

    .line 17
    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/CompanionAdSlot$ClickListener;->onCompanionAdClick()V

    goto :goto_0

    .line 19
    :cond_0
    iget-object v0, p0, Lcom/google/obf/hh;->b:Lcom/google/obf/hj;

    iget-object v1, p0, Lcom/google/obf/hh;->a:Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;->clickThroughUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/obf/hj;->d(Ljava/lang/String;)V

    .line 20
    return-void
.end method
