.class public Lcom/yandex/metrica/impl/bh$a$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/impl/bh$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field public final c:Ljava/lang/Long;

.field public final d:Ljava/lang/Float;

.field public final e:Ljava/lang/Integer;

.field public final f:Ljava/lang/Integer;

.field public final g:Ljava/lang/Long;

.field public final h:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Float;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168
    iput-object p1, p0, Lcom/yandex/metrica/impl/bh$a$b;->c:Ljava/lang/Long;

    .line 169
    iput-object p2, p0, Lcom/yandex/metrica/impl/bh$a$b;->d:Ljava/lang/Float;

    .line 170
    iput-object p3, p0, Lcom/yandex/metrica/impl/bh$a$b;->e:Ljava/lang/Integer;

    .line 171
    iput-object p4, p0, Lcom/yandex/metrica/impl/bh$a$b;->f:Ljava/lang/Integer;

    .line 172
    iput-object p5, p0, Lcom/yandex/metrica/impl/bh$a$b;->g:Ljava/lang/Long;

    .line 173
    iput-object p6, p0, Lcom/yandex/metrica/impl/bh$a$b;->h:Ljava/lang/Integer;

    .line 174
    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 7

    .prologue
    .line 153
    const-string v0, "min_update_interval_seconds"

    .line 154
    invoke-static {p1, v0}, Lcom/yandex/metrica/impl/utils/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    const-string v0, "min_update_distance_meters"

    .line 155
    invoke-static {p1, v0}, Lcom/yandex/metrica/impl/utils/g;->c(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v2

    const-string v0, "records_count_to_force_flush"

    .line 156
    invoke-static {p1, v0}, Lcom/yandex/metrica/impl/utils/g;->b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    const-string v0, "max_records_count_in_batch"

    .line 157
    invoke-static {p1, v0}, Lcom/yandex/metrica/impl/utils/g;->b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    const-string v0, "max_age_seconds_to_force_flush"

    .line 158
    invoke-static {p1, v0}, Lcom/yandex/metrica/impl/utils/g;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    const-string v0, "max_records_to_store_locally"

    .line 159
    invoke-static {p1, v0}, Lcom/yandex/metrica/impl/utils/g;->b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    move-object v0, p0

    .line 153
    invoke-direct/range {v0 .. v6}, Lcom/yandex/metrica/impl/bh$a$b;-><init>(Ljava/lang/Long;Ljava/lang/Float;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;)V

    .line 161
    return-void
.end method
