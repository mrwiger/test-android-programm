.class Ltoothpick/configuration/RuntimeCheckOnConfiguration;
.super Ljava/lang/Object;
.source "RuntimeCheckOnConfiguration.java"

# interfaces
.implements Ltoothpick/configuration/RuntimeCheckConfiguration;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;
    }
.end annotation


# instance fields
.field private cycleDetectionStack:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/LinkedHashSet",
            "<",
            "Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ltoothpick/configuration/RuntimeCheckOnConfiguration$1;

    invoke-direct {v0, p0}, Ltoothpick/configuration/RuntimeCheckOnConfiguration$1;-><init>(Ltoothpick/configuration/RuntimeCheckOnConfiguration;)V

    iput-object v0, p0, Ltoothpick/configuration/RuntimeCheckOnConfiguration;->cycleDetectionStack:Ljava/lang/ThreadLocal;

    return-void
.end method


# virtual methods
.method public checkCyclesEnd(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 2
    .param p1, "clazz"    # Ljava/lang/Class;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 71
    iget-object v0, p0, Ltoothpick/configuration/RuntimeCheckOnConfiguration;->cycleDetectionStack:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedHashSet;

    new-instance v1, Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;

    invoke-direct {v1, p1, p2}, Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->remove(Ljava/lang/Object;)Z

    .line 72
    return-void
.end method

.method public checkCyclesStart(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 4
    .param p1, "clazz"    # Ljava/lang/Class;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 60
    new-instance v1, Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;

    invoke-direct {v1, p1, p2}, Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    .line 61
    .local v1, "pair":Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;
    iget-object v2, p0, Ltoothpick/configuration/RuntimeCheckOnConfiguration;->cycleDetectionStack:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedHashSet;

    .line 62
    .local v0, "linkedHashSet":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;>;"
    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 63
    new-instance v2, Ltoothpick/configuration/CyclicDependencyException;

    invoke-static {v0}, Ltoothpick/configuration/RuntimeCheckOnConfiguration$Pair;->access$000(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3, p1}, Ltoothpick/configuration/CyclicDependencyException;-><init>(Ljava/util/List;Ljava/lang/Class;)V

    throw v2

    .line 66
    :cond_0
    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 67
    return-void
.end method

.method public checkIllegalBinding(Ltoothpick/config/Binding;Ltoothpick/Scope;)V
    .locals 8
    .param p1, "binding"    # Ltoothpick/config/Binding;
    .param p2, "scope"    # Ltoothpick/Scope;

    .prologue
    const/4 v4, 0x0

    .line 32
    sget-object v3, Ltoothpick/configuration/RuntimeCheckOnConfiguration$2;->$SwitchMap$toothpick$config$Binding$Mode:[I

    invoke-virtual {p1}, Ltoothpick/config/Binding;->getMode()Ltoothpick/config/Binding$Mode;

    move-result-object v5

    invoke-virtual {v5}, Ltoothpick/config/Binding$Mode;->ordinal()I

    move-result v5

    aget v3, v3, v5

    packed-switch v3, :pswitch_data_0

    .line 56
    :cond_0
    return-void

    .line 34
    :pswitch_0
    invoke-virtual {p1}, Ltoothpick/config/Binding;->getKey()Ljava/lang/Class;

    move-result-object v2

    .line 46
    .local v2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_0
    invoke-virtual {v2}, Ljava/lang/Class;->getAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v5

    array-length v6, v5

    move v3, v4

    :goto_1
    if-ge v3, v6, :cond_0

    aget-object v0, v5, v3

    .line 47
    .local v0, "annotation":Ljava/lang/annotation/Annotation;
    invoke-interface {v0}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v1

    .line 48
    .local v1, "annotationType":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/annotation/Annotation;>;"
    const-class v7, Ljavax/inject/Scope;

    invoke-virtual {v1, v7}, Ljava/lang/Class;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 49
    invoke-interface {p2, v1}, Ltoothpick/Scope;->isBoundToScopeAnnotation(Ljava/lang/Class;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 50
    new-instance v3, Ltoothpick/configuration/IllegalBindingException;

    const-string v5, "Class %s cannot be bound. It has a scope annotation: %s that is not supported by current scope: %s"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    .line 52
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    const/4 v4, 0x1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    const/4 v4, 0x2

    invoke-interface {p2}, Ltoothpick/Scope;->getName()Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v4

    .line 50
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ltoothpick/configuration/IllegalBindingException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 37
    .end local v0    # "annotation":Ljava/lang/annotation/Annotation;
    .end local v1    # "annotationType":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/annotation/Annotation;>;"
    .end local v2    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :pswitch_1
    invoke-virtual {p1}, Ltoothpick/config/Binding;->getImplementationClass()Ljava/lang/Class;

    move-result-object v2

    .line 38
    .restart local v2    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    goto :goto_0

    .line 40
    .end local v2    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :pswitch_2
    invoke-virtual {p1}, Ltoothpick/config/Binding;->getProviderClass()Ljava/lang/Class;

    move-result-object v2

    .line 41
    .restart local v2    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    goto :goto_0

    .line 46
    .restart local v0    # "annotation":Ljava/lang/annotation/Annotation;
    .restart local v1    # "annotationType":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/annotation/Annotation;>;"
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 32
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
