.class public final enum Lru/cn/draggableview/DraggableView$DragOrientation;
.super Ljava/lang/Enum;
.source "DraggableView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/draggableview/DraggableView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DragOrientation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/draggableview/DraggableView$DragOrientation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/draggableview/DraggableView$DragOrientation;

.field public static final enum ORIENTATION_HORIZONTAL:Lru/cn/draggableview/DraggableView$DragOrientation;

.field public static final enum ORIENTATION_UNSPEC:Lru/cn/draggableview/DraggableView$DragOrientation;

.field public static final enum ORIENTATION_VERTICAL:Lru/cn/draggableview/DraggableView$DragOrientation;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 118
    new-instance v0, Lru/cn/draggableview/DraggableView$DragOrientation;

    const-string v1, "ORIENTATION_VERTICAL"

    invoke-direct {v0, v1, v2}, Lru/cn/draggableview/DraggableView$DragOrientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_VERTICAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    new-instance v0, Lru/cn/draggableview/DraggableView$DragOrientation;

    const-string v1, "ORIENTATION_HORIZONTAL"

    invoke-direct {v0, v1, v3}, Lru/cn/draggableview/DraggableView$DragOrientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_HORIZONTAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    new-instance v0, Lru/cn/draggableview/DraggableView$DragOrientation;

    const-string v1, "ORIENTATION_UNSPEC"

    invoke-direct {v0, v1, v4}, Lru/cn/draggableview/DraggableView$DragOrientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_UNSPEC:Lru/cn/draggableview/DraggableView$DragOrientation;

    .line 117
    const/4 v0, 0x3

    new-array v0, v0, [Lru/cn/draggableview/DraggableView$DragOrientation;

    sget-object v1, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_VERTICAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    aput-object v1, v0, v2

    sget-object v1, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_HORIZONTAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    aput-object v1, v0, v3

    sget-object v1, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_UNSPEC:Lru/cn/draggableview/DraggableView$DragOrientation;

    aput-object v1, v0, v4

    sput-object v0, Lru/cn/draggableview/DraggableView$DragOrientation;->$VALUES:[Lru/cn/draggableview/DraggableView$DragOrientation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 117
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/draggableview/DraggableView$DragOrientation;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 117
    const-class v0, Lru/cn/draggableview/DraggableView$DragOrientation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/draggableview/DraggableView$DragOrientation;

    return-object v0
.end method

.method public static values()[Lru/cn/draggableview/DraggableView$DragOrientation;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lru/cn/draggableview/DraggableView$DragOrientation;->$VALUES:[Lru/cn/draggableview/DraggableView$DragOrientation;

    invoke-virtual {v0}, [Lru/cn/draggableview/DraggableView$DragOrientation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/draggableview/DraggableView$DragOrientation;

    return-object v0
.end method
