.class public interface abstract Lcom/my/target/v;
.super Ljava/lang/Object;
.source "JsEvent.java"


# static fields
.field public static final aD:Ljava/lang/String; = "onReady"

.field public static final aE:Ljava/lang/String; = "onExpand"

.field public static final aF:Ljava/lang/String; = "onCollapse"

.field public static final aG:Ljava/lang/String; = "onError"

.field public static final aH:Ljava/lang/String; = "onAdError"

.field public static final aI:Ljava/lang/String; = "onCloseClick"

.field public static final aJ:Ljava/lang/String; = "onComplete"

.field public static final aK:Ljava/lang/String; = "onNoAd"

.field public static final aL:Ljava/lang/String; = "onAdStart"

.field public static final aM:Ljava/lang/String; = "onAdStop"

.field public static final aN:Ljava/lang/String; = "onAdPause"

.field public static final aO:Ljava/lang/String; = "onAdResume"

.field public static final aP:Ljava/lang/String; = "onAdComplete"

.field public static final aQ:Ljava/lang/String; = "onAdClick"

.field public static final aR:Ljava/lang/String; = "onStat"

.field public static final aS:Ljava/lang/String; = "onSizeChange"

.field public static final aT:Ljava/lang/String; = "onRequestNewAds"


# virtual methods
.method public abstract getType()Ljava/lang/String;
.end method
