.class Lru/cn/tv/player/FloatingPlayerFragment$4;
.super Ljava/lang/Object;
.source "FloatingPlayerFragment.java"

# interfaces
.implements Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/player/FloatingPlayerFragment;->instantiateTelecastInfoFragment()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/player/FloatingPlayerFragment;

.field final synthetic val$fragment:Lru/cn/tv/mobile/telecast/TelecastInfoFragment;


# direct methods
.method constructor <init>(Lru/cn/tv/player/FloatingPlayerFragment;Lru/cn/tv/mobile/telecast/TelecastInfoFragment;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/player/FloatingPlayerFragment;

    .prologue
    .line 355
    iput-object p1, p0, Lru/cn/tv/player/FloatingPlayerFragment$4;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    iput-object p2, p0, Lru/cn/tv/player/FloatingPlayerFragment$4;->val$fragment:Lru/cn/tv/mobile/telecast/TelecastInfoFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRecordSelected(J)V
    .locals 7
    .param p1, "telecastId"    # J

    .prologue
    .line 358
    iget-object v1, p0, Lru/cn/tv/player/FloatingPlayerFragment$4;->val$fragment:Lru/cn/tv/mobile/telecast/TelecastInfoFragment;

    invoke-virtual {v1}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->getRelatedRubric()Lru/cn/api/catalogue/replies/Rubric;

    move-result-object v0

    .line 359
    .local v0, "r":Lru/cn/api/catalogue/replies/Rubric;
    if-eqz v0, :cond_0

    .line 360
    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-wide v4, v0, Lru/cn/api/catalogue/replies/Rubric;->id:J

    invoke-static {v1, v2, v4, v5}, Lru/cn/domain/statistics/inetra/InetraTracker;->setSessionParams(IIJ)V

    .line 364
    iget-object v1, p0, Lru/cn/tv/player/FloatingPlayerFragment$4;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v1, p1, p2}, Lru/cn/tv/player/FloatingPlayerFragment;->playTelecast(J)V

    .line 366
    :cond_0
    return-void
.end method
