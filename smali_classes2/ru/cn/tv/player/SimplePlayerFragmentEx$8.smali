.class Lru/cn/tv/player/SimplePlayerFragmentEx$8;
.super Ljava/lang/Object;
.source "SimplePlayerFragmentEx.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/player/SimplePlayerFragmentEx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;


# direct methods
.method constructor <init>(Lru/cn/tv/player/SimplePlayerFragmentEx;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/player/SimplePlayerFragmentEx;

    .prologue
    .line 783
    iput-object p1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$8;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 786
    sget-object v0, Lru/cn/tv/player/SimplePlayerFragmentEx$10;->$SwitchMap$ru$cn$player$SimplePlayer$FitMode:[I

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$8;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    iget-object v1, v1, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v1}, Lru/cn/player/SimplePlayer;->getFitMode()Lru/cn/player/SimplePlayer$FitMode;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/player/SimplePlayer$FitMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 796
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$8;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    sget-object v1, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_HEIGHT:Lru/cn/player/SimplePlayer$FitMode;

    invoke-virtual {v0, v1}, Lru/cn/tv/player/SimplePlayerFragmentEx;->setFitMode(Lru/cn/player/SimplePlayer$FitMode;)Z

    .line 800
    :goto_0
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$8;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->access$000(Lru/cn/tv/player/SimplePlayerFragmentEx;)Lru/cn/tv/player/controller/TouchPlayerController;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$8;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    iget-object v1, v1, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v1}, Lru/cn/player/SimplePlayer;->getFitMode()Lru/cn/player/SimplePlayer$FitMode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/cn/tv/player/controller/TouchPlayerController;->setFitMode(Lru/cn/player/SimplePlayer$FitMode;)V

    .line 801
    return-void

    .line 788
    :pswitch_0
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$8;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    sget-object v1, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_HEIGHT:Lru/cn/player/SimplePlayer$FitMode;

    invoke-virtual {v0, v1}, Lru/cn/tv/player/SimplePlayerFragmentEx;->setFitMode(Lru/cn/player/SimplePlayer$FitMode;)Z

    goto :goto_0

    .line 792
    :pswitch_1
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$8;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    sget-object v1, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_WIDTH:Lru/cn/player/SimplePlayer$FitMode;

    invoke-virtual {v0, v1}, Lru/cn/tv/player/SimplePlayerFragmentEx;->setFitMode(Lru/cn/player/SimplePlayer$FitMode;)Z

    goto :goto_0

    .line 786
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
