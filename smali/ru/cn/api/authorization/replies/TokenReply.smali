.class public final Lru/cn/api/authorization/replies/TokenReply;
.super Ljava/lang/Object;
.source "TokenReply.java"


# instance fields
.field public accessToken:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "access_token"
    .end annotation
.end field

.field public error:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "error"
    .end annotation
.end field

.field public expiresIn:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "expires_in"
    .end annotation
.end field

.field public refreshToken:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "refresh_token"
    .end annotation
.end field

.field public tokenType:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "token_type"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public token()Lru/cn/api/authorization/Account$Token;
    .locals 6

    .prologue
    .line 28
    iget-object v1, p0, Lru/cn/api/authorization/replies/TokenReply;->error:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 29
    const/4 v0, 0x0

    .line 39
    :goto_0
    return-object v0

    .line 31
    :cond_0
    new-instance v0, Lru/cn/api/authorization/Account$Token;

    invoke-direct {v0}, Lru/cn/api/authorization/Account$Token;-><init>()V

    .line 32
    .local v0, "token":Lru/cn/api/authorization/Account$Token;
    iget-object v1, p0, Lru/cn/api/authorization/replies/TokenReply;->accessToken:Ljava/lang/String;

    iput-object v1, v0, Lru/cn/api/authorization/Account$Token;->accessToken:Ljava/lang/String;

    .line 33
    iget-object v1, p0, Lru/cn/api/authorization/replies/TokenReply;->refreshToken:Ljava/lang/String;

    iput-object v1, v0, Lru/cn/api/authorization/Account$Token;->refreshToken:Ljava/lang/String;

    .line 34
    iget-object v1, p0, Lru/cn/api/authorization/replies/TokenReply;->tokenType:Ljava/lang/String;

    iput-object v1, v0, Lru/cn/api/authorization/Account$Token;->type:Ljava/lang/String;

    .line 36
    iget-wide v2, p0, Lru/cn/api/authorization/replies/TokenReply;->expiresIn:J

    iput-wide v2, v0, Lru/cn/api/authorization/Account$Token;->expiresIn:J

    .line 37
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    iput-wide v2, v0, Lru/cn/api/authorization/Account$Token;->createdAt:J

    goto :goto_0
.end method
