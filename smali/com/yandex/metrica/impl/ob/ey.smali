.class public Lcom/yandex/metrica/impl/ob/ey;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/ey;->a:Ljava/lang/String;

    .line 21
    iput-boolean p2, p0, Lcom/yandex/metrica/impl/ob/ey;->b:Z

    .line 22
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/ob/ey;->b:Z

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ey;->a:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 34
    if-ne p0, p1, :cond_1

    const/4 v0, 0x1

    .line 40
    .end local p1    # "o":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .line 35
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 37
    check-cast p1, Lcom/yandex/metrica/impl/ob/ey;

    .line 39
    .end local p1    # "o":Ljava/lang/Object;
    iget-boolean v1, p0, Lcom/yandex/metrica/impl/ob/ey;->b:Z

    iget-boolean v2, p1, Lcom/yandex/metrica/impl/ob/ey;->b:Z

    if-ne v1, v2, :cond_0

    .line 40
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ey;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/yandex/metrica/impl/ob/ey;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ey;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 47
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/yandex/metrica/impl/ob/ey;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    .line 48
    return v0

    .line 47
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
