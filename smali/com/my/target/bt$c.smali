.class Lcom/my/target/bt$c;
.super Landroid/webkit/WebViewClient;
.source "BannerWebView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/bt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic if:Lcom/my/target/bt;


# direct methods
.method private constructor <init>(Lcom/my/target/bt;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/my/target/bt$c;->if:Lcom/my/target/bt;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/my/target/bt;Lcom/my/target/bt$1;)V
    .locals 0

    .prologue
    .line 169
    invoke-direct {p0, p1}, Lcom/my/target/bt$c;-><init>(Lcom/my/target/bt;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 216
    iget-object v0, p0, Lcom/my/target/bt$c;->if:Lcom/my/target/bt;

    invoke-static {v0}, Lcom/my/target/bt;->d(Lcom/my/target/bt;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/my/target/bt$c;->if:Lcom/my/target/bt;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/my/target/bt;->b(Lcom/my/target/bt;Z)Z

    .line 219
    const-string v0, "page loaded"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 220
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 221
    iget-object v0, p0, Lcom/my/target/bt$c;->if:Lcom/my/target/bt;

    invoke-static {v0}, Lcom/my/target/bt;->e(Lcom/my/target/bt;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 225
    :try_start_0
    iget-object v0, p0, Lcom/my/target/bt$c;->if:Lcom/my/target/bt;

    new-instance v1, Lcom/my/target/n;

    iget-object v2, p0, Lcom/my/target/bt$c;->if:Lcom/my/target/bt;

    invoke-static {v2}, Lcom/my/target/bt;->e(Lcom/my/target/bt;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/my/target/n;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {v0, v1}, Lcom/my/target/bt;->a(Lcom/my/target/m;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    :cond_0
    :goto_0
    return-void

    .line 227
    :catch_0
    move-exception v0

    .line 229
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "js call executing error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "favicon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 209
    const-string v0, "load page started"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 210
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 211
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 239
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "load failed. error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " description: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " url: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 240
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 241
    iget-object v0, p0, Lcom/my/target/bt$c;->if:Lcom/my/target/bt;

    invoke-static {v0}, Lcom/my/target/bt;->a(Lcom/my/target/bt;)Lcom/my/target/bt$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/my/target/bt$c;->if:Lcom/my/target/bt;

    invoke-static {v0}, Lcom/my/target/bt;->a(Lcom/my/target/bt;)Lcom/my/target/bt$a;

    move-result-object v0

    if-eqz p3, :cond_1

    .end local p3    # "description":Ljava/lang/String;
    :goto_0
    invoke-interface {v0, p3}, Lcom/my/target/bt$a;->onError(Ljava/lang/String;)V

    .line 245
    :cond_0
    return-void

    .line 243
    .restart local p3    # "description":Ljava/lang/String;
    :cond_1
    const-string p3, "unknown JS error"

    goto :goto_0
.end method

.method public onReceivedError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceError;)V
    .locals 5
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "request"    # Landroid/webkit/WebResourceRequest;
    .param p3, "error"    # Landroid/webkit/WebResourceError;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    .line 251
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceError;)V

    .line 252
    invoke-virtual {p3}, Landroid/webkit/WebResourceError;->getDescription()Ljava/lang/CharSequence;

    move-result-object v1

    .line 253
    const/4 v0, 0x0

    .line 254
    if-eqz v1, :cond_0

    .line 256
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 258
    :cond_0
    invoke-virtual {p3}, Landroid/webkit/WebResourceError;->getErrorCode()I

    move-result v1

    .line 259
    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->getUrl()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 260
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "load failed. error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " description: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " url: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 261
    iget-object v1, p0, Lcom/my/target/bt$c;->if:Lcom/my/target/bt;

    invoke-static {v1}, Lcom/my/target/bt;->a(Lcom/my/target/bt;)Lcom/my/target/bt$a;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 263
    iget-object v1, p0, Lcom/my/target/bt$c;->if:Lcom/my/target/bt;

    invoke-static {v1}, Lcom/my/target/bt;->a(Lcom/my/target/bt;)Lcom/my/target/bt$a;

    move-result-object v1

    if-eqz v0, :cond_2

    :goto_0
    invoke-interface {v1, v0}, Lcom/my/target/bt$a;->onError(Ljava/lang/String;)V

    .line 265
    :cond_1
    return-void

    .line 263
    :cond_2
    const-string v0, "Unknown JS error"

    goto :goto_0
.end method

.method public onScaleChanged(Landroid/webkit/WebView;FF)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "oldScale"    # F
    .param p3, "newScale"    # F

    .prologue
    .line 270
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onScaleChanged(Landroid/webkit/WebView;FF)V

    .line 271
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "scale new: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " old: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 272
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;)Z
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "request"    # Landroid/webkit/WebResourceRequest;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x18
    .end annotation

    .prologue
    .line 189
    iget-object v0, p0, Lcom/my/target/bt$c;->if:Lcom/my/target/bt;

    invoke-static {v0}, Lcom/my/target/bt;->b(Lcom/my/target/bt;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->getUrl()Landroid/net/Uri;

    move-result-object v0

    .line 192
    if-eqz v0, :cond_0

    .line 194
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 195
    if-eqz v0, :cond_0

    const-string v1, "adman://onEvent,"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 197
    iget-object v1, p0, Lcom/my/target/bt$c;->if:Lcom/my/target/bt;

    invoke-static {v1, v0}, Lcom/my/target/bt;->a(Lcom/my/target/bt;Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lcom/my/target/bt$c;->if:Lcom/my/target/bt;

    invoke-static {v0}, Lcom/my/target/bt;->c(Lcom/my/target/bt;)V

    .line 203
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/my/target/bt$c;->if:Lcom/my/target/bt;

    invoke-static {v0}, Lcom/my/target/bt;->b(Lcom/my/target/bt;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    if-eqz p2, :cond_0

    const-string v0, "adman://onEvent,"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/my/target/bt$c;->if:Lcom/my/target/bt;

    invoke-static {v0, p2}, Lcom/my/target/bt;->a(Lcom/my/target/bt;Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/my/target/bt$c;->if:Lcom/my/target/bt;

    invoke-static {v0}, Lcom/my/target/bt;->c(Lcom/my/target/bt;)V

    .line 182
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
