.class public final Lcom/my/target/dj;
.super Landroid/widget/FrameLayout;
.source "StandardNativeHostFlipView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/dj$b;,
        Lcom/my/target/dj$a;
    }
.end annotation


# instance fields
.field private aA:Z

.field private final at:Landroid/widget/ViewFlipper;

.field private final au:Landroid/widget/ViewFlipper;

.field private final av:Landroid/widget/ViewFlipper;

.field private final aw:Lcom/my/target/dj$b;

.field private ax:I

.field private ay:Lcom/my/target/dj$a;

.field private az:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/my/target/dj;-><init>(Landroid/content/Context;B)V

    .line 35
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/my/target/dj;-><init>(Landroid/content/Context;C)V

    .line 40
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;C)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    new-instance v0, Landroid/widget/ViewFlipper;

    invoke-direct {v0, p1}, Landroid/widget/ViewFlipper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dj;->at:Landroid/widget/ViewFlipper;

    .line 46
    new-instance v0, Landroid/widget/ViewFlipper;

    invoke-direct {v0, p1}, Landroid/widget/ViewFlipper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dj;->au:Landroid/widget/ViewFlipper;

    .line 47
    new-instance v0, Landroid/widget/ViewFlipper;

    invoke-direct {v0, p1}, Landroid/widget/ViewFlipper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dj;->av:Landroid/widget/ViewFlipper;

    .line 48
    iget-object v0, p0, Lcom/my/target/dj;->at:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/my/target/dj;->au:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->addView(Landroid/view/View;)V

    .line 49
    iget-object v0, p0, Lcom/my/target/dj;->at:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/my/target/dj;->av:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->addView(Landroid/view/View;)V

    .line 50
    iput v2, p0, Lcom/my/target/dj;->ax:I

    .line 51
    iget-object v0, p0, Lcom/my/target/dj;->at:Landroid/widget/ViewFlipper;

    invoke-virtual {p0, v0}, Lcom/my/target/dj;->addView(Landroid/view/View;)V

    .line 52
    new-instance v0, Lcom/my/target/dj$b;

    invoke-direct {v0, p0, v2}, Lcom/my/target/dj$b;-><init>(Lcom/my/target/dj;B)V

    iput-object v0, p0, Lcom/my/target/dj;->aw:Lcom/my/target/dj$b;

    .line 53
    return-void
.end method

.method static synthetic a(Lcom/my/target/dj;I)I
    .locals 0

    .prologue
    .line 20
    iput p1, p0, Lcom/my/target/dj;->az:I

    return p1
.end method

.method static synthetic a(Lcom/my/target/dj;)Lcom/my/target/dj$a;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/my/target/dj;->ay:Lcom/my/target/dj$a;

    return-object v0
.end method

.method static synthetic b(Lcom/my/target/dj;)I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/my/target/dj;->az:I

    return v0
.end method

.method static synthetic c(Lcom/my/target/dj;)Z
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/dj;->aA:Z

    return v0
.end method


# virtual methods
.method public final K()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 82
    iget v0, p0, Lcom/my/target/dj;->ax:I

    if-nez v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/my/target/dj;->at:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v2}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 85
    iput v2, p0, Lcom/my/target/dj;->ax:I

    .line 92
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/my/target/dj;->at:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 90
    iput v1, p0, Lcom/my/target/dj;->ax:I

    goto :goto_0
.end method

.method public final L()Z
    .locals 2

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/my/target/dj;->getCurrentFlipper()Landroid/widget/ViewFlipper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v0

    invoke-virtual {p0}, Lcom/my/target/dj;->getCurrentFlipper()Landroid/widget/ViewFlipper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ViewFlipper;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final M()V
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/dj;->aA:Z

    .line 159
    invoke-virtual {p0}, Lcom/my/target/dj;->getCurrentFlipper()Landroid/widget/ViewFlipper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->showNext()V

    .line 160
    return-void
.end method

.method public final N()V
    .locals 2

    .prologue
    .line 164
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/dj;->aA:Z

    .line 165
    invoke-virtual {p0}, Lcom/my/target/dj;->getCurrentFlipper()Landroid/widget/ViewFlipper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 166
    return-void
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/dm;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 57
    invoke-virtual {p0}, Lcom/my/target/dj;->getCurrentFlipper()Landroid/widget/ViewFlipper;

    move-result-object v1

    .line 58
    invoke-virtual {v1}, Landroid/widget/ViewFlipper;->removeAllViews()V

    .line 59
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/dm;

    .line 61
    invoke-interface {v0}, Lcom/my/target/dm;->O()Landroid/view/View;

    move-result-object v0

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0, v3}, Landroid/widget/ViewFlipper;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 63
    :cond_0
    return-void
.end method

.method public final getBackgroundFlipper()Landroid/widget/ViewFlipper;
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/my/target/dj;->ax:I

    if-nez v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/my/target/dj;->av:Landroid/widget/ViewFlipper;

    .line 114
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/my/target/dj;->au:Landroid/widget/ViewFlipper;

    goto :goto_0
.end method

.method public final getCurrentFlipper()Landroid/widget/ViewFlipper;
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/my/target/dj;->ax:I

    if-nez v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/my/target/dj;->au:Landroid/widget/ViewFlipper;

    .line 102
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/my/target/dj;->av:Landroid/widget/ViewFlipper;

    goto :goto_0
.end method

.method public final getDisplayedBannerNumber()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/my/target/dj;->az:I

    return v0
.end method

.method public final isAnimating()Z
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/my/target/dj;->aA:Z

    return v0
.end method

.method public final setAnimationEndListener(Lcom/my/target/dj$a;)V
    .locals 0
    .param p1, "animationEndListener"    # Lcom/my/target/dj$a;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/my/target/dj;->ay:Lcom/my/target/dj$a;

    .line 144
    return-void
.end method

.method public final setAnimationType(I)V
    .locals 14
    .param p1, "animationType"    # I

    .prologue
    const-wide/16 v12, 0xa

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v1, 0x2

    .line 126
    .line 1194
    if-nez p1, :cond_0

    .line 2108
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v4, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 2109
    invoke-virtual {v0, v12, v13}, Landroid/view/animation/Animation;->setDuration(J)V

    move-object v11, v0

    .line 3176
    :goto_0
    if-nez p1, :cond_2

    .line 4115
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v2, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 4116
    invoke-virtual {v0, v12, v13}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 4117
    invoke-virtual {v0, v12, v13}, Landroid/view/animation/Animation;->setDuration(J)V

    move-object v2, v0

    .line 129
    :goto_1
    iget-object v0, p0, Lcom/my/target/dj;->aw:Lcom/my/target/dj$b;

    invoke-virtual {v11, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 131
    iget-object v0, p0, Lcom/my/target/dj;->at:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v11}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 132
    iget-object v0, p0, Lcom/my/target/dj;->at:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v2}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 134
    iget-object v0, p0, Lcom/my/target/dj;->au:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v11}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 135
    iget-object v0, p0, Lcom/my/target/dj;->au:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v2}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 137
    iget-object v0, p0, Lcom/my/target/dj;->av:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v11}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 138
    iget-object v0, p0, Lcom/my/target/dj;->av:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v2}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 139
    return-void

    .line 1198
    :cond_0
    if-ne p1, v1, :cond_1

    .line 1200
    invoke-static {}, Lcom/my/target/core/presenters/a;->B()J

    move-result-wide v10

    .line 3097
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    move v3, v1

    move v5, v1

    move v6, v4

    move v7, v1

    move v8, v4

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 3101
    invoke-virtual {v0, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 3102
    new-instance v3, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    move-object v11, v0

    .line 1200
    goto :goto_0

    .line 1204
    :cond_1
    invoke-static {}, Lcom/my/target/core/presenters/a;->B()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/my/target/do;->b(J)Landroid/view/animation/Animation;

    move-result-object v0

    move-object v11, v0

    goto :goto_0

    .line 3180
    :cond_2
    if-ne p1, v1, :cond_3

    .line 3182
    invoke-static {}, Lcom/my/target/core/presenters/a;->B()J

    move-result-wide v12

    .line 5086
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    const/high16 v6, -0x40800000    # -1.0f

    move v3, v1

    move v5, v1

    move v7, v1

    move v8, v4

    move v9, v1

    move v10, v4

    invoke-direct/range {v2 .. v10}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 5090
    invoke-virtual {v2, v12, v13}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 5091
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    goto :goto_1

    .line 3186
    :cond_3
    invoke-static {}, Lcom/my/target/core/presenters/a;->B()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/my/target/do;->c(J)Landroid/view/animation/Animation;

    move-result-object v2

    goto :goto_1
.end method
