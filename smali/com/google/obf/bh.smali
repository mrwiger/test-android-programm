.class public final Lcom/google/obf/bh;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/aj;
.implements Lcom/google/obf/aq;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/bh$a;
    }
.end annotation


# static fields
.field private static final a:I


# instance fields
.field private final b:Lcom/google/obf/dw;

.field private final c:Lcom/google/obf/dw;

.field private final d:Lcom/google/obf/dw;

.field private final e:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/google/obf/bc$a;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:I

.field private i:J

.field private j:I

.field private k:Lcom/google/obf/dw;

.field private l:I

.field private m:I

.field private n:I

.field private o:Lcom/google/obf/al;

.field private p:[Lcom/google/obf/bh$a;

.field private q:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 200
    const-string v0, "qt  "

    invoke-static {v0}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/obf/bh;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/google/obf/dw;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/bh;->d:Lcom/google/obf/dw;

    .line 3
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/obf/bh;->e:Ljava/util/Stack;

    .line 4
    new-instance v0, Lcom/google/obf/dw;

    sget-object v1, Lcom/google/obf/du;->a:[B

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>([B)V

    iput-object v0, p0, Lcom/google/obf/bh;->b:Lcom/google/obf/dw;

    .line 5
    new-instance v0, Lcom/google/obf/dw;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/bh;->c:Lcom/google/obf/dw;

    .line 6
    invoke-direct {p0}, Lcom/google/obf/bh;->d()V

    .line 7
    return-void
.end method

.method private a(J)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/4 v3, 0x3

    .line 86
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/obf/bh;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/obf/bh;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bc$a;

    iget-wide v0, v0, Lcom/google/obf/bc$a;->aP:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_2

    .line 87
    iget-object v0, p0, Lcom/google/obf/bh;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bc$a;

    .line 88
    iget v1, v0, Lcom/google/obf/bc$a;->aO:I

    sget v2, Lcom/google/obf/bc;->B:I

    if-ne v1, v2, :cond_1

    .line 89
    invoke-direct {p0, v0}, Lcom/google/obf/bh;->a(Lcom/google/obf/bc$a;)V

    .line 90
    iget-object v0, p0, Lcom/google/obf/bh;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 91
    iput v3, p0, Lcom/google/obf/bh;->g:I

    goto :goto_0

    .line 92
    :cond_1
    iget-object v1, p0, Lcom/google/obf/bh;->e:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 93
    iget-object v1, p0, Lcom/google/obf/bh;->e:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/obf/bc$a;

    invoke-virtual {v1, v0}, Lcom/google/obf/bc$a;->a(Lcom/google/obf/bc$a;)V

    goto :goto_0

    .line 95
    :cond_2
    iget v0, p0, Lcom/google/obf/bh;->g:I

    if-eq v0, v3, :cond_3

    .line 96
    invoke-direct {p0}, Lcom/google/obf/bh;->d()V

    .line 97
    :cond_3
    return-void
.end method

.method private a(Lcom/google/obf/bc$a;)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 107
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 108
    const-wide v4, 0x7fffffffffffffffL

    .line 109
    const/4 v0, 0x0

    .line 110
    sget v1, Lcom/google/obf/bc;->az:I

    invoke-virtual {p1, v1}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v1

    .line 111
    if-eqz v1, :cond_4

    .line 112
    iget-boolean v0, p0, Lcom/google/obf/bh;->q:Z

    invoke-static {v1, v0}, Lcom/google/obf/bd;->a(Lcom/google/obf/bc$b;Z)Lcom/google/obf/an;

    move-result-object v0

    move-object v1, v0

    :goto_0
    move v2, v3

    .line 113
    :goto_1
    iget-object v0, p1, Lcom/google/obf/bc$a;->aR:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 114
    iget-object v0, p1, Lcom/google/obf/bc$a;->aR:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bc$a;

    .line 115
    iget v6, v0, Lcom/google/obf/bc$a;->aO:I

    sget v7, Lcom/google/obf/bc;->D:I

    if-eq v6, v7, :cond_1

    .line 136
    :cond_0
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 117
    :cond_1
    sget v6, Lcom/google/obf/bc;->C:I

    invoke-virtual {p1, v6}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v6

    const-wide/16 v10, -0x1

    iget-boolean v7, p0, Lcom/google/obf/bh;->q:Z

    invoke-static {v0, v6, v10, v11, v7}, Lcom/google/obf/bd;->a(Lcom/google/obf/bc$a;Lcom/google/obf/bc$b;JZ)Lcom/google/obf/bk;

    move-result-object v6

    .line 118
    if-eqz v6, :cond_0

    .line 120
    sget v7, Lcom/google/obf/bc;->E:I

    invoke-virtual {v0, v7}, Lcom/google/obf/bc$a;->e(I)Lcom/google/obf/bc$a;

    move-result-object v0

    sget v7, Lcom/google/obf/bc;->F:I

    .line 121
    invoke-virtual {v0, v7}, Lcom/google/obf/bc$a;->e(I)Lcom/google/obf/bc$a;

    move-result-object v0

    sget v7, Lcom/google/obf/bc;->G:I

    invoke-virtual {v0, v7}, Lcom/google/obf/bc$a;->e(I)Lcom/google/obf/bc$a;

    move-result-object v0

    .line 122
    invoke-static {v6, v0}, Lcom/google/obf/bd;->a(Lcom/google/obf/bk;Lcom/google/obf/bc$a;)Lcom/google/obf/bn;

    move-result-object v7

    .line 123
    iget v0, v7, Lcom/google/obf/bn;->a:I

    if-eqz v0, :cond_0

    .line 125
    new-instance v9, Lcom/google/obf/bh$a;

    iget-object v0, p0, Lcom/google/obf/bh;->o:Lcom/google/obf/al;

    invoke-interface {v0, v2}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v0

    invoke-direct {v9, v6, v7, v0}, Lcom/google/obf/bh$a;-><init>(Lcom/google/obf/bk;Lcom/google/obf/bn;Lcom/google/obf/ar;)V

    .line 126
    iget v0, v7, Lcom/google/obf/bn;->d:I

    add-int/lit8 v0, v0, 0x1e

    .line 127
    iget-object v6, v6, Lcom/google/obf/bk;->l:Lcom/google/obf/q;

    invoke-virtual {v6, v0}, Lcom/google/obf/q;->a(I)Lcom/google/obf/q;

    move-result-object v0

    .line 128
    if-eqz v1, :cond_2

    .line 129
    iget v6, v1, Lcom/google/obf/an;->a:I

    iget v10, v1, Lcom/google/obf/an;->b:I

    .line 130
    invoke-virtual {v0, v6, v10}, Lcom/google/obf/q;->a(II)Lcom/google/obf/q;

    move-result-object v0

    .line 131
    :cond_2
    iget-object v6, v9, Lcom/google/obf/bh$a;->c:Lcom/google/obf/ar;

    invoke-interface {v6, v0}, Lcom/google/obf/ar;->a(Lcom/google/obf/q;)V

    .line 132
    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    iget-object v0, v7, Lcom/google/obf/bn;->b:[J

    aget-wide v6, v0, v3

    .line 134
    cmp-long v0, v6, v4

    if-gez v0, :cond_0

    move-wide v4, v6

    .line 135
    goto :goto_2

    .line 137
    :cond_3
    new-array v0, v3, [Lcom/google/obf/bh$a;

    invoke-interface {v8, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/obf/bh$a;

    iput-object v0, p0, Lcom/google/obf/bh;->p:[Lcom/google/obf/bh$a;

    .line 138
    iget-object v0, p0, Lcom/google/obf/bh;->o:Lcom/google/obf/al;

    invoke-interface {v0}, Lcom/google/obf/al;->f()V

    .line 139
    iget-object v0, p0, Lcom/google/obf/bh;->o:Lcom/google/obf/al;

    invoke-interface {v0, p0}, Lcom/google/obf/al;->a(Lcom/google/obf/aq;)V

    .line 140
    return-void

    :cond_4
    move-object v1, v0

    goto/16 :goto_0
.end method

.method private static a(I)Z
    .locals 1

    .prologue
    .line 198
    sget v0, Lcom/google/obf/bc;->R:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->C:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->S:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->T:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->am:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->an:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->ao:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->Q:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->ap:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->aq:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->ar:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->as:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->at:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->O:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->a:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->az:I

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/google/obf/dw;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 98
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/google/obf/dw;->c(I)V

    .line 99
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v1

    .line 100
    sget v2, Lcom/google/obf/bh;->a:I

    if-ne v1, v2, :cond_0

    .line 106
    :goto_0
    return v0

    .line 102
    :cond_0
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/google/obf/dw;->d(I)V

    .line 103
    :cond_1
    invoke-virtual {p0}, Lcom/google/obf/dw;->b()I

    move-result v1

    if-lez v1, :cond_2

    .line 104
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v1

    sget v2, Lcom/google/obf/bh;->a:I

    if-ne v1, v2, :cond_1

    goto :goto_0

    .line 106
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(I)Z
    .locals 1

    .prologue
    .line 199
    sget v0, Lcom/google/obf/bc;->B:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->D:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->E:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->F:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->G:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->P:I

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/google/obf/ak;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v1, 0x1

    const/16 v8, 0x8

    const/4 v2, 0x0

    .line 44
    iget v0, p0, Lcom/google/obf/bh;->j:I

    if-nez v0, :cond_1

    .line 45
    iget-object v0, p0, Lcom/google/obf/bh;->d:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    invoke-interface {p1, v0, v2, v8, v1}, Lcom/google/obf/ak;->a([BIIZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    :goto_0
    return v2

    .line 47
    :cond_0
    iput v8, p0, Lcom/google/obf/bh;->j:I

    .line 48
    iget-object v0, p0, Lcom/google/obf/bh;->d:Lcom/google/obf/dw;

    invoke-virtual {v0, v2}, Lcom/google/obf/dw;->c(I)V

    .line 49
    iget-object v0, p0, Lcom/google/obf/bh;->d:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->k()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/obf/bh;->i:J

    .line 50
    iget-object v0, p0, Lcom/google/obf/bh;->d:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->m()I

    move-result v0

    iput v0, p0, Lcom/google/obf/bh;->h:I

    .line 51
    :cond_1
    iget-wide v4, p0, Lcom/google/obf/bh;->i:J

    const-wide/16 v6, 0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_2

    .line 53
    iget-object v0, p0, Lcom/google/obf/bh;->d:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    invoke-interface {p1, v0, v8, v8}, Lcom/google/obf/ak;->b([BII)V

    .line 54
    iget v0, p0, Lcom/google/obf/bh;->j:I

    add-int/2addr v0, v8

    iput v0, p0, Lcom/google/obf/bh;->j:I

    .line 55
    iget-object v0, p0, Lcom/google/obf/bh;->d:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->u()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/obf/bh;->i:J

    .line 56
    :cond_2
    iget v0, p0, Lcom/google/obf/bh;->h:I

    invoke-static {v0}, Lcom/google/obf/bh;->b(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 57
    invoke-interface {p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/obf/bh;->i:J

    add-long/2addr v2, v4

    iget v0, p0, Lcom/google/obf/bh;->j:I

    int-to-long v4, v0

    sub-long/2addr v2, v4

    .line 58
    iget-object v0, p0, Lcom/google/obf/bh;->e:Ljava/util/Stack;

    new-instance v4, Lcom/google/obf/bc$a;

    iget v5, p0, Lcom/google/obf/bh;->h:I

    invoke-direct {v4, v5, v2, v3}, Lcom/google/obf/bc$a;-><init>(IJ)V

    invoke-virtual {v0, v4}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 59
    iget-wide v4, p0, Lcom/google/obf/bh;->i:J

    iget v0, p0, Lcom/google/obf/bh;->j:I

    int-to-long v6, v0

    cmp-long v0, v4, v6

    if-nez v0, :cond_3

    .line 60
    invoke-direct {p0, v2, v3}, Lcom/google/obf/bh;->a(J)V

    :goto_1
    move v2, v1

    .line 70
    goto :goto_0

    .line 61
    :cond_3
    invoke-direct {p0}, Lcom/google/obf/bh;->d()V

    goto :goto_1

    .line 62
    :cond_4
    iget v0, p0, Lcom/google/obf/bh;->h:I

    invoke-static {v0}, Lcom/google/obf/bh;->a(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 63
    iget v0, p0, Lcom/google/obf/bh;->j:I

    if-ne v0, v8, :cond_5

    move v0, v1

    :goto_2
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 64
    iget-wide v4, p0, Lcom/google/obf/bh;->i:J

    const-wide/32 v6, 0x7fffffff

    cmp-long v0, v4, v6

    if-gtz v0, :cond_6

    move v0, v1

    :goto_3
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 65
    new-instance v0, Lcom/google/obf/dw;

    iget-wide v4, p0, Lcom/google/obf/bh;->i:J

    long-to-int v3, v4

    invoke-direct {v0, v3}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/bh;->k:Lcom/google/obf/dw;

    .line 66
    iget-object v0, p0, Lcom/google/obf/bh;->d:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    iget-object v3, p0, Lcom/google/obf/bh;->k:Lcom/google/obf/dw;

    iget-object v3, v3, Lcom/google/obf/dw;->a:[B

    invoke-static {v0, v2, v3, v2, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 67
    iput v9, p0, Lcom/google/obf/bh;->g:I

    goto :goto_1

    :cond_5
    move v0, v2

    .line 63
    goto :goto_2

    :cond_6
    move v0, v2

    .line 64
    goto :goto_3

    .line 68
    :cond_7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/bh;->k:Lcom/google/obf/dw;

    .line 69
    iput v9, p0, Lcom/google/obf/bh;->g:I

    goto :goto_1
.end method

.method private b(Lcom/google/obf/ak;Lcom/google/obf/ao;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 71
    iget-wide v4, p0, Lcom/google/obf/bh;->i:J

    iget v0, p0, Lcom/google/obf/bh;->j:I

    int-to-long v6, v0

    sub-long/2addr v4, v6

    .line 72
    invoke-interface {p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v6

    add-long/2addr v6, v4

    .line 74
    iget-object v0, p0, Lcom/google/obf/bh;->k:Lcom/google/obf/dw;

    if-eqz v0, :cond_1

    .line 75
    iget-object v0, p0, Lcom/google/obf/bh;->k:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/bh;->j:I

    long-to-int v4, v4

    invoke-interface {p1, v0, v3, v4}, Lcom/google/obf/ak;->b([BII)V

    .line 76
    iget v0, p0, Lcom/google/obf/bh;->h:I

    sget v3, Lcom/google/obf/bc;->a:I

    if-ne v0, v3, :cond_0

    .line 77
    iget-object v0, p0, Lcom/google/obf/bh;->k:Lcom/google/obf/dw;

    invoke-static {v0}, Lcom/google/obf/bh;->a(Lcom/google/obf/dw;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/obf/bh;->q:Z

    move v0, v1

    .line 84
    :goto_0
    invoke-direct {p0, v6, v7}, Lcom/google/obf/bh;->a(J)V

    .line 85
    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/obf/bh;->g:I

    const/4 v3, 0x3

    if-eq v0, v3, :cond_3

    :goto_1
    return v2

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/google/obf/bh;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 79
    iget-object v0, p0, Lcom/google/obf/bh;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bc$a;

    new-instance v3, Lcom/google/obf/bc$b;

    iget v4, p0, Lcom/google/obf/bh;->h:I

    iget-object v5, p0, Lcom/google/obf/bh;->k:Lcom/google/obf/dw;

    invoke-direct {v3, v4, v5}, Lcom/google/obf/bc$b;-><init>(ILcom/google/obf/dw;)V

    invoke-virtual {v0, v3}, Lcom/google/obf/bc$a;->a(Lcom/google/obf/bc$b;)V

    move v0, v1

    goto :goto_0

    .line 80
    :cond_1
    const-wide/32 v8, 0x40000

    cmp-long v0, v4, v8

    if-gez v0, :cond_2

    .line 81
    long-to-int v0, v4

    invoke-interface {p1, v0}, Lcom/google/obf/ak;->b(I)V

    move v0, v1

    goto :goto_0

    .line 82
    :cond_2
    invoke-interface {p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v8

    add-long/2addr v4, v8

    iput-wide v4, p2, Lcom/google/obf/ao;->a:J

    move v0, v2

    .line 83
    goto :goto_0

    :cond_3
    move v2, v1

    .line 85
    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method private c(Lcom/google/obf/ak;Lcom/google/obf/ao;)I
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/google/obf/bh;->e()I

    move-result v0

    .line 142
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 143
    const/4 v0, -0x1

    .line 184
    :goto_0
    return v0

    .line 144
    :cond_0
    iget-object v1, p0, Lcom/google/obf/bh;->p:[Lcom/google/obf/bh$a;

    aget-object v0, v1, v0

    .line 145
    iget-object v1, v0, Lcom/google/obf/bh$a;->c:Lcom/google/obf/ar;

    .line 146
    iget v4, v0, Lcom/google/obf/bh$a;->d:I

    .line 147
    iget-object v2, v0, Lcom/google/obf/bh$a;->b:Lcom/google/obf/bn;

    iget-object v2, v2, Lcom/google/obf/bn;->b:[J

    aget-wide v2, v2, v4

    .line 148
    invoke-interface {p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v6

    sub-long v6, v2, v6

    iget v5, p0, Lcom/google/obf/bh;->m:I

    int-to-long v8, v5

    add-long/2addr v6, v8

    .line 149
    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-ltz v5, :cond_1

    const-wide/32 v8, 0x40000

    cmp-long v5, v6, v8

    if-ltz v5, :cond_2

    .line 150
    :cond_1
    iput-wide v2, p2, Lcom/google/obf/ao;->a:J

    .line 151
    const/4 v0, 0x1

    goto :goto_0

    .line 152
    :cond_2
    long-to-int v2, v6

    invoke-interface {p1, v2}, Lcom/google/obf/ak;->b(I)V

    .line 153
    iget-object v2, v0, Lcom/google/obf/bh$a;->b:Lcom/google/obf/bn;

    iget-object v2, v2, Lcom/google/obf/bn;->c:[I

    aget v2, v2, v4

    iput v2, p0, Lcom/google/obf/bh;->l:I

    .line 154
    iget-object v2, v0, Lcom/google/obf/bh$a;->a:Lcom/google/obf/bk;

    iget v2, v2, Lcom/google/obf/bk;->p:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_4

    .line 155
    iget-object v2, p0, Lcom/google/obf/bh;->c:Lcom/google/obf/dw;

    iget-object v2, v2, Lcom/google/obf/dw;->a:[B

    .line 156
    const/4 v3, 0x0

    const/4 v5, 0x0

    aput-byte v5, v2, v3

    .line 157
    const/4 v3, 0x1

    const/4 v5, 0x0

    aput-byte v5, v2, v3

    .line 158
    const/4 v3, 0x2

    const/4 v5, 0x0

    aput-byte v5, v2, v3

    .line 159
    iget-object v2, v0, Lcom/google/obf/bh$a;->a:Lcom/google/obf/bk;

    iget v2, v2, Lcom/google/obf/bk;->p:I

    .line 160
    iget-object v3, v0, Lcom/google/obf/bh$a;->a:Lcom/google/obf/bk;

    iget v3, v3, Lcom/google/obf/bk;->p:I

    rsub-int/lit8 v3, v3, 0x4

    .line 161
    :goto_1
    iget v5, p0, Lcom/google/obf/bh;->m:I

    iget v6, p0, Lcom/google/obf/bh;->l:I

    if-ge v5, v6, :cond_5

    .line 162
    iget v5, p0, Lcom/google/obf/bh;->n:I

    if-nez v5, :cond_3

    .line 163
    iget-object v5, p0, Lcom/google/obf/bh;->c:Lcom/google/obf/dw;

    iget-object v5, v5, Lcom/google/obf/dw;->a:[B

    invoke-interface {p1, v5, v3, v2}, Lcom/google/obf/ak;->b([BII)V

    .line 164
    iget-object v5, p0, Lcom/google/obf/bh;->c:Lcom/google/obf/dw;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/obf/dw;->c(I)V

    .line 165
    iget-object v5, p0, Lcom/google/obf/bh;->c:Lcom/google/obf/dw;

    invoke-virtual {v5}, Lcom/google/obf/dw;->s()I

    move-result v5

    iput v5, p0, Lcom/google/obf/bh;->n:I

    .line 166
    iget-object v5, p0, Lcom/google/obf/bh;->b:Lcom/google/obf/dw;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/obf/dw;->c(I)V

    .line 167
    iget-object v5, p0, Lcom/google/obf/bh;->b:Lcom/google/obf/dw;

    const/4 v6, 0x4

    invoke-interface {v1, v5, v6}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 168
    iget v5, p0, Lcom/google/obf/bh;->m:I

    add-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/obf/bh;->m:I

    .line 169
    iget v5, p0, Lcom/google/obf/bh;->l:I

    add-int/2addr v5, v3

    iput v5, p0, Lcom/google/obf/bh;->l:I

    goto :goto_1

    .line 170
    :cond_3
    iget v5, p0, Lcom/google/obf/bh;->n:I

    const/4 v6, 0x0

    invoke-interface {v1, p1, v5, v6}, Lcom/google/obf/ar;->a(Lcom/google/obf/ak;IZ)I

    move-result v5

    .line 171
    iget v6, p0, Lcom/google/obf/bh;->m:I

    add-int/2addr v6, v5

    iput v6, p0, Lcom/google/obf/bh;->m:I

    .line 172
    iget v6, p0, Lcom/google/obf/bh;->n:I

    sub-int v5, v6, v5

    iput v5, p0, Lcom/google/obf/bh;->n:I

    goto :goto_1

    .line 175
    :cond_4
    :goto_2
    iget v2, p0, Lcom/google/obf/bh;->m:I

    iget v3, p0, Lcom/google/obf/bh;->l:I

    if-ge v2, v3, :cond_5

    .line 176
    iget v2, p0, Lcom/google/obf/bh;->l:I

    iget v3, p0, Lcom/google/obf/bh;->m:I

    sub-int/2addr v2, v3

    const/4 v3, 0x0

    invoke-interface {v1, p1, v2, v3}, Lcom/google/obf/ar;->a(Lcom/google/obf/ak;IZ)I

    move-result v2

    .line 177
    iget v3, p0, Lcom/google/obf/bh;->m:I

    add-int/2addr v3, v2

    iput v3, p0, Lcom/google/obf/bh;->m:I

    .line 178
    iget v3, p0, Lcom/google/obf/bh;->n:I

    sub-int v2, v3, v2

    iput v2, p0, Lcom/google/obf/bh;->n:I

    goto :goto_2

    .line 180
    :cond_5
    iget-object v2, v0, Lcom/google/obf/bh$a;->b:Lcom/google/obf/bn;

    iget-object v2, v2, Lcom/google/obf/bn;->e:[J

    aget-wide v2, v2, v4

    iget-object v5, v0, Lcom/google/obf/bh$a;->b:Lcom/google/obf/bn;

    iget-object v5, v5, Lcom/google/obf/bn;->f:[I

    aget v4, v5, v4

    iget v5, p0, Lcom/google/obf/bh;->l:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/google/obf/ar;->a(JIII[B)V

    .line 181
    iget v1, v0, Lcom/google/obf/bh$a;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/obf/bh$a;->d:I

    .line 182
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/bh;->m:I

    .line 183
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/bh;->n:I

    .line 184
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/obf/bh;->g:I

    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/bh;->j:I

    .line 43
    return-void
.end method

.method private e()I
    .locals 7

    .prologue
    .line 185
    const/4 v1, -0x1

    .line 186
    const-wide v2, 0x7fffffffffffffffL

    .line 187
    const/4 v0, 0x0

    :goto_0
    iget-object v4, p0, Lcom/google/obf/bh;->p:[Lcom/google/obf/bh$a;

    array-length v4, v4

    if-ge v0, v4, :cond_2

    .line 188
    iget-object v4, p0, Lcom/google/obf/bh;->p:[Lcom/google/obf/bh$a;

    aget-object v4, v4, v0

    .line 189
    iget v5, v4, Lcom/google/obf/bh$a;->d:I

    .line 190
    iget-object v6, v4, Lcom/google/obf/bh$a;->b:Lcom/google/obf/bn;

    iget v6, v6, Lcom/google/obf/bn;->a:I

    if-ne v5, v6, :cond_1

    .line 196
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 192
    :cond_1
    iget-object v4, v4, Lcom/google/obf/bh$a;->b:Lcom/google/obf/bn;

    iget-object v4, v4, Lcom/google/obf/bn;->b:[J

    aget-wide v4, v4, v5

    .line 193
    cmp-long v6, v4, v2

    if-gez v6, :cond_0

    move-wide v2, v4

    move v1, v0

    .line 195
    goto :goto_1

    .line 197
    :cond_2
    return v1
.end method


# virtual methods
.method public a(Lcom/google/obf/ak;Lcom/google/obf/ao;)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 18
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/obf/bh;->g:I

    packed-switch v0, :pswitch_data_0

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/google/obf/bh;->c(Lcom/google/obf/ak;Lcom/google/obf/ao;)I

    move-result v0

    :goto_1
    return v0

    .line 19
    :pswitch_0
    invoke-interface {p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 20
    invoke-direct {p0}, Lcom/google/obf/bh;->d()V

    goto :goto_0

    .line 21
    :cond_1
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/obf/bh;->g:I

    goto :goto_0

    .line 23
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/obf/bh;->b(Lcom/google/obf/ak;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24
    const/4 v0, -0x1

    goto :goto_1

    .line 25
    :pswitch_2
    invoke-direct {p0, p1, p2}, Lcom/google/obf/bh;->b(Lcom/google/obf/ak;Lcom/google/obf/ao;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    const/4 v0, 0x1

    goto :goto_1

    .line 18
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lcom/google/obf/al;)V
    .locals 0

    .prologue
    .line 9
    iput-object p1, p0, Lcom/google/obf/bh;->o:Lcom/google/obf/al;

    .line 10
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lcom/google/obf/ak;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-static {p1}, Lcom/google/obf/bj;->b(Lcom/google/obf/ak;)Z

    move-result v0

    return v0
.end method

.method public b(J)J
    .locals 7

    .prologue
    .line 29
    const-wide v2, 0x7fffffffffffffffL

    .line 30
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/obf/bh;->p:[Lcom/google/obf/bh$a;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 31
    iget-object v1, p0, Lcom/google/obf/bh;->p:[Lcom/google/obf/bh$a;

    aget-object v1, v1, v0

    iget-object v4, v1, Lcom/google/obf/bh$a;->b:Lcom/google/obf/bn;

    .line 32
    invoke-virtual {v4, p1, p2}, Lcom/google/obf/bn;->a(J)I

    move-result v1

    .line 33
    const/4 v5, -0x1

    if-ne v1, v5, :cond_0

    .line 34
    invoke-virtual {v4, p1, p2}, Lcom/google/obf/bn;->b(J)I

    move-result v1

    .line 35
    :cond_0
    iget-object v5, p0, Lcom/google/obf/bh;->p:[Lcom/google/obf/bh$a;

    aget-object v5, v5, v0

    iput v1, v5, Lcom/google/obf/bh$a;->d:I

    .line 36
    iget-object v4, v4, Lcom/google/obf/bn;->b:[J

    aget-wide v4, v4, v1

    .line 37
    cmp-long v1, v4, v2

    if-gez v1, :cond_1

    move-wide v2, v4

    .line 39
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 40
    :cond_2
    return-wide v2
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 11
    iget-object v0, p0, Lcom/google/obf/bh;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 12
    iput v1, p0, Lcom/google/obf/bh;->j:I

    .line 13
    iput v1, p0, Lcom/google/obf/bh;->m:I

    .line 14
    iput v1, p0, Lcom/google/obf/bh;->n:I

    .line 15
    iput v1, p0, Lcom/google/obf/bh;->g:I

    .line 16
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 17
    return-void
.end method
