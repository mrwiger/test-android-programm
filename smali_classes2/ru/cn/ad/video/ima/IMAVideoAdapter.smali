.class public Lru/cn/ad/video/ima/IMAVideoAdapter;
.super Lru/cn/ad/video/VideoAdAdapter;
.source "IMAVideoAdapter.java"

# interfaces
.implements Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent$AdErrorListener;
.implements Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventListener;
.implements Lcom/google/ads/interactivemedia/v3/api/AdsLoader$AdsLoadedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;
    }
.end annotation


# instance fields
.field private adContainer:Landroid/view/ViewGroup;

.field private adVideoPlayer:Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;

.field private adsManager:Lcom/google/ads/interactivemedia/v3/api/AdsManager;

.field private needHandleError:Z

.field private final sdkFactory:Lcom/google/ads/interactivemedia/v3/api/ImaSdkFactory;

.field private final vastTag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "system"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lru/cn/ad/video/VideoAdAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->needHandleError:Z

    .line 60
    const-string v0, "vast_ad_tag"

    invoke-virtual {p2, v0}, Lru/cn/api/money_miner/replies/AdSystem;->getParamOrThrow(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->vastTag:Ljava/lang/String;

    .line 62
    invoke-static {}, Lcom/google/ads/interactivemedia/v3/api/ImaSdkFactory;->getInstance()Lcom/google/ads/interactivemedia/v3/api/ImaSdkFactory;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->sdkFactory:Lcom/google/ads/interactivemedia/v3/api/ImaSdkFactory;

    .line 63
    return-void
.end method

.method static synthetic access$200(Lru/cn/ad/video/ima/IMAVideoAdapter;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/video/ima/IMAVideoAdapter;
    .param p1, "x1"    # Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lru/cn/ad/video/ima/IMAVideoAdapter;->trackError(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;)V

    return-void
.end method

.method private handleError(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;)V
    .locals 3
    .param p1, "errorCode"    # Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;

    .prologue
    .line 97
    const-string v0, "IMAVideoAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "error with code "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    invoke-direct {p0, p1}, Lru/cn/ad/video/ima/IMAVideoAdapter;->trackError(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;)V

    .line 100
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adsManager:Lcom/google/ads/interactivemedia/v3/api/AdsManager;

    if-nez v0, :cond_1

    .line 102
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onError()V

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    invoke-virtual {p0}, Lru/cn/ad/video/ima/IMAVideoAdapter;->destroy()V

    goto :goto_0
.end method

.method private trackError(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;)V
    .locals 6
    .param p1, "code"    # Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;

    .prologue
    .line 239
    iget-boolean v2, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->needHandleError:Z

    if-nez v2, :cond_0

    .line 277
    :goto_0
    return-void

    .line 242
    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->needHandleError:Z

    .line 244
    sget-object v2, Lru/cn/ad/video/ima/IMAVideoAdapter$1;->$SwitchMap$com$google$ads$interactivemedia$v3$api$AdError$AdErrorCode:[I

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 249
    :pswitch_0
    sget-object v2, Lru/cn/domain/statistics/inetra/ErrorCode;->ADV_ERROR_LOAD_BANNER:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v3, "ImaSDK"

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;->getErrorNumber()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {p0, v2, v3, v4, v5}, Lru/cn/ad/video/ima/IMAVideoAdapter;->reportError(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/util/Map;)V

    goto :goto_0

    .line 256
    :pswitch_1
    const/4 v1, 0x0

    .line 257
    .local v1, "extra":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adsManager:Lcom/google/ads/interactivemedia/v3/api/AdsManager;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adsManager:Lcom/google/ads/interactivemedia/v3/api/AdsManager;

    invoke-interface {v2}, Lcom/google/ads/interactivemedia/v3/api/AdsManager;->getCurrentAd()Lcom/google/ads/interactivemedia/v3/api/Ad;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 258
    new-instance v1, Ljava/util/LinkedHashMap;

    .end local v1    # "extra":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 260
    .restart local v1    # "extra":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adsManager:Lcom/google/ads/interactivemedia/v3/api/AdsManager;

    invoke-interface {v2}, Lcom/google/ads/interactivemedia/v3/api/AdsManager;->getCurrentAd()Lcom/google/ads/interactivemedia/v3/api/Ad;

    move-result-object v0

    .line 261
    .local v0, "adInfo":Lcom/google/ads/interactivemedia/v3/api/Ad;
    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/Ad;->getAdSystem()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 262
    const-string v2, "ad_system"

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/Ad;->getAdSystem()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    :cond_1
    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/Ad;->getAdId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 266
    const-string v2, "ad_id"

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/Ad;->getAdId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    :cond_2
    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/Ad;->getTitle()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 270
    const-string v2, "banner_title"

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/Ad;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    .end local v0    # "adInfo":Lcom/google/ads/interactivemedia/v3/api/Ad;
    :cond_3
    sget-object v2, Lru/cn/domain/statistics/inetra/ErrorCode;->ADV_ERROR_PLAY:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v3, "ImaSDK"

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;->getErrorNumber()I

    move-result v4

    invoke-virtual {p0, v2, v3, v4, v1}, Lru/cn/ad/video/ima/IMAVideoAdapter;->reportError(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/util/Map;)V

    goto :goto_0

    .line 244
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private trackEvent(Lru/cn/domain/statistics/inetra/AdvEvent;)V
    .locals 4
    .param p1, "adEventId"    # Lru/cn/domain/statistics/inetra/AdvEvent;

    .prologue
    .line 221
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 222
    .local v1, "extra":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adsManager:Lcom/google/ads/interactivemedia/v3/api/AdsManager;

    invoke-interface {v2}, Lcom/google/ads/interactivemedia/v3/api/AdsManager;->getCurrentAd()Lcom/google/ads/interactivemedia/v3/api/Ad;

    move-result-object v0

    .line 223
    .local v0, "adInfo":Lcom/google/ads/interactivemedia/v3/api/Ad;
    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/Ad;->getAdId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 224
    const-string v2, "banner_id"

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/Ad;->getAdId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    :cond_0
    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/Ad;->getTitle()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 228
    const-string v2, "banner_title"

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/Ad;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    :cond_1
    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/Ad;->getAdSystem()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 232
    const-string v2, "ad_system"

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/Ad;->getAdSystem()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    :cond_2
    invoke-virtual {p0, p1, v1}, Lru/cn/ad/video/ima/IMAVideoAdapter;->reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/util/Map;)V

    .line 236
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 3

    .prologue
    .line 191
    iget-object v1, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adsManager:Lcom/google/ads/interactivemedia/v3/api/AdsManager;

    if-nez v1, :cond_1

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    iget-object v1, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adsManager:Lcom/google/ads/interactivemedia/v3/api/AdsManager;

    invoke-interface {v1, p0}, Lcom/google/ads/interactivemedia/v3/api/AdsManager;->removeAdErrorListener(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent$AdErrorListener;)V

    .line 195
    iget-object v1, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adsManager:Lcom/google/ads/interactivemedia/v3/api/AdsManager;

    invoke-interface {v1, p0}, Lcom/google/ads/interactivemedia/v3/api/AdsManager;->removeAdEventListener(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventListener;)V

    .line 196
    iget-object v1, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adsManager:Lcom/google/ads/interactivemedia/v3/api/AdsManager;

    invoke-interface {v1}, Lcom/google/ads/interactivemedia/v3/api/AdsManager;->destroy()V

    .line 197
    const/4 v1, 0x0

    iput-object v1, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adsManager:Lcom/google/ads/interactivemedia/v3/api/AdsManager;

    .line 199
    invoke-virtual {p0}, Lru/cn/ad/video/ima/IMAVideoAdapter;->getRenderingSettings()Lru/cn/ad/video/RenderingSettings;

    move-result-object v0

    .line 200
    .local v0, "settings":Lru/cn/ad/video/RenderingSettings;
    if-eqz v0, :cond_2

    .line 201
    iget-object v1, v0, Lru/cn/ad/video/RenderingSettings;->container:Landroid/view/ViewGroup;

    iget-object v2, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 202
    iget-object v1, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adVideoPlayer:Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;

    invoke-virtual {v1}, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->destroy()V

    .line 205
    :cond_2
    iget-object v1, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    if-eqz v1, :cond_0

    .line 206
    iget-object v1, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    invoke-interface {v1}, Lru/cn/ad/AdAdapter$Listener;->onAdEnded()V

    goto :goto_0
.end method

.method public onAdError(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent;)V
    .locals 2
    .param p1, "adErrorEvent"    # Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent;

    .prologue
    .line 92
    invoke-interface {p1}, Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent;->getError()Lcom/google/ads/interactivemedia/v3/api/AdError;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/v3/api/AdError;->getErrorCode()Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;

    move-result-object v0

    .line 93
    .local v0, "code":Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;
    invoke-direct {p0, v0}, Lru/cn/ad/video/ima/IMAVideoAdapter;->handleError(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;)V

    .line 94
    return-void
.end method

.method public onAdEvent(Lcom/google/ads/interactivemedia/v3/api/AdEvent;)V
    .locals 3
    .param p1, "adEvent"    # Lcom/google/ads/interactivemedia/v3/api/AdEvent;

    .prologue
    .line 124
    const-string v0, "IMAVideoAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "on ad event "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/ads/interactivemedia/v3/api/AdEvent;->getType()Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    sget-object v0, Lru/cn/ad/video/ima/IMAVideoAdapter$1;->$SwitchMap$com$google$ads$interactivemedia$v3$api$AdEvent$AdEventType:[I

    invoke-interface {p1}, Lcom/google/ads/interactivemedia/v3/api/AdEvent;->getType()Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 166
    :goto_0
    return-void

    .line 128
    :pswitch_0
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adsManager:Lcom/google/ads/interactivemedia/v3/api/AdsManager;

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/AdsManager;->start()V

    goto :goto_0

    .line 132
    :pswitch_1
    invoke-virtual {p0}, Lru/cn/ad/video/ima/IMAVideoAdapter;->destroy()V

    goto :goto_0

    .line 136
    :pswitch_2
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_CLICK:Lru/cn/domain/statistics/inetra/AdvEvent;

    invoke-direct {p0, v0}, Lru/cn/ad/video/ima/IMAVideoAdapter;->trackEvent(Lru/cn/domain/statistics/inetra/AdvEvent;)V

    goto :goto_0

    .line 140
    :pswitch_3
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_START:Lru/cn/domain/statistics/inetra/AdvEvent;

    invoke-direct {p0, v0}, Lru/cn/ad/video/ima/IMAVideoAdapter;->trackEvent(Lru/cn/domain/statistics/inetra/AdvEvent;)V

    goto :goto_0

    .line 144
    :pswitch_4
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_FIRST_QUARTILE:Lru/cn/domain/statistics/inetra/AdvEvent;

    invoke-direct {p0, v0}, Lru/cn/ad/video/ima/IMAVideoAdapter;->trackEvent(Lru/cn/domain/statistics/inetra/AdvEvent;)V

    goto :goto_0

    .line 148
    :pswitch_5
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_MIDPOINT:Lru/cn/domain/statistics/inetra/AdvEvent;

    invoke-direct {p0, v0}, Lru/cn/ad/video/ima/IMAVideoAdapter;->trackEvent(Lru/cn/domain/statistics/inetra/AdvEvent;)V

    goto :goto_0

    .line 152
    :pswitch_6
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_THIRD_QUARTILE:Lru/cn/domain/statistics/inetra/AdvEvent;

    invoke-direct {p0, v0}, Lru/cn/ad/video/ima/IMAVideoAdapter;->trackEvent(Lru/cn/domain/statistics/inetra/AdvEvent;)V

    goto :goto_0

    .line 156
    :pswitch_7
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_COMPLETE:Lru/cn/domain/statistics/inetra/AdvEvent;

    invoke-direct {p0, v0}, Lru/cn/ad/video/ima/IMAVideoAdapter;->trackEvent(Lru/cn/domain/statistics/inetra/AdvEvent;)V

    goto :goto_0

    .line 160
    :pswitch_8
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_SKIP:Lru/cn/domain/statistics/inetra/AdvEvent;

    invoke-direct {p0, v0}, Lru/cn/ad/video/ima/IMAVideoAdapter;->trackEvent(Lru/cn/domain/statistics/inetra/AdvEvent;)V

    goto :goto_0

    .line 126
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public onAdsManagerLoaded(Lcom/google/ads/interactivemedia/v3/api/AdsManagerLoadedEvent;)V
    .locals 2
    .param p1, "adsManagerLoadedEvent"    # Lcom/google/ads/interactivemedia/v3/api/AdsManagerLoadedEvent;

    .prologue
    .line 112
    const-string v0, "IMAVideoAdapter"

    const-string v1, "ads Loaded"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    invoke-interface {p1}, Lcom/google/ads/interactivemedia/v3/api/AdsManagerLoadedEvent;->getAdsManager()Lcom/google/ads/interactivemedia/v3/api/AdsManager;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adsManager:Lcom/google/ads/interactivemedia/v3/api/AdsManager;

    .line 115
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adsManager:Lcom/google/ads/interactivemedia/v3/api/AdsManager;

    invoke-interface {v0, p0}, Lcom/google/ads/interactivemedia/v3/api/AdsManager;->addAdErrorListener(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent$AdErrorListener;)V

    .line 116
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adsManager:Lcom/google/ads/interactivemedia/v3/api/AdsManager;

    invoke-interface {v0, p0}, Lcom/google/ads/interactivemedia/v3/api/AdsManager;->addAdEventListener(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventListener;)V

    .line 118
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdLoaded()V

    .line 120
    :cond_0
    return-void
.end method

.method protected onLoad()V
    .locals 8

    .prologue
    .line 67
    const/4 v4, 0x1

    iput-boolean v4, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->needHandleError:Z

    .line 69
    iget-object v4, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->sdkFactory:Lcom/google/ads/interactivemedia/v3/api/ImaSdkFactory;

    invoke-virtual {v4}, Lcom/google/ads/interactivemedia/v3/api/ImaSdkFactory;->createImaSdkSettings()Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;

    move-result-object v3

    .line 70
    .local v3, "sdkSettings":Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;->setAutoPlayAdBreaks(Z)V

    .line 72
    iget-object v4, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->sdkFactory:Lcom/google/ads/interactivemedia/v3/api/ImaSdkFactory;

    iget-object v5, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->context:Landroid/content/Context;

    invoke-virtual {v4, v5, v3}, Lcom/google/ads/interactivemedia/v3/api/ImaSdkFactory;->createAdsLoader(Landroid/content/Context;Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;)Lcom/google/ads/interactivemedia/v3/api/AdsLoader;

    move-result-object v0

    .line 73
    .local v0, "adsLoader":Lcom/google/ads/interactivemedia/v3/api/AdsLoader;
    invoke-interface {v0, p0}, Lcom/google/ads/interactivemedia/v3/api/AdsLoader;->addAdErrorListener(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent$AdErrorListener;)V

    .line 74
    invoke-interface {v0, p0}, Lcom/google/ads/interactivemedia/v3/api/AdsLoader;->addAdsLoadedListener(Lcom/google/ads/interactivemedia/v3/api/AdsLoader$AdsLoadedListener;)V

    .line 76
    iget-object v4, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->sdkFactory:Lcom/google/ads/interactivemedia/v3/api/ImaSdkFactory;

    invoke-virtual {v4}, Lcom/google/ads/interactivemedia/v3/api/ImaSdkFactory;->createAdDisplayContainer()Lcom/google/ads/interactivemedia/v3/api/AdDisplayContainer;

    move-result-object v1

    .line 77
    .local v1, "displayContainer":Lcom/google/ads/interactivemedia/v3/api/AdDisplayContainer;
    new-instance v4, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;

    invoke-direct {v4, p0}, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;-><init>(Lru/cn/ad/video/ima/IMAVideoAdapter;)V

    iput-object v4, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adVideoPlayer:Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;

    .line 78
    iget-object v4, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adVideoPlayer:Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;

    invoke-interface {v1, v4}, Lcom/google/ads/interactivemedia/v3/api/AdDisplayContainer;->setPlayer(Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;)V

    .line 79
    new-instance v4, Landroid/widget/FrameLayout;

    iget-object v5, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->context:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adContainer:Landroid/view/ViewGroup;

    .line 80
    iget-object v4, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adContainer:Landroid/view/ViewGroup;

    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v6, 0x500

    const/16 v7, 0x2d0

    invoke-direct {v5, v6, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 81
    iget-object v4, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adContainer:Landroid/view/ViewGroup;

    invoke-interface {v1, v4}, Lcom/google/ads/interactivemedia/v3/api/AdDisplayContainer;->setAdContainer(Landroid/view/ViewGroup;)V

    .line 83
    iget-object v4, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->sdkFactory:Lcom/google/ads/interactivemedia/v3/api/ImaSdkFactory;

    invoke-virtual {v4}, Lcom/google/ads/interactivemedia/v3/api/ImaSdkFactory;->createAdsRequest()Lcom/google/ads/interactivemedia/v3/api/AdsRequest;

    move-result-object v2

    .line 84
    .local v2, "request":Lcom/google/ads/interactivemedia/v3/api/AdsRequest;
    iget-object v4, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->vastTag:Ljava/lang/String;

    invoke-interface {v2, v4}, Lcom/google/ads/interactivemedia/v3/api/AdsRequest;->setAdTagUrl(Ljava/lang/String;)V

    .line 85
    invoke-interface {v2, v1}, Lcom/google/ads/interactivemedia/v3/api/AdsRequest;->setAdDisplayContainer(Lcom/google/ads/interactivemedia/v3/api/AdDisplayContainer;)V

    .line 87
    invoke-interface {v0, v2}, Lcom/google/ads/interactivemedia/v3/api/AdsLoader;->requestAds(Lcom/google/ads/interactivemedia/v3/api/AdsRequest;)V

    .line 88
    return-void
.end method

.method public show()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 170
    iget-object v2, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adsManager:Lcom/google/ads/interactivemedia/v3/api/AdsManager;

    if-nez v2, :cond_1

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    invoke-virtual {p0}, Lru/cn/ad/video/ima/IMAVideoAdapter;->getRenderingSettings()Lru/cn/ad/video/RenderingSettings;

    move-result-object v0

    .line 175
    .local v0, "renderingSettings":Lru/cn/ad/video/RenderingSettings;
    iget-object v2, v0, Lru/cn/ad/video/RenderingSettings;->container:Landroid/view/ViewGroup;

    iget-object v3, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 176
    iget-object v2, v0, Lru/cn/ad/video/RenderingSettings;->container:Landroid/view/ViewGroup;

    iget-object v3, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3, v4, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 178
    iget-object v2, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adVideoPlayer:Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;

    iget-object v3, v0, Lru/cn/ad/video/RenderingSettings;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v2, v3}, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->attachPlayer(Lru/cn/player/SimplePlayer;)V

    .line 180
    new-instance v1, Lru/cn/ad/video/ima/IMARenderingSettings;

    invoke-direct {v1}, Lru/cn/ad/video/ima/IMARenderingSettings;-><init>()V

    .line 181
    .local v1, "settings":Lcom/google/ads/interactivemedia/v3/api/AdsRenderingSettings;
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "video/mp4"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "application/x-mpegURL"

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/ads/interactivemedia/v3/api/AdsRenderingSettings;->setMimeTypes(Ljava/util/List;)V

    .line 182
    const/16 v2, 0x7d0

    invoke-interface {v1, v2}, Lcom/google/ads/interactivemedia/v3/api/AdsRenderingSettings;->setBitrateKbps(I)V

    .line 183
    iget-object v2, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adsManager:Lcom/google/ads/interactivemedia/v3/api/AdsManager;

    invoke-interface {v2, v1}, Lcom/google/ads/interactivemedia/v3/api/AdsManager;->init(Lcom/google/ads/interactivemedia/v3/api/AdsRenderingSettings;)V

    .line 185
    iget-object v2, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    if-eqz v2, :cond_0

    .line 186
    iget-object v2, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    invoke-interface {v2}, Lru/cn/ad/AdAdapter$Listener;->onAdStarted()V

    goto :goto_0
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adVideoPlayer:Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter;->adVideoPlayer:Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;

    invoke-virtual {v0}, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->stopAd()V

    .line 213
    :cond_0
    return-void
.end method
