.class public Lcom/annimon/stream/iterator/LazyIterator;
.super Ljava/lang/Object;
.source "LazyIterator.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final iterable:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<+TT;>;"
        }
    .end annotation
.end field

.field private iterator:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Iterable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 14
    .local p0, "this":Lcom/annimon/stream/iterator/LazyIterator;, "Lcom/annimon/stream/iterator/LazyIterator<TT;>;"
    .local p1, "iterable":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/annimon/stream/iterator/LazyIterator;->iterable:Ljava/lang/Iterable;

    .line 16
    return-void
.end method

.method private ensureIterator()V
    .locals 1

    .prologue
    .line 19
    .local p0, "this":Lcom/annimon/stream/iterator/LazyIterator;, "Lcom/annimon/stream/iterator/LazyIterator<TT;>;"
    iget-object v0, p0, Lcom/annimon/stream/iterator/LazyIterator;->iterator:Ljava/util/Iterator;

    if-eqz v0, :cond_0

    .line 24
    :goto_0
    return-void

    .line 23
    :cond_0
    iget-object v0, p0, Lcom/annimon/stream/iterator/LazyIterator;->iterable:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/annimon/stream/iterator/LazyIterator;->iterator:Ljava/util/Iterator;

    goto :goto_0
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 28
    .local p0, "this":Lcom/annimon/stream/iterator/LazyIterator;, "Lcom/annimon/stream/iterator/LazyIterator<TT;>;"
    invoke-direct {p0}, Lcom/annimon/stream/iterator/LazyIterator;->ensureIterator()V

    .line 29
    iget-object v0, p0, Lcom/annimon/stream/iterator/LazyIterator;->iterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 34
    .local p0, "this":Lcom/annimon/stream/iterator/LazyIterator;, "Lcom/annimon/stream/iterator/LazyIterator<TT;>;"
    invoke-direct {p0}, Lcom/annimon/stream/iterator/LazyIterator;->ensureIterator()V

    .line 35
    iget-object v0, p0, Lcom/annimon/stream/iterator/LazyIterator;->iterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 40
    .local p0, "this":Lcom/annimon/stream/iterator/LazyIterator;, "Lcom/annimon/stream/iterator/LazyIterator<TT;>;"
    invoke-direct {p0}, Lcom/annimon/stream/iterator/LazyIterator;->ensureIterator()V

    .line 41
    iget-object v0, p0, Lcom/annimon/stream/iterator/LazyIterator;->iterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 42
    return-void
.end method
