.class public Lru/inetra/bytefog/service/BytefogService;
.super Landroid/app/Service;
.source "BytefogService.java"

# interfaces
.implements Lru/inetra/bytefog/service/BytefogLibraryListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/inetra/bytefog/service/BytefogService$NetworkChangeReceiver;
    }
.end annotation


# static fields
.field public static final EXTRA_UID:Ljava/lang/String; = "EXTRA_UID"


# instance fields
.field private bytefogLibrary:Lru/inetra/bytefog/service/BytefogLibrary;

.field private listener:Lru/inetra/bytefog/service/IServiceListener;

.field private mBinder:Lru/inetra/bytefog/service/IService$Stub;

.field private networkChangeReceiver:Lru/inetra/bytefog/service/BytefogService$NetworkChangeReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 22
    new-instance v0, Lru/inetra/bytefog/service/BytefogService$NetworkChangeReceiver;

    invoke-direct {v0, p0, p0}, Lru/inetra/bytefog/service/BytefogService$NetworkChangeReceiver;-><init>(Lru/inetra/bytefog/service/BytefogService;Landroid/content/Context;)V

    iput-object v0, p0, Lru/inetra/bytefog/service/BytefogService;->networkChangeReceiver:Lru/inetra/bytefog/service/BytefogService$NetworkChangeReceiver;

    .line 54
    new-instance v0, Lru/inetra/bytefog/service/BytefogService$1;

    invoke-direct {v0, p0}, Lru/inetra/bytefog/service/BytefogService$1;-><init>(Lru/inetra/bytefog/service/BytefogService;)V

    iput-object v0, p0, Lru/inetra/bytefog/service/BytefogService;->mBinder:Lru/inetra/bytefog/service/IService$Stub;

    return-void
.end method

.method static synthetic access$002(Lru/inetra/bytefog/service/BytefogService;Lru/inetra/bytefog/service/IServiceListener;)Lru/inetra/bytefog/service/IServiceListener;
    .locals 0
    .param p0, "x0"    # Lru/inetra/bytefog/service/BytefogService;
    .param p1, "x1"    # Lru/inetra/bytefog/service/IServiceListener;

    .prologue
    .line 17
    iput-object p1, p0, Lru/inetra/bytefog/service/BytefogService;->listener:Lru/inetra/bytefog/service/IServiceListener;

    return-object p1
.end method

.method static synthetic access$100(Lru/inetra/bytefog/service/BytefogService;)Lru/inetra/bytefog/service/BytefogLibrary;
    .locals 1
    .param p0, "x0"    # Lru/inetra/bytefog/service/BytefogService;

    .prologue
    .line 17
    iget-object v0, p0, Lru/inetra/bytefog/service/BytefogService;->bytefogLibrary:Lru/inetra/bytefog/service/BytefogLibrary;

    return-object v0
.end method

.method private startBytefogInstance(Ljava/lang/String;)V
    .locals 6
    .param p1, "uid"    # Ljava/lang/String;

    .prologue
    .line 40
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lru/inetra/bytefog/service/BytefogService;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/node.conf"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 41
    .local v0, "pathToConf":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lru/inetra/bytefog/service/BytefogService;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 42
    .local v1, "pathToStat":Ljava/lang/String;
    sput-object p0, Lru/inetra/bytefog/service/BytefogLibrary;->context:Landroid/content/Context;

    .line 43
    new-instance v2, Lru/inetra/bytefog/service/BytefogLibrary;

    invoke-direct {v2, p0}, Lru/inetra/bytefog/service/BytefogLibrary;-><init>(Lru/inetra/bytefog/service/BytefogLibraryListener;)V

    iput-object v2, p0, Lru/inetra/bytefog/service/BytefogService;->bytefogLibrary:Lru/inetra/bytefog/service/BytefogLibrary;

    .line 44
    iget-object v2, p0, Lru/inetra/bytefog/service/BytefogService;->bytefogLibrary:Lru/inetra/bytefog/service/BytefogLibrary;

    const/16 v3, 0x9

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "bytefog"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "-d"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "4"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "-c"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    aput-object v0, v3, v4

    const/4 v4, 0x5

    const-string v5, "-p"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    aput-object v1, v3, v4

    const/4 v4, 0x7

    const-string v5, "-i"

    aput-object v5, v3, v4

    const/16 v4, 0x8

    aput-object p1, v3, v4

    invoke-virtual {v2, v3}, Lru/inetra/bytefog/service/BytefogLibrary;->start([Ljava/lang/String;)Z

    .line 45
    return-void
.end method


# virtual methods
.method public hlsErrorCallback(I)V
    .locals 3
    .param p1, "statusCode"    # I

    .prologue
    .line 84
    :try_start_0
    iget-object v1, p0, Lru/inetra/bytefog/service/BytefogService;->listener:Lru/inetra/bytefog/service/IServiceListener;

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, p0, Lru/inetra/bytefog/service/BytefogService;->listener:Lru/inetra/bytefog/service/IServiceListener;

    invoke-interface {v1, p1}, Lru/inetra/bytefog/service/IServiceListener;->hlsErrorCallback(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 86
    :catch_0
    move-exception v0

    .line 87
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "bytefog"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 32
    const-string v1, "EXTRA_UID"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 33
    .local v0, "uid":Ljava/lang/String;
    invoke-direct {p0, v0}, Lru/inetra/bytefog/service/BytefogService;->startBytefogInstance(Ljava/lang/String;)V

    .line 34
    iget-object v1, p0, Lru/inetra/bytefog/service/BytefogService;->networkChangeReceiver:Lru/inetra/bytefog/service/BytefogService$NetworkChangeReceiver;

    invoke-virtual {v1}, Lru/inetra/bytefog/service/BytefogService$NetworkChangeReceiver;->register()V

    .line 35
    const-string v1, "bytefog"

    const-string v2, "Bytefog service is started"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    iget-object v1, p0, Lru/inetra/bytefog/service/BytefogService;->mBinder:Lru/inetra/bytefog/service/IService$Stub;

    return-object v1
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 26
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lru/inetra/bytefog/service/BytefogService;->listener:Lru/inetra/bytefog/service/IServiceListener;

    .line 50
    iget-object v0, p0, Lru/inetra/bytefog/service/BytefogService;->networkChangeReceiver:Lru/inetra/bytefog/service/BytefogService$NetworkChangeReceiver;

    invoke-virtual {v0}, Lru/inetra/bytefog/service/BytefogService$NetworkChangeReceiver;->unregister()V

    .line 51
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public onUpdateResult(Lru/inetra/bytefog/service/UpdateInfo;)V
    .locals 3
    .param p1, "info"    # Lru/inetra/bytefog/service/UpdateInfo;

    .prologue
    .line 94
    :try_start_0
    iget-object v1, p0, Lru/inetra/bytefog/service/BytefogService;->listener:Lru/inetra/bytefog/service/IServiceListener;

    if-eqz v1, :cond_0

    .line 95
    iget-object v1, p0, Lru/inetra/bytefog/service/BytefogService;->listener:Lru/inetra/bytefog/service/IServiceListener;

    invoke-interface {v1, p1}, Lru/inetra/bytefog/service/IServiceListener;->onUpdateResult(Lru/inetra/bytefog/service/UpdateInfo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 96
    :catch_0
    move-exception v0

    .line 97
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "bytefog"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
