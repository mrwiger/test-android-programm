.class public final Lru/cn/mvvm/ToothpickFactory;
.super Ljava/lang/Object;
.source "ToothpickFactory.java"

# interfaces
.implements Landroid/arch/lifecycle/ViewModelProvider$Factory;


# instance fields
.field private final scope:Ltoothpick/Scope;


# direct methods
.method public constructor <init>(Ltoothpick/Scope;)V
    .locals 0
    .param p1, "scope"    # Ltoothpick/Scope;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lru/cn/mvvm/ToothpickFactory;->scope:Ltoothpick/Scope;

    .line 15
    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/arch/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 20
    .local p1, "modelClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    iget-object v0, p0, Lru/cn/mvvm/ToothpickFactory;->scope:Ltoothpick/Scope;

    invoke-interface {v0, p1}, Ltoothpick/Scope;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/arch/lifecycle/ViewModel;

    return-object v0
.end method
