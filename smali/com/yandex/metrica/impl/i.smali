.class public Lcom/yandex/metrica/impl/i;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/i$a;
    }
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:I

.field d:I

.field e:I

.field private f:Lcom/yandex/metrica/impl/i$a;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:I

.field private l:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Lcom/yandex/metrica/impl/i$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/i$a;-><init>(B)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/i;->f:Lcom/yandex/metrica/impl/i$a;

    .line 80
    const/4 v0, 0x2

    iput v0, p0, Lcom/yandex/metrica/impl/i;->k:I

    .line 83
    return-void
.end method

.method public constructor <init>(Lcom/yandex/metrica/impl/i;)V
    .locals 2

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Lcom/yandex/metrica/impl/i$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/i$a;-><init>(B)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/i;->f:Lcom/yandex/metrica/impl/i$a;

    .line 80
    const/4 v0, 0x2

    iput v0, p0, Lcom/yandex/metrica/impl/i;->k:I

    .line 86
    if-eqz p1, :cond_0

    .line 87
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/i;->a:Ljava/lang/String;

    .line 88
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/i;->b:Ljava/lang/String;

    .line 89
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->c()I

    move-result v0

    iput v0, p0, Lcom/yandex/metrica/impl/i;->c:I

    .line 90
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->d()I

    move-result v0

    iput v0, p0, Lcom/yandex/metrica/impl/i;->d:I

    .line 91
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/i;->g:Ljava/lang/String;

    .line 92
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/i;->i:Ljava/lang/String;

    .line 93
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/i;->h:Ljava/lang/String;

    .line 94
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->f:Lcom/yandex/metrica/impl/i$a;

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->e()Landroid/location/Location;

    move-result-object v1

    iput-object v1, v0, Lcom/yandex/metrica/impl/i$a;->a:Landroid/location/Location;

    .line 95
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->f:Lcom/yandex/metrica/impl/i$a;

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/yandex/metrica/impl/i$a;->b:Ljava/lang/String;

    .line 96
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->f:Lcom/yandex/metrica/impl/i$a;

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->h()Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/yandex/metrica/impl/i$a;->c:Ljava/lang/Integer;

    .line 97
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->j()Landroid/util/Pair;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/i;->j:Landroid/util/Pair;

    .line 98
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->o()I

    move-result v0

    iput v0, p0, Lcom/yandex/metrica/impl/i;->e:I

    .line 99
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->p()I

    move-result v0

    iput v0, p0, Lcom/yandex/metrica/impl/i;->k:I

    .line 100
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/i;->l:Ljava/lang/String;

    .line 102
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Lcom/yandex/metrica/impl/i$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/i$a;-><init>(B)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/i;->f:Lcom/yandex/metrica/impl/i$a;

    .line 80
    const/4 v0, 0x2

    iput v0, p0, Lcom/yandex/metrica/impl/i;->k:I

    .line 111
    iput-object p2, p0, Lcom/yandex/metrica/impl/i;->a:Ljava/lang/String;

    .line 112
    iput p3, p0, Lcom/yandex/metrica/impl/i;->c:I

    .line 113
    iput-object p1, p0, Lcom/yandex/metrica/impl/i;->b:Ljava/lang/String;

    .line 114
    return-void
.end method

.method public static a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/q$a;)Lcom/yandex/metrica/impl/i;
    .locals 2

    .prologue
    .line 350
    new-instance v0, Lcom/yandex/metrica/impl/i;

    invoke-direct {v0, p0}, Lcom/yandex/metrica/impl/i;-><init>(Lcom/yandex/metrica/impl/i;)V

    .line 351
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/q$a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/i;->b(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    .line 352
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/i;->a(I)Lcom/yandex/metrica/impl/i;

    .line 353
    return-object v0
.end method

.method public static a(Lcom/yandex/metrica/impl/i;Ljava/util/List;)Lcom/yandex/metrica/impl/i;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/yandex/metrica/impl/i;",
            "Ljava/util/List",
            "<",
            "Lcom/yandex/metrica/impl/ob/ey;",
            ">;)",
            "Lcom/yandex/metrica/impl/i;"
        }
    .end annotation

    .prologue
    .line 393
    new-instance v2, Lcom/yandex/metrica/impl/i;

    invoke-direct {v2, p0}, Lcom/yandex/metrica/impl/i;-><init>(Lcom/yandex/metrica/impl/i;)V

    .line 394
    const-string v1, ""

    .line 396
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 397
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/ey;

    .line 398
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    const-string v6, "name"

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/ey;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "granted"

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/ey;->a()Z

    move-result v0

    invoke-virtual {v5, v6, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    .line 404
    :goto_1
    sget-object v1, Lcom/yandex/metrica/impl/q$a;->F:Lcom/yandex/metrica/impl/q$a;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/yandex/metrica/impl/i;->a(I)Lcom/yandex/metrica/impl/i;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/i;->c(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    return-object v0

    .line 400
    :cond_0
    :try_start_1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "permissions"

    invoke-virtual {v0, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/i;)Lcom/yandex/metrica/impl/i;
    .locals 3

    .prologue
    .line 369
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->n()Landroid/content/Context;

    move-result-object v0

    .line 370
    new-instance v1, Lcom/yandex/metrica/impl/u;

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/yandex/metrica/impl/u;-><init>(Ljava/lang/String;)V

    .line 371
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/u;->a()Lcom/yandex/metrica/impl/u;

    move-result-object v1

    .line 374
    :try_start_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->H()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 375
    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/u;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/u;

    .line 377
    :cond_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->i()Lcom/yandex/metrica/impl/bb;

    move-result-object v2

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->L()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 378
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->i()Lcom/yandex/metrica/impl/bb;

    move-result-object v2

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->M()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/yandex/metrica/impl/u;->a(Landroid/content/Context;Z)Lcom/yandex/metrica/impl/u;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 384
    :cond_1
    :goto_0
    new-instance v0, Lcom/yandex/metrica/impl/i;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/i;-><init>(Lcom/yandex/metrica/impl/i;)V

    .line 386
    sget-object v2, Lcom/yandex/metrica/impl/q$a;->r:Lcom/yandex/metrica/impl/q$a;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/i;->a(I)Lcom/yandex/metrica/impl/i;

    move-result-object v2

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/u;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/yandex/metrica/impl/i;->c(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    .line 387
    return-object v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static b(Landroid/os/Bundle;)Lcom/yandex/metrica/impl/i;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 321
    const-string v0, "CounterReport.Object"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "CounterReport.Object"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    move-object v1, v0

    .line 323
    :goto_0
    const-string v0, "CounterReport.TRUNCATED"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 325
    if-eqz v0, :cond_0

    .line 326
    instance-of v3, v0, Ljava/lang/Boolean;

    if-eqz v3, :cond_3

    .line 327
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    move v2, v0

    .line 332
    :cond_0
    :goto_2
    new-instance v0, Lcom/yandex/metrica/impl/i;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/i;-><init>()V

    const-string v3, "CounterReport.Type"

    sget-object v4, Lcom/yandex/metrica/impl/q$a;->a:Lcom/yandex/metrica/impl/q$a;

    .line 333
    invoke-virtual {v4}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/yandex/metrica/impl/i;->a(I)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    const-string v3, "CounterReport.CustomType"

    .line 334
    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/yandex/metrica/impl/i;->b(I)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    const-string v3, "CounterReport.GeoLocation"

    .line 335
    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v3

    invoke-static {v3}, Lcom/yandex/metrica/impl/ob/eb;->a([B)Landroid/location/Location;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/yandex/metrica/impl/i;->a(Landroid/location/Location;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    const-string v3, "CounterReport.Value"

    .line 336
    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-static {v3, v4}, Lcom/yandex/metrica/impl/bj;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/yandex/metrica/impl/i;->c(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    const-string v3, "CounterReport.UserInfo"

    .line 337
    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/yandex/metrica/impl/i;->a(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    const-string v3, "CounterReport.Environment"

    .line 338
    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/yandex/metrica/impl/i;->e(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    const-string v3, "CounterReport.Wifi"

    .line 339
    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/yandex/metrica/impl/i;->d(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v3

    const-string v0, "CounterReport.CellId"

    .line 340
    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v3, v0}, Lcom/yandex/metrica/impl/i;->a(Ljava/lang/Integer;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    const-string v3, "CounterReport.Event"

    .line 341
    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/yandex/metrica/impl/i;->b(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    const-string v3, "CounterReport.PackageName"

    .line 342
    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/yandex/metrica/impl/i;->f(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v3

    .line 1310
    const-string v0, "CounterReport.AppEnvironmentDiffKey"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "CounterReport.AppEnvironmentDiffValue"

    .line 1311
    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1312
    const-string v0, "CounterReport.AppEnvironmentDiffKey"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1313
    const-string v0, "CounterReport.AppEnvironmentDiffValue"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1314
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2212
    :goto_3
    iput-object v0, v3, Lcom/yandex/metrica/impl/i;->j:Landroid/util/Pair;

    .line 344
    invoke-virtual {v3, v2}, Lcom/yandex/metrica/impl/i;->c(I)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    const-string v2, "CounterReport.ConnectionType"

    .line 345
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/i;->d(I)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    const-string v2, "CounterReport.CellularConnectionType"

    .line 346
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/i;->g(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    .line 332
    return-object v0

    .line 321
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    move-object v1, v0

    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 327
    goto/16 :goto_1

    .line 328
    :cond_3
    instance-of v3, v0, Ljava/lang/Integer;

    if-eqz v3, :cond_0

    .line 329
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto/16 :goto_2

    .line 1316
    :cond_4
    const/4 v0, 0x0

    goto :goto_3
.end method


# virtual methods
.method a(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 270
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 272
    const-string v0, "CounterReport.Event"

    iget-object v2, p0, Lcom/yandex/metrica/impl/i;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    const-string v0, "CounterReport.Value"

    iget-object v2, p0, Lcom/yandex/metrica/impl/i;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    const-string v0, "CounterReport.Type"

    iget v2, p0, Lcom/yandex/metrica/impl/i;->c:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 275
    const-string v0, "CounterReport.CustomType"

    iget v2, p0, Lcom/yandex/metrica/impl/i;->d:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 276
    const-string v0, "CounterReport.Wifi"

    iget-object v2, p0, Lcom/yandex/metrica/impl/i;->f:Lcom/yandex/metrica/impl/i$a;

    iget-object v2, v2, Lcom/yandex/metrica/impl/i$a;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    const-string v0, "CounterReport.GeoLocation"

    iget-object v2, p0, Lcom/yandex/metrica/impl/i;->f:Lcom/yandex/metrica/impl/i$a;

    iget-object v2, v2, Lcom/yandex/metrica/impl/i$a;->a:Landroid/location/Location;

    invoke-static {v2}, Lcom/yandex/metrica/impl/ob/eb;->a(Landroid/location/Location;)[B

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 278
    const-string v0, "CounterReport.TRUNCATED"

    iget v2, p0, Lcom/yandex/metrica/impl/i;->e:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 279
    const-string v0, "CounterReport.ConnectionType"

    iget v2, p0, Lcom/yandex/metrica/impl/i;->k:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 280
    const-string v0, "CounterReport.CellularConnectionType"

    iget-object v2, p0, Lcom/yandex/metrica/impl/i;->l:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->f:Lcom/yandex/metrica/impl/i$a;

    iget-object v0, v0, Lcom/yandex/metrica/impl/i$a;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 283
    const-string v0, "CounterReport.CellId"

    iget-object v2, p0, Lcom/yandex/metrica/impl/i;->f:Lcom/yandex/metrica/impl/i$a;

    iget-object v2, v2, Lcom/yandex/metrica/impl/i$a;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->h:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 286
    const-string v0, "CounterReport.Environment"

    iget-object v2, p0, Lcom/yandex/metrica/impl/i;->h:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->g:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 289
    const-string v0, "CounterReport.UserInfo"

    iget-object v2, p0, Lcom/yandex/metrica/impl/i;->g:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    :cond_2
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->i:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 292
    const-string v0, "CounterReport.PackageName"

    iget-object v2, p0, Lcom/yandex/metrica/impl/i;->i:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    :cond_3
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->j:Landroid/util/Pair;

    if-eqz v0, :cond_4

    .line 295
    iget-object v2, p0, Lcom/yandex/metrica/impl/i;->j:Landroid/util/Pair;

    .line 1304
    const-string v3, "CounterReport.AppEnvironmentDiffKey"

    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1305
    const-string v3, "CounterReport.AppEnvironmentDiffValue"

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    :cond_4
    if-eqz p1, :cond_5

    .line 299
    :goto_0
    const-string v0, "CounterReport.Object"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 300
    return-object p1

    .line 298
    :cond_5
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    goto :goto_0
.end method

.method public a(I)Lcom/yandex/metrica/impl/i;
    .locals 0

    .prologue
    .line 139
    iput p1, p0, Lcom/yandex/metrica/impl/i;->c:I

    .line 140
    return-object p0
.end method

.method a(Landroid/location/Location;)Lcom/yandex/metrica/impl/i;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->f:Lcom/yandex/metrica/impl/i$a;

    iput-object p1, v0, Lcom/yandex/metrica/impl/i$a;->a:Landroid/location/Location;

    .line 158
    return-object p0
.end method

.method a(Ljava/lang/Integer;)Lcom/yandex/metrica/impl/i;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->f:Lcom/yandex/metrica/impl/i$a;

    iput-object p1, v0, Lcom/yandex/metrica/impl/i$a;->c:Ljava/lang/Integer;

    .line 188
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/yandex/metrica/impl/i;->g:Ljava/lang/String;

    .line 222
    return-object p0
.end method

.method a(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/i;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->j:Landroid/util/Pair;

    if-nez v0, :cond_0

    .line 206
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/i;->j:Landroid/util/Pair;

    .line 208
    :cond_0
    return-object p0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b(I)Lcom/yandex/metrica/impl/i;
    .locals 0

    .prologue
    .line 148
    iput p1, p0, Lcom/yandex/metrica/impl/i;->d:I

    .line 149
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/yandex/metrica/impl/i;->a:Ljava/lang/String;

    .line 122
    return-object p0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Lcom/yandex/metrica/impl/i;->c:I

    return v0
.end method

.method protected c(I)Lcom/yandex/metrica/impl/i;
    .locals 0

    .prologue
    .line 235
    iput p1, p0, Lcom/yandex/metrica/impl/i;->e:I

    .line 236
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/yandex/metrica/impl/i;->b:Ljava/lang/String;

    .line 131
    return-object p0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/yandex/metrica/impl/i;->d:I

    return v0
.end method

.method protected d(I)Lcom/yandex/metrica/impl/i;
    .locals 0

    .prologue
    .line 240
    iput p1, p0, Lcom/yandex/metrica/impl/i;->k:I

    .line 241
    return-object p0
.end method

.method d(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->f:Lcom/yandex/metrica/impl/i$a;

    iput-object p1, v0, Lcom/yandex/metrica/impl/i$a;->b:Ljava/lang/String;

    .line 179
    return-object p0
.end method

.method public e()Landroid/location/Location;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->f:Lcom/yandex/metrica/impl/i$a;

    iget-object v0, v0, Lcom/yandex/metrica/impl/i$a;->a:Landroid/location/Location;

    return-object v0
.end method

.method e(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lcom/yandex/metrica/impl/i;->h:Ljava/lang/String;

    .line 201
    return-object p0
.end method

.method public f(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/yandex/metrica/impl/i;->i:Ljava/lang/String;

    .line 231
    return-object p0
.end method

.method f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->f:Lcom/yandex/metrica/impl/i$a;

    iget-object v0, v0, Lcom/yandex/metrica/impl/i$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method protected g(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Lcom/yandex/metrica/impl/i;->l:Ljava/lang/String;

    .line 246
    return-object p0
.end method

.method g()Lorg/json/JSONArray;
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->f:Lcom/yandex/metrica/impl/i$a;

    iget-object v0, v0, Lcom/yandex/metrica/impl/i$a;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 168
    :try_start_0
    new-instance v0, Lorg/json/JSONArray;

    iget-object v1, p0, Lcom/yandex/metrica/impl/i;->f:Lcom/yandex/metrica/impl/i$a;

    iget-object v1, v1, Lcom/yandex/metrica/impl/i$a;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 174
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    goto :goto_0
.end method

.method h()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->f:Lcom/yandex/metrica/impl/i$a;

    iget-object v0, v0, Lcom/yandex/metrica/impl/i$a;->c:Ljava/lang/Integer;

    return-object v0
.end method

.method i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->h:Ljava/lang/String;

    return-object v0
.end method

.method public j()Landroid/util/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 196
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->j:Landroid/util/Pair;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->g:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->i:Ljava/lang/String;

    return-object v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Z
    .locals 2

    .prologue
    .line 254
    sget-object v0, Lcom/yandex/metrica/impl/q$a;->a:Lcom/yandex/metrica/impl/q$a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v0

    iget v1, p0, Lcom/yandex/metrica/impl/i;->c:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 258
    iget v0, p0, Lcom/yandex/metrica/impl/i;->e:I

    return v0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 262
    iget v0, p0, Lcom/yandex/metrica/impl/i;->k:I

    return v0
.end method

.method public q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/yandex/metrica/impl/i;->l:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 423
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "[event: %s, type: %d, value: %s]"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/yandex/metrica/impl/i;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/yandex/metrica/impl/i;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/yandex/metrica/impl/i;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
