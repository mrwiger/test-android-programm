.class public abstract Lru/cn/view/CursorRecyclerViewAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "CursorRecyclerViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/view/CursorRecyclerViewAdapter$NotifyingDataSetObserver;,
        Lru/cn/view/CursorRecyclerViewAdapter$OnItemClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<VH:",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">",
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<TVH;>;"
    }
.end annotation


# instance fields
.field private mCursor:Landroid/database/Cursor;

.field private mDataSetObserver:Landroid/database/DataSetObserver;

.field private mDataValid:Z

.field private mItemListener:Lru/cn/view/CursorRecyclerViewAdapter$OnItemClickListener;

.field private mRowIdColumn:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 43
    .local p0, "this":Lru/cn/view/CursorRecyclerViewAdapter;, "Lru/cn/view/CursorRecyclerViewAdapter<TVH;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lru/cn/view/CursorRecyclerViewAdapter;-><init>(Landroid/database/Cursor;Z)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;Z)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "hasStableIds"    # Z

    .prologue
    .line 46
    .local p0, "this":Lru/cn/view/CursorRecyclerViewAdapter;, "Lru/cn/view/CursorRecyclerViewAdapter<TVH;>;"
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 47
    iput-object p1, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mCursor:Landroid/database/Cursor;

    .line 48
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mDataValid:Z

    .line 49
    iget-boolean v0, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mDataValid:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mCursor:Landroid/database/Cursor;

    const-string v1, "_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    :goto_1
    iput v0, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mRowIdColumn:I

    .line 50
    invoke-virtual {p0, p2}, Lru/cn/view/CursorRecyclerViewAdapter;->setHasStableIds(Z)V

    .line 51
    new-instance v0, Lru/cn/view/CursorRecyclerViewAdapter$NotifyingDataSetObserver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lru/cn/view/CursorRecyclerViewAdapter$NotifyingDataSetObserver;-><init>(Lru/cn/view/CursorRecyclerViewAdapter;Lru/cn/view/CursorRecyclerViewAdapter$1;)V

    iput-object v0, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mDataSetObserver:Landroid/database/DataSetObserver;

    .line 52
    iget-object v0, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mDataSetObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 55
    :cond_0
    return-void

    .line 48
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 49
    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method

.method static synthetic access$100(Lru/cn/view/CursorRecyclerViewAdapter;)Lru/cn/view/CursorRecyclerViewAdapter$OnItemClickListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/view/CursorRecyclerViewAdapter;

    .prologue
    .line 26
    iget-object v0, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mItemListener:Lru/cn/view/CursorRecyclerViewAdapter$OnItemClickListener;

    return-object v0
.end method

.method static synthetic access$202(Lru/cn/view/CursorRecyclerViewAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lru/cn/view/CursorRecyclerViewAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mDataValid:Z

    return p1
.end method


# virtual methods
.method public changeCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 118
    .local p0, "this":Lru/cn/view/CursorRecyclerViewAdapter;, "Lru/cn/view/CursorRecyclerViewAdapter<TVH;>;"
    invoke-virtual {p0, p1}, Lru/cn/view/CursorRecyclerViewAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 119
    .local v0, "old":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 120
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 122
    :cond_0
    return-void
.end method

.method public getCursor()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 62
    .local p0, "this":Lru/cn/view/CursorRecyclerViewAdapter;, "Lru/cn/view/CursorRecyclerViewAdapter<TVH;>;"
    iget-object v0, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method public getItem(I)Landroid/database/Cursor;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 84
    .local p0, "this":Lru/cn/view/CursorRecyclerViewAdapter;, "Lru/cn/view/CursorRecyclerViewAdapter<TVH;>;"
    iget-boolean v0, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mDataValid:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mCursor:Landroid/database/Cursor;

    .line 88
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 67
    .local p0, "this":Lru/cn/view/CursorRecyclerViewAdapter;, "Lru/cn/view/CursorRecyclerViewAdapter<TVH;>;"
    iget-boolean v0, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mDataValid:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 70
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 75
    .local p0, "this":Lru/cn/view/CursorRecyclerViewAdapter;, "Lru/cn/view/CursorRecyclerViewAdapter<TVH;>;"
    iget-boolean v0, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mDataValid:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mCursor:Landroid/database/Cursor;

    iget v1, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mRowIdColumn:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 79
    :goto_0
    return-wide v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$Adapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 4
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVH;I)V"
        }
    .end annotation

    .prologue
    .line 95
    .local p0, "this":Lru/cn/view/CursorRecyclerViewAdapter;, "Lru/cn/view/CursorRecyclerViewAdapter<TVH;>;"
    .local p1, "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TVH;"
    iget-boolean v1, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mDataValid:Z

    if-nez v1, :cond_0

    .line 96
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "this should only be called when the cursor is valid"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 98
    :cond_0
    iget-object v1, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 99
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "couldn\'t move cursor to position "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 101
    :cond_1
    iget-object v1, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mCursor:Landroid/database/Cursor;

    invoke-virtual {p0, p1, v1}, Lru/cn/view/CursorRecyclerViewAdapter;->onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/database/Cursor;)V

    .line 103
    move-object v0, p1

    .line 104
    .local v0, "vh":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TVH;"
    iget-object v1, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    new-instance v2, Lru/cn/view/CursorRecyclerViewAdapter$1;

    invoke-direct {v2, p0, v0}, Lru/cn/view/CursorRecyclerViewAdapter$1;-><init>(Lru/cn/view/CursorRecyclerViewAdapter;Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    return-void
.end method

.method public abstract onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/database/Cursor;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVH;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation
.end method

.method public setOnItemClickListener(Lru/cn/view/CursorRecyclerViewAdapter$OnItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/view/CursorRecyclerViewAdapter$OnItemClickListener;

    .prologue
    .line 58
    .local p0, "this":Lru/cn/view/CursorRecyclerViewAdapter;, "Lru/cn/view/CursorRecyclerViewAdapter<TVH;>;"
    iput-object p1, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mItemListener:Lru/cn/view/CursorRecyclerViewAdapter$OnItemClickListener;

    .line 59
    return-void
.end method

.method public swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 3
    .param p1, "newCursor"    # Landroid/database/Cursor;

    .prologue
    .line 130
    .local p0, "this":Lru/cn/view/CursorRecyclerViewAdapter;, "Lru/cn/view/CursorRecyclerViewAdapter<TVH;>;"
    iget-object v1, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mCursor:Landroid/database/Cursor;

    if-ne p1, v1, :cond_0

    .line 131
    const/4 v0, 0x0

    .line 151
    :goto_0
    return-object v0

    .line 133
    :cond_0
    iget-object v0, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mCursor:Landroid/database/Cursor;

    .line 134
    .local v0, "oldCursor":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    iget-object v1, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mDataSetObserver:Landroid/database/DataSetObserver;

    if-eqz v1, :cond_1

    .line 135
    iget-object v1, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mDataSetObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 137
    :cond_1
    iput-object p1, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mCursor:Landroid/database/Cursor;

    .line 138
    iget-object v1, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_3

    .line 139
    iget-object v1, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mDataSetObserver:Landroid/database/DataSetObserver;

    if-eqz v1, :cond_2

    .line 140
    iget-object v1, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mCursor:Landroid/database/Cursor;

    iget-object v2, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mDataSetObserver:Landroid/database/DataSetObserver;

    invoke-interface {v1, v2}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 142
    :cond_2
    const-string v1, "_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mRowIdColumn:I

    .line 143
    const/4 v1, 0x1

    iput-boolean v1, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mDataValid:Z

    .line 144
    invoke-virtual {p0}, Lru/cn/view/CursorRecyclerViewAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 146
    :cond_3
    const/4 v1, -0x1

    iput v1, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mRowIdColumn:I

    .line 147
    const/4 v1, 0x0

    iput-boolean v1, p0, Lru/cn/view/CursorRecyclerViewAdapter;->mDataValid:Z

    .line 148
    invoke-virtual {p0}, Lru/cn/view/CursorRecyclerViewAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method
