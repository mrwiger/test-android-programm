.class public Lru/cn/ad/video/VAST/VastAdapter;
.super Lru/cn/ad/video/VAST/VastBaseAdapter;
.source "VastAdapter.java"


# instance fields
.field private final vastTag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "system"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lru/cn/ad/video/VAST/VastBaseAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    .line 17
    const-string v0, "vast_ad_tag"

    invoke-virtual {p2, v0}, Lru/cn/api/money_miner/replies/AdSystem;->getParamOrThrow(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/video/VAST/VastAdapter;->vastTag:Ljava/lang/String;

    .line 18
    return-void
.end method

.method static synthetic access$000(Lru/cn/ad/video/VAST/VastAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/VAST/VastAdapter;

    .prologue
    .line 11
    iget-object v0, p0, Lru/cn/ad/video/VAST/VastAdapter;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/ad/video/VAST/VastAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/VAST/VastAdapter;

    .prologue
    .line 11
    iget-object v0, p0, Lru/cn/ad/video/VAST/VastAdapter;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/ad/video/VAST/VastAdapter;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/VAST/VastAdapter;

    .prologue
    .line 11
    iget-object v0, p0, Lru/cn/ad/video/VAST/VastAdapter;->vastTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/ad/video/VAST/VastAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/VAST/VastAdapter;

    .prologue
    .line 11
    iget-object v0, p0, Lru/cn/ad/video/VAST/VastAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$400(Lru/cn/ad/video/VAST/VastAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/VAST/VastAdapter;

    .prologue
    .line 11
    iget-object v0, p0, Lru/cn/ad/video/VAST/VastAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$500(Lru/cn/ad/video/VAST/VastAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/VAST/VastAdapter;

    .prologue
    .line 11
    iget-object v0, p0, Lru/cn/ad/video/VAST/VastAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method


# virtual methods
.method protected onLoad()V
    .locals 2

    .prologue
    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/ad/video/VAST/VastAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/ad/video/VAST/VastAdapter;->parsers:Ljava/util/List;

    .line 25
    new-instance v0, Lru/cn/ad/video/VAST/VastAdapter$1;

    invoke-direct {v0, p0}, Lru/cn/ad/video/VAST/VastAdapter$1;-><init>(Lru/cn/ad/video/VAST/VastAdapter;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 47
    invoke-virtual {v0, v1}, Lru/cn/ad/video/VAST/VastAdapter$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 48
    return-void
.end method
