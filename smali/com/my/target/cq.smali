.class public final Lcom/my/target/cq;
.super Lcom/my/target/e;
.source "NativeAdResultProcessor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/my/target/e",
        "<",
        "Lcom/my/target/cs;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/my/target/e;-><init>()V

    .line 36
    return-void
.end method

.method public static e()Lcom/my/target/cq;
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/my/target/cq;

    invoke-direct {v0}, Lcom/my/target/cq;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Lcom/my/target/ak;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/ak;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 26
    check-cast p1, Lcom/my/target/cs;

    .line 1041
    invoke-virtual {p1}, Lcom/my/target/cs;->R()Ljava/util/List;

    move-result-object v0

    .line 1042
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1044
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/a;

    .line 1046
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/a;->getVideoBanner()Lcom/my/target/aj;

    move-result-object v1

    .line 1047
    if-eqz v1, :cond_1

    .line 1049
    invoke-virtual {v1}, Lcom/my/target/aj;->getMediaData()Lcom/my/target/ag;

    move-result-object v1

    check-cast v1, Lcom/my/target/common/models/VideoData;

    .line 1050
    if-eqz v1, :cond_1

    invoke-virtual {p2}, Lcom/my/target/b;->isAutoLoadVideo()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/my/target/common/models/VideoData;->isCacheable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1052
    invoke-static {}, Lcom/my/target/ay;->an()Lcom/my/target/ay;

    move-result-object v2

    invoke-virtual {v1}, Lcom/my/target/common/models/VideoData;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5, p3}, Lcom/my/target/ay;->f(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1053
    invoke-virtual {v1, v2}, Lcom/my/target/common/models/VideoData;->setData(Ljava/lang/Object;)V

    .line 1057
    :cond_1
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/a;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    .line 1058
    if-eqz v1, :cond_2

    .line 1060
    invoke-virtual {v1, v6}, Lcom/my/target/common/models/ImageData;->useCache(Z)V

    .line 1061
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1063
    :cond_2
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/a;->getIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    .line 1064
    if-eqz v1, :cond_3

    .line 1066
    invoke-virtual {v1, v6}, Lcom/my/target/common/models/ImageData;->useCache(Z)V

    .line 1067
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1069
    :cond_3
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/a;->getNativeAdCards()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/b;

    .line 1071
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/b;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 1072
    if-eqz v0, :cond_4

    .line 1074
    invoke-virtual {v0, v6}, Lcom/my/target/common/models/ImageData;->useCache(Z)V

    .line 1075
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1079
    :cond_5
    invoke-virtual {p2}, Lcom/my/target/b;->isAutoLoadImages()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 1081
    invoke-static {v3}, Lcom/my/target/ch;->a(Ljava/util/List;)Lcom/my/target/ch;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/my/target/ch;->v(Landroid/content/Context;)V

    .line 26
    :cond_6
    return-object p1
.end method
