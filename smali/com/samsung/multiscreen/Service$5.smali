.class final Lcom/samsung/multiscreen/Service$5;
.super Ljava/lang/Object;
.source "Service.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/multiscreen/Service;->WakeOnWirelessLan(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$magicPacket:[B


# direct methods
.method constructor <init>([B)V
    .locals 0

    .prologue
    .line 408
    iput-object p1, p0, Lcom/samsung/multiscreen/Service$5;->val$magicPacket:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 411
    const/4 v2, 0x0

    .line 414
    .local v2, "wowSocket":Ljava/net/DatagramSocket;
    :try_start_0
    new-instance v3, Ljava/net/DatagramSocket;

    const/16 v4, 0x7de

    invoke-direct {v3, v4}, Ljava/net/DatagramSocket;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 415
    .end local v2    # "wowSocket":Ljava/net/DatagramSocket;
    .local v3, "wowSocket":Ljava/net/DatagramSocket;
    :try_start_1
    new-instance v1, Ljava/net/DatagramPacket;

    iget-object v4, p0, Lcom/samsung/multiscreen/Service$5;->val$magicPacket:[B

    iget-object v5, p0, Lcom/samsung/multiscreen/Service$5;->val$magicPacket:[B

    array-length v5, v5

    invoke-direct {v1, v4, v5}, Ljava/net/DatagramPacket;-><init>([BI)V

    .line 417
    .local v1, "wowPacket":Ljava/net/DatagramPacket;
    const-string v4, "255.255.255.255"

    invoke-static {v4}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/net/DatagramPacket;->setAddress(Ljava/net/InetAddress;)V

    .line 418
    const/16 v4, 0x7de

    invoke-virtual {v1, v4}, Ljava/net/DatagramPacket;->setPort(I)V

    .line 420
    invoke-virtual {v3, v1}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 425
    if-eqz v3, :cond_2

    .line 426
    invoke-virtual {v3}, Ljava/net/DatagramSocket;->close()V

    move-object v2, v3

    .line 428
    .end local v1    # "wowPacket":Ljava/net/DatagramPacket;
    .end local v3    # "wowSocket":Ljava/net/DatagramSocket;
    .restart local v2    # "wowSocket":Ljava/net/DatagramSocket;
    :cond_0
    :goto_0
    return-void

    .line 422
    :catch_0
    move-exception v0

    .line 423
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 425
    if-eqz v2, :cond_0

    .line 426
    invoke-virtual {v2}, Ljava/net/DatagramSocket;->close()V

    goto :goto_0

    .line 425
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    :goto_2
    if-eqz v2, :cond_1

    .line 426
    invoke-virtual {v2}, Ljava/net/DatagramSocket;->close()V

    :cond_1
    throw v4

    .line 425
    .end local v2    # "wowSocket":Ljava/net/DatagramSocket;
    .restart local v3    # "wowSocket":Ljava/net/DatagramSocket;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "wowSocket":Ljava/net/DatagramSocket;
    .restart local v2    # "wowSocket":Ljava/net/DatagramSocket;
    goto :goto_2

    .line 422
    .end local v2    # "wowSocket":Ljava/net/DatagramSocket;
    .restart local v3    # "wowSocket":Ljava/net/DatagramSocket;
    :catch_1
    move-exception v0

    move-object v2, v3

    .end local v3    # "wowSocket":Ljava/net/DatagramSocket;
    .restart local v2    # "wowSocket":Ljava/net/DatagramSocket;
    goto :goto_1

    .end local v2    # "wowSocket":Ljava/net/DatagramSocket;
    .restart local v1    # "wowPacket":Ljava/net/DatagramPacket;
    .restart local v3    # "wowSocket":Ljava/net/DatagramSocket;
    :cond_2
    move-object v2, v3

    .end local v3    # "wowSocket":Ljava/net/DatagramSocket;
    .restart local v2    # "wowSocket":Ljava/net/DatagramSocket;
    goto :goto_0
.end method
