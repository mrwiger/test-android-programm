.class public Lcom/google/obf/ge;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field private static final b:[C


# instance fields
.field a:I

.field private final c:Ljava/io/Reader;

.field private d:Z

.field private final e:[C

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:J

.field private k:I

.field private l:Ljava/lang/String;

.field private m:[I

.field private n:I

.field private o:[Ljava/lang/String;

.field private p:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 663
    const-string v0, ")]}\'\n"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/google/obf/ge;->b:[C

    .line 664
    new-instance v0, Lcom/google/obf/ge$1;

    invoke-direct {v0}, Lcom/google/obf/ge$1;-><init>()V

    sput-object v0, Lcom/google/obf/fh;->a:Lcom/google/obf/fh;

    .line 665
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 4

    .prologue
    const/16 v3, 0x20

    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-boolean v1, p0, Lcom/google/obf/ge;->d:Z

    .line 3
    const/16 v0, 0x400

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/google/obf/ge;->e:[C

    .line 4
    iput v1, p0, Lcom/google/obf/ge;->f:I

    .line 5
    iput v1, p0, Lcom/google/obf/ge;->g:I

    .line 6
    iput v1, p0, Lcom/google/obf/ge;->h:I

    .line 7
    iput v1, p0, Lcom/google/obf/ge;->i:I

    .line 8
    iput v1, p0, Lcom/google/obf/ge;->a:I

    .line 9
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/google/obf/ge;->m:[I

    .line 10
    iput v1, p0, Lcom/google/obf/ge;->n:I

    .line 11
    iget-object v0, p0, Lcom/google/obf/ge;->m:[I

    iget v1, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/obf/ge;->n:I

    const/4 v2, 0x6

    aput v2, v0, v1

    .line 12
    new-array v0, v3, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/obf/ge;->o:[Ljava/lang/String;

    .line 13
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/google/obf/ge;->p:[I

    .line 14
    if-nez p1, :cond_0

    .line 15
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "in == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 16
    :cond_0
    iput-object p1, p0, Lcom/google/obf/ge;->c:Ljava/io/Reader;

    .line 17
    return-void
.end method

.method static synthetic a(Lcom/google/obf/ge;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 662
    invoke-direct {p0}, Lcom/google/obf/ge;->x()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 504
    iget v0, p0, Lcom/google/obf/ge;->n:I

    iget-object v1, p0, Lcom/google/obf/ge;->m:[I

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 505
    iget v0, p0, Lcom/google/obf/ge;->n:I

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [I

    .line 506
    iget v1, p0, Lcom/google/obf/ge;->n:I

    mul-int/lit8 v1, v1, 0x2

    new-array v1, v1, [I

    .line 507
    iget v2, p0, Lcom/google/obf/ge;->n:I

    mul-int/lit8 v2, v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    .line 508
    iget-object v3, p0, Lcom/google/obf/ge;->m:[I

    iget v4, p0, Lcom/google/obf/ge;->n:I

    invoke-static {v3, v5, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 509
    iget-object v3, p0, Lcom/google/obf/ge;->p:[I

    iget v4, p0, Lcom/google/obf/ge;->n:I

    invoke-static {v3, v5, v1, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 510
    iget-object v3, p0, Lcom/google/obf/ge;->o:[Ljava/lang/String;

    iget v4, p0, Lcom/google/obf/ge;->n:I

    invoke-static {v3, v5, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 511
    iput-object v0, p0, Lcom/google/obf/ge;->m:[I

    .line 512
    iput-object v1, p0, Lcom/google/obf/ge;->p:[I

    .line 513
    iput-object v2, p0, Lcom/google/obf/ge;->o:[Ljava/lang/String;

    .line 514
    :cond_0
    iget-object v0, p0, Lcom/google/obf/ge;->m:[I

    iget v1, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/obf/ge;->n:I

    aput p1, v0, v1

    .line 515
    return-void
.end method

.method private a(C)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 244
    sparse-switch p1, :sswitch_data_0

    .line 247
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 245
    :sswitch_0
    invoke-direct {p0}, Lcom/google/obf/ge;->v()V

    .line 246
    :sswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 244
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_1
        0xa -> :sswitch_1
        0xc -> :sswitch_1
        0xd -> :sswitch_1
        0x20 -> :sswitch_1
        0x23 -> :sswitch_0
        0x2c -> :sswitch_1
        0x2f -> :sswitch_0
        0x3a -> :sswitch_1
        0x3b -> :sswitch_0
        0x3d -> :sswitch_0
        0x5b -> :sswitch_1
        0x5c -> :sswitch_0
        0x5d -> :sswitch_1
        0x7b -> :sswitch_1
        0x7d -> :sswitch_1
    .end sparse-switch
.end method

.method private a(Ljava/lang/String;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 596
    :goto_0
    iget v0, p0, Lcom/google/obf/ge;->f:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v0, v2

    iget v2, p0, Lcom/google/obf/ge;->g:I

    if-le v0, v2, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 597
    :cond_0
    iget-object v0, p0, Lcom/google/obf/ge;->e:[C

    iget v2, p0, Lcom/google/obf/ge;->f:I

    aget-char v0, v0, v2

    const/16 v2, 0xa

    if-ne v0, v2, :cond_2

    .line 598
    iget v0, p0, Lcom/google/obf/ge;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/ge;->h:I

    .line 599
    iget v0, p0, Lcom/google/obf/ge;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/ge;->i:I

    .line 606
    :cond_1
    iget v0, p0, Lcom/google/obf/ge;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/ge;->f:I

    goto :goto_0

    :cond_2
    move v0, v1

    .line 601
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 602
    iget-object v2, p0, Lcom/google/obf/ge;->e:[C

    iget v3, p0, Lcom/google/obf/ge;->f:I

    add-int/2addr v3, v0

    aget-char v2, v2, v3

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v2, v3, :cond_1

    .line 604
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 605
    :cond_3
    const/4 v1, 0x1

    .line 607
    :cond_4
    return v1
.end method

.method private b(Z)I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 532
    iget-object v3, p0, Lcom/google/obf/ge;->e:[C

    .line 533
    iget v1, p0, Lcom/google/obf/ge;->f:I

    .line 534
    iget v0, p0, Lcom/google/obf/ge;->g:I

    .line 535
    :goto_0
    if-ne v1, v0, :cond_1

    .line 536
    iput v1, p0, Lcom/google/obf/ge;->f:I

    .line 537
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 580
    if-eqz p1, :cond_8

    .line 581
    new-instance v0, Ljava/io/EOFException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "End of input"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/obf/ge;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 539
    :cond_0
    iget v1, p0, Lcom/google/obf/ge;->f:I

    .line 540
    iget v0, p0, Lcom/google/obf/ge;->g:I

    .line 541
    :cond_1
    add-int/lit8 v2, v1, 0x1

    aget-char v1, v3, v1

    .line 542
    const/16 v4, 0xa

    if-ne v1, v4, :cond_2

    .line 543
    iget v1, p0, Lcom/google/obf/ge;->h:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/obf/ge;->h:I

    .line 544
    iput v2, p0, Lcom/google/obf/ge;->i:I

    move v1, v2

    .line 545
    goto :goto_0

    .line 546
    :cond_2
    const/16 v4, 0x20

    if-eq v1, v4, :cond_9

    const/16 v4, 0xd

    if-eq v1, v4, :cond_9

    const/16 v4, 0x9

    if-ne v1, v4, :cond_3

    move v1, v2

    .line 547
    goto :goto_0

    .line 548
    :cond_3
    const/16 v4, 0x2f

    if-ne v1, v4, :cond_6

    .line 549
    iput v2, p0, Lcom/google/obf/ge;->f:I

    .line 550
    if-ne v2, v0, :cond_4

    .line 551
    iget v0, p0, Lcom/google/obf/ge;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/obf/ge;->f:I

    .line 552
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(I)Z

    move-result v0

    .line 553
    iget v2, p0, Lcom/google/obf/ge;->f:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/obf/ge;->f:I

    .line 554
    if-nez v0, :cond_4

    move v0, v1

    .line 582
    :goto_1
    return v0

    .line 556
    :cond_4
    invoke-direct {p0}, Lcom/google/obf/ge;->v()V

    .line 557
    iget v0, p0, Lcom/google/obf/ge;->f:I

    aget-char v0, v3, v0

    .line 558
    sparse-switch v0, :sswitch_data_0

    move v0, v1

    .line 570
    goto :goto_1

    .line 559
    :sswitch_0
    iget v0, p0, Lcom/google/obf/ge;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/ge;->f:I

    .line 560
    const-string v0, "*/"

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 561
    const-string v0, "Unterminated comment"

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 562
    :cond_5
    iget v0, p0, Lcom/google/obf/ge;->f:I

    add-int/lit8 v1, v0, 0x2

    .line 563
    iget v0, p0, Lcom/google/obf/ge;->g:I

    goto/16 :goto_0

    .line 565
    :sswitch_1
    iget v0, p0, Lcom/google/obf/ge;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/ge;->f:I

    .line 566
    invoke-direct {p0}, Lcom/google/obf/ge;->w()V

    .line 567
    iget v1, p0, Lcom/google/obf/ge;->f:I

    .line 568
    iget v0, p0, Lcom/google/obf/ge;->g:I

    goto/16 :goto_0

    .line 571
    :cond_6
    const/16 v0, 0x23

    if-ne v1, v0, :cond_7

    .line 572
    iput v2, p0, Lcom/google/obf/ge;->f:I

    .line 573
    invoke-direct {p0}, Lcom/google/obf/ge;->v()V

    .line 574
    invoke-direct {p0}, Lcom/google/obf/ge;->w()V

    .line 575
    iget v1, p0, Lcom/google/obf/ge;->f:I

    .line 576
    iget v0, p0, Lcom/google/obf/ge;->g:I

    goto/16 :goto_0

    .line 577
    :cond_7
    iput v2, p0, Lcom/google/obf/ge;->f:I

    move v0, v1

    .line 578
    goto :goto_1

    .line 582
    :cond_8
    const/4 v0, -0x1

    goto :goto_1

    :cond_9
    move v1, v2

    goto/16 :goto_0

    .line 558
    :sswitch_data_0
    .sparse-switch
        0x2a -> :sswitch_0
        0x2f -> :sswitch_1
    .end sparse-switch
.end method

.method private b(Ljava/lang/String;)Ljava/io/IOException;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 651
    new-instance v0, Lcom/google/obf/gh;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/obf/ge;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/gh;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b(C)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 357
    iget-object v4, p0, Lcom/google/obf/ge;->e:[C

    .line 358
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 359
    :cond_0
    iget v0, p0, Lcom/google/obf/ge;->f:I

    .line 360
    iget v1, p0, Lcom/google/obf/ge;->g:I

    move v3, v0

    .line 362
    :goto_0
    if-ge v3, v1, :cond_4

    .line 363
    add-int/lit8 v2, v3, 0x1

    aget-char v3, v4, v3

    .line 364
    if-ne v3, p1, :cond_1

    .line 365
    iput v2, p0, Lcom/google/obf/ge;->f:I

    .line 366
    sub-int v1, v2, v0

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v5, v4, v0, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 367
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 368
    :cond_1
    const/16 v6, 0x5c

    if-ne v3, v6, :cond_3

    .line 369
    iput v2, p0, Lcom/google/obf/ge;->f:I

    .line 370
    sub-int v1, v2, v0

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v5, v4, v0, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 371
    invoke-direct {p0}, Lcom/google/obf/ge;->y()C

    move-result v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 372
    iget v0, p0, Lcom/google/obf/ge;->f:I

    .line 373
    iget v1, p0, Lcom/google/obf/ge;->g:I

    move v2, v0

    :cond_2
    :goto_1
    move v3, v2

    .line 378
    goto :goto_0

    .line 375
    :cond_3
    const/16 v6, 0xa

    if-ne v3, v6, :cond_2

    .line 376
    iget v3, p0, Lcom/google/obf/ge;->h:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/obf/ge;->h:I

    .line 377
    iput v2, p0, Lcom/google/obf/ge;->i:I

    goto :goto_1

    .line 379
    :cond_4
    sub-int v1, v3, v0

    invoke-virtual {v5, v4, v0, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 380
    iput v3, p0, Lcom/google/obf/ge;->f:I

    .line 381
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 382
    const-string v0, "Unterminated string"

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method private b(I)Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 516
    iget-object v1, p0, Lcom/google/obf/ge;->e:[C

    .line 517
    iget v2, p0, Lcom/google/obf/ge;->i:I

    iget v3, p0, Lcom/google/obf/ge;->f:I

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/google/obf/ge;->i:I

    .line 518
    iget v2, p0, Lcom/google/obf/ge;->g:I

    iget v3, p0, Lcom/google/obf/ge;->f:I

    if-eq v2, v3, :cond_3

    .line 519
    iget v2, p0, Lcom/google/obf/ge;->g:I

    iget v3, p0, Lcom/google/obf/ge;->f:I

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/google/obf/ge;->g:I

    .line 520
    iget v2, p0, Lcom/google/obf/ge;->f:I

    iget v3, p0, Lcom/google/obf/ge;->g:I

    invoke-static {v1, v2, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 522
    :goto_0
    iput v0, p0, Lcom/google/obf/ge;->f:I

    .line 523
    :cond_0
    iget-object v2, p0, Lcom/google/obf/ge;->c:Ljava/io/Reader;

    iget v3, p0, Lcom/google/obf/ge;->g:I

    array-length v4, v1

    iget v5, p0, Lcom/google/obf/ge;->g:I

    sub-int/2addr v4, v5

    invoke-virtual {v2, v1, v3, v4}, Ljava/io/Reader;->read([CII)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 524
    iget v3, p0, Lcom/google/obf/ge;->g:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/google/obf/ge;->g:I

    .line 525
    iget v2, p0, Lcom/google/obf/ge;->h:I

    if-nez v2, :cond_1

    iget v2, p0, Lcom/google/obf/ge;->i:I

    if-nez v2, :cond_1

    iget v2, p0, Lcom/google/obf/ge;->g:I

    if-lez v2, :cond_1

    aget-char v2, v1, v0

    const v3, 0xfeff

    if-ne v2, v3, :cond_1

    .line 526
    iget v2, p0, Lcom/google/obf/ge;->f:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/obf/ge;->f:I

    .line 527
    iget v2, p0, Lcom/google/obf/ge;->i:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/obf/ge;->i:I

    .line 528
    add-int/lit8 p1, p1, 0x1

    .line 529
    :cond_1
    iget v2, p0, Lcom/google/obf/ge;->g:I

    if-lt v2, p1, :cond_0

    .line 530
    const/4 v0, 0x1

    .line 531
    :cond_2
    return v0

    .line 521
    :cond_3
    iput v0, p0, Lcom/google/obf/ge;->g:I

    goto :goto_0
.end method

.method private c(C)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 407
    iget-object v3, p0, Lcom/google/obf/ge;->e:[C

    .line 408
    :cond_0
    iget v1, p0, Lcom/google/obf/ge;->f:I

    .line 409
    iget v0, p0, Lcom/google/obf/ge;->g:I

    move v2, v1

    .line 410
    :goto_0
    if-ge v2, v0, :cond_4

    .line 411
    add-int/lit8 v1, v2, 0x1

    aget-char v2, v3, v2

    .line 412
    if-ne v2, p1, :cond_1

    .line 413
    iput v1, p0, Lcom/google/obf/ge;->f:I

    .line 414
    return-void

    .line 415
    :cond_1
    const/16 v4, 0x5c

    if-ne v2, v4, :cond_3

    .line 416
    iput v1, p0, Lcom/google/obf/ge;->f:I

    .line 417
    invoke-direct {p0}, Lcom/google/obf/ge;->y()C

    .line 418
    iget v1, p0, Lcom/google/obf/ge;->f:I

    .line 419
    iget v0, p0, Lcom/google/obf/ge;->g:I

    :cond_2
    :goto_1
    move v2, v1

    .line 423
    goto :goto_0

    .line 420
    :cond_3
    const/16 v4, 0xa

    if-ne v2, v4, :cond_2

    .line 421
    iget v2, p0, Lcom/google/obf/ge;->h:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/obf/ge;->h:I

    .line 422
    iput v1, p0, Lcom/google/obf/ge;->i:I

    goto :goto_1

    .line 424
    :cond_4
    iput v2, p0, Lcom/google/obf/ge;->f:I

    .line 425
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 426
    const-string v0, "Unterminated string"

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method private o()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 154
    iget-object v0, p0, Lcom/google/obf/ge;->e:[C

    iget v1, p0, Lcom/google/obf/ge;->f:I

    aget-char v0, v0, v1

    .line 155
    const/16 v1, 0x74

    if-eq v0, v1, :cond_0

    const/16 v1, 0x54

    if-ne v0, v1, :cond_1

    .line 156
    :cond_0
    const-string v2, "true"

    .line 157
    const-string v1, "TRUE"

    .line 158
    const/4 v0, 0x5

    .line 168
    :goto_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    .line 169
    const/4 v4, 0x1

    :goto_1
    if-ge v4, v5, :cond_8

    .line 170
    iget v6, p0, Lcom/google/obf/ge;->f:I

    add-int/2addr v6, v4

    iget v7, p0, Lcom/google/obf/ge;->g:I

    if-lt v6, v7, :cond_6

    add-int/lit8 v6, v4, 0x1

    invoke-direct {p0, v6}, Lcom/google/obf/ge;->b(I)Z

    move-result v6

    if-nez v6, :cond_6

    move v0, v3

    .line 180
    :goto_2
    return v0

    .line 159
    :cond_1
    const/16 v1, 0x66

    if-eq v0, v1, :cond_2

    const/16 v1, 0x46

    if-ne v0, v1, :cond_3

    .line 160
    :cond_2
    const-string v2, "false"

    .line 161
    const-string v1, "FALSE"

    .line 162
    const/4 v0, 0x6

    goto :goto_0

    .line 163
    :cond_3
    const/16 v1, 0x6e

    if-eq v0, v1, :cond_4

    const/16 v1, 0x4e

    if-ne v0, v1, :cond_5

    .line 164
    :cond_4
    const-string v2, "null"

    .line 165
    const-string v1, "NULL"

    .line 166
    const/4 v0, 0x7

    goto :goto_0

    :cond_5
    move v0, v3

    .line 167
    goto :goto_2

    .line 172
    :cond_6
    iget-object v6, p0, Lcom/google/obf/ge;->e:[C

    iget v7, p0, Lcom/google/obf/ge;->f:I

    add-int/2addr v7, v4

    aget-char v6, v6, v7

    .line 173
    invoke-virtual {v2, v4}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-eq v6, v7, :cond_7

    invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-eq v6, v7, :cond_7

    move v0, v3

    .line 174
    goto :goto_2

    .line 175
    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 176
    :cond_8
    iget v1, p0, Lcom/google/obf/ge;->f:I

    add-int/2addr v1, v5

    iget v2, p0, Lcom/google/obf/ge;->g:I

    if-lt v1, v2, :cond_9

    add-int/lit8 v1, v5, 0x1

    invoke-direct {p0, v1}, Lcom/google/obf/ge;->b(I)Z

    move-result v1

    if-eqz v1, :cond_a

    :cond_9
    iget-object v1, p0, Lcom/google/obf/ge;->e:[C

    iget v2, p0, Lcom/google/obf/ge;->f:I

    add-int/2addr v2, v5

    aget-char v1, v1, v2

    .line 177
    invoke-direct {p0, v1}, Lcom/google/obf/ge;->a(C)Z

    move-result v1

    if-eqz v1, :cond_a

    move v0, v3

    .line 178
    goto :goto_2

    .line 179
    :cond_a
    iget v1, p0, Lcom/google/obf/ge;->f:I

    add-int/2addr v1, v5

    iput v1, p0, Lcom/google/obf/ge;->f:I

    .line 180
    iput v0, p0, Lcom/google/obf/ge;->a:I

    goto :goto_2
.end method

.method private s()I
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 181
    iget-object v11, p0, Lcom/google/obf/ge;->e:[C

    .line 182
    iget v2, p0, Lcom/google/obf/ge;->f:I

    .line 183
    iget v1, p0, Lcom/google/obf/ge;->g:I

    .line 184
    const-wide/16 v6, 0x0

    .line 185
    const/4 v5, 0x0

    .line 186
    const/4 v4, 0x1

    .line 187
    const/4 v3, 0x0

    .line 188
    const/4 v0, 0x0

    move v10, v0

    move v0, v1

    move v1, v2

    .line 189
    :goto_0
    add-int v2, v1, v10

    if-ne v2, v0, :cond_4

    .line 190
    array-length v0, v11

    if-ne v10, v0, :cond_0

    .line 191
    const/4 v0, 0x0

    .line 243
    :goto_1
    return v0

    .line 192
    :cond_0
    add-int/lit8 v0, v10, 0x1

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 236
    :cond_1
    const/4 v0, 0x2

    if-ne v3, v0, :cond_16

    if-eqz v4, :cond_16

    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, v6, v0

    if-nez v0, :cond_2

    if-eqz v5, :cond_16

    .line 237
    :cond_2
    if-eqz v5, :cond_15

    :goto_2
    iput-wide v6, p0, Lcom/google/obf/ge;->j:J

    .line 238
    iget v0, p0, Lcom/google/obf/ge;->f:I

    add-int/2addr v0, v10

    iput v0, p0, Lcom/google/obf/ge;->f:I

    .line 239
    const/16 v0, 0xf

    iput v0, p0, Lcom/google/obf/ge;->a:I

    goto :goto_1

    .line 194
    :cond_3
    iget v1, p0, Lcom/google/obf/ge;->f:I

    .line 195
    iget v0, p0, Lcom/google/obf/ge;->g:I

    .line 196
    :cond_4
    add-int v2, v1, v10

    aget-char v2, v11, v2

    .line 197
    sparse-switch v2, :sswitch_data_0

    .line 218
    const/16 v8, 0x30

    if-lt v2, v8, :cond_5

    const/16 v8, 0x39

    if-le v2, v8, :cond_c

    .line 219
    :cond_5
    invoke-direct {p0, v2}, Lcom/google/obf/ge;->a(C)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 221
    const/4 v0, 0x0

    goto :goto_1

    .line 198
    :sswitch_0
    if-nez v3, :cond_6

    .line 199
    const/4 v3, 0x1

    .line 200
    const/4 v2, 0x1

    move v14, v4

    move v4, v3

    move v3, v14

    .line 235
    :goto_3
    add-int/lit8 v5, v10, 0x1

    move v10, v5

    move v5, v4

    move v4, v3

    move v3, v2

    goto :goto_0

    .line 202
    :cond_6
    const/4 v2, 0x5

    if-ne v3, v2, :cond_7

    .line 203
    const/4 v2, 0x6

    move v3, v4

    move v4, v5

    .line 204
    goto :goto_3

    .line 205
    :cond_7
    const/4 v0, 0x0

    goto :goto_1

    .line 206
    :sswitch_1
    const/4 v2, 0x5

    if-ne v3, v2, :cond_8

    .line 207
    const/4 v2, 0x6

    move v3, v4

    move v4, v5

    .line 208
    goto :goto_3

    .line 209
    :cond_8
    const/4 v0, 0x0

    goto :goto_1

    .line 210
    :sswitch_2
    const/4 v2, 0x2

    if-eq v3, v2, :cond_9

    const/4 v2, 0x4

    if-ne v3, v2, :cond_a

    .line 211
    :cond_9
    const/4 v2, 0x5

    move v3, v4

    move v4, v5

    .line 212
    goto :goto_3

    .line 213
    :cond_a
    const/4 v0, 0x0

    goto :goto_1

    .line 214
    :sswitch_3
    const/4 v2, 0x2

    if-ne v3, v2, :cond_b

    .line 215
    const/4 v2, 0x3

    move v3, v4

    move v4, v5

    .line 216
    goto :goto_3

    .line 217
    :cond_b
    const/4 v0, 0x0

    goto :goto_1

    .line 222
    :cond_c
    const/4 v8, 0x1

    if-eq v3, v8, :cond_d

    if-nez v3, :cond_e

    .line 223
    :cond_d
    add-int/lit8 v2, v2, -0x30

    neg-int v2, v2

    int-to-long v6, v2

    .line 224
    const/4 v2, 0x2

    move v3, v4

    move v4, v5

    goto :goto_3

    .line 225
    :cond_e
    const/4 v8, 0x2

    if-ne v3, v8, :cond_12

    .line 226
    const-wide/16 v8, 0x0

    cmp-long v8, v6, v8

    if-nez v8, :cond_f

    .line 227
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 228
    :cond_f
    const-wide/16 v8, 0xa

    mul-long/2addr v8, v6

    add-int/lit8 v2, v2, -0x30

    int-to-long v12, v2

    sub-long/2addr v8, v12

    .line 229
    const-wide v12, -0xcccccccccccccccL

    cmp-long v2, v6, v12

    if-gtz v2, :cond_10

    const-wide v12, -0xcccccccccccccccL

    cmp-long v2, v6, v12

    if-nez v2, :cond_11

    cmp-long v2, v8, v6

    if-gez v2, :cond_11

    :cond_10
    const/4 v2, 0x1

    :goto_4
    and-int/2addr v2, v4

    move v4, v5

    move-wide v6, v8

    move v14, v3

    move v3, v2

    move v2, v14

    .line 231
    goto :goto_3

    .line 229
    :cond_11
    const/4 v2, 0x0

    goto :goto_4

    .line 231
    :cond_12
    const/4 v2, 0x3

    if-ne v3, v2, :cond_13

    .line 232
    const/4 v2, 0x4

    move v3, v4

    move v4, v5

    goto :goto_3

    .line 233
    :cond_13
    const/4 v2, 0x5

    if-eq v3, v2, :cond_14

    const/4 v2, 0x6

    if-ne v3, v2, :cond_19

    .line 234
    :cond_14
    const/4 v2, 0x7

    move v3, v4

    move v4, v5

    goto :goto_3

    .line 237
    :cond_15
    neg-long v6, v6

    goto/16 :goto_2

    .line 240
    :cond_16
    const/4 v0, 0x2

    if-eq v3, v0, :cond_17

    const/4 v0, 0x4

    if-eq v3, v0, :cond_17

    const/4 v0, 0x7

    if-ne v3, v0, :cond_18

    .line 241
    :cond_17
    iput v10, p0, Lcom/google/obf/ge;->k:I

    .line 242
    const/16 v0, 0x10

    iput v0, p0, Lcom/google/obf/ge;->a:I

    goto/16 :goto_1

    .line 243
    :cond_18
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_19
    move v2, v3

    move v3, v4

    move v4, v5

    goto/16 :goto_3

    .line 197
    :sswitch_data_0
    .sparse-switch
        0x2b -> :sswitch_1
        0x2d -> :sswitch_0
        0x2e -> :sswitch_3
        0x45 -> :sswitch_2
        0x65 -> :sswitch_2
    .end sparse-switch
.end method

.method private t()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 384
    const/4 v0, 0x0

    move v1, v2

    .line 386
    :goto_0
    iget v3, p0, Lcom/google/obf/ge;->f:I

    add-int/2addr v3, v1

    iget v4, p0, Lcom/google/obf/ge;->g:I

    if-ge v3, v4, :cond_1

    .line 387
    iget-object v3, p0, Lcom/google/obf/ge;->e:[C

    iget v4, p0, Lcom/google/obf/ge;->f:I

    add-int/2addr v4, v1

    aget-char v3, v3, v4

    sparse-switch v3, :sswitch_data_0

    .line 390
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 388
    :sswitch_0
    invoke-direct {p0}, Lcom/google/obf/ge;->v()V

    .line 401
    :cond_0
    :goto_1
    :sswitch_1
    if-nez v0, :cond_4

    .line 402
    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/obf/ge;->e:[C

    iget v3, p0, Lcom/google/obf/ge;->f:I

    invoke-direct {v0, v2, v3, v1}, Ljava/lang/String;-><init>([CII)V

    .line 405
    :goto_2
    iget v2, p0, Lcom/google/obf/ge;->f:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/obf/ge;->f:I

    .line 406
    return-object v0

    .line 391
    :cond_1
    iget-object v3, p0, Lcom/google/obf/ge;->e:[C

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 392
    add-int/lit8 v3, v1, 0x1

    invoke-direct {p0, v3}, Lcom/google/obf/ge;->b(I)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    .line 394
    :cond_2
    if-nez v0, :cond_3

    .line 395
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 396
    :cond_3
    iget-object v3, p0, Lcom/google/obf/ge;->e:[C

    iget v4, p0, Lcom/google/obf/ge;->f:I

    invoke-virtual {v0, v3, v4, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 397
    iget v3, p0, Lcom/google/obf/ge;->f:I

    add-int/2addr v1, v3

    iput v1, p0, Lcom/google/obf/ge;->f:I

    .line 399
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/obf/ge;->b(I)Z

    move-result v1

    if-nez v1, :cond_5

    move v1, v2

    .line 400
    goto :goto_1

    .line 403
    :cond_4
    iget-object v2, p0, Lcom/google/obf/ge;->e:[C

    iget v3, p0, Lcom/google/obf/ge;->f:I

    invoke-virtual {v0, v2, v3, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 404
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_0

    .line 387
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_1
        0xa -> :sswitch_1
        0xc -> :sswitch_1
        0xd -> :sswitch_1
        0x20 -> :sswitch_1
        0x23 -> :sswitch_0
        0x2c -> :sswitch_1
        0x2f -> :sswitch_0
        0x3a -> :sswitch_1
        0x3b -> :sswitch_0
        0x3d -> :sswitch_0
        0x5b -> :sswitch_1
        0x5c -> :sswitch_0
        0x5d -> :sswitch_1
        0x7b -> :sswitch_1
        0x7d -> :sswitch_1
    .end sparse-switch
.end method

.method private u()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 427
    :cond_0
    const/4 v0, 0x0

    .line 428
    :goto_0
    iget v1, p0, Lcom/google/obf/ge;->f:I

    add-int/2addr v1, v0

    iget v2, p0, Lcom/google/obf/ge;->g:I

    if-ge v1, v2, :cond_1

    .line 429
    iget-object v1, p0, Lcom/google/obf/ge;->e:[C

    iget v2, p0, Lcom/google/obf/ge;->f:I

    add-int/2addr v2, v0

    aget-char v1, v1, v2

    sparse-switch v1, :sswitch_data_0

    .line 433
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 430
    :sswitch_0
    invoke-direct {p0}, Lcom/google/obf/ge;->v()V

    .line 431
    :sswitch_1
    iget v1, p0, Lcom/google/obf/ge;->f:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/ge;->f:I

    .line 436
    :goto_1
    return-void

    .line 434
    :cond_1
    iget v1, p0, Lcom/google/obf/ge;->f:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/ge;->f:I

    .line 435
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 429
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_1
        0xa -> :sswitch_1
        0xc -> :sswitch_1
        0xd -> :sswitch_1
        0x20 -> :sswitch_1
        0x23 -> :sswitch_0
        0x2c -> :sswitch_1
        0x2f -> :sswitch_0
        0x3a -> :sswitch_1
        0x3b -> :sswitch_0
        0x3d -> :sswitch_0
        0x5b -> :sswitch_1
        0x5c -> :sswitch_0
        0x5d -> :sswitch_1
        0x7b -> :sswitch_1
        0x7d -> :sswitch_1
    .end sparse-switch
.end method

.method private v()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 583
    iget-boolean v0, p0, Lcom/google/obf/ge;->d:Z

    if-nez v0, :cond_0

    .line 584
    const-string v0, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 585
    :cond_0
    return-void
.end method

.method private w()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 586
    :cond_0
    iget v0, p0, Lcom/google/obf/ge;->f:I

    iget v1, p0, Lcom/google/obf/ge;->g:I

    if-lt v0, v1, :cond_1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 587
    :cond_1
    iget-object v0, p0, Lcom/google/obf/ge;->e:[C

    iget v1, p0, Lcom/google/obf/ge;->f:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/obf/ge;->f:I

    aget-char v0, v0, v1

    .line 588
    const/16 v1, 0xa

    if-ne v0, v1, :cond_3

    .line 589
    iget v0, p0, Lcom/google/obf/ge;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/ge;->h:I

    .line 590
    iget v0, p0, Lcom/google/obf/ge;->f:I

    iput v0, p0, Lcom/google/obf/ge;->i:I

    .line 595
    :cond_2
    :goto_0
    return-void

    .line 592
    :cond_3
    const/16 v1, 0xd

    if-ne v0, v1, :cond_0

    goto :goto_0
.end method

.method private x()Ljava/lang/String;
    .locals 4

    .prologue
    .line 609
    iget v0, p0, Lcom/google/obf/ge;->h:I

    add-int/lit8 v0, v0, 0x1

    .line 610
    iget v1, p0, Lcom/google/obf/ge;->f:I

    iget v2, p0, Lcom/google/obf/ge;->i:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    .line 611
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " at line "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " column "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " path "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/obf/ge;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private y()C
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x4

    .line 622
    iget v0, p0, Lcom/google/obf/ge;->f:I

    iget v1, p0, Lcom/google/obf/ge;->g:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 623
    const-string v0, "Unterminated escape sequence"

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 624
    :cond_0
    iget-object v0, p0, Lcom/google/obf/ge;->e:[C

    iget v1, p0, Lcom/google/obf/ge;->f:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/obf/ge;->f:I

    aget-char v0, v0, v1

    .line 625
    sparse-switch v0, :sswitch_data_0

    .line 650
    const-string v0, "Invalid escape sequence"

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 626
    :sswitch_0
    iget v0, p0, Lcom/google/obf/ge;->f:I

    add-int/lit8 v0, v0, 0x4

    iget v1, p0, Lcom/google/obf/ge;->g:I

    if-le v0, v1, :cond_1

    invoke-direct {p0, v5}, Lcom/google/obf/ge;->b(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 627
    const-string v0, "Unterminated escape sequence"

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 628
    :cond_1
    const/4 v1, 0x0

    .line 629
    iget v0, p0, Lcom/google/obf/ge;->f:I

    add-int/lit8 v2, v0, 0x4

    move v6, v0

    move v0, v1

    move v1, v6

    :goto_0
    if-ge v1, v2, :cond_5

    .line 630
    iget-object v3, p0, Lcom/google/obf/ge;->e:[C

    aget-char v3, v3, v1

    .line 631
    shl-int/lit8 v0, v0, 0x4

    int-to-char v0, v0

    .line 632
    const/16 v4, 0x30

    if-lt v3, v4, :cond_2

    const/16 v4, 0x39

    if-gt v3, v4, :cond_2

    .line 633
    add-int/lit8 v3, v3, -0x30

    add-int/2addr v0, v3

    int-to-char v0, v0

    .line 639
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 634
    :cond_2
    const/16 v4, 0x61

    if-lt v3, v4, :cond_3

    const/16 v4, 0x66

    if-gt v3, v4, :cond_3

    .line 635
    add-int/lit8 v3, v3, -0x61

    add-int/lit8 v3, v3, 0xa

    add-int/2addr v0, v3

    int-to-char v0, v0

    goto :goto_1

    .line 636
    :cond_3
    const/16 v4, 0x41

    if-lt v3, v4, :cond_4

    const/16 v4, 0x46

    if-gt v3, v4, :cond_4

    .line 637
    add-int/lit8 v3, v3, -0x41

    add-int/lit8 v3, v3, 0xa

    add-int/2addr v0, v3

    int-to-char v0, v0

    goto :goto_1

    .line 638
    :cond_4
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\\u"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/obf/ge;->e:[C

    iget v4, p0, Lcom/google/obf/ge;->f:I

    invoke-direct {v2, v3, v4, v5}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 640
    :cond_5
    iget v1, p0, Lcom/google/obf/ge;->f:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/obf/ge;->f:I

    .line 649
    :goto_2
    :sswitch_1
    return v0

    .line 642
    :sswitch_2
    const/16 v0, 0x9

    goto :goto_2

    .line 643
    :sswitch_3
    const/16 v0, 0x8

    goto :goto_2

    .line 644
    :sswitch_4
    const/16 v0, 0xa

    goto :goto_2

    .line 645
    :sswitch_5
    const/16 v0, 0xd

    goto :goto_2

    .line 646
    :sswitch_6
    const/16 v0, 0xc

    goto :goto_2

    .line 647
    :sswitch_7
    iget v1, p0, Lcom/google/obf/ge;->h:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/obf/ge;->h:I

    .line 648
    iget v1, p0, Lcom/google/obf/ge;->f:I

    iput v1, p0, Lcom/google/obf/ge;->i:I

    goto :goto_2

    .line 625
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_7
        0x22 -> :sswitch_1
        0x27 -> :sswitch_1
        0x2f -> :sswitch_1
        0x5c -> :sswitch_1
        0x62 -> :sswitch_3
        0x66 -> :sswitch_6
        0x6e -> :sswitch_4
        0x72 -> :sswitch_5
        0x74 -> :sswitch_2
        0x75 -> :sswitch_0
    .end sparse-switch
.end method

.method private z()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 652
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(Z)I

    .line 653
    iget v0, p0, Lcom/google/obf/ge;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/obf/ge;->f:I

    .line 654
    iget v0, p0, Lcom/google/obf/ge;->f:I

    sget-object v1, Lcom/google/obf/ge;->b:[C

    array-length v1, v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/obf/ge;->g:I

    if-le v0, v1, :cond_1

    sget-object v0, Lcom/google/obf/ge;->b:[C

    array-length v0, v0

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 661
    :cond_0
    :goto_0
    return-void

    .line 656
    :cond_1
    const/4 v0, 0x0

    :goto_1
    sget-object v1, Lcom/google/obf/ge;->b:[C

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 657
    iget-object v1, p0, Lcom/google/obf/ge;->e:[C

    iget v2, p0, Lcom/google/obf/ge;->f:I

    add-int/2addr v2, v0

    aget-char v1, v1, v2

    sget-object v2, Lcom/google/obf/ge;->b:[C

    aget-char v2, v2, v0

    if-ne v1, v2, :cond_0

    .line 659
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 660
    :cond_2
    iget v0, p0, Lcom/google/obf/ge;->f:I

    sget-object v1, Lcom/google/obf/ge;->b:[C

    array-length v1, v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/ge;->f:I

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 21
    iget v0, p0, Lcom/google/obf/ge;->a:I

    .line 22
    if-nez v0, :cond_0

    .line 23
    invoke-virtual {p0}, Lcom/google/obf/ge;->r()I

    move-result v0

    .line 24
    :cond_0
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 25
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->a(I)V

    .line 26
    iget-object v0, p0, Lcom/google/obf/ge;->p:[I

    iget v1, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v1, v1, -0x1

    aput v2, v0, v1

    .line 27
    iput v2, p0, Lcom/google/obf/ge;->a:I

    .line 29
    return-void

    .line 28
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected BEGIN_ARRAY but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/obf/ge;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/google/obf/ge;->d:Z

    .line 19
    return-void
.end method

.method public b()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    iget v0, p0, Lcom/google/obf/ge;->a:I

    .line 31
    if-nez v0, :cond_0

    .line 32
    invoke-virtual {p0}, Lcom/google/obf/ge;->r()I

    move-result v0

    .line 33
    :cond_0
    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 34
    iget v0, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/obf/ge;->n:I

    .line 35
    iget-object v0, p0, Lcom/google/obf/ge;->p:[I

    iget v1, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/ge;->a:I

    .line 38
    return-void

    .line 37
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected END_ARRAY but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/obf/ge;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    iget v0, p0, Lcom/google/obf/ge;->a:I

    .line 40
    if-nez v0, :cond_0

    .line 41
    invoke-virtual {p0}, Lcom/google/obf/ge;->r()I

    move-result v0

    .line 42
    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 43
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->a(I)V

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/ge;->a:I

    .line 46
    return-void

    .line 45
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected BEGIN_OBJECT but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/obf/ge;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 470
    iput v2, p0, Lcom/google/obf/ge;->a:I

    .line 471
    iget-object v0, p0, Lcom/google/obf/ge;->m:[I

    const/16 v1, 0x8

    aput v1, v0, v2

    .line 472
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/obf/ge;->n:I

    .line 473
    iget-object v0, p0, Lcom/google/obf/ge;->c:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->close()V

    .line 474
    return-void
.end method

.method public d()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    iget v0, p0, Lcom/google/obf/ge;->a:I

    .line 48
    if-nez v0, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/google/obf/ge;->r()I

    move-result v0

    .line 50
    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 51
    iget v0, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/obf/ge;->n:I

    .line 52
    iget-object v0, p0, Lcom/google/obf/ge;->o:[Ljava/lang/String;

    iget v1, p0, Lcom/google/obf/ge;->n:I

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 53
    iget-object v0, p0, Lcom/google/obf/ge;->p:[I

    iget v1, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 54
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/ge;->a:I

    .line 56
    return-void

    .line 55
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected END_OBJECT but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/obf/ge;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public e()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    iget v0, p0, Lcom/google/obf/ge;->a:I

    .line 58
    if-nez v0, :cond_0

    .line 59
    invoke-virtual {p0}, Lcom/google/obf/ge;->r()I

    move-result v0

    .line 60
    :cond_0
    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/google/obf/gf;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    iget v0, p0, Lcom/google/obf/ge;->a:I

    .line 62
    if-nez v0, :cond_0

    .line 63
    invoke-virtual {p0}, Lcom/google/obf/ge;->r()I

    move-result v0

    .line 64
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 75
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 65
    :pswitch_0
    sget-object v0, Lcom/google/obf/gf;->c:Lcom/google/obf/gf;

    .line 74
    :goto_0
    return-object v0

    .line 66
    :pswitch_1
    sget-object v0, Lcom/google/obf/gf;->d:Lcom/google/obf/gf;

    goto :goto_0

    .line 67
    :pswitch_2
    sget-object v0, Lcom/google/obf/gf;->a:Lcom/google/obf/gf;

    goto :goto_0

    .line 68
    :pswitch_3
    sget-object v0, Lcom/google/obf/gf;->b:Lcom/google/obf/gf;

    goto :goto_0

    .line 69
    :pswitch_4
    sget-object v0, Lcom/google/obf/gf;->e:Lcom/google/obf/gf;

    goto :goto_0

    .line 70
    :pswitch_5
    sget-object v0, Lcom/google/obf/gf;->h:Lcom/google/obf/gf;

    goto :goto_0

    .line 71
    :pswitch_6
    sget-object v0, Lcom/google/obf/gf;->i:Lcom/google/obf/gf;

    goto :goto_0

    .line 72
    :pswitch_7
    sget-object v0, Lcom/google/obf/gf;->f:Lcom/google/obf/gf;

    goto :goto_0

    .line 73
    :pswitch_8
    sget-object v0, Lcom/google/obf/gf;->g:Lcom/google/obf/gf;

    goto :goto_0

    .line 74
    :pswitch_9
    sget-object v0, Lcom/google/obf/gf;->j:Lcom/google/obf/gf;

    goto :goto_0

    .line 64
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_8
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public g()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 248
    iget v0, p0, Lcom/google/obf/ge;->a:I

    .line 249
    if-nez v0, :cond_0

    .line 250
    invoke-virtual {p0}, Lcom/google/obf/ge;->r()I

    move-result v0

    .line 251
    :cond_0
    const/16 v1, 0xe

    if-ne v0, v1, :cond_1

    .line 252
    invoke-direct {p0}, Lcom/google/obf/ge;->t()Ljava/lang/String;

    move-result-object v0

    .line 258
    :goto_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/obf/ge;->a:I

    .line 259
    iget-object v1, p0, Lcom/google/obf/ge;->o:[Ljava/lang/String;

    iget v2, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v2, v2, -0x1

    aput-object v0, v1, v2

    .line 260
    return-object v0

    .line 253
    :cond_1
    const/16 v1, 0xc

    if-ne v0, v1, :cond_2

    .line 254
    const/16 v0, 0x27

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 255
    :cond_2
    const/16 v1, 0xd

    if-ne v0, v1, :cond_3

    .line 256
    const/16 v0, 0x22

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 257
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a name but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/obf/ge;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public h()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 261
    iget v0, p0, Lcom/google/obf/ge;->a:I

    .line 262
    if-nez v0, :cond_0

    .line 263
    invoke-virtual {p0}, Lcom/google/obf/ge;->r()I

    move-result v0

    .line 264
    :cond_0
    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    .line 265
    invoke-direct {p0}, Lcom/google/obf/ge;->t()Ljava/lang/String;

    move-result-object v0

    .line 279
    :goto_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/obf/ge;->a:I

    .line 280
    iget-object v1, p0, Lcom/google/obf/ge;->p:[I

    iget v2, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v2, v2, -0x1

    aget v3, v1, v2

    add-int/lit8 v3, v3, 0x1

    aput v3, v1, v2

    .line 281
    return-object v0

    .line 266
    :cond_1
    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 267
    const/16 v0, 0x27

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 268
    :cond_2
    const/16 v1, 0x9

    if-ne v0, v1, :cond_3

    .line 269
    const/16 v0, 0x22

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 270
    :cond_3
    const/16 v1, 0xb

    if-ne v0, v1, :cond_4

    .line 271
    iget-object v0, p0, Lcom/google/obf/ge;->l:Ljava/lang/String;

    .line 272
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/obf/ge;->l:Ljava/lang/String;

    goto :goto_0

    .line 273
    :cond_4
    const/16 v1, 0xf

    if-ne v0, v1, :cond_5

    .line 274
    iget-wide v0, p0, Lcom/google/obf/ge;->j:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 275
    :cond_5
    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    .line 276
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/obf/ge;->e:[C

    iget v2, p0, Lcom/google/obf/ge;->f:I

    iget v3, p0, Lcom/google/obf/ge;->k:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    .line 277
    iget v1, p0, Lcom/google/obf/ge;->f:I

    iget v2, p0, Lcom/google/obf/ge;->k:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/obf/ge;->f:I

    goto :goto_0

    .line 278
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a string but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/obf/ge;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public i()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 282
    iget v1, p0, Lcom/google/obf/ge;->a:I

    .line 283
    if-nez v1, :cond_0

    .line 284
    invoke-virtual {p0}, Lcom/google/obf/ge;->r()I

    move-result v1

    .line 285
    :cond_0
    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    .line 286
    iput v0, p0, Lcom/google/obf/ge;->a:I

    .line 287
    iget-object v0, p0, Lcom/google/obf/ge;->p:[I

    iget v1, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 288
    const/4 v0, 0x1

    .line 292
    :goto_0
    return v0

    .line 289
    :cond_1
    const/4 v2, 0x6

    if-ne v1, v2, :cond_2

    .line 290
    iput v0, p0, Lcom/google/obf/ge;->a:I

    .line 291
    iget-object v1, p0, Lcom/google/obf/ge;->p:[I

    iget v2, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v2, v2, -0x1

    aget v3, v1, v2

    add-int/lit8 v3, v3, 0x1

    aput v3, v1, v2

    goto :goto_0

    .line 293
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a boolean but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/obf/ge;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public j()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 294
    iget v0, p0, Lcom/google/obf/ge;->a:I

    .line 295
    if-nez v0, :cond_0

    .line 296
    invoke-virtual {p0}, Lcom/google/obf/ge;->r()I

    move-result v0

    .line 297
    :cond_0
    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    .line 298
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/ge;->a:I

    .line 299
    iget-object v0, p0, Lcom/google/obf/ge;->p:[I

    iget v1, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 301
    return-void

    .line 300
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected null but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/obf/ge;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public k()D
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0xb

    const/16 v2, 0x8

    const/4 v4, 0x0

    .line 302
    iget v0, p0, Lcom/google/obf/ge;->a:I

    .line 303
    if-nez v0, :cond_0

    .line 304
    invoke-virtual {p0}, Lcom/google/obf/ge;->r()I

    move-result v0

    .line 305
    :cond_0
    const/16 v1, 0xf

    if-ne v0, v1, :cond_1

    .line 306
    iput v4, p0, Lcom/google/obf/ge;->a:I

    .line 307
    iget-object v0, p0, Lcom/google/obf/ge;->p:[I

    iget v1, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 308
    iget-wide v0, p0, Lcom/google/obf/ge;->j:J

    long-to-double v0, v0

    .line 326
    :goto_0
    return-wide v0

    .line 309
    :cond_1
    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 310
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/obf/ge;->e:[C

    iget v2, p0, Lcom/google/obf/ge;->f:I

    iget v3, p0, Lcom/google/obf/ge;->k:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, p0, Lcom/google/obf/ge;->l:Ljava/lang/String;

    .line 311
    iget v0, p0, Lcom/google/obf/ge;->f:I

    iget v1, p0, Lcom/google/obf/ge;->k:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/ge;->f:I

    .line 318
    :cond_2
    :goto_1
    iput v5, p0, Lcom/google/obf/ge;->a:I

    .line 319
    iget-object v0, p0, Lcom/google/obf/ge;->l:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 320
    iget-boolean v2, p0, Lcom/google/obf/ge;->d:Z

    if-nez v2, :cond_9

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {v0, v1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 321
    :cond_3
    new-instance v2, Lcom/google/obf/gh;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "JSON forbids NaN and infinities: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 322
    invoke-direct {p0}, Lcom/google/obf/ge;->x()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/obf/gh;-><init>(Ljava/lang/String;)V

    throw v2

    .line 312
    :cond_4
    if-eq v0, v2, :cond_5

    const/16 v1, 0x9

    if-ne v0, v1, :cond_7

    .line 313
    :cond_5
    if-ne v0, v2, :cond_6

    const/16 v0, 0x27

    :goto_2
    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/ge;->l:Ljava/lang/String;

    goto :goto_1

    :cond_6
    const/16 v0, 0x22

    goto :goto_2

    .line 314
    :cond_7
    const/16 v1, 0xa

    if-ne v0, v1, :cond_8

    .line 315
    invoke-direct {p0}, Lcom/google/obf/ge;->t()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/ge;->l:Ljava/lang/String;

    goto :goto_1

    .line 316
    :cond_8
    if-eq v0, v5, :cond_2

    .line 317
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a double but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/obf/ge;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 323
    :cond_9
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/obf/ge;->l:Ljava/lang/String;

    .line 324
    iput v4, p0, Lcom/google/obf/ge;->a:I

    .line 325
    iget-object v2, p0, Lcom/google/obf/ge;->p:[I

    iget v3, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v3, v3, -0x1

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    goto/16 :goto_0
.end method

.method public l()J
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v3, 0xa

    const/16 v2, 0x8

    const/4 v6, 0x0

    .line 327
    iget v0, p0, Lcom/google/obf/ge;->a:I

    .line 328
    if-nez v0, :cond_0

    .line 329
    invoke-virtual {p0}, Lcom/google/obf/ge;->r()I

    move-result v0

    .line 330
    :cond_0
    const/16 v1, 0xf

    if-ne v0, v1, :cond_1

    .line 331
    iput v6, p0, Lcom/google/obf/ge;->a:I

    .line 332
    iget-object v0, p0, Lcom/google/obf/ge;->p:[I

    iget v1, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 333
    iget-wide v0, p0, Lcom/google/obf/ge;->j:J

    .line 356
    :goto_0
    return-wide v0

    .line 334
    :cond_1
    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    .line 335
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/obf/ge;->e:[C

    iget v2, p0, Lcom/google/obf/ge;->f:I

    iget v3, p0, Lcom/google/obf/ge;->k:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, p0, Lcom/google/obf/ge;->l:Ljava/lang/String;

    .line 336
    iget v0, p0, Lcom/google/obf/ge;->f:I

    iget v1, p0, Lcom/google/obf/ge;->k:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/ge;->f:I

    .line 348
    :goto_1
    const/16 v0, 0xb

    iput v0, p0, Lcom/google/obf/ge;->a:I

    .line 349
    iget-object v0, p0, Lcom/google/obf/ge;->l:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 350
    double-to-long v0, v2

    .line 351
    long-to-double v4, v0

    cmpl-double v2, v4, v2

    if-eqz v2, :cond_7

    .line 352
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a long but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/obf/ge;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/obf/ge;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 337
    :cond_2
    if-eq v0, v2, :cond_3

    const/16 v1, 0x9

    if-eq v0, v1, :cond_3

    if-ne v0, v3, :cond_6

    .line 338
    :cond_3
    if-ne v0, v3, :cond_4

    .line 339
    invoke-direct {p0}, Lcom/google/obf/ge;->t()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/ge;->l:Ljava/lang/String;

    .line 341
    :goto_2
    :try_start_0
    iget-object v0, p0, Lcom/google/obf/ge;->l:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 342
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/obf/ge;->a:I

    .line 343
    iget-object v2, p0, Lcom/google/obf/ge;->p:[I

    iget v3, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v3, v3, -0x1

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 345
    :catch_0
    move-exception v0

    goto :goto_1

    .line 340
    :cond_4
    if-ne v0, v2, :cond_5

    const/16 v0, 0x27

    :goto_3
    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/ge;->l:Ljava/lang/String;

    goto :goto_2

    :cond_5
    const/16 v0, 0x22

    goto :goto_3

    .line 347
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a long but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/obf/ge;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 353
    :cond_7
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/obf/ge;->l:Ljava/lang/String;

    .line 354
    iput v6, p0, Lcom/google/obf/ge;->a:I

    .line 355
    iget-object v2, p0, Lcom/google/obf/ge;->p:[I

    iget v3, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v3, v3, -0x1

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    goto/16 :goto_0
.end method

.method public m()I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v3, 0xa

    const/16 v2, 0x8

    const/4 v6, 0x0

    .line 437
    iget v0, p0, Lcom/google/obf/ge;->a:I

    .line 438
    if-nez v0, :cond_0

    .line 439
    invoke-virtual {p0}, Lcom/google/obf/ge;->r()I

    move-result v0

    .line 440
    :cond_0
    const/16 v1, 0xf

    if-ne v0, v1, :cond_2

    .line 441
    iget-wide v0, p0, Lcom/google/obf/ge;->j:J

    long-to-int v0, v0

    .line 442
    iget-wide v2, p0, Lcom/google/obf/ge;->j:J

    int-to-long v4, v0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 443
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected an int but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/obf/ge;->j:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/obf/ge;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 444
    :cond_1
    iput v6, p0, Lcom/google/obf/ge;->a:I

    .line 445
    iget-object v1, p0, Lcom/google/obf/ge;->p:[I

    iget v2, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v2, v2, -0x1

    aget v3, v1, v2

    add-int/lit8 v3, v3, 0x1

    aput v3, v1, v2

    .line 469
    :goto_0
    return v0

    .line 447
    :cond_2
    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 448
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/obf/ge;->e:[C

    iget v2, p0, Lcom/google/obf/ge;->f:I

    iget v3, p0, Lcom/google/obf/ge;->k:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, p0, Lcom/google/obf/ge;->l:Ljava/lang/String;

    .line 449
    iget v0, p0, Lcom/google/obf/ge;->f:I

    iget v1, p0, Lcom/google/obf/ge;->k:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/ge;->f:I

    .line 461
    :goto_1
    const/16 v0, 0xb

    iput v0, p0, Lcom/google/obf/ge;->a:I

    .line 462
    iget-object v0, p0, Lcom/google/obf/ge;->l:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 463
    double-to-int v0, v2

    .line 464
    int-to-double v4, v0

    cmpl-double v1, v4, v2

    if-eqz v1, :cond_8

    .line 465
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected an int but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/obf/ge;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/obf/ge;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 450
    :cond_3
    if-eq v0, v2, :cond_4

    const/16 v1, 0x9

    if-eq v0, v1, :cond_4

    if-ne v0, v3, :cond_7

    .line 451
    :cond_4
    if-ne v0, v3, :cond_5

    .line 452
    invoke-direct {p0}, Lcom/google/obf/ge;->t()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/ge;->l:Ljava/lang/String;

    .line 454
    :goto_2
    :try_start_0
    iget-object v0, p0, Lcom/google/obf/ge;->l:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 455
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/obf/ge;->a:I

    .line 456
    iget-object v1, p0, Lcom/google/obf/ge;->p:[I

    iget v2, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v2, v2, -0x1

    aget v3, v1, v2

    add-int/lit8 v3, v3, 0x1

    aput v3, v1, v2
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 458
    :catch_0
    move-exception v0

    goto :goto_1

    .line 453
    :cond_5
    if-ne v0, v2, :cond_6

    const/16 v0, 0x27

    :goto_3
    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/ge;->l:Ljava/lang/String;

    goto :goto_2

    :cond_6
    const/16 v0, 0x22

    goto :goto_3

    .line 460
    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected an int but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/obf/ge;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 466
    :cond_8
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/obf/ge;->l:Ljava/lang/String;

    .line 467
    iput v6, p0, Lcom/google/obf/ge;->a:I

    .line 468
    iget-object v1, p0, Lcom/google/obf/ge;->p:[I

    iget v2, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v2, v2, -0x1

    aget v3, v1, v2

    add-int/lit8 v3, v3, 0x1

    aput v3, v1, v2

    goto/16 :goto_0
.end method

.method public n()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 475
    move v0, v1

    .line 476
    :cond_0
    iget v2, p0, Lcom/google/obf/ge;->a:I

    .line 477
    if-nez v2, :cond_1

    .line 478
    invoke-virtual {p0}, Lcom/google/obf/ge;->r()I

    move-result v2

    .line 479
    :cond_1
    if-ne v2, v5, :cond_3

    .line 480
    invoke-direct {p0, v4}, Lcom/google/obf/ge;->a(I)V

    .line 481
    add-int/lit8 v0, v0, 0x1

    .line 499
    :cond_2
    :goto_0
    iput v1, p0, Lcom/google/obf/ge;->a:I

    .line 500
    if-nez v0, :cond_0

    .line 501
    iget-object v0, p0, Lcom/google/obf/ge;->p:[I

    iget v1, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 502
    iget-object v0, p0, Lcom/google/obf/ge;->o:[Ljava/lang/String;

    iget v1, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v1, v1, -0x1

    const-string v2, "null"

    aput-object v2, v0, v1

    .line 503
    return-void

    .line 482
    :cond_3
    if-ne v2, v4, :cond_4

    .line 483
    invoke-direct {p0, v5}, Lcom/google/obf/ge;->a(I)V

    .line 484
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 485
    :cond_4
    const/4 v3, 0x4

    if-ne v2, v3, :cond_5

    .line 486
    iget v2, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/google/obf/ge;->n:I

    .line 487
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 488
    :cond_5
    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    .line 489
    iget v2, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/google/obf/ge;->n:I

    .line 490
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 491
    :cond_6
    const/16 v3, 0xe

    if-eq v2, v3, :cond_7

    const/16 v3, 0xa

    if-ne v2, v3, :cond_8

    .line 492
    :cond_7
    invoke-direct {p0}, Lcom/google/obf/ge;->u()V

    goto :goto_0

    .line 493
    :cond_8
    const/16 v3, 0x8

    if-eq v2, v3, :cond_9

    const/16 v3, 0xc

    if-ne v2, v3, :cond_a

    .line 494
    :cond_9
    const/16 v2, 0x27

    invoke-direct {p0, v2}, Lcom/google/obf/ge;->c(C)V

    goto :goto_0

    .line 495
    :cond_a
    const/16 v3, 0x9

    if-eq v2, v3, :cond_b

    const/16 v3, 0xd

    if-ne v2, v3, :cond_c

    .line 496
    :cond_b
    const/16 v2, 0x22

    invoke-direct {p0, v2}, Lcom/google/obf/ge;->c(C)V

    goto :goto_0

    .line 497
    :cond_c
    const/16 v3, 0x10

    if-ne v2, v3, :cond_2

    .line 498
    iget v2, p0, Lcom/google/obf/ge;->f:I

    iget v3, p0, Lcom/google/obf/ge;->k:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/google/obf/ge;->f:I

    goto :goto_0
.end method

.method public p()Ljava/lang/String;
    .locals 5

    .prologue
    .line 612
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 613
    const/4 v0, 0x0

    iget v2, p0, Lcom/google/obf/ge;->n:I

    :goto_0
    if-ge v0, v2, :cond_1

    .line 614
    iget-object v3, p0, Lcom/google/obf/ge;->m:[I

    aget v3, v3, v0

    packed-switch v3, :pswitch_data_0

    .line 620
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 615
    :pswitch_0
    const/16 v3, 0x5b

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/obf/ge;->p:[I

    aget v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x5d

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 617
    :pswitch_1
    const/16 v3, 0x2e

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 618
    iget-object v3, p0, Lcom/google/obf/ge;->o:[Ljava/lang/String;

    aget-object v3, v3, v0

    if-eqz v3, :cond_0

    .line 619
    iget-object v3, p0, Lcom/google/obf/ge;->o:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 621
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 614
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/google/obf/ge;->d:Z

    return v0
.end method

.method r()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x7

    const/4 v7, 0x5

    const/4 v0, 0x4

    const/4 v1, 0x2

    const/4 v2, 0x1

    .line 76
    iget-object v4, p0, Lcom/google/obf/ge;->m:[I

    iget v5, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v5, v5, -0x1

    aget v4, v4, v5

    .line 77
    if-ne v4, v2, :cond_2

    .line 78
    iget-object v5, p0, Lcom/google/obf/ge;->m:[I

    iget v6, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v6, v6, -0x1

    aput v1, v5, v6

    .line 129
    :cond_0
    :goto_0
    :pswitch_0
    :sswitch_0
    invoke-direct {p0, v2}, Lcom/google/obf/ge;->b(Z)I

    move-result v5

    .line 130
    sparse-switch v5, :sswitch_data_0

    .line 143
    iget v0, p0, Lcom/google/obf/ge;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/obf/ge;->f:I

    .line 144
    invoke-direct {p0}, Lcom/google/obf/ge;->o()I

    move-result v0

    .line 145
    if-eqz v0, :cond_12

    .line 153
    :cond_1
    :goto_1
    return v0

    .line 79
    :cond_2
    if-ne v4, v1, :cond_3

    .line 80
    invoke-direct {p0, v2}, Lcom/google/obf/ge;->b(Z)I

    move-result v5

    .line 81
    sparse-switch v5, :sswitch_data_1

    .line 85
    const-string v0, "Unterminated array"

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 82
    :sswitch_1
    iput v0, p0, Lcom/google/obf/ge;->a:I

    goto :goto_1

    .line 83
    :sswitch_2
    invoke-direct {p0}, Lcom/google/obf/ge;->v()V

    goto :goto_0

    .line 86
    :cond_3
    const/4 v5, 0x3

    if-eq v4, v5, :cond_4

    if-ne v4, v7, :cond_8

    .line 87
    :cond_4
    iget-object v3, p0, Lcom/google/obf/ge;->m:[I

    iget v5, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v5, v5, -0x1

    aput v0, v3, v5

    .line 88
    if-ne v4, v7, :cond_5

    .line 89
    invoke-direct {p0, v2}, Lcom/google/obf/ge;->b(Z)I

    move-result v0

    .line 90
    sparse-switch v0, :sswitch_data_2

    .line 94
    const-string v0, "Unterminated object"

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 91
    :sswitch_3
    iput v1, p0, Lcom/google/obf/ge;->a:I

    move v0, v1

    goto :goto_1

    .line 92
    :sswitch_4
    invoke-direct {p0}, Lcom/google/obf/ge;->v()V

    .line 95
    :cond_5
    :sswitch_5
    invoke-direct {p0, v2}, Lcom/google/obf/ge;->b(Z)I

    move-result v0

    .line 96
    sparse-switch v0, :sswitch_data_3

    .line 103
    invoke-direct {p0}, Lcom/google/obf/ge;->v()V

    .line 104
    iget v1, p0, Lcom/google/obf/ge;->f:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/obf/ge;->f:I

    .line 105
    int-to-char v0, v0

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->a(C)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 106
    const/16 v0, 0xe

    iput v0, p0, Lcom/google/obf/ge;->a:I

    goto :goto_1

    .line 97
    :sswitch_6
    const/16 v0, 0xd

    iput v0, p0, Lcom/google/obf/ge;->a:I

    goto :goto_1

    .line 98
    :sswitch_7
    invoke-direct {p0}, Lcom/google/obf/ge;->v()V

    .line 99
    const/16 v0, 0xc

    iput v0, p0, Lcom/google/obf/ge;->a:I

    goto :goto_1

    .line 100
    :sswitch_8
    if-eq v4, v7, :cond_6

    .line 101
    iput v1, p0, Lcom/google/obf/ge;->a:I

    move v0, v1

    goto :goto_1

    .line 102
    :cond_6
    const-string v0, "Expected name"

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 107
    :cond_7
    const-string v0, "Expected name"

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 108
    :cond_8
    if-ne v4, v0, :cond_a

    .line 109
    iget-object v5, p0, Lcom/google/obf/ge;->m:[I

    iget v6, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v6, v6, -0x1

    aput v7, v5, v6

    .line 110
    invoke-direct {p0, v2}, Lcom/google/obf/ge;->b(Z)I

    move-result v5

    .line 111
    packed-switch v5, :pswitch_data_0

    .line 116
    :pswitch_1
    const-string v0, "Expected \':\'"

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 113
    :pswitch_2
    invoke-direct {p0}, Lcom/google/obf/ge;->v()V

    .line 114
    iget v5, p0, Lcom/google/obf/ge;->f:I

    iget v6, p0, Lcom/google/obf/ge;->g:I

    if-lt v5, v6, :cond_9

    invoke-direct {p0, v2}, Lcom/google/obf/ge;->b(I)Z

    move-result v5

    if-eqz v5, :cond_0

    :cond_9
    iget-object v5, p0, Lcom/google/obf/ge;->e:[C

    iget v6, p0, Lcom/google/obf/ge;->f:I

    aget-char v5, v5, v6

    const/16 v6, 0x3e

    if-ne v5, v6, :cond_0

    .line 115
    iget v5, p0, Lcom/google/obf/ge;->f:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/obf/ge;->f:I

    goto/16 :goto_0

    .line 117
    :cond_a
    const/4 v5, 0x6

    if-ne v4, v5, :cond_c

    .line 118
    iget-boolean v5, p0, Lcom/google/obf/ge;->d:Z

    if-eqz v5, :cond_b

    .line 119
    invoke-direct {p0}, Lcom/google/obf/ge;->z()V

    .line 120
    :cond_b
    iget-object v5, p0, Lcom/google/obf/ge;->m:[I

    iget v6, p0, Lcom/google/obf/ge;->n:I

    add-int/lit8 v6, v6, -0x1

    aput v3, v5, v6

    goto/16 :goto_0

    .line 121
    :cond_c
    if-ne v4, v3, :cond_e

    .line 122
    const/4 v5, 0x0

    invoke-direct {p0, v5}, Lcom/google/obf/ge;->b(Z)I

    move-result v5

    .line 123
    const/4 v6, -0x1

    if-ne v5, v6, :cond_d

    .line 124
    const/16 v0, 0x11

    iput v0, p0, Lcom/google/obf/ge;->a:I

    goto/16 :goto_1

    .line 125
    :cond_d
    invoke-direct {p0}, Lcom/google/obf/ge;->v()V

    .line 126
    iget v5, p0, Lcom/google/obf/ge;->f:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/google/obf/ge;->f:I

    goto/16 :goto_0

    .line 127
    :cond_e
    const/16 v5, 0x8

    if-ne v4, v5, :cond_0

    .line 128
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "JsonReader is closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 131
    :sswitch_9
    if-ne v4, v2, :cond_f

    .line 132
    iput v0, p0, Lcom/google/obf/ge;->a:I

    goto/16 :goto_1

    .line 133
    :cond_f
    :sswitch_a
    if-eq v4, v2, :cond_10

    if-ne v4, v1, :cond_11

    .line 134
    :cond_10
    invoke-direct {p0}, Lcom/google/obf/ge;->v()V

    .line 135
    iget v0, p0, Lcom/google/obf/ge;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/obf/ge;->f:I

    .line 136
    iput v3, p0, Lcom/google/obf/ge;->a:I

    move v0, v3

    goto/16 :goto_1

    .line 137
    :cond_11
    const-string v0, "Unexpected value"

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 138
    :sswitch_b
    invoke-direct {p0}, Lcom/google/obf/ge;->v()V

    .line 139
    const/16 v0, 0x8

    iput v0, p0, Lcom/google/obf/ge;->a:I

    goto/16 :goto_1

    .line 140
    :sswitch_c
    const/16 v0, 0x9

    iput v0, p0, Lcom/google/obf/ge;->a:I

    goto/16 :goto_1

    .line 141
    :sswitch_d
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/obf/ge;->a:I

    goto/16 :goto_1

    .line 142
    :sswitch_e
    iput v2, p0, Lcom/google/obf/ge;->a:I

    move v0, v2

    goto/16 :goto_1

    .line 147
    :cond_12
    invoke-direct {p0}, Lcom/google/obf/ge;->s()I

    move-result v0

    .line 148
    if-nez v0, :cond_1

    .line 150
    iget-object v0, p0, Lcom/google/obf/ge;->e:[C

    iget v1, p0, Lcom/google/obf/ge;->f:I

    aget-char v0, v0, v1

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->a(C)Z

    move-result v0

    if-nez v0, :cond_13

    .line 151
    const-string v0, "Expected value"

    invoke-direct {p0, v0}, Lcom/google/obf/ge;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 152
    :cond_13
    invoke-direct {p0}, Lcom/google/obf/ge;->v()V

    .line 153
    const/16 v0, 0xa

    iput v0, p0, Lcom/google/obf/ge;->a:I

    goto/16 :goto_1

    .line 130
    nop

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_c
        0x27 -> :sswitch_b
        0x2c -> :sswitch_a
        0x3b -> :sswitch_a
        0x5b -> :sswitch_d
        0x5d -> :sswitch_9
        0x7b -> :sswitch_e
    .end sparse-switch

    .line 81
    :sswitch_data_1
    .sparse-switch
        0x2c -> :sswitch_0
        0x3b -> :sswitch_2
        0x5d -> :sswitch_1
    .end sparse-switch

    .line 90
    :sswitch_data_2
    .sparse-switch
        0x2c -> :sswitch_5
        0x3b -> :sswitch_4
        0x7d -> :sswitch_3
    .end sparse-switch

    .line 96
    :sswitch_data_3
    .sparse-switch
        0x22 -> :sswitch_6
        0x27 -> :sswitch_7
        0x7d -> :sswitch_8
    .end sparse-switch

    .line 111
    :pswitch_data_0
    .packed-switch 0x3a
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 608
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/obf/ge;->x()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
