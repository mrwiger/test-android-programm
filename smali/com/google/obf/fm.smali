.class public final Lcom/google/obf/fm;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/fm$a;
    }
.end annotation


# direct methods
.method public static a(Lcom/google/obf/ge;)Lcom/google/obf/em;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/eq;
        }
    .end annotation

    .prologue
    .line 1
    const/4 v1, 0x1

    .line 2
    :try_start_0
    invoke-virtual {p0}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    .line 3
    const/4 v1, 0x0

    .line 4
    sget-object v0, Lcom/google/obf/gb;->X:Lcom/google/obf/ew;

    invoke-virtual {v0, p0}, Lcom/google/obf/ew;->read(Lcom/google/obf/ge;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/em;
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/obf/gh; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_3

    .line 7
    :goto_0
    return-object v0

    .line 5
    :catch_0
    move-exception v0

    .line 6
    if-eqz v1, :cond_0

    .line 7
    sget-object v0, Lcom/google/obf/eo;->a:Lcom/google/obf/eo;

    goto :goto_0

    .line 8
    :cond_0
    new-instance v1, Lcom/google/obf/eu;

    invoke-direct {v1, v0}, Lcom/google/obf/eu;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 9
    :catch_1
    move-exception v0

    .line 10
    new-instance v1, Lcom/google/obf/eu;

    invoke-direct {v1, v0}, Lcom/google/obf/eu;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 11
    :catch_2
    move-exception v0

    .line 12
    new-instance v1, Lcom/google/obf/en;

    invoke-direct {v1, v0}, Lcom/google/obf/en;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 13
    :catch_3
    move-exception v0

    .line 14
    new-instance v1, Lcom/google/obf/eu;

    invoke-direct {v1, v0}, Lcom/google/obf/eu;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Ljava/lang/Appendable;)Ljava/io/Writer;
    .locals 1

    .prologue
    .line 17
    instance-of v0, p0, Ljava/io/Writer;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/io/Writer;

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lcom/google/obf/fm$a;

    invoke-direct {v0, p0}, Lcom/google/obf/fm$a;-><init>(Ljava/lang/Appendable;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static a(Lcom/google/obf/em;Lcom/google/obf/gg;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15
    sget-object v0, Lcom/google/obf/gb;->X:Lcom/google/obf/ew;

    invoke-virtual {v0, p1, p0}, Lcom/google/obf/ew;->write(Lcom/google/obf/gg;Ljava/lang/Object;)V

    .line 16
    return-void
.end method
