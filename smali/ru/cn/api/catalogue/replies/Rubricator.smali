.class public Lru/cn/api/catalogue/replies/Rubricator;
.super Ljava/lang/Object;
.source "Rubricator.java"


# instance fields
.field public rubrics:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "items"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fromJson(Ljava/lang/String;)Lru/cn/api/catalogue/replies/Rubricator;
    .locals 3
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 19
    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    .line 20
    .local v0, "builder":Lcom/google/gson/GsonBuilder;
    const-class v1, Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    new-instance v2, Lru/cn/api/catalogue/retrofit/RubricUiHintDeserializer;

    invoke-direct {v2}, Lru/cn/api/catalogue/retrofit/RubricUiHintDeserializer;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    .line 23
    const-class v1, Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

    new-instance v2, Lru/cn/api/catalogue/retrofit/RubricOptionTypeDeserializer;

    invoke-direct {v2}, Lru/cn/api/catalogue/retrofit/RubricOptionTypeDeserializer;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    .line 26
    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v1

    const-class v2, Lru/cn/api/catalogue/replies/Rubricator;

    invoke-virtual {v1, p0, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/catalogue/replies/Rubricator;

    return-object v1
.end method


# virtual methods
.method public getFirstRubricByHint(Lru/cn/api/catalogue/replies/Rubric$UiHintType;)Lru/cn/api/catalogue/replies/Rubric;
    .locals 3
    .param p1, "hint"    # Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    .prologue
    .line 39
    iget-object v1, p0, Lru/cn/api/catalogue/replies/Rubricator;->rubrics:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/catalogue/replies/Rubric;

    .line 40
    .local v0, "rubric":Lru/cn/api/catalogue/replies/Rubric;
    iget-object v2, v0, Lru/cn/api/catalogue/replies/Rubric;->uiHint:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    if-ne v2, p1, :cond_0

    .line 44
    .end local v0    # "rubric":Lru/cn/api/catalogue/replies/Rubric;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSearchRubric()Lru/cn/api/catalogue/replies/Rubric;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->SEARCH:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    invoke-virtual {p0, v0}, Lru/cn/api/catalogue/replies/Rubricator;->getFirstRubricByHint(Lru/cn/api/catalogue/replies/Rubric$UiHintType;)Lru/cn/api/catalogue/replies/Rubric;

    move-result-object v0

    return-object v0
.end method

.method public toJson()Ljava/lang/String;
    .locals 3

    .prologue
    .line 30
    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    .line 31
    .local v0, "builder":Lcom/google/gson/GsonBuilder;
    const-class v1, Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    new-instance v2, Lru/cn/api/catalogue/retrofit/UiHintTypeSerializer;

    invoke-direct {v2}, Lru/cn/api/catalogue/retrofit/UiHintTypeSerializer;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    .line 33
    const-class v1, Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

    new-instance v2, Lru/cn/api/catalogue/retrofit/RubricOptionTypeSerializer;

    invoke-direct {v2}, Lru/cn/api/catalogue/retrofit/RubricOptionTypeSerializer;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    .line 35
    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
