.class public Lcom/my/target/nativeads/views/AppwallAdView;
.super Landroid/widget/FrameLayout;
.source "AppwallAdView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/nativeads/views/AppwallAdView$AppwallCardPlaceholder;,
        Lcom/my/target/nativeads/views/AppwallAdView$AppwallAdapter;,
        Lcom/my/target/nativeads/views/AppwallAdView$AppwallAdViewListener;
    }
.end annotation


# static fields
.field private static final BACKGROUND_COLOR:I = -0x111112


# instance fields
.field private appwallAdViewListener:Lcom/my/target/nativeads/views/AppwallAdView$AppwallAdViewListener;

.field private final listView:Landroid/widget/ListView;

.field private final uiUtils:Lcom/my/target/cm;

.field private final viewMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/my/target/nativeads/banners/NativeAppwallBanner;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdView;->viewMap:Ljava/util/HashMap;

    .line 48
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/my/target/nativeads/views/AppwallAdView;->setVerticalFadingEdgeEnabled(Z)V

    .line 49
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/my/target/nativeads/views/AppwallAdView;->setBackgroundColor(I)V

    .line 50
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdView;->uiUtils:Lcom/my/target/cm;

    .line 51
    new-instance v0, Landroid/widget/ListView;

    invoke-direct {v0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdView;->listView:Landroid/widget/ListView;

    .line 52
    invoke-direct {p0}, Lcom/my/target/nativeads/views/AppwallAdView;->initLayout()V

    .line 53
    return-void
.end method

.method private countVisibleBanners()V
    .locals 6

    .prologue
    .line 128
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdView;->listView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_1

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdView;->listView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    .line 134
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdView;->listView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v2

    .line 136
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v0

    .line 137
    :goto_1
    if-gt v1, v2, :cond_3

    .line 139
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdView;->listView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;

    .line 140
    iget-object v4, p0, Lcom/my/target/nativeads/views/AppwallAdView;->viewMap:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_2

    .line 142
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    iget-object v4, p0, Lcom/my/target/nativeads/views/AppwallAdView;->viewMap:Ljava/util/HashMap;

    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 147
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdView;->appwallAdViewListener:Lcom/my/target/nativeads/views/AppwallAdView$AppwallAdViewListener;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdView;->appwallAdViewListener:Lcom/my/target/nativeads/views/AppwallAdView$AppwallAdViewListener;

    invoke-interface {v0, v3}, Lcom/my/target/nativeads/views/AppwallAdView$AppwallAdViewListener;->onBannersShow(Ljava/util/List;)V

    goto :goto_0
.end method

.method private initLayout()V
    .locals 5

    .prologue
    const/4 v2, 0x4

    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 113
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v0, v2}, Lcom/my/target/cm;->n(I)I

    move-result v0

    .line 114
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    .line 116
    iget-object v2, p0, Lcom/my/target/nativeads/views/AppwallAdView;->listView:Landroid/widget/ListView;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 117
    iget-object v2, p0, Lcom/my/target/nativeads/views/AppwallAdView;->listView:Landroid/widget/ListView;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setVerticalFadingEdgeEnabled(Z)V

    .line 118
    iget-object v2, p0, Lcom/my/target/nativeads/views/AppwallAdView;->listView:Landroid/widget/ListView;

    invoke-virtual {v2, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 119
    iget-object v2, p0, Lcom/my/target/nativeads/views/AppwallAdView;->listView:Landroid/widget/ListView;

    invoke-virtual {v2, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 120
    iget-object v2, p0, Lcom/my/target/nativeads/views/AppwallAdView;->listView:Landroid/widget/ListView;

    invoke-virtual {v2, v3, v0, v3, v1}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 121
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdView;->listView:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setClipToPadding(Z)V

    .line 122
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdView;->listView:Landroid/widget/ListView;

    invoke-virtual {p0, v0, v4, v4}, Lcom/my/target/nativeads/views/AppwallAdView;->addView(Landroid/view/View;II)V

    .line 123
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdView;->listView:Landroid/widget/ListView;

    const v1, -0x111112

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setBackgroundColor(I)V

    .line 124
    return-void
.end method


# virtual methods
.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdView;->listView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/my/target/nativeads/views/AppwallAdView$AppwallAdapter;

    invoke-virtual {v0}, Lcom/my/target/nativeads/views/AppwallAdView$AppwallAdapter;->notifyDataSetChanged()V

    .line 80
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 101
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 102
    invoke-direct {p0}, Lcom/my/target/nativeads/views/AppwallAdView;->countVisibleBanners()V

    .line 104
    invoke-virtual {p0}, Lcom/my/target/nativeads/views/AppwallAdView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 107
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 109
    :cond_0
    return-void
.end method

.method public onGlobalLayout()V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/my/target/nativeads/views/AppwallAdView;->countVisibleBanners()V

    .line 96
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p3, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdView;->listView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;

    .line 86
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdView;->appwallAdViewListener:Lcom/my/target/nativeads/views/AppwallAdView$AppwallAdViewListener;

    if-eqz v1, :cond_0

    .line 88
    iget-object v1, p0, Lcom/my/target/nativeads/views/AppwallAdView;->appwallAdViewListener:Lcom/my/target/nativeads/views/AppwallAdView$AppwallAdViewListener;

    invoke-interface {v1, v0}, Lcom/my/target/nativeads/views/AppwallAdView$AppwallAdViewListener;->onBannerClick(Lcom/my/target/nativeads/banners/NativeAppwallBanner;)V

    .line 90
    :cond_0
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/my/target/nativeads/views/AppwallAdView;->countVisibleBanners()V

    .line 70
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method public setAppwallAdViewListener(Lcom/my/target/nativeads/views/AppwallAdView$AppwallAdViewListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/my/target/nativeads/views/AppwallAdView$AppwallAdViewListener;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/my/target/nativeads/views/AppwallAdView;->appwallAdViewListener:Lcom/my/target/nativeads/views/AppwallAdView$AppwallAdViewListener;

    .line 58
    return-void
.end method

.method public setupView(Lcom/my/target/nativeads/NativeAppwallAd;)V
    .locals 4
    .param p1, "appwalAd"    # Lcom/my/target/nativeads/NativeAppwallAd;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/my/target/nativeads/views/AppwallAdView;->listView:Landroid/widget/ListView;

    new-instance v1, Lcom/my/target/nativeads/views/AppwallAdView$AppwallAdapter;

    invoke-virtual {p0}, Lcom/my/target/nativeads/views/AppwallAdView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p1}, Lcom/my/target/nativeads/NativeAppwallAd;->getBanners()Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/my/target/nativeads/views/AppwallAdView$AppwallAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 75
    return-void
.end method
