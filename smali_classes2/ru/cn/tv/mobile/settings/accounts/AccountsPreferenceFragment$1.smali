.class Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment$1;
.super Ljava/lang/Object;
.source "AccountsPreferenceFragment.java"

# interfaces
.implements Landroid/support/v7/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;->addContractor(Ljava/lang/String;JLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;

.field final synthetic val$contractorId:J

.field final synthetic val$title:Ljava/lang/String;


# direct methods
.method constructor <init>(Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;Ljava/lang/String;J)V
    .locals 1
    .param p1, "this$0"    # Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;

    .prologue
    .line 71
    iput-object p1, p0, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment$1;->this$0:Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;

    iput-object p2, p0, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment$1;->val$title:Ljava/lang/String;

    iput-wide p3, p0, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment$1;->val$contractorId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/support/v7/preference/Preference;)Z
    .locals 4
    .param p1, "preference"    # Landroid/support/v7/preference/Preference;

    .prologue
    .line 74
    iget-object v1, p0, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment$1;->val$title:Ljava/lang/String;

    const-string v2, "settings"

    invoke-static {v1, v2}, Lru/cn/domain/statistics/AnalyticsManager;->store_open(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 77
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "contractor"

    iget-wide v2, p0, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment$1;->val$contractorId:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 78
    iget-object v1, p0, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment$1;->this$0:Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;

    invoke-virtual {v1}, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 79
    iget-object v1, p0, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment$1;->this$0:Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;

    invoke-virtual {v1}, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 80
    const/4 v1, 0x1

    return v1
.end method
