.class public Lcom/samsung/multiscreen/Error;
.super Ljava/lang/Object;
.source "Error.java"


# instance fields
.field private final code:J

.field private final message:Ljava/lang/String;

.field private final name:Ljava/lang/String;


# direct methods
.method private constructor <init>(JLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "code"    # J
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/samsung/multiscreen/Error;->code:J

    iput-object p3, p0, Lcom/samsung/multiscreen/Error;->name:Ljava/lang/String;

    iput-object p4, p0, Lcom/samsung/multiscreen/Error;->message:Ljava/lang/String;

    return-void
.end method

.method static create(JLjava/lang/String;Ljava/lang/String;)Lcom/samsung/multiscreen/Error;
    .locals 2
    .param p0, "code"    # J
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 99
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 100
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 103
    :cond_1
    new-instance v0, Lcom/samsung/multiscreen/Error;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/samsung/multiscreen/Error;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method static create(JLjava/util/Map;)Lcom/samsung/multiscreen/Error;
    .locals 4
    .param p0, "code"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/samsung/multiscreen/Error;"
        }
    .end annotation

    .prologue
    .line 84
    .local p2, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-nez p2, :cond_0

    .line 85
    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    .line 88
    :cond_0
    const-string v2, "name"

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 89
    .local v1, "name":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 90
    const-string v1, "error"

    .line 93
    :cond_1
    const-string v2, "message"

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 95
    .local v0, "message":Ljava/lang/String;
    new-instance v2, Lcom/samsung/multiscreen/Error;

    invoke-direct {v2, p0, p1, v1, v0}, Lcom/samsung/multiscreen/Error;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method static create(Lcom/koushikdutta/async/http/AsyncHttpResponse;)Lcom/samsung/multiscreen/Error;
    .locals 5
    .param p0, "response"    # Lcom/koushikdutta/async/http/AsyncHttpResponse;

    .prologue
    .line 64
    if-nez p0, :cond_0

    .line 65
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 67
    :cond_0
    new-instance v0, Lcom/samsung/multiscreen/Error;

    invoke-interface {p0}, Lcom/koushikdutta/async/http/AsyncHttpResponse;->code()I

    move-result v1

    int-to-long v2, v1

    const-string v1, "http error"

    .line 69
    invoke-interface {p0}, Lcom/koushikdutta/async/http/AsyncHttpResponse;->message()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v2, v3, v1, v4}, Lcom/samsung/multiscreen/Error;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method static create(Ljava/lang/Exception;)Lcom/samsung/multiscreen/Error;
    .locals 5
    .param p0, "e"    # Ljava/lang/Exception;

    .prologue
    .line 77
    if-nez p0, :cond_0

    .line 78
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 80
    :cond_0
    new-instance v0, Lcom/samsung/multiscreen/Error;

    const-wide/16 v2, -0x1

    const-string v1, "error"

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v2, v3, v1, v4}, Lcom/samsung/multiscreen/Error;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method static create(Ljava/lang/String;)Lcom/samsung/multiscreen/Error;
    .locals 3
    .param p0, "message"    # Ljava/lang/String;

    .prologue
    .line 73
    const-wide/16 v0, -0x1

    const-string v2, "error"

    invoke-static {v0, v1, v2, p0}, Lcom/samsung/multiscreen/Error;->create(JLjava/lang/String;Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected canEqual(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 43
    instance-of v0, p1, Lcom/samsung/multiscreen/Error;

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 12
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 43
    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    instance-of v7, p1, Lcom/samsung/multiscreen/Error;

    if-nez v7, :cond_2

    move v5, v6

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/samsung/multiscreen/Error;

    .local v0, "other":Lcom/samsung/multiscreen/Error;
    invoke-virtual {v0, p0}, Lcom/samsung/multiscreen/Error;->canEqual(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    move v5, v6

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Error;->getCode()J

    move-result-wide v8

    invoke-virtual {v0}, Lcom/samsung/multiscreen/Error;->getCode()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-eqz v7, :cond_4

    move v5, v6

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Error;->getName()Ljava/lang/String;

    move-result-object v4

    .local v4, "this$name":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/samsung/multiscreen/Error;->getName()Ljava/lang/String;

    move-result-object v2

    .local v2, "other$name":Ljava/lang/String;
    if-nez v4, :cond_6

    if-eqz v2, :cond_7

    :cond_5
    move v5, v6

    goto :goto_0

    :cond_6
    invoke-virtual {v4, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    :cond_7
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Error;->getMessage()Ljava/lang/String;

    move-result-object v3

    .local v3, "this$message":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/samsung/multiscreen/Error;->getMessage()Ljava/lang/String;

    move-result-object v1

    .local v1, "other$message":Ljava/lang/String;
    if-nez v3, :cond_8

    if-eqz v1, :cond_0

    :goto_1
    move v5, v6

    goto :goto_0

    :cond_8
    invoke-virtual {v3, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    goto :goto_1
.end method

.method public getCode()J
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/samsung/multiscreen/Error;->code:J

    return-wide v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/multiscreen/Error;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/multiscreen/Error;->name:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 43
    const/16 v4, 0x3b

    .local v4, "PRIME":I
    const/4 v5, 0x1

    .local v5, "result":I
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Error;->getCode()J

    move-result-wide v0

    .local v0, "$code":J
    const/16 v6, 0x20

    ushr-long v8, v0, v6

    xor-long/2addr v8, v0

    long-to-int v6, v8

    add-int/lit8 v5, v6, 0x3b

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Error;->getName()Ljava/lang/String;

    move-result-object v3

    .local v3, "$name":Ljava/lang/String;
    mul-int/lit8 v8, v5, 0x3b

    if-nez v3, :cond_0

    move v6, v7

    :goto_0
    add-int v5, v8, v6

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Error;->getMessage()Ljava/lang/String;

    move-result-object v2

    .local v2, "$message":Ljava/lang/String;
    mul-int/lit8 v6, v5, 0x3b

    if-nez v2, :cond_1

    :goto_1
    add-int v5, v6, v7

    return v5

    .end local v2    # "$message":Ljava/lang/String;
    :cond_0
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v6

    goto :goto_0

    .restart local v2    # "$message":Ljava/lang/String;
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v7

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error(code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Error;->getCode()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Error;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Error;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
