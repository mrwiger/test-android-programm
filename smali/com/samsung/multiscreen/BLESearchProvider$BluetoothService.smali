.class Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;
.super Ljava/lang/Object;
.source "BLESearchProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/multiscreen/BLESearchProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BluetoothService"
.end annotation


# instance fields
.field private final device:Landroid/bluetooth/BluetoothDevice;

.field private id:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/multiscreen/BLESearchProvider;


# direct methods
.method public constructor <init>(Lcom/samsung/multiscreen/BLESearchProvider;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p2, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;->this$0:Lcom/samsung/multiscreen/BLESearchProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;->device:Landroid/bluetooth/BluetoothDevice;

    return-void
.end method


# virtual methods
.method protected canEqual(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 85
    instance-of v0, p1, Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 85
    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    instance-of v5, p1, Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;

    if-nez v5, :cond_2

    move v3, v4

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;

    .local v0, "other":Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;
    invoke-virtual {v0, p0}, Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;->canEqual(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    move v3, v4

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    .local v2, "this$device":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v0}, Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    .local v1, "other$device":Landroid/bluetooth/BluetoothDevice;
    if-nez v2, :cond_4

    if-eqz v1, :cond_0

    :goto_1
    move v3, v4

    goto :goto_0

    :cond_4
    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    goto :goto_1
.end method

.method public getDevice()Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;->device:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;->id:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 85
    const/16 v1, 0x3b

    .local v1, "PRIME":I
    const/4 v2, 0x1

    .local v2, "result":I
    invoke-virtual {p0}, Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    .local v0, "$device":Landroid/bluetooth/BluetoothDevice;
    if-nez v0, :cond_0

    const/4 v3, 0x0

    :goto_0
    add-int/lit8 v2, v3, 0x3b

    return v2

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    goto :goto_0
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;->id:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BLESearchProvider.BluetoothService(device="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
