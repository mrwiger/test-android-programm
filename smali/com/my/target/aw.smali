.class public abstract Lcom/my/target/aw;
.super Ljava/lang/Object;
.source "HttpRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected bV:Z

.field protected dO:Z

.field protected dP:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field protected responseCode:I

.field protected s:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, -0x1

    iput v0, p0, Lcom/my/target/aw;->responseCode:I

    .line 50
    return-void
.end method


# virtual methods
.method public C()Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/my/target/aw;->bV:Z

    return v0
.end method

.method public ak()Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/my/target/aw;->dO:Z

    return v0
.end method

.method public al()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/my/target/aw;->dP:Ljava/lang/Object;

    return-object v0
.end method

.method protected abstract c(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            ")TT;"
        }
    .end annotation
.end method

.method public final f(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            ")TT;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/aw;->dO:Z

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/aw;->bV:Z

    .line 56
    const/4 v0, -0x1

    iput v0, p0, Lcom/my/target/aw;->responseCode:I

    .line 57
    iput-object v1, p0, Lcom/my/target/aw;->dP:Ljava/lang/Object;

    .line 58
    iput-object v1, p0, Lcom/my/target/aw;->s:Ljava/lang/String;

    .line 59
    invoke-virtual {p0, p1, p2}, Lcom/my/target/aw;->c(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getResponseCode()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/my/target/aw;->responseCode:I

    return v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/my/target/aw;->s:Ljava/lang/String;

    return-object v0
.end method
