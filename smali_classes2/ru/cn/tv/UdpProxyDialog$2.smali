.class Lru/cn/tv/UdpProxyDialog$2;
.super Ljava/lang/Object;
.source "UdpProxyDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/UdpProxyDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/UdpProxyDialog;


# direct methods
.method constructor <init>(Lru/cn/tv/UdpProxyDialog;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/UdpProxyDialog;

    .prologue
    .line 85
    iput-object p1, p0, Lru/cn/tv/UdpProxyDialog$2;->this$0:Lru/cn/tv/UdpProxyDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    .line 88
    iget-object v5, p0, Lru/cn/tv/UdpProxyDialog$2;->this$0:Lru/cn/tv/UdpProxyDialog;

    invoke-static {v5}, Lru/cn/tv/UdpProxyDialog;->access$000(Lru/cn/tv/UdpProxyDialog;)Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 89
    .local v0, "address":Ljava/lang/String;
    iget-object v5, p0, Lru/cn/tv/UdpProxyDialog$2;->this$0:Lru/cn/tv/UdpProxyDialog;

    invoke-static {v5}, Lru/cn/tv/UdpProxyDialog;->access$100(Lru/cn/tv/UdpProxyDialog;)Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 91
    .local v2, "portString":Ljava/lang/String;
    iget-object v5, p0, Lru/cn/tv/UdpProxyDialog$2;->this$0:Lru/cn/tv/UdpProxyDialog;

    invoke-virtual {v5}, Lru/cn/tv/UdpProxyDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 93
    .local v1, "context":Landroid/content/Context;
    iget-object v5, p0, Lru/cn/tv/UdpProxyDialog$2;->this$0:Lru/cn/tv/UdpProxyDialog;

    invoke-static {v5}, Lru/cn/tv/UdpProxyDialog;->access$200(Lru/cn/tv/UdpProxyDialog;)Landroid/widget/CheckBox;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    if-nez v5, :cond_0

    .line 95
    iget-object v5, p0, Lru/cn/tv/UdpProxyDialog$2;->this$0:Lru/cn/tv/UdpProxyDialog;

    invoke-static {v5}, Lru/cn/tv/UdpProxyDialog;->access$300(Lru/cn/tv/UdpProxyDialog;)Lru/cn/domain/UDPProxySettings;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lru/cn/domain/UDPProxySettings;->enabled(Z)Lru/cn/domain/UDPProxySettings;

    move-result-object v5

    .line 96
    invoke-virtual {v5, v1}, Lru/cn/domain/UDPProxySettings;->save(Landroid/content/Context;)V

    .line 99
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->channels()Landroid/net/Uri;

    move-result-object v4

    .line 100
    .local v4, "uri":Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-virtual {v5, v4, v7, v7, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 102
    iget-object v5, p0, Lru/cn/tv/UdpProxyDialog$2;->this$0:Lru/cn/tv/UdpProxyDialog;

    invoke-virtual {v5}, Lru/cn/tv/UdpProxyDialog;->dismiss()V

    .line 122
    .end local v4    # "uri":Landroid/net/Uri;
    :goto_0
    return-void

    .line 107
    :cond_0
    iget-object v5, p0, Lru/cn/tv/UdpProxyDialog$2;->this$0:Lru/cn/tv/UdpProxyDialog;

    invoke-static {v5, v0, v2}, Lru/cn/tv/UdpProxyDialog;->access$400(Lru/cn/tv/UdpProxyDialog;Ljava/lang/String;Ljava/lang/String;)Lru/cn/utils/validation/CompositeValidator;

    move-result-object v3

    .line 109
    .local v3, "udpProxyValidator":Lru/cn/utils/validation/Validator;
    invoke-interface {v3}, Lru/cn/utils/validation/Validator;->validate()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 110
    iget-object v5, p0, Lru/cn/tv/UdpProxyDialog$2;->this$0:Lru/cn/tv/UdpProxyDialog;

    invoke-static {v5}, Lru/cn/tv/UdpProxyDialog;->access$300(Lru/cn/tv/UdpProxyDialog;)Lru/cn/domain/UDPProxySettings;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lru/cn/domain/UDPProxySettings;->enabled(Z)Lru/cn/domain/UDPProxySettings;

    move-result-object v5

    .line 111
    invoke-virtual {v5, v0}, Lru/cn/domain/UDPProxySettings;->address(Ljava/lang/String;)Lru/cn/domain/UDPProxySettings;

    move-result-object v5

    .line 112
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Lru/cn/domain/UDPProxySettings;->port(I)Lru/cn/domain/UDPProxySettings;

    move-result-object v5

    .line 113
    invoke-virtual {v5, v1}, Lru/cn/domain/UDPProxySettings;->save(Landroid/content/Context;)V

    .line 116
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->channels()Landroid/net/Uri;

    move-result-object v4

    .line 117
    .restart local v4    # "uri":Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-virtual {v5, v4, v7, v7, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 118
    iget-object v5, p0, Lru/cn/tv/UdpProxyDialog$2;->this$0:Lru/cn/tv/UdpProxyDialog;

    invoke-virtual {v5}, Lru/cn/tv/UdpProxyDialog;->dismiss()V

    goto :goto_0

    .line 120
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_1
    iget-object v5, p0, Lru/cn/tv/UdpProxyDialog$2;->this$0:Lru/cn/tv/UdpProxyDialog;

    invoke-interface {v3}, Lru/cn/utils/validation/Validator;->getDescription()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v1, v6}, Lru/cn/tv/UdpProxyDialog;->access$500(Lru/cn/tv/UdpProxyDialog;Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method
