.class public final Lcom/google/obf/dp;
.super Ljava/lang/Object;
.source "IMASDK"


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:J


# direct methods
.method public constructor <init>([BI)V
    .locals 4

    .prologue
    const/16 v3, 0x18

    const/16 v2, 0x10

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/google/obf/dv;

    invoke-direct {v0, p1}, Lcom/google/obf/dv;-><init>([B)V

    .line 3
    mul-int/lit8 v1, p2, 0x8

    invoke-virtual {v0, v1}, Lcom/google/obf/dv;->a(I)V

    .line 4
    invoke-virtual {v0, v2}, Lcom/google/obf/dv;->c(I)I

    move-result v1

    iput v1, p0, Lcom/google/obf/dp;->a:I

    .line 5
    invoke-virtual {v0, v2}, Lcom/google/obf/dv;->c(I)I

    move-result v1

    iput v1, p0, Lcom/google/obf/dp;->b:I

    .line 6
    invoke-virtual {v0, v3}, Lcom/google/obf/dv;->c(I)I

    move-result v1

    iput v1, p0, Lcom/google/obf/dp;->c:I

    .line 7
    invoke-virtual {v0, v3}, Lcom/google/obf/dv;->c(I)I

    move-result v1

    iput v1, p0, Lcom/google/obf/dp;->d:I

    .line 8
    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lcom/google/obf/dv;->c(I)I

    move-result v1

    iput v1, p0, Lcom/google/obf/dp;->e:I

    .line 9
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/obf/dv;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/obf/dp;->f:I

    .line 10
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/obf/dv;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/obf/dp;->g:I

    .line 11
    const/16 v1, 0x24

    invoke-virtual {v0, v1}, Lcom/google/obf/dv;->c(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/obf/dp;->h:J

    .line 12
    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 13
    iget v0, p0, Lcom/google/obf/dp;->g:I

    iget v1, p0, Lcom/google/obf/dp;->e:I

    mul-int/2addr v0, v1

    return v0
.end method

.method public b()J
    .locals 4

    .prologue
    .line 14
    iget-wide v0, p0, Lcom/google/obf/dp;->h:J

    const-wide/32 v2, 0xf4240

    mul-long/2addr v0, v2

    iget v2, p0, Lcom/google/obf/dp;->e:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    return-wide v0
.end method
