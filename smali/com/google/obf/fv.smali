.class public final Lcom/google/obf/fv;
.super Lcom/google/obf/ew;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/obf/ew",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/google/obf/ex;


# instance fields
.field private final b:Lcom/google/obf/eg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/google/obf/fv$1;

    invoke-direct {v0}, Lcom/google/obf/fv$1;-><init>()V

    sput-object v0, Lcom/google/obf/fv;->a:Lcom/google/obf/ex;

    return-void
.end method

.method constructor <init>(Lcom/google/obf/eg;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/obf/ew;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/obf/fv;->b:Lcom/google/obf/eg;

    .line 3
    return-void
.end method


# virtual methods
.method public read(Lcom/google/obf/ge;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4
    invoke-virtual {p1}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    move-result-object v0

    .line 5
    sget-object v1, Lcom/google/obf/fv$2;->a:[I

    invoke-virtual {v0}, Lcom/google/obf/gf;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 23
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 6
    :pswitch_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 7
    invoke-virtual {p1}, Lcom/google/obf/ge;->a()V

    .line 8
    :goto_0
    invoke-virtual {p1}, Lcom/google/obf/ge;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9
    invoke-virtual {p0, p1}, Lcom/google/obf/fv;->read(Lcom/google/obf/ge;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 10
    :cond_0
    invoke-virtual {p1}, Lcom/google/obf/ge;->b()V

    .line 22
    :goto_1
    return-object v0

    .line 12
    :pswitch_1
    new-instance v0, Lcom/google/obf/fj;

    invoke-direct {v0}, Lcom/google/obf/fj;-><init>()V

    .line 13
    invoke-virtual {p1}, Lcom/google/obf/ge;->c()V

    .line 14
    :goto_2
    invoke-virtual {p1}, Lcom/google/obf/ge;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 15
    invoke-virtual {p1}, Lcom/google/obf/ge;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/google/obf/fv;->read(Lcom/google/obf/ge;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 16
    :cond_1
    invoke-virtual {p1}, Lcom/google/obf/ge;->d()V

    goto :goto_1

    .line 18
    :pswitch_2
    invoke-virtual {p1}, Lcom/google/obf/ge;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 19
    :pswitch_3
    invoke-virtual {p1}, Lcom/google/obf/ge;->k()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_1

    .line 20
    :pswitch_4
    invoke-virtual {p1}, Lcom/google/obf/ge;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    .line 21
    :pswitch_5
    invoke-virtual {p1}, Lcom/google/obf/ge;->j()V

    .line 22
    const/4 v0, 0x0

    goto :goto_1

    .line 5
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public write(Lcom/google/obf/gg;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    if-nez p2, :cond_0

    .line 25
    invoke-virtual {p1}, Lcom/google/obf/gg;->f()Lcom/google/obf/gg;

    .line 33
    :goto_0
    return-void

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/google/obf/fv;->b:Lcom/google/obf/eg;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/obf/eg;->a(Ljava/lang/Class;)Lcom/google/obf/ew;

    move-result-object v0

    .line 28
    instance-of v1, v0, Lcom/google/obf/fv;

    if-eqz v1, :cond_1

    .line 29
    invoke-virtual {p1}, Lcom/google/obf/gg;->d()Lcom/google/obf/gg;

    .line 30
    invoke-virtual {p1}, Lcom/google/obf/gg;->e()Lcom/google/obf/gg;

    goto :goto_0

    .line 32
    :cond_1
    invoke-virtual {v0, p1, p2}, Lcom/google/obf/ew;->write(Lcom/google/obf/gg;Ljava/lang/Object;)V

    goto :goto_0
.end method
