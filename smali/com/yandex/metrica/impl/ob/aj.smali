.class public Lcom/yandex/metrica/impl/ob/aj;
.super Lcom/yandex/metrica/impl/ob/ah;
.source "SourceFile"


# instance fields
.field private a:Lcom/yandex/metrica/impl/ob/db;

.field private b:Lcom/yandex/metrica/impl/utils/n;


# direct methods
.method public constructor <init>(Lcom/yandex/metrica/impl/ob/v;)V
    .locals 2

    .prologue
    .line 30
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->D()Lcom/yandex/metrica/impl/ob/db;

    move-result-object v0

    invoke-static {}, Lcom/yandex/metrica/impl/utils/n;->a()Lcom/yandex/metrica/impl/utils/n;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/yandex/metrica/impl/ob/aj;-><init>(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/db;Lcom/yandex/metrica/impl/utils/n;)V

    .line 31
    return-void
.end method

.method constructor <init>(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/db;Lcom/yandex/metrica/impl/utils/n;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/ah;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    .line 37
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/aj;->a:Lcom/yandex/metrica/impl/ob/db;

    .line 38
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/aj;->b:Lcom/yandex/metrica/impl/utils/n;

    .line 39
    return-void
.end method


# virtual methods
.method public a(Lcom/yandex/metrica/impl/i;)Z
    .locals 3

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/aj;->a()Lcom/yandex/metrica/impl/ob/v;

    move-result-object v0

    .line 47
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/aj;->a:Lcom/yandex/metrica/impl/ob/db;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/db;->d()Z

    move-result v1

    if-nez v1, :cond_2

    .line 49
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/aj;->a:Lcom/yandex/metrica/impl/ob/db;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/db;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 50
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/v;->k()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/metrica/CounterConfiguration;->y()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/aj;->b:Lcom/yandex/metrica/impl/utils/n;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/v;->n()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/utils/n;->a(Landroid/content/Context;)V

    .line 52
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/aj;->b:Lcom/yandex/metrica/impl/utils/n;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/utils/n;->c()V

    .line 55
    :cond_0
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->b()Ljava/lang/String;

    move-result-object v1

    .line 56
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aj;->a:Lcom/yandex/metrica/impl/ob/db;

    invoke-virtual {v2, v1}, Lcom/yandex/metrica/impl/ob/db;->c(Ljava/lang/String;)V

    .line 1414
    sget-object v2, Lcom/yandex/metrica/impl/q$a;->A:Lcom/yandex/metrica/impl/q$a;

    invoke-static {p1, v2}, Lcom/yandex/metrica/impl/i;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/q$a;)Lcom/yandex/metrica/impl/i;

    move-result-object v2

    .line 57
    invoke-virtual {v2, v1}, Lcom/yandex/metrica/impl/i;->c(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/v;->d(Lcom/yandex/metrica/impl/i;)V

    .line 58
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/v;->b(Z)V

    .line 59
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/aj;->a:Lcom/yandex/metrica/impl/ob/db;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/v;->k()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->w()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/ob/db;->a(Z)V

    .line 61
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/aj;->a:Lcom/yandex/metrica/impl/ob/db;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/db;->b()V

    .line 63
    :cond_2
    const/4 v0, 0x0

    return v0
.end method
