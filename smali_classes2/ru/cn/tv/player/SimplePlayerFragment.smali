.class public Lru/cn/tv/player/SimplePlayerFragment;
.super Landroid/support/v4/app/Fragment;
.source "SimplePlayerFragment.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lru/cn/ad/AdPlayController$Listener;
.implements Lru/cn/ad/ContentProgress;
.implements Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;
.implements Lru/cn/player/timeshift/StreamTimeshifter$Callback;
.implements Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;,
        Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private final MINIMUM_TIME_SHIFT_OFFSET:I

.field protected adController:Lru/cn/ad/AdPlayController;

.field private aspectRatio:F

.field private castListener:Lcom/google/android/gms/cast/Cast$Listener;

.field private castSession:Lcom/google/android/gms/cast/framework/CastSession;

.field private channelLocations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation
.end field

.field private channelLocationsCurrentItem:I

.field private context:Landroid/content/Context;

.field private continuePlaybackAfterAd:Z

.field private contractorId:J

.field private cropX:I

.field private cropY:I

.field private currentChannelId:J

.field protected currentChannelTitle:Ljava/lang/String;

.field private currentSource:Ljava/lang/String;

.field protected currentTelecastDate:Ljava/util/Calendar;

.field private currentTelecastEnd:Ljava/util/Calendar;

.field private currentTelecastId:J

.field private hasSchedule:I

.field private isInPlayingState:Z

.field private isIntersections:Z

.field private isPorno:Z

.field protected listener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

.field private locationTerritoryId:J

.field private mHandler:Landroid/os/Handler;

.field private onAirOrTimeshift:Z

.field private playerCallbacks:Lru/cn/player/SimplePlayer$Listener;

.field private playerController:Lru/cn/tv/player/controller/PlayerController;

.field protected playerView:Lru/cn/player/SimplePlayer;

.field protected playingAd:Z

.field private progressBar:Landroid/widget/ProgressBar;

.field private remoteController:Lru/cn/peersay/controllers/PlayerRemoteController;

.field private startOverCorrection:J

.field private stoppedPosition:I

.field private timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

.field private timeShiftOffset:I

.field private timeshifter:Lru/cn/player/timeshift/StreamTimeshifter;

.field private viewModel:Lru/cn/tv/player/PlaybackViewModel;

.field private waitingTelecastInfoWithoutLocations:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const-class v0, Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lru/cn/tv/player/SimplePlayerFragment;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 66
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 86
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x3c

    :goto_0
    iput v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->MINIMUM_TIME_SHIFT_OFFSET:I

    .line 94
    iput-boolean v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->playingAd:Z

    .line 97
    iput-boolean v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->continuePlaybackAfterAd:Z

    .line 109
    iput-boolean v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    .line 115
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastId:J

    .line 118
    iput v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->hasSchedule:I

    .line 125
    iput v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeShiftOffset:I

    .line 126
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->startOverCorrection:J

    .line 135
    iput v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocationsCurrentItem:I

    .line 484
    new-instance v0, Lru/cn/tv/player/SimplePlayerFragment$1;

    invoke-direct {v0, p0}, Lru/cn/tv/player/SimplePlayerFragment$1;-><init>(Lru/cn/tv/player/SimplePlayerFragment;)V

    iput-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerCallbacks:Lru/cn/player/SimplePlayer$Listener;

    .line 1186
    new-instance v0, Lru/cn/tv/player/SimplePlayerFragment$4;

    invoke-direct {v0, p0}, Lru/cn/tv/player/SimplePlayerFragment$4;-><init>(Lru/cn/tv/player/SimplePlayerFragment;)V

    iput-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->castListener:Lcom/google/android/gms/cast/Cast$Listener;

    return-void

    .line 86
    :cond_0
    const/16 v0, 0xf

    goto :goto_0
.end method

.method private _loadFitMode()V
    .locals 10

    .prologue
    .line 1421
    iget-wide v6, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_1

    .line 1422
    iget-object v5, p0, Lru/cn/tv/player/SimplePlayerFragment;->context:Landroid/content/Context;

    const-string v6, "SimplePlayerFragment_fitMode"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 1425
    .local v3, "preferences":Landroid/content/SharedPreferences;
    iget-object v5, p0, Lru/cn/tv/player/SimplePlayerFragment;->context:Landroid/content/Context;

    invoke-virtual {p0, v5}, Lru/cn/tv/player/SimplePlayerFragment;->getDefaultFitMode(Landroid/content/Context;)Lru/cn/player/SimplePlayer$FitMode;

    move-result-object v1

    .line 1426
    .local v1, "f":Lru/cn/player/SimplePlayer$FitMode;
    iget-wide v6, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1427
    .local v2, "fitMode":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 1429
    :try_start_0
    invoke-static {v2}, Lru/cn/player/SimplePlayer$FitMode;->valueOf(Ljava/lang/String;)Lru/cn/player/SimplePlayer$FitMode;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1441
    :cond_0
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v6, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_zoom"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v4

    .line 1442
    .local v4, "zoom":F
    iget-object v5, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v5, v1}, Lru/cn/player/SimplePlayer;->setFitMode(Lru/cn/player/SimplePlayer$FitMode;)V

    .line 1443
    iget-object v5, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v5, v4}, Lru/cn/player/SimplePlayer;->setZoom(F)V

    .line 1445
    .end local v1    # "f":Lru/cn/player/SimplePlayer$FitMode;
    .end local v2    # "fitMode":Ljava/lang/String;
    .end local v3    # "preferences":Landroid/content/SharedPreferences;
    .end local v4    # "zoom":F
    :cond_1
    return-void

    .line 1430
    .restart local v1    # "f":Lru/cn/player/SimplePlayer$FitMode;
    .restart local v2    # "fitMode":Ljava/lang/String;
    .restart local v3    # "preferences":Landroid/content/SharedPreferences;
    :catch_0
    move-exception v0

    .line 1432
    .local v0, "e":Ljava/lang/Exception;
    sget-object v5, Lru/cn/tv/player/SimplePlayerFragment;->LOG_TAG:Ljava/lang/String;

    const-string v6, "Unsupported fit-mode"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1435
    const-string v5, "NONE"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1436
    sget-object v1, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_USER:Lru/cn/player/SimplePlayer$FitMode;

    goto :goto_0
.end method

.method private _playContent()V
    .locals 3

    .prologue
    .line 1379
    sget-object v0, Lru/cn/tv/player/SimplePlayerFragment;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Try play "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentSource:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lru/cn/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1381
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

    if-nez v0, :cond_2

    .line 1382
    new-instance v0, Lru/cn/domain/statistics/inetra/TimeMeasure;

    invoke-direct {v0}, Lru/cn/domain/statistics/inetra/TimeMeasure;-><init>()V

    iput-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

    .line 1383
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentSource:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lru/cn/domain/statistics/inetra/TimeMeasure;->measureStart(Ljava/lang/String;)V

    .line 1388
    :goto_0
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->remoteController:Lru/cn/peersay/controllers/PlayerRemoteController;

    if-eqz v0, :cond_0

    .line 1389
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->remoteController:Lru/cn/peersay/controllers/PlayerRemoteController;

    invoke-virtual {v0, p0}, Lru/cn/peersay/controllers/PlayerRemoteController;->setPlayer(Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;)V

    .line 1392
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    iget v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->aspectRatio:F

    invoke-virtual {v0, v1}, Lru/cn/player/SimplePlayer;->setAspectRatio(F)V

    .line 1393
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    iget v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->cropX:I

    iget v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->cropY:I

    invoke-virtual {v0, v1, v2}, Lru/cn/player/SimplePlayer;->setAutoCrop(II)V

    .line 1395
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentSource:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lru/cn/player/SimplePlayer;->play(Ljava/lang/String;)V

    .line 1397
    iget v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->stoppedPosition:I

    if-lez v0, :cond_1

    .line 1398
    iget v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->stoppedPosition:I

    invoke-virtual {p0, v0}, Lru/cn/tv/player/SimplePlayerFragment;->seekTo(I)V

    .line 1400
    :cond_1
    return-void

    .line 1385
    :cond_2
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentSource:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lru/cn/domain/statistics/inetra/TimeMeasure;->measureContinue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private _playCurrentChannel()V
    .locals 18

    .prologue
    .line 321
    invoke-virtual/range {p0 .. p0}, Lru/cn/tv/player/SimplePlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    if-nez v3, :cond_1

    .line 378
    :cond_0
    :goto_0
    return-void

    .line 325
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocations:Ljava/util/List;

    if-eqz v3, :cond_2

    invoke-virtual/range {p0 .. p0}, Lru/cn/tv/player/SimplePlayerFragment;->isResumed()Z

    move-result v3

    if-nez v3, :cond_3

    .line 326
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lru/cn/tv/player/SimplePlayerFragment;->viewModel:Lru/cn/tv/player/PlaybackViewModel;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    invoke-virtual {v3, v4, v5}, Lru/cn/tv/player/PlaybackViewModel;->loadChannelLocations(J)V

    goto :goto_0

    .line 330
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocationsCurrentItem:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocations:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-gt v3, v4, :cond_0

    .line 334
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    .line 335
    move-object/from16 v0, p0

    iget-object v3, v0, Lru/cn/tv/player/SimplePlayerFragment;->timeshifter:Lru/cn/player/timeshift/StreamTimeshifter;

    if-eqz v3, :cond_4

    .line 336
    move-object/from16 v0, p0

    iget-object v3, v0, Lru/cn/tv/player/SimplePlayerFragment;->timeshifter:Lru/cn/player/timeshift/StreamTimeshifter;

    invoke-virtual {v3}, Lru/cn/player/timeshift/StreamTimeshifter;->cancelRequests()V

    .line 337
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lru/cn/tv/player/SimplePlayerFragment;->timeshifter:Lru/cn/player/timeshift/StreamTimeshifter;

    .line 340
    :cond_4
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lru/cn/tv/player/SimplePlayerFragment;->timeShiftOffset:I

    .line 341
    const-wide/16 v4, 0x0

    move-object/from16 v0, p0

    iput-wide v4, v0, Lru/cn/tv/player/SimplePlayerFragment;->startOverCorrection:J

    .line 343
    move-object/from16 v0, p0

    iget-object v3, v0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocations:Ljava/util/List;

    move-object/from16 v0, p0

    iget v4, v0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocationsCurrentItem:I

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/api/iptv/replies/MediaLocation;

    .line 344
    .local v2, "location":Lru/cn/api/iptv/replies/MediaLocation;
    iget-wide v4, v2, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lru/cn/tv/player/SimplePlayerFragment;->_setContractorId(J)V

    .line 345
    iget-wide v4, v2, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    move-object/from16 v0, p0

    iput-wide v4, v0, Lru/cn/tv/player/SimplePlayerFragment;->locationTerritoryId:J

    .line 347
    move-object/from16 v0, p0

    iget-object v3, v0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocations:Ljava/util/List;

    invoke-static {v2, v3}, Lru/cn/tv/player/SimplePlayerFragment;->timeshiftLocation(Lru/cn/api/iptv/replies/MediaLocation;Ljava/util/List;)Lru/cn/api/iptv/replies/MediaLocation;

    move-result-object v11

    .line 348
    .local v11, "timeshiftLocation":Lru/cn/api/iptv/replies/MediaLocation;
    if-eqz v11, :cond_5

    .line 349
    new-instance v3, Lru/cn/player/timeshift/StreamTimeshifter;

    move-object/from16 v0, p0

    invoke-direct {v3, v11, v0}, Lru/cn/player/timeshift/StreamTimeshifter;-><init>(Lru/cn/api/iptv/replies/MediaLocation;Lru/cn/player/timeshift/StreamTimeshifter$Callback;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lru/cn/tv/player/SimplePlayerFragment;->timeshifter:Lru/cn/player/timeshift/StreamTimeshifter;

    .line 352
    :cond_5
    iget-object v7, v2, Lru/cn/api/iptv/replies/MediaLocation;->location:Ljava/lang/String;

    .line 353
    .local v7, "source":Ljava/lang/String;
    const/4 v3, 0x1

    iget-wide v4, v2, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    iget-object v6, v2, Lru/cn/api/iptv/replies/MediaLocation;->sourceUri:Ljava/lang/String;

    iget-wide v14, v2, Lru/cn/api/iptv/replies/MediaLocation;->channelId:J

    const-wide/16 v16, 0x0

    cmp-long v8, v14, v16

    if-nez v8, :cond_8

    iget-object v8, v2, Lru/cn/api/iptv/replies/MediaLocation;->title:Ljava/lang/String;

    :goto_1
    invoke-static/range {v3 .. v8}, Lru/cn/domain/statistics/inetra/InetraTracker;->createTrackingSession(IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    iget-object v3, v2, Lru/cn/api/iptv/replies/MediaLocation;->type:Lru/cn/api/iptv/replies/MediaLocation$Type;

    sget-object v4, Lru/cn/api/iptv/replies/MediaLocation$Type;->mcastudp:Lru/cn/api/iptv/replies/MediaLocation$Type;

    if-ne v3, v4, :cond_7

    .line 358
    new-instance v10, Lru/cn/domain/UDPProxySettings;

    invoke-virtual/range {p0 .. p0}, Lru/cn/tv/player/SimplePlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v10, v3}, Lru/cn/domain/UDPProxySettings;-><init>(Landroid/content/Context;)V

    .line 359
    .local v10, "settings":Lru/cn/domain/UDPProxySettings;
    invoke-virtual {v10}, Lru/cn/domain/UDPProxySettings;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 360
    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 362
    .local v9, "mcastUri":Landroid/net/Uri;
    const-string v12, "%s:%d/udp/%s:%d"

    .line 363
    .local v12, "uriFormat":Ljava/lang/String;
    invoke-virtual {v10}, Lru/cn/domain/UDPProxySettings;->getAddress()Ljava/lang/String;

    move-result-object v3

    const-string v4, "http://"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 364
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "http://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 367
    :cond_6
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v10}, Lru/cn/domain/UDPProxySettings;->getAddress()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    .line 368
    invoke-virtual {v10}, Lru/cn/domain/UDPProxySettings;->getPort()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {v9}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-virtual {v9}, Landroid/net/Uri;->getPort()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 367
    invoke-static {v3, v12, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 372
    .end local v9    # "mcastUri":Landroid/net/Uri;
    .end local v10    # "settings":Lru/cn/domain/UDPProxySettings;
    .end local v12    # "uriFormat":Ljava/lang/String;
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lru/cn/tv/player/SimplePlayerFragment;->adController:Lru/cn/ad/AdPlayController;

    iget-object v4, v2, Lru/cn/api/iptv/replies/MediaLocation;->adTags:Ljava/util/List;

    invoke-virtual {v3, v4}, Lru/cn/ad/AdPlayController;->setMetaTags(Ljava/util/List;)V

    .line 374
    invoke-virtual {v2}, Lru/cn/api/iptv/replies/MediaLocation;->getAspectRatio()F

    move-result v3

    iget v4, v2, Lru/cn/api/iptv/replies/MediaLocation;->cropX:I

    iget v5, v2, Lru/cn/api/iptv/replies/MediaLocation;->cropY:I

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v3, v4, v5}, Lru/cn/tv/player/SimplePlayerFragment;->setSource(Ljava/lang/String;FII)V

    .line 375
    invoke-direct/range {p0 .. p0}, Lru/cn/tv/player/SimplePlayerFragment;->playSource()V

    .line 377
    move-object/from16 v0, p0

    iget-object v3, v0, Lru/cn/tv/player/SimplePlayerFragment;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 353
    :cond_8
    const/4 v8, 0x0

    goto/16 :goto_1
.end method

.method private _playCurrentTelecast()V
    .locals 4

    .prologue
    .line 399
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 408
    :goto_0
    return-void

    .line 403
    :cond_0
    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    if-nez v0, :cond_1

    iget-wide v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 404
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->viewModel:Lru/cn/tv/player/PlaybackViewModel;

    iget-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastId:J

    invoke-virtual {v0, v2, v3}, Lru/cn/tv/player/PlaybackViewModel;->loadTelecastLocations(J)V

    .line 407
    :cond_1
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method private _saveFitMode()V
    .locals 8

    .prologue
    .line 1403
    iget-wide v4, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_1

    .line 1404
    iget-object v4, p0, Lru/cn/tv/player/SimplePlayerFragment;->context:Landroid/content/Context;

    const-string v5, "SimplePlayerFragment_fitMode"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 1406
    .local v2, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1407
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    iget-object v4, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v4}, Lru/cn/player/SimplePlayer;->getFitMode()Lru/cn/player/SimplePlayer$FitMode;

    move-result-object v1

    .line 1408
    .local v1, "mode":Lru/cn/player/SimplePlayer$FitMode;
    iget-wide v4, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lru/cn/player/SimplePlayer$FitMode;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1410
    sget-object v4, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_USER:Lru/cn/player/SimplePlayer$FitMode;

    if-ne v1, v4, :cond_0

    .line 1411
    iget-object v4, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v4}, Lru/cn/player/SimplePlayer;->getUserZoom()F

    move-result v3

    .line 1412
    .local v3, "zoom":F
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v6, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_zoom"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 1416
    .end local v3    # "zoom":F
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1418
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "mode":Lru/cn/player/SimplePlayer$FitMode;
    .end local v2    # "preferences":Landroid/content/SharedPreferences;
    :cond_1
    return-void
.end method

.method private _setChannelId(J)V
    .locals 5
    .param p1, "channelId"    # J

    .prologue
    const/4 v2, 0x0

    .line 427
    iget-wide v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_1

    .line 428
    iput-wide p1, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    .line 429
    iput-object v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelTitle:Ljava/lang/String;

    .line 430
    iput-object v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocations:Ljava/util/List;

    .line 433
    iget-wide v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 434
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->viewModel:Lru/cn/tv/player/PlaybackViewModel;

    invoke-virtual {v0, p1, p2}, Lru/cn/tv/player/PlaybackViewModel;->loadChannel(J)V

    .line 437
    :cond_0
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->_loadFitMode()V

    .line 438
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->listener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

    if-eqz v0, :cond_1

    .line 439
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->listener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

    invoke-interface {v0, p1, p2}, Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;->channelChanged(J)V

    .line 442
    :cond_1
    return-void
.end method

.method private _setContractorId(J)V
    .locals 3
    .param p1, "contractorId"    # J

    .prologue
    .line 475
    iget-wide v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->contractorId:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    .line 477
    iput-wide p1, p0, Lru/cn/tv/player/SimplePlayerFragment;->contractorId:J

    .line 478
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->listener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

    if-eqz v0, :cond_0

    .line 479
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->listener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

    invoke-interface {v0, p1, p2}, Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;->contractorChanged(J)V

    .line 482
    :cond_0
    return-void
.end method

.method private _setTelecastId(J)V
    .locals 7
    .param p1, "telecastId"    # J

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x1

    const-wide/16 v4, 0x0

    .line 445
    iget-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastId:J

    cmp-long v1, v2, p1

    if-eqz v1, :cond_2

    .line 447
    iget-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastId:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    cmp-long v1, p1, v4

    if-lez v1, :cond_0

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v1}, Lru/cn/player/SimplePlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    if-eqz v1, :cond_0

    .line 449
    const/16 v1, 0x3e9

    invoke-static {v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->watch(I)V

    .line 452
    invoke-static {}, Lru/cn/domain/statistics/inetra/InetraTracker;->getViewFrom()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 451
    invoke-static {v1, v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->setSessionParams(II)V

    .line 455
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->context:Landroid/content/Context;

    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v2}, Lru/cn/player/SimplePlayer;->getPlayerType()I

    move-result v2

    invoke-static {v1, p1, p2, v2}, Lru/cn/domain/statistics/inetra/InetraTracker;->telecastStart(Landroid/content/Context;JI)V

    .line 458
    :cond_0
    iput-wide p1, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastId:J

    .line 459
    iput-object v6, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastDate:Ljava/util/Calendar;

    .line 460
    iput-object v6, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastEnd:Ljava/util/Calendar;

    .line 462
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->viewModel:Lru/cn/tv/player/PlaybackViewModel;

    iget-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    :goto_0
    invoke-virtual {v1, p1, p2, v0}, Lru/cn/tv/player/PlaybackViewModel;->loadTelecast(JZ)V

    .line 464
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->listener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

    if-eqz v0, :cond_1

    .line 465
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->listener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

    invoke-interface {v0, p1, p2}, Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;->telecastChanged(J)V

    .line 468
    :cond_1
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerController:Lru/cn/tv/player/controller/PlayerController;

    if-eqz v0, :cond_2

    .line 469
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerController:Lru/cn/tv/player/controller/PlayerController;

    invoke-virtual {v0, p1, p2}, Lru/cn/tv/player/controller/PlayerController;->setTelecast(J)V

    .line 472
    :cond_2
    return-void

    .line 462
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lru/cn/tv/player/SimplePlayerFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentSource:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/tv/player/SimplePlayerFragment;)Lru/cn/tv/player/controller/PlayerController;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerController:Lru/cn/tv/player/controller/PlayerController;

    return-object v0
.end method

.method static synthetic access$1000(Lru/cn/tv/player/SimplePlayerFragment;)I
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragment;

    .prologue
    .line 66
    iget v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocationsCurrentItem:I

    return v0
.end method

.method static synthetic access$1004(Lru/cn/tv/player/SimplePlayerFragment;)I
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragment;

    .prologue
    .line 66
    iget v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocationsCurrentItem:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocationsCurrentItem:I

    return v0
.end method

.method static synthetic access$1100(Lru/cn/tv/player/SimplePlayerFragment;II)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lru/cn/tv/player/SimplePlayerFragment;->showErrorDialog(II)V

    return-void
.end method

.method static synthetic access$1200(Lru/cn/tv/player/SimplePlayerFragment;)Lru/cn/domain/statistics/inetra/TimeMeasure;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

    return-object v0
.end method

.method static synthetic access$1202(Lru/cn/tv/player/SimplePlayerFragment;Lru/cn/domain/statistics/inetra/TimeMeasure;)Lru/cn/domain/statistics/inetra/TimeMeasure;
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragment;
    .param p1, "x1"    # Lru/cn/domain/statistics/inetra/TimeMeasure;

    .prologue
    .line 66
    iput-object p1, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

    return-object p1
.end method

.method static synthetic access$1300(Lru/cn/tv/player/SimplePlayerFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1400(Lru/cn/tv/player/SimplePlayerFragment;)I
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragment;

    .prologue
    .line 66
    iget v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->hasSchedule:I

    return v0
.end method

.method static synthetic access$1500(Lru/cn/tv/player/SimplePlayerFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1600(Lru/cn/tv/player/SimplePlayerFragment;)Lru/cn/peersay/controllers/PlayerRemoteController;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->remoteController:Lru/cn/peersay/controllers/PlayerRemoteController;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/tv/player/SimplePlayerFragment;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->progressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/tv/player/SimplePlayerFragment;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragment;

    .prologue
    .line 66
    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->isInPlayingState:Z

    return v0
.end method

.method static synthetic access$302(Lru/cn/tv/player/SimplePlayerFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lru/cn/tv/player/SimplePlayerFragment;->isInPlayingState:Z

    return p1
.end method

.method static synthetic access$400(Lru/cn/tv/player/SimplePlayerFragment;)J
    .locals 2
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragment;

    .prologue
    .line 66
    iget-wide v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    return-wide v0
.end method

.method static synthetic access$500(Lru/cn/tv/player/SimplePlayerFragment;)J
    .locals 2
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragment;

    .prologue
    .line 66
    iget-wide v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastId:J

    return-wide v0
.end method

.method static synthetic access$600(Lru/cn/tv/player/SimplePlayerFragment;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragment;

    .prologue
    .line 66
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->playSource()V

    return-void
.end method

.method static synthetic access$700(Lru/cn/tv/player/SimplePlayerFragment;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragment;

    .prologue
    .line 66
    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    return v0
.end method

.method static synthetic access$800(Lru/cn/tv/player/SimplePlayerFragment;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragment;

    .prologue
    .line 66
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocations:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$900(Lru/cn/tv/player/SimplePlayerFragment;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragment;

    .prologue
    .line 66
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->_playCurrentChannel()V

    return-void
.end method

.method private goToAir()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 913
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->stop()V

    .line 914
    const/16 v0, 0x3e8

    invoke-static {v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->watch(I)V

    .line 916
    iput v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocationsCurrentItem:I

    .line 918
    invoke-static {}, Lru/cn/domain/statistics/inetra/InetraTracker;->getViewFrom()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 917
    invoke-static {v0, v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->setSessionParams(II)V

    .line 922
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocations:Ljava/util/List;

    if-nez v0, :cond_0

    .line 923
    iget-wide v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    invoke-virtual {p0, v0, v1}, Lru/cn/tv/player/SimplePlayerFragment;->playChannel(J)V

    .line 927
    :goto_0
    return-void

    .line 925
    :cond_0
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->_playCurrentChannel()V

    goto :goto_0
.end method

.method private loadDeniedLocation(JJ)V
    .locals 3
    .param p1, "channelId"    # J
    .param p3, "contractorId"    # J

    .prologue
    .line 1121
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1122
    invoke-virtual {p0, p1, p2, p3, p4}, Lru/cn/tv/player/SimplePlayerFragment;->onLoadDeniedLocation(JJ)V

    .line 1123
    return-void
.end method

.method private parseAdTags(Landroid/database/Cursor;)Ljava/util/List;
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1653
    const-string v2, "ad_tags"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1654
    .local v0, "formattedTags":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1655
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1658
    :goto_0
    return-object v2

    .line 1657
    :cond_0
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1658
    .local v1, "tags":[Ljava/lang/String;
    invoke-static {v1}, Lcom/annimon/stream/Stream;->of([Ljava/lang/Object;)Lcom/annimon/stream/Stream;

    move-result-object v2

    sget-object v3, Lru/cn/tv/player/SimplePlayerFragment$$Lambda$5;->$instance:Lcom/annimon/stream/function/Function;

    .line 1659
    invoke-virtual {v2, v3}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v2

    .line 1660
    invoke-virtual {v2}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object v2

    goto :goto_0
.end method

.method private playSource()V
    .locals 4

    .prologue
    .line 1358
    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v2}, Lru/cn/player/SimplePlayer;->getPlayerType()I

    move-result v2

    const/16 v3, 0x69

    if-ne v2, v3, :cond_1

    const/4 v0, 0x1

    .line 1359
    .local v0, "isChromecast":Z
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->getPlaybackMode()Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    move-result-object v2

    sget-object v3, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;->ON_AIR:Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    if-ne v2, v3, :cond_2

    .line 1360
    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocations:Ljava/util/List;

    iget v3, p0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocationsCurrentItem:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/iptv/replies/MediaLocation;

    .line 1361
    .local v1, "location":Lru/cn/api/iptv/replies/MediaLocation;
    iget-object v2, v1, Lru/cn/api/iptv/replies/MediaLocation;->type:Lru/cn/api/iptv/replies/MediaLocation$Type;

    sget-object v3, Lru/cn/api/iptv/replies/MediaLocation$Type;->magnet:Lru/cn/api/iptv/replies/MediaLocation$Type;

    if-ne v2, v3, :cond_2

    .line 1362
    iget v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocationsCurrentItem:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocationsCurrentItem:I

    .line 1363
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->_playCurrentChannel()V

    .line 1376
    .end local v1    # "location":Lru/cn/api/iptv/replies/MediaLocation;
    :cond_0
    :goto_1
    return-void

    .line 1358
    .end local v0    # "isChromecast":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1368
    .restart local v0    # "isChromecast":Z
    :cond_2
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->isResumed()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1369
    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->castSession:Lcom/google/android/gms/cast/framework/CastSession;

    if-nez v2, :cond_3

    .line 1370
    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->adController:Lru/cn/ad/AdPlayController;

    invoke-virtual {v2}, Lru/cn/ad/AdPlayController;->startPreRoll()V

    goto :goto_1

    .line 1374
    :cond_3
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->_playContent()V

    goto :goto_1
.end method

.method private setChannelInfo(Landroid/database/Cursor;)V
    .locals 13
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v12, 0x0

    const/4 v9, 0x1

    .line 1478
    sget-object v1, Lru/cn/tv/player/SimplePlayerFragment;->LOG_TAG:Ljava/lang/String;

    const-string v4, "load channel info"

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1480
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 1481
    check-cast p1, Landroid/database/CursorWrapper;

    .line 1482
    .end local p1    # "cursor":Landroid/database/Cursor;
    invoke-virtual {p1}, Landroid/database/CursorWrapper;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/cursor/ChannelCursor;

    .line 1483
    .local v0, "c":Lru/cn/api/provider/cursor/ChannelCursor;
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->moveToFirst()Z

    .line 1485
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getChannelId()J

    move-result-wide v2

    .line 1486
    .local v2, "channelId":J
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getFavourite()I

    move-result v1

    if-ne v1, v9, :cond_3

    move v5, v9

    .line 1487
    .local v5, "isFavourite":Z
    :goto_0
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getTitle()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelTitle:Ljava/lang/String;

    .line 1488
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getIsPorno()I

    move-result v1

    if-ne v1, v9, :cond_4

    move v1, v9

    :goto_1
    iput-boolean v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->isPorno:Z

    .line 1489
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getHasSchedule()I

    move-result v1

    iput v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->hasSchedule:I

    .line 1490
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getIsIntersections()I

    move-result v1

    if-ne v1, v9, :cond_5

    move v1, v9

    :goto_2
    iput-boolean v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->isIntersections:Z

    .line 1491
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getIsDenied()Z

    move-result v7

    .line 1492
    .local v7, "isDenied":Z
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getNumber()I

    move-result v6

    .line 1494
    .local v6, "channelNumber":I
    iget-object v4, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelTitle:Ljava/lang/String;

    iget-boolean v8, p0, Lru/cn/tv/player/SimplePlayerFragment;->isIntersections:Z

    move-object v1, p0

    invoke-virtual/range {v1 .. v8}, Lru/cn/tv/player/SimplePlayerFragment;->channelInfoLoaded(JLjava/lang/String;ZIZZ)V

    .line 1496
    invoke-direct {p0, v2, v3}, Lru/cn/tv/player/SimplePlayerFragment;->_setChannelId(J)V

    .line 1498
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->listener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

    iget v4, p0, Lru/cn/tv/player/SimplePlayerFragment;->hasSchedule:I

    if-ne v4, v9, :cond_0

    move v12, v9

    :cond_0
    invoke-interface {v1, v12}, Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;->hasSchedule(Z)V

    .line 1500
    iget v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->hasSchedule:I

    if-ne v1, v9, :cond_2

    .line 1501
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->getPlaybackMode()Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    move-result-object v1

    sget-object v4, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;->ON_AIR:Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    if-ne v1, v4, :cond_2

    .line 1502
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getCurrentTelecastId()J

    move-result-wide v10

    .line 1503
    .local v10, "telecastId":J
    iget-wide v8, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastId:J

    cmp-long v1, v10, v8

    if-nez v1, :cond_1

    .line 1504
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->waitForTelecastChange()V

    .line 1507
    :cond_1
    invoke-direct {p0, v10, v11}, Lru/cn/tv/player/SimplePlayerFragment;->_setTelecastId(J)V

    .line 1514
    .end local v0    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    .end local v2    # "channelId":J
    .end local v5    # "isFavourite":Z
    .end local v6    # "channelNumber":I
    .end local v7    # "isDenied":Z
    .end local v10    # "telecastId":J
    :cond_2
    return-void

    .restart local v0    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    .restart local v2    # "channelId":J
    :cond_3
    move v5, v12

    .line 1486
    goto :goto_0

    .restart local v5    # "isFavourite":Z
    :cond_4
    move v1, v12

    .line 1488
    goto :goto_1

    :cond_5
    move v1, v12

    .line 1490
    goto :goto_2
.end method

.method private setChannelLocations(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "locations":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/iptv/replies/MediaLocation;>;"
    const/4 v4, 0x0

    .line 1517
    sget-object v1, Lru/cn/tv/player/SimplePlayerFragment;->LOG_TAG:Ljava/lang/String;

    const-string v2, "load channel location"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1519
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1520
    iput-object p1, p0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocations:Ljava/util/List;

    .line 1521
    iput v4, p0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocationsCurrentItem:I

    .line 1523
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocations:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/iptv/replies/MediaLocation;

    .line 1524
    .local v0, "defaultLocation":Lru/cn/api/iptv/replies/MediaLocation;
    iget-object v1, v0, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;

    sget-object v2, Lru/cn/api/iptv/replies/MediaLocation$Access;->denied:Lru/cn/api/iptv/replies/MediaLocation$Access;

    if-ne v1, v2, :cond_1

    .line 1525
    iget-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocations:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/iptv/replies/MediaLocation;

    iget-wide v4, v1, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    invoke-direct {p0, v2, v3, v4, v5}, Lru/cn/tv/player/SimplePlayerFragment;->loadDeniedLocation(JJ)V

    .line 1536
    .end local v0    # "defaultLocation":Lru/cn/api/iptv/replies/MediaLocation;
    :cond_0
    :goto_0
    return-void

    .line 1527
    .restart local v0    # "defaultLocation":Lru/cn/api/iptv/replies/MediaLocation;
    :cond_1
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1528
    iget-object v1, v0, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;

    sget-object v2, Lru/cn/api/iptv/replies/MediaLocation$Access;->purchased:Lru/cn/api/iptv/replies/MediaLocation$Access;

    if-ne v1, v2, :cond_2

    iget-boolean v1, v0, Lru/cn/api/iptv/replies/MediaLocation;->noAdsPartner:Z

    if-eqz v1, :cond_2

    .line 1529
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->adController:Lru/cn/ad/AdPlayController;

    invoke-virtual {v1, v4}, Lru/cn/ad/AdPlayController;->setAdsEnabled(Z)V

    .line 1532
    :cond_2
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->_playCurrentChannel()V

    goto :goto_0
.end method

.method private setCurrentTelecast(Landroid/database/Cursor;)V
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 1539
    sget-object v2, Lru/cn/tv/player/SimplePlayerFragment;->LOG_TAG:Ljava/lang/String;

    const-string v3, "load telecast info by time"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1541
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 1542
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1543
    const-string v2, "data"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1544
    .local v0, "t":Ljava/lang/String;
    invoke-static {v0}, Lru/cn/api/tv/replies/Telecast;->fromJson(Ljava/lang/String;)Lru/cn/api/tv/replies/Telecast;

    move-result-object v1

    .line 1546
    .local v1, "telecast":Lru/cn/api/tv/replies/Telecast;
    iget-wide v2, v1, Lru/cn/api/tv/replies/Telecast;->id:J

    iget-wide v4, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastId:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1547
    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->adController:Lru/cn/ad/AdPlayController;

    iget-object v3, v1, Lru/cn/api/tv/replies/Telecast;->adTags:Ljava/util/List;

    invoke-virtual {v2, v3}, Lru/cn/ad/AdPlayController;->setMetaTags(Ljava/util/List;)V

    .line 1548
    iget-wide v2, v1, Lru/cn/api/tv/replies/Telecast;->id:J

    invoke-direct {p0, v2, v3}, Lru/cn/tv/player/SimplePlayerFragment;->_setTelecastId(J)V

    .line 1553
    .end local v0    # "t":Ljava/lang/String;
    .end local v1    # "telecast":Lru/cn/api/tv/replies/Telecast;
    :cond_0
    :goto_0
    return-void

    .line 1550
    .restart local v0    # "t":Ljava/lang/String;
    .restart local v1    # "telecast":Lru/cn/api/tv/replies/Telecast;
    :cond_1
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->waitForTelecastChange()V

    goto :goto_0
.end method

.method private setSource(Ljava/lang/String;)V
    .locals 2
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    const/16 v1, 0x64

    .line 1345
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v1, v1}, Lru/cn/tv/player/SimplePlayerFragment;->setSource(Ljava/lang/String;FII)V

    .line 1346
    return-void
.end method

.method private setSource(Ljava/lang/String;FII)V
    .locals 1
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "aspectRatio"    # F
    .param p3, "cropX"    # I
    .param p4, "cropY"    # I

    .prologue
    .line 1349
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->isInPlayingState:Z

    .line 1350
    iput-object p1, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentSource:Ljava/lang/String;

    .line 1352
    iput p2, p0, Lru/cn/tv/player/SimplePlayerFragment;->aspectRatio:F

    .line 1353
    iput p3, p0, Lru/cn/tv/player/SimplePlayerFragment;->cropX:I

    .line 1354
    iput p4, p0, Lru/cn/tv/player/SimplePlayerFragment;->cropY:I

    .line 1355
    return-void
.end method

.method private setTelecastInfo(Landroid/database/Cursor;)V
    .locals 14
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 1556
    sget-object v7, Lru/cn/tv/player/SimplePlayerFragment;->LOG_TAG:Ljava/lang/String;

    const-string v8, "load telecast info"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1558
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-lez v7, :cond_3

    .line 1559
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1561
    const-string v7, "data"

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1562
    .local v5, "t":Ljava/lang/String;
    invoke-static {v5}, Lru/cn/api/tv/replies/Telecast;->fromJson(Ljava/lang/String;)Lru/cn/api/tv/replies/Telecast;

    move-result-object v6

    .line 1564
    .local v6, "telecast":Lru/cn/api/tv/replies/Telecast;
    iget-object v7, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerController:Lru/cn/tv/player/controller/PlayerController;

    invoke-virtual {v6}, Lru/cn/api/tv/replies/Telecast;->isHighlight()Z

    move-result v8

    iput-boolean v8, v7, Lru/cn/tv/player/controller/PlayerController;->automaticTransition:Z

    .line 1565
    invoke-virtual {v6}, Lru/cn/api/tv/replies/Telecast;->isHighlight()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1566
    iget-object v7, p0, Lru/cn/tv/player/SimplePlayerFragment;->adController:Lru/cn/ad/AdPlayController;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lru/cn/ad/AdPlayController;->setAdsEnabled(Z)V

    .line 1569
    :cond_0
    iget-object v7, v6, Lru/cn/api/tv/replies/Telecast;->channel:Lru/cn/api/tv/replies/Telecast$Channel;

    iget-wide v0, v7, Lru/cn/api/tv/replies/Telecast$Channel;->channelId:J

    .line 1570
    .local v0, "channelId":J
    invoke-direct {p0, v0, v1}, Lru/cn/tv/player/SimplePlayerFragment;->_setChannelId(J)V

    .line 1571
    const-string v7, "channel"

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelTitle:Ljava/lang/String;

    .line 1573
    iget-wide v8, v6, Lru/cn/api/tv/replies/Telecast;->id:J

    iget-wide v10, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastId:J

    cmp-long v7, v8, v10

    if-nez v7, :cond_1

    .line 1574
    iget-object v7, v6, Lru/cn/api/tv/replies/Telecast;->date:Lru/cn/api/tv/replies/DateTime;

    invoke-virtual {v7}, Lru/cn/api/tv/replies/DateTime;->toCalendar()Ljava/util/Calendar;

    move-result-object v7

    iput-object v7, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastDate:Ljava/util/Calendar;

    .line 1576
    iget-object v7, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastDate:Ljava/util/Calendar;

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    iget-wide v10, v6, Lru/cn/api/tv/replies/Telecast;->duration:J

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    add-long v2, v8, v10

    .line 1577
    .local v2, "endTime":J
    new-instance v7, Ljava/util/Date;

    invoke-direct {v7, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-static {v7}, Lru/cn/utils/Utils;->getCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v7

    iput-object v7, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastEnd:Ljava/util/Calendar;

    .line 1580
    .end local v2    # "endTime":J
    :cond_1
    iget-object v7, p0, Lru/cn/tv/player/SimplePlayerFragment;->adController:Lru/cn/ad/AdPlayController;

    iget-object v8, v6, Lru/cn/api/tv/replies/Telecast;->adTags:Ljava/util/List;

    invoke-virtual {v7, v8}, Lru/cn/ad/AdPlayController;->setMetaTags(Ljava/util/List;)V

    .line 1581
    invoke-virtual {p0, v6}, Lru/cn/tv/player/SimplePlayerFragment;->telecastInfoLoaded(Lru/cn/api/tv/replies/Telecast;)V

    .line 1583
    iget-boolean v7, p0, Lru/cn/tv/player/SimplePlayerFragment;->waitingTelecastInfoWithoutLocations:Z

    if-eqz v7, :cond_2

    .line 1584
    const/4 v7, 0x0

    iput-boolean v7, p0, Lru/cn/tv/player/SimplePlayerFragment;->waitingTelecastInfoWithoutLocations:Z

    .line 1585
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 1586
    .local v4, "now":Ljava/util/Calendar;
    iget-object v7, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastDate:Ljava/util/Calendar;

    invoke-virtual {v4, v7}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    iget-object v7, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastEnd:Ljava/util/Calendar;

    invoke-virtual {v4, v7}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1587
    iget-wide v8, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    invoke-virtual {p0, v8, v9}, Lru/cn/tv/player/SimplePlayerFragment;->playChannel(J)V

    .line 1593
    .end local v4    # "now":Ljava/util/Calendar;
    :cond_2
    :goto_0
    iget-wide v8, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    cmp-long v7, v8, v0

    if-nez v7, :cond_3

    iget-boolean v7, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    if-eqz v7, :cond_3

    .line 1594
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->waitForTelecastChange()V

    .line 1597
    .end local v0    # "channelId":J
    .end local v5    # "t":Ljava/lang/String;
    .end local v6    # "telecast":Lru/cn/api/tv/replies/Telecast;
    :cond_3
    return-void

    .line 1589
    .restart local v0    # "channelId":J
    .restart local v4    # "now":Ljava/util/Calendar;
    .restart local v5    # "t":Ljava/lang/String;
    .restart local v6    # "telecast":Lru/cn/api/tv/replies/Telecast;
    :cond_4
    iget-wide v8, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    iget-wide v10, p0, Lru/cn/tv/player/SimplePlayerFragment;->contractorId:J

    invoke-virtual {p0, v8, v9, v10, v11}, Lru/cn/tv/player/SimplePlayerFragment;->loadTelecastWithoutLocation(JJ)V

    goto :goto_0
.end method

.method private setTelecastLocations(Landroid/database/Cursor;)V
    .locals 22
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 1600
    sget-object v17, Lru/cn/tv/player/SimplePlayerFragment;->LOG_TAG:Ljava/lang/String;

    const-string v18, "load telecast location"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1602
    const-string v17, "location"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 1603
    .local v10, "locationIndex":I
    const-string v17, "contractor_id"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 1604
    .local v8, "contractorIndex":I
    const-string v17, "status"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 1605
    .local v15, "statusIndex":I
    const-string v17, "no_ads_partner"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 1607
    .local v12, "noAdsPartnerIndex":I
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v17

    if-lez v17, :cond_3

    .line 1608
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1610
    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1611
    .local v9, "location":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1612
    .local v6, "contractorId":J
    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 1613
    .local v16, "statusString":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    const/4 v11, 0x1

    .line 1615
    .local v11, "noAdsPartner":Z
    :goto_0
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lru/cn/tv/player/SimplePlayerFragment;->_setContractorId(J)V

    .line 1617
    invoke-static/range {v16 .. v16}, Lru/cn/api/medialocator/replies/Rip$RipStatus;->valueOf(Ljava/lang/String;)Lru/cn/api/medialocator/replies/Rip$RipStatus;

    move-result-object v14

    .line 1618
    .local v14, "status":Lru/cn/api/medialocator/replies/Rip$RipStatus;
    sget-object v17, Lru/cn/api/medialocator/replies/Rip$RipStatus;->ACCESS_PURCHASED:Lru/cn/api/medialocator/replies/Rip$RipStatus;

    move-object/from16 v0, v17

    if-ne v14, v0, :cond_0

    if-eqz v11, :cond_0

    .line 1619
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/player/SimplePlayerFragment;->adController:Lru/cn/ad/AdPlayController;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lru/cn/ad/AdPlayController;->setAdsEnabled(Z)V

    .line 1622
    :cond_0
    sget-object v17, Lru/cn/api/medialocator/replies/Rip$RipStatus;->ACCESS_DENIED:Lru/cn/api/medialocator/replies/Rip$RipStatus;

    move-object/from16 v0, v17

    if-ne v14, v0, :cond_2

    .line 1623
    sget-object v17, Lru/cn/tv/player/SimplePlayerFragment;->LOG_TAG:Ljava/lang/String;

    const-string v18, "denied telecast"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1624
    move-object/from16 v0, p0

    iget-wide v0, v0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v0, v1, v2, v6, v7}, Lru/cn/tv/player/SimplePlayerFragment;->loadDeniedLocation(JJ)V

    .line 1649
    .end local v6    # "contractorId":J
    .end local v9    # "location":Ljava/lang/String;
    .end local v11    # "noAdsPartner":Z
    .end local v14    # "status":Lru/cn/api/medialocator/replies/Rip$RipStatus;
    .end local v16    # "statusString":Ljava/lang/String;
    :goto_1
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    .line 1650
    return-void

    .line 1613
    .restart local v6    # "contractorId":J
    .restart local v9    # "location":Ljava/lang/String;
    .restart local v16    # "statusString":Ljava/lang/String;
    :cond_1
    const/4 v11, 0x0

    goto :goto_0

    .line 1626
    .restart local v11    # "noAdsPartner":Z
    .restart local v14    # "status":Lru/cn/api/medialocator/replies/Rip$RipStatus;
    :cond_2
    const/16 v17, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lru/cn/tv/player/SimplePlayerFragment;->timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

    .line 1627
    const/16 v17, 0x0

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v6, v7, v1, v9}, Lru/cn/domain/statistics/inetra/InetraTracker;->createTrackingSession(IJLjava/lang/String;Ljava/lang/String;)V

    .line 1630
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/player/SimplePlayerFragment;->adController:Lru/cn/ad/AdPlayController;

    move-object/from16 v17, v0

    invoke-direct/range {p0 .. p1}, Lru/cn/tv/player/SimplePlayerFragment;->parseAdTags(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lru/cn/ad/AdPlayController;->setMetaTags(Ljava/util/List;)V

    .line 1631
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lru/cn/tv/player/SimplePlayerFragment;->setSource(Ljava/lang/String;)V

    .line 1632
    invoke-direct/range {p0 .. p0}, Lru/cn/tv/player/SimplePlayerFragment;->playSource()V

    goto :goto_1

    .line 1637
    .end local v6    # "contractorId":J
    .end local v9    # "location":Ljava/lang/String;
    .end local v11    # "noAdsPartner":Z
    .end local v14    # "status":Lru/cn/api/medialocator/replies/Rip$RipStatus;
    .end local v16    # "statusString":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastDate:Ljava/util/Calendar;

    move-object/from16 v17, v0

    if-eqz v17, :cond_5

    .line 1638
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v13

    .line 1640
    .local v13, "now":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastDate:Ljava/util/Calendar;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastEnd:Ljava/util/Calendar;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 1641
    move-object/from16 v0, p0

    iget-wide v0, v0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Lru/cn/tv/player/SimplePlayerFragment;->playChannel(J)V

    goto :goto_1

    .line 1643
    :cond_4
    move-object/from16 v0, p0

    iget-wide v0, v0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lru/cn/tv/player/SimplePlayerFragment;->contractorId:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    move-wide/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3, v4}, Lru/cn/tv/player/SimplePlayerFragment;->loadTelecastWithoutLocation(JJ)V

    goto :goto_1

    .line 1646
    .end local v13    # "now":Ljava/util/Calendar;
    :cond_5
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lru/cn/tv/player/SimplePlayerFragment;->waitingTelecastInfoWithoutLocations:Z

    goto/16 :goto_1
.end method

.method private showErrorDialog(II)V
    .locals 10
    .param p1, "code"    # I
    .param p2, "playerType"    # I

    .prologue
    const v6, 0x7f0e0034

    const/4 v9, 0x0

    .line 1449
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1475
    :goto_0
    return-void

    .line 1452
    :cond_0
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lru/cn/tv/player/PlaybackErrors;->errorMessage(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v8

    .line 1454
    .local v8, "errorMessage":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x1040011

    invoke-virtual {p0, v1}, Lru/cn/tv/player/SimplePlayerFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1455
    .local v4, "message":Ljava/lang/String;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1456
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lru/cn/tv/player/SimplePlayerFragment$5;

    invoke-direct {v1, p0}, Lru/cn/tv/player/SimplePlayerFragment$5;-><init>(Lru/cn/tv/player/SimplePlayerFragment;)V

    .line 1457
    invoke-virtual {v0, v6, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 1464
    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 1465
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v7

    .line 1468
    .local v7, "dialog":Landroid/app/AlertDialog;
    invoke-static {}, Lru/cn/peersay/controllers/DialogsRemoteController;->sharedInstance()Lru/cn/peersay/controllers/DialogsRemoteController;

    move-result-object v0

    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "player_error"

    const-string v3, ""

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    .line 1469
    invoke-virtual {p0, v6}, Lru/cn/tv/player/SimplePlayerFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    new-instance v6, Lru/cn/tv/player/SimplePlayerFragment$6;

    invoke-direct {v6, p0, v7}, Lru/cn/tv/player/SimplePlayerFragment$6;-><init>(Lru/cn/tv/player/SimplePlayerFragment;Landroid/app/AlertDialog;)V

    .line 1468
    invoke-virtual/range {v0 .. v6}, Lru/cn/peersay/controllers/DialogsRemoteController;->registerDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lru/cn/peersay/controllers/DialogsRemoteController$DismissListener;)V

    goto :goto_0
.end method

.method private static timeshiftLocation(Lru/cn/api/iptv/replies/MediaLocation;Ljava/util/List;)Lru/cn/api/iptv/replies/MediaLocation;
    .locals 6
    .param p0, "current"    # Lru/cn/api/iptv/replies/MediaLocation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;)",
            "Lru/cn/api/iptv/replies/MediaLocation;"
        }
    .end annotation

    .prologue
    .line 381
    .local p1, "locations":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/iptv/replies/MediaLocation;>;"
    iget-boolean v1, p0, Lru/cn/api/iptv/replies/MediaLocation;->hasTimeshift:Z

    if-eqz v1, :cond_1

    move-object v0, p0

    .line 395
    :cond_0
    :goto_0
    return-object v0

    .line 384
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/iptv/replies/MediaLocation;

    .line 385
    .local v0, "mediaLocation":Lru/cn/api/iptv/replies/MediaLocation;
    iget-boolean v2, v0, Lru/cn/api/iptv/replies/MediaLocation;->hasTimeshift:Z

    if-eqz v2, :cond_2

    .line 386
    iget-wide v2, v0, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    iget-wide v4, p0, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 389
    iget-object v2, v0, Lru/cn/api/iptv/replies/MediaLocation;->timezoneOffset:Ljava/lang/Long;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lru/cn/api/iptv/replies/MediaLocation;->timezoneOffset:Ljava/lang/Long;

    iget-object v3, p0, Lru/cn/api/iptv/replies/MediaLocation;->timezoneOffset:Ljava/lang/Long;

    .line 390
    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    .line 395
    .end local v0    # "mediaLocation":Lru/cn/api/iptv/replies/MediaLocation;
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private waitForTelecastChange()V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 1664
    iget-object v8, p0, Lru/cn/tv/player/SimplePlayerFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 1665
    iget-object v8, p0, Lru/cn/tv/player/SimplePlayerFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v13}, Landroid/os/Handler;->removeMessages(I)V

    .line 1667
    const-wide/32 v2, 0xea60

    .line 1668
    .local v2, "delay":J
    iget-object v8, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastEnd:Ljava/util/Calendar;

    if-eqz v8, :cond_0

    .line 1669
    iget-object v8, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastEnd:Ljava/util/Calendar;

    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    .line 1670
    .local v6, "telecastEnd":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iget v10, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeShiftOffset:I

    mul-int/lit16 v10, v10, 0x3e8

    int-to-long v10, v10

    sub-long v4, v8, v10

    .line 1671
    .local v4, "playingTime":J
    sub-long v2, v6, v4

    .line 1675
    .end local v4    # "playingTime":J
    .end local v6    # "telecastEnd":J
    :cond_0
    iget v8, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeShiftOffset:I

    if-nez v8, :cond_2

    .line 1677
    new-instance v8, Ljava/util/Random;

    invoke-direct {v8}, Ljava/util/Random;-><init>()V

    const/4 v9, 0x5

    invoke-virtual {v8, v9}, Ljava/util/Random;->nextInt(I)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    mul-int/lit16 v8, v8, 0x3e8

    int-to-long v8, v8

    add-long/2addr v2, v8

    .line 1679
    iget-object v8, p0, Lru/cn/tv/player/SimplePlayerFragment;->mHandler:Landroid/os/Handler;

    invoke-static {v8, v12}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 1684
    .local v1, "msg":Landroid/os/Message;
    :goto_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1685
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v8, "cnId"

    iget-wide v10, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    invoke-virtual {v0, v8, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1686
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1689
    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-gez v8, :cond_1

    .line 1690
    const-wide/32 v2, 0xea60

    .line 1693
    :cond_1
    iget-object v8, p0, Lru/cn/tv/player/SimplePlayerFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1694
    return-void

    .line 1681
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :cond_2
    iget-object v8, p0, Lru/cn/tv/player/SimplePlayerFragment;->mHandler:Landroid/os/Handler;

    invoke-static {v8, v13}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .restart local v1    # "msg":Landroid/os/Message;
    goto :goto_0
.end method


# virtual methods
.method public allowBlocking()Z
    .locals 2

    .prologue
    .line 959
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->getPlaybackMode()Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    move-result-object v0

    sget-object v1, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;->ARCHIVE:Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final bridge synthetic bridge$lambda$0$SimplePlayerFragment(Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/player/SimplePlayerFragment;->setChannelInfo(Landroid/database/Cursor;)V

    return-void
.end method

.method final bridge synthetic bridge$lambda$1$SimplePlayerFragment(Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/player/SimplePlayerFragment;->setChannelLocations(Ljava/util/List;)V

    return-void
.end method

.method final bridge synthetic bridge$lambda$2$SimplePlayerFragment(Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/player/SimplePlayerFragment;->setCurrentTelecast(Landroid/database/Cursor;)V

    return-void
.end method

.method final bridge synthetic bridge$lambda$3$SimplePlayerFragment(Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/player/SimplePlayerFragment;->setTelecastInfo(Landroid/database/Cursor;)V

    return-void
.end method

.method final bridge synthetic bridge$lambda$4$SimplePlayerFragment(Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/player/SimplePlayerFragment;->setTelecastLocations(Landroid/database/Cursor;)V

    return-void
.end method

.method protected channelInfoLoaded(JLjava/lang/String;ZIZZ)V
    .locals 9
    .param p1, "cnId"    # J
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "isFavourite"    # Z
    .param p5, "channelNumber"    # I
    .param p6, "isDenied"    # Z
    .param p7, "isIntersections"    # Z

    .prologue
    .line 1051
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->listener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

    if-eqz v0, :cond_0

    .line 1052
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->listener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

    move-wide v2, p1

    move-object v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-interface/range {v1 .. v7}, Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;->onChannelInfoLoaded(JLjava/lang/String;ZIZ)V

    .line 1055
    :cond_0
    return-void
.end method

.method public getAudioTrackInfoProvider()Lru/cn/player/ITrackSelector;
    .locals 1

    .prologue
    .line 1273
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getAudioTrackProvider()Lru/cn/player/ITrackSelector;

    move-result-object v0

    return-object v0
.end method

.method public getBufferPosition()J
    .locals 2

    .prologue
    .line 992
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getBufferPosition()J

    move-result-wide v0

    return-wide v0
.end method

.method public final getChannelId()J
    .locals 2

    .prologue
    .line 737
    iget-wide v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    return-wide v0
.end method

.method public getCurrentPosition()I
    .locals 8

    .prologue
    .line 969
    const/4 v0, 0x0

    .line 970
    .local v0, "position":I
    iget-boolean v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    if-eqz v1, :cond_2

    .line 971
    iget v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->stoppedPosition:I

    if-lez v1, :cond_1

    .line 972
    iget v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->stoppedPosition:I

    .line 982
    :cond_0
    :goto_0
    return v0

    .line 973
    :cond_1
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastDate:Ljava/util/Calendar;

    if-eqz v1, :cond_0

    .line 974
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 975
    .local v2, "referenceTime":J
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastDate:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    sub-long v4, v2, v4

    iget v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeShiftOffset:I

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v6, v1

    sub-long/2addr v4, v6

    iget-wide v6, p0, Lru/cn/tv/player/SimplePlayerFragment;->startOverCorrection:J

    sub-long/2addr v4, v6

    long-to-int v0, v4

    .line 977
    goto :goto_0

    .line 979
    .end local v2    # "referenceTime":J
    :cond_2
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v1}, Lru/cn/player/SimplePlayer;->getCurrentPosition()I

    move-result v0

    goto :goto_0
.end method

.method public final getCurrentTelecastDate()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 757
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastDate:Ljava/util/Calendar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastDate:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getCurrentTelecastId()J
    .locals 2

    .prologue
    .line 753
    iget-wide v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastId:J

    return-wide v0
.end method

.method public getDateTime()J
    .locals 2

    .prologue
    .line 964
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getDateTime()J

    move-result-wide v0

    return-wide v0
.end method

.method protected getDefaultFitMode(Landroid/content/Context;)Lru/cn/player/SimplePlayer$FitMode;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1024
    const-string v1, "scaling_level"

    invoke-static {p1, v1}, Lru/cn/domain/Preferences;->getInt(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 1025
    .local v0, "scalingLevel":I
    packed-switch v0, :pswitch_data_0

    .line 1036
    sget-object v1, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_HEIGHT:Lru/cn/player/SimplePlayer$FitMode;

    :goto_0
    return-object v1

    .line 1027
    :pswitch_0
    sget-object v1, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_HEIGHT:Lru/cn/player/SimplePlayer$FitMode;

    goto :goto_0

    .line 1030
    :pswitch_1
    sget-object v1, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_WIDTH:Lru/cn/player/SimplePlayer$FitMode;

    goto :goto_0

    .line 1033
    :pswitch_2
    sget-object v1, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_SIZE:Lru/cn/player/SimplePlayer$FitMode;

    goto :goto_0

    .line 1025
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getDuration()I
    .locals 4

    .prologue
    .line 941
    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    if-eqz v0, :cond_1

    .line 942
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastDate:Ljava/util/Calendar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastEnd:Ljava/util/Calendar;

    if-eqz v0, :cond_0

    .line 943
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastEnd:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastDate:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    .line 949
    :goto_0
    return v0

    .line 946
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 949
    :cond_1
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getDuration()I

    move-result v0

    goto :goto_0
.end method

.method public getFitMode()Lru/cn/player/SimplePlayer$FitMode;
    .locals 1

    .prologue
    .line 1006
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getFitMode()Lru/cn/player/SimplePlayer$FitMode;

    move-result-object v0

    return-object v0
.end method

.method public getLivePosition()I
    .locals 4

    .prologue
    .line 892
    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastDate:Ljava/util/Calendar;

    if-nez v0, :cond_1

    .line 893
    :cond_0
    const/4 v0, -0x1

    .line 895
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastDate:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->startOverCorrection:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0
.end method

.method public final getLocationTerritoryId()J
    .locals 2

    .prologue
    .line 749
    iget-wide v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->locationTerritoryId:J

    return-wide v0
.end method

.method getPlaybackMode()Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;
    .locals 1

    .prologue
    .line 1334
    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    if-eqz v0, :cond_1

    .line 1335
    iget v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeShiftOffset:I

    if-nez v0, :cond_0

    .line 1336
    sget-object v0, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;->ON_AIR:Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    .line 1341
    :goto_0
    return-object v0

    .line 1338
    :cond_0
    sget-object v0, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;->TIMESHIFT:Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    goto :goto_0

    .line 1341
    :cond_1
    sget-object v0, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;->ARCHIVE:Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    goto :goto_0
.end method

.method public getState()Lru/cn/player/AbstractMediaPlayer$PlayerState;
    .locals 1

    .prologue
    .line 954
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getState()Lru/cn/player/AbstractMediaPlayer$PlayerState;

    move-result-object v0

    return-object v0
.end method

.method public getSubtitlesTrackProvider()Lru/cn/player/ITrackSelector;
    .locals 1

    .prologue
    .line 1278
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getSubtitlesTrackProvider()Lru/cn/player/ITrackSelector;

    move-result-object v0

    return-object v0
.end method

.method public getVideoTrackInfoProvider()Lru/cn/player/ITrackSelector;
    .locals 1

    .prologue
    .line 1268
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getVideoTracksProvider()Lru/cn/player/ITrackSelector;

    move-result-object v0

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 1283
    iget v7, p1, Landroid/os/Message;->what:I

    packed-switch v7, :pswitch_data_0

    move v1, v6

    .line 1321
    :cond_0
    :goto_0
    return v1

    .line 1285
    :pswitch_0
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 1286
    invoke-virtual {p1}, Landroid/os/Message;->peekData()Landroid/os/Bundle;

    move-result-object v0

    .line 1287
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 1288
    const-string v6, "cnId"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1289
    .local v2, "channelId":J
    cmp-long v6, v2, v8

    if-eqz v6, :cond_0

    .line 1290
    iget-object v6, p0, Lru/cn/tv/player/SimplePlayerFragment;->viewModel:Lru/cn/tv/player/PlaybackViewModel;

    invoke-virtual {v6, v2, v3}, Lru/cn/tv/player/PlaybackViewModel;->loadChannel(J)V

    goto :goto_0

    .line 1297
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v2    # "channelId":J
    :pswitch_1
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 1298
    invoke-virtual {p1}, Landroid/os/Message;->peekData()Landroid/os/Bundle;

    move-result-object v0

    .line 1299
    .restart local v0    # "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 1300
    const-string v6, "cnId"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1302
    .restart local v2    # "channelId":J
    cmp-long v6, v2, v8

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->getPlaybackMode()Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    move-result-object v6

    sget-object v7, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;->TIMESHIFT:Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    if-ne v6, v7, :cond_1

    .line 1303
    invoke-static {}, Lru/cn/utils/Utils;->getCalendar()Ljava/util/Calendar;

    move-result-object v6

    .line 1304
    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    iget v8, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeShiftOffset:I

    int-to-long v8, v8

    sub-long v4, v6, v8

    .line 1306
    .local v4, "currentPlayedTime":J
    iget-object v6, p0, Lru/cn/tv/player/SimplePlayerFragment;->viewModel:Lru/cn/tv/player/PlaybackViewModel;

    invoke-virtual {v6, v2, v3, v4, v5}, Lru/cn/tv/player/PlaybackViewModel;->loadCurrentTelecast(JJ)V

    goto :goto_0

    .line 1309
    .end local v4    # "currentPlayedTime":J
    :cond_1
    sget-object v6, Lru/cn/tv/player/SimplePlayerFragment;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Something goes wrong with timeshift timer id="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " air="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " offset="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeShiftOffset:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1317
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v2    # "channelId":J
    :pswitch_2
    iget-object v7, p0, Lru/cn/tv/player/SimplePlayerFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v7, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_0

    .line 1283
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final isIntersectionChannelPlayed()Z
    .locals 1

    .prologue
    .line 745
    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->isIntersections:Z

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 987
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public final isPornoChannelPlayed()Z
    .locals 1

    .prologue
    .line 741
    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->isPorno:Z

    return v0
.end method

.method public isSeekable()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 900
    iget-boolean v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeshifter:Lru/cn/player/timeshift/StreamTimeshifter;

    if-nez v1, :cond_1

    .line 903
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->getDuration()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected loadTelecastWithoutLocation(JJ)V
    .locals 11
    .param p1, "channelId"    # J
    .param p3, "contractorId"    # J

    .prologue
    const v6, 0x7f0e0034

    const/4 v10, 0x0

    .line 1129
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1166
    :goto_0
    return-void

    .line 1132
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1133
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerController:Lru/cn/tv/player/controller/PlayerController;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/PlayerController;->show()V

    .line 1135
    const v8, 0x7f0e0159

    .line 1136
    .local v8, "messageId":I
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    .line 1137
    const v8, 0x7f0e015a

    .line 1139
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v9

    .line 1140
    .local v9, "now":Ljava/util/Calendar;
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastDate:Ljava/util/Calendar;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastDate:Ljava/util/Calendar;

    invoke-virtual {v9, v0}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1141
    const v8, 0x7f0e015b

    .line 1145
    .end local v9    # "now":Ljava/util/Calendar;
    :cond_1
    invoke-virtual {p0, v8}, Lru/cn/tv/player/SimplePlayerFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1146
    .local v4, "message":Ljava/lang/String;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1147
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lru/cn/tv/player/SimplePlayerFragment$2;

    invoke-direct {v1, p0}, Lru/cn/tv/player/SimplePlayerFragment$2;-><init>(Lru/cn/tv/player/SimplePlayerFragment;)V

    .line 1148
    invoke-virtual {v0, v6, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 1155
    invoke-virtual {v0, v10}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 1156
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v7

    .line 1159
    .local v7, "dialog":Landroid/app/AlertDialog;
    invoke-static {}, Lru/cn/peersay/controllers/DialogsRemoteController;->sharedInstance()Lru/cn/peersay/controllers/DialogsRemoteController;

    move-result-object v0

    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "content_error"

    const-string v3, ""

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    .line 1160
    invoke-virtual {p0, v6}, Lru/cn/tv/player/SimplePlayerFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v10

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    new-instance v6, Lru/cn/tv/player/SimplePlayerFragment$3;

    invoke-direct {v6, p0, v7}, Lru/cn/tv/player/SimplePlayerFragment$3;-><init>(Lru/cn/tv/player/SimplePlayerFragment;Landroid/app/AlertDialog;)V

    .line 1159
    invoke-virtual/range {v0 .. v6}, Lru/cn/peersay/controllers/DialogsRemoteController;->registerDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lru/cn/peersay/controllers/DialogsRemoteController$DismissListener;)V

    goto :goto_0
.end method

.method public onAdBreak()V
    .locals 2

    .prologue
    .line 1065
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerCallbacks:Lru/cn/player/SimplePlayer$Listener;

    invoke-virtual {v0, v1}, Lru/cn/player/SimplePlayer;->removeListener(Lru/cn/player/SimplePlayer$Listener;)V

    .line 1066
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->remoteController:Lru/cn/peersay/controllers/PlayerRemoteController;

    if-eqz v0, :cond_0

    .line 1067
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->remoteController:Lru/cn/peersay/controllers/PlayerRemoteController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lru/cn/peersay/controllers/PlayerRemoteController;->setPlayer(Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;)V

    .line 1071
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->continuePlaybackAfterAd:Z

    .line 1072
    return-void
.end method

.method public onAdBreakEnded(Z)V
    .locals 3
    .param p1, "isMiddleRoll"    # Z

    .prologue
    const/4 v2, 0x0

    .line 1096
    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playingAd:Z

    if-eqz v0, :cond_3

    .line 1097
    iput-boolean v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->playingAd:Z

    .line 1098
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lru/cn/player/SimplePlayer;->setNeedHeadsetUnpluggedHandle(Z)V

    .line 1099
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->listener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

    if-eqz v0, :cond_0

    .line 1100
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->listener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

    invoke-interface {v0}, Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;->adStopped()V

    .line 1103
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerCallbacks:Lru/cn/player/SimplePlayer$Listener;

    invoke-virtual {v0, v1}, Lru/cn/player/SimplePlayer;->addListener(Lru/cn/player/SimplePlayer$Listener;)V

    .line 1105
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->continuePlaybackAfterAd:Z

    if-eqz v0, :cond_1

    .line 1106
    iput-boolean v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->continuePlaybackAfterAd:Z

    .line 1108
    if-nez p1, :cond_2

    .line 1109
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->_playContent()V

    .line 1117
    :cond_1
    :goto_0
    return-void

    .line 1111
    :cond_2
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->resume()V

    goto :goto_0

    .line 1115
    :cond_3
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->_playContent()V

    goto :goto_0
.end method

.method public onAdCompanion(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1077
    return-void
.end method

.method public onAdStart()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1081
    iput-boolean v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->playingAd:Z

    .line 1082
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lru/cn/player/SimplePlayer;->setNeedHeadsetUnpluggedHandle(Z)V

    .line 1083
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->listener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

    if-eqz v0, :cond_0

    .line 1084
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->listener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

    invoke-interface {v0}, Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;->adStarted()V

    .line 1087
    :cond_0
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1088
    iput-boolean v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->continuePlaybackAfterAd:Z

    .line 1091
    :cond_1
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1092
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 184
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 185
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->context:Landroid/content/Context;

    .line 186
    return-void
.end method

.method protected onCastConnectionInProgress()V
    .locals 2

    .prologue
    .line 1207
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->stoppedPosition:I

    .line 1209
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->stop()V

    .line 1210
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1211
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerController:Lru/cn/tv/player/controller/PlayerController;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/PlayerController;->hide()V

    .line 1212
    return-void
.end method

.method protected onCastConnectionSuccess(Ljava/lang/String;)V
    .locals 2
    .param p1, "castDeviceName"    # Ljava/lang/String;

    .prologue
    .line 1215
    const-string v0, "1"

    invoke-static {v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->setCurrentViewScreen(Ljava/lang/String;)V

    .line 1218
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentSource:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1227
    :cond_0
    :goto_0
    return-void

    .line 1221
    :cond_1
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    const/16 v1, 0x69

    invoke-virtual {v0, v1}, Lru/cn/player/SimplePlayer;->setPlayerType(I)V

    .line 1224
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1225
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->playSource()V

    goto :goto_0
.end method

.method protected onCastDisconnect()V
    .locals 2

    .prologue
    .line 1230
    const-string v0, "0"

    invoke-static {v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->setCurrentViewScreen(Ljava/lang/String;)V

    .line 1233
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentSource:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1246
    :cond_0
    :goto_0
    return-void

    .line 1236
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocationsCurrentItem:I

    .line 1238
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->stoppedPosition:I

    .line 1240
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lru/cn/player/SimplePlayer;->setPlayerType(I)V

    .line 1243
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1244
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->playSource()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 170
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 171
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->mHandler:Landroid/os/Handler;

    .line 173
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v0

    const-class v1, Lru/cn/tv/player/PlaybackViewModel;

    invoke-static {p0, v0, v1}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lru/cn/tv/player/PlaybackViewModel;

    iput-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->viewModel:Lru/cn/tv/player/PlaybackViewModel;

    .line 174
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->viewModel:Lru/cn/tv/player/PlaybackViewModel;

    invoke-virtual {v0}, Lru/cn/tv/player/PlaybackViewModel;->channel()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/player/SimplePlayerFragment$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/player/SimplePlayerFragment$$Lambda$0;-><init>(Lru/cn/tv/player/SimplePlayerFragment;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 175
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->viewModel:Lru/cn/tv/player/PlaybackViewModel;

    invoke-virtual {v0}, Lru/cn/tv/player/PlaybackViewModel;->channelLocations()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/player/SimplePlayerFragment$$Lambda$1;

    invoke-direct {v1, p0}, Lru/cn/tv/player/SimplePlayerFragment$$Lambda$1;-><init>(Lru/cn/tv/player/SimplePlayerFragment;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 176
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->viewModel:Lru/cn/tv/player/PlaybackViewModel;

    invoke-virtual {v0}, Lru/cn/tv/player/PlaybackViewModel;->currentTelecast()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/player/SimplePlayerFragment$$Lambda$2;

    invoke-direct {v1, p0}, Lru/cn/tv/player/SimplePlayerFragment$$Lambda$2;-><init>(Lru/cn/tv/player/SimplePlayerFragment;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 178
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->viewModel:Lru/cn/tv/player/PlaybackViewModel;

    invoke-virtual {v0}, Lru/cn/tv/player/PlaybackViewModel;->telecast()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/player/SimplePlayerFragment$$Lambda$3;

    invoke-direct {v1, p0}, Lru/cn/tv/player/SimplePlayerFragment$$Lambda$3;-><init>(Lru/cn/tv/player/SimplePlayerFragment;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 179
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->viewModel:Lru/cn/tv/player/PlaybackViewModel;

    invoke-virtual {v0}, Lru/cn/tv/player/PlaybackViewModel;->telecastLocations()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/player/SimplePlayerFragment$$Lambda$4;

    invoke-direct {v1, p0}, Lru/cn/tv/player/SimplePlayerFragment$$Lambda$4;-><init>(Lru/cn/tv/player/SimplePlayerFragment;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 180
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 191
    const v0, 0x7f0c008e

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected onFeedbackIntent(Lru/cn/utils/FeedbackBuilder;)V
    .locals 4
    .param p1, "feedbackBuilder"    # Lru/cn/utils/FeedbackBuilder;

    .prologue
    .line 1325
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->getPlaybackMode()Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    move-result-object v0

    invoke-virtual {p1, v0}, Lru/cn/utils/FeedbackBuilder;->setPlaybackMode(Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;)Lru/cn/utils/FeedbackBuilder;

    move-result-object v0

    iget-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->contractorId:J

    .line 1326
    invoke-virtual {v0, v2, v3}, Lru/cn/utils/FeedbackBuilder;->setContractorId(J)Lru/cn/utils/FeedbackBuilder;

    move-result-object v0

    .line 1327
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->getChannelId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lru/cn/utils/FeedbackBuilder;->setChanelId(J)Lru/cn/utils/FeedbackBuilder;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelTitle:Ljava/lang/String;

    .line 1328
    invoke-virtual {v0, v1}, Lru/cn/utils/FeedbackBuilder;->setChannelTitle(Ljava/lang/String;)Lru/cn/utils/FeedbackBuilder;

    move-result-object v0

    iget-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastId:J

    .line 1329
    invoke-virtual {v0, v2, v3}, Lru/cn/utils/FeedbackBuilder;->setTelecastId(J)Lru/cn/utils/FeedbackBuilder;

    .line 1330
    return-void
.end method

.method protected onLoadDeniedLocation(JJ)V
    .locals 0
    .param p1, "channelId"    # J
    .param p3, "contractorId"    # J

    .prologue
    .line 1125
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 662
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getPlayerType()I

    move-result v0

    const/16 v1, 0x69

    if-eq v0, v1, :cond_0

    .line 663
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->pause()V

    .line 664
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->stoppedPosition:I

    .line 666
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeshifter:Lru/cn/player/timeshift/StreamTimeshifter;

    if-eqz v0, :cond_0

    .line 667
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeshifter:Lru/cn/player/timeshift/StreamTimeshifter;

    invoke-virtual {v0}, Lru/cn/player/timeshift/StreamTimeshifter;->cancelRequests()V

    .line 671
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 672
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 674
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 675
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 690
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->adController:Lru/cn/ad/AdPlayController;

    invoke-virtual {v0}, Lru/cn/ad/AdPlayController;->onActivityResume()V

    .line 691
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 692
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 696
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 697
    const-string v0, "stoppedPosition"

    iget v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->stoppedPosition:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 698
    const-string v0, "currentTelecastId"

    iget-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastId:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 699
    const-string v0, "currentChannelId"

    iget-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 700
    const-string v0, "timeShiftOffset"

    iget v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeShiftOffset:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 701
    const-string v0, "startOverCorrection"

    iget-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->startOverCorrection:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 703
    const-string v0, "onAir"

    iget-boolean v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 705
    invoke-static {p1}, Lru/cn/domain/statistics/inetra/InetraTracker;->saveSessionParams(Landroid/os/Bundle;)V

    .line 706
    return-void
.end method

.method public onSeekCompleted(Ljava/lang/String;IZ)V
    .locals 12
    .param p1, "playbackUri"    # Ljava/lang/String;
    .param p2, "offset"    # I
    .param p3, "startOver"    # Z

    .prologue
    const/4 v6, 0x0

    .line 763
    const/16 v1, 0x3e8

    invoke-static {v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->watch(I)V

    .line 766
    invoke-static {}, Lru/cn/domain/statistics/inetra/InetraTracker;->getViewFrom()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x0

    .line 765
    invoke-static {v1, v2}, Lru/cn/domain/statistics/inetra/InetraTracker;->setSessionParams(II)V

    .line 769
    if-nez p2, :cond_0

    .line 770
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->_playCurrentChannel()V

    .line 792
    :goto_0
    return-void

    .line 774
    :cond_0
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeshifter:Lru/cn/player/timeshift/StreamTimeshifter;

    iget-object v0, v1, Lru/cn/player/timeshift/StreamTimeshifter;->location:Lru/cn/api/iptv/replies/MediaLocation;

    .line 775
    .local v0, "location":Lru/cn/api/iptv/replies/MediaLocation;
    iput p2, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeShiftOffset:I

    .line 776
    if-eqz p3, :cond_1

    .line 777
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->getCurrentPosition()I

    move-result v1

    int-to-long v2, v1

    iput-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->startOverCorrection:J

    .line 780
    :cond_1
    iget-wide v2, v0, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    invoke-direct {p0, v2, v3}, Lru/cn/tv/player/SimplePlayerFragment;->_setContractorId(J)V

    .line 781
    iget-wide v2, v0, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    iput-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->locationTerritoryId:J

    .line 783
    iput-object v6, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

    .line 784
    const/4 v1, 0x2

    iget-wide v2, v0, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    iget-object v4, v0, Lru/cn/api/iptv/replies/MediaLocation;->sourceUri:Ljava/lang/String;

    iget-wide v8, v0, Lru/cn/api/iptv/replies/MediaLocation;->channelId:J

    const-wide/16 v10, 0x0

    cmp-long v5, v8, v10

    if-nez v5, :cond_2

    iget-object v6, v0, Lru/cn/api/iptv/replies/MediaLocation;->title:Ljava/lang/String;

    :cond_2
    move-object v5, p1

    invoke-static/range {v1 .. v6}, Lru/cn/domain/statistics/inetra/InetraTracker;->createTrackingSession(IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->waitForTelecastChange()V

    .line 790
    invoke-virtual {v0}, Lru/cn/api/iptv/replies/MediaLocation;->getAspectRatio()F

    move-result v1

    iget v2, v0, Lru/cn/api/iptv/replies/MediaLocation;->cropX:I

    iget v3, v0, Lru/cn/api/iptv/replies/MediaLocation;->cropY:I

    invoke-direct {p0, p1, v1, v2, v3}, Lru/cn/tv/player/SimplePlayerFragment;->setSource(Ljava/lang/String;FII)V

    .line 791
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->playSource()V

    goto :goto_0
.end method

.method public onSeekFailed()V
    .locals 2

    .prologue
    .line 800
    const/16 v0, 0x3e8

    invoke-static {v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->watch(I)V

    .line 803
    invoke-static {}, Lru/cn/domain/statistics/inetra/InetraTracker;->getViewFrom()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x0

    .line 802
    invoke-static {v0, v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->setSessionParams(II)V

    .line 806
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->_playCurrentChannel()V

    .line 807
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 679
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getPlayerType()I

    move-result v0

    const/16 v1, 0x69

    if-eq v0, v1, :cond_0

    .line 681
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocations:Ljava/util/List;

    .line 682
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->stop()V

    .line 685
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 686
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 10
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const-wide/16 v8, 0x0

    .line 196
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 198
    const v1, 0x7f090172

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lru/cn/player/SimplePlayer;

    iput-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    .line 199
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    iget-object v6, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerCallbacks:Lru/cn/player/SimplePlayer$Listener;

    invoke-virtual {v1, v6}, Lru/cn/player/SimplePlayer;->addListener(Lru/cn/player/SimplePlayer$Listener;)V

    .line 201
    const v1, 0x7f09016f

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 203
    const v1, 0x7f09001c

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 204
    .local v0, "adsControlContainer":Landroid/view/ViewGroup;
    new-instance v1, Lru/cn/ad/AdPlayController;

    iget-object v6, p0, Lru/cn/tv/player/SimplePlayerFragment;->context:Landroid/content/Context;

    iget-object v7, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-direct {v1, v6, v7, v0, p0}, Lru/cn/ad/AdPlayController;-><init>(Landroid/content/Context;Lru/cn/player/SimplePlayer;Landroid/view/ViewGroup;Lru/cn/ad/AdPlayController$Listener;)V

    iput-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->adController:Lru/cn/ad/AdPlayController;

    .line 206
    if-eqz p2, :cond_0

    .line 207
    const-string v1, "stoppedPosition"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->stoppedPosition:I

    .line 208
    const-string v1, "currentTelecastId"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 209
    .local v4, "telecastId":J
    invoke-direct {p0, v4, v5}, Lru/cn/tv/player/SimplePlayerFragment;->_setTelecastId(J)V

    .line 211
    const-string v1, "currentChannelId"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 212
    .local v2, "channelId":J
    invoke-direct {p0, v2, v3}, Lru/cn/tv/player/SimplePlayerFragment;->_setChannelId(J)V

    .line 214
    const-string v1, "timeShiftOffset"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeShiftOffset:I

    .line 215
    const-string v1, "startOverCorrection"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p0, Lru/cn/tv/player/SimplePlayerFragment;->startOverCorrection:J

    .line 216
    const-string v1, "onAir"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    .line 218
    invoke-static {p2}, Lru/cn/domain/statistics/inetra/InetraTracker;->restoreSessionParams(Landroid/os/Bundle;)V

    .line 220
    iget-boolean v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    if-nez v1, :cond_1

    .line 221
    iget-wide v6, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastId:J

    cmp-long v1, v6, v8

    if-lez v1, :cond_0

    .line 222
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->_playCurrentTelecast()V

    .line 228
    .end local v2    # "channelId":J
    .end local v4    # "telecastId":J
    :cond_0
    :goto_0
    return-void

    .line 224
    .restart local v2    # "channelId":J
    .restart local v4    # "telecastId":J
    :cond_1
    iget-wide v6, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    cmp-long v1, v6, v8

    if-lez v1, :cond_0

    .line 225
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->_playCurrentChannel()V

    goto :goto_0
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 831
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->pause()V

    .line 833
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeshifter:Lru/cn/player/timeshift/StreamTimeshifter;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    if-eqz v0, :cond_0

    .line 834
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->stoppedPosition:I

    .line 836
    :cond_0
    return-void
.end method

.method public play()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 811
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v1}, Lru/cn/player/SimplePlayer;->getState()Lru/cn/player/AbstractMediaPlayer$PlayerState;

    move-result-object v0

    .line 813
    .local v0, "playerState":Lru/cn/player/AbstractMediaPlayer$PlayerState;
    sget-object v1, Lru/cn/player/AbstractMediaPlayer$PlayerState;->PAUSED:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_2

    .line 814
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeshifter:Lru/cn/player/timeshift/StreamTimeshifter;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    if-eqz v1, :cond_0

    .line 815
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->getCurrentPosition()I

    move-result v1

    invoke-virtual {p0, v1}, Lru/cn/tv/player/SimplePlayerFragment;->seekTo(I)V

    .line 818
    :cond_0
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v1}, Lru/cn/player/SimplePlayer;->resume()V

    .line 827
    :cond_1
    :goto_0
    return-void

    .line 819
    :cond_2
    sget-object v1, Lru/cn/player/AbstractMediaPlayer$PlayerState;->PLAYING:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    if-eq v0, v1, :cond_1

    .line 820
    iget-boolean v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    if-eqz v1, :cond_3

    iget-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 821
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->_playCurrentChannel()V

    goto :goto_0

    .line 823
    :cond_3
    iget-boolean v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    if-nez v1, :cond_1

    iget-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastId:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 824
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->_playCurrentTelecast()V

    goto :goto_0
.end method

.method public playChannel(J)V
    .locals 9
    .param p1, "channelId"    # J

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 231
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->adController:Lru/cn/ad/AdPlayController;

    invoke-virtual {v1, v5}, Lru/cn/ad/AdPlayController;->setAdsEnabled(Z)V

    .line 233
    iget-boolean v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    cmp-long v1, p1, v2

    if-nez v1, :cond_0

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v1}, Lru/cn/player/SimplePlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 277
    :goto_0
    return-void

    .line 236
    :cond_0
    invoke-direct {p0, v6, v7}, Lru/cn/tv/player/SimplePlayerFragment;->_setContractorId(J)V

    .line 237
    const/4 v1, 0x0

    iput-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

    .line 239
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 241
    iget-boolean v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    if-eqz v1, :cond_1

    iget-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    cmp-long v1, p1, v2

    if-eqz v1, :cond_4

    .line 242
    :cond_1
    iput v4, p0, Lru/cn/tv/player/SimplePlayerFragment;->stoppedPosition:I

    .line 243
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->stop()V

    .line 246
    iget-boolean v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeShiftOffset:I

    if-lez v1, :cond_3

    .line 247
    :cond_2
    iput-wide v6, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    .line 250
    :cond_3
    iput-boolean v5, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    .line 251
    iput v4, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeShiftOffset:I

    .line 252
    iput-wide v6, p0, Lru/cn/tv/player/SimplePlayerFragment;->startOverCorrection:J

    .line 253
    iput-boolean v4, p0, Lru/cn/tv/player/SimplePlayerFragment;->waitingTelecastInfoWithoutLocations:Z

    .line 255
    const-wide/16 v2, -0x1

    invoke-direct {p0, v2, v3}, Lru/cn/tv/player/SimplePlayerFragment;->_setTelecastId(J)V

    .line 256
    invoke-direct {p0, p1, p2}, Lru/cn/tv/player/SimplePlayerFragment;->_setChannelId(J)V

    .line 259
    :cond_4
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->castSession:Lcom/google/android/gms/cast/framework/CastSession;

    if-nez v1, :cond_5

    .line 260
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerController:Lru/cn/tv/player/controller/PlayerController;

    invoke-virtual {v1, v4}, Lru/cn/tv/player/controller/PlayerController;->setAlwaysShow(Z)V

    .line 261
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerController:Lru/cn/tv/player/controller/PlayerController;

    invoke-virtual {v1}, Lru/cn/tv/player/controller/PlayerController;->hide()V

    .line 263
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->context:Landroid/content/Context;

    invoke-static {v1, v5}, Lru/cn/ad/AdsManager;->getPreRollPlaceId(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    .line 264
    .local v0, "placeId":Ljava/lang/String;
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->adController:Lru/cn/ad/AdPlayController;

    invoke-virtual {v1, v0}, Lru/cn/ad/AdPlayController;->resetPreRoll(Ljava/lang/String;)V

    .line 268
    .end local v0    # "placeId":Ljava/lang/String;
    :cond_5
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocations:Ljava/util/List;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_6

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocations:Ljava/util/List;

    .line 269
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/iptv/replies/MediaLocation;

    iget-object v1, v1, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;

    sget-object v2, Lru/cn/api/iptv/replies/MediaLocation$Access;->denied:Lru/cn/api/iptv/replies/MediaLocation$Access;

    if-ne v1, v2, :cond_6

    .line 270
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocations:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/iptv/replies/MediaLocation;

    iget-wide v2, v1, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    invoke-direct {p0, v2, v3}, Lru/cn/tv/player/SimplePlayerFragment;->_setContractorId(J)V

    .line 271
    iget-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    iget-wide v4, p0, Lru/cn/tv/player/SimplePlayerFragment;->contractorId:J

    invoke-direct {p0, v2, v3, v4, v5}, Lru/cn/tv/player/SimplePlayerFragment;->loadDeniedLocation(JJ)V

    goto :goto_0

    .line 275
    :cond_6
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lru/cn/player/SimplePlayer;->setPlayerType(I)V

    .line 276
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->_playCurrentChannel()V

    goto/16 :goto_0
.end method

.method public playTelecast(J)V
    .locals 5
    .param p1, "telecastId"    # J

    .prologue
    const/4 v4, 0x0

    .line 295
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->adController:Lru/cn/ad/AdPlayController;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lru/cn/ad/AdPlayController;->setAdsEnabled(Z)V

    .line 298
    iget-boolean v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastId:J

    cmp-long v1, p1, v2

    if-eqz v1, :cond_2

    .line 299
    :cond_0
    iput v4, p0, Lru/cn/tv/player/SimplePlayerFragment;->stoppedPosition:I

    .line 300
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->stop()V

    .line 302
    iput-boolean v4, p0, Lru/cn/tv/player/SimplePlayerFragment;->waitingTelecastInfoWithoutLocations:Z

    .line 303
    invoke-virtual {p0, p1, p2}, Lru/cn/tv/player/SimplePlayerFragment;->setTelecast(J)V

    .line 305
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lru/cn/player/SimplePlayer;->setPlayerType(I)V

    .line 306
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 308
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->castSession:Lcom/google/android/gms/cast/framework/CastSession;

    if-nez v1, :cond_1

    .line 309
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerController:Lru/cn/tv/player/controller/PlayerController;

    invoke-virtual {v1, v4}, Lru/cn/tv/player/controller/PlayerController;->setAlwaysShow(Z)V

    .line 310
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerController:Lru/cn/tv/player/controller/PlayerController;

    invoke-virtual {v1}, Lru/cn/tv/player/controller/PlayerController;->hide()V

    .line 312
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->context:Landroid/content/Context;

    invoke-static {v1, v4}, Lru/cn/ad/AdsManager;->getPreRollPlaceId(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    .line 313
    .local v0, "placeId":Ljava/lang/String;
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->adController:Lru/cn/ad/AdPlayController;

    invoke-virtual {v1, v0}, Lru/cn/ad/AdPlayController;->resetPreRoll(Ljava/lang/String;)V

    .line 316
    .end local v0    # "placeId":Ljava/lang/String;
    :cond_1
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->_playCurrentTelecast()V

    .line 318
    :cond_2
    return-void
.end method

.method protected playing(Z)V
    .locals 1
    .param p1, "playing"    # Z

    .prologue
    .line 1044
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->listener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

    if-eqz v0, :cond_0

    .line 1045
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->listener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

    invoke-interface {v0, p1}, Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;->playing(Z)V

    .line 1047
    :cond_0
    return-void
.end method

.method public resetPositionIfNeeded()V
    .locals 2

    .prologue
    .line 907
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->getPlaybackMode()Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    move-result-object v0

    sget-object v1, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;->ON_AIR:Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    if-ne v0, v1, :cond_0

    iget v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->stoppedPosition:I

    if-lez v0, :cond_0

    .line 908
    const/4 v0, 0x0

    iput v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->stoppedPosition:I

    .line 910
    :cond_0
    return-void
.end method

.method public restartCurrentLocation()V
    .locals 4

    .prologue
    .line 1252
    iget-boolean v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    if-eqz v2, :cond_0

    .line 1253
    const/4 v2, 0x0

    iput-object v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->channelLocations:Ljava/util/List;

    .line 1254
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->_playCurrentChannel()V

    .line 1260
    :goto_0
    return-void

    .line 1256
    :cond_0
    iget-wide v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastId:J

    .line 1257
    .local v0, "currentTelecastId":J
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastId:J

    .line 1258
    invoke-virtual {p0, v0, v1}, Lru/cn/tv/player/SimplePlayerFragment;->playTelecast(J)V

    goto :goto_0
.end method

.method public seekTo(I)V
    .locals 10
    .param p1, "pos"    # I

    .prologue
    const/4 v4, 0x0

    .line 840
    iput v4, p0, Lru/cn/tv/player/SimplePlayerFragment;->stoppedPosition:I

    .line 841
    iget-object v5, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeshifter:Lru/cn/player/timeshift/StreamTimeshifter;

    if-eqz v5, :cond_8

    iget-boolean v5, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    if-eqz v5, :cond_8

    .line 842
    iget-object v5, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastDate:Ljava/util/Calendar;

    if-nez v5, :cond_1

    .line 888
    :cond_0
    :goto_0
    return-void

    .line 846
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-object v5, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastDate:Ljava/util/Calendar;

    .line 847
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    sub-long v2, v6, v8

    .line 848
    .local v2, "elapsedTime":J
    int-to-long v6, p1

    sub-long v6, v2, v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    long-to-int v1, v6

    .line 851
    .local v1, "offset":I
    iget v5, p0, Lru/cn/tv/player/SimplePlayerFragment;->MINIMUM_TIME_SHIFT_OFFSET:I

    if-ge v1, v5, :cond_2

    .line 852
    iget v5, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeShiftOffset:I

    sub-int v0, v1, v5

    .line 853
    .local v0, "change":I
    if-lez v0, :cond_4

    const/16 v5, 0xa

    if-le v0, v5, :cond_4

    .line 854
    iget v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->MINIMUM_TIME_SHIFT_OFFSET:I

    .line 861
    .end local v0    # "change":I
    :cond_2
    :goto_1
    const v5, 0x15180

    if-le v1, v5, :cond_3

    .line 862
    const/4 v1, 0x0

    .line 866
    :cond_3
    iget v5, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeShiftOffset:I

    if-eq v5, v1, :cond_0

    .line 870
    iput v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeShiftOffset:I

    .line 871
    iget v5, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeShiftOffset:I

    if-nez v5, :cond_5

    .line 872
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->goToAir()V

    goto :goto_0

    .line 856
    .restart local v0    # "change":I
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    .line 874
    .end local v0    # "change":I
    :cond_5
    iget-object v5, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeshifter:Lru/cn/player/timeshift/StreamTimeshifter;

    if-eqz v5, :cond_0

    .line 875
    if-nez p1, :cond_6

    const/4 v4, 0x1

    .line 876
    .local v4, "shouldStartOver":Z
    :cond_6
    if-eqz v4, :cond_7

    .line 877
    iget-wide v6, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastId:J

    invoke-static {v6, v7}, Lru/cn/domain/statistics/inetra/InetraTracker;->startoverUsed(J)V

    .line 879
    iget-object v5, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeshifter:Lru/cn/player/timeshift/StreamTimeshifter;

    iget-object v6, p0, Lru/cn/tv/player/SimplePlayerFragment;->context:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lru/cn/player/timeshift/StreamTimeshifter;->startOver(Landroid/content/Context;)V

    goto :goto_0

    .line 881
    :cond_7
    iget-object v5, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeshifter:Lru/cn/player/timeshift/StreamTimeshifter;

    iget-object v6, p0, Lru/cn/tv/player/SimplePlayerFragment;->context:Landroid/content/Context;

    invoke-virtual {v5, v6, v1}, Lru/cn/player/timeshift/StreamTimeshifter;->seekTo(Landroid/content/Context;I)V

    goto :goto_0

    .line 886
    .end local v1    # "offset":I
    .end local v2    # "elapsedTime":J
    .end local v4    # "shouldStartOver":Z
    :cond_8
    iget-object v5, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v5, p1}, Lru/cn/player/SimplePlayer;->seekTo(I)V

    goto :goto_0
.end method

.method public selectMedia(JZ)V
    .locals 3
    .param p1, "mediaId"    # J
    .param p3, "automaticTransition"    # Z

    .prologue
    .line 931
    if-eqz p3, :cond_0

    const/4 v0, 0x1

    .line 933
    .local v0, "mode":I
    :goto_0
    invoke-static {}, Lru/cn/domain/statistics/inetra/InetraTracker;->getViewFrom()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1, v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->setSessionParams(II)V

    .line 935
    invoke-virtual {p0, p1, p2}, Lru/cn/tv/player/SimplePlayerFragment;->playTelecast(J)V

    .line 936
    return-void

    .line 931
    .end local v0    # "mode":I
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setCastSession(Lcom/google/android/gms/cast/framework/CastSession;)V
    .locals 2
    .param p1, "session"    # Lcom/google/android/gms/cast/framework/CastSession;

    .prologue
    .line 1169
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->castSession:Lcom/google/android/gms/cast/framework/CastSession;

    if-eqz v0, :cond_0

    .line 1170
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->castSession:Lcom/google/android/gms/cast/framework/CastSession;

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment;->castListener:Lcom/google/android/gms/cast/Cast$Listener;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/framework/CastSession;->removeCastListener(Lcom/google/android/gms/cast/Cast$Listener;)V

    .line 1173
    :cond_0
    if-eqz p1, :cond_2

    .line 1174
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->castListener:Lcom/google/android/gms/cast/Cast$Listener;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/framework/CastSession;->addCastListener(Lcom/google/android/gms/cast/Cast$Listener;)V

    .line 1182
    :cond_1
    :goto_0
    iput-object p1, p0, Lru/cn/tv/player/SimplePlayerFragment;->castSession:Lcom/google/android/gms/cast/framework/CastSession;

    .line 1183
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0, p1}, Lru/cn/player/SimplePlayer;->setCastSession(Lcom/google/android/gms/cast/framework/CastSession;)V

    .line 1184
    return-void

    .line 1176
    :cond_2
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getPlayerType()I

    move-result v0

    const/16 v1, 0x69

    if-ne v0, v1, :cond_1

    .line 1177
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lru/cn/player/SimplePlayer;->setPlayerType(I)V

    .line 1178
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->stop()V

    goto :goto_0
.end method

.method public setFitMode(Lru/cn/player/SimplePlayer$FitMode;)Z
    .locals 1
    .param p1, "mode"    # Lru/cn/player/SimplePlayer$FitMode;

    .prologue
    .line 996
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getFitMode()Lru/cn/player/SimplePlayer$FitMode;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 997
    const/4 v0, 0x0

    .line 1002
    :goto_0
    return v0

    .line 1000
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0, p1}, Lru/cn/player/SimplePlayer;->setFitMode(Lru/cn/player/SimplePlayer$FitMode;)V

    .line 1001
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->_saveFitMode()V

    .line 1002
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setListener(Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

    .prologue
    .line 411
    iput-object p1, p0, Lru/cn/tv/player/SimplePlayerFragment;->listener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

    .line 412
    return-void
.end method

.method public setMediaController(Lru/cn/tv/player/controller/PlayerController;)V
    .locals 4
    .param p1, "controller"    # Lru/cn/tv/player/controller/PlayerController;

    .prologue
    .line 415
    iput-object p1, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerController:Lru/cn/tv/player/controller/PlayerController;

    .line 416
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerController:Lru/cn/tv/player/controller/PlayerController;

    if-eqz v0, :cond_0

    .line 417
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerController:Lru/cn/tv/player/controller/PlayerController;

    invoke-virtual {v0, p0}, Lru/cn/tv/player/controller/PlayerController;->setMediaPlayer(Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;)V

    .line 418
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerController:Lru/cn/tv/player/controller/PlayerController;

    iget-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastId:J

    invoke-virtual {v0, v2, v3}, Lru/cn/tv/player/controller/PlayerController;->setTelecast(J)V

    .line 420
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 421
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerController:Lru/cn/tv/player/controller/PlayerController;

    iget-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    invoke-virtual {v0, v2, v3}, Lru/cn/tv/player/controller/PlayerController;->setChannel(J)V

    .line 424
    :cond_0
    return-void
.end method

.method public setRemoteController(Lru/cn/peersay/controllers/PlayerRemoteController;)V
    .locals 0
    .param p1, "remoteController"    # Lru/cn/peersay/controllers/PlayerRemoteController;

    .prologue
    .line 1263
    iput-object p1, p0, Lru/cn/tv/player/SimplePlayerFragment;->remoteController:Lru/cn/peersay/controllers/PlayerRemoteController;

    .line 1264
    return-void
.end method

.method protected setTelecast(J)V
    .locals 5
    .param p1, "telecastId"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 280
    invoke-direct {p0, v2, v3}, Lru/cn/tv/player/SimplePlayerFragment;->_setContractorId(J)V

    .line 282
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->onAirOrTimeshift:Z

    .line 284
    iget-wide v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentTelecastId:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    .line 285
    iput-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelId:J

    .line 288
    :cond_0
    invoke-direct {p0, p1, p2}, Lru/cn/tv/player/SimplePlayerFragment;->_setTelecastId(J)V

    .line 289
    return-void
.end method

.method public setVolume(F)V
    .locals 1
    .param p1, "volume"    # F

    .prologue
    .line 1040
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0, p1}, Lru/cn/player/SimplePlayer;->setVolume(F)V

    .line 1041
    return-void
.end method

.method protected showPlayerControlIfMaximize()V
    .locals 0

    .prologue
    .line 1249
    return-void
.end method

.method public final stop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 709
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 711
    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->continuePlaybackAfterAd:Z

    if-eqz v0, :cond_0

    .line 713
    iput-boolean v2, p0, Lru/cn/tv/player/SimplePlayerFragment;->continuePlaybackAfterAd:Z

    .line 714
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->adController:Lru/cn/ad/AdPlayController;

    invoke-virtual {v0}, Lru/cn/ad/AdPlayController;->stop()V

    .line 716
    invoke-virtual {p0, v2}, Lru/cn/tv/player/SimplePlayerFragment;->onAdBreakEnded(Z)V

    .line 717
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerController:Lru/cn/tv/player/controller/PlayerController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lru/cn/tv/player/controller/PlayerController;->setAlwaysShow(Z)V

    .line 718
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragment;->showPlayerControlIfMaximize()V

    .line 721
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->stop()V

    .line 723
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerController:Lru/cn/tv/player/controller/PlayerController;

    if-eqz v0, :cond_1

    .line 724
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerController:Lru/cn/tv/player/controller/PlayerController;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/PlayerController;->reset()V

    .line 727
    :cond_1
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->remoteController:Lru/cn/peersay/controllers/PlayerRemoteController;

    if-eqz v0, :cond_2

    .line 728
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->remoteController:Lru/cn/peersay/controllers/PlayerRemoteController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lru/cn/peersay/controllers/PlayerRemoteController;->setPlayer(Lru/cn/peersay/controllers/PlayerRemoteController$IRemotePlayer;)V

    .line 731
    :cond_2
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeshifter:Lru/cn/player/timeshift/StreamTimeshifter;

    if-eqz v0, :cond_3

    .line 732
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->timeshifter:Lru/cn/player/timeshift/StreamTimeshifter;

    invoke-virtual {v0}, Lru/cn/player/timeshift/StreamTimeshifter;->cancelRequests()V

    .line 734
    :cond_3
    return-void
.end method

.method protected telecastInfoLoaded(Lru/cn/api/tv/replies/Telecast;)V
    .locals 3
    .param p1, "telecast"    # Lru/cn/api/tv/replies/Telecast;

    .prologue
    .line 1058
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->listener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

    if-eqz v0, :cond_0

    .line 1059
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->listener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

    iget-object v1, p1, Lru/cn/api/tv/replies/Telecast;->title:Ljava/lang/String;

    iget-object v2, p1, Lru/cn/api/tv/replies/Telecast;->description:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;->onTelecastInfoLoaded(Ljava/lang/String;Ljava/lang/String;)V

    .line 1061
    :cond_0
    return-void
.end method

.method public zoomIn()V
    .locals 2

    .prologue
    .line 1010
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getFitMode()Lru/cn/player/SimplePlayer$FitMode;

    move-result-object v0

    sget-object v1, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_USER:Lru/cn/player/SimplePlayer$FitMode;

    if-ne v0, v1, :cond_0

    .line 1011
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->zoomIn()V

    .line 1012
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->_saveFitMode()V

    .line 1014
    :cond_0
    return-void
.end method

.method public zoomOut()V
    .locals 2

    .prologue
    .line 1017
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getFitMode()Lru/cn/player/SimplePlayer$FitMode;

    move-result-object v0

    sget-object v1, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_USER:Lru/cn/player/SimplePlayer$FitMode;

    if-ne v0, v1, :cond_0

    .line 1018
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->zoomOut()V

    .line 1019
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;->_saveFitMode()V

    .line 1021
    :cond_0
    return-void
.end method
