.class public interface abstract Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;
.super Ljava/lang/Object;
.source "NativeAdAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/ad/natives/adapters/NativeAdAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ViewBinder"
.end annotation


# virtual methods
.method public abstract bind(Landroid/widget/Button;I)Z
.end method

.method public abstract bind(Landroid/widget/ImageView;I)Z
.end method

.method public abstract bind(Landroid/widget/TextView;I)Z
.end method

.method public abstract getAdType()I
.end method

.method public abstract registerTemplate(Landroid/view/View;Ljava/util/List;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation
.end method
