.class final enum Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;
.super Ljava/lang/Enum;
.source "WapStartRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/ad/video/Wapstart/WapStartRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "VideoType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;

.field public static final enum INTERSTITIAL:Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;

.field public static final enum IN_STREAM:Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 26
    new-instance v0, Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;

    const-string v1, "INTERSTITIAL"

    invoke-direct {v0, v1, v3, v2}, Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;->INTERSTITIAL:Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;

    new-instance v0, Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;

    const-string v1, "IN_STREAM"

    invoke-direct {v0, v1, v2, v4}, Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;->IN_STREAM:Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;

    .line 25
    new-array v0, v4, [Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;

    sget-object v1, Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;->INTERSTITIAL:Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;

    aput-object v1, v0, v3

    sget-object v1, Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;->IN_STREAM:Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;

    aput-object v1, v0, v2

    sput-object v0, Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;->$VALUES:[Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 30
    iput p3, p0, Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;->value:I

    .line 31
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 25
    const-class v0, Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;

    return-object v0
.end method

.method public static values()[Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;->$VALUES:[Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;

    invoke-virtual {v0}, [Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;->value:I

    return v0
.end method
