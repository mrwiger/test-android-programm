.class public final Lru/cn/domain/tv/CurrentCategory$$Factory;
.super Ljava/lang/Object;
.source "CurrentCategory$$Factory.java"

# interfaces
.implements Ltoothpick/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ltoothpick/Factory",
        "<",
        "Lru/cn/domain/tv/CurrentCategory;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic createInstance(Ltoothpick/Scope;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7
    invoke-virtual {p0, p1}, Lru/cn/domain/tv/CurrentCategory$$Factory;->createInstance(Ltoothpick/Scope;)Lru/cn/domain/tv/CurrentCategory;

    move-result-object v0

    return-object v0
.end method

.method public createInstance(Ltoothpick/Scope;)Lru/cn/domain/tv/CurrentCategory;
    .locals 1
    .param p1, "scope"    # Ltoothpick/Scope;

    .prologue
    .line 10
    new-instance v0, Lru/cn/domain/tv/CurrentCategory;

    invoke-direct {v0}, Lru/cn/domain/tv/CurrentCategory;-><init>()V

    .line 11
    .local v0, "currentCategory":Lru/cn/domain/tv/CurrentCategory;
    return-object v0
.end method

.method public getTargetScope(Ltoothpick/Scope;)Ltoothpick/Scope;
    .locals 0
    .param p1, "scope"    # Ltoothpick/Scope;

    .prologue
    .line 16
    return-object p1
.end method

.method public hasProvidesSingletonInScopeAnnotation()Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    return v0
.end method

.method public hasScopeAnnotation()Z
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    return v0
.end method
