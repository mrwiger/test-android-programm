.class public final Lcom/google/obf/am;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/al;
.implements Lcom/google/obf/dh$a;
.implements Lcom/google/obf/u;
.implements Lcom/google/obf/u$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/am$c;,
        Lcom/google/obf/am$b;,
        Lcom/google/obf/am$d;,
        Lcom/google/obf/am$e;,
        Lcom/google/obf/am$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/obf/aj;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private A:J

.field private B:J

.field private C:Lcom/google/obf/dh;

.field private D:Lcom/google/obf/am$b;

.field private E:Ljava/io/IOException;

.field private F:I

.field private G:J

.field private H:Z

.field private I:I

.field private J:I

.field private final b:Lcom/google/obf/am$c;

.field private final c:Lcom/google/obf/cx;

.field private final d:I

.field private final e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/obf/am$d;",
            ">;"
        }
    .end annotation
.end field

.field private final f:I

.field private final g:Landroid/net/Uri;

.field private final h:Lcom/google/obf/da;

.field private final i:Landroid/os/Handler;

.field private final j:Lcom/google/obf/am$a;

.field private final k:I

.field private volatile l:Z

.field private volatile m:Lcom/google/obf/aq;

.field private volatile n:Lcom/google/obf/ab;

.field private o:Z

.field private p:I

.field private q:[Lcom/google/obf/q;

.field private r:J

.field private s:[Z

.field private t:[Z

.field private u:[Z

.field private v:I

.field private w:J

.field private x:J

.field private y:J

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 259
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/obf/am;->a:Ljava/util/List;

    .line 260
    :try_start_0
    sget-object v0, Lcom/google/obf/am;->a:Ljava/util/List;

    const-string v1, "com.google.obf.cu"

    .line 261
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/obf/aj;

    .line 262
    invoke-virtual {v1, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    .line 263
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_a

    .line 266
    :goto_0
    :try_start_1
    sget-object v0, Lcom/google/obf/am;->a:Ljava/util/List;

    const-string v1, "com.google.obf.bg"

    .line 267
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/obf/aj;

    .line 268
    invoke-virtual {v1, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    .line 269
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_9

    .line 272
    :goto_1
    :try_start_2
    sget-object v0, Lcom/google/obf/am;->a:Ljava/util/List;

    const-string v1, "com.google.obf.bh"

    .line 273
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/obf/aj;

    .line 274
    invoke-virtual {v1, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    .line 275
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_8

    .line 278
    :goto_2
    :try_start_3
    sget-object v0, Lcom/google/obf/am;->a:Ljava/util/List;

    const-string v1, "com.google.obf.az"

    .line 279
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/obf/aj;

    .line 280
    invoke-virtual {v1, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    .line 281
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_7

    .line 284
    :goto_3
    :try_start_4
    sget-object v0, Lcom/google/obf/am;->a:Ljava/util/List;

    const-string v1, "com.google.obf.by"

    .line 285
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/obf/aj;

    .line 286
    invoke-virtual {v1, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    .line 287
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4 .. :try_end_4} :catch_6

    .line 290
    :goto_4
    :try_start_5
    sget-object v0, Lcom/google/obf/am;->a:Ljava/util/List;

    const-string v1, "com.google.obf.cl"

    .line 291
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/obf/aj;

    .line 292
    invoke-virtual {v1, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    .line 293
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/lang/ClassNotFoundException; {:try_start_5 .. :try_end_5} :catch_5

    .line 296
    :goto_5
    :try_start_6
    sget-object v0, Lcom/google/obf/am;->a:Ljava/util/List;

    const-string v1, "com.google.obf.at"

    .line 297
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/obf/aj;

    .line 298
    invoke-virtual {v1, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    .line 299
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Ljava/lang/ClassNotFoundException; {:try_start_6 .. :try_end_6} :catch_4

    .line 302
    :goto_6
    :try_start_7
    sget-object v0, Lcom/google/obf/am;->a:Ljava/util/List;

    const-string v1, "com.google.obf.bp"

    .line 303
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/obf/aj;

    .line 304
    invoke-virtual {v1, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    .line 305
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Ljava/lang/ClassNotFoundException; {:try_start_7 .. :try_end_7} :catch_3

    .line 308
    :goto_7
    :try_start_8
    sget-object v0, Lcom/google/obf/am;->a:Ljava/util/List;

    const-string v1, "com.google.obf.ci"

    .line 309
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/obf/aj;

    .line 310
    invoke-virtual {v1, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    .line 311
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_8
    .catch Ljava/lang/ClassNotFoundException; {:try_start_8 .. :try_end_8} :catch_2

    .line 314
    :goto_8
    :try_start_9
    sget-object v0, Lcom/google/obf/am;->a:Ljava/util/List;

    const-string v1, "com.google.obf.cm"

    .line 315
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/obf/aj;

    .line 316
    invoke-virtual {v1, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    .line 317
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Ljava/lang/ClassNotFoundException; {:try_start_9 .. :try_end_9} :catch_1

    .line 320
    :goto_9
    :try_start_a
    sget-object v0, Lcom/google/obf/am;->a:Ljava/util/List;

    const-string v1, "com.google.ads.interactivemedia.v3.exoplayer.ext.flac.FlacExtractor"

    .line 321
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/obf/aj;

    .line 322
    invoke-virtual {v1, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    .line 323
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_a
    .catch Ljava/lang/ClassNotFoundException; {:try_start_a .. :try_end_a} :catch_0

    .line 326
    :goto_a
    return-void

    .line 325
    :catch_0
    move-exception v0

    goto :goto_a

    .line 319
    :catch_1
    move-exception v0

    goto :goto_9

    .line 313
    :catch_2
    move-exception v0

    goto :goto_8

    .line 307
    :catch_3
    move-exception v0

    goto :goto_7

    .line 301
    :catch_4
    move-exception v0

    goto :goto_6

    .line 295
    :catch_5
    move-exception v0

    goto :goto_5

    .line 289
    :catch_6
    move-exception v0

    goto :goto_4

    .line 283
    :catch_7
    move-exception v0

    goto/16 :goto_3

    .line 277
    :catch_8
    move-exception v0

    goto/16 :goto_2

    .line 271
    :catch_9
    move-exception v0

    goto/16 :goto_1

    .line 265
    :catch_a
    move-exception v0

    goto/16 :goto_0
.end method

.method public varargs constructor <init>(Landroid/net/Uri;Lcom/google/obf/da;Lcom/google/obf/cx;IILandroid/os/Handler;Lcom/google/obf/am$a;I[Lcom/google/obf/aj;)V
    .locals 3

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lcom/google/obf/am;->g:Landroid/net/Uri;

    .line 5
    iput-object p2, p0, Lcom/google/obf/am;->h:Lcom/google/obf/da;

    .line 6
    iput-object p7, p0, Lcom/google/obf/am;->j:Lcom/google/obf/am$a;

    .line 7
    iput-object p6, p0, Lcom/google/obf/am;->i:Landroid/os/Handler;

    .line 8
    iput p8, p0, Lcom/google/obf/am;->k:I

    .line 9
    iput-object p3, p0, Lcom/google/obf/am;->c:Lcom/google/obf/cx;

    .line 10
    iput p4, p0, Lcom/google/obf/am;->d:I

    .line 11
    iput p5, p0, Lcom/google/obf/am;->f:I

    .line 12
    if-eqz p9, :cond_0

    array-length v0, p9

    if-nez v0, :cond_1

    .line 13
    :cond_0
    sget-object v0, Lcom/google/obf/am;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array p9, v0, [Lcom/google/obf/aj;

    .line 14
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, p9

    if-ge v1, v0, :cond_1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/obf/am;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/aj;

    aput-object v0, p9, v1
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 21
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 17
    :catch_0
    move-exception v0

    .line 18
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unexpected error creating default extractor"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 19
    :catch_1
    move-exception v0

    .line 20
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unexpected error creating default extractor"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 22
    :cond_1
    new-instance v0, Lcom/google/obf/am$c;

    invoke-direct {v0, p9, p0}, Lcom/google/obf/am$c;-><init>([Lcom/google/obf/aj;Lcom/google/obf/al;)V

    iput-object v0, p0, Lcom/google/obf/am;->b:Lcom/google/obf/am$c;

    .line 23
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/obf/am;->e:Landroid/util/SparseArray;

    .line 24
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/obf/am;->y:J

    .line 25
    return-void
.end method

.method public varargs constructor <init>(Landroid/net/Uri;Lcom/google/obf/da;Lcom/google/obf/cx;ILandroid/os/Handler;Lcom/google/obf/am$a;I[Lcom/google/obf/aj;)V
    .locals 10

    .prologue
    .line 1
    const/4 v5, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/google/obf/am;-><init>(Landroid/net/Uri;Lcom/google/obf/da;Lcom/google/obf/cx;IILandroid/os/Handler;Lcom/google/obf/am$a;I[Lcom/google/obf/aj;)V

    .line 2
    return-void
.end method

.method static synthetic a(Lcom/google/obf/am;)Lcom/google/obf/am$c;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/obf/am;->b:Lcom/google/obf/am$c;

    return-object v0
.end method

.method private a(Ljava/io/IOException;)V
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/obf/am;->i:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/am;->j:Lcom/google/obf/am$a;

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/google/obf/am;->i:Landroid/os/Handler;

    new-instance v1, Lcom/google/obf/am$2;

    invoke-direct {v1, p0, p1}, Lcom/google/obf/am$2;-><init>(Lcom/google/obf/am;Ljava/io/IOException;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 254
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/obf/am;)I
    .locals 1

    .prologue
    .line 256
    iget v0, p0, Lcom/google/obf/am;->k:I

    return v0
.end method

.method static synthetic c(Lcom/google/obf/am;)Lcom/google/obf/am$a;
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/obf/am;->j:Lcom/google/obf/am$a;

    return-object v0
.end method

.method private c(J)V
    .locals 1

    .prologue
    .line 183
    iput-wide p1, p0, Lcom/google/obf/am;->y:J

    .line 184
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/am;->H:Z

    .line 185
    iget-object v0, p0, Lcom/google/obf/am;->C:Lcom/google/obf/dh;

    invoke-virtual {v0}, Lcom/google/obf/dh;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/google/obf/am;->C:Lcom/google/obf/dh;

    invoke-virtual {v0}, Lcom/google/obf/dh;->b()V

    .line 189
    :goto_0
    return-void

    .line 187
    :cond_0
    invoke-direct {p0}, Lcom/google/obf/am;->j()V

    .line 188
    invoke-direct {p0}, Lcom/google/obf/am;->g()V

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/obf/am;)I
    .locals 2

    .prologue
    .line 258
    iget v0, p0, Lcom/google/obf/am;->I:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/obf/am;->I:I

    return v0
.end method

.method private d(J)Lcom/google/obf/am$b;
    .locals 9

    .prologue
    .line 229
    new-instance v0, Lcom/google/obf/am$b;

    iget-object v1, p0, Lcom/google/obf/am;->g:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/obf/am;->h:Lcom/google/obf/da;

    iget-object v3, p0, Lcom/google/obf/am;->b:Lcom/google/obf/am$c;

    iget-object v4, p0, Lcom/google/obf/am;->c:Lcom/google/obf/cx;

    iget v5, p0, Lcom/google/obf/am;->d:I

    iget-object v6, p0, Lcom/google/obf/am;->m:Lcom/google/obf/aq;

    .line 230
    invoke-interface {v6, p1, p2}, Lcom/google/obf/aq;->b(J)J

    move-result-wide v6

    invoke-direct/range {v0 .. v7}, Lcom/google/obf/am$b;-><init>(Landroid/net/Uri;Lcom/google/obf/da;Lcom/google/obf/am$c;Lcom/google/obf/cx;IJ)V

    .line 231
    return-object v0
.end method

.method private e(J)V
    .locals 3

    .prologue
    .line 237
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/obf/am;->u:[Z

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 238
    iget-object v0, p0, Lcom/google/obf/am;->u:[Z

    aget-boolean v0, v0, v1

    if-nez v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/google/obf/am;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/am$d;

    invoke-virtual {v0, p1, p2}, Lcom/google/obf/am$d;->a(J)V

    .line 240
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 241
    :cond_1
    return-void
.end method

.method private f(J)J
    .locals 5

    .prologue
    .line 251
    const-wide/16 v0, 0x1

    sub-long v0, p1, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    const-wide/16 v2, 0x1388

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private g()V
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const-wide/high16 v6, -0x8000000000000000L

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 190
    iget-boolean v0, p0, Lcom/google/obf/am;->H:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/am;->C:Lcom/google/obf/dh;

    invoke-virtual {v0}, Lcom/google/obf/dh;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 227
    :cond_0
    :goto_0
    return-void

    .line 192
    :cond_1
    iget-object v0, p0, Lcom/google/obf/am;->E:Ljava/io/IOException;

    if-eqz v0, :cond_7

    .line 193
    invoke-direct {p0}, Lcom/google/obf/am;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/google/obf/am;->D:Lcom/google/obf/am$b;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 196
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/obf/am;->G:J

    sub-long/2addr v4, v6

    .line 197
    iget v0, p0, Lcom/google/obf/am;->F:I

    int-to-long v6, v0

    invoke-direct {p0, v6, v7}, Lcom/google/obf/am;->f(J)J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-ltz v0, :cond_0

    .line 198
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/am;->E:Ljava/io/IOException;

    .line 199
    iget-boolean v0, p0, Lcom/google/obf/am;->o:Z

    if-nez v0, :cond_5

    .line 200
    :goto_2
    iget-object v0, p0, Lcom/google/obf/am;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 201
    iget-object v0, p0, Lcom/google/obf/am;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/am$d;

    invoke-virtual {v0}, Lcom/google/obf/am$d;->a()V

    .line 202
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    move v0, v2

    .line 195
    goto :goto_1

    .line 203
    :cond_3
    invoke-direct {p0}, Lcom/google/obf/am;->h()Lcom/google/obf/am$b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/am;->D:Lcom/google/obf/am$b;

    .line 211
    :cond_4
    :goto_3
    iget v0, p0, Lcom/google/obf/am;->I:I

    iput v0, p0, Lcom/google/obf/am;->J:I

    .line 212
    iget-object v0, p0, Lcom/google/obf/am;->C:Lcom/google/obf/dh;

    iget-object v1, p0, Lcom/google/obf/am;->D:Lcom/google/obf/am$b;

    invoke-virtual {v0, v1, p0}, Lcom/google/obf/dh;->a(Lcom/google/obf/dh$c;Lcom/google/obf/dh$a;)V

    goto :goto_0

    .line 204
    :cond_5
    iget-object v0, p0, Lcom/google/obf/am;->m:Lcom/google/obf/aq;

    invoke-interface {v0}, Lcom/google/obf/aq;->a()Z

    move-result v0

    if-nez v0, :cond_4

    iget-wide v4, p0, Lcom/google/obf/am;->r:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_4

    .line 205
    :goto_4
    iget-object v0, p0, Lcom/google/obf/am;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 206
    iget-object v0, p0, Lcom/google/obf/am;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/am$d;

    invoke-virtual {v0}, Lcom/google/obf/am$d;->a()V

    .line 207
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 208
    :cond_6
    invoke-direct {p0}, Lcom/google/obf/am;->h()Lcom/google/obf/am$b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/am;->D:Lcom/google/obf/am$b;

    .line 209
    iget-wide v2, p0, Lcom/google/obf/am;->w:J

    iput-wide v2, p0, Lcom/google/obf/am;->A:J

    .line 210
    iput-boolean v1, p0, Lcom/google/obf/am;->z:Z

    goto :goto_3

    .line 214
    :cond_7
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/google/obf/am;->B:J

    .line 215
    iput-boolean v2, p0, Lcom/google/obf/am;->z:Z

    .line 216
    iget-boolean v0, p0, Lcom/google/obf/am;->o:Z

    if-nez v0, :cond_8

    .line 217
    invoke-direct {p0}, Lcom/google/obf/am;->h()Lcom/google/obf/am$b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/am;->D:Lcom/google/obf/am$b;

    .line 225
    :goto_5
    iget v0, p0, Lcom/google/obf/am;->I:I

    iput v0, p0, Lcom/google/obf/am;->J:I

    .line 226
    iget-object v0, p0, Lcom/google/obf/am;->C:Lcom/google/obf/dh;

    iget-object v1, p0, Lcom/google/obf/am;->D:Lcom/google/obf/am$b;

    invoke-virtual {v0, v1, p0}, Lcom/google/obf/dh;->a(Lcom/google/obf/dh$c;Lcom/google/obf/dh$a;)V

    goto/16 :goto_0

    .line 218
    :cond_8
    invoke-direct {p0}, Lcom/google/obf/am;->k()Z

    move-result v0

    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 219
    iget-wide v2, p0, Lcom/google/obf/am;->r:J

    cmp-long v0, v2, v8

    if-eqz v0, :cond_9

    iget-wide v2, p0, Lcom/google/obf/am;->y:J

    iget-wide v4, p0, Lcom/google/obf/am;->r:J

    cmp-long v0, v2, v4

    if-ltz v0, :cond_9

    .line 220
    iput-boolean v1, p0, Lcom/google/obf/am;->H:Z

    .line 221
    iput-wide v6, p0, Lcom/google/obf/am;->y:J

    goto/16 :goto_0

    .line 223
    :cond_9
    iget-wide v0, p0, Lcom/google/obf/am;->y:J

    invoke-direct {p0, v0, v1}, Lcom/google/obf/am;->d(J)Lcom/google/obf/am$b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/am;->D:Lcom/google/obf/am$b;

    .line 224
    iput-wide v6, p0, Lcom/google/obf/am;->y:J

    goto :goto_5
.end method

.method private h()Lcom/google/obf/am$b;
    .locals 8

    .prologue
    .line 228
    new-instance v0, Lcom/google/obf/am$b;

    iget-object v1, p0, Lcom/google/obf/am;->g:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/obf/am;->h:Lcom/google/obf/da;

    iget-object v3, p0, Lcom/google/obf/am;->b:Lcom/google/obf/am$c;

    iget-object v4, p0, Lcom/google/obf/am;->c:Lcom/google/obf/cx;

    iget v5, p0, Lcom/google/obf/am;->d:I

    const-wide/16 v6, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/obf/am$b;-><init>(Landroid/net/Uri;Lcom/google/obf/da;Lcom/google/obf/am$c;Lcom/google/obf/cx;IJ)V

    return-object v0
.end method

.method private i()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 232
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/obf/am;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 233
    iget-object v0, p0, Lcom/google/obf/am;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/am$d;

    invoke-virtual {v0}, Lcom/google/obf/am$d;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 236
    :goto_1
    return v2

    .line 235
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 236
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method private j()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 242
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/obf/am;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/google/obf/am;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/am$d;

    invoke-virtual {v0}, Lcom/google/obf/am$d;->a()V

    .line 244
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 245
    :cond_0
    iput-object v3, p0, Lcom/google/obf/am;->D:Lcom/google/obf/am$b;

    .line 246
    iput-object v3, p0, Lcom/google/obf/am;->E:Ljava/io/IOException;

    .line 247
    iput v2, p0, Lcom/google/obf/am;->F:I

    .line 248
    return-void
.end method

.method private k()Z
    .locals 4

    .prologue
    .line 249
    iget-wide v0, p0, Lcom/google/obf/am;->y:J

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l()Z
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/obf/am;->E:Ljava/io/IOException;

    instance-of v0, v0, Lcom/google/obf/am$e;

    return v0
.end method


# virtual methods
.method public a(IJLcom/google/obf/r;Lcom/google/obf/t;)I
    .locals 6

    .prologue
    const/4 v2, -0x2

    const/4 v1, 0x0

    .line 89
    iput-wide p2, p0, Lcom/google/obf/am;->w:J

    .line 90
    iget-object v0, p0, Lcom/google/obf/am;->t:[Z

    aget-boolean v0, v0, p1

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/obf/am;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    .line 108
    :goto_0
    return v0

    .line 92
    :cond_1
    iget-object v0, p0, Lcom/google/obf/am;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/am$d;

    .line 93
    iget-object v3, p0, Lcom/google/obf/am;->s:[Z

    aget-boolean v3, v3, p1

    if-eqz v3, :cond_2

    .line 94
    invoke-virtual {v0}, Lcom/google/obf/am$d;->c()Lcom/google/obf/q;

    move-result-object v0

    iput-object v0, p4, Lcom/google/obf/r;->a:Lcom/google/obf/q;

    .line 95
    iget-object v0, p0, Lcom/google/obf/am;->n:Lcom/google/obf/ab;

    iput-object v0, p4, Lcom/google/obf/r;->b:Lcom/google/obf/ab;

    .line 96
    iget-object v0, p0, Lcom/google/obf/am;->s:[Z

    aput-boolean v1, v0, p1

    .line 97
    const/4 v0, -0x4

    goto :goto_0

    .line 98
    :cond_2
    invoke-virtual {v0, p5}, Lcom/google/obf/am$d;->a(Lcom/google/obf/t;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 99
    iget-wide v2, p5, Lcom/google/obf/t;->e:J

    iget-wide v4, p0, Lcom/google/obf/am;->x:J

    cmp-long v0, v2, v4

    if-gez v0, :cond_4

    const/4 v0, 0x1

    .line 100
    :goto_1
    iget v2, p5, Lcom/google/obf/t;->d:I

    if-eqz v0, :cond_5

    const/high16 v0, 0x8000000

    :goto_2
    or-int/2addr v0, v2

    iput v0, p5, Lcom/google/obf/t;->d:I

    .line 101
    iget-boolean v0, p0, Lcom/google/obf/am;->z:Z

    if-eqz v0, :cond_3

    .line 102
    iget-wide v2, p0, Lcom/google/obf/am;->A:J

    iget-wide v4, p5, Lcom/google/obf/t;->e:J

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/obf/am;->B:J

    .line 103
    iput-boolean v1, p0, Lcom/google/obf/am;->z:Z

    .line 104
    :cond_3
    iget-wide v0, p5, Lcom/google/obf/t;->e:J

    iget-wide v2, p0, Lcom/google/obf/am;->B:J

    add-long/2addr v0, v2

    iput-wide v0, p5, Lcom/google/obf/t;->e:J

    .line 105
    const/4 v0, -0x3

    goto :goto_0

    :cond_4
    move v0, v1

    .line 99
    goto :goto_1

    :cond_5
    move v0, v1

    .line 100
    goto :goto_2

    .line 106
    :cond_6
    iget-boolean v0, p0, Lcom/google/obf/am;->H:Z

    if-eqz v0, :cond_7

    .line 107
    const/4 v0, -0x1

    goto :goto_0

    :cond_7
    move v0, v2

    .line 108
    goto :goto_0
.end method

.method public a(I)Lcom/google/obf/q;
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/obf/am;->o:Z

    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 51
    iget-object v0, p0, Lcom/google/obf/am;->q:[Lcom/google/obf/q;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public a()Lcom/google/obf/u$a;
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/google/obf/am;->v:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/am;->v:I

    .line 27
    return-object p0
.end method

.method public a(IJ)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 52
    iget-boolean v0, p0, Lcom/google/obf/am;->o:Z

    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 53
    iget-object v0, p0, Lcom/google/obf/am;->u:[Z

    aget-boolean v0, v0, p1

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 54
    iget v0, p0, Lcom/google/obf/am;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/am;->p:I

    .line 55
    iget-object v0, p0, Lcom/google/obf/am;->u:[Z

    aput-boolean v1, v0, p1

    .line 56
    iget-object v0, p0, Lcom/google/obf/am;->s:[Z

    aput-boolean v1, v0, p1

    .line 57
    iget-object v0, p0, Lcom/google/obf/am;->t:[Z

    aput-boolean v2, v0, p1

    .line 58
    iget v0, p0, Lcom/google/obf/am;->p:I

    if-ne v0, v1, :cond_1

    .line 59
    iget-object v0, p0, Lcom/google/obf/am;->m:Lcom/google/obf/aq;

    invoke-interface {v0}, Lcom/google/obf/aq;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 p2, 0x0

    .line 60
    :cond_0
    iput-wide p2, p0, Lcom/google/obf/am;->w:J

    .line 61
    iput-wide p2, p0, Lcom/google/obf/am;->x:J

    .line 62
    invoke-direct {p0, p2, p3}, Lcom/google/obf/am;->c(J)V

    .line 63
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 53
    goto :goto_0
.end method

.method public a(Lcom/google/obf/ab;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/google/obf/am;->n:Lcom/google/obf/ab;

    .line 182
    return-void
.end method

.method public a(Lcom/google/obf/aq;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/google/obf/am;->m:Lcom/google/obf/aq;

    .line 180
    return-void
.end method

.method public a(Lcom/google/obf/dh$c;)V
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/am;->H:Z

    .line 159
    return-void
.end method

.method public a(Lcom/google/obf/dh$c;Ljava/io/IOException;)V
    .locals 2

    .prologue
    .line 165
    iput-object p2, p0, Lcom/google/obf/am;->E:Ljava/io/IOException;

    .line 166
    iget v0, p0, Lcom/google/obf/am;->I:I

    iget v1, p0, Lcom/google/obf/am;->J:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    .line 167
    :goto_0
    iput v0, p0, Lcom/google/obf/am;->F:I

    .line 168
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/obf/am;->G:J

    .line 169
    invoke-direct {p0, p2}, Lcom/google/obf/am;->a(Ljava/io/IOException;)V

    .line 170
    invoke-direct {p0}, Lcom/google/obf/am;->g()V

    .line 171
    return-void

    .line 167
    :cond_0
    iget v0, p0, Lcom/google/obf/am;->F:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a(J)Z
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 28
    iget-boolean v2, p0, Lcom/google/obf/am;->o:Z

    if-eqz v2, :cond_1

    move v0, v1

    .line 48
    :cond_0
    :goto_0
    return v0

    .line 30
    :cond_1
    iget-object v2, p0, Lcom/google/obf/am;->C:Lcom/google/obf/dh;

    if-nez v2, :cond_2

    .line 31
    new-instance v2, Lcom/google/obf/dh;

    const-string v3, "Loader:ExtractorSampleSource"

    invoke-direct {v2, v3}, Lcom/google/obf/dh;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/obf/am;->C:Lcom/google/obf/dh;

    .line 32
    :cond_2
    invoke-direct {p0}, Lcom/google/obf/am;->g()V

    .line 33
    iget-object v2, p0, Lcom/google/obf/am;->m:Lcom/google/obf/aq;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/obf/am;->l:Z

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/google/obf/am;->i()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 34
    iget-object v2, p0, Lcom/google/obf/am;->e:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v3

    .line 35
    new-array v2, v3, [Z

    iput-object v2, p0, Lcom/google/obf/am;->u:[Z

    .line 36
    new-array v2, v3, [Z

    iput-object v2, p0, Lcom/google/obf/am;->t:[Z

    .line 37
    new-array v2, v3, [Z

    iput-object v2, p0, Lcom/google/obf/am;->s:[Z

    .line 38
    new-array v2, v3, [Lcom/google/obf/q;

    iput-object v2, p0, Lcom/google/obf/am;->q:[Lcom/google/obf/q;

    .line 39
    iput-wide v8, p0, Lcom/google/obf/am;->r:J

    move v2, v0

    .line 40
    :goto_1
    if-ge v2, v3, :cond_4

    .line 41
    iget-object v0, p0, Lcom/google/obf/am;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/am$d;

    invoke-virtual {v0}, Lcom/google/obf/am$d;->c()Lcom/google/obf/q;

    move-result-object v0

    .line 42
    iget-object v4, p0, Lcom/google/obf/am;->q:[Lcom/google/obf/q;

    aput-object v0, v4, v2

    .line 43
    iget-wide v4, v0, Lcom/google/obf/q;->e:J

    cmp-long v4, v4, v8

    if-eqz v4, :cond_3

    iget-wide v4, v0, Lcom/google/obf/q;->e:J

    iget-wide v6, p0, Lcom/google/obf/am;->r:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    .line 44
    iget-wide v4, v0, Lcom/google/obf/q;->e:J

    iput-wide v4, p0, Lcom/google/obf/am;->r:J

    .line 45
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 46
    :cond_4
    iput-boolean v1, p0, Lcom/google/obf/am;->o:Z

    move v0, v1

    .line 47
    goto :goto_0
.end method

.method public b(I)J
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/obf/am;->t:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/google/obf/am;->t:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, p1

    .line 87
    iget-wide v0, p0, Lcom/google/obf/am;->x:J

    .line 88
    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_0
.end method

.method public b()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/obf/am;->E:Ljava/io/IOException;

    if-nez v0, :cond_1

    .line 120
    :cond_0
    return-void

    .line 111
    :cond_1
    invoke-direct {p0}, Lcom/google/obf/am;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 112
    iget-object v0, p0, Lcom/google/obf/am;->E:Ljava/io/IOException;

    throw v0

    .line 113
    :cond_2
    iget v0, p0, Lcom/google/obf/am;->f:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    .line 114
    iget v0, p0, Lcom/google/obf/am;->f:I

    .line 118
    :goto_0
    iget v1, p0, Lcom/google/obf/am;->F:I

    if-le v1, v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/google/obf/am;->E:Ljava/io/IOException;

    throw v0

    .line 115
    :cond_3
    iget-object v0, p0, Lcom/google/obf/am;->m:Lcom/google/obf/aq;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/obf/am;->m:Lcom/google/obf/aq;

    invoke-interface {v0}, Lcom/google/obf/aq;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 116
    const/4 v0, 0x6

    goto :goto_0

    .line 117
    :cond_4
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public b(J)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 121
    iget-boolean v0, p0, Lcom/google/obf/am;->o:Z

    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 122
    iget v0, p0, Lcom/google/obf/am;->p:I

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 123
    iget-object v0, p0, Lcom/google/obf/am;->m:Lcom/google/obf/aq;

    invoke-interface {v0}, Lcom/google/obf/aq;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 p1, 0x0

    .line 124
    :cond_0
    invoke-direct {p0}, Lcom/google/obf/am;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-wide v4, p0, Lcom/google/obf/am;->y:J

    .line 125
    :goto_1
    iput-wide p1, p0, Lcom/google/obf/am;->w:J

    .line 126
    iput-wide p1, p0, Lcom/google/obf/am;->x:J

    .line 127
    cmp-long v0, v4, p1

    if-nez v0, :cond_4

    .line 138
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 122
    goto :goto_0

    .line 124
    :cond_3
    iget-wide v4, p0, Lcom/google/obf/am;->w:J

    goto :goto_1

    .line 129
    :cond_4
    invoke-direct {p0}, Lcom/google/obf/am;->k()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    move v3, v2

    move v4, v0

    .line 130
    :goto_3
    if-eqz v4, :cond_6

    iget-object v0, p0, Lcom/google/obf/am;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v3, v0, :cond_6

    .line 131
    iget-object v0, p0, Lcom/google/obf/am;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/am$d;

    invoke-virtual {v0, p1, p2}, Lcom/google/obf/am$d;->b(J)Z

    move-result v0

    and-int/2addr v4, v0

    .line 132
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_5
    move v0, v2

    .line 129
    goto :goto_2

    .line 133
    :cond_6
    if-nez v4, :cond_7

    .line 134
    invoke-direct {p0, p1, p2}, Lcom/google/obf/am;->c(J)V

    .line 135
    :cond_7
    :goto_4
    iget-object v0, p0, Lcom/google/obf/am;->t:[Z

    array-length v0, v0

    if-ge v2, v0, :cond_1

    .line 136
    iget-object v0, p0, Lcom/google/obf/am;->t:[Z

    aput-boolean v1, v0, v2

    .line 137
    add-int/lit8 v2, v2, 0x1

    goto :goto_4
.end method

.method public b(Lcom/google/obf/dh$c;)V
    .locals 2

    .prologue
    .line 160
    iget v0, p0, Lcom/google/obf/am;->p:I

    if-lez v0, :cond_0

    .line 161
    iget-wide v0, p0, Lcom/google/obf/am;->y:J

    invoke-direct {p0, v0, v1}, Lcom/google/obf/am;->c(J)V

    .line 164
    :goto_0
    return-void

    .line 162
    :cond_0
    invoke-direct {p0}, Lcom/google/obf/am;->j()V

    .line 163
    iget-object v0, p0, Lcom/google/obf/am;->c:Lcom/google/obf/cx;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/obf/cx;->a(I)V

    goto :goto_0
.end method

.method public b(IJ)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 75
    iget-boolean v0, p0, Lcom/google/obf/am;->o:Z

    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 76
    iget-object v0, p0, Lcom/google/obf/am;->u:[Z

    aget-boolean v0, v0, p1

    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 77
    iput-wide p2, p0, Lcom/google/obf/am;->w:J

    .line 78
    iget-wide v4, p0, Lcom/google/obf/am;->w:J

    invoke-direct {p0, v4, v5}, Lcom/google/obf/am;->e(J)V

    .line 79
    iget-boolean v0, p0, Lcom/google/obf/am;->H:Z

    if-eqz v0, :cond_0

    .line 84
    :goto_0
    return v1

    .line 81
    :cond_0
    invoke-direct {p0}, Lcom/google/obf/am;->g()V

    .line 82
    invoke-direct {p0}, Lcom/google/obf/am;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v2

    .line 83
    goto :goto_0

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/google/obf/am;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/am$d;

    invoke-virtual {v0}, Lcom/google/obf/am$d;->e()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public c()I
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/obf/am;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    return v0
.end method

.method public c(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 64
    iget-boolean v0, p0, Lcom/google/obf/am;->o:Z

    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 65
    iget-object v0, p0, Lcom/google/obf/am;->u:[Z

    aget-boolean v0, v0, p1

    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 66
    iget v0, p0, Lcom/google/obf/am;->p:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/obf/am;->p:I

    .line 67
    iget-object v0, p0, Lcom/google/obf/am;->u:[Z

    aput-boolean v2, v0, p1

    .line 68
    iget v0, p0, Lcom/google/obf/am;->p:I

    if-nez v0, :cond_0

    .line 69
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/obf/am;->w:J

    .line 70
    iget-object v0, p0, Lcom/google/obf/am;->C:Lcom/google/obf/dh;

    invoke-virtual {v0}, Lcom/google/obf/dh;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    iget-object v0, p0, Lcom/google/obf/am;->C:Lcom/google/obf/dh;

    invoke-virtual {v0}, Lcom/google/obf/dh;->b()V

    .line 74
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    invoke-direct {p0}, Lcom/google/obf/am;->j()V

    .line 73
    iget-object v0, p0, Lcom/google/obf/am;->c:Lcom/google/obf/cx;

    invoke-interface {v0, v2}, Lcom/google/obf/cx;->a(I)V

    goto :goto_0
.end method

.method public d()J
    .locals 8

    .prologue
    const-wide/high16 v4, -0x8000000000000000L

    .line 139
    iget-boolean v0, p0, Lcom/google/obf/am;->H:Z

    if-eqz v0, :cond_1

    .line 140
    const-wide/16 v2, -0x3

    .line 151
    :cond_0
    :goto_0
    return-wide v2

    .line 141
    :cond_1
    invoke-direct {p0}, Lcom/google/obf/am;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 142
    iget-wide v2, p0, Lcom/google/obf/am;->y:J

    goto :goto_0

    .line 144
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    move-wide v2, v4

    :goto_1
    iget-object v0, p0, Lcom/google/obf/am;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 145
    iget-object v0, p0, Lcom/google/obf/am;->e:Landroid/util/SparseArray;

    .line 146
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/am$d;

    invoke-virtual {v0}, Lcom/google/obf/am$d;->d()J

    move-result-wide v6

    .line 147
    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 148
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 149
    :cond_3
    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    iget-wide v2, p0, Lcom/google/obf/am;->w:J

    goto :goto_0
.end method

.method public d(I)Lcom/google/obf/ar;
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/obf/am;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/am$d;

    .line 173
    if-nez v0, :cond_0

    .line 174
    new-instance v0, Lcom/google/obf/am$d;

    iget-object v1, p0, Lcom/google/obf/am;->c:Lcom/google/obf/cx;

    invoke-direct {v0, p0, v1}, Lcom/google/obf/am$d;-><init>(Lcom/google/obf/am;Lcom/google/obf/cx;)V

    .line 175
    iget-object v1, p0, Lcom/google/obf/am;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 176
    :cond_0
    return-object v0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 152
    iget v0, p0, Lcom/google/obf/am;->v:I

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 153
    iget v0, p0, Lcom/google/obf/am;->v:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/obf/am;->v:I

    if-nez v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/google/obf/am;->C:Lcom/google/obf/dh;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/google/obf/am;->C:Lcom/google/obf/dh;

    new-instance v1, Lcom/google/obf/am$1;

    invoke-direct {v1, p0}, Lcom/google/obf/am$1;-><init>(Lcom/google/obf/am;)V

    invoke-virtual {v0, v1}, Lcom/google/obf/dh;->a(Ljava/lang/Runnable;)V

    .line 156
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/am;->C:Lcom/google/obf/dh;

    .line 157
    :cond_0
    return-void

    .line 152
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 177
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/am;->l:Z

    .line 178
    return-void
.end method
