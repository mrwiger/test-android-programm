.class public interface abstract Lru/cn/notifications/INotificationsRepository;
.super Ljava/lang/Object;
.source "INotificationsRepository.java"


# virtual methods
.method public abstract delete(Lru/cn/notifications/entity/INotification;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/notifications/entity/INotification;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract notifications()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/util/List",
            "<",
            "Lru/cn/notifications/entity/INotification;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract update()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method
