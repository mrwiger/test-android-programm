.class Lru/cn/tv/stb/StbActivity$9;
.super Ljava/lang/Object;
.source "StbActivity.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/stb/StbActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/stb/StbActivity;


# direct methods
.method constructor <init>(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 400
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity$9;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    const/4 v8, 0x0

    .line 403
    if-nez p2, :cond_0

    .line 405
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$9;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$1700(Lru/cn/tv/stb/StbActivity;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 406
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$9;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$1800(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v1

    iget-object v4, p0, Lru/cn/tv/stb/StbActivity$9;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v4}, Lru/cn/tv/stb/StbActivity;->access$1800(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v4

    invoke-virtual {v1, v4, v8}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 408
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$9;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$1300(Lru/cn/tv/stb/StbActivity;)V

    .line 410
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$9;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$1900(Lru/cn/tv/stb/StbActivity;)Landroid/os/Handler;

    move-result-object v1

    new-instance v4, Lru/cn/tv/stb/StbActivity$9$1;

    invoke-direct {v4, p0}, Lru/cn/tv/stb/StbActivity$9$1;-><init>(Lru/cn/tv/stb/StbActivity$9;)V

    invoke-virtual {v1, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 426
    :goto_0
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$9;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$300(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/categories/CategoryFragment;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/tv/stb/categories/CategoryFragment;->getContractorId()J

    move-result-wide v2

    .line 428
    .local v2, "contractorId":J
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->channels()Landroid/net/Uri;

    move-result-object v0

    .line 429
    .local v0, "channelsUri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$9;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-virtual {v1}, Lru/cn/tv/stb/StbActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v4, 0x0

    const-string v5, "contractor"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    .line 430
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    .line 429
    invoke-virtual {v1, v0, v4, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 432
    .end local v0    # "channelsUri":Landroid/net/Uri;
    .end local v2    # "contractorId":J
    :cond_0
    return-void

    .line 420
    :cond_1
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$9;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$400(Lru/cn/tv/stb/StbActivity;)V

    goto :goto_0
.end method
