.class public Lcom/yandex/metrica/impl/bn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/yandex/metrica/impl/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/bn$a;
    }
.end annotation


# static fields
.field private static volatile c:Lcom/yandex/metrica/impl/bn;

.field private static final d:Ljava/lang/Object;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/net/wifi/WifiManager;

.field private e:Lcom/yandex/metrica/impl/e$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/yandex/metrica/impl/e$a",
            "<",
            "Lorg/json/JSONArray;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/yandex/metrica/impl/e$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/yandex/metrica/impl/e$a",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/yandex/metrica/impl/bn$a;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/yandex/metrica/impl/bn;->d:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-direct {p0, p1, v0}, Lcom/yandex/metrica/impl/bn;-><init>(Landroid/content/Context;Landroid/net/wifi/WifiManager;)V

    .line 70
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/net/wifi/WifiManager;)V
    .locals 1

    .prologue
    .line 288
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Lcom/yandex/metrica/impl/e$a;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/e$a;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/bn;->e:Lcom/yandex/metrica/impl/e$a;

    .line 66
    new-instance v0, Lcom/yandex/metrica/impl/e$a;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/e$a;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/bn;->f:Lcom/yandex/metrica/impl/e$a;

    .line 289
    iput-object p1, p0, Lcom/yandex/metrica/impl/bn;->a:Landroid/content/Context;

    .line 290
    iput-object p2, p0, Lcom/yandex/metrica/impl/bn;->b:Landroid/net/wifi/WifiManager;

    .line 291
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/yandex/metrica/impl/bn;
    .locals 3

    .prologue
    .line 73
    sget-object v0, Lcom/yandex/metrica/impl/bn;->c:Lcom/yandex/metrica/impl/bn;

    if-nez v0, :cond_1

    .line 74
    sget-object v1, Lcom/yandex/metrica/impl/bn;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 75
    :try_start_0
    sget-object v0, Lcom/yandex/metrica/impl/bn;->c:Lcom/yandex/metrica/impl/bn;

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Lcom/yandex/metrica/impl/bn;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/yandex/metrica/impl/bn;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/yandex/metrica/impl/bn;->c:Lcom/yandex/metrica/impl/bn;

    .line 78
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    :cond_1
    sget-object v0, Lcom/yandex/metrica/impl/bn;->c:Lcom/yandex/metrica/impl/bn;

    return-object v0

    .line 78
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 100
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ":"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;ZI)Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 180
    const/4 v0, 0x0

    .line 182
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "mac"

    .line 183
    invoke-virtual {v1, v2, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "ssid"

    .line 184
    invoke-virtual {v1, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "signal_strength"

    .line 185
    invoke-virtual {v1, v2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "is_connected"

    .line 186
    invoke-virtual {v1, v2, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 190
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static a(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/yandex/metrica/impl/bn$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 211
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 213
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;

    move-result-object v0

    .line 214
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/NetworkInterface;

    .line 215
    invoke-virtual {v0}, Ljava/net/NetworkInterface;->getHardwareAddress()[B

    move-result-object v5

    .line 217
    if-eqz v5, :cond_0

    .line 221
    array-length v6, v5

    move v1, v2

    :goto_1
    if-ge v1, v6, :cond_1

    aget-byte v7, v5, v1

    .line 222
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "%02X:"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    aput-object v7, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 224
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 225
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 226
    new-instance v1, Lcom/yandex/metrica/impl/bn$a;

    invoke-virtual {v0}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v0, v5}, Lcom/yandex/metrica/impl/bn$a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 227
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->setLength(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 233
    :catch_0
    move-exception v0

    :cond_2
    return-void
.end method

.method private c()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    const/4 v0, 0x0

    .line 107
    :try_start_0
    iget-object v1, p0, Lcom/yandex/metrica/impl/bn;->b:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 111
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private d()Lorg/json/JSONArray;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 128
    .line 129
    iget-object v0, p0, Lcom/yandex/metrica/impl/bn;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/metrica/impl/am;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 130
    invoke-direct {p0}, Lcom/yandex/metrica/impl/bn;->c()Ljava/util/List;

    move-result-object v0

    .line 134
    :goto_0
    iget-object v2, p0, Lcom/yandex/metrica/impl/bn;->a:Landroid/content/Context;

    .line 1055
    const-string v3, "android.permission.ACCESS_WIFI_STATE"

    invoke-static {v2, v3}, Lcom/yandex/metrica/impl/am;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    .line 134
    if-eqz v2, :cond_8

    .line 1116
    iget-object v2, p0, Lcom/yandex/metrica/impl/bn;->b:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    .line 1118
    if-eqz v2, :cond_1

    const-string v3, "00:00:00:00:00:00"

    .line 1119
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1120
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getIpAddress()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    move-object v2, v1

    :cond_1
    move-object v3, v2

    .line 137
    :goto_1
    if-nez v3, :cond_3

    move-object v2, v1

    .line 138
    :goto_2
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5}, Lorg/json/JSONArray;-><init>()V

    .line 139
    if-eqz v0, :cond_5

    .line 140
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    .line 141
    if-eqz v0, :cond_2

    .line 143
    const/4 v3, 0x0

    .line 145
    :try_start_0
    iget-object v4, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    .line 146
    if-eqz v4, :cond_4

    .line 147
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 148
    invoke-static {v4}, Lcom/yandex/metrica/impl/bn;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 153
    :goto_4
    iget-object v7, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    iget v0, v0, Landroid/net/wifi/ScanResult;->level:I

    invoke-static {v4, v7, v3, v0}, Lcom/yandex/metrica/impl/bn;->a(Ljava/lang/String;Ljava/lang/String;ZI)Lorg/json/JSONObject;

    move-result-object v0

    .line 154
    if-eqz v0, :cond_2

    .line 155
    invoke-virtual {v5, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_3

    .line 137
    :cond_3
    invoke-virtual {v3}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :catch_0
    move-exception v4

    :cond_4
    move-object v4, v1

    goto :goto_4

    .line 159
    :cond_5
    if-eqz v3, :cond_6

    .line 160
    invoke-static {v2}, Lcom/yandex/metrica/impl/bn;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 161
    invoke-virtual {v3}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v2

    .line 162
    if-nez v2, :cond_7

    .line 163
    :goto_5
    const/4 v2, 0x1

    invoke-virtual {v3}, Landroid/net/wifi/WifiInfo;->getRssi()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/yandex/metrica/impl/bn;->a(Ljava/lang/String;Ljava/lang/String;ZI)Lorg/json/JSONObject;

    move-result-object v0

    .line 164
    if-eqz v0, :cond_6

    .line 165
    invoke-virtual {v5, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 169
    :cond_6
    return-object v5

    .line 1176
    :cond_7
    const-string v1, "\""

    const-string v4, ""

    invoke-virtual {v2, v1, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    :cond_8
    move-object v3, v1

    goto :goto_1

    :cond_9
    move-object v0, v1

    goto/16 :goto_0
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 195
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/bn;->b:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 197
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized a()Lorg/json/JSONArray;
    .locals 2

    .prologue
    .line 86
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/yandex/metrica/impl/bn;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    :goto_0
    monitor-exit p0

    return-object v0

    .line 91
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/bn;->e:Lcom/yandex/metrica/impl/e$a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/e$a;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/yandex/metrica/impl/bn;->e:Lcom/yandex/metrica/impl/e$a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/e$a;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 92
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/bn;->e:Lcom/yandex/metrica/impl/e$a;

    invoke-direct {p0}, Lcom/yandex/metrica/impl/bn;->d()Lorg/json/JSONArray;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/e$a;->a(Ljava/lang/Object;)V

    .line 95
    :cond_2
    iget-object v0, p0, Lcom/yandex/metrica/impl/bn;->e:Lcom/yandex/metrica/impl/e$a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONArray;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 237
    .line 2055
    :try_start_0
    const-string v0, "android.permission.ACCESS_WIFI_STATE"

    invoke-static {p1, v0}, Lcom/yandex/metrica/impl/am;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 237
    if-eqz v0, :cond_1

    .line 238
    iget-object v0, p0, Lcom/yandex/metrica/impl/bn;->b:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v2, "getWifiApConfiguration"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 239
    iget-object v2, p0, Lcom/yandex/metrica/impl/bn;->b:Landroid/net/wifi/WifiManager;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    .line 240
    if-eqz v0, :cond_0

    iget-object v0, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 245
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    .line 240
    goto :goto_0

    :catch_0
    move-exception v0

    :cond_1
    move-object v0, v1

    .line 245
    goto :goto_0
.end method

.method public b()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/yandex/metrica/impl/bn$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202
    iget-object v0, p0, Lcom/yandex/metrica/impl/bn;->f:Lcom/yandex/metrica/impl/e$a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/e$a;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/bn;->f:Lcom/yandex/metrica/impl/e$a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/e$a;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 203
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 204
    invoke-static {v0}, Lcom/yandex/metrica/impl/bn;->a(Ljava/util/List;)V

    .line 205
    iget-object v1, p0, Lcom/yandex/metrica/impl/bn;->f:Lcom/yandex/metrica/impl/e$a;

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/e$a;->a(Ljava/lang/Object;)V

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/bn;->f:Lcom/yandex/metrica/impl/e$a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public c(Landroid/content/Context;)I
    .locals 4

    .prologue
    .line 257
    const/4 v1, -0x1

    .line 3055
    :try_start_0
    const-string v0, "android.permission.ACCESS_WIFI_STATE"

    invoke-static {p1, v0}, Lcom/yandex/metrica/impl/am;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 259
    if-eqz v0, :cond_1

    .line 260
    iget-object v0, p0, Lcom/yandex/metrica/impl/bn;->b:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v2, "getWifiApState"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 262
    iget-object v2, p0, Lcom/yandex/metrica/impl/bn;->b:Landroid/net/wifi/WifiManager;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 265
    const/16 v1, 0xa

    if-le v0, v1, :cond_0

    .line 266
    add-int/lit8 v0, v0, -0xa

    .line 273
    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
