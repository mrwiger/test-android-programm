.class public Lcom/samsung/multiscreen/util/HttpUtil;
.super Ljava/lang/Object;
.source "HttpUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/multiscreen/util/HttpUtil$ResultCreator;
    }
.end annotation


# static fields
.field public static final METHOD_DELETE:Ljava/lang/String; = "DELETE"

.field public static final METHOD_GET:Ljava/lang/String; = "GET"

.field public static final METHOD_POST:Ljava/lang/String; = "POST"

.field public static final METHOD_PUT:Ljava/lang/String; = "PUT"

.field private static final TAG:Ljava/lang/String; = "HttpUtil"

.field private static logging:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/multiscreen/util/HttpUtil;->logging:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static enableLogging(Z)V
    .locals 0
    .param p0, "logging"    # Z

    .prologue
    .line 59
    sput-boolean p0, Lcom/samsung/multiscreen/util/HttpUtil;->logging:Z

    .line 60
    return-void
.end method

.method public static executeJSONRequest(Landroid/net/Uri;Ljava/lang/String;ILcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;)V
    .locals 1
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "timeout"    # I
    .param p3, "httpStringCallback"    # Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;

    .prologue
    .line 82
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0, p3}, Lcom/samsung/multiscreen/util/HttpUtil;->executeJSONRequest(Landroid/net/Uri;Ljava/lang/String;ILjava/util/Map;Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;)V

    .line 83
    return-void
.end method

.method public static executeJSONRequest(Landroid/net/Uri;Ljava/lang/String;ILjava/util/Map;Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;)V
    .locals 6
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "timeout"    # I
    .param p4, "httpStringCallback"    # Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 109
    .local p3, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v1, Lcom/koushikdutta/async/http/AsyncHttpRequest;

    invoke-direct {v1, p0, p1}, Lcom/koushikdutta/async/http/AsyncHttpRequest;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    .line 110
    .local v1, "request":Lcom/koushikdutta/async/http/AsyncHttpRequest;
    if-gtz p2, :cond_0

    const/16 p2, 0x7530

    .end local p2    # "timeout":I
    :cond_0
    invoke-virtual {v1, p2}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->setTimeout(I)Lcom/koushikdutta/async/http/AsyncHttpRequest;

    .line 111
    const-string v3, "Content-Type"

    const-string v4, "application/json"

    invoke-virtual {v1, v3, v4}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)Lcom/koushikdutta/async/http/AsyncHttpRequest;

    .line 113
    if-eqz p3, :cond_1

    .line 114
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p3}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 115
    .local v0, "jsonParams":Lorg/json/JSONObject;
    new-instance v2, Lcom/koushikdutta/async/http/body/JSONObjectBody;

    invoke-direct {v2, v0}, Lcom/koushikdutta/async/http/body/JSONObjectBody;-><init>(Lorg/json/JSONObject;)V

    .line 116
    .local v2, "requestBody":Lcom/koushikdutta/async/http/body/JSONObjectBody;
    invoke-virtual {v1, v2}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->setBody(Lcom/koushikdutta/async/http/body/AsyncHttpRequestBody;)V

    .line 119
    .end local v0    # "jsonParams":Lorg/json/JSONObject;
    .end local v2    # "requestBody":Lcom/koushikdutta/async/http/body/JSONObjectBody;
    :cond_1
    sget-boolean v3, Lcom/samsung/multiscreen/util/HttpUtil;->logging:Z

    if-eqz v3, :cond_2

    .line 120
    const-string v3, "HttpUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "executeJSONRequest() method: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->getMethod()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", uri: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    const-string v3, "HttpUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "executeJSONRequest() request.getHeaders() "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->getHeaders()Lcom/koushikdutta/async/http/Headers;

    move-result-object v5

    invoke-virtual {v5}, Lcom/koushikdutta/async/http/Headers;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    :cond_2
    invoke-static {}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getDefaultInstance()Lcom/koushikdutta/async/http/AsyncHttpClient;

    move-result-object v3

    invoke-virtual {v3, v1, p4}, Lcom/koushikdutta/async/http/AsyncHttpClient;->executeString(Lcom/koushikdutta/async/http/AsyncHttpRequest;Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;)Lcom/koushikdutta/async/future/Future;

    .line 125
    return-void
.end method

.method public static executeJSONRequest(Landroid/net/Uri;Ljava/lang/String;Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;)V
    .locals 1
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "httpStringCallback"    # Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2}, Lcom/samsung/multiscreen/util/HttpUtil;->executeJSONRequest(Landroid/net/Uri;Ljava/lang/String;Ljava/util/Map;Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;)V

    .line 71
    return-void
.end method

.method public static executeJSONRequest(Landroid/net/Uri;Ljava/lang/String;Ljava/util/Map;Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;)V
    .locals 1
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "method"    # Ljava/lang/String;
    .param p3, "httpStringCallback"    # Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 95
    .local p2, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/16 v0, 0x7530

    invoke-static {p0, p1, v0, p2, p3}, Lcom/samsung/multiscreen/util/HttpUtil;->executeJSONRequest(Landroid/net/Uri;Ljava/lang/String;ILjava/util/Map;Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;)V

    .line 96
    return-void
.end method
