.class public Lru/cn/domain/Channels;
.super Ljava/lang/Object;
.source "Channels.java"


# instance fields
.field private final loader:Lru/cn/mvvm/RxLoader;


# direct methods
.method public constructor <init>(Lru/cn/mvvm/RxLoader;)V
    .locals 0
    .param p1, "loader"    # Lru/cn/mvvm/RxLoader;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lru/cn/domain/Channels;->loader:Lru/cn/mvvm/RxLoader;

    .line 26
    return-void
.end method

.method private channels(Landroid/net/Uri;Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 2
    .param p1, "channelsUri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lru/cn/domain/Channels;->loader:Lru/cn/mvvm/RxLoader;

    .line 40
    invoke-virtual {v0, p1, p2}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v0

    .line 41
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lru/cn/domain/Channels$$Lambda$0;->$instance:Lio/reactivex/functions/Function;

    .line 42
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->retryWhen(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 39
    return-object v0
.end method

.method static final synthetic lambda$channels$1$Channels(Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "errors"    # Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 42
    const/4 v0, 0x1

    const/4 v1, 0x4

    .line 43
    invoke-static {v0, v1}, Lio/reactivex/Observable;->range(II)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lru/cn/domain/Channels$$Lambda$1;->$instance:Lio/reactivex/functions/BiFunction;

    invoke-virtual {p0, v0, v1}, Lio/reactivex/Observable;->zipWith(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lru/cn/domain/Channels$$Lambda$2;->$instance:Lio/reactivex/functions/Function;

    .line 44
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 42
    return-object v0
.end method

.method static final synthetic lambda$null$0$Channels(Landroid/util/Pair;)Lio/reactivex/ObservableSource;
    .locals 6
    .param p0, "retry"    # Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 45
    iget-object v1, p0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 46
    .local v0, "retryCount":I
    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    .line 47
    iget-object v1, p0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Throwable;

    invoke-static {v1}, Lio/reactivex/exceptions/Exceptions;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1

    .line 49
    :cond_0
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    int-to-double v4, v0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-int v1, v2

    int-to-long v2, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v2, v3, v1}, Lio/reactivex/Observable;->timer(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public channels(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 2
    .param p1, "selection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->channels()Landroid/net/Uri;

    move-result-object v0

    .line 35
    .local v0, "channelsUri":Landroid/net/Uri;
    invoke-direct {p0, v0, p1}, Lru/cn/domain/Channels;->channels(Landroid/net/Uri;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v1

    return-object v1
.end method

.method public channels(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 2
    .param p1, "ageFilter"    # Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    invoke-static {p1}, Lru/cn/api/provider/TvContentProviderContract;->channels(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 30
    .local v0, "channelsUri":Landroid/net/Uri;
    invoke-direct {p0, v0, p2}, Lru/cn/domain/Channels;->channels(Landroid/net/Uri;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v1

    return-object v1
.end method
