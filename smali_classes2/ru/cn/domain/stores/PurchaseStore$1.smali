.class Lru/cn/domain/stores/PurchaseStore$1;
.super Landroid/os/AsyncTask;
.source "PurchaseStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/domain/stores/PurchaseStore;->savePurchaseAsync(ILru/cn/domain/stores/iabhelper/Purchase;Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/domain/stores/PurchaseStore;

.field final synthetic val$info:Lru/cn/domain/stores/iabhelper/Purchase;

.field final synthetic val$listener:Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;

.field final synthetic val$resultCode:I


# direct methods
.method constructor <init>(Lru/cn/domain/stores/PurchaseStore;Lru/cn/domain/stores/iabhelper/Purchase;Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;I)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/domain/stores/PurchaseStore;

    .prologue
    .line 56
    iput-object p1, p0, Lru/cn/domain/stores/PurchaseStore$1;->this$0:Lru/cn/domain/stores/PurchaseStore;

    iput-object p2, p0, Lru/cn/domain/stores/PurchaseStore$1;->val$info:Lru/cn/domain/stores/iabhelper/Purchase;

    iput-object p3, p0, Lru/cn/domain/stores/PurchaseStore$1;->val$listener:Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;

    iput p4, p0, Lru/cn/domain/stores/PurchaseStore$1;->val$resultCode:I

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 56
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lru/cn/domain/stores/PurchaseStore$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 59
    iget-object v0, p0, Lru/cn/domain/stores/PurchaseStore$1;->val$info:Lru/cn/domain/stores/iabhelper/Purchase;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lru/cn/domain/stores/PurchaseStore$1;->this$0:Lru/cn/domain/stores/PurchaseStore;

    iget-object v0, v0, Lru/cn/domain/stores/PurchaseStore;->persistor:Lru/cn/domain/stores/PurchasePersistor;

    const/4 v1, 0x1

    new-array v1, v1, [Lru/cn/domain/stores/iabhelper/Purchase;

    const/4 v2, 0x0

    iget-object v3, p0, Lru/cn/domain/stores/PurchaseStore$1;->val$info:Lru/cn/domain/stores/iabhelper/Purchase;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/cn/domain/stores/PurchasePersistor;->savePurchases(Ljava/util/List;)V

    .line 63
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 56
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lru/cn/domain/stores/PurchaseStore$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .param p1, "arg"    # Ljava/lang/Void;

    .prologue
    .line 68
    iget-object v0, p0, Lru/cn/domain/stores/PurchaseStore$1;->val$listener:Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;

    if-eqz v0, :cond_0

    .line 69
    iget-object v1, p0, Lru/cn/domain/stores/PurchaseStore$1;->val$listener:Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;

    iget v2, p0, Lru/cn/domain/stores/PurchaseStore$1;->val$resultCode:I

    iget-object v0, p0, Lru/cn/domain/stores/PurchaseStore$1;->val$info:Lru/cn/domain/stores/iabhelper/Purchase;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lru/cn/domain/stores/PurchaseStore$1;->val$info:Lru/cn/domain/stores/iabhelper/Purchase;

    invoke-virtual {v0}, Lru/cn/domain/stores/iabhelper/Purchase;->getSku()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-interface {v1, v2, v0}, Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;->onPurchaseCompleted(ILjava/lang/String;)V

    .line 70
    :cond_0
    return-void

    .line 69
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
