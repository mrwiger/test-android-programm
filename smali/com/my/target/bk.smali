.class public final Lcom/my/target/bk;
.super Lcom/my/target/bm;
.source "DeviceParamsDataProvider.java"


# static fields
.field private static final gH:Ljava/lang/String; = "Android"


# instance fields
.field private density:F

.field private gI:Z

.field private gJ:Ljava/lang/String;

.field private gK:Ljava/lang/String;

.field private gL:Ljava/lang/String;

.field private gM:Ljava/lang/String;

.field private gN:Ljava/lang/String;

.field private gO:Ljava/lang/String;

.field private gP:Ljava/lang/String;

.field private gQ:Ljava/lang/String;

.field private gR:Ljava/lang/String;

.field private gS:Ljava/lang/String;

.field private gT:Ljava/lang/String;

.field private gU:I

.field private gV:Ljava/lang/String;

.field private gW:Ljava/lang/String;

.field private gX:Ljava/lang/String;

.field private gY:Ljava/lang/String;

.field private height:I

.field private timezone:Ljava/lang/String;

.field private width:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-direct {p0}, Lcom/my/target/bm;-><init>()V

    .line 49
    iput-boolean v1, p0, Lcom/my/target/bk;->gI:Z

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/bk;->gJ:Ljava/lang/String;

    .line 51
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/bk;->gK:Ljava/lang/String;

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/bk;->gL:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/bk;->gM:Ljava/lang/String;

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/bk;->gN:Ljava/lang/String;

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/bk;->gO:Ljava/lang/String;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/bk;->gP:Ljava/lang/String;

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/bk;->gQ:Ljava/lang/String;

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/bk;->gR:Ljava/lang/String;

    .line 59
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/bk;->gS:Ljava/lang/String;

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/bk;->gT:Ljava/lang/String;

    .line 61
    iput v1, p0, Lcom/my/target/bk;->width:I

    .line 62
    iput v1, p0, Lcom/my/target/bk;->height:I

    .line 63
    iput v1, p0, Lcom/my/target/bk;->gU:I

    .line 64
    const/4 v0, 0x0

    iput v0, p0, Lcom/my/target/bk;->density:F

    .line 65
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/bk;->gV:Ljava/lang/String;

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/bk;->gW:Ljava/lang/String;

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/bk;->gX:Ljava/lang/String;

    .line 68
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/bk;->timezone:Ljava/lang/String;

    .line 69
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/bk;->gY:Ljava/lang/String;

    .line 76
    return-void
.end method

.method private h(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 305
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 306
    if-nez v0, :cond_0

    .line 325
    :goto_0
    return-void

    .line 310
    :cond_0
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 312
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 314
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_1

    .line 316
    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 323
    :goto_1
    iget v0, v1, Landroid/graphics/Point;->x:I

    iput v0, p0, Lcom/my/target/bk;->width:I

    .line 324
    iget v0, v1, Landroid/graphics/Point;->y:I

    iput v0, p0, Lcom/my/target/bk;->height:I

    goto :goto_0

    .line 320
    :cond_1
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    goto :goto_1
.end method

.method private i(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 329
    .line 333
    :try_start_0
    invoke-static {p1}, Lcom/my/target/cj;->w(Landroid/content/Context;)Lcom/my/target/cj;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 334
    :try_start_1
    invoke-virtual {v0}, Lcom/my/target/cj;->bB()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 342
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 345
    sget-object v2, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    .line 347
    const-string v1, ""

    .line 348
    invoke-direct {p0, p1}, Lcom/my/target/bk;->j(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 350
    invoke-virtual {p0, p1}, Lcom/my/target/bk;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 352
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/my/target/bk;->gK:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/my/target/cg;->P(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 353
    if-eqz v0, :cond_1

    .line 355
    invoke-virtual {v0, v1}, Lcom/my/target/cj;->Q(Ljava/lang/String;)V

    .line 358
    :cond_1
    return-object v1

    .line 336
    :catch_0
    move-exception v0

    move-object v2, v0

    move-object v0, v1

    .line 338
    :goto_1
    const-string v3, "PreferencesManager error"

    invoke-static {v3}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 339
    invoke-static {v2}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 336
    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method private j(Landroid/content/Context;)Z
    .locals 4

    .prologue
    .line 363
    const/4 v1, -0x1

    .line 366
    :try_start_0
    const-string v0, "android.permission.GET_ACCOUNTS"

    invoke-virtual {p1, v0}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 376
    :goto_0
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 368
    :catch_0
    move-exception v0

    .line 370
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unable to check android.permission.GET_ACCOUNTS permission! Unexpected throwable in Context.checkCallingOrSelfPermission() method: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 374
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 370
    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    .line 376
    :cond_0
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public aA()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/my/target/bk;->gP:Ljava/lang/String;

    return-object v0
.end method

.method public aB()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/my/target/bk;->gQ:Ljava/lang/String;

    return-object v0
.end method

.method public aC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/my/target/bk;->gS:Ljava/lang/String;

    return-object v0
.end method

.method public aD()I
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/my/target/bk;->gU:I

    return v0
.end method

.method public aE()F
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lcom/my/target/bk;->density:F

    return v0
.end method

.method public aF()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/my/target/bk;->gV:Ljava/lang/String;

    return-object v0
.end method

.method public aG()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/my/target/bk;->gX:Ljava/lang/String;

    return-object v0
.end method

.method public aH()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/my/target/bk;->gK:Ljava/lang/String;

    return-object v0
.end method

.method public aI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/my/target/bk;->gW:Ljava/lang/String;

    return-object v0
.end method

.method public aJ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/my/target/bk;->timezone:Ljava/lang/String;

    return-object v0
.end method

.method public aK()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/my/target/bk;->gY:Ljava/lang/String;

    return-object v0
.end method

.method public aw()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/my/target/bk;->gJ:Ljava/lang/String;

    return-object v0
.end method

.method public ax()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/my/target/bk;->gL:Ljava/lang/String;

    return-object v0
.end method

.method public ay()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/my/target/bk;->gM:Ljava/lang/String;

    return-object v0
.end method

.method public az()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/my/target/bk;->gN:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized collectData(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HardwareIds"
        }
    .end annotation

    .prologue
    const/4 v3, 0x3

    .line 184
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/my/target/bk;->gI:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 281
    :goto_0
    monitor-exit p0

    return-void

    .line 189
    :cond_0
    :try_start_1
    const-string v0, "collect application info..."

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 190
    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    iput-object v0, p0, Lcom/my/target/bk;->gJ:Ljava/lang/String;

    .line 191
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v0, p0, Lcom/my/target/bk;->gP:Ljava/lang/String;

    .line 192
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v0, p0, Lcom/my/target/bk;->gQ:Ljava/lang/String;

    .line 194
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iput-object v0, p0, Lcom/my/target/bk;->gL:Ljava/lang/String;

    .line 195
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/bk;->gM:Ljava/lang/String;

    .line 196
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/bk;->gR:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 199
    :try_start_2
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/bk;->gM:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 200
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v1, p0, Lcom/my/target/bk;->gN:Ljava/lang/String;

    .line 201
    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/bk;->gO:Ljava/lang/String;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 207
    :goto_1
    :try_start_3
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 208
    if-eqz v0, :cond_1

    .line 210
    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/bk;->gK:Ljava/lang/String;

    .line 211
    iget-object v0, p0, Lcom/my/target/bk;->gK:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 213
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/bk;->gK:Ljava/lang/String;

    .line 216
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/bk;->gS:Ljava/lang/String;

    .line 217
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 218
    if-eqz v0, :cond_3

    .line 220
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/my/target/bk;->gW:Ljava/lang/String;

    .line 221
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_2

    .line 223
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/my/target/bk;->gX:Ljava/lang/String;

    .line 225
    :cond_2
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    .line 226
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v3, :cond_5

    .line 228
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/my/target/bk;->gV:Ljava/lang/String;

    .line 229
    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/bk;->gT:Ljava/lang/String;

    .line 237
    :cond_3
    :goto_2
    invoke-direct {p0, p1}, Lcom/my/target/bk;->h(Landroid/content/Context;)V

    .line 239
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 240
    iget v1, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v1, p0, Lcom/my/target/bk;->gU:I

    .line 241
    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/my/target/bk;->density:F

    .line 243
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    .line 244
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/TimeZone;->getDisplayName(ZI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/bk;->timezone:Ljava/lang/String;

    .line 246
    invoke-direct {p0, p1}, Lcom/my/target/bk;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 247
    if-eqz v0, :cond_4

    .line 249
    iput-object v0, p0, Lcom/my/target/bk;->gY:Ljava/lang/String;

    .line 252
    :cond_4
    const-string v0, "android_id"

    iget-object v1, p0, Lcom/my/target/bk;->gK:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/my/target/bk;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 253
    const-string v0, "device"

    iget-object v1, p0, Lcom/my/target/bk;->gJ:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/my/target/bk;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 254
    const-string v0, "os"

    const-string v1, "Android"

    invoke-virtual {p0, v0, v1}, Lcom/my/target/bk;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 255
    const-string v0, "manufacture"

    iget-object v1, p0, Lcom/my/target/bk;->gP:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/my/target/bk;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 256
    const-string v0, "osver"

    iget-object v1, p0, Lcom/my/target/bk;->gL:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/my/target/bk;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 257
    const-string v0, "app"

    iget-object v1, p0, Lcom/my/target/bk;->gM:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/my/target/bk;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 258
    const-string v0, "appver"

    iget-object v1, p0, Lcom/my/target/bk;->gN:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/my/target/bk;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 259
    const-string v0, "appbuild"

    iget-object v1, p0, Lcom/my/target/bk;->gO:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/my/target/bk;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 260
    const-string v0, "lang"

    iget-object v1, p0, Lcom/my/target/bk;->gR:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/my/target/bk;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 261
    const-string v0, "app_lang"

    iget-object v1, p0, Lcom/my/target/bk;->gS:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/my/target/bk;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 262
    const-string v0, "sim_loc"

    iget-object v1, p0, Lcom/my/target/bk;->gT:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/my/target/bk;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 263
    const-string v0, "euname"

    iget-object v1, p0, Lcom/my/target/bk;->gQ:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/my/target/bk;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 264
    const-string v0, "w"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/my/target/bk;->width:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/my/target/bk;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 265
    const-string v0, "h"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/my/target/bk;->height:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/my/target/bk;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 266
    const-string v0, "dpi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/my/target/bk;->gU:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/my/target/bk;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 267
    const-string v0, "density"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/my/target/bk;->density:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/my/target/bk;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 268
    const-string v0, "operator_id"

    iget-object v1, p0, Lcom/my/target/bk;->gV:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/my/target/bk;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 269
    const-string v0, "operator_name"

    iget-object v1, p0, Lcom/my/target/bk;->gW:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/my/target/bk;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 270
    const-string v0, "sim_operator_id"

    iget-object v1, p0, Lcom/my/target/bk;->gX:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/my/target/bk;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 271
    const-string v0, "timezone"

    iget-object v1, p0, Lcom/my/target/bk;->timezone:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/my/target/bk;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 272
    const-string v0, "mrgs_device_id"

    iget-object v1, p0, Lcom/my/target/bk;->gY:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/my/target/bk;->addParam(Ljava/lang/String;Ljava/lang/String;)Z

    .line 274
    invoke-virtual {p0}, Lcom/my/target/bk;->getMap()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 276
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 184
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 233
    :cond_5
    :try_start_4
    iput-object v0, p0, Lcom/my/target/bk;->gV:Ljava/lang/String;

    goto/16 :goto_2

    .line 279
    :cond_6
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/bk;->gI:Z

    .line 280
    const-string v0, "collected"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 203
    :catch_0
    move-exception v0

    goto/16 :goto_1
.end method

.method public g(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 285
    const/4 v0, 0x0

    .line 289
    :try_start_0
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 295
    :goto_0
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 297
    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 300
    :goto_1
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_1

    .line 291
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public getCountryCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/my/target/bk;->gT:Ljava/lang/String;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/my/target/bk;->height:I

    return v0
.end method

.method public getLang()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/my/target/bk;->gR:Ljava/lang/String;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/my/target/bk;->width:I

    return v0
.end method
