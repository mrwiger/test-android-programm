.class public final Lcom/google/obf/dc;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/cx;


# instance fields
.field private final a:I

.field private final b:[B

.field private c:I

.field private d:I

.field private e:[Lcom/google/obf/cw;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/obf/dc;-><init>(II)V

    .line 2
    return-void
.end method

.method public constructor <init>(II)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    if-lez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->a(Z)V

    .line 5
    if-ltz p2, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/google/obf/dl;->a(Z)V

    .line 6
    iput p1, p0, Lcom/google/obf/dc;->a:I

    .line 7
    iput p2, p0, Lcom/google/obf/dc;->d:I

    .line 8
    add-int/lit8 v0, p2, 0x64

    new-array v0, v0, [Lcom/google/obf/cw;

    iput-object v0, p0, Lcom/google/obf/dc;->e:[Lcom/google/obf/cw;

    .line 9
    if-lez p2, :cond_2

    .line 10
    mul-int v0, p2, p1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/obf/dc;->b:[B

    .line 11
    :goto_2
    if-ge v2, p2, :cond_3

    .line 12
    mul-int v0, v2, p1

    .line 13
    iget-object v1, p0, Lcom/google/obf/dc;->e:[Lcom/google/obf/cw;

    new-instance v3, Lcom/google/obf/cw;

    iget-object v4, p0, Lcom/google/obf/dc;->b:[B

    invoke-direct {v3, v4, v0}, Lcom/google/obf/cw;-><init>([BI)V

    aput-object v3, v1, v2

    .line 14
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_0
    move v0, v2

    .line 4
    goto :goto_0

    :cond_1
    move v1, v2

    .line 5
    goto :goto_1

    .line 15
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/dc;->b:[B

    .line 16
    :cond_3
    return-void
.end method


# virtual methods
.method public declared-synchronized a()Lcom/google/obf/cw;
    .locals 4

    .prologue
    .line 17
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/obf/dc;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/dc;->c:I

    .line 18
    iget v0, p0, Lcom/google/obf/dc;->d:I

    if-lez v0, :cond_0

    .line 19
    iget-object v0, p0, Lcom/google/obf/dc;->e:[Lcom/google/obf/cw;

    iget v1, p0, Lcom/google/obf/dc;->d:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/obf/dc;->d:I

    aget-object v0, v0, v1

    .line 20
    iget-object v1, p0, Lcom/google/obf/dc;->e:[Lcom/google/obf/cw;

    iget v2, p0, Lcom/google/obf/dc;->d:I

    const/4 v3, 0x0

    aput-object v3, v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :goto_0
    monitor-exit p0

    return-object v0

    .line 21
    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/obf/cw;

    iget v1, p0, Lcom/google/obf/dc;->a:I

    new-array v1, v1, [B

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/obf/cw;-><init>([BI)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 17
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(I)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 41
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/obf/dc;->a:I

    invoke-static {p1, v0}, Lcom/google/obf/ea;->a(II)I

    move-result v0

    .line 42
    const/4 v2, 0x0

    iget v3, p0, Lcom/google/obf/dc;->c:I

    sub-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 43
    iget v0, p0, Lcom/google/obf/dc;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lt v3, v0, :cond_1

    .line 63
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 45
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/obf/dc;->b:[B

    if-eqz v0, :cond_5

    .line 47
    iget v0, p0, Lcom/google/obf/dc;->d:I

    add-int/lit8 v0, v0, -0x1

    .line 48
    :goto_1
    if-gt v1, v0, :cond_4

    .line 49
    iget-object v2, p0, Lcom/google/obf/dc;->e:[Lcom/google/obf/cw;

    aget-object v4, v2, v1

    .line 50
    iget-object v2, v4, Lcom/google/obf/cw;->a:[B

    iget-object v5, p0, Lcom/google/obf/dc;->b:[B

    if-ne v2, v5, :cond_2

    .line 51
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 52
    :cond_2
    iget-object v2, p0, Lcom/google/obf/dc;->e:[Lcom/google/obf/cw;

    aget-object v5, v2, v0

    .line 53
    iget-object v2, v5, Lcom/google/obf/cw;->a:[B

    iget-object v6, p0, Lcom/google/obf/dc;->b:[B

    if-eq v2, v6, :cond_3

    .line 54
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 55
    :cond_3
    iget-object v6, p0, Lcom/google/obf/dc;->e:[Lcom/google/obf/cw;

    add-int/lit8 v2, v1, 0x1

    aput-object v5, v6, v1

    .line 56
    iget-object v5, p0, Lcom/google/obf/dc;->e:[Lcom/google/obf/cw;

    add-int/lit8 v1, v0, -0x1

    aput-object v4, v5, v0

    move v0, v1

    move v1, v2

    goto :goto_1

    .line 58
    :cond_4
    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 59
    iget v1, p0, Lcom/google/obf/dc;->d:I

    if-ge v0, v1, :cond_0

    .line 61
    :goto_2
    iget-object v1, p0, Lcom/google/obf/dc;->e:[Lcom/google/obf/cw;

    iget v2, p0, Lcom/google/obf/dc;->d:I

    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    .line 62
    iput v0, p0, Lcom/google/obf/dc;->d:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 41
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_5
    move v0, v3

    goto :goto_2
.end method

.method public declared-synchronized a(Lcom/google/obf/cw;)V
    .locals 3

    .prologue
    .line 23
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lcom/google/obf/cw;->a:[B

    iget-object v1, p0, Lcom/google/obf/dc;->b:[B

    if-eq v0, v1, :cond_0

    iget-object v0, p1, Lcom/google/obf/cw;->a:[B

    array-length v0, v0

    iget v1, p0, Lcom/google/obf/dc;->a:I

    if-ne v0, v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->a(Z)V

    .line 24
    iget v0, p0, Lcom/google/obf/dc;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/obf/dc;->c:I

    .line 25
    iget v0, p0, Lcom/google/obf/dc;->d:I

    iget-object v1, p0, Lcom/google/obf/dc;->e:[Lcom/google/obf/cw;

    array-length v1, v1

    if-ne v0, v1, :cond_1

    .line 26
    iget-object v0, p0, Lcom/google/obf/dc;->e:[Lcom/google/obf/cw;

    iget-object v1, p0, Lcom/google/obf/dc;->e:[Lcom/google/obf/cw;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/obf/cw;

    iput-object v0, p0, Lcom/google/obf/dc;->e:[Lcom/google/obf/cw;

    .line 27
    :cond_1
    iget-object v0, p0, Lcom/google/obf/dc;->e:[Lcom/google/obf/cw;

    iget v1, p0, Lcom/google/obf/dc;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/obf/dc;->d:I

    aput-object p1, v0, v1

    .line 28
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    monitor-exit p0

    return-void

    .line 23
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a([Lcom/google/obf/cw;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 30
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/obf/dc;->d:I

    array-length v2, p1

    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/google/obf/dc;->e:[Lcom/google/obf/cw;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 31
    iget-object v0, p0, Lcom/google/obf/dc;->e:[Lcom/google/obf/cw;

    iget-object v2, p0, Lcom/google/obf/dc;->e:[Lcom/google/obf/cw;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    iget v3, p0, Lcom/google/obf/dc;->d:I

    array-length v4, p1

    add-int/2addr v3, v4

    .line 32
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 33
    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/obf/cw;

    iput-object v0, p0, Lcom/google/obf/dc;->e:[Lcom/google/obf/cw;

    .line 34
    :cond_0
    array-length v3, p1

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_3

    aget-object v4, p1, v2

    .line 35
    iget-object v0, v4, Lcom/google/obf/cw;->a:[B

    iget-object v5, p0, Lcom/google/obf/dc;->b:[B

    if-eq v0, v5, :cond_1

    iget-object v0, v4, Lcom/google/obf/cw;->a:[B

    array-length v0, v0

    iget v5, p0, Lcom/google/obf/dc;->a:I

    if-ne v0, v5, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/google/obf/dl;->a(Z)V

    .line 36
    iget-object v0, p0, Lcom/google/obf/dc;->e:[Lcom/google/obf/cw;

    iget v5, p0, Lcom/google/obf/dc;->d:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lcom/google/obf/dc;->d:I

    aput-object v4, v0, v5

    .line 37
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 35
    goto :goto_1

    .line 38
    :cond_3
    iget v0, p0, Lcom/google/obf/dc;->c:I

    array-length v1, p1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/dc;->c:I

    .line 39
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    monitor-exit p0

    return-void

    .line 30
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/google/obf/dc;->a:I

    return v0
.end method

.method public declared-synchronized b(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 65
    monitor-enter p0

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/obf/dc;->c()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 66
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 67
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized c()I
    .locals 2

    .prologue
    .line 64
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/obf/dc;->c:I

    iget v1, p0, Lcom/google/obf/dc;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    mul-int/2addr v0, v1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
