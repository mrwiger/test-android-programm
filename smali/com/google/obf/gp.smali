.class public Lcom/google/obf/gp;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/ads/interactivemedia/v3/api/AdsLoader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/gp$a;,
        Lcom/google/obf/gp$b;
    }
.end annotation


# instance fields
.field a:Lcom/google/obf/hj$b;

.field private final b:Lcom/google/obf/hj;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/obf/he;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/ads/interactivemedia/v3/api/AdsLoader$AdsLoadedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/ads/interactivemedia/v3/api/AdsRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/ads/interactivemedia/v3/api/StreamRequest;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/google/obf/ik;

.field private final i:Ljava/lang/Object;

.field private j:Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;

.field private k:Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/obf/gp;-><init>(Landroid/content/Context;Landroid/net/Uri;Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;)V

    .line 2
    iget-object v0, p0, Lcom/google/obf/gp;->b:Lcom/google/obf/hj;

    invoke-virtual {v0}, Lcom/google/obf/hj;->a()V

    .line 3
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;)V
    .locals 1

    .prologue
    .line 4
    new-instance v0, Lcom/google/obf/hj;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/obf/hj;-><init>(Landroid/content/Context;Landroid/net/Uri;Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;)V

    invoke-direct {p0, v0, p1}, Lcom/google/obf/gp;-><init>(Lcom/google/obf/hj;Landroid/content/Context;)V

    .line 5
    iput-object p3, p0, Lcom/google/obf/gp;->j:Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;

    .line 6
    iput-object p4, p0, Lcom/google/obf/gp;->k:Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;

    .line 7
    return-void
.end method

.method public constructor <init>(Lcom/google/obf/hj;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    new-instance v0, Lcom/google/obf/gp$1;

    invoke-direct {v0, p0}, Lcom/google/obf/gp$1;-><init>(Lcom/google/obf/gp;)V

    iput-object v0, p0, Lcom/google/obf/gp;->a:Lcom/google/obf/hj$b;

    .line 10
    new-instance v0, Lcom/google/obf/he;

    invoke-direct {v0}, Lcom/google/obf/he;-><init>()V

    iput-object v0, p0, Lcom/google/obf/gp;->d:Lcom/google/obf/he;

    .line 11
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/gp;->e:Ljava/util/List;

    .line 12
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/obf/gp;->f:Ljava/util/Map;

    .line 13
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/obf/gp;->g:Ljava/util/Map;

    .line 14
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/obf/gp;->i:Ljava/lang/Object;

    .line 15
    new-instance v0, Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;

    invoke-direct {v0}, Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;-><init>()V

    iput-object v0, p0, Lcom/google/obf/gp;->j:Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;

    .line 16
    iput-object p1, p0, Lcom/google/obf/gp;->b:Lcom/google/obf/hj;

    .line 17
    iput-object p2, p0, Lcom/google/obf/gp;->c:Landroid/content/Context;

    .line 18
    return-void
.end method

.method static synthetic a(Lcom/google/obf/gp;Lcom/google/obf/ik;)Lcom/google/obf/ik;
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/google/obf/gp;->h:Lcom/google/obf/ik;

    return-object p1
.end method

.method static synthetic a(Lcom/google/obf/gp;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/obf/gp;->f:Ljava/util/Map;

    return-object v0
.end method

.method private a(Lcom/google/ads/interactivemedia/v3/api/AdsRequest;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 52
    if-nez p1, :cond_0

    .line 53
    iget-object v1, p0, Lcom/google/obf/gp;->d:Lcom/google/obf/he;

    new-instance v2, Lcom/google/obf/gk;

    new-instance v3, Lcom/google/ads/interactivemedia/v3/api/AdError;

    sget-object v4, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;->LOAD:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;

    sget-object v5, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;->INVALID_ARGUMENTS:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;

    const-string v6, "AdsRequest cannot be null."

    invoke-direct {v3, v4, v5, v6}, Lcom/google/ads/interactivemedia/v3/api/AdError;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/google/obf/gk;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError;)V

    invoke-virtual {v1, v2}, Lcom/google/obf/he;->a(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent;)V

    .line 71
    :goto_0
    return v0

    .line 55
    :cond_0
    invoke-interface {p1}, Lcom/google/ads/interactivemedia/v3/api/AdsRequest;->getAdDisplayContainer()Lcom/google/ads/interactivemedia/v3/api/AdDisplayContainer;

    move-result-object v1

    .line 56
    if-nez v1, :cond_1

    .line 57
    iget-object v1, p0, Lcom/google/obf/gp;->d:Lcom/google/obf/he;

    new-instance v2, Lcom/google/obf/gk;

    new-instance v3, Lcom/google/ads/interactivemedia/v3/api/AdError;

    sget-object v4, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;->LOAD:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;

    sget-object v5, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;->INVALID_ARGUMENTS:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;

    const-string v6, "Ad display container must be provided in the AdsRequest."

    invoke-direct {v3, v4, v5, v6}, Lcom/google/ads/interactivemedia/v3/api/AdError;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/google/obf/gk;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError;)V

    invoke-virtual {v1, v2}, Lcom/google/obf/he;->a(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent;)V

    goto :goto_0

    .line 59
    :cond_1
    invoke-interface {v1}, Lcom/google/ads/interactivemedia/v3/api/AdDisplayContainer;->getAdContainer()Landroid/view/ViewGroup;

    move-result-object v2

    if-nez v2, :cond_2

    .line 60
    iget-object v1, p0, Lcom/google/obf/gp;->d:Lcom/google/obf/he;

    new-instance v2, Lcom/google/obf/gk;

    new-instance v3, Lcom/google/ads/interactivemedia/v3/api/AdError;

    sget-object v4, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;->LOAD:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;

    sget-object v5, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;->INVALID_ARGUMENTS:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;

    const-string v6, "Ad display container must have a UI container."

    invoke-direct {v3, v4, v5, v6}, Lcom/google/ads/interactivemedia/v3/api/AdError;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/google/obf/gk;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError;)V

    invoke-virtual {v1, v2}, Lcom/google/obf/he;->a(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent;)V

    goto :goto_0

    .line 62
    :cond_2
    iget-object v2, p0, Lcom/google/obf/gp;->b:Lcom/google/obf/hj;

    invoke-virtual {v2}, Lcom/google/obf/hj;->c()Lcom/google/obf/hl;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/obf/gp;->b:Lcom/google/obf/hj;

    .line 63
    invoke-virtual {v2}, Lcom/google/obf/hj;->c()Lcom/google/obf/hl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/obf/hl;->b()Lcom/google/obf/hi$a;

    move-result-object v2

    sget-object v3, Lcom/google/obf/hi$a;->a:Lcom/google/obf/hi$a;

    if-ne v2, v3, :cond_3

    .line 64
    invoke-interface {v1}, Lcom/google/ads/interactivemedia/v3/api/AdDisplayContainer;->getPlayer()Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;

    move-result-object v1

    if-nez v1, :cond_3

    .line 65
    iget-object v1, p0, Lcom/google/obf/gp;->d:Lcom/google/obf/he;

    new-instance v2, Lcom/google/obf/gk;

    new-instance v3, Lcom/google/ads/interactivemedia/v3/api/AdError;

    sget-object v4, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;->LOAD:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;

    sget-object v5, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;->INVALID_ARGUMENTS:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;

    const-string v6, "Ad Player was not provided."

    invoke-direct {v3, v4, v5, v6}, Lcom/google/ads/interactivemedia/v3/api/AdError;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/google/obf/gk;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError;)V

    invoke-virtual {v1, v2}, Lcom/google/obf/he;->a(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent;)V

    goto :goto_0

    .line 67
    :cond_3
    invoke-interface {p1}, Lcom/google/ads/interactivemedia/v3/api/AdsRequest;->getAdTagUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/obf/jb;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 68
    invoke-interface {p1}, Lcom/google/ads/interactivemedia/v3/api/AdsRequest;->getAdsResponse()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/obf/jb;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 69
    iget-object v1, p0, Lcom/google/obf/gp;->d:Lcom/google/obf/he;

    new-instance v2, Lcom/google/obf/gk;

    new-instance v3, Lcom/google/ads/interactivemedia/v3/api/AdError;

    sget-object v4, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;->LOAD:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;

    sget-object v5, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;->INVALID_ARGUMENTS:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;

    const-string v6, "Ad tag url must non-null and non empty."

    invoke-direct {v3, v4, v5, v6}, Lcom/google/ads/interactivemedia/v3/api/AdError;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/google/obf/gk;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError;)V

    invoke-virtual {v1, v2}, Lcom/google/obf/he;->a(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent;)V

    goto/16 :goto_0

    .line 71
    :cond_4
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method static synthetic b(Lcom/google/obf/gp;)Lcom/google/obf/hj;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/obf/gp;->b:Lcom/google/obf/hj;

    return-object v0
.end method

.method private b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/obf/gp;->k:Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/gp;->k:Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;->ignoreStrictModeFalsePositives()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    invoke-static {}, Landroid/os/StrictMode;->getThreadPolicy()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v1

    .line 44
    new-instance v0, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {v0, v1}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 45
    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->permitDiskReads()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    .line 47
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 48
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 49
    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 51
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/obf/gp;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/obf/gp;->c:Landroid/content/Context;

    return-object v0
.end method

.method private c()Ljava/lang/String;
    .locals 4

    .prologue
    .line 96
    const-string v0, "android%s:%s:%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "3.7.4"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/obf/gp;->c:Landroid/content/Context;

    .line 97
    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 98
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/google/obf/gp;)Lcom/google/obf/he;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/obf/gp;->d:Lcom/google/obf/he;

    return-object v0
.end method

.method private d()Ljava/lang/String;
    .locals 6

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/obf/gp;->c:Landroid/content/Context;

    const-string v1, "android.permission.ACCESS_NETWORK_STATE"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    const-string v0, "IMASDK"

    const-string v1, "Host application doesn\'t have ACCESS_NETWORK_STATE permission"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    const-string v0, "android:0"

    .line 107
    :goto_0
    return-object v0

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/google/obf/gp;->c:Landroid/content/Context;

    const-string v1, "connectivity"

    .line 103
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 104
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 105
    if-nez v0, :cond_1

    .line 106
    const-string v0, "android:0"

    goto :goto_0

    .line 107
    :cond_1
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "android:%d:%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private e()Lcom/google/obf/gp$b;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 108
    iget-object v1, p0, Lcom/google/obf/gp;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 109
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    const-string v4, "market://details?id=com.google.ads.interactivemedia.v3"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 110
    const/high16 v3, 0x10000

    .line 111
    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 112
    if-nez v2, :cond_1

    .line 123
    :cond_0
    :goto_0
    return-object v0

    .line 114
    :cond_1
    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 115
    if-eqz v2, :cond_0

    .line 117
    :try_start_0
    iget-object v3, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 118
    if-eqz v1, :cond_0

    .line 123
    iget v0, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    iget-object v1, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/obf/gp$b;->create(ILjava/lang/String;)Lcom/google/obf/gp$b;

    move-result-object v0

    goto :goto_0

    .line 121
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/obf/gp;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/obf/gp;->g:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic f(Lcom/google/obf/gp;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/obf/gp;->i:Ljava/lang/Object;

    return-object v0
.end method

.method private f()Z
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xd
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 124
    iget-object v0, p0, Lcom/google/obf/gp;->k:Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/gp;->k:Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/v3/impl/data/TestingConfiguration;->forceTvMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    :goto_0
    return v1

    .line 126
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xc

    if-gt v0, v3, :cond_1

    move v1, v2

    .line 127
    goto :goto_0

    .line 128
    :cond_1
    iget-object v0, p0, Lcom/google/obf/gp;->c:Landroid/content/Context;

    const-string v3, "uimode"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/UiModeManager;

    .line 129
    if-eqz v0, :cond_2

    .line 130
    invoke-virtual {v0}, Landroid/app/UiModeManager;->getCurrentModeType()I

    move-result v0

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    move v1, v0

    .line 131
    goto :goto_0

    :cond_2
    move v0, v2

    .line 130
    goto :goto_1
.end method

.method static synthetic g(Lcom/google/obf/gp;)Lcom/google/obf/ik;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/obf/gp;->h:Lcom/google/obf/ik;

    return-object v0
.end method

.method static synthetic h(Lcom/google/obf/gp;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/google/obf/gp;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/google/obf/gp;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/google/obf/gp;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/google/obf/gp;)Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/obf/gp;->j:Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;

    return-object v0
.end method

.method static synthetic k(Lcom/google/obf/gp;)Lcom/google/obf/gp$b;
    .locals 1

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/google/obf/gp;->e()Lcom/google/obf/gp$b;

    move-result-object v0

    return-object v0
.end method

.method static synthetic l(Lcom/google/obf/gp;)Z
    .locals 1

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/google/obf/gp;->f()Z

    move-result v0

    return v0
.end method


# virtual methods
.method a(Lcom/google/ads/interactivemedia/v3/api/AdsManagerLoadedEvent;)V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/obf/gp;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/v3/api/AdsLoader$AdsLoadedListener;

    .line 93
    invoke-interface {v0, p1}, Lcom/google/ads/interactivemedia/v3/api/AdsLoader$AdsLoadedListener;->onAdsManagerLoaded(Lcom/google/ads/interactivemedia/v3/api/AdsManagerLoadedEvent;)V

    goto :goto_0

    .line 95
    :cond_0
    return-void
.end method

.method a(Lcom/google/ads/interactivemedia/v3/api/AdsRequest;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/obf/gp;->a(Lcom/google/ads/interactivemedia/v3/api/AdsRequest;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 34
    :goto_0
    return-void

    .line 30
    :cond_0
    iget-object v0, p0, Lcom/google/obf/gp;->f:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    iget-object v0, p0, Lcom/google/obf/gp;->b:Lcom/google/obf/hj;

    iget-object v1, p0, Lcom/google/obf/gp;->a:Lcom/google/obf/hj$b;

    invoke-virtual {v0, v1, p2}, Lcom/google/obf/hj;->a(Lcom/google/obf/hj$b;Ljava/lang/String;)V

    .line 32
    iget-object v0, p0, Lcom/google/obf/gp;->b:Lcom/google/obf/hj;

    invoke-interface {p1}, Lcom/google/ads/interactivemedia/v3/api/AdsRequest;->getAdDisplayContainer()Lcom/google/ads/interactivemedia/v3/api/AdDisplayContainer;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/google/obf/hj;->a(Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;Ljava/lang/String;)V

    .line 33
    new-instance v0, Lcom/google/obf/gp$a;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/obf/gp$a;-><init>(Lcom/google/obf/gp;Lcom/google/ads/interactivemedia/v3/api/AdsRequest;Ljava/lang/String;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {p1}, Lcom/google/ads/interactivemedia/v3/api/AdsRequest;->getAdTagUrl()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/obf/gp$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public addAdErrorListener(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent$AdErrorListener;)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/obf/gp;->d:Lcom/google/obf/he;

    invoke-virtual {v0, p1}, Lcom/google/obf/he;->a(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent$AdErrorListener;)V

    .line 89
    return-void
.end method

.method public addAdsLoadedListener(Lcom/google/ads/interactivemedia/v3/api/AdsLoader$AdsLoadedListener;)V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/obf/gp;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    return-void
.end method

.method public requestAds(Lcom/google/ads/interactivemedia/v3/api/AdsRequest;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/obf/gp;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/obf/gp;->a(Lcom/google/ads/interactivemedia/v3/api/AdsRequest;Ljava/lang/String;)V

    .line 24
    return-void
.end method
