.class public final Lru/cn/tv/billing/BillingViewModel$$Factory;
.super Ljava/lang/Object;
.source "BillingViewModel$$Factory.java"

# interfaces
.implements Ltoothpick/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ltoothpick/Factory",
        "<",
        "Lru/cn/tv/billing/BillingViewModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic createInstance(Ltoothpick/Scope;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lru/cn/tv/billing/BillingViewModel$$Factory;->createInstance(Ltoothpick/Scope;)Lru/cn/tv/billing/BillingViewModel;

    move-result-object v0

    return-object v0
.end method

.method public createInstance(Ltoothpick/Scope;)Lru/cn/tv/billing/BillingViewModel;
    .locals 4
    .param p1, "scope"    # Ltoothpick/Scope;

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lru/cn/tv/billing/BillingViewModel$$Factory;->getTargetScope(Ltoothpick/Scope;)Ltoothpick/Scope;

    move-result-object p1

    .line 13
    const-class v3, Lru/cn/mvvm/RxLoader;

    invoke-interface {p1, v3}, Ltoothpick/Scope;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/mvvm/RxLoader;

    .line 14
    .local v1, "param1":Lru/cn/mvvm/RxLoader;
    const-class v3, Landroid/app/Application;

    invoke-interface {p1, v3}, Ltoothpick/Scope;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Application;

    .line 15
    .local v2, "param2":Landroid/app/Application;
    new-instance v0, Lru/cn/tv/billing/BillingViewModel;

    invoke-direct {v0, v1, v2}, Lru/cn/tv/billing/BillingViewModel;-><init>(Lru/cn/mvvm/RxLoader;Landroid/app/Application;)V

    .line 16
    .local v0, "billingViewModel":Lru/cn/tv/billing/BillingViewModel;
    return-object v0
.end method

.method public getTargetScope(Ltoothpick/Scope;)Ltoothpick/Scope;
    .locals 0
    .param p1, "scope"    # Ltoothpick/Scope;

    .prologue
    .line 21
    return-object p1
.end method

.method public hasProvidesSingletonInScopeAnnotation()Z
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    return v0
.end method

.method public hasScopeAnnotation()Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    return v0
.end method
