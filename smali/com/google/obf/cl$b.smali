.class final Lcom/google/obf/cl$b;
.super Lcom/google/obf/cl$d;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/cl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation


# instance fields
.field private final a:Lcom/google/obf/cb;

.field private final b:Lcom/google/obf/cj;

.field private final c:Lcom/google/obf/dv;

.field private d:I

.field private e:I

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:I

.field private j:I

.field private k:Z

.field private l:J


# direct methods
.method public constructor <init>(Lcom/google/obf/cb;Lcom/google/obf/cj;)V
    .locals 2

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/obf/cl$d;-><init>(Lcom/google/obf/cl$1;)V

    .line 2
    iput-object p1, p0, Lcom/google/obf/cl$b;->a:Lcom/google/obf/cb;

    .line 3
    iput-object p2, p0, Lcom/google/obf/cl$b;->b:Lcom/google/obf/cj;

    .line 4
    new-instance v0, Lcom/google/obf/dv;

    const/16 v1, 0xa

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Lcom/google/obf/dv;-><init>([B)V

    iput-object v0, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    .line 5
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/cl$b;->d:I

    .line 6
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 46
    iput p1, p0, Lcom/google/obf/cl$b;->d:I

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/cl$b;->e:I

    .line 48
    return-void
.end method

.method private a(Lcom/google/obf/dw;[BI)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 49
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v1

    iget v2, p0, Lcom/google/obf/cl$b;->e:I

    sub-int v2, p3, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 50
    if-gtz v1, :cond_1

    .line 56
    :cond_0
    :goto_0
    return v0

    .line 52
    :cond_1
    if-nez p2, :cond_2

    .line 53
    invoke-virtual {p1, v1}, Lcom/google/obf/dw;->d(I)V

    .line 55
    :goto_1
    iget v2, p0, Lcom/google/obf/cl$b;->e:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/obf/cl$b;->e:I

    .line 56
    iget v1, p0, Lcom/google/obf/cl$b;->e:I

    if-eq v1, p3, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 54
    :cond_2
    iget v2, p0, Lcom/google/obf/cl$b;->e:I

    invoke-virtual {p1, p2, v2, v1}, Lcom/google/obf/dw;->a([BII)V

    goto :goto_1
.end method

.method private b()Z
    .locals 6

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v5, -0x1

    .line 57
    iget-object v2, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    invoke-virtual {v2, v0}, Lcom/google/obf/dv;->a(I)V

    .line 58
    iget-object v2, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    const/16 v3, 0x18

    invoke-virtual {v2, v3}, Lcom/google/obf/dv;->c(I)I

    move-result v2

    .line 59
    if-eq v2, v1, :cond_0

    .line 60
    const-string v1, "TsExtractor"

    const/16 v3, 0x29

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unexpected start code prefix: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    iput v5, p0, Lcom/google/obf/cl$b;->j:I

    .line 75
    :goto_0
    return v0

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    invoke-virtual {v0, v4}, Lcom/google/obf/dv;->b(I)V

    .line 64
    iget-object v0, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    const/16 v2, 0x10

    invoke-virtual {v0, v2}, Lcom/google/obf/dv;->c(I)I

    move-result v0

    .line 65
    iget-object v2, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/google/obf/dv;->b(I)V

    .line 66
    iget-object v2, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    invoke-virtual {v2}, Lcom/google/obf/dv;->b()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/obf/cl$b;->k:Z

    .line 67
    iget-object v2, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/obf/dv;->b(I)V

    .line 68
    iget-object v2, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    invoke-virtual {v2}, Lcom/google/obf/dv;->b()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/obf/cl$b;->f:Z

    .line 69
    iget-object v2, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    invoke-virtual {v2}, Lcom/google/obf/dv;->b()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/obf/cl$b;->g:Z

    .line 70
    iget-object v2, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Lcom/google/obf/dv;->b(I)V

    .line 71
    iget-object v2, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    invoke-virtual {v2, v4}, Lcom/google/obf/dv;->c(I)I

    move-result v2

    iput v2, p0, Lcom/google/obf/cl$b;->i:I

    .line 72
    if-nez v0, :cond_1

    .line 73
    iput v5, p0, Lcom/google/obf/cl$b;->j:I

    :goto_1
    move v0, v1

    .line 75
    goto :goto_0

    .line 74
    :cond_1
    add-int/lit8 v0, v0, 0x6

    add-int/lit8 v0, v0, -0x9

    iget v2, p0, Lcom/google/obf/cl$b;->i:I

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/google/obf/cl$b;->j:I

    goto :goto_1
.end method

.method private c()V
    .locals 9

    .prologue
    const/16 v8, 0x1e

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/16 v7, 0xf

    const/4 v6, 0x1

    .line 76
    iget-object v0, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/obf/dv;->a(I)V

    .line 77
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/obf/cl$b;->l:J

    .line 78
    iget-boolean v0, p0, Lcom/google/obf/cl$b;->f:Z

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    invoke-virtual {v0, v5}, Lcom/google/obf/dv;->b(I)V

    .line 80
    iget-object v0, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    invoke-virtual {v0, v4}, Lcom/google/obf/dv;->c(I)I

    move-result v0

    int-to-long v0, v0

    shl-long/2addr v0, v8

    .line 81
    iget-object v2, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    invoke-virtual {v2, v6}, Lcom/google/obf/dv;->b(I)V

    .line 82
    iget-object v2, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    invoke-virtual {v2, v7}, Lcom/google/obf/dv;->c(I)I

    move-result v2

    shl-int/lit8 v2, v2, 0xf

    int-to-long v2, v2

    or-long/2addr v0, v2

    .line 83
    iget-object v2, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    invoke-virtual {v2, v6}, Lcom/google/obf/dv;->b(I)V

    .line 84
    iget-object v2, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    invoke-virtual {v2, v7}, Lcom/google/obf/dv;->c(I)I

    move-result v2

    int-to-long v2, v2

    or-long/2addr v0, v2

    .line 85
    iget-object v2, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    invoke-virtual {v2, v6}, Lcom/google/obf/dv;->b(I)V

    .line 86
    iget-boolean v2, p0, Lcom/google/obf/cl$b;->h:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/obf/cl$b;->g:Z

    if-eqz v2, :cond_0

    .line 87
    iget-object v2, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    invoke-virtual {v2, v5}, Lcom/google/obf/dv;->b(I)V

    .line 88
    iget-object v2, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    invoke-virtual {v2, v4}, Lcom/google/obf/dv;->c(I)I

    move-result v2

    int-to-long v2, v2

    shl-long/2addr v2, v8

    .line 89
    iget-object v4, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    invoke-virtual {v4, v6}, Lcom/google/obf/dv;->b(I)V

    .line 90
    iget-object v4, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    invoke-virtual {v4, v7}, Lcom/google/obf/dv;->c(I)I

    move-result v4

    shl-int/lit8 v4, v4, 0xf

    int-to-long v4, v4

    or-long/2addr v2, v4

    .line 91
    iget-object v4, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    invoke-virtual {v4, v6}, Lcom/google/obf/dv;->b(I)V

    .line 92
    iget-object v4, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    invoke-virtual {v4, v7}, Lcom/google/obf/dv;->c(I)I

    move-result v4

    int-to-long v4, v4

    or-long/2addr v2, v4

    .line 93
    iget-object v4, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    invoke-virtual {v4, v6}, Lcom/google/obf/dv;->b(I)V

    .line 94
    iget-object v4, p0, Lcom/google/obf/cl$b;->b:Lcom/google/obf/cj;

    invoke-virtual {v4, v2, v3}, Lcom/google/obf/cj;->a(J)J

    .line 95
    iput-boolean v6, p0, Lcom/google/obf/cl$b;->h:Z

    .line 96
    :cond_0
    iget-object v2, p0, Lcom/google/obf/cl$b;->b:Lcom/google/obf/cj;

    invoke-virtual {v2, v0, v1}, Lcom/google/obf/cj;->a(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/obf/cl$b;->l:J

    .line 97
    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7
    iput v0, p0, Lcom/google/obf/cl$b;->d:I

    .line 8
    iput v0, p0, Lcom/google/obf/cl$b;->e:I

    .line 9
    iput-boolean v0, p0, Lcom/google/obf/cl$b;->h:Z

    .line 10
    iget-object v0, p0, Lcom/google/obf/cl$b;->a:Lcom/google/obf/cb;

    invoke-virtual {v0}, Lcom/google/obf/cb;->a()V

    .line 11
    return-void
.end method

.method public a(Lcom/google/obf/dw;ZLcom/google/obf/al;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    const/4 v5, -0x1

    .line 12
    if-eqz p2, :cond_0

    .line 13
    iget v0, p0, Lcom/google/obf/cl$b;->d:I

    packed-switch v0, :pswitch_data_0

    .line 20
    :goto_0
    :pswitch_0
    invoke-direct {p0, v6}, Lcom/google/obf/cl$b;->a(I)V

    .line 21
    :cond_0
    :goto_1
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v0

    if-lez v0, :cond_5

    .line 22
    iget v0, p0, Lcom/google/obf/cl$b;->d:I

    packed-switch v0, :pswitch_data_1

    goto :goto_1

    .line 23
    :pswitch_1
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/obf/dw;->d(I)V

    goto :goto_1

    .line 15
    :pswitch_2
    const-string v0, "TsExtractor"

    const-string v2, "Unexpected start indicator reading extended header"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 17
    :pswitch_3
    iget v0, p0, Lcom/google/obf/cl$b;->j:I

    if-eq v0, v5, :cond_1

    .line 18
    const-string v0, "TsExtractor"

    iget v2, p0, Lcom/google/obf/cl$b;->j:I

    const/16 v3, 0x3b

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unexpected start indicator: expected "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " more bytes"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 19
    :cond_1
    iget-object v0, p0, Lcom/google/obf/cl$b;->a:Lcom/google/obf/cb;

    invoke-virtual {v0}, Lcom/google/obf/cb;->b()V

    goto :goto_0

    .line 25
    :pswitch_4
    iget-object v0, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    iget-object v0, v0, Lcom/google/obf/dv;->a:[B

    const/16 v2, 0x9

    invoke-direct {p0, p1, v0, v2}, Lcom/google/obf/cl$b;->a(Lcom/google/obf/dw;[BI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    invoke-direct {p0}, Lcom/google/obf/cl$b;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    :goto_2
    invoke-direct {p0, v0}, Lcom/google/obf/cl$b;->a(I)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    .line 27
    :pswitch_5
    const/16 v0, 0xa

    iget v2, p0, Lcom/google/obf/cl$b;->i:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 28
    iget-object v2, p0, Lcom/google/obf/cl$b;->c:Lcom/google/obf/dv;

    iget-object v2, v2, Lcom/google/obf/dv;->a:[B

    invoke-direct {p0, p1, v2, v0}, Lcom/google/obf/cl$b;->a(Lcom/google/obf/dw;[BI)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iget v2, p0, Lcom/google/obf/cl$b;->i:I

    .line 29
    invoke-direct {p0, p1, v0, v2}, Lcom/google/obf/cl$b;->a(Lcom/google/obf/dw;[BI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    invoke-direct {p0}, Lcom/google/obf/cl$b;->c()V

    .line 31
    iget-object v0, p0, Lcom/google/obf/cl$b;->a:Lcom/google/obf/cb;

    iget-wide v2, p0, Lcom/google/obf/cl$b;->l:J

    iget-boolean v4, p0, Lcom/google/obf/cl$b;->k:Z

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/obf/cb;->a(JZ)V

    .line 32
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/obf/cl$b;->a(I)V

    goto/16 :goto_1

    .line 33
    :pswitch_6
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v0

    .line 34
    iget v2, p0, Lcom/google/obf/cl$b;->j:I

    if-ne v2, v5, :cond_4

    move v2, v1

    .line 35
    :goto_3
    if-lez v2, :cond_3

    .line 36
    sub-int/2addr v0, v2

    .line 37
    invoke-virtual {p1}, Lcom/google/obf/dw;->d()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {p1, v2}, Lcom/google/obf/dw;->b(I)V

    .line 38
    :cond_3
    iget-object v2, p0, Lcom/google/obf/cl$b;->a:Lcom/google/obf/cb;

    invoke-virtual {v2, p1}, Lcom/google/obf/cb;->a(Lcom/google/obf/dw;)V

    .line 39
    iget v2, p0, Lcom/google/obf/cl$b;->j:I

    if-eq v2, v5, :cond_0

    .line 40
    iget v2, p0, Lcom/google/obf/cl$b;->j:I

    sub-int v0, v2, v0

    iput v0, p0, Lcom/google/obf/cl$b;->j:I

    .line 41
    iget v0, p0, Lcom/google/obf/cl$b;->j:I

    if-nez v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/google/obf/cl$b;->a:Lcom/google/obf/cb;

    invoke-virtual {v0}, Lcom/google/obf/cb;->b()V

    .line 43
    invoke-direct {p0, v6}, Lcom/google/obf/cl$b;->a(I)V

    goto/16 :goto_1

    .line 34
    :cond_4
    iget v2, p0, Lcom/google/obf/cl$b;->j:I

    sub-int v2, v0, v2

    goto :goto_3

    .line 45
    :cond_5
    return-void

    .line 13
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 22
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
