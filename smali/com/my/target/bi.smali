.class public Lcom/my/target/bi;
.super Ljava/lang/Object;
.source "VastParser.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/my/target/ag;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final fw:[Ljava/lang/String;

.field private static final fx:[Ljava/lang/String;


# instance fields
.field private final adConfig:Lcom/my/target/b;

.field private final bJ:Landroid/content/Context;

.field private final bO:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/aq;",
            ">;"
        }
    .end annotation
.end field

.field private final bP:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/aq;",
            ">;"
        }
    .end annotation
.end field

.field private final bS:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/ap;",
            ">;"
        }
    .end annotation
.end field

.field private ctaText:Ljava/lang/String;

.field private final eo:Lcom/my/target/ae;

.field private fA:Z

.field private fB:Lcom/my/target/ae;

.field private final fy:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/ai;",
            ">;"
        }
    .end annotation
.end field

.field private final fz:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/aj",
            "<TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 40
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "video/mp4"

    aput-object v1, v0, v3

    const-string v1, "application/vnd.apple.mpegurl"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "application/x-mpegurl"

    aput-object v2, v0, v1

    sput-object v0, Lcom/my/target/bi;->fw:[Ljava/lang/String;

    .line 41
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "linkTxt"

    aput-object v1, v0, v3

    sput-object v0, Lcom/my/target/bi;->fx:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/my/target/b;Lcom/my/target/ae;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/bi;->bP:Ljava/util/ArrayList;

    .line 150
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/bi;->bS:Ljava/util/ArrayList;

    .line 151
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/bi;->fy:Ljava/util/ArrayList;

    .line 152
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/bi;->bO:Ljava/util/ArrayList;

    .line 153
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/bi;->fz:Ljava/util/ArrayList;

    .line 160
    iput-object p1, p0, Lcom/my/target/bi;->adConfig:Lcom/my/target/b;

    .line 161
    iput-object p2, p0, Lcom/my/target/bi;->eo:Lcom/my/target/ae;

    .line 162
    iput-object p3, p0, Lcom/my/target/bi;->bJ:Landroid/content/Context;

    .line 163
    return-void
.end method

.method private static E(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 112
    const-string v0, "&amp;"

    const-string v1, "&"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "&lt;"

    const-string v2, "<"

    .line 113
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "&gt;"

    const-string v2, ">"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 112
    return-object v0
.end method

.method private G(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 216
    :try_start_0
    const-string v0, "utf-8"

    invoke-static {p1, v0}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/bi;->ctaText:Ljava/lang/String;

    .line 217
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VAST linkTxt decoded text = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/bi;->ctaText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    :goto_0
    return-void

    .line 219
    :catch_0
    move-exception v0

    .line 221
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unable to decode linkTxt extension: \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Lcom/my/target/b;Lcom/my/target/ae;Landroid/content/Context;)Lcom/my/target/bi;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/my/target/ag;",
            ">(",
            "Lcom/my/target/b;",
            "Lcom/my/target/ae;",
            "Landroid/content/Context;",
            ")",
            "Lcom/my/target/bi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 47
    new-instance v0, Lcom/my/target/bi;

    invoke-direct {v0, p0, p1, p2}, Lcom/my/target/bi;-><init>(Lcom/my/target/b;Lcom/my/target/ae;Landroid/content/Context;)V

    return-object v0
.end method

.method private static a(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    invoke-interface {p1, v0, p0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(FLjava/lang/String;Lcom/my/target/ah;)V
    .locals 2

    .prologue
    .line 989
    invoke-static {p2}, Lcom/my/target/ap;->v(Ljava/lang/String;)Lcom/my/target/ap;

    move-result-object v0

    .line 990
    if-eqz p3, :cond_0

    .line 992
    invoke-virtual {p3}, Lcom/my/target/ah;->getDuration()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/my/target/ap;->c(F)V

    .line 993
    invoke-virtual {p3}, Lcom/my/target/ah;->getStatHolder()Lcom/my/target/ar;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/my/target/ar;->a(Lcom/my/target/ap;)V

    .line 1000
    :goto_0
    return-void

    .line 997
    :cond_0
    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/my/target/ap;->d(F)V

    .line 998
    iget-object v1, p0, Lcom/my/target/bi;->bS:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/my/target/ah;)V
    .locals 2

    .prologue
    .line 946
    if-eqz p3, :cond_0

    .line 948
    invoke-virtual {p3}, Lcom/my/target/ah;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/my/target/ar;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 954
    :goto_0
    return-void

    .line 952
    :cond_0
    iget-object v0, p0, Lcom/my/target/bi;->bO:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Lcom/my/target/aq;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/my/target/aq;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 227
    invoke-static {p2}, Lcom/my/target/az;->y(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/my/target/az;->z(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/bi;->adConfig:Lcom/my/target/b;

    .line 228
    invoke-virtual {v1}, Lcom/my/target/b;->getSlotId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/az;->h(I)Lcom/my/target/az;

    move-result-object v0

    .line 229
    invoke-virtual {v0, p1}, Lcom/my/target/az;->B(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/bi;->eo:Lcom/my/target/ae;

    .line 230
    invoke-virtual {v1}, Lcom/my/target/ae;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/az;->A(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/bi;->bJ:Landroid/content/Context;

    .line 231
    invoke-virtual {v0, v1}, Lcom/my/target/az;->e(Landroid/content/Context;)V

    .line 232
    return-void
.end method

.method private static a(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 2

    .prologue
    .line 52
    invoke-static {p0}, Lcom/my/target/bi;->e(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 71
    :cond_0
    return-void

    .line 56
    :cond_1
    const/4 v0, 0x1

    .line 57
    :goto_0
    if-eqz v0, :cond_0

    .line 59
    invoke-static {p0}, Lcom/my/target/bi;->b(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 65
    :pswitch_0
    add-int/lit8 v0, v0, 0x1

    .line 66
    goto :goto_0

    .line 62
    :pswitch_1
    add-int/lit8 v0, v0, -0x1

    .line 63
    goto :goto_0

    .line 59
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;I)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 341
    const/4 v0, 0x0

    .line 342
    :cond_0
    :goto_0
    invoke-static {p1}, Lcom/my/target/bi;->d(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v1

    if-ne v1, v3, :cond_5

    .line 344
    invoke-static {p1}, Lcom/my/target/bi;->e(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 348
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 349
    const-string v2, "Impression"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 351
    invoke-direct {p0, p1}, Lcom/my/target/bi;->j(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 353
    :cond_1
    const-string v2, "Creatives"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 355
    invoke-direct {p0, p1}, Lcom/my/target/bi;->k(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 357
    :cond_2
    const-string v2, "Extensions"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 359
    invoke-direct {p0, p1}, Lcom/my/target/bi;->i(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 361
    :cond_3
    const-string v2, "VASTAdTagURI"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 363
    invoke-static {p1}, Lcom/my/target/bi;->c(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 367
    :cond_4
    invoke-static {p1}, Lcom/my/target/bi;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 371
    :cond_5
    if-eqz v0, :cond_7

    .line 373
    invoke-static {v0}, Lcom/my/target/ae;->m(Ljava/lang/String;)Lcom/my/target/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/bi;->fB:Lcom/my/target/ae;

    .line 374
    iget-object v0, p0, Lcom/my/target/bi;->fB:Lcom/my/target/ae;

    add-int/lit8 v1, p2, 0x1

    invoke-virtual {v0, v1}, Lcom/my/target/ae;->a(I)V

    .line 375
    iget-object v0, p0, Lcom/my/target/bi;->fB:Lcom/my/target/ae;

    iget-object v1, p0, Lcom/my/target/bi;->bO:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/my/target/ae;->c(Ljava/util/ArrayList;)V

    .line 376
    iget-object v0, p0, Lcom/my/target/bi;->fB:Lcom/my/target/ae;

    iget-object v1, p0, Lcom/my/target/bi;->bS:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/my/target/ae;->g(Ljava/util/ArrayList;)V

    .line 377
    iget-object v0, p0, Lcom/my/target/bi;->fB:Lcom/my/target/ae;

    iget-object v1, p0, Lcom/my/target/bi;->bP:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/my/target/ae;->d(Ljava/util/ArrayList;)V

    .line 378
    iget-object v1, p0, Lcom/my/target/bi;->fB:Lcom/my/target/ae;

    iget-object v0, p0, Lcom/my/target/bi;->ctaText:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/my/target/bi;->ctaText:Ljava/lang/String;

    :goto_1
    invoke-virtual {v1, v0}, Lcom/my/target/ae;->setCtaText(Ljava/lang/String;)V

    .line 379
    iget-object v0, p0, Lcom/my/target/bi;->fB:Lcom/my/target/ae;

    iget-object v1, p0, Lcom/my/target/bi;->fy:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/my/target/ae;->b(Ljava/util/ArrayList;)V

    .line 380
    iget-object v0, p0, Lcom/my/target/bi;->eo:Lcom/my/target/ae;

    iget-object v1, p0, Lcom/my/target/bi;->fB:Lcom/my/target/ae;

    invoke-virtual {v0, v1}, Lcom/my/target/ae;->b(Lcom/my/target/ae;)V

    .line 386
    :goto_2
    return-void

    .line 378
    :cond_6
    iget-object v0, p0, Lcom/my/target/bi;->eo:Lcom/my/target/ae;

    invoke-virtual {v0}, Lcom/my/target/ae;->getCtaText()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 384
    :cond_7
    const-string v0, "got VAST wrapper, but no vastAdTagUri"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;Lcom/my/target/ah;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 910
    :cond_0
    :goto_0
    invoke-static {p1}, Lcom/my/target/bi;->d(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    if-ne v0, v3, :cond_4

    .line 912
    invoke-static {p1}, Lcom/my/target/bi;->e(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 916
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 917
    const-string v1, "Tracking"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 919
    const-string v0, "event"

    invoke-static {v0, p1}, Lcom/my/target/bi;->a(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 920
    const-string v1, "offset"

    invoke-static {v1, p1}, Lcom/my/target/bi;->a(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 922
    if-eqz v0, :cond_1

    .line 924
    const-string v2, "progress"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 926
    invoke-static {p1}, Lcom/my/target/bi;->c(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2, p2}, Lcom/my/target/bi;->b(Ljava/lang/String;Ljava/lang/String;Lcom/my/target/ah;)V

    .line 933
    :cond_1
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Added VAST tracking \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 930
    :cond_2
    invoke-static {p1}, Lcom/my/target/bi;->c(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, p2}, Lcom/my/target/bi;->c(Ljava/lang/String;Ljava/lang/String;Lcom/my/target/ah;)V

    goto :goto_1

    .line 937
    :cond_3
    invoke-static {p1}, Lcom/my/target/bi;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 940
    :cond_4
    return-void
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;Lcom/my/target/aj;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x0

    .line 521
    :cond_0
    :goto_0
    invoke-static {p1}, Lcom/my/target/bi;->d(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    if-ne v0, v6, :cond_6

    .line 523
    invoke-static {p1}, Lcom/my/target/bi;->e(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    if-ne v0, v6, :cond_0

    .line 527
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 529
    const-string v2, "Linear"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 531
    const-string v0, "skipoffset"

    invoke-static {v0, p1}, Lcom/my/target/bi;->a(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 532
    invoke-direct {p0, p1, p2, v0}, Lcom/my/target/bi;->a(Lorg/xmlpull/v1/XmlPullParser;Lcom/my/target/aj;Ljava/lang/String;)V

    goto :goto_0

    .line 534
    :cond_1
    if-eqz v0, :cond_5

    const-string v2, "CompanionAds"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 536
    const-string v0, "required"

    invoke-static {v0, p1}, Lcom/my/target/bi;->a(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v2

    .line 538
    if-eqz v2, :cond_2

    .line 540
    const-string v0, "all"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "any"

    .line 541
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "none"

    .line 542
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 544
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/my/target/aj;->getId()Ljava/lang/String;

    move-result-object v0

    :goto_1
    const-string v3, "Bad value"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Wrong companion required attribute:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v3, v2}, Lcom/my/target/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v1

    .line 549
    :cond_2
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lcom/my/target/aj;->getId()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {p0, p1, v0, v2}, Lcom/my/target/bi;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 544
    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 549
    goto :goto_2

    .line 553
    :cond_5
    invoke-static {p1}, Lcom/my/target/bi;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 556
    :cond_6
    return-void
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;Lcom/my/target/aj;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 1103
    :cond_0
    :goto_0
    invoke-static {p1}, Lcom/my/target/bi;->d(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 1105
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1106
    invoke-static {p1}, Lcom/my/target/bi;->e(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 1110
    const-string v1, "Duration"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1112
    if-eqz p2, :cond_0

    .line 1114
    invoke-direct {p0, p1, p2}, Lcom/my/target/bi;->b(Lorg/xmlpull/v1/XmlPullParser;Lcom/my/target/aj;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1150
    :cond_1
    :goto_1
    return-void

    .line 1119
    :cond_2
    invoke-virtual {p0, p2, p3}, Lcom/my/target/bi;->a(Lcom/my/target/aj;Ljava/lang/String;)V

    goto :goto_0

    .line 1122
    :cond_3
    const-string v1, "TrackingEvents"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1124
    invoke-direct {p0, p1, p2}, Lcom/my/target/bi;->a(Lorg/xmlpull/v1/XmlPullParser;Lcom/my/target/ah;)V

    goto :goto_0

    .line 1126
    :cond_4
    const-string v1, "MediaFiles"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1128
    if-eqz p2, :cond_0

    .line 1133
    invoke-direct {p0, p1, p2}, Lcom/my/target/bi;->d(Lorg/xmlpull/v1/XmlPullParser;Lcom/my/target/aj;)V

    .line 1135
    invoke-virtual {p2}, Lcom/my/target/aj;->getMediaData()Lcom/my/target/ag;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1137
    const-string v0, "Unable to find valid mediafile!"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 1141
    :cond_5
    const-string v1, "VideoClicks"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1143
    invoke-direct {p0, p1, p2}, Lcom/my/target/bi;->c(Lorg/xmlpull/v1/XmlPullParser;Lcom/my/target/aj;)V

    goto :goto_0

    .line 1147
    :cond_6
    invoke-static {p1}, Lcom/my/target/bi;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 454
    const-string v0, "linkTxt"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    invoke-static {p1}, Lcom/my/target/bi;->c(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 457
    invoke-direct {p0, v0}, Lcom/my/target/bi;->G(Ljava/lang/String;)V

    .line 458
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "VAST linkTxt raw text: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 460
    :cond_0
    return-void
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 560
    :goto_0
    invoke-static {p1}, Lcom/my/target/bi;->d(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 562
    invoke-direct {p0, p1, p2, p3}, Lcom/my/target/bi;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 564
    :cond_0
    return-void
.end method

.method private au()V
    .locals 2

    .prologue
    .line 236
    iget-object v0, p0, Lcom/my/target/bi;->eo:Lcom/my/target/ae;

    invoke-virtual {v0}, Lcom/my/target/ae;->I()Ljava/util/ArrayList;

    move-result-object v0

    .line 237
    if-eqz v0, :cond_0

    .line 239
    iget-object v1, p0, Lcom/my/target/bi;->bS:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 241
    :cond_0
    iget-object v0, p0, Lcom/my/target/bi;->eo:Lcom/my/target/ae;

    invoke-virtual {v0}, Lcom/my/target/ae;->F()Ljava/util/ArrayList;

    move-result-object v0

    .line 242
    if-eqz v0, :cond_1

    .line 244
    iget-object v1, p0, Lcom/my/target/bi;->bP:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 246
    :cond_1
    iget-object v0, p0, Lcom/my/target/bi;->eo:Lcom/my/target/ae;

    invoke-virtual {v0}, Lcom/my/target/ae;->E()Ljava/util/ArrayList;

    move-result-object v0

    .line 247
    if-eqz v0, :cond_2

    .line 249
    iget-object v1, p0, Lcom/my/target/bi;->bO:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 251
    :cond_2
    iget-object v0, p0, Lcom/my/target/bi;->eo:Lcom/my/target/ae;

    invoke-virtual {v0}, Lcom/my/target/ae;->getCompanionBanners()Ljava/util/ArrayList;

    move-result-object v0

    .line 252
    if-eqz v0, :cond_3

    .line 254
    iget-object v1, p0, Lcom/my/target/bi;->fy:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 256
    :cond_3
    return-void
.end method

.method private av()V
    .locals 6

    .prologue
    .line 260
    iget-object v0, p0, Lcom/my/target/bi;->fz:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/aj;

    .line 262
    iget-object v1, p0, Lcom/my/target/bi;->ctaText:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 264
    iget-object v1, p0, Lcom/my/target/bi;->ctaText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/my/target/aj;->setCtaText(Ljava/lang/String;)V

    .line 271
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/my/target/bi;->bS:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/my/target/ap;

    .line 273
    invoke-virtual {v1}, Lcom/my/target/ap;->aa()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    div-float/2addr v4, v5

    invoke-virtual {v1}, Lcom/my/target/ap;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v4, v1, v0}, Lcom/my/target/bi;->a(FLjava/lang/String;Lcom/my/target/ah;)V

    goto :goto_1

    .line 266
    :cond_2
    iget-object v1, p0, Lcom/my/target/bi;->eo:Lcom/my/target/ae;

    invoke-virtual {v1}, Lcom/my/target/ae;->getCtaText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 268
    iget-object v1, p0, Lcom/my/target/bi;->eo:Lcom/my/target/ae;

    invoke-virtual {v1}, Lcom/my/target/ae;->getCtaText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/aj;->setCtaText(Ljava/lang/String;)V

    goto :goto_0

    .line 275
    :cond_3
    iget-object v1, p0, Lcom/my/target/bi;->bO:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/my/target/aq;

    .line 277
    invoke-virtual {v0}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/my/target/ar;->b(Lcom/my/target/aq;)V

    goto :goto_2

    .line 279
    :cond_4
    iget-object v1, p0, Lcom/my/target/bi;->fy:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/my/target/ai;

    .line 281
    invoke-virtual {v0, v1}, Lcom/my/target/aj;->addCompanion(Lcom/my/target/ai;)V

    goto :goto_3

    .line 284
    :cond_5
    return-void
.end method

.method private static b(Lorg/xmlpull/v1/XmlPullParser;)I
    .locals 1

    .prologue
    .line 77
    :try_start_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 87
    :goto_0
    return v0

    .line 79
    :catch_0
    move-exception v0

    .line 81
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 87
    :goto_1
    const/high16 v0, -0x80000000

    goto :goto_0

    .line 83
    :catch_1
    move-exception v0

    .line 85
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Lcom/my/target/ah;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 960
    .line 963
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/my/target/bi;->H(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 968
    :goto_0
    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1

    .line 970
    invoke-static {p2}, Lcom/my/target/ap;->v(Ljava/lang/String;)Lcom/my/target/ap;

    move-result-object v1

    .line 971
    invoke-virtual {v1, v0}, Lcom/my/target/ap;->c(F)V

    .line 972
    if-eqz p3, :cond_0

    .line 974
    invoke-virtual {p3}, Lcom/my/target/ah;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/my/target/ar;->a(Lcom/my/target/ap;)V

    .line 985
    :goto_1
    return-void

    .line 965
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 978
    :cond_0
    iget-object v0, p0, Lcom/my/target/bi;->bO:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 983
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unable to parse progress stat with value "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 569
    invoke-static {p1}, Lcom/my/target/bi;->e(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    if-eq v0, v6, :cond_1

    .line 683
    :cond_0
    :goto_0
    return-void

    .line 573
    :cond_1
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 574
    if-eqz v0, :cond_e

    const-string v1, "Companion"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 576
    const-string v0, "width"

    invoke-static {v0, p1}, Lcom/my/target/bi;->a(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 577
    const-string v0, "height"

    invoke-static {v0, p1}, Lcom/my/target/bi;->a(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v2

    .line 578
    const-string v0, "id"

    invoke-static {v0, p1}, Lcom/my/target/bi;->a(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 580
    invoke-static {}, Lcom/my/target/ai;->newBanner()Lcom/my/target/ai;

    move-result-object v3

    .line 581
    if-eqz v0, :cond_7

    :goto_1
    invoke-virtual {v3, v0}, Lcom/my/target/ai;->setId(Ljava/lang/String;)V

    .line 585
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/my/target/ai;->setWidth(I)V

    .line 586
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/my/target/ai;->setHeight(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 594
    :goto_2
    invoke-virtual {v3, p3}, Lcom/my/target/ai;->setRequired(Ljava/lang/String;)V

    .line 596
    const-string v0, "assetWidth"

    invoke-static {v0, p1}, Lcom/my/target/bi;->a(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 597
    const-string v1, "assetHeight"

    invoke-static {v1, p1}, Lcom/my/target/bi;->a(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 601
    :try_start_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 603
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/my/target/ai;->setAssetWidth(I)V

    .line 605
    :cond_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 607
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/my/target/ai;->setAssetHeight(I)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 615
    :cond_3
    :goto_3
    const-string v0, "expandedWidth"

    invoke-static {v0, p1}, Lcom/my/target/bi;->a(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 616
    const-string v1, "expandedHeight"

    invoke-static {v1, p1}, Lcom/my/target/bi;->a(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 619
    :try_start_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 621
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/my/target/ai;->setExpandedWidth(I)V

    .line 623
    :cond_4
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 625
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/my/target/ai;->setExpandedHeight(I)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    .line 633
    :cond_5
    :goto_4
    const-string v0, "adSlotID"

    invoke-static {v0, p1}, Lcom/my/target/bi;->a(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/my/target/ai;->setAdSlotID(Ljava/lang/String;)V

    .line 634
    const-string v0, "apiFramework"

    invoke-static {v0, p1}, Lcom/my/target/bi;->a(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/my/target/ai;->setApiFramework(Ljava/lang/String;)V

    .line 636
    iget-object v0, p0, Lcom/my/target/bi;->fy:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 638
    :cond_6
    :goto_5
    invoke-static {p1}, Lcom/my/target/bi;->d(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    if-ne v0, v6, :cond_0

    .line 640
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 641
    const-string v1, "StaticResource"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 643
    invoke-static {p1}, Lcom/my/target/bi;->c(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/my/target/ai;->setStaticResource(Ljava/lang/String;)V

    goto :goto_5

    .line 581
    :cond_7
    const-string v0, ""

    goto/16 :goto_1

    .line 588
    :catch_0
    move-exception v0

    .line 590
    const-string v0, "Bad value"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable  to convert required companion attributes, width = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " height = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p2, v0, v1}, Lcom/my/target/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 610
    :catch_1
    move-exception v0

    .line 612
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "wrong VAST asset dimensions: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 628
    :catch_2
    move-exception v0

    .line 630
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "wrong VAST expanded dimensions "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 645
    :cond_8
    const-string v1, "HTMLResource"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 647
    invoke-static {p1}, Lcom/my/target/bi;->c(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/my/target/ai;->setHtmlResource(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 649
    :cond_9
    const-string v1, "IFrameResource"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 651
    invoke-static {p1}, Lcom/my/target/bi;->c(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/my/target/ai;->setIframeResource(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 653
    :cond_a
    const-string v1, "CompanionClickThrough"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 655
    invoke-static {p1}, Lcom/my/target/bi;->c(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 656
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 658
    invoke-static {v0}, Lcom/my/target/bi;->E(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/my/target/ai;->setTrackingLink(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 661
    :cond_b
    const-string v1, "CompanionClickTracking"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 663
    invoke-static {p1}, Lcom/my/target/bi;->c(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 664
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 666
    invoke-virtual {v3}, Lcom/my/target/ai;->getStatHolder()Lcom/my/target/ar;

    move-result-object v1

    const-string v2, "click"

    invoke-virtual {v1, v2, v0}, Lcom/my/target/ar;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 669
    :cond_c
    const-string v1, "TrackingEvents"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 671
    invoke-direct {p0, p1, v3}, Lcom/my/target/bi;->a(Lorg/xmlpull/v1/XmlPullParser;Lcom/my/target/ah;)V

    goto/16 :goto_5

    .line 675
    :cond_d
    invoke-static {p1}, Lcom/my/target/bi;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto/16 :goto_5

    .line 681
    :cond_e
    invoke-static {p1}, Lcom/my/target/bi;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto/16 :goto_0
.end method

.method private b(Lorg/xmlpull/v1/XmlPullParser;Lcom/my/target/aj;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 724
    invoke-static {p1}, Lcom/my/target/bi;->c(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 728
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/my/target/bi;->H(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 733
    :goto_0
    cmpg-float v1, v0, v1

    if-gtz v1, :cond_0

    .line 735
    const/4 v0, 0x0

    .line 738
    :goto_1
    return v0

    .line 730
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 737
    :cond_0
    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setDuration(F)V

    .line 738
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private static c(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 92
    const-string v0, ""

    .line 93
    invoke-static {p0}, Lcom/my/target/bi;->b(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 95
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v0

    .line 96
    invoke-static {p0}, Lcom/my/target/bi;->d(Lorg/xmlpull/v1/XmlPullParser;)I

    .line 102
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 100
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No text: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;Lcom/my/target/ah;)V
    .locals 1

    .prologue
    .line 1004
    const-string v0, "start"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1006
    const-string v0, "playbackStarted"

    invoke-direct {p0, v0, p2, p3}, Lcom/my/target/bi;->a(Ljava/lang/String;Ljava/lang/String;Lcom/my/target/ah;)V

    .line 1068
    :cond_0
    :goto_0
    return-void

    .line 1008
    :cond_1
    const-string v0, "firstQuartile"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1010
    const/high16 v0, 0x3e800000    # 0.25f

    invoke-direct {p0, v0, p2, p3}, Lcom/my/target/bi;->a(FLjava/lang/String;Lcom/my/target/ah;)V

    goto :goto_0

    .line 1012
    :cond_2
    const-string v0, "midpoint"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1014
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-direct {p0, v0, p2, p3}, Lcom/my/target/bi;->a(FLjava/lang/String;Lcom/my/target/ah;)V

    goto :goto_0

    .line 1016
    :cond_3
    const-string v0, "thirdQuartile"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1018
    const/high16 v0, 0x3f400000    # 0.75f

    invoke-direct {p0, v0, p2, p3}, Lcom/my/target/bi;->a(FLjava/lang/String;Lcom/my/target/ah;)V

    goto :goto_0

    .line 1020
    :cond_4
    const-string v0, "complete"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1022
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, v0, p2, p3}, Lcom/my/target/bi;->a(FLjava/lang/String;Lcom/my/target/ah;)V

    goto :goto_0

    .line 1024
    :cond_5
    const-string v0, "creativeView"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1026
    const-string v0, "playbackStarted"

    invoke-direct {p0, v0, p2, p3}, Lcom/my/target/bi;->a(Ljava/lang/String;Ljava/lang/String;Lcom/my/target/ah;)V

    goto :goto_0

    .line 1028
    :cond_6
    const-string v0, "mute"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1030
    const-string v0, "volumeOff"

    invoke-direct {p0, v0, p2, p3}, Lcom/my/target/bi;->a(Ljava/lang/String;Ljava/lang/String;Lcom/my/target/ah;)V

    goto :goto_0

    .line 1032
    :cond_7
    const-string v0, "unmute"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1034
    const-string v0, "volumeOn"

    invoke-direct {p0, v0, p2, p3}, Lcom/my/target/bi;->a(Ljava/lang/String;Ljava/lang/String;Lcom/my/target/ah;)V

    goto :goto_0

    .line 1036
    :cond_8
    const-string v0, "pause"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1038
    const-string v0, "playbackPaused"

    invoke-direct {p0, v0, p2, p3}, Lcom/my/target/bi;->a(Ljava/lang/String;Ljava/lang/String;Lcom/my/target/ah;)V

    goto :goto_0

    .line 1040
    :cond_9
    const-string v0, "resume"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1042
    const-string v0, "playbackResumed"

    invoke-direct {p0, v0, p2, p3}, Lcom/my/target/bi;->a(Ljava/lang/String;Ljava/lang/String;Lcom/my/target/ah;)V

    goto :goto_0

    .line 1044
    :cond_a
    const-string v0, "fullscreen"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1046
    const-string v0, "fullscreenOn"

    invoke-direct {p0, v0, p2, p3}, Lcom/my/target/bi;->a(Ljava/lang/String;Ljava/lang/String;Lcom/my/target/ah;)V

    goto/16 :goto_0

    .line 1048
    :cond_b
    const-string v0, "exitFullscreen"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1050
    const-string v0, "fullscreenOff"

    invoke-direct {p0, v0, p2, p3}, Lcom/my/target/bi;->a(Ljava/lang/String;Ljava/lang/String;Lcom/my/target/ah;)V

    goto/16 :goto_0

    .line 1052
    :cond_c
    const-string v0, "skip"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1054
    const-string v0, "closedByUser"

    invoke-direct {p0, v0, p2, p3}, Lcom/my/target/bi;->a(Ljava/lang/String;Ljava/lang/String;Lcom/my/target/ah;)V

    goto/16 :goto_0

    .line 1056
    :cond_d
    const-string v0, "error"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1058
    const-string v0, "error"

    invoke-direct {p0, v0, p2, p3}, Lcom/my/target/bi;->a(Ljava/lang/String;Ljava/lang/String;Lcom/my/target/ah;)V

    goto/16 :goto_0

    .line 1060
    :cond_e
    const-string v0, "ClickTracking"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1062
    const-string v0, "click"

    invoke-direct {p0, v0, p2, p3}, Lcom/my/target/bi;->a(Ljava/lang/String;Ljava/lang/String;Lcom/my/target/ah;)V

    goto/16 :goto_0

    .line 1064
    :cond_f
    const-string v0, "close"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1066
    const-string v0, "closedByUser"

    invoke-direct {p0, v0, p2, p3}, Lcom/my/target/bi;->a(Ljava/lang/String;Ljava/lang/String;Lcom/my/target/ah;)V

    goto/16 :goto_0
.end method

.method private c(Lorg/xmlpull/v1/XmlPullParser;Lcom/my/target/aj;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 743
    :cond_0
    :goto_0
    invoke-static {p1}, Lcom/my/target/bi;->d(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    if-ne v0, v3, :cond_3

    .line 745
    invoke-static {p1}, Lcom/my/target/bi;->e(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 749
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 750
    const-string v1, "ClickThrough"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 752
    if-eqz p2, :cond_0

    .line 758
    invoke-static {p1}, Lcom/my/target/bi;->c(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 759
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 761
    invoke-static {v0}, Lcom/my/target/bi;->E(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setTrackingLink(Ljava/lang/String;)V

    goto :goto_0

    .line 764
    :cond_1
    const-string v1, "ClickTracking"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 766
    invoke-static {p1}, Lcom/my/target/bi;->c(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 767
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 769
    iget-object v1, p0, Lcom/my/target/bi;->bO:Ljava/util/ArrayList;

    const-string v2, "click"

    invoke-static {v2, v0}, Lcom/my/target/aq;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/my/target/aq;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 774
    :cond_2
    invoke-static {p1}, Lcom/my/target/bi;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 777
    :cond_3
    return-void
.end method

.method private static d(Lorg/xmlpull/v1/XmlPullParser;)I
    .locals 1

    .prologue
    .line 120
    :try_start_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 130
    :goto_0
    return v0

    .line 122
    :catch_0
    move-exception v0

    .line 124
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 130
    :goto_1
    const/high16 v0, -0x80000000

    goto :goto_0

    .line 126
    :catch_1
    move-exception v0

    .line 128
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private d(Lorg/xmlpull/v1/XmlPullParser;Lcom/my/target/aj;)V
    .locals 2

    .prologue
    .line 782
    const-string v0, "instreamads"

    iget-object v1, p0, Lcom/my/target/bi;->adConfig:Lcom/my/target/b;

    invoke-virtual {v1}, Lcom/my/target/b;->getFormat()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "fullscreen"

    iget-object v1, p0, Lcom/my/target/bi;->adConfig:Lcom/my/target/b;

    invoke-virtual {v1}, Lcom/my/target/b;->getFormat()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 784
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/my/target/bi;->f(Lorg/xmlpull/v1/XmlPullParser;Lcom/my/target/aj;)V

    .line 790
    :cond_1
    :goto_0
    return-void

    .line 786
    :cond_2
    const-string v0, "instreamaudioads"

    iget-object v1, p0, Lcom/my/target/bi;->adConfig:Lcom/my/target/b;

    invoke-virtual {v1}, Lcom/my/target/b;->getFormat()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 788
    invoke-direct {p0, p1, p2}, Lcom/my/target/bi;->e(Lorg/xmlpull/v1/XmlPullParser;Lcom/my/target/aj;)V

    goto :goto_0
.end method

.method private static e(Lorg/xmlpull/v1/XmlPullParser;)I
    .locals 1

    .prologue
    .line 137
    :try_start_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 143
    :goto_0
    return v0

    .line 139
    :catch_0
    move-exception v0

    .line 141
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 143
    const/high16 v0, -0x80000000

    goto :goto_0
.end method

.method private e(Lorg/xmlpull/v1/XmlPullParser;Lcom/my/target/aj;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlPullParser;",
            "Lcom/my/target/aj",
            "<",
            "Lcom/my/target/common/models/AudioData;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    .line 794
    :cond_0
    :goto_0
    invoke-static {p1}, Lcom/my/target/bi;->d(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    if-ne v0, v6, :cond_5

    .line 796
    invoke-static {p1}, Lcom/my/target/bi;->e(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    if-ne v0, v6, :cond_0

    .line 800
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 801
    const-string v1, "MediaFile"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 803
    const-string v0, "type"

    invoke-static {v0, p1}, Lcom/my/target/bi;->a(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v2

    .line 804
    const-string v0, "bitrate"

    invoke-static {v0, p1}, Lcom/my/target/bi;->a(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 805
    invoke-static {p1}, Lcom/my/target/bi;->c(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/bi;->E(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 807
    const/4 v0, 0x0

    .line 808
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 810
    sget-object v4, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {v2, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, "audio"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 812
    const/4 v0, 0x0

    .line 813
    if-eqz v1, :cond_1

    .line 817
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 824
    :cond_1
    :goto_1
    invoke-static {v3}, Lcom/my/target/common/models/AudioData;->newAudioData(Ljava/lang/String;)Lcom/my/target/common/models/AudioData;

    move-result-object v1

    .line 825
    invoke-virtual {v1, v0}, Lcom/my/target/common/models/AudioData;->setBitrate(I)V

    move-object v0, v1

    .line 829
    :cond_2
    if-nez v0, :cond_3

    .line 831
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Skipping unsupported VAST file (mimetype="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 835
    :cond_3
    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setMediaData(Lcom/my/target/ag;)V

    goto :goto_0

    .line 839
    :cond_4
    invoke-static {p1}, Lcom/my/target/bi;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto/16 :goto_0

    .line 819
    :catch_0
    move-exception v1

    goto :goto_1

    .line 842
    :cond_5
    return-void
.end method

.method private f(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 288
    :cond_0
    :goto_0
    invoke-static {p1}, Lcom/my/target/bi;->d(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 290
    invoke-static {p1}, Lcom/my/target/bi;->e(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 294
    const-string v0, "Ad"

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296
    invoke-direct {p0, p1}, Lcom/my/target/bi;->g(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 299
    :cond_1
    return-void
.end method

.method private f(Lorg/xmlpull/v1/XmlPullParser;Lcom/my/target/aj;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlPullParser;",
            "Lcom/my/target/aj",
            "<",
            "Lcom/my/target/common/models/VideoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 846
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 848
    :cond_0
    :goto_0
    invoke-static {p1}, Lcom/my/target/bi;->d(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    .line 850
    invoke-static {p1}, Lcom/my/target/bi;->e(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 854
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 855
    const-string v1, "MediaFile"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 857
    const-string v0, "type"

    invoke-static {v0, p1}, Lcom/my/target/bi;->a(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v6

    .line 858
    const-string v0, "bitrate"

    invoke-static {v0, p1}, Lcom/my/target/bi;->a(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v7

    .line 859
    const-string v0, "width"

    invoke-static {v0, p1}, Lcom/my/target/bi;->a(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v8

    .line 860
    const-string v0, "height"

    invoke-static {v0, p1}, Lcom/my/target/bi;->a(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v9

    .line 861
    invoke-static {p1}, Lcom/my/target/bi;->c(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/bi;->E(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 863
    const/4 v3, 0x0

    .line 864
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 866
    sget-object v1, Lcom/my/target/bi;->fw:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_8

    aget-object v4, v1, v0

    .line 868
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 870
    const/4 v1, 0x0

    .line 871
    const/4 v0, 0x0

    .line 872
    const/4 v4, 0x0

    .line 875
    if-eqz v8, :cond_1

    :try_start_0
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 876
    :goto_2
    if-eqz v9, :cond_2

    :try_start_1
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    .line 877
    :goto_3
    if-eqz v7, :cond_3

    :try_start_2
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v0

    :goto_4
    move v11, v0

    move v0, v1

    move v1, v11

    .line 882
    :goto_5
    if-lez v2, :cond_8

    if-lez v0, :cond_8

    .line 884
    invoke-static {v10, v2, v0}, Lcom/my/target/common/models/VideoData;->newVideoData(Ljava/lang/String;II)Lcom/my/target/common/models/VideoData;

    move-result-object v0

    .line 885
    invoke-virtual {v0, v1}, Lcom/my/target/common/models/VideoData;->setBitrate(I)V

    .line 892
    :goto_6
    if-nez v0, :cond_5

    .line 894
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Skipping unsupported VAST file (mimeType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 875
    :cond_1
    const/4 v2, 0x0

    goto :goto_2

    .line 876
    :cond_2
    const/4 v1, 0x0

    goto :goto_3

    .line 877
    :cond_3
    const/4 v0, 0x0

    goto :goto_4

    .line 879
    :catch_0
    move-exception v2

    :goto_7
    move v2, v1

    move v1, v4

    goto :goto_5

    .line 866
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 897
    :cond_5
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 901
    :cond_6
    invoke-static {p1}, Lcom/my/target/bi;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto/16 :goto_0

    .line 904
    :cond_7
    iget-object v0, p0, Lcom/my/target/bi;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0}, Lcom/my/target/b;->getVideoQuality()I

    move-result v0

    invoke-static {v5, v0}, Lcom/my/target/common/models/VideoData;->chooseBest(Ljava/util/List;I)Lcom/my/target/common/models/VideoData;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setMediaData(Lcom/my/target/ag;)V

    .line 905
    return-void

    .line 879
    :catch_1
    move-exception v1

    move v1, v2

    goto :goto_7

    :catch_2
    move-exception v0

    move v0, v1

    move v1, v2

    goto :goto_7

    :cond_8
    move-object v0, v3

    goto :goto_6
.end method

.method private g(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 303
    :cond_0
    :goto_0
    invoke-static {p1}, Lcom/my/target/bi;->d(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    if-ne v0, v2, :cond_4

    .line 305
    invoke-static {p1}, Lcom/my/target/bi;->e(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 309
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 311
    const-string v1, "Wrapper"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 313
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/bi;->fA:Z

    .line 314
    const-string v0, "VAST file contains wrapped ad information."

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 315
    iget-object v0, p0, Lcom/my/target/bi;->eo:Lcom/my/target/ae;

    invoke-virtual {v0}, Lcom/my/target/ae;->G()I

    move-result v0

    .line 316
    const/4 v1, 0x5

    if-ge v0, v1, :cond_1

    .line 318
    invoke-direct {p0, p1, v0}, Lcom/my/target/bi;->a(Lorg/xmlpull/v1/XmlPullParser;I)V

    goto :goto_0

    .line 322
    :cond_1
    const-string v0, "got VAST wrapper, but max redirects limit exceeded"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 323
    invoke-static {p1}, Lcom/my/target/bi;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 326
    :cond_2
    const-string v1, "InLine"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 328
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/bi;->fA:Z

    .line 329
    const-string v0, "VAST file contains inline ad information."

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 330
    invoke-direct {p0, p1}, Lcom/my/target/bi;->h(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 334
    :cond_3
    invoke-static {p1}, Lcom/my/target/bi;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 337
    :cond_4
    return-void
.end method

.method private h(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 390
    :cond_0
    :goto_0
    invoke-static {p1}, Lcom/my/target/bi;->d(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    if-ne v0, v2, :cond_4

    .line 392
    invoke-static {p1}, Lcom/my/target/bi;->e(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 396
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 397
    const-string v1, "Impression"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 399
    invoke-direct {p0, p1}, Lcom/my/target/bi;->j(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 401
    :cond_1
    if-eqz v0, :cond_2

    const-string v1, "Creatives"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 403
    invoke-direct {p0, p1}, Lcom/my/target/bi;->k(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 409
    :cond_2
    if-eqz v0, :cond_3

    const-string v1, "Extensions"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 411
    invoke-direct {p0, p1}, Lcom/my/target/bi;->i(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 415
    :cond_3
    invoke-static {p1}, Lcom/my/target/bi;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 418
    :cond_4
    invoke-direct {p0}, Lcom/my/target/bi;->av()V

    .line 419
    return-void
.end method

.method private i(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 423
    :cond_0
    :goto_0
    invoke-static {p1}, Lcom/my/target/bi;->d(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    if-ne v0, v5, :cond_3

    .line 425
    invoke-static {p1}, Lcom/my/target/bi;->e(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    if-ne v0, v5, :cond_0

    .line 429
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 430
    const-string v1, "Extension"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 432
    const-string v0, "type"

    invoke-static {v0, p1}, Lcom/my/target/bi;->a(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 433
    sget-object v2, Lcom/my/target/bi;->fx:[Ljava/lang/String;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 435
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 437
    invoke-direct {p0, p1, v1}, Lcom/my/target/bi;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    .line 433
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 441
    :cond_1
    invoke-static {p1}, Lcom/my/target/bi;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_2

    .line 447
    :cond_2
    invoke-static {p1}, Lcom/my/target/bi;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 450
    :cond_3
    return-void
.end method

.method private j(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 3

    .prologue
    .line 464
    invoke-static {p1}, Lcom/my/target/bi;->c(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 466
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 468
    iget-object v1, p0, Lcom/my/target/bi;->bP:Ljava/util/ArrayList;

    const-string v2, "impression"

    invoke-static {v2, v0}, Lcom/my/target/aq;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/my/target/aq;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 469
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Impression tracker url for wrapper: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 471
    :cond_0
    return-void
.end method

.method private k(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 475
    :cond_0
    :goto_0
    invoke-static {p1}, Lcom/my/target/bi;->d(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    if-ne v0, v3, :cond_5

    .line 477
    invoke-static {p1}, Lcom/my/target/bi;->e(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 481
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 482
    const-string v1, "Creative"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 484
    const-string v0, "id"

    invoke-static {v0, p1}, Lcom/my/target/bi;->a(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 485
    const/4 v1, 0x0

    .line 486
    iget-boolean v2, p0, Lcom/my/target/bi;->fA:Z

    if-nez v2, :cond_6

    .line 488
    invoke-static {}, Lcom/my/target/aj;->newBanner()Lcom/my/target/aj;

    move-result-object v1

    .line 489
    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/my/target/aj;->setId(Ljava/lang/String;)V

    move-object v0, v1

    .line 491
    :goto_2
    invoke-direct {p0, p1, v0}, Lcom/my/target/bi;->a(Lorg/xmlpull/v1/XmlPullParser;Lcom/my/target/aj;)V

    .line 493
    if-eqz v0, :cond_0

    .line 495
    invoke-virtual {v0}, Lcom/my/target/aj;->getDuration()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    .line 497
    invoke-virtual {v0}, Lcom/my/target/aj;->getMediaData()Lcom/my/target/ag;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 499
    iget-object v1, p0, Lcom/my/target/bi;->fz:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 489
    :cond_1
    const-string v0, ""

    goto :goto_1

    .line 503
    :cond_2
    invoke-virtual {v0}, Lcom/my/target/aj;->getId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Required field"

    const-string v2, "VAST has no valid mediaData"

    invoke-direct {p0, v0, v1, v2}, Lcom/my/target/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 508
    :cond_3
    invoke-virtual {v0}, Lcom/my/target/aj;->getId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Required field"

    const-string v2, "VAST has no valid Duration"

    invoke-direct {p0, v0, v1, v2}, Lcom/my/target/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 514
    :cond_4
    invoke-static {p1}, Lcom/my/target/bi;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 517
    :cond_5
    return-void

    :cond_6
    move-object v0, v1

    goto :goto_2
.end method


# virtual methods
.method public F()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 172
    iget-object v0, p0, Lcom/my/target/bi;->bP:Ljava/util/ArrayList;

    return-object v0
.end method

.method public F(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 177
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v1

    .line 180
    :try_start_0
    const-string v0, "http://xmlpull.org/v1/doc/features.html#process-namespaces"

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lorg/xmlpull/v1/XmlPullParser;->setFeature(Ljava/lang/String;Z)V

    .line 181
    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    invoke-direct {p0}, Lcom/my/target/bi;->au()V

    .line 189
    invoke-static {v1}, Lcom/my/target/bi;->e(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    .line 190
    :goto_0
    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    .line 192
    const/high16 v2, -0x80000000

    if-ne v0, v2, :cond_1

    .line 205
    :cond_0
    :goto_1
    return-void

    .line 183
    :catch_0
    move-exception v0

    .line 185
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to parse VAST: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 196
    :cond_1
    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    .line 198
    const-string v0, "VAST"

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 200
    invoke-direct {p0, v1}, Lcom/my/target/bi;->f(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 203
    :cond_2
    invoke-static {v1}, Lcom/my/target/bi;->b(Lorg/xmlpull/v1/XmlPullParser;)I

    move-result v0

    goto :goto_0
.end method

.method H(Ljava/lang/String;)F
    .locals 12

    .prologue
    const-wide/16 v10, 0x3e8

    const-wide/16 v8, 0x3c

    .line 688
    const-wide/16 v2, 0x0

    .line 695
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 697
    const-string v1, "."

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 699
    const-string v0, "."

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 700
    const-string v1, "."

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 701
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 704
    :cond_0
    const-string v1, ":"

    .line 705
    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 706
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 708
    const-string v4, ":"

    .line 709
    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    const-string v5, ":"

    invoke-virtual {p1, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 710
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 712
    const/4 v6, 0x0

    const-string v7, ":"

    invoke-virtual {p1, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {p1, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 713
    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 715
    mul-long/2addr v0, v10

    add-long/2addr v0, v2

    mul-long v2, v4, v8

    mul-long/2addr v2, v10

    add-long/2addr v0, v2

    mul-long v2, v6, v8

    mul-long/2addr v2, v8

    mul-long/2addr v2, v10

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 718
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    .line 719
    return v0
.end method

.method a(Lcom/my/target/aj;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1074
    const/high16 v0, -0x40800000    # -1.0f

    .line 1075
    if-eqz p2, :cond_0

    .line 1077
    const-string v1, "%"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1079
    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1080
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Linear skipoffset is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " [%]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 1081
    invoke-virtual {p1}, Lcom/my/target/aj;->getDuration()F

    move-result v1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    int-to-float v0, v0

    mul-float/2addr v0, v1

    .line 1095
    :cond_0
    :goto_0
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    .line 1097
    invoke-virtual {p1, v0}, Lcom/my/target/aj;->setAllowCloseDelay(F)V

    .line 1099
    :cond_1
    return-void

    .line 1083
    :cond_2
    const-string v1, ":"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1087
    :try_start_0
    invoke-virtual {p0, p2}, Lcom/my/target/bi;->H(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 1089
    :catch_0
    move-exception v1

    .line 1091
    invoke-virtual {p1}, Lcom/my/target/aj;->getId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Bad value"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to convert ISO time skipoffset string "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/my/target/bi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public as()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/aj",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 167
    iget-object v0, p0, Lcom/my/target/bi;->fz:Ljava/util/ArrayList;

    return-object v0
.end method

.method public at()Lcom/my/target/ae;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/my/target/bi;->fB:Lcom/my/target/ae;

    return-object v0
.end method
