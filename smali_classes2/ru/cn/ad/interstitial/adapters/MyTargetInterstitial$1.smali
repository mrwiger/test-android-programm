.class Lru/cn/ad/interstitial/adapters/MyTargetInterstitial$1;
.super Ljava/lang/Object;
.source "MyTargetInterstitial.java"

# interfaces
.implements Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->onLoad()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;


# direct methods
.method constructor <init>(Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;

    .prologue
    .line 28
    iput-object p1, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Lcom/my/target/ads/InterstitialAd;)V
    .locals 3
    .param p1, "interstitialAd"    # Lcom/my/target/ads/InterstitialAd;

    .prologue
    .line 51
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;

    sget-object v1, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_CLICK:Lru/cn/domain/statistics/inetra/AdvEvent;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/util/Map;)V

    .line 52
    return-void
.end method

.method public onDismiss(Lcom/my/target/ads/InterstitialAd;)V
    .locals 1
    .param p1, "interstitialAd"    # Lcom/my/target/ads/InterstitialAd;

    .prologue
    .line 56
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;

    invoke-static {v0}, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->access$400(Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;

    invoke-static {v0}, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->access$500(Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdEnded()V

    .line 58
    :cond_0
    return-void
.end method

.method public onDisplay(Lcom/my/target/ads/InterstitialAd;)V
    .locals 3
    .param p1, "interstitialAd"    # Lcom/my/target/ads/InterstitialAd;

    .prologue
    .line 67
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;

    sget-object v1, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_START:Lru/cn/domain/statistics/inetra/AdvEvent;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/util/Map;)V

    .line 68
    return-void
.end method

.method public onLoad(Lcom/my/target/ads/InterstitialAd;)V
    .locals 1
    .param p1, "interstitialAd"    # Lcom/my/target/ads/InterstitialAd;

    .prologue
    .line 31
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;

    invoke-static {v0}, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->access$000(Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;

    invoke-static {v0}, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->access$100(Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdLoaded()V

    .line 33
    :cond_0
    return-void
.end method

.method public onNoAd(Ljava/lang/String;Lcom/my/target/ads/InterstitialAd;)V
    .locals 1
    .param p1, "reason"    # Ljava/lang/String;
    .param p2, "interstitialAd"    # Lcom/my/target/ads/InterstitialAd;

    .prologue
    .line 37
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;

    invoke-static {v0}, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->access$200(Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;

    invoke-static {v0}, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->access$300(Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onError()V

    .line 47
    :cond_0
    return-void
.end method

.method public onVideoCompleted(Lcom/my/target/ads/InterstitialAd;)V
    .locals 0
    .param p1, "interstitialAd"    # Lcom/my/target/ads/InterstitialAd;

    .prologue
    .line 63
    return-void
.end method
