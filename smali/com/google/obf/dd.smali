.class public Lcom/google/obf/dd;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/dg;


# static fields
.field private static final b:Ljava/util/regex/Pattern;

.field private static final c:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<[B>;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Z

.field private final e:I

.field private final f:I

.field private final g:Ljava/lang/String;

.field private final h:Lcom/google/obf/dx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/dx",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/google/obf/di;

.field private k:Lcom/google/obf/db;

.field private l:Ljava/net/HttpURLConnection;

.field private m:Ljava/io/InputStream;

.field private n:Z

.field private o:J

.field private p:J

.field private q:J

.field private r:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 187
    const-string v0, "^bytes (\\d+)-(\\d+)/(\\d+)$"

    .line 188
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/obf/dd;->b:Ljava/util/regex/Pattern;

    .line 189
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    sput-object v0, Lcom/google/obf/dd;->c:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/obf/dx;Lcom/google/obf/di;IIZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/obf/dx",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/obf/di;",
            "IIZ)V"
        }
    .end annotation

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p1}, Lcom/google/obf/dl;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/dd;->g:Ljava/lang/String;

    .line 3
    iput-object p2, p0, Lcom/google/obf/dd;->h:Lcom/google/obf/dx;

    .line 4
    iput-object p3, p0, Lcom/google/obf/dd;->j:Lcom/google/obf/di;

    .line 5
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/obf/dd;->i:Ljava/util/HashMap;

    .line 6
    iput p4, p0, Lcom/google/obf/dd;->e:I

    .line 7
    iput p5, p0, Lcom/google/obf/dd;->f:I

    .line 8
    iput-boolean p6, p0, Lcom/google/obf/dd;->d:Z

    .line 9
    return-void
.end method

.method private static a(Ljava/net/HttpURLConnection;)J
    .locals 9

    .prologue
    .line 129
    const-wide/16 v0, -0x1

    .line 130
    const-string v2, "Content-Length"

    invoke-virtual {p0, v2}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 131
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 132
    :try_start_0
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 136
    :cond_0
    :goto_0
    const-string v2, "Content-Range"

    invoke-virtual {p0, v2}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 137
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 138
    sget-object v2, Lcom/google/obf/dd;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 139
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 140
    const/4 v3, 0x2

    .line 141
    :try_start_1
    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v2

    sub-long v2, v6, v2

    const-wide/16 v6, 0x1

    add-long/2addr v2, v6

    .line 142
    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-gez v6, :cond_2

    move-wide v0, v2

    .line 150
    :cond_1
    :goto_1
    return-wide v0

    .line 134
    :catch_0
    move-exception v2

    .line 135
    const-string v2, "DefaultHttpDataSource"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1c

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unexpected Content-Length ["

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "]"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 144
    :cond_2
    cmp-long v6, v0, v2

    if-eqz v6, :cond_1

    .line 145
    :try_start_2
    const-string v6, "DefaultHttpDataSource"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1a

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Inconsistent headers ["

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "] ["

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "]"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-wide v0

    goto :goto_1

    .line 148
    :catch_1
    move-exception v2

    .line 149
    const-string v2, "DefaultHttpDataSource"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1b

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unexpected Content-Range ["

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method private a(Ljava/net/URL;[BJJZZ)Ljava/net/HttpURLConnection;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 93
    iget v1, p0, Lcom/google/obf/dd;->e:I

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 94
    iget v1, p0, Lcom/google/obf/dd;->f:I

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 95
    iget-object v3, p0, Lcom/google/obf/dd;->i:Ljava/util/HashMap;

    monitor-enter v3

    .line 96
    :try_start_0
    iget-object v1, p0, Lcom/google/obf/dd;->i:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 97
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 100
    const-wide/16 v2, 0x0

    cmp-long v1, p3, v2

    if-nez v1, :cond_1

    const-wide/16 v2, -0x1

    cmp-long v1, p5, v2

    if-eqz v1, :cond_3

    .line 101
    :cond_1
    const/16 v1, 0x1b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "bytes="

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 102
    const-wide/16 v2, -0x1

    cmp-long v2, p5, v2

    if-eqz v2, :cond_2

    .line 103
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    add-long v2, p3, p5

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x14

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 104
    :cond_2
    const-string v2, "Range"

    invoke-virtual {v0, v2, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :cond_3
    const-string v1, "User-Agent"

    iget-object v2, p0, Lcom/google/obf/dd;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    if-nez p7, :cond_4

    .line 107
    const-string v1, "Accept-Encoding"

    const-string v2, "identity"

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    :cond_4
    invoke-virtual {v0, p8}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 109
    if-eqz p2, :cond_5

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 110
    if-eqz p2, :cond_7

    .line 111
    const-string v1, "POST"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 112
    array-length v1, p2

    if-nez v1, :cond_6

    .line 113
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V

    .line 121
    :goto_2
    return-object v0

    .line 109
    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    .line 114
    :cond_6
    array-length v1, p2

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 115
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V

    .line 116
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    .line 117
    invoke-virtual {v1, p2}, Ljava/io/OutputStream;->write([B)V

    .line 118
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    goto :goto_2

    .line 120
    :cond_7
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V

    goto :goto_2
.end method

.method private static a(Ljava/net/URL;Ljava/lang/String;)Ljava/net/URL;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    if-nez p1, :cond_0

    .line 123
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Null location redirect"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 124
    :cond_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p0, p1}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V

    .line 125
    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    .line 126
    const-string v2, "https"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "http"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 127
    new-instance v2, Ljava/net/ProtocolException;

    const-string v3, "Unsupported protocol redirect: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v2, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 128
    :cond_2
    return-object v0
.end method

.method private b([BII)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 169
    if-nez p3, :cond_1

    .line 170
    const/4 v0, 0x0

    .line 179
    :cond_0
    :goto_0
    return v0

    .line 171
    :cond_1
    iget-object v1, p0, Lcom/google/obf/dd;->m:Ljava/io/InputStream;

    invoke-virtual {v1, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 172
    if-ne v1, v0, :cond_2

    .line 173
    iget-wide v2, p0, Lcom/google/obf/dd;->p:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/google/obf/dd;->p:J

    iget-wide v4, p0, Lcom/google/obf/dd;->r:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 174
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 176
    :cond_2
    iget-wide v2, p0, Lcom/google/obf/dd;->r:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/obf/dd;->r:J

    .line 177
    iget-object v0, p0, Lcom/google/obf/dd;->j:Lcom/google/obf/di;

    if-eqz v0, :cond_3

    .line 178
    iget-object v0, p0, Lcom/google/obf/dd;->j:Lcom/google/obf/di;

    invoke-interface {v0, v1}, Lcom/google/obf/di;->a(I)V

    :cond_3
    move v0, v1

    .line 179
    goto :goto_0
.end method

.method private b(Lcom/google/obf/db;)Ljava/net/HttpURLConnection;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v0, 0x0

    .line 71
    new-instance v2, Ljava/net/URL;

    iget-object v1, p1, Lcom/google/obf/db;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 72
    iget-object v3, p1, Lcom/google/obf/db;->b:[B

    .line 73
    iget-wide v4, p1, Lcom/google/obf/db;->d:J

    .line 74
    iget-wide v6, p1, Lcom/google/obf/db;->e:J

    .line 75
    iget v1, p1, Lcom/google/obf/db;->g:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    move v8, v9

    .line 76
    :goto_0
    iget-boolean v1, p0, Lcom/google/obf/dd;->d:Z

    if-nez v1, :cond_1

    move-object v1, p0

    .line 77
    invoke-direct/range {v1 .. v9}, Lcom/google/obf/dd;->a(Ljava/net/URL;[BJJZZ)Ljava/net/HttpURLConnection;

    move-result-object v0

    .line 89
    :goto_1
    return-object v0

    :cond_0
    move v8, v0

    .line 75
    goto :goto_0

    :cond_1
    move v1, v0

    .line 80
    :goto_2
    add-int/lit8 v10, v1, 0x1

    const/16 v9, 0x14

    if-gt v1, v9, :cond_4

    move-object v1, p0

    move v9, v0

    .line 81
    invoke-direct/range {v1 .. v9}, Lcom/google/obf/dd;->a(Ljava/net/URL;[BJJZZ)Ljava/net/HttpURLConnection;

    move-result-object v1

    .line 82
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v9

    .line 83
    const/16 v11, 0x12c

    if-eq v9, v11, :cond_2

    const/16 v11, 0x12d

    if-eq v9, v11, :cond_2

    const/16 v11, 0x12e

    if-eq v9, v11, :cond_2

    const/16 v11, 0x12f

    if-eq v9, v11, :cond_2

    if-nez v3, :cond_3

    const/16 v3, 0x133

    if-eq v9, v3, :cond_2

    const/16 v3, 0x134

    if-ne v9, v3, :cond_3

    .line 84
    :cond_2
    const/4 v3, 0x0

    .line 85
    const-string v9, "Location"

    invoke-virtual {v1, v9}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 86
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 87
    invoke-static {v2, v9}, Lcom/google/obf/dd;->a(Ljava/net/URL;Ljava/lang/String;)Ljava/net/URL;

    move-result-object v2

    move v1, v10

    .line 90
    goto :goto_2

    :cond_3
    move-object v0, v1

    .line 89
    goto :goto_1

    .line 91
    :cond_4
    new-instance v0, Ljava/net/NoRouteToHostException;

    const/16 v1, 0x1f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Too many redirects: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/NoRouteToHostException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private c()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 151
    iget-wide v0, p0, Lcom/google/obf/dd;->q:J

    iget-wide v2, p0, Lcom/google/obf/dd;->o:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 168
    :goto_0
    return-void

    .line 153
    :cond_0
    sget-object v0, Lcom/google/obf/dd;->c:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 154
    if-nez v0, :cond_1

    .line 155
    const/16 v0, 0x1000

    new-array v0, v0, [B

    .line 156
    :cond_1
    :goto_1
    iget-wide v2, p0, Lcom/google/obf/dd;->q:J

    iget-wide v4, p0, Lcom/google/obf/dd;->o:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_4

    .line 157
    iget-wide v2, p0, Lcom/google/obf/dd;->o:J

    iget-wide v4, p0, Lcom/google/obf/dd;->q:J

    sub-long/2addr v2, v4

    array-length v1, v0

    int-to-long v4, v1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v1, v2

    .line 158
    iget-object v2, p0, Lcom/google/obf/dd;->m:Ljava/io/InputStream;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 159
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 160
    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0

    .line 161
    :cond_2
    const/4 v2, -0x1

    if-ne v1, v2, :cond_3

    .line 162
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 163
    :cond_3
    iget-wide v2, p0, Lcom/google/obf/dd;->q:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/obf/dd;->q:J

    .line 164
    iget-object v2, p0, Lcom/google/obf/dd;->j:Lcom/google/obf/di;

    if-eqz v2, :cond_1

    .line 165
    iget-object v2, p0, Lcom/google/obf/dd;->j:Lcom/google/obf/di;

    invoke-interface {v2, v1}, Lcom/google/obf/di;->a(I)V

    goto :goto_1

    .line 167
    :cond_4
    sget-object v1, Lcom/google/obf/dd;->c:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/obf/dd;->l:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    .line 181
    :try_start_0
    iget-object v0, p0, Lcom/google/obf/dd;->l:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 185
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/dd;->l:Ljava/net/HttpURLConnection;

    .line 186
    :cond_0
    return-void

    .line 183
    :catch_0
    move-exception v0

    .line 184
    const-string v1, "DefaultHttpDataSource"

    const-string v2, "Unexpected error while disconnecting"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public a([BII)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/dg$a;
        }
    .end annotation

    .prologue
    .line 47
    :try_start_0
    invoke-direct {p0}, Lcom/google/obf/dd;->c()V

    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/google/obf/dd;->b([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 49
    :catch_0
    move-exception v0

    .line 50
    new-instance v1, Lcom/google/obf/dg$a;

    iget-object v2, p0, Lcom/google/obf/dd;->k:Lcom/google/obf/db;

    const/4 v3, 0x2

    invoke-direct {v1, v0, v2, v3}, Lcom/google/obf/dg$a;-><init>(Ljava/io/IOException;Lcom/google/obf/db;I)V

    throw v1
.end method

.method public a(Lcom/google/obf/db;)J
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/dg$a;
        }
    .end annotation

    .prologue
    const/16 v8, 0xc8

    const-wide/16 v2, -0x1

    const-wide/16 v0, 0x0

    const/4 v7, 0x1

    .line 10
    iput-object p1, p0, Lcom/google/obf/dd;->k:Lcom/google/obf/db;

    .line 11
    iput-wide v0, p0, Lcom/google/obf/dd;->r:J

    .line 12
    iput-wide v0, p0, Lcom/google/obf/dd;->q:J

    .line 13
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/obf/dd;->b(Lcom/google/obf/db;)Ljava/net/HttpURLConnection;

    move-result-object v4

    iput-object v4, p0, Lcom/google/obf/dd;->l:Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 17
    :try_start_1
    iget-object v4, p0, Lcom/google/obf/dd;->l:Ljava/net/HttpURLConnection;

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v4

    .line 22
    if-lt v4, v8, :cond_0

    const/16 v5, 0x12b

    if-le v4, v5, :cond_3

    .line 23
    :cond_0
    iget-object v0, p0, Lcom/google/obf/dd;->l:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v0

    .line 24
    invoke-direct {p0}, Lcom/google/obf/dd;->d()V

    .line 25
    new-instance v1, Lcom/google/obf/dg$c;

    invoke-direct {v1, v4, v0, p1}, Lcom/google/obf/dg$c;-><init>(ILjava/util/Map;Lcom/google/obf/db;)V

    throw v1

    .line 15
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 16
    new-instance v2, Lcom/google/obf/dg$a;

    const-string v3, "Unable to connect to "

    iget-object v0, p1, Lcom/google/obf/db;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v2, v0, v1, p1, v7}, Lcom/google/obf/dg$a;-><init>(Ljava/lang/String;Ljava/io/IOException;Lcom/google/obf/db;I)V

    throw v2

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 19
    :catch_1
    move-exception v0

    move-object v1, v0

    .line 20
    invoke-direct {p0}, Lcom/google/obf/dd;->d()V

    .line 21
    new-instance v2, Lcom/google/obf/dg$a;

    const-string v3, "Unable to connect to "

    iget-object v0, p1, Lcom/google/obf/db;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v2, v0, v1, p1, v7}, Lcom/google/obf/dg$a;-><init>(Ljava/lang/String;Ljava/io/IOException;Lcom/google/obf/db;I)V

    throw v2

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 26
    :cond_3
    iget-object v5, p0, Lcom/google/obf/dd;->l:Ljava/net/HttpURLConnection;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v5

    .line 27
    iget-object v6, p0, Lcom/google/obf/dd;->h:Lcom/google/obf/dx;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/google/obf/dd;->h:Lcom/google/obf/dx;

    invoke-interface {v6, v5}, Lcom/google/obf/dx;->a(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 28
    invoke-direct {p0}, Lcom/google/obf/dd;->d()V

    .line 29
    new-instance v0, Lcom/google/obf/dg$b;

    invoke-direct {v0, v5, p1}, Lcom/google/obf/dg$b;-><init>(Ljava/lang/String;Lcom/google/obf/db;)V

    throw v0

    .line 30
    :cond_4
    if-ne v4, v8, :cond_5

    iget-wide v4, p1, Lcom/google/obf/db;->d:J

    cmp-long v4, v4, v0

    if-eqz v4, :cond_5

    iget-wide v0, p1, Lcom/google/obf/db;->d:J

    :cond_5
    iput-wide v0, p0, Lcom/google/obf/dd;->o:J

    .line 31
    iget v0, p1, Lcom/google/obf/db;->g:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_9

    .line 32
    iget-object v0, p0, Lcom/google/obf/dd;->l:Ljava/net/HttpURLConnection;

    invoke-static {v0}, Lcom/google/obf/dd;->a(Ljava/net/HttpURLConnection;)J

    move-result-wide v0

    .line 33
    iget-wide v4, p1, Lcom/google/obf/db;->e:J

    cmp-long v4, v4, v2

    if-eqz v4, :cond_7

    iget-wide v0, p1, Lcom/google/obf/db;->e:J

    .line 35
    :goto_2
    iput-wide v0, p0, Lcom/google/obf/dd;->p:J

    .line 38
    :goto_3
    :try_start_2
    iget-object v0, p0, Lcom/google/obf/dd;->l:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/dd;->m:Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 43
    iput-boolean v7, p0, Lcom/google/obf/dd;->n:Z

    .line 44
    iget-object v0, p0, Lcom/google/obf/dd;->j:Lcom/google/obf/di;

    if-eqz v0, :cond_6

    .line 45
    iget-object v0, p0, Lcom/google/obf/dd;->j:Lcom/google/obf/di;

    invoke-interface {v0}, Lcom/google/obf/di;->a()V

    .line 46
    :cond_6
    iget-wide v0, p0, Lcom/google/obf/dd;->p:J

    return-wide v0

    .line 34
    :cond_7
    cmp-long v4, v0, v2

    if-eqz v4, :cond_8

    iget-wide v2, p0, Lcom/google/obf/dd;->o:J

    sub-long/2addr v0, v2

    goto :goto_2

    :cond_8
    move-wide v0, v2

    .line 35
    goto :goto_2

    .line 37
    :cond_9
    iget-wide v0, p1, Lcom/google/obf/db;->e:J

    iput-wide v0, p0, Lcom/google/obf/dd;->p:J

    goto :goto_3

    .line 40
    :catch_2
    move-exception v0

    .line 41
    invoke-direct {p0}, Lcom/google/obf/dd;->d()V

    .line 42
    new-instance v1, Lcom/google/obf/dg$a;

    invoke-direct {v1, v0, p1, v7}, Lcom/google/obf/dg$a;-><init>(Ljava/io/IOException;Lcom/google/obf/db;I)V

    throw v1
.end method

.method public a()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/dg$a;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 51
    :try_start_0
    iget-object v0, p0, Lcom/google/obf/dd;->m:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/google/obf/dd;->l:Ljava/net/HttpURLConnection;

    invoke-virtual {p0}, Lcom/google/obf/dd;->b()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/google/obf/ea;->a(Ljava/net/HttpURLConnection;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    :try_start_1
    iget-object v0, p0, Lcom/google/obf/dd;->m:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 57
    :cond_0
    iput-object v5, p0, Lcom/google/obf/dd;->m:Ljava/io/InputStream;

    .line 58
    invoke-direct {p0}, Lcom/google/obf/dd;->d()V

    .line 59
    iget-boolean v0, p0, Lcom/google/obf/dd;->n:Z

    if-eqz v0, :cond_1

    .line 60
    iput-boolean v4, p0, Lcom/google/obf/dd;->n:Z

    .line 61
    iget-object v0, p0, Lcom/google/obf/dd;->j:Lcom/google/obf/di;

    if-eqz v0, :cond_1

    .line 62
    iget-object v0, p0, Lcom/google/obf/dd;->j:Lcom/google/obf/di;

    invoke-interface {v0}, Lcom/google/obf/di;->b()V

    .line 69
    :cond_1
    return-void

    .line 55
    :catch_0
    move-exception v0

    .line 56
    :try_start_2
    new-instance v1, Lcom/google/obf/dg$a;

    iget-object v2, p0, Lcom/google/obf/dd;->k:Lcom/google/obf/db;

    const/4 v3, 0x3

    invoke-direct {v1, v0, v2, v3}, Lcom/google/obf/dg$a;-><init>(Ljava/io/IOException;Lcom/google/obf/db;I)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 63
    :catchall_0
    move-exception v0

    iput-object v5, p0, Lcom/google/obf/dd;->m:Ljava/io/InputStream;

    .line 64
    invoke-direct {p0}, Lcom/google/obf/dd;->d()V

    .line 65
    iget-boolean v1, p0, Lcom/google/obf/dd;->n:Z

    if-eqz v1, :cond_2

    .line 66
    iput-boolean v4, p0, Lcom/google/obf/dd;->n:Z

    .line 67
    iget-object v1, p0, Lcom/google/obf/dd;->j:Lcom/google/obf/di;

    if-eqz v1, :cond_2

    .line 68
    iget-object v1, p0, Lcom/google/obf/dd;->j:Lcom/google/obf/di;

    invoke-interface {v1}, Lcom/google/obf/di;->b()V

    :cond_2
    throw v0
.end method

.method protected final b()J
    .locals 4

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/google/obf/dd;->p:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/obf/dd;->p:J

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/google/obf/dd;->p:J

    iget-wide v2, p0, Lcom/google/obf/dd;->r:J

    sub-long/2addr v0, v2

    goto :goto_0
.end method
