.class public final Ltoothpick/Toothpick;
.super Ljava/lang/Object;
.source "Toothpick.java"


# static fields
.field private static final MAP_KEY_TO_SCOPE:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Object;",
            "Ltoothpick/Scope;",
            ">;"
        }
    .end annotation
.end field

.field private static injector:Ltoothpick/Injector;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Ltoothpick/Toothpick;->MAP_KEY_TO_SCOPE:Ljava/util/concurrent/ConcurrentHashMap;

    .line 25
    new-instance v0, Ltoothpick/InjectorImpl;

    invoke-direct {v0}, Ltoothpick/InjectorImpl;-><init>()V

    sput-object v0, Ltoothpick/Toothpick;->injector:Ltoothpick/Injector;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Constructor can\'t be invoked even via reflection."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static closeScope(Ljava/lang/Object;)V
    .locals 3
    .param p0, "name"    # Ljava/lang/Object;

    .prologue
    .line 108
    sget-object v2, Ltoothpick/Toothpick;->MAP_KEY_TO_SCOPE:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltoothpick/ScopeNode;

    .line 109
    .local v1, "scope":Ltoothpick/ScopeNode;
    if-eqz v1, :cond_0

    .line 110
    invoke-virtual {v1}, Ltoothpick/ScopeNode;->getParentScope()Ltoothpick/ScopeNode;

    move-result-object v0

    .line 111
    .local v0, "parentScope":Ltoothpick/ScopeNode;
    if-eqz v0, :cond_1

    .line 112
    invoke-virtual {v0, v1}, Ltoothpick/ScopeNode;->removeChild(Ltoothpick/ScopeNode;)V

    .line 116
    :goto_0
    invoke-static {v1}, Ltoothpick/Toothpick;->removeScopeAndChildrenFromMap(Ltoothpick/ScopeNode;)V

    .line 118
    .end local v0    # "parentScope":Ltoothpick/ScopeNode;
    :cond_0
    return-void

    .line 114
    .restart local v0    # "parentScope":Ltoothpick/ScopeNode;
    :cond_1
    sget-object v2, Ltoothpick/configuration/ConfigurationHolder;->configuration:Ltoothpick/configuration/Configuration;

    invoke-virtual {v2}, Ltoothpick/configuration/Configuration;->onScopeForestReset()V

    goto :goto_0
.end method

.method static getScopeNamesSize()I
    .locals 1

    .prologue
    .line 179
    sget-object v0, Ltoothpick/Toothpick;->MAP_KEY_TO_SCOPE:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    return v0
.end method

.method public static inject(Ljava/lang/Object;Ltoothpick/Scope;)V
    .locals 1
    .param p0, "obj"    # Ljava/lang/Object;
    .param p1, "scope"    # Ltoothpick/Scope;

    .prologue
    .line 149
    sget-object v0, Ltoothpick/Toothpick;->injector:Ltoothpick/Injector;

    invoke-interface {v0, p0, p1}, Ltoothpick/Injector;->inject(Ljava/lang/Object;Ltoothpick/Scope;)V

    .line 150
    return-void
.end method

.method public static openScope(Ljava/lang/Object;)Ltoothpick/Scope;
    .locals 1
    .param p0, "name"    # Ljava/lang/Object;

    .prologue
    .line 67
    const/4 v0, 0x1

    invoke-static {p0, v0}, Ltoothpick/Toothpick;->openScope(Ljava/lang/Object;Z)Ltoothpick/Scope;

    move-result-object v0

    return-object v0
.end method

.method private static openScope(Ljava/lang/Object;Z)Ltoothpick/Scope;
    .locals 5
    .param p0, "name"    # Ljava/lang/Object;
    .param p1, "shouldCheckMultipleRootScopes"    # Z

    .prologue
    .line 80
    if-nez p0, :cond_0

    .line 81
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "null scope names are not allowed."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 84
    :cond_0
    sget-object v3, Ltoothpick/Toothpick;->MAP_KEY_TO_SCOPE:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, p0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltoothpick/Scope;

    .line 85
    .local v1, "scope":Ltoothpick/Scope;
    if-eqz v1, :cond_1

    move-object v2, v1

    .line 96
    .end local v1    # "scope":Ltoothpick/Scope;
    .local v2, "scope":Ltoothpick/Scope;
    :goto_0
    return-object v2

    .line 88
    .end local v2    # "scope":Ltoothpick/Scope;
    .restart local v1    # "scope":Ltoothpick/Scope;
    :cond_1
    new-instance v1, Ltoothpick/ScopeImpl;

    .end local v1    # "scope":Ltoothpick/Scope;
    invoke-direct {v1, p0}, Ltoothpick/ScopeImpl;-><init>(Ljava/lang/Object;)V

    .line 89
    .restart local v1    # "scope":Ltoothpick/Scope;
    sget-object v3, Ltoothpick/Toothpick;->MAP_KEY_TO_SCOPE:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, p0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltoothpick/Scope;

    .line 90
    .local v0, "previous":Ltoothpick/Scope;
    if-eqz v0, :cond_3

    .line 92
    move-object v1, v0

    :cond_2
    :goto_1
    move-object v2, v1

    .line 96
    .end local v1    # "scope":Ltoothpick/Scope;
    .restart local v2    # "scope":Ltoothpick/Scope;
    goto :goto_0

    .line 93
    .end local v2    # "scope":Ltoothpick/Scope;
    .restart local v1    # "scope":Ltoothpick/Scope;
    :cond_3
    if-eqz p1, :cond_2

    .line 94
    sget-object v3, Ltoothpick/configuration/ConfigurationHolder;->configuration:Ltoothpick/configuration/Configuration;

    invoke-virtual {v3, v1}, Ltoothpick/configuration/Configuration;->checkMultipleRootScopes(Ltoothpick/Scope;)V

    goto :goto_1
.end method

.method public static varargs openScopes([Ljava/lang/Object;)Ltoothpick/Scope;
    .locals 6
    .param p0, "names"    # [Ljava/lang/Object;

    .prologue
    const/4 v5, 0x0

    .line 42
    if-nez p0, :cond_0

    .line 43
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "null scope names are not allowed."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 46
    :cond_0
    array-length v3, p0

    if-nez v3, :cond_1

    .line 47
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Minimally, one scope name is required."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 50
    :cond_1
    const/4 v1, 0x0

    .line 51
    .local v1, "lastScope":Ltoothpick/ScopeNode;
    aget-object v3, p0, v5

    const/4 v4, 0x1

    invoke-static {v3, v4}, Ltoothpick/Toothpick;->openScope(Ljava/lang/Object;Z)Ltoothpick/Scope;

    move-result-object v2

    check-cast v2, Ltoothpick/ScopeNode;

    .line 52
    .local v2, "previousScope":Ltoothpick/ScopeNode;
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    array-length v3, p0

    if-ge v0, v3, :cond_2

    .line 53
    aget-object v3, p0, v0

    invoke-static {v3, v5}, Ltoothpick/Toothpick;->openScope(Ljava/lang/Object;Z)Ltoothpick/Scope;

    move-result-object v1

    .end local v1    # "lastScope":Ltoothpick/ScopeNode;
    check-cast v1, Ltoothpick/ScopeNode;

    .line 54
    .restart local v1    # "lastScope":Ltoothpick/ScopeNode;
    invoke-virtual {v2, v1}, Ltoothpick/ScopeNode;->addChild(Ltoothpick/ScopeNode;)Ltoothpick/ScopeNode;

    move-result-object v1

    .line 55
    move-object v2, v1

    .line 52
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 58
    :cond_2
    return-object v2
.end method

.method private static removeScopeAndChildrenFromMap(Ltoothpick/ScopeNode;)V
    .locals 3
    .param p0, "scope"    # Ltoothpick/ScopeNode;

    .prologue
    .line 161
    sget-object v1, Ltoothpick/Toothpick;->MAP_KEY_TO_SCOPE:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p0}, Ltoothpick/ScopeNode;->getName()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    invoke-virtual {p0}, Ltoothpick/ScopeNode;->close()V

    .line 163
    iget-object v1, p0, Ltoothpick/ScopeNode;->childrenScopes:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltoothpick/ScopeNode;

    .line 164
    .local v0, "childScope":Ltoothpick/ScopeNode;
    invoke-static {v0}, Ltoothpick/Toothpick;->removeScopeAndChildrenFromMap(Ltoothpick/ScopeNode;)V

    goto :goto_0

    .line 166
    .end local v0    # "childScope":Ltoothpick/ScopeNode;
    :cond_0
    return-void
.end method

.method public static reset()V
    .locals 3

    .prologue
    .line 124
    sget-object v1, Ltoothpick/Toothpick;->MAP_KEY_TO_SCOPE:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->keys()Ljava/util/Enumeration;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v0, "name":Ljava/lang/Object;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 125
    invoke-static {v0}, Ltoothpick/Toothpick;->closeScope(Ljava/lang/Object;)V

    goto :goto_0

    .line 127
    :cond_0
    sget-object v1, Ltoothpick/configuration/ConfigurationHolder;->configuration:Ltoothpick/configuration/Configuration;

    invoke-virtual {v1}, Ltoothpick/configuration/Configuration;->onScopeForestReset()V

    .line 128
    invoke-static {}, Ltoothpick/ScopeImpl;->resetUnBoundProviders()V

    .line 129
    return-void
.end method

.method public static reset(Ltoothpick/Scope;)V
    .locals 1
    .param p0, "scope"    # Ltoothpick/Scope;

    .prologue
    .line 137
    move-object v0, p0

    check-cast v0, Ltoothpick/ScopeNode;

    .line 138
    .local v0, "scopeNode":Ltoothpick/ScopeNode;
    invoke-virtual {v0}, Ltoothpick/ScopeNode;->reset()V

    .line 139
    return-void
.end method

.method public static setConfiguration(Ltoothpick/configuration/Configuration;)V
    .locals 0
    .param p0, "configuration"    # Ltoothpick/configuration/Configuration;

    .prologue
    .line 174
    sput-object p0, Ltoothpick/configuration/ConfigurationHolder;->configuration:Ltoothpick/configuration/Configuration;

    .line 175
    return-void
.end method
