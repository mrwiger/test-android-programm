.class final Lcom/google/obf/ci$a;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/ci;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/google/obf/cb;

.field private final b:Lcom/google/obf/cj;

.field private final c:Lcom/google/obf/dv;

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:I

.field private h:J


# direct methods
.method public constructor <init>(Lcom/google/obf/cb;Lcom/google/obf/cj;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/obf/ci$a;->a:Lcom/google/obf/cb;

    .line 3
    iput-object p2, p0, Lcom/google/obf/ci$a;->b:Lcom/google/obf/cj;

    .line 4
    new-instance v0, Lcom/google/obf/dv;

    const/16 v1, 0x40

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Lcom/google/obf/dv;-><init>([B)V

    iput-object v0, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    .line 5
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 19
    iget-object v0, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v0, v2}, Lcom/google/obf/dv;->b(I)V

    .line 20
    iget-object v0, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v0}, Lcom/google/obf/dv;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/obf/ci$a;->d:Z

    .line 21
    iget-object v0, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v0}, Lcom/google/obf/dv;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/obf/ci$a;->e:Z

    .line 22
    iget-object v0, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/obf/dv;->b(I)V

    .line 23
    iget-object v0, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v0, v2}, Lcom/google/obf/dv;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/obf/ci$a;->g:I

    .line 24
    return-void
.end method

.method private c()V
    .locals 9

    .prologue
    const/16 v8, 0x1e

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/16 v7, 0xf

    const/4 v6, 0x1

    .line 25
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/obf/ci$a;->h:J

    .line 26
    iget-boolean v0, p0, Lcom/google/obf/ci$a;->d:Z

    if-eqz v0, :cond_1

    .line 27
    iget-object v0, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v0, v5}, Lcom/google/obf/dv;->b(I)V

    .line 28
    iget-object v0, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v0, v4}, Lcom/google/obf/dv;->c(I)I

    move-result v0

    int-to-long v0, v0

    shl-long/2addr v0, v8

    .line 29
    iget-object v2, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v2, v6}, Lcom/google/obf/dv;->b(I)V

    .line 30
    iget-object v2, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v2, v7}, Lcom/google/obf/dv;->c(I)I

    move-result v2

    shl-int/lit8 v2, v2, 0xf

    int-to-long v2, v2

    or-long/2addr v0, v2

    .line 31
    iget-object v2, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v2, v6}, Lcom/google/obf/dv;->b(I)V

    .line 32
    iget-object v2, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v2, v7}, Lcom/google/obf/dv;->c(I)I

    move-result v2

    int-to-long v2, v2

    or-long/2addr v0, v2

    .line 33
    iget-object v2, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v2, v6}, Lcom/google/obf/dv;->b(I)V

    .line 34
    iget-boolean v2, p0, Lcom/google/obf/ci$a;->f:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/obf/ci$a;->e:Z

    if-eqz v2, :cond_0

    .line 35
    iget-object v2, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v2, v5}, Lcom/google/obf/dv;->b(I)V

    .line 36
    iget-object v2, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v2, v4}, Lcom/google/obf/dv;->c(I)I

    move-result v2

    int-to-long v2, v2

    shl-long/2addr v2, v8

    .line 37
    iget-object v4, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v4, v6}, Lcom/google/obf/dv;->b(I)V

    .line 38
    iget-object v4, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v4, v7}, Lcom/google/obf/dv;->c(I)I

    move-result v4

    shl-int/lit8 v4, v4, 0xf

    int-to-long v4, v4

    or-long/2addr v2, v4

    .line 39
    iget-object v4, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v4, v6}, Lcom/google/obf/dv;->b(I)V

    .line 40
    iget-object v4, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v4, v7}, Lcom/google/obf/dv;->c(I)I

    move-result v4

    int-to-long v4, v4

    or-long/2addr v2, v4

    .line 41
    iget-object v4, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v4, v6}, Lcom/google/obf/dv;->b(I)V

    .line 42
    iget-object v4, p0, Lcom/google/obf/ci$a;->b:Lcom/google/obf/cj;

    invoke-virtual {v4, v2, v3}, Lcom/google/obf/cj;->a(J)J

    .line 43
    iput-boolean v6, p0, Lcom/google/obf/ci$a;->f:Z

    .line 44
    :cond_0
    iget-object v2, p0, Lcom/google/obf/ci$a;->b:Lcom/google/obf/cj;

    invoke-virtual {v2, v0, v1}, Lcom/google/obf/cj;->a(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/obf/ci$a;->h:J

    .line 45
    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/ci$a;->f:Z

    .line 7
    iget-object v0, p0, Lcom/google/obf/ci$a;->a:Lcom/google/obf/cb;

    invoke-virtual {v0}, Lcom/google/obf/cb;->a()V

    .line 8
    return-void
.end method

.method public a(Lcom/google/obf/dw;Lcom/google/obf/al;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 9
    iget-object v0, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    iget-object v0, v0, Lcom/google/obf/dv;->a:[B

    const/4 v1, 0x3

    invoke-virtual {p1, v0, v2, v1}, Lcom/google/obf/dw;->a([BII)V

    .line 10
    iget-object v0, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v0, v2}, Lcom/google/obf/dv;->a(I)V

    .line 11
    invoke-direct {p0}, Lcom/google/obf/ci$a;->b()V

    .line 12
    iget-object v0, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    iget-object v0, v0, Lcom/google/obf/dv;->a:[B

    iget v1, p0, Lcom/google/obf/ci$a;->g:I

    invoke-virtual {p1, v0, v2, v1}, Lcom/google/obf/dw;->a([BII)V

    .line 13
    iget-object v0, p0, Lcom/google/obf/ci$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v0, v2}, Lcom/google/obf/dv;->a(I)V

    .line 14
    invoke-direct {p0}, Lcom/google/obf/ci$a;->c()V

    .line 15
    iget-object v0, p0, Lcom/google/obf/ci$a;->a:Lcom/google/obf/cb;

    iget-wide v2, p0, Lcom/google/obf/ci$a;->h:J

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/obf/cb;->a(JZ)V

    .line 16
    iget-object v0, p0, Lcom/google/obf/ci$a;->a:Lcom/google/obf/cb;

    invoke-virtual {v0, p1}, Lcom/google/obf/cb;->a(Lcom/google/obf/dw;)V

    .line 17
    iget-object v0, p0, Lcom/google/obf/ci$a;->a:Lcom/google/obf/cb;

    invoke-virtual {v0}, Lcom/google/obf/cb;->b()V

    .line 18
    return-void
.end method
