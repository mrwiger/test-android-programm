.class public final Lcom/my/target/common/models/VideoData;
.super Lcom/my/target/ag;
.source "VideoData.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/my/target/ag",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final M3U8:Ljava/lang/String; = ".m3u8"


# instance fields
.field private bitrate:I

.field private final cacheable:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;II)V
    .locals 2
    .param p1, "videoUrl"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/my/target/ag;-><init>(Ljava/lang/String;)V

    .line 55
    iput p2, p0, Lcom/my/target/common/models/VideoData;->width:I

    .line 56
    iput p3, p0, Lcom/my/target/common/models/VideoData;->height:I

    .line 57
    iget-object v0, p0, Lcom/my/target/common/models/VideoData;->url:Ljava/lang/String;

    const-string v1, ".m3u8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/my/target/common/models/VideoData;->cacheable:Z

    .line 58
    return-void

    .line 57
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static chooseBest(Ljava/util/List;I)Lcom/my/target/common/models/VideoData;
    .locals 5
    .param p1, "videoQuality"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/common/models/VideoData;",
            ">;I)",
            "Lcom/my/target/common/models/VideoData;"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, "videoDatas":Ljava/util/List;, "Ljava/util/List<Lcom/my/target/common/models/VideoData;>;"
    const/4 v3, 0x0

    .line 25
    const/4 v1, 0x0

    .line 27
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/common/models/VideoData;

    .line 29
    invoke-virtual {v0}, Lcom/my/target/common/models/VideoData;->getHeight()I

    move-result v2

    .line 30
    if-eqz v3, :cond_2

    if-gt v2, p1, :cond_0

    if-gt v1, p1, :cond_2

    :cond_0
    if-gt v2, p1, :cond_1

    if-gt v2, v1, :cond_2

    :cond_1
    if-le v2, p1, :cond_4

    if-ge v2, v1, :cond_4

    :cond_2
    move-object v1, v0

    move v0, v2

    :goto_1
    move-object v3, v1

    move v1, v0

    .line 39
    goto :goto_0

    .line 40
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Accepted videoData quality = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "p"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 41
    return-object v3

    :cond_4
    move v0, v1

    move-object v1, v3

    goto :goto_1
.end method

.method public static newVideoData(Ljava/lang/String;II)Lcom/my/target/common/models/VideoData;
    .locals 1
    .param p0, "videoUrl"    # Ljava/lang/String;
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 46
    new-instance v0, Lcom/my/target/common/models/VideoData;

    invoke-direct {v0, p0, p1, p2}, Lcom/my/target/common/models/VideoData;-><init>(Ljava/lang/String;II)V

    return-object v0
.end method


# virtual methods
.method public getBitrate()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/my/target/common/models/VideoData;->bitrate:I

    return v0
.end method

.method public isCacheable()Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/my/target/common/models/VideoData;->cacheable:Z

    return v0
.end method

.method public setBitrate(I)V
    .locals 0
    .param p1, "bitrate"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/my/target/common/models/VideoData;->bitrate:I

    .line 68
    return-void
.end method
