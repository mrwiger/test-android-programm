.class public Lcom/samsung/multiscreen/ble/adparser/AdParser;
.super Ljava/lang/Object;
.source "AdParser.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parseAdData([B)Ljava/util/ArrayList;
    .locals 9
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/multiscreen/ble/adparser/AdElement;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    const/4 v6, 0x0

    .line 29
    .local v6, "pos":I
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 30
    .local v5, "out":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/multiscreen/ble/adparser/AdElement;>;"
    array-length v2, p0

    .line 31
    .local v2, "dlen":I
    :goto_0
    add-int/lit8 v8, v6, 0x1

    if-ge v8, v2, :cond_0

    .line 32
    move v1, v6

    .line 33
    .local v1, "bpos":I
    aget-byte v8, p0, v6

    and-int/lit16 v0, v8, 0xff

    .line 34
    .local v0, "blen":I
    if-nez v0, :cond_1

    .line 113
    .end local v0    # "blen":I
    .end local v1    # "bpos":I
    :cond_0
    return-object v5

    .line 36
    .restart local v0    # "blen":I
    .restart local v1    # "bpos":I
    :cond_1
    add-int v8, v1, v0

    if-gt v8, v2, :cond_0

    .line 38
    add-int/lit8 v6, v6, 0x1

    .line 39
    aget-byte v8, p0, v6

    and-int/lit16 v7, v8, 0xff

    .line 40
    .local v7, "type":I
    add-int/lit8 v6, v6, 0x1

    .line 41
    add-int/lit8 v4, v0, -0x1

    .line 43
    .local v4, "len":I
    sparse-switch v7, :sswitch_data_0

    .line 107
    new-instance v3, Lcom/samsung/multiscreen/ble/adparser/TypeUnknown;

    invoke-direct {v3, v7, p0, v6, v4}, Lcom/samsung/multiscreen/ble/adparser/TypeUnknown;-><init>(I[BII)V

    .line 110
    .local v3, "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    :goto_1
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    add-int v8, v1, v0

    add-int/lit8 v6, v8, 0x1

    .line 112
    goto :goto_0

    .line 46
    .end local v3    # "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    :sswitch_0
    new-instance v3, Lcom/samsung/multiscreen/ble/adparser/TypeFlags;

    invoke-direct {v3, p0, v6}, Lcom/samsung/multiscreen/ble/adparser/TypeFlags;-><init>([BI)V

    .line 47
    .restart local v3    # "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    goto :goto_1

    .line 53
    .end local v3    # "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    :sswitch_1
    new-instance v3, Lcom/samsung/multiscreen/ble/adparser/Type16BitUUIDs;

    invoke-direct {v3, v7, p0, v6, v4}, Lcom/samsung/multiscreen/ble/adparser/Type16BitUUIDs;-><init>(I[BII)V

    .line 54
    .restart local v3    # "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    goto :goto_1

    .line 58
    .end local v3    # "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    :sswitch_2
    new-instance v3, Lcom/samsung/multiscreen/ble/adparser/Type32BitUUIDs;

    invoke-direct {v3, v7, p0, v6, v4}, Lcom/samsung/multiscreen/ble/adparser/Type32BitUUIDs;-><init>(I[BII)V

    .line 59
    .restart local v3    # "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    goto :goto_1

    .line 65
    .end local v3    # "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    :sswitch_3
    new-instance v3, Lcom/samsung/multiscreen/ble/adparser/Type128BitUUIDs;

    invoke-direct {v3, v7, p0, v6, v4}, Lcom/samsung/multiscreen/ble/adparser/Type128BitUUIDs;-><init>(I[BII)V

    .line 66
    .restart local v3    # "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    goto :goto_1

    .line 71
    .end local v3    # "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    :sswitch_4
    new-instance v3, Lcom/samsung/multiscreen/ble/adparser/TypeString;

    invoke-direct {v3, v7, p0, v6, v4}, Lcom/samsung/multiscreen/ble/adparser/TypeString;-><init>(I[BII)V

    .line 72
    .restart local v3    # "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    goto :goto_1

    .line 76
    .end local v3    # "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    :sswitch_5
    new-instance v3, Lcom/samsung/multiscreen/ble/adparser/TypeTXPowerLevel;

    invoke-direct {v3, p0, v6}, Lcom/samsung/multiscreen/ble/adparser/TypeTXPowerLevel;-><init>([BI)V

    .line 77
    .restart local v3    # "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    goto :goto_1

    .line 84
    .end local v3    # "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    :sswitch_6
    new-instance v3, Lcom/samsung/multiscreen/ble/adparser/TypeByteDump;

    invoke-direct {v3, v7, p0, v6, v4}, Lcom/samsung/multiscreen/ble/adparser/TypeByteDump;-><init>(I[BII)V

    .line 85
    .restart local v3    # "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    goto :goto_1

    .line 89
    .end local v3    # "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    :sswitch_7
    new-instance v3, Lcom/samsung/multiscreen/ble/adparser/TypeSecOOBFlags;

    invoke-direct {v3, p0, v6}, Lcom/samsung/multiscreen/ble/adparser/TypeSecOOBFlags;-><init>([BI)V

    .line 90
    .restart local v3    # "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    goto :goto_1

    .line 94
    .end local v3    # "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    :sswitch_8
    new-instance v3, Lcom/samsung/multiscreen/ble/adparser/TypeSlaveConnectionIntervalRange;

    invoke-direct {v3, p0, v6, v4}, Lcom/samsung/multiscreen/ble/adparser/TypeSlaveConnectionIntervalRange;-><init>([BII)V

    .line 95
    .restart local v3    # "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    goto :goto_1

    .line 99
    .end local v3    # "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    :sswitch_9
    new-instance v3, Lcom/samsung/multiscreen/ble/adparser/TypeServiceData;

    invoke-direct {v3, p0, v6, v4}, Lcom/samsung/multiscreen/ble/adparser/TypeServiceData;-><init>([BII)V

    .line 100
    .restart local v3    # "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    goto :goto_1

    .line 103
    .end local v3    # "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    :sswitch_a
    new-instance v3, Lcom/samsung/multiscreen/ble/adparser/TypeManufacturerData;

    invoke-direct {v3, p0, v6, v4}, Lcom/samsung/multiscreen/ble/adparser/TypeManufacturerData;-><init>([BII)V

    .line 104
    .restart local v3    # "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    goto :goto_1

    .line 43
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_1
        0x4 -> :sswitch_2
        0x5 -> :sswitch_2
        0x6 -> :sswitch_3
        0x7 -> :sswitch_3
        0x8 -> :sswitch_4
        0x9 -> :sswitch_4
        0xa -> :sswitch_5
        0xd -> :sswitch_6
        0xe -> :sswitch_6
        0xf -> :sswitch_6
        0x10 -> :sswitch_6
        0x11 -> :sswitch_7
        0x12 -> :sswitch_8
        0x14 -> :sswitch_1
        0x15 -> :sswitch_3
        0x16 -> :sswitch_9
        0xff -> :sswitch_a
    .end sparse-switch
.end method
