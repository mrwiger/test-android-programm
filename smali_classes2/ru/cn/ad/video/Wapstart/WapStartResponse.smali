.class public Lru/cn/ad/video/Wapstart/WapStartResponse;
.super Ljava/lang/Object;
.source "WapStartResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/ad/video/Wapstart/WapStartResponse$Seat;
    }
.end annotation


# instance fields
.field public seat:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/ad/video/Wapstart/WapStartResponse$Seat;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fromJson(Ljava/lang/String;)Lru/cn/ad/video/Wapstart/WapStartResponse;
    .locals 2
    .param p0, "content"    # Ljava/lang/String;

    .prologue
    .line 28
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    .line 29
    .local v0, "gson":Lcom/google/gson/Gson;
    const-class v1, Lru/cn/ad/video/Wapstart/WapStartResponse;

    invoke-virtual {v0, p0, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/ad/video/Wapstart/WapStartResponse;

    return-object v1
.end method
