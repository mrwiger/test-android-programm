.class Lru/cn/player/exoplayer/ExoMediaPlayer$RecoverHandler;
.super Landroid/os/Handler;
.source "ExoMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/player/exoplayer/ExoMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RecoverHandler"
.end annotation


# instance fields
.field private playerRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lru/cn/player/exoplayer/ExoMediaPlayer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lru/cn/player/exoplayer/ExoMediaPlayer;)V
    .locals 1
    .param p1, "player"    # Lru/cn/player/exoplayer/ExoMediaPlayer;

    .prologue
    .line 122
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 123
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer$RecoverHandler;->playerRef:Ljava/lang/ref/WeakReference;

    .line 124
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 128
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 130
    :pswitch_0
    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer$RecoverHandler;->playerRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/player/exoplayer/ExoMediaPlayer;

    .line 131
    .local v0, "player":Lru/cn/player/exoplayer/ExoMediaPlayer;
    if-eqz v0, :cond_0

    .line 132
    invoke-virtual {v0}, Lru/cn/player/exoplayer/ExoMediaPlayer;->getCurrentPosition()I

    move-result v1

    .line 133
    .local v1, "position":I
    invoke-static {v0}, Lru/cn/player/exoplayer/ExoMediaPlayer;->access$000(Lru/cn/player/exoplayer/ExoMediaPlayer;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Lru/cn/player/exoplayer/ExoMediaPlayer;->play(Landroid/net/Uri;)V

    .line 135
    if-eqz v1, :cond_0

    .line 136
    invoke-virtual {v0, v1}, Lru/cn/player/exoplayer/ExoMediaPlayer;->seekTo(I)V

    goto :goto_0

    .line 128
    nop

    :pswitch_data_0
    .packed-switch 0x14d
        :pswitch_0
    .end packed-switch
.end method
