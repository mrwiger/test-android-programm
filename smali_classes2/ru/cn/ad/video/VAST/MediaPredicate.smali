.class public Lru/cn/ad/video/VAST/MediaPredicate;
.super Ljava/lang/Object;
.source "MediaPredicate.java"

# interfaces
.implements Lcom/android/internal/util/Predicate;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/internal/util/Predicate",
        "<",
        "Lru/cn/ad/video/VAST/parser/MediaFile;",
        ">;"
    }
.end annotation


# static fields
.field public static final DEFAULT_MIME_TYPES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final maxDimension:I

.field private final mimeTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 11
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "video/mp4"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "application/x-mpegURL"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "application/vnd.apple.mpegurl"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lru/cn/ad/video/VAST/MediaPredicate;->DEFAULT_MIME_TYPES:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "maxDimension"    # I

    .prologue
    .line 25
    sget-object v0, Lru/cn/ad/video/VAST/MediaPredicate;->DEFAULT_MIME_TYPES:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lru/cn/ad/video/VAST/MediaPredicate;-><init>(Ljava/util/List;I)V

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/util/List;I)V
    .locals 0
    .param p2, "maxDimension"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p1, "mimeTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lru/cn/ad/video/VAST/MediaPredicate;->mimeTypes:Ljava/util/List;

    .line 21
    iput p2, p0, Lru/cn/ad/video/VAST/MediaPredicate;->maxDimension:I

    .line 22
    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 10
    check-cast p1, Lru/cn/ad/video/VAST/parser/MediaFile;

    invoke-virtual {p0, p1}, Lru/cn/ad/video/VAST/MediaPredicate;->apply(Lru/cn/ad/video/VAST/parser/MediaFile;)Z

    move-result v0

    return v0
.end method

.method public apply(Lru/cn/ad/video/VAST/parser/MediaFile;)Z
    .locals 3
    .param p1, "file"    # Lru/cn/ad/video/VAST/parser/MediaFile;

    .prologue
    const/4 v0, 0x0

    .line 30
    iget-object v1, p0, Lru/cn/ad/video/VAST/MediaPredicate;->mimeTypes:Ljava/util/List;

    iget-object v2, p1, Lru/cn/ad/video/VAST/parser/MediaFile;->type:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 36
    :cond_0
    :goto_0
    return v0

    .line 33
    :cond_1
    iget v1, p1, Lru/cn/ad/video/VAST/parser/MediaFile;->height:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    iget v1, p1, Lru/cn/ad/video/VAST/parser/MediaFile;->height:I

    iget v2, p0, Lru/cn/ad/video/VAST/MediaPredicate;->maxDimension:I

    if-gt v1, v2, :cond_0

    .line 36
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
