.class Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$1;
.super Ljava/lang/Object;
.source "MyTargetNativeAdapter.java"

# interfaces
.implements Lcom/my/target/nativeads/NativeAd$NativeAdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->onLoad()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;


# direct methods
.method constructor <init>(Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;

    .prologue
    .line 39
    iput-object p1, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$1;->this$0:Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Lcom/my/target/nativeads/NativeAd;)V
    .locals 2
    .param p1, "nativeAd"    # Lcom/my/target/nativeads/NativeAd;

    .prologue
    .line 65
    const-string v0, "MyTargetNativeAdapter"

    const-string v1, "On click banner"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    iget-object v0, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$1;->this$0:Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;

    invoke-static {v0}, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->access$500(Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$1;->this$0:Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;

    invoke-static {v0}, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->access$600(Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdEnded()V

    .line 69
    :cond_0
    return-void
.end method

.method public onLoad(Lcom/my/target/nativeads/NativeAd;)V
    .locals 2
    .param p1, "nativeAd"    # Lcom/my/target/nativeads/NativeAd;

    .prologue
    .line 42
    const-string v0, "MyTargetNativeAdapter"

    const-string v1, "On load native ad"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    iget-object v0, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$1;->this$0:Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;

    new-instance v1, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;

    invoke-direct {v1, p1}, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;-><init>(Lcom/my/target/nativeads/NativeAd;)V

    invoke-static {v0, v1}, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->access$002(Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;)Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;

    .line 46
    iget-object v0, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$1;->this$0:Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;

    invoke-static {v0}, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->access$100(Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$1;->this$0:Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;

    invoke-static {v0}, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->access$200(Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdLoaded()V

    .line 48
    :cond_0
    return-void
.end method

.method public onNoAd(Ljava/lang/String;Lcom/my/target/nativeads/NativeAd;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;
    .param p2, "nativeAd"    # Lcom/my/target/nativeads/NativeAd;

    .prologue
    .line 57
    const-string v0, "MyTargetNativeAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to load ad wit reason "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    iget-object v0, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$1;->this$0:Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;

    invoke-static {v0}, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->access$300(Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$1;->this$0:Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;

    invoke-static {v0}, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->access$400(Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onError()V

    .line 61
    :cond_0
    return-void
.end method

.method public onShow(Lcom/my/target/nativeads/NativeAd;)V
    .locals 0
    .param p1, "nativeAd"    # Lcom/my/target/nativeads/NativeAd;

    .prologue
    .line 53
    return-void
.end method

.method public onVideoComplete(Lcom/my/target/nativeads/NativeAd;)V
    .locals 0
    .param p1, "nativeAd"    # Lcom/my/target/nativeads/NativeAd;

    .prologue
    .line 74
    return-void
.end method

.method public onVideoPause(Lcom/my/target/nativeads/NativeAd;)V
    .locals 0
    .param p1, "nativeAd"    # Lcom/my/target/nativeads/NativeAd;

    .prologue
    .line 79
    return-void
.end method

.method public onVideoPlay(Lcom/my/target/nativeads/NativeAd;)V
    .locals 0
    .param p1, "nativeAd"    # Lcom/my/target/nativeads/NativeAd;

    .prologue
    .line 84
    return-void
.end method
