.class public final Lcom/google/obf/du;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/du$a;,
        Lcom/google/obf/du$b;
    }
.end annotation


# static fields
.field public static final a:[B

.field public static final b:[F

.field private static final c:Ljava/lang/Object;

.field private static d:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/obf/du;->a:[B

    .line 185
    const/16 v0, 0x11

    new-array v0, v0, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/obf/du;->b:[F

    .line 186
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/obf/du;->c:Ljava/lang/Object;

    .line 187
    const/16 v0, 0xa

    new-array v0, v0, [I

    sput-object v0, Lcom/google/obf/du;->d:[I

    return-void

    .line 184
    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data

    .line 185
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f8ba2e9
        0x3f68ba2f
        0x3fba2e8c
        0x3f9b26ca
        0x400ba2e9
        0x3fe8ba2f
        0x403a2e8c
        0x401b26ca
        0x3fd1745d
        0x3fae8ba3
        0x3ff83e10
        0x3fcede62
        0x3faaaaab
        0x3fc00000    # 1.5f
        0x40000000    # 2.0f
    .end array-data
.end method

.method public static a([BI)I
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 1
    sget-object v4, Lcom/google/obf/du;->c:Ljava/lang/Object;

    monitor-enter v4

    move v3, v0

    move v1, v0

    .line 4
    :goto_0
    if-ge v1, p1, :cond_1

    .line 5
    :try_start_0
    invoke-static {p0, v1, p1}, Lcom/google/obf/du;->a([BII)I

    move-result v2

    .line 6
    if-ge v2, p1, :cond_3

    .line 7
    sget-object v1, Lcom/google/obf/du;->d:[I

    array-length v1, v1

    if-gt v1, v3, :cond_0

    .line 8
    sget-object v1, Lcom/google/obf/du;->d:[I

    sget-object v5, Lcom/google/obf/du;->d:[I

    array-length v5, v5

    mul-int/lit8 v5, v5, 0x2

    invoke-static {v1, v5}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v1

    sput-object v1, Lcom/google/obf/du;->d:[I

    .line 9
    :cond_0
    sget-object v5, Lcom/google/obf/du;->d:[I

    add-int/lit8 v1, v3, 0x1

    aput v2, v5, v3

    .line 10
    add-int/lit8 v2, v2, 0x3

    move v3, v1

    move v1, v2

    goto :goto_0

    .line 11
    :cond_1
    sub-int v5, p1, v3

    move v1, v0

    move v2, v0

    .line 14
    :goto_1
    if-ge v0, v3, :cond_2

    .line 15
    sget-object v6, Lcom/google/obf/du;->d:[I

    aget v6, v6, v0

    .line 16
    sub-int/2addr v6, v2

    .line 17
    invoke-static {p0, v2, p0, v1, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 18
    add-int/2addr v1, v6

    .line 19
    add-int/lit8 v7, v1, 0x1

    const/4 v8, 0x0

    aput-byte v8, p0, v1

    .line 20
    add-int/lit8 v1, v7, 0x1

    const/4 v8, 0x0

    aput-byte v8, p0, v7

    .line 21
    add-int/lit8 v6, v6, 0x3

    add-int/2addr v2, v6

    .line 22
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 23
    :cond_2
    sub-int v0, v5, v1

    .line 24
    invoke-static {p0, v2, p0, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 25
    monitor-exit v4

    return v5

    .line 26
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method private static a([BII)I
    .locals 3

    .prologue
    .line 170
    move v0, p1

    :goto_0
    add-int/lit8 v1, p2, -0x2

    if-ge v0, v1, :cond_0

    .line 171
    aget-byte v1, p0, v0

    if-nez v1, :cond_1

    add-int/lit8 v1, v0, 0x1

    aget-byte v1, p0, v1

    if-nez v1, :cond_1

    add-int/lit8 v1, v0, 0x2

    aget-byte v1, p0, v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    move p2, v0

    .line 174
    :cond_0
    return p2

    .line 173
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static a([BII[Z)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 134
    sub-int v3, p2, p1

    .line 135
    if-ltz v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 136
    if-nez v3, :cond_2

    .line 165
    :cond_0
    :goto_1
    return p2

    :cond_1
    move v0, v2

    .line 135
    goto :goto_0

    .line 138
    :cond_2
    if-eqz p3, :cond_5

    .line 139
    aget-boolean v0, p3, v2

    if-eqz v0, :cond_3

    .line 140
    invoke-static {p3}, Lcom/google/obf/du;->a([Z)V

    .line 141
    add-int/lit8 p2, p1, -0x3

    goto :goto_1

    .line 142
    :cond_3
    if-le v3, v1, :cond_4

    aget-boolean v0, p3, v1

    if-eqz v0, :cond_4

    aget-byte v0, p0, p1

    if-ne v0, v1, :cond_4

    .line 143
    invoke-static {p3}, Lcom/google/obf/du;->a([Z)V

    .line 144
    add-int/lit8 p2, p1, -0x2

    goto :goto_1

    .line 145
    :cond_4
    if-le v3, v6, :cond_5

    aget-boolean v0, p3, v6

    if-eqz v0, :cond_5

    aget-byte v0, p0, p1

    if-nez v0, :cond_5

    add-int/lit8 v0, p1, 0x1

    aget-byte v0, p0, v0

    if-ne v0, v1, :cond_5

    .line 146
    invoke-static {p3}, Lcom/google/obf/du;->a([Z)V

    .line 147
    add-int/lit8 p2, p1, -0x1

    goto :goto_1

    .line 148
    :cond_5
    add-int/lit8 v4, p2, -0x1

    .line 149
    add-int/lit8 v0, p1, 0x2

    :goto_2
    if-ge v0, v4, :cond_9

    .line 150
    aget-byte v5, p0, v0

    and-int/lit16 v5, v5, 0xfe

    if-eqz v5, :cond_6

    .line 156
    :goto_3
    add-int/lit8 v0, v0, 0x3

    goto :goto_2

    .line 151
    :cond_6
    add-int/lit8 v5, v0, -0x2

    aget-byte v5, p0, v5

    if-nez v5, :cond_8

    add-int/lit8 v5, v0, -0x1

    aget-byte v5, p0, v5

    if-nez v5, :cond_8

    aget-byte v5, p0, v0

    if-ne v5, v1, :cond_8

    .line 152
    if-eqz p3, :cond_7

    .line 153
    invoke-static {p3}, Lcom/google/obf/du;->a([Z)V

    .line 154
    :cond_7
    add-int/lit8 p2, v0, -0x2

    goto :goto_1

    .line 155
    :cond_8
    add-int/lit8 v0, v0, -0x2

    goto :goto_3

    .line 157
    :cond_9
    if-eqz p3, :cond_0

    .line 158
    if-le v3, v6, :cond_b

    .line 159
    add-int/lit8 v0, p2, -0x3

    aget-byte v0, p0, v0

    if-nez v0, :cond_a

    add-int/lit8 v0, p2, -0x2

    aget-byte v0, p0, v0

    if-nez v0, :cond_a

    add-int/lit8 v0, p2, -0x1

    aget-byte v0, p0, v0

    if-ne v0, v1, :cond_a

    move v0, v1

    .line 161
    :goto_4
    aput-boolean v0, p3, v2

    .line 162
    if-le v3, v1, :cond_10

    add-int/lit8 v0, p2, -0x2

    aget-byte v0, p0, v0

    if-nez v0, :cond_f

    add-int/lit8 v0, p2, -0x1

    aget-byte v0, p0, v0

    if-nez v0, :cond_f

    move v0, v1

    .line 163
    :goto_5
    aput-boolean v0, p3, v1

    .line 164
    add-int/lit8 v0, p2, -0x1

    aget-byte v0, p0, v0

    if-nez v0, :cond_12

    :goto_6
    aput-boolean v1, p3, v6

    goto/16 :goto_1

    :cond_a
    move v0, v2

    .line 159
    goto :goto_4

    .line 160
    :cond_b
    if-ne v3, v6, :cond_d

    aget-boolean v0, p3, v6

    if-eqz v0, :cond_c

    add-int/lit8 v0, p2, -0x2

    aget-byte v0, p0, v0

    if-nez v0, :cond_c

    add-int/lit8 v0, p2, -0x1

    aget-byte v0, p0, v0

    if-ne v0, v1, :cond_c

    move v0, v1

    goto :goto_4

    :cond_c
    move v0, v2

    goto :goto_4

    .line 161
    :cond_d
    aget-boolean v0, p3, v1

    if-eqz v0, :cond_e

    add-int/lit8 v0, p2, -0x1

    aget-byte v0, p0, v0

    if-ne v0, v1, :cond_e

    move v0, v1

    goto :goto_4

    :cond_e
    move v0, v2

    goto :goto_4

    :cond_f
    move v0, v2

    .line 162
    goto :goto_5

    .line 163
    :cond_10
    aget-boolean v0, p3, v6

    if-eqz v0, :cond_11

    add-int/lit8 v0, p2, -0x1

    aget-byte v0, p0, v0

    if-nez v0, :cond_11

    move v0, v1

    goto :goto_5

    :cond_11
    move v0, v2

    goto :goto_5

    :cond_12
    move v1, v2

    .line 164
    goto :goto_6
.end method

.method public static a(Lcom/google/obf/dv;)Lcom/google/obf/du$b;
    .locals 18

    .prologue
    .line 54
    const/16 v2, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/obf/dv;->c(I)I

    move-result v5

    .line 55
    const/16 v2, 0x10

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/obf/dv;->b(I)V

    .line 56
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->d()I

    move-result v3

    .line 57
    const/4 v4, 0x1

    .line 58
    const/4 v2, 0x0

    .line 59
    const/16 v6, 0x64

    if-eq v5, v6, :cond_0

    const/16 v6, 0x6e

    if-eq v5, v6, :cond_0

    const/16 v6, 0x7a

    if-eq v5, v6, :cond_0

    const/16 v6, 0xf4

    if-eq v5, v6, :cond_0

    const/16 v6, 0x2c

    if-eq v5, v6, :cond_0

    const/16 v6, 0x53

    if-eq v5, v6, :cond_0

    const/16 v6, 0x56

    if-eq v5, v6, :cond_0

    const/16 v6, 0x76

    if-eq v5, v6, :cond_0

    const/16 v6, 0x80

    if-eq v5, v6, :cond_0

    const/16 v6, 0x8a

    if-ne v5, v6, :cond_14

    .line 60
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->d()I

    move-result v6

    .line 61
    const/4 v4, 0x3

    if-ne v6, v4, :cond_1

    .line 62
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->b()Z

    move-result v2

    .line 63
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->d()I

    .line 64
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->d()I

    .line 65
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/obf/dv;->b(I)V

    .line 66
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->b()Z

    move-result v4

    .line 67
    if-eqz v4, :cond_5

    .line 68
    const/4 v4, 0x3

    if-eq v6, v4, :cond_3

    const/16 v4, 0x8

    .line 69
    :goto_0
    const/4 v5, 0x0

    move v7, v5

    :goto_1
    if-ge v7, v4, :cond_5

    .line 70
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->b()Z

    move-result v5

    .line 71
    if-eqz v5, :cond_2

    .line 72
    const/4 v5, 0x6

    if-ge v7, v5, :cond_4

    const/16 v5, 0x10

    :goto_2
    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/google/obf/du;->a(Lcom/google/obf/dv;I)V

    .line 73
    :cond_2
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    goto :goto_1

    .line 68
    :cond_3
    const/16 v4, 0xc

    goto :goto_0

    .line 72
    :cond_4
    const/16 v5, 0x40

    goto :goto_2

    :cond_5
    move v7, v2

    move v2, v6

    .line 74
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->d()I

    move-result v4

    add-int/lit8 v9, v4, 0x4

    .line 75
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->d()I

    move-result v10

    .line 76
    const/4 v11, 0x0

    .line 77
    const/4 v12, 0x0

    .line 78
    if-nez v10, :cond_9

    .line 79
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->d()I

    move-result v4

    add-int/lit8 v11, v4, 0x4

    .line 88
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->d()I

    .line 89
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/obf/dv;->b(I)V

    .line 90
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->d()I

    move-result v4

    add-int/lit8 v5, v4, 0x1

    .line 91
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->d()I

    move-result v4

    add-int/lit8 v6, v4, 0x1

    .line 92
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->b()Z

    move-result v8

    .line 93
    if-eqz v8, :cond_a

    const/4 v4, 0x1

    :goto_4
    rsub-int/lit8 v4, v4, 0x2

    mul-int/2addr v4, v6

    .line 94
    if-nez v8, :cond_7

    .line 95
    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/obf/dv;->b(I)V

    .line 96
    :cond_7
    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/obf/dv;->b(I)V

    .line 97
    mul-int/lit8 v6, v5, 0x10

    .line 98
    mul-int/lit8 v5, v4, 0x10

    .line 99
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->b()Z

    move-result v4

    .line 100
    if-eqz v4, :cond_13

    .line 101
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->d()I

    move-result v14

    .line 102
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->d()I

    move-result v15

    .line 103
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->d()I

    move-result v16

    .line 104
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->d()I

    move-result v17

    .line 105
    if-nez v2, :cond_c

    .line 106
    const/4 v4, 0x1

    .line 107
    if-eqz v8, :cond_b

    const/4 v2, 0x1

    :goto_5
    rsub-int/lit8 v2, v2, 0x2

    .line 112
    :goto_6
    add-int v13, v14, v15

    mul-int/2addr v4, v13

    sub-int v4, v6, v4

    .line 113
    add-int v6, v16, v17

    mul-int/2addr v2, v6

    sub-int/2addr v5, v2

    .line 114
    :goto_7
    const/high16 v2, 0x3f800000    # 1.0f

    .line 115
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->b()Z

    move-result v6

    .line 116
    if-eqz v6, :cond_12

    .line 117
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->b()Z

    move-result v6

    .line 118
    if-eqz v6, :cond_12

    .line 119
    const/16 v6, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/obf/dv;->c(I)I

    move-result v6

    .line 120
    const/16 v13, 0xff

    if-ne v6, v13, :cond_10

    .line 121
    const/16 v6, 0x10

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/obf/dv;->c(I)I

    move-result v6

    .line 122
    const/16 v13, 0x10

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/obf/dv;->c(I)I

    move-result v13

    .line 123
    if-eqz v6, :cond_8

    if-eqz v13, :cond_8

    .line 124
    int-to-float v2, v6

    int-to-float v6, v13

    div-float/2addr v2, v6

    :cond_8
    move v6, v2

    .line 128
    :goto_8
    new-instance v2, Lcom/google/obf/du$b;

    invoke-direct/range {v2 .. v12}, Lcom/google/obf/du$b;-><init>(IIIFZZIIIZ)V

    return-object v2

    .line 80
    :cond_9
    const/4 v4, 0x1

    if-ne v10, v4, :cond_6

    .line 81
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->b()Z

    move-result v12

    .line 82
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->e()I

    .line 83
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->e()I

    .line 84
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->d()I

    move-result v4

    int-to-long v14, v4

    .line 85
    const/4 v4, 0x0

    :goto_9
    int-to-long v0, v4

    move-wide/from16 v16, v0

    cmp-long v5, v16, v14

    if-gez v5, :cond_6

    .line 86
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dv;->d()I

    .line 87
    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    .line 93
    :cond_a
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 107
    :cond_b
    const/4 v2, 0x0

    goto :goto_5

    .line 108
    :cond_c
    const/4 v4, 0x3

    if-ne v2, v4, :cond_d

    const/4 v4, 0x1

    .line 109
    :goto_a
    const/4 v13, 0x1

    if-ne v2, v13, :cond_e

    const/4 v2, 0x2

    move v13, v2

    .line 111
    :goto_b
    if-eqz v8, :cond_f

    const/4 v2, 0x1

    :goto_c
    rsub-int/lit8 v2, v2, 0x2

    mul-int/2addr v2, v13

    goto :goto_6

    .line 108
    :cond_d
    const/4 v4, 0x2

    goto :goto_a

    .line 109
    :cond_e
    const/4 v2, 0x1

    move v13, v2

    goto :goto_b

    .line 111
    :cond_f
    const/4 v2, 0x0

    goto :goto_c

    .line 125
    :cond_10
    sget-object v13, Lcom/google/obf/du;->b:[F

    array-length v13, v13

    if-ge v6, v13, :cond_11

    .line 126
    sget-object v2, Lcom/google/obf/du;->b:[F

    aget v2, v2, v6

    move v6, v2

    goto :goto_8

    .line 127
    :cond_11
    const-string v13, "NalUnitUtil"

    const/16 v14, 0x2e

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15, v14}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v14, "Unexpected aspect_ratio_idc value: "

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v13, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_12
    move v6, v2

    goto :goto_8

    :cond_13
    move v4, v6

    goto/16 :goto_7

    :cond_14
    move v7, v2

    move v2, v4

    goto/16 :goto_3
.end method

.method private static a(Lcom/google/obf/dv;I)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 175
    .line 177
    const/4 v0, 0x0

    move v2, v0

    move v0, v1

    :goto_0
    if-ge v2, p1, :cond_2

    .line 178
    if-eqz v1, :cond_0

    .line 179
    invoke-virtual {p0}, Lcom/google/obf/dv;->e()I

    move-result v1

    .line 180
    add-int/2addr v1, v0

    add-int/lit16 v1, v1, 0x100

    rem-int/lit16 v1, v1, 0x100

    .line 181
    :cond_0
    if-nez v1, :cond_1

    .line 182
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 181
    goto :goto_1

    .line 183
    :cond_2
    return-void
.end method

.method public static a(Ljava/nio/ByteBuffer;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    move v2, v1

    move v0, v1

    .line 30
    :goto_0
    add-int/lit8 v4, v2, 0x1

    if-ge v4, v3, :cond_3

    .line 31
    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    .line 32
    const/4 v5, 0x3

    if-ne v0, v5, :cond_0

    .line 33
    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    add-int/lit8 v5, v2, 0x1

    invoke-virtual {p0, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v5

    and-int/lit8 v5, v5, 0x1f

    const/4 v6, 0x7

    if-ne v5, v6, :cond_1

    .line 34
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 35
    add-int/lit8 v2, v2, -0x3

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 36
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 37
    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 38
    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 47
    :goto_1
    return-void

    .line 40
    :cond_0
    if-nez v4, :cond_1

    .line 41
    add-int/lit8 v0, v0, 0x1

    .line 42
    :cond_1
    if-eqz v4, :cond_2

    move v0, v1

    .line 44
    :cond_2
    add-int/lit8 v2, v2, 0x1

    .line 45
    goto :goto_0

    .line 46
    :cond_3
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_1
.end method

.method public static a([Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 166
    aput-boolean v1, p0, v1

    .line 167
    const/4 v0, 0x1

    aput-boolean v1, p0, v0

    .line 168
    const/4 v0, 0x2

    aput-boolean v1, p0, v0

    .line 169
    return-void
.end method

.method public static a(Lcom/google/obf/dw;)[B
    .locals 3

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/google/obf/dw;->g()I

    move-result v0

    .line 49
    invoke-virtual {p0}, Lcom/google/obf/dw;->d()I

    move-result v1

    .line 50
    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->d(I)V

    .line 51
    iget-object v2, p0, Lcom/google/obf/dw;->a:[B

    invoke-static {v2, v1, v0}, Lcom/google/obf/dm;->a([BII)[B

    move-result-object v0

    return-object v0
.end method

.method public static b([BI)I
    .locals 1

    .prologue
    .line 52
    add-int/lit8 v0, p1, 0x3

    aget-byte v0, p0, v0

    and-int/lit8 v0, v0, 0x1f

    return v0
.end method

.method public static b(Lcom/google/obf/dv;)Lcom/google/obf/du$a;
    .locals 4

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/google/obf/dv;->d()I

    move-result v0

    .line 130
    invoke-virtual {p0}, Lcom/google/obf/dv;->d()I

    move-result v1

    .line 131
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/obf/dv;->b(I)V

    .line 132
    invoke-virtual {p0}, Lcom/google/obf/dv;->b()Z

    move-result v2

    .line 133
    new-instance v3, Lcom/google/obf/du$a;

    invoke-direct {v3, v0, v1, v2}, Lcom/google/obf/du$a;-><init>(IIZ)V

    return-object v3
.end method

.method public static c([BI)I
    .locals 1

    .prologue
    .line 53
    add-int/lit8 v0, p1, 0x3

    aget-byte v0, p0, v0

    and-int/lit8 v0, v0, 0x7e

    shr-int/lit8 v0, v0, 0x1

    return v0
.end method
