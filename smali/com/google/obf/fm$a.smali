.class final Lcom/google/obf/fm$a;
.super Ljava/io/Writer;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/fm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/fm$a$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Appendable;

.field private final b:Lcom/google/obf/fm$a$a;


# direct methods
.method constructor <init>(Ljava/lang/Appendable;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/io/Writer;-><init>()V

    .line 2
    new-instance v0, Lcom/google/obf/fm$a$a;

    invoke-direct {v0}, Lcom/google/obf/fm$a$a;-><init>()V

    iput-object v0, p0, Lcom/google/obf/fm$a;->b:Lcom/google/obf/fm$a$a;

    .line 3
    iput-object p1, p0, Lcom/google/obf/fm$a;->a:Ljava/lang/Appendable;

    .line 4
    return-void
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 11
    return-void
.end method

.method public flush()V
    .locals 0

    .prologue
    .line 10
    return-void
.end method

.method public write(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    iget-object v0, p0, Lcom/google/obf/fm$a;->a:Ljava/lang/Appendable;

    int-to-char v1, p1

    invoke-interface {v0, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 9
    return-void
.end method

.method public write([CII)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/obf/fm$a;->b:Lcom/google/obf/fm$a$a;

    iput-object p1, v0, Lcom/google/obf/fm$a$a;->a:[C

    .line 6
    iget-object v0, p0, Lcom/google/obf/fm$a;->a:Ljava/lang/Appendable;

    iget-object v1, p0, Lcom/google/obf/fm$a;->b:Lcom/google/obf/fm$a$a;

    add-int v2, p2, p3

    invoke-interface {v0, v1, p2, v2}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;

    .line 7
    return-void
.end method
