.class public Lcom/yandex/metrica/impl/ob/aa;
.super Lcom/yandex/metrica/impl/ob/ac;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/yandex/metrica/impl/ob/ac",
        "<",
        "Lcom/yandex/metrica/impl/ob/ah;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/yandex/metrica/impl/ob/aq;

.field private final b:Lcom/yandex/metrica/impl/ob/ag;

.field private final c:Lcom/yandex/metrica/impl/ob/ao;

.field private final d:Lcom/yandex/metrica/impl/ob/at;

.field private final e:Lcom/yandex/metrica/impl/ob/av;

.field private final f:Lcom/yandex/metrica/impl/ob/ay;

.field private final g:Lcom/yandex/metrica/impl/ob/bc;

.field private final h:Lcom/yandex/metrica/impl/ob/aw;

.field private final i:Lcom/yandex/metrica/impl/ob/au;

.field private final j:Lcom/yandex/metrica/impl/ob/ax;

.field private final k:Lcom/yandex/metrica/impl/ob/ap;

.field private final l:Lcom/yandex/metrica/impl/ob/as;

.field private final m:Lcom/yandex/metrica/impl/ob/af;

.field private final n:Lcom/yandex/metrica/impl/ob/ae;

.field private final o:Lcom/yandex/metrica/impl/ob/aj;

.field private final p:Lcom/yandex/metrica/impl/ob/al;


# direct methods
.method public constructor <init>(Lcom/yandex/metrica/impl/ob/v;)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ac;-><init>()V

    .line 58
    new-instance v0, Lcom/yandex/metrica/impl/ob/aq;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/aq;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/aa;->a:Lcom/yandex/metrica/impl/ob/aq;

    .line 59
    new-instance v0, Lcom/yandex/metrica/impl/ob/ag;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/ag;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/aa;->b:Lcom/yandex/metrica/impl/ob/ag;

    .line 60
    new-instance v0, Lcom/yandex/metrica/impl/ob/ao;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/ao;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/aa;->c:Lcom/yandex/metrica/impl/ob/ao;

    .line 61
    new-instance v0, Lcom/yandex/metrica/impl/ob/at;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/at;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/aa;->d:Lcom/yandex/metrica/impl/ob/at;

    .line 62
    new-instance v0, Lcom/yandex/metrica/impl/ob/av;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/av;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/aa;->e:Lcom/yandex/metrica/impl/ob/av;

    .line 63
    new-instance v0, Lcom/yandex/metrica/impl/ob/ay;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/ay;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/aa;->f:Lcom/yandex/metrica/impl/ob/ay;

    .line 64
    new-instance v0, Lcom/yandex/metrica/impl/ob/bc;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/bc;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/aa;->g:Lcom/yandex/metrica/impl/ob/bc;

    .line 65
    new-instance v0, Lcom/yandex/metrica/impl/ob/aw;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/aw;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/aa;->h:Lcom/yandex/metrica/impl/ob/aw;

    .line 66
    new-instance v0, Lcom/yandex/metrica/impl/ob/au;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/au;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/aa;->i:Lcom/yandex/metrica/impl/ob/au;

    .line 67
    new-instance v0, Lcom/yandex/metrica/impl/ob/ax;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/ax;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/aa;->j:Lcom/yandex/metrica/impl/ob/ax;

    .line 68
    new-instance v0, Lcom/yandex/metrica/impl/ob/ap;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/ap;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/aa;->k:Lcom/yandex/metrica/impl/ob/ap;

    .line 69
    new-instance v0, Lcom/yandex/metrica/impl/ob/as;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/as;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/aa;->l:Lcom/yandex/metrica/impl/ob/as;

    .line 70
    new-instance v0, Lcom/yandex/metrica/impl/ob/af;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/af;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/aa;->m:Lcom/yandex/metrica/impl/ob/af;

    .line 71
    new-instance v0, Lcom/yandex/metrica/impl/ob/ae;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/ae;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/aa;->n:Lcom/yandex/metrica/impl/ob/ae;

    .line 72
    new-instance v0, Lcom/yandex/metrica/impl/ob/aj;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/aj;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/aa;->o:Lcom/yandex/metrica/impl/ob/aj;

    .line 73
    new-instance v0, Lcom/yandex/metrica/impl/ob/al;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/al;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/aa;->p:Lcom/yandex/metrica/impl/ob/al;

    .line 74
    return-void
.end method


# virtual methods
.method a(I)Lcom/yandex/metrica/impl/ob/z;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/yandex/metrica/impl/ob/z",
            "<",
            "Lcom/yandex/metrica/impl/ob/ah;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 79
    invoke-static {p1}, Lcom/yandex/metrica/impl/q$a;->a(I)Lcom/yandex/metrica/impl/q$a;

    move-result-object v1

    .line 1162
    invoke-static {v1}, Lcom/yandex/metrica/impl/q;->b(Lcom/yandex/metrica/impl/q$a;)Z

    move-result v2

    .line 80
    if-eqz v2, :cond_0

    .line 81
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aa;->j:Lcom/yandex/metrica/impl/ob/ax;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2154
    :cond_0
    invoke-static {v1}, Lcom/yandex/metrica/impl/q;->a(Lcom/yandex/metrica/impl/q$a;)Z

    move-result v2

    .line 83
    if-eqz v2, :cond_1

    .line 84
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aa;->e:Lcom/yandex/metrica/impl/ob/av;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    :cond_1
    sget-object v2, Lcom/yandex/metrica/impl/ob/aa$1;->a:[I

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/q$a;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2158
    :goto_0
    invoke-static {v1}, Lcom/yandex/metrica/impl/q;->c(Lcom/yandex/metrica/impl/q$a;)Z

    move-result v1

    .line 146
    if-eqz v1, :cond_2

    .line 147
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/aa;->k:Lcom/yandex/metrica/impl/ob/ap;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    :cond_2
    new-instance v1, Lcom/yandex/metrica/impl/ob/y;

    invoke-direct {v1, v0}, Lcom/yandex/metrica/impl/ob/y;-><init>(Ljava/util/List;)V

    return-object v1

    .line 88
    :pswitch_0
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aa;->o:Lcom/yandex/metrica/impl/ob/aj;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aa;->f:Lcom/yandex/metrica/impl/ob/ay;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 92
    :pswitch_1
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aa;->a:Lcom/yandex/metrica/impl/ob/aq;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 95
    :pswitch_2
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aa;->a:Lcom/yandex/metrica/impl/ob/aq;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aa;->d:Lcom/yandex/metrica/impl/ob/at;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 103
    :pswitch_3
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aa;->d:Lcom/yandex/metrica/impl/ob/at;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 106
    :pswitch_4
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aa;->f:Lcom/yandex/metrica/impl/ob/ay;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aa;->a:Lcom/yandex/metrica/impl/ob/aq;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 110
    :pswitch_5
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aa;->c:Lcom/yandex/metrica/impl/ob/ao;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 113
    :pswitch_6
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aa;->p:Lcom/yandex/metrica/impl/ob/al;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 117
    :pswitch_7
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aa;->c:Lcom/yandex/metrica/impl/ob/ao;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aa;->d:Lcom/yandex/metrica/impl/ob/at;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aa;->b:Lcom/yandex/metrica/impl/ob/ag;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aa;->g:Lcom/yandex/metrica/impl/ob/bc;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 123
    :pswitch_8
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aa;->f:Lcom/yandex/metrica/impl/ob/ay;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 124
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aa;->a:Lcom/yandex/metrica/impl/ob/aq;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 127
    :pswitch_9
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aa;->h:Lcom/yandex/metrica/impl/ob/aw;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 130
    :pswitch_a
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aa;->i:Lcom/yandex/metrica/impl/ob/au;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 133
    :pswitch_b
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aa;->l:Lcom/yandex/metrica/impl/ob/as;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 136
    :pswitch_c
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aa;->m:Lcom/yandex/metrica/impl/ob/af;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 139
    :pswitch_d
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/aa;->n:Lcom/yandex/metrica/impl/ob/ae;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 86
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method
