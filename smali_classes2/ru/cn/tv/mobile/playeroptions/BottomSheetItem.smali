.class public Lru/cn/tv/mobile/playeroptions/BottomSheetItem;
.super Ljava/lang/Object;
.source "BottomSheetItem.java"


# instance fields
.field private imageResource:I

.field private text:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "imageResource"    # I
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput p1, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetItem;->imageResource:I

    .line 10
    iput-object p2, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetItem;->text:Ljava/lang/String;

    .line 11
    return-void
.end method


# virtual methods
.method public getImageResource()I
    .locals 1

    .prologue
    .line 14
    iget v0, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetItem;->imageResource:I

    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetItem;->text:Ljava/lang/String;

    return-object v0
.end method
