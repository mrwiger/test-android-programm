.class public Lru/cn/tv/playlists/UserPlaylistsActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "UserPlaylistsActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 14
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 15
    const v2, 0x7f0c00b9

    invoke-virtual {p0, v2}, Lru/cn/tv/playlists/UserPlaylistsActivity;->setContentView(I)V

    .line 17
    const v2, 0x7f0901e2

    invoke-virtual {p0, v2}, Lru/cn/tv/playlists/UserPlaylistsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/Toolbar;

    .line 18
    .local v1, "toolbar":Landroid/support/v7/widget/Toolbar;
    invoke-virtual {p0, v1}, Lru/cn/tv/playlists/UserPlaylistsActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 20
    invoke-virtual {p0}, Lru/cn/tv/playlists/UserPlaylistsActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 21
    invoke-virtual {p0}, Lru/cn/tv/playlists/UserPlaylistsActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/support/v7/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 23
    if-nez p1, :cond_0

    .line 24
    new-instance v0, Lru/cn/tv/playlists/UserPlaylistsFragment;

    invoke-direct {v0}, Lru/cn/tv/playlists/UserPlaylistsFragment;-><init>()V

    .line 25
    .local v0, "fragment":Lru/cn/tv/playlists/UserPlaylistsFragment;
    invoke-virtual {p0}, Lru/cn/tv/playlists/UserPlaylistsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    const v3, 0x7f090085

    .line 26
    invoke-virtual {v2, v3, v0}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 28
    .end local v0    # "fragment":Lru/cn/tv/playlists/UserPlaylistsFragment;
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 32
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 33
    invoke-virtual {p0}, Lru/cn/tv/playlists/UserPlaylistsActivity;->onBackPressed()V

    .line 34
    const/4 v0, 0x1

    .line 36
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
