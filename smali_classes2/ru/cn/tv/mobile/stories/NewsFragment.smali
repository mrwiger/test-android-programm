.class public Lru/cn/tv/mobile/stories/NewsFragment;
.super Landroid/support/v4/app/ListFragment;
.source "NewsFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/mobile/stories/NewsFragment$NewsFragmentListener;
    }
.end annotation


# instance fields
.field private adapter:Lru/cn/tv/mobile/stories/StoriesRubricAdapter;

.field private listener:Lru/cn/tv/mobile/stories/NewsFragment$NewsFragmentListener;

.field private rubricId:J

.field private viewModel:Lru/cn/tv/mobile/stories/NewsViewModel;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/mobile/stories/NewsFragment;)J
    .locals 2
    .param p0, "x0"    # Lru/cn/tv/mobile/stories/NewsFragment;

    .prologue
    .line 21
    iget-wide v0, p0, Lru/cn/tv/mobile/stories/NewsFragment;->rubricId:J

    return-wide v0
.end method

.method static synthetic access$100(Lru/cn/tv/mobile/stories/NewsFragment;)Lru/cn/tv/mobile/stories/NewsViewModel;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/stories/NewsFragment;

    .prologue
    .line 21
    iget-object v0, p0, Lru/cn/tv/mobile/stories/NewsFragment;->viewModel:Lru/cn/tv/mobile/stories/NewsViewModel;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/tv/mobile/stories/NewsFragment;)Lru/cn/tv/mobile/stories/NewsFragment$NewsFragmentListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/stories/NewsFragment;

    .prologue
    .line 21
    iget-object v0, p0, Lru/cn/tv/mobile/stories/NewsFragment;->listener:Lru/cn/tv/mobile/stories/NewsFragment$NewsFragmentListener;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/tv/mobile/stories/NewsFragment;)Lru/cn/tv/mobile/stories/StoriesRubricAdapter;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/stories/NewsFragment;

    .prologue
    .line 21
    iget-object v0, p0, Lru/cn/tv/mobile/stories/NewsFragment;->adapter:Lru/cn/tv/mobile/stories/StoriesRubricAdapter;

    return-object v0
.end method

.method public static newInstance(J)Lru/cn/tv/mobile/stories/NewsFragment;
    .locals 4
    .param p0, "rubricId"    # J

    .prologue
    .line 35
    new-instance v1, Lru/cn/tv/mobile/stories/NewsFragment;

    invoke-direct {v1}, Lru/cn/tv/mobile/stories/NewsFragment;-><init>()V

    .line 37
    .local v1, "fragment":Lru/cn/tv/mobile/stories/NewsFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 38
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "rubric"

    invoke-virtual {v0, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 39
    invoke-virtual {v1, v0}, Lru/cn/tv/mobile/stories/NewsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 41
    return-object v1
.end method

.method private setNews(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 103
    iget-object v0, p0, Lru/cn/tv/mobile/stories/NewsFragment;->adapter:Lru/cn/tv/mobile/stories/StoriesRubricAdapter;

    invoke-virtual {v0, p1}, Lru/cn/tv/mobile/stories/StoriesRubricAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 104
    return-void
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$NewsFragment(Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/mobile/stories/NewsFragment;->setNews(Landroid/database/Cursor;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 48
    invoke-virtual {p0}, Lru/cn/tv/mobile/stories/NewsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 49
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v1, "rubric"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lru/cn/tv/mobile/stories/NewsFragment;->rubricId:J

    .line 50
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v1

    const-class v2, Lru/cn/tv/mobile/stories/NewsViewModel;

    invoke-static {p0, v1, v2}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v1

    check-cast v1, Lru/cn/tv/mobile/stories/NewsViewModel;

    iput-object v1, p0, Lru/cn/tv/mobile/stories/NewsFragment;->viewModel:Lru/cn/tv/mobile/stories/NewsViewModel;

    .line 51
    iget-object v1, p0, Lru/cn/tv/mobile/stories/NewsFragment;->viewModel:Lru/cn/tv/mobile/stories/NewsViewModel;

    invoke-virtual {v1}, Lru/cn/tv/mobile/stories/NewsViewModel;->news()Landroid/arch/lifecycle/LiveData;

    move-result-object v1

    new-instance v2, Lru/cn/tv/mobile/stories/NewsFragment$$Lambda$0;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/stories/NewsFragment$$Lambda$0;-><init>(Lru/cn/tv/mobile/stories/NewsFragment;)V

    invoke-virtual {v1, p0, v2}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 53
    iget-object v1, p0, Lru/cn/tv/mobile/stories/NewsFragment;->viewModel:Lru/cn/tv/mobile/stories/NewsViewModel;

    iget-wide v2, p0, Lru/cn/tv/mobile/stories/NewsFragment;->rubricId:J

    invoke-virtual {v1, v2, v3}, Lru/cn/tv/mobile/stories/NewsViewModel;->setRubricId(J)V

    .line 54
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 58
    const v0, 0x7f0c00a7

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 63
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 65
    new-instance v1, Lru/cn/tv/mobile/stories/StoriesRubricAdapter;

    invoke-virtual {p0}, Lru/cn/tv/mobile/stories/NewsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    const v4, 0x7f0c00a8

    invoke-direct {v1, v2, v3, v4}, Lru/cn/tv/mobile/stories/StoriesRubricAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    iput-object v1, p0, Lru/cn/tv/mobile/stories/NewsFragment;->adapter:Lru/cn/tv/mobile/stories/StoriesRubricAdapter;

    .line 66
    iget-object v1, p0, Lru/cn/tv/mobile/stories/NewsFragment;->adapter:Lru/cn/tv/mobile/stories/StoriesRubricAdapter;

    invoke-virtual {p0, v1}, Lru/cn/tv/mobile/stories/NewsFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 68
    const v1, 0x7f09017e

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 69
    .local v0, "repeatButton":Landroid/widget/Button;
    new-instance v1, Lru/cn/tv/mobile/stories/NewsFragment$1;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/stories/NewsFragment$1;-><init>(Lru/cn/tv/mobile/stories/NewsFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    invoke-virtual {p0}, Lru/cn/tv/mobile/stories/NewsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    new-instance v2, Lru/cn/tv/mobile/stories/NewsFragment$2;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/stories/NewsFragment$2;-><init>(Lru/cn/tv/mobile/stories/NewsFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 95
    iget-object v1, p0, Lru/cn/tv/mobile/stories/NewsFragment;->viewModel:Lru/cn/tv/mobile/stories/NewsViewModel;

    invoke-virtual {v1}, Lru/cn/tv/mobile/stories/NewsViewModel;->news()Landroid/arch/lifecycle/LiveData;

    move-result-object v1

    invoke-virtual {v1}, Landroid/arch/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    invoke-direct {p0, v1}, Lru/cn/tv/mobile/stories/NewsFragment;->setNews(Landroid/database/Cursor;)V

    .line 96
    return-void
.end method

.method public setListener(Lru/cn/tv/mobile/stories/NewsFragment$NewsFragmentListener;)V
    .locals 0
    .param p1, "l"    # Lru/cn/tv/mobile/stories/NewsFragment$NewsFragmentListener;

    .prologue
    .line 99
    iput-object p1, p0, Lru/cn/tv/mobile/stories/NewsFragment;->listener:Lru/cn/tv/mobile/stories/NewsFragment$NewsFragmentListener;

    .line 100
    return-void
.end method
