.class public Lru/cn/tv/playlists/UserPlaylistsFragment;
.super Landroid/support/v4/app/Fragment;
.source "UserPlaylistsFragment.java"


# instance fields
.field private adapter:Lru/cn/tv/playlists/UserPlaylistsAdapter;

.field private emptyTextView:Landroid/view/View;

.field private list:Landroid/widget/ListView;

.field mListItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private progressView:Landroid/view/View;

.field private viewModel:Lru/cn/tv/playlists/UserPlaylistsViewModel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 33
    new-instance v0, Lru/cn/tv/playlists/UserPlaylistsFragment$1;

    invoke-direct {v0, p0}, Lru/cn/tv/playlists/UserPlaylistsFragment$1;-><init>(Lru/cn/tv/playlists/UserPlaylistsFragment;)V

    iput-object v0, p0, Lru/cn/tv/playlists/UserPlaylistsFragment;->mListItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/playlists/UserPlaylistsFragment;)Lru/cn/tv/playlists/UserPlaylistsAdapter;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/playlists/UserPlaylistsFragment;

    .prologue
    .line 22
    iget-object v0, p0, Lru/cn/tv/playlists/UserPlaylistsFragment;->adapter:Lru/cn/tv/playlists/UserPlaylistsAdapter;

    return-object v0
.end method

.method private setPlaylist(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x1

    .line 101
    iget-object v0, p0, Lru/cn/tv/playlists/UserPlaylistsFragment;->progressView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 103
    if-nez p1, :cond_0

    .line 104
    invoke-virtual {p0}, Lru/cn/tv/playlists/UserPlaylistsFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0e00a5

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 114
    :goto_0
    return-void

    .line 108
    :cond_0
    iget-object v0, p0, Lru/cn/tv/playlists/UserPlaylistsFragment;->adapter:Lru/cn/tv/playlists/UserPlaylistsAdapter;

    invoke-virtual {v0, p1}, Lru/cn/tv/playlists/UserPlaylistsAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 109
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lt v0, v2, :cond_1

    .line 110
    iget-object v0, p0, Lru/cn/tv/playlists/UserPlaylistsFragment;->emptyTextView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 112
    :cond_1
    iget-object v0, p0, Lru/cn/tv/playlists/UserPlaylistsFragment;->emptyTextView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$UserPlaylistsFragment(Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/playlists/UserPlaylistsFragment;->setPlaylist(Landroid/database/Cursor;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 48
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 49
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lru/cn/tv/playlists/UserPlaylistsFragment;->setHasOptionsMenu(Z)V

    .line 51
    new-instance v0, Lru/cn/tv/playlists/UserPlaylistsAdapter;

    .line 52
    invoke-virtual {p0}, Lru/cn/tv/playlists/UserPlaylistsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0c00bb

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lru/cn/tv/playlists/UserPlaylistsAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;)V

    iput-object v0, p0, Lru/cn/tv/playlists/UserPlaylistsFragment;->adapter:Lru/cn/tv/playlists/UserPlaylistsAdapter;

    .line 57
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v0

    const-class v1, Lru/cn/tv/playlists/UserPlaylistsViewModel;

    invoke-static {p0, v0, v1}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lru/cn/tv/playlists/UserPlaylistsViewModel;

    iput-object v0, p0, Lru/cn/tv/playlists/UserPlaylistsFragment;->viewModel:Lru/cn/tv/playlists/UserPlaylistsViewModel;

    .line 58
    iget-object v0, p0, Lru/cn/tv/playlists/UserPlaylistsFragment;->viewModel:Lru/cn/tv/playlists/UserPlaylistsViewModel;

    invoke-virtual {v0}, Lru/cn/tv/playlists/UserPlaylistsViewModel;->userPlaylists()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/playlists/UserPlaylistsFragment$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/playlists/UserPlaylistsFragment$$Lambda$0;-><init>(Lru/cn/tv/playlists/UserPlaylistsFragment;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 59
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 83
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 84
    const v0, 0x7f0e0186

    invoke-interface {p1, v1, v2, v1, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0802e3

    .line 85
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 86
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 87
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 63
    const v0, 0x7f0c00ba

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 91
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 96
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 93
    :pswitch_0
    invoke-static {}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->newInstance()Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    move-result-object v0

    invoke-virtual {p0}, Lru/cn/tv/playlists/UserPlaylistsFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "addPlaylist"

    invoke-virtual {v0, v1, v2}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 94
    const/4 v0, 0x1

    goto :goto_0

    .line 91
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 68
    const v1, 0x7f0901ca

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lru/cn/tv/playlists/UserPlaylistsFragment;->emptyTextView:Landroid/view/View;

    .line 69
    const v1, 0x7f090175

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lru/cn/tv/playlists/UserPlaylistsFragment;->progressView:Landroid/view/View;

    .line 70
    const v1, 0x7f0900b1

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 72
    .local v0, "empty":Landroid/view/View;
    const v1, 0x102000a

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lru/cn/tv/playlists/UserPlaylistsFragment;->list:Landroid/widget/ListView;

    .line 73
    iget-object v1, p0, Lru/cn/tv/playlists/UserPlaylistsFragment;->list:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 74
    iget-object v1, p0, Lru/cn/tv/playlists/UserPlaylistsFragment;->list:Landroid/widget/ListView;

    iget-object v2, p0, Lru/cn/tv/playlists/UserPlaylistsFragment;->mListItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 75
    iget-object v1, p0, Lru/cn/tv/playlists/UserPlaylistsFragment;->list:Landroid/widget/ListView;

    iget-object v2, p0, Lru/cn/tv/playlists/UserPlaylistsFragment;->adapter:Lru/cn/tv/playlists/UserPlaylistsAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 77
    iget-object v1, p0, Lru/cn/tv/playlists/UserPlaylistsFragment;->progressView:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 78
    iget-object v1, p0, Lru/cn/tv/playlists/UserPlaylistsFragment;->emptyTextView:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 79
    return-void
.end method
