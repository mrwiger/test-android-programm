.class public final Lcom/my/target/core/models/banners/h;
.super Lcom/my/target/core/models/banners/d;
.source "InterstitialAdPromoBanner.java"


# instance fields
.field private ctaButtonColor:I

.field private ctaButtonTextColor:I

.field private ctaButtonTouchColor:I

.field private footerColor:I

.field private final interstitialAdCards:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/e;",
            ">;"
        }
    .end annotation
.end field

.field private playIcon:Lcom/my/target/common/models/ImageData;

.field private storeIcon:Lcom/my/target/common/models/ImageData;

.field private style:I

.field private videoBanner:Lcom/my/target/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/my/target/aj",
            "<",
            "Lcom/my/target/common/models/VideoData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/my/target/core/models/banners/d;-><init>()V

    .line 36
    const v0, -0x999a

    iput v0, p0, Lcom/my/target/core/models/banners/h;->footerColor:I

    .line 37
    const v0, -0xff540e

    iput v0, p0, Lcom/my/target/core/models/banners/h;->ctaButtonColor:I

    .line 38
    const v0, -0xff8957

    iput v0, p0, Lcom/my/target/core/models/banners/h;->ctaButtonTouchColor:I

    .line 39
    const/4 v0, -0x1

    iput v0, p0, Lcom/my/target/core/models/banners/h;->ctaButtonTextColor:I

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/core/models/banners/h;->interstitialAdCards:Ljava/util/List;

    .line 45
    return-void
.end method

.method public static newBanner()Lcom/my/target/core/models/banners/h;
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/my/target/core/models/banners/h;

    invoke-direct {v0}, Lcom/my/target/core/models/banners/h;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final addInterstitialAdCard(Lcom/my/target/core/models/banners/e;)V
    .locals 1
    .param p1, "card"    # Lcom/my/target/core/models/banners/e;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/my/target/core/models/banners/h;->interstitialAdCards:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    return-void
.end method

.method public final getCtaButtonColor()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/my/target/core/models/banners/h;->ctaButtonColor:I

    return v0
.end method

.method public final getCtaButtonTextColor()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lcom/my/target/core/models/banners/h;->ctaButtonTextColor:I

    return v0
.end method

.method public final getCtaButtonTouchColor()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/my/target/core/models/banners/h;->ctaButtonTouchColor:I

    return v0
.end method

.method public final getFooterColor()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/my/target/core/models/banners/h;->footerColor:I

    return v0
.end method

.method public final getInterstitialAdCards()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lcom/my/target/core/models/banners/h;->interstitialAdCards:Ljava/util/List;

    return-object v0
.end method

.method public final getPlayIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/my/target/core/models/banners/h;->playIcon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public final getStoreIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/my/target/core/models/banners/h;->storeIcon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public final getStyle()I
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lcom/my/target/core/models/banners/h;->style:I

    return v0
.end method

.method public final getVideoBanner()Lcom/my/target/aj;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/my/target/aj",
            "<",
            "Lcom/my/target/common/models/VideoData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lcom/my/target/core/models/banners/h;->videoBanner:Lcom/my/target/aj;

    return-object v0
.end method

.method public final setCtaButtonColor(I)V
    .locals 0
    .param p1, "ctaButtonColor"    # I

    .prologue
    .line 84
    iput p1, p0, Lcom/my/target/core/models/banners/h;->ctaButtonColor:I

    .line 85
    return-void
.end method

.method public final setCtaButtonTextColor(I)V
    .locals 0
    .param p1, "ctaButtonTextColor"    # I

    .prologue
    .line 94
    iput p1, p0, Lcom/my/target/core/models/banners/h;->ctaButtonTextColor:I

    .line 95
    return-void
.end method

.method public final setCtaButtonTouchColor(I)V
    .locals 0
    .param p1, "ctaButtonTouchColor"    # I

    .prologue
    .line 89
    iput p1, p0, Lcom/my/target/core/models/banners/h;->ctaButtonTouchColor:I

    .line 90
    return-void
.end method

.method public final setFooterColor(I)V
    .locals 0
    .param p1, "footerColor"    # I

    .prologue
    .line 79
    iput p1, p0, Lcom/my/target/core/models/banners/h;->footerColor:I

    .line 80
    return-void
.end method

.method public final setPlayIcon(Lcom/my/target/common/models/ImageData;)V
    .locals 0
    .param p1, "playIcon"    # Lcom/my/target/common/models/ImageData;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/my/target/core/models/banners/h;->playIcon:Lcom/my/target/common/models/ImageData;

    .line 65
    return-void
.end method

.method public final setStoreIcon(Lcom/my/target/common/models/ImageData;)V
    .locals 0
    .param p1, "storeIcon"    # Lcom/my/target/common/models/ImageData;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/my/target/core/models/banners/h;->storeIcon:Lcom/my/target/common/models/ImageData;

    .line 75
    return-void
.end method

.method public final setStyle(I)V
    .locals 0
    .param p1, "style"    # I

    .prologue
    .line 135
    iput p1, p0, Lcom/my/target/core/models/banners/h;->style:I

    .line 136
    return-void
.end method

.method public final setVideoBanner(Lcom/my/target/aj;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/aj",
            "<",
            "Lcom/my/target/common/models/VideoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p1, "videoBanner":Lcom/my/target/aj;, "Lcom/my/target/aj<Lcom/my/target/common/models/VideoData;>;"
    iput-object p1, p0, Lcom/my/target/core/models/banners/h;->videoBanner:Lcom/my/target/aj;

    .line 50
    return-void
.end method
