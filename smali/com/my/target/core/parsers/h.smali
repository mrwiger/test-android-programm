.class public final Lcom/my/target/core/parsers/h;
.super Ljava/lang/Object;
.source "NativeAppwallAdBannerParser.java"


# instance fields
.field private final l:Lcom/my/target/be;

.field private final section:Lcom/my/target/fg;


# direct methods
.method private constructor <init>(Lcom/my/target/fg;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/my/target/core/parsers/h;->section:Lcom/my/target/fg;

    .line 41
    invoke-static {p1, p2, p3, p4}, Lcom/my/target/be;->a(Lcom/my/target/ak;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/be;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/parsers/h;->l:Lcom/my/target/be;

    .line 42
    return-void
.end method

.method public static a(Lcom/my/target/fg;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/h;
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/my/target/core/parsers/h;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/my/target/core/parsers/h;-><init>(Lcom/my/target/fg;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;Lcom/my/target/core/models/banners/i;)V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/my/target/core/parsers/h;->l:Lcom/my/target/be;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/my/target/be;->a(Lorg/json/JSONObject;Lcom/my/target/ah;Lcom/my/target/af;)V

    .line 47
    const-string v0, "hasNotification"

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/i;->isHasNotification()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/i;->setHasNotification(Z)V

    .line 48
    const-string v0, "Banner"

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/i;->isBanner()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/i;->setBanner(Z)V

    .line 49
    const-string v0, "RequireCategoryHighlight"

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/i;->isRequireCategoryHighlight()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/i;->setRequireCategoryHighlight(Z)V

    .line 50
    const-string v0, "ItemHighlight"

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/i;->isItemHighlight()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/i;->setItemHighlight(Z)V

    .line 51
    const-string v0, "Main"

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/i;->isMain()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/i;->setMain(Z)V

    .line 52
    const-string v0, "RequireWifi"

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/i;->isRequireWifi()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/i;->setRequireWifi(Z)V

    .line 53
    const-string v0, "subitem"

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/i;->isSubItem()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/i;->setSubItem(Z)V

    .line 55
    const-string v0, "bubble_id"

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/i;->getBubbleId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/i;->setBubbleId(Ljava/lang/String;)V

    .line 56
    const-string v0, "labelType"

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/i;->getLabelType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/i;->setLabelType(Ljava/lang/String;)V

    .line 57
    const-string v0, "status"

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/i;->getStatus()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/i;->setStatus(Ljava/lang/String;)V

    .line 58
    const-string v0, "paidType"

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/i;->getPaidType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/i;->setPaidType(Ljava/lang/String;)V

    .line 60
    const-string v0, "mrgs_id"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/i;->setMrgsId(I)V

    .line 61
    const-string v0, "coins"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/i;->setCoins(I)V

    .line 63
    const-string v0, "coins_icon_bgcolor"

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/i;->getCoinsIconBgColor()I

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/my/target/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/i;->setCoinsIconBgColor(I)V

    .line 64
    const-string v0, "coins_icon_textcolor"

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/i;->getCoinsIconTextColor()I

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/my/target/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/i;->setCoinsIconTextColor(I)V

    .line 66
    const-string v0, "icon_hd"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 67
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 69
    invoke-static {v0}, Lcom/my/target/common/models/ImageData;->newImageData(Ljava/lang/String;)Lcom/my/target/common/models/ImageData;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/i;->setIcon(Lcom/my/target/common/models/ImageData;)V

    .line 72
    :cond_0
    const-string v0, "coins_icon_hd"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 73
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 75
    invoke-static {v0}, Lcom/my/target/common/models/ImageData;->newImageData(Ljava/lang/String;)Lcom/my/target/common/models/ImageData;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/i;->setCoinsIcon(Lcom/my/target/common/models/ImageData;)V

    .line 78
    :cond_1
    const-string v0, "cross_notif_icon_hd"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 79
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 81
    invoke-static {v0}, Lcom/my/target/common/models/ImageData;->newImageData(Ljava/lang/String;)Lcom/my/target/common/models/ImageData;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/i;->setCrossNotifIcon(Lcom/my/target/common/models/ImageData;)V

    .line 84
    :cond_2
    iget-object v0, p0, Lcom/my/target/core/parsers/h;->section:Lcom/my/target/fg;

    invoke-virtual {v0}, Lcom/my/target/fg;->h()Ljava/lang/String;

    move-result-object v0

    .line 85
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 87
    invoke-static {v0}, Lcom/my/target/common/models/ImageData;->newImageData(Ljava/lang/String;)Lcom/my/target/common/models/ImageData;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/i;->setBubbleIcon(Lcom/my/target/common/models/ImageData;)V

    .line 90
    :cond_3
    iget-object v0, p0, Lcom/my/target/core/parsers/h;->section:Lcom/my/target/fg;

    invoke-virtual {v0}, Lcom/my/target/fg;->j()Ljava/lang/String;

    move-result-object v0

    .line 91
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 93
    invoke-static {v0}, Lcom/my/target/common/models/ImageData;->newImageData(Ljava/lang/String;)Lcom/my/target/common/models/ImageData;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/i;->setGotoAppIcon(Lcom/my/target/common/models/ImageData;)V

    .line 96
    :cond_4
    iget-object v0, p0, Lcom/my/target/core/parsers/h;->section:Lcom/my/target/fg;

    invoke-virtual {v0}, Lcom/my/target/fg;->i()Ljava/lang/String;

    move-result-object v0

    .line 97
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 99
    invoke-static {v0}, Lcom/my/target/common/models/ImageData;->newImageData(Ljava/lang/String;)Lcom/my/target/common/models/ImageData;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/i;->setLabelIcon(Lcom/my/target/common/models/ImageData;)V

    .line 102
    :cond_5
    invoke-virtual {p2}, Lcom/my/target/core/models/banners/i;->getStatus()Ljava/lang/String;

    move-result-object v0

    .line 104
    if-eqz v0, :cond_6

    .line 106
    iget-object v1, p0, Lcom/my/target/core/parsers/h;->section:Lcom/my/target/fg;

    invoke-virtual {v1, v0}, Lcom/my/target/fg;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 107
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 109
    invoke-static {v0}, Lcom/my/target/common/models/ImageData;->newImageData(Ljava/lang/String;)Lcom/my/target/common/models/ImageData;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/i;->setStatusIcon(Lcom/my/target/common/models/ImageData;)V

    .line 113
    :cond_6
    iget-object v0, p0, Lcom/my/target/core/parsers/h;->section:Lcom/my/target/fg;

    invoke-virtual {v0}, Lcom/my/target/fg;->k()Ljava/lang/String;

    move-result-object v0

    .line 114
    invoke-virtual {p2}, Lcom/my/target/core/models/banners/i;->isItemHighlight()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 116
    invoke-static {v0}, Lcom/my/target/common/models/ImageData;->newImageData(Ljava/lang/String;)Lcom/my/target/common/models/ImageData;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/i;->setItemHighlightIcon(Lcom/my/target/common/models/ImageData;)V

    .line 118
    :cond_7
    return-void
.end method
