.class public Lcom/annimon/stream/Optional;
.super Ljava/lang/Object;
.source "Optional.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final EMPTY:Lcom/annimon/stream/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/annimon/stream/Optional",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final value:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/annimon/stream/Optional;

    invoke-direct {v0}, Lcom/annimon/stream/Optional;-><init>()V

    sput-object v0, Lcom/annimon/stream/Optional;->EMPTY:Lcom/annimon/stream/Optional;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 59
    .local p0, "this":Lcom/annimon/stream/Optional;, "Lcom/annimon/stream/Optional<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/annimon/stream/Optional;->value:Ljava/lang/Object;

    .line 61
    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 63
    .local p0, "this":Lcom/annimon/stream/Optional;, "Lcom/annimon/stream/Optional<TT;>;"
    .local p1, "value":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    invoke-static {p1}, Lcom/annimon/stream/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/annimon/stream/Optional;->value:Ljava/lang/Object;

    .line 65
    return-void
.end method

.method public static empty()Lcom/annimon/stream/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/annimon/stream/Optional",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 54
    sget-object v0, Lcom/annimon/stream/Optional;->EMPTY:Lcom/annimon/stream/Optional;

    return-object v0
.end method

.method public static of(Ljava/lang/Object;)Lcom/annimon/stream/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/annimon/stream/Optional",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "value":Ljava/lang/Object;, "TT;"
    new-instance v0, Lcom/annimon/stream/Optional;

    invoke-direct {v0, p0}, Lcom/annimon/stream/Optional;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static ofNullable(Ljava/lang/Object;)Lcom/annimon/stream/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/annimon/stream/Optional",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 43
    .local p0, "value":Ljava/lang/Object;, "TT;"
    if-nez p0, :cond_0

    invoke-static {}, Lcom/annimon/stream/Optional;->empty()Lcom/annimon/stream/Optional;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/annimon/stream/Optional;->of(Ljava/lang/Object;)Lcom/annimon/stream/Optional;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 329
    .local p0, "this":Lcom/annimon/stream/Optional;, "Lcom/annimon/stream/Optional<TT;>;"
    if-ne p0, p1, :cond_0

    .line 330
    const/4 v1, 0x1

    .line 338
    :goto_0
    return v1

    .line 333
    :cond_0
    instance-of v1, p1, Lcom/annimon/stream/Optional;

    if-nez v1, :cond_1

    .line 334
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 337
    check-cast v0, Lcom/annimon/stream/Optional;

    .line 338
    .local v0, "other":Lcom/annimon/stream/Optional;, "Lcom/annimon/stream/Optional<*>;"
    iget-object v1, p0, Lcom/annimon/stream/Optional;->value:Ljava/lang/Object;

    iget-object v2, v0, Lcom/annimon/stream/Optional;->value:Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/annimon/stream/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public get()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 74
    .local p0, "this":Lcom/annimon/stream/Optional;, "Lcom/annimon/stream/Optional<TT;>;"
    iget-object v0, p0, Lcom/annimon/stream/Optional;->value:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 75
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "No value present"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/annimon/stream/Optional;->value:Ljava/lang/Object;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 343
    .local p0, "this":Lcom/annimon/stream/Optional;, "Lcom/annimon/stream/Optional<TT;>;"
    iget-object v0, p0, Lcom/annimon/stream/Optional;->value:Ljava/lang/Object;

    invoke-static {v0}, Lcom/annimon/stream/Objects;->hashCode(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isPresent()Z
    .locals 1

    .prologue
    .line 86
    .local p0, "this":Lcom/annimon/stream/Optional;, "Lcom/annimon/stream/Optional<TT;>;"
    iget-object v0, p0, Lcom/annimon/stream/Optional;->value:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public orElse(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TT;"
        }
    .end annotation

    .prologue
    .line 301
    .local p0, "this":Lcom/annimon/stream/Optional;, "Lcom/annimon/stream/Optional<TT;>;"
    .local p1, "other":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lcom/annimon/stream/Optional;->value:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/annimon/stream/Optional;->value:Ljava/lang/Object;

    .end local p1    # "other":Ljava/lang/Object;, "TT;"
    :cond_0
    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 348
    .local p0, "this":Lcom/annimon/stream/Optional;, "Lcom/annimon/stream/Optional<TT;>;"
    iget-object v0, p0, Lcom/annimon/stream/Optional;->value:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const-string v0, "Optional[%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/annimon/stream/Optional;->value:Ljava/lang/Object;

    aput-object v3, v1, v2

    .line 349
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 348
    :goto_0
    return-object v0

    .line 349
    :cond_0
    const-string v0, "Optional.empty"

    goto :goto_0
.end method
