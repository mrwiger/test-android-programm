.class public Ltoothpick/configuration/Configuration;
.super Ljava/lang/Object;
.source "Configuration.java"

# interfaces
.implements Ltoothpick/configuration/MultipleRootScopeCheckConfiguration;
.implements Ltoothpick/configuration/ReflectionConfiguration;
.implements Ltoothpick/configuration/RuntimeCheckConfiguration;


# instance fields
.field private multipleRootScopeCheckConfiguration:Ltoothpick/configuration/MultipleRootScopeCheckConfiguration;

.field private reflectionConfiguration:Ltoothpick/configuration/ReflectionConfiguration;

.field private runtimeCheckConfiguration:Ltoothpick/configuration/RuntimeCheckConfiguration;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ltoothpick/configuration/ReflectionOnConfiguration;

    invoke-direct {v0}, Ltoothpick/configuration/ReflectionOnConfiguration;-><init>()V

    iput-object v0, p0, Ltoothpick/configuration/Configuration;->reflectionConfiguration:Ltoothpick/configuration/ReflectionConfiguration;

    .line 18
    new-instance v0, Ltoothpick/configuration/RuntimeCheckOffConfiguration;

    invoke-direct {v0}, Ltoothpick/configuration/RuntimeCheckOffConfiguration;-><init>()V

    iput-object v0, p0, Ltoothpick/configuration/Configuration;->runtimeCheckConfiguration:Ltoothpick/configuration/RuntimeCheckConfiguration;

    .line 19
    new-instance v0, Ltoothpick/configuration/MultipleRootScopeCheckOffConfiguration;

    invoke-direct {v0}, Ltoothpick/configuration/MultipleRootScopeCheckOffConfiguration;-><init>()V

    iput-object v0, p0, Ltoothpick/configuration/Configuration;->multipleRootScopeCheckConfiguration:Ltoothpick/configuration/MultipleRootScopeCheckConfiguration;

    return-void
.end method

.method public static forDevelopment()Ltoothpick/configuration/Configuration;
    .locals 2

    .prologue
    .line 38
    new-instance v0, Ltoothpick/configuration/Configuration;

    invoke-direct {v0}, Ltoothpick/configuration/Configuration;-><init>()V

    .line 39
    .local v0, "configuration":Ltoothpick/configuration/Configuration;
    new-instance v1, Ltoothpick/configuration/RuntimeCheckOnConfiguration;

    invoke-direct {v1}, Ltoothpick/configuration/RuntimeCheckOnConfiguration;-><init>()V

    iput-object v1, v0, Ltoothpick/configuration/Configuration;->runtimeCheckConfiguration:Ltoothpick/configuration/RuntimeCheckConfiguration;

    .line 40
    return-object v0
.end method

.method public static forProduction()Ltoothpick/configuration/Configuration;
    .locals 1

    .prologue
    .line 51
    new-instance v0, Ltoothpick/configuration/Configuration;

    invoke-direct {v0}, Ltoothpick/configuration/Configuration;-><init>()V

    return-object v0
.end method


# virtual methods
.method public allowMultipleRootScopes()Ltoothpick/configuration/Configuration;
    .locals 1

    .prologue
    .line 85
    new-instance v0, Ltoothpick/configuration/MultipleRootScopeCheckOffConfiguration;

    invoke-direct {v0}, Ltoothpick/configuration/MultipleRootScopeCheckOffConfiguration;-><init>()V

    iput-object v0, p0, Ltoothpick/configuration/Configuration;->multipleRootScopeCheckConfiguration:Ltoothpick/configuration/MultipleRootScopeCheckConfiguration;

    .line 86
    return-object p0
.end method

.method public checkCyclesEnd(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 1
    .param p1, "clazz"    # Ljava/lang/Class;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 109
    iget-object v0, p0, Ltoothpick/configuration/Configuration;->runtimeCheckConfiguration:Ltoothpick/configuration/RuntimeCheckConfiguration;

    invoke-interface {v0, p1, p2}, Ltoothpick/configuration/RuntimeCheckConfiguration;->checkCyclesEnd(Ljava/lang/Class;Ljava/lang/String;)V

    .line 110
    return-void
.end method

.method public checkCyclesStart(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 1
    .param p1, "clazz"    # Ljava/lang/Class;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 105
    iget-object v0, p0, Ltoothpick/configuration/Configuration;->runtimeCheckConfiguration:Ltoothpick/configuration/RuntimeCheckConfiguration;

    invoke-interface {v0, p1, p2}, Ltoothpick/configuration/RuntimeCheckConfiguration;->checkCyclesStart(Ljava/lang/Class;Ljava/lang/String;)V

    .line 106
    return-void
.end method

.method public checkIllegalBinding(Ltoothpick/config/Binding;Ltoothpick/Scope;)V
    .locals 1
    .param p1, "binding"    # Ltoothpick/config/Binding;
    .param p2, "scope"    # Ltoothpick/Scope;

    .prologue
    .line 101
    iget-object v0, p0, Ltoothpick/configuration/Configuration;->runtimeCheckConfiguration:Ltoothpick/configuration/RuntimeCheckConfiguration;

    invoke-interface {v0, p1, p2}, Ltoothpick/configuration/RuntimeCheckConfiguration;->checkIllegalBinding(Ltoothpick/config/Binding;Ltoothpick/Scope;)V

    .line 102
    return-void
.end method

.method public checkMultipleRootScopes(Ltoothpick/Scope;)V
    .locals 1
    .param p1, "scope"    # Ltoothpick/Scope;

    .prologue
    .line 121
    iget-object v0, p0, Ltoothpick/configuration/Configuration;->multipleRootScopeCheckConfiguration:Ltoothpick/configuration/MultipleRootScopeCheckConfiguration;

    invoke-interface {v0, p1}, Ltoothpick/configuration/MultipleRootScopeCheckConfiguration;->checkMultipleRootScopes(Ltoothpick/Scope;)V

    .line 122
    return-void
.end method

.method public disableReflection()Ltoothpick/configuration/Configuration;
    .locals 1

    .prologue
    .line 76
    new-instance v0, Ltoothpick/configuration/ReflectionOffConfiguration;

    invoke-direct {v0}, Ltoothpick/configuration/ReflectionOffConfiguration;-><init>()V

    iput-object v0, p0, Ltoothpick/configuration/Configuration;->reflectionConfiguration:Ltoothpick/configuration/ReflectionConfiguration;

    .line 77
    return-object p0
.end method

.method public enableReflection()Ltoothpick/configuration/Configuration;
    .locals 1

    .prologue
    .line 63
    new-instance v0, Ltoothpick/configuration/ReflectionOnConfiguration;

    invoke-direct {v0}, Ltoothpick/configuration/ReflectionOnConfiguration;-><init>()V

    iput-object v0, p0, Ltoothpick/configuration/Configuration;->reflectionConfiguration:Ltoothpick/configuration/ReflectionConfiguration;

    .line 64
    return-object p0
.end method

.method public getFactory(Ljava/lang/Class;)Ltoothpick/Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ltoothpick/Factory",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 113
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    iget-object v0, p0, Ltoothpick/configuration/Configuration;->reflectionConfiguration:Ltoothpick/configuration/ReflectionConfiguration;

    invoke-interface {v0, p1}, Ltoothpick/configuration/ReflectionConfiguration;->getFactory(Ljava/lang/Class;)Ltoothpick/Factory;

    move-result-object v0

    return-object v0
.end method

.method public getMemberInjector(Ljava/lang/Class;)Ltoothpick/MemberInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ltoothpick/MemberInjector",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 117
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    iget-object v0, p0, Ltoothpick/configuration/Configuration;->reflectionConfiguration:Ltoothpick/configuration/ReflectionConfiguration;

    invoke-interface {v0, p1}, Ltoothpick/configuration/ReflectionConfiguration;->getMemberInjector(Ljava/lang/Class;)Ltoothpick/MemberInjector;

    move-result-object v0

    return-object v0
.end method

.method public onScopeForestReset()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Ltoothpick/configuration/Configuration;->multipleRootScopeCheckConfiguration:Ltoothpick/configuration/MultipleRootScopeCheckConfiguration;

    invoke-interface {v0}, Ltoothpick/configuration/MultipleRootScopeCheckConfiguration;->onScopeForestReset()V

    .line 126
    return-void
.end method

.method public preventMultipleRootScopes()Ltoothpick/configuration/Configuration;
    .locals 1

    .prologue
    .line 96
    new-instance v0, Ltoothpick/configuration/MultipleRootScopeCheckOnConfiguration;

    invoke-direct {v0}, Ltoothpick/configuration/MultipleRootScopeCheckOnConfiguration;-><init>()V

    iput-object v0, p0, Ltoothpick/configuration/Configuration;->multipleRootScopeCheckConfiguration:Ltoothpick/configuration/MultipleRootScopeCheckConfiguration;

    .line 97
    return-object p0
.end method
