.class public Lru/cn/view/OneTwoFragmentLayout;
.super Landroid/widget/RelativeLayout;
.source "OneTwoFragmentLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/view/OneTwoFragmentLayout$Mode;
    }
.end annotation


# instance fields
.field private currentPage:I

.field private firstView:Landroid/widget/FrameLayout;

.field private firstViewId:I

.field private fm:Landroid/support/v4/app/FragmentManager;

.field private mode:Lru/cn/view/OneTwoFragmentLayout$Mode;

.field private secondView:Landroid/widget/FrameLayout;

.field private secondViewId:I

.field private shadowDrawable:Landroid/graphics/drawable/Drawable;

.field private showSecondFragment:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 85
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    const/4 v0, 0x1

    iput v0, p0, Lru/cn/view/OneTwoFragmentLayout;->firstViewId:I

    .line 18
    const/4 v0, 0x2

    iput v0, p0, Lru/cn/view/OneTwoFragmentLayout;->secondViewId:I

    .line 25
    iput-boolean v1, p0, Lru/cn/view/OneTwoFragmentLayout;->showSecondFragment:Z

    .line 37
    sget-object v0, Lru/cn/view/OneTwoFragmentLayout$Mode;->MODE_1:Lru/cn/view/OneTwoFragmentLayout$Mode;

    iput-object v0, p0, Lru/cn/view/OneTwoFragmentLayout;->mode:Lru/cn/view/OneTwoFragmentLayout$Mode;

    .line 49
    iput v1, p0, Lru/cn/view/OneTwoFragmentLayout;->currentPage:I

    .line 86
    return-void
.end method

.method private addFragment(ILandroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "f"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 81
    iget-object v0, p0, Lru/cn/view/OneTwoFragmentLayout;->fm:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 82
    return-void
.end method

.method private createAndAddView(I)Landroid/widget/FrameLayout;
    .locals 4
    .param p1, "id"    # I

    .prologue
    const/4 v3, -0x1

    .line 89
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lru/cn/view/OneTwoFragmentLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 90
    .local v1, "v":Landroid/widget/FrameLayout;
    invoke-virtual {v1, p1}, Landroid/widget/FrameLayout;->setId(I)V

    .line 91
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 94
    .local v0, "p":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 95
    invoke-virtual {p0, v1}, Lru/cn/view/OneTwoFragmentLayout;->addView(Landroid/view/View;)V

    .line 96
    return-object v1
.end method

.method private fixShadow()V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lru/cn/view/OneTwoFragmentLayout;->firstView:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lru/cn/view/OneTwoFragmentLayout;->mode:Lru/cn/view/OneTwoFragmentLayout$Mode;

    sget-object v1, Lru/cn/view/OneTwoFragmentLayout$Mode;->MODE_2:Lru/cn/view/OneTwoFragmentLayout$Mode;

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lru/cn/view/OneTwoFragmentLayout;->showSecondFragment:Z

    if-eqz v0, :cond_1

    .line 117
    iget-object v0, p0, Lru/cn/view/OneTwoFragmentLayout;->firstView:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lru/cn/view/OneTwoFragmentLayout;->shadowDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    iget-object v0, p0, Lru/cn/view/OneTwoFragmentLayout;->firstView:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method public addFirstFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1, "f"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 68
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lru/cn/view/OneTwoFragmentLayout;->addFirstFragment(Landroid/support/v4/app/Fragment;I)V

    .line 69
    return-void
.end method

.method public addFirstFragment(Landroid/support/v4/app/Fragment;I)V
    .locals 1
    .param p1, "f"    # Landroid/support/v4/app/Fragment;
    .param p2, "bgColor"    # I

    .prologue
    .line 72
    iget-object v0, p0, Lru/cn/view/OneTwoFragmentLayout;->firstView:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p2}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 73
    iget v0, p0, Lru/cn/view/OneTwoFragmentLayout;->firstViewId:I

    invoke-direct {p0, v0, p1}, Lru/cn/view/OneTwoFragmentLayout;->addFragment(ILandroid/support/v4/app/Fragment;)V

    .line 74
    return-void
.end method

.method public addSecondFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1, "f"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 77
    iget v0, p0, Lru/cn/view/OneTwoFragmentLayout;->secondViewId:I

    invoke-direct {p0, v0, p1}, Lru/cn/view/OneTwoFragmentLayout;->addFragment(ILandroid/support/v4/app/Fragment;)V

    .line 78
    return-void
.end method

.method public getCurrentPage()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lru/cn/view/OneTwoFragmentLayout;->currentPage:I

    return v0
.end method

.method public getMode()Lru/cn/view/OneTwoFragmentLayout$Mode;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lru/cn/view/OneTwoFragmentLayout;->mode:Lru/cn/view/OneTwoFragmentLayout$Mode;

    return-object v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 126
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 127
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 101
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 102
    invoke-virtual {p0}, Lru/cn/view/OneTwoFragmentLayout;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    :goto_0
    return-void

    .line 105
    :cond_0
    iget v0, p0, Lru/cn/view/OneTwoFragmentLayout;->firstViewId:I

    invoke-direct {p0, v0}, Lru/cn/view/OneTwoFragmentLayout;->createAndAddView(I)Landroid/widget/FrameLayout;

    move-result-object v0

    iput-object v0, p0, Lru/cn/view/OneTwoFragmentLayout;->firstView:Landroid/widget/FrameLayout;

    .line 106
    iget v0, p0, Lru/cn/view/OneTwoFragmentLayout;->secondViewId:I

    invoke-direct {p0, v0}, Lru/cn/view/OneTwoFragmentLayout;->createAndAddView(I)Landroid/widget/FrameLayout;

    move-result-object v0

    iput-object v0, p0, Lru/cn/view/OneTwoFragmentLayout;->secondView:Landroid/widget/FrameLayout;

    .line 108
    invoke-virtual {p0}, Lru/cn/view/OneTwoFragmentLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080305

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lru/cn/view/OneTwoFragmentLayout;->shadowDrawable:Landroid/graphics/drawable/Drawable;

    .line 109
    iget-object v0, p0, Lru/cn/view/OneTwoFragmentLayout;->firstView:Landroid/widget/FrameLayout;

    const/16 v1, 0x75

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setForegroundGravity(I)V

    .line 111
    invoke-direct {p0}, Lru/cn/view/OneTwoFragmentLayout;->fixShadow()V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 10
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    .line 132
    invoke-virtual {p0}, Lru/cn/view/OneTwoFragmentLayout;->isInEditMode()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 133
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 171
    :goto_0
    return-void

    .line 137
    :cond_0
    invoke-virtual {p0}, Lru/cn/view/OneTwoFragmentLayout;->getMeasuredWidth()I

    move-result v3

    .line 138
    .local v3, "parentW":I
    invoke-virtual {p0}, Lru/cn/view/OneTwoFragmentLayout;->getMeasuredHeight()I

    move-result v2

    .line 141
    .local v2, "parentH":I
    iget-object v6, p0, Lru/cn/view/OneTwoFragmentLayout;->mode:Lru/cn/view/OneTwoFragmentLayout$Mode;

    sget-object v7, Lru/cn/view/OneTwoFragmentLayout$Mode;->MODE_1:Lru/cn/view/OneTwoFragmentLayout$Mode;

    if-eq v6, v7, :cond_1

    iget-boolean v6, p0, Lru/cn/view/OneTwoFragmentLayout;->showSecondFragment:Z

    if-nez v6, :cond_2

    .line 142
    :cond_1
    iget v6, p0, Lru/cn/view/OneTwoFragmentLayout;->currentPage:I

    mul-int/2addr v6, v3

    rsub-int/lit8 v1, v6, 0x0

    .line 143
    .local v1, "l":I
    const/4 v5, 0x0

    .line 144
    .local v5, "t":I
    add-int v4, v1, v3

    .line 145
    .local v4, "r":I
    move v0, v2

    .line 146
    .local v0, "b":I
    iget-object v6, p0, Lru/cn/view/OneTwoFragmentLayout;->firstView:Landroid/widget/FrameLayout;

    sub-int v7, v4, v1

    or-int/2addr v7, v9

    sub-int v8, v0, v5

    or-int/2addr v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/widget/FrameLayout;->measure(II)V

    .line 148
    iget-object v6, p0, Lru/cn/view/OneTwoFragmentLayout;->firstView:Landroid/widget/FrameLayout;

    invoke-virtual {v6, v1, v5, v4, v0}, Landroid/widget/FrameLayout;->layout(IIII)V

    .line 150
    move v1, v4

    .line 151
    move v4, v3

    .line 152
    iget-object v6, p0, Lru/cn/view/OneTwoFragmentLayout;->secondView:Landroid/widget/FrameLayout;

    sub-int v7, v4, v1

    or-int/2addr v7, v9

    sub-int v8, v0, v5

    or-int/2addr v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/widget/FrameLayout;->measure(II)V

    .line 154
    iget-object v6, p0, Lru/cn/view/OneTwoFragmentLayout;->secondView:Landroid/widget/FrameLayout;

    invoke-virtual {v6, v1, v5, v4, v0}, Landroid/widget/FrameLayout;->layout(IIII)V

    goto :goto_0

    .line 156
    .end local v0    # "b":I
    .end local v1    # "l":I
    .end local v4    # "r":I
    .end local v5    # "t":I
    :cond_2
    const/4 v1, 0x0

    .line 157
    .restart local v1    # "l":I
    const/4 v5, 0x0

    .line 158
    .restart local v5    # "t":I
    div-int/lit8 v4, v3, 0x2

    .line 159
    .restart local v4    # "r":I
    move v0, v2

    .line 160
    .restart local v0    # "b":I
    iget-object v6, p0, Lru/cn/view/OneTwoFragmentLayout;->firstView:Landroid/widget/FrameLayout;

    sub-int v7, v4, v1

    or-int/2addr v7, v9

    sub-int v8, v0, v5

    or-int/2addr v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/widget/FrameLayout;->measure(II)V

    .line 162
    iget-object v6, p0, Lru/cn/view/OneTwoFragmentLayout;->firstView:Landroid/widget/FrameLayout;

    invoke-virtual {v6, v1, v5, v4, v0}, Landroid/widget/FrameLayout;->layout(IIII)V

    .line 164
    move v1, v4

    .line 165
    move v4, v3

    .line 166
    iget-object v6, p0, Lru/cn/view/OneTwoFragmentLayout;->secondView:Landroid/widget/FrameLayout;

    sub-int v7, v4, v1

    or-int/2addr v7, v9

    sub-int v8, v0, v5

    or-int/2addr v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/widget/FrameLayout;->measure(II)V

    .line 168
    iget-object v6, p0, Lru/cn/view/OneTwoFragmentLayout;->secondView:Landroid/widget/FrameLayout;

    invoke-virtual {v6, v1, v5, v4, v0}, Landroid/widget/FrameLayout;->layout(IIII)V

    goto :goto_0
.end method

.method public setCurrentPage(I)V
    .locals 0
    .param p1, "page"    # I

    .prologue
    .line 52
    iput p1, p0, Lru/cn/view/OneTwoFragmentLayout;->currentPage:I

    .line 53
    invoke-direct {p0}, Lru/cn/view/OneTwoFragmentLayout;->fixShadow()V

    .line 54
    invoke-virtual {p0}, Lru/cn/view/OneTwoFragmentLayout;->requestLayout()V

    .line 55
    return-void
.end method

.method public setFragmentManager(Landroid/support/v4/app/FragmentManager;)V
    .locals 0
    .param p1, "fm"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 64
    iput-object p1, p0, Lru/cn/view/OneTwoFragmentLayout;->fm:Landroid/support/v4/app/FragmentManager;

    .line 65
    return-void
.end method

.method public setMode(Lru/cn/view/OneTwoFragmentLayout$Mode;)V
    .locals 0
    .param p1, "mode"    # Lru/cn/view/OneTwoFragmentLayout$Mode;

    .prologue
    .line 40
    iput-object p1, p0, Lru/cn/view/OneTwoFragmentLayout;->mode:Lru/cn/view/OneTwoFragmentLayout$Mode;

    .line 41
    invoke-direct {p0}, Lru/cn/view/OneTwoFragmentLayout;->fixShadow()V

    .line 42
    invoke-virtual {p0}, Lru/cn/view/OneTwoFragmentLayout;->requestLayout()V

    .line 43
    return-void
.end method

.method public showSecondFragment(Z)V
    .locals 0
    .param p1, "show"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lru/cn/view/OneTwoFragmentLayout;->showSecondFragment:Z

    .line 29
    invoke-direct {p0}, Lru/cn/view/OneTwoFragmentLayout;->fixShadow()V

    .line 30
    invoke-virtual {p0}, Lru/cn/view/OneTwoFragmentLayout;->requestLayout()V

    .line 31
    return-void
.end method
