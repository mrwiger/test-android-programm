.class Lru/cn/domain/statistics/inetra/TrackingSender$2;
.super Ljava/lang/Object;
.source "TrackingSender.java"

# interfaces
.implements Lokhttp3/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/domain/statistics/inetra/TrackingSender;->dispatchEvents(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/domain/statistics/inetra/TrackingSender;

.field final synthetic val$event:Lru/cn/domain/statistics/inetra/TrackingEvent;

.field final synthetic val$requestURL:Ljava/lang/String;


# direct methods
.method constructor <init>(Lru/cn/domain/statistics/inetra/TrackingSender;Lru/cn/domain/statistics/inetra/TrackingEvent;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/domain/statistics/inetra/TrackingSender;

    .prologue
    .line 186
    iput-object p1, p0, Lru/cn/domain/statistics/inetra/TrackingSender$2;->this$0:Lru/cn/domain/statistics/inetra/TrackingSender;

    iput-object p2, p0, Lru/cn/domain/statistics/inetra/TrackingSender$2;->val$event:Lru/cn/domain/statistics/inetra/TrackingEvent;

    iput-object p3, p0, Lru/cn/domain/statistics/inetra/TrackingSender$2;->val$requestURL:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lokhttp3/Call;Ljava/io/IOException;)V
    .locals 4
    .param p1, "call"    # Lokhttp3/Call;
    .param p2, "e"    # Ljava/io/IOException;

    .prologue
    .line 189
    const-string v0, "TrackingApi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lru/cn/domain/statistics/inetra/TrackingSender$2;->val$event:Lru/cn/domain/statistics/inetra/TrackingEvent;

    iget-wide v2, v2, Lru/cn/domain/statistics/inetra/TrackingEvent;->id:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] Request failed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lru/cn/domain/statistics/inetra/TrackingSender$2;->val$requestURL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    iget-object v0, p0, Lru/cn/domain/statistics/inetra/TrackingSender$2;->val$event:Lru/cn/domain/statistics/inetra/TrackingEvent;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lru/cn/domain/statistics/inetra/TrackingEvent;->onFly:Z

    .line 191
    iget-object v0, p0, Lru/cn/domain/statistics/inetra/TrackingSender$2;->this$0:Lru/cn/domain/statistics/inetra/TrackingSender;

    iget-object v1, p0, Lru/cn/domain/statistics/inetra/TrackingSender$2;->val$event:Lru/cn/domain/statistics/inetra/TrackingEvent;

    invoke-virtual {v0, v1}, Lru/cn/domain/statistics/inetra/TrackingSender;->sendEvent(Lru/cn/domain/statistics/inetra/TrackingEvent;)V

    .line 192
    return-void
.end method

.method public onResponse(Lokhttp3/Call;Lokhttp3/Response;)V
    .locals 4
    .param p1, "call"    # Lokhttp3/Call;
    .param p2, "response"    # Lokhttp3/Response;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 196
    const-string v0, "TrackingApi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lru/cn/domain/statistics/inetra/TrackingSender$2;->val$event:Lru/cn/domain/statistics/inetra/TrackingEvent;

    iget-wide v2, v2, Lru/cn/domain/statistics/inetra/TrackingEvent;->id:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] Request completed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lru/cn/domain/statistics/inetra/TrackingSender$2;->val$requestURL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    invoke-virtual {p2}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->close()V

    .line 198
    iget-object v0, p0, Lru/cn/domain/statistics/inetra/TrackingSender$2;->this$0:Lru/cn/domain/statistics/inetra/TrackingSender;

    invoke-static {v0}, Lru/cn/domain/statistics/inetra/TrackingSender;->access$000(Lru/cn/domain/statistics/inetra/TrackingSender;)Lru/cn/domain/statistics/inetra/EventsStorage;

    move-result-object v0

    iget-object v1, p0, Lru/cn/domain/statistics/inetra/TrackingSender$2;->val$event:Lru/cn/domain/statistics/inetra/TrackingEvent;

    invoke-interface {v0, v1}, Lru/cn/domain/statistics/inetra/EventsStorage;->removeEvent(Lru/cn/domain/statistics/inetra/TrackingEvent;)V

    .line 199
    return-void
.end method
