.class final Lru/cn/tv/player/PlaybackViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "PlaybackViewModel.java"


# instance fields
.field private final channel:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final channelLocations:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;>;"
        }
    .end annotation
.end field

.field private final currentTelecast:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final loader:Lru/cn/mvvm/RxLoader;

.field private final telecast:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final telecastLocations:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lru/cn/mvvm/RxLoader;)V
    .locals 1
    .param p1, "loader"    # Lru/cn/mvvm/RxLoader;

    .prologue
    .line 38
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 39
    iput-object p1, p0, Lru/cn/tv/player/PlaybackViewModel;->loader:Lru/cn/mvvm/RxLoader;

    .line 40
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/player/PlaybackViewModel;->channel:Landroid/arch/lifecycle/MutableLiveData;

    .line 41
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/player/PlaybackViewModel;->currentTelecast:Landroid/arch/lifecycle/MutableLiveData;

    .line 42
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/player/PlaybackViewModel;->channelLocations:Landroid/arch/lifecycle/MutableLiveData;

    .line 43
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/player/PlaybackViewModel;->telecast:Landroid/arch/lifecycle/MutableLiveData;

    .line 44
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/player/PlaybackViewModel;->telecastLocations:Landroid/arch/lifecycle/MutableLiveData;

    .line 45
    return-void
.end method

.method private channel(J)Lio/reactivex/Observable;
    .locals 3
    .param p1, "channelId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lio/reactivex/Observable",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    invoke-static {p1, p2}, Lru/cn/api/provider/TvContentProviderContract;->channel(J)Landroid/net/Uri;

    move-result-object v0

    .line 109
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/player/PlaybackViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v1

    .line 110
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 109
    return-object v1
.end method

.method private channelLocations(J)Lio/reactivex/Observable;
    .locals 3
    .param p1, "channelId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lio/reactivex/Observable",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    invoke-static {p1, p2}, Lru/cn/api/provider/TvContentProviderContract;->channelLocations(J)Landroid/net/Uri;

    move-result-object v0

    .line 116
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/player/PlaybackViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v1

    .line 117
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 116
    return-object v1
.end method

.method private currentTelecast(JJ)Lio/reactivex/Observable;
    .locals 3
    .param p1, "channelId"    # J
    .param p3, "time"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lio/reactivex/Observable",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    invoke-static {p1, p2, p3, p4}, Lru/cn/api/provider/TvContentProviderContract;->telecastByTime(JJ)Landroid/net/Uri;

    move-result-object v0

    .line 199
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/player/PlaybackViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v1

    .line 200
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 199
    return-object v1
.end method

.method private mapChannelLocations(Landroid/database/Cursor;)Ljava/util/List;
    .locals 18
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 123
    .local v11, "locations":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/iptv/replies/MediaLocation;>;"
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v14

    if-lez v14, :cond_3

    .line 124
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 125
    :goto_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v14

    if-nez v14, :cond_3

    .line 126
    const-string v14, "_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 127
    .local v4, "channelId":J
    const-string v14, "title"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 128
    .local v10, "locationTitle":Ljava/lang/String;
    const-string v14, "location"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 129
    .local v8, "l":Ljava/lang/String;
    const-string v14, "has_timeshift"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 130
    .local v12, "timeshift":I
    const-string v14, "crop_x"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 131
    .local v6, "cropX":I
    const-string v14, "crop_y"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 133
    .local v7, "cropY":I
    const-string v14, "aspect_w"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    .line 134
    .local v3, "aspectW":F
    const-string v14, "aspect_h"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    .line 136
    .local v2, "aspectH":F
    new-instance v9, Lru/cn/api/iptv/replies/MediaLocation;

    invoke-direct {v9, v8}, Lru/cn/api/iptv/replies/MediaLocation;-><init>(Ljava/lang/String;)V

    .line 137
    .local v9, "location":Lru/cn/api/iptv/replies/MediaLocation;
    iput-wide v4, v9, Lru/cn/api/iptv/replies/MediaLocation;->channelId:J

    .line 138
    iput-object v10, v9, Lru/cn/api/iptv/replies/MediaLocation;->title:Ljava/lang/String;

    .line 139
    const/4 v14, 0x1

    if-ne v12, v14, :cond_0

    const/4 v14, 0x1

    :goto_1
    iput-boolean v14, v9, Lru/cn/api/iptv/replies/MediaLocation;->hasTimeshift:Z

    .line 141
    iput v6, v9, Lru/cn/api/iptv/replies/MediaLocation;->cropX:I

    .line 142
    iput v7, v9, Lru/cn/api/iptv/replies/MediaLocation;->cropY:I

    .line 144
    const-string v14, "stream_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    iput-wide v14, v9, Lru/cn/api/iptv/replies/MediaLocation;->streamId:J

    .line 145
    const-string v14, "territory_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    iput-wide v14, v9, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    .line 146
    const-string v14, "contractor_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    iput-wide v14, v9, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    .line 148
    iput v3, v9, Lru/cn/api/iptv/replies/MediaLocation;->aspectW:F

    .line 149
    iput v2, v9, Lru/cn/api/iptv/replies/MediaLocation;->aspectH:F

    .line 150
    const-string v14, "source_uri"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v9, Lru/cn/api/iptv/replies/MediaLocation;->sourceUri:Ljava/lang/String;

    .line 151
    const-string v14, "access"

    .line 152
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lru/cn/api/iptv/replies/MediaLocation$Access;->valueOf(Ljava/lang/String;)Lru/cn/api/iptv/replies/MediaLocation$Access;

    move-result-object v14

    iput-object v14, v9, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;

    .line 154
    const-string v14, "allowed-till"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    iput v14, v9, Lru/cn/api/iptv/replies/MediaLocation;->allowedTill:I

    .line 155
    const-string v14, "no_ads_partner"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_1

    const/4 v14, 0x1

    :goto_2
    iput-boolean v14, v9, Lru/cn/api/iptv/replies/MediaLocation;->noAdsPartner:Z

    .line 156
    invoke-direct/range {p0 .. p1}, Lru/cn/tv/player/PlaybackViewModel;->parseAdTags(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v14

    iput-object v14, v9, Lru/cn/api/iptv/replies/MediaLocation;->adTags:Ljava/util/List;

    .line 158
    const-string v14, "timezone"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 159
    .local v13, "timezoneValue":Ljava/lang/String;
    if-eqz v13, :cond_2

    invoke-static {v13}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v14

    :goto_3
    iput-object v14, v9, Lru/cn/api/iptv/replies/MediaLocation;->timezoneOffset:Ljava/lang/Long;

    .line 161
    invoke-interface {v11, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    const-string v14, "PlaybackViewModel"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Receive new location "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget-object v0, v9, Lru/cn/api/iptv/replies/MediaLocation;->type:Lru/cn/api/iptv/replies/MediaLocation$Type;

    move-object/from16 v16, v0

    .line 164
    invoke-virtual/range {v16 .. v16}, Lru/cn/api/iptv/replies/MediaLocation$Type;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget-boolean v0, v9, Lru/cn/api/iptv/replies/MediaLocation;->hasTimeshift:Z

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "%:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "% territory: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget-wide v0, v9, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    move-wide/from16 v16, v0

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 163
    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_0

    .line 139
    .end local v13    # "timezoneValue":Ljava/lang/String;
    :cond_0
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 155
    :cond_1
    const/4 v14, 0x0

    goto/16 :goto_2

    .line 159
    .restart local v13    # "timezoneValue":Ljava/lang/String;
    :cond_2
    const/4 v14, 0x0

    goto :goto_3

    .line 173
    .end local v2    # "aspectH":F
    .end local v3    # "aspectW":F
    .end local v4    # "channelId":J
    .end local v6    # "cropX":I
    .end local v7    # "cropY":I
    .end local v8    # "l":Ljava/lang/String;
    .end local v9    # "location":Lru/cn/api/iptv/replies/MediaLocation;
    .end local v10    # "locationTitle":Ljava/lang/String;
    .end local v12    # "timeshift":I
    .end local v13    # "timezoneValue":Ljava/lang/String;
    :cond_3
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    .line 175
    return-object v11
.end method

.method private parseAdTags(Landroid/database/Cursor;)Ljava/util/List;
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 179
    const-string v2, "ad_tags"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 180
    .local v0, "formattedTags":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 181
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 184
    :goto_0
    return-object v2

    .line 183
    :cond_0
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 184
    .local v1, "tags":[Ljava/lang/String;
    invoke-static {v1}, Lcom/annimon/stream/Stream;->of([Ljava/lang/Object;)Lcom/annimon/stream/Stream;

    move-result-object v2

    sget-object v3, Lru/cn/tv/player/PlaybackViewModel$$Lambda$6;->$instance:Lcom/annimon/stream/function/Function;

    .line 185
    invoke-virtual {v2, v3}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v2

    .line 186
    invoke-virtual {v2}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object v2

    goto :goto_0
.end method

.method private telecast(J)Lio/reactivex/Observable;
    .locals 3
    .param p1, "telecastId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lio/reactivex/Observable",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 190
    invoke-static {p1, p2}, Lru/cn/api/provider/TvContentProviderContract;->telecastUri(J)Landroid/net/Uri;

    move-result-object v0

    .line 192
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/player/PlaybackViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v1

    .line 193
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 192
    return-object v1
.end method

.method private telecastLocations(J)Lio/reactivex/Observable;
    .locals 3
    .param p1, "telecastId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lio/reactivex/Observable",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 204
    invoke-static {p1, p2}, Lru/cn/api/provider/TvContentProviderContract;->telecastLocationsUri(J)Landroid/net/Uri;

    move-result-object v0

    .line 206
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/player/PlaybackViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v1

    .line 207
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 206
    return-object v1
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$PlaybackViewModel(Landroid/database/Cursor;)Ljava/util/List;
    .locals 1

    invoke-direct {p0, p1}, Lru/cn/tv/player/PlaybackViewModel;->mapChannelLocations(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method channel()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lru/cn/tv/player/PlaybackViewModel;->channel:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method channelLocations()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lru/cn/tv/player/PlaybackViewModel;->channelLocations:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method currentTelecast()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lru/cn/tv/player/PlaybackViewModel;->currentTelecast:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method loadChannel(J)V
    .locals 3
    .param p1, "channelId"    # J

    .prologue
    .line 68
    invoke-virtual {p0}, Lru/cn/tv/player/PlaybackViewModel;->unbindAll()V

    .line 70
    invoke-direct {p0, p1, p2}, Lru/cn/tv/player/PlaybackViewModel;->channel(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 71
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/player/PlaybackViewModel;->channel:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lru/cn/tv/player/PlaybackViewModel$$Lambda$0;->get$Lambda(Landroid/arch/lifecycle/MutableLiveData;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 72
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 70
    invoke-virtual {p0, v0}, Lru/cn/tv/player/PlaybackViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 73
    return-void
.end method

.method loadChannelLocations(J)V
    .locals 3
    .param p1, "channelId"    # J

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Lru/cn/tv/player/PlaybackViewModel;->channelLocations(J)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lru/cn/tv/player/PlaybackViewModel$$Lambda$2;

    invoke-direct {v1, p0}, Lru/cn/tv/player/PlaybackViewModel$$Lambda$2;-><init>(Lru/cn/tv/player/PlaybackViewModel;)V

    .line 89
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 90
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/player/PlaybackViewModel;->channelLocations:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lru/cn/tv/player/PlaybackViewModel$$Lambda$3;->get$Lambda(Landroid/arch/lifecycle/MutableLiveData;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 91
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 88
    invoke-virtual {p0, v0}, Lru/cn/tv/player/PlaybackViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 92
    return-void
.end method

.method loadCurrentTelecast(JJ)V
    .locals 3
    .param p1, "cnId"    # J
    .param p3, "currentPlayedTime"    # J

    .prologue
    .line 101
    invoke-direct {p0, p1, p2, p3, p4}, Lru/cn/tv/player/PlaybackViewModel;->currentTelecast(JJ)Lio/reactivex/Observable;

    move-result-object v0

    .line 102
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/player/PlaybackViewModel;->currentTelecast:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lru/cn/tv/player/PlaybackViewModel$$Lambda$5;->get$Lambda(Landroid/arch/lifecycle/MutableLiveData;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 103
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 101
    invoke-virtual {p0, v0}, Lru/cn/tv/player/PlaybackViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 104
    return-void
.end method

.method loadTelecast(JZ)V
    .locals 3
    .param p1, "telecastId"    # J
    .param p3, "playingChannel"    # Z

    .prologue
    .line 76
    if-nez p3, :cond_0

    .line 77
    invoke-virtual {p0}, Lru/cn/tv/player/PlaybackViewModel;->unbindAll()V

    .line 80
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    .line 81
    invoke-direct {p0, p1, p2}, Lru/cn/tv/player/PlaybackViewModel;->telecast(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 82
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/player/PlaybackViewModel;->telecast:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lru/cn/tv/player/PlaybackViewModel$$Lambda$1;->get$Lambda(Landroid/arch/lifecycle/MutableLiveData;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 83
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 81
    invoke-virtual {p0, v0}, Lru/cn/tv/player/PlaybackViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 85
    :cond_1
    return-void
.end method

.method loadTelecastLocations(J)V
    .locals 3
    .param p1, "telecastId"    # J

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Lru/cn/tv/player/PlaybackViewModel;->telecastLocations(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 96
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/player/PlaybackViewModel;->telecastLocations:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lru/cn/tv/player/PlaybackViewModel$$Lambda$4;->get$Lambda(Landroid/arch/lifecycle/MutableLiveData;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 97
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 95
    invoke-virtual {p0, v0}, Lru/cn/tv/player/PlaybackViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 98
    return-void
.end method

.method telecast()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lru/cn/tv/player/PlaybackViewModel;->telecast:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method telecastLocations()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lru/cn/tv/player/PlaybackViewModel;->telecastLocations:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method
