.class public final Lcom/my/target/core/parsers/g;
.super Ljava/lang/Object;
.source "InterstitialSliderAdSectionParser.java"


# instance fields
.field private final C:Lcom/my/target/bf;


# direct methods
.method private constructor <init>(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {p1, p2, p3}, Lcom/my/target/bf;->b(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/parsers/g;->C:Lcom/my/target/bf;

    .line 28
    return-void
.end method

.method public static c(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/g;
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/my/target/core/parsers/g;

    invoke-direct {v0, p0, p1, p2}, Lcom/my/target/core/parsers/g;-><init>(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;Lcom/my/target/dw;)V
    .locals 3

    .prologue
    .line 32
    iget-object v0, p0, Lcom/my/target/core/parsers/g;->C:Lcom/my/target/bf;

    invoke-virtual {v0, p1, p2}, Lcom/my/target/bf;->a(Lorg/json/JSONObject;Lcom/my/target/ak;)V

    .line 33
    const-string v0, "settings"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 34
    if-eqz v0, :cond_1

    .line 1042
    const-string v1, "close_icon_hd"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1043
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1045
    invoke-static {v1}, Lcom/my/target/common/models/ImageData;->newImageData(Ljava/lang/String;)Lcom/my/target/common/models/ImageData;

    move-result-object v1

    .line 1046
    invoke-virtual {p2, v1}, Lcom/my/target/dw;->setCloseIcon(Lcom/my/target/common/models/ImageData;)V

    .line 1048
    :cond_0
    const-string v1, "backgroundColor"

    invoke-virtual {p2}, Lcom/my/target/dw;->getBackgroundColor()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/my/target/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/my/target/dw;->setBackgroundColor(I)V

    .line 1049
    const-string v1, "markerColor"

    invoke-virtual {p2}, Lcom/my/target/dw;->n()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/my/target/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/my/target/dw;->b(I)V

    .line 1050
    const-string v1, "activeMarkerColor"

    invoke-virtual {p2}, Lcom/my/target/dw;->m()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/my/target/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/dw;->a(I)V

    .line 38
    :cond_1
    return-void
.end method
