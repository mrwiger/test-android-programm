.class public final Lcom/google/android/exoplayer2/ui/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/ui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final exo_artwork:I = 0x7f0900bc

.field public static final exo_content_frame:I = 0x7f0900bd

.field public static final exo_controller:I = 0x7f0900be

.field public static final exo_controller_placeholder:I = 0x7f0900bf

.field public static final exo_duration:I = 0x7f0900c0

.field public static final exo_ffwd:I = 0x7f0900c1

.field public static final exo_next:I = 0x7f0900c2

.field public static final exo_overlay:I = 0x7f0900c3

.field public static final exo_pause:I = 0x7f0900c4

.field public static final exo_play:I = 0x7f0900c5

.field public static final exo_position:I = 0x7f0900c6

.field public static final exo_prev:I = 0x7f0900c7

.field public static final exo_progress:I = 0x7f0900c8

.field public static final exo_repeat_toggle:I = 0x7f0900c9

.field public static final exo_rew:I = 0x7f0900ca

.field public static final exo_shuffle:I = 0x7f0900cb

.field public static final exo_shutter:I = 0x7f0900cc

.field public static final exo_subtitles:I = 0x7f0900cd

.field public static final fill:I = 0x7f0900d4

.field public static final fit:I = 0x7f0900d7

.field public static final fixed_height:I = 0x7f0900e2

.field public static final fixed_width:I = 0x7f0900e3

.field public static final none:I = 0x7f09014d

.field public static final surface_view:I = 0x7f0901bf

.field public static final texture_view:I = 0x7f0901d3

.field public static final zoom:I = 0x7f0901fc
