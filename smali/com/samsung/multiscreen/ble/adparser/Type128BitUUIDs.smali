.class public Lcom/samsung/multiscreen/ble/adparser/Type128BitUUIDs;
.super Lcom/samsung/multiscreen/ble/adparser/AdElement;
.source "Type128BitUUIDs.java"


# instance fields
.field type:I

.field uuids:[Ljava/util/UUID;


# direct methods
.method public constructor <init>(I[BII)V
    .locals 12
    .param p1, "type"    # I
    .param p2, "data"    # [B
    .param p3, "pos"    # I
    .param p4, "len"    # I

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/samsung/multiscreen/ble/adparser/AdElement;-><init>()V

    .line 28
    iput p1, p0, Lcom/samsung/multiscreen/ble/adparser/Type128BitUUIDs;->type:I

    .line 29
    div-int/lit8 v1, p4, 0x10

    .line 30
    .local v1, "items":I
    new-array v3, v1, [Ljava/util/UUID;

    iput-object v3, p0, Lcom/samsung/multiscreen/ble/adparser/Type128BitUUIDs;->uuids:[Ljava/util/UUID;

    .line 31
    move v2, p3

    .line 32
    .local v2, "ptr":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 33
    aget-byte v3, p2, v2

    int-to-long v8, v3

    const-wide/16 v10, 0xff

    and-long v6, v8, v10

    .line 34
    .local v6, "vl":J
    add-int/lit8 v2, v2, 0x1

    .line 35
    aget-byte v3, p2, v2

    int-to-long v8, v3

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    const/16 v3, 0x8

    shl-long/2addr v8, v3

    or-long/2addr v6, v8

    .line 36
    add-int/lit8 v2, v2, 0x1

    .line 37
    aget-byte v3, p2, v2

    int-to-long v8, v3

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    const/16 v3, 0x10

    shl-long/2addr v8, v3

    or-long/2addr v6, v8

    .line 38
    add-int/lit8 v2, v2, 0x1

    .line 39
    aget-byte v3, p2, v2

    int-to-long v8, v3

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    const/16 v3, 0x18

    shl-long/2addr v8, v3

    or-long/2addr v6, v8

    .line 40
    add-int/lit8 v2, v2, 0x1

    .line 41
    aget-byte v3, p2, v2

    int-to-long v8, v3

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    const/16 v3, 0x20

    shl-long/2addr v8, v3

    or-long/2addr v6, v8

    .line 42
    add-int/lit8 v2, v2, 0x1

    .line 43
    aget-byte v3, p2, v2

    int-to-long v8, v3

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    const/16 v3, 0x28

    shl-long/2addr v8, v3

    or-long/2addr v6, v8

    .line 44
    add-int/lit8 v2, v2, 0x1

    .line 45
    aget-byte v3, p2, v2

    int-to-long v8, v3

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    const/16 v3, 0x30

    shl-long/2addr v8, v3

    or-long/2addr v6, v8

    .line 46
    add-int/lit8 v2, v2, 0x1

    .line 47
    aget-byte v3, p2, v2

    int-to-long v8, v3

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    const/16 v3, 0x38

    shl-long/2addr v8, v3

    or-long/2addr v6, v8

    .line 48
    add-int/lit8 v2, v2, 0x1

    .line 49
    aget-byte v3, p2, v2

    int-to-long v8, v3

    const-wide/16 v10, 0xff

    and-long v4, v8, v10

    .line 50
    .local v4, "vh":J
    add-int/lit8 v2, v2, 0x1

    .line 51
    aget-byte v3, p2, v2

    int-to-long v8, v3

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    const/16 v3, 0x8

    shl-long/2addr v8, v3

    or-long/2addr v4, v8

    .line 52
    add-int/lit8 v2, v2, 0x1

    .line 53
    aget-byte v3, p2, v2

    int-to-long v8, v3

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    const/16 v3, 0x10

    shl-long/2addr v8, v3

    or-long/2addr v4, v8

    .line 54
    add-int/lit8 v2, v2, 0x1

    .line 55
    aget-byte v3, p2, v2

    int-to-long v8, v3

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    const/16 v3, 0x18

    shl-long/2addr v8, v3

    or-long/2addr v4, v8

    .line 56
    add-int/lit8 v2, v2, 0x1

    .line 57
    aget-byte v3, p2, v2

    int-to-long v8, v3

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    const/16 v3, 0x20

    shl-long/2addr v8, v3

    or-long/2addr v4, v8

    .line 58
    add-int/lit8 v2, v2, 0x1

    .line 59
    aget-byte v3, p2, v2

    int-to-long v8, v3

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    const/16 v3, 0x28

    shl-long/2addr v8, v3

    or-long/2addr v4, v8

    .line 60
    add-int/lit8 v2, v2, 0x1

    .line 61
    aget-byte v3, p2, v2

    int-to-long v8, v3

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    const/16 v3, 0x30

    shl-long/2addr v8, v3

    or-long/2addr v4, v8

    .line 62
    add-int/lit8 v2, v2, 0x1

    .line 63
    aget-byte v3, p2, v2

    int-to-long v8, v3

    const-wide/16 v10, 0xff

    and-long/2addr v8, v10

    const/16 v3, 0x38

    shl-long/2addr v8, v3

    or-long/2addr v4, v8

    .line 64
    add-int/lit8 v2, v2, 0x1

    .line 65
    iget-object v3, p0, Lcom/samsung/multiscreen/ble/adparser/Type128BitUUIDs;->uuids:[Ljava/util/UUID;

    new-instance v8, Ljava/util/UUID;

    invoke-direct {v8, v4, v5, v6, v7}, Ljava/util/UUID;-><init>(JJ)V

    aput-object v8, v3, v0

    .line 32
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 67
    .end local v4    # "vh":J
    .end local v6    # "vl":J
    :cond_0
    return-void
.end method


# virtual methods
.method public getType()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/samsung/multiscreen/ble/adparser/Type128BitUUIDs;->type:I

    return v0
.end method

.method public getUUIDs()[Ljava/util/UUID;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/multiscreen/ble/adparser/Type128BitUUIDs;->uuids:[Ljava/util/UUID;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 71
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, "flags: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 72
    .local v1, "sb":Ljava/lang/StringBuffer;
    iget v2, p0, Lcom/samsung/multiscreen/ble/adparser/Type128BitUUIDs;->type:I

    packed-switch v2, :pswitch_data_0

    .line 80
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown 32Bit UUIDs type: 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/multiscreen/ble/adparser/Type128BitUUIDs;->type:I

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 83
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/samsung/multiscreen/ble/adparser/Type128BitUUIDs;->uuids:[Ljava/util/UUID;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 84
    if-lez v0, :cond_0

    .line 85
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 86
    :cond_0
    iget-object v2, p0, Lcom/samsung/multiscreen/ble/adparser/Type128BitUUIDs;->uuids:[Ljava/util/UUID;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 83
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 74
    .end local v0    # "i":I
    :pswitch_0
    const-string v2, "More 32-bit UUIDs: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 77
    :pswitch_1
    const-string v2, "Complete list of 32-bit UUIDs: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 88
    .restart local v0    # "i":I
    :cond_1
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/StringBuffer;)V

    return-object v2

    .line 72
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
