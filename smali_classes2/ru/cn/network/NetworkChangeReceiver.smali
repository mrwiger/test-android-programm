.class public final Lru/cn/network/NetworkChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "NetworkChangeReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/network/NetworkChangeReceiver$NetworkChangeListener;
    }
.end annotation


# instance fields
.field private listener:Lru/cn/network/NetworkChangeReceiver$NetworkChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 31
    return-void
.end method

.method private buildNetworkName(Landroid/content/Context;Landroid/net/NetworkInfo;)Ljava/lang/String;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "info"    # Landroid/net/NetworkInfo;

    .prologue
    .line 82
    const-string v5, "unknown"

    .line 83
    .local v5, "name":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/net/NetworkInfo;->getType()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_1

    .line 84
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const-string v10, "wifi"

    .line 85
    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/wifi/WifiManager;

    .line 86
    .local v8, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v8}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v7

    .line 87
    .local v7, "wifiInfo":Landroid/net/wifi/WifiInfo;
    if-eqz v7, :cond_0

    .line 88
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ("

    .line 89
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v7}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 90
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 120
    .end local v7    # "wifiInfo":Landroid/net/wifi/WifiInfo;
    .end local v8    # "wifiManager":Landroid/net/wifi/WifiManager;
    :cond_0
    :goto_0
    return-object v5

    .line 93
    :cond_1
    invoke-virtual {p2}, Landroid/net/NetworkInfo;->getType()I

    move-result v9

    const/16 v10, 0x9

    if-ne v9, v10, :cond_2

    .line 95
    const-string v5, "ethernet"

    goto :goto_0

    .line 96
    :cond_2
    invoke-virtual {p2}, Landroid/net/NetworkInfo;->getType()I

    move-result v9

    if-nez v9, :cond_0

    .line 97
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const-string v10, "phone"

    .line 98
    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/telephony/TelephonyManager;

    .line 100
    .local v4, "manager":Landroid/telephony/TelephonyManager;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 101
    .local v0, "b":Ljava/lang/StringBuilder;
    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    const-string v9, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-static {p1, v9}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v6

    .line 105
    .local v6, "permissionStatus":I
    if-nez v6, :cond_3

    .line 106
    const-string v9, "_"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v2

    .line 108
    .local v2, "cellLocation":Landroid/telephony/CellLocation;
    instance-of v9, v2, Landroid/telephony/gsm/GsmCellLocation;

    if-eqz v9, :cond_4

    move-object v3, v2

    .line 109
    check-cast v3, Landroid/telephony/gsm/GsmCellLocation;

    .line 110
    .local v3, "gsmCellLocation":Landroid/telephony/gsm/GsmCellLocation;
    invoke-virtual {v3}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 117
    .end local v2    # "cellLocation":Landroid/telephony/CellLocation;
    .end local v3    # "gsmCellLocation":Landroid/telephony/gsm/GsmCellLocation;
    :cond_3
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 111
    .restart local v2    # "cellLocation":Landroid/telephony/CellLocation;
    :cond_4
    instance-of v9, v2, Landroid/telephony/cdma/CdmaCellLocation;

    if-eqz v9, :cond_3

    move-object v1, v2

    .line 112
    check-cast v1, Landroid/telephony/cdma/CdmaCellLocation;

    .line 113
    .local v1, "cdmaCellLocation":Landroid/telephony/cdma/CdmaCellLocation;
    invoke-virtual {v1}, Landroid/telephony/cdma/CdmaCellLocation;->getNetworkId()I

    move-result v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private getSavedNetworkName(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 132
    const-string v1, "NetworkChangeReceiver"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 134
    .local v0, "preferences":Landroid/content/SharedPreferences;
    const-string v1, "networkName"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private saveNetworkName(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 124
    const-string v2, "NetworkChangeReceiver"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 126
    .local v1, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 127
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "networkName"

    invoke-interface {v0, v2, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 128
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 129
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x0

    .line 45
    const-string v5, "NetworkChangeReceiver"

    const-string v6, "onReceive"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "connectivity"

    .line 48
    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 50
    .local v1, "connMgr":Landroid/net/ConnectivityManager;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 51
    .local v0, "activeNetworkInfo":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_3

    .line 52
    invoke-direct {p0, p1, v0}, Lru/cn/network/NetworkChangeReceiver;->buildNetworkName(Landroid/content/Context;Landroid/net/NetworkInfo;)Ljava/lang/String;

    move-result-object v3

    .line 53
    .local v3, "name":Ljava/lang/String;
    invoke-direct {p0, p1}, Lru/cn/network/NetworkChangeReceiver;->getSavedNetworkName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 54
    .local v2, "lastNetworkName":Ljava/lang/String;
    if-nez v2, :cond_1

    .line 55
    invoke-direct {p0, p1, v3}, Lru/cn/network/NetworkChangeReceiver;->saveNetworkName(Landroid/content/Context;Ljava/lang/String;)V

    .line 78
    .end local v2    # "lastNetworkName":Ljava/lang/String;
    .end local v3    # "name":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 59
    .restart local v2    # "lastNetworkName":Ljava/lang/String;
    .restart local v3    # "name":Ljava/lang/String;
    :cond_1
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 60
    iget-object v5, p0, Lru/cn/network/NetworkChangeReceiver;->listener:Lru/cn/network/NetworkChangeReceiver$NetworkChangeListener;

    if-eqz v5, :cond_2

    .line 61
    iget-object v5, p0, Lru/cn/network/NetworkChangeReceiver;->listener:Lru/cn/network/NetworkChangeReceiver$NetworkChangeListener;

    invoke-interface {v5}, Lru/cn/network/NetworkChangeReceiver$NetworkChangeListener;->onNetworkChange()V

    .line 64
    :cond_2
    invoke-direct {p0, p1, v3}, Lru/cn/network/NetworkChangeReceiver;->saveNetworkName(Landroid/content/Context;Ljava/lang/String;)V

    .line 66
    const-string v5, "NetworkChangeReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Network name: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    new-instance v5, Landroid/net/Uri$Builder;

    invoke-direct {v5}, Landroid/net/Uri$Builder;-><init>()V

    const-string v6, "content"

    .line 69
    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "ru.cn.api.tv"

    .line 70
    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "clear_cache"

    .line 71
    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    .line 72
    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    .line 73
    .local v4, "reloadUri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-virtual {v5, v4, v8, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 76
    .end local v2    # "lastNetworkName":Ljava/lang/String;
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "reloadUri":Landroid/net/Uri;
    :cond_3
    const-string v5, "NetworkChangeReceiver"

    const-string v6, "No active network available"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setListener(Lru/cn/network/NetworkChangeReceiver$NetworkChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/network/NetworkChangeReceiver$NetworkChangeListener;

    .prologue
    .line 40
    iput-object p1, p0, Lru/cn/network/NetworkChangeReceiver;->listener:Lru/cn/network/NetworkChangeReceiver$NetworkChangeListener;

    .line 41
    return-void
.end method
