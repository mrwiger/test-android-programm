.class public Lru/cn/tv/stb/calendar/CalendarAdapter;
.super Landroid/widget/SimpleCursorAdapter;
.source "CalendarAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/stb/calendar/CalendarAdapter$ViewHolder;
    }
.end annotation


# static fields
.field private static from:[Ljava/lang/String;

.field private static to:[I


# instance fields
.field private isKidsMode:Z

.field private layout:I

.field private final textColor:I

.field private final textColorWeekend:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 25
    new-array v0, v1, [Ljava/lang/String;

    sput-object v0, Lru/cn/tv/stb/calendar/CalendarAdapter;->from:[Ljava/lang/String;

    .line 27
    new-array v0, v1, [I

    sput-object v0, Lru/cn/tv/stb/calendar/CalendarAdapter;->to:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "layout"    # I

    .prologue
    .line 34
    sget-object v4, Lru/cn/tv/stb/calendar/CalendarAdapter;->from:[Ljava/lang/String;

    sget-object v5, Lru/cn/tv/stb/calendar/CalendarAdapter;->to:[I

    const/4 v6, 0x2

    move-object v0, p0

    move-object v1, p1

    move v2, p3

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/tv/stb/calendar/CalendarAdapter;->isKidsMode:Z

    .line 36
    iput p3, p0, Lru/cn/tv/stb/calendar/CalendarAdapter;->layout:I

    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 38
    .local v7, "res":Landroid/content/res/Resources;
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    invoke-static {p1}, Lru/cn/domain/KidsObject;->isKidsMode(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lru/cn/tv/stb/calendar/CalendarAdapter;->isKidsMode:Z

    .line 41
    const v0, 0x7f060039

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lru/cn/tv/stb/calendar/CalendarAdapter;->textColor:I

    .line 42
    const v0, 0x7f06003b

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lru/cn/tv/stb/calendar/CalendarAdapter;->textColorWeekend:I

    .line 47
    :goto_0
    return-void

    .line 44
    :cond_0
    const v0, 0x7f060038

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lru/cn/tv/stb/calendar/CalendarAdapter;->textColor:I

    .line 45
    const v0, 0x7f06003a

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lru/cn/tv/stb/calendar/CalendarAdapter;->textColorWeekend:I

    goto :goto_0
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 11
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 73
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lru/cn/tv/stb/calendar/CalendarAdapter$ViewHolder;

    .line 75
    .local v3, "holder":Lru/cn/tv/stb/calendar/CalendarAdapter$ViewHolder;
    sget-object v8, Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;->year:Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;

    .line 77
    invoke-virtual {v8}, Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;->toString()Ljava/lang/String;

    move-result-object v8

    .line 76
    invoke-interface {p3, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 75
    invoke-interface {p3, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 78
    .local v7, "year":I
    sget-object v8, Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;->month:Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;

    .line 80
    invoke-virtual {v8}, Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;->toString()Ljava/lang/String;

    move-result-object v8

    .line 79
    invoke-interface {p3, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 78
    invoke-interface {p3, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 81
    .local v4, "month":I
    sget-object v8, Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;->day:Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;

    .line 83
    invoke-virtual {v8}, Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;->toString()Ljava/lang/String;

    move-result-object v8

    .line 82
    invoke-interface {p3, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 81
    invoke-interface {p3, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 84
    .local v1, "day":I
    sget-object v8, Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;->timezone:Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;

    .line 86
    invoke-virtual {v8}, Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;->toString()Ljava/lang/String;

    move-result-object v8

    .line 85
    invoke-interface {p3, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 84
    invoke-interface {p3, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 88
    .local v6, "timezone":I
    invoke-static {v7, v4, v1, v6}, Lru/cn/utils/Utils;->getCalendar(IIII)Ljava/util/Calendar;

    move-result-object v0

    .line 93
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v8

    if-eqz v8, :cond_7

    .line 94
    const/4 v8, 0x7

    const/4 v9, 0x2

    .line 95
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v10

    .line 94
    invoke-virtual {v0, v8, v9, v10}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 96
    .local v2, "dayOfWeek":Ljava/lang/String;
    iget-boolean v8, p0, Lru/cn/tv/stb/calendar/CalendarAdapter;->isKidsMode:Z

    if-eqz v8, :cond_0

    .line 97
    invoke-static {v0}, Lru/cn/utils/DateUtils;->isToday(Ljava/util/Calendar;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 98
    const v8, 0x7f0e0160

    invoke-virtual {p2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 110
    :cond_0
    :goto_0
    const-string v8, "dd MMMM"

    invoke-static {v0, v8}, Lru/cn/utils/Utils;->format(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 116
    .local v5, "monthText":Ljava/lang/String;
    :goto_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-virtual {v2, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    .line 117
    invoke-virtual {v2, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 118
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-virtual {v5, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    .line 119
    invoke-virtual {v5, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 121
    iget-object v8, v3, Lru/cn/tv/stb/calendar/CalendarAdapter$ViewHolder;->day_of_week:Landroid/widget/TextView;

    invoke-virtual {v8, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v8, v3, Lru/cn/tv/stb/calendar/CalendarAdapter$ViewHolder;->date:Landroid/widget/TextView;

    invoke-virtual {v8, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v8

    if-nez v8, :cond_2

    .line 125
    const/4 v8, 0x7

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    const/4 v9, 0x7

    if-eq v8, v9, :cond_1

    const/4 v8, 0x7

    .line 126
    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_8

    .line 127
    :cond_1
    iget-object v8, v3, Lru/cn/tv/stb/calendar/CalendarAdapter$ViewHolder;->day_of_week:Landroid/widget/TextView;

    iget v9, p0, Lru/cn/tv/stb/calendar/CalendarAdapter;->textColorWeekend:I

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 128
    iget-object v8, v3, Lru/cn/tv/stb/calendar/CalendarAdapter$ViewHolder;->date:Landroid/widget/TextView;

    iget v9, p0, Lru/cn/tv/stb/calendar/CalendarAdapter;->textColorWeekend:I

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 134
    :cond_2
    :goto_2
    return-void

    .line 99
    .end local v5    # "monthText":Ljava/lang/String;
    :cond_3
    const/4 v8, 0x1

    invoke-static {v0, v8}, Lru/cn/utils/DateUtils;->isWithinDaysFuture(Ljava/util/Calendar;I)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 100
    const v8, 0x7f0e0161

    invoke-virtual {p2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 101
    :cond_4
    const/4 v8, 0x2

    invoke-static {v0, v8}, Lru/cn/utils/DateUtils;->isWithinDaysFuture(Ljava/util/Calendar;I)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 102
    const v8, 0x7f0e0029

    invoke-virtual {p2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 103
    :cond_5
    const/4 v8, -0x1

    invoke-static {v0, v8}, Lru/cn/utils/DateUtils;->isWithinDaysFuture(Ljava/util/Calendar;I)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 104
    const v8, 0x7f0e018e

    invoke-virtual {p2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 105
    :cond_6
    const/4 v8, -0x2

    invoke-static {v0, v8}, Lru/cn/utils/DateUtils;->isWithinDaysFuture(Ljava/util/Calendar;I)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 106
    const v8, 0x7f0e002e

    invoke-virtual {p2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 112
    .end local v2    # "dayOfWeek":Ljava/lang/String;
    :cond_7
    const/4 v8, 0x7

    const/4 v9, 0x1

    .line 113
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v10

    .line 112
    invoke-virtual {v0, v8, v9, v10}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 114
    .restart local v2    # "dayOfWeek":Ljava/lang/String;
    const-string v8, "dd MM"

    invoke-static {v0, v8}, Lru/cn/utils/Utils;->format(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "monthText":Ljava/lang/String;
    goto/16 :goto_1

    .line 130
    :cond_8
    iget-object v8, v3, Lru/cn/tv/stb/calendar/CalendarAdapter$ViewHolder;->day_of_week:Landroid/widget/TextView;

    iget v9, p0, Lru/cn/tv/stb/calendar/CalendarAdapter;->textColor:I

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 131
    iget-object v8, v3, Lru/cn/tv/stb/calendar/CalendarAdapter$ViewHolder;->date:Landroid/widget/TextView;

    iget v9, p0, Lru/cn/tv/stb/calendar/CalendarAdapter;->textColor:I

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 56
    const-string v3, "layout_inflater"

    .line 57
    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 58
    .local v1, "inflater":Landroid/view/LayoutInflater;
    iget v3, p0, Lru/cn/tv/stb/calendar/CalendarAdapter;->layout:I

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 60
    .local v2, "view":Landroid/view/View;
    new-instance v0, Lru/cn/tv/stb/calendar/CalendarAdapter$ViewHolder;

    invoke-direct {v0, p0}, Lru/cn/tv/stb/calendar/CalendarAdapter$ViewHolder;-><init>(Lru/cn/tv/stb/calendar/CalendarAdapter;)V

    .line 61
    .local v0, "holder":Lru/cn/tv/stb/calendar/CalendarAdapter$ViewHolder;
    const v3, 0x7f090098

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lru/cn/tv/stb/calendar/CalendarAdapter$ViewHolder;->day_of_week:Landroid/widget/TextView;

    .line 62
    const v3, 0x7f090095

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lru/cn/tv/stb/calendar/CalendarAdapter$ViewHolder;->date:Landroid/widget/TextView;

    .line 63
    iget-boolean v3, p0, Lru/cn/tv/stb/calendar/CalendarAdapter;->isKidsMode:Z

    if-nez v3, :cond_0

    .line 64
    iget-object v3, v0, Lru/cn/tv/stb/calendar/CalendarAdapter$ViewHolder;->date:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 66
    :cond_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 67
    return-object v2
.end method
