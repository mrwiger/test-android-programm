.class public Lcom/google/obf/ia;
.super Ljava/lang/Object;
.source "IMASDK"


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field private final b:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(Lcom/google/obf/hj;Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p2, p0, Lcom/google/obf/ia;->a:Landroid/view/ViewGroup;

    .line 3
    invoke-virtual {p1}, Lcom/google/obf/hj;->b()Landroid/webkit/WebView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/ia;->b:Landroid/webkit/WebView;

    .line 4
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 5
    iget-object v0, p0, Lcom/google/obf/ia;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 6
    if-eqz v0, :cond_0

    .line 7
    iget-object v1, p0, Lcom/google/obf/ia;->b:Landroid/webkit/WebView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 8
    iget-object v1, p0, Lcom/google/obf/ia;->b:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/google/obf/ia;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/obf/ia;->b:Landroid/webkit/WebView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 10
    iget-object v0, p0, Lcom/google/obf/ia;->b:Landroid/webkit/WebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 11
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 12
    iget-object v0, p0, Lcom/google/obf/ia;->b:Landroid/webkit/WebView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 13
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/google/obf/ia;->b()V

    .line 15
    iget-object v0, p0, Lcom/google/obf/ia;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/obf/ia;->b:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 16
    return-void
.end method
