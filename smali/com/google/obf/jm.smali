.class public abstract Lcom/google/obf/jm;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/Map;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/jm$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/util/Map",
        "<TK;TV;>;"
    }
.end annotation


# static fields
.field static final a:[Ljava/util/Map$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/Map$Entry",
            "<**>;"
        }
    .end annotation
.end field


# instance fields
.field private transient b:Lcom/google/obf/jn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/jn",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field private transient c:Lcom/google/obf/jn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/jn",
            "<TK;>;"
        }
    .end annotation
.end field

.field private transient d:Lcom/google/obf/jj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/jj",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/util/Map$Entry;

    sput-object v0, Lcom/google/obf/jm;->a:[Ljava/util/Map$Entry;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/obf/jn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/obf/jn",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 9
    iget-object v0, p0, Lcom/google/obf/jm;->b:Lcom/google/obf/jn;

    .line 10
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/obf/jm;->b()Lcom/google/obf/jn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/jm;->b:Lcom/google/obf/jn;

    :cond_0
    return-object v0
.end method

.method abstract b()Lcom/google/obf/jn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/obf/jn",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation
.end method

.method public c()Lcom/google/obf/jn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/obf/jn",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 11
    iget-object v0, p0, Lcom/google/obf/jm;->c:Lcom/google/obf/jn;

    .line 12
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/obf/jm;->d()Lcom/google/obf/jn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/jm;->c:Lcom/google/obf/jn;

    :cond_0
    return-object v0
.end method

.method public final clear()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 5
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 7
    invoke-virtual {p0, p1}, Lcom/google/obf/jm;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Lcom/google/obf/jm;->e()Lcom/google/obf/jj;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/obf/jj;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method abstract d()Lcom/google/obf/jn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/obf/jn",
            "<TK;>;"
        }
    .end annotation
.end method

.method public e()Lcom/google/obf/jj;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/obf/jj",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 13
    iget-object v0, p0, Lcom/google/obf/jm;->d:Lcom/google/obf/jj;

    .line 14
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/obf/jm;->f()Lcom/google/obf/jj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/jm;->d:Lcom/google/obf/jj;

    :cond_0
    return-object v0
.end method

.method public synthetic entrySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/google/obf/jm;->a()Lcom/google/obf/jn;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 15
    invoke-static {p0, p1}, Lcom/google/obf/jq;->a(Ljava/util/Map;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method abstract f()Lcom/google/obf/jj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/obf/jj",
            "<TV;>;"
        }
    .end annotation
.end method

.method public abstract get(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/google/obf/jm;->a()Lcom/google/obf/jn;

    move-result-object v0

    invoke-static {v0}, Lcom/google/obf/jw;->a(Ljava/util/Set;)I

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0}, Lcom/google/obf/jm;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/google/obf/jm;->c()Lcom/google/obf/jn;

    move-result-object v0

    return-object v0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final putAll(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 4
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    invoke-static {p0}, Lcom/google/obf/jq;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/google/obf/jm;->e()Lcom/google/obf/jj;

    move-result-object v0

    return-object v0
.end method
