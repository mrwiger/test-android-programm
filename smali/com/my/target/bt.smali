.class public Lcom/my/target/bt;
.super Landroid/webkit/WebView;
.source "BannerWebView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/bt$d;,
        Lcom/my/target/bt$c;,
        Lcom/my/target/bt$b;,
        Lcom/my/target/bt$a;
    }
.end annotation


# static fields
.field private static final hW:Ljava/lang/String; = "https://ad.mail.ru/"

.field private static final hX:Ljava/lang/String; = "javascript:AdmanJS.execute"


# instance fields
.field private final hY:Landroid/webkit/WebViewClient;

.field private final hZ:Landroid/webkit/WebChromeClient;

.field private ia:Lorg/json/JSONObject;

.field private ib:Lcom/my/target/bt$a;

.field private ic:Z

.field private ie:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/my/target/bt;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/my/target/bt;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 59
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    new-instance v0, Lcom/my/target/bt$b;

    invoke-direct {v0, p0, v1}, Lcom/my/target/bt$b;-><init>(Lcom/my/target/bt;Lcom/my/target/bt$1;)V

    iput-object v0, p0, Lcom/my/target/bt;->hZ:Landroid/webkit/WebChromeClient;

    .line 61
    new-instance v0, Lcom/my/target/bt$c;

    invoke-direct {v0, p0, v1}, Lcom/my/target/bt$c;-><init>(Lcom/my/target/bt;Lcom/my/target/bt$1;)V

    iput-object v0, p0, Lcom/my/target/bt;->hY:Landroid/webkit/WebViewClient;

    .line 63
    new-instance v0, Lcom/my/target/bt$d;

    invoke-virtual {p0}, Lcom/my/target/bt;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/my/target/bt$d;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 64
    new-instance v1, Lcom/my/target/bt$1;

    invoke-direct {v1, p0}, Lcom/my/target/bt$1;-><init>(Lcom/my/target/bt;)V

    invoke-virtual {v0, v1}, Lcom/my/target/bt$d;->a(Lcom/my/target/bt$d$a;)V

    .line 72
    new-instance v1, Lcom/my/target/bt$2;

    invoke-direct {v1, p0, v0}, Lcom/my/target/bt$2;-><init>(Lcom/my/target/bt;Lcom/my/target/bt$d;)V

    invoke-virtual {p0, v1}, Lcom/my/target/bt;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 83
    invoke-virtual {p0, v3}, Lcom/my/target/bt;->setHorizontalScrollBarEnabled(Z)V

    .line 84
    invoke-virtual {p0, v3}, Lcom/my/target/bt;->setVerticalScrollBarEnabled(Z)V

    .line 86
    invoke-virtual {p0}, Lcom/my/target/bt;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 87
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 88
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 89
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    .line 90
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 94
    invoke-virtual {p0}, Lcom/my/target/bt;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setAppCachePath(Ljava/lang/String;)V

    .line 95
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 96
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setAllowContentAccess(Z)V

    .line 97
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    .line 99
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setAllowFileAccessFromFileURLs(Z)V

    .line 100
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setAllowUniversalAccessFromFileURLs(Z)V

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/my/target/bt;->hZ:Landroid/webkit/WebChromeClient;

    invoke-virtual {p0, v0}, Lcom/my/target/bt;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 103
    iget-object v0, p0, Lcom/my/target/bt;->hY:Landroid/webkit/WebViewClient;

    invoke-virtual {p0, v0}, Lcom/my/target/bt;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 104
    return-void
.end method

.method private I(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/my/target/bt;->ib:Lcom/my/target/bt$a;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/my/target/bt;->ib:Lcom/my/target/bt$a;

    invoke-interface {v0, p1}, Lcom/my/target/bt$a;->J(Ljava/lang/String;)V

    .line 135
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/my/target/bt;)Lcom/my/target/bt$a;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/my/target/bt;->ib:Lcom/my/target/bt$a;

    return-object v0
.end method

.method static synthetic a(Lcom/my/target/bt;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/my/target/bt;->I(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/my/target/bt;Z)Z
    .locals 0

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/my/target/bt;->ie:Z

    return p1
.end method

.method private aZ()V
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/bt;->ie:Z

    .line 127
    return-void
.end method

.method static synthetic b(Lcom/my/target/bt;)Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/my/target/bt;->ie:Z

    return v0
.end method

.method static synthetic b(Lcom/my/target/bt;Z)Z
    .locals 0

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/my/target/bt;->ic:Z

    return p1
.end method

.method static synthetic c(Lcom/my/target/bt;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/my/target/bt;->aZ()V

    return-void
.end method

.method static synthetic d(Lcom/my/target/bt;)Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/my/target/bt;->ic:Z

    return v0
.end method

.method static synthetic e(Lcom/my/target/bt;)Lorg/json/JSONObject;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/my/target/bt;->ia:Lorg/json/JSONObject;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/my/target/m;)V
    .locals 2

    .prologue
    .line 119
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "javascript:AdmanJS.execute("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Lcom/my/target/m;->g()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 120
    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 121
    invoke-virtual {p0, v0}, Lcom/my/target/bt;->loadUrl(Ljava/lang/String;)V

    .line 122
    return-void
.end method

.method public a(Lorg/json/JSONObject;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 113
    const-string v1, "https://ad.mail.ru/"

    const-string v3, "text/html"

    const-string v4, "UTF-8"

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/my/target/bt;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    iput-object p1, p0, Lcom/my/target/bt;->ia:Lorg/json/JSONObject;

    .line 115
    return-void
.end method

.method public setBannerWebViewListener(Lcom/my/target/bt$a;)V
    .locals 0
    .param p1, "bannerWebViewListener"    # Lcom/my/target/bt$a;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/my/target/bt;->ib:Lcom/my/target/bt$a;

    .line 109
    return-void
.end method
