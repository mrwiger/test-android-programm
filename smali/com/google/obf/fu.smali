.class public final Lcom/google/obf/fu;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/ex;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/fu$a;
    }
.end annotation


# instance fields
.field final a:Z

.field private final b:Lcom/google/obf/ff;


# direct methods
.method public constructor <init>(Lcom/google/obf/ff;Z)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/obf/fu;->b:Lcom/google/obf/ff;

    .line 3
    iput-boolean p2, p0, Lcom/google/obf/fu;->a:Z

    .line 4
    return-void
.end method

.method private a(Lcom/google/obf/eg;Ljava/lang/reflect/Type;)Lcom/google/obf/ew;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/eg;",
            "Ljava/lang/reflect/Type;",
            ")",
            "Lcom/google/obf/ew",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 16
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-eq p2, v0, :cond_0

    const-class v0, Ljava/lang/Boolean;

    if-ne p2, v0, :cond_1

    :cond_0
    sget-object v0, Lcom/google/obf/gb;->f:Lcom/google/obf/ew;

    .line 17
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p2}, Lcom/google/obf/gd;->a(Ljava/lang/reflect/Type;)Lcom/google/obf/gd;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/obf/eg;->a(Lcom/google/obf/gd;)Lcom/google/obf/ew;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/obf/eg;Lcom/google/obf/gd;)Lcom/google/obf/ew;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/obf/eg;",
            "Lcom/google/obf/gd",
            "<TT;>;)",
            "Lcom/google/obf/ew",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 5
    invoke-virtual {p2}, Lcom/google/obf/gd;->b()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 6
    invoke-virtual {p2}, Lcom/google/obf/gd;->a()Ljava/lang/Class;

    move-result-object v1

    .line 7
    const-class v2, Ljava/util/Map;

    invoke-virtual {v2, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 8
    const/4 v0, 0x0

    .line 15
    :goto_0
    return-object v0

    .line 9
    :cond_0
    invoke-static {v0}, Lcom/google/obf/fe;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v1

    .line 10
    invoke-static {v0, v1}, Lcom/google/obf/fe;->b(Ljava/lang/reflect/Type;Ljava/lang/Class;)[Ljava/lang/reflect/Type;

    move-result-object v1

    .line 11
    aget-object v0, v1, v3

    invoke-direct {p0, p1, v0}, Lcom/google/obf/fu;->a(Lcom/google/obf/eg;Ljava/lang/reflect/Type;)Lcom/google/obf/ew;

    move-result-object v4

    .line 12
    aget-object v0, v1, v5

    invoke-static {v0}, Lcom/google/obf/gd;->a(Ljava/lang/reflect/Type;)Lcom/google/obf/gd;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/obf/eg;->a(Lcom/google/obf/gd;)Lcom/google/obf/ew;

    move-result-object v6

    .line 13
    iget-object v0, p0, Lcom/google/obf/fu;->b:Lcom/google/obf/ff;

    invoke-virtual {v0, p2}, Lcom/google/obf/ff;->a(Lcom/google/obf/gd;)Lcom/google/obf/fk;

    move-result-object v7

    .line 14
    new-instance v0, Lcom/google/obf/fu$a;

    aget-object v3, v1, v3

    aget-object v5, v1, v5

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/obf/fu$a;-><init>(Lcom/google/obf/fu;Lcom/google/obf/eg;Ljava/lang/reflect/Type;Lcom/google/obf/ew;Ljava/lang/reflect/Type;Lcom/google/obf/ew;Lcom/google/obf/fk;)V

    goto :goto_0
.end method
