.class final Lcom/my/target/eg$a;
.super Ljava/lang/Object;
.source "PromoMediaAdView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/eg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic bY:Lcom/my/target/eg;


# direct methods
.method private constructor <init>(Lcom/my/target/eg;)V
    .locals 0

    .prologue
    .line 422
    iput-object p1, p0, Lcom/my/target/eg$a;->bY:Lcom/my/target/eg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/my/target/eg;B)V
    .locals 0

    .prologue
    .line 422
    invoke-direct {p0, p1}, Lcom/my/target/eg$a;-><init>(Lcom/my/target/eg;)V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Lcom/my/target/eg$a;->bY:Lcom/my/target/eg;

    invoke-static {v0}, Lcom/my/target/eg;->a(Lcom/my/target/eg;)Lcom/my/target/ee$b;

    move-result-object v0

    if-nez v0, :cond_0

    .line 444
    :goto_0
    return-void

    .line 432
    :cond_0
    iget-object v0, p0, Lcom/my/target/eg$a;->bY:Lcom/my/target/eg;

    invoke-virtual {v0}, Lcom/my/target/eg;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/my/target/eg$a;->bY:Lcom/my/target/eg;

    invoke-virtual {v0}, Lcom/my/target/eg;->isPaused()Z

    move-result v0

    if-nez v0, :cond_1

    .line 434
    iget-object v0, p0, Lcom/my/target/eg$a;->bY:Lcom/my/target/eg;

    invoke-static {v0}, Lcom/my/target/eg;->a(Lcom/my/target/eg;)Lcom/my/target/ee$b;

    move-result-object v0

    invoke-interface {v0}, Lcom/my/target/ee$b;->onPlayClicked()V

    goto :goto_0

    .line 436
    :cond_1
    iget-object v0, p0, Lcom/my/target/eg$a;->bY:Lcom/my/target/eg;

    invoke-virtual {v0}, Lcom/my/target/eg;->isPaused()Z

    move-result v0

    if-nez v0, :cond_2

    .line 438
    iget-object v0, p0, Lcom/my/target/eg$a;->bY:Lcom/my/target/eg;

    invoke-static {v0}, Lcom/my/target/eg;->a(Lcom/my/target/eg;)Lcom/my/target/ee$b;

    move-result-object v0

    invoke-interface {v0}, Lcom/my/target/ee$b;->onPauseClicked()V

    goto :goto_0

    .line 442
    :cond_2
    iget-object v0, p0, Lcom/my/target/eg$a;->bY:Lcom/my/target/eg;

    invoke-static {v0}, Lcom/my/target/eg;->a(Lcom/my/target/eg;)Lcom/my/target/ee$b;

    move-result-object v0

    invoke-interface {v0}, Lcom/my/target/ee$b;->D()V

    goto :goto_0
.end method
