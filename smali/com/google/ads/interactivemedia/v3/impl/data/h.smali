.class final Lcom/google/ads/interactivemedia/v3/impl/data/h;
.super Lcom/google/ads/interactivemedia/v3/impl/data/k;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ads/interactivemedia/v3/impl/data/h$a;
    }
.end annotation


# instance fields
.field private final adContainerBounds:Lcom/google/ads/interactivemedia/v3/impl/data/a$a;

.field private final adTagParameters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final adTagUrl:Ljava/lang/String;

.field private final adsResponse:Ljava/lang/String;

.field private final apiKey:Ljava/lang/String;

.field private final assetKey:Ljava/lang/String;

.field private final authToken:Ljava/lang/String;

.field private final companionSlots:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final contentDuration:Ljava/lang/Float;

.field private final contentKeywords:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final contentSourceId:Ljava/lang/String;

.field private final contentTitle:Ljava/lang/String;

.field private final env:Ljava/lang/String;

.field private final extraParameters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final idType:Ljava/lang/String;

.field private final isAdContainerAttachedToWindow:Ljava/lang/Boolean;

.field private final isLat:Ljava/lang/String;

.field private final isTv:Ljava/lang/Boolean;

.field private final marketAppInfo:Lcom/google/obf/gp$b;

.field private final msParameter:Ljava/lang/String;

.field private final network:Ljava/lang/String;

.field private final rdid:Ljava/lang/String;

.field private final settings:Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;

.field private final streamActivityMonitorId:Ljava/lang/String;

.field private final vastLoadTimeout:Ljava/lang/Float;

.field private final videoId:Ljava/lang/String;

.field private final videoPlayActivation:Lcom/google/obf/gu$a;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/google/obf/gu$a;Ljava/lang/Float;Ljava/util/List;Ljava/lang/String;Ljava/lang/Float;Ljava/util/Map;Ljava/util/Map;Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;Lcom/google/obf/gp$b;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Lcom/google/ads/interactivemedia/v3/impl/data/a$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/obf/gu$a;",
            "Ljava/lang/Float;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;",
            "Lcom/google/obf/gp$b;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Lcom/google/ads/interactivemedia/v3/impl/data/a$a;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/ads/interactivemedia/v3/impl/data/k;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adsResponse:Ljava/lang/String;

    .line 3
    iput-object p2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adTagUrl:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->assetKey:Ljava/lang/String;

    .line 5
    iput-object p4, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->authToken:Ljava/lang/String;

    .line 6
    iput-object p5, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentSourceId:Ljava/lang/String;

    .line 7
    iput-object p6, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->videoId:Ljava/lang/String;

    .line 8
    iput-object p7, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->apiKey:Ljava/lang/String;

    .line 9
    iput-object p8, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adTagParameters:Ljava/util/Map;

    .line 10
    iput-object p9, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->env:Ljava/lang/String;

    .line 11
    iput-object p10, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->network:Ljava/lang/String;

    .line 12
    iput-object p11, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->videoPlayActivation:Lcom/google/obf/gu$a;

    .line 13
    iput-object p12, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentDuration:Ljava/lang/Float;

    .line 14
    iput-object p13, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentKeywords:Ljava/util/List;

    .line 15
    iput-object p14, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentTitle:Ljava/lang/String;

    .line 16
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->vastLoadTimeout:Ljava/lang/Float;

    .line 17
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->companionSlots:Ljava/util/Map;

    .line 18
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->extraParameters:Ljava/util/Map;

    .line 19
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->settings:Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;

    .line 20
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->marketAppInfo:Lcom/google/obf/gp$b;

    .line 21
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->isTv:Ljava/lang/Boolean;

    .line 22
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->msParameter:Ljava/lang/String;

    .line 23
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->isAdContainerAttachedToWindow:Ljava/lang/Boolean;

    .line 24
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adContainerBounds:Lcom/google/ads/interactivemedia/v3/impl/data/a$a;

    .line 25
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->streamActivityMonitorId:Ljava/lang/String;

    .line 26
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->rdid:Ljava/lang/String;

    .line 27
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->idType:Ljava/lang/String;

    .line 28
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->isLat:Ljava/lang/String;

    .line 29
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/google/obf/gu$a;Ljava/lang/Float;Ljava/util/List;Ljava/lang/String;Ljava/lang/Float;Ljava/util/Map;Ljava/util/Map;Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;Lcom/google/obf/gp$b;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Lcom/google/ads/interactivemedia/v3/impl/data/a$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/impl/data/h$1;)V
    .locals 0

    .prologue
    .line 147
    invoke-direct/range {p0 .. p27}, Lcom/google/ads/interactivemedia/v3/impl/data/h;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/google/obf/gu$a;Ljava/lang/Float;Ljava/util/List;Ljava/lang/String;Ljava/lang/Float;Ljava/util/Map;Ljava/util/Map;Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;Lcom/google/obf/gp$b;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Lcom/google/ads/interactivemedia/v3/impl/data/a$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public adContainerBounds()Lcom/google/ads/interactivemedia/v3/impl/data/a$a;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adContainerBounds:Lcom/google/ads/interactivemedia/v3/impl/data/a$a;

    return-object v0
.end method

.method public adTagParameters()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adTagParameters:Ljava/util/Map;

    return-object v0
.end method

.method public adTagUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adTagUrl:Ljava/lang/String;

    return-object v0
.end method

.method public adsResponse()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adsResponse:Ljava/lang/String;

    return-object v0
.end method

.method public apiKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->apiKey:Ljava/lang/String;

    return-object v0
.end method

.method public assetKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->assetKey:Ljava/lang/String;

    return-object v0
.end method

.method public authToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->authToken:Ljava/lang/String;

    return-object v0
.end method

.method public companionSlots()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->companionSlots:Ljava/util/Map;

    return-object v0
.end method

.method public contentDuration()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentDuration:Ljava/lang/Float;

    return-object v0
.end method

.method public contentKeywords()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentKeywords:Ljava/util/List;

    return-object v0
.end method

.method public contentSourceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentSourceId:Ljava/lang/String;

    return-object v0
.end method

.method public contentTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentTitle:Ljava/lang/String;

    return-object v0
.end method

.method public env()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->env:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 58
    if-ne p1, p0, :cond_1

    .line 90
    :cond_0
    :goto_0
    return v0

    .line 60
    :cond_1
    instance-of v2, p1, Lcom/google/ads/interactivemedia/v3/impl/data/k;

    if-eqz v2, :cond_1e

    .line 61
    check-cast p1, Lcom/google/ads/interactivemedia/v3/impl/data/k;

    .line 62
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adsResponse:Ljava/lang/String;

    if-nez v2, :cond_3

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->adsResponse()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adTagUrl:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 63
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->adTagUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_2
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->assetKey:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 64
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->assetKey()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_3
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->authToken:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 65
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->authToken()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_4
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentSourceId:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 66
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->contentSourceId()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_5
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->videoId:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 67
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->videoId()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_6
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->apiKey:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 68
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->apiKey()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_7
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adTagParameters:Ljava/util/Map;

    if-nez v2, :cond_a

    .line 69
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->adTagParameters()Ljava/util/Map;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_8
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->env:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 70
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->env()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_9
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->network:Ljava/lang/String;

    if-nez v2, :cond_c

    .line 71
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->network()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_a
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->videoPlayActivation:Lcom/google/obf/gu$a;

    if-nez v2, :cond_d

    .line 72
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->videoPlayActivation()Lcom/google/obf/gu$a;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_b
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentDuration:Ljava/lang/Float;

    if-nez v2, :cond_e

    .line 73
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->contentDuration()Ljava/lang/Float;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_c
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentKeywords:Ljava/util/List;

    if-nez v2, :cond_f

    .line 74
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->contentKeywords()Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_d
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentTitle:Ljava/lang/String;

    if-nez v2, :cond_10

    .line 75
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->contentTitle()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_e
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->vastLoadTimeout:Ljava/lang/Float;

    if-nez v2, :cond_11

    .line 76
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->vastLoadTimeout()Ljava/lang/Float;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_f
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->companionSlots:Ljava/util/Map;

    if-nez v2, :cond_12

    .line 77
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->companionSlots()Ljava/util/Map;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_10
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->extraParameters:Ljava/util/Map;

    if-nez v2, :cond_13

    .line 78
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->extraParameters()Ljava/util/Map;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_11
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->settings:Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;

    if-nez v2, :cond_14

    .line 79
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->settings()Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_12
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->marketAppInfo:Lcom/google/obf/gp$b;

    if-nez v2, :cond_15

    .line 80
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->marketAppInfo()Lcom/google/obf/gp$b;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_13
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->isTv:Ljava/lang/Boolean;

    if-nez v2, :cond_16

    .line 81
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->isTv()Ljava/lang/Boolean;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_14
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->msParameter:Ljava/lang/String;

    if-nez v2, :cond_17

    .line 82
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->msParameter()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_15
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->isAdContainerAttachedToWindow:Ljava/lang/Boolean;

    if-nez v2, :cond_18

    .line 83
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->isAdContainerAttachedToWindow()Ljava/lang/Boolean;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_16
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adContainerBounds:Lcom/google/ads/interactivemedia/v3/impl/data/a$a;

    if-nez v2, :cond_19

    .line 84
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->adContainerBounds()Lcom/google/ads/interactivemedia/v3/impl/data/a$a;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_17
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->streamActivityMonitorId:Ljava/lang/String;

    if-nez v2, :cond_1a

    .line 85
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->streamActivityMonitorId()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_18
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->rdid:Ljava/lang/String;

    if-nez v2, :cond_1b

    .line 86
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->rdid()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_19
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->idType:Ljava/lang/String;

    if-nez v2, :cond_1c

    .line 87
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->idType()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1a
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->isLat:Ljava/lang/String;

    if-nez v2, :cond_1d

    .line 88
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->isLat()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto/16 :goto_0

    .line 62
    :cond_3
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adsResponse:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->adsResponse()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_1

    .line 63
    :cond_4
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adTagUrl:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->adTagUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_2

    .line 64
    :cond_5
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->assetKey:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->assetKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_3

    .line 65
    :cond_6
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->authToken:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->authToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_4

    .line 66
    :cond_7
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentSourceId:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->contentSourceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_5

    .line 67
    :cond_8
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->videoId:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->videoId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_6

    .line 68
    :cond_9
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->apiKey:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->apiKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_7

    .line 69
    :cond_a
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adTagParameters:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->adTagParameters()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_8

    .line 70
    :cond_b
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->env:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->env()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_9

    .line 71
    :cond_c
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->network:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->network()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_a

    .line 72
    :cond_d
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->videoPlayActivation:Lcom/google/obf/gu$a;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->videoPlayActivation()Lcom/google/obf/gu$a;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/obf/gu$a;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_b

    .line 73
    :cond_e
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentDuration:Ljava/lang/Float;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->contentDuration()Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_c

    .line 74
    :cond_f
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentKeywords:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->contentKeywords()Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_d

    .line 75
    :cond_10
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentTitle:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->contentTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_e

    .line 76
    :cond_11
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->vastLoadTimeout:Ljava/lang/Float;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->vastLoadTimeout()Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_f

    .line 77
    :cond_12
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->companionSlots:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->companionSlots()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_10

    .line 78
    :cond_13
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->extraParameters:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->extraParameters()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_11

    .line 79
    :cond_14
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->settings:Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->settings()Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_12

    .line 80
    :cond_15
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->marketAppInfo:Lcom/google/obf/gp$b;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->marketAppInfo()Lcom/google/obf/gp$b;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_13

    .line 81
    :cond_16
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->isTv:Ljava/lang/Boolean;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->isTv()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_14

    .line 82
    :cond_17
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->msParameter:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->msParameter()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_15

    .line 83
    :cond_18
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->isAdContainerAttachedToWindow:Ljava/lang/Boolean;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->isAdContainerAttachedToWindow()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_16

    .line 84
    :cond_19
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adContainerBounds:Lcom/google/ads/interactivemedia/v3/impl/data/a$a;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->adContainerBounds()Lcom/google/ads/interactivemedia/v3/impl/data/a$a;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_17

    .line 85
    :cond_1a
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->streamActivityMonitorId:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->streamActivityMonitorId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_18

    .line 86
    :cond_1b
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->rdid:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->rdid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_19

    .line 87
    :cond_1c
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->idType:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->idType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_1a

    .line 88
    :cond_1d
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->isLat:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->isLat()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto/16 :goto_0

    :cond_1e
    move v0, v1

    .line 90
    goto/16 :goto_0
.end method

.method public extraParameters()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->extraParameters:Ljava/util/Map;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const v3, 0xf4243

    const/4 v1, 0x0

    .line 91
    .line 93
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adsResponse:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    xor-int/2addr v0, v3

    .line 94
    mul-int v2, v0, v3

    .line 95
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adTagUrl:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    xor-int/2addr v0, v2

    .line 96
    mul-int v2, v0, v3

    .line 97
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->assetKey:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    xor-int/2addr v0, v2

    .line 98
    mul-int v2, v0, v3

    .line 99
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->authToken:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    xor-int/2addr v0, v2

    .line 100
    mul-int v2, v0, v3

    .line 101
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentSourceId:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    xor-int/2addr v0, v2

    .line 102
    mul-int v2, v0, v3

    .line 103
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->videoId:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    xor-int/2addr v0, v2

    .line 104
    mul-int v2, v0, v3

    .line 105
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->apiKey:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    xor-int/2addr v0, v2

    .line 106
    mul-int v2, v0, v3

    .line 107
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adTagParameters:Ljava/util/Map;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    xor-int/2addr v0, v2

    .line 108
    mul-int v2, v0, v3

    .line 109
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->env:Ljava/lang/String;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    xor-int/2addr v0, v2

    .line 110
    mul-int v2, v0, v3

    .line 111
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->network:Ljava/lang/String;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    xor-int/2addr v0, v2

    .line 112
    mul-int v2, v0, v3

    .line 113
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->videoPlayActivation:Lcom/google/obf/gu$a;

    if-nez v0, :cond_a

    move v0, v1

    :goto_a
    xor-int/2addr v0, v2

    .line 114
    mul-int v2, v0, v3

    .line 115
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentDuration:Ljava/lang/Float;

    if-nez v0, :cond_b

    move v0, v1

    :goto_b
    xor-int/2addr v0, v2

    .line 116
    mul-int v2, v0, v3

    .line 117
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentKeywords:Ljava/util/List;

    if-nez v0, :cond_c

    move v0, v1

    :goto_c
    xor-int/2addr v0, v2

    .line 118
    mul-int v2, v0, v3

    .line 119
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentTitle:Ljava/lang/String;

    if-nez v0, :cond_d

    move v0, v1

    :goto_d
    xor-int/2addr v0, v2

    .line 120
    mul-int v2, v0, v3

    .line 121
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->vastLoadTimeout:Ljava/lang/Float;

    if-nez v0, :cond_e

    move v0, v1

    :goto_e
    xor-int/2addr v0, v2

    .line 122
    mul-int v2, v0, v3

    .line 123
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->companionSlots:Ljava/util/Map;

    if-nez v0, :cond_f

    move v0, v1

    :goto_f
    xor-int/2addr v0, v2

    .line 124
    mul-int v2, v0, v3

    .line 125
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->extraParameters:Ljava/util/Map;

    if-nez v0, :cond_10

    move v0, v1

    :goto_10
    xor-int/2addr v0, v2

    .line 126
    mul-int v2, v0, v3

    .line 127
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->settings:Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;

    if-nez v0, :cond_11

    move v0, v1

    :goto_11
    xor-int/2addr v0, v2

    .line 128
    mul-int v2, v0, v3

    .line 129
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->marketAppInfo:Lcom/google/obf/gp$b;

    if-nez v0, :cond_12

    move v0, v1

    :goto_12
    xor-int/2addr v0, v2

    .line 130
    mul-int v2, v0, v3

    .line 131
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->isTv:Ljava/lang/Boolean;

    if-nez v0, :cond_13

    move v0, v1

    :goto_13
    xor-int/2addr v0, v2

    .line 132
    mul-int v2, v0, v3

    .line 133
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->msParameter:Ljava/lang/String;

    if-nez v0, :cond_14

    move v0, v1

    :goto_14
    xor-int/2addr v0, v2

    .line 134
    mul-int v2, v0, v3

    .line 135
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->isAdContainerAttachedToWindow:Ljava/lang/Boolean;

    if-nez v0, :cond_15

    move v0, v1

    :goto_15
    xor-int/2addr v0, v2

    .line 136
    mul-int v2, v0, v3

    .line 137
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adContainerBounds:Lcom/google/ads/interactivemedia/v3/impl/data/a$a;

    if-nez v0, :cond_16

    move v0, v1

    :goto_16
    xor-int/2addr v0, v2

    .line 138
    mul-int v2, v0, v3

    .line 139
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->streamActivityMonitorId:Ljava/lang/String;

    if-nez v0, :cond_17

    move v0, v1

    :goto_17
    xor-int/2addr v0, v2

    .line 140
    mul-int v2, v0, v3

    .line 141
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->rdid:Ljava/lang/String;

    if-nez v0, :cond_18

    move v0, v1

    :goto_18
    xor-int/2addr v0, v2

    .line 142
    mul-int v2, v0, v3

    .line 143
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->idType:Ljava/lang/String;

    if-nez v0, :cond_19

    move v0, v1

    :goto_19
    xor-int/2addr v0, v2

    .line 144
    mul-int/2addr v0, v3

    .line 145
    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->isLat:Ljava/lang/String;

    if-nez v2, :cond_1a

    :goto_1a
    xor-int/2addr v0, v1

    .line 146
    return v0

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adsResponse:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_0

    .line 95
    :cond_1
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adTagUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_1

    .line 97
    :cond_2
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->assetKey:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_2

    .line 99
    :cond_3
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->authToken:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_3

    .line 101
    :cond_4
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentSourceId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_4

    .line 103
    :cond_5
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->videoId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_5

    .line 105
    :cond_6
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->apiKey:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_6

    .line 107
    :cond_7
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adTagParameters:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    goto/16 :goto_7

    .line 109
    :cond_8
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->env:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_8

    .line 111
    :cond_9
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->network:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_9

    .line 113
    :cond_a
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->videoPlayActivation:Lcom/google/obf/gu$a;

    invoke-virtual {v0}, Lcom/google/obf/gu$a;->hashCode()I

    move-result v0

    goto/16 :goto_a

    .line 115
    :cond_b
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentDuration:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    goto/16 :goto_b

    .line 117
    :cond_c
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentKeywords:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    goto/16 :goto_c

    .line 119
    :cond_d
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentTitle:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_d

    .line 121
    :cond_e
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->vastLoadTimeout:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    goto/16 :goto_e

    .line 123
    :cond_f
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->companionSlots:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    goto/16 :goto_f

    .line 125
    :cond_10
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->extraParameters:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    goto/16 :goto_10

    .line 127
    :cond_11
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->settings:Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto/16 :goto_11

    .line 129
    :cond_12
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->marketAppInfo:Lcom/google/obf/gp$b;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto/16 :goto_12

    .line 131
    :cond_13
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->isTv:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_13

    .line 133
    :cond_14
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->msParameter:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_14

    .line 135
    :cond_15
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->isAdContainerAttachedToWindow:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_15

    .line 137
    :cond_16
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adContainerBounds:Lcom/google/ads/interactivemedia/v3/impl/data/a$a;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto/16 :goto_16

    .line 139
    :cond_17
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->streamActivityMonitorId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_17

    .line 141
    :cond_18
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->rdid:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_18

    .line 143
    :cond_19
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->idType:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_19

    .line 145
    :cond_1a
    iget-object v1, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->isLat:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto/16 :goto_1a
.end method

.method public idType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->idType:Ljava/lang/String;

    return-object v0
.end method

.method public isAdContainerAttachedToWindow()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->isAdContainerAttachedToWindow:Ljava/lang/Boolean;

    return-object v0
.end method

.method public isLat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->isLat:Ljava/lang/String;

    return-object v0
.end method

.method public isTv()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->isTv:Ljava/lang/Boolean;

    return-object v0
.end method

.method public marketAppInfo()Lcom/google/obf/gp$b;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->marketAppInfo:Lcom/google/obf/gp$b;

    return-object v0
.end method

.method public msParameter()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->msParameter:Ljava/lang/String;

    return-object v0
.end method

.method public network()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->network:Ljava/lang/String;

    return-object v0
.end method

.method public rdid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->rdid:Ljava/lang/String;

    return-object v0
.end method

.method public settings()Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->settings:Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;

    return-object v0
.end method

.method public streamActivityMonitorId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->streamActivityMonitorId:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 31

    .prologue
    .line 57
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adsResponse:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adTagUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->assetKey:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->authToken:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentSourceId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->videoId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->apiKey:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adTagParameters:Ljava/util/Map;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->env:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->network:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->videoPlayActivation:Lcom/google/obf/gu$a;

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentDuration:Ljava/lang/Float;

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentKeywords:Ljava/util/List;

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->contentTitle:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->vastLoadTimeout:Ljava/lang/Float;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->companionSlots:Ljava/util/Map;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->extraParameters:Ljava/util/Map;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->settings:Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->marketAppInfo:Lcom/google/obf/gp$b;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->isTv:Ljava/lang/Boolean;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->msParameter:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->isAdContainerAttachedToWindow:Ljava/lang/Boolean;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->adContainerBounds:Lcom/google/ads/interactivemedia/v3/impl/data/a$a;

    move-object/from16 v24, v0

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->streamActivityMonitorId:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->rdid:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->idType:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->isLat:Ljava/lang/String;

    move-object/from16 v28, v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/String;->length()I

    move-result v29

    move/from16 v0, v29

    add-int/lit16 v0, v0, 0x199

    move/from16 v29, v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static {v15}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static/range {v25 .. v25}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static/range {v26 .. v26}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static/range {v27 .. v27}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    invoke-static/range {v28 .. v28}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v30

    add-int v29, v29, v30

    new-instance v30, Ljava/lang/StringBuilder;

    move-object/from16 v0, v30

    move/from16 v1, v29

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v29, "GsonAdsRequest{adsResponse="

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v29, ", adTagUrl="

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", assetKey="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", authToken="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", contentSourceId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", videoId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", apiKey="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", adTagParameters="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", env="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", network="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", videoPlayActivation="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", contentDuration="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", contentKeywords="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", contentTitle="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", vastLoadTimeout="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", companionSlots="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", extraParameters="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", settings="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", marketAppInfo="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isTv="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", msParameter="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isAdContainerAttachedToWindow="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", adContainerBounds="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", streamActivityMonitorId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", rdid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v26

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", idType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isLat="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public vastLoadTimeout()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->vastLoadTimeout:Ljava/lang/Float;

    return-object v0
.end method

.method public videoId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->videoId:Ljava/lang/String;

    return-object v0
.end method

.method public videoPlayActivation()Lcom/google/obf/gu$a;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h;->videoPlayActivation:Lcom/google/obf/gu$a;

    return-object v0
.end method
