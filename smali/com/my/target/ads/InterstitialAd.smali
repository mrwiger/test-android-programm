.class public final Lcom/my/target/ads/InterstitialAd;
.super Lcom/my/target/common/BaseAd;
.source "InterstitialAd.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private engine:Lcom/my/target/core/engines/c;

.field private hideStatusBarInDialog:Z

.field private listener:Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;


# direct methods
.method public constructor <init>(ILandroid/content/Context;)V
    .locals 1
    .param p1, "slotId"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const-string v0, "fullscreen"

    invoke-direct {p0, p1, v0}, Lcom/my/target/common/BaseAd;-><init>(ILjava/lang/String;)V

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/ads/InterstitialAd;->hideStatusBarInDialog:Z

    .line 33
    iput-object p2, p0, Lcom/my/target/ads/InterstitialAd;->context:Landroid/content/Context;

    .line 34
    const-string v0, "InterstitialAd created. Version: 5.1.0"

    invoke-static {v0}, Lcom/my/target/g;->c(Ljava/lang/String;)V

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/my/target/ads/InterstitialAd;Lcom/my/target/dv;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/my/target/ads/InterstitialAd;
    .param p1, "x1"    # Lcom/my/target/dv;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/my/target/ads/InterstitialAd;->handleResult(Lcom/my/target/dv;Ljava/lang/String;)V

    return-void
.end method

.method private handleResult(Lcom/my/target/dv;Ljava/lang/String;)V
    .locals 2
    .param p1, "result"    # Lcom/my/target/dv;
    .param p2, "error"    # Ljava/lang/String;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/my/target/ads/InterstitialAd;->listener:Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;

    if-eqz v0, :cond_1

    .line 112
    if-nez p1, :cond_2

    const/4 v0, 0x0

    .line 113
    :goto_0
    if-nez v0, :cond_3

    .line 115
    iget-object v0, p0, Lcom/my/target/ads/InterstitialAd;->listener:Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;

    if-nez p2, :cond_0

    const-string p2, "no ad"

    .end local p2    # "error":Ljava/lang/String;
    :cond_0
    invoke-interface {v0, p2, p0}, Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;->onNoAd(Ljava/lang/String;Lcom/my/target/ads/InterstitialAd;)V

    .line 131
    :cond_1
    :goto_1
    return-void

    .line 112
    .restart local p2    # "error":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Lcom/my/target/dv;->j()Lcom/my/target/core/models/banners/d;

    move-result-object v0

    goto :goto_0

    .line 119
    :cond_3
    invoke-static {p0, v0, p1}, Lcom/my/target/core/engines/c;->a(Lcom/my/target/ads/InterstitialAd;Lcom/my/target/core/models/banners/d;Lcom/my/target/dv;)Lcom/my/target/core/engines/c;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/ads/InterstitialAd;->engine:Lcom/my/target/core/engines/c;

    .line 120
    iget-object v0, p0, Lcom/my/target/ads/InterstitialAd;->engine:Lcom/my/target/core/engines/c;

    if-eqz v0, :cond_4

    .line 122
    iget-object v0, p0, Lcom/my/target/ads/InterstitialAd;->listener:Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;

    invoke-interface {v0, p0}, Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;->onLoad(Lcom/my/target/ads/InterstitialAd;)V

    goto :goto_1

    .line 126
    :cond_4
    iget-object v0, p0, Lcom/my/target/ads/InterstitialAd;->listener:Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;

    const-string v1, "no ad"

    invoke-interface {v0, v1, p0}, Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;->onNoAd(Ljava/lang/String;Lcom/my/target/ads/InterstitialAd;)V

    goto :goto_1
.end method


# virtual methods
.method public final destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 100
    iget-object v0, p0, Lcom/my/target/ads/InterstitialAd;->engine:Lcom/my/target/core/engines/c;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/my/target/ads/InterstitialAd;->engine:Lcom/my/target/core/engines/c;

    .line 1104
    invoke-virtual {v0}, Lcom/my/target/core/engines/c;->dismiss()V

    .line 103
    iput-object v1, p0, Lcom/my/target/ads/InterstitialAd;->engine:Lcom/my/target/core/engines/c;

    .line 105
    :cond_0
    iput-object v1, p0, Lcom/my/target/ads/InterstitialAd;->listener:Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;

    .line 106
    return-void
.end method

.method public final dismiss()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/my/target/ads/InterstitialAd;->engine:Lcom/my/target/core/engines/c;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/my/target/ads/InterstitialAd;->engine:Lcom/my/target/core/engines/c;

    invoke-virtual {v0}, Lcom/my/target/core/engines/c;->dismiss()V

    .line 96
    :cond_0
    return-void
.end method

.method public final getListener()Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/my/target/ads/InterstitialAd;->listener:Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;

    return-object v0
.end method

.method public final isHideStatusBarInDialog()Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/my/target/ads/InterstitialAd;->hideStatusBarInDialog:Z

    return v0
.end method

.method public final load()V
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/my/target/ads/InterstitialAd;->adConfig:Lcom/my/target/b;

    invoke-static {v0}, Lcom/my/target/dp;->newFactory(Lcom/my/target/b;)Lcom/my/target/c;

    move-result-object v0

    new-instance v1, Lcom/my/target/ads/InterstitialAd$1;

    invoke-direct {v1, p0}, Lcom/my/target/ads/InterstitialAd$1;-><init>(Lcom/my/target/ads/InterstitialAd;)V

    invoke-virtual {v0, v1}, Lcom/my/target/c;->a(Lcom/my/target/c$b;)Lcom/my/target/c;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/ads/InterstitialAd;->context:Landroid/content/Context;

    .line 66
    invoke-virtual {v0, v1}, Lcom/my/target/c;->a(Landroid/content/Context;)Lcom/my/target/c;

    .line 67
    return-void
.end method

.method public final setHideStatusBarInDialog(Z)V
    .locals 0
    .param p1, "hideStatusBarInDialog"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/my/target/ads/InterstitialAd;->hideStatusBarInDialog:Z

    .line 45
    return-void
.end method

.method public final setListener(Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/my/target/ads/InterstitialAd;->listener:Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;

    .line 55
    return-void
.end method

.method public final show()V
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lcom/my/target/ads/InterstitialAd;->engine:Lcom/my/target/core/engines/c;

    if-nez v0, :cond_0

    .line 73
    const-string v0, "InterstitialAd.show: No ad"

    invoke-static {v0}, Lcom/my/target/g;->c(Ljava/lang/String;)V

    .line 78
    :goto_0
    return-void

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/my/target/ads/InterstitialAd;->engine:Lcom/my/target/core/engines/c;

    iget-object v1, p0, Lcom/my/target/ads/InterstitialAd;->context:Landroid/content/Context;

    .line 1064
    sput-object v0, Lcom/my/target/common/MyTargetActivity;->activityEngine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

    .line 1065
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/my/target/common/MyTargetActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1066
    instance-of v2, v1, Landroid/app/Activity;

    if-nez v2, :cond_1

    .line 1068
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1070
    :cond_1
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final showDialog()V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/my/target/ads/InterstitialAd;->engine:Lcom/my/target/core/engines/c;

    if-nez v0, :cond_0

    .line 84
    const-string v0, "InterstitialAd.showDialog: No ad"

    invoke-static {v0}, Lcom/my/target/g;->c(Ljava/lang/String;)V

    .line 88
    :goto_0
    return-void

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/my/target/ads/InterstitialAd;->engine:Lcom/my/target/core/engines/c;

    iget-object v1, p0, Lcom/my/target/ads/InterstitialAd;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/my/target/core/engines/c;->showDialog(Landroid/content/Context;)V

    goto :goto_0
.end method
