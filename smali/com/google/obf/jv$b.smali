.class final Lcom/google/obf/jv$b;
.super Lcom/google/obf/jn;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/jv;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/obf/jn",
        "<TK;>;"
    }
.end annotation


# instance fields
.field private final transient a:Lcom/google/obf/jm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/jm",
            "<TK;*>;"
        }
    .end annotation
.end field

.field private final transient b:Lcom/google/obf/jl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/jl",
            "<TK;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/obf/jm;Lcom/google/obf/jl;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/jm",
            "<TK;*>;",
            "Lcom/google/obf/jl",
            "<TK;>;)V"
        }
    .end annotation

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/obf/jn;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/obf/jv$b;->a:Lcom/google/obf/jm;

    .line 3
    iput-object p2, p0, Lcom/google/obf/jv$b;->b:Lcom/google/obf/jl;

    .line 4
    return-void
.end method


# virtual methods
.method public a()Lcom/google/obf/jy;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/obf/jy",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/google/obf/jv$b;->b()Lcom/google/obf/jl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/obf/jl;->a()Lcom/google/obf/jy;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/google/obf/jl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/obf/jl",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 6
    iget-object v0, p0, Lcom/google/obf/jv$b;->b:Lcom/google/obf/jl;

    return-object v0
.end method

.method c()Z
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x1

    return v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lcom/google/obf/jv$b;->a:Lcom/google/obf/jm;

    invoke-virtual {v0, p1}, Lcom/google/obf/jm;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lcom/google/obf/jv$b;->a()Lcom/google/obf/jy;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lcom/google/obf/jv$b;->a:Lcom/google/obf/jm;

    invoke-virtual {v0}, Lcom/google/obf/jm;->size()I

    move-result v0

    return v0
.end method
