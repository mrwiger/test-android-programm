.class public Lcom/my/target/ai;
.super Lcom/my/target/ah;
.source "CompanionBanner.java"


# instance fields
.field private adSlotID:Ljava/lang/String;

.field private apiFramework:Ljava/lang/String;

.field private assetHeight:I

.field private assetWidth:I

.field private expandedHeight:I

.field private expandedWidth:I

.field private htmlResource:Ljava/lang/String;

.field private iframeResource:Ljava/lang/String;

.field private required:Ljava/lang/String;

.field private staticResource:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/my/target/ah;-><init>()V

    .line 28
    const-string v0, "companion"

    iput-object v0, p0, Lcom/my/target/ai;->type:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public static newBanner()Lcom/my/target/ai;
    .locals 1

    .prologue
    .line 10
    new-instance v0, Lcom/my/target/ai;

    invoke-direct {v0}, Lcom/my/target/ai;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getAdSlotID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/my/target/ai;->adSlotID:Ljava/lang/String;

    return-object v0
.end method

.method public getApiFramework()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/my/target/ai;->apiFramework:Ljava/lang/String;

    return-object v0
.end method

.method public getAssetHeight()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/my/target/ai;->assetHeight:I

    return v0
.end method

.method public getAssetWidth()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/my/target/ai;->assetWidth:I

    return v0
.end method

.method public getExpandedHeight()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/my/target/ai;->expandedHeight:I

    return v0
.end method

.method public getExpandedWidth()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/my/target/ai;->expandedWidth:I

    return v0
.end method

.method public getHtmlResource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/my/target/ai;->htmlResource:Ljava/lang/String;

    return-object v0
.end method

.method public getIframeResource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/my/target/ai;->iframeResource:Ljava/lang/String;

    return-object v0
.end method

.method public getRequired()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/my/target/ai;->required:Ljava/lang/String;

    return-object v0
.end method

.method public getStaticResource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/my/target/ai;->staticResource:Ljava/lang/String;

    return-object v0
.end method

.method public setAdSlotID(Ljava/lang/String;)V
    .locals 0
    .param p1, "adSlotID"    # Ljava/lang/String;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/my/target/ai;->adSlotID:Ljava/lang/String;

    .line 119
    return-void
.end method

.method public setApiFramework(Ljava/lang/String;)V
    .locals 0
    .param p1, "apiFramework"    # Ljava/lang/String;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/my/target/ai;->apiFramework:Ljava/lang/String;

    .line 109
    return-void
.end method

.method public setAssetHeight(I)V
    .locals 0
    .param p1, "assetHeight"    # I

    .prologue
    .line 48
    iput p1, p0, Lcom/my/target/ai;->assetHeight:I

    .line 49
    return-void
.end method

.method public setAssetWidth(I)V
    .locals 0
    .param p1, "assetWidth"    # I

    .prologue
    .line 38
    iput p1, p0, Lcom/my/target/ai;->assetWidth:I

    .line 39
    return-void
.end method

.method public setExpandedHeight(I)V
    .locals 0
    .param p1, "expandedHeight"    # I

    .prologue
    .line 68
    iput p1, p0, Lcom/my/target/ai;->expandedHeight:I

    .line 69
    return-void
.end method

.method public setExpandedWidth(I)V
    .locals 0
    .param p1, "expandedWidth"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/my/target/ai;->expandedWidth:I

    .line 59
    return-void
.end method

.method public setHtmlResource(Ljava/lang/String;)V
    .locals 0
    .param p1, "htmlResource"    # Ljava/lang/String;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/my/target/ai;->htmlResource:Ljava/lang/String;

    .line 99
    return-void
.end method

.method public setIframeResource(Ljava/lang/String;)V
    .locals 0
    .param p1, "iframeResource"    # Ljava/lang/String;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/my/target/ai;->iframeResource:Ljava/lang/String;

    .line 89
    return-void
.end method

.method public setRequired(Ljava/lang/String;)V
    .locals 0
    .param p1, "required"    # Ljava/lang/String;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/my/target/ai;->required:Ljava/lang/String;

    .line 129
    return-void
.end method

.method public setStaticResource(Ljava/lang/String;)V
    .locals 0
    .param p1, "staticResource"    # Ljava/lang/String;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/my/target/ai;->staticResource:Ljava/lang/String;

    .line 79
    return-void
.end method
