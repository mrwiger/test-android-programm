.class public final Lru/cn/api/iptv/M3UChannelsParser;
.super Ljava/lang/Object;
.source "M3UChannelsParser.java"


# instance fields
.field private final content:Ljava/lang/String;

.field private groupTitle:Ljava/lang/String;

.field private final ignoredExceptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Exception;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .param p1, "content"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Exception;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p2, "ignoredExceptions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Exception;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/api/iptv/M3UChannelsParser;->content:Ljava/lang/String;

    .line 44
    iput-object p2, p0, Lru/cn/api/iptv/M3UChannelsParser;->ignoredExceptions:Ljava/util/List;

    .line 45
    return-void
.end method

.method private static parseKeyValue(Ljava/lang/String;)Landroid/util/Pair;
    .locals 4
    .param p0, "src"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 262
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 263
    .local v0, "m":[Ljava/lang/String;
    array-length v1, v0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 264
    new-instance v1, Landroid/util/Pair;

    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v3, v0, v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 267
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private parseLocation(Ljava/util/Map;Ljava/lang/String;I)Lru/cn/api/iptv/replies/MediaLocation;
    .locals 21
    .param p2, "address"    # Ljava/lang/String;
    .param p3, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "I)",
            "Lru/cn/api/iptv/replies/MediaLocation;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lru/cn/api/iptv/M3UParserException;
        }
    .end annotation

    .prologue
    .line 118
    .local p1, "attrs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static/range {p2 .. p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v17

    .line 119
    .local v17, "uri":Landroid/net/Uri;
    if-eqz v17, :cond_0

    invoke-virtual/range {v17 .. v17}, Landroid/net/Uri;->isAbsolute()Z

    move-result v18

    if-nez v18, :cond_1

    .line 120
    :cond_0
    new-instance v18, Lru/cn/api/iptv/M3UParserException;

    const-string v19, "Invalid URL"

    const/16 v20, 0x3eb

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, p3

    invoke-direct {v0, v1, v2, v3}, Lru/cn/api/iptv/M3UParserException;-><init>(Ljava/lang/String;II)V

    throw v18

    .line 123
    :cond_1
    new-instance v14, Lru/cn/api/iptv/replies/MediaLocation;

    move-object/from16 v0, p2

    invoke-direct {v14, v0}, Lru/cn/api/iptv/replies/MediaLocation;-><init>(Ljava/lang/String;)V

    .line 125
    .local v14, "location":Lru/cn/api/iptv/replies/MediaLocation;
    const-string v18, "#EXTINF:"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 126
    .local v16, "t":Ljava/lang/String;
    if-eqz v16, :cond_4

    .line 127
    const/16 v18, 0x2c

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    .line 128
    .local v9, "commaIndex":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v9, v0, :cond_4

    .line 129
    const/16 v18, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 130
    .local v4, "additionalInfo":Ljava/lang/String;
    const-string v18, " "

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 132
    .local v8, "attributes":[Ljava/lang/String;
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    array-length v0, v8

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v13, v0, :cond_3

    .line 133
    aget-object v18, v8, v13

    invoke-static/range {v18 .. v18}, Lru/cn/api/iptv/M3UChannelsParser;->parseKeyValue(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v15

    .line 134
    .local v15, "m":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v15, :cond_2

    .line 135
    iget-object v0, v15, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Ljava/lang/String;

    const-string v19, "group-title"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 136
    iget-object v0, v15, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lru/cn/api/iptv/M3UChannelsParser;->groupTitle:Ljava/lang/String;

    .line 132
    :cond_2
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 141
    .end local v15    # "m":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_3
    add-int/lit8 v18, v9, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v14, Lru/cn/api/iptv/replies/MediaLocation;->title:Ljava/lang/String;

    .line 145
    .end local v4    # "additionalInfo":Ljava/lang/String;
    .end local v8    # "attributes":[Ljava/lang/String;
    .end local v9    # "commaIndex":I
    .end local v13    # "i":I
    :cond_4
    const-string v18, "#EXT-INETRA-CHANNEL-INF:"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "t":Ljava/lang/String;
    check-cast v16, Ljava/lang/String;

    .line 146
    .restart local v16    # "t":Ljava/lang/String;
    if-eqz v16, :cond_7

    .line 147
    const-string v18, " "

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 148
    .restart local v8    # "attributes":[Ljava/lang/String;
    const/4 v13, 0x0

    .restart local v13    # "i":I
    :goto_1
    array-length v0, v8

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v13, v0, :cond_7

    .line 149
    aget-object v18, v8, v13

    invoke-static/range {v18 .. v18}, Lru/cn/api/iptv/M3UChannelsParser;->parseKeyValue(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v15

    .line 150
    .restart local v15    # "m":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v15, :cond_6

    .line 151
    iget-object v0, v15, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Ljava/lang/String;

    const/16 v19, -0x1

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->hashCode()I

    move-result v20

    sparse-switch v20, :sswitch_data_0

    :cond_5
    move/from16 v18, v19

    :goto_2
    packed-switch v18, :pswitch_data_0

    .line 148
    :cond_6
    :goto_3
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 151
    :sswitch_0
    const-string v20, "channel-id"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_5

    const/16 v18, 0x0

    goto :goto_2

    :sswitch_1
    const-string v20, "recordable"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_5

    const/16 v18, 0x1

    goto :goto_2

    :sswitch_2
    const-string v20, "territory-id"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_5

    const/16 v18, 0x2

    goto :goto_2

    :sswitch_3
    const-string v20, "age-restriction"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_5

    const/16 v18, 0x3

    goto :goto_2

    .line 153
    :pswitch_0
    iget-object v0, v15, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    iput-wide v0, v14, Lru/cn/api/iptv/replies/MediaLocation;->channelId:J

    goto :goto_3

    .line 157
    :pswitch_1
    iget-object v0, v15, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v18

    move/from16 v0, v18

    iput-boolean v0, v14, Lru/cn/api/iptv/replies/MediaLocation;->hasRecords:Z

    goto :goto_3

    .line 161
    :pswitch_2
    iget-object v0, v15, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    iput-wide v0, v14, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    goto :goto_3

    .line 172
    .end local v8    # "attributes":[Ljava/lang/String;
    .end local v13    # "i":I
    .end local v15    # "m":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_7
    const-string v18, "#EXT-INETRA-STREAM-INF:"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "t":Ljava/lang/String;
    check-cast v16, Ljava/lang/String;

    .line 173
    .restart local v16    # "t":Ljava/lang/String;
    if-eqz v16, :cond_10

    .line 174
    const-string v18, " "

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 176
    .restart local v8    # "attributes":[Ljava/lang/String;
    const/4 v13, 0x0

    .restart local v13    # "i":I
    :goto_4
    array-length v0, v8

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v13, v0, :cond_10

    .line 177
    aget-object v18, v8, v13

    invoke-static/range {v18 .. v18}, Lru/cn/api/iptv/M3UChannelsParser;->parseKeyValue(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v15

    .line 178
    .restart local v15    # "m":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v15, :cond_9

    .line 179
    iget-object v0, v15, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Ljava/lang/String;

    const/16 v19, -0x1

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->hashCode()I

    move-result v20

    sparse-switch v20, :sswitch_data_1

    :cond_8
    move/from16 v18, v19

    :goto_5
    packed-switch v18, :pswitch_data_1

    .line 176
    :cond_9
    :goto_6
    :pswitch_3
    add-int/lit8 v13, v13, 0x1

    goto :goto_4

    .line 179
    :sswitch_4
    const-string v20, "has-timeshift"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    const/16 v18, 0x0

    goto :goto_5

    :sswitch_5
    const-string v20, "aspect-ratio"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    const/16 v18, 0x1

    goto :goto_5

    :sswitch_6
    const-string v20, "resolution"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    const/16 v18, 0x2

    goto :goto_5

    :sswitch_7
    const-string v20, "crop"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    const/16 v18, 0x3

    goto :goto_5

    :sswitch_8
    const-string v20, "access"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    const/16 v18, 0x4

    goto :goto_5

    :sswitch_9
    const-string v20, "allowed-till"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    const/16 v18, 0x5

    goto :goto_5

    :sswitch_a
    const-string v20, "ad-targeting"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    const/16 v18, 0x6

    goto :goto_5

    :sswitch_b
    const-string v20, "id"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    const/16 v18, 0x7

    goto :goto_5

    .line 181
    :pswitch_4
    iget-object v0, v15, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v18

    move/from16 v0, v18

    iput-boolean v0, v14, Lru/cn/api/iptv/replies/MediaLocation;->hasTimeshift:Z

    goto/16 :goto_6

    .line 185
    :pswitch_5
    iget-object v0, v15, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Ljava/lang/String;

    const-string v19, ":"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 186
    .local v6, "aspectRatio":[Ljava/lang/String;
    array-length v0, v6

    move/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_9

    .line 188
    const/16 v18, 0x0

    :try_start_0
    aget-object v18, v6, v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    .line 189
    .local v7, "aspectW":F
    const/16 v18, 0x1

    aget-object v18, v6, v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    .line 190
    .local v5, "aspectH":F
    iput v5, v14, Lru/cn/api/iptv/replies/MediaLocation;->aspectH:F

    .line 191
    iput v7, v14, Lru/cn/api/iptv/replies/MediaLocation;->aspectW:F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_6

    .line 192
    .end local v5    # "aspectH":F
    .end local v7    # "aspectW":F
    :catch_0
    move-exception v18

    goto/16 :goto_6

    .line 200
    .end local v6    # "aspectRatio":[Ljava/lang/String;
    :pswitch_6
    iget-object v0, v15, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Ljava/lang/String;

    const-string v19, ":"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 201
    .local v10, "crop":[Ljava/lang/String;
    array-length v0, v10

    move/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_9

    .line 202
    const/16 v11, 0x64

    .local v11, "cropX":I
    const/16 v12, 0x64

    .line 204
    .local v12, "cropY":I
    const/16 v18, 0x0

    :try_start_1
    aget-object v18, v10, v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    .line 205
    const/16 v18, 0x1

    aget-object v18, v10, v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    move-result v12

    .line 207
    const/16 v18, 0x64

    move/from16 v0, v18

    if-gt v11, v0, :cond_a

    if-nez v11, :cond_b

    .line 208
    :cond_a
    const/16 v11, 0x64

    .line 210
    :cond_b
    const/16 v18, 0x64

    move/from16 v0, v18

    if-gt v12, v0, :cond_c

    if-nez v12, :cond_d

    .line 211
    :cond_c
    const/16 v12, 0x64

    .line 213
    :cond_d
    const/16 v18, 0x32

    move/from16 v0, v18

    if-ge v11, v0, :cond_e

    .line 214
    const/16 v11, 0x32

    .line 216
    :cond_e
    const/16 v18, 0x32

    move/from16 v0, v18

    if-ge v12, v0, :cond_f

    .line 217
    const/16 v12, 0x32

    .line 222
    :cond_f
    :goto_7
    iput v11, v14, Lru/cn/api/iptv/replies/MediaLocation;->cropX:I

    .line 223
    iput v12, v14, Lru/cn/api/iptv/replies/MediaLocation;->cropY:I

    goto/16 :goto_6

    .line 230
    .end local v10    # "crop":[Ljava/lang/String;
    .end local v11    # "cropX":I
    .end local v12    # "cropY":I
    :pswitch_7
    :try_start_2
    iget-object v0, v15, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lru/cn/api/iptv/replies/MediaLocation$Access;->valueOf(Ljava/lang/String;)Lru/cn/api/iptv/replies/MediaLocation$Access;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v14, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;
    :try_end_2
    .catch Ljava/lang/IllegalAccessError; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_6

    .line 231
    :catch_1
    move-exception v18

    goto/16 :goto_6

    .line 236
    :pswitch_8
    iget-object v0, v15, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    iput v0, v14, Lru/cn/api/iptv/replies/MediaLocation;->allowedTill:I

    goto/16 :goto_6

    .line 240
    :pswitch_9
    iget-object v0, v15, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v18

    move/from16 v0, v18

    iput-boolean v0, v14, Lru/cn/api/iptv/replies/MediaLocation;->adTargeting:Z

    goto/16 :goto_6

    .line 245
    :pswitch_a
    :try_start_3
    iget-object v0, v15, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    iput-wide v0, v14, Lru/cn/api/iptv/replies/MediaLocation;->streamId:J
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_6

    .line 246
    :catch_2
    move-exception v18

    goto/16 :goto_6

    .line 254
    .end local v8    # "attributes":[Ljava/lang/String;
    .end local v13    # "i":I
    .end local v15    # "m":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/api/iptv/M3UChannelsParser;->groupTitle:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_11

    .line 255
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/api/iptv/M3UChannelsParser;->groupTitle:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput-object v0, v14, Lru/cn/api/iptv/replies/MediaLocation;->groupTitle:Ljava/lang/String;

    .line 258
    :cond_11
    return-object v14

    .line 219
    .restart local v8    # "attributes":[Ljava/lang/String;
    .restart local v10    # "crop":[Ljava/lang/String;
    .restart local v11    # "cropX":I
    .restart local v12    # "cropY":I
    .restart local v13    # "i":I
    .restart local v15    # "m":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_3
    move-exception v18

    goto :goto_7

    .line 151
    :sswitch_data_0
    .sparse-switch
        -0x731689db -> :sswitch_0
        0x33f17fe -> :sswitch_3
        0x2bd3712b -> :sswitch_1
        0x617d9fae -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 179
    :sswitch_data_1
    .sparse-switch
        -0x5f5e8754 -> :sswitch_6
        -0x5c2d29aa -> :sswitch_5
        -0x598cb19e -> :sswitch_4
        -0x54d84a9c -> :sswitch_8
        -0x2c0e3079 -> :sswitch_a
        -0x16b6c446 -> :sswitch_9
        0xd1b -> :sswitch_b
        0x2eba90 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method


# virtual methods
.method public parse()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lru/cn/api/BaseAPI$ParseException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 49
    const/4 v7, 0x0

    iput-object v7, p0, Lru/cn/api/iptv/M3UChannelsParser;->groupTitle:Ljava/lang/String;

    .line 51
    iget-object v7, p0, Lru/cn/api/iptv/M3UChannelsParser;->content:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_0

    .line 52
    new-instance v7, Lru/cn/api/iptv/M3UParserException;

    const-string v8, "Empty file"

    const/16 v9, 0x3ea

    invoke-direct {v7, v8, v9, v10}, Lru/cn/api/iptv/M3UParserException;-><init>(Ljava/lang/String;II)V

    throw v7

    .line 55
    :cond_0
    iget-object v7, p0, Lru/cn/api/iptv/M3UChannelsParser;->content:Ljava/lang/String;

    const-string v8, "[\\r\\n]+"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 58
    .local v6, "records":[Ljava/lang/String;
    aget-object v7, v6, v10

    const-string v8, "\ufeff"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 59
    aget-object v7, v6, v10

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    .line 62
    :cond_1
    aget-object v7, v6, v10

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const-string v8, "#EXTM3U"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 63
    new-instance v7, Lru/cn/api/iptv/M3UParserException;

    const-string v8, "Missing EXTM3U tag"

    const/16 v9, 0x3ec

    invoke-direct {v7, v8, v9, v10}, Lru/cn/api/iptv/M3UParserException;-><init>(Ljava/lang/String;II)V

    throw v7

    .line 66
    :cond_2
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 67
    .local v5, "locations":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/iptv/replies/MediaLocation;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 68
    .local v0, "attrs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_0
    array-length v7, v6

    if-ge v2, v7, :cond_9

    .line 69
    aget-object v7, v6, v2

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 70
    .local v3, "line":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_3

    .line 71
    const-string v7, "#"

    invoke-virtual {v3, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 72
    const-string v7, "#EXTINF:"

    invoke-virtual {v3, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 73
    const-string v7, "#EXTINF:"

    const-string v8, "#EXTINF:"

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v3, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 74
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 73
    invoke-interface {v0, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    :cond_3
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 75
    :cond_4
    const-string v7, "#EXT-INETRA-CHANNEL-INF:"

    invoke-virtual {v3, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 76
    const-string v7, "#EXT-INETRA-CHANNEL-INF:"

    const-string v8, "#EXT-INETRA-CHANNEL-INF:"

    .line 77
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v3, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 78
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 76
    invoke-interface {v0, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 79
    :cond_5
    const-string v7, "#EXT-INETRA-STREAM-INF:"

    invoke-virtual {v3, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 80
    const-string v7, "#EXT-INETRA-STREAM-INF:"

    const-string v8, "#EXT-INETRA-STREAM-INF:"

    .line 81
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v3, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 82
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 80
    invoke-interface {v0, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 86
    :cond_6
    :try_start_0
    invoke-direct {p0, v0, v3, v2}, Lru/cn/api/iptv/M3UChannelsParser;->parseLocation(Ljava/util/Map;Ljava/lang/String;I)Lru/cn/api/iptv/replies/MediaLocation;

    move-result-object v4

    .line 87
    .local v4, "location":Lru/cn/api/iptv/replies/MediaLocation;
    if-eqz v4, :cond_7

    .line 88
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lru/cn/api/iptv/M3UParserException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    .end local v4    # "location":Lru/cn/api/iptv/replies/MediaLocation;
    :cond_7
    :goto_2
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    goto :goto_1

    .line 90
    :catch_0
    move-exception v1

    .line 91
    .local v1, "ex":Lru/cn/api/iptv/M3UParserException;
    iget-object v7, p0, Lru/cn/api/iptv/M3UChannelsParser;->ignoredExceptions:Ljava/util/List;

    if-eqz v7, :cond_8

    iget v7, v1, Lru/cn/api/iptv/M3UParserException;->code:I

    const/16 v8, 0x3eb

    if-ne v7, v8, :cond_8

    .line 92
    iget-object v7, p0, Lru/cn/api/iptv/M3UChannelsParser;->ignoredExceptions:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 95
    :cond_8
    throw v1

    .line 104
    .end local v1    # "ex":Lru/cn/api/iptv/M3UParserException;
    .end local v3    # "line":Ljava/lang/String;
    :cond_9
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    if-nez v7, :cond_a

    .line 105
    new-instance v7, Lru/cn/api/BaseAPI$ParseException;

    const-string v8, "Invalid"

    invoke-direct {v7, v8}, Lru/cn/api/BaseAPI$ParseException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 108
    :cond_a
    return-object v5
.end method
