.class public Lcom/samsung/multiscreen/ble/adparser/TypeSlaveConnectionIntervalRange;
.super Lcom/samsung/multiscreen/ble/adparser/AdElement;
.source "TypeSlaveConnectionIntervalRange.java"


# instance fields
.field connIntervalMax:I

.field connIntervalMin:I


# direct methods
.method public constructor <init>([BII)V
    .locals 3
    .param p1, "data"    # [B
    .param p2, "pos"    # I
    .param p3, "len"    # I

    .prologue
    const v2, 0xffff

    .line 25
    invoke-direct {p0}, Lcom/samsung/multiscreen/ble/adparser/AdElement;-><init>()V

    .line 26
    iput v2, p0, Lcom/samsung/multiscreen/ble/adparser/TypeSlaveConnectionIntervalRange;->connIntervalMin:I

    .line 27
    iput v2, p0, Lcom/samsung/multiscreen/ble/adparser/TypeSlaveConnectionIntervalRange;->connIntervalMax:I

    .line 28
    move v0, p2

    .line 29
    .local v0, "ptr":I
    const/4 v2, 0x4

    if-lt p3, v2, :cond_0

    .line 30
    aget-byte v2, p1, v0

    and-int/lit16 v1, v2, 0xff

    .line 31
    .local v1, "v":I
    add-int/lit8 v0, v0, 0x1

    .line 32
    aget-byte v2, p1, v0

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    .line 33
    add-int/lit8 v0, v0, 0x1

    .line 34
    iput v1, p0, Lcom/samsung/multiscreen/ble/adparser/TypeSlaveConnectionIntervalRange;->connIntervalMin:I

    .line 35
    aget-byte v2, p1, v0

    and-int/lit16 v1, v2, 0xff

    .line 36
    add-int/lit8 v0, v0, 0x1

    .line 37
    aget-byte v2, p1, v0

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    .line 38
    add-int/lit8 v0, v0, 0x1

    .line 39
    iput v1, p0, Lcom/samsung/multiscreen/ble/adparser/TypeSlaveConnectionIntervalRange;->connIntervalMax:I

    .line 41
    .end local v1    # "v":I
    :cond_0
    return-void
.end method


# virtual methods
.method public getConnIntervalMax()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/samsung/multiscreen/ble/adparser/TypeSlaveConnectionIntervalRange;->connIntervalMax:I

    return v0
.end method

.method public getConnIntervalMin()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/samsung/multiscreen/ble/adparser/TypeSlaveConnectionIntervalRange;->connIntervalMin:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const v4, 0xffff

    const/high16 v3, 0x3fa00000    # 1.25f

    .line 45
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "Slave Connection Interval Range: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 46
    .local v0, "sb":Ljava/lang/StringBuffer;
    const-string v1, "conn_interval_min: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 47
    iget v1, p0, Lcom/samsung/multiscreen/ble/adparser/TypeSlaveConnectionIntervalRange;->connIntervalMin:I

    if-ne v1, v4, :cond_0

    .line 48
    const-string v1, "none"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 51
    :goto_0
    const-string v1, ",conn_interval_max: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 52
    iget v1, p0, Lcom/samsung/multiscreen/ble/adparser/TypeSlaveConnectionIntervalRange;->connIntervalMax:I

    if-ne v1, v4, :cond_1

    .line 53
    const-string v1, "none"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 56
    :goto_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/StringBuffer;)V

    return-object v1

    .line 50
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/samsung/multiscreen/ble/adparser/TypeSlaveConnectionIntervalRange;->connIntervalMin:I

    int-to-float v2, v2

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " msec"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 55
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/samsung/multiscreen/ble/adparser/TypeSlaveConnectionIntervalRange;->connIntervalMax:I

    int-to-float v2, v2

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " msec"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1
.end method
