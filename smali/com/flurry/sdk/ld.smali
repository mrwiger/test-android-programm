.class public Lcom/flurry/sdk/ld;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flurry/sdk/ld$a;
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field a:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/flurry/sdk/ld;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/flurry/sdk/ld;->b:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/flurry/sdk/ld;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/flurry/sdk/le;)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, -0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v3, 0x0

    .line 39
    :try_start_0
    new-instance v8, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v8}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 40
    new-instance v4, Ljava/io/DataOutputStream;

    invoke-direct {v4, v8}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 43
    const/16 v2, 0xb

    :try_start_1
    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 1045
    iget-object v2, p1, Lcom/flurry/sdk/le;->a:Ljava/lang/String;

    .line 46
    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 49
    const-string v2, ""

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 53
    const-string v2, "7020300"

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 1053
    iget-wide v2, p1, Lcom/flurry/sdk/le;->b:J

    .line 56
    invoke-virtual {v4, v2, v3}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 1077
    iget-wide v2, p1, Lcom/flurry/sdk/le;->c:J

    .line 59
    invoke-virtual {v4, v2, v3}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 1085
    iget-wide v2, p1, Lcom/flurry/sdk/le;->d:J

    .line 62
    invoke-virtual {v4, v2, v3}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 65
    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 2061
    iget-boolean v2, p1, Lcom/flurry/sdk/le;->r:Z

    .line 75
    if-eqz v2, :cond_4

    .line 76
    const/4 v2, 0x2

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 2069
    :goto_0
    iget-boolean v2, p1, Lcom/flurry/sdk/le;->s:Z

    .line 82
    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 2101
    iget-object v2, p1, Lcom/flurry/sdk/le;->f:Ljava/lang/String;

    .line 85
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 86
    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 3101
    iget-object v2, p1, Lcom/flurry/sdk/le;->f:Ljava/lang/String;

    .line 87
    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 3109
    :goto_1
    iget-object v2, p1, Lcom/flurry/sdk/le;->g:Ljava/lang/String;

    .line 93
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 94
    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 4109
    iget-object v2, p1, Lcom/flurry/sdk/le;->g:Ljava/lang/String;

    .line 95
    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 4117
    :goto_2
    iget-object v2, p1, Lcom/flurry/sdk/le;->h:Ljava/util/Map;

    .line 102
    if-nez v2, :cond_7

    .line 103
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 5093
    :cond_0
    iget-object v2, p1, Lcom/flurry/sdk/le;->e:Ljava/util/Map;

    .line 114
    if-nez v2, :cond_8

    .line 115
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 5125
    :cond_1
    iget-object v2, p1, Lcom/flurry/sdk/le;->i:Ljava/lang/String;

    .line 126
    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 5133
    iget-object v2, p1, Lcom/flurry/sdk/le;->j:Ljava/lang/String;

    .line 129
    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 5141
    iget v2, p1, Lcom/flurry/sdk/le;->k:I

    .line 132
    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 5149
    iget v2, p1, Lcom/flurry/sdk/le;->l:I

    .line 135
    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 5157
    iget-object v2, p1, Lcom/flurry/sdk/le;->m:Ljava/lang/String;

    .line 138
    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 5165
    iget-object v2, p1, Lcom/flurry/sdk/le;->n:Landroid/location/Location;

    .line 141
    if-nez v2, :cond_9

    move-object v2, v4

    :goto_3
    move-object v3, v2

    move v2, v6

    .line 151
    :goto_4
    invoke-virtual {v3, v2}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 8173
    iget v2, p1, Lcom/flurry/sdk/le;->o:I

    .line 155
    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 156
    const/4 v2, -0x1

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 157
    const/4 v2, -0x1

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 8181
    iget-byte v2, p1, Lcom/flurry/sdk/le;->p:B

    .line 160
    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 8189
    iget-object v2, p1, Lcom/flurry/sdk/le;->q:Ljava/lang/Long;

    .line 163
    if-nez v2, :cond_a

    .line 164
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 9197
    :goto_5
    iget-object v2, p1, Lcom/flurry/sdk/le;->t:Ljava/util/Map;

    .line 172
    if-nez v2, :cond_b

    .line 173
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 9205
    :cond_2
    iget-object v2, p1, Lcom/flurry/sdk/le;->u:Ljava/util/List;

    .line 184
    if-nez v2, :cond_c

    .line 185
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 9213
    :cond_3
    iget-boolean v2, p1, Lcom/flurry/sdk/le;->w:Z

    .line 192
    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 9229
    iget-object v9, p1, Lcom/flurry/sdk/le;->y:Ljava/util/List;

    .line 198
    if-eqz v9, :cond_14

    .line 199
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v3, v6

    move v7, v6

    :goto_6
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/flurry/sdk/ky;

    .line 10031
    invoke-virtual {v2}, Lcom/flurry/sdk/ky;->a()[B

    move-result-object v2

    array-length v2, v2

    .line 200
    add-int/2addr v2, v7

    .line 201
    const v7, 0x27100

    if-le v2, v7, :cond_d

    .line 202
    const/4 v2, 0x5

    sget-object v7, Lcom/flurry/sdk/ld;->b:Ljava/lang/String;

    const-string v10, "Error Log size exceeded. No more event details logged."

    invoke-static {v2, v7, v10}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V

    move v7, v3

    .line 10221
    :goto_7
    iget v2, p1, Lcom/flurry/sdk/le;->x:I

    .line 211
    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 212
    invoke-virtual {v4, v7}, Ljava/io/DataOutputStream;->writeShort(I)V

    move v3, v6

    .line 213
    :goto_8
    if-ge v3, v7, :cond_e

    .line 214
    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/flurry/sdk/ky;

    invoke-virtual {v2}, Lcom/flurry/sdk/ky;->a()[B

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->write([B)V

    .line 213
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_8

    .line 78
    :cond_4
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 264
    :catch_0
    move-exception v2

    .line 265
    :goto_9
    const/4 v3, 0x6

    :try_start_2
    sget-object v5, Lcom/flurry/sdk/ld;->b:Ljava/lang/String;

    const-string v6, ""

    invoke-static {v3, v5, v6, v2}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 266
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 268
    :catchall_0
    move-exception v2

    :goto_a
    invoke-static {v4}, Lcom/flurry/sdk/nx;->a(Ljava/io/Closeable;)V

    throw v2

    .line 89
    :cond_5
    const/4 v2, 0x0

    :try_start_3
    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    goto/16 :goto_1

    .line 97
    :cond_6
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    goto/16 :goto_2

    .line 105
    :cond_7
    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v3

    invoke-virtual {v4, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 106
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_b
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 107
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 108
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    goto :goto_b

    .line 117
    :cond_8
    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v3

    invoke-virtual {v4, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 118
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_c
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/util/Map$Entry;

    move-object v3, v0

    .line 119
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 120
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 121
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    goto :goto_c

    .line 144
    :cond_9
    invoke-static {}, Lcom/flurry/sdk/lp;->b()I

    move-result v2

    .line 145
    const/4 v3, 0x1

    invoke-virtual {v4, v3}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 6165
    iget-object v3, p1, Lcom/flurry/sdk/le;->n:Landroid/location/Location;

    .line 147
    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v10

    invoke-static {v10, v11, v2}, Lcom/flurry/sdk/nx;->a(DI)D

    move-result-wide v10

    .line 146
    invoke-virtual {v4, v10, v11}, Ljava/io/DataOutputStream;->writeDouble(D)V

    .line 7165
    iget-object v3, p1, Lcom/flurry/sdk/le;->n:Landroid/location/Location;

    .line 149
    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v10

    invoke-static {v10, v11, v2}, Lcom/flurry/sdk/nx;->a(DI)D

    move-result-wide v10

    .line 148
    invoke-virtual {v4, v10, v11}, Ljava/io/DataOutputStream;->writeDouble(D)V

    .line 8165
    iget-object v3, p1, Lcom/flurry/sdk/le;->n:Landroid/location/Location;

    .line 150
    invoke-virtual {v3}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    invoke-virtual {v4, v3}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 151
    if-eq v2, v9, :cond_15

    move v2, v5

    move-object v3, v4

    goto/16 :goto_4

    .line 166
    :cond_a
    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 9189
    iget-object v2, p1, Lcom/flurry/sdk/le;->q:Ljava/lang/Long;

    .line 167
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v4, v2, v3}, Ljava/io/DataOutputStream;->writeLong(J)V

    goto/16 :goto_5

    .line 175
    :cond_b
    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v3

    invoke-virtual {v4, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 176
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_d
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/util/Map$Entry;

    move-object v3, v0

    .line 177
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 178
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/flurry/sdk/kz;

    iget v2, v2, Lcom/flurry/sdk/kz;->a:I

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    goto :goto_d

    .line 187
    :cond_c
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v4, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 188
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/flurry/sdk/la;

    .line 189
    invoke-virtual {v2}, Lcom/flurry/sdk/la;->b()[B

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->write([B)V

    goto :goto_e

    .line 206
    :cond_d
    add-int/lit8 v3, v3, 0x1

    move v7, v2

    .line 207
    goto/16 :goto_6

    .line 218
    :cond_e
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 219
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 222
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 225
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 10237
    iget-object v2, p1, Lcom/flurry/sdk/le;->v:Ljava/util/List;

    .line 229
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v4, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 230
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_f
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 231
    const/4 v3, 0x0

    new-array v3, v3, [B
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 233
    :try_start_4
    const-string v7, "UTF8"

    invoke-virtual {v2, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_4
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v2

    .line 238
    :goto_10
    const/4 v3, 0x2

    :try_start_5
    invoke-virtual {v4, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 239
    array-length v3, v2

    invoke-virtual {v4, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 240
    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->write([B)V

    goto :goto_f

    .line 235
    :catch_1
    move-exception v2

    const/4 v2, 0x6

    sget-object v7, Lcom/flurry/sdk/ld;->b:Ljava/lang/String;

    const-string v9, "Error encoding purchase receipt."

    invoke-static {v2, v7, v9}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    goto :goto_10

    .line 10245
    :cond_f
    iget-object v2, p1, Lcom/flurry/sdk/le;->z:Ljava/lang/String;

    .line 245
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 246
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 263
    :cond_10
    :goto_11
    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    iput-object v2, p0, Lcom/flurry/sdk/ld;->a:[B
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 268
    invoke-static {v4}, Lcom/flurry/sdk/nx;->a(Ljava/io/Closeable;)V

    .line 269
    return-void

    .line 248
    :cond_11
    :try_start_6
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 249
    array-length v2, v3

    rem-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_12

    .line 250
    const/4 v2, 0x0

    aget-object v2, v3, v2

    invoke-static {v2}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    move-result v2

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 251
    array-length v2, v3

    add-int/lit8 v6, v2, -0x1

    move v2, v5

    .line 252
    :goto_12
    if-ge v2, v6, :cond_10

    .line 253
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 254
    aget-object v5, v3, v2

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-virtual {v4, v10, v11}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 255
    add-int/lit8 v2, v2, 0x1

    aget-object v5, v3, v2

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 252
    add-int/lit8 v2, v2, 0x1

    goto :goto_12

    .line 258
    :cond_12
    const/4 v2, 0x6

    sget-object v3, Lcom/flurry/sdk/ld;->b:Ljava/lang/String;

    const-string v5, "Error variant IDs."

    invoke-static {v2, v3, v5}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 259
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/io/DataOutputStream;->writeShort(I)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_11

    .line 268
    :catchall_1
    move-exception v2

    move-object v4, v3

    goto/16 :goto_a

    .line 264
    :catch_2
    move-exception v2

    move-object v4, v3

    goto/16 :goto_9

    :cond_13
    move v7, v3

    goto/16 :goto_7

    :cond_14
    move v7, v6

    goto/16 :goto_7

    :cond_15
    move-object v2, v4

    goto/16 :goto_3
.end method

.method public constructor <init>([B)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/flurry/sdk/ld;->a:[B

    .line 33
    return-void
.end method
