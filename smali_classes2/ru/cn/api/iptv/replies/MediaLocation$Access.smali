.class public final enum Lru/cn/api/iptv/replies/MediaLocation$Access;
.super Ljava/lang/Enum;
.source "MediaLocation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/api/iptv/replies/MediaLocation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Access"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/api/iptv/replies/MediaLocation$Access;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/api/iptv/replies/MediaLocation$Access;

.field public static final enum allowed:Lru/cn/api/iptv/replies/MediaLocation$Access;

.field public static final enum denied:Lru/cn/api/iptv/replies/MediaLocation$Access;

.field public static final enum pending:Lru/cn/api/iptv/replies/MediaLocation$Access;

.field public static final enum purchased:Lru/cn/api/iptv/replies/MediaLocation$Access;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 12
    new-instance v0, Lru/cn/api/iptv/replies/MediaLocation$Access;

    const-string v1, "allowed"

    invoke-direct {v0, v1, v2}, Lru/cn/api/iptv/replies/MediaLocation$Access;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/iptv/replies/MediaLocation$Access;->allowed:Lru/cn/api/iptv/replies/MediaLocation$Access;

    new-instance v0, Lru/cn/api/iptv/replies/MediaLocation$Access;

    const-string v1, "denied"

    invoke-direct {v0, v1, v3}, Lru/cn/api/iptv/replies/MediaLocation$Access;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/iptv/replies/MediaLocation$Access;->denied:Lru/cn/api/iptv/replies/MediaLocation$Access;

    new-instance v0, Lru/cn/api/iptv/replies/MediaLocation$Access;

    const-string v1, "pending"

    invoke-direct {v0, v1, v4}, Lru/cn/api/iptv/replies/MediaLocation$Access;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/iptv/replies/MediaLocation$Access;->pending:Lru/cn/api/iptv/replies/MediaLocation$Access;

    new-instance v0, Lru/cn/api/iptv/replies/MediaLocation$Access;

    const-string v1, "purchased"

    invoke-direct {v0, v1, v5}, Lru/cn/api/iptv/replies/MediaLocation$Access;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/iptv/replies/MediaLocation$Access;->purchased:Lru/cn/api/iptv/replies/MediaLocation$Access;

    .line 11
    const/4 v0, 0x4

    new-array v0, v0, [Lru/cn/api/iptv/replies/MediaLocation$Access;

    sget-object v1, Lru/cn/api/iptv/replies/MediaLocation$Access;->allowed:Lru/cn/api/iptv/replies/MediaLocation$Access;

    aput-object v1, v0, v2

    sget-object v1, Lru/cn/api/iptv/replies/MediaLocation$Access;->denied:Lru/cn/api/iptv/replies/MediaLocation$Access;

    aput-object v1, v0, v3

    sget-object v1, Lru/cn/api/iptv/replies/MediaLocation$Access;->pending:Lru/cn/api/iptv/replies/MediaLocation$Access;

    aput-object v1, v0, v4

    sget-object v1, Lru/cn/api/iptv/replies/MediaLocation$Access;->purchased:Lru/cn/api/iptv/replies/MediaLocation$Access;

    aput-object v1, v0, v5

    sput-object v0, Lru/cn/api/iptv/replies/MediaLocation$Access;->$VALUES:[Lru/cn/api/iptv/replies/MediaLocation$Access;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/api/iptv/replies/MediaLocation$Access;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    const-class v0, Lru/cn/api/iptv/replies/MediaLocation$Access;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/api/iptv/replies/MediaLocation$Access;

    return-object v0
.end method

.method public static values()[Lru/cn/api/iptv/replies/MediaLocation$Access;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lru/cn/api/iptv/replies/MediaLocation$Access;->$VALUES:[Lru/cn/api/iptv/replies/MediaLocation$Access;

    invoke-virtual {v0}, [Lru/cn/api/iptv/replies/MediaLocation$Access;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/api/iptv/replies/MediaLocation$Access;

    return-object v0
.end method
