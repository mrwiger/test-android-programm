.class public Lru/cn/draggableview/DraggableView;
.super Landroid/widget/FrameLayout;
.source "DraggableView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/draggableview/DraggableView$DragOrientation;,
        Lru/cn/draggableview/DraggableView$DraggableViewListener;
    }
.end annotation


# instance fields
.field private dragOrientation:Lru/cn/draggableview/DraggableView$DragOrientation;

.field private firstView:Landroid/view/View;

.field private firstViewId:I

.field private firstViewIsCaptured:Z

.field private firstViewMaxHeight:I

.field private firstViewMaxWidth:I

.field private firstViewMinHeight:I

.field private firstViewMinWidth:I

.field private fm:Landroid/support/v4/app/FragmentManager;

.field private gestureDetector:Landroid/view/GestureDetector;

.field private gestureListener:Landroid/view/GestureDetector$OnGestureListener;

.field private inflated:Z

.field private isLockInMax:Z

.field private lastOffsetX:F

.field private lastOffsetY:F

.field private listener:Lru/cn/draggableview/DraggableView$DraggableViewListener;

.field private marginBottom:I

.field private marginRight:I

.field private scroller:Landroid/widget/Scroller;

.field private secondView:Landroid/view/View;

.field private secondViewId:I

.field private thirdView:Landroid/view/View;

.field private thirdViewId:I

.field private transformer:Lru/cn/draggableview/Transformer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/high16 v2, -0x40800000    # -1.0f

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 226
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    const/4 v1, 0x1

    iput v1, p0, Lru/cn/draggableview/DraggableView;->firstViewId:I

    .line 22
    const/4 v1, 0x2

    iput v1, p0, Lru/cn/draggableview/DraggableView;->secondViewId:I

    .line 23
    const/4 v1, 0x3

    iput v1, p0, Lru/cn/draggableview/DraggableView;->thirdViewId:I

    .line 37
    iput-boolean v4, p0, Lru/cn/draggableview/DraggableView;->isLockInMax:Z

    .line 45
    iput-boolean v4, p0, Lru/cn/draggableview/DraggableView;->inflated:Z

    .line 92
    iput v2, p0, Lru/cn/draggableview/DraggableView;->lastOffsetX:F

    .line 93
    iput v2, p0, Lru/cn/draggableview/DraggableView;->lastOffsetY:F

    .line 121
    sget-object v1, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_UNSPEC:Lru/cn/draggableview/DraggableView$DragOrientation;

    iput-object v1, p0, Lru/cn/draggableview/DraggableView;->dragOrientation:Lru/cn/draggableview/DraggableView$DragOrientation;

    .line 125
    new-instance v1, Lru/cn/draggableview/DraggableView$1;

    invoke-direct {v1, p0}, Lru/cn/draggableview/DraggableView$1;-><init>(Lru/cn/draggableview/DraggableView;)V

    iput-object v1, p0, Lru/cn/draggableview/DraggableView;->gestureListener:Landroid/view/GestureDetector$OnGestureListener;

    .line 303
    iput-boolean v4, p0, Lru/cn/draggableview/DraggableView;->firstViewIsCaptured:Z

    .line 228
    invoke-virtual {p0}, Lru/cn/draggableview/DraggableView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lru/cn/draggableview/R$styleable;->draggable_view:[I

    invoke-virtual {v1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 231
    .local v0, "attributes":Landroid/content/res/TypedArray;
    sget v1, Lru/cn/draggableview/R$styleable;->draggable_view_first_view_margin_right:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lru/cn/draggableview/DraggableView;->marginRight:I

    .line 233
    sget v1, Lru/cn/draggableview/R$styleable;->draggable_view_first_view_margin_bottom:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lru/cn/draggableview/DraggableView;->marginBottom:I

    .line 235
    sget v1, Lru/cn/draggableview/R$styleable;->draggable_view_first_view_min_width:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lru/cn/draggableview/DraggableView;->firstViewMinWidth:I

    .line 237
    sget v1, Lru/cn/draggableview/R$styleable;->draggable_view_first_view_min_height:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lru/cn/draggableview/DraggableView;->firstViewMinHeight:I

    .line 239
    sget v1, Lru/cn/draggableview/R$styleable;->draggable_view_first_view_max_width:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lru/cn/draggableview/DraggableView;->firstViewMaxWidth:I

    .line 241
    sget v1, Lru/cn/draggableview/R$styleable;->draggable_view_first_view_max_height:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lru/cn/draggableview/DraggableView;->firstViewMaxHeight:I

    .line 244
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 246
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, Lru/cn/draggableview/DraggableView;->gestureListener:Landroid/view/GestureDetector$OnGestureListener;

    invoke-direct {v1, p1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lru/cn/draggableview/DraggableView;->gestureDetector:Landroid/view/GestureDetector;

    .line 247
    iget-object v1, p0, Lru/cn/draggableview/DraggableView;->gestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, v4}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 249
    new-instance v1, Landroid/widget/Scroller;

    invoke-direct {v1, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lru/cn/draggableview/DraggableView;->scroller:Landroid/widget/Scroller;

    .line 250
    return-void
.end method

.method static synthetic access$000(Lru/cn/draggableview/DraggableView;)Lru/cn/draggableview/DraggableView$DragOrientation;
    .locals 1
    .param p0, "x0"    # Lru/cn/draggableview/DraggableView;

    .prologue
    .line 17
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->dragOrientation:Lru/cn/draggableview/DraggableView$DragOrientation;

    return-object v0
.end method

.method static synthetic access$002(Lru/cn/draggableview/DraggableView;Lru/cn/draggableview/DraggableView$DragOrientation;)Lru/cn/draggableview/DraggableView$DragOrientation;
    .locals 0
    .param p0, "x0"    # Lru/cn/draggableview/DraggableView;
    .param p1, "x1"    # Lru/cn/draggableview/DraggableView$DragOrientation;

    .prologue
    .line 17
    iput-object p1, p0, Lru/cn/draggableview/DraggableView;->dragOrientation:Lru/cn/draggableview/DraggableView$DragOrientation;

    return-object p1
.end method

.method static synthetic access$100(Lru/cn/draggableview/DraggableView;)Landroid/widget/Scroller;
    .locals 1
    .param p0, "x0"    # Lru/cn/draggableview/DraggableView;

    .prologue
    .line 17
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->scroller:Landroid/widget/Scroller;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/draggableview/DraggableView;)Lru/cn/draggableview/DraggableView$DraggableViewListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/draggableview/DraggableView;

    .prologue
    .line 17
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->listener:Lru/cn/draggableview/DraggableView$DraggableViewListener;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/draggableview/DraggableView;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/draggableview/DraggableView;

    .prologue
    .line 17
    iget-boolean v0, p0, Lru/cn/draggableview/DraggableView;->isLockInMax:Z

    return v0
.end method

.method static synthetic access$400(Lru/cn/draggableview/DraggableView;)Lru/cn/draggableview/Transformer;
    .locals 1
    .param p0, "x0"    # Lru/cn/draggableview/DraggableView;

    .prologue
    .line 17
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    return-object v0
.end method

.method static synthetic access$500(Lru/cn/draggableview/DraggableView;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/draggableview/DraggableView;

    .prologue
    .line 17
    invoke-direct {p0}, Lru/cn/draggableview/DraggableView;->dispatchOffsetChanged()V

    return-void
.end method

.method private addFragment(ILandroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "f"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 69
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->fm:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 70
    return-void
.end method

.method private computeDuration(I)I
    .locals 1
    .param p1, "distance"    # I

    .prologue
    .line 406
    const/16 v0, 0xfa

    .line 407
    .local v0, "duration":I
    return v0
.end method

.method private createAndAddView(I)Landroid/view/View;
    .locals 4
    .param p1, "id"    # I

    .prologue
    const/4 v3, -0x1

    .line 253
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lru/cn/draggableview/DraggableView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 254
    .local v1, "v":Landroid/view/View;
    invoke-virtual {v1, p1}, Landroid/view/View;->setId(I)V

    .line 255
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 258
    .local v0, "p":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 259
    invoke-virtual {p0, v1}, Lru/cn/draggableview/DraggableView;->addView(Landroid/view/View;)V

    .line 260
    return-object v1
.end method

.method private dispatchOffsetChanged()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    .line 96
    iget-object v2, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v2}, Lru/cn/draggableview/Transformer;->getHorizontalDragOffset()F

    move-result v0

    .line 97
    .local v0, "offsetX":F
    iget-object v2, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v2}, Lru/cn/draggableview/Transformer;->getVerticalDragOffset()F

    move-result v1

    .line 98
    .local v1, "offsetY":F
    iget v2, p0, Lru/cn/draggableview/DraggableView;->lastOffsetX:F

    cmpl-float v2, v2, v0

    if-nez v2, :cond_0

    iget v2, p0, Lru/cn/draggableview/DraggableView;->lastOffsetY:F

    cmpl-float v2, v2, v1

    if-eqz v2, :cond_1

    .line 99
    :cond_0
    iput v0, p0, Lru/cn/draggableview/DraggableView;->lastOffsetX:F

    .line 100
    iput v1, p0, Lru/cn/draggableview/DraggableView;->lastOffsetY:F

    .line 101
    iget-object v2, p0, Lru/cn/draggableview/DraggableView;->listener:Lru/cn/draggableview/DraggableView$DraggableViewListener;

    if-eqz v2, :cond_1

    .line 102
    iget-object v2, p0, Lru/cn/draggableview/DraggableView;->listener:Lru/cn/draggableview/DraggableView$DraggableViewListener;

    invoke-interface {v2, v0, v1}, Lru/cn/draggableview/DraggableView$DraggableViewListener;->offsetChanged(FF)V

    .line 103
    cmpl-float v2, v1, v4

    if-nez v2, :cond_2

    .line 104
    iget-object v2, p0, Lru/cn/draggableview/DraggableView;->listener:Lru/cn/draggableview/DraggableView$DraggableViewListener;

    invoke-interface {v2}, Lru/cn/draggableview/DraggableView$DraggableViewListener;->maximized()V

    .line 105
    sget-object v2, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_UNSPEC:Lru/cn/draggableview/DraggableView$DragOrientation;

    iput-object v2, p0, Lru/cn/draggableview/DraggableView;->dragOrientation:Lru/cn/draggableview/DraggableView$DragOrientation;

    .line 115
    :cond_1
    :goto_0
    return-void

    .line 106
    :cond_2
    cmpl-float v2, v1, v3

    if-nez v2, :cond_3

    cmpl-float v2, v0, v4

    if-nez v2, :cond_3

    .line 107
    iget-object v2, p0, Lru/cn/draggableview/DraggableView;->listener:Lru/cn/draggableview/DraggableView$DraggableViewListener;

    invoke-interface {v2}, Lru/cn/draggableview/DraggableView$DraggableViewListener;->minimized()V

    .line 108
    sget-object v2, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_UNSPEC:Lru/cn/draggableview/DraggableView$DragOrientation;

    iput-object v2, p0, Lru/cn/draggableview/DraggableView;->dragOrientation:Lru/cn/draggableview/DraggableView$DragOrientation;

    goto :goto_0

    .line 109
    :cond_3
    cmpl-float v2, v1, v3

    if-nez v2, :cond_1

    cmpl-float v2, v0, v3

    if-lez v2, :cond_1

    .line 110
    iget-object v2, p0, Lru/cn/draggableview/DraggableView;->listener:Lru/cn/draggableview/DraggableView$DraggableViewListener;

    invoke-interface {v2}, Lru/cn/draggableview/DraggableView$DraggableViewListener;->closed()V

    .line 111
    sget-object v2, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_UNSPEC:Lru/cn/draggableview/DraggableView$DragOrientation;

    iput-object v2, p0, Lru/cn/draggableview/DraggableView;->dragOrientation:Lru/cn/draggableview/DraggableView$DragOrientation;

    goto :goto_0
.end method

.method private isAboveTheMiddle()Z
    .locals 2

    .prologue
    .line 520
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0}, Lru/cn/draggableview/Transformer;->getVerticalDragOffset()F

    move-result v0

    const/high16 v1, 0x3f000000    # 0.5f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isViewUnder(Landroid/view/View;FF)Z
    .locals 5
    .param p0, "v"    # Landroid/view/View;
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 398
    invoke-virtual {p0}, Landroid/view/View;->getX()F

    move-result v1

    .line 399
    .local v1, "l":F
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    int-to-float v4, v4

    add-float v2, v1, v4

    .line 400
    .local v2, "r":F
    invoke-virtual {p0}, Landroid/view/View;->getY()F

    move-result v3

    .line 401
    .local v3, "t":F
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    int-to-float v4, v4

    add-float v0, v3, v4

    .line 402
    .local v0, "b":F
    cmpl-float v4, p1, v1

    if-ltz v4, :cond_0

    cmpg-float v4, p1, v2

    if-gtz v4, :cond_0

    cmpl-float v4, p2, v3

    if-ltz v4, :cond_0

    cmpg-float v4, p2, v0

    if-gtz v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public Swap(Z)V
    .locals 2
    .param p1, "swap"    # Z

    .prologue
    .line 533
    if-eqz p1, :cond_0

    .line 534
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    iget-object v1, p0, Lru/cn/draggableview/DraggableView;->thirdView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lru/cn/draggableview/Transformer;->setSecondView(Landroid/view/View;)V

    .line 535
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    iget-object v1, p0, Lru/cn/draggableview/DraggableView;->secondView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lru/cn/draggableview/Transformer;->setThirdView(Landroid/view/View;)V

    .line 540
    :goto_0
    return-void

    .line 537
    :cond_0
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    iget-object v1, p0, Lru/cn/draggableview/DraggableView;->secondView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lru/cn/draggableview/Transformer;->setSecondView(Landroid/view/View;)V

    .line 538
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    iget-object v1, p0, Lru/cn/draggableview/DraggableView;->thirdView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lru/cn/draggableview/Transformer;->setThirdView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public addFirstFragment(Landroid/support/v4/app/Fragment;I)V
    .locals 1
    .param p1, "f"    # Landroid/support/v4/app/Fragment;
    .param p2, "bgColor"    # I

    .prologue
    .line 56
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->firstView:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 57
    iget v0, p0, Lru/cn/draggableview/DraggableView;->firstViewId:I

    invoke-direct {p0, v0, p1}, Lru/cn/draggableview/DraggableView;->addFragment(ILandroid/support/v4/app/Fragment;)V

    .line 58
    return-void
.end method

.method public addSecondFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1, "f"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 61
    iget v0, p0, Lru/cn/draggableview/DraggableView;->secondViewId:I

    invoke-direct {p0, v0, p1}, Lru/cn/draggableview/DraggableView;->addFragment(ILandroid/support/v4/app/Fragment;)V

    .line 62
    return-void
.end method

.method public close(Z)V
    .locals 10
    .param p1, "animate"    # Z

    .prologue
    .line 458
    if-eqz p1, :cond_0

    .line 459
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->firstView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    float-to-int v1, v0

    .line 460
    .local v1, "startX":I
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->firstView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getY()F

    move-result v0

    float-to-int v2, v0

    .line 461
    .local v2, "startY":I
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0}, Lru/cn/draggableview/Transformer;->getXDistanceToClose()I

    move-result v3

    .line 462
    .local v3, "xDistance":I
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0}, Lru/cn/draggableview/Transformer;->getYDistanceToClose()I

    move-result v4

    .line 464
    .local v4, "yDistance":I
    mul-int v0, v3, v3

    mul-int v7, v4, v4

    add-int/2addr v0, v7

    int-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-int v6, v8

    .line 466
    .local v6, "distance":I
    invoke-direct {p0, v6}, Lru/cn/draggableview/DraggableView;->computeDuration(I)I

    move-result v5

    .line 467
    .local v5, "duration":I
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->scroller:Landroid/widget/Scroller;

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 468
    invoke-virtual {p0}, Lru/cn/draggableview/DraggableView;->invalidate()V

    .line 475
    .end local v1    # "startX":I
    .end local v2    # "startY":I
    .end local v3    # "xDistance":I
    .end local v4    # "yDistance":I
    .end local v5    # "duration":I
    .end local v6    # "distance":I
    :goto_0
    return-void

    .line 470
    :cond_0
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    const/high16 v7, 0x40000000    # 2.0f

    invoke-virtual {v0, v7}, Lru/cn/draggableview/Transformer;->setHorizontalDragOffset(F)V

    .line 471
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v0, v7}, Lru/cn/draggableview/Transformer;->setVerticalDragOffset(F)V

    .line 472
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0}, Lru/cn/draggableview/Transformer;->updateViews()V

    .line 473
    invoke-direct {p0}, Lru/cn/draggableview/DraggableView;->dispatchOffsetChanged()V

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 3

    .prologue
    .line 290
    invoke-virtual {p0}, Lru/cn/draggableview/DraggableView;->isInEditMode()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lru/cn/draggableview/DraggableView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 291
    iget-object v2, p0, Lru/cn/draggableview/DraggableView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrY()I

    move-result v2

    int-to-float v1, v2

    .line 292
    .local v1, "y":F
    iget-object v2, p0, Lru/cn/draggableview/DraggableView;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    int-to-float v0, v2

    .line 294
    .local v0, "x":F
    iget-object v2, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v2, v0, v1}, Lru/cn/draggableview/Transformer;->computeOffsets(FF)V

    .line 295
    iget-object v2, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v2}, Lru/cn/draggableview/Transformer;->updateViews()V

    .line 296
    invoke-direct {p0}, Lru/cn/draggableview/DraggableView;->dispatchOffsetChanged()V

    .line 297
    invoke-virtual {p0}, Lru/cn/draggableview/DraggableView;->postInvalidate()V

    .line 299
    .end local v0    # "x":F
    .end local v1    # "y":F
    :cond_0
    return-void
.end method

.method public getFirstViewMinHeight()I
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0}, Lru/cn/draggableview/Transformer;->getMinHeight()I

    move-result v0

    return v0
.end method

.method public getFirstViewMinWidth()I
    .locals 1

    .prologue
    .line 496
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0}, Lru/cn/draggableview/Transformer;->getMinWidth()I

    move-result v0

    return v0
.end method

.method public isClosed()Z
    .locals 2

    .prologue
    .line 516
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0}, Lru/cn/draggableview/Transformer;->getHorizontalDragOffset()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLeftTheMiddle()Z
    .locals 4

    .prologue
    .line 478
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0}, Lru/cn/draggableview/Transformer;->getHorizontalDragOffset()F

    move-result v0

    float-to-double v0, v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMaximized()Z
    .locals 2

    .prologue
    .line 508
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0}, Lru/cn/draggableview/Transformer;->getVerticalDragOffset()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMinimized()Z
    .locals 2

    .prologue
    .line 512
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0}, Lru/cn/draggableview/Transformer;->getVerticalDragOffset()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lockInMax(Z)V
    .locals 0
    .param p1, "lock"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lru/cn/draggableview/DraggableView;->isLockInMax:Z

    .line 41
    return-void
.end method

.method public maximize(Z)V
    .locals 10
    .param p1, "animate"    # Z

    .prologue
    const/4 v7, 0x0

    .line 411
    if-eqz p1, :cond_0

    .line 412
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    sget-object v7, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_VERTICAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    invoke-virtual {v0, v7}, Lru/cn/draggableview/Transformer;->setDragOrientation(Lru/cn/draggableview/DraggableView$DragOrientation;)V

    .line 414
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->firstView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    float-to-int v1, v0

    .line 415
    .local v1, "startX":I
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->firstView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getY()F

    move-result v0

    float-to-int v2, v0

    .line 416
    .local v2, "startY":I
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0}, Lru/cn/draggableview/Transformer;->getXDistanceToMaximize()I

    move-result v3

    .line 417
    .local v3, "xDistance":I
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0}, Lru/cn/draggableview/Transformer;->getYDistanceToMaximize()I

    move-result v4

    .line 419
    .local v4, "yDistance":I
    mul-int v0, v3, v3

    mul-int v7, v4, v4

    add-int/2addr v0, v7

    int-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-int v6, v8

    .line 421
    .local v6, "distance":I
    invoke-direct {p0, v6}, Lru/cn/draggableview/DraggableView;->computeDuration(I)I

    move-result v5

    .line 422
    .local v5, "duration":I
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->scroller:Landroid/widget/Scroller;

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 423
    invoke-virtual {p0}, Lru/cn/draggableview/DraggableView;->invalidate()V

    .line 430
    .end local v1    # "startX":I
    .end local v2    # "startY":I
    .end local v3    # "xDistance":I
    .end local v4    # "yDistance":I
    .end local v5    # "duration":I
    .end local v6    # "distance":I
    :goto_0
    return-void

    .line 425
    :cond_0
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0, v7}, Lru/cn/draggableview/Transformer;->setHorizontalDragOffset(F)V

    .line 426
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0, v7}, Lru/cn/draggableview/Transformer;->setVerticalDragOffset(F)V

    .line 427
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0}, Lru/cn/draggableview/Transformer;->updateViews()V

    .line 428
    invoke-direct {p0}, Lru/cn/draggableview/DraggableView;->dispatchOffsetChanged()V

    goto :goto_0
.end method

.method public minimize(Z)V
    .locals 10
    .param p1, "animate"    # Z

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    .line 433
    if-eqz p1, :cond_1

    .line 434
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0}, Lru/cn/draggableview/Transformer;->getVerticalDragOffset()F

    move-result v0

    cmpl-float v0, v0, v8

    if-nez v0, :cond_0

    .line 435
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    sget-object v7, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_HORIZONTAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    invoke-virtual {v0, v7}, Lru/cn/draggableview/Transformer;->setDragOrientation(Lru/cn/draggableview/DraggableView$DragOrientation;)V

    .line 440
    :goto_0
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->firstView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    float-to-int v1, v0

    .line 441
    .local v1, "startX":I
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->firstView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getY()F

    move-result v0

    float-to-int v2, v0

    .line 442
    .local v2, "startY":I
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0}, Lru/cn/draggableview/Transformer;->getXDistanceToMinimize()I

    move-result v3

    .line 443
    .local v3, "xDistance":I
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0}, Lru/cn/draggableview/Transformer;->getYDistanceToMinimize()I

    move-result v4

    .line 445
    .local v4, "yDistance":I
    mul-int v0, v3, v3

    mul-int v7, v4, v4

    add-int/2addr v0, v7

    int-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-int v6, v8

    .line 446
    .local v6, "distance":I
    invoke-direct {p0, v6}, Lru/cn/draggableview/DraggableView;->computeDuration(I)I

    move-result v5

    .line 447
    .local v5, "duration":I
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->scroller:Landroid/widget/Scroller;

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 448
    invoke-virtual {p0}, Lru/cn/draggableview/DraggableView;->invalidate()V

    .line 455
    .end local v1    # "startX":I
    .end local v2    # "startY":I
    .end local v3    # "xDistance":I
    .end local v4    # "yDistance":I
    .end local v5    # "duration":I
    .end local v6    # "distance":I
    :goto_1
    return-void

    .line 437
    :cond_0
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    sget-object v7, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_VERTICAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    invoke-virtual {v0, v7}, Lru/cn/draggableview/Transformer;->setDragOrientation(Lru/cn/draggableview/DraggableView$DragOrientation;)V

    goto :goto_0

    .line 450
    :cond_1
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Lru/cn/draggableview/Transformer;->setHorizontalDragOffset(F)V

    .line 451
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0, v8}, Lru/cn/draggableview/Transformer;->setVerticalDragOffset(F)V

    .line 452
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0}, Lru/cn/draggableview/Transformer;->updateViews()V

    .line 453
    invoke-direct {p0}, Lru/cn/draggableview/DraggableView;->dispatchOffsetChanged()V

    goto :goto_1
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v2, 0x0

    .line 381
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 382
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->dragOrientation:Lru/cn/draggableview/DraggableView$DragOrientation;

    sget-object v1, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_VERTICAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    if-ne v0, v1, :cond_2

    .line 383
    invoke-direct {p0}, Lru/cn/draggableview/DraggableView;->isAboveTheMiddle()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 384
    invoke-virtual {p0, v2}, Lru/cn/draggableview/DraggableView;->maximize(Z)V

    .line 395
    :cond_0
    :goto_0
    return-void

    .line 386
    :cond_1
    invoke-virtual {p0, v2}, Lru/cn/draggableview/DraggableView;->minimize(Z)V

    goto :goto_0

    .line 388
    :cond_2
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->dragOrientation:Lru/cn/draggableview/DraggableView$DragOrientation;

    sget-object v1, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_HORIZONTAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    if-ne v0, v1, :cond_0

    .line 389
    invoke-virtual {p0}, Lru/cn/draggableview/DraggableView;->isLeftTheMiddle()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 390
    invoke-virtual {p0, v2}, Lru/cn/draggableview/DraggableView;->close(Z)V

    goto :goto_0

    .line 392
    :cond_3
    invoke-virtual {p0, v2}, Lru/cn/draggableview/DraggableView;->minimize(Z)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    .line 265
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 266
    invoke-virtual {p0}, Lru/cn/draggableview/DraggableView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 286
    :cond_0
    :goto_0
    return-void

    .line 270
    :cond_1
    iget-boolean v0, p0, Lru/cn/draggableview/DraggableView;->inflated:Z

    if-nez v0, :cond_0

    .line 271
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/draggableview/DraggableView;->inflated:Z

    .line 273
    iget v0, p0, Lru/cn/draggableview/DraggableView;->firstViewId:I

    invoke-direct {p0, v0}, Lru/cn/draggableview/DraggableView;->createAndAddView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/draggableview/DraggableView;->firstView:Landroid/view/View;

    .line 274
    iget v0, p0, Lru/cn/draggableview/DraggableView;->secondViewId:I

    invoke-direct {p0, v0}, Lru/cn/draggableview/DraggableView;->createAndAddView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/draggableview/DraggableView;->secondView:Landroid/view/View;

    .line 275
    iget v0, p0, Lru/cn/draggableview/DraggableView;->thirdViewId:I

    invoke-direct {p0, v0}, Lru/cn/draggableview/DraggableView;->createAndAddView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/draggableview/DraggableView;->thirdView:Landroid/view/View;

    .line 277
    new-instance v0, Lru/cn/draggableview/Transformer;

    iget-object v1, p0, Lru/cn/draggableview/DraggableView;->firstView:Landroid/view/View;

    iget-object v2, p0, Lru/cn/draggableview/DraggableView;->secondView:Landroid/view/View;

    iget-object v3, p0, Lru/cn/draggableview/DraggableView;->thirdView:Landroid/view/View;

    invoke-direct {v0, p0, v1, v2, v3}, Lru/cn/draggableview/Transformer;-><init>(Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V

    iput-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    .line 278
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    iget v1, p0, Lru/cn/draggableview/DraggableView;->firstViewMinWidth:I

    invoke-virtual {v0, v1}, Lru/cn/draggableview/Transformer;->setMinWidth(I)V

    .line 279
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    iget v1, p0, Lru/cn/draggableview/DraggableView;->firstViewMinHeight:I

    invoke-virtual {v0, v1}, Lru/cn/draggableview/Transformer;->setMinHeight(I)V

    .line 280
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    iget v1, p0, Lru/cn/draggableview/DraggableView;->firstViewMaxHeight:I

    invoke-virtual {v0, v1}, Lru/cn/draggableview/Transformer;->setMaxHeight(I)V

    .line 281
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    iget v1, p0, Lru/cn/draggableview/DraggableView;->firstViewMaxWidth:I

    invoke-virtual {v0, v1}, Lru/cn/draggableview/Transformer;->setMaxWidth(I)V

    .line 283
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    iget v1, p0, Lru/cn/draggableview/DraggableView;->marginRight:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lru/cn/draggableview/Transformer;->setMarginRight(F)V

    .line 284
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    iget v1, p0, Lru/cn/draggableview/DraggableView;->marginBottom:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lru/cn/draggableview/Transformer;->setMarginBottom(F)V

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x0

    .line 307
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    invoke-virtual {p0}, Lru/cn/draggableview/DraggableView;->isMinimized()Z

    move-result v4

    or-int v0, v3, v4

    .line 309
    .local v0, "handled":Z
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    .line 310
    .local v1, "x":F
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    .line 311
    .local v2, "y":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 326
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 313
    :pswitch_1
    iget-object v3, p0, Lru/cn/draggableview/DraggableView;->firstView:Landroid/view/View;

    invoke-static {v3, v1, v2}, Lru/cn/draggableview/DraggableView;->isViewUnder(Landroid/view/View;FF)Z

    move-result v3

    iput-boolean v3, p0, Lru/cn/draggableview/DraggableView;->firstViewIsCaptured:Z

    .line 314
    iget-boolean v3, p0, Lru/cn/draggableview/DraggableView;->firstViewIsCaptured:Z

    if-eqz v3, :cond_0

    .line 315
    iget-object v3, p0, Lru/cn/draggableview/DraggableView;->gestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v3, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 320
    :pswitch_2
    iget-boolean v3, p0, Lru/cn/draggableview/DraggableView;->firstViewIsCaptured:Z

    if-eqz v3, :cond_0

    .line 321
    iget-object v3, p0, Lru/cn/draggableview/DraggableView;->gestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v3, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    or-int/2addr v0, v3

    goto :goto_0

    .line 311
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 484
    invoke-virtual {p0}, Lru/cn/draggableview/DraggableView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 485
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 489
    :goto_0
    return-void

    .line 488
    :cond_0
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0}, Lru/cn/draggableview/Transformer;->updateViews()V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 331
    invoke-virtual {p0}, Lru/cn/draggableview/DraggableView;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 376
    :cond_0
    :goto_0
    return v0

    .line 335
    :cond_1
    const/4 v0, 0x0

    .line 336
    .local v0, "handled":Z
    iget-boolean v2, p0, Lru/cn/draggableview/DraggableView;->firstViewIsCaptured:Z

    if-eqz v2, :cond_2

    .line 337
    iget-object v2, p0, Lru/cn/draggableview/DraggableView;->gestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 340
    :cond_2
    if-nez v0, :cond_0

    .line 341
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 343
    :pswitch_1
    invoke-virtual {p0}, Lru/cn/draggableview/DraggableView;->isMinimized()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 344
    invoke-virtual {p0}, Lru/cn/draggableview/DraggableView;->isLeftTheMiddle()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 345
    invoke-virtual {p0, v3}, Lru/cn/draggableview/DraggableView;->close(Z)V

    goto :goto_0

    .line 347
    :cond_3
    invoke-virtual {p0, v3}, Lru/cn/draggableview/DraggableView;->minimize(Z)V

    goto :goto_0

    .line 350
    :cond_4
    invoke-direct {p0}, Lru/cn/draggableview/DraggableView;->isAboveTheMiddle()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 351
    invoke-virtual {p0, v3}, Lru/cn/draggableview/DraggableView;->maximize(Z)V

    goto :goto_0

    .line 353
    :cond_5
    invoke-virtual {p0, v3}, Lru/cn/draggableview/DraggableView;->minimize(Z)V

    goto :goto_0

    .line 359
    :pswitch_2
    iget-object v2, p0, Lru/cn/draggableview/DraggableView;->dragOrientation:Lru/cn/draggableview/DraggableView$DragOrientation;

    sget-object v3, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_VERTICAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    if-ne v2, v3, :cond_7

    .line 360
    invoke-direct {p0}, Lru/cn/draggableview/DraggableView;->isAboveTheMiddle()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 361
    invoke-virtual {p0, v1}, Lru/cn/draggableview/DraggableView;->maximize(Z)V

    goto :goto_0

    .line 363
    :cond_6
    invoke-virtual {p0, v1}, Lru/cn/draggableview/DraggableView;->minimize(Z)V

    goto :goto_0

    .line 365
    :cond_7
    iget-object v2, p0, Lru/cn/draggableview/DraggableView;->dragOrientation:Lru/cn/draggableview/DraggableView$DragOrientation;

    sget-object v3, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_HORIZONTAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    if-ne v2, v3, :cond_0

    .line 366
    invoke-virtual {p0}, Lru/cn/draggableview/DraggableView;->isLeftTheMiddle()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 367
    invoke-virtual {p0, v1}, Lru/cn/draggableview/DraggableView;->close(Z)V

    goto :goto_0

    .line 369
    :cond_8
    invoke-virtual {p0, v1}, Lru/cn/draggableview/DraggableView;->minimize(Z)V

    goto :goto_0

    .line 341
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setFirstViewMinHeight(I)V
    .locals 1
    .param p1, "h"    # I

    .prologue
    .line 500
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0, p1}, Lru/cn/draggableview/Transformer;->setMinHeight(I)V

    .line 501
    return-void
.end method

.method public setFirstViewMinWidth(I)V
    .locals 1
    .param p1, "w"    # I

    .prologue
    .line 504
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0, p1}, Lru/cn/draggableview/Transformer;->setMinWidth(I)V

    .line 505
    return-void
.end method

.method public setFragmentManager(Landroid/support/v4/app/FragmentManager;)V
    .locals 0
    .param p1, "fm"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 48
    iput-object p1, p0, Lru/cn/draggableview/DraggableView;->fm:Landroid/support/v4/app/FragmentManager;

    .line 49
    return-void
.end method

.method public setListener(Lru/cn/draggableview/DraggableView$DraggableViewListener;)V
    .locals 0
    .param p1, "l"    # Lru/cn/draggableview/DraggableView$DraggableViewListener;

    .prologue
    .line 89
    iput-object p1, p0, Lru/cn/draggableview/DraggableView;->listener:Lru/cn/draggableview/DraggableView$DraggableViewListener;

    .line 90
    return-void
.end method

.method public setSecondViewVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 524
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0, p1}, Lru/cn/draggableview/Transformer;->setSecondViewVisible(Z)V

    .line 525
    return-void
.end method

.method public setThirdViewVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 528
    iget-object v0, p0, Lru/cn/draggableview/DraggableView;->transformer:Lru/cn/draggableview/Transformer;

    invoke-virtual {v0, p1}, Lru/cn/draggableview/Transformer;->setThirdViewVisible(Z)V

    .line 529
    return-void
.end method
