.class public final Lru/cn/api/provider/TvContentProvider;
.super Landroid/content/ContentProvider;
.source "TvContentProvider.java"


# instance fields
.field private data:Lru/cn/api/provider/TvContentProviderData;

.field private uriMatcher:Landroid/content/UriMatcher;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method private buildCursorFromTelecast(Ljava/util/List;Landroid/support/v4/util/LongSparseArray;)Lru/cn/api/provider/cursor/TelecastItemCursor;
    .locals 32
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/tv/replies/Telecast;",
            ">;",
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Lru/cn/api/provider/TelecastLocationInfo;",
            ">;)",
            "Lru/cn/api/provider/cursor/TelecastItemCursor;"
        }
    .end annotation

    .prologue
    .line 211
    .local p1, "telecasts":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    .local p2, "records":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Lru/cn/api/provider/TelecastLocationInfo;>;"
    new-instance v3, Lru/cn/api/provider/cursor/TelecastItemCursor;

    invoke-direct {v3}, Lru/cn/api/provider/cursor/TelecastItemCursor;-><init>()V

    .line 212
    .local v3, "ret":Lru/cn/api/provider/cursor/TelecastItemCursor;
    if-nez p1, :cond_1

    .line 259
    :cond_0
    return-object v3

    .line 215
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long v28, v4, v6

    .line 216
    .local v28, "nowSec":J
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v31

    :cond_2
    :goto_0
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lru/cn/api/tv/replies/Telecast;

    .line 217
    .local v27, "telecast":Lru/cn/api/tv/replies/Telecast;
    move-object/from16 v0, v27

    iget-object v4, v0, Lru/cn/api/tv/replies/Telecast;->date:Lru/cn/api/tv/replies/DateTime;

    invoke-virtual {v4}, Lru/cn/api/tv/replies/DateTime;->toSeconds()J

    move-result-wide v4

    sub-long v20, v28, v4

    .line 219
    .local v20, "elapsedTime":J
    move-object/from16 v0, v27

    iget-wide v4, v0, Lru/cn/api/tv/replies/Telecast;->id:J

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lru/cn/api/provider/TelecastLocationInfo;

    .line 223
    .local v26, "locationInfo":Lru/cn/api/provider/TelecastLocationInfo;
    const-wide/16 v4, 0x0

    cmp-long v4, v20, v4

    if-gez v4, :cond_7

    const/16 v23, 0x1

    .line 224
    .local v23, "isFuture":Z
    :goto_1
    move-object/from16 v0, v27

    iget-wide v4, v0, Lru/cn/api/tv/replies/Telecast;->duration:J

    cmp-long v4, v20, v4

    if-lez v4, :cond_8

    const/16 v22, 0x1

    .line 225
    .local v22, "isArchived":Z
    :goto_2
    move-object/from16 v0, v27

    iget-wide v4, v0, Lru/cn/api/tv/replies/Telecast;->duration:J

    cmp-long v4, v20, v4

    if-gez v4, :cond_9

    const/16 v24, 0x1

    .line 227
    .local v24, "isLive":Z
    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    move-object/from16 v0, v27

    iget-object v5, v0, Lru/cn/api/tv/replies/Telecast;->channel:Lru/cn/api/tv/replies/Telecast$Channel;

    iget-wide v6, v5, Lru/cn/api/tv/replies/Telecast$Channel;->channelId:J

    invoke-virtual {v4, v6, v7}, Lru/cn/api/provider/TvContentProviderData;->getChannelFromCache(J)Lru/cn/api/provider/Channel;

    move-result-object v2

    .line 228
    .local v2, "channel":Lru/cn/api/provider/Channel;
    if-eqz v26, :cond_a

    const/16 v25, 0x1

    .line 230
    .local v25, "isLocationExist":Z
    :goto_4
    if-nez v23, :cond_3

    if-eqz v24, :cond_3

    if-nez v2, :cond_4

    :cond_3
    if-eqz v22, :cond_2

    if-eqz v25, :cond_2

    .line 234
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    move-object/from16 v0, v27

    iget-object v5, v0, Lru/cn/api/tv/replies/Telecast;->channel:Lru/cn/api/tv/replies/Telecast$Channel;

    iget-wide v6, v5, Lru/cn/api/tv/replies/Telecast$Channel;->channelId:J

    invoke-virtual {v4, v6, v7}, Lru/cn/api/provider/TvContentProviderData;->getChannelTitle(J)Ljava/lang/String;

    move-result-object v12

    .line 235
    .local v12, "channelTitle":Ljava/lang/String;
    move-object/from16 v0, v27

    iget-object v4, v0, Lru/cn/api/tv/replies/Telecast;->channel:Lru/cn/api/tv/replies/Telecast$Channel;

    iget-wide v13, v4, Lru/cn/api/tv/replies/Telecast$Channel;->channelId:J

    .line 237
    .local v13, "channelId":J
    const/4 v15, 0x0

    .line 238
    .local v15, "isDenied":I
    if-eqz v25, :cond_5

    move-object/from16 v0, v26

    iget-object v4, v0, Lru/cn/api/provider/TelecastLocationInfo;->status:Lru/cn/api/medialocator/replies/Rip$RipStatus;

    sget-object v5, Lru/cn/api/medialocator/replies/Rip$RipStatus;->ACCESS_DENIED:Lru/cn/api/medialocator/replies/Rip$RipStatus;

    if-ne v4, v5, :cond_5

    .line 239
    const/4 v15, 0x1

    .line 242
    :cond_5
    if-eqz v24, :cond_6

    if-eqz v2, :cond_6

    .line 243
    iget-object v4, v2, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lru/cn/api/iptv/replies/MediaLocation;

    .line 244
    .local v19, "defaultLocation":Lru/cn/api/iptv/replies/MediaLocation;
    move-object/from16 v0, v19

    iget-object v4, v0, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;

    sget-object v5, Lru/cn/api/iptv/replies/MediaLocation$Access;->denied:Lru/cn/api/iptv/replies/MediaLocation$Access;

    if-ne v4, v5, :cond_6

    .line 245
    const/4 v15, 0x1

    .line 249
    .end local v19    # "defaultLocation":Lru/cn/api/iptv/replies/MediaLocation;
    :cond_6
    sget-object v4, Lru/cn/api/tv/replies/TelecastImage$Profile;->IMAGE:Lru/cn/api/tv/replies/TelecastImage$Profile;

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Lru/cn/api/tv/replies/Telecast;->getImage(Lru/cn/api/tv/replies/TelecastImage$Profile;)Lru/cn/api/tv/replies/TelecastImage;

    move-result-object v30

    .line 250
    .local v30, "telecastImage":Lru/cn/api/tv/replies/TelecastImage;
    if-eqz v30, :cond_b

    move-object/from16 v0, v30

    iget-object v11, v0, Lru/cn/api/tv/replies/TelecastImage;->location:Ljava/lang/String;

    .line 251
    .local v11, "image":Ljava/lang/String;
    :goto_5
    move-object/from16 v0, v27

    iget v0, v0, Lru/cn/api/tv/replies/Telecast;->viewsCount:I

    move/from16 v17, v0

    .line 253
    .local v17, "viewsCount":I
    move-object/from16 v0, v27

    iget-wide v4, v0, Lru/cn/api/tv/replies/Telecast;->id:J

    move-object/from16 v0, v27

    iget-object v6, v0, Lru/cn/api/tv/replies/Telecast;->date:Lru/cn/api/tv/replies/DateTime;

    invoke-virtual {v6}, Lru/cn/api/tv/replies/DateTime;->toSeconds()J

    move-result-wide v6

    move-object/from16 v0, v27

    iget-wide v8, v0, Lru/cn/api/tv/replies/Telecast;->duration:J

    move-object/from16 v0, v27

    iget-object v10, v0, Lru/cn/api/tv/replies/Telecast;->title:Ljava/lang/String;

    .line 254
    invoke-virtual/range {v27 .. v27}, Lru/cn/api/tv/replies/Telecast;->isHighlight()Z

    move-result v16

    move-object/from16 v0, v27

    iget-object v0, v0, Lru/cn/api/tv/replies/Telecast;->description:Ljava/lang/String;

    move-object/from16 v18, v0

    .line 253
    invoke-virtual/range {v3 .. v18}, Lru/cn/api/provider/cursor/TelecastItemCursor;->addRow(JJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JIZILjava/lang/String;)V

    goto/16 :goto_0

    .line 223
    .end local v2    # "channel":Lru/cn/api/provider/Channel;
    .end local v11    # "image":Ljava/lang/String;
    .end local v12    # "channelTitle":Ljava/lang/String;
    .end local v13    # "channelId":J
    .end local v15    # "isDenied":I
    .end local v17    # "viewsCount":I
    .end local v22    # "isArchived":Z
    .end local v23    # "isFuture":Z
    .end local v24    # "isLive":Z
    .end local v25    # "isLocationExist":Z
    .end local v30    # "telecastImage":Lru/cn/api/tv/replies/TelecastImage;
    :cond_7
    const/16 v23, 0x0

    goto/16 :goto_1

    .line 224
    .restart local v23    # "isFuture":Z
    :cond_8
    const/16 v22, 0x0

    goto/16 :goto_2

    .line 225
    .restart local v22    # "isArchived":Z
    :cond_9
    const/16 v24, 0x0

    goto/16 :goto_3

    .line 228
    .restart local v2    # "channel":Lru/cn/api/provider/Channel;
    .restart local v24    # "isLive":Z
    :cond_a
    const/16 v25, 0x0

    goto/16 :goto_4

    .line 250
    .restart local v12    # "channelTitle":Ljava/lang/String;
    .restart local v13    # "channelId":J
    .restart local v15    # "isDenied":I
    .restart local v25    # "isLocationExist":Z
    .restart local v30    # "telecastImage":Lru/cn/api/tv/replies/TelecastImage;
    :cond_b
    const/4 v11, 0x0

    goto :goto_5
.end method

.method private formattedAdTags(Lru/cn/api/tv/replies/Telecast;)Ljava/lang/String;
    .locals 2
    .param p1, "telecast"    # Lru/cn/api/tv/replies/Telecast;

    .prologue
    .line 1256
    if-eqz p1, :cond_0

    iget-object v0, p1, Lru/cn/api/tv/replies/Telecast;->adTags:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1257
    :cond_0
    const-string v0, ""

    .line 1259
    :goto_0
    return-object v0

    :cond_1
    const-string v0, ","

    iget-object v1, p1, Lru/cn/api/tv/replies/Telecast;->adTags:Ljava/util/List;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getCachedLocations(Ljava/util/List;)Landroid/support/v4/util/LongSparseArray;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/tv/replies/Telecast;",
            ">;)",
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Lru/cn/api/provider/TelecastLocationInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200
    .local p1, "telecasts":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    new-array v0, v3, [J

    .line 201
    .local v0, "ids":[J
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 202
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/api/tv/replies/Telecast;

    .line 203
    .local v2, "telecast":Lru/cn/api/tv/replies/Telecast;
    iget-wide v4, v2, Lru/cn/api/tv/replies/Telecast;->id:J

    aput-wide v4, v0, v1

    .line 201
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 206
    .end local v2    # "telecast":Lru/cn/api/tv/replies/Telecast;
    :cond_0
    iget-object v3, p0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v3, v0}, Lru/cn/api/provider/TvContentProviderData;->getCachedLocations([J)Landroid/support/v4/util/LongSparseArray;

    move-result-object v3

    return-object v3
.end method

.method private populateChannelCursor(Lru/cn/api/provider/cursor/ChannelCursor;Lru/cn/api/provider/Channel;Z)V
    .locals 13
    .param p1, "cursor"    # Lru/cn/api/provider/cursor/ChannelCursor;
    .param p2, "channel"    # Lru/cn/api/provider/Channel;
    .param p3, "reload"    # Z

    .prologue
    .line 1213
    if-nez p2, :cond_0

    .line 1253
    :goto_0
    return-void

    .line 1216
    :cond_0
    iget-object v1, p2, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/iptv/replies/MediaLocation;

    .line 1217
    .local v0, "defaultLocation":Lru/cn/api/iptv/replies/MediaLocation;
    iget-wide v4, v0, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    .line 1219
    .local v4, "territoryId":J
    const/4 v8, 0x0

    .line 1220
    .local v8, "info":Lru/cn/api/tv/replies/ChannelInfo;
    const/4 v9, 0x0

    .line 1221
    .local v9, "currentTelecast":Lru/cn/api/tv/replies/Telecast;
    if-eqz p3, :cond_1

    .line 1223
    :try_start_0
    iget-object v1, p0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    iget-wide v2, p2, Lru/cn/api/provider/Channel;->channelId:J

    invoke-virtual {v1, v2, v3, v4, v5}, Lru/cn/api/provider/TvContentProviderData;->getChannelInfo(JJ)Lru/cn/api/tv/replies/ChannelInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 1229
    :goto_1
    :try_start_1
    iget-object v1, p0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    iget-wide v2, p2, Lru/cn/api/provider/Channel;->channelId:J

    invoke-virtual {v1, v2, v3, v4, v5}, Lru/cn/api/provider/TvContentProviderData;->getCurrentTelecast(JJ)Lru/cn/api/tv/replies/Telecast;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v9

    .line 1238
    :goto_2
    const/4 v10, 0x0

    .line 1240
    .local v10, "isFavourite":Z
    :try_start_2
    iget-object v1, p0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    iget-wide v2, p2, Lru/cn/api/provider/Channel;->channelId:J

    invoke-virtual {v1, v2, v3}, Lru/cn/api/provider/TvContentProviderData;->isFavourite(J)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result v10

    .line 1245
    :goto_3
    const/4 v11, 0x0

    .line 1247
    .local v11, "allowPurchase":Z
    :try_start_3
    iget-object v1, p0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    iget-wide v2, v0, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    invoke-virtual {v1, v2, v3}, Lru/cn/api/provider/TvContentProviderData;->allowPurchases(J)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move-result v11

    :goto_4
    move-object v6, p1

    move-object v7, p2

    .line 1252
    invoke-virtual/range {v6 .. v11}, Lru/cn/api/provider/cursor/ChannelCursor;->addRow(Lru/cn/api/provider/Channel;Lru/cn/api/tv/replies/ChannelInfo;Lru/cn/api/tv/replies/Telecast;ZZ)V

    goto :goto_0

    .line 1224
    .end local v10    # "isFavourite":Z
    .end local v11    # "allowPurchase":Z
    :catch_0
    move-exception v12

    .line 1225
    .local v12, "e":Ljava/lang/Exception;
    invoke-static {v12}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1230
    .end local v12    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v12

    .line 1231
    .restart local v12    # "e":Ljava/lang/Exception;
    invoke-static {v12}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 1234
    .end local v12    # "e":Ljava/lang/Exception;
    :cond_1
    iget-object v1, p0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    iget-wide v2, p2, Lru/cn/api/provider/Channel;->channelId:J

    invoke-virtual {v1, v2, v3, v4, v5}, Lru/cn/api/provider/TvContentProviderData;->getStoredChannelInfo(JJ)Lru/cn/api/tv/replies/ChannelInfo;

    move-result-object v8

    .line 1235
    iget-object v1, p0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    iget-wide v2, p2, Lru/cn/api/provider/Channel;->channelId:J

    const/4 v6, 0x1

    invoke-virtual/range {v1 .. v6}, Lru/cn/api/provider/TvContentProviderData;->getStoredCurrentTelecast(JJZ)Lru/cn/api/tv/replies/Telecast;

    move-result-object v9

    goto :goto_2

    .line 1241
    .restart local v10    # "isFavourite":Z
    :catch_2
    move-exception v12

    .line 1242
    .restart local v12    # "e":Ljava/lang/Exception;
    const-string v1, "TvContentProvider"

    const-string v2, "Unable to get favorite state"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 1248
    .end local v12    # "e":Ljava/lang/Exception;
    .restart local v11    # "allowPurchase":Z
    :catch_3
    move-exception v12

    .line 1249
    .restart local v12    # "e":Ljava/lang/Exception;
    const-string v1, "TvContentProvider"

    const-string v2, "Unable to get allow purchase status"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 1081
    const-string v8, "TvContentProvider"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "delete, "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1083
    new-instance v8, Landroid/net/Uri$Builder;

    invoke-direct {v8}, Landroid/net/Uri$Builder;-><init>()V

    const-string v9, "content"

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "ru.cn.api.tv"

    .line 1084
    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "last_channels"

    .line 1085
    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 1087
    .local v3, "lastChannelsUri":Landroid/net/Uri;
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->channels()Landroid/net/Uri;

    move-result-object v1

    .line 1089
    .local v1, "channelsUri":Landroid/net/Uri;
    iget-object v8, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v8, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    .line 1155
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Wrong URI: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1092
    :sswitch_0
    const/4 v8, 0x0

    aget-object v7, p3, v8

    .line 1093
    .local v7, "url":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1095
    .local v6, "rez":Z
    :try_start_0
    iget-object v8, p0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v8, v7}, Lru/cn/api/provider/TvContentProviderData;->delUserPlaylist(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    .line 1100
    :goto_0
    if-eqz v6, :cond_0

    .line 1101
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, p1, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1104
    iget-object v8, p0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lru/cn/api/provider/TvContentProviderData;->invalidate(Z)V

    .line 1106
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v1, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1107
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v3, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1109
    :cond_0
    const/4 v8, 0x1

    .line 1151
    .end local v6    # "rez":Z
    .end local v7    # "url":Ljava/lang/String;
    :goto_1
    return v8

    .line 1096
    .restart local v6    # "rez":Z
    .restart local v7    # "url":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 1097
    .local v2, "e":Ljava/lang/Exception;
    invoke-static {v2}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1113
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v6    # "rez":Z
    .end local v7    # "url":Ljava/lang/String;
    :sswitch_1
    iget-object v8, p0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lru/cn/api/provider/TvContentProviderData;->invalidate(Z)V

    .line 1114
    invoke-static {}, Lru/cn/ad/AdsManager;->setNeedsUpdate()V

    .line 1116
    invoke-static {}, Lru/cn/api/ServiceLocator;->clearWhereAmI()V

    .line 1118
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v1, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1119
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v3, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1120
    const/4 v8, 0x1

    goto :goto_1

    .line 1123
    :sswitch_2
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    .line 1125
    .local v4, "id":J
    :try_start_1
    iget-object v8, p0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v8, v4, v5}, Lru/cn/api/provider/TvContentProviderData;->delFavouriteChannel(J)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1126
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v1, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1127
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v3, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1128
    const/4 v8, 0x1

    goto :goto_1

    .line 1131
    :catch_1
    move-exception v2

    .line 1132
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-static {v2}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .line 1134
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    const/4 v8, 0x0

    goto :goto_1

    .line 1138
    .end local v4    # "id":J
    :sswitch_3
    :try_start_2
    iget-object v8, p0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v8, p3}, Lru/cn/api/provider/TvContentProviderData;->delAllowedChannel([Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1139
    new-instance v8, Landroid/net/Uri$Builder;

    invoke-direct {v8}, Landroid/net/Uri$Builder;-><init>()V

    const-string v9, "content"

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "ru.cn.api.tv"

    .line 1140
    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    sget-object v9, Lru/cn/api/provider/TvContentProviderContract;->queryAllowedChannels:Ljava/lang/String;

    .line 1141
    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 1142
    .local v0, "allowedChannelsUri":Landroid/net/Uri;
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v1, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1143
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v3, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1144
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v0, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 1145
    const/4 v8, 0x1

    goto/16 :goto_1

    .line 1148
    .end local v0    # "allowedChannelsUri":Landroid/net/Uri;
    :catch_2
    move-exception v2

    .line 1149
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-static {v2}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .line 1151
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 1089
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0xd -> :sswitch_1
        0xe -> :sswitch_2
        0x1c -> :sswitch_3
    .end sparse-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1209
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 12
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v8, 0x0

    .line 1000
    const-string v9, "TvContentProvider"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "insert, "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1002
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->channels()Landroid/net/Uri;

    move-result-object v1

    .line 1003
    .local v1, "channelsUri":Landroid/net/Uri;
    new-instance v9, Landroid/net/Uri$Builder;

    invoke-direct {v9}, Landroid/net/Uri$Builder;-><init>()V

    const-string v10, "content"

    invoke-virtual {v9, v10}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v9

    const-string v10, "ru.cn.api.tv"

    .line 1004
    invoke-virtual {v9, v10}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v9

    const-string v10, "last_channels"

    .line 1005
    invoke-virtual {v9, v10}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 1007
    .local v3, "lastChannelsUri":Landroid/net/Uri;
    iget-object v9, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v9, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    .line 1075
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Wrong URI: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1010
    :sswitch_0
    const/4 v7, 0x0

    .line 1012
    .local v7, "res":Z
    :try_start_0
    iget-object v9, p0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v9, p2}, Lru/cn/api/provider/TvContentProviderData;->addUserPlaylist(Landroid/content/ContentValues;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    .line 1017
    :goto_0
    if-eqz v7, :cond_0

    .line 1019
    iget-object v9, p0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lru/cn/api/provider/TvContentProviderData;->invalidate(Z)V

    .line 1021
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-virtual {v9, v1, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1022
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-virtual {v9, v3, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1024
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-virtual {v9, p1, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1071
    .end local v7    # "res":Z
    .end local p1    # "uri":Landroid/net/Uri;
    :goto_1
    return-object p1

    .line 1013
    .restart local v7    # "res":Z
    .restart local p1    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v2

    .line 1014
    .local v2, "e":Ljava/lang/Exception;
    invoke-static {v2}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0

    .end local v2    # "e":Ljava/lang/Exception;
    :cond_0
    move-object p1, v8

    .line 1027
    goto :goto_1

    .line 1030
    .end local v7    # "res":Z
    :sswitch_1
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    .line 1032
    .local v4, "id":J
    :try_start_1
    iget-object v9, p0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v9, v4, v5}, Lru/cn/api/provider/TvContentProviderData;->addFavouriteChannel(J)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1033
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v1, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1034
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v3, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 1039
    :catch_1
    move-exception v2

    .line 1040
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-static {v2}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    move-object p1, v8

    .line 1042
    goto :goto_1

    .line 1047
    .end local v4    # "id":J
    :sswitch_2
    :try_start_2
    iget-object v9, p0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v9, p2}, Lru/cn/api/provider/TvContentProviderData;->addAllowedChannel(Landroid/content/ContentValues;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1048
    new-instance v9, Landroid/net/Uri$Builder;

    invoke-direct {v9}, Landroid/net/Uri$Builder;-><init>()V

    const-string v10, "content"

    invoke-virtual {v9, v10}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v9

    const-string v10, "ru.cn.api.tv"

    .line 1049
    invoke-virtual {v9, v10}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v9

    sget-object v10, Lru/cn/api/provider/TvContentProviderContract;->queryAllowedChannels:Ljava/lang/String;

    .line 1050
    invoke-virtual {v9, v10}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 1051
    .local v0, "allowedChannelsUri":Landroid/net/Uri;
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v1, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1052
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v3, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1053
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v0, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 1056
    .end local v0    # "allowedChannelsUri":Landroid/net/Uri;
    :catch_2
    move-exception v2

    .line 1057
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-static {v2}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    move-object p1, v8

    .line 1059
    goto :goto_1

    .line 1063
    :sswitch_3
    const-string v9, "pin_code"

    invoke-virtual {p2, v9}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1065
    .local v6, "pinCode":Ljava/lang/String;
    :try_start_3
    iget-object v9, p0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v9, v6}, Lru/cn/api/provider/TvContentProviderData;->addPinCode(Ljava/lang/String;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 1070
    :goto_2
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-virtual {v9, p1, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_1

    .line 1066
    :catch_3
    move-exception v2

    .line 1067
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-static {v2}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 1007
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0xe -> :sswitch_1
        0x12 -> :sswitch_3
        0x1c -> :sswitch_2
    .end sparse-switch
.end method

.method public onCreate()Z
    .locals 6

    .prologue
    const/16 v5, 0x1c

    const/4 v4, 0x1

    .line 92
    const-string v0, "TvContentProvider"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    new-instance v0, Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lru/cn/api/provider/TvContentProviderData;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    .line 96
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    .line 98
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "playlists"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 101
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "channels"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 103
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "channels/next/*"

    const/4 v3, 0x6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 106
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "channels/prev/*"

    const/4 v3, 0x7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 109
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "channels/locations/*"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 112
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "channels/current_telecasts"

    const/16 v3, 0x18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 116
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "channels/*"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 119
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "navigation/#"

    const/16 v3, 0x19

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 122
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "last_channels"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 125
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "last_channel"

    const/16 v3, 0x1a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 128
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "dates/#"

    const/16 v3, 0x8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 131
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "schedule/locations/#"

    const/16 v3, 0xc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 134
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "schedule/#"

    const/16 v3, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 136
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "schedule/time/#"

    const/16 v3, 0xb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 139
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "schedule/*/#"

    const/16 v3, 0x9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 143
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "clear_cache"

    const/16 v3, 0xd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 146
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "favourite_channels/*"

    const/16 v3, 0xe

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 150
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    sget-object v2, Lru/cn/api/provider/TvContentProviderContract;->queryAllowedChannels:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 154
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lru/cn/api/provider/TvContentProviderContract;->queryAllowedChannels:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 158
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "contractor/#"

    const/16 v3, 0xf

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 160
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "contractor"

    const/16 v3, 0x10

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 162
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "content_providers"

    const/16 v3, 0x1b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 166
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "channel_by_number/#"

    const/16 v3, 0x11

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 169
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "user_pin_code"

    const/16 v3, 0x12

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 172
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "rubricator"

    const/16 v3, 0x13

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 175
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "rubricator/#"

    const/16 v3, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 179
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "rubricator/related/#"

    const/16 v3, 0x17

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 185
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "rubricator/items/#"

    const/16 v3, 0x15

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 191
    iget-object v0, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.tv"

    const-string v2, "rubricator/search_items/#"

    const/16 v3, 0x16

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 196
    return v4
.end method

.method public declared-synchronized query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 119
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 266
    monitor-enter p0

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lru/cn/domain/KidsObject;->isKidsMode(Landroid/content/Context;)Z

    move-result v78

    .line 268
    .local v78, "isKidsMode":Z
    const-string v5, "TvContentProvider"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "query, "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v5, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 994
    :pswitch_0
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Wrong URI: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 266
    .end local v78    # "isKidsMode":Z
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 272
    .restart local v78    # "isKidsMode":Z
    :pswitch_1
    const/4 v5, 0x3

    :try_start_1
    new-array v0, v5, [Ljava/lang/String;

    move-object/from16 v52, v0

    const/4 v5, 0x0

    const-string v10, "_id"

    aput-object v10, v52, v5

    const/4 v5, 0x1

    const-string v10, "title"

    aput-object v10, v52, v5

    const/4 v5, 0x2

    const-string v10, "location"

    aput-object v10, v52, v5

    .line 273
    .local v52, "channelFields":[Ljava/lang/String;
    new-instance v15, Lru/cn/api/provider/cursor/MatrixCursorEx;

    move-object/from16 v0, v52

    invoke-direct {v15, v0}, Lru/cn/api/provider/cursor/MatrixCursorEx;-><init>([Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 275
    .local v15, "c":Landroid/database/MatrixCursor;
    :try_start_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v5}, Lru/cn/api/provider/TvContentProviderData;->getUserPlayists()Ljava/util/Map;

    move-result-object v94

    .line 276
    .local v94, "playlistsMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface/range {v94 .. v94}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v109

    .line 277
    .local v109, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface/range {v109 .. v109}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v66

    check-cast v66, Ljava/util/Map$Entry;

    .line 278
    .local v66, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v12, 0x0

    const/16 v16, 0x0

    aput-object v16, v10, v12

    const/4 v12, 0x1

    invoke-interface/range {v66 .. v66}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v16

    aput-object v16, v10, v12

    const/4 v12, 0x2

    invoke-interface/range {v66 .. v66}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v16

    aput-object v16, v10, v12

    invoke-virtual {v15, v10}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 280
    .end local v66    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v94    # "playlistsMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v109    # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    :catch_0
    move-exception v65

    .line 281
    .local v65, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-static/range {v65 .. v65}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .line 283
    .end local v65    # "e":Ljava/lang/Exception;
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v15, v5, v0}, Landroid/database/MatrixCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object/from16 v26, v15

    .line 990
    .end local v15    # "c":Landroid/database/MatrixCursor;
    .end local v52    # "channelFields":[Ljava/lang/String;
    :cond_1
    :goto_1
    monitor-exit p0

    return-object v26

    .line 289
    :pswitch_2
    :try_start_4
    const-string v5, "age_filter"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    .line 290
    .local v48, "ageFilter":Ljava/lang/String;
    if-nez v48, :cond_3

    const/16 v47, -0x1

    .line 291
    .local v47, "age":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    move/from16 v0, v47

    invoke-virtual {v5, v0}, Lru/cn/api/provider/TvContentProviderData;->setAge(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 293
    const/16 v55, 0x0

    .line 295
    .local v55, "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    if-nez p3, :cond_4

    .line 296
    :try_start_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v5}, Lru/cn/api/provider/TvContentProviderData;->getChannels()Ljava/util/List;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v55

    .line 312
    :cond_2
    :goto_3
    :try_start_6
    new-instance v15, Lru/cn/api/provider/cursor/ChannelCursor;

    invoke-direct {v15}, Lru/cn/api/provider/cursor/ChannelCursor;-><init>()V

    .line 313
    .local v15, "c":Lru/cn/api/provider/cursor/ChannelCursor;
    if-eqz v55, :cond_9

    .line 314
    invoke-interface/range {v55 .. v55}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v49

    check-cast v49, Lru/cn/api/provider/Channel;

    .line 315
    .local v49, "channel":Lru/cn/api/provider/Channel;
    const/4 v10, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v49

    invoke-direct {v0, v15, v1, v10}, Lru/cn/api/provider/TvContentProvider;->populateChannelCursor(Lru/cn/api/provider/cursor/ChannelCursor;Lru/cn/api/provider/Channel;Z)V

    goto :goto_4

    .line 290
    .end local v15    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    .end local v47    # "age":I
    .end local v49    # "channel":Lru/cn/api/provider/Channel;
    .end local v55    # "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    :cond_3
    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v47

    goto :goto_2

    .line 298
    .restart local v47    # "age":I
    .restart local v55    # "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    :cond_4
    :try_start_7
    const-string v5, "porno"

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 299
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v5}, Lru/cn/api/provider/TvContentProviderData;->getPornoChannels()Ljava/util/List;

    move-result-object v55

    goto :goto_3

    .line 300
    :cond_5
    const-string v5, "hd"

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 301
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v5}, Lru/cn/api/provider/TvContentProviderData;->getHdChannels()Ljava/util/List;

    move-result-object v55

    goto :goto_3

    .line 302
    :cond_6
    const-string v5, "favourite"

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 303
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v5}, Lru/cn/api/provider/TvContentProviderData;->getFavouriteChannels()Ljava/util/List;

    move-result-object v55

    goto :goto_3

    .line 304
    :cond_7
    const-string v5, "intersections"

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 305
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v5}, Lru/cn/api/provider/TvContentProviderData;->getIntersectionsChannels()Ljava/util/List;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v55

    goto :goto_3

    .line 308
    :catch_1
    move-exception v65

    .line 309
    .restart local v65    # "e":Ljava/lang/Exception;
    :try_start_8
    invoke-static/range {v65 .. v65}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_3

    .line 319
    .end local v65    # "e":Ljava/lang/Exception;
    .restart local v15    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v5}, Lru/cn/api/provider/TvContentProviderData;->rubricatorIsValid()Z

    move-result v5

    if-nez v5, :cond_9

    if-nez p3, :cond_9

    .line 320
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->rubricatorUri()Landroid/net/Uri;

    move-result-object v103

    .line 321
    .local v103, "rubricatorUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const/4 v10, 0x0

    move-object/from16 v0, v103

    invoke-virtual {v5, v0, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 325
    .end local v103    # "rubricatorUri":Landroid/net/Uri;
    :cond_9
    invoke-virtual {v15}, Lru/cn/api/provider/cursor/ChannelCursor;->getCount()I

    move-result v5

    if-nez v5, :cond_a

    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v5}, Lru/cn/api/provider/TvContentProviderData;->getChannelsCount()I

    move-result v5

    if-nez v5, :cond_a

    .line 326
    new-instance v67, Landroid/os/Bundle;

    invoke-direct/range {v67 .. v67}, Landroid/os/Bundle;-><init>()V

    .line 327
    .local v67, "extras":Landroid/os/Bundle;
    const-string v5, "error"

    const-string v10, "error"

    move-object/from16 v0, v67

    invoke-virtual {v0, v5, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    move-object/from16 v0, v67

    invoke-virtual {v15, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->setExtras(Landroid/os/Bundle;)V

    .line 331
    .end local v67    # "extras":Landroid/os/Bundle;
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v15, v5, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    move-object/from16 v26, v15

    .line 332
    goto/16 :goto_1

    .line 336
    .end local v15    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    .end local v47    # "age":I
    .end local v48    # "ageFilter":Ljava/lang/String;
    .end local v55    # "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    :pswitch_3
    new-instance v15, Lru/cn/api/provider/cursor/ChannelCursor;

    invoke-direct {v15}, Lru/cn/api/provider/cursor/ChannelCursor;-><init>()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 338
    .restart local v15    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    :try_start_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v5}, Lru/cn/api/provider/TvContentProviderData;->getAllowedChannels()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v49

    check-cast v49, Lru/cn/api/provider/Channel;

    .line 339
    .restart local v49    # "channel":Lru/cn/api/provider/Channel;
    const/4 v10, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v49

    invoke-direct {v0, v15, v1, v10}, Lru/cn/api/provider/TvContentProvider;->populateChannelCursor(Lru/cn/api/provider/cursor/ChannelCursor;Lru/cn/api/provider/Channel;Z)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_5

    .line 341
    .end local v49    # "channel":Lru/cn/api/provider/Channel;
    :catch_2
    move-exception v65

    .line 342
    .restart local v65    # "e":Ljava/lang/Exception;
    :try_start_a
    invoke-static/range {v65 .. v65}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .line 344
    .end local v65    # "e":Ljava/lang/Exception;
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v15, v5, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    move-object/from16 v26, v15

    .line 345
    goto/16 :goto_1

    .line 349
    .end local v15    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    :pswitch_4
    new-instance v11, Lru/cn/api/provider/cursor/TelecastItemCursor;

    invoke-direct {v11}, Lru/cn/api/provider/cursor/TelecastItemCursor;-><init>()V

    .line 350
    .local v11, "cursor":Lru/cn/api/provider/cursor/TelecastItemCursor;
    if-eqz p4, :cond_14

    .line 351
    new-instance v97, Ljava/util/HashMap;

    invoke-direct/range {v97 .. v97}, Ljava/util/HashMap;-><init>()V

    .line 353
    .local v97, "requestsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/util/List<Ljava/lang/Long;>;>;"
    new-instance v83, Ljava/util/HashMap;

    move-object/from16 v0, p4

    array-length v5, v0

    move-object/from16 v0, v83

    invoke-direct {v0, v5}, Ljava/util/HashMap;-><init>(I)V

    .line 354
    .local v83, "loadedInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Lru/cn/api/tv/replies/Telecast;>;"
    move-object/from16 v0, p4

    array-length v0, v0

    move/from16 v16, v0

    const/4 v5, 0x0

    move v12, v5

    :goto_6
    move/from16 v0, v16

    if-ge v12, v0, :cond_f

    aget-object v53, p4, v12

    .line 355
    .local v53, "channelIdString":Ljava/lang/String;
    invoke-static/range {v53 .. v53}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move-result-wide v6

    .line 356
    .local v6, "channelId":J
    const/16 v49, 0x0

    .line 358
    .restart local v49    # "channel":Lru/cn/api/provider/Channel;
    :try_start_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v5, v6, v7}, Lru/cn/api/provider/TvContentProviderData;->getChannel(J)Lru/cn/api/provider/Channel;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move-result-object v49

    .line 362
    :goto_7
    if-nez v49, :cond_c

    .line 354
    :goto_8
    add-int/lit8 v5, v12, 0x1

    move v12, v5

    goto :goto_6

    .line 365
    :cond_c
    :try_start_c
    move-object/from16 v0, v49

    iget-object v5, v0, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    const/4 v10, 0x0

    invoke-interface {v5, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v86

    check-cast v86, Lru/cn/api/iptv/replies/MediaLocation;

    .line 366
    .local v86, "location":Lru/cn/api/iptv/replies/MediaLocation;
    move-object/from16 v0, v86

    iget-wide v8, v0, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    .line 368
    .local v8, "territoryId":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    const/4 v10, 0x0

    invoke-virtual/range {v5 .. v10}, Lru/cn/api/provider/TvContentProviderData;->getStoredCurrentTelecast(JJZ)Lru/cn/api/tv/replies/Telecast;

    move-result-object v111

    .line 369
    .local v111, "telecast":Lru/cn/api/tv/replies/Telecast;
    if-eqz v111, :cond_d

    .line 370
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v83

    move-object/from16 v1, v111

    invoke-interface {v0, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    .line 372
    :cond_d
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v97

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v74

    check-cast v74, Ljava/util/List;

    .line 373
    .local v74, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    if-nez v74, :cond_e

    .line 374
    new-instance v74, Ljava/util/ArrayList;

    .end local v74    # "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-direct/range {v74 .. v74}, Ljava/util/ArrayList;-><init>()V

    .line 375
    .restart local v74    # "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v97

    move-object/from16 v1, v74

    invoke-virtual {v0, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377
    :cond_e
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v74

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 381
    .end local v6    # "channelId":J
    .end local v8    # "territoryId":J
    .end local v49    # "channel":Lru/cn/api/provider/Channel;
    .end local v53    # "channelIdString":Ljava/lang/String;
    .end local v74    # "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v86    # "location":Lru/cn/api/iptv/replies/MediaLocation;
    .end local v111    # "telecast":Lru/cn/api/tv/replies/Telecast;
    :cond_f
    invoke-virtual/range {v97 .. v97}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_10
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_11

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    .line 382
    .local v8, "territoryId":Ljava/lang/Long;
    move-object/from16 v0, v97

    invoke-virtual {v0, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v74

    check-cast v74, Ljava/util/List;
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 384
    .restart local v74    # "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/16 v114, 0x0

    .line 386
    .local v114, "telecasts":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    :try_start_d
    move-object/from16 v0, p0

    iget-object v10, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    move-object/from16 v0, v74

    move-wide/from16 v1, v16

    invoke-virtual {v10, v0, v1, v2}, Lru/cn/api/provider/TvContentProviderData;->getCurrentTelecasts(Ljava/util/List;J)Ljava/util/List;
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_1a
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    move-result-object v114

    .line 390
    :goto_9
    if-eqz v114, :cond_10

    .line 391
    :try_start_e
    invoke-interface/range {v114 .. v114}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_a
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_10

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v111

    check-cast v111, Lru/cn/api/tv/replies/Telecast;

    .line 392
    .restart local v111    # "telecast":Lru/cn/api/tv/replies/Telecast;
    move-object/from16 v0, v111

    iget-object v12, v0, Lru/cn/api/tv/replies/Telecast;->channel:Lru/cn/api/tv/replies/Telecast$Channel;

    iget-wide v0, v12, Lru/cn/api/tv/replies/Telecast$Channel;->channelId:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    move-object/from16 v0, v83

    move-object/from16 v1, v111

    invoke-interface {v0, v12, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_a

    .line 397
    .end local v8    # "territoryId":Ljava/lang/Long;
    .end local v74    # "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v111    # "telecast":Lru/cn/api/tv/replies/Telecast;
    .end local v114    # "telecasts":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    :cond_11
    const/16 v76, 0x0

    .local v76, "index":I
    :goto_b
    move-object/from16 v0, p4

    array-length v5, v0

    move/from16 v0, v76

    if-ge v0, v5, :cond_14

    .line 398
    aget-object v5, p4, v76

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 399
    .restart local v6    # "channelId":J
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v83

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v111

    check-cast v111, Lru/cn/api/tv/replies/Telecast;

    .line 400
    .restart local v111    # "telecast":Lru/cn/api/tv/replies/Telecast;
    if-eqz v111, :cond_12

    .line 401
    sget-object v5, Lru/cn/api/tv/replies/TelecastImage$Profile;->IMAGE:Lru/cn/api/tv/replies/TelecastImage$Profile;

    move-object/from16 v0, v111

    invoke-virtual {v0, v5}, Lru/cn/api/tv/replies/Telecast;->getImage(Lru/cn/api/tv/replies/TelecastImage$Profile;)Lru/cn/api/tv/replies/TelecastImage;

    move-result-object v113

    .line 402
    .local v113, "telecastImage":Lru/cn/api/tv/replies/TelecastImage;
    if-eqz v113, :cond_13

    move-object/from16 v0, v113

    iget-object v0, v0, Lru/cn/api/tv/replies/TelecastImage;->location:Ljava/lang/String;

    move-object/from16 v19, v0

    .line 404
    .local v19, "image":Ljava/lang/String;
    :goto_c
    move-object/from16 v0, v111

    iget v0, v0, Lru/cn/api/tv/replies/Telecast;->viewsCount:I

    move/from16 v25, v0

    .line 406
    .local v25, "viewsCount":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    move-object/from16 v0, v111

    iget-object v10, v0, Lru/cn/api/tv/replies/Telecast;->channel:Lru/cn/api/tv/replies/Telecast$Channel;

    iget-wide v0, v10, Lru/cn/api/tv/replies/Telecast$Channel;->channelId:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v5, v0, v1}, Lru/cn/api/provider/TvContentProviderData;->getChannelTitle(J)Ljava/lang/String;

    move-result-object v20

    .line 407
    .local v20, "channelTitle":Ljava/lang/String;
    move-object/from16 v0, v111

    iget-wide v12, v0, Lru/cn/api/tv/replies/Telecast;->id:J

    move-object/from16 v0, v111

    iget-object v5, v0, Lru/cn/api/tv/replies/Telecast;->date:Lru/cn/api/tv/replies/DateTime;

    invoke-virtual {v5}, Lru/cn/api/tv/replies/DateTime;->toSeconds()J

    move-result-wide v14

    move-object/from16 v0, v111

    iget-wide v0, v0, Lru/cn/api/tv/replies/Telecast;->duration:J

    move-wide/from16 v16, v0

    move-object/from16 v0, v111

    iget-object v0, v0, Lru/cn/api/tv/replies/Telecast;->title:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v111

    iget-object v5, v0, Lru/cn/api/tv/replies/Telecast;->channel:Lru/cn/api/tv/replies/Telecast$Channel;

    iget-wide v0, v5, Lru/cn/api/tv/replies/Telecast$Channel;->channelId:J

    move-wide/from16 v21, v0

    const/16 v23, 0x0

    .line 409
    invoke-virtual/range {v111 .. v111}, Lru/cn/api/tv/replies/Telecast;->isHighlight()Z

    move-result v24

    move-object/from16 v0, v111

    iget-object v0, v0, Lru/cn/api/tv/replies/Telecast;->description:Ljava/lang/String;

    move-object/from16 v26, v0

    .line 407
    invoke-virtual/range {v11 .. v26}, Lru/cn/api/provider/cursor/TelecastItemCursor;->addRow(JJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JIZILjava/lang/String;)V

    .line 397
    .end local v19    # "image":Ljava/lang/String;
    .end local v20    # "channelTitle":Ljava/lang/String;
    .end local v25    # "viewsCount":I
    .end local v113    # "telecastImage":Lru/cn/api/tv/replies/TelecastImage;
    :cond_12
    add-int/lit8 v76, v76, 0x1

    goto :goto_b

    .line 402
    .restart local v113    # "telecastImage":Lru/cn/api/tv/replies/TelecastImage;
    :cond_13
    const/16 v19, 0x0

    goto :goto_c

    .line 414
    .end local v6    # "channelId":J
    .end local v76    # "index":I
    .end local v83    # "loadedInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Lru/cn/api/tv/replies/Telecast;>;"
    .end local v97    # "requestsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/util/List<Ljava/lang/Long;>;>;"
    .end local v111    # "telecast":Lru/cn/api/tv/replies/Telecast;
    .end local v113    # "telecastImage":Lru/cn/api/tv/replies/TelecastImage;
    :cond_14
    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v11, v5, v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    move-object/from16 v26, v11

    .line 415
    goto/16 :goto_1

    .line 419
    .end local v11    # "cursor":Lru/cn/api/provider/cursor/TelecastItemCursor;
    :pswitch_5
    const/16 v55, 0x0

    .line 420
    .restart local v55    # "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    const/16 v70, 0x0

    .line 422
    .local v70, "favouriteChannels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    const-string v5, "age_filter"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    .line 423
    .restart local v48    # "ageFilter":Ljava/lang/String;
    if-nez v48, :cond_16

    const/16 v47, -0x1

    .line 424
    .restart local v47    # "age":I
    :goto_d
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    move/from16 v0, v47

    invoke-virtual {v5, v0}, Lru/cn/api/provider/TvContentProviderData;->setAge(I)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 427
    :try_start_f
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v5}, Lru/cn/api/provider/TvContentProviderData;->getChannels()Ljava/util/List;

    move-result-object v55

    .line 428
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v5}, Lru/cn/api/provider/TvContentProviderData;->getFavouriteChannels()Ljava/util/List;
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_19
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    move-result-object v70

    .line 432
    :goto_e
    :try_start_10
    new-instance v99, Ljava/util/ArrayList;

    invoke-direct/range {v99 .. v99}, Ljava/util/ArrayList;-><init>()V

    .line 433
    .local v99, "ret":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    new-instance v46, Ljava/util/HashSet;

    invoke-direct/range {v46 .. v46}, Ljava/util/HashSet;-><init>()V

    .line 435
    .local v46, "addedChannels":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    if-eqz v70, :cond_17

    .line 436
    invoke-interface/range {v70 .. v70}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_15
    :goto_f
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_17

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lru/cn/api/provider/Channel;

    .line 437
    .local v15, "c":Lru/cn/api/provider/Channel;
    iget-wide v0, v15, Lru/cn/api/provider/Channel;->channelId:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    move-object/from16 v0, v46

    invoke-interface {v0, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_15

    .line 438
    iget-wide v0, v15, Lru/cn/api/provider/Channel;->channelId:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    move-object/from16 v0, v46

    invoke-interface {v0, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 439
    move-object/from16 v0, v99

    invoke-interface {v0, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_f

    .line 423
    .end local v15    # "c":Lru/cn/api/provider/Channel;
    .end local v46    # "addedChannels":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v47    # "age":I
    .end local v99    # "ret":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    :cond_16
    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v47

    goto :goto_d

    .line 444
    .restart local v46    # "addedChannels":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .restart local v47    # "age":I
    .restart local v99    # "ret":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    :cond_17
    if-eqz v55, :cond_19

    .line 445
    invoke-interface/range {v55 .. v55}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_18
    :goto_10
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_19

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lru/cn/api/provider/Channel;

    .line 446
    .restart local v15    # "c":Lru/cn/api/provider/Channel;
    iget-wide v0, v15, Lru/cn/api/provider/Channel;->channelId:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    move-object/from16 v0, v46

    invoke-interface {v0, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_18

    .line 447
    iget-wide v0, v15, Lru/cn/api/provider/Channel;->channelId:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    move-object/from16 v0, v46

    invoke-interface {v0, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 448
    move-object/from16 v0, v99

    invoke-interface {v0, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_10

    .line 453
    .end local v15    # "c":Lru/cn/api/provider/Channel;
    :cond_19
    new-instance v15, Lru/cn/api/provider/cursor/ChannelCursor;

    invoke-direct {v15}, Lru/cn/api/provider/cursor/ChannelCursor;-><init>()V

    .line 454
    .local v15, "c":Lru/cn/api/provider/cursor/ChannelCursor;
    if-eqz v55, :cond_1a

    .line 455
    invoke-interface/range {v99 .. v99}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_11
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v49

    check-cast v49, Lru/cn/api/provider/Channel;

    .line 456
    .restart local v49    # "channel":Lru/cn/api/provider/Channel;
    const/4 v10, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v49

    invoke-direct {v0, v15, v1, v10}, Lru/cn/api/provider/TvContentProvider;->populateChannelCursor(Lru/cn/api/provider/cursor/ChannelCursor;Lru/cn/api/provider/Channel;Z)V

    goto :goto_11

    .line 460
    .end local v49    # "channel":Lru/cn/api/provider/Channel;
    :cond_1a
    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v15, v5, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    move-object/from16 v26, v15

    .line 462
    goto/16 :goto_1

    .line 466
    .end local v15    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    .end local v46    # "addedChannels":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v47    # "age":I
    .end local v48    # "ageFilter":Ljava/lang/String;
    .end local v55    # "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    .end local v70    # "favouriteChannels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    .end local v99    # "ret":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    :pswitch_6
    const/4 v5, 0x1

    new-array v0, v5, [Ljava/lang/String;

    move-object/from16 v52, v0

    const/4 v5, 0x0

    const-string v10, "cn_id"

    aput-object v10, v52, v5

    .line 467
    .restart local v52    # "channelFields":[Ljava/lang/String;
    new-instance v15, Lru/cn/api/provider/cursor/MatrixCursorEx;

    move-object/from16 v0, v52

    invoke-direct {v15, v0}, Lru/cn/api/provider/cursor/MatrixCursorEx;-><init>([Ljava/lang/String;)V

    .line 468
    .local v15, "c":Landroid/database/MatrixCursor;
    const/16 v55, 0x0

    .line 470
    .restart local v55    # "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    const-string v5, "age_filter"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    .line 471
    .restart local v48    # "ageFilter":Ljava/lang/String;
    if-nez v48, :cond_1f

    const/16 v47, -0x1

    .line 472
    .restart local v47    # "age":I
    :goto_12
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    move/from16 v0, v47

    invoke-virtual {v5, v0}, Lru/cn/api/provider/TvContentProviderData;->setAge(I)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 475
    :try_start_11
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v5}, Lru/cn/api/provider/TvContentProviderData;->getChannels()Ljava/util/List;
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_18
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    move-result-object v55

    .line 479
    :goto_13
    if-eqz v55, :cond_1e

    .line 481
    :try_start_12
    new-instance v82, Lru/cn/domain/LastChannel;

    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, v82

    move/from16 v1, v78

    invoke-direct {v0, v5, v1}, Lru/cn/domain/LastChannel;-><init>(Landroid/content/Context;Z)V

    .line 482
    .local v82, "lastChannelPrefs":Lru/cn/domain/LastChannel;
    invoke-virtual/range {v82 .. v82}, Lru/cn/domain/LastChannel;->getLastChannel()J

    move-result-wide v84

    .line 484
    .local v84, "lastCnId":J
    const/16 v81, 0x0

    .line 485
    .local v81, "lastChannel":Lru/cn/api/provider/Channel;
    invoke-interface/range {v55 .. v55}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1b
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v49

    check-cast v49, Lru/cn/api/provider/Channel;

    .line 486
    .restart local v49    # "channel":Lru/cn/api/provider/Channel;
    move-object/from16 v0, v49

    iget-wide v0, v0, Lru/cn/api/provider/Channel;->channelId:J

    move-wide/from16 v16, v0

    cmp-long v10, v16, v84

    if-nez v10, :cond_1b

    .line 487
    move-object/from16 v81, v49

    .line 492
    .end local v49    # "channel":Lru/cn/api/provider/Channel;
    :cond_1c
    if-nez v81, :cond_1d

    .line 493
    const/4 v5, 0x0

    move-object/from16 v0, v55

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v81

    .end local v81    # "lastChannel":Lru/cn/api/provider/Channel;
    check-cast v81, Lru/cn/api/provider/Channel;

    .line 496
    .restart local v81    # "lastChannel":Lru/cn/api/provider/Channel;
    :cond_1d
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v10, 0x0

    move-object/from16 v0, v81

    iget-wide v0, v0, Lru/cn/api/provider/Channel;->channelId:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v5, v10

    invoke-virtual {v15, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .end local v81    # "lastChannel":Lru/cn/api/provider/Channel;
    .end local v82    # "lastChannelPrefs":Lru/cn/domain/LastChannel;
    .end local v84    # "lastCnId":J
    :cond_1e
    move-object/from16 v26, v15

    .line 499
    goto/16 :goto_1

    .line 471
    .end local v47    # "age":I
    :cond_1f
    invoke-static/range {v48 .. v48}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v47

    goto :goto_12

    .line 503
    .end local v15    # "c":Landroid/database/MatrixCursor;
    .end local v48    # "ageFilter":Ljava/lang/String;
    .end local v52    # "channelFields":[Ljava/lang/String;
    .end local v55    # "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    :pswitch_7
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    move-result-wide v13

    .line 504
    .local v13, "cnId":J
    const/16 v49, 0x0

    .line 506
    .restart local v49    # "channel":Lru/cn/api/provider/Channel;
    :try_start_13
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v5, v13, v14}, Lru/cn/api/provider/TvContentProviderData;->getChannel(J)Lru/cn/api/provider/Channel;
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_17
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    move-result-object v49

    .line 510
    :goto_14
    :try_start_14
    new-instance v15, Lru/cn/api/provider/cursor/ChannelCursor;

    invoke-direct {v15}, Lru/cn/api/provider/cursor/ChannelCursor;-><init>()V

    .line 511
    .local v15, "c":Lru/cn/api/provider/cursor/ChannelCursor;
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v49

    invoke-direct {v0, v15, v1, v5}, Lru/cn/api/provider/TvContentProvider;->populateChannelCursor(Lru/cn/api/provider/cursor/ChannelCursor;Lru/cn/api/provider/Channel;Z)V

    .line 513
    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v15, v5, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    move-object/from16 v26, v15

    .line 514
    goto/16 :goto_1

    .line 518
    .end local v13    # "cnId":J
    .end local v15    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    .end local v49    # "channel":Lru/cn/api/provider/Channel;
    :pswitch_8
    const/16 v5, 0x11

    new-array v0, v5, [Ljava/lang/String;

    move-object/from16 v71, v0

    const/4 v5, 0x0

    const-string v10, "_id"

    aput-object v10, v71, v5

    const/4 v5, 0x1

    const-string v10, "title"

    aput-object v10, v71, v5

    const/4 v5, 0x2

    const-string v10, "location"

    aput-object v10, v71, v5

    const/4 v5, 0x3

    const-string v10, "has_timeshift"

    aput-object v10, v71, v5

    const/4 v5, 0x4

    const-string v10, "crop_x"

    aput-object v10, v71, v5

    const/4 v5, 0x5

    const-string v10, "crop_y"

    aput-object v10, v71, v5

    const/4 v5, 0x6

    const-string v10, "access"

    aput-object v10, v71, v5

    const/4 v5, 0x7

    const-string v10, "allowed-till"

    aput-object v10, v71, v5

    const/16 v5, 0x8

    const-string v10, "territory_id"

    aput-object v10, v71, v5

    const/16 v5, 0x9

    const-string v10, "contractor_id"

    aput-object v10, v71, v5

    const/16 v5, 0xa

    const-string v10, "aspect_w"

    aput-object v10, v71, v5

    const/16 v5, 0xb

    const-string v10, "aspect_h"

    aput-object v10, v71, v5

    const/16 v5, 0xc

    const-string v10, "source_uri"

    aput-object v10, v71, v5

    const/16 v5, 0xd

    const-string v10, "ad_tags"

    aput-object v10, v71, v5

    const/16 v5, 0xe

    const-string v10, "stream_id"

    aput-object v10, v71, v5

    const/16 v5, 0xf

    const-string v10, "no_ads_partner"

    aput-object v10, v71, v5

    const/16 v5, 0x10

    const-string v10, "timezone"

    aput-object v10, v71, v5

    .line 523
    .local v71, "fields":[Ljava/lang/String;
    new-instance v26, Lru/cn/api/provider/cursor/MatrixCursorEx;

    move-object/from16 v0, v26

    move-object/from16 v1, v71

    invoke-direct {v0, v1}, Lru/cn/api/provider/cursor/MatrixCursorEx;-><init>([Ljava/lang/String;)V

    .line 525
    .local v26, "ret":Landroid/database/MatrixCursor;
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    move-result-wide v13

    .line 527
    .restart local v13    # "cnId":J
    :try_start_15
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v5, v13, v14}, Lru/cn/api/provider/TvContentProviderData;->getChannelLocations(J)Ljava/util/List;

    move-result-object v88

    .line 528
    .local v88, "locations":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/iptv/replies/MediaLocation;>;"
    if-eqz v88, :cond_1

    .line 531
    const-string v4, ""
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_3
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    .line 533
    .local v4, "adTags":Ljava/lang/String;
    :try_start_16
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    const-wide/16 v16, 0x0

    move-wide/from16 v0, v16

    invoke-virtual {v5, v13, v14, v0, v1}, Lru/cn/api/provider/TvContentProviderData;->getCurrentTelecast(JJ)Lru/cn/api/tv/replies/Telecast;

    move-result-object v111

    .line 534
    .restart local v111    # "telecast":Lru/cn/api/tv/replies/Telecast;
    move-object/from16 v0, p0

    move-object/from16 v1, v111

    invoke-direct {v0, v1}, Lru/cn/api/provider/TvContentProvider;->formattedAdTags(Lru/cn/api/tv/replies/Telecast;)Ljava/lang/String;
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    move-result-object v4

    .line 538
    .end local v111    # "telecast":Lru/cn/api/tv/replies/Telecast;
    :goto_15
    :try_start_17
    invoke-interface/range {v88 .. v88}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_16
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v80

    check-cast v80, Lru/cn/api/iptv/replies/MediaLocation;

    .line 539
    .local v80, "l":Lru/cn/api/iptv/replies/MediaLocation;
    move-object/from16 v0, v80

    iget-object v0, v0, Lru/cn/api/iptv/replies/MediaLocation;->location:Ljava/lang/String;

    move-object/from16 v87, v0

    .line 540
    .local v87, "locationUri":Ljava/lang/String;
    move-object/from16 v0, v80

    iget-boolean v5, v0, Lru/cn/api/iptv/replies/MediaLocation;->adTargeting:Z

    if-eqz v5, :cond_20

    .line 541
    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, v87

    invoke-static {v5, v0}, Lru/cn/utils/Utils;->appendAdTargetingParams(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v87

    .line 544
    :cond_20
    const/16 v5, 0x11

    new-array v12, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, v80

    iget-wide v0, v0, Lru/cn/api/iptv/replies/MediaLocation;->channelId:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v12, v5

    const/4 v5, 0x1

    move-object/from16 v0, v80

    iget-object v0, v0, Lru/cn/api/iptv/replies/MediaLocation;->title:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v12, v5

    const/4 v5, 0x2

    aput-object v87, v12, v5

    const/16 v16, 0x3

    move-object/from16 v0, v80

    iget-boolean v5, v0, Lru/cn/api/iptv/replies/MediaLocation;->hasTimeshift:Z

    if-eqz v5, :cond_21

    const/4 v5, 0x1

    :goto_17
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v12, v16

    const/4 v5, 0x4

    move-object/from16 v0, v80

    iget v0, v0, Lru/cn/api/iptv/replies/MediaLocation;->cropX:I

    move/from16 v16, v0

    .line 545
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v12, v5

    const/4 v5, 0x5

    move-object/from16 v0, v80

    iget v0, v0, Lru/cn/api/iptv/replies/MediaLocation;->cropY:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v12, v5

    const/4 v5, 0x6

    move-object/from16 v0, v80

    iget-object v0, v0, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lru/cn/api/iptv/replies/MediaLocation$Access;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v12, v5

    const/4 v5, 0x7

    move-object/from16 v0, v80

    iget v0, v0, Lru/cn/api/iptv/replies/MediaLocation;->allowedTill:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v12, v5

    const/16 v5, 0x8

    move-object/from16 v0, v80

    iget-wide v0, v0, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    move-wide/from16 v16, v0

    .line 546
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v12, v5

    const/16 v5, 0x9

    move-object/from16 v0, v80

    iget-wide v0, v0, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v12, v5

    const/16 v5, 0xa

    move-object/from16 v0, v80

    iget v0, v0, Lru/cn/api/iptv/replies/MediaLocation;->aspectW:F

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v16

    aput-object v16, v12, v5

    const/16 v5, 0xb

    move-object/from16 v0, v80

    iget v0, v0, Lru/cn/api/iptv/replies/MediaLocation;->aspectH:F

    move/from16 v16, v0

    .line 547
    invoke-static/range {v16 .. v16}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v16

    aput-object v16, v12, v5

    const/16 v5, 0xc

    move-object/from16 v0, v80

    iget-object v0, v0, Lru/cn/api/iptv/replies/MediaLocation;->sourceUri:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v12, v5

    const/16 v5, 0xd

    aput-object v4, v12, v5

    const/16 v5, 0xe

    move-object/from16 v0, v80

    iget-wide v0, v0, Lru/cn/api/iptv/replies/MediaLocation;->streamId:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v12, v5

    const/16 v16, 0xf

    move-object/from16 v0, v80

    iget-boolean v5, v0, Lru/cn/api/iptv/replies/MediaLocation;->noAdsPartner:Z

    if-eqz v5, :cond_22

    const/4 v5, 0x1

    :goto_18
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v12, v16

    const/16 v5, 0x10

    move-object/from16 v0, v80

    iget-object v0, v0, Lru/cn/api/iptv/replies/MediaLocation;->timezoneOffset:Ljava/lang/Long;

    move-object/from16 v16, v0

    aput-object v16, v12, v5

    .line 544
    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_3
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    goto/16 :goto_16

    .line 549
    .end local v4    # "adTags":Ljava/lang/String;
    .end local v80    # "l":Lru/cn/api/iptv/replies/MediaLocation;
    .end local v87    # "locationUri":Ljava/lang/String;
    .end local v88    # "locations":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/iptv/replies/MediaLocation;>;"
    :catch_3
    move-exception v5

    goto/16 :goto_1

    .line 544
    .restart local v4    # "adTags":Ljava/lang/String;
    .restart local v80    # "l":Lru/cn/api/iptv/replies/MediaLocation;
    .restart local v87    # "locationUri":Ljava/lang/String;
    .restart local v88    # "locations":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/iptv/replies/MediaLocation;>;"
    :cond_21
    const/4 v5, 0x0

    goto/16 :goto_17

    .line 547
    :cond_22
    const/4 v5, 0x0

    goto :goto_18

    .line 555
    .end local v4    # "adTags":Ljava/lang/String;
    .end local v13    # "cnId":J
    .end local v26    # "ret":Landroid/database/MatrixCursor;
    .end local v71    # "fields":[Ljava/lang/String;
    .end local v80    # "l":Lru/cn/api/iptv/replies/MediaLocation;
    .end local v87    # "locationUri":Ljava/lang/String;
    .end local v88    # "locations":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/iptv/replies/MediaLocation;>;"
    :pswitch_9
    :try_start_18
    const-string v5, "favourite"

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    .line 556
    .local v69, "favourite":Z
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v13

    .line 557
    .restart local v13    # "cnId":J
    new-instance v15, Lru/cn/api/provider/cursor/ChannelCursor;

    invoke-direct {v15}, Lru/cn/api/provider/cursor/ChannelCursor;-><init>()V
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    .line 559
    .restart local v15    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    :try_start_19
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    move/from16 v0, v69

    invoke-virtual {v5, v13, v14, v0}, Lru/cn/api/provider/TvContentProviderData;->getNextChannel(JZ)Lru/cn/api/provider/Channel;

    move-result-object v49

    .line 560
    .restart local v49    # "channel":Lru/cn/api/provider/Channel;
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v49

    invoke-direct {v0, v15, v1, v5}, Lru/cn/api/provider/TvContentProvider;->populateChannelCursor(Lru/cn/api/provider/cursor/ChannelCursor;Lru/cn/api/provider/Channel;Z)V
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_15
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    .line 564
    .end local v49    # "channel":Lru/cn/api/provider/Channel;
    :goto_19
    :try_start_1a
    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v15, v5, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    move-object/from16 v26, v15

    .line 565
    goto/16 :goto_1

    .line 568
    .end local v13    # "cnId":J
    .end local v15    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    .end local v69    # "favourite":Z
    :pswitch_a
    const-string v5, "favourite"

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v69

    .line 569
    .restart local v69    # "favourite":Z
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v13

    .line 570
    .restart local v13    # "cnId":J
    new-instance v15, Lru/cn/api/provider/cursor/ChannelCursor;

    invoke-direct {v15}, Lru/cn/api/provider/cursor/ChannelCursor;-><init>()V
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_0

    .line 572
    .restart local v15    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    :try_start_1b
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    move/from16 v0, v69

    invoke-virtual {v5, v13, v14, v0}, Lru/cn/api/provider/TvContentProviderData;->getPrevChannel(JZ)Lru/cn/api/provider/Channel;

    move-result-object v49

    .line 573
    .restart local v49    # "channel":Lru/cn/api/provider/Channel;
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v49

    invoke-direct {v0, v15, v1, v5}, Lru/cn/api/provider/TvContentProvider;->populateChannelCursor(Lru/cn/api/provider/cursor/ChannelCursor;Lru/cn/api/provider/Channel;Z)V
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_14
    .catchall {:try_start_1b .. :try_end_1b} :catchall_0

    .line 576
    .end local v49    # "channel":Lru/cn/api/provider/Channel;
    :goto_1a
    :try_start_1c
    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v15, v5, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    move-object/from16 v26, v15

    .line 577
    goto/16 :goto_1

    .line 581
    .end local v13    # "cnId":J
    .end local v15    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    .end local v69    # "favourite":Z
    :pswitch_b
    const/4 v5, 0x6

    new-array v0, v5, [Ljava/lang/String;

    move-object/from16 v71, v0

    const/4 v5, 0x0

    const-string v10, "_id"

    aput-object v10, v71, v5

    const/4 v5, 0x1

    const-string v10, "cn_id"

    aput-object v10, v71, v5

    const/4 v5, 0x2

    const-string v10, "year"

    aput-object v10, v71, v5

    const/4 v5, 0x3

    const-string v10, "month"

    aput-object v10, v71, v5

    const/4 v5, 0x4

    const-string v10, "day"

    aput-object v10, v71, v5

    const/4 v5, 0x5

    const-string v10, "timezone"

    aput-object v10, v71, v5

    .line 583
    .restart local v71    # "fields":[Ljava/lang/String;
    new-instance v26, Lru/cn/api/provider/cursor/MatrixCursorEx;

    move-object/from16 v0, v26

    move-object/from16 v1, v71

    invoke-direct {v0, v1}, Lru/cn/api/provider/cursor/MatrixCursorEx;-><init>([Ljava/lang/String;)V

    .line 585
    .restart local v26    # "ret":Landroid/database/MatrixCursor;
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_0

    move-result-wide v13

    .line 588
    .restart local v13    # "cnId":J
    :try_start_1d
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v5, v13, v14}, Lru/cn/api/provider/TvContentProviderData;->getChannelDates(J)Ljava/util/List;

    move-result-object v64

    .line 589
    .local v64, "dates":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/DateTime;>;"
    if-eqz v64, :cond_1

    .line 590
    invoke-interface/range {v64 .. v64}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1b
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v63

    check-cast v63, Lru/cn/api/tv/replies/DateTime;

    .line 591
    .local v63, "date":Lru/cn/api/tv/replies/DateTime;
    const/4 v10, 0x6

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v12, 0x0

    const/16 v16, 0x0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v10, v12

    const/4 v12, 0x1

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v10, v12

    const/4 v12, 0x2

    move-object/from16 v0, v63

    iget v0, v0, Lru/cn/api/tv/replies/DateTime;->year:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v10, v12

    const/4 v12, 0x3

    move-object/from16 v0, v63

    iget v0, v0, Lru/cn/api/tv/replies/DateTime;->month:I

    move/from16 v16, v0

    .line 592
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v10, v12

    const/4 v12, 0x4

    move-object/from16 v0, v63

    iget v0, v0, Lru/cn/api/tv/replies/DateTime;->day:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v10, v12

    const/4 v12, 0x5

    move-object/from16 v0, v63

    iget v0, v0, Lru/cn/api/tv/replies/DateTime;->timezone:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v10, v12

    .line 591
    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_4
    .catchall {:try_start_1d .. :try_end_1d} :catchall_0

    goto :goto_1b

    .line 595
    .end local v63    # "date":Lru/cn/api/tv/replies/DateTime;
    .end local v64    # "dates":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/DateTime;>;"
    :catch_4
    move-exception v65

    .line 596
    .restart local v65    # "e":Ljava/lang/Exception;
    :try_start_1e
    invoke-static/range {v65 .. v65}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 603
    .end local v13    # "cnId":J
    .end local v26    # "ret":Landroid/database/MatrixCursor;
    .end local v65    # "e":Ljava/lang/Exception;
    .end local v71    # "fields":[Ljava/lang/String;
    :pswitch_c
    new-instance v26, Lru/cn/api/provider/cursor/ScheduleItemCursor;

    invoke-direct/range {v26 .. v26}, Lru/cn/api/provider/cursor/ScheduleItemCursor;-><init>()V

    .line 605
    .local v26, "ret":Lru/cn/api/provider/cursor/ScheduleItemCursor;
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v108

    .line 606
    .local v108, "segments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v108 .. v108}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    move-object/from16 v0, v108

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v13

    .line 607
    .restart local v13    # "cnId":J
    const-string v5, "territoryId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v110

    .line 608
    .local v110, "t":Ljava/lang/String;
    const-wide/16 v8, 0x0

    .line 609
    .local v8, "territoryId":J
    if-eqz v110, :cond_23

    .line 610
    invoke-static/range {v110 .. v110}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 613
    :cond_23
    invoke-interface/range {v108 .. v108}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    move-object/from16 v0, v108

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v107

    check-cast v107, Ljava/lang/String;
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_0

    .line 615
    .local v107, "sDate":Ljava/lang/String;
    :try_start_1f
    invoke-static/range {v107 .. v107}, Lru/cn/utils/Utils;->getCalendar(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v15

    .line 616
    .local v15, "c":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget-object v12, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    move-wide/from16 v16, v8

    invoke-virtual/range {v12 .. v17}, Lru/cn/api/provider/TvContentProviderData;->getChannelSchedule(JLjava/util/Calendar;J)Ljava/util/List;

    move-result-object v114

    .line 617
    .restart local v114    # "telecasts":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    if-eqz v114, :cond_24

    .line 618
    invoke-interface/range {v114 .. v114}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1c
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_24

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v111

    check-cast v111, Lru/cn/api/tv/replies/Telecast;

    .line 619
    .restart local v111    # "telecast":Lru/cn/api/tv/replies/Telecast;
    move-object/from16 v0, v111

    iget-object v10, v0, Lru/cn/api/tv/replies/Telecast;->date:Lru/cn/api/tv/replies/DateTime;

    invoke-virtual {v10}, Lru/cn/api/tv/replies/DateTime;->toSeconds()J

    move-result-wide v32

    .line 621
    .local v32, "startTimeSec":J
    move-object/from16 v0, p0

    iget-object v10, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    move-object/from16 v0, v111

    iget-object v12, v0, Lru/cn/api/tv/replies/Telecast;->channel:Lru/cn/api/tv/replies/Telecast$Channel;

    iget-wide v0, v12, Lru/cn/api/tv/replies/Telecast$Channel;->channelId:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v10, v0, v1}, Lru/cn/api/provider/TvContentProviderData;->getChannelTitle(J)Ljava/lang/String;

    move-result-object v20

    .line 622
    .restart local v20    # "channelTitle":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v10, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    move-object/from16 v0, v111

    iget-wide v0, v0, Lru/cn/api/tv/replies/Telecast;->id:J

    move-wide/from16 v16, v0

    const/4 v12, 0x1

    move-wide/from16 v0, v16

    invoke-virtual {v10, v0, v1, v12}, Lru/cn/api/provider/TvContentProviderData;->getTelecastLocations(JZ)Lru/cn/api/provider/TelecastLocationInfo;

    move-result-object v77

    .line 623
    .local v77, "info":Lru/cn/api/provider/TelecastLocationInfo;
    if-eqz v77, :cond_25

    move-object/from16 v0, v77

    iget-object v10, v0, Lru/cn/api/provider/TelecastLocationInfo;->status:Lru/cn/api/medialocator/replies/Rip$RipStatus;

    sget-object v12, Lru/cn/api/medialocator/replies/Rip$RipStatus;->ACCESS_DENIED:Lru/cn/api/medialocator/replies/Rip$RipStatus;

    if-ne v10, v12, :cond_25

    const/16 v38, 0x1

    .line 625
    .local v38, "isDenied":Z
    :goto_1d
    move-object/from16 v0, v111

    iget-wide v0, v0, Lru/cn/api/tv/replies/Telecast;->id:J

    move-wide/from16 v27, v0

    move-object/from16 v0, v111

    iget-object v10, v0, Lru/cn/api/tv/replies/Telecast;->channel:Lru/cn/api/tv/replies/Telecast$Channel;

    iget-wide v0, v10, Lru/cn/api/tv/replies/Telecast$Channel;->channelId:J

    move-wide/from16 v29, v0

    move-object/from16 v0, v111

    iget-wide v0, v0, Lru/cn/api/tv/replies/Telecast;->duration:J

    move-wide/from16 v34, v0

    move-object/from16 v0, v111

    iget-object v0, v0, Lru/cn/api/tv/replies/Telecast;->title:Ljava/lang/String;

    move-object/from16 v36, v0

    if-eqz v77, :cond_26

    const/16 v37, 0x1

    :goto_1e
    move-object/from16 v31, v20

    invoke-virtual/range {v26 .. v38}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->addRow(JJLjava/lang/String;JJLjava/lang/String;ZZ)V
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_5
    .catchall {:try_start_1f .. :try_end_1f} :catchall_0

    goto :goto_1c

    .line 630
    .end local v15    # "c":Ljava/util/Calendar;
    .end local v20    # "channelTitle":Ljava/lang/String;
    .end local v32    # "startTimeSec":J
    .end local v38    # "isDenied":Z
    .end local v77    # "info":Lru/cn/api/provider/TelecastLocationInfo;
    .end local v111    # "telecast":Lru/cn/api/tv/replies/Telecast;
    .end local v114    # "telecasts":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    :catch_5
    move-exception v65

    .line 631
    .restart local v65    # "e":Ljava/lang/Exception;
    :try_start_20
    invoke-static/range {v65 .. v65}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .line 633
    .end local v65    # "e":Ljava/lang/Exception;
    :cond_24
    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v5, v1}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto/16 :goto_1

    .line 623
    .restart local v15    # "c":Ljava/util/Calendar;
    .restart local v20    # "channelTitle":Ljava/lang/String;
    .restart local v32    # "startTimeSec":J
    .restart local v77    # "info":Lru/cn/api/provider/TelecastLocationInfo;
    .restart local v111    # "telecast":Lru/cn/api/tv/replies/Telecast;
    .restart local v114    # "telecasts":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    :cond_25
    const/16 v38, 0x0

    goto :goto_1d

    .line 625
    .restart local v38    # "isDenied":Z
    :cond_26
    const/16 v37, 0x0

    goto :goto_1e

    .line 639
    .end local v8    # "territoryId":J
    .end local v13    # "cnId":J
    .end local v15    # "c":Ljava/util/Calendar;
    .end local v20    # "channelTitle":Ljava/lang/String;
    .end local v26    # "ret":Lru/cn/api/provider/cursor/ScheduleItemCursor;
    .end local v32    # "startTimeSec":J
    .end local v38    # "isDenied":Z
    .end local v77    # "info":Lru/cn/api/provider/TelecastLocationInfo;
    .end local v107    # "sDate":Ljava/lang/String;
    .end local v108    # "segments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v110    # "t":Ljava/lang/String;
    .end local v111    # "telecast":Lru/cn/api/tv/replies/Telecast;
    .end local v114    # "telecasts":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    :pswitch_d
    const/4 v5, 0x3

    new-array v0, v5, [Ljava/lang/String;

    move-object/from16 v71, v0

    const/4 v5, 0x0

    const-string v10, "_id"

    aput-object v10, v71, v5

    const/4 v5, 0x1

    const-string v10, "prev_telecast"

    aput-object v10, v71, v5

    const/4 v5, 0x2

    const-string v10, "next_telecast"

    aput-object v10, v71, v5

    .line 640
    .restart local v71    # "fields":[Ljava/lang/String;
    new-instance v26, Lru/cn/api/provider/cursor/MatrixCursorEx;

    move-object/from16 v0, v26

    move-object/from16 v1, v71

    invoke-direct {v0, v1}, Lru/cn/api/provider/cursor/MatrixCursorEx;-><init>([Ljava/lang/String;)V

    .line 642
    .local v26, "ret":Landroid/database/MatrixCursor;
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v58

    .line 643
    .local v58, "collectionId":J
    const-wide/16 v16, 0x0

    cmp-long v5, v58, v16

    if-lez v5, :cond_1

    .line 646
    const-string v5, "telecast"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v112

    .line 647
    .local v112, "telecastId":Ljava/lang/String;
    invoke-static/range {v112 .. v112}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_0

    move-result-wide v72

    .line 650
    .local v72, "id":J
    :try_start_21
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    move-wide/from16 v0, v58

    move-wide/from16 v2, v72

    invoke-virtual {v5, v0, v1, v2, v3}, Lru/cn/api/provider/TvContentProviderData;->getNavigationItems(JJ)[Lru/cn/api/tv/replies/Telecast;

    move-result-object v79

    .line 651
    .local v79, "items":[Lru/cn/api/tv/replies/Telecast;
    const/4 v5, 0x0

    aget-object v95, v79, v5

    .line 652
    .local v95, "prevTelecast":Lru/cn/api/tv/replies/Telecast;
    const/4 v5, 0x1

    aget-object v90, v79, v5

    .line 654
    .local v90, "nextTelecast":Lru/cn/api/tv/replies/Telecast;
    const/4 v5, 0x3

    new-array v10, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v112, v10, v5

    const/4 v12, 0x1

    if-eqz v95, :cond_27

    invoke-virtual/range {v95 .. v95}, Lru/cn/api/tv/replies/Telecast;->toJson()Ljava/lang/String;

    move-result-object v5

    :goto_1f
    aput-object v5, v10, v12

    const/4 v12, 0x2

    if-eqz v90, :cond_28

    .line 655
    invoke-virtual/range {v90 .. v90}, Lru/cn/api/tv/replies/Telecast;->toJson()Ljava/lang/String;

    move-result-object v5

    :goto_20
    aput-object v5, v10, v12

    .line 654
    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_21
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_21} :catch_6
    .catchall {:try_start_21 .. :try_end_21} :catchall_0

    goto/16 :goto_1

    .line 656
    .end local v79    # "items":[Lru/cn/api/tv/replies/Telecast;
    .end local v90    # "nextTelecast":Lru/cn/api/tv/replies/Telecast;
    .end local v95    # "prevTelecast":Lru/cn/api/tv/replies/Telecast;
    :catch_6
    move-exception v65

    .line 657
    .restart local v65    # "e":Ljava/lang/Exception;
    :try_start_22
    invoke-static/range {v65 .. v65}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 654
    .end local v65    # "e":Ljava/lang/Exception;
    .restart local v79    # "items":[Lru/cn/api/tv/replies/Telecast;
    .restart local v90    # "nextTelecast":Lru/cn/api/tv/replies/Telecast;
    .restart local v95    # "prevTelecast":Lru/cn/api/tv/replies/Telecast;
    :cond_27
    const/4 v5, 0x0

    goto :goto_1f

    .line 655
    :cond_28
    const/4 v5, 0x0

    goto :goto_20

    .line 664
    .end local v26    # "ret":Landroid/database/MatrixCursor;
    .end local v58    # "collectionId":J
    .end local v71    # "fields":[Ljava/lang/String;
    .end local v72    # "id":J
    .end local v79    # "items":[Lru/cn/api/tv/replies/Telecast;
    .end local v90    # "nextTelecast":Lru/cn/api/tv/replies/Telecast;
    .end local v95    # "prevTelecast":Lru/cn/api/tv/replies/Telecast;
    .end local v112    # "telecastId":Ljava/lang/String;
    :pswitch_e
    const/4 v5, 0x3

    new-array v0, v5, [Ljava/lang/String;

    move-object/from16 v71, v0

    const/4 v5, 0x0

    const-string v10, "_id"

    aput-object v10, v71, v5

    const/4 v5, 0x1

    const-string v10, "data"

    aput-object v10, v71, v5

    const/4 v5, 0x2

    const-string v10, "channel"

    aput-object v10, v71, v5

    .line 665
    .restart local v71    # "fields":[Ljava/lang/String;
    new-instance v26, Lru/cn/api/provider/cursor/MatrixCursorEx;

    move-object/from16 v0, v26

    move-object/from16 v1, v71

    invoke-direct {v0, v1}, Lru/cn/api/provider/cursor/MatrixCursorEx;-><init>([Ljava/lang/String;)V

    .line 667
    .restart local v26    # "ret":Landroid/database/MatrixCursor;
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_0

    move-result-wide v72

    .line 668
    .restart local v72    # "id":J
    const/16 v111, 0x0

    .line 669
    .restart local v111    # "telecast":Lru/cn/api/tv/replies/Telecast;
    const/16 v20, 0x0

    .line 672
    .restart local v20    # "channelTitle":Ljava/lang/String;
    :try_start_23
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    move-wide/from16 v0, v72

    invoke-virtual {v5, v0, v1}, Lru/cn/api/provider/TvContentProviderData;->getTelecastItem(J)Lru/cn/api/tv/replies/Telecast;

    move-result-object v111

    .line 673
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    move-object/from16 v0, v111

    iget-object v10, v0, Lru/cn/api/tv/replies/Telecast;->channel:Lru/cn/api/tv/replies/Telecast$Channel;

    iget-wide v0, v10, Lru/cn/api/tv/replies/Telecast$Channel;->channelId:J

    move-wide/from16 v16, v0

    const-wide/16 v22, 0x0

    move-wide/from16 v0, v16

    move-wide/from16 v2, v22

    invoke-virtual {v5, v0, v1, v2, v3}, Lru/cn/api/provider/TvContentProviderData;->getChannelInfo(JJ)Lru/cn/api/tv/replies/ChannelInfo;

    move-result-object v54

    .line 674
    .local v54, "channelInfo":Lru/cn/api/tv/replies/ChannelInfo;
    move-object/from16 v0, v54

    iget-object v0, v0, Lru/cn/api/tv/replies/ChannelInfo;->title:Ljava/lang/String;

    move-object/from16 v20, v0
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_23} :catch_7
    .catchall {:try_start_23 .. :try_end_23} :catchall_0

    .line 679
    .end local v54    # "channelInfo":Lru/cn/api/tv/replies/ChannelInfo;
    :goto_21
    if-eqz v111, :cond_1

    .line 680
    const/4 v5, 0x3

    :try_start_24
    new-array v5, v5, [Ljava/lang/Object;

    const/4 v10, 0x0

    move-object/from16 v0, v111

    iget-wide v0, v0, Lru/cn/api/tv/replies/Telecast;->id:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v5, v10

    const/4 v10, 0x1

    invoke-virtual/range {v111 .. v111}, Lru/cn/api/tv/replies/Telecast;->toJson()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v5, v10

    const/4 v10, 0x2

    aput-object v20, v5, v10

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 675
    :catch_7
    move-exception v65

    .line 676
    .restart local v65    # "e":Ljava/lang/Exception;
    invoke-static/range {v65 .. v65}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_21

    .line 686
    .end local v20    # "channelTitle":Ljava/lang/String;
    .end local v26    # "ret":Landroid/database/MatrixCursor;
    .end local v65    # "e":Ljava/lang/Exception;
    .end local v71    # "fields":[Ljava/lang/String;
    .end local v72    # "id":J
    .end local v111    # "telecast":Lru/cn/api/tv/replies/Telecast;
    :pswitch_f
    const/4 v5, 0x2

    new-array v0, v5, [Ljava/lang/String;

    move-object/from16 v71, v0

    const/4 v5, 0x0

    const-string v10, "_id"

    aput-object v10, v71, v5

    const/4 v5, 0x1

    const-string v10, "data"

    aput-object v10, v71, v5

    .line 687
    .restart local v71    # "fields":[Ljava/lang/String;
    new-instance v26, Lru/cn/api/provider/cursor/MatrixCursorEx;

    move-object/from16 v0, v26

    move-object/from16 v1, v71

    invoke-direct {v0, v1}, Lru/cn/api/provider/cursor/MatrixCursorEx;-><init>([Ljava/lang/String;)V

    .line 689
    .restart local v26    # "ret":Landroid/database/MatrixCursor;
    const-string v5, "cnId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v13

    .line 690
    .restart local v13    # "cnId":J
    const-string v5, "territoryId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v110

    .line 691
    .restart local v110    # "t":Ljava/lang/String;
    const-wide/16 v8, 0x0

    .line 692
    .restart local v8    # "territoryId":J
    if-eqz v110, :cond_29

    .line 693
    invoke-static/range {v110 .. v110}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 695
    :cond_29
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_0

    move-result-wide v42

    .line 697
    .local v42, "searchTime":J
    :try_start_25
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    move-object/from16 v39, v0

    move-wide/from16 v40, v13

    move-wide/from16 v44, v8

    invoke-virtual/range {v39 .. v45}, Lru/cn/api/provider/TvContentProviderData;->getTelecastItemByTime(JJJ)Lru/cn/api/tv/replies/Telecast;

    move-result-object v111

    .line 699
    .restart local v111    # "telecast":Lru/cn/api/tv/replies/Telecast;
    if-eqz v111, :cond_1

    .line 700
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v10, 0x0

    move-object/from16 v0, v111

    iget-wide v0, v0, Lru/cn/api/tv/replies/Telecast;->id:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v5, v10

    const/4 v10, 0x1

    invoke-virtual/range {v111 .. v111}, Lru/cn/api/tv/replies/Telecast;->toJson()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v5, v10

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_25
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_25} :catch_8
    .catchall {:try_start_25 .. :try_end_25} :catchall_0

    goto/16 :goto_1

    .line 702
    .end local v111    # "telecast":Lru/cn/api/tv/replies/Telecast;
    :catch_8
    move-exception v65

    .line 703
    .restart local v65    # "e":Ljava/lang/Exception;
    :try_start_26
    invoke-static/range {v65 .. v65}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 710
    .end local v8    # "territoryId":J
    .end local v13    # "cnId":J
    .end local v26    # "ret":Landroid/database/MatrixCursor;
    .end local v42    # "searchTime":J
    .end local v65    # "e":Ljava/lang/Exception;
    .end local v71    # "fields":[Ljava/lang/String;
    .end local v110    # "t":Ljava/lang/String;
    :pswitch_10
    const/4 v5, 0x6

    new-array v0, v5, [Ljava/lang/String;

    move-object/from16 v71, v0

    const/4 v5, 0x0

    const-string v10, "_id"

    aput-object v10, v71, v5

    const/4 v5, 0x1

    const-string v10, "location"

    aput-object v10, v71, v5

    const/4 v5, 0x2

    const-string v10, "contractor_id"

    aput-object v10, v71, v5

    const/4 v5, 0x3

    const-string v10, "status"

    aput-object v10, v71, v5

    const/4 v5, 0x4

    const-string v10, "ad_tags"

    aput-object v10, v71, v5

    const/4 v5, 0x5

    const-string v10, "no_ads_partner"

    aput-object v10, v71, v5

    .line 711
    .restart local v71    # "fields":[Ljava/lang/String;
    new-instance v26, Lru/cn/api/provider/cursor/MatrixCursorEx;

    move-object/from16 v0, v26

    move-object/from16 v1, v71

    invoke-direct {v0, v1}, Lru/cn/api/provider/cursor/MatrixCursorEx;-><init>([Ljava/lang/String;)V

    .line 713
    .restart local v26    # "ret":Landroid/database/MatrixCursor;
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_26
    .catchall {:try_start_26 .. :try_end_26} :catchall_0

    move-result-wide v72

    .line 715
    .restart local v72    # "id":J
    :try_start_27
    const-string v4, ""
    :try_end_27
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_27} :catch_9
    .catchall {:try_start_27 .. :try_end_27} :catchall_0

    .line 717
    .restart local v4    # "adTags":Ljava/lang/String;
    :try_start_28
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    move-wide/from16 v0, v72

    invoke-virtual {v5, v0, v1}, Lru/cn/api/provider/TvContentProviderData;->getTelecastItem(J)Lru/cn/api/tv/replies/Telecast;

    move-result-object v111

    .line 718
    .restart local v111    # "telecast":Lru/cn/api/tv/replies/Telecast;
    move-object/from16 v0, p0

    move-object/from16 v1, v111

    invoke-direct {v0, v1}, Lru/cn/api/provider/TvContentProvider;->formattedAdTags(Lru/cn/api/tv/replies/Telecast;)Ljava/lang/String;
    :try_end_28
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_28} :catch_13
    .catchall {:try_start_28 .. :try_end_28} :catchall_0

    move-result-object v4

    .line 722
    .end local v111    # "telecast":Lru/cn/api/tv/replies/Telecast;
    :goto_22
    :try_start_29
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    move-wide/from16 v0, v72

    invoke-virtual {v5, v0, v1}, Lru/cn/api/provider/TvContentProviderData;->getTelecastLocations(J)Lru/cn/api/provider/TelecastLocationInfo;

    move-result-object v77

    .line 723
    .restart local v77    # "info":Lru/cn/api/provider/TelecastLocationInfo;
    if-eqz v77, :cond_1

    move-object/from16 v0, v77

    iget-object v5, v0, Lru/cn/api/provider/TelecastLocationInfo;->location:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 724
    move-object/from16 v0, v77

    iget-object v0, v0, Lru/cn/api/provider/TelecastLocationInfo;->location:Ljava/lang/String;

    move-object/from16 v87, v0

    .line 725
    .restart local v87    # "locationUri":Ljava/lang/String;
    move-object/from16 v0, v77

    iget-boolean v5, v0, Lru/cn/api/provider/TelecastLocationInfo;->adTargeting:Z

    if-eqz v5, :cond_2a

    .line 726
    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, v87

    invoke-static {v5, v0}, Lru/cn/utils/Utils;->appendAdTargetingParams(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v87

    .line 728
    :cond_2a
    const/4 v5, 0x6

    new-array v10, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v5

    const/4 v5, 0x1

    aput-object v87, v10, v5

    const/4 v5, 0x2

    move-object/from16 v0, v77

    iget-wide v0, v0, Lru/cn/api/provider/TelecastLocationInfo;->contractorId:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v5

    const/4 v5, 0x3

    move-object/from16 v0, v77

    iget-object v12, v0, Lru/cn/api/provider/TelecastLocationInfo;->status:Lru/cn/api/medialocator/replies/Rip$RipStatus;

    .line 729
    invoke-virtual {v12}, Lru/cn/api/medialocator/replies/Rip$RipStatus;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v5

    const/4 v5, 0x4

    aput-object v4, v10, v5

    const/4 v12, 0x5

    move-object/from16 v0, v77

    iget-boolean v5, v0, Lru/cn/api/provider/TelecastLocationInfo;->noAdsPartner:Z

    if-eqz v5, :cond_2b

    const/4 v5, 0x1

    :goto_23
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v10, v12

    .line 728
    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_29
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_29} :catch_9
    .catchall {:try_start_29 .. :try_end_29} :catchall_0

    goto/16 :goto_1

    .line 731
    .end local v4    # "adTags":Ljava/lang/String;
    .end local v77    # "info":Lru/cn/api/provider/TelecastLocationInfo;
    .end local v87    # "locationUri":Ljava/lang/String;
    :catch_9
    move-exception v65

    .line 732
    .restart local v65    # "e":Ljava/lang/Exception;
    :try_start_2a
    invoke-static/range {v65 .. v65}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 729
    .end local v65    # "e":Ljava/lang/Exception;
    .restart local v4    # "adTags":Ljava/lang/String;
    .restart local v77    # "info":Lru/cn/api/provider/TelecastLocationInfo;
    .restart local v87    # "locationUri":Ljava/lang/String;
    :cond_2b
    const/4 v5, 0x0

    goto :goto_23

    .line 738
    .end local v4    # "adTags":Ljava/lang/String;
    .end local v26    # "ret":Landroid/database/MatrixCursor;
    .end local v71    # "fields":[Ljava/lang/String;
    .end local v72    # "id":J
    .end local v77    # "info":Lru/cn/api/provider/TelecastLocationInfo;
    .end local v87    # "locationUri":Ljava/lang/String;
    :pswitch_11
    const/4 v5, 0x5

    new-array v0, v5, [Ljava/lang/String;

    move-object/from16 v71, v0

    const/4 v5, 0x0

    const-string v10, "_id"

    aput-object v10, v71, v5

    const/4 v5, 0x1

    const-string v10, "name"

    aput-object v10, v71, v5

    const/4 v5, 0x2

    const-string v10, "brand_name"

    aput-object v10, v71, v5

    const/4 v5, 0x3

    const-string v10, "image"

    aput-object v10, v71, v5

    const/4 v5, 0x4

    const-string v10, "private_office_uri"

    aput-object v10, v71, v5

    .line 740
    .restart local v71    # "fields":[Ljava/lang/String;
    new-instance v26, Lru/cn/api/provider/cursor/MatrixCursorEx;

    move-object/from16 v0, v26

    move-object/from16 v1, v71

    invoke-direct {v0, v1}, Lru/cn/api/provider/cursor/MatrixCursorEx;-><init>([Ljava/lang/String;)V

    .line 742
    .restart local v26    # "ret":Landroid/database/MatrixCursor;
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v72

    .line 743
    .restart local v72    # "id":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    move-wide/from16 v0, v72

    invoke-virtual {v5, v0, v1}, Lru/cn/api/provider/TvContentProviderData;->getContractor(J)Lru/cn/api/registry/replies/Contractor;

    move-result-object v57

    .line 744
    .local v57, "contractor":Lru/cn/api/registry/replies/Contractor;
    if-eqz v57, :cond_1

    .line 745
    sget-object v5, Lru/cn/api/registry/replies/ContractorImage$ProfileType;->SQUARE_SOLID:Lru/cn/api/registry/replies/ContractorImage$ProfileType;

    move-object/from16 v0, v57

    invoke-virtual {v0, v5}, Lru/cn/api/registry/replies/Contractor;->getImage(Lru/cn/api/registry/replies/ContractorImage$ProfileType;)Lru/cn/api/registry/replies/ContractorImage;

    move-result-object v19

    .line 746
    .local v19, "image":Lru/cn/api/registry/replies/ContractorImage;
    if-eqz v19, :cond_2c

    invoke-virtual/range {v19 .. v19}, Lru/cn/api/registry/replies/ContractorImage;->getUrl()Ljava/lang/String;

    move-result-object v75

    .line 748
    .local v75, "imageUri":Ljava/lang/String;
    :goto_24
    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v10, 0x0

    move-object/from16 v0, v57

    iget-wide v0, v0, Lru/cn/api/registry/replies/Contractor;->contractorId:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v5, v10

    const/4 v10, 0x1

    move-object/from16 v0, v57

    iget-object v12, v0, Lru/cn/api/registry/replies/Contractor;->name:Ljava/lang/String;

    aput-object v12, v5, v10

    const/4 v10, 0x2

    move-object/from16 v0, v57

    iget-object v12, v0, Lru/cn/api/registry/replies/Contractor;->brandName:Ljava/lang/String;

    aput-object v12, v5, v10

    const/4 v10, 0x3

    aput-object v75, v5, v10

    const/4 v10, 0x4

    move-object/from16 v0, v57

    iget-object v12, v0, Lru/cn/api/registry/replies/Contractor;->privateOfficeURL:Ljava/lang/String;

    aput-object v12, v5, v10

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 746
    .end local v75    # "imageUri":Ljava/lang/String;
    :cond_2c
    const/16 v75, 0x0

    goto :goto_24

    .line 756
    .end local v19    # "image":Lru/cn/api/registry/replies/ContractorImage;
    .end local v26    # "ret":Landroid/database/MatrixCursor;
    .end local v57    # "contractor":Lru/cn/api/registry/replies/Contractor;
    .end local v71    # "fields":[Ljava/lang/String;
    .end local v72    # "id":J
    :pswitch_12
    const/4 v5, 0x5

    new-array v0, v5, [Ljava/lang/String;

    move-object/from16 v71, v0

    const/4 v5, 0x0

    const-string v10, "_id"

    aput-object v10, v71, v5

    const/4 v5, 0x1

    const-string v10, "name"

    aput-object v10, v71, v5

    const/4 v5, 0x2

    const-string v10, "brand_name"

    aput-object v10, v71, v5

    const/4 v5, 0x3

    const-string v10, "image"

    aput-object v10, v71, v5

    const/4 v5, 0x4

    const-string v10, "private_office_uri"

    aput-object v10, v71, v5

    .line 758
    .restart local v71    # "fields":[Ljava/lang/String;
    new-instance v26, Lru/cn/api/provider/cursor/MatrixCursorEx;

    move-object/from16 v0, v26

    move-object/from16 v1, v71

    invoke-direct {v0, v1}, Lru/cn/api/provider/cursor/MatrixCursorEx;-><init>([Ljava/lang/String;)V
    :try_end_2a
    .catchall {:try_start_2a .. :try_end_2a} :catchall_0

    .line 761
    .restart local v26    # "ret":Landroid/database/MatrixCursor;
    :try_start_2b
    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lru/cn/api/ServiceLocator;->getWhereAmI(Landroid/content/Context;)Lru/cn/api/registry/replies/WhereAmI;

    move-result-object v118

    .line 762
    .local v118, "whereAmI":Lru/cn/api/registry/replies/WhereAmI;
    if-eqz v118, :cond_1

    move-object/from16 v0, v118

    iget-object v5, v0, Lru/cn/api/registry/replies/WhereAmI;->contractor:Lru/cn/api/registry/replies/Contractor;

    if-eqz v5, :cond_1

    .line 763
    move-object/from16 v0, v118

    iget-object v0, v0, Lru/cn/api/registry/replies/WhereAmI;->contractor:Lru/cn/api/registry/replies/Contractor;

    move-object/from16 v57, v0

    .line 764
    .restart local v57    # "contractor":Lru/cn/api/registry/replies/Contractor;
    sget-object v5, Lru/cn/api/registry/replies/ContractorImage$ProfileType;->SQUARE_SOLID:Lru/cn/api/registry/replies/ContractorImage$ProfileType;

    move-object/from16 v0, v57

    invoke-virtual {v0, v5}, Lru/cn/api/registry/replies/Contractor;->getImage(Lru/cn/api/registry/replies/ContractorImage$ProfileType;)Lru/cn/api/registry/replies/ContractorImage;

    move-result-object v19

    .line 765
    .restart local v19    # "image":Lru/cn/api/registry/replies/ContractorImage;
    if-eqz v19, :cond_2d

    invoke-virtual/range {v19 .. v19}, Lru/cn/api/registry/replies/ContractorImage;->getUrl()Ljava/lang/String;

    move-result-object v75

    .line 767
    .restart local v75    # "imageUri":Ljava/lang/String;
    :goto_25
    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v10, 0x0

    move-object/from16 v0, v57

    iget-wide v0, v0, Lru/cn/api/registry/replies/Contractor;->contractorId:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v5, v10

    const/4 v10, 0x1

    move-object/from16 v0, v57

    iget-object v12, v0, Lru/cn/api/registry/replies/Contractor;->name:Ljava/lang/String;

    aput-object v12, v5, v10

    const/4 v10, 0x2

    move-object/from16 v0, v57

    iget-object v12, v0, Lru/cn/api/registry/replies/Contractor;->brandName:Ljava/lang/String;

    aput-object v12, v5, v10

    const/4 v10, 0x3

    aput-object v75, v5, v10

    const/4 v10, 0x4

    move-object/from16 v0, v57

    iget-object v12, v0, Lru/cn/api/registry/replies/Contractor;->privateOfficeURL:Ljava/lang/String;

    aput-object v12, v5, v10

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_2b
    .catch Ljava/lang/Exception; {:try_start_2b .. :try_end_2b} :catch_a
    .catchall {:try_start_2b .. :try_end_2b} :catchall_0

    goto/16 :goto_1

    .line 771
    .end local v19    # "image":Lru/cn/api/registry/replies/ContractorImage;
    .end local v57    # "contractor":Lru/cn/api/registry/replies/Contractor;
    .end local v75    # "imageUri":Ljava/lang/String;
    .end local v118    # "whereAmI":Lru/cn/api/registry/replies/WhereAmI;
    :catch_a
    move-exception v65

    .line 772
    .restart local v65    # "e":Ljava/lang/Exception;
    :try_start_2c
    invoke-static/range {v65 .. v65}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 765
    .end local v65    # "e":Ljava/lang/Exception;
    .restart local v19    # "image":Lru/cn/api/registry/replies/ContractorImage;
    .restart local v57    # "contractor":Lru/cn/api/registry/replies/Contractor;
    .restart local v118    # "whereAmI":Lru/cn/api/registry/replies/WhereAmI;
    :cond_2d
    const/16 v75, 0x0

    goto :goto_25

    .line 778
    .end local v19    # "image":Lru/cn/api/registry/replies/ContractorImage;
    .end local v26    # "ret":Landroid/database/MatrixCursor;
    .end local v57    # "contractor":Lru/cn/api/registry/replies/Contractor;
    .end local v71    # "fields":[Ljava/lang/String;
    .end local v118    # "whereAmI":Lru/cn/api/registry/replies/WhereAmI;
    :pswitch_13
    const/4 v5, 0x5

    new-array v0, v5, [Ljava/lang/String;

    move-object/from16 v71, v0

    const/4 v5, 0x0

    const-string v10, "_id"

    aput-object v10, v71, v5

    const/4 v5, 0x1

    const-string v10, "name"

    aput-object v10, v71, v5

    const/4 v5, 0x2

    const-string v10, "brand_name"

    aput-object v10, v71, v5

    const/4 v5, 0x3

    const-string v10, "image"

    aput-object v10, v71, v5

    const/4 v5, 0x4

    const-string v10, "private_office_uri"

    aput-object v10, v71, v5

    .line 780
    .restart local v71    # "fields":[Ljava/lang/String;
    new-instance v11, Lru/cn/api/provider/cursor/MatrixCursorEx;

    move-object/from16 v0, v71

    invoke-direct {v11, v0}, Lru/cn/api/provider/cursor/MatrixCursorEx;-><init>([Ljava/lang/String;)V
    :try_end_2c
    .catchall {:try_start_2c .. :try_end_2c} :catchall_0

    .line 783
    .local v11, "cursor":Landroid/database/MatrixCursor;
    :try_start_2d
    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    sget-object v10, Lru/cn/api/registry/replies/Service$Type;->iptv:Lru/cn/api/registry/replies/Service$Type;

    invoke-static {v5, v10}, Lru/cn/api/ServiceLocator;->contractorsByService(Landroid/content/Context;Lru/cn/api/registry/replies/Service$Type;)Ljava/util/List;

    move-result-object v60

    .line 784
    .local v60, "contractors":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/registry/replies/Contractor;>;"
    invoke-interface/range {v60 .. v60}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2e
    :goto_26
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_30

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v57

    check-cast v57, Lru/cn/api/registry/replies/Contractor;

    .line 785
    .restart local v57    # "contractor":Lru/cn/api/registry/replies/Contractor;
    move-object/from16 v0, v57

    iget-object v10, v0, Lru/cn/api/registry/replies/Contractor;->privateOfficeURL:Ljava/lang/String;

    if-eqz v10, :cond_2e

    .line 788
    sget-object v98, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;->MOBILE:Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

    .line 789
    .local v98, "requiredIdiom":Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v10

    if-eqz v10, :cond_2f

    .line 790
    sget-object v98, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;->TV:Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

    .line 793
    :cond_2f
    move-object/from16 v0, v57

    iget-object v10, v0, Lru/cn/api/registry/replies/Contractor;->supportedOfficeIdioms:Ljava/util/List;

    move-object/from16 v0, v98

    invoke-interface {v10, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2e

    .line 796
    sget-object v10, Lru/cn/api/registry/replies/ContractorImage$ProfileType;->SQUARE_SOLID:Lru/cn/api/registry/replies/ContractorImage$ProfileType;

    move-object/from16 v0, v57

    invoke-virtual {v0, v10}, Lru/cn/api/registry/replies/Contractor;->getImage(Lru/cn/api/registry/replies/ContractorImage$ProfileType;)Lru/cn/api/registry/replies/ContractorImage;

    move-result-object v19

    .line 797
    .restart local v19    # "image":Lru/cn/api/registry/replies/ContractorImage;
    if-eqz v19, :cond_31

    invoke-virtual/range {v19 .. v19}, Lru/cn/api/registry/replies/ContractorImage;->getUrl()Ljava/lang/String;

    move-result-object v75

    .line 799
    .restart local v75    # "imageUri":Ljava/lang/String;
    :goto_27
    const/4 v10, 0x5

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v12, 0x0

    move-object/from16 v0, v57

    iget-wide v0, v0, Lru/cn/api/registry/replies/Contractor;->contractorId:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v10, v12

    const/4 v12, 0x1

    move-object/from16 v0, v57

    iget-object v0, v0, Lru/cn/api/registry/replies/Contractor;->name:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v10, v12

    const/4 v12, 0x2

    move-object/from16 v0, v57

    iget-object v0, v0, Lru/cn/api/registry/replies/Contractor;->brandName:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v10, v12

    const/4 v12, 0x3

    aput-object v75, v10, v12

    const/4 v12, 0x4

    move-object/from16 v0, v57

    iget-object v0, v0, Lru/cn/api/registry/replies/Contractor;->privateOfficeURL:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v10, v12

    invoke-virtual {v11, v10}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_2d
    .catch Ljava/lang/Exception; {:try_start_2d .. :try_end_2d} :catch_b
    .catchall {:try_start_2d .. :try_end_2d} :catchall_0

    goto :goto_26

    .line 803
    .end local v19    # "image":Lru/cn/api/registry/replies/ContractorImage;
    .end local v57    # "contractor":Lru/cn/api/registry/replies/Contractor;
    .end local v60    # "contractors":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/registry/replies/Contractor;>;"
    .end local v75    # "imageUri":Ljava/lang/String;
    .end local v98    # "requiredIdiom":Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;
    :catch_b
    move-exception v65

    .line 804
    .restart local v65    # "e":Ljava/lang/Exception;
    :try_start_2e
    const-string v5, "TvContentProvider"

    const-string v10, "Unable to get contractors"

    move-object/from16 v0, v65

    invoke-static {v5, v10, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v65    # "e":Ljava/lang/Exception;
    :cond_30
    move-object/from16 v26, v11

    .line 807
    goto/16 :goto_1

    .line 797
    .restart local v19    # "image":Lru/cn/api/registry/replies/ContractorImage;
    .restart local v57    # "contractor":Lru/cn/api/registry/replies/Contractor;
    .restart local v60    # "contractors":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/registry/replies/Contractor;>;"
    .restart local v98    # "requiredIdiom":Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;
    :cond_31
    const/16 v75, 0x0

    goto :goto_27

    .line 811
    .end local v11    # "cursor":Landroid/database/MatrixCursor;
    .end local v19    # "image":Lru/cn/api/registry/replies/ContractorImage;
    .end local v57    # "contractor":Lru/cn/api/registry/replies/Contractor;
    .end local v60    # "contractors":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/registry/replies/Contractor;>;"
    .end local v71    # "fields":[Ljava/lang/String;
    .end local v98    # "requiredIdiom":Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;
    :pswitch_14
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v16

    move-wide/from16 v0, v16

    long-to-int v5, v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v91

    .line 812
    .local v91, "number":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    move-object/from16 v0, v91

    move-object/from16 v1, p3

    invoke-virtual {v5, v0, v1}, Lru/cn/api/provider/TvContentProviderData;->getChannelByNumber(Ljava/lang/Integer;Ljava/lang/String;)Lru/cn/api/provider/Channel;

    move-result-object v49

    .line 814
    .restart local v49    # "channel":Lru/cn/api/provider/Channel;
    new-instance v15, Lru/cn/api/provider/cursor/ChannelCursor;

    invoke-direct {v15}, Lru/cn/api/provider/cursor/ChannelCursor;-><init>()V

    .line 815
    .local v15, "c":Lru/cn/api/provider/cursor/ChannelCursor;
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v49

    invoke-direct {v0, v15, v1, v5}, Lru/cn/api/provider/TvContentProvider;->populateChannelCursor(Lru/cn/api/provider/cursor/ChannelCursor;Lru/cn/api/provider/Channel;Z)V

    .line 816
    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v15, v5, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    move-object/from16 v26, v15

    .line 818
    goto/16 :goto_1

    .line 822
    .end local v15    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    .end local v49    # "channel":Lru/cn/api/provider/Channel;
    .end local v91    # "number":Ljava/lang/Integer;
    :pswitch_15
    const/4 v5, 0x1

    new-array v0, v5, [Ljava/lang/String;

    move-object/from16 v71, v0

    const/4 v5, 0x0

    const-string v10, "pincode"

    aput-object v10, v71, v5

    .line 823
    .restart local v71    # "fields":[Ljava/lang/String;
    new-instance v26, Lru/cn/api/provider/cursor/MatrixCursorEx;

    move-object/from16 v0, v26

    move-object/from16 v1, v71

    invoke-direct {v0, v1}, Lru/cn/api/provider/cursor/MatrixCursorEx;-><init>([Ljava/lang/String;)V
    :try_end_2e
    .catchall {:try_start_2e .. :try_end_2e} :catchall_0

    .line 826
    .restart local v26    # "ret":Landroid/database/MatrixCursor;
    :try_start_2f
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v5}, Lru/cn/api/provider/TvContentProviderData;->getPinCode()Ljava/lang/String;

    move-result-object v93

    .line 827
    .local v93, "pinCode":Ljava/lang/String;
    if-eqz v93, :cond_32

    .line 828
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v93, v5, v10

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_2f
    .catch Ljava/lang/Exception; {:try_start_2f .. :try_end_2f} :catch_c
    .catchall {:try_start_2f .. :try_end_2f} :catchall_0

    .line 834
    .end local v93    # "pinCode":Ljava/lang/String;
    :cond_32
    :goto_28
    :try_start_30
    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v5, v1}, Landroid/database/MatrixCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto/16 :goto_1

    .line 830
    :catch_c
    move-exception v65

    .line 831
    .restart local v65    # "e":Ljava/lang/Exception;
    invoke-static/range {v65 .. v65}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_28

    .line 840
    .end local v26    # "ret":Landroid/database/MatrixCursor;
    .end local v65    # "e":Ljava/lang/Exception;
    .end local v71    # "fields":[Ljava/lang/String;
    :pswitch_16
    const/4 v5, 0x2

    new-array v0, v5, [Ljava/lang/String;

    move-object/from16 v71, v0

    const/4 v5, 0x0

    const-string v10, "_id"

    aput-object v10, v71, v5

    const/4 v5, 0x1

    const-string v10, "data"

    aput-object v10, v71, v5

    .line 841
    .restart local v71    # "fields":[Ljava/lang/String;
    new-instance v26, Lru/cn/api/provider/cursor/MatrixCursorEx;

    move-object/from16 v0, v26

    move-object/from16 v1, v71

    invoke-direct {v0, v1}, Lru/cn/api/provider/cursor/MatrixCursorEx;-><init>([Ljava/lang/String;)V
    :try_end_30
    .catchall {:try_start_30 .. :try_end_30} :catchall_0

    .line 844
    .restart local v26    # "ret":Landroid/database/MatrixCursor;
    :try_start_31
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v5}, Lru/cn/api/provider/TvContentProviderData;->getRubricator()Lru/cn/api/catalogue/replies/Rubricator;

    move-result-object v96

    .line 845
    .local v96, "r":Lru/cn/api/catalogue/replies/Rubricator;
    if-eqz v96, :cond_33

    .line 846
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v5, v10

    const/4 v10, 0x1

    invoke-virtual/range {v96 .. v96}, Lru/cn/api/catalogue/replies/Rubricator;->toJson()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v5, v10

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_31
    .catch Ljava/lang/Exception; {:try_start_31 .. :try_end_31} :catch_d
    .catchall {:try_start_31 .. :try_end_31} :catchall_0

    .line 852
    .end local v96    # "r":Lru/cn/api/catalogue/replies/Rubricator;
    :cond_33
    :goto_29
    :try_start_32
    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v5, v1}, Landroid/database/MatrixCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    goto/16 :goto_1

    .line 848
    :catch_d
    move-exception v65

    .line 849
    .restart local v65    # "e":Ljava/lang/Exception;
    invoke-static/range {v65 .. v65}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_29

    .line 857
    .end local v26    # "ret":Landroid/database/MatrixCursor;
    .end local v65    # "e":Ljava/lang/Exception;
    .end local v71    # "fields":[Ljava/lang/String;
    :pswitch_17
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v104

    .line 858
    .local v104, "rubricId":J
    const/4 v5, 0x2

    new-array v0, v5, [Ljava/lang/String;

    move-object/from16 v71, v0

    const/4 v5, 0x0

    const-string v10, "_id"

    aput-object v10, v71, v5

    const/4 v5, 0x1

    const-string v10, "data"

    aput-object v10, v71, v5

    .line 859
    .restart local v71    # "fields":[Ljava/lang/String;
    new-instance v26, Lru/cn/api/provider/cursor/MatrixCursorEx;

    move-object/from16 v0, v26

    move-object/from16 v1, v71

    invoke-direct {v0, v1}, Lru/cn/api/provider/cursor/MatrixCursorEx;-><init>([Ljava/lang/String;)V
    :try_end_32
    .catchall {:try_start_32 .. :try_end_32} :catchall_0

    .line 861
    .restart local v26    # "ret":Landroid/database/MatrixCursor;
    const-wide/16 v16, -0x1

    cmp-long v5, v104, v16

    if-eqz v5, :cond_1

    .line 863
    :try_start_33
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    move-wide/from16 v0, v104

    invoke-virtual {v5, v0, v1}, Lru/cn/api/provider/TvContentProviderData;->getRubricInfo(J)Lru/cn/api/catalogue/replies/Rubric;

    move-result-object v96

    .line 864
    .local v96, "r":Lru/cn/api/catalogue/replies/Rubric;
    if-eqz v96, :cond_1

    .line 865
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static/range {v104 .. v105}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v5, v10

    const/4 v10, 0x1

    new-instance v12, Lcom/google/gson/Gson;

    invoke-direct {v12}, Lcom/google/gson/Gson;-><init>()V

    move-object/from16 v0, v96

    invoke-virtual {v12, v0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v5, v10

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_33
    .catch Ljava/lang/Exception; {:try_start_33 .. :try_end_33} :catch_e
    .catchall {:try_start_33 .. :try_end_33} :catchall_0

    goto/16 :goto_1

    .line 867
    .end local v96    # "r":Lru/cn/api/catalogue/replies/Rubric;
    :catch_e
    move-exception v65

    .line 868
    .restart local v65    # "e":Ljava/lang/Exception;
    :try_start_34
    invoke-static/range {v65 .. v65}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 875
    .end local v26    # "ret":Landroid/database/MatrixCursor;
    .end local v65    # "e":Ljava/lang/Exception;
    .end local v71    # "fields":[Ljava/lang/String;
    .end local v104    # "rubricId":J
    :pswitch_18
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v50

    .line 876
    .local v50, "catalogueId":J
    const/4 v5, 0x2

    new-array v0, v5, [Ljava/lang/String;

    move-object/from16 v71, v0

    const/4 v5, 0x0

    const-string v10, "_id"

    aput-object v10, v71, v5

    const/4 v5, 0x1

    const-string v10, "data"

    aput-object v10, v71, v5

    .line 877
    .restart local v71    # "fields":[Ljava/lang/String;
    new-instance v26, Lru/cn/api/provider/cursor/MatrixCursorEx;

    move-object/from16 v0, v26

    move-object/from16 v1, v71

    invoke-direct {v0, v1}, Lru/cn/api/provider/cursor/MatrixCursorEx;-><init>([Ljava/lang/String;)V
    :try_end_34
    .catchall {:try_start_34 .. :try_end_34} :catchall_0

    .line 879
    .restart local v26    # "ret":Landroid/database/MatrixCursor;
    const-wide/16 v16, -0x1

    cmp-long v5, v50, v16

    if-eqz v5, :cond_1

    .line 881
    :try_start_35
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    move-wide/from16 v0, v50

    invoke-virtual {v5, v0, v1}, Lru/cn/api/provider/TvContentProviderData;->getRelatedRubrics(J)Ljava/util/List;

    move-result-object v106

    .line 882
    .local v106, "rubrics":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/catalogue/replies/Rubric;>;"
    if-eqz v106, :cond_1

    .line 883
    invoke-interface/range {v106 .. v106}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2a
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v102

    check-cast v102, Lru/cn/api/catalogue/replies/Rubric;

    .line 884
    .local v102, "rubric":Lru/cn/api/catalogue/replies/Rubric;
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v12, 0x0

    move-object/from16 v0, v102

    iget-wide v0, v0, Lru/cn/api/catalogue/replies/Rubric;->id:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v10, v12

    const/4 v12, 0x1

    new-instance v16, Lcom/google/gson/Gson;

    invoke-direct/range {v16 .. v16}, Lcom/google/gson/Gson;-><init>()V

    move-object/from16 v0, v16

    move-object/from16 v1, v102

    invoke-virtual {v0, v1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v10, v12

    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_35
    .catch Ljava/lang/Exception; {:try_start_35 .. :try_end_35} :catch_f
    .catchall {:try_start_35 .. :try_end_35} :catchall_0

    goto :goto_2a

    .line 888
    .end local v102    # "rubric":Lru/cn/api/catalogue/replies/Rubric;
    .end local v106    # "rubrics":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/catalogue/replies/Rubric;>;"
    :catch_f
    move-exception v65

    .line 889
    .restart local v65    # "e":Ljava/lang/Exception;
    :try_start_36
    invoke-static/range {v65 .. v65}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 897
    .end local v26    # "ret":Landroid/database/MatrixCursor;
    .end local v50    # "catalogueId":J
    .end local v65    # "e":Ljava/lang/Exception;
    .end local v71    # "fields":[Ljava/lang/String;
    :pswitch_19
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v104

    .line 898
    .restart local v104    # "rubricId":J
    const-string v5, "rubric_options"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v92

    .line 899
    .local v92, "options":Ljava/lang/String;
    new-instance v26, Lru/cn/api/provider/cursor/TelecastItemCursor;

    invoke-direct/range {v26 .. v26}, Lru/cn/api/provider/cursor/TelecastItemCursor;-><init>()V
    :try_end_36
    .catchall {:try_start_36 .. :try_end_36} :catchall_0

    .line 901
    .local v26, "ret":Lru/cn/api/provider/cursor/TelecastItemCursor;
    const-wide/16 v16, -0x1

    cmp-long v5, v104, v16

    if-eqz v5, :cond_34

    .line 906
    if-eqz p3, :cond_35

    :try_start_37
    const-string v5, "more"

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_35

    .line 907
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    move-wide/from16 v0, v104

    move-object/from16 v2, v92

    invoke-virtual {v5, v0, v1, v2}, Lru/cn/api/provider/TvContentProviderData;->getMoreRubricItems(JLjava/lang/String;)Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;

    move-result-object v101

    .line 912
    .local v101, "rubTelecastCache":Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;
    :goto_2b
    move-object/from16 v0, v101

    iget-object v0, v0, Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;->telecasts:Ljava/util/List;

    move-object/from16 v114, v0

    .line 914
    .restart local v114    # "telecasts":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v114

    invoke-direct {v0, v1}, Lru/cn/api/provider/TvContentProvider;->getCachedLocations(Ljava/util/List;)Landroid/support/v4/util/LongSparseArray;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, v114

    invoke-direct {v0, v1, v5}, Lru/cn/api/provider/TvContentProvider;->buildCursorFromTelecast(Ljava/util/List;Landroid/support/v4/util/LongSparseArray;)Lru/cn/api/provider/cursor/TelecastItemCursor;

    move-result-object v26

    .line 915
    move-object/from16 v0, v101

    iget-boolean v5, v0, Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;->noMoreData:Z

    if-eqz v5, :cond_34

    .line 916
    new-instance v67, Landroid/os/Bundle;

    invoke-direct/range {v67 .. v67}, Landroid/os/Bundle;-><init>()V

    .line 917
    .restart local v67    # "extras":Landroid/os/Bundle;
    const-string v5, "no_more_data"

    const/4 v10, 0x1

    move-object/from16 v0, v67

    invoke-virtual {v0, v5, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 918
    move-object/from16 v0, v26

    move-object/from16 v1, v67

    invoke-virtual {v0, v1}, Lru/cn/api/provider/cursor/TelecastItemCursor;->setExtras(Landroid/os/Bundle;)V
    :try_end_37
    .catch Ljava/lang/Exception; {:try_start_37 .. :try_end_37} :catch_10
    .catchall {:try_start_37 .. :try_end_37} :catchall_0

    .line 925
    .end local v67    # "extras":Landroid/os/Bundle;
    .end local v101    # "rubTelecastCache":Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;
    .end local v114    # "telecasts":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    :cond_34
    :goto_2c
    :try_start_38
    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v5, v1}, Lru/cn/api/provider/cursor/TelecastItemCursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    :try_end_38
    .catchall {:try_start_38 .. :try_end_38} :catchall_0

    goto/16 :goto_1

    .line 909
    :cond_35
    :try_start_39
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    move-wide/from16 v0, v104

    move-object/from16 v2, v92

    invoke-virtual {v5, v0, v1, v2}, Lru/cn/api/provider/TvContentProviderData;->getRubricItems(JLjava/lang/String;)Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;
    :try_end_39
    .catch Ljava/lang/Exception; {:try_start_39 .. :try_end_39} :catch_10
    .catchall {:try_start_39 .. :try_end_39} :catchall_0

    move-result-object v101

    .restart local v101    # "rubTelecastCache":Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;
    goto :goto_2b

    .line 920
    .end local v101    # "rubTelecastCache":Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;
    :catch_10
    move-exception v65

    .line 921
    .restart local v65    # "e":Ljava/lang/Exception;
    :try_start_3a
    invoke-static/range {v65 .. v65}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_2c

    .line 930
    .end local v26    # "ret":Lru/cn/api/provider/cursor/TelecastItemCursor;
    .end local v65    # "e":Ljava/lang/Exception;
    .end local v92    # "options":Ljava/lang/String;
    .end local v104    # "rubricId":J
    :pswitch_1a
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v104

    .line 931
    .restart local v104    # "rubricId":J
    const-string v5, "rubric_options"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v92

    .line 932
    .restart local v92    # "options":Ljava/lang/String;
    new-instance v62, Ljava/util/ArrayList;

    invoke-direct/range {v62 .. v62}, Ljava/util/ArrayList;-><init>()V
    :try_end_3a
    .catchall {:try_start_3a .. :try_end_3a} :catchall_0

    .line 933
    .local v62, "cursorsForMerge":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/database/Cursor;>;"
    const/16 v114, 0x0

    .line 934
    .restart local v114    # "telecasts":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    const/16 v55, 0x0

    .line 935
    .restart local v55    # "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    const/16 v67, 0x0

    .line 938
    .restart local v67    # "extras":Landroid/os/Bundle;
    const-wide/16 v16, -0x1

    cmp-long v5, v104, v16

    if-eqz v5, :cond_36

    .line 942
    :try_start_3b
    move-object/from16 v0, p0

    iget-object v5, v0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    const/16 v10, 0x64

    move-wide/from16 v0, v104

    move-object/from16 v2, v92

    invoke-virtual {v5, v0, v1, v2, v10}, Lru/cn/api/provider/TvContentProviderData;->getRubricItems(JLjava/lang/String;I)Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;

    move-result-object v100

    .line 943
    .local v100, "rubItemsCache":Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;
    move-object/from16 v0, v100

    iget-object v0, v0, Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;->telecasts:Ljava/util/List;

    move-object/from16 v114, v0

    .line 944
    move-object/from16 v0, v100

    iget-object v0, v0, Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;->channels:Ljava/util/List;

    move-object/from16 v55, v0

    .line 946
    new-instance v68, Landroid/os/Bundle;

    invoke-direct/range {v68 .. v68}, Landroid/os/Bundle;-><init>()V
    :try_end_3b
    .catch Ljava/lang/Exception; {:try_start_3b .. :try_end_3b} :catch_11
    .catchall {:try_start_3b .. :try_end_3b} :catchall_0

    .line 947
    .end local v67    # "extras":Landroid/os/Bundle;
    .local v68, "extras":Landroid/os/Bundle;
    :try_start_3c
    const-string v5, "no_more_data"

    const/4 v10, 0x1

    move-object/from16 v0, v68

    invoke-virtual {v0, v5, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
    :try_end_3c
    .catch Ljava/lang/Exception; {:try_start_3c .. :try_end_3c} :catch_12
    .catchall {:try_start_3c .. :try_end_3c} :catchall_0

    move-object/from16 v67, v68

    .line 954
    .end local v68    # "extras":Landroid/os/Bundle;
    .end local v100    # "rubItemsCache":Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;
    .restart local v67    # "extras":Landroid/os/Bundle;
    :cond_36
    :goto_2d
    :try_start_3d
    new-instance v56, Lru/cn/api/provider/cursor/ChannelCursor;

    invoke-direct/range {v56 .. v56}, Lru/cn/api/provider/cursor/ChannelCursor;-><init>()V

    .line 955
    .local v56, "channelsCursor":Lru/cn/api/provider/cursor/ChannelCursor;
    if-eqz v55, :cond_38

    .line 956
    invoke-interface/range {v55 .. v55}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2e
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v49

    check-cast v49, Lru/cn/api/provider/Channel;

    .line 957
    .restart local v49    # "channel":Lru/cn/api/provider/Channel;
    const/4 v10, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v56

    move-object/from16 v2, v49

    invoke-direct {v0, v1, v2, v10}, Lru/cn/api/provider/TvContentProvider;->populateChannelCursor(Lru/cn/api/provider/cursor/ChannelCursor;Lru/cn/api/provider/Channel;Z)V

    goto :goto_2e

    .line 949
    .end local v49    # "channel":Lru/cn/api/provider/Channel;
    .end local v56    # "channelsCursor":Lru/cn/api/provider/cursor/ChannelCursor;
    :catch_11
    move-exception v65

    .line 950
    .restart local v65    # "e":Ljava/lang/Exception;
    :goto_2f
    invoke-static/range {v65 .. v65}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_2d

    .line 960
    .end local v65    # "e":Ljava/lang/Exception;
    .restart local v56    # "channelsCursor":Lru/cn/api/provider/cursor/ChannelCursor;
    :cond_37
    invoke-virtual/range {v56 .. v56}, Lru/cn/api/provider/cursor/ChannelCursor;->getCount()I

    move-result v5

    if-lez v5, :cond_38

    .line 961
    new-instance v89, Landroid/database/MatrixCursor;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v12, "_id"

    aput-object v12, v5, v10

    const/4 v10, 0x1

    const-string v12, "title"

    aput-object v12, v5, v10

    move-object/from16 v0, v89

    invoke-direct {v0, v5}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 964
    .local v89, "m":Landroid/database/MatrixCursor;
    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v10, 0x7f0e012a

    invoke-virtual {v5, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v116

    .line 965
    .local v116, "titleChannels":Ljava/lang/String;
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v5, v10

    const/4 v10, 0x1

    aput-object v116, v5, v10

    move-object/from16 v0, v89

    invoke-virtual {v0, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 966
    move-object/from16 v0, v62

    move-object/from16 v1, v89

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 969
    .end local v89    # "m":Landroid/database/MatrixCursor;
    .end local v116    # "titleChannels":Ljava/lang/String;
    :cond_38
    move-object/from16 v0, v62

    move-object/from16 v1, v56

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 972
    move-object/from16 v0, p0

    move-object/from16 v1, v114

    invoke-direct {v0, v1}, Lru/cn/api/provider/TvContentProvider;->getCachedLocations(Ljava/util/List;)Landroid/support/v4/util/LongSparseArray;

    move-result-object v5

    .line 971
    move-object/from16 v0, p0

    move-object/from16 v1, v114

    invoke-direct {v0, v1, v5}, Lru/cn/api/provider/TvContentProvider;->buildCursorFromTelecast(Ljava/util/List;Landroid/support/v4/util/LongSparseArray;)Lru/cn/api/provider/cursor/TelecastItemCursor;

    move-result-object v115

    .line 973
    .local v115, "telecastsCursor":Lru/cn/api/provider/cursor/TelecastItemCursor;
    if-eqz v114, :cond_39

    invoke-interface/range {v114 .. v114}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_39

    invoke-virtual/range {v115 .. v115}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getCount()I

    move-result v5

    if-lez v5, :cond_39

    .line 974
    new-instance v89, Landroid/database/MatrixCursor;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v12, "_id"

    aput-object v12, v5, v10

    const/4 v10, 0x1

    const-string v12, "title"

    aput-object v12, v5, v10

    move-object/from16 v0, v89

    invoke-direct {v0, v5}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 977
    .restart local v89    # "m":Landroid/database/MatrixCursor;
    invoke-virtual/range {p0 .. p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v10, 0x7f0e012d

    invoke-virtual {v5, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v117

    .line 978
    .local v117, "titleTelecasts":Ljava/lang/String;
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v5, v10

    const/4 v10, 0x1

    aput-object v117, v5, v10

    move-object/from16 v0, v89

    invoke-virtual {v0, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 979
    move-object/from16 v0, v62

    move-object/from16 v1, v89

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 981
    .end local v89    # "m":Landroid/database/MatrixCursor;
    .end local v117    # "titleTelecasts":Ljava/lang/String;
    :cond_39
    move-object/from16 v0, v62

    move-object/from16 v1, v115

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 984
    invoke-virtual/range {v62 .. v62}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v0, v5, [Landroid/database/Cursor;

    move-object/from16 v61, v0

    .line 985
    .local v61, "cursors":[Landroid/database/Cursor;
    move-object/from16 v0, v62

    move-object/from16 v1, v61

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 986
    new-instance v11, Lru/cn/api/provider/cursor/MergeCursorEx;

    move-object/from16 v0, v61

    invoke-direct {v11, v0}, Lru/cn/api/provider/cursor/MergeCursorEx;-><init>([Landroid/database/Cursor;)V

    .line 987
    .local v11, "cursor":Lru/cn/api/provider/cursor/MergeCursorEx;
    if-eqz v67, :cond_3a

    .line 988
    move-object/from16 v0, v67

    invoke-virtual {v11, v0}, Lru/cn/api/provider/cursor/MergeCursorEx;->setExtras(Landroid/os/Bundle;)V
    :try_end_3d
    .catchall {:try_start_3d .. :try_end_3d} :catchall_0

    :cond_3a
    move-object/from16 v26, v11

    .line 990
    goto/16 :goto_1

    .line 949
    .end local v11    # "cursor":Lru/cn/api/provider/cursor/MergeCursorEx;
    .end local v56    # "channelsCursor":Lru/cn/api/provider/cursor/ChannelCursor;
    .end local v61    # "cursors":[Landroid/database/Cursor;
    .end local v67    # "extras":Landroid/os/Bundle;
    .end local v115    # "telecastsCursor":Lru/cn/api/provider/cursor/TelecastItemCursor;
    .restart local v68    # "extras":Landroid/os/Bundle;
    .restart local v100    # "rubItemsCache":Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;
    :catch_12
    move-exception v65

    move-object/from16 v67, v68

    .end local v68    # "extras":Landroid/os/Bundle;
    .restart local v67    # "extras":Landroid/os/Bundle;
    goto/16 :goto_2f

    .line 719
    .end local v55    # "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    .end local v62    # "cursorsForMerge":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/database/Cursor;>;"
    .end local v67    # "extras":Landroid/os/Bundle;
    .end local v92    # "options":Ljava/lang/String;
    .end local v100    # "rubItemsCache":Lru/cn/api/provider/TvContentProviderData$RubricTelecastCacheObj;
    .end local v104    # "rubricId":J
    .end local v114    # "telecasts":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    .restart local v4    # "adTags":Ljava/lang/String;
    .local v26, "ret":Landroid/database/MatrixCursor;
    .restart local v71    # "fields":[Ljava/lang/String;
    .restart local v72    # "id":J
    :catch_13
    move-exception v5

    goto/16 :goto_22

    .line 574
    .end local v4    # "adTags":Ljava/lang/String;
    .end local v26    # "ret":Landroid/database/MatrixCursor;
    .end local v71    # "fields":[Ljava/lang/String;
    .end local v72    # "id":J
    .restart local v13    # "cnId":J
    .restart local v15    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    .restart local v69    # "favourite":Z
    :catch_14
    move-exception v5

    goto/16 :goto_1a

    .line 561
    :catch_15
    move-exception v5

    goto/16 :goto_19

    .line 535
    .end local v15    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    .end local v69    # "favourite":Z
    .restart local v4    # "adTags":Ljava/lang/String;
    .restart local v26    # "ret":Landroid/database/MatrixCursor;
    .restart local v71    # "fields":[Ljava/lang/String;
    .restart local v88    # "locations":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/iptv/replies/MediaLocation;>;"
    :catch_16
    move-exception v5

    goto/16 :goto_15

    .line 507
    .end local v4    # "adTags":Ljava/lang/String;
    .end local v26    # "ret":Landroid/database/MatrixCursor;
    .end local v71    # "fields":[Ljava/lang/String;
    .end local v88    # "locations":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/iptv/replies/MediaLocation;>;"
    .restart local v49    # "channel":Lru/cn/api/provider/Channel;
    :catch_17
    move-exception v5

    goto/16 :goto_14

    .line 476
    .end local v13    # "cnId":J
    .end local v49    # "channel":Lru/cn/api/provider/Channel;
    .local v15, "c":Landroid/database/MatrixCursor;
    .restart local v47    # "age":I
    .restart local v48    # "ageFilter":Ljava/lang/String;
    .restart local v52    # "channelFields":[Ljava/lang/String;
    .restart local v55    # "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    :catch_18
    move-exception v5

    goto/16 :goto_13

    .line 429
    .end local v15    # "c":Landroid/database/MatrixCursor;
    .end local v52    # "channelFields":[Ljava/lang/String;
    .restart local v70    # "favouriteChannels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    :catch_19
    move-exception v5

    goto/16 :goto_e

    .line 387
    .end local v47    # "age":I
    .end local v48    # "ageFilter":Ljava/lang/String;
    .end local v55    # "channels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    .end local v70    # "favouriteChannels":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/provider/Channel;>;"
    .local v8, "territoryId":Ljava/lang/Long;
    .local v11, "cursor":Lru/cn/api/provider/cursor/TelecastItemCursor;
    .restart local v74    # "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v83    # "loadedInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Lru/cn/api/tv/replies/Telecast;>;"
    .restart local v97    # "requestsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/util/List<Ljava/lang/Long;>;>;"
    .restart local v114    # "telecasts":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    :catch_1a
    move-exception v10

    goto/16 :goto_9

    .line 359
    .end local v8    # "territoryId":Ljava/lang/Long;
    .end local v74    # "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v114    # "telecasts":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/tv/replies/Telecast;>;"
    .restart local v6    # "channelId":J
    .restart local v49    # "channel":Lru/cn/api/provider/Channel;
    .restart local v53    # "channelIdString":Ljava/lang/String;
    :catch_1b
    move-exception v5

    goto/16 :goto_7

    .line 269
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_0
        :pswitch_0
        :pswitch_11
        :pswitch_12
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_19
        :pswitch_1a
        :pswitch_18
        :pswitch_4
        :pswitch_d
        :pswitch_6
        :pswitch_13
        :pswitch_3
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 1161
    const-string v6, "TvContentProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "update, "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1163
    iget-object v6, p0, Lru/cn/api/provider/TvContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    .line 1203
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Wrong URI: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 1165
    :sswitch_0
    const/4 v5, 0x0

    .line 1166
    .local v5, "rez":Z
    const/4 v6, 0x0

    aget-object v4, p4, v6

    .line 1168
    .local v4, "location":Ljava/lang/String;
    :try_start_0
    iget-object v6, p0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v6, v4, p2}, Lru/cn/api/provider/TvContentProviderData;->updateUserPlaylist(Ljava/lang/String;Landroid/content/ContentValues;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 1173
    :goto_0
    if-eqz v5, :cond_0

    .line 1174
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, p1, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1176
    :cond_0
    const/4 v6, 0x1

    .line 1200
    .end local v4    # "location":Ljava/lang/String;
    .end local v5    # "rez":Z
    :goto_1
    return v6

    .line 1169
    .restart local v4    # "location":Ljava/lang/String;
    .restart local v5    # "rez":Z
    :catch_0
    move-exception v2

    .line 1170
    .local v2, "e":Ljava/lang/Exception;
    invoke-static {v2}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1179
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v4    # "location":Ljava/lang/String;
    .end local v5    # "rez":Z
    :sswitch_1
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, p1, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1180
    const/4 v6, 0x1

    goto :goto_1

    .line 1184
    :sswitch_2
    iget-object v6, p0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lru/cn/api/provider/TvContentProviderData;->invalidate(Z)V

    .line 1186
    const-string v6, "contractor"

    invoke-virtual {v6, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz p4, :cond_1

    array-length v6, p4

    if-lez v6, :cond_1

    .line 1188
    const/4 v6, 0x0

    aget-object v6, p4, v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 1189
    .local v0, "contractorId":J
    iget-object v6, p0, Lru/cn/api/provider/TvContentProvider;->data:Lru/cn/api/provider/TvContentProviderData;

    invoke-virtual {v6, v0, v1}, Lru/cn/api/provider/TvContentProviderData;->preventServerCacheOnce(J)V

    .line 1194
    .end local v0    # "contractorId":J
    :cond_1
    new-instance v6, Landroid/net/Uri$Builder;

    invoke-direct {v6}, Landroid/net/Uri$Builder;-><init>()V

    const-string v7, "content"

    invoke-virtual {v6, v7}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "ru.cn.api.tv"

    .line 1195
    invoke-virtual {v6, v7}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "last_channels"

    .line 1196
    invoke-virtual {v6, v7}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 1198
    .local v3, "lastChannelsUri":Landroid/net/Uri;
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, p1, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1199
    invoke-virtual {p0}, Lru/cn/api/provider/TvContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v3, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1200
    const/4 v6, 0x1

    goto :goto_1

    .line 1163
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x18 -> :sswitch_1
    .end sparse-switch
.end method
