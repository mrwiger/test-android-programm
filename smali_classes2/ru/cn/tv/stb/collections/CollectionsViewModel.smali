.class Lru/cn/tv/stb/collections/CollectionsViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "CollectionsViewModel.java"


# instance fields
.field private final collections:Lru/cn/domain/Collections;

.field private final collectionsOut:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Ljava/util/List",
            "<",
            "Lru/cn/tv/stb/collections/CollectionInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private final loader:Lru/cn/mvvm/RxLoader;

.field private loadingSignal:Lio/reactivex/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/PublishSubject",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lru/cn/mvvm/RxLoader;Lru/cn/domain/Collections;)V
    .locals 4
    .param p1, "loader"    # Lru/cn/mvvm/RxLoader;
    .param p2, "collections"    # Lru/cn/domain/Collections;

    .prologue
    .line 34
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 35
    iput-object p1, p0, Lru/cn/tv/stb/collections/CollectionsViewModel;->loader:Lru/cn/mvvm/RxLoader;

    .line 36
    iput-object p2, p0, Lru/cn/tv/stb/collections/CollectionsViewModel;->collections:Lru/cn/domain/Collections;

    .line 38
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/stb/collections/CollectionsViewModel;->collectionsOut:Landroid/arch/lifecycle/MutableLiveData;

    .line 39
    invoke-static {}, Lio/reactivex/subjects/PublishSubject;->create()Lio/reactivex/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/stb/collections/CollectionsViewModel;->loadingSignal:Lio/reactivex/subjects/PublishSubject;

    .line 41
    iget-object v0, p0, Lru/cn/tv/stb/collections/CollectionsViewModel;->loadingSignal:Lio/reactivex/subjects/PublishSubject;

    const-wide/16 v2, 0x1

    .line 42
    invoke-virtual {v0, v2, v3}, Lio/reactivex/subjects/PublishSubject;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$0;-><init>(Lru/cn/tv/stb/collections/CollectionsViewModel;)V

    .line 43
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$1;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$1;-><init>(Lru/cn/tv/stb/collections/CollectionsViewModel;)V

    .line 44
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$2;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$2;-><init>(Lru/cn/tv/stb/collections/CollectionsViewModel;)V

    .line 45
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 46
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/stb/collections/CollectionsViewModel;->collectionsOut:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$3;->get$Lambda(Landroid/arch/lifecycle/MutableLiveData;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    new-instance v2, Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$4;

    invoke-direct {v2, p0}, Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$4;-><init>(Lru/cn/tv/stb/collections/CollectionsViewModel;)V

    .line 47
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 41
    invoke-virtual {p0, v0}, Lru/cn/tv/stb/collections/CollectionsViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 49
    return-void
.end method

.method private allCollections(Ljava/util/List;)Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/tv/stb/collections/CollectionInfo;",
            ">;)",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/util/List",
            "<",
            "Lru/cn/tv/stb/collections/CollectionInfo;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 97
    .local p1, "collections":Ljava/util/List;, "Ljava/util/List<Lru/cn/tv/stb/collections/CollectionInfo;>;"
    invoke-static {p1}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v1

    new-instance v2, Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$9;

    invoke-direct {v2, p0}, Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$9;-><init>(Lru/cn/tv/stb/collections/CollectionsViewModel;)V

    .line 98
    invoke-virtual {v1, v2}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v1

    .line 99
    invoke-virtual {v1}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object v0

    .line 101
    .local v0, "observables":Ljava/util/List;, "Ljava/util/List<Lio/reactivex/Observable<Lru/cn/tv/stb/collections/CollectionInfo;>;>;"
    sget-object v1, Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$10;->$instance:Lio/reactivex/functions/Function;

    .line 102
    invoke-static {v0, v1}, Lio/reactivex/Observable;->zip(Ljava/lang/Iterable;Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 101
    return-object v1
.end method

.method private collectionElements(Lru/cn/tv/stb/collections/CollectionInfo;Z)Lio/reactivex/Observable;
    .locals 4
    .param p1, "collection"    # Lru/cn/tv/stb/collections/CollectionInfo;
    .param p2, "more"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/tv/stb/collections/CollectionInfo;",
            "Z)",
            "Lio/reactivex/Observable",
            "<",
            "Lru/cn/tv/stb/collections/CollectionInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 85
    invoke-virtual {p1}, Lru/cn/tv/stb/collections/CollectionInfo;->getRubric()Lru/cn/api/catalogue/replies/Rubric;

    move-result-object v2

    iget-wide v2, v2, Lru/cn/api/catalogue/replies/Rubric;->id:J

    invoke-static {v2, v3, v0}, Lru/cn/api/provider/TvContentProviderContract;->rubricItemsUri(JLandroid/support/v4/util/LongSparseArray;)Landroid/net/Uri;

    move-result-object v1

    .line 87
    .local v1, "uri":Landroid/net/Uri;
    if-eqz p2, :cond_0

    const-string v0, "more"

    .line 88
    .local v0, "selection":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lru/cn/tv/stb/collections/CollectionsViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v2, v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v2

    .line 89
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    .line 90
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$7;->get$Lambda(Lru/cn/tv/stb/collections/CollectionInfo;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 91
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    .line 92
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$8;

    invoke-direct {v3, p1}, Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$8;-><init>(Lru/cn/tv/stb/collections/CollectionInfo;)V

    .line 93
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 88
    return-object v2
.end method

.method static final synthetic lambda$allCollections$7$CollectionsViewModel([Ljava/lang/Object;)Ljava/util/List;
    .locals 2
    .param p0, "results"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 102
    invoke-static {p0}, Lcom/annimon/stream/Stream;->of([Ljava/lang/Object;)Lcom/annimon/stream/Stream;

    move-result-object v0

    .line 103
    invoke-virtual {v0}, Lcom/annimon/stream/Stream;->withoutNulls()Lcom/annimon/stream/Stream;

    move-result-object v0

    sget-object v1, Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$11;->$instance:Lcom/annimon/stream/function/Function;

    .line 104
    invoke-virtual {v0, v1}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object v0

    .line 102
    return-object v0
.end method

.method static final synthetic lambda$collectionElements$4$CollectionsViewModel(Lru/cn/tv/stb/collections/CollectionInfo;Landroid/database/Cursor;)Lru/cn/tv/stb/collections/CollectionInfo;
    .locals 0
    .param p0, "collection"    # Lru/cn/tv/stb/collections/CollectionInfo;
    .param p1, "it"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 93
    return-object p0
.end method

.method static final synthetic lambda$null$2$CollectionsViewModel(Lru/cn/api/catalogue/replies/Rubric;)Z
    .locals 2
    .param p0, "rubric"    # Lru/cn/api/catalogue/replies/Rubric;

    .prologue
    .line 72
    iget-object v0, p0, Lru/cn/api/catalogue/replies/Rubric;->uiHint:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/api/catalogue/replies/Rubric;->uiHint:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    sget-object v1, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->TOP:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static final synthetic lambda$null$6$CollectionsViewModel(Ljava/lang/Object;)Lru/cn/tv/stb/collections/CollectionInfo;
    .locals 0
    .param p0, "object"    # Ljava/lang/Object;

    .prologue
    .line 104
    check-cast p0, Lru/cn/tv/stb/collections/CollectionInfo;

    .end local p0    # "object":Ljava/lang/Object;
    return-object p0
.end method

.method static final synthetic lambda$rubrics$3$CollectionsViewModel(Lru/cn/api/catalogue/replies/Rubricator;)Ljava/util/List;
    .locals 2
    .param p0, "rubricator"    # Lru/cn/api/catalogue/replies/Rubricator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lru/cn/api/catalogue/replies/Rubricator;->rubrics:Ljava/util/List;

    invoke-static {v0}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v0

    sget-object v1, Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$12;->$instance:Lcom/annimon/stream/function/Predicate;

    .line 72
    invoke-virtual {v0, v1}, Lcom/annimon/stream/Stream;->filter(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;

    move-result-object v0

    sget-object v1, Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$13;->$instance:Lcom/annimon/stream/function/Function;

    .line 73
    invoke-virtual {v0, v1}, Lcom/annimon/stream/Stream;->sortBy(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object v0

    .line 71
    return-object v0
.end method

.method private mapRubrics(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lru/cn/tv/stb/collections/CollectionInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    .local p1, "rubrics":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/catalogue/replies/Rubric;>;"
    invoke-static {p1}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v0

    sget-object v1, Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$6;->$instance:Lcom/annimon/stream/function/Function;

    .line 80
    invoke-virtual {v0, v1}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object v0

    .line 79
    return-object v0
.end method

.method private rubrics()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lru/cn/tv/stb/collections/CollectionsViewModel;->collections:Lru/cn/domain/Collections;

    .line 70
    invoke-virtual {v0}, Lru/cn/domain/Collections;->rubricator()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$5;->$instance:Lio/reactivex/functions/Function;

    .line 71
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 69
    return-object v0
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$CollectionsViewModel(Ljava/util/List;)Ljava/util/List;
    .locals 1

    invoke-direct {p0, p1}, Lru/cn/tv/stb/collections/CollectionsViewModel;->mapRubrics(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method final bridge synthetic bridge$lambda$1$CollectionsViewModel(Ljava/util/List;)Lio/reactivex/Observable;
    .locals 1

    invoke-direct {p0, p1}, Lru/cn/tv/stb/collections/CollectionsViewModel;->allCollections(Ljava/util/List;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public collections()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Ljava/util/List",
            "<",
            "Lru/cn/tv/stb/collections/CollectionInfo;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lru/cn/tv/stb/collections/CollectionsViewModel;->collectionsOut:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method final synthetic lambda$allCollections$5$CollectionsViewModel(Lru/cn/tv/stb/collections/CollectionInfo;)Lio/reactivex/Observable;
    .locals 1
    .param p1, "collection"    # Lru/cn/tv/stb/collections/CollectionInfo;

    .prologue
    .line 98
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lru/cn/tv/stb/collections/CollectionsViewModel;->collectionElements(Lru/cn/tv/stb/collections/CollectionInfo;Z)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method final synthetic lambda$new$0$CollectionsViewModel(Ljava/lang/Integer;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p1, "it"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Lru/cn/tv/stb/collections/CollectionsViewModel;->rubrics()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method final synthetic lambda$new$1$CollectionsViewModel(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "throwable"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lru/cn/tv/stb/collections/CollectionsViewModel;->collectionsOut:Landroid/arch/lifecycle/MutableLiveData;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method public load()V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lru/cn/tv/stb/collections/CollectionsViewModel;->loadingSignal:Lio/reactivex/subjects/PublishSubject;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 57
    return-void
.end method

.method public loadCollection(Lru/cn/tv/stb/collections/CollectionInfo;)V
    .locals 3
    .param p1, "collection"    # Lru/cn/tv/stb/collections/CollectionInfo;

    .prologue
    .line 60
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lru/cn/tv/stb/collections/CollectionsViewModel;->collectionElements(Lru/cn/tv/stb/collections/CollectionInfo;Z)Lio/reactivex/Observable;

    move-result-object v1

    .line 61
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 62
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 63
    invoke-virtual {v1}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 65
    .local v0, "disposable":Lio/reactivex/disposables/Disposable;
    invoke-virtual {p0, v0}, Lru/cn/tv/stb/collections/CollectionsViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 66
    return-void
.end method
