.class public Lru/cn/tv/stb/schedule/ScheduleFragment;
.super Lru/cn/view/CustomListFragment;
.source "ScheduleFragment.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/stb/schedule/ScheduleFragment$ScheduleItemClickListener;
    }
.end annotation


# instance fields
.field private adapter:Lru/cn/tv/stb/schedule/ScheduleAdapter;

.field private currentChannelId:J

.field private currentDate:Ljava/util/Calendar;

.field private currentTelecastId:J

.field private currentTerritoryId:J

.field private goToAirButton:Landroid/view/View;

.field private listener:Lru/cn/tv/stb/schedule/ScheduleFragment$ScheduleItemClickListener;

.field private mHandler:Landroid/os/Handler;

.field private needFocus:Z

.field private noSchedule:Landroid/view/View;

.field private onAirTelecastId:J

.field private positionToFocus:I

.field private progressBar:Landroid/view/View;

.field private viewModel:Lru/cn/tv/stb/schedule/ScheduleViewModel;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    .line 25
    invoke-direct {p0}, Lru/cn/view/CustomListFragment;-><init>()V

    .line 34
    iput-wide v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->currentChannelId:J

    .line 35
    iput-wide v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->currentTerritoryId:J

    .line 36
    iput-wide v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->currentTelecastId:J

    .line 37
    iput-wide v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->onAirTelecastId:J

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->currentDate:Ljava/util/Calendar;

    .line 40
    iput-boolean v2, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->needFocus:Z

    .line 42
    iput v2, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->positionToFocus:I

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/stb/schedule/ScheduleFragment;)Lru/cn/tv/stb/schedule/ScheduleFragment$ScheduleItemClickListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/schedule/ScheduleFragment;

    .prologue
    .line 25
    iget-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->listener:Lru/cn/tv/stb/schedule/ScheduleFragment$ScheduleItemClickListener;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/tv/stb/schedule/ScheduleFragment;)J
    .locals 2
    .param p0, "x0"    # Lru/cn/tv/stb/schedule/ScheduleFragment;

    .prologue
    .line 25
    iget-wide v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->currentChannelId:J

    return-wide v0
.end method

.method private setSchedule(Landroid/database/Cursor;)V
    .locals 24
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 239
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/stb/schedule/ScheduleFragment;->progressBar:Landroid/view/View;

    move-object/from16 v18, v0

    const/16 v19, 0x8

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->setVisibility(I)V

    .line 241
    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v18

    if-nez v18, :cond_3

    .line 242
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/stb/schedule/ScheduleFragment;->noSchedule:Landroid/view/View;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->setVisibility(I)V

    .line 244
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/stb/schedule/ScheduleFragment;->currentDate:Ljava/util/Calendar;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lru/cn/utils/DateUtils;->isToday(Ljava/util/Calendar;)Z

    move-result v5

    .line 245
    .local v5, "isToday":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/stb/schedule/ScheduleFragment;->goToAirButton:Landroid/view/View;

    move-object/from16 v19, v0

    if-eqz v5, :cond_2

    const/16 v18, 0x0

    :goto_0
    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 303
    .end local v5    # "isToday":Z
    .end local p1    # "cursor":Landroid/database/Cursor;
    :cond_1
    :goto_1
    return-void

    .line 245
    .restart local v5    # "isToday":Z
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :cond_2
    const/16 v18, 0x8

    goto :goto_0

    .line 250
    .end local v5    # "isToday":Z
    :cond_3
    check-cast p1, Landroid/database/CursorWrapper;

    .line 251
    .end local p1    # "cursor":Landroid/database/Cursor;
    invoke-virtual/range {p1 .. p1}, Landroid/database/CursorWrapper;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v4

    check-cast v4, Lru/cn/api/provider/cursor/ScheduleItemCursor;

    .line 253
    .local v4, "c":Lru/cn/api/provider/cursor/ScheduleItemCursor;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 255
    .local v10, "nowMs":J
    invoke-virtual {v4}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->moveToFirst()Z

    .line 256
    :goto_2
    invoke-virtual {v4}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->isAfterLast()Z

    move-result v18

    if-nez v18, :cond_5

    .line 257
    invoke-virtual {v4, v10, v11}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->isOnTime(J)Z

    move-result v12

    .line 258
    .local v12, "onAir":Z
    if-eqz v12, :cond_7

    .line 259
    invoke-virtual {v4}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getTelecastId()J

    move-result-wide v8

    .line 262
    .local v8, "id":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lru/cn/tv/stb/schedule/ScheduleFragment;->onAirTelecastId:J

    move-wide/from16 v18, v0

    cmp-long v18, v18, v8

    if-eqz v18, :cond_6

    .line 263
    move-object/from16 v0, p0

    iput-wide v8, v0, Lru/cn/tv/stb/schedule/ScheduleFragment;->onAirTelecastId:J

    .line 265
    invoke-virtual {v4}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getTime()J

    move-result-wide v16

    .line 266
    .local v16, "telecastTime":J
    invoke-virtual {v4}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getDuration()J

    move-result-wide v14

    .line 267
    .local v14, "telecastDuration":J
    new-instance v18, Ljava/util/Date;

    add-long v20, v16, v14

    const-wide/16 v22, 0x3e8

    mul-long v20, v20, v22

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-static/range {v18 .. v18}, Lru/cn/utils/Utils;->getCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v13

    .line 270
    .local v13, "telecastEnd":Ljava/util/Calendar;
    invoke-virtual {v13}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v18

    .line 271
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    sub-long v18, v18, v20

    const-wide/16 v20, 0x2710

    add-long v6, v18, v20

    .line 276
    .end local v13    # "telecastEnd":Ljava/util/Calendar;
    .end local v14    # "telecastDuration":J
    .end local v16    # "telecastTime":J
    .local v6, "delay":J
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/stb/schedule/ScheduleFragment;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 277
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/stb/schedule/ScheduleFragment;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->removeMessages(I)V

    .line 280
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/stb/schedule/ScheduleFragment;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 286
    .end local v6    # "delay":J
    .end local v8    # "id":J
    .end local v12    # "onAir":Z
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/stb/schedule/ScheduleFragment;->noSchedule:Landroid/view/View;

    move-object/from16 v18, v0

    const/16 v19, 0x8

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->setVisibility(I)V

    .line 287
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/stb/schedule/ScheduleFragment;->adapter:Lru/cn/tv/stb/schedule/ScheduleAdapter;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lru/cn/tv/stb/schedule/ScheduleAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 289
    move-object/from16 v0, p0

    iget-wide v0, v0, Lru/cn/tv/stb/schedule/ScheduleFragment;->currentTelecastId:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Lru/cn/tv/stb/schedule/ScheduleFragment;->setCurrentTelecast(J)V

    .line 291
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lru/cn/tv/stb/schedule/ScheduleFragment;->needFocus:Z

    move/from16 v18, v0

    if-eqz v18, :cond_1

    invoke-virtual/range {p0 .. p0}, Lru/cn/tv/stb/schedule/ScheduleFragment;->getView()Landroid/view/View;

    move-result-object v18

    if-eqz v18, :cond_1

    invoke-virtual/range {p0 .. p0}, Lru/cn/tv/stb/schedule/ScheduleFragment;->getView()Landroid/view/View;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->isShown()Z

    move-result v18

    if-eqz v18, :cond_1

    .line 292
    invoke-virtual/range {p0 .. p0}, Lru/cn/tv/stb/schedule/ScheduleFragment;->getListView()Landroid/widget/ListView;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/widget/ListView;->requestFocus()Z

    .line 293
    move-object/from16 v0, p0

    iget v0, v0, Lru/cn/tv/stb/schedule/ScheduleFragment;->positionToFocus:I

    move/from16 v18, v0

    if-lez v18, :cond_8

    .line 294
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lru/cn/tv/stb/schedule/ScheduleFragment;->selectPosition(I)V

    .line 301
    :goto_4
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lru/cn/tv/stb/schedule/ScheduleFragment;->needFocus:Z

    goto/16 :goto_1

    .line 273
    .restart local v8    # "id":J
    .restart local v12    # "onAir":Z
    :cond_6
    const-wide/32 v6, 0xea60

    .restart local v6    # "delay":J
    goto/16 :goto_3

    .line 283
    .end local v6    # "delay":J
    .end local v8    # "id":J
    :cond_7
    invoke-virtual {v4}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->moveToNext()Z

    goto/16 :goto_2

    .line 295
    .end local v12    # "onAir":Z
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lru/cn/tv/stb/schedule/ScheduleFragment;->positionToFocus:I

    move/from16 v18, v0

    if-gez v18, :cond_9

    .line 296
    invoke-virtual/range {p0 .. p0}, Lru/cn/tv/stb/schedule/ScheduleFragment;->getListView()Landroid/widget/ListView;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/widget/ListView;->getCount()I

    move-result v18

    add-int/lit8 v18, v18, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lru/cn/tv/stb/schedule/ScheduleFragment;->selectPosition(I)V

    goto :goto_4

    .line 298
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lru/cn/tv/stb/schedule/ScheduleFragment;->selectCurrentTelecast()V

    goto :goto_4
.end method

.method private updateData()V
    .locals 7

    .prologue
    .line 223
    invoke-virtual {p0}, Lru/cn/tv/stb/schedule/ScheduleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 227
    :cond_1
    iget-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->adapter:Lru/cn/tv/stb/schedule/ScheduleAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->currentDate:Ljava/util/Calendar;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->currentChannelId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->viewModel:Lru/cn/tv/stb/schedule/ScheduleViewModel;

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->adapter:Lru/cn/tv/stb/schedule/ScheduleAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lru/cn/tv/stb/schedule/ScheduleAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 231
    invoke-virtual {p0}, Lru/cn/tv/stb/schedule/ScheduleFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lru/cn/tv/stb/schedule/ScheduleFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->progressBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 234
    iget-object v1, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->viewModel:Lru/cn/tv/stb/schedule/ScheduleViewModel;

    iget-wide v2, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->currentChannelId:J

    iget-wide v4, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->currentTerritoryId:J

    iget-object v6, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->currentDate:Ljava/util/Calendar;

    invoke-virtual/range {v1 .. v6}, Lru/cn/tv/stb/schedule/ScheduleViewModel;->setChannel(JJLjava/util/Calendar;)V

    goto :goto_0
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$ScheduleFragment(Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/stb/schedule/ScheduleFragment;->setSchedule(Landroid/database/Cursor;)V

    return-void
.end method

.method protected getItemHeight()I
    .locals 1

    .prologue
    .line 219
    const/16 v0, 0x40

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v0, 0x0

    .line 111
    invoke-virtual {p0}, Lru/cn/tv/stb/schedule/ScheduleFragment;->isAdded()Z

    move-result v1

    if-nez v1, :cond_0

    .line 121
    :goto_0
    return v0

    .line 115
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 117
    :pswitch_0
    invoke-direct {p0}, Lru/cn/tv/stb/schedule/ScheduleFragment;->updateData()V

    .line 118
    const/4 v0, 0x1

    goto :goto_0

    .line 115
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 58
    invoke-super {p0, p1}, Lru/cn/view/CustomListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 59
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->mHandler:Landroid/os/Handler;

    .line 61
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v0

    const-class v1, Lru/cn/tv/stb/schedule/ScheduleViewModel;

    invoke-static {p0, v0, v1}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lru/cn/tv/stb/schedule/ScheduleViewModel;

    iput-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->viewModel:Lru/cn/tv/stb/schedule/ScheduleViewModel;

    .line 62
    iget-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->viewModel:Lru/cn/tv/stb/schedule/ScheduleViewModel;

    .line 63
    invoke-virtual {v0}, Lru/cn/tv/stb/schedule/ScheduleViewModel;->schedule()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/stb/schedule/ScheduleFragment$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/schedule/ScheduleFragment$$Lambda$0;-><init>(Lru/cn/tv/stb/schedule/ScheduleFragment;)V

    .line 64
    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 65
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 69
    const v0, 0x7f0c0089

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 102
    invoke-super {p0}, Lru/cn/view/CustomListFragment;->onDestroyView()V

    .line 104
    iget-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 107
    :cond_0
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 158
    invoke-super/range {p0 .. p5}, Lru/cn/view/CustomListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 161
    invoke-virtual {p0}, Lru/cn/tv/stb/schedule/ScheduleFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, p3, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 163
    iget-object v1, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->adapter:Lru/cn/tv/stb/schedule/ScheduleAdapter;

    invoke-virtual {v1, p3}, Lru/cn/tv/stb/schedule/ScheduleAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/cursor/ScheduleItemCursor;

    .line 165
    .local v0, "cursor":Lru/cn/api/provider/cursor/ScheduleItemCursor;
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getTelecastId()J

    move-result-wide v2

    .line 167
    .local v2, "telecastId":J
    iget-object v1, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->listener:Lru/cn/tv/stb/schedule/ScheduleFragment$ScheduleItemClickListener;

    if-eqz v1, :cond_0

    .line 168
    iget-object v1, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->listener:Lru/cn/tv/stb/schedule/ScheduleFragment$ScheduleItemClickListener;

    invoke-interface {v1, v2, v3, v0}, Lru/cn/tv/stb/schedule/ScheduleFragment$ScheduleItemClickListener;->onScheduleItemClicked(JLru/cn/api/provider/cursor/ScheduleItemCursor;)V

    .line 170
    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 74
    invoke-super {p0, p1, p2}, Lru/cn/view/CustomListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 76
    const v0, 0x7f090175

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->progressBar:Landroid/view/View;

    .line 77
    const v0, 0x7f09014b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->noSchedule:Landroid/view/View;

    .line 79
    const v0, 0x7f0900e8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->goToAirButton:Landroid/view/View;

    .line 80
    iget-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->goToAirButton:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->goToAirButton:Landroid/view/View;

    new-instance v1, Lru/cn/tv/stb/schedule/ScheduleFragment$1;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/schedule/ScheduleFragment$1;-><init>(Lru/cn/tv/stb/schedule/ScheduleFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    :cond_0
    new-instance v0, Lru/cn/tv/stb/schedule/ScheduleAdapter;

    invoke-virtual {p0}, Lru/cn/tv/stb/schedule/ScheduleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {}, Lru/cn/utils/Utils;->isLauncherInstalled()Z

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lru/cn/tv/stb/schedule/ScheduleAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    iput-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->adapter:Lru/cn/tv/stb/schedule/ScheduleAdapter;

    .line 94
    iget-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->adapter:Lru/cn/tv/stb/schedule/ScheduleAdapter;

    invoke-virtual {p0, v0}, Lru/cn/tv/stb/schedule/ScheduleFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 95
    invoke-virtual {p0}, Lru/cn/tv/stb/schedule/ScheduleFragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 96
    invoke-direct {p0}, Lru/cn/tv/stb/schedule/ScheduleFragment;->updateData()V

    .line 98
    :cond_1
    return-void
.end method

.method public requestFocus(I)V
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 125
    iput p1, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->positionToFocus:I

    .line 126
    iget-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->adapter:Lru/cn/tv/stb/schedule/ScheduleAdapter;

    invoke-virtual {v0}, Lru/cn/tv/stb/schedule/ScheduleAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 127
    invoke-virtual {p0}, Lru/cn/tv/stb/schedule/ScheduleFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    invoke-virtual {p0}, Lru/cn/tv/stb/schedule/ScheduleFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lru/cn/tv/stb/schedule/ScheduleFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->needFocus:Z

    goto :goto_0
.end method

.method public selectCurrentTelecast()V
    .locals 2

    .prologue
    .line 173
    iget-wide v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->currentTelecastId:J

    invoke-virtual {p0, v0, v1}, Lru/cn/tv/stb/schedule/ScheduleFragment;->setCurrentTelecast(J)V

    .line 174
    return-void
.end method

.method public setChannelId(JJ)V
    .locals 3
    .param p1, "channelId"    # J
    .param p3, "territoryId"    # J

    .prologue
    .line 134
    iget-wide v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->currentChannelId:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    iget-wide v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->currentTerritoryId:J

    cmp-long v0, v0, p3

    if-eqz v0, :cond_1

    .line 135
    :cond_0
    iput-wide p1, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->currentChannelId:J

    .line 136
    iput-wide p3, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->currentTerritoryId:J

    .line 137
    invoke-direct {p0}, Lru/cn/tv/stb/schedule/ScheduleFragment;->updateData()V

    .line 139
    :cond_1
    return-void
.end method

.method public setCurrentTelecast(J)V
    .locals 11
    .param p1, "telecastId"    # J

    .prologue
    .line 177
    iput-wide p1, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->currentTelecastId:J

    .line 178
    iget-object v5, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->adapter:Lru/cn/tv/stb/schedule/ScheduleAdapter;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->adapter:Lru/cn/tv/stb/schedule/ScheduleAdapter;

    invoke-virtual {v5}, Lru/cn/tv/stb/schedule/ScheduleAdapter;->getCount()I

    move-result v5

    if-lez v5, :cond_0

    .line 179
    iget-object v5, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->adapter:Lru/cn/tv/stb/schedule/ScheduleAdapter;

    invoke-virtual {v5}, Lru/cn/tv/stb/schedule/ScheduleAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/cursor/ScheduleItemCursor;

    .line 180
    .local v0, "c":Lru/cn/api/provider/cursor/ScheduleItemCursor;
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->isClosed()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 207
    .end local v0    # "c":Lru/cn/api/provider/cursor/ScheduleItemCursor;
    :cond_0
    :goto_0
    return-void

    .line 184
    .restart local v0    # "c":Lru/cn/api/provider/cursor/ScheduleItemCursor;
    :cond_1
    const/4 v1, -0x1

    .line 185
    .local v1, "currentPosition":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 187
    .local v2, "nowMs":J
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->moveToFirst()Z

    .line 188
    :goto_1
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_4

    .line 189
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getPosition()I

    move-result v4

    .line 192
    .local v4, "position":I
    invoke-virtual {v0, v2, v3}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->isOnTime(J)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 193
    move v1, v4

    .line 196
    :cond_2
    iget-wide v6, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->currentTelecastId:J

    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getTelecastId()J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-nez v5, :cond_3

    .line 197
    invoke-virtual {p0, v4}, Lru/cn/tv/stb/schedule/ScheduleFragment;->selectPosition(I)V

    goto :goto_0

    .line 200
    :cond_3
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->moveToNext()Z

    goto :goto_1

    .line 203
    .end local v4    # "position":I
    :cond_4
    const/4 v5, -0x1

    if-eq v1, v5, :cond_0

    .line 204
    invoke-virtual {p0, v1}, Lru/cn/tv/stb/schedule/ScheduleFragment;->selectPosition(I)V

    goto :goto_0
.end method

.method public setDate(Ljava/util/Calendar;)V
    .locals 2
    .param p1, "date"    # Ljava/util/Calendar;

    .prologue
    const/4 v1, 0x0

    .line 142
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 143
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 144
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 145
    const/16 v0, 0xe

    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 146
    iget-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->currentDate:Ljava/util/Calendar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->currentDate:Ljava/util/Calendar;

    invoke-virtual {v0, p1}, Ljava/util/Calendar;->compareTo(Ljava/util/Calendar;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 147
    :cond_0
    iput-object p1, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->currentDate:Ljava/util/Calendar;

    .line 148
    invoke-direct {p0}, Lru/cn/tv/stb/schedule/ScheduleFragment;->updateData()V

    .line 150
    :cond_1
    return-void
.end method

.method public setListener(Lru/cn/tv/stb/schedule/ScheduleFragment$ScheduleItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/tv/stb/schedule/ScheduleFragment$ScheduleItemClickListener;

    .prologue
    .line 153
    iput-object p1, p0, Lru/cn/tv/stb/schedule/ScheduleFragment;->listener:Lru/cn/tv/stb/schedule/ScheduleFragment$ScheduleItemClickListener;

    .line 154
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 0
    .param p1, "isVisibleToUser"    # Z

    .prologue
    .line 211
    invoke-super {p0, p1}, Lru/cn/view/CustomListFragment;->setUserVisibleHint(Z)V

    .line 212
    if-eqz p1, :cond_0

    .line 213
    invoke-direct {p0}, Lru/cn/tv/stb/schedule/ScheduleFragment;->updateData()V

    .line 215
    :cond_0
    return-void
.end method
