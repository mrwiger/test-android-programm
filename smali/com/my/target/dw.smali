.class public final Lcom/my/target/dw;
.super Lcom/my/target/ak;
.source "InterstitialSliderAdSection.java"


# instance fields
.field private backgroundColor:I

.field private closeIcon:Lcom/my/target/common/models/ImageData;

.field private final w:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/core/models/banners/g;",
            ">;"
        }
    .end annotation
.end field

.field private x:I

.field private y:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/my/target/ak;-><init>()V

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/dw;->w:Ljava/util/ArrayList;

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lcom/my/target/dw;->x:I

    .line 33
    const v0, -0xe0414d

    iput v0, p0, Lcom/my/target/dw;->y:I

    .line 34
    const v0, -0xf9c399

    iput v0, p0, Lcom/my/target/dw;->backgroundColor:I

    .line 79
    return-void
.end method

.method public static l()Lcom/my/target/dw;
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/my/target/dw;

    invoke-direct {v0}, Lcom/my/target/dw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final R()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/my/target/dw;->w:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 58
    iput p1, p0, Lcom/my/target/dw;->y:I

    .line 59
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 73
    iput p1, p0, Lcom/my/target/dw;->x:I

    .line 74
    return-void
.end method

.method public final c(Lcom/my/target/core/models/banners/g;)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/my/target/dw;->w:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    return-void
.end method

.method public final d(Lcom/my/target/core/models/banners/g;)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/my/target/dw;->w:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 89
    return-void
.end method

.method public final getBackgroundColor()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/my/target/dw;->backgroundColor:I

    return v0
.end method

.method public final getBannersCount()I
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/my/target/dw;->w:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getCloseIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/my/target/dw;->closeIcon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/my/target/dw;->y:I

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/my/target/dw;->x:I

    return v0
.end method

.method public final setBackgroundColor(I)V
    .locals 0
    .param p1, "backgroundColor"    # I

    .prologue
    .line 63
    iput p1, p0, Lcom/my/target/dw;->backgroundColor:I

    .line 64
    return-void
.end method

.method public final setCloseIcon(Lcom/my/target/common/models/ImageData;)V
    .locals 0
    .param p1, "closeIcon"    # Lcom/my/target/common/models/ImageData;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/my/target/dw;->closeIcon:Lcom/my/target/common/models/ImageData;

    .line 69
    return-void
.end method
