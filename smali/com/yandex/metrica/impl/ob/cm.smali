.class public abstract Lcom/yandex/metrica/impl/ob/cm;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/yandex/metrica/impl/ob/cp;

.field private final b:Lcom/yandex/metrica/impl/ob/df;

.field private final c:Ljava/util/concurrent/atomic/AtomicLong;

.field private final d:Ljava/util/concurrent/atomic/AtomicLong;

.field private final e:Ljava/util/concurrent/atomic/AtomicLong;

.field private final f:Landroid/content/ContentValues;


# direct methods
.method constructor <init>(Lcom/yandex/metrica/impl/ob/cp;Lcom/yandex/metrica/impl/ob/df;)V
    .locals 4

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/cm;->f:Landroid/content/ContentValues;

    .line 50
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/cm;->a:Lcom/yandex/metrica/impl/ob/cp;

    .line 51
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/cm;->b:Lcom/yandex/metrica/impl/ob/df;

    .line 52
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/cm;->f()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/cm;->c:Ljava/util/concurrent/atomic/AtomicLong;

    .line 53
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide v2, 0x7fffffffffffffffL

    invoke-virtual {p0, v2, v3}, Lcom/yandex/metrica/impl/ob/cm;->b(J)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/cm;->d:Ljava/util/concurrent/atomic/AtomicLong;

    .line 54
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, -0x1

    invoke-virtual {p0, v2, v3}, Lcom/yandex/metrica/impl/ob/cm;->d(J)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/cm;->e:Ljava/util/concurrent/atomic/AtomicLong;

    .line 55
    return-void
.end method

.method private f()J
    .locals 2

    .prologue
    .line 82
    :try_start_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/cm;->d()Lcom/yandex/metrica/impl/ob/cp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/cp;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 83
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/cm;->e()Ljava/lang/String;

    move-result-object v1

    .line 84
    invoke-static {v0, v1}, Lcom/yandex/metrica/impl/utils/e;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 87
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cm;->c:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method a(I)V
    .locals 4

    .prologue
    .line 103
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cm;->c:Ljava/util/concurrent/atomic/AtomicLong;

    neg-int v1, p1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->getAndAdd(J)J

    .line 104
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cm;->d:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide v2, 0x7fffffffffffffffL

    invoke-virtual {p0, v2, v3}, Lcom/yandex/metrica/impl/ob/cm;->b(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 105
    return-void
.end method

.method a(J)V
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cm;->c:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 93
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cm;->e:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 94
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cm;->e:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/cm;->e(J)Lcom/yandex/metrica/impl/ob/df;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/df;->g()V

    .line 97
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cm;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-lez v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cm;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 100
    :cond_0
    return-void
.end method

.method public declared-synchronized a(JLjava/lang/String;)V
    .locals 9

    .prologue
    .line 132
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/cm;->d()Lcom/yandex/metrica/impl/ob/cp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/cp;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1147
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cm;->f:Landroid/content/ContentValues;

    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 1148
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cm;->f:Landroid/content/ContentValues;

    const-string v2, "incremental_id"

    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/cm;->e:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1149
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cm;->f:Landroid/content/ContentValues;

    const-string v2, "timestamp"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1150
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cm;->f:Landroid/content/ContentValues;

    const-string v2, "data"

    invoke-virtual {v1, v2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1152
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cm;->f:Landroid/content/ContentValues;

    .line 134
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/cm;->e()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 135
    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 136
    invoke-virtual {p0, p1, p2}, Lcom/yandex/metrica/impl/ob/cm;->a(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 143
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cm;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public b(J)J
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 108
    .line 110
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Select min(%s) from %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "timestamp"

    aput-object v4, v3, v5

    const/4 v4, 0x1

    .line 111
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/cm;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 110
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 113
    :try_start_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/cm;->d()Lcom/yandex/metrica/impl/ob/cp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/cp;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 114
    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 115
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v0

    .line 118
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-eqz v3, :cond_0

    move-wide p1, v0

    .line 125
    :cond_0
    invoke-static {v2}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/Cursor;)V

    .line 127
    :goto_0
    return-wide p1

    .line 125
    :catch_0
    move-exception v1

    :goto_1
    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_2
    invoke-static {v2}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/Cursor;)V

    .line 126
    throw v0

    .line 125
    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v0, v2

    goto :goto_1
.end method

.method public declared-synchronized b(I)Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 157
    monitor-enter p0

    :try_start_0
    new-instance v10, Ljava/util/LinkedHashMap;

    invoke-direct {v10}, Ljava/util/LinkedHashMap;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    :try_start_1
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/cm;->d()Lcom/yandex/metrica/impl/ob/cp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/cp;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 162
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/cm;->e()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "incremental_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "data"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "incremental_id ASC"

    .line 170
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    .line 161
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 171
    :goto_0
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 172
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cm;->f:Landroid/content/ContentValues;

    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 173
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cm;->f:Landroid/content/ContentValues;

    invoke-static {v0, v1}, Lcom/yandex/metrica/impl/utils/e;->a(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    .line 174
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cm;->f:Landroid/content/ContentValues;

    const-string v2, "incremental_id"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/cm;->f:Landroid/content/ContentValues;

    const-string v3, "data"

    .line 175
    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 174
    invoke-interface {v10, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    .line 180
    :catch_0
    move-exception v1

    :goto_1
    :try_start_3
    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/Cursor;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 182
    :goto_2
    monitor-exit p0

    return-object v10

    .line 180
    :cond_0
    :try_start_4
    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/Cursor;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 157
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 180
    :catchall_1
    move-exception v0

    :goto_3
    :try_start_5
    invoke-static {v9}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/Cursor;)V

    .line 181
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 180
    :catchall_2
    move-exception v1

    move-object v9, v0

    move-object v0, v1

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v0, v9

    goto :goto_1
.end method

.method public declared-synchronized c(I)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 208
    monitor-enter p0

    if-gtz p1, :cond_1

    .line 230
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 211
    :cond_1
    :try_start_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%1$s <= (select max(%1$s) from (select %1$s from %2$s order by %1$s limit ?))"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "incremental_id"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 213
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/cm;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 211
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 217
    :try_start_1
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/cm;->d()Lcom/yandex/metrica/impl/ob/cp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/cp;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 219
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/cm;->e()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 221
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 218
    invoke-virtual {v2, v3, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 223
    if-lez v0, :cond_0

    .line 225
    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/cm;->a(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    .line 208
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c(J)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 186
    monitor-enter p0

    .line 187
    :try_start_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s <= ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "incremental_id"

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 191
    :try_start_1
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/cm;->d()Lcom/yandex/metrica/impl/ob/cp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/cp;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 193
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/cm;->e()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 195
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 192
    invoke-virtual {v2, v3, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 197
    if-lez v0, :cond_0

    .line 199
    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/cm;->a(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 204
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 186
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method c()Lcom/yandex/metrica/impl/ob/df;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cm;->b:Lcom/yandex/metrica/impl/ob/df;

    return-object v0
.end method

.method protected abstract d(J)J
.end method

.method protected d()Lcom/yandex/metrica/impl/ob/cp;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cm;->a:Lcom/yandex/metrica/impl/ob/cp;

    return-object v0
.end method

.method protected abstract e(J)Lcom/yandex/metrica/impl/ob/df;
.end method

.method public abstract e()Ljava/lang/String;
.end method
