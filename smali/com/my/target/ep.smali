.class public final Lcom/my/target/ep;
.super Lcom/my/target/ee;
.source "CarouselVerticalView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static final az:I

.field private static final bD:I

.field private static final bi:I

.field private static final cO:I

.field private static final cQ:I

.field private static final cS:I

.field private static final dd:I


# instance fields
.field private final F:Lcom/my/target/by;

.field private final aX:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final aw:Lcom/my/target/cm;

.field private final bk:Lcom/my/target/bv;

.field private final bp:Lcom/my/target/by;

.field private final cT:Landroid/widget/TextView;

.field private final cZ:Landroid/widget/TextView;

.field private final ch:Landroid/widget/TextView;

.field private final de:Lcom/my/target/ew;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ep;->cO:I

    .line 36
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ep;->bi:I

    .line 37
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ep;->az:I

    .line 38
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ep;->cQ:I

    .line 39
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ep;->dd:I

    .line 40
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ep;->bD:I

    .line 41
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ep;->cS:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 11

    .prologue
    const/4 v5, 0x1

    const/4 v10, -0x1

    const/16 v9, 0xa

    const/4 v8, 0x0

    const/4 v7, -0x2

    .line 68
    invoke-direct {p0, p1, v8}, Lcom/my/target/ee;-><init>(Landroid/content/Context;I)V

    .line 69
    const v0, -0x3a1508

    invoke-static {p0, v10, v0}, Lcom/my/target/cm;->a(Landroid/view/View;II)V

    .line 71
    new-instance v0, Lcom/my/target/by;

    invoke-direct {v0, p1}, Lcom/my/target/by;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ep;->F:Lcom/my/target/by;

    .line 72
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/ep;->aw:Lcom/my/target/cm;

    .line 73
    new-instance v0, Lcom/my/target/by;

    invoke-direct {v0, p1}, Lcom/my/target/by;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ep;->bp:Lcom/my/target/by;

    .line 74
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ep;->ch:Landroid/widget/TextView;

    .line 75
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ep;->cT:Landroid/widget/TextView;

    .line 76
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ep;->cZ:Landroid/widget/TextView;

    .line 77
    new-instance v0, Lcom/my/target/bv;

    invoke-direct {v0, p1}, Lcom/my/target/bv;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ep;->bk:Lcom/my/target/bv;

    .line 79
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 80
    sget v1, Lcom/my/target/ep;->cQ:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 81
    iget-object v1, p0, Lcom/my/target/ep;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v9}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/ep;->aw:Lcom/my/target/cm;

    .line 82
    invoke-virtual {v2, v8}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/ep;->aw:Lcom/my/target/cm;

    .line 83
    invoke-virtual {v3, v9}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/ep;->aw:Lcom/my/target/cm;

    .line 84
    invoke-virtual {v4, v8}, Lcom/my/target/cm;->n(I)I

    move-result v4

    .line 81
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 85
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v10, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 86
    const/4 v2, 0x3

    sget v3, Lcom/my/target/ep;->cO:I

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 87
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 89
    iget-object v1, p0, Lcom/my/target/ep;->F:Lcom/my/target/by;

    sget v2, Lcom/my/target/ep;->cO:I

    invoke-virtual {v1, v2}, Lcom/my/target/by;->setId(I)V

    .line 90
    iget-object v1, p0, Lcom/my/target/ep;->F:Lcom/my/target/by;

    const-string v2, "close"

    invoke-virtual {v1, v2}, Lcom/my/target/by;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 91
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 92
    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 93
    iget-object v2, p0, Lcom/my/target/ep;->F:Lcom/my/target/by;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/my/target/by;->setVisibility(I)V

    .line 94
    iget-object v2, p0, Lcom/my/target/ep;->F:Lcom/my/target/by;

    invoke-virtual {v2, v1}, Lcom/my/target/by;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 96
    iget-object v1, p0, Lcom/my/target/ep;->bk:Lcom/my/target/bv;

    sget v2, Lcom/my/target/ep;->bi:I

    invoke-virtual {v1, v2}, Lcom/my/target/bv;->setId(I)V

    .line 97
    iget-object v1, p0, Lcom/my/target/ep;->bk:Lcom/my/target/bv;

    const-string v2, "icon"

    invoke-virtual {v1, v2}, Lcom/my/target/bv;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v1, p0, Lcom/my/target/ep;->ch:Landroid/widget/TextView;

    sget v2, Lcom/my/target/ep;->az:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setId(I)V

    .line 100
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 101
    iget-object v2, p0, Lcom/my/target/ep;->ch:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setLines(I)V

    .line 102
    iget-object v2, p0, Lcom/my/target/ep;->ch:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 103
    sget v2, Lcom/my/target/ep;->bi:I

    invoke-virtual {v1, v5, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 104
    iget-object v2, p0, Lcom/my/target/ep;->ch:Landroid/widget/TextView;

    const/high16 v3, 0x41b00000    # 22.0f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 105
    iget-object v2, p0, Lcom/my/target/ep;->ch:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 107
    iget-object v1, p0, Lcom/my/target/ep;->cT:Landroid/widget/TextView;

    sget v2, Lcom/my/target/ep;->cS:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setId(I)V

    .line 108
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 109
    sget v2, Lcom/my/target/ep;->bi:I

    invoke-virtual {v1, v5, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 110
    const/4 v2, 0x3

    sget v3, Lcom/my/target/ep;->az:I

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 111
    iget-object v2, p0, Lcom/my/target/ep;->cT:Landroid/widget/TextView;

    const/high16 v3, 0x41900000    # 18.0f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 112
    iget-object v2, p0, Lcom/my/target/ep;->cT:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setLines(I)V

    .line 113
    iget-object v2, p0, Lcom/my/target/ep;->cT:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 114
    iget-object v2, p0, Lcom/my/target/ep;->cT:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 116
    iget-object v1, p0, Lcom/my/target/ep;->cZ:Landroid/widget/TextView;

    sget v2, Lcom/my/target/ep;->bD:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setId(I)V

    .line 117
    iget-object v1, p0, Lcom/my/target/ep;->cZ:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/my/target/ep;->aw:Lcom/my/target/cm;

    invoke-virtual {v2, v9}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/ep;->aw:Lcom/my/target/cm;

    const/4 v4, 0x4

    .line 118
    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/ep;->aw:Lcom/my/target/cm;

    .line 119
    invoke-virtual {v4, v9}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v5, p0, Lcom/my/target/ep;->aw:Lcom/my/target/cm;

    const/4 v6, 0x4

    .line 120
    invoke-virtual {v5, v6}, Lcom/my/target/cm;->n(I)I

    move-result v5

    .line 117
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 121
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 122
    const/4 v2, 0x3

    sget v3, Lcom/my/target/ep;->cQ:I

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 123
    iget-object v2, p0, Lcom/my/target/ep;->aw:Lcom/my/target/cm;

    const/16 v3, 0xc

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 124
    iget-object v2, p0, Lcom/my/target/ep;->cZ:Landroid/widget/TextView;

    const/high16 v3, 0x41900000    # 18.0f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 125
    iget-object v2, p0, Lcom/my/target/ep;->cZ:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 126
    iget-object v1, p0, Lcom/my/target/ep;->cZ:Landroid/widget/TextView;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 128
    new-instance v1, Lcom/my/target/ew;

    invoke-direct {v1, p1}, Lcom/my/target/ew;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/my/target/ep;->de:Lcom/my/target/ew;

    .line 130
    iget-object v1, p0, Lcom/my/target/ep;->de:Lcom/my/target/ew;

    sget v2, Lcom/my/target/ep;->dd:I

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setId(I)V

    .line 131
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v10, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 132
    const/4 v2, 0x3

    sget v3, Lcom/my/target/ep;->bD:I

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 134
    iget-object v2, p0, Lcom/my/target/ep;->de:Lcom/my/target/ew;

    iget-object v3, p0, Lcom/my/target/ep;->aw:Lcom/my/target/cm;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    invoke-virtual {v2, v8, v8, v8, v3}, Landroid/support/v7/widget/RecyclerView;->setPadding(IIII)V

    .line 135
    iget-object v2, p0, Lcom/my/target/ep;->de:Lcom/my/target/ew;

    iget-object v3, p0, Lcom/my/target/ep;->aw:Lcom/my/target/cm;

    invoke-virtual {v3, v9}, Lcom/my/target/cm;->n(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/my/target/ew;->setSideSlidesMargins(I)V

    .line 137
    iget-object v2, p0, Lcom/my/target/ep;->de:Lcom/my/target/ew;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 139
    iget-object v1, p0, Lcom/my/target/ep;->de:Lcom/my/target/ew;

    invoke-virtual {p0, v1}, Lcom/my/target/ep;->addView(Landroid/view/View;)V

    .line 141
    iget-object v1, p0, Lcom/my/target/ep;->bk:Lcom/my/target/bv;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 142
    iget-object v1, p0, Lcom/my/target/ep;->ch:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 143
    iget-object v1, p0, Lcom/my/target/ep;->cT:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 145
    invoke-virtual {p0, v0}, Lcom/my/target/ep;->addView(Landroid/view/View;)V

    .line 147
    iget-object v0, p0, Lcom/my/target/ep;->cZ:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/my/target/ep;->addView(Landroid/view/View;)V

    .line 148
    iget-object v0, p0, Lcom/my/target/ep;->F:Lcom/my/target/by;

    invoke-virtual {p0, v0}, Lcom/my/target/ep;->addView(Landroid/view/View;)V

    .line 149
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/my/target/ep;->aX:Ljava/util/HashMap;

    .line 150
    return-void
.end method


# virtual methods
.method public final G()V
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, Lcom/my/target/ep;->F:Lcom/my/target/by;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setVisibility(I)V

    .line 253
    return-void
.end method

.method public final b(Lcom/my/target/core/models/banners/h;)V
    .locals 0

    .prologue
    .line 248
    return-void
.end method

.method public final e(I)V
    .locals 0

    .prologue
    .line 387
    return-void
.end method

.method public final f(Z)V
    .locals 0

    .prologue
    .line 377
    return-void
.end method

.method public final finish()V
    .locals 0

    .prologue
    .line 287
    return-void
.end method

.method public final getCloseButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/my/target/ep;->F:Lcom/my/target/by;

    return-object v0
.end method

.method public final getNumbersOfCurrentShowingCards()[I
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 359
    iget-object v0, p0, Lcom/my/target/ep;->de:Lcom/my/target/ew;

    invoke-virtual {v0}, Lcom/my/target/ew;->getCardLayoutManager()Lcom/my/target/eq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/eq;->findFirstVisibleItemPosition()I

    move-result v2

    .line 360
    iget-object v0, p0, Lcom/my/target/ep;->de:Lcom/my/target/ew;

    invoke-virtual {v0}, Lcom/my/target/ew;->getCardLayoutManager()Lcom/my/target/eq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/eq;->findLastCompletelyVisibleItemPosition()I

    move-result v0

    .line 361
    if-eq v2, v3, :cond_0

    if-ne v0, v3, :cond_2

    .line 363
    :cond_0
    new-array v0, v1, [I

    .line 371
    :cond_1
    return-object v0

    .line 365
    :cond_2
    sub-int/2addr v0, v2

    add-int/lit8 v4, v0, 0x1

    .line 366
    new-array v0, v4, [I

    .line 367
    :goto_0
    if-ge v1, v4, :cond_1

    .line 369
    add-int/lit8 v3, v2, 0x1

    aput v2, v0, v1

    .line 367
    add-int/lit8 v1, v1, 0x1

    move v2, v3

    goto :goto_0
.end method

.method public final getSoundButton()Lcom/my/target/by;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/my/target/ep;->bp:Lcom/my/target/by;

    return-object v0
.end method

.method public final isPaused()Z
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x0

    return v0
.end method

.method public final isPlaying()Z
    .locals 1

    .prologue
    .line 257
    const/4 v0, 0x0

    return v0
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v2, -0x1

    .line 330
    iget-object v0, p0, Lcom/my/target/ep;->aX:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 332
    const/4 v0, 0x0

    .line 353
    :goto_0
    return v0

    .line 334
    :cond_0
    iget-object v0, p0, Lcom/my/target/ep;->aX:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 336
    goto :goto_0

    .line 338
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    move v0, v1

    .line 353
    goto :goto_0

    .line 341
    :pswitch_1
    const v0, -0x3a1508

    invoke-virtual {p0, v0}, Lcom/my/target/ep;->setBackgroundColor(I)V

    goto :goto_1

    .line 344
    :pswitch_2
    invoke-virtual {p0, v2}, Lcom/my/target/ep;->setBackgroundColor(I)V

    .line 345
    iget-object v0, p0, Lcom/my/target/ep;->bx:Lcom/my/target/ee$a;

    invoke-virtual {v0, p1}, Lcom/my/target/ee$a;->onClick(Landroid/view/View;)V

    goto :goto_1

    .line 348
    :pswitch_3
    invoke-virtual {p0, v2}, Lcom/my/target/ep;->setBackgroundColor(I)V

    goto :goto_1

    .line 338
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final pause()V
    .locals 0

    .prologue
    .line 382
    return-void
.end method

.method public final play()V
    .locals 0

    .prologue
    .line 281
    return-void
.end method

.method public final resume()V
    .locals 0

    .prologue
    .line 297
    return-void
.end method

.method public final setBanner(Lcom/my/target/core/models/banners/h;)V
    .locals 7
    .param p1, "banner"    # Lcom/my/target/core/models/banners/h;

    .prologue
    const/16 v6, 0x40

    const/4 v4, -0x2

    const/4 v1, 0x0

    .line 155
    invoke-super {p0, p1}, Lcom/my/target/ee;->setBanner(Lcom/my/target/core/models/banners/h;)V

    .line 157
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getCloseIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 158
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 160
    iget-object v2, p0, Lcom/my/target/ep;->F:Lcom/my/target/by;

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Lcom/my/target/by;->b(Landroid/graphics/Bitmap;Z)V

    .line 171
    :cond_0
    :goto_0
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 175
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v4

    .line 176
    if-eqz v4, :cond_8

    .line 178
    invoke-virtual {v4}, Lcom/my/target/common/models/ImageData;->getWidth()I

    move-result v2

    .line 179
    invoke-virtual {v4}, Lcom/my/target/common/models/ImageData;->getHeight()I

    move-result v0

    .line 182
    :goto_1
    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    .line 184
    int-to-float v0, v0

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 186
    iget-object v2, p0, Lcom/my/target/ep;->aw:Lcom/my/target/cm;

    invoke-virtual {v2, v6}, Lcom/my/target/cm;->n(I)I

    move-result v2

    .line 187
    iget-object v5, p0, Lcom/my/target/ep;->aw:Lcom/my/target/cm;

    invoke-virtual {v5, v6}, Lcom/my/target/cm;->n(I)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v0, v5

    float-to-int v0, v0

    .line 189
    iput v2, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 190
    iput v0, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 193
    :cond_1
    iget-object v0, p0, Lcom/my/target/ep;->bk:Lcom/my/target/bv;

    invoke-virtual {v0, v3}, Lcom/my/target/bv;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 195
    if-eqz v4, :cond_2

    .line 197
    iget-object v0, p0, Lcom/my/target/ep;->bk:Lcom/my/target/bv;

    invoke-virtual {v4}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/my/target/bv;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 200
    :cond_2
    iget-object v0, p0, Lcom/my/target/ep;->ch:Landroid/widget/TextView;

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 201
    iget-object v0, p0, Lcom/my/target/ep;->ch:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 203
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getCategory()Ljava/lang/String;

    move-result-object v2

    .line 204
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getSubCategory()Ljava/lang/String;

    move-result-object v3

    .line 206
    const-string v0, ""

    .line 207
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 209
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 212
    :cond_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 214
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 217
    :cond_4
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 219
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 222
    :cond_5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 224
    iget-object v2, p0, Lcom/my/target/ep;->cT:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 225
    iget-object v0, p0, Lcom/my/target/ep;->cT:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 232
    :goto_2
    iget-object v0, p0, Lcom/my/target/ep;->cZ:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 234
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getInterstitialAdCards()Ljava/util/List;

    move-result-object v0

    .line 236
    iget-object v1, p0, Lcom/my/target/ep;->de:Lcom/my/target/ew;

    invoke-virtual {v1, v0}, Lcom/my/target/ew;->c(Ljava/util/List;)V

    .line 237
    return-void

    .line 164
    :cond_6
    iget-object v0, p0, Lcom/my/target/ep;->aw:Lcom/my/target/cm;

    const/16 v2, 0x1c

    invoke-virtual {v0, v2}, Lcom/my/target/cm;->n(I)I

    move-result v0

    invoke-static {v0}, Lcom/my/target/bq;->i(I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 165
    if-eqz v0, :cond_0

    .line 167
    iget-object v2, p0, Lcom/my/target/ep;->F:Lcom/my/target/by;

    invoke-virtual {v2, v0, v1}, Lcom/my/target/by;->b(Landroid/graphics/Bitmap;Z)V

    goto/16 :goto_0

    .line 229
    :cond_7
    iget-object v0, p0, Lcom/my/target/ep;->cT:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_8
    move v0, v1

    move v2, v1

    goto/16 :goto_1
.end method

.method public final setClickArea(Lcom/my/target/af;)V
    .locals 3
    .param p1, "area"    # Lcom/my/target/af;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .prologue
    .line 303
    iget-boolean v0, p1, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/my/target/ep;->bx:Lcom/my/target/ee$a;

    invoke-virtual {p0, v0}, Lcom/my/target/ep;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 306
    const/4 v0, -0x1

    const v1, -0x3a1508

    invoke-static {p0, v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;II)V

    .line 307
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/my/target/ep;->setClickable(Z)V

    .line 324
    :goto_0
    iget-object v0, p0, Lcom/my/target/ep;->de:Lcom/my/target/ew;

    iget-object v1, p0, Lcom/my/target/ep;->ad:Lcom/my/target/ee$b;

    invoke-virtual {v0, v1}, Lcom/my/target/ew;->setOnPromoCardListener(Lcom/my/target/ee$b;)V

    .line 325
    return-void

    .line 311
    :cond_0
    iget-object v0, p0, Lcom/my/target/ep;->ch:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 312
    iget-object v0, p0, Lcom/my/target/ep;->cT:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 313
    iget-object v0, p0, Lcom/my/target/ep;->bk:Lcom/my/target/bv;

    invoke-virtual {v0, p0}, Lcom/my/target/bv;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 314
    iget-object v0, p0, Lcom/my/target/ep;->cZ:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 315
    invoke-virtual {p0, p0}, Lcom/my/target/ep;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 317
    iget-object v0, p0, Lcom/my/target/ep;->aX:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/my/target/ep;->ch:Landroid/widget/TextView;

    iget-boolean v2, p1, Lcom/my/target/af;->cs:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 318
    iget-object v0, p0, Lcom/my/target/ep;->aX:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/my/target/ep;->cT:Landroid/widget/TextView;

    iget-boolean v2, p1, Lcom/my/target/af;->cC:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 319
    iget-object v0, p0, Lcom/my/target/ep;->aX:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/my/target/ep;->bk:Lcom/my/target/bv;

    iget-boolean v2, p1, Lcom/my/target/af;->cu:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    iget-object v0, p0, Lcom/my/target/ep;->aX:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/my/target/ep;->cZ:Landroid/widget/TextView;

    iget-boolean v2, p1, Lcom/my/target/af;->ct:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    iget-object v0, p0, Lcom/my/target/ep;->aX:Ljava/util/HashMap;

    iget-boolean v1, p1, Lcom/my/target/af;->cD:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final setTimeChanged(F)V
    .locals 0

    .prologue
    .line 292
    return-void
.end method
