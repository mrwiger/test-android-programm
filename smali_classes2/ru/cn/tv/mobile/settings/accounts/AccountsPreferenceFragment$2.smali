.class Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment$2;
.super Ljava/lang/Object;
.source "AccountsPreferenceFragment.java"

# interfaces
.implements Lcom/squareup/picasso/Target;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;->addContractor(Ljava/lang/String;JLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;

.field final synthetic val$account:Landroid/support/v7/preference/PreferenceScreen;


# direct methods
.method constructor <init>(Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;Landroid/support/v7/preference/PreferenceScreen;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;

    .prologue
    .line 88
    iput-object p1, p0, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment$2;->this$0:Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;

    iput-object p2, p0, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment$2;->val$account:Landroid/support/v7/preference/PreferenceScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBitmapFailed(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "errorDrawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 99
    return-void
.end method

.method public onBitmapLoaded(Landroid/graphics/Bitmap;Lcom/squareup/picasso/Picasso$LoadedFrom;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "from"    # Lcom/squareup/picasso/Picasso$LoadedFrom;

    .prologue
    .line 91
    iget-object v1, p0, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment$2;->this$0:Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;

    invoke-virtual {v1}, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_0

    .line 96
    :goto_0
    return-void

    .line 94
    :cond_0
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment$2;->this$0:Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;

    invoke-virtual {v1}, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 95
    .local v0, "d":Landroid/graphics/drawable/BitmapDrawable;
    iget-object v1, p0, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment$2;->val$account:Landroid/support/v7/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/support/v7/preference/PreferenceScreen;->setIcon(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public onPrepareLoad(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "placeHolderDrawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 103
    iget-object v0, p0, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment$2;->this$0:Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;

    invoke-virtual {v0}, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 107
    :goto_0
    return-void

    .line 106
    :cond_0
    iget-object v0, p0, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment$2;->val$account:Landroid/support/v7/preference/PreferenceScreen;

    invoke-virtual {v0, p1}, Landroid/support/v7/preference/PreferenceScreen;->setIcon(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
