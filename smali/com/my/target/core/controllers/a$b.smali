.class final Lcom/my/target/core/controllers/a$b;
.super Ljava/lang/Object;
.source "NativeAdVideoController.java"

# interfaces
.implements Lcom/my/target/cw$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/core/controllers/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic u:Lcom/my/target/core/controllers/a;


# direct methods
.method private constructor <init>(Lcom/my/target/core/controllers/a;)V
    .locals 0

    .prologue
    .line 764
    iput-object p1, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/my/target/core/controllers/a;B)V
    .locals 0

    .prologue
    .line 764
    invoke-direct {p0, p1}, Lcom/my/target/core/controllers/a$b;-><init>(Lcom/my/target/core/controllers/a;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 810
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->i(Lcom/my/target/core/controllers/a;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 812
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->b(Lcom/my/target/core/controllers/a;)V

    .line 814
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->j(Lcom/my/target/core/controllers/a;)Landroid/view/View$OnClickListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 816
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->j(Lcom/my/target/core/controllers/a;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 818
    :cond_1
    return-void
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 788
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->f(Lcom/my/target/core/controllers/a;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 790
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->f(Lcom/my/target/core/controllers/a;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/nativeads/views/MediaAdView;

    .line 791
    if-eqz v0, :cond_0

    .line 793
    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 794
    iget-object v1, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v1, v0}, Lcom/my/target/core/controllers/a;->a(Lcom/my/target/core/controllers/a;Landroid/content/Context;)V

    .line 797
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->h(Lcom/my/target/core/controllers/a;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 799
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->h(Lcom/my/target/core/controllers/a;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/cw;

    .line 800
    if-eqz v0, :cond_1

    .line 802
    invoke-virtual {v0}, Lcom/my/target/cw;->D()V

    .line 805
    :cond_1
    return-void
.end method

.method public final l()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 842
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->l(Lcom/my/target/core/controllers/a;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    if-nez v0, :cond_4

    const/4 v0, 0x0

    .line 844
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/my/target/br;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 846
    invoke-virtual {v0}, Lcom/my/target/br;->dismiss()V

    .line 850
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->f(Lcom/my/target/core/controllers/a;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 852
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->f(Lcom/my/target/core/controllers/a;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/nativeads/views/MediaAdView;

    .line 853
    if-eqz v0, :cond_3

    .line 855
    iget-object v1, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v1}, Lcom/my/target/core/controllers/a;->i(Lcom/my/target/core/controllers/a;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 857
    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getProgressBarView()Landroid/widget/ProgressBar;

    move-result-object v1

    .line 858
    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 860
    :cond_1
    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    .line 861
    iget-object v1, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v1}, Lcom/my/target/core/controllers/a;->m(Lcom/my/target/core/controllers/a;)Lcom/my/target/aj;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/aj;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    .line 862
    if-eqz v1, :cond_2

    .line 864
    invoke-virtual {v1}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 866
    :cond_2
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 869
    :cond_3
    return-void

    .line 842
    :cond_4
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->l(Lcom/my/target/core/controllers/a;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/br;

    goto :goto_0
.end method

.method public final m()V
    .locals 3

    .prologue
    .line 874
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->n(Lcom/my/target/core/controllers/a;)Lcom/my/target/cv;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 876
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->n(Lcom/my/target/core/controllers/a;)Lcom/my/target/cv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/cv;->isMuted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 878
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->n(Lcom/my/target/core/controllers/a;)Lcom/my/target/cv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/cv;->bk()V

    .line 879
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    const-string v1, "volumeOn"

    iget-object v2, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v2}, Lcom/my/target/core/controllers/a;->n(Lcom/my/target/core/controllers/a;)Lcom/my/target/cv;

    move-result-object v2

    invoke-virtual {v2}, Lcom/my/target/cv;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/my/target/core/controllers/a;->a(Lcom/my/target/core/controllers/a;Ljava/lang/String;Landroid/content/Context;)V

    .line 880
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/my/target/core/controllers/a;->a(Lcom/my/target/core/controllers/a;Z)Z

    .line 889
    :cond_0
    :goto_0
    return-void

    .line 884
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->n(Lcom/my/target/core/controllers/a;)Lcom/my/target/cv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/cv;->bj()V

    .line 885
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    const-string v1, "volumeOff"

    iget-object v2, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v2}, Lcom/my/target/core/controllers/a;->n(Lcom/my/target/core/controllers/a;)Lcom/my/target/cv;

    move-result-object v2

    invoke-virtual {v2}, Lcom/my/target/cv;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/my/target/core/controllers/a;->a(Lcom/my/target/core/controllers/a;Ljava/lang/String;Landroid/content/Context;)V

    .line 886
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/my/target/core/controllers/a;->a(Lcom/my/target/core/controllers/a;Z)Z

    goto :goto_0
.end method

.method public final onPauseClicked()V
    .locals 3

    .prologue
    .line 823
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->i(Lcom/my/target/core/controllers/a;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 825
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->b(Lcom/my/target/core/controllers/a;)V

    .line 826
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->k(Lcom/my/target/core/controllers/a;)I

    .line 827
    const/4 v0, 0x0

    .line 828
    iget-object v1, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v1}, Lcom/my/target/core/controllers/a;->f(Lcom/my/target/core/controllers/a;)Ljava/lang/ref/WeakReference;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 830
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->f(Lcom/my/target/core/controllers/a;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/nativeads/views/MediaAdView;

    .line 832
    :cond_0
    if-eqz v0, :cond_1

    .line 834
    iget-object v1, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    const-string v2, "playbackPaused"

    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/my/target/core/controllers/a;->a(Lcom/my/target/core/controllers/a;Ljava/lang/String;Landroid/content/Context;)V

    .line 837
    :cond_1
    return-void
.end method

.method public final onPlayClicked()V
    .locals 3

    .prologue
    .line 769
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->f(Lcom/my/target/core/controllers/a;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 771
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->f(Lcom/my/target/core/controllers/a;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/nativeads/views/MediaAdView;

    .line 772
    if-eqz v0, :cond_0

    .line 774
    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 775
    iget-object v1, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v1, v0}, Lcom/my/target/core/controllers/a;->a(Lcom/my/target/core/controllers/a;Landroid/content/Context;)V

    .line 776
    iget-object v1, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    const-string v2, "playbackResumed"

    invoke-static {v1, v2, v0}, Lcom/my/target/core/controllers/a;->a(Lcom/my/target/core/controllers/a;Ljava/lang/String;Landroid/content/Context;)V

    .line 779
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->g(Lcom/my/target/core/controllers/a;)Lcom/my/target/core/controllers/a$c;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 781
    iget-object v0, p0, Lcom/my/target/core/controllers/a$b;->u:Lcom/my/target/core/controllers/a;

    invoke-static {v0}, Lcom/my/target/core/controllers/a;->g(Lcom/my/target/core/controllers/a;)Lcom/my/target/core/controllers/a$c;

    move-result-object v0

    invoke-interface {v0}, Lcom/my/target/core/controllers/a$c;->n()V

    .line 783
    :cond_1
    return-void
.end method
