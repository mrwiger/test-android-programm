.class public Lcom/yandex/metrica/impl/ob/dx;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Landroid/content/Context;

.field private c:Landroid/location/LocationManager;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 33
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "gps"

    aput-object v3, v1, v2

    .line 34
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dx;->a:Ljava/util/Set;

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/location/LocationManager;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/dx;->b:Landroid/content/Context;

    .line 44
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/dx;->c:Landroid/location/LocationManager;

    .line 45
    return-void
.end method


# virtual methods
.method public a()Landroid/location/Location;
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 50
    .line 52
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/dx;->c:Landroid/location/LocationManager;

    if-eqz v0, :cond_4

    .line 53
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/dx;->b:Landroid/content/Context;

    .line 1035
    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-static {v0, v1}, Lcom/yandex/metrica/impl/am;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    .line 54
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/dx;->b:Landroid/content/Context;

    .line 1039
    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v0, v1}, Lcom/yandex/metrica/impl/am;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v8

    .line 56
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/dx;->c:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    .line 57
    if-eqz v0, :cond_4

    .line 59
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move-object v1, v6

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 60
    sget-object v2, Lcom/yandex/metrica/impl/ob/dx;->a:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 64
    if-eqz v7, :cond_2

    :try_start_0
    const-string v2, "passive"

    .line 65
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v8, :cond_2

    .line 67
    :cond_1
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/dx;->c:Landroid/location/LocationManager;

    invoke-virtual {v2, v0}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 74
    :goto_1
    if-eqz v0, :cond_0

    sget-wide v2, Lcom/yandex/metrica/impl/ob/dw;->a:J

    const-wide/16 v4, 0xc8

    invoke-static/range {v0 .. v5}, Lcom/yandex/metrica/impl/ob/dw;->a(Landroid/location/Location;Landroid/location/Location;JJ)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v1, v0

    .line 76
    goto :goto_0

    :catch_0
    move-exception v0

    :cond_2
    move-object v0, v6

    goto :goto_1

    :cond_3
    move-object v6, v1

    .line 87
    :cond_4
    return-object v6
.end method
