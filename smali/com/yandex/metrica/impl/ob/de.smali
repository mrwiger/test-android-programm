.class public abstract Lcom/yandex/metrica/impl/ob/de;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/yandex/metrica/impl/ob/cr;

.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/yandex/metrica/impl/ob/de;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/yandex/metrica/impl/ob/cr;)V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/yandex/metrica/impl/ob/de;-><init>(Lcom/yandex/metrica/impl/ob/cr;Ljava/lang/String;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Lcom/yandex/metrica/impl/ob/cr;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/de;->a:Lcom/yandex/metrica/impl/ob/cr;

    .line 38
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/de;->b:Ljava/lang/String;

    .line 39
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/String;I)Lcom/yandex/metrica/impl/ob/de;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/yandex/metrica/impl/ob/de;",
            ">(",
            "Ljava/lang/String;",
            "I)TT;"
        }
    .end annotation

    .prologue
    .line 65
    monitor-enter p0

    .line 66
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/de;->a:Lcom/yandex/metrica/impl/ob/cr;

    invoke-virtual {v0, p1, p2}, Lcom/yandex/metrica/impl/ob/cr;->b(Ljava/lang/String;I)Lcom/yandex/metrica/impl/ob/cr;

    .line 67
    monitor-exit p0

    return-object p0

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;J)Lcom/yandex/metrica/impl/ob/de;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/yandex/metrica/impl/ob/de;",
            ">(",
            "Ljava/lang/String;",
            "J)TT;"
        }
    .end annotation

    .prologue
    .line 58
    monitor-enter p0

    .line 59
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/de;->a:Lcom/yandex/metrica/impl/ob/cr;

    invoke-virtual {v0, p1, p2, p3}, Lcom/yandex/metrica/impl/ob/cr;->b(Ljava/lang/String;J)Lcom/yandex/metrica/impl/ob/cr;

    .line 60
    monitor-exit p0

    return-object p0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;Z)Lcom/yandex/metrica/impl/ob/de;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/yandex/metrica/impl/ob/de;",
            ">(",
            "Ljava/lang/String;",
            "Z)TT;"
        }
    .end annotation

    .prologue
    .line 73
    monitor-enter p0

    .line 74
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/de;->a:Lcom/yandex/metrica/impl/ob/cr;

    invoke-virtual {v0, p1, p2}, Lcom/yandex/metrica/impl/ob/cr;->b(Ljava/lang/String;Z)Lcom/yandex/metrica/impl/ob/cr;

    .line 75
    monitor-exit p0

    return-object p0

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method b(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/de;->a:Lcom/yandex/metrica/impl/ob/cr;

    invoke-virtual {v0, p1, p2}, Lcom/yandex/metrica/impl/ob/cr;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;J)J
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/de;->a:Lcom/yandex/metrica/impl/ob/cr;

    invoke-virtual {v0, p1, p2, p3}, Lcom/yandex/metrica/impl/ob/cr;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method protected b(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/yandex/metrica/impl/ob/de;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 50
    monitor-enter p0

    .line 51
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/de;->a:Lcom/yandex/metrica/impl/ob/cr;

    invoke-virtual {v0, p1, p2}, Lcom/yandex/metrica/impl/ob/cr;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/cr;

    .line 52
    monitor-exit p0

    return-object p0

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/de;->a:Lcom/yandex/metrica/impl/ob/cr;

    invoke-virtual {v0, p1, p2}, Lcom/yandex/metrica/impl/ob/cr;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/de;->a:Lcom/yandex/metrica/impl/ob/cr;

    invoke-virtual {v0, p1, p2}, Lcom/yandex/metrica/impl/ob/cr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/de;->b:Ljava/lang/String;

    return-object v0
.end method

.method public g()V
    .locals 1

    .prologue
    .line 113
    monitor-enter p0

    .line 114
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/de;->a:Lcom/yandex/metrica/impl/ob/cr;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/cr;->b()V

    .line 115
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected q(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/fm;
    .locals 2

    .prologue
    .line 46
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/de;->f()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public r(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/yandex/metrica/impl/ob/de;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 106
    monitor-enter p0

    .line 107
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/de;->a:Lcom/yandex/metrica/impl/ob/cr;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/cr;->a(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/cr;

    .line 108
    monitor-exit p0

    return-object p0

    .line 109
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
