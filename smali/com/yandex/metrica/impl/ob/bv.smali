.class public Lcom/yandex/metrica/impl/ob/bv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/yandex/metrica/impl/ob/cb;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/app/job/JobScheduler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 39
    const-string v0, "jobscheduler"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    invoke-direct {p0, p1, v0}, Lcom/yandex/metrica/impl/ob/bv;-><init>(Landroid/content/Context;Landroid/app/job/JobScheduler;)V

    .line 40
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/app/job/JobScheduler;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/bv;->a:Landroid/content/Context;

    .line 66
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/bv;->b:Landroid/app/job/JobScheduler;

    .line 67
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bv;->b:Landroid/app/job/JobScheduler;

    const v1, 0x5a23e709

    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->cancel(I)V

    .line 61
    return-void
.end method

.method public a(J)V
    .locals 5

    .prologue
    .line 46
    new-instance v0, Landroid/app/job/JobInfo$Builder;

    const v1, 0x5a23e709

    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/bv;->a:Landroid/content/Context;

    .line 47
    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/yandex/metrica/ConfigurationJobService;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    .line 48
    invoke-virtual {v0, p1, p2}, Landroid/app/job/JobInfo$Builder;->setPeriodic(J)Landroid/app/job/JobInfo$Builder;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v0

    .line 51
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/bv;->b:Landroid/app/job/JobScheduler;

    invoke-virtual {v1, v0}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    .line 55
    return-void
.end method
