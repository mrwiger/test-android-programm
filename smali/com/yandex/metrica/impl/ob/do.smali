.class public abstract Lcom/yandex/metrica/impl/ob/do;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ob/do$b;,
        Lcom/yandex/metrica/impl/ob/do$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a(Landroid/content/pm/FeatureInfo;)Lcom/yandex/metrica/impl/ob/dp;
.end method

.method public b(Landroid/content/pm/FeatureInfo;)Lcom/yandex/metrica/impl/ob/dp;
    .locals 4

    .prologue
    .line 26
    iget-object v0, p1, Landroid/content/pm/FeatureInfo;->name:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 27
    iget v0, p1, Landroid/content/pm/FeatureInfo;->reqGlEsVersion:I

    if-nez v0, :cond_0

    .line 28
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/ob/do;->a(Landroid/content/pm/FeatureInfo;)Lcom/yandex/metrica/impl/ob/dp;

    move-result-object v0

    .line 37
    :goto_0
    return-object v0

    .line 30
    :cond_0
    new-instance v0, Lcom/yandex/metrica/impl/ob/dp;

    const-string v1, "openGlFeature"

    iget v2, p1, Landroid/content/pm/FeatureInfo;->reqGlEsVersion:I

    .line 33
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/ob/do;->c(Landroid/content/pm/FeatureInfo;)Z

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/yandex/metrica/impl/ob/dp;-><init>(Ljava/lang/String;IZ)V

    goto :goto_0

    .line 37
    :cond_1
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/ob/do;->a(Landroid/content/pm/FeatureInfo;)Lcom/yandex/metrica/impl/ob/dp;

    move-result-object v0

    goto :goto_0
.end method

.method c(Landroid/content/pm/FeatureInfo;)Z
    .locals 1

    .prologue
    .line 42
    iget v0, p1, Landroid/content/pm/FeatureInfo;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
