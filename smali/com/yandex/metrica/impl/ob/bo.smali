.class public Lcom/yandex/metrica/impl/ob/bo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/yandex/metrica/impl/ob/bn;


# instance fields
.field protected a:Lcom/yandex/metrica/impl/ob/dd;

.field private final b:Ljava/lang/String;

.field private c:Lorg/json/JSONObject;


# direct methods
.method public constructor <init>(Lcom/yandex/metrica/impl/ob/dd;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/bo;->a:Lcom/yandex/metrica/impl/ob/dd;

    .line 37
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/bo;->b:Ljava/lang/String;

    .line 38
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 40
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bo;->a:Lcom/yandex/metrica/impl/ob/dd;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/bo;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/dd;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 41
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    :goto_0
    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bo;->c:Lorg/json/JSONObject;

    .line 48
    return-void

    :catch_0
    move-exception v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 106
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bo;->c:Lorg/json/JSONObject;

    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public a(J)Lcom/yandex/metrica/impl/ob/bo;
    .locals 3

    .prologue
    .line 73
    const-string v0, "SESSION_COUNTER_ID"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/bo;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 74
    return-object p0
.end method

.method public a(Z)Lcom/yandex/metrica/impl/ob/bo;
    .locals 2

    .prologue
    .line 91
    const-string v0, "SESSION_IS_ALIVE_REPORT_NEEDED"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/bo;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 92
    return-object p0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 111
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bo;->c:Lorg/json/JSONObject;

    .line 112
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/bo;->g()V

    .line 113
    return-void
.end method

.method public b(J)Lcom/yandex/metrica/impl/ob/bo;
    .locals 3

    .prologue
    .line 82
    const-string v0, "SESSION_SLEEP_START"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/bo;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 83
    return-object p0
.end method

.method public b()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bo;->c:Lorg/json/JSONObject;

    const-string v1, "SESSION_ID"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public c(J)Lcom/yandex/metrica/impl/ob/bo;
    .locals 3

    .prologue
    .line 55
    const-string v0, "SESSION_ID"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/bo;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 56
    return-object p0
.end method

.method public c()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bo;->c:Lorg/json/JSONObject;

    const-string v1, "SESSION_INIT_TIME"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public d(J)Lcom/yandex/metrica/impl/ob/bo;
    .locals 3

    .prologue
    .line 64
    const-string v0, "SESSION_INIT_TIME"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/bo;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 65
    return-object p0
.end method

.method public d()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bo;->c:Lorg/json/JSONObject;

    const-string v1, "SESSION_COUNTER_ID"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bo;->c:Lorg/json/JSONObject;

    const-string v1, "SESSION_SLEEP_START"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bo;->c:Lorg/json/JSONObject;

    const-string v1, "SESSION_IS_ALIVE_REPORT_NEEDED"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .locals 3

    .prologue
    .line 96
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bo;->a:Lcom/yandex/metrica/impl/ob/dd;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/bo;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/bo;->c:Lorg/json/JSONObject;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/dd;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dd;

    .line 97
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bo;->a:Lcom/yandex/metrica/impl/ob/dd;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dd;->g()V

    .line 98
    return-void
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bo;->c:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
