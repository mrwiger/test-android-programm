.class Lcom/my/target/ce$d;
.super Lcom/my/target/ce$a;
.source "ClickHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/ce;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "d"
.end annotation


# static fields
.field private static final jL:Ljava/lang/String; = "android.support.customtabs.extra.SESSION"


# instance fields
.field protected final url:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/my/target/ah;)V
    .locals 0

    .prologue
    .line 336
    invoke-direct {p0, p2}, Lcom/my/target/ce$a;-><init>(Lcom/my/target/ah;)V

    .line 337
    iput-object p1, p0, Lcom/my/target/ce$d;->url:Ljava/lang/String;

    .line 338
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcom/my/target/ah;Lcom/my/target/ce$1;)V
    .locals 0

    .prologue
    .line 328
    invoke-direct {p0, p1, p2}, Lcom/my/target/ce$d;-><init>(Ljava/lang/String;Lcom/my/target/ah;)V

    return-void
.end method

.method private l(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 376
    invoke-static {p1}, Lcom/my/target/ce$e;->N(Ljava/lang/String;)Lcom/my/target/ce$e;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/my/target/ce$e;->s(Landroid/content/Context;)V

    .line 377
    const/4 v0, 0x1

    return v0
.end method

.method private m(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 385
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 386
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 387
    const-string v2, "android.support.customtabs.extra.SESSION"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBinder(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 388
    instance-of v2, p2, Landroid/app/Activity;

    if-nez v2, :cond_0

    .line 390
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 392
    :cond_0
    const-string v2, "com.android.chrome"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 393
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 394
    invoke-virtual {p2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 395
    const/4 v0, 0x1

    .line 401
    :goto_0
    return v0

    .line 397
    :catch_0
    move-exception v0

    .line 401
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected r(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 343
    iget-object v1, p0, Lcom/my/target/ce$d;->jK:Lcom/my/target/ah;

    invoke-virtual {v1}, Lcom/my/target/ah;->isOpenInBrowser()Z

    move-result v1

    .line 344
    if-eqz v1, :cond_2

    .line 348
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    iget-object v3, p0, Lcom/my/target/ce$d;->url:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 349
    instance-of v2, p1, Landroid/app/Activity;

    if-nez v2, :cond_0

    .line 351
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 353
    :cond_0
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 371
    :cond_1
    :goto_0
    return v0

    .line 362
    :cond_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_3

    .line 364
    iget-object v1, p0, Lcom/my/target/ce$d;->url:Ljava/lang/String;

    invoke-direct {p0, v1, p1}, Lcom/my/target/ce$d;->m(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 369
    :cond_3
    iget-object v0, p0, Lcom/my/target/ce$d;->url:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/my/target/ce$d;->l(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v0

    goto :goto_0

    .line 356
    :catch_0
    move-exception v0

    .line 371
    const/4 v0, 0x0

    goto :goto_0
.end method
