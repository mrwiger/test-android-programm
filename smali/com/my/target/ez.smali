.class public final Lcom/my/target/ez;
.super Landroid/widget/FrameLayout;
.source "SliderImageView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# instance fields
.field private av:Lcom/my/target/bu;

.field private final aw:Lcom/my/target/cm;

.field private bV:I

.field private bW:I

.field private final dU:I

.field private final dV:I

.field private final dW:Landroid/widget/ImageView;

.field private final dX:Landroid/widget/RelativeLayout;

.field private final dY:Landroid/widget/FrameLayout$LayoutParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, -0x2

    const/4 v2, -0x1

    .line 42
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 44
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/ez;->aw:Lcom/my/target/cm;

    .line 46
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ez;->dX:Landroid/widget/RelativeLayout;

    .line 48
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/my/target/ez;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ez;->dW:Landroid/widget/ImageView;

    .line 49
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 52
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 53
    iget-object v1, p0, Lcom/my/target/ez;->dW:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 54
    iget-object v0, p0, Lcom/my/target/ez;->dW:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 56
    iget-object v0, p0, Lcom/my/target/ez;->aw:Lcom/my/target/cm;

    invoke-virtual {v0, v4}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iput v0, p0, Lcom/my/target/ez;->dU:I

    .line 57
    iget-object v0, p0, Lcom/my/target/ez;->aw:Lcom/my/target/cm;

    invoke-virtual {v0, v4}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iput v0, p0, Lcom/my/target/ez;->dV:I

    .line 59
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/my/target/ez;->dY:Landroid/widget/FrameLayout$LayoutParams;

    .line 61
    iget-object v0, p0, Lcom/my/target/ez;->dY:Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 63
    iget-object v0, p0, Lcom/my/target/ez;->dX:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/my/target/ez;->dY:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0, v0, v1}, Lcom/my/target/ez;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 64
    iget-object v0, p0, Lcom/my/target/ez;->dX:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/my/target/ez;->dW:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 66
    iget-object v0, p0, Lcom/my/target/ez;->dX:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p2}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 68
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/my/target/ez;->setClipToPadding(Z)V

    .line 69
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 71
    iget-object v0, p0, Lcom/my/target/ez;->dX:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/my/target/ez;->aw:Lcom/my/target/cm;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setElevation(F)V

    .line 73
    :cond_0
    return-void
.end method


# virtual methods
.method protected final onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 131
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 132
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 3
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 137
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 139
    invoke-virtual {p0}, Lcom/my/target/ez;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/my/target/ez;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 141
    iget v1, p0, Lcom/my/target/ez;->bW:I

    if-lez v1, :cond_0

    .line 143
    int-to-float v1, v0

    iget v2, p0, Lcom/my/target/ez;->bV:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/my/target/ez;->bW:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/my/target/ez;->bV:I

    .line 144
    iput v0, p0, Lcom/my/target/ez;->bW:I

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/my/target/ez;->dY:Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/my/target/ez;->bW:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 148
    iget-object v0, p0, Lcom/my/target/ez;->dY:Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/my/target/ez;->bV:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 150
    iget-object v0, p0, Lcom/my/target/ez;->dX:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/my/target/ez;->dY:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 152
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 153
    return-void
.end method

.method public final setAgeRestrictions(Ljava/lang/String;)V
    .locals 8
    .param p1, "ageRestrictions"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0x8

    const/4 v7, 0x1

    const/4 v4, -0x2

    const v6, -0x111112

    const/4 v3, 0x0

    .line 103
    iget-object v0, p0, Lcom/my/target/ez;->av:Lcom/my/target/bu;

    if-nez v0, :cond_0

    .line 107
    new-instance v0, Lcom/my/target/bu;

    invoke-virtual {p0}, Lcom/my/target/ez;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/my/target/bu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ez;->av:Lcom/my/target/bu;

    .line 108
    iget-object v0, p0, Lcom/my/target/ez;->av:Lcom/my/target/bu;

    const v1, -0x777778

    invoke-virtual {v0, v7, v1}, Lcom/my/target/bu;->c(II)V

    .line 109
    iget-object v0, p0, Lcom/my/target/ez;->av:Lcom/my/target/bu;

    iget-object v1, p0, Lcom/my/target/ez;->aw:Lcom/my/target/cm;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v1, v3, v3, v3}, Lcom/my/target/bu;->setPadding(IIII)V

    .line 111
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 114
    iget-object v1, p0, Lcom/my/target/ez;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v5}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/ez;->aw:Lcom/my/target/cm;

    const/16 v3, 0x14

    .line 115
    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/ez;->aw:Lcom/my/target/cm;

    .line 116
    invoke-virtual {v3, v5}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/ez;->aw:Lcom/my/target/cm;

    const/16 v5, 0x14

    .line 117
    invoke-virtual {v4, v5}, Lcom/my/target/cm;->n(I)I

    move-result v4

    .line 114
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 118
    iget-object v1, p0, Lcom/my/target/ez;->av:Lcom/my/target/bu;

    invoke-virtual {v1, v0}, Lcom/my/target/bu;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 120
    iget-object v0, p0, Lcom/my/target/ez;->av:Lcom/my/target/bu;

    invoke-virtual {v0, v6}, Lcom/my/target/bu;->setTextColor(I)V

    .line 121
    iget-object v0, p0, Lcom/my/target/ez;->av:Lcom/my/target/bu;

    iget-object v1, p0, Lcom/my/target/ez;->aw:Lcom/my/target/cm;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v7, v6, v1}, Lcom/my/target/bu;->a(III)V

    .line 122
    iget-object v0, p0, Lcom/my/target/ez;->av:Lcom/my/target/bu;

    const/high16 v1, 0x66000000

    invoke-virtual {v0, v1}, Lcom/my/target/bu;->setBackgroundColor(I)V

    .line 123
    iget-object v0, p0, Lcom/my/target/ez;->dX:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/my/target/ez;->av:Lcom/my/target/bu;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/my/target/ez;->av:Lcom/my/target/bu;

    invoke-virtual {v0, p1}, Lcom/my/target/bu;->setText(Ljava/lang/CharSequence;)V

    .line 126
    return-void
.end method

.method public final setImage(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "bitmapToApply"    # Landroid/graphics/Bitmap;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/my/target/ez;->dW:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 79
    invoke-virtual {p0}, Lcom/my/target/ez;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 81
    iget v0, p0, Lcom/my/target/ez;->dU:I

    iget v1, p0, Lcom/my/target/ez;->dU:I

    iget v2, p0, Lcom/my/target/ez;->dU:I

    iget v3, p0, Lcom/my/target/ez;->dU:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/my/target/ez;->setPadding(IIII)V

    .line 94
    :goto_0
    if-eqz p1, :cond_0

    .line 96
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/my/target/ez;->bW:I

    .line 97
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/my/target/ez;->bV:I

    .line 99
    :cond_0
    return-void

    .line 88
    :cond_1
    iget v0, p0, Lcom/my/target/ez;->dV:I

    iget v1, p0, Lcom/my/target/ez;->dV:I

    iget v2, p0, Lcom/my/target/ez;->dV:I

    iget v3, p0, Lcom/my/target/ez;->dV:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/my/target/ez;->setPadding(IIII)V

    goto :goto_0
.end method
