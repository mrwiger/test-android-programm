.class public final Lru/cn/domain/PinCode$$Factory;
.super Ljava/lang/Object;
.source "PinCode$$Factory.java"

# interfaces
.implements Ltoothpick/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ltoothpick/Factory",
        "<",
        "Lru/cn/domain/PinCode;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic createInstance(Ltoothpick/Scope;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lru/cn/domain/PinCode$$Factory;->createInstance(Ltoothpick/Scope;)Lru/cn/domain/PinCode;

    move-result-object v0

    return-object v0
.end method

.method public createInstance(Ltoothpick/Scope;)Lru/cn/domain/PinCode;
    .locals 3
    .param p1, "scope"    # Ltoothpick/Scope;

    .prologue
    .line 11
    invoke-virtual {p0, p1}, Lru/cn/domain/PinCode$$Factory;->getTargetScope(Ltoothpick/Scope;)Ltoothpick/Scope;

    move-result-object p1

    .line 12
    const-class v2, Landroid/content/Context;

    invoke-interface {p1, v2}, Ltoothpick/Scope;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 13
    .local v0, "param1":Landroid/content/Context;
    new-instance v1, Lru/cn/domain/PinCode;

    invoke-direct {v1, v0}, Lru/cn/domain/PinCode;-><init>(Landroid/content/Context;)V

    .line 14
    .local v1, "pinCode":Lru/cn/domain/PinCode;
    return-object v1
.end method

.method public getTargetScope(Ltoothpick/Scope;)Ltoothpick/Scope;
    .locals 0
    .param p1, "scope"    # Ltoothpick/Scope;

    .prologue
    .line 19
    return-object p1
.end method

.method public hasProvidesSingletonInScopeAnnotation()Z
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    return v0
.end method

.method public hasScopeAnnotation()Z
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    return v0
.end method
