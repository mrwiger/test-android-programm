.class final enum Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;
.super Ljava/lang/Enum;
.source "VideoPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/multiscreen/VideoPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "VideoPlayerAttributes"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

.field public static final enum thumbnailUrl:Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

.field public static final enum title:Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38
    new-instance v0, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

    const-string v1, "title"

    invoke-direct {v0, v1, v2}, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->title:Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

    .line 39
    new-instance v0, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

    const-string v1, "thumbnailUrl"

    invoke-direct {v0, v1, v3}, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->thumbnailUrl:Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

    .line 37
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

    sget-object v1, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->title:Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->thumbnailUrl:Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->$VALUES:[Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    const-class v0, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

    return-object v0
.end method

.method public static values()[Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->$VALUES:[Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

    invoke-virtual {v0}, [Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

    return-object v0
.end method
