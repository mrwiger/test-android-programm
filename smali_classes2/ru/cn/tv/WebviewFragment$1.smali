.class Lru/cn/tv/WebviewFragment$1;
.super Landroid/webkit/WebChromeClient;
.source "WebviewFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/WebviewFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/WebviewFragment;


# direct methods
.method constructor <init>(Lru/cn/tv/WebviewFragment;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/WebviewFragment;

    .prologue
    .line 70
    iput-object p1, p0, Lru/cn/tv/WebviewFragment$1;->this$0:Lru/cn/tv/WebviewFragment;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onJsAlert(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z
    .locals 4
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "result"    # Landroid/webkit/JsResult;

    .prologue
    .line 74
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 75
    invoke-virtual {v1, p3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    new-instance v3, Lru/cn/tv/WebviewFragment$1$1;

    invoke-direct {v3, p0, p4}, Lru/cn/tv/WebviewFragment$1$1;-><init>(Lru/cn/tv/WebviewFragment$1;Landroid/webkit/JsResult;)V

    .line 76
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 85
    .local v0, "b":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 86
    const/4 v1, 0x1

    return v1
.end method

.method public onJsConfirm(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z
    .locals 4
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "result"    # Landroid/webkit/JsResult;

    .prologue
    .line 92
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 93
    invoke-virtual {v1, p3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    new-instance v3, Lru/cn/tv/WebviewFragment$1$3;

    invoke-direct {v3, p0, p4}, Lru/cn/tv/WebviewFragment$1$3;-><init>(Lru/cn/tv/WebviewFragment$1;Landroid/webkit/JsResult;)V

    .line 94
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    new-instance v3, Lru/cn/tv/WebviewFragment$1$2;

    invoke-direct {v3, p0, p4}, Lru/cn/tv/WebviewFragment$1$2;-><init>(Lru/cn/tv/WebviewFragment$1;Landroid/webkit/JsResult;)V

    .line 102
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 110
    .local v0, "b":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 111
    const/4 v1, 0x1

    return v1
.end method

.method public onReceivedTitle(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 116
    invoke-super {p0, p1, p2}, Landroid/webkit/WebChromeClient;->onReceivedTitle(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lru/cn/tv/WebviewFragment$1;->this$0:Lru/cn/tv/WebviewFragment;

    invoke-static {v0}, Lru/cn/tv/WebviewFragment;->access$000(Lru/cn/tv/WebviewFragment;)Lru/cn/tv/WebviewFragment$WebviewFragmentListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lru/cn/tv/WebviewFragment$1;->this$0:Lru/cn/tv/WebviewFragment;

    invoke-static {v0}, Lru/cn/tv/WebviewFragment;->access$000(Lru/cn/tv/WebviewFragment;)Lru/cn/tv/WebviewFragment$WebviewFragmentListener;

    move-result-object v0

    invoke-interface {v0, p2}, Lru/cn/tv/WebviewFragment$WebviewFragmentListener;->onReceivedTitle(Ljava/lang/String;)V

    .line 120
    :cond_0
    return-void
.end method
