.class public final Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$OnLoadListener;
    }
.end annotation


# instance fields
.field a:Lcom/yandex/mobile/ads/ar$a;

.field private b:Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$OnLoadListener;

.field private final c:Lcom/yandex/mobile/ads/ar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/yandex/mobile/ads/nativeads/NativeAdLoaderConfiguration;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "configuration"    # Lcom/yandex/mobile/ads/nativeads/NativeAdLoaderConfiguration;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$1;

    invoke-direct {v0, p0}, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$1;-><init>(Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;)V

    iput-object v0, p0, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;->a:Lcom/yandex/mobile/ads/ar$a;

    .line 71
    new-instance v0, Lcom/yandex/mobile/ads/ar;

    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;->a:Lcom/yandex/mobile/ads/ar$a;

    invoke-direct {v0, p1, p2, v1}, Lcom/yandex/mobile/ads/ar;-><init>(Landroid/content/Context;Lcom/yandex/mobile/ads/nativeads/NativeAdLoaderConfiguration;Lcom/yandex/mobile/ads/ar$a;)V

    iput-object v0, p0, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;->c:Lcom/yandex/mobile/ads/ar;

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "blockId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 59
    new-instance v0, Lcom/yandex/mobile/ads/nativeads/NativeAdLoaderConfiguration$Builder;

    invoke-direct {v0, p2, v1}, Lcom/yandex/mobile/ads/nativeads/NativeAdLoaderConfiguration$Builder;-><init>(Ljava/lang/String;Z)V

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "small"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/yandex/mobile/ads/nativeads/NativeAdLoaderConfiguration$Builder;->setImageSizes([Ljava/lang/String;)Lcom/yandex/mobile/ads/nativeads/NativeAdLoaderConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/NativeAdLoaderConfiguration$Builder;->build()Lcom/yandex/mobile/ads/nativeads/NativeAdLoaderConfiguration;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;-><init>(Landroid/content/Context;Lcom/yandex/mobile/ads/nativeads/NativeAdLoaderConfiguration;)V

    .line 62
    return-void
.end method

.method static synthetic a(Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;)Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$OnLoadListener;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;->b:Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$OnLoadListener;

    return-object v0
.end method

.method static synthetic b(Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;)Lcom/yandex/mobile/ads/ar;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;->c:Lcom/yandex/mobile/ads/ar;

    return-object v0
.end method


# virtual methods
.method public cancelLoading()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;->c:Lcom/yandex/mobile/ads/ar;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/ar;->a()V

    .line 97
    return-void
.end method

.method public loadAd(Lcom/yandex/mobile/ads/AdRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/yandex/mobile/ads/AdRequest;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;->c:Lcom/yandex/mobile/ads/ar;

    invoke-virtual {v0, p1}, Lcom/yandex/mobile/ads/ar;->a(Lcom/yandex/mobile/ads/AdRequest;)V

    .line 90
    return-void
.end method

.method public setOnLoadListener(Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$OnLoadListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$OnLoadListener;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/yandex/mobile/ads/nativeads/NativeAdLoader;->b:Lcom/yandex/mobile/ads/nativeads/NativeAdLoader$OnLoadListener;

    .line 81
    return-void
.end method
