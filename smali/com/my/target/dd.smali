.class public final Lcom/my/target/dd;
.super Lcom/my/target/e;
.source "StandardAdResultProcessor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/my/target/e",
        "<",
        "Lcom/my/target/dh;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/my/target/e;-><init>()V

    .line 33
    return-void
.end method

.method public static e()Lcom/my/target/dd;
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/my/target/dd;

    invoke-direct {v0}, Lcom/my/target/dd;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Lcom/my/target/ak;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/ak;
    .locals 4

    .prologue
    .line 23
    check-cast p1, Lcom/my/target/dh;

    .line 1038
    const-string v0, "native"

    invoke-virtual {p1}, Lcom/my/target/dh;->w()Lcom/my/target/dg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/dg;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1040
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1041
    invoke-virtual {p1}, Lcom/my/target/dh;->R()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/c;

    .line 1043
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/c;->getIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v3

    .line 1044
    if-eqz v3, :cond_1

    .line 1046
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1048
    :cond_1
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/c;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 1049
    if-eqz v0, :cond_0

    .line 1051
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1055
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 1057
    invoke-static {v1}, Lcom/my/target/ch;->a(Ljava/util/List;)Lcom/my/target/ch;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/my/target/ch;->v(Landroid/content/Context;)V

    .line 1060
    :cond_3
    invoke-virtual {p1}, Lcom/my/target/dh;->R()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/c;

    .line 1062
    const-string v2, "banner"

    invoke-virtual {v0}, Lcom/my/target/core/models/banners/c;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1064
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/c;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v2

    .line 1065
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_4

    .line 1067
    :cond_5
    invoke-virtual {p1, v0}, Lcom/my/target/dh;->b(Lcom/my/target/core/models/banners/c;)V

    goto :goto_1

    .line 1071
    :cond_6
    invoke-virtual {p1}, Lcom/my/target/dh;->getBannersCount()I

    move-result v0

    if-nez v0, :cond_7

    .line 1073
    const/4 p1, 0x0

    :cond_7
    return-object p1
.end method
