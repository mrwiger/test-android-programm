.class public final Lru/cn/player/androidplayer/AndroidMediaPlayer;
.super Lru/cn/player/AbstractMediaPlayer;
.source "AndroidMediaPlayer.java"


# instance fields
.field private final infoListener:Landroid/media/MediaPlayer$OnInfoListener;

.field private final mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private final mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private final mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private mSeekWhenPrepared:I

.field private final mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

.field private final pixelWidthRatio:F

.field private player:Landroid/media/MediaPlayer;

.field private trackSelector:Lru/cn/player/androidplayer/AndroidTrackSelector;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lru/cn/player/AbstractMediaPlayer;-><init>(Landroid/content/Context;)V

    .line 55
    new-instance v0, Lru/cn/player/androidplayer/AndroidMediaPlayer$1;

    invoke-direct {v0, p0}, Lru/cn/player/androidplayer/AndroidMediaPlayer$1;-><init>(Lru/cn/player/androidplayer/AndroidMediaPlayer;)V

    iput-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 73
    new-instance v0, Lru/cn/player/androidplayer/AndroidMediaPlayer$2;

    invoke-direct {v0, p0}, Lru/cn/player/androidplayer/AndroidMediaPlayer$2;-><init>(Lru/cn/player/androidplayer/AndroidMediaPlayer;)V

    iput-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    .line 90
    new-instance v0, Lru/cn/player/androidplayer/AndroidMediaPlayer$3;

    invoke-direct {v0, p0}, Lru/cn/player/androidplayer/AndroidMediaPlayer$3;-><init>(Lru/cn/player/androidplayer/AndroidMediaPlayer;)V

    iput-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 99
    new-instance v0, Lru/cn/player/androidplayer/AndroidMediaPlayer$4;

    invoke-direct {v0, p0}, Lru/cn/player/androidplayer/AndroidMediaPlayer$4;-><init>(Lru/cn/player/androidplayer/AndroidMediaPlayer;)V

    iput-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 118
    new-instance v0, Lru/cn/player/androidplayer/AndroidMediaPlayer$5;

    invoke-direct {v0, p0}, Lru/cn/player/androidplayer/AndroidMediaPlayer$5;-><init>(Lru/cn/player/androidplayer/AndroidMediaPlayer;)V

    iput-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->infoListener:Landroid/media/MediaPlayer$OnInfoListener;

    .line 38
    invoke-direct {p0}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->supportsAnamorphicPixels()Z

    move-result v0

    if-eqz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    iput v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->pixelWidthRatio:F

    .line 39
    invoke-direct {p0}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->init()V

    .line 40
    return-void

    .line 38
    :cond_0
    const/high16 v0, -0x40800000    # -1.0f

    goto :goto_0
.end method

.method static synthetic access$000(Lru/cn/player/androidplayer/AndroidMediaPlayer;)I
    .locals 1
    .param p0, "x0"    # Lru/cn/player/androidplayer/AndroidMediaPlayer;

    .prologue
    .line 20
    iget v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->mSeekWhenPrepared:I

    return v0
.end method

.method static synthetic access$002(Lru/cn/player/androidplayer/AndroidMediaPlayer;I)I
    .locals 0
    .param p0, "x0"    # Lru/cn/player/androidplayer/AndroidMediaPlayer;
    .param p1, "x1"    # I

    .prologue
    .line 20
    iput p1, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->mSeekWhenPrepared:I

    return p1
.end method

.method static synthetic access$100(Lru/cn/player/androidplayer/AndroidMediaPlayer;)Lru/cn/player/AudioFocus;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/androidplayer/AndroidMediaPlayer;

    .prologue
    .line 20
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->audioFocus:Lru/cn/player/AudioFocus;

    return-object v0
.end method

.method static synthetic access$1000(Lru/cn/player/androidplayer/AndroidMediaPlayer;)Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/androidplayer/AndroidMediaPlayer;

    .prologue
    .line 20
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    return-object v0
.end method

.method static synthetic access$1100(Lru/cn/player/androidplayer/AndroidMediaPlayer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/androidplayer/AndroidMediaPlayer;

    .prologue
    .line 20
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lru/cn/player/androidplayer/AndroidMediaPlayer;Z)V
    .locals 0
    .param p0, "x0"    # Lru/cn/player/androidplayer/AndroidMediaPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->notifyBuffering(Z)V

    return-void
.end method

.method static synthetic access$1300(Lru/cn/player/androidplayer/AndroidMediaPlayer;Z)V
    .locals 0
    .param p0, "x0"    # Lru/cn/player/androidplayer/AndroidMediaPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->notifyBuffering(Z)V

    return-void
.end method

.method static synthetic access$1400(Lru/cn/player/androidplayer/AndroidMediaPlayer;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/androidplayer/AndroidMediaPlayer;

    .prologue
    .line 20
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$1500(Lru/cn/player/androidplayer/AndroidMediaPlayer;)Landroid/media/MediaPlayer$OnErrorListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/androidplayer/AndroidMediaPlayer;

    .prologue
    .line 20
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/player/androidplayer/AndroidMediaPlayer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/androidplayer/AndroidMediaPlayer;

    .prologue
    .line 20
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/player/androidplayer/AndroidMediaPlayer;)Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/androidplayer/AndroidMediaPlayer;

    .prologue
    .line 20
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    return-object v0
.end method

.method static synthetic access$400(Lru/cn/player/androidplayer/AndroidMediaPlayer;)F
    .locals 1
    .param p0, "x0"    # Lru/cn/player/androidplayer/AndroidMediaPlayer;

    .prologue
    .line 20
    iget v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->pixelWidthRatio:F

    return v0
.end method

.method static synthetic access$500(Lru/cn/player/androidplayer/AndroidMediaPlayer;)Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/androidplayer/AndroidMediaPlayer;

    .prologue
    .line 20
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    return-object v0
.end method

.method static synthetic access$600(Lru/cn/player/androidplayer/AndroidMediaPlayer;)Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/androidplayer/AndroidMediaPlayer;

    .prologue
    .line 20
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    return-object v0
.end method

.method static synthetic access$700(Lru/cn/player/androidplayer/AndroidMediaPlayer;)Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/androidplayer/AndroidMediaPlayer;

    .prologue
    .line 20
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    return-object v0
.end method

.method static synthetic access$800(Lru/cn/player/androidplayer/AndroidMediaPlayer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/androidplayer/AndroidMediaPlayer;

    .prologue
    .line 20
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lru/cn/player/androidplayer/AndroidMediaPlayer;)Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/androidplayer/AndroidMediaPlayer;

    .prologue
    .line 20
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    return-object v0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 139
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    .line 140
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 141
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 142
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 143
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 144
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->infoListener:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 145
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 148
    :cond_0
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 149
    new-instance v0, Lru/cn/player/androidplayer/AndroidTrackSelector;

    iget-object v1, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    invoke-direct {v0, v1}, Lru/cn/player/androidplayer/AndroidTrackSelector;-><init>(Landroid/media/MediaPlayer;)V

    iput-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->trackSelector:Lru/cn/player/androidplayer/AndroidTrackSelector;

    .line 150
    return-void
.end method

.method private supportsAnamorphicPixels()Z
    .locals 2

    .prologue
    .line 43
    const-string v0, "Eltex"

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "NV312W"

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 44
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "NV-310-Wac:revB"

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 45
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "NV310WAC"

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 46
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "NV501WAC"

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 47
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "NV501"

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 48
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 49
    :cond_0
    const/4 v0, 0x1

    .line 52
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 167
    invoke-virtual {p0}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->stop()V

    .line 168
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 170
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 171
    iput-object v1, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    .line 173
    :cond_0
    return-void
.end method

.method public getAudioTrackProvider()Lru/cn/player/AudioTrackSelector;
    .locals 2

    .prologue
    .line 325
    new-instance v0, Lru/cn/player/AudioTrackSelector;

    iget-object v1, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->trackSelector:Lru/cn/player/androidplayer/AndroidTrackSelector;

    invoke-direct {v0, v1}, Lru/cn/player/AudioTrackSelector;-><init>(Lru/cn/player/Selector;)V

    return-object v0
.end method

.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    .line 271
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDuration()I
    .locals 2

    .prologue
    .line 254
    iget-object v1, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_1

    .line 255
    iget-object v1, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    .line 256
    .local v0, "duration":I
    const v1, 0x5265c00

    if-le v0, v1, :cond_0

    .line 257
    const/4 v0, 0x0

    .line 263
    .end local v0    # "duration":I
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getSubtitleTracksProvider()Lru/cn/player/SubtitleTrackSelector;
    .locals 2

    .prologue
    .line 330
    new-instance v0, Lru/cn/player/SubtitleTrackSelector;

    iget-object v1, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->trackSelector:Lru/cn/player/androidplayer/AndroidTrackSelector;

    invoke-direct {v0, v1}, Lru/cn/player/SubtitleTrackSelector;-><init>(Lru/cn/player/Selector;)V

    return-object v0
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 214
    sget-object v0, Lru/cn/player/AbstractMediaPlayer$PlayerState;->PAUSED:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    .line 216
    :cond_0
    return-void
.end method

.method public play(Landroid/net/Uri;)V
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v5, 0x0

    .line 177
    const/16 v1, 0x7530

    .line 178
    .local v1, "timeout":I
    const-string v2, "udp"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 179
    const/16 v1, 0x1770

    .line 182
    :cond_0
    iget-object v2, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->playerTimeoutHandler:Lru/cn/player/TimeoutHandler;

    new-instance v3, Lru/cn/player/androidplayer/AndroidMediaPlayer$6;

    invoke-direct {v3, p0}, Lru/cn/player/androidplayer/AndroidMediaPlayer$6;-><init>(Lru/cn/player/androidplayer/AndroidMediaPlayer;)V

    invoke-virtual {v2, v3, v1}, Lru/cn/player/TimeoutHandler;->startTimer(Ljava/lang/Runnable;I)V

    .line 190
    iget-object v2, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_1

    .line 191
    invoke-virtual {p0}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->stop()V

    .line 194
    :cond_1
    iget-object v2, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    if-nez v2, :cond_2

    .line 195
    invoke-direct {p0}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->init()V

    .line 198
    :cond_2
    sget-object v2, Lru/cn/player/AbstractMediaPlayer$PlayerState;->LOADING:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    invoke-virtual {p0, v2}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    .line 201
    :try_start_0
    iget-object v2, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->context:Landroid/content/Context;

    invoke-virtual {v2, v3, p1}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 202
    iget-object v2, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 208
    :goto_0
    return-void

    .line 203
    :catch_0
    move-exception v0

    .line 204
    .local v0, "e":Ljava/io/IOException;
    iget-object v2, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    iget-object v3, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    const/16 v4, -0x3ec

    invoke-interface {v2, v3, v5, v4}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    goto :goto_0

    .line 205
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 206
    .local v0, "e":Ljava/lang/IllegalStateException;
    iget-object v2, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    iget-object v3, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    const/16 v4, 0x3ea

    invoke-interface {v2, v3, v5, v4}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    goto :goto_0
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->audioFocus:Lru/cn/player/AudioFocus;

    invoke-virtual {v0}, Lru/cn/player/AudioFocus;->request()Z

    .line 223
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 224
    sget-object v0, Lru/cn/player/AbstractMediaPlayer$PlayerState;->PLAYING:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    .line 226
    :cond_0
    return-void
.end method

.method public seekTo(I)V
    .locals 2
    .param p1, "msec"    # I

    .prologue
    .line 283
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 284
    sget-object v0, Lru/cn/player/androidplayer/AndroidMediaPlayer$7;->$SwitchMap$ru$cn$player$AbstractMediaPlayer$PlayerState:[I

    invoke-virtual {p0}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->getState()Lru/cn/player/AbstractMediaPlayer$PlayerState;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/player/AbstractMediaPlayer$PlayerState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 296
    iput p1, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->mSeekWhenPrepared:I

    .line 302
    :goto_0
    return-void

    .line 287
    :pswitch_0
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    goto :goto_0

    .line 291
    :pswitch_1
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 292
    invoke-virtual {p0}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->resume()V

    goto :goto_0

    .line 300
    :cond_0
    iput p1, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->mSeekWhenPrepared:I

    goto :goto_0

    .line 284
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setChangeQualityListener(Lru/cn/player/ChangeQualityListener;)V
    .locals 0
    .param p1, "changeQualityListener"    # Lru/cn/player/ChangeQualityListener;

    .prologue
    .line 321
    return-void
.end method

.method public setDisplay(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "surfaceHolder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 154
    invoke-super {p0, p1}, Lru/cn/player/AbstractMediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 155
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 157
    :try_start_0
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 158
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V
    .locals 2
    .param p1, "state"    # Lru/cn/player/AbstractMediaPlayer$PlayerState;

    .prologue
    .line 306
    invoke-super {p0, p1}, Lru/cn/player/AbstractMediaPlayer;->setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    .line 308
    sget-object v0, Lru/cn/player/androidplayer/AndroidMediaPlayer$7;->$SwitchMap$ru$cn$player$AbstractMediaPlayer$PlayerState:[I

    invoke-virtual {p1}, Lru/cn/player/AbstractMediaPlayer$PlayerState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 318
    :goto_0
    :pswitch_0
    return-void

    .line 310
    :pswitch_1
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->trackSelector:Lru/cn/player/androidplayer/AndroidTrackSelector;

    invoke-virtual {v0}, Lru/cn/player/androidplayer/AndroidTrackSelector;->reset()V

    .line 311
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->playerTimeoutHandler:Lru/cn/player/TimeoutHandler;

    invoke-virtual {v0}, Lru/cn/player/TimeoutHandler;->reset()V

    goto :goto_0

    .line 315
    :pswitch_2
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->playerTimeoutHandler:Lru/cn/player/TimeoutHandler;

    invoke-virtual {v0}, Lru/cn/player/TimeoutHandler;->reset()V

    goto :goto_0

    .line 308
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setVolume(F)V
    .locals 1
    .param p1, "volume"    # F

    .prologue
    .line 276
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1, p1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 279
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->audioFocus:Lru/cn/player/AudioFocus;

    invoke-virtual {v0}, Lru/cn/player/AudioFocus;->abandon()V

    .line 232
    const/4 v0, 0x0

    iput v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->mSeekWhenPrepared:I

    .line 233
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 239
    invoke-virtual {p0}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->getState()Lru/cn/player/AbstractMediaPlayer$PlayerState;

    move-result-object v0

    sget-object v1, Lru/cn/player/AbstractMediaPlayer$PlayerState;->LOADING:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_1

    .line 240
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 241
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    .line 246
    :goto_0
    sget-object v0, Lru/cn/player/AbstractMediaPlayer$PlayerState;->STOPPED:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    .line 249
    :cond_0
    invoke-virtual {p0}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->clearSurface()V

    .line 250
    return-void

    .line 243
    :cond_1
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    goto :goto_0
.end method
