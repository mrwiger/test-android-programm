.class public final Lcom/google/android/exoplayer2/text/cea/CeaUtil;
.super Ljava/lang/Object;
.source "CeaUtil.java"


# static fields
.field private static final USER_ID_DTG1:I

.field private static final USER_ID_GA94:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-string v0, "GA94"

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/Util;->getIntegerCodeForString(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/exoplayer2/text/cea/CeaUtil;->USER_ID_GA94:I

    .line 34
    const-string v0, "DTG1"

    invoke-static {v0}, Lcom/google/android/exoplayer2/util/Util;->getIntegerCodeForString(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/exoplayer2/text/cea/CeaUtil;->USER_ID_DTG1:I

    return-void
.end method

.method public static consume(JLcom/google/android/exoplayer2/util/ParsableByteArray;[Lcom/google/android/exoplayer2/extractor/TrackOutput;)V
    .locals 22
    .param p0, "presentationTimeUs"    # J
    .param p2, "seiBuffer"    # Lcom/google/android/exoplayer2/util/ParsableByteArray;
    .param p3, "outputs"    # [Lcom/google/android/exoplayer2/extractor/TrackOutput;

    .prologue
    .line 47
    :goto_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/exoplayer2/util/ParsableByteArray;->bytesLeft()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_a

    .line 48
    invoke-static/range {p2 .. p2}, Lcom/google/android/exoplayer2/text/cea/CeaUtil;->readNon255TerminatedValue(Lcom/google/android/exoplayer2/util/ParsableByteArray;)I

    move-result v14

    .line 49
    .local v14, "payloadType":I
    invoke-static/range {p2 .. p2}, Lcom/google/android/exoplayer2/text/cea/CeaUtil;->readNon255TerminatedValue(Lcom/google/android/exoplayer2/util/ParsableByteArray;)I

    move-result v13

    .line 50
    .local v13, "payloadSize":I
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/exoplayer2/util/ParsableByteArray;->getPosition()I

    move-result v4

    add-int v12, v4, v13

    .line 52
    .local v12, "nextPayloadPosition":I
    const/4 v4, -0x1

    if-eq v13, v4, :cond_0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/exoplayer2/util/ParsableByteArray;->bytesLeft()I

    move-result v4

    if-le v13, v4, :cond_2

    .line 54
    :cond_0
    const-string v4, "CeaUtil"

    const-string v5, "Skipping remainder of malformed SEI NAL unit."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/exoplayer2/util/ParsableByteArray;->limit()I

    move-result v12

    .line 92
    :cond_1
    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Lcom/google/android/exoplayer2/util/ParsableByteArray;->setPosition(I)V

    goto :goto_0

    .line 56
    :cond_2
    const/4 v4, 0x4

    if-ne v14, v4, :cond_1

    const/16 v4, 0x8

    if-lt v13, v4, :cond_1

    .line 57
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/exoplayer2/util/ParsableByteArray;->readUnsignedByte()I

    move-result v10

    .line 58
    .local v10, "countryCode":I
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/exoplayer2/util/ParsableByteArray;->readUnsignedShort()I

    move-result v15

    .line 59
    .local v15, "providerCode":I
    const/16 v18, 0x0

    .line 60
    .local v18, "userIdentifier":I
    const/16 v4, 0x31

    if-ne v15, v4, :cond_3

    .line 61
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/exoplayer2/util/ParsableByteArray;->readInt()I

    move-result v18

    .line 63
    :cond_3
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/exoplayer2/util/ParsableByteArray;->readUnsignedByte()I

    move-result v17

    .line 64
    .local v17, "userDataTypeCode":I
    const/16 v4, 0x2f

    if-ne v15, v4, :cond_4

    .line 65
    const/4 v4, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer2/util/ParsableByteArray;->skipBytes(I)V

    .line 67
    :cond_4
    const/16 v4, 0xb5

    if-ne v10, v4, :cond_8

    const/16 v4, 0x31

    if-eq v15, v4, :cond_5

    const/16 v4, 0x2f

    if-ne v15, v4, :cond_8

    :cond_5
    const/4 v4, 0x3

    move/from16 v0, v17

    if-ne v0, v4, :cond_8

    const/4 v11, 0x1

    .line 71
    .local v11, "messageIsSupportedCeaCaption":Z
    :goto_1
    const/16 v4, 0x31

    if-ne v15, v4, :cond_7

    .line 72
    sget v4, Lcom/google/android/exoplayer2/text/cea/CeaUtil;->USER_ID_GA94:I

    move/from16 v0, v18

    if-eq v0, v4, :cond_6

    sget v4, Lcom/google/android/exoplayer2/text/cea/CeaUtil;->USER_ID_DTG1:I

    move/from16 v0, v18

    if-ne v0, v4, :cond_9

    :cond_6
    const/4 v4, 0x1

    :goto_2
    and-int/2addr v11, v4

    .line 75
    :cond_7
    if-eqz v11, :cond_1

    .line 77
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/exoplayer2/util/ParsableByteArray;->readUnsignedByte()I

    move-result v4

    and-int/lit8 v2, v4, 0x1f

    .line 79
    .local v2, "ccCount":I
    const/4 v4, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer2/util/ParsableByteArray;->skipBytes(I)V

    .line 82
    mul-int/lit8 v7, v2, 0x3

    .line 83
    .local v7, "sampleLength":I
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/exoplayer2/util/ParsableByteArray;->getPosition()I

    move-result v16

    .line 84
    .local v16, "sampleStartPosition":I
    move-object/from16 v0, p3

    array-length v0, v0

    move/from16 v20, v0

    const/4 v4, 0x0

    move/from16 v19, v4

    :goto_3
    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_1

    aget-object v3, p3, v19

    .line 85
    .local v3, "output":Lcom/google/android/exoplayer2/extractor/TrackOutput;
    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/util/ParsableByteArray;->setPosition(I)V

    .line 86
    move-object/from16 v0, p2

    invoke-interface {v3, v0, v7}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->sampleData(Lcom/google/android/exoplayer2/util/ParsableByteArray;I)V

    .line 87
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-wide/from16 v4, p0

    invoke-interface/range {v3 .. v9}, Lcom/google/android/exoplayer2/extractor/TrackOutput;->sampleMetadata(JIIILcom/google/android/exoplayer2/extractor/TrackOutput$CryptoData;)V

    .line 84
    add-int/lit8 v4, v19, 0x1

    move/from16 v19, v4

    goto :goto_3

    .line 67
    .end local v2    # "ccCount":I
    .end local v3    # "output":Lcom/google/android/exoplayer2/extractor/TrackOutput;
    .end local v7    # "sampleLength":I
    .end local v11    # "messageIsSupportedCeaCaption":Z
    .end local v16    # "sampleStartPosition":I
    :cond_8
    const/4 v11, 0x0

    goto :goto_1

    .line 72
    .restart local v11    # "messageIsSupportedCeaCaption":Z
    :cond_9
    const/4 v4, 0x0

    goto :goto_2

    .line 94
    .end local v10    # "countryCode":I
    .end local v11    # "messageIsSupportedCeaCaption":Z
    .end local v12    # "nextPayloadPosition":I
    .end local v13    # "payloadSize":I
    .end local v14    # "payloadType":I
    .end local v15    # "providerCode":I
    .end local v17    # "userDataTypeCode":I
    .end local v18    # "userIdentifier":I
    :cond_a
    return-void
.end method

.method private static readNon255TerminatedValue(Lcom/google/android/exoplayer2/util/ParsableByteArray;)I
    .locals 3
    .param p0, "buffer"    # Lcom/google/android/exoplayer2/util/ParsableByteArray;

    .prologue
    .line 106
    const/4 v1, 0x0

    .line 108
    .local v1, "value":I
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/ParsableByteArray;->bytesLeft()I

    move-result v2

    if-nez v2, :cond_1

    .line 109
    const/4 v2, -0x1

    .line 114
    :goto_0
    return v2

    .line 111
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/util/ParsableByteArray;->readUnsignedByte()I

    move-result v0

    .line 112
    .local v0, "b":I
    add-int/2addr v1, v0

    .line 113
    const/16 v2, 0xff

    if-eq v0, v2, :cond_0

    move v2, v1

    .line 114
    goto :goto_0
.end method
