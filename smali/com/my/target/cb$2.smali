.class Lcom/my/target/cb$2;
.super Landroid/webkit/WebChromeClient;
.source "WebViewBrowser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/my/target/cb;->be()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic jn:Lcom/my/target/cb;


# direct methods
.method constructor <init>(Lcom/my/target/cb;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/my/target/cb$2;->jn:Lcom/my/target/cb;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/webkit/WebView;I)V
    .locals 4
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "progress"    # I

    .prologue
    const/16 v3, 0x64

    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 159
    if-ge p2, v3, :cond_0

    iget-object v0, p0, Lcom/my/target/cb$2;->jn:Lcom/my/target/cb;

    invoke-static {v0}, Lcom/my/target/cb;->c(Lcom/my/target/cb;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 161
    iget-object v0, p0, Lcom/my/target/cb$2;->jn:Lcom/my/target/cb;

    invoke-static {v0}, Lcom/my/target/cb;->c(Lcom/my/target/cb;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 162
    iget-object v0, p0, Lcom/my/target/cb$2;->jn:Lcom/my/target/cb;

    invoke-static {v0}, Lcom/my/target/cb;->d(Lcom/my/target/cb;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/my/target/cb$2;->jn:Lcom/my/target/cb;

    invoke-static {v0}, Lcom/my/target/cb;->c(Lcom/my/target/cb;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 165
    if-lt p2, v3, :cond_1

    .line 167
    iget-object v0, p0, Lcom/my/target/cb$2;->jn:Lcom/my/target/cb;

    invoke-static {v0}, Lcom/my/target/cb;->c(Lcom/my/target/cb;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 168
    iget-object v0, p0, Lcom/my/target/cb$2;->jn:Lcom/my/target/cb;

    invoke-static {v0}, Lcom/my/target/cb;->d(Lcom/my/target/cb;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 170
    :cond_1
    return-void
.end method

.method public onReceivedTitle(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 151
    invoke-super {p0, p1, p2}, Landroid/webkit/WebChromeClient;->onReceivedTitle(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/my/target/cb$2;->jn:Lcom/my/target/cb;

    invoke-static {v0}, Lcom/my/target/cb;->b(Lcom/my/target/cb;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Landroid/webkit/WebView;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    iget-object v0, p0, Lcom/my/target/cb$2;->jn:Lcom/my/target/cb;

    invoke-static {v0}, Lcom/my/target/cb;->b(Lcom/my/target/cb;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 154
    return-void
.end method
