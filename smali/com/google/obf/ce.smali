.class final Lcom/google/obf/ce;
.super Lcom/google/obf/cb;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/ce$a;
    }
.end annotation


# instance fields
.field private b:Z

.field private final c:Lcom/google/obf/ck;

.field private final d:[Z

.field private final e:Lcom/google/obf/ch;

.field private final f:Lcom/google/obf/ch;

.field private final g:Lcom/google/obf/ch;

.field private final h:Lcom/google/obf/ch;

.field private final i:Lcom/google/obf/ch;

.field private final j:Lcom/google/obf/ce$a;

.field private k:J

.field private l:J

.field private final m:Lcom/google/obf/dw;


# direct methods
.method public constructor <init>(Lcom/google/obf/ar;Lcom/google/obf/ck;)V
    .locals 3

    .prologue
    const/16 v2, 0x80

    .line 1
    invoke-direct {p0, p1}, Lcom/google/obf/cb;-><init>(Lcom/google/obf/ar;)V

    .line 2
    iput-object p2, p0, Lcom/google/obf/ce;->c:Lcom/google/obf/ck;

    .line 3
    const/4 v0, 0x3

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/google/obf/ce;->d:[Z

    .line 4
    new-instance v0, Lcom/google/obf/ch;

    const/16 v1, 0x20

    invoke-direct {v0, v1, v2}, Lcom/google/obf/ch;-><init>(II)V

    iput-object v0, p0, Lcom/google/obf/ce;->e:Lcom/google/obf/ch;

    .line 5
    new-instance v0, Lcom/google/obf/ch;

    const/16 v1, 0x21

    invoke-direct {v0, v1, v2}, Lcom/google/obf/ch;-><init>(II)V

    iput-object v0, p0, Lcom/google/obf/ce;->f:Lcom/google/obf/ch;

    .line 6
    new-instance v0, Lcom/google/obf/ch;

    const/16 v1, 0x22

    invoke-direct {v0, v1, v2}, Lcom/google/obf/ch;-><init>(II)V

    iput-object v0, p0, Lcom/google/obf/ce;->g:Lcom/google/obf/ch;

    .line 7
    new-instance v0, Lcom/google/obf/ch;

    const/16 v1, 0x27

    invoke-direct {v0, v1, v2}, Lcom/google/obf/ch;-><init>(II)V

    iput-object v0, p0, Lcom/google/obf/ce;->h:Lcom/google/obf/ch;

    .line 8
    new-instance v0, Lcom/google/obf/ch;

    const/16 v1, 0x28

    invoke-direct {v0, v1, v2}, Lcom/google/obf/ch;-><init>(II)V

    iput-object v0, p0, Lcom/google/obf/ce;->i:Lcom/google/obf/ch;

    .line 9
    new-instance v0, Lcom/google/obf/ce$a;

    invoke-direct {v0, p1}, Lcom/google/obf/ce$a;-><init>(Lcom/google/obf/ar;)V

    iput-object v0, p0, Lcom/google/obf/ce;->j:Lcom/google/obf/ce$a;

    .line 10
    new-instance v0, Lcom/google/obf/dw;

    invoke-direct {v0}, Lcom/google/obf/dw;-><init>()V

    iput-object v0, p0, Lcom/google/obf/ce;->m:Lcom/google/obf/dw;

    .line 11
    return-void
.end method

.method private static a(Lcom/google/obf/ch;Lcom/google/obf/ch;Lcom/google/obf/ch;)Lcom/google/obf/q;
    .locals 13

    .prologue
    .line 84
    iget v0, p0, Lcom/google/obf/ch;->b:I

    iget v1, p1, Lcom/google/obf/ch;->b:I

    add-int/2addr v0, v1

    iget v1, p2, Lcom/google/obf/ch;->b:I

    add-int/2addr v0, v1

    new-array v8, v0, [B

    .line 85
    iget-object v0, p0, Lcom/google/obf/ch;->a:[B

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/obf/ch;->b:I

    invoke-static {v0, v1, v8, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 86
    iget-object v0, p1, Lcom/google/obf/ch;->a:[B

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/obf/ch;->b:I

    iget v3, p1, Lcom/google/obf/ch;->b:I

    invoke-static {v0, v1, v8, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 87
    iget-object v0, p2, Lcom/google/obf/ch;->a:[B

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/obf/ch;->b:I

    iget v3, p1, Lcom/google/obf/ch;->b:I

    add-int/2addr v2, v3

    iget v3, p2, Lcom/google/obf/ch;->b:I

    invoke-static {v0, v1, v8, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 88
    iget-object v0, p1, Lcom/google/obf/ch;->a:[B

    iget v1, p1, Lcom/google/obf/ch;->b:I

    invoke-static {v0, v1}, Lcom/google/obf/du;->a([BI)I

    .line 89
    new-instance v3, Lcom/google/obf/dv;

    iget-object v0, p1, Lcom/google/obf/ch;->a:[B

    invoke-direct {v3, v0}, Lcom/google/obf/dv;-><init>([B)V

    .line 90
    const/16 v0, 0x2c

    invoke-virtual {v3, v0}, Lcom/google/obf/dv;->b(I)V

    .line 91
    const/4 v0, 0x3

    invoke-virtual {v3, v0}, Lcom/google/obf/dv;->c(I)I

    move-result v1

    .line 92
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lcom/google/obf/dv;->b(I)V

    .line 93
    const/16 v0, 0x58

    invoke-virtual {v3, v0}, Lcom/google/obf/dv;->b(I)V

    .line 94
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, Lcom/google/obf/dv;->b(I)V

    .line 95
    const/4 v2, 0x0

    .line 96
    const/4 v0, 0x0

    move v12, v0

    move v0, v2

    move v2, v12

    :goto_0
    if-ge v2, v1, :cond_2

    .line 97
    invoke-virtual {v3}, Lcom/google/obf/dv;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 98
    add-int/lit8 v0, v0, 0x59

    .line 99
    :cond_0
    invoke-virtual {v3}, Lcom/google/obf/dv;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 100
    add-int/lit8 v0, v0, 0x8

    .line 101
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 102
    :cond_2
    invoke-virtual {v3, v0}, Lcom/google/obf/dv;->b(I)V

    .line 103
    if-lez v1, :cond_3

    .line 104
    rsub-int/lit8 v0, v1, 0x8

    mul-int/lit8 v0, v0, 0x2

    invoke-virtual {v3, v0}, Lcom/google/obf/dv;->b(I)V

    .line 105
    :cond_3
    invoke-virtual {v3}, Lcom/google/obf/dv;->d()I

    .line 106
    invoke-virtual {v3}, Lcom/google/obf/dv;->d()I

    move-result v4

    .line 107
    const/4 v0, 0x3

    if-ne v4, v0, :cond_4

    .line 108
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lcom/google/obf/dv;->b(I)V

    .line 109
    :cond_4
    invoke-virtual {v3}, Lcom/google/obf/dv;->d()I

    move-result v6

    .line 110
    invoke-virtual {v3}, Lcom/google/obf/dv;->d()I

    move-result v7

    .line 111
    invoke-virtual {v3}, Lcom/google/obf/dv;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 112
    invoke-virtual {v3}, Lcom/google/obf/dv;->d()I

    move-result v5

    .line 113
    invoke-virtual {v3}, Lcom/google/obf/dv;->d()I

    move-result v9

    .line 114
    invoke-virtual {v3}, Lcom/google/obf/dv;->d()I

    move-result v10

    .line 115
    invoke-virtual {v3}, Lcom/google/obf/dv;->d()I

    move-result v11

    .line 116
    const/4 v0, 0x1

    if-eq v4, v0, :cond_5

    const/4 v0, 0x2

    if-ne v4, v0, :cond_7

    :cond_5
    const/4 v0, 0x2

    move v2, v0

    .line 117
    :goto_1
    const/4 v0, 0x1

    if-ne v4, v0, :cond_8

    const/4 v0, 0x2

    .line 118
    :goto_2
    add-int v4, v5, v9

    mul-int/2addr v2, v4

    sub-int/2addr v6, v2

    .line 119
    add-int v2, v10, v11

    mul-int/2addr v0, v2

    sub-int/2addr v7, v0

    .line 120
    :cond_6
    invoke-virtual {v3}, Lcom/google/obf/dv;->d()I

    .line 121
    invoke-virtual {v3}, Lcom/google/obf/dv;->d()I

    .line 122
    invoke-virtual {v3}, Lcom/google/obf/dv;->d()I

    move-result v2

    .line 123
    invoke-virtual {v3}, Lcom/google/obf/dv;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x0

    :goto_3
    if-gt v0, v1, :cond_a

    .line 124
    invoke-virtual {v3}, Lcom/google/obf/dv;->d()I

    .line 125
    invoke-virtual {v3}, Lcom/google/obf/dv;->d()I

    .line 126
    invoke-virtual {v3}, Lcom/google/obf/dv;->d()I

    .line 127
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 116
    :cond_7
    const/4 v0, 0x1

    move v2, v0

    goto :goto_1

    .line 117
    :cond_8
    const/4 v0, 0x1

    goto :goto_2

    :cond_9
    move v0, v1

    .line 123
    goto :goto_3

    .line 128
    :cond_a
    invoke-virtual {v3}, Lcom/google/obf/dv;->d()I

    .line 129
    invoke-virtual {v3}, Lcom/google/obf/dv;->d()I

    .line 130
    invoke-virtual {v3}, Lcom/google/obf/dv;->d()I

    .line 131
    invoke-virtual {v3}, Lcom/google/obf/dv;->d()I

    .line 132
    invoke-virtual {v3}, Lcom/google/obf/dv;->d()I

    .line 133
    invoke-virtual {v3}, Lcom/google/obf/dv;->d()I

    .line 134
    invoke-virtual {v3}, Lcom/google/obf/dv;->b()Z

    move-result v0

    .line 135
    if-eqz v0, :cond_b

    invoke-virtual {v3}, Lcom/google/obf/dv;->b()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 136
    invoke-static {v3}, Lcom/google/obf/ce;->a(Lcom/google/obf/dv;)V

    .line 137
    :cond_b
    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Lcom/google/obf/dv;->b(I)V

    .line 138
    invoke-virtual {v3}, Lcom/google/obf/dv;->b()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 139
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, Lcom/google/obf/dv;->b(I)V

    .line 140
    invoke-virtual {v3}, Lcom/google/obf/dv;->d()I

    .line 141
    invoke-virtual {v3}, Lcom/google/obf/dv;->d()I

    .line 142
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lcom/google/obf/dv;->b(I)V

    .line 143
    :cond_c
    invoke-static {v3}, Lcom/google/obf/ce;->b(Lcom/google/obf/dv;)V

    .line 144
    invoke-virtual {v3}, Lcom/google/obf/dv;->b()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 145
    const/4 v0, 0x0

    :goto_4
    invoke-virtual {v3}, Lcom/google/obf/dv;->d()I

    move-result v1

    if-ge v0, v1, :cond_d

    .line 146
    add-int/lit8 v1, v2, 0x4

    .line 147
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v3, v1}, Lcom/google/obf/dv;->b(I)V

    .line 148
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 149
    :cond_d
    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Lcom/google/obf/dv;->b(I)V

    .line 150
    const/high16 v0, 0x3f800000    # 1.0f

    .line 151
    invoke-virtual {v3}, Lcom/google/obf/dv;->b()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 152
    invoke-virtual {v3}, Lcom/google/obf/dv;->b()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 153
    const/16 v1, 0x8

    invoke-virtual {v3, v1}, Lcom/google/obf/dv;->c(I)I

    move-result v1

    .line 154
    const/16 v2, 0xff

    if-ne v1, v2, :cond_f

    .line 155
    const/16 v1, 0x10

    invoke-virtual {v3, v1}, Lcom/google/obf/dv;->c(I)I

    move-result v1

    .line 156
    const/16 v2, 0x10

    invoke-virtual {v3, v2}, Lcom/google/obf/dv;->c(I)I

    move-result v2

    .line 157
    if-eqz v1, :cond_e

    if-eqz v2, :cond_e

    .line 158
    int-to-float v0, v1

    int-to-float v1, v2

    div-float/2addr v0, v1

    :cond_e
    move v10, v0

    .line 162
    :goto_5
    const/4 v0, 0x0

    const-string v1, "video/hevc"

    const/4 v2, -0x1

    const/4 v3, -0x1

    const-wide/16 v4, -0x1

    .line 163
    invoke-static {v8}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    const/4 v9, -0x1

    .line 164
    invoke-static/range {v0 .. v10}, Lcom/google/obf/q;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;IF)Lcom/google/obf/q;

    move-result-object v0

    return-object v0

    .line 159
    :cond_f
    sget-object v2, Lcom/google/obf/du;->b:[F

    array-length v2, v2

    if-ge v1, v2, :cond_10

    .line 160
    sget-object v0, Lcom/google/obf/du;->b:[F

    aget v0, v0, v1

    move v10, v0

    goto :goto_5

    .line 161
    :cond_10
    const-string v2, "H265Reader"

    const/16 v3, 0x2e

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unexpected aspect_ratio_idc value: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_11
    move v10, v0

    goto :goto_5
.end method

.method private a(JIIJ)V
    .locals 9

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/google/obf/ce;->b:Z

    if-eqz v0, :cond_0

    .line 50
    iget-object v1, p0, Lcom/google/obf/ce;->j:Lcom/google/obf/ce$a;

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move-wide v6, p5

    invoke-virtual/range {v1 .. v7}, Lcom/google/obf/ce$a;->a(JIIJ)V

    .line 54
    :goto_0
    iget-object v0, p0, Lcom/google/obf/ce;->h:Lcom/google/obf/ch;

    invoke-virtual {v0, p4}, Lcom/google/obf/ch;->a(I)V

    .line 55
    iget-object v0, p0, Lcom/google/obf/ce;->i:Lcom/google/obf/ch;

    invoke-virtual {v0, p4}, Lcom/google/obf/ch;->a(I)V

    .line 56
    return-void

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/google/obf/ce;->e:Lcom/google/obf/ch;

    invoke-virtual {v0, p4}, Lcom/google/obf/ch;->a(I)V

    .line 52
    iget-object v0, p0, Lcom/google/obf/ce;->f:Lcom/google/obf/ch;

    invoke-virtual {v0, p4}, Lcom/google/obf/ch;->a(I)V

    .line 53
    iget-object v0, p0, Lcom/google/obf/ce;->g:Lcom/google/obf/ch;

    invoke-virtual {v0, p4}, Lcom/google/obf/ch;->a(I)V

    goto :goto_0
.end method

.method private static a(Lcom/google/obf/dv;)V
    .locals 7

    .prologue
    const/4 v1, 0x3

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 165
    move v5, v3

    :goto_0
    const/4 v0, 0x4

    if-ge v5, v0, :cond_5

    move v4, v3

    .line 166
    :goto_1
    const/4 v0, 0x6

    if-ge v4, v0, :cond_4

    .line 167
    invoke-virtual {p0}, Lcom/google/obf/dv;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 168
    invoke-virtual {p0}, Lcom/google/obf/dv;->d()I

    .line 175
    :cond_0
    if-ne v5, v1, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    move v4, v0

    goto :goto_1

    .line 169
    :cond_1
    const/16 v0, 0x40

    shl-int/lit8 v6, v5, 0x1

    add-int/lit8 v6, v6, 0x4

    shl-int v6, v2, v6

    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 170
    if-le v5, v2, :cond_2

    .line 171
    invoke-virtual {p0}, Lcom/google/obf/dv;->e()I

    :cond_2
    move v0, v3

    .line 172
    :goto_3
    if-ge v0, v6, :cond_0

    .line 173
    invoke-virtual {p0}, Lcom/google/obf/dv;->e()I

    .line 174
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    move v0, v2

    .line 175
    goto :goto_2

    .line 176
    :cond_4
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    .line 177
    :cond_5
    return-void
.end method

.method private a([BII)V
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/google/obf/ce;->b:Z

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/google/obf/ce;->j:Lcom/google/obf/ce$a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/obf/ce$a;->a([BII)V

    .line 62
    :goto_0
    iget-object v0, p0, Lcom/google/obf/ce;->h:Lcom/google/obf/ch;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/obf/ch;->a([BII)V

    .line 63
    iget-object v0, p0, Lcom/google/obf/ce;->i:Lcom/google/obf/ch;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/obf/ch;->a([BII)V

    .line 64
    return-void

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/google/obf/ce;->e:Lcom/google/obf/ch;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/obf/ch;->a([BII)V

    .line 60
    iget-object v0, p0, Lcom/google/obf/ce;->f:Lcom/google/obf/ch;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/obf/ch;->a([BII)V

    .line 61
    iget-object v0, p0, Lcom/google/obf/ce;->g:Lcom/google/obf/ch;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/obf/ch;->a([BII)V

    goto :goto_0
.end method

.method private b(JIIJ)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    .line 65
    iget-boolean v0, p0, Lcom/google/obf/ce;->b:Z

    if-eqz v0, :cond_3

    .line 66
    iget-object v0, p0, Lcom/google/obf/ce;->j:Lcom/google/obf/ce$a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/obf/ce$a;->a(JI)V

    .line 73
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/obf/ce;->h:Lcom/google/obf/ch;

    invoke-virtual {v0, p4}, Lcom/google/obf/ch;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    iget-object v0, p0, Lcom/google/obf/ce;->h:Lcom/google/obf/ch;

    iget-object v0, v0, Lcom/google/obf/ch;->a:[B

    iget-object v1, p0, Lcom/google/obf/ce;->h:Lcom/google/obf/ch;

    iget v1, v1, Lcom/google/obf/ch;->b:I

    invoke-static {v0, v1}, Lcom/google/obf/du;->a([BI)I

    move-result v0

    .line 75
    iget-object v1, p0, Lcom/google/obf/ce;->m:Lcom/google/obf/dw;

    iget-object v2, p0, Lcom/google/obf/ce;->h:Lcom/google/obf/ch;

    iget-object v2, v2, Lcom/google/obf/ch;->a:[B

    invoke-virtual {v1, v2, v0}, Lcom/google/obf/dw;->a([BI)V

    .line 76
    iget-object v0, p0, Lcom/google/obf/ce;->m:Lcom/google/obf/dw;

    invoke-virtual {v0, v4}, Lcom/google/obf/dw;->d(I)V

    .line 77
    iget-object v0, p0, Lcom/google/obf/ce;->c:Lcom/google/obf/ck;

    iget-object v1, p0, Lcom/google/obf/ce;->m:Lcom/google/obf/dw;

    invoke-virtual {v0, p5, p6, v1}, Lcom/google/obf/ck;->a(JLcom/google/obf/dw;)V

    .line 78
    :cond_1
    iget-object v0, p0, Lcom/google/obf/ce;->i:Lcom/google/obf/ch;

    invoke-virtual {v0, p4}, Lcom/google/obf/ch;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 79
    iget-object v0, p0, Lcom/google/obf/ce;->i:Lcom/google/obf/ch;

    iget-object v0, v0, Lcom/google/obf/ch;->a:[B

    iget-object v1, p0, Lcom/google/obf/ce;->i:Lcom/google/obf/ch;

    iget v1, v1, Lcom/google/obf/ch;->b:I

    invoke-static {v0, v1}, Lcom/google/obf/du;->a([BI)I

    move-result v0

    .line 80
    iget-object v1, p0, Lcom/google/obf/ce;->m:Lcom/google/obf/dw;

    iget-object v2, p0, Lcom/google/obf/ce;->i:Lcom/google/obf/ch;

    iget-object v2, v2, Lcom/google/obf/ch;->a:[B

    invoke-virtual {v1, v2, v0}, Lcom/google/obf/dw;->a([BI)V

    .line 81
    iget-object v0, p0, Lcom/google/obf/ce;->m:Lcom/google/obf/dw;

    invoke-virtual {v0, v4}, Lcom/google/obf/dw;->d(I)V

    .line 82
    iget-object v0, p0, Lcom/google/obf/ce;->c:Lcom/google/obf/ck;

    iget-object v1, p0, Lcom/google/obf/ce;->m:Lcom/google/obf/dw;

    invoke-virtual {v0, p5, p6, v1}, Lcom/google/obf/ck;->a(JLcom/google/obf/dw;)V

    .line 83
    :cond_2
    return-void

    .line 67
    :cond_3
    iget-object v0, p0, Lcom/google/obf/ce;->e:Lcom/google/obf/ch;

    invoke-virtual {v0, p4}, Lcom/google/obf/ch;->b(I)Z

    .line 68
    iget-object v0, p0, Lcom/google/obf/ce;->f:Lcom/google/obf/ch;

    invoke-virtual {v0, p4}, Lcom/google/obf/ch;->b(I)Z

    .line 69
    iget-object v0, p0, Lcom/google/obf/ce;->g:Lcom/google/obf/ch;

    invoke-virtual {v0, p4}, Lcom/google/obf/ch;->b(I)Z

    .line 70
    iget-object v0, p0, Lcom/google/obf/ce;->e:Lcom/google/obf/ch;

    invoke-virtual {v0}, Lcom/google/obf/ch;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/ce;->f:Lcom/google/obf/ch;

    invoke-virtual {v0}, Lcom/google/obf/ch;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/ce;->g:Lcom/google/obf/ch;

    invoke-virtual {v0}, Lcom/google/obf/ch;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/google/obf/ce;->a:Lcom/google/obf/ar;

    iget-object v1, p0, Lcom/google/obf/ce;->e:Lcom/google/obf/ch;

    iget-object v2, p0, Lcom/google/obf/ce;->f:Lcom/google/obf/ch;

    iget-object v3, p0, Lcom/google/obf/ce;->g:Lcom/google/obf/ch;

    invoke-static {v1, v2, v3}, Lcom/google/obf/ce;->a(Lcom/google/obf/ch;Lcom/google/obf/ch;Lcom/google/obf/ch;)Lcom/google/obf/q;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/obf/ar;->a(Lcom/google/obf/q;)V

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/ce;->b:Z

    goto/16 :goto_0
.end method

.method private static b(Lcom/google/obf/dv;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 178
    invoke-virtual {p0}, Lcom/google/obf/dv;->d()I

    move-result v5

    move v4, v2

    move v0, v2

    move v1, v2

    .line 183
    :goto_0
    if-ge v4, v5, :cond_4

    .line 184
    if-eqz v4, :cond_5

    .line 185
    invoke-virtual {p0}, Lcom/google/obf/dv;->b()Z

    move-result v1

    move v3, v1

    .line 186
    :goto_1
    if-eqz v3, :cond_1

    .line 187
    invoke-virtual {p0, v8}, Lcom/google/obf/dv;->b(I)V

    .line 188
    invoke-virtual {p0}, Lcom/google/obf/dv;->d()I

    move v1, v2

    .line 189
    :goto_2
    if-gt v1, v0, :cond_3

    .line 190
    invoke-virtual {p0}, Lcom/google/obf/dv;->b()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 191
    invoke-virtual {p0, v8}, Lcom/google/obf/dv;->b(I)V

    .line 192
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 193
    :cond_1
    invoke-virtual {p0}, Lcom/google/obf/dv;->d()I

    move-result v6

    .line 194
    invoke-virtual {p0}, Lcom/google/obf/dv;->d()I

    move-result v7

    .line 195
    add-int v0, v6, v7

    move v1, v2

    .line 196
    :goto_3
    if-ge v1, v6, :cond_2

    .line 197
    invoke-virtual {p0}, Lcom/google/obf/dv;->d()I

    .line 198
    invoke-virtual {p0, v8}, Lcom/google/obf/dv;->b(I)V

    .line 199
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_2
    move v1, v2

    .line 200
    :goto_4
    if-ge v1, v7, :cond_3

    .line 201
    invoke-virtual {p0}, Lcom/google/obf/dv;->d()I

    .line 202
    invoke-virtual {p0, v8}, Lcom/google/obf/dv;->b(I)V

    .line 203
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 204
    :cond_3
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v3

    goto :goto_0

    .line 205
    :cond_4
    return-void

    :cond_5
    move v3, v1

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 12
    iget-object v0, p0, Lcom/google/obf/ce;->d:[Z

    invoke-static {v0}, Lcom/google/obf/du;->a([Z)V

    .line 13
    iget-object v0, p0, Lcom/google/obf/ce;->e:Lcom/google/obf/ch;

    invoke-virtual {v0}, Lcom/google/obf/ch;->a()V

    .line 14
    iget-object v0, p0, Lcom/google/obf/ce;->f:Lcom/google/obf/ch;

    invoke-virtual {v0}, Lcom/google/obf/ch;->a()V

    .line 15
    iget-object v0, p0, Lcom/google/obf/ce;->g:Lcom/google/obf/ch;

    invoke-virtual {v0}, Lcom/google/obf/ch;->a()V

    .line 16
    iget-object v0, p0, Lcom/google/obf/ce;->h:Lcom/google/obf/ch;

    invoke-virtual {v0}, Lcom/google/obf/ch;->a()V

    .line 17
    iget-object v0, p0, Lcom/google/obf/ce;->i:Lcom/google/obf/ch;

    invoke-virtual {v0}, Lcom/google/obf/ch;->a()V

    .line 18
    iget-object v0, p0, Lcom/google/obf/ce;->j:Lcom/google/obf/ce$a;

    invoke-virtual {v0}, Lcom/google/obf/ce$a;->a()V

    .line 19
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/obf/ce;->k:J

    .line 20
    return-void
.end method

.method public a(JZ)V
    .locals 1

    .prologue
    .line 21
    iput-wide p1, p0, Lcom/google/obf/ce;->l:J

    .line 22
    return-void
.end method

.method public a(Lcom/google/obf/dw;)V
    .locals 12

    .prologue
    .line 23
    :cond_0
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v0

    if-lez v0, :cond_1

    .line 24
    invoke-virtual {p1}, Lcom/google/obf/dw;->d()I

    move-result v0

    .line 25
    invoke-virtual {p1}, Lcom/google/obf/dw;->c()I

    move-result v8

    .line 26
    iget-object v9, p1, Lcom/google/obf/dw;->a:[B

    .line 27
    iget-wide v2, p0, Lcom/google/obf/ce;->k:J

    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v1

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/obf/ce;->k:J

    .line 28
    iget-object v1, p0, Lcom/google/obf/ce;->a:Lcom/google/obf/ar;

    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v2

    invoke-interface {v1, p1, v2}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 29
    :goto_0
    if-ge v0, v8, :cond_0

    .line 30
    iget-object v1, p0, Lcom/google/obf/ce;->d:[Z

    invoke-static {v9, v0, v8, v1}, Lcom/google/obf/du;->a([BII[Z)I

    move-result v10

    .line 31
    if-ne v10, v8, :cond_2

    .line 32
    invoke-direct {p0, v9, v0, v8}, Lcom/google/obf/ce;->a([BII)V

    .line 47
    :cond_1
    return-void

    .line 34
    :cond_2
    invoke-static {v9, v10}, Lcom/google/obf/du;->c([BI)I

    move-result v11

    .line 35
    sub-int v1, v10, v0

    .line 36
    if-lez v1, :cond_3

    .line 37
    invoke-direct {p0, v9, v0, v10}, Lcom/google/obf/ce;->a([BII)V

    .line 38
    :cond_3
    sub-int v4, v8, v10

    .line 39
    iget-wide v2, p0, Lcom/google/obf/ce;->k:J

    int-to-long v6, v4

    sub-long/2addr v2, v6

    .line 41
    if-gez v1, :cond_4

    neg-int v5, v1

    :goto_1
    iget-wide v6, p0, Lcom/google/obf/ce;->l:J

    move-object v1, p0

    .line 42
    invoke-direct/range {v1 .. v7}, Lcom/google/obf/ce;->b(JIIJ)V

    .line 43
    iget-wide v6, p0, Lcom/google/obf/ce;->l:J

    move-object v1, p0

    move v5, v11

    invoke-direct/range {v1 .. v7}, Lcom/google/obf/ce;->a(JIIJ)V

    .line 44
    add-int/lit8 v0, v10, 0x3

    .line 45
    goto :goto_0

    .line 41
    :cond_4
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public b()V
    .locals 0

    .prologue
    .line 48
    return-void
.end method
