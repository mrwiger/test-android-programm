.class Lru/cn/ad/preloader/AdPreLoader$1;
.super Ljava/lang/Object;
.source "AdPreLoader.java"

# interfaces
.implements Lru/cn/ad/AdapterLoader$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/ad/preloader/AdPreLoader;->loadAdapters(Lru/cn/api/money_miner/replies/AdPlace;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/ad/preloader/AdPreLoader;

.field final synthetic val$entry:Lru/cn/ad/preloader/LoadingEntry;

.field final synthetic val$loader:Lru/cn/ad/AdapterLoader;


# direct methods
.method constructor <init>(Lru/cn/ad/preloader/AdPreLoader;Lru/cn/ad/preloader/LoadingEntry;Lru/cn/ad/AdapterLoader;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/ad/preloader/AdPreLoader;

    .prologue
    .line 155
    iput-object p1, p0, Lru/cn/ad/preloader/AdPreLoader$1;->this$0:Lru/cn/ad/preloader/AdPreLoader;

    iput-object p2, p0, Lru/cn/ad/preloader/AdPreLoader$1;->val$entry:Lru/cn/ad/preloader/LoadingEntry;

    iput-object p3, p0, Lru/cn/ad/preloader/AdPreLoader$1;->val$loader:Lru/cn/ad/AdapterLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method final synthetic lambda$onError$0$AdPreLoader$1(Lru/cn/ad/AdapterLoader;)V
    .locals 1
    .param p1, "loader"    # Lru/cn/ad/AdapterLoader;

    .prologue
    .line 170
    iget-object v0, p0, Lru/cn/ad/preloader/AdPreLoader$1;->this$0:Lru/cn/ad/preloader/AdPreLoader;

    invoke-static {v0, p1}, Lru/cn/ad/preloader/AdPreLoader;->access$300(Lru/cn/ad/preloader/AdPreLoader;Lru/cn/ad/AdapterLoader;)V

    return-void
.end method

.method public onError()V
    .locals 4

    .prologue
    .line 168
    iget-object v0, p0, Lru/cn/ad/preloader/AdPreLoader$1;->this$0:Lru/cn/ad/preloader/AdPreLoader;

    iget-object v1, p0, Lru/cn/ad/preloader/AdPreLoader$1;->val$loader:Lru/cn/ad/AdapterLoader;

    invoke-static {v0, v1}, Lru/cn/ad/preloader/AdPreLoader;->access$100(Lru/cn/ad/preloader/AdPreLoader;Lru/cn/ad/AdapterLoader;)V

    .line 170
    iget-object v0, p0, Lru/cn/ad/preloader/AdPreLoader$1;->this$0:Lru/cn/ad/preloader/AdPreLoader;

    invoke-static {v0}, Lru/cn/ad/preloader/AdPreLoader;->access$200(Lru/cn/ad/preloader/AdPreLoader;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lru/cn/ad/preloader/AdPreLoader$1$$Lambda$0;

    iget-object v2, p0, Lru/cn/ad/preloader/AdPreLoader$1;->val$loader:Lru/cn/ad/AdapterLoader;

    invoke-direct {v1, p0, v2}, Lru/cn/ad/preloader/AdPreLoader$1$$Lambda$0;-><init>(Lru/cn/ad/preloader/AdPreLoader$1;Lru/cn/ad/AdapterLoader;)V

    iget-object v2, p0, Lru/cn/ad/preloader/AdPreLoader$1;->val$entry:Lru/cn/ad/preloader/LoadingEntry;

    iget-wide v2, v2, Lru/cn/ad/preloader/LoadingEntry;->reloadIntervalMs:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 171
    return-void
.end method

.method public onLoaded(Lru/cn/ad/AdAdapter;)V
    .locals 2
    .param p1, "adapter"    # Lru/cn/ad/AdAdapter;

    .prologue
    .line 158
    iget-object v0, p0, Lru/cn/ad/preloader/AdPreLoader$1;->this$0:Lru/cn/ad/preloader/AdPreLoader;

    invoke-static {v0}, Lru/cn/ad/preloader/AdPreLoader;->access$000(Lru/cn/ad/preloader/AdPreLoader;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    iget-object v0, p0, Lru/cn/ad/preloader/AdPreLoader$1;->this$0:Lru/cn/ad/preloader/AdPreLoader;

    invoke-static {v0}, Lru/cn/ad/preloader/AdPreLoader;->access$000(Lru/cn/ad/preloader/AdPreLoader;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    :cond_0
    iget-object v0, p0, Lru/cn/ad/preloader/AdPreLoader$1;->val$entry:Lru/cn/ad/preloader/LoadingEntry;

    invoke-virtual {v0, p1}, Lru/cn/ad/preloader/LoadingEntry;->setAdapter(Lru/cn/ad/AdAdapter;)V

    .line 163
    iget-object v0, p0, Lru/cn/ad/preloader/AdPreLoader$1;->this$0:Lru/cn/ad/preloader/AdPreLoader;

    iget-object v1, p0, Lru/cn/ad/preloader/AdPreLoader$1;->val$loader:Lru/cn/ad/AdapterLoader;

    invoke-static {v0, v1}, Lru/cn/ad/preloader/AdPreLoader;->access$100(Lru/cn/ad/preloader/AdPreLoader;Lru/cn/ad/AdapterLoader;)V

    .line 164
    return-void
.end method
