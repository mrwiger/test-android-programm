.class final Lcom/google/obf/au;
.super Lcom/google/obf/av;
.source "IMASDK"


# direct methods
.method public constructor <init>(Lcom/google/obf/ar;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lcom/google/obf/av;-><init>(Lcom/google/obf/ar;)V

    .line 2
    return-void
.end method

.method private static a(Lcom/google/obf/dw;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 52
    packed-switch p1, :pswitch_data_0

    .line 60
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 53
    :pswitch_1
    invoke-static {p0}, Lcom/google/obf/au;->d(Lcom/google/obf/dw;)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 54
    :pswitch_2
    invoke-static {p0}, Lcom/google/obf/au;->c(Lcom/google/obf/dw;)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 55
    :pswitch_3
    invoke-static {p0}, Lcom/google/obf/au;->e(Lcom/google/obf/dw;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 56
    :pswitch_4
    invoke-static {p0}, Lcom/google/obf/au;->g(Lcom/google/obf/dw;)Ljava/util/HashMap;

    move-result-object v0

    goto :goto_0

    .line 57
    :pswitch_5
    invoke-static {p0}, Lcom/google/obf/au;->h(Lcom/google/obf/dw;)Ljava/util/HashMap;

    move-result-object v0

    goto :goto_0

    .line 58
    :pswitch_6
    invoke-static {p0}, Lcom/google/obf/au;->f(Lcom/google/obf/dw;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    .line 59
    :pswitch_7
    invoke-static {p0}, Lcom/google/obf/au;->i(Lcom/google/obf/dw;)Ljava/util/Date;

    move-result-object v0

    goto :goto_0

    .line 52
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private static b(Lcom/google/obf/dw;)I
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v0

    return v0
.end method

.method private static c(Lcom/google/obf/dw;)Ljava/lang/Boolean;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 20
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Lcom/google/obf/dw;)Ljava/lang/Double;
    .locals 2

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/google/obf/dw;->o()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method private static e(Lcom/google/obf/dw;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/google/obf/dw;->g()I

    move-result v0

    .line 23
    invoke-virtual {p0}, Lcom/google/obf/dw;->d()I

    move-result v1

    .line 24
    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->d(I)V

    .line 25
    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/obf/dw;->a:[B

    invoke-direct {v2, v3, v1, v0}, Ljava/lang/String;-><init>([BII)V

    return-object v2
.end method

.method private static f(Lcom/google/obf/dw;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/dw;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/google/obf/dw;->s()I

    move-result v1

    .line 27
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 28
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 29
    invoke-static {p0}, Lcom/google/obf/au;->b(Lcom/google/obf/dw;)I

    move-result v3

    .line 30
    invoke-static {p0, v3}, Lcom/google/obf/au;->a(Lcom/google/obf/dw;I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 31
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 32
    :cond_0
    return-object v2
.end method

.method private static g(Lcom/google/obf/dw;)Ljava/util/HashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/dw;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 34
    :goto_0
    invoke-static {p0}, Lcom/google/obf/au;->e(Lcom/google/obf/dw;)Ljava/lang/String;

    move-result-object v1

    .line 35
    invoke-static {p0}, Lcom/google/obf/au;->b(Lcom/google/obf/dw;)I

    move-result v2

    .line 36
    const/16 v3, 0x9

    if-ne v2, v3, :cond_0

    .line 40
    return-object v0

    .line 38
    :cond_0
    invoke-static {p0, v2}, Lcom/google/obf/au;->a(Lcom/google/obf/dw;I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private static h(Lcom/google/obf/dw;)Ljava/util/HashMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/dw;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/google/obf/dw;->s()I

    move-result v1

    .line 42
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 43
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 44
    invoke-static {p0}, Lcom/google/obf/au;->e(Lcom/google/obf/dw;)Ljava/lang/String;

    move-result-object v3

    .line 45
    invoke-static {p0}, Lcom/google/obf/au;->b(Lcom/google/obf/dw;)I

    move-result v4

    .line 46
    invoke-static {p0, v4}, Lcom/google/obf/au;->a(Lcom/google/obf/dw;I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 48
    :cond_0
    return-object v2
.end method

.method private static i(Lcom/google/obf/dw;)Ljava/util/Date;
    .locals 4

    .prologue
    .line 49
    new-instance v0, Ljava/util/Date;

    invoke-static {p0}, Lcom/google/obf/au;->d(Lcom/google/obf/dw;)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    double-to-long v2, v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 50
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/google/obf/dw;->d(I)V

    .line 51
    return-object v0
.end method


# virtual methods
.method protected a(Lcom/google/obf/dw;J)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    .line 4
    invoke-static {p1}, Lcom/google/obf/au;->b(Lcom/google/obf/dw;)I

    move-result v0

    .line 5
    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 6
    new-instance v0, Lcom/google/obf/s;

    invoke-direct {v0}, Lcom/google/obf/s;-><init>()V

    throw v0

    .line 7
    :cond_0
    invoke-static {p1}, Lcom/google/obf/au;->e(Lcom/google/obf/dw;)Ljava/lang/String;

    move-result-object v0

    .line 8
    const-string v1, "onMetaData"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 18
    :cond_1
    :goto_0
    return-void

    .line 10
    :cond_2
    invoke-static {p1}, Lcom/google/obf/au;->b(Lcom/google/obf/dw;)I

    move-result v0

    .line 11
    const/16 v1, 0x8

    if-eq v0, v1, :cond_3

    .line 12
    new-instance v0, Lcom/google/obf/s;

    invoke-direct {v0}, Lcom/google/obf/s;-><init>()V

    throw v0

    .line 13
    :cond_3
    invoke-static {p1}, Lcom/google/obf/au;->h(Lcom/google/obf/dw;)Ljava/util/HashMap;

    move-result-object v0

    .line 14
    const-string v1, "duration"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 15
    const-string v1, "duration"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 16
    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_1

    .line 17
    const-wide v2, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v0, v2

    double-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lcom/google/obf/au;->a(J)V

    goto :goto_0
.end method

.method protected a(Lcom/google/obf/dw;)Z
    .locals 1

    .prologue
    .line 3
    const/4 v0, 0x1

    return v0
.end method
