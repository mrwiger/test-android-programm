.class public final Lcom/yandex/metrica/p;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/yandex/metrica/IIdentifierCallback;)V
    .locals 1
    .param p0, "callback"    # Lcom/yandex/metrica/IIdentifierCallback;

    .prologue
    .line 29
    .line 1019
    invoke-static {}, Lcom/yandex/metrica/impl/bp;->b()Lcom/yandex/metrica/impl/bp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/yandex/metrica/impl/bp;->a(Lcom/yandex/metrica/IIdentifierCallback;)V

    .line 30
    return-void
.end method

.method public static cpcwh(Lcom/yandex/metrica/YandexMetricaConfig;Ljava/lang/String;)Lcom/yandex/metrica/YandexMetricaConfig;
    .locals 1
    .param p0, "config"    # Lcom/yandex/metrica/YandexMetricaConfig;
    .param p1, "h"    # Ljava/lang/String;

    .prologue
    .line 74
    invoke-static {p0}, Lcom/yandex/metrica/e;->b(Lcom/yandex/metrica/YandexMetricaConfig;)Lcom/yandex/metrica/e$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/e$a;->d(Ljava/lang/String;)Lcom/yandex/metrica/e$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/e$a;->b()Lcom/yandex/metrica/e;

    move-result-object v0

    return-object v0
.end method

.method public static gbc(Landroid/content/Context;)Ljava/lang/Integer;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    const/4 v4, -0x1

    .line 94
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 2204
    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    .line 2205
    if-eqz v1, :cond_0

    .line 2206
    const-string v2, "level"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 2207
    const-string v3, "scale"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 2208
    if-ltz v2, :cond_0

    if-lez v1, :cond_0

    .line 2209
    int-to-float v0, v2

    int-to-float v1, v1

    div-float/2addr v0, v1

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 94
    :cond_0
    return-object v0
.end method

.method public static gcni(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    .line 1035
    new-instance v0, Lcom/yandex/metrica/impl/interact/CellularNetworkInfo;

    invoke-direct {v0, p0}, Lcom/yandex/metrica/impl/interact/CellularNetworkInfo;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/interact/CellularNetworkInfo;->getCelluralInfo()Ljava/lang/String;

    move-result-object v0

    .line 53
    return-object v0
.end method

.method public static gdi(Landroid/content/Context;)Lcom/yandex/metrica/impl/interact/DeviceInfo;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    .line 1031
    invoke-static {p0}, Lcom/yandex/metrica/impl/interact/DeviceInfo;->getInstance(Landroid/content/Context;)Lcom/yandex/metrica/impl/interact/DeviceInfo;

    move-result-object v0

    .line 49
    return-object v0
.end method

.method public static glkl(Landroid/content/Context;)Landroid/location/Location;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 89
    invoke-static {p0}, Lcom/yandex/metrica/impl/ob/eb;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/eb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/eb;->e()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public static gmsvn(I)Ljava/lang/String;
    .locals 1
    .param p0, "apiLevel"    # I

    .prologue
    .line 69
    invoke-static {p0}, Lcom/yandex/metrica/impl/ar;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static guid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1043
    invoke-static {}, Lcom/yandex/metrica/impl/bp;->b()Lcom/yandex/metrica/impl/bp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/bp;->f()Ljava/lang/String;

    move-result-object v0

    .line 57
    return-object v0
.end method

.method public static iifa()Z
    .locals 1

    .prologue
    .line 41
    invoke-static {}, Lcom/yandex/metrica/impl/bo;->a()Z

    move-result v0

    return v0
.end method

.method public static mpn(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    .line 1047
    invoke-static {p0}, Lcom/yandex/metrica/impl/bf;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 61
    return-object v0
.end method

.method public static pgai()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lcom/yandex/metrica/impl/bo;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static plat()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 37
    invoke-static {}, Lcom/yandex/metrica/impl/bo;->c()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static rce(ILjava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .param p0, "type"    # I
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 65
    .line 1051
    .local p3, "environment":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {}, Lcom/yandex/metrica/impl/bp;->c()Lcom/yandex/metrica/impl/z;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/yandex/metrica/impl/z;->a(ILjava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 66
    return-void
.end method

.method public static rolu(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "registrant"    # Ljava/lang/Object;

    .prologue
    .line 79
    invoke-static {p0}, Lcom/yandex/metrica/impl/ob/eb;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/eb;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/eb;->a(Ljava/lang/Object;)V

    .line 80
    return-void
.end method

.method public static u(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "sdkName"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-static {p0}, Lcom/yandex/metrica/impl/bd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static urolu(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "registrant"    # Ljava/lang/Object;

    .prologue
    .line 84
    invoke-static {p0}, Lcom/yandex/metrica/impl/ob/eb;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/eb;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/eb;->b(Ljava/lang/Object;)V

    .line 85
    return-void
.end method
