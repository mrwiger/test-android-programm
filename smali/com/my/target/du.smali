.class public final Lcom/my/target/du;
.super Lcom/my/target/e;
.source "InterstitialSliderAdResultProcessor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/my/target/e",
        "<",
        "Lcom/my/target/dw;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/my/target/e;-><init>()V

    .line 32
    return-void
.end method

.method public static f()Lcom/my/target/du;
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/my/target/du;

    invoke-direct {v0}, Lcom/my/target/du;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Lcom/my/target/ak;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/ak;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 22
    check-cast p1, Lcom/my/target/dw;

    .line 1037
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1039
    invoke-virtual {p1}, Lcom/my/target/dw;->R()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/g;

    .line 1041
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/g;->getPortraitImages()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 1043
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/g;->getPortraitImages()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/my/target/common/models/ImageData;

    .line 1044
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1045
    invoke-virtual {v0, v1}, Lcom/my/target/core/models/banners/g;->setOptimalPortraitImage(Lcom/my/target/common/models/ImageData;)V

    .line 1048
    :cond_1
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/g;->getLandscapeImages()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1050
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/g;->getLandscapeImages()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/my/target/common/models/ImageData;

    .line 1051
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1052
    invoke-virtual {v0, v1}, Lcom/my/target/core/models/banners/g;->setOptimalLandscapeImage(Lcom/my/target/common/models/ImageData;)V

    goto :goto_0

    .line 1055
    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 1057
    invoke-virtual {p1}, Lcom/my/target/dw;->getCloseIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1059
    invoke-virtual {p1}, Lcom/my/target/dw;->getCloseIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 1060
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1062
    :cond_3
    invoke-static {v2}, Lcom/my/target/ch;->a(Ljava/util/List;)Lcom/my/target/ch;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/my/target/ch;->v(Landroid/content/Context;)V

    .line 1065
    invoke-virtual {p1}, Lcom/my/target/dw;->R()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/g;

    .line 1067
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/g;->getOptimalLandscapeImage()Lcom/my/target/common/models/ImageData;

    move-result-object v2

    .line 1068
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_4

    .line 1072
    :cond_5
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/g;->getOptimalPortraitImage()Lcom/my/target/common/models/ImageData;

    move-result-object v2

    .line 1073
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_4

    .line 1077
    :cond_6
    invoke-virtual {p1, v0}, Lcom/my/target/dw;->d(Lcom/my/target/core/models/banners/g;)V

    goto :goto_1

    .line 1079
    :cond_7
    invoke-virtual {p1}, Lcom/my/target/dw;->getBannersCount()I

    move-result v0

    if-lez v0, :cond_8

    .line 1081
    :goto_2
    return-object p1

    .line 1084
    :cond_8
    const/4 p1, 0x0

    .line 22
    goto :goto_2
.end method
