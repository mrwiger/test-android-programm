.class public Lru/cn/tv/mobile/collections/RubricOptionsController;
.super Ljava/lang/Object;
.source "RubricOptionsController.java"

# interfaces
.implements Lru/cn/tv/mobile/collections/RubricFragmentController;


# instance fields
.field private currentRubric:Lru/cn/api/catalogue/replies/Rubric;

.field private currentRubricOption:Lru/cn/api/catalogue/replies/RubricOption;

.field private final listener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;


# direct methods
.method constructor <init>(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lru/cn/tv/mobile/collections/RubricOptionsController;->listener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    .line 23
    return-void
.end method


# virtual methods
.method public createRubricFragment(I)Landroid/support/v4/app/Fragment;
    .locals 7
    .param p1, "position"    # I

    .prologue
    .line 48
    iget-object v4, p0, Lru/cn/tv/mobile/collections/RubricOptionsController;->currentRubric:Lru/cn/api/catalogue/replies/Rubric;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lru/cn/tv/mobile/collections/RubricOptionsController;->currentRubricOption:Lru/cn/api/catalogue/replies/RubricOption;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lru/cn/tv/mobile/collections/RubricOptionsController;->currentRubricOption:Lru/cn/api/catalogue/replies/RubricOption;

    iget-object v4, v4, Lru/cn/api/catalogue/replies/RubricOption;->values:Ljava/util/List;

    .line 49
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge p1, v4, :cond_0

    .line 51
    iget-object v4, p0, Lru/cn/tv/mobile/collections/RubricOptionsController;->currentRubricOption:Lru/cn/api/catalogue/replies/RubricOption;

    iget-object v4, v4, Lru/cn/api/catalogue/replies/RubricOption;->values:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/catalogue/replies/RubricOptionValue;

    .line 52
    .local v1, "optionValue":Lru/cn/api/catalogue/replies/RubricOptionValue;
    new-instance v2, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v2}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    .line 53
    .local v2, "options":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Ljava/lang/String;>;"
    iget-object v4, p0, Lru/cn/tv/mobile/collections/RubricOptionsController;->currentRubricOption:Lru/cn/api/catalogue/replies/RubricOption;

    iget-wide v4, v4, Lru/cn/api/catalogue/replies/RubricOption;->id:J

    iget-object v6, v1, Lru/cn/api/catalogue/replies/RubricOptionValue;->value:Ljava/lang/String;

    invoke-virtual {v2, v4, v5, v6}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 54
    iget-object v4, p0, Lru/cn/tv/mobile/collections/RubricOptionsController;->currentRubric:Lru/cn/api/catalogue/replies/Rubric;

    iget-wide v4, v4, Lru/cn/api/catalogue/replies/Rubric;->id:J

    invoke-static {v4, v5, v2}, Lru/cn/api/provider/TvContentProviderContract;->rubricItemsUri(JLandroid/support/v4/util/LongSparseArray;)Landroid/net/Uri;

    move-result-object v3

    .line 56
    .local v3, "uri":Landroid/net/Uri;
    invoke-static {v3}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->newInstance(Landroid/net/Uri;)Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    move-result-object v0

    .line 57
    .local v0, "f":Lru/cn/tv/mobile/telecasts/TelecastsListFragment;
    iget-object v4, p0, Lru/cn/tv/mobile/collections/RubricOptionsController;->listener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    invoke-virtual {v0, v4}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->setOnSelectedListener(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)V

    .line 61
    .end local v0    # "f":Lru/cn/tv/mobile/telecasts/TelecastsListFragment;
    .end local v1    # "optionValue":Lru/cn/api/catalogue/replies/RubricOptionValue;
    .end local v2    # "options":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Ljava/lang/String;>;"
    .end local v3    # "uri":Landroid/net/Uri;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTitle(I)Ljava/lang/String;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 75
    iget-object v1, p0, Lru/cn/tv/mobile/collections/RubricOptionsController;->currentRubricOption:Lru/cn/api/catalogue/replies/RubricOption;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lru/cn/tv/mobile/collections/RubricOptionsController;->currentRubricOption:Lru/cn/api/catalogue/replies/RubricOption;

    iget-object v1, v1, Lru/cn/api/catalogue/replies/RubricOption;->values:Ljava/util/List;

    .line 76
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 77
    iget-object v1, p0, Lru/cn/tv/mobile/collections/RubricOptionsController;->currentRubricOption:Lru/cn/api/catalogue/replies/RubricOption;

    iget-object v1, v1, Lru/cn/api/catalogue/replies/RubricOption;->values:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/catalogue/replies/RubricOptionValue;

    .line 78
    .local v0, "optionValue":Lru/cn/api/catalogue/replies/RubricOptionValue;
    iget-object v1, v0, Lru/cn/api/catalogue/replies/RubricOptionValue;->title:Ljava/lang/String;

    .line 81
    .end local v0    # "optionValue":Lru/cn/api/catalogue/replies/RubricOptionValue;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public rubricCount()I
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lru/cn/tv/mobile/collections/RubricOptionsController;->currentRubricOption:Lru/cn/api/catalogue/replies/RubricOption;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lru/cn/tv/mobile/collections/RubricOptionsController;->currentRubricOption:Lru/cn/api/catalogue/replies/RubricOption;

    iget-object v0, v0, Lru/cn/api/catalogue/replies/RubricOption;->values:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 70
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setRubric(Lru/cn/api/catalogue/replies/Rubric;)V
    .locals 5
    .param p1, "rubric"    # Lru/cn/api/catalogue/replies/Rubric;

    .prologue
    .line 26
    iput-object p1, p0, Lru/cn/tv/mobile/collections/RubricOptionsController;->currentRubric:Lru/cn/api/catalogue/replies/Rubric;

    .line 28
    iget-object v2, p0, Lru/cn/tv/mobile/collections/RubricOptionsController;->currentRubric:Lru/cn/api/catalogue/replies/Rubric;

    iget-object v2, v2, Lru/cn/api/catalogue/replies/Rubric;->options:Ljava/util/List;

    if-nez v2, :cond_0

    .line 29
    new-instance v0, Lru/cn/api/catalogue/replies/RubricOption;

    invoke-direct {v0}, Lru/cn/api/catalogue/replies/RubricOption;-><init>()V

    .line 30
    .local v0, "fakeOption":Lru/cn/api/catalogue/replies/RubricOption;
    sget-object v2, Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;->SWITCH:Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

    iput-object v2, v0, Lru/cn/api/catalogue/replies/RubricOption;->type:Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

    .line 31
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lru/cn/api/catalogue/replies/RubricOption;->values:Ljava/util/List;

    .line 32
    iget-object v2, v0, Lru/cn/api/catalogue/replies/RubricOption;->values:Ljava/util/List;

    new-instance v3, Lru/cn/api/catalogue/replies/RubricOptionValue;

    invoke-direct {v3}, Lru/cn/api/catalogue/replies/RubricOptionValue;-><init>()V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    iget-object v2, p0, Lru/cn/tv/mobile/collections/RubricOptionsController;->currentRubric:Lru/cn/api/catalogue/replies/Rubric;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v2, Lru/cn/api/catalogue/replies/Rubric;->options:Ljava/util/List;

    .line 34
    iget-object v2, p0, Lru/cn/tv/mobile/collections/RubricOptionsController;->currentRubric:Lru/cn/api/catalogue/replies/Rubric;

    iget-object v2, v2, Lru/cn/api/catalogue/replies/Rubric;->options:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37
    .end local v0    # "fakeOption":Lru/cn/api/catalogue/replies/RubricOption;
    :cond_0
    iget-object v2, p0, Lru/cn/tv/mobile/collections/RubricOptionsController;->currentRubric:Lru/cn/api/catalogue/replies/Rubric;

    iget-object v2, v2, Lru/cn/api/catalogue/replies/Rubric;->options:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 38
    iget-object v2, p0, Lru/cn/tv/mobile/collections/RubricOptionsController;->currentRubric:Lru/cn/api/catalogue/replies/Rubric;

    iget-object v2, v2, Lru/cn/api/catalogue/replies/Rubric;->options:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/catalogue/replies/RubricOption;

    .line 39
    .local v1, "option":Lru/cn/api/catalogue/replies/RubricOption;
    iget-object v3, v1, Lru/cn/api/catalogue/replies/RubricOption;->type:Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

    sget-object v4, Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;->SWITCH:Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

    if-ne v3, v4, :cond_1

    .line 40
    iput-object v1, p0, Lru/cn/tv/mobile/collections/RubricOptionsController;->currentRubricOption:Lru/cn/api/catalogue/replies/RubricOption;

    .line 45
    .end local v1    # "option":Lru/cn/api/catalogue/replies/RubricOption;
    :cond_2
    return-void
.end method
