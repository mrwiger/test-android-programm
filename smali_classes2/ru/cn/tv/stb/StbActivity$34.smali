.class Lru/cn/tv/stb/StbActivity$34;
.super Ljava/lang/Object;
.source "StbActivity.java"

# interfaces
.implements Lru/cn/domain/PinCode$PinOperationCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/stb/StbActivity;->pinAuthorizedForDisable()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/stb/StbActivity;


# direct methods
.method constructor <init>(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 2408
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity$34;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError()V
    .locals 0

    .prologue
    .line 2420
    return-void
.end method

.method public onSuccess()V
    .locals 4

    .prologue
    .line 2411
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$34;->this$0:Lru/cn/tv/stb/StbActivity;

    const-string v2, "pin_disabled"

    invoke-static {v1, v2}, Lru/cn/domain/Preferences;->getBoolean(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 2412
    .local v0, "isPinDisabled":Z
    iget-object v2, p0, Lru/cn/tv/stb/StbActivity$34;->this$0:Lru/cn/tv/stb/StbActivity;

    const-string v3, "pin_disabled"

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v2, v3, v1}, Lru/cn/domain/Preferences;->setBoolean(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 2414
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$34;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$7000(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/settings/SettingFragment;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/tv/stb/settings/SettingFragment;->reloadData()V

    .line 2415
    return-void

    .line 2412
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
