.class final Lcom/google/obf/fj$d;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Ljava/util/Map$Entry;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/fj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Map$Entry",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field a:Lcom/google/obf/fj$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field b:Lcom/google/obf/fj$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field c:Lcom/google/obf/fj$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field d:Lcom/google/obf/fj$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field e:Lcom/google/obf/fj$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final f:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TK;"
        }
    .end annotation
.end field

.field g:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field

.field h:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/fj$d;->f:Ljava/lang/Object;

    .line 3
    iput-object p0, p0, Lcom/google/obf/fj$d;->e:Lcom/google/obf/fj$d;

    iput-object p0, p0, Lcom/google/obf/fj$d;->d:Lcom/google/obf/fj$d;

    .line 4
    return-void
.end method

.method constructor <init>(Lcom/google/obf/fj$d;Ljava/lang/Object;Lcom/google/obf/fj$d;Lcom/google/obf/fj$d;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;TK;",
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;",
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    iput-object p1, p0, Lcom/google/obf/fj$d;->a:Lcom/google/obf/fj$d;

    .line 7
    iput-object p2, p0, Lcom/google/obf/fj$d;->f:Ljava/lang/Object;

    .line 8
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/obf/fj$d;->h:I

    .line 9
    iput-object p3, p0, Lcom/google/obf/fj$d;->d:Lcom/google/obf/fj$d;

    .line 10
    iput-object p4, p0, Lcom/google/obf/fj$d;->e:Lcom/google/obf/fj$d;

    .line 11
    iput-object p0, p4, Lcom/google/obf/fj$d;->d:Lcom/google/obf/fj$d;

    .line 12
    iput-object p0, p3, Lcom/google/obf/fj$d;->e:Lcom/google/obf/fj$d;

    .line 13
    return-void
.end method


# virtual methods
.method public a()Lcom/google/obf/fj$d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 27
    .line 28
    iget-object v0, p0, Lcom/google/obf/fj$d;->b:Lcom/google/obf/fj$d;

    .line 29
    :goto_0
    if-eqz v0, :cond_0

    .line 31
    iget-object v1, v0, Lcom/google/obf/fj$d;->b:Lcom/google/obf/fj$d;

    move-object p0, v0

    move-object v0, v1

    goto :goto_0

    .line 32
    :cond_0
    return-object p0
.end method

.method public b()Lcom/google/obf/fj$d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 33
    .line 34
    iget-object v0, p0, Lcom/google/obf/fj$d;->c:Lcom/google/obf/fj$d;

    .line 35
    :goto_0
    if-eqz v0, :cond_0

    .line 37
    iget-object v1, v0, Lcom/google/obf/fj$d;->c:Lcom/google/obf/fj$d;

    move-object p0, v0

    move-object v0, v1

    goto :goto_0

    .line 38
    :cond_0
    return-object p0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 19
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-eqz v1, :cond_0

    .line 20
    check-cast p1, Ljava/util/Map$Entry;

    .line 21
    iget-object v1, p0, Lcom/google/obf/fj$d;->f:Ljava/lang/Object;

    if-nez v1, :cond_1

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    iget-object v1, p0, Lcom/google/obf/fj$d;->g:Ljava/lang/Object;

    if-nez v1, :cond_2

    .line 22
    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_1
    const/4 v0, 0x1

    .line 23
    :cond_0
    return v0

    .line 21
    :cond_1
    iget-object v1, p0, Lcom/google/obf/fj$d;->f:Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 22
    :cond_2
    iget-object v1, p0, Lcom/google/obf/fj$d;->g:Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1
.end method

.method public getKey()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/obf/fj$d;->f:Ljava/lang/Object;

    return-object v0
.end method

.method public getValue()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 15
    iget-object v0, p0, Lcom/google/obf/fj$d;->g:Ljava/lang/Object;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 24
    iget-object v0, p0, Lcom/google/obf/fj$d;->f:Ljava/lang/Object;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/obf/fj$d;->g:Ljava/lang/Object;

    if-nez v2, :cond_1

    .line 25
    :goto_1
    xor-int/2addr v0, v1

    return v0

    .line 24
    :cond_0
    iget-object v0, p0, Lcom/google/obf/fj$d;->f:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/obf/fj$d;->g:Ljava/lang/Object;

    .line 25
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)TV;"
        }
    .end annotation

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/obf/fj$d;->g:Ljava/lang/Object;

    .line 17
    iput-object p1, p0, Lcom/google/obf/fj$d;->g:Ljava/lang/Object;

    .line 18
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 26
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/obf/fj$d;->f:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/obf/fj$d;->g:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
