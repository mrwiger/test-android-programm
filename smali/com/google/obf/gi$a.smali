.class public Lcom/google/obf/gi$a;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/gi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/obf/gi;


# direct methods
.method protected constructor <init>(Lcom/google/obf/gi;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/obf/gi$a;->a:Lcom/google/obf/gi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 24
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/obf/gi$a;->a:Lcom/google/obf/gi;

    invoke-static {v0}, Lcom/google/obf/gi;->a(Lcom/google/obf/gi;)Landroid/app/Activity;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 19
    iget-object v0, p0, Lcom/google/obf/gi$a;->a:Lcom/google/obf/gi;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/obf/gi;->a(Lcom/google/obf/gi;Landroid/app/Activity;)Landroid/app/Activity;

    .line 20
    iget-object v0, p0, Lcom/google/obf/gi$a;->a:Lcom/google/obf/gi;

    invoke-static {v0}, Lcom/google/obf/gi;->d(Lcom/google/obf/gi;)Landroid/app/Application;

    move-result-object v0

    .line 21
    if-eqz v0, :cond_0

    .line 22
    iget-object v1, p0, Lcom/google/obf/gi$a;->a:Lcom/google/obf/gi;

    invoke-static {v1}, Lcom/google/obf/gi;->e(Lcom/google/obf/gi;)Lcom/google/obf/gi$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 23
    :cond_0
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 6

    .prologue
    .line 11
    iget-object v0, p0, Lcom/google/obf/gi$a;->a:Lcom/google/obf/gi;

    invoke-static {v0}, Lcom/google/obf/gi;->a(Lcom/google/obf/gi;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/gi$a;->a:Lcom/google/obf/gi;

    invoke-static {v0}, Lcom/google/obf/gi;->a(Lcom/google/obf/gi;)Landroid/app/Activity;

    move-result-object v0

    if-ne v0, p1, :cond_1

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/google/obf/gi$a;->a:Lcom/google/obf/gi;

    invoke-static {v0, p1}, Lcom/google/obf/gi;->a(Lcom/google/obf/gi;Landroid/app/Activity;)Landroid/app/Activity;

    .line 13
    iget-object v0, p0, Lcom/google/obf/gi$a;->a:Lcom/google/obf/gi;

    const-string v1, ""

    const-string v2, ""

    const-string v3, ""

    const-string v4, "inactive"

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/obf/gi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/ads/interactivemedia/v3/impl/data/a;

    move-result-object v0

    .line 14
    iget-object v1, p0, Lcom/google/obf/gi$a;->a:Lcom/google/obf/gi;

    invoke-static {v1}, Lcom/google/obf/gi;->c(Lcom/google/obf/gi;)Lcom/google/obf/hj;

    move-result-object v1

    new-instance v2, Lcom/google/obf/hi;

    sget-object v3, Lcom/google/obf/hi$b;->activityMonitor:Lcom/google/obf/hi$b;

    sget-object v4, Lcom/google/obf/hi$c;->appStateChanged:Lcom/google/obf/hi$c;

    iget-object v5, p0, Lcom/google/obf/gi$a;->a:Lcom/google/obf/gi;

    .line 15
    invoke-static {v5}, Lcom/google/obf/gi;->b(Lcom/google/obf/gi;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/google/obf/hi;-><init>(Lcom/google/obf/hi$b;Lcom/google/obf/hi$c;Ljava/lang/String;Ljava/lang/Object;)V

    .line 16
    invoke-virtual {v1, v2}, Lcom/google/obf/hj;->b(Lcom/google/obf/hi;)V

    .line 17
    :cond_1
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 6

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/obf/gi$a;->a:Lcom/google/obf/gi;

    invoke-static {v0}, Lcom/google/obf/gi;->a(Lcom/google/obf/gi;)Landroid/app/Activity;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 6
    iget-object v0, p0, Lcom/google/obf/gi$a;->a:Lcom/google/obf/gi;

    const-string v1, ""

    const-string v2, ""

    const-string v3, ""

    const-string v4, "active"

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/obf/gi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/ads/interactivemedia/v3/impl/data/a;

    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/google/obf/gi$a;->a:Lcom/google/obf/gi;

    invoke-static {v1}, Lcom/google/obf/gi;->c(Lcom/google/obf/gi;)Lcom/google/obf/hj;

    move-result-object v1

    new-instance v2, Lcom/google/obf/hi;

    sget-object v3, Lcom/google/obf/hi$b;->activityMonitor:Lcom/google/obf/hi$b;

    sget-object v4, Lcom/google/obf/hi$c;->appStateChanged:Lcom/google/obf/hi$c;

    iget-object v5, p0, Lcom/google/obf/gi$a;->a:Lcom/google/obf/gi;

    .line 8
    invoke-static {v5}, Lcom/google/obf/gi;->b(Lcom/google/obf/gi;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/google/obf/hi;-><init>(Lcom/google/obf/hi$b;Lcom/google/obf/hi$c;Ljava/lang/String;Ljava/lang/Object;)V

    .line 9
    invoke-virtual {v1, v2}, Lcom/google/obf/hj;->b(Lcom/google/obf/hi;)V

    .line 10
    :cond_0
    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 4
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 3
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 2
    return-void
.end method
