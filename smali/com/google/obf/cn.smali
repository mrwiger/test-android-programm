.class final Lcom/google/obf/cn;
.super Ljava/lang/Object;
.source "IMASDK"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private g:J

.field private h:J


# direct methods
.method public constructor <init>(IIIIII)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lcom/google/obf/cn;->a:I

    .line 3
    iput p2, p0, Lcom/google/obf/cn;->b:I

    .line 4
    iput p3, p0, Lcom/google/obf/cn;->c:I

    .line 5
    iput p4, p0, Lcom/google/obf/cn;->d:I

    .line 6
    iput p5, p0, Lcom/google/obf/cn;->e:I

    .line 7
    iput p6, p0, Lcom/google/obf/cn;->f:I

    .line 8
    return-void
.end method


# virtual methods
.method public a()J
    .locals 4

    .prologue
    .line 9
    iget-wide v0, p0, Lcom/google/obf/cn;->h:J

    iget v2, p0, Lcom/google/obf/cn;->d:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    .line 10
    const-wide/32 v2, 0xf4240

    mul-long/2addr v0, v2

    iget v2, p0, Lcom/google/obf/cn;->b:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public a(J)J
    .locals 5

    .prologue
    .line 15
    iget v0, p0, Lcom/google/obf/cn;->c:I

    int-to-long v0, v0

    mul-long/2addr v0, p1

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    .line 16
    iget v2, p0, Lcom/google/obf/cn;->d:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    iget v2, p0, Lcom/google/obf/cn;->d:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/obf/cn;->g:J

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public a(JJ)V
    .locals 1

    .prologue
    .line 19
    iput-wide p1, p0, Lcom/google/obf/cn;->g:J

    .line 20
    iput-wide p3, p0, Lcom/google/obf/cn;->h:J

    .line 21
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 11
    iget v0, p0, Lcom/google/obf/cn;->d:I

    return v0
.end method

.method public b(J)J
    .locals 5

    .prologue
    .line 17
    const-wide/32 v0, 0xf4240

    mul-long/2addr v0, p1

    iget v2, p0, Lcom/google/obf/cn;->c:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public c()I
    .locals 2

    .prologue
    .line 12
    iget v0, p0, Lcom/google/obf/cn;->b:I

    iget v1, p0, Lcom/google/obf/cn;->e:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/google/obf/cn;->a:I

    mul-int/2addr v0, v1

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/google/obf/cn;->b:I

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 14
    iget v0, p0, Lcom/google/obf/cn;->a:I

    return v0
.end method

.method public f()Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 18
    iget-wide v0, p0, Lcom/google/obf/cn;->g:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/obf/cn;->h:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/google/obf/cn;->f:I

    return v0
.end method
