.class Lru/cn/draggableview/Vector2f;
.super Ljava/lang/Object;
.source "Vector2f.java"


# instance fields
.field x:F

.field y:F


# direct methods
.method public constructor <init>(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput p1, p0, Lru/cn/draggableview/Vector2f;->x:F

    .line 9
    iput p2, p0, Lru/cn/draggableview/Vector2f;->y:F

    .line 10
    return-void
.end method


# virtual methods
.method public abs()F
    .locals 3

    .prologue
    .line 17
    iget v0, p0, Lru/cn/draggableview/Vector2f;->x:F

    iget v1, p0, Lru/cn/draggableview/Vector2f;->x:F

    mul-float/2addr v0, v1

    iget v1, p0, Lru/cn/draggableview/Vector2f;->y:F

    iget v2, p0, Lru/cn/draggableview/Vector2f;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public projection(Lru/cn/draggableview/Vector2f;)F
    .locals 2
    .param p1, "v"    # Lru/cn/draggableview/Vector2f;

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lru/cn/draggableview/Vector2f;->scalar(Lru/cn/draggableview/Vector2f;)F

    move-result v0

    invoke-virtual {p1}, Lru/cn/draggableview/Vector2f;->abs()F

    move-result v1

    div-float/2addr v0, v1

    return v0
.end method

.method public scalar(Lru/cn/draggableview/Vector2f;)F
    .locals 3
    .param p1, "v"    # Lru/cn/draggableview/Vector2f;

    .prologue
    .line 13
    iget v0, p0, Lru/cn/draggableview/Vector2f;->x:F

    iget v1, p1, Lru/cn/draggableview/Vector2f;->x:F

    mul-float/2addr v0, v1

    iget v1, p0, Lru/cn/draggableview/Vector2f;->y:F

    iget v2, p1, Lru/cn/draggableview/Vector2f;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method
