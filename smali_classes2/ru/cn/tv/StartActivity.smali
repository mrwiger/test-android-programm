.class public Lru/cn/tv/StartActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "StartActivity.java"


# instance fields
.field private guidManager:Lru/cn/guides/GuidManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    .line 26
    new-instance v0, Lru/cn/guides/GuidManager;

    invoke-direct {v0}, Lru/cn/guides/GuidManager;-><init>()V

    iput-object v0, p0, Lru/cn/tv/StartActivity;->guidManager:Lru/cn/guides/GuidManager;

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/StartActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/StartActivity;

    .prologue
    .line 25
    invoke-direct {p0}, Lru/cn/tv/StartActivity;->init()V

    return-void
.end method

.method private init()V
    .locals 17

    .prologue
    .line 40
    const-class v12, Lru/cn/tv/mobile/NewActivity;

    .line 41
    .local v12, "targetActivity":Ljava/lang/Class;
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v14

    if-eqz v14, :cond_4

    .line 42
    const-class v12, Lru/cn/tv/stb/StbActivity;

    .line 47
    :goto_0
    new-instance v6, Landroid/content/Intent;

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v12}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 49
    .local v6, "i":Landroid/content/Intent;
    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lru/cn/tv/StartActivity;->overridePendingTransition(II)V

    .line 50
    invoke-virtual/range {p0 .. p0}, Lru/cn/tv/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v11

    .line 51
    .local v11, "startIntent":Landroid/content/Intent;
    if-eqz v11, :cond_0

    .line 52
    invoke-virtual {v11}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 53
    .local v5, "extras":Landroid/os/Bundle;
    if-eqz v5, :cond_0

    .line 54
    invoke-virtual {v6, v5}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 58
    .end local v5    # "extras":Landroid/os/Bundle;
    :cond_0
    const-string v14, "config_launcher"

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lru/cn/tv/StartActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    .line 59
    .local v8, "pref":Landroid/content/SharedPreferences;
    const-string v1, "api_url"

    .line 61
    .local v1, "apiUrlNameInFile":Ljava/lang/String;
    const/4 v4, 0x0

    .line 64
    .local v4, "entryPoint":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lru/cn/tv/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 65
    .restart local v5    # "extras":Landroid/os/Bundle;
    if-eqz v5, :cond_1

    .line 66
    const-string v14, "bundleFromLauncher"

    invoke-virtual {v5, v14}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 67
    .local v2, "bundle":Landroid/os/Bundle;
    if-eqz v2, :cond_1

    .line 68
    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 69
    .local v3, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v14, "api_url"

    invoke-virtual {v2, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 70
    invoke-interface {v3, v1, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 71
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 76
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v3    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    if-nez v4, :cond_2

    .line 77
    const/4 v14, 0x0

    invoke-interface {v8, v1, v14}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 80
    :cond_2
    if-eqz v4, :cond_3

    .line 81
    sput-object v4, Lru/cn/utils/customization/Config;->API_URL:Ljava/lang/String;

    .line 83
    new-instance v14, Landroid/net/Uri$Builder;

    invoke-direct {v14}, Landroid/net/Uri$Builder;-><init>()V

    const-string v15, "content"

    .line 84
    invoke-virtual {v14, v15}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v14

    const-string v15, "ru.cn.api.tv"

    .line 85
    invoke-virtual {v14, v15}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v14

    const-string v15, "clear_cache"

    .line 86
    invoke-virtual {v14, v15}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v14

    .line 87
    invoke-virtual {v14}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v9

    .line 88
    .local v9, "reloadUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lru/cn/tv/StartActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v14, v9, v15, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 91
    .end local v9    # "reloadUri":Landroid/net/Uri;
    :cond_3
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v14

    if-eqz v14, :cond_5

    .line 92
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lru/cn/tv/StartActivity;->startActivity(Landroid/content/Intent;)V

    .line 93
    invoke-virtual/range {p0 .. p0}, Lru/cn/tv/StartActivity;->finish()V

    .line 114
    :goto_1
    return-void

    .line 44
    .end local v1    # "apiUrlNameInFile":Ljava/lang/String;
    .end local v4    # "entryPoint":Ljava/lang/String;
    .end local v5    # "extras":Landroid/os/Bundle;
    .end local v6    # "i":Landroid/content/Intent;
    .end local v8    # "pref":Landroid/content/SharedPreferences;
    .end local v11    # "startIntent":Landroid/content/Intent;
    :cond_4
    invoke-static/range {p0 .. p0}, Lru/cn/tv/rating/Rating;->saveStartCount(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 95
    .restart local v1    # "apiUrlNameInFile":Ljava/lang/String;
    .restart local v4    # "entryPoint":Ljava/lang/String;
    .restart local v5    # "extras":Landroid/os/Bundle;
    .restart local v6    # "i":Landroid/content/Intent;
    .restart local v8    # "pref":Landroid/content/SharedPreferences;
    .restart local v11    # "startIntent":Landroid/content/Intent;
    :cond_5
    new-instance v13, Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 96
    .local v13, "v":Landroid/widget/FrameLayout;
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lru/cn/tv/StartActivity;->setContentView(Landroid/view/View;)V

    .line 97
    const/4 v14, -0x1

    invoke-virtual {v13, v14}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 99
    const/4 v10, 0x7

    .line 100
    .local v10, "requestedOrientation":I
    invoke-static/range {p0 .. p0}, Lru/cn/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 101
    const/4 v10, 0x6

    .line 103
    :cond_6
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lru/cn/tv/StartActivity;->setRequestedOrientation(I)V

    .line 105
    move-object v7, v6

    .line 106
    .local v7, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v14, v0, Lru/cn/tv/StartActivity;->guidManager:Lru/cn/guides/GuidManager;

    new-instance v15, Lru/cn/tv/StartActivity$1;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v7}, Lru/cn/tv/StartActivity$1;-><init>(Lru/cn/tv/StartActivity;Landroid/content/Intent;)V

    move-object/from16 v0, p0

    invoke-virtual {v14, v0, v13, v15}, Lru/cn/guides/GuidManager;->Init(Landroid/content/Context;Landroid/view/ViewGroup;Lru/cn/guides/GuidManager$GuidManagerListener;)V

    goto :goto_1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    invoke-static {p0}, Lru/cn/utils/PermissionUtils;->isGranted(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    invoke-direct {p0}, Lru/cn/tv/StartActivity;->init()V

    .line 37
    :goto_0
    return-void

    .line 34
    :cond_0
    const v0, 0x7f0c00b2

    invoke-virtual {p0, v0}, Lru/cn/tv/StartActivity;->setContentView(I)V

    .line 35
    invoke-static {p0}, Lru/cn/utils/PermissionUtils;->requestPermissions(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "permissions"    # [Ljava/lang/String;
    .param p3, "grantResults"    # [I

    .prologue
    const/4 v3, 0x0

    .line 118
    const/16 v2, 0x1674

    if-ne p1, v2, :cond_2

    .line 119
    const/4 v0, 0x1

    .line 120
    .local v0, "granted":Z
    array-length v4, p3

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_1

    aget v1, p3, v2

    .line 121
    .local v1, "r":I
    if-eqz v1, :cond_0

    .line 122
    const/4 v0, 0x0

    .line 120
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 126
    .end local v1    # "r":I
    :cond_1
    if-eqz v0, :cond_3

    .line 127
    invoke-direct {p0}, Lru/cn/tv/StartActivity;->init()V

    .line 144
    .end local v0    # "granted":Z
    :cond_2
    :goto_1
    return-void

    .line 129
    .restart local v0    # "granted":Z
    :cond_3
    new-instance v2, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v4, "\u0412\u043d\u0438\u043c\u0430\u043d\u0438\u0435!"

    .line 130
    invoke-virtual {v2, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v2

    const-string v4, "\u0414\u043b\u044f \u043a\u043e\u0440\u0440\u0435\u043a\u0442\u043d\u043e\u0439 \u0440\u0430\u0431\u043e\u0442\u044b \u043d\u0435\u043e\u0431\u0445\u043e\u0434\u0438\u043c\u043e \u043f\u0440\u0435\u0434\u043e\u0441\u0442\u0430\u0432\u0438\u0442\u044c \u0440\u0430\u0437\u0440\u0435\u0448\u0435\u043d\u0438\u044f.\n\n\u0415\u0441\u043b\u0438 \u0432\u044b \u043e\u0442\u043c\u0435\u0442\u0438\u043b\u0438 \"\u043d\u0435 \u043f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0441\u043d\u043e\u0432\u0430\", \u0432\u044b \u043c\u043e\u0436\u0435\u0442\u0435 \u043f\u0440\u0435\u0434\u043e\u0441\u0442\u0430\u0432\u0438\u0442\u044c \u0440\u0430\u0437\u0440\u0435\u0448\u0435\u043d\u0438\u044f \u0432 \u043d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0430\u0445 \u0432\u0430\u0448\u0435\u0433\u043e \u0443\u0441\u0442\u0440\u043e\u0439\u0441\u0442\u0432\u0430 (\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438 - \u041f\u0440\u0438\u043b\u043e\u0436\u0435\u043d\u0438\u044f - Peers.TV - \u0420\u0430\u0437\u0440\u0435\u0448\u0435\u043d\u0438\u044f)"

    .line 131
    invoke-virtual {v2, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v2

    const-string v4, "\u041e\u043a"

    new-instance v5, Lru/cn/tv/StartActivity$2;

    invoke-direct {v5, p0}, Lru/cn/tv/StartActivity$2;-><init>(Lru/cn/tv/StartActivity;)V

    .line 133
    invoke-virtual {v2, v4, v5}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v2

    .line 140
    invoke-virtual {v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v2

    .line 141
    invoke-virtual {v2}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    goto :goto_1
.end method
