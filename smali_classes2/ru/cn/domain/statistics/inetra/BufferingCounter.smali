.class final Lru/cn/domain/statistics/inetra/BufferingCounter;
.super Ljava/lang/Object;
.source "BufferingCounter.java"


# instance fields
.field bufferingCount:I

.field private startBufferingTimeMs:J

.field totalBufferingTimeMs:J

.field totalPlayingContentTime:J


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method addWatchedTime(J)V
    .locals 3
    .param p1, "watchedTime"    # J

    .prologue
    .line 29
    iget-wide v0, p0, Lru/cn/domain/statistics/inetra/BufferingCounter;->totalPlayingContentTime:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lru/cn/domain/statistics/inetra/BufferingCounter;->totalPlayingContentTime:J

    .line 30
    return-void
.end method

.method reset()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lru/cn/domain/statistics/inetra/BufferingCounter;->bufferingCount:I

    .line 34
    iput-wide v2, p0, Lru/cn/domain/statistics/inetra/BufferingCounter;->totalBufferingTimeMs:J

    .line 35
    iput-wide v2, p0, Lru/cn/domain/statistics/inetra/BufferingCounter;->totalPlayingContentTime:J

    .line 36
    return-void
.end method

.method startCountBuffering()V
    .locals 2

    .prologue
    .line 17
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lru/cn/domain/statistics/inetra/BufferingCounter;->startBufferingTimeMs:J

    .line 18
    return-void
.end method

.method stopCountBuffering()V
    .locals 6

    .prologue
    .line 21
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lru/cn/domain/statistics/inetra/BufferingCounter;->startBufferingTimeMs:J

    sub-long v0, v2, v4

    .line 22
    .local v0, "elapsed":J
    const-wide/16 v2, 0x3e8

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 23
    iget v2, p0, Lru/cn/domain/statistics/inetra/BufferingCounter;->bufferingCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lru/cn/domain/statistics/inetra/BufferingCounter;->bufferingCount:I

    .line 24
    iget-wide v2, p0, Lru/cn/domain/statistics/inetra/BufferingCounter;->totalBufferingTimeMs:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lru/cn/domain/statistics/inetra/BufferingCounter;->totalBufferingTimeMs:J

    .line 26
    :cond_0
    return-void
.end method
