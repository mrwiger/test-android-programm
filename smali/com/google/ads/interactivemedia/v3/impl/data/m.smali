.class public Lcom/google/ads/interactivemedia/v3/impl/data/m;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/ads/interactivemedia/v3/api/UiElement;


# static fields
.field public static final GSON_TYPE_ADAPTER:Lcom/google/obf/ew;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/ew",
            "<",
            "Lcom/google/ads/interactivemedia/v3/impl/data/m;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/google/ads/interactivemedia/v3/impl/data/m$1;

    invoke-direct {v0}, Lcom/google/ads/interactivemedia/v3/impl/data/m$1;-><init>()V

    sput-object v0, Lcom/google/ads/interactivemedia/v3/impl/data/m;->GSON_TYPE_ADAPTER:Lcom/google/obf/ew;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/ads/interactivemedia/v3/impl/data/m;->name:Ljava/lang/String;

    .line 3
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 5
    if-ne p0, p1, :cond_1

    .line 6
    const/4 v0, 0x1

    .line 12
    :cond_0
    :goto_0
    return v0

    .line 7
    :cond_1
    if-eqz p1, :cond_0

    .line 9
    instance-of v1, p1, Lcom/google/ads/interactivemedia/v3/impl/data/m;

    if-eqz v1, :cond_0

    .line 11
    check-cast p1, Lcom/google/ads/interactivemedia/v3/impl/data/m;

    .line 12
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/m;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/ads/interactivemedia/v3/impl/data/m;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/m;->name:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 13
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/ads/interactivemedia/v3/impl/data/m;->name:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/obf/ix;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/ads/interactivemedia/v3/impl/data/m;->name:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x14

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "UiElementImpl[name="

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
