.class public Lcom/yandex/metrica/impl/ob/dd;
.super Lcom/yandex/metrica/impl/ob/de;
.source "SourceFile"


# static fields
.field private static final a:Lcom/yandex/metrica/impl/ob/fm;

.field private static final b:Lcom/yandex/metrica/impl/ob/fm;

.field private static final c:Lcom/yandex/metrica/impl/ob/fm;

.field private static final d:Lcom/yandex/metrica/impl/ob/fm;

.field private static final e:Lcom/yandex/metrica/impl/ob/fm;

.field private static final f:Lcom/yandex/metrica/impl/ob/fm;

.field private static final g:Lcom/yandex/metrica/impl/ob/fm;

.field private static final h:Lcom/yandex/metrica/impl/ob/fm;

.field private static final i:Lcom/yandex/metrica/impl/ob/fm;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "COLLECT_INSTALLED_APPS"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dd;->a:Lcom/yandex/metrica/impl/ob/fm;

    .line 23
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "IDENTITY_SEND_TIME"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dd;->b:Lcom/yandex/metrica/impl/ob/fm;

    .line 24
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "PERMISSIONS_CHECK_TIME"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dd;->c:Lcom/yandex/metrica/impl/ob/fm;

    .line 25
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "USER_INFO"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dd;->d:Lcom/yandex/metrica/impl/ob/fm;

    .line 27
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "APP_ENVIRONMENT"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dd;->e:Lcom/yandex/metrica/impl/ob/fm;

    .line 28
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "APP_ENVIRONMENT_REVISION"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dd;->f:Lcom/yandex/metrica/impl/ob/fm;

    .line 30
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "LAST_MIGRATION_VERSION"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dd;->g:Lcom/yandex/metrica/impl/ob/fm;

    .line 31
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "LAST_APP_VERSION_WITH_FEATURES"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dd;->h:Lcom/yandex/metrica/impl/ob/fm;

    .line 32
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "APPLICATION_FEATURES"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dd;->i:Lcom/yandex/metrica/impl/ob/fm;

    return-void
.end method

.method public constructor <init>(Lcom/yandex/metrica/impl/ob/cr;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/de;-><init>(Lcom/yandex/metrica/impl/ob/cr;)V

    .line 38
    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/yandex/metrica/impl/ob/dd;->h:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/dd;->b(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public a(J)J
    .locals 3

    .prologue
    .line 41
    sget-object v0, Lcom/yandex/metrica/impl/ob/dd;->b:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/dd;->b(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public a()Lcom/yandex/metrica/impl/a$a;
    .locals 6

    .prologue
    .line 53
    monitor-enter p0

    .line 54
    :try_start_0
    new-instance v0, Lcom/yandex/metrica/impl/a$a;

    sget-object v1, Lcom/yandex/metrica/impl/ob/dd;->e:Lcom/yandex/metrica/impl/ob/fm;

    .line 55
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "{}"

    invoke-virtual {p0, v1, v2}, Lcom/yandex/metrica/impl/ob/dd;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/yandex/metrica/impl/ob/dd;->f:Lcom/yandex/metrica/impl/ob/fm;

    .line 56
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v2

    const-wide/16 v4, 0x0

    invoke-virtual {p0, v2, v4, v5}, Lcom/yandex/metrica/impl/ob/dd;->b(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Lcom/yandex/metrica/impl/a$a;-><init>(Ljava/lang/String;J)V

    monitor-exit p0

    .line 54
    return-object v0

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/yandex/metrica/CounterConfiguration$a;)Lcom/yandex/metrica/impl/ob/dd;
    .locals 4

    .prologue
    .line 91
    sget-object v0, Lcom/yandex/metrica/impl/ob/dd;->a:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p1, Lcom/yandex/metrica/CounterConfiguration$a;->d:I

    int-to-long v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/yandex/metrica/impl/ob/dd;->a(Ljava/lang/String;J)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dd;

    return-object v0
.end method

.method public a(Lcom/yandex/metrica/impl/a$a;)Lcom/yandex/metrica/impl/ob/dd;
    .locals 4

    .prologue
    .line 75
    monitor-enter p0

    .line 76
    :try_start_0
    sget-object v0, Lcom/yandex/metrica/impl/ob/dd;->e:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/yandex/metrica/impl/a$a;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/dd;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    .line 77
    sget-object v0, Lcom/yandex/metrica/impl/ob/dd;->f:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p1, Lcom/yandex/metrica/impl/a$a;->b:J

    invoke-virtual {p0, v0, v2, v3}, Lcom/yandex/metrica/impl/ob/dd;->a(Ljava/lang/String;J)Lcom/yandex/metrica/impl/ob/de;

    .line 78
    monitor-exit p0

    .line 79
    return-object p0

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dd;
    .locals 2

    .prologue
    .line 115
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "SESSION_"

    invoke-direct {v0, v1, p1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/yandex/metrica/impl/ob/dd;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dd;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/yandex/metrica/impl/ob/dd;->d:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/dd;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(J)J
    .locals 3

    .prologue
    .line 45
    sget-object v0, Lcom/yandex/metrica/impl/ob/dd;->c:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/dd;->b(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public b(I)Lcom/yandex/metrica/impl/ob/dd;
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lcom/yandex/metrica/impl/ob/dd;->h:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/dd;->a(Ljava/lang/String;I)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dd;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dd;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/yandex/metrica/impl/ob/dd;->d:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/dd;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dd;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 62
    sget-object v0, Lcom/yandex/metrica/impl/ob/dd;->i:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/dd;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/yandex/metrica/CounterConfiguration$a;
    .locals 4

    .prologue
    .line 66
    sget-object v0, Lcom/yandex/metrica/impl/ob/dd;->a:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/yandex/metrica/CounterConfiguration$a;->a:Lcom/yandex/metrica/CounterConfiguration$a;

    iget v1, v1, Lcom/yandex/metrica/CounterConfiguration$a;->d:I

    int-to-long v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/yandex/metrica/impl/ob/dd;->b(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/yandex/metrica/CounterConfiguration$a;->a(I)Lcom/yandex/metrica/CounterConfiguration$a;

    move-result-object v0

    return-object v0
.end method

.method public c(J)Lcom/yandex/metrica/impl/ob/dd;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/yandex/metrica/impl/ob/dd;->b:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/dd;->a(Ljava/lang/String;J)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dd;

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dd;
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/yandex/metrica/impl/ob/dd;->i:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/dd;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dd;

    return-object v0
.end method

.method public d()J
    .locals 4

    .prologue
    .line 99
    sget-object v0, Lcom/yandex/metrica/impl/ob/dd;->g:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/yandex/metrica/impl/ob/dd;->b(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public d(J)Lcom/yandex/metrica/impl/ob/dd;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/yandex/metrica/impl/ob/dd;->c:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/dd;->a(Ljava/lang/String;J)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dd;

    return-object v0
.end method

.method public d(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 119
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "SESSION_"

    invoke-direct {v0, v1, p1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/dd;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e(J)Lcom/yandex/metrica/impl/ob/dd;
    .locals 1

    .prologue
    .line 103
    sget-object v0, Lcom/yandex/metrica/impl/ob/dd;->g:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/dd;->a(Ljava/lang/String;J)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dd;

    return-object v0
.end method
