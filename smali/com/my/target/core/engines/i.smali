.class public final Lcom/my/target/core/engines/i;
.super Ljava/lang/Object;
.source "InstreamAudioAdEngine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/core/engines/i$a;
    }
.end annotation


# instance fields
.field private A:I

.field private B:I

.field private C:Z

.field private final adConfig:Lcom/my/target/b;

.field private loadingTimeoutSeconds:I

.field private midpoints:[F

.field private final q:Lcom/my/target/instreamads/InstreamAudioAd;

.field private final r:Lcom/my/target/fm;

.field private final s:Lcom/my/target/core/controllers/c;

.field private final t:Lcom/my/target/ce;

.field private u:Lcom/my/target/al;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/my/target/al",
            "<",
            "Lcom/my/target/common/models/AudioData;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lcom/my/target/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/my/target/aj",
            "<",
            "Lcom/my/target/common/models/AudioData;",
            ">;"
        }
    .end annotation
.end field

.field private w:Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;

.field private x:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;",
            ">;"
        }
    .end annotation
.end field

.field private y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/my/target/aj",
            "<",
            "Lcom/my/target/common/models/AudioData;",
            ">;>;"
        }
    .end annotation
.end field

.field private z:F


# direct methods
.method private constructor <init>(Lcom/my/target/instreamads/InstreamAudioAd;Lcom/my/target/fm;Lcom/my/target/b;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/my/target/core/engines/i;->midpoints:[F

    .line 69
    iput-object p1, p0, Lcom/my/target/core/engines/i;->q:Lcom/my/target/instreamads/InstreamAudioAd;

    .line 70
    iput-object p2, p0, Lcom/my/target/core/engines/i;->r:Lcom/my/target/fm;

    .line 71
    iput-object p3, p0, Lcom/my/target/core/engines/i;->adConfig:Lcom/my/target/b;

    .line 72
    invoke-static {}, Lcom/my/target/core/controllers/c;->f()Lcom/my/target/core/controllers/c;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/engines/i;->s:Lcom/my/target/core/controllers/c;

    .line 73
    iget-object v0, p0, Lcom/my/target/core/engines/i;->s:Lcom/my/target/core/controllers/c;

    new-instance v1, Lcom/my/target/core/engines/i$a;

    invoke-direct {v1, p0, v2}, Lcom/my/target/core/engines/i$a;-><init>(Lcom/my/target/core/engines/i;B)V

    invoke-virtual {v0, v1}, Lcom/my/target/core/controllers/c;->a(Lcom/my/target/core/controllers/c$b;)V

    .line 74
    invoke-static {}, Lcom/my/target/ce;->bw()Lcom/my/target/ce;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/engines/i;->t:Lcom/my/target/ce;

    .line 75
    return-void
.end method

.method private a(Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;)Lcom/my/target/ai;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 479
    iget-object v1, p0, Lcom/my/target/core/engines/i;->x:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/my/target/core/engines/i;->w:Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/my/target/core/engines/i;->v:Lcom/my/target/aj;

    if-nez v1, :cond_1

    .line 481
    :cond_0
    const-string v1, "can\'t find companion banner: no playing banner"

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 493
    :goto_0
    return-object v0

    .line 484
    :cond_1
    iget-object v1, p0, Lcom/my/target/core/engines/i;->v:Lcom/my/target/aj;

    invoke-virtual {v1}, Lcom/my/target/aj;->getCompanionBanners()Ljava/util/ArrayList;

    move-result-object v1

    .line 485
    iget-object v2, p0, Lcom/my/target/core/engines/i;->x:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 486
    if-ltz v2, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v2, v3, :cond_3

    .line 489
    :cond_2
    const-string v1, "can\'t find companion banner: provided instreamAdCompanionBanner not found in current playing banner"

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 493
    :cond_3
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/ai;

    goto :goto_0
.end method

.method static synthetic a(Lcom/my/target/core/engines/i;)Lcom/my/target/al;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    return-object v0
.end method

.method public static a(Lcom/my/target/instreamads/InstreamAudioAd;Lcom/my/target/fm;Lcom/my/target/b;)Lcom/my/target/core/engines/i;
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/my/target/core/engines/i;

    invoke-direct {v0, p0, p1, p2}, Lcom/my/target/core/engines/i;-><init>(Lcom/my/target/instreamads/InstreamAudioAd;Lcom/my/target/fm;Lcom/my/target/b;)V

    return-object v0
.end method

.method private a(Lcom/my/target/ae;Lcom/my/target/al;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/ae;",
            "Lcom/my/target/al",
            "<",
            "Lcom/my/target/common/models/AudioData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 365
    iget-object v0, p0, Lcom/my/target/core/engines/i;->s:Lcom/my/target/core/controllers/c;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/c;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 366
    if-nez v0, :cond_0

    .line 368
    const-string v0, "can\'t load doAfter service: context is null"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 380
    :goto_0
    return-void

    .line 371
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "loading doAfter service: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/my/target/ae;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 372
    iget-object v1, p0, Lcom/my/target/core/engines/i;->adConfig:Lcom/my/target/b;

    iget v2, p0, Lcom/my/target/core/engines/i;->loadingTimeoutSeconds:I

    invoke-static {p1, v1, v2}, Lcom/my/target/fj;->newFactoryForAdService(Lcom/my/target/ae;Lcom/my/target/b;I)Lcom/my/target/c;

    move-result-object v1

    new-instance v2, Lcom/my/target/core/engines/i$1;

    invoke-direct {v2, p0, p2}, Lcom/my/target/core/engines/i$1;-><init>(Lcom/my/target/core/engines/i;Lcom/my/target/al;)V

    invoke-virtual {v1, v2}, Lcom/my/target/c;->a(Lcom/my/target/c$b;)Lcom/my/target/c;

    move-result-object v1

    .line 379
    invoke-virtual {v1, v0}, Lcom/my/target/c;->a(Landroid/content/Context;)Lcom/my/target/c;

    goto :goto_0
.end method

.method private a(Lcom/my/target/aj;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 463
    if-nez p1, :cond_0

    .line 465
    const-string v0, "can\'t send stat: banner is null"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 475
    :goto_0
    return-void

    .line 468
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/engines/i;->s:Lcom/my/target/core/controllers/c;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/c;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 469
    if-nez v0, :cond_1

    .line 471
    const-string v0, "can\'t send stat: context is null"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 474
    :cond_1
    invoke-virtual {p1}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    goto :goto_0
.end method

.method private a(Lcom/my/target/al;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/al",
            "<",
            "Lcom/my/target/common/models/AudioData;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 344
    iget-object v0, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    if-ne p1, v0, :cond_1

    .line 346
    const-string v0, "midroll"

    invoke-virtual {p1}, Lcom/my/target/al;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    iget v1, p0, Lcom/my/target/core/engines/i;->B:I

    invoke-virtual {v0, v1}, Lcom/my/target/al;->f(I)V

    .line 350
    :cond_0
    iput-object v2, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    .line 351
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/core/engines/i;->C:Z

    .line 352
    iput-object v2, p0, Lcom/my/target/core/engines/i;->v:Lcom/my/target/aj;

    .line 353
    iput-object v2, p0, Lcom/my/target/core/engines/i;->w:Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;

    .line 354
    const/4 v0, -0x1

    iput v0, p0, Lcom/my/target/core/engines/i;->A:I

    .line 355
    iget-object v0, p0, Lcom/my/target/core/engines/i;->q:Lcom/my/target/instreamads/InstreamAudioAd;

    invoke-virtual {v0}, Lcom/my/target/instreamads/InstreamAudioAd;->getListener()Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;

    move-result-object v0

    .line 356
    if-eqz v0, :cond_1

    .line 358
    invoke-virtual {p1}, Lcom/my/target/al;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/my/target/core/engines/i;->q:Lcom/my/target/instreamads/InstreamAudioAd;

    invoke-interface {v0, v1, v2}, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;->onComplete(Ljava/lang/String;Lcom/my/target/instreamads/InstreamAudioAd;)V

    .line 361
    :cond_1
    return-void
.end method

.method private a(Lcom/my/target/al;F)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/al",
            "<",
            "Lcom/my/target/common/models/AudioData;",
            ">;F)V"
        }
    .end annotation

    .prologue
    .line 248
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 249
    invoke-virtual {p1}, Lcom/my/target/al;->R()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/aj;

    .line 251
    invoke-virtual {v0}, Lcom/my/target/aj;->getPoint()F

    move-result v3

    cmpl-float v3, v3, p2

    if-nez v3, :cond_0

    .line 253
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 256
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 257
    if-lez v0, :cond_2

    iget v2, p0, Lcom/my/target/core/engines/i;->A:I

    add-int/lit8 v0, v0, -0x1

    if-ge v2, v0, :cond_2

    .line 259
    iput-object v1, p0, Lcom/my/target/core/engines/i;->y:Ljava/util/List;

    .line 260
    invoke-direct {p0}, Lcom/my/target/core/engines/i;->g()V

    .line 275
    :goto_1
    return-void

    .line 264
    :cond_2
    invoke-virtual {p1, p2}, Lcom/my/target/al;->a(F)Ljava/util/ArrayList;

    move-result-object v0

    .line 265
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 267
    invoke-direct {p0, v0, p1, p2}, Lcom/my/target/core/engines/i;->a(Ljava/util/ArrayList;Lcom/my/target/al;F)V

    goto :goto_1

    .line 271
    :cond_3
    const-string v0, "There is no one midpoint service for point: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 272
    invoke-direct {p0, p1, p2}, Lcom/my/target/core/engines/i;->b(Lcom/my/target/al;F)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/my/target/core/engines/i;Lcom/my/target/al;Lcom/my/target/fm;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1386
    if-nez p2, :cond_2

    .line 1388
    if-eqz p3, :cond_0

    .line 1390
    const-string v0, "loading doAfter service failed: "

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 1392
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    if-ne p1, v0, :cond_1

    .line 1394
    iget v0, p0, Lcom/my/target/core/engines/i;->z:F

    invoke-direct {p0, p1, v0}, Lcom/my/target/core/engines/i;->b(Lcom/my/target/al;F)V

    .line 1396
    :cond_1
    :goto_0
    return-void

    .line 1399
    :cond_2
    invoke-virtual {p1}, Lcom/my/target/al;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/fm;->a(Ljava/lang/String;)Lcom/my/target/al;

    move-result-object v0

    .line 1400
    if-eqz v0, :cond_3

    .line 1402
    invoke-virtual {p1, v0}, Lcom/my/target/al;->a(Lcom/my/target/al;)V

    .line 1404
    :cond_3
    iget-object v0, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    if-ne p1, v0, :cond_1

    .line 1406
    invoke-virtual {p1}, Lcom/my/target/al;->R()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/engines/i;->y:Ljava/util/List;

    .line 1407
    invoke-direct {p0}, Lcom/my/target/core/engines/i;->g()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/my/target/core/engines/i;Lcom/my/target/al;Lcom/my/target/fm;Ljava/lang/String;F)V
    .locals 2

    .prologue
    .line 39
    .line 1437
    if-nez p2, :cond_2

    .line 1439
    if-eqz p3, :cond_0

    .line 1441
    const-string v0, "loading midpoint services failed: "

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 1443
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/my/target/core/engines/i;->z:F

    cmpl-float v0, p4, v0

    if-nez v0, :cond_1

    .line 1445
    invoke-direct {p0, p1, p4}, Lcom/my/target/core/engines/i;->b(Lcom/my/target/al;F)V

    .line 1447
    :cond_1
    :goto_0
    return-void

    .line 1450
    :cond_2
    invoke-virtual {p1}, Lcom/my/target/al;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/fm;->a(Ljava/lang/String;)Lcom/my/target/al;

    move-result-object v0

    .line 1451
    if-eqz v0, :cond_3

    .line 1453
    invoke-virtual {p1, v0}, Lcom/my/target/al;->a(Lcom/my/target/al;)V

    .line 1455
    :cond_3
    iget-object v0, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/my/target/core/engines/i;->z:F

    cmpl-float v0, p4, v0

    if-nez v0, :cond_1

    .line 1457
    invoke-direct {p0, p1, p4}, Lcom/my/target/core/engines/i;->a(Lcom/my/target/al;F)V

    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;Lcom/my/target/al;F)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/ae;",
            ">;",
            "Lcom/my/target/al",
            "<",
            "Lcom/my/target/common/models/AudioData;",
            ">;F)V"
        }
    .end annotation

    .prologue
    .line 415
    iget-object v0, p0, Lcom/my/target/core/engines/i;->s:Lcom/my/target/core/controllers/c;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/c;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 416
    if-nez v0, :cond_0

    .line 418
    const-string v0, "can\'t load midpoint services: context is null"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 430
    :goto_0
    return-void

    .line 421
    :cond_0
    const-string v1, "loading midpoint services for point: "

    invoke-static {p3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 422
    iget-object v1, p0, Lcom/my/target/core/engines/i;->adConfig:Lcom/my/target/b;

    iget v2, p0, Lcom/my/target/core/engines/i;->loadingTimeoutSeconds:I

    invoke-static {p1, v1, v2}, Lcom/my/target/fj;->newFactoryForAdServices(Ljava/util/List;Lcom/my/target/b;I)Lcom/my/target/c;

    move-result-object v1

    new-instance v2, Lcom/my/target/core/engines/i$2;

    invoke-direct {v2, p0, p2, p3}, Lcom/my/target/core/engines/i$2;-><init>(Lcom/my/target/core/engines/i;Lcom/my/target/al;F)V

    invoke-virtual {v1, v2}, Lcom/my/target/c;->a(Lcom/my/target/c$b;)Lcom/my/target/c;

    move-result-object v1

    .line 429
    invoke-virtual {v1, v0}, Lcom/my/target/c;->a(Landroid/content/Context;)Lcom/my/target/c;

    goto :goto_0
.end method

.method static synthetic b(Lcom/my/target/core/engines/i;)Lcom/my/target/aj;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/my/target/core/engines/i;->v:Lcom/my/target/aj;

    return-object v0
.end method

.method private b(Lcom/my/target/al;F)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/al",
            "<",
            "Lcom/my/target/common/models/AudioData;",
            ">;F)V"
        }
    .end annotation

    .prologue
    .line 319
    invoke-virtual {p1}, Lcom/my/target/al;->T()Lcom/my/target/ae;

    move-result-object v0

    .line 320
    if-eqz v0, :cond_1

    .line 322
    const-string v1, "midroll"

    invoke-virtual {p1}, Lcom/my/target/al;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 324
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/my/target/ae;->e(Z)V

    .line 325
    invoke-virtual {v0, p2}, Lcom/my/target/ae;->setPoint(F)V

    .line 326
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 327
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 328
    const-string v0, "using doAfter service for point: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 329
    invoke-direct {p0, v1, p1, p2}, Lcom/my/target/core/engines/i;->a(Ljava/util/ArrayList;Lcom/my/target/al;F)V

    .line 340
    :goto_0
    return-void

    .line 333
    :cond_0
    invoke-direct {p0, v0, p1}, Lcom/my/target/core/engines/i;->a(Lcom/my/target/ae;Lcom/my/target/al;)V

    goto :goto_0

    .line 338
    :cond_1
    invoke-direct {p0, p1}, Lcom/my/target/core/engines/i;->a(Lcom/my/target/al;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/my/target/core/engines/i;)Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/my/target/core/engines/i;->w:Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;

    return-object v0
.end method

.method static synthetic d(Lcom/my/target/core/engines/i;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/my/target/core/engines/i;->C:Z

    return v0
.end method

.method static synthetic e(Lcom/my/target/core/engines/i;)Z
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/core/engines/i;->C:Z

    return v0
.end method

.method static synthetic f(Lcom/my/target/core/engines/i;)Lcom/my/target/core/controllers/c;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/my/target/core/engines/i;->s:Lcom/my/target/core/controllers/c;

    return-object v0
.end method

.method static synthetic g(Lcom/my/target/core/engines/i;)Lcom/my/target/instreamads/InstreamAudioAd;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/my/target/core/engines/i;->q:Lcom/my/target/instreamads/InstreamAudioAd;

    return-object v0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 279
    :goto_0
    iget-object v0, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    if-nez v0, :cond_0

    .line 315
    :goto_1
    return-void

    .line 283
    :cond_0
    iget v0, p0, Lcom/my/target/core/engines/i;->B:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/my/target/core/engines/i;->y:Ljava/util/List;

    if-nez v0, :cond_2

    .line 285
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    iget v1, p0, Lcom/my/target/core/engines/i;->z:F

    invoke-direct {p0, v0, v1}, Lcom/my/target/core/engines/i;->b(Lcom/my/target/al;F)V

    goto :goto_1

    .line 289
    :cond_2
    iget v0, p0, Lcom/my/target/core/engines/i;->A:I

    add-int/lit8 v0, v0, 0x1

    .line 290
    iget-object v1, p0, Lcom/my/target/core/engines/i;->y:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 292
    iput v0, p0, Lcom/my/target/core/engines/i;->A:I

    .line 293
    iget-object v1, p0, Lcom/my/target/core/engines/i;->y:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/aj;

    .line 294
    const-string v1, "statistics"

    invoke-virtual {v0}, Lcom/my/target/aj;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 296
    const-string v1, "playbackStarted"

    invoke-direct {p0, v0, v1}, Lcom/my/target/core/engines/i;->a(Lcom/my/target/aj;Ljava/lang/String;)V

    goto :goto_0

    .line 301
    :cond_3
    iget v1, p0, Lcom/my/target/core/engines/i;->B:I

    if-lez v1, :cond_4

    .line 303
    iget v1, p0, Lcom/my/target/core/engines/i;->B:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/my/target/core/engines/i;->B:I

    .line 305
    :cond_4
    iput-object v0, p0, Lcom/my/target/core/engines/i;->v:Lcom/my/target/aj;

    .line 306
    invoke-static {v0}, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;->newBanner(Lcom/my/target/aj;)Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;

    move-result-object v1

    iput-object v1, p0, Lcom/my/target/core/engines/i;->w:Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;

    .line 307
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/my/target/core/engines/i;->w:Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;

    iget-object v2, v2, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;->companionBanners:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/my/target/core/engines/i;->x:Ljava/util/List;

    .line 308
    iget-object v1, p0, Lcom/my/target/core/engines/i;->s:Lcom/my/target/core/controllers/c;

    invoke-virtual {v1, v0}, Lcom/my/target/core/controllers/c;->play(Lcom/my/target/aj;)V

    goto :goto_1

    .line 313
    :cond_5
    iget-object v0, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    iget v1, p0, Lcom/my/target/core/engines/i;->z:F

    invoke-direct {p0, v0, v1}, Lcom/my/target/core/engines/i;->b(Lcom/my/target/al;F)V

    goto :goto_1
.end method

.method static synthetic h(Lcom/my/target/core/engines/i;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/my/target/core/engines/i;->g()V

    return-void
.end method


# virtual methods
.method public final destroy()V
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/my/target/core/engines/i;->s:Lcom/my/target/core/controllers/c;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/c;->destroy()V

    .line 239
    return-void
.end method

.method public final getPlayer()Lcom/my/target/instreamads/InstreamAudioAdPlayer;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/my/target/core/engines/i;->s:Lcom/my/target/core/controllers/c;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/c;->getPlayer()Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    move-result-object v0

    return-object v0
.end method

.method public final getVolume()F
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/my/target/core/engines/i;->s:Lcom/my/target/core/controllers/c;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/c;->getVolume()F

    move-result v0

    return v0
.end method

.method public final handleCompanionClick(Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;)V
    .locals 3
    .param p1, "instreamAdCompanionBanner"    # Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;

    .prologue
    .line 192
    iget-object v0, p0, Lcom/my/target/core/engines/i;->s:Lcom/my/target/core/controllers/c;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/c;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 193
    if-nez v0, :cond_0

    .line 195
    const-string v0, "can\'t handle click: context is null"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 205
    :goto_0
    return-void

    .line 198
    :cond_0
    invoke-direct {p0, p1}, Lcom/my/target/core/engines/i;->a(Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;)Lcom/my/target/ai;

    move-result-object v1

    .line 199
    if-nez v1, :cond_1

    .line 201
    const-string v0, "can\'t handle click: companion banner not found"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 204
    :cond_1
    iget-object v2, p0, Lcom/my/target/core/engines/i;->t:Lcom/my/target/ce;

    invoke-virtual {v2, v1, v0}, Lcom/my/target/ce;->a(Lcom/my/target/ah;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final handleCompanionClick(Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;Landroid/content/Context;)V
    .locals 2
    .param p1, "instreamAdCompanionBanner"    # Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 209
    invoke-direct {p0, p1}, Lcom/my/target/core/engines/i;->a(Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;)Lcom/my/target/ai;

    move-result-object v0

    .line 210
    if-nez v0, :cond_0

    .line 212
    const-string v0, "can\'t handle click: companion banner not found"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 216
    :goto_0
    return-void

    .line 215
    :cond_0
    iget-object v1, p0, Lcom/my/target/core/engines/i;->t:Lcom/my/target/ce;

    invoke-virtual {v1, v0, p2}, Lcom/my/target/ce;->a(Lcom/my/target/ah;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final handleCompanionShow(Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;)V
    .locals 3
    .param p1, "instreamAdCompanionBanner"    # Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;

    .prologue
    .line 221
    iget-object v0, p0, Lcom/my/target/core/engines/i;->s:Lcom/my/target/core/controllers/c;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/c;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 222
    if-nez v0, :cond_0

    .line 224
    const-string v0, "can\'t handle show: context is null"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 234
    :goto_0
    return-void

    .line 227
    :cond_0
    invoke-direct {p0, p1}, Lcom/my/target/core/engines/i;->a(Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;)Lcom/my/target/ai;

    move-result-object v1

    .line 228
    if-nez v1, :cond_1

    .line 230
    const-string v0, "can\'t handle show: companion banner not found"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 233
    :cond_1
    invoke-virtual {v1}, Lcom/my/target/ai;->getStatHolder()Lcom/my/target/ar;

    move-result-object v1

    const-string v2, "playbackStarted"

    invoke-virtual {v1, v2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final pause()V
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/my/target/core/engines/i;->s:Lcom/my/target/core/controllers/c;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/c;->pause()V

    .line 158
    :cond_0
    return-void
.end method

.method public final resume()V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/my/target/core/engines/i;->s:Lcom/my/target/core/controllers/c;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/c;->resume()V

    .line 166
    :cond_0
    return-void
.end method

.method public final setLoadingTimeoutSeconds(I)V
    .locals 0
    .param p1, "loadingTimeoutSeconds"    # I

    .prologue
    .line 99
    iput p1, p0, Lcom/my/target/core/engines/i;->loadingTimeoutSeconds:I

    .line 100
    return-void
.end method

.method public final setMidpoints([F)V
    .locals 0
    .param p1, "midpoints"    # [F

    .prologue
    .line 94
    iput-object p1, p0, Lcom/my/target/core/engines/i;->midpoints:[F

    .line 95
    return-void
.end method

.method public final setPlayer(Lcom/my/target/instreamads/InstreamAudioAdPlayer;)V
    .locals 1
    .param p1, "player"    # Lcom/my/target/instreamads/InstreamAudioAdPlayer;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/my/target/core/engines/i;->s:Lcom/my/target/core/controllers/c;

    invoke-virtual {v0, p1}, Lcom/my/target/core/controllers/c;->setPlayer(Lcom/my/target/instreamads/InstreamAudioAdPlayer;)V

    .line 90
    return-void
.end method

.method public final setVolume(F)V
    .locals 1
    .param p1, "volume"    # F

    .prologue
    .line 79
    iget-object v0, p0, Lcom/my/target/core/engines/i;->s:Lcom/my/target/core/controllers/c;

    invoke-virtual {v0, p1}, Lcom/my/target/core/controllers/c;->setVolume(F)V

    .line 80
    return-void
.end method

.method public final skip()V
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, Lcom/my/target/core/engines/i;->v:Lcom/my/target/aj;

    const-string v1, "closedByUser"

    invoke-direct {p0, v0, v1}, Lcom/my/target/core/engines/i;->a(Lcom/my/target/aj;Ljava/lang/String;)V

    .line 180
    invoke-virtual {p0}, Lcom/my/target/core/engines/i;->stop()V

    .line 181
    return-void
.end method

.method public final skipBanner()V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lcom/my/target/core/engines/i;->v:Lcom/my/target/aj;

    const-string v1, "closedByUser"

    invoke-direct {p0, v0, v1}, Lcom/my/target/core/engines/i;->a(Lcom/my/target/aj;Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lcom/my/target/core/engines/i;->s:Lcom/my/target/core/controllers/c;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/c;->stop()V

    .line 187
    invoke-direct {p0}, Lcom/my/target/core/engines/i;->g()V

    .line 188
    return-void
.end method

.method public final start(Ljava/lang/String;)V
    .locals 2
    .param p1, "sectionName"    # Ljava/lang/String;

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/my/target/core/engines/i;->stop()V

    .line 105
    iget-object v0, p0, Lcom/my/target/core/engines/i;->r:Lcom/my/target/fm;

    invoke-virtual {v0, p1}, Lcom/my/target/fm;->a(Ljava/lang/String;)Lcom/my/target/al;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    .line 106
    iget-object v0, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/my/target/core/engines/i;->s:Lcom/my/target/core/controllers/c;

    iget-object v1, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    invoke-virtual {v1}, Lcom/my/target/al;->P()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/core/controllers/c;->setConnectionTimeout(I)V

    .line 109
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/core/engines/i;->C:Z

    .line 110
    iget-object v0, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    invoke-virtual {v0}, Lcom/my/target/al;->Q()I

    move-result v0

    iput v0, p0, Lcom/my/target/core/engines/i;->B:I

    .line 111
    const/4 v0, -0x1

    iput v0, p0, Lcom/my/target/core/engines/i;->A:I

    .line 112
    iget-object v0, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    invoke-virtual {v0}, Lcom/my/target/al;->R()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/engines/i;->y:Ljava/util/List;

    .line 113
    invoke-direct {p0}, Lcom/my/target/core/engines/i;->g()V

    .line 119
    :goto_0
    return-void

    .line 117
    :cond_0
    const-string v0, "no section with name "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final startMidroll(F)V
    .locals 5
    .param p1, "point"    # F

    .prologue
    const/4 v1, 0x0

    .line 123
    invoke-virtual {p0}, Lcom/my/target/core/engines/i;->stop()V

    .line 125
    iget-object v2, p0, Lcom/my/target/core/engines/i;->midpoints:[F

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_3

    aget v4, v2, v0

    .line 127
    invoke-static {v4, p1}, Ljava/lang/Float;->compare(FF)I

    move-result v4

    if-nez v4, :cond_1

    .line 129
    const/4 v0, 0x1

    .line 133
    :goto_1
    if-eqz v0, :cond_2

    .line 135
    iget-object v0, p0, Lcom/my/target/core/engines/i;->r:Lcom/my/target/fm;

    const-string v2, "midroll"

    invoke-virtual {v0, v2}, Lcom/my/target/fm;->a(Ljava/lang/String;)Lcom/my/target/al;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    .line 136
    iget-object v0, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/my/target/core/engines/i;->s:Lcom/my/target/core/controllers/c;

    iget-object v2, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    invoke-virtual {v2}, Lcom/my/target/al;->P()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/my/target/core/controllers/c;->setConnectionTimeout(I)V

    .line 139
    iput-boolean v1, p0, Lcom/my/target/core/engines/i;->C:Z

    .line 140
    iget-object v0, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    invoke-virtual {v0}, Lcom/my/target/al;->Q()I

    move-result v0

    iput v0, p0, Lcom/my/target/core/engines/i;->B:I

    .line 141
    const/4 v0, -0x1

    iput v0, p0, Lcom/my/target/core/engines/i;->A:I

    .line 142
    iput p1, p0, Lcom/my/target/core/engines/i;->z:F

    .line 143
    iget-object v0, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    invoke-direct {p0, v0, p1}, Lcom/my/target/core/engines/i;->a(Lcom/my/target/al;F)V

    .line 150
    :cond_0
    :goto_2
    return-void

    .line 125
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 148
    :cond_2
    const-string v0, "attempt to start wrong midpoint, use one of InstreamAd.getMidPoints()"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final stop()V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/my/target/core/engines/i;->s:Lcom/my/target/core/controllers/c;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/c;->stop()V

    .line 173
    iget-object v0, p0, Lcom/my/target/core/engines/i;->u:Lcom/my/target/al;

    invoke-direct {p0, v0}, Lcom/my/target/core/engines/i;->a(Lcom/my/target/al;)V

    .line 175
    :cond_0
    return-void
.end method
