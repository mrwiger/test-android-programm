.class public Lru/cn/tv/mobile/search/SearchResultAdapter;
.super Landroid/support/v4/widget/SimpleCursorAdapter;
.source "SearchResultAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/mobile/search/SearchResultAdapter$ChannelViewHolder;,
        Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;,
        Lru/cn/tv/mobile/search/SearchResultAdapter$GroupViewHolder;
    }
.end annotation


# instance fields
.field private channelsLayout:I

.field private groupLayout:I

.field private final maskResId:I

.field private final maskedPreloader:Landroid/graphics/drawable/Drawable;

.field private telecastLayout:I


# direct methods
.method public constructor <init>(Landroid/content/Context;III)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "telecastLayout"    # I
    .param p3, "channelsLayout"    # I
    .param p4, "groupLayout"    # I

    .prologue
    const/4 v0, 0x0

    .line 56
    const/4 v3, 0x0

    new-array v4, v0, [Ljava/lang/String;

    new-array v5, v0, [I

    const/4 v6, 0x2

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 58
    iput p4, p0, Lru/cn/tv/mobile/search/SearchResultAdapter;->groupLayout:I

    .line 59
    iput p2, p0, Lru/cn/tv/mobile/search/SearchResultAdapter;->telecastLayout:I

    .line 60
    iput p3, p0, Lru/cn/tv/mobile/search/SearchResultAdapter;->channelsLayout:I

    .line 63
    const v0, 0x7f080096

    iput v0, p0, Lru/cn/tv/mobile/search/SearchResultAdapter;->maskResId:I

    .line 64
    const v0, 0x7f080095

    iget v1, p0, Lru/cn/tv/mobile/search/SearchResultAdapter;->maskResId:I

    invoke-static {p1, v0, v1}, Lru/cn/utils/Utils;->maskedDrawable(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/mobile/search/SearchResultAdapter;->maskedPreloader:Landroid/graphics/drawable/Drawable;

    .line 65
    return-void
.end method

.method private calculateElapsed(J)I
    .locals 9
    .param p1, "time"    # J

    .prologue
    .line 173
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long v2, v4, v6

    .line 174
    .local v2, "timeNowSec":J
    sub-long v0, v2, p1

    .line 176
    .local v0, "timeElapsed":J
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-gez v4, :cond_0

    .line 177
    const-wide/16 v0, 0x0

    .line 180
    :cond_0
    long-to-int v4, v0

    return v4
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 20
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 101
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v13

    .line 103
    .local v13, "position":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lru/cn/tv/mobile/search/SearchResultAdapter;->getItemViewType(I)I

    move-result v17

    if-nez v17, :cond_0

    .line 104
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lru/cn/tv/mobile/search/SearchResultAdapter$GroupViewHolder;

    .line 105
    .local v10, "holder":Lru/cn/tv/mobile/search/SearchResultAdapter$GroupViewHolder;
    const-string v17, "title"

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p3

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 106
    .local v16, "title":Ljava/lang/String;
    iget-object v0, v10, Lru/cn/tv/mobile/search/SearchResultAdapter$GroupViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    .end local v10    # "holder":Lru/cn/tv/mobile/search/SearchResultAdapter$GroupViewHolder;
    :goto_0
    return-void

    .line 107
    .end local v16    # "title":Ljava/lang/String;
    :cond_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lru/cn/tv/mobile/search/SearchResultAdapter;->getItemViewType(I)I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_6

    .line 108
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;

    .line 110
    .local v10, "holder":Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;
    const-string v17, "title"

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p3

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 111
    .restart local v16    # "title":Ljava/lang/String;
    const-string v17, "channel_title"

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p3

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 112
    .local v5, "channelTitle":Ljava/lang/String;
    const-string v17, "time"

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p3

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 113
    .local v14, "time":J
    const-string v17, "duration"

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p3

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 115
    .local v8, "duration":J
    iget-object v0, v10, Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    iget-object v0, v10, Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;->channelTitle:Landroid/widget/TextView;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1

    .line 117
    iget-object v0, v10, Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;->channelTitle:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    :cond_1
    iget-object v0, v10, Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;->dateTime:Landroid/widget/TextView;

    move-object/from16 v17, v0

    if-eqz v17, :cond_2

    .line 121
    new-instance v6, Ljava/sql/Date;

    const-wide/16 v18, 0x3e8

    mul-long v18, v18, v14

    move-wide/from16 v0, v18

    invoke-direct {v6, v0, v1}, Ljava/sql/Date;-><init>(J)V

    .line 122
    .local v6, "d":Ljava/sql/Date;
    invoke-static {v6}, Lru/cn/utils/Utils;->getCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v4

    .line 123
    .local v4, "calendar":Ljava/util/Calendar;
    iget-object v0, v10, Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;->dateTime:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const-string v18, "dd MMMM, HH:mm"

    move-object/from16 v0, v18

    invoke-static {v4, v0}, Lru/cn/utils/Utils;->format(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    .end local v4    # "calendar":Ljava/util/Calendar;
    .end local v6    # "d":Ljava/sql/Date;
    :cond_2
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lru/cn/tv/mobile/search/SearchResultAdapter;->calculateElapsed(J)I

    move-result v7

    .line 127
    .local v7, "elapsedTime":I
    int-to-long v0, v7

    move-wide/from16 v18, v0

    cmp-long v17, v18, v8

    if-gez v17, :cond_3

    .line 128
    iget-object v0, v10, Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;->airProgress:Landroid/widget/ProgressBar;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 129
    iget-object v0, v10, Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;->airProgress:Landroid/widget/ProgressBar;

    move-object/from16 v17, v0

    long-to-int v0, v8

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 130
    iget-object v0, v10, Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;->airProgress:Landroid/widget/ProgressBar;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 135
    :goto_1
    const-string v17, "image"

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p3

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 136
    .local v11, "image":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/mobile/search/SearchResultAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/squareup/picasso/RequestCreator;->fit()Lcom/squareup/picasso/RequestCreator;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/squareup/picasso/RequestCreator;->centerCrop()Lcom/squareup/picasso/RequestCreator;

    move-result-object v17

    const v18, 0x7f0800cd

    .line 137
    invoke-virtual/range {v17 .. v18}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v17

    const v18, 0x7f0800cd

    .line 138
    invoke-virtual/range {v17 .. v18}, Lcom/squareup/picasso/RequestCreator;->error(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v17

    iget-object v0, v10, Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;->image:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 140
    const-string v17, "is_denied"

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p3

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_4

    const/4 v12, 0x1

    .line 141
    .local v12, "isDenied":Z
    :goto_2
    if-eqz v12, :cond_5

    .line 142
    iget-object v0, v10, Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;->accessIndicator:Landroid/view/View;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 132
    .end local v11    # "image":Ljava/lang/String;
    .end local v12    # "isDenied":Z
    :cond_3
    iget-object v0, v10, Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;->airProgress:Landroid/widget/ProgressBar;

    move-object/from16 v17, v0

    const/16 v18, 0x8

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1

    .line 140
    .restart local v11    # "image":Ljava/lang/String;
    :cond_4
    const/4 v12, 0x0

    goto :goto_2

    .line 144
    .restart local v12    # "isDenied":Z
    :cond_5
    iget-object v0, v10, Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;->accessIndicator:Landroid/view/View;

    move-object/from16 v17, v0

    const/16 v18, 0x8

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 147
    .end local v5    # "channelTitle":Ljava/lang/String;
    .end local v7    # "elapsedTime":I
    .end local v8    # "duration":J
    .end local v10    # "holder":Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;
    .end local v11    # "image":Ljava/lang/String;
    .end local v12    # "isDenied":Z
    .end local v14    # "time":J
    .end local v16    # "title":Ljava/lang/String;
    :cond_6
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lru/cn/tv/mobile/search/SearchResultAdapter$ChannelViewHolder;

    .line 148
    .local v10, "holder":Lru/cn/tv/mobile/search/SearchResultAdapter$ChannelViewHolder;
    const-string v17, "title"

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p3

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 149
    .restart local v16    # "title":Ljava/lang/String;
    iget-object v0, v10, Lru/cn/tv/mobile/search/SearchResultAdapter$ChannelViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    const-string v17, "image"

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p3

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 153
    .restart local v11    # "image":Ljava/lang/String;
    if-eqz v11, :cond_7

    .line 154
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/mobile/search/SearchResultAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/squareup/picasso/RequestCreator;->fit()Lcom/squareup/picasso/RequestCreator;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/squareup/picasso/RequestCreator;->centerCrop()Lcom/squareup/picasso/RequestCreator;

    move-result-object v17

    new-instance v18, Lru/cn/utils/MaskTransformation;

    move-object/from16 v0, p0

    iget v0, v0, Lru/cn/tv/mobile/search/SearchResultAdapter;->maskResId:I

    move/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lru/cn/utils/MaskTransformation;-><init>(Landroid/content/Context;I)V

    .line 155
    invoke-virtual/range {v17 .. v18}, Lcom/squareup/picasso/RequestCreator;->transform(Lcom/squareup/picasso/Transformation;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/mobile/search/SearchResultAdapter;->maskedPreloader:Landroid/graphics/drawable/Drawable;

    move-object/from16 v18, v0

    .line 156
    invoke-virtual/range {v17 .. v18}, Lcom/squareup/picasso/RequestCreator;->placeholder(Landroid/graphics/drawable/Drawable;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v17

    iget-object v0, v10, Lru/cn/tv/mobile/search/SearchResultAdapter$ChannelViewHolder;->image:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 163
    :goto_3
    const-string v17, "is_denied"

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p3

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_8

    const/4 v12, 0x1

    .line 164
    .restart local v12    # "isDenied":Z
    :goto_4
    if-eqz v12, :cond_9

    .line 165
    iget-object v0, v10, Lru/cn/tv/mobile/search/SearchResultAdapter$ChannelViewHolder;->accessIndicator:Landroid/view/View;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 158
    .end local v12    # "isDenied":Z
    :cond_7
    invoke-static/range {p2 .. p2}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v17

    const v18, 0x7f080094

    invoke-virtual/range {v17 .. v18}, Lcom/squareup/picasso/Picasso;->load(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v17

    new-instance v18, Lru/cn/utils/MaskTransformation;

    move-object/from16 v0, p0

    iget v0, v0, Lru/cn/tv/mobile/search/SearchResultAdapter;->maskResId:I

    move/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lru/cn/utils/MaskTransformation;-><init>(Landroid/content/Context;I)V

    .line 159
    invoke-virtual/range {v17 .. v18}, Lcom/squareup/picasso/RequestCreator;->transform(Lcom/squareup/picasso/Transformation;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v17

    iget-object v0, v10, Lru/cn/tv/mobile/search/SearchResultAdapter$ChannelViewHolder;->image:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    .line 160
    invoke-virtual/range {v17 .. v18}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    goto :goto_3

    .line 163
    :cond_8
    const/4 v12, 0x0

    goto :goto_4

    .line 167
    .restart local v12    # "isDenied":Z
    :cond_9
    iget-object v0, v10, Lru/cn/tv/mobile/search/SearchResultAdapter$ChannelViewHolder;->accessIndicator:Landroid/view/View;

    move-object/from16 v17, v0

    const/16 v18, 0x8

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public getItemViewType(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v2, -0x1

    .line 190
    invoke-virtual {p0}, Lru/cn/tv/mobile/search/SearchResultAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 191
    .local v0, "c":Landroid/database/Cursor;
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 193
    const-string v1, "telecastId"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v2, :cond_0

    .line 194
    const/4 v1, 0x1

    .line 198
    :goto_0
    return v1

    .line 195
    :cond_0
    const-string v1, "cn_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v2, :cond_1

    .line 196
    const/4 v1, 0x2

    goto :goto_0

    .line 198
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x3

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 204
    invoke-virtual {p0, p1}, Lru/cn/tv/mobile/search/SearchResultAdapter;->getItemViewType(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v10, 0x7f0900f3

    const v9, 0x7f090006

    const v8, 0x7f0901d7

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 69
    const-string v4, "layout_inflater"

    .line 70
    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 72
    .local v1, "inflater":Landroid/view/LayoutInflater;
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    .line 73
    .local v2, "position":I
    invoke-virtual {p0, v2}, Lru/cn/tv/mobile/search/SearchResultAdapter;->getItemViewType(I)I

    move-result v4

    if-nez v4, :cond_0

    .line 74
    iget v4, p0, Lru/cn/tv/mobile/search/SearchResultAdapter;->groupLayout:I

    invoke-virtual {v1, v4, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 75
    .local v3, "view":Landroid/view/View;
    new-instance v0, Lru/cn/tv/mobile/search/SearchResultAdapter$GroupViewHolder;

    invoke-direct {v0, v6}, Lru/cn/tv/mobile/search/SearchResultAdapter$GroupViewHolder;-><init>(Lru/cn/tv/mobile/search/SearchResultAdapter$1;)V

    .line 76
    .local v0, "holder":Lru/cn/tv/mobile/search/SearchResultAdapter$GroupViewHolder;
    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v0, Lru/cn/tv/mobile/search/SearchResultAdapter$GroupViewHolder;->title:Landroid/widget/TextView;

    .line 77
    invoke-virtual {v3, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 96
    .end local v0    # "holder":Lru/cn/tv/mobile/search/SearchResultAdapter$GroupViewHolder;
    :goto_0
    return-object v3

    .line 78
    .end local v3    # "view":Landroid/view/View;
    :cond_0
    invoke-virtual {p0, v2}, Lru/cn/tv/mobile/search/SearchResultAdapter;->getItemViewType(I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 79
    iget v4, p0, Lru/cn/tv/mobile/search/SearchResultAdapter;->telecastLayout:I

    invoke-virtual {v1, v4, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 80
    .restart local v3    # "view":Landroid/view/View;
    new-instance v0, Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;

    invoke-direct {v0, v6}, Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;-><init>(Lru/cn/tv/mobile/search/SearchResultAdapter$1;)V

    .line 81
    .local v0, "holder":Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;
    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v0, Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;->image:Landroid/widget/ImageView;

    .line 82
    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v0, Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;->title:Landroid/widget/TextView;

    .line 83
    const v4, 0x7f09006b

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v0, Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;->channelTitle:Landroid/widget/TextView;

    .line 84
    const v4, 0x7f090096

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v0, Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;->dateTime:Landroid/widget/TextView;

    .line 85
    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v0, Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;->accessIndicator:Landroid/view/View;

    .line 86
    const v4, 0x7f09002c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ProgressBar;

    iput-object v4, v0, Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;->airProgress:Landroid/widget/ProgressBar;

    .line 87
    invoke-virtual {v3, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 89
    .end local v0    # "holder":Lru/cn/tv/mobile/search/SearchResultAdapter$TelecastViewHolder;
    .end local v3    # "view":Landroid/view/View;
    :cond_1
    iget v4, p0, Lru/cn/tv/mobile/search/SearchResultAdapter;->channelsLayout:I

    invoke-virtual {v1, v4, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 90
    .restart local v3    # "view":Landroid/view/View;
    new-instance v0, Lru/cn/tv/mobile/search/SearchResultAdapter$ChannelViewHolder;

    invoke-direct {v0, v6}, Lru/cn/tv/mobile/search/SearchResultAdapter$ChannelViewHolder;-><init>(Lru/cn/tv/mobile/search/SearchResultAdapter$1;)V

    .line 91
    .local v0, "holder":Lru/cn/tv/mobile/search/SearchResultAdapter$ChannelViewHolder;
    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v0, Lru/cn/tv/mobile/search/SearchResultAdapter$ChannelViewHolder;->image:Landroid/widget/ImageView;

    .line 92
    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v0, Lru/cn/tv/mobile/search/SearchResultAdapter$ChannelViewHolder;->title:Landroid/widget/TextView;

    .line 93
    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v0, Lru/cn/tv/mobile/search/SearchResultAdapter$ChannelViewHolder;->accessIndicator:Landroid/view/View;

    .line 94
    invoke-virtual {v3, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0
.end method
