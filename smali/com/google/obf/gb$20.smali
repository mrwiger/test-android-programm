.class final Lcom/google/obf/gb$20;
.super Lcom/google/obf/ew;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/gb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/obf/ew",
        "<",
        "Ljava/util/Calendar;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/obf/ew;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/obf/ge;)Ljava/util/Calendar;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2
    invoke-virtual {p1}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    move-result-object v0

    sget-object v1, Lcom/google/obf/gf;->i:Lcom/google/obf/gf;

    if-ne v0, v1, :cond_0

    .line 3
    invoke-virtual {p1}, Lcom/google/obf/ge;->j()V

    .line 4
    const/4 v0, 0x0

    .line 29
    :goto_0
    return-object v0

    .line 5
    :cond_0
    invoke-virtual {p1}, Lcom/google/obf/ge;->c()V

    move v5, v6

    move v4, v6

    move v3, v6

    move v2, v6

    move v1, v6

    .line 12
    :cond_1
    :goto_1
    invoke-virtual {p1}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    move-result-object v0

    sget-object v7, Lcom/google/obf/gf;->d:Lcom/google/obf/gf;

    if-eq v0, v7, :cond_7

    .line 13
    invoke-virtual {p1}, Lcom/google/obf/ge;->g()Ljava/lang/String;

    move-result-object v7

    .line 14
    invoke-virtual {p1}, Lcom/google/obf/ge;->m()I

    move-result v0

    .line 15
    const-string v8, "year"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    move v1, v0

    .line 16
    goto :goto_1

    .line 17
    :cond_2
    const-string v8, "month"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    move v2, v0

    .line 18
    goto :goto_1

    .line 19
    :cond_3
    const-string v8, "dayOfMonth"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    move v3, v0

    .line 20
    goto :goto_1

    .line 21
    :cond_4
    const-string v8, "hourOfDay"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    move v4, v0

    .line 22
    goto :goto_1

    .line 23
    :cond_5
    const-string v8, "minute"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    move v5, v0

    .line 24
    goto :goto_1

    .line 25
    :cond_6
    const-string v8, "second"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    move v6, v0

    .line 26
    goto :goto_1

    .line 28
    :cond_7
    invoke-virtual {p1}, Lcom/google/obf/ge;->d()V

    .line 29
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct/range {v0 .. v6}, Ljava/util/GregorianCalendar;-><init>(IIIIII)V

    goto :goto_0
.end method

.method public a(Lcom/google/obf/gg;Ljava/util/Calendar;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    if-nez p2, :cond_0

    .line 31
    invoke-virtual {p1}, Lcom/google/obf/gg;->f()Lcom/google/obf/gg;

    .line 47
    :goto_0
    return-void

    .line 33
    :cond_0
    invoke-virtual {p1}, Lcom/google/obf/gg;->d()Lcom/google/obf/gg;

    .line 34
    const-string v0, "year"

    invoke-virtual {p1, v0}, Lcom/google/obf/gg;->a(Ljava/lang/String;)Lcom/google/obf/gg;

    .line 35
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/obf/gg;->a(J)Lcom/google/obf/gg;

    .line 36
    const-string v0, "month"

    invoke-virtual {p1, v0}, Lcom/google/obf/gg;->a(Ljava/lang/String;)Lcom/google/obf/gg;

    .line 37
    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/obf/gg;->a(J)Lcom/google/obf/gg;

    .line 38
    const-string v0, "dayOfMonth"

    invoke-virtual {p1, v0}, Lcom/google/obf/gg;->a(Ljava/lang/String;)Lcom/google/obf/gg;

    .line 39
    const/4 v0, 0x5

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/obf/gg;->a(J)Lcom/google/obf/gg;

    .line 40
    const-string v0, "hourOfDay"

    invoke-virtual {p1, v0}, Lcom/google/obf/gg;->a(Ljava/lang/String;)Lcom/google/obf/gg;

    .line 41
    const/16 v0, 0xb

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/obf/gg;->a(J)Lcom/google/obf/gg;

    .line 42
    const-string v0, "minute"

    invoke-virtual {p1, v0}, Lcom/google/obf/gg;->a(Ljava/lang/String;)Lcom/google/obf/gg;

    .line 43
    const/16 v0, 0xc

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/obf/gg;->a(J)Lcom/google/obf/gg;

    .line 44
    const-string v0, "second"

    invoke-virtual {p1, v0}, Lcom/google/obf/gg;->a(Ljava/lang/String;)Lcom/google/obf/gg;

    .line 45
    const/16 v0, 0xd

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/obf/gg;->a(J)Lcom/google/obf/gg;

    .line 46
    invoke-virtual {p1}, Lcom/google/obf/gg;->e()Lcom/google/obf/gg;

    goto :goto_0
.end method

.method public synthetic read(Lcom/google/obf/ge;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    invoke-virtual {p0, p1}, Lcom/google/obf/gb$20;->a(Lcom/google/obf/ge;)Ljava/util/Calendar;

    move-result-object v0

    return-object v0
.end method

.method public synthetic write(Lcom/google/obf/gg;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    check-cast p2, Ljava/util/Calendar;

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/gb$20;->a(Lcom/google/obf/gg;Ljava/util/Calendar;)V

    return-void
.end method
