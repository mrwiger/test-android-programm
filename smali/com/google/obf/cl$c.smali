.class Lcom/google/obf/cl$c;
.super Lcom/google/obf/cl$d;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/cl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/obf/cl;

.field private final b:Lcom/google/obf/dv;

.field private final c:Lcom/google/obf/dw;

.field private final d:I

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Lcom/google/obf/cl;I)V
    .locals 2

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/obf/cl$c;->a:Lcom/google/obf/cl;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/obf/cl$d;-><init>(Lcom/google/obf/cl$1;)V

    .line 2
    new-instance v0, Lcom/google/obf/dv;

    const/4 v1, 0x5

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Lcom/google/obf/dv;-><init>([B)V

    iput-object v0, p0, Lcom/google/obf/cl$c;->b:Lcom/google/obf/dv;

    .line 3
    new-instance v0, Lcom/google/obf/dw;

    invoke-direct {v0}, Lcom/google/obf/dw;-><init>()V

    iput-object v0, p0, Lcom/google/obf/cl$c;->c:Lcom/google/obf/dw;

    .line 4
    iput p2, p0, Lcom/google/obf/cl$c;->d:I

    .line 5
    return-void
.end method

.method private a(Lcom/google/obf/dw;I)I
    .locals 8

    .prologue
    const/16 v2, 0x87

    const/16 v1, 0x81

    .line 89
    const/4 v0, -0x1

    .line 90
    invoke-virtual {p1}, Lcom/google/obf/dw;->d()I

    move-result v3

    add-int/2addr v3, p2

    .line 91
    :goto_0
    invoke-virtual {p1}, Lcom/google/obf/dw;->d()I

    move-result v4

    if-ge v4, v3, :cond_0

    .line 92
    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v4

    .line 93
    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v5

    .line 94
    const/4 v6, 0x5

    if-ne v4, v6, :cond_3

    .line 95
    invoke-virtual {p1}, Lcom/google/obf/dw;->k()J

    move-result-wide v4

    .line 96
    invoke-static {}, Lcom/google/obf/cl;->a()J

    move-result-wide v6

    cmp-long v6, v4, v6

    if-nez v6, :cond_1

    move v0, v1

    .line 110
    :cond_0
    :goto_1
    invoke-virtual {p1, v3}, Lcom/google/obf/dw;->c(I)V

    .line 111
    return v0

    .line 98
    :cond_1
    invoke-static {}, Lcom/google/obf/cl;->d()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-nez v1, :cond_2

    move v0, v2

    .line 99
    goto :goto_1

    .line 100
    :cond_2
    invoke-static {}, Lcom/google/obf/cl;->e()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-nez v1, :cond_0

    .line 101
    const/16 v0, 0x24

    goto :goto_1

    .line 102
    :cond_3
    const/16 v6, 0x6a

    if-ne v4, v6, :cond_5

    move v0, v1

    .line 108
    :cond_4
    :goto_2
    invoke-virtual {p1, v5}, Lcom/google/obf/dw;->d(I)V

    goto :goto_0

    .line 104
    :cond_5
    const/16 v6, 0x7a

    if-ne v4, v6, :cond_6

    move v0, v2

    .line 105
    goto :goto_2

    .line 106
    :cond_6
    const/16 v6, 0x7b

    if-ne v4, v6, :cond_4

    .line 107
    const/16 v0, 0x8a

    goto :goto_2
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 6
    return-void
.end method

.method public a(Lcom/google/obf/dw;ZLcom/google/obf/al;)V
    .locals 8

    .prologue
    .line 7
    if-eqz p2, :cond_0

    .line 8
    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v0

    .line 9
    invoke-virtual {p1, v0}, Lcom/google/obf/dw;->d(I)V

    .line 10
    iget-object v0, p0, Lcom/google/obf/cl$c;->b:Lcom/google/obf/dv;

    const/4 v1, 0x3

    invoke-virtual {p1, v0, v1}, Lcom/google/obf/dw;->a(Lcom/google/obf/dv;I)V

    .line 11
    iget-object v0, p0, Lcom/google/obf/cl$c;->b:Lcom/google/obf/dv;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/google/obf/dv;->b(I)V

    .line 12
    iget-object v0, p0, Lcom/google/obf/cl$c;->b:Lcom/google/obf/dv;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/google/obf/dv;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/obf/cl$c;->e:I

    .line 13
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/cl$c;->f:I

    .line 14
    iget-object v0, p0, Lcom/google/obf/cl$c;->b:Lcom/google/obf/dv;

    iget-object v0, v0, Lcom/google/obf/dv;->a:[B

    const/4 v1, 0x0

    const/4 v2, 0x3

    const/4 v3, -0x1

    invoke-static {v0, v1, v2, v3}, Lcom/google/obf/ea;->a([BIII)I

    move-result v0

    iput v0, p0, Lcom/google/obf/cl$c;->g:I

    .line 15
    iget-object v0, p0, Lcom/google/obf/cl$c;->c:Lcom/google/obf/dw;

    iget v1, p0, Lcom/google/obf/cl$c;->e:I

    invoke-virtual {v0, v1}, Lcom/google/obf/dw;->a(I)V

    .line 16
    :cond_0
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v0

    iget v1, p0, Lcom/google/obf/cl$c;->e:I

    iget v2, p0, Lcom/google/obf/cl$c;->f:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 17
    iget-object v1, p0, Lcom/google/obf/cl$c;->c:Lcom/google/obf/dw;

    iget-object v1, v1, Lcom/google/obf/dw;->a:[B

    iget v2, p0, Lcom/google/obf/cl$c;->f:I

    invoke-virtual {p1, v1, v2, v0}, Lcom/google/obf/dw;->a([BII)V

    .line 18
    iget v1, p0, Lcom/google/obf/cl$c;->f:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/cl$c;->f:I

    .line 19
    iget v0, p0, Lcom/google/obf/cl$c;->f:I

    iget v1, p0, Lcom/google/obf/cl$c;->e:I

    if-ge v0, v1, :cond_2

    .line 88
    :cond_1
    :goto_0
    return-void

    .line 21
    :cond_2
    iget-object v0, p0, Lcom/google/obf/cl$c;->c:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/obf/cl$c;->e:I

    iget v3, p0, Lcom/google/obf/cl$c;->g:I

    invoke-static {v0, v1, v2, v3}, Lcom/google/obf/ea;->a([BIII)I

    move-result v0

    if-nez v0, :cond_1

    .line 23
    iget-object v0, p0, Lcom/google/obf/cl$c;->c:Lcom/google/obf/dw;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/obf/dw;->d(I)V

    .line 24
    iget-object v0, p0, Lcom/google/obf/cl$c;->c:Lcom/google/obf/dw;

    iget-object v1, p0, Lcom/google/obf/cl$c;->b:Lcom/google/obf/dv;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/google/obf/dw;->a(Lcom/google/obf/dv;I)V

    .line 25
    iget-object v0, p0, Lcom/google/obf/cl$c;->b:Lcom/google/obf/dv;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/obf/dv;->b(I)V

    .line 26
    iget-object v0, p0, Lcom/google/obf/cl$c;->b:Lcom/google/obf/dv;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/google/obf/dv;->c(I)I

    move-result v0

    .line 27
    iget-object v1, p0, Lcom/google/obf/cl$c;->c:Lcom/google/obf/dw;

    invoke-virtual {v1, v0}, Lcom/google/obf/dw;->d(I)V

    .line 28
    iget-object v1, p0, Lcom/google/obf/cl$c;->a:Lcom/google/obf/cl;

    invoke-static {v1}, Lcom/google/obf/cl;->a(Lcom/google/obf/cl;)I

    move-result v1

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/obf/cl$c;->a:Lcom/google/obf/cl;

    iget-object v1, v1, Lcom/google/obf/cl;->c:Lcom/google/obf/cf;

    if-nez v1, :cond_3

    .line 29
    iget-object v1, p0, Lcom/google/obf/cl$c;->a:Lcom/google/obf/cl;

    new-instance v2, Lcom/google/obf/cf;

    const/16 v3, 0x15

    invoke-interface {p3, v3}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/obf/cf;-><init>(Lcom/google/obf/ar;)V

    iput-object v2, v1, Lcom/google/obf/cl;->c:Lcom/google/obf/cf;

    .line 30
    :cond_3
    iget v1, p0, Lcom/google/obf/cl$c;->e:I

    add-int/lit8 v1, v1, -0x9

    sub-int v0, v1, v0

    add-int/lit8 v0, v0, -0x4

    move v1, v0

    .line 31
    :goto_1
    if-lez v1, :cond_d

    .line 32
    iget-object v0, p0, Lcom/google/obf/cl$c;->c:Lcom/google/obf/dw;

    iget-object v2, p0, Lcom/google/obf/cl$c;->b:Lcom/google/obf/dv;

    const/4 v3, 0x5

    invoke-virtual {v0, v2, v3}, Lcom/google/obf/dw;->a(Lcom/google/obf/dv;I)V

    .line 33
    iget-object v0, p0, Lcom/google/obf/cl$c;->b:Lcom/google/obf/dv;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/google/obf/dv;->c(I)I

    move-result v0

    .line 34
    iget-object v2, p0, Lcom/google/obf/cl$c;->b:Lcom/google/obf/dv;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/google/obf/dv;->b(I)V

    .line 35
    iget-object v2, p0, Lcom/google/obf/cl$c;->b:Lcom/google/obf/dv;

    const/16 v3, 0xd

    invoke-virtual {v2, v3}, Lcom/google/obf/dv;->c(I)I

    move-result v2

    .line 36
    iget-object v3, p0, Lcom/google/obf/cl$c;->b:Lcom/google/obf/dv;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/google/obf/dv;->b(I)V

    .line 37
    iget-object v3, p0, Lcom/google/obf/cl$c;->b:Lcom/google/obf/dv;

    const/16 v4, 0xc

    invoke-virtual {v3, v4}, Lcom/google/obf/dv;->c(I)I

    move-result v3

    .line 38
    const/4 v4, 0x6

    if-ne v0, v4, :cond_4

    .line 39
    iget-object v0, p0, Lcom/google/obf/cl$c;->c:Lcom/google/obf/dw;

    invoke-direct {p0, v0, v3}, Lcom/google/obf/cl$c;->a(Lcom/google/obf/dw;I)I

    move-result v0

    .line 41
    :goto_2
    add-int/lit8 v3, v3, 0x5

    sub-int v5, v1, v3

    .line 42
    iget-object v1, p0, Lcom/google/obf/cl$c;->a:Lcom/google/obf/cl;

    invoke-static {v1}, Lcom/google/obf/cl;->a(Lcom/google/obf/cl;)I

    move-result v1

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_5

    move v1, v0

    .line 43
    :goto_3
    iget-object v3, p0, Lcom/google/obf/cl$c;->a:Lcom/google/obf/cl;

    iget-object v3, v3, Lcom/google/obf/cl;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_6

    move v1, v5

    .line 44
    goto :goto_1

    .line 40
    :cond_4
    iget-object v4, p0, Lcom/google/obf/cl$c;->c:Lcom/google/obf/dw;

    invoke-virtual {v4, v3}, Lcom/google/obf/dw;->d(I)V

    goto :goto_2

    :cond_5
    move v1, v2

    .line 42
    goto :goto_3

    .line 45
    :cond_6
    sparse-switch v0, :sswitch_data_0

    .line 74
    const/4 v0, 0x0

    .line 75
    :goto_4
    if-eqz v0, :cond_7

    .line 76
    iget-object v3, p0, Lcom/google/obf/cl$c;->a:Lcom/google/obf/cl;

    iget-object v3, v3, Lcom/google/obf/cl;->b:Landroid/util/SparseBooleanArray;

    const/4 v4, 0x1

    invoke-virtual {v3, v1, v4}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 77
    iget-object v1, p0, Lcom/google/obf/cl$c;->a:Lcom/google/obf/cl;

    iget-object v1, v1, Lcom/google/obf/cl;->a:Landroid/util/SparseArray;

    new-instance v3, Lcom/google/obf/cl$b;

    iget-object v4, p0, Lcom/google/obf/cl$c;->a:Lcom/google/obf/cl;

    .line 78
    invoke-static {v4}, Lcom/google/obf/cl;->c(Lcom/google/obf/cl;)Lcom/google/obf/cj;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Lcom/google/obf/cl$b;-><init>(Lcom/google/obf/cb;Lcom/google/obf/cj;)V

    .line 79
    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_7
    move v1, v5

    .line 80
    goto :goto_1

    .line 46
    :sswitch_0
    new-instance v0, Lcom/google/obf/cg;

    invoke-interface {p3, v1}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/google/obf/cg;-><init>(Lcom/google/obf/ar;)V

    goto :goto_4

    .line 48
    :sswitch_1
    new-instance v0, Lcom/google/obf/cg;

    invoke-interface {p3, v1}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/google/obf/cg;-><init>(Lcom/google/obf/ar;)V

    goto :goto_4

    .line 50
    :sswitch_2
    iget-object v0, p0, Lcom/google/obf/cl$c;->a:Lcom/google/obf/cl;

    invoke-static {v0}, Lcom/google/obf/cl;->a(Lcom/google/obf/cl;)I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_8

    const/4 v0, 0x0

    goto :goto_4

    .line 51
    :cond_8
    new-instance v0, Lcom/google/obf/bz;

    invoke-interface {p3, v1}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v3

    new-instance v4, Lcom/google/obf/ai;

    invoke-direct {v4}, Lcom/google/obf/ai;-><init>()V

    invoke-direct {v0, v3, v4}, Lcom/google/obf/bz;-><init>(Lcom/google/obf/ar;Lcom/google/obf/ar;)V

    goto :goto_4

    .line 53
    :sswitch_3
    new-instance v0, Lcom/google/obf/bx;

    invoke-interface {p3, v1}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, v3, v4}, Lcom/google/obf/bx;-><init>(Lcom/google/obf/ar;Z)V

    goto :goto_4

    .line 55
    :sswitch_4
    new-instance v0, Lcom/google/obf/bx;

    invoke-interface {p3, v1}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v0, v3, v4}, Lcom/google/obf/bx;-><init>(Lcom/google/obf/ar;Z)V

    goto :goto_4

    .line 57
    :sswitch_5
    new-instance v0, Lcom/google/obf/ca;

    invoke-interface {p3, v1}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/google/obf/ca;-><init>(Lcom/google/obf/ar;)V

    goto :goto_4

    .line 59
    :sswitch_6
    new-instance v0, Lcom/google/obf/cc;

    invoke-interface {p3, v1}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/google/obf/cc;-><init>(Lcom/google/obf/ar;)V

    goto :goto_4

    .line 61
    :sswitch_7
    iget-object v0, p0, Lcom/google/obf/cl$c;->a:Lcom/google/obf/cl;

    invoke-static {v0}, Lcom/google/obf/cl;->a(Lcom/google/obf/cl;)I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_9

    const/4 v0, 0x0

    goto/16 :goto_4

    .line 62
    :cond_9
    new-instance v4, Lcom/google/obf/cd;

    invoke-interface {p3, v1}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v6

    new-instance v7, Lcom/google/obf/ck;

    iget-object v0, p0, Lcom/google/obf/cl$c;->a:Lcom/google/obf/cl;

    .line 63
    invoke-static {v0}, Lcom/google/obf/cl;->b(Lcom/google/obf/cl;)I

    move-result v0

    invoke-interface {p3, v0}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v0

    invoke-direct {v7, v0}, Lcom/google/obf/ck;-><init>(Lcom/google/obf/ar;)V

    iget-object v0, p0, Lcom/google/obf/cl$c;->a:Lcom/google/obf/cl;

    .line 64
    invoke-static {v0}, Lcom/google/obf/cl;->a(Lcom/google/obf/cl;)I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_5
    iget-object v3, p0, Lcom/google/obf/cl$c;->a:Lcom/google/obf/cl;

    .line 65
    invoke-static {v3}, Lcom/google/obf/cl;->a(Lcom/google/obf/cl;)I

    move-result v3

    and-int/lit8 v3, v3, 0x8

    if-eqz v3, :cond_b

    const/4 v3, 0x1

    :goto_6
    invoke-direct {v4, v6, v7, v0, v3}, Lcom/google/obf/cd;-><init>(Lcom/google/obf/ar;Lcom/google/obf/ck;ZZ)V

    move-object v0, v4

    goto/16 :goto_4

    .line 64
    :cond_a
    const/4 v0, 0x0

    goto :goto_5

    .line 65
    :cond_b
    const/4 v3, 0x0

    goto :goto_6

    .line 67
    :sswitch_8
    new-instance v0, Lcom/google/obf/ce;

    invoke-interface {p3, v1}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v3

    new-instance v4, Lcom/google/obf/ck;

    iget-object v6, p0, Lcom/google/obf/cl$c;->a:Lcom/google/obf/cl;

    .line 68
    invoke-static {v6}, Lcom/google/obf/cl;->b(Lcom/google/obf/cl;)I

    move-result v6

    invoke-interface {p3, v6}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v6

    invoke-direct {v4, v6}, Lcom/google/obf/ck;-><init>(Lcom/google/obf/ar;)V

    invoke-direct {v0, v3, v4}, Lcom/google/obf/ce;-><init>(Lcom/google/obf/ar;Lcom/google/obf/ck;)V

    goto/16 :goto_4

    .line 70
    :sswitch_9
    iget-object v0, p0, Lcom/google/obf/cl$c;->a:Lcom/google/obf/cl;

    invoke-static {v0}, Lcom/google/obf/cl;->a(Lcom/google/obf/cl;)I

    move-result v0

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_c

    .line 71
    iget-object v0, p0, Lcom/google/obf/cl$c;->a:Lcom/google/obf/cl;

    iget-object v0, v0, Lcom/google/obf/cl;->c:Lcom/google/obf/cf;

    goto/16 :goto_4

    .line 72
    :cond_c
    new-instance v0, Lcom/google/obf/cf;

    iget-object v3, p0, Lcom/google/obf/cl$c;->a:Lcom/google/obf/cl;

    invoke-static {v3}, Lcom/google/obf/cl;->b(Lcom/google/obf/cl;)I

    move-result v3

    invoke-interface {p3, v3}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/google/obf/cf;-><init>(Lcom/google/obf/ar;)V

    goto/16 :goto_4

    .line 81
    :cond_d
    iget-object v0, p0, Lcom/google/obf/cl$c;->a:Lcom/google/obf/cl;

    invoke-static {v0}, Lcom/google/obf/cl;->a(Lcom/google/obf/cl;)I

    move-result v0

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_f

    .line 82
    iget-object v0, p0, Lcom/google/obf/cl$c;->a:Lcom/google/obf/cl;

    invoke-static {v0}, Lcom/google/obf/cl;->d(Lcom/google/obf/cl;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 83
    invoke-interface {p3}, Lcom/google/obf/al;->f()V

    .line 87
    :cond_e
    :goto_7
    iget-object v0, p0, Lcom/google/obf/cl$c;->a:Lcom/google/obf/cl;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/obf/cl;->a(Lcom/google/obf/cl;Z)Z

    goto/16 :goto_0

    .line 84
    :cond_f
    iget-object v0, p0, Lcom/google/obf/cl$c;->a:Lcom/google/obf/cl;

    iget-object v0, v0, Lcom/google/obf/cl;->a:Landroid/util/SparseArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V

    .line 85
    iget-object v0, p0, Lcom/google/obf/cl$c;->a:Lcom/google/obf/cl;

    iget-object v0, v0, Lcom/google/obf/cl;->a:Landroid/util/SparseArray;

    iget v1, p0, Lcom/google/obf/cl$c;->d:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V

    .line 86
    invoke-interface {p3}, Lcom/google/obf/al;->f()V

    goto :goto_7

    .line 45
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_6
        0x3 -> :sswitch_0
        0x4 -> :sswitch_1
        0xf -> :sswitch_2
        0x15 -> :sswitch_9
        0x1b -> :sswitch_7
        0x24 -> :sswitch_8
        0x81 -> :sswitch_3
        0x82 -> :sswitch_5
        0x87 -> :sswitch_4
        0x8a -> :sswitch_5
    .end sparse-switch
.end method
