.class public Lcom/samsung/multiscreen/VideoPlayer;
.super Lcom/samsung/multiscreen/Player;
.source "VideoPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;,
        Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;,
        Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerInternalEvents;,
        Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VideoPlayer"

.field private static final VIDEO_PLAYER_CONTROL_RESPONSE:Ljava/lang/String; = "state"

.field private static final VIDEO_PLAYER_INTERNAL_RESPONSE:Ljava/lang/String; = "Video State"

.field private static final VIDEO_PLAYER_QUEUE_EVENT_RESPONSE:Ljava/lang/String; = "queue"


# instance fields
.field private mVideoPlayerListener:Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 1
    .param p1, "service"    # Lcom/samsung/multiscreen/Service;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "appName"    # Ljava/lang/String;

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/multiscreen/Player;-><init>(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;Ljava/lang/String;)V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/multiscreen/VideoPlayer;->mVideoPlayerListener:Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    .line 63
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/multiscreen/VideoPlayer;)Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/VideoPlayer;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/multiscreen/VideoPlayer;->mVideoPlayerListener:Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    return-object v0
.end method


# virtual methods
.method public addOnMessageListener(Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    .prologue
    .line 441
    iput-object p1, p0, Lcom/samsung/multiscreen/VideoPlayer;->mVideoPlayerListener:Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    .line 442
    sget-object v0, Lcom/samsung/multiscreen/VideoPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerNotice"

    new-instance v2, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerMessageListener;-><init>(Lcom/samsung/multiscreen/VideoPlayer;Lcom/samsung/multiscreen/VideoPlayer$1;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->addOnMessageListener(Ljava/lang/String;Lcom/samsung/multiscreen/Channel$OnMessageListener;)V

    .line 443
    return-void
.end method

.method public addToList(Landroid/net/Uri;)V
    .locals 1
    .param p1, "ContentUri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 233
    invoke-virtual {p0, p1, v0, v0}, Lcom/samsung/multiscreen/VideoPlayer;->addToList(Landroid/net/Uri;Ljava/lang/String;Landroid/net/Uri;)V

    .line 234
    return-void
.end method

.method public addToList(Landroid/net/Uri;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "thumbUri"    # Landroid/net/Uri;

    .prologue
    .line 246
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 247
    .local v0, "item":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "uri"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    sget-object v2, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->title:Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    sget-object v2, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->thumbnailUrl:Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 252
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    invoke-virtual {p0, v1}, Lcom/samsung/multiscreen/VideoPlayer;->addToList(Ljava/util/List;)V

    .line 255
    return-void
.end method

.method public addToList(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 263
    .local p1, "videoList":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 264
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/multiscreen/VideoPlayer;->isDebug()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "VideoPlayer"

    const-string v2, "enQueue(): videoList is NULL."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    :cond_1
    :goto_0
    return-void

    .line 271
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/multiscreen/VideoPlayer;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 272
    new-instance v1, Lcom/samsung/multiscreen/VideoPlayer$1;

    invoke-direct {v1, p0, p1}, Lcom/samsung/multiscreen/VideoPlayer$1;-><init>(Lcom/samsung/multiscreen/VideoPlayer;Ljava/util/List;)V

    invoke-virtual {p0, v1}, Lcom/samsung/multiscreen/VideoPlayer;->getDMPStatus(Lcom/samsung/multiscreen/Result;)V

    goto :goto_0

    .line 367
    :cond_3
    iget-object v1, p0, Lcom/samsung/multiscreen/VideoPlayer;->mVideoPlayerListener:Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    if-eqz v1, :cond_1

    .line 368
    new-instance v0, Lcom/samsung/multiscreen/ErrorCode;

    const-string v1, "PLAYER_ERROR_PLAYER_NOT_LOADED"

    invoke-direct {v0, v1}, Lcom/samsung/multiscreen/ErrorCode;-><init>(Ljava/lang/String;)V

    .line 369
    .local v0, "errorCode":Lcom/samsung/multiscreen/ErrorCode;
    iget-object v1, p0, Lcom/samsung/multiscreen/VideoPlayer;->mVideoPlayerListener:Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;

    invoke-virtual {v0}, Lcom/samsung/multiscreen/ErrorCode;->value()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/multiscreen/Error;->create(JLjava/lang/String;Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/multiscreen/VideoPlayer$OnVideoPlayerListener;->onError(Lcom/samsung/multiscreen/Error;)V

    goto :goto_0
.end method

.method public clearList()V
    .locals 5

    .prologue
    .line 200
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 202
    .local v0, "data":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "subEvent"

    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->clear:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 203
    const-string v2, "playerType"

    sget-object v3, Lcom/samsung/multiscreen/Player$ContentType;->video:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 207
    :goto_0
    sget-object v2, Lcom/samsung/multiscreen/VideoPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v3, "playerQueueEvent"

    invoke-virtual {v2, v3, v0}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 208
    return-void

    .line 204
    :catch_0
    move-exception v1

    .line 205
    .local v1, "exception":Ljava/lang/Exception;
    const-string v2, "VideoPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "clearQueue(): Error in parsing JSON object: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public forward()V
    .locals 3

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/samsung/multiscreen/VideoPlayer;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "VideoPlayer"

    const-string v1, "Send Forward"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :cond_0
    sget-object v0, Lcom/samsung/multiscreen/VideoPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerControl"

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->FF:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 121
    return-void
.end method

.method public getList()V
    .locals 5

    .prologue
    .line 186
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 188
    .local v0, "data":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "subEvent"

    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->fetch:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 189
    const-string v2, "playerType"

    sget-object v3, Lcom/samsung/multiscreen/Player$ContentType;->video:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    :goto_0
    sget-object v2, Lcom/samsung/multiscreen/VideoPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v3, "playerQueueEvent"

    invoke-virtual {v2, v3, v0}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 194
    return-void

    .line 190
    :catch_0
    move-exception v1

    .line 191
    .local v1, "exception":Ljava/lang/Exception;
    const-string v2, "VideoPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fetchQueue(): Error in parsing JSON object: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public playContent(Landroid/net/Uri;Lcom/samsung/multiscreen/Result;)V
    .locals 1
    .param p1, "contentUrl"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Ljava/lang/Boolean;>;"
    const/4 v0, 0x0

    .line 72
    invoke-virtual {p0, p1, v0, v0, p2}, Lcom/samsung/multiscreen/VideoPlayer;->playContent(Landroid/net/Uri;Ljava/lang/String;Landroid/net/Uri;Lcom/samsung/multiscreen/Result;)V

    .line 73
    return-void
.end method

.method public playContent(Landroid/net/Uri;Ljava/lang/String;Landroid/net/Uri;Lcom/samsung/multiscreen/Result;)V
    .locals 7
    .param p1, "contentUrl"    # Landroid/net/Uri;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "thumbnailUrl"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p4, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Ljava/lang/Boolean;>;"
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 90
    .local v0, "contentInfo":Lorg/json/JSONObject;
    if-eqz p1, :cond_3

    :try_start_0
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 91
    const-string v3, "uri"

    invoke-virtual {v0, v3, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 98
    if-eqz p2, :cond_0

    .line 99
    sget-object v3, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->title:Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 101
    :cond_0
    if-eqz p3, :cond_1

    .line 102
    sget-object v3, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->thumbnailUrl:Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/VideoPlayer$VideoPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :cond_1
    :goto_0
    sget-object v3, Lcom/samsung/multiscreen/Player$ContentType;->video:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-super {p0, v0, v3, p4}, Lcom/samsung/multiscreen/Player;->playContent(Lorg/json/JSONObject;Lcom/samsung/multiscreen/Player$ContentType;Lcom/samsung/multiscreen/Result;)V

    .line 110
    :cond_2
    :goto_1
    return-void

    .line 93
    :cond_3
    :try_start_1
    new-instance v2, Lcom/samsung/multiscreen/ErrorCode;

    const-string v3, "PLAYER_ERROR_INVALID_URI"

    invoke-direct {v2, v3}, Lcom/samsung/multiscreen/ErrorCode;-><init>(Ljava/lang/String;)V

    .line 94
    .local v2, "error":Lcom/samsung/multiscreen/ErrorCode;
    invoke-virtual {p0}, Lcom/samsung/multiscreen/VideoPlayer;->isDebug()Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "VideoPlayer"

    const-string v4, "There\'s no media url to launch!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    :cond_4
    if-eqz p4, :cond_2

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->value()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v3, v6}, Lcom/samsung/multiscreen/Error;->create(JLjava/lang/String;Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v3

    invoke-interface {p4, v3}, Lcom/samsung/multiscreen/Result;->onError(Lcom/samsung/multiscreen/Error;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 104
    .end local v2    # "error":Lcom/samsung/multiscreen/ErrorCode;
    :catch_0
    move-exception v1

    .line 105
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Lcom/samsung/multiscreen/ErrorCode;

    const-string v3, "PLAYER_ERROR_UNKNOWN"

    invoke-direct {v2, v3}, Lcom/samsung/multiscreen/ErrorCode;-><init>(Ljava/lang/String;)V

    .line 106
    .restart local v2    # "error":Lcom/samsung/multiscreen/ErrorCode;
    invoke-virtual {p0}, Lcom/samsung/multiscreen/VideoPlayer;->isDebug()Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "VideoPlayer"

    const-string v4, "Unable to create JSONObject!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    :cond_5
    if-eqz p4, :cond_1

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->value()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v3, v6}, Lcom/samsung/multiscreen/Error;->create(JLjava/lang/String;Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v3

    invoke-interface {p4, v3}, Lcom/samsung/multiscreen/Result;->onError(Lcom/samsung/multiscreen/Error;)V

    goto :goto_0
.end method

.method public removeFromList(Landroid/net/Uri;)V
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 216
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 218
    .local v0, "data":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "subEvent"

    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->dequeue:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 219
    const-string v2, "playerType"

    sget-object v3, Lcom/samsung/multiscreen/Player$ContentType;->video:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 220
    const-string v2, "uri"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    :goto_0
    sget-object v2, Lcom/samsung/multiscreen/VideoPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v3, "playerQueueEvent"

    invoke-virtual {v2, v3, v0}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 225
    return-void

    .line 221
    :catch_0
    move-exception v1

    .line 222
    .local v1, "exception":Ljava/lang/Exception;
    const-string v2, "VideoPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deQueue(): Error in parsing JSON object: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public repeat()V
    .locals 3

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/samsung/multiscreen/VideoPlayer;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "VideoPlayer"

    const-string v1, "Send repeat"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :cond_0
    sget-object v0, Lcom/samsung/multiscreen/VideoPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerControl"

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->repeat:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 149
    return-void
.end method

.method public rewind()V
    .locals 3

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/samsung/multiscreen/VideoPlayer;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "VideoPlayer"

    const-string v1, "Send Rewind"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    :cond_0
    sget-object v0, Lcom/samsung/multiscreen/VideoPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerControl"

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->RWD:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 129
    return-void
.end method

.method public seekTo(ILjava/util/concurrent/TimeUnit;)V
    .locals 6
    .param p1, "time"    # I
    .param p2, "timeUnit"    # Ljava/util/concurrent/TimeUnit;

    .prologue
    .line 138
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v4, p1

    invoke-virtual {v2, v4, v5, p2}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    .line 139
    .local v0, "seconds":J
    invoke-virtual {p0}, Lcom/samsung/multiscreen/VideoPlayer;->isDebug()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "VideoPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send SeekTo : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " seconds"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    :cond_0
    sget-object v2, Lcom/samsung/multiscreen/VideoPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v3, "playerControl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->seekTo:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v5}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 141
    return-void
.end method

.method public setRepeat(Lcom/samsung/multiscreen/Player$RepeatMode;)V
    .locals 4
    .param p1, "mode"    # Lcom/samsung/multiscreen/Player$RepeatMode;

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/samsung/multiscreen/VideoPlayer;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "VideoPlayer"

    const-string v1, "Send setRepeat"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :cond_0
    sget-object v0, Lcom/samsung/multiscreen/VideoPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerControl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->setRepeat:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/multiscreen/Player$RepeatMode;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 160
    return-void
.end method
