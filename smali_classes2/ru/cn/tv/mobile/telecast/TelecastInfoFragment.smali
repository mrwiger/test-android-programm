.class public Lru/cn/tv/mobile/telecast/TelecastInfoFragment;
.super Landroid/support/v4/app/Fragment;
.source "TelecastInfoFragment.java"


# instance fields
.field private contractorId:J

.field private contractorImage:Landroid/widget/ImageView;

.field private contractorName:Landroid/widget/TextView;

.field private contractorWrapper:Landroid/view/View;

.field private expand:Z

.field private expandIndicator:Landroid/widget/ImageView;

.field private headerView:Landroid/view/View;

.field private list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

.field private listener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

.field private noScheduleText:Landroid/widget/TextView;

.field private progress:Landroid/view/View;

.field private relatedRubric:Lru/cn/api/catalogue/replies/Rubric;

.field private relatedRubricTitle:Landroid/widget/TextView;

.field private telecastDescription:Landroid/widget/TextView;

.field private telecastId:J

.field private telecastInfoWrapper:Landroid/view/View;

.field private telecastTime:Landroid/widget/TextView;

.field private telecastTitle:Landroid/widget/TextView;

.field private viewModel:Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private setContractor(Landroid/database/Cursor;)V
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 199
    iget-object v3, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->contractorWrapper:Landroid/view/View;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_1

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 201
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 202
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 203
    const-string v2, "brand_name"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 204
    .local v1, "title":Ljava/lang/String;
    const-string v2, "image"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 205
    .local v0, "image":Ljava/lang/String;
    iget-object v2, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->contractorName:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    if-eqz v0, :cond_2

    .line 208
    invoke-virtual {p0}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v2

    .line 209
    invoke-virtual {v2, v0}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    .line 210
    invoke-virtual {v2}, Lcom/squareup/picasso/RequestCreator;->fit()Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    new-instance v3, Lru/cn/utils/MaskTransformation;

    .line 211
    invoke-virtual {p0}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f080096

    invoke-direct {v3, v4, v5}, Lru/cn/utils/MaskTransformation;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v2, v3}, Lcom/squareup/picasso/RequestCreator;->transform(Lcom/squareup/picasso/Transformation;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    const v3, 0x7f0802e0

    .line 212
    invoke-virtual {v2, v3}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    iget-object v3, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->contractorImage:Landroid/widget/ImageView;

    .line 213
    invoke-virtual {v2, v3}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 218
    .end local v0    # "image":Ljava/lang/String;
    .end local v1    # "title":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 199
    :cond_1
    const/16 v2, 0x8

    goto :goto_0

    .line 215
    .restart local v0    # "image":Ljava/lang/String;
    .restart local v1    # "title":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->contractorImage:Landroid/widget/ImageView;

    const v3, 0x7f0802df

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method private setRelatedRubric(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 247
    const/4 v1, 0x0

    .line 248
    .local v1, "rubric":Lru/cn/api/catalogue/replies/Rubric;
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 249
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 250
    const-string v2, "data"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 251
    .local v0, "d":Ljava/lang/String;
    invoke-static {v0}, Lru/cn/api/catalogue/replies/Rubric;->fromJson(Ljava/lang/String;)Lru/cn/api/catalogue/replies/Rubric;

    move-result-object v1

    .line 254
    .end local v0    # "d":Ljava/lang/String;
    :cond_0
    invoke-direct {p0, v1}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->setRelatedRubric(Lru/cn/api/catalogue/replies/Rubric;)V

    .line 255
    return-void
.end method

.method private setRelatedRubric(Lru/cn/api/catalogue/replies/Rubric;)V
    .locals 5
    .param p1, "rubric"    # Lru/cn/api/catalogue/replies/Rubric;

    .prologue
    .line 258
    iput-object p1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->relatedRubric:Lru/cn/api/catalogue/replies/Rubric;

    .line 260
    iget-object v2, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->relatedRubric:Lru/cn/api/catalogue/replies/Rubric;

    if-eqz v2, :cond_0

    .line 261
    iget-object v2, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->relatedRubricTitle:Landroid/widget/TextView;

    iget-object v3, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->relatedRubric:Lru/cn/api/catalogue/replies/Rubric;

    iget-object v3, v3, Lru/cn/api/catalogue/replies/Rubric;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 262
    iget-object v2, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->relatedRubric:Lru/cn/api/catalogue/replies/Rubric;

    invoke-virtual {v2}, Lru/cn/api/catalogue/replies/Rubric;->defaultFilter()Landroid/support/v4/util/LongSparseArray;

    move-result-object v0

    .line 263
    .local v0, "options":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Ljava/lang/String;>;"
    iget-object v2, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->relatedRubric:Lru/cn/api/catalogue/replies/Rubric;

    iget-wide v2, v2, Lru/cn/api/catalogue/replies/Rubric;->id:J

    invoke-static {v2, v3, v0}, Lru/cn/api/provider/TvContentProviderContract;->rubricItemsUri(JLandroid/support/v4/util/LongSparseArray;)Landroid/net/Uri;

    move-result-object v1

    .line 265
    .local v1, "uri":Landroid/net/Uri;
    invoke-static {v1}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->newInstance(Landroid/net/Uri;)Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    move-result-object v2

    iput-object v2, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    .line 266
    iget-object v2, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    iget-object v3, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->listener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    invoke-virtual {v2, v3}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->setOnSelectedListener(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)V

    .line 267
    iget-object v2, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    new-instance v3, Lru/cn/tv/mobile/telecast/TelecastInfoFragment$$Lambda$4;

    invoke-direct {v3, p0}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment$$Lambda$4;-><init>(Lru/cn/tv/mobile/telecast/TelecastInfoFragment;)V

    invoke-virtual {v2, v3}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->setOnLoadListener(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastsLoadedListener;)V

    .line 275
    .end local v0    # "options":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Ljava/lang/String;>;"
    .end local v1    # "uri":Landroid/net/Uri;
    :goto_0
    iget-object v2, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    iget-object v3, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->headerView:Landroid/view/View;

    invoke-virtual {v2, v3}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->setHeaderView(Landroid/view/View;)V

    .line 277
    invoke-virtual {p0}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 278
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    const v3, 0x7f090085

    iget-object v4, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    .line 279
    invoke-virtual {v2, v3, v4}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    .line 280
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 281
    return-void

    .line 271
    :cond_0
    new-instance v2, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    invoke-direct {v2}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;-><init>()V

    iput-object v2, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    goto :goto_0
.end method

.method private setTelecast(Landroid/database/Cursor;)V
    .locals 11
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/16 v8, 0x8

    const/4 v10, 0x0

    .line 221
    iget-object v7, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->progress:Landroid/view/View;

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 223
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-lez v7, :cond_0

    .line 224
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 226
    const-string v7, "data"

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 227
    .local v2, "t":Ljava/lang/String;
    invoke-static {v2}, Lru/cn/api/tv/replies/Telecast;->fromJson(Ljava/lang/String;)Lru/cn/api/tv/replies/Telecast;

    move-result-object v3

    .line 229
    .local v3, "telecast":Lru/cn/api/tv/replies/Telecast;
    iget-object v6, v3, Lru/cn/api/tv/replies/Telecast;->title:Ljava/lang/String;

    .line 230
    .local v6, "title":Ljava/lang/String;
    iget-object v1, v3, Lru/cn/api/tv/replies/Telecast;->description:Ljava/lang/String;

    .line 231
    .local v1, "description":Ljava/lang/String;
    iget-object v7, v3, Lru/cn/api/tv/replies/Telecast;->date:Lru/cn/api/tv/replies/DateTime;

    invoke-virtual {v7}, Lru/cn/api/tv/replies/DateTime;->toSeconds()J

    move-result-wide v4

    .line 233
    .local v4, "time":J
    invoke-static {}, Lru/cn/utils/Utils;->getCalendar()Ljava/util/Calendar;

    move-result-object v0

    .line 234
    .local v0, "c":Ljava/util/Calendar;
    const-wide/16 v8, 0x3e8

    mul-long/2addr v8, v4

    invoke-virtual {v0, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 235
    iget-object v7, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->telecastInfoWrapper:Landroid/view/View;

    invoke-virtual {v7, v10}, Landroid/view/View;->setVisibility(I)V

    .line 237
    iget-object v7, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->telecastTitle:Landroid/widget/TextView;

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 238
    iget-object v7, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->telecastDescription:Landroid/widget/TextView;

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 239
    invoke-virtual {p0, v10}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->hideDescription(I)V

    .line 240
    iget-object v7, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->telecastTime:Landroid/widget/TextView;

    const-string v8, "d MMMM, HH:mm"

    invoke-static {v0, v8}, Lru/cn/utils/Utils;->format(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    .end local v0    # "c":Ljava/util/Calendar;
    .end local v1    # "description":Ljava/lang/String;
    .end local v2    # "t":Ljava/lang/String;
    .end local v3    # "telecast":Lru/cn/api/tv/replies/Telecast;
    .end local v4    # "time":J
    .end local v6    # "title":Ljava/lang/String;
    :goto_0
    return-void

    .line 242
    :cond_0
    iget-object v7, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->telecastInfoWrapper:Landroid/view/View;

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private showDescription(I)V
    .locals 7
    .param p1, "duration"    # I

    .prologue
    const/16 v6, 0x32

    const/4 v5, 0x1

    .line 174
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    .line 175
    const/4 p1, 0x0

    .line 178
    :cond_0
    if-nez p1, :cond_1

    .line 179
    iget-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->telecastDescription:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 186
    :goto_0
    iget-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->expandIndicator:Landroid/widget/ImageView;

    const v2, 0x7f0802f2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 187
    iput-boolean v5, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->expand:Z

    .line 188
    return-void

    .line 182
    :cond_1
    iget-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->telecastDescription:Landroid/widget/TextView;

    const-string v2, "maxLines"

    new-array v3, v5, [I

    const/4 v4, 0x0

    aput v6, v3, v4

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 183
    .local v0, "animation":Landroid/animation/ObjectAnimator;
    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0
.end method

.method private toggleDescription()V
    .locals 2

    .prologue
    const/16 v1, 0xc8

    .line 191
    iget-boolean v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->expand:Z

    if-eqz v0, :cond_0

    .line 192
    invoke-virtual {p0, v1}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->hideDescription(I)V

    .line 196
    :goto_0
    return-void

    .line 194
    :cond_0
    invoke-direct {p0, v1}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->showDescription(I)V

    goto :goto_0
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$TelecastInfoFragment(Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->setContractor(Landroid/database/Cursor;)V

    return-void
.end method

.method final bridge synthetic bridge$lambda$1$TelecastInfoFragment(Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->setTelecast(Landroid/database/Cursor;)V

    return-void
.end method

.method final bridge synthetic bridge$lambda$2$TelecastInfoFragment(Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->setRelatedRubric(Landroid/database/Cursor;)V

    return-void
.end method

.method public getRelatedRubric()Lru/cn/api/catalogue/replies/Rubric;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->relatedRubric:Lru/cn/api/catalogue/replies/Rubric;

    return-object v0
.end method

.method public hideDescription(I)V
    .locals 6
    .param p1, "duration"    # I

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 157
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    .line 158
    const/4 p1, 0x0

    .line 161
    :cond_0
    if-nez p1, :cond_1

    .line 162
    iget-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->telecastDescription:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 169
    :goto_0
    iget-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->expandIndicator:Landroid/widget/ImageView;

    const v2, 0x7f0802f3

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 170
    iput-boolean v4, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->expand:Z

    .line 171
    return-void

    .line 165
    :cond_1
    iget-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->telecastDescription:Landroid/widget/TextView;

    const-string v2, "maxLines"

    const/4 v3, 0x1

    new-array v3, v3, [I

    aput v5, v3, v4

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 166
    .local v0, "animation":Landroid/animation/ObjectAnimator;
    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0
.end method

.method public hideNoScheduleText()V
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->noScheduleText:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 154
    return-void
.end method

.method final synthetic lambda$onCreateView$0$TelecastInfoFragment(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 86
    invoke-direct {p0}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->toggleDescription()V

    return-void
.end method

.method final synthetic lambda$setRelatedRubric$1$TelecastInfoFragment()V
    .locals 2

    .prologue
    .line 268
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->relatedRubricTitle:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 269
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 60
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 62
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v0

    const-class v1, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;

    invoke-static {p0, v0, v1}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;

    iput-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->viewModel:Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;

    .line 64
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->viewModel:Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;

    invoke-virtual {v0}, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->contractor()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/mobile/telecast/TelecastInfoFragment$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment$$Lambda$0;-><init>(Lru/cn/tv/mobile/telecast/TelecastInfoFragment;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 65
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->viewModel:Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;

    invoke-virtual {v0}, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->telecast()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/mobile/telecast/TelecastInfoFragment$$Lambda$1;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment$$Lambda$1;-><init>(Lru/cn/tv/mobile/telecast/TelecastInfoFragment;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 66
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->viewModel:Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;

    invoke-virtual {v0}, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->relatedRubric()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/mobile/telecast/TelecastInfoFragment$$Lambda$2;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment$$Lambda$2;-><init>(Lru/cn/tv/mobile/telecast/TelecastInfoFragment;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 68
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->viewModel:Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;

    iget-wide v2, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->contractorId:J

    invoke-virtual {v0, v2, v3}, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->setContractor(J)V

    .line 69
    iget-wide v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->telecastId:J

    invoke-virtual {p0, v0, v1}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->setTelecast(J)V

    .line 70
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 74
    const v1, 0x7f0c00b5

    invoke-virtual {p1, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->headerView:Landroid/view/View;

    .line 76
    iget-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->headerView:Landroid/view/View;

    const v2, 0x7f090088

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->contractorWrapper:Landroid/view/View;

    .line 77
    iget-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->headerView:Landroid/view/View;

    const v2, 0x7f090087

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->contractorImage:Landroid/widget/ImageView;

    .line 78
    iget-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->headerView:Landroid/view/View;

    const v2, 0x7f090089

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->contractorName:Landroid/widget/TextView;

    .line 80
    iget-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->headerView:Landroid/view/View;

    const v2, 0x7f0901c7

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->telecastInfoWrapper:Landroid/view/View;

    .line 81
    iget-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->headerView:Landroid/view/View;

    const v2, 0x7f0901c9

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->telecastTitle:Landroid/widget/TextView;

    .line 82
    iget-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->headerView:Landroid/view/View;

    const v2, 0x7f0901c8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->telecastTime:Landroid/widget/TextView;

    .line 83
    iget-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->headerView:Landroid/view/View;

    const v2, 0x7f0901c6

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->telecastDescription:Landroid/widget/TextView;

    .line 84
    iget-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->headerView:Landroid/view/View;

    const v2, 0x7f0900cf

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->expandIndicator:Landroid/widget/ImageView;

    .line 86
    new-instance v0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment$$Lambda$3;

    invoke-direct {v0, p0}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment$$Lambda$3;-><init>(Lru/cn/tv/mobile/telecast/TelecastInfoFragment;)V

    .line 88
    .local v0, "expandListener":Landroid/view/View$OnClickListener;
    iget-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->telecastDescription:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    iget-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->expandIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    iget-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->headerView:Landroid/view/View;

    const v2, 0x7f09017d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->relatedRubricTitle:Landroid/widget/TextView;

    .line 92
    iget-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->headerView:Landroid/view/View;

    const v2, 0x7f09014c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->noScheduleText:Landroid/widget/TextView;

    .line 93
    const v1, 0x7f0c00b4

    invoke-virtual {p1, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 98
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 99
    invoke-virtual {p0}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->hideNoScheduleText()V

    .line 100
    const v0, 0x7f090175

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->progress:Landroid/view/View;

    .line 101
    return-void
.end method

.method public setContractor(J)V
    .locals 3
    .param p1, "contractorId"    # J

    .prologue
    .line 137
    iput-wide p1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->contractorId:J

    .line 138
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->viewModel:Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;

    if-nez v0, :cond_1

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->viewModel:Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;

    invoke-virtual {v0, p1, p2}, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->setContractor(J)V

    .line 142
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 143
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->contractorWrapper:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setListener(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)V
    .locals 1
    .param p1, "listener"    # Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    .prologue
    .line 104
    iput-object p1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->listener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    .line 105
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    invoke-virtual {v0, p1}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->setOnSelectedListener(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)V

    .line 108
    :cond_0
    return-void
.end method

.method public setTelecast(J)V
    .locals 3
    .param p1, "telecastId"    # J

    .prologue
    const/16 v2, 0x8

    .line 115
    iput-wide p1, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->telecastId:J

    .line 116
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->viewModel:Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;

    if-nez v0, :cond_1

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->viewModel:Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;

    invoke-virtual {v0, p1, p2}, Lru/cn/tv/mobile/telecast/TelecastInfoViewModel;->setTelecast(J)V

    .line 121
    invoke-virtual {p0}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->relatedRubricTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 125
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    if-eqz v0, :cond_2

    .line 126
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->list:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 129
    :cond_2
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_3

    .line 130
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->telecastInfoWrapper:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 132
    :cond_3
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->progress:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public showNoScheduleText()V
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->noScheduleText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 149
    iget-object v0, p0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->noScheduleText:Landroid/widget/TextView;

    const v1, 0x7f0e00e7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 150
    return-void
.end method
