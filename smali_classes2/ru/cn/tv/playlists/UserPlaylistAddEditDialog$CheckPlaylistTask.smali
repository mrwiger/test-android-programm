.class Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;
.super Landroid/os/AsyncTask;
.source "UserPlaylistAddEditDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/playlists/UserPlaylistAddEditDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CheckPlaylistTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private location:Ljava/lang/String;

.field final synthetic this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/playlists/UserPlaylistAddEditDialog;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "location"    # Ljava/lang/String;

    .prologue
    .line 104
    iput-object p1, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    .line 105
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 106
    iput-object p2, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->title:Ljava/lang/String;

    .line 107
    iput-object p3, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->location:Ljava/lang/String;

    .line 108
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 99
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 123
    iget-object v3, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    invoke-virtual {v3}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    if-nez v3, :cond_1

    .line 159
    :cond_0
    :goto_0
    return-object v2

    .line 128
    :cond_1
    :try_start_0
    iget-object v3, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->location:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 129
    .local v1, "playlistUri":Landroid/net/Uri;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/net/Uri;->isAbsolute()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Landroid/net/Uri;->isHierarchical()Z

    move-result v3

    if-nez v3, :cond_3

    .line 130
    :cond_2
    const-string v2, "format"

    goto :goto_0

    .line 132
    :cond_3
    iget-object v3, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->location:Ljava/lang/String;

    invoke-static {v3}, Lru/cn/api/iptv/IptvLoader;->getUserChannels(Ljava/lang/String;)Ljava/util/List;

    .line 134
    iget-object v3, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    invoke-static {v3}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->access$400(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;)Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

    move-result-object v3

    sget-object v4, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;->newPlaylist:Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

    if-ne v3, v4, :cond_4

    .line 135
    iget-object v3, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    iget-object v4, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->title:Ljava/lang/String;

    iget-object v5, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->location:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->access$500(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 136
    const-string v2, "playlist"

    goto :goto_0

    .line 139
    :cond_4
    iget-object v3, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    iget-object v4, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->location:Ljava/lang/String;

    iget-object v5, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->title:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->access$600(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 140
    const-string v2, "playlist"
    :try_end_0
    .catch Lru/cn/utils/http/NetworkException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lru/cn/api/iptv/M3UParserException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lru/cn/api/BaseAPI$ParseException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 143
    .end local v1    # "playlistUri":Landroid/net/Uri;
    :catch_0
    move-exception v0

    .line 144
    .local v0, "e":Lru/cn/utils/http/NetworkException;
    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .line 145
    const-string v2, "net"

    goto :goto_0

    .line 146
    .end local v0    # "e":Lru/cn/utils/http/NetworkException;
    :catch_1
    move-exception v0

    .line 147
    .local v0, "e":Lru/cn/api/iptv/M3UParserException;
    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .line 148
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "line:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lru/cn/api/iptv/M3UParserException;->line:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 150
    .end local v0    # "e":Lru/cn/api/iptv/M3UParserException;
    :catch_2
    move-exception v0

    .line 151
    .local v0, "e":Lru/cn/api/BaseAPI$ParseException;
    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .line 152
    const-string v2, "parse"

    goto :goto_0

    .line 154
    .end local v0    # "e":Lru/cn/api/BaseAPI$ParseException;
    :catch_3
    move-exception v0

    .line 155
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .line 156
    const-string v2, "net"

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 99
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 6
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 164
    iget-object v3, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    invoke-virtual {v3}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    if-nez v3, :cond_0

    .line 198
    :goto_0
    return-void

    .line 168
    :cond_0
    iget-object v3, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    invoke-static {v3}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->access$300(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 169
    iget-object v3, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    invoke-static {v3}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->access$200(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 170
    iget-object v3, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    invoke-static {v3}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->access$200(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->requestFocus()Z

    .line 171
    iget-object v3, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    invoke-static {v3}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->access$100(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 172
    iget-object v3, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    invoke-static {v3}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->access$000(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 174
    const/4 v0, 0x0

    .line 175
    .local v0, "errorMessage":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 176
    const-string v3, "net"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 177
    iget-object v3, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    const v4, 0x7f0e0183

    invoke-virtual {v3, v4}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 193
    :cond_1
    :goto_1
    if-eqz v0, :cond_7

    .line 194
    iget-object v3, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    invoke-virtual {v3}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3, v0}, Lru/cn/tv/errors/ErrorDialog;->create(Landroid/content/Context;Ljava/lang/CharSequence;)Landroid/app/Dialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    goto :goto_0

    .line 178
    :cond_2
    const-string v3, "parse"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 179
    iget-object v3, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    const v4, 0x7f0e0181

    invoke-virtual {v3, v4}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 180
    :cond_3
    const-string v3, "playlist"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 181
    iget-object v3, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    const v4, 0x7f0e0184

    invoke-virtual {v3, v4}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 182
    :cond_4
    const-string v3, "line"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 183
    const/4 v3, 0x5

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 184
    .local v2, "line":I
    iget-object v3, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    const v4, 0x7f0e0182

    invoke-virtual {v3, v4}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 185
    .local v1, "format":Ljava/lang/String;
    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x0

    add-int/lit8 v5, v2, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 186
    goto :goto_1

    .end local v1    # "format":Ljava/lang/String;
    .end local v2    # "line":I
    :cond_5
    const-string v3, "format"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 187
    iget-object v3, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    const v4, 0x7f0e0185

    invoke-virtual {v3, v4}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 189
    :cond_6
    iget-object v3, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    const v4, 0x7f0e017f

    invoke-virtual {v3, v4}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 197
    :cond_7
    iget-object v3, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    invoke-virtual {v3}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->dismiss()V

    goto/16 :goto_0
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 112
    iget-object v0, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    invoke-virtual {v0}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 119
    :goto_0
    return-void

    .line 115
    :cond_0
    iget-object v0, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    invoke-static {v0}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->access$000(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    invoke-static {v0}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->access$100(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 117
    iget-object v0, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    invoke-static {v0}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->access$200(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 118
    iget-object v0, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->this$0:Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    invoke-static {v0}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->access$300(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
