.class public final Lcom/yandex/metrica/impl/ob/eb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/yandex/metrica/impl/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ob/eb$a;
    }
.end annotation


# static fields
.field public static a:J

.field private static volatile b:Lcom/yandex/metrica/impl/ob/eb;

.field private static final c:Ljava/lang/Object;


# instance fields
.field private final d:Landroid/content/Context;

.field private e:Lcom/yandex/metrica/impl/ob/ec;

.field private final f:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private g:Z

.field private h:Lcom/yandex/metrica/impl/ob/df;

.field private i:Lcom/yandex/metrica/impl/ob/dv;

.field private j:Lcom/yandex/metrica/impl/ob/ej;

.field private k:Lcom/yandex/metrica/impl/ob/eb$a;

.field private l:Ljava/lang/Runnable;

.field private m:Lcom/yandex/metrica/impl/ob/ct;

.field private n:Lcom/yandex/metrica/impl/ob/cs;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 67
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/yandex/metrica/impl/ob/eb;->a:J

    .line 69
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/yandex/metrica/impl/ob/eb;->c:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 96
    new-instance v2, Lcom/yandex/metrica/impl/ob/ec;

    invoke-direct {v2}, Lcom/yandex/metrica/impl/ob/ec;-><init>()V

    new-instance v3, Lcom/yandex/metrica/impl/ob/df;

    .line 102
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/cq;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/cq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/cq;->b()Lcom/yandex/metrica/impl/ob/cr;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/yandex/metrica/impl/ob/df;-><init>(Lcom/yandex/metrica/impl/ob/cr;)V

    new-instance v4, Lcom/yandex/metrica/impl/ob/eb$a;

    invoke-direct {v4}, Lcom/yandex/metrica/impl/ob/eb$a;-><init>()V

    .line 104
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/cq;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/cq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/cq;->f()Lcom/yandex/metrica/impl/ob/ct;

    move-result-object v5

    .line 105
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/cq;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/cq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/cq;->g()Lcom/yandex/metrica/impl/ob/cs;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    .line 96
    invoke-direct/range {v0 .. v6}, Lcom/yandex/metrica/impl/ob/eb;-><init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/ec;Lcom/yandex/metrica/impl/ob/df;Lcom/yandex/metrica/impl/ob/eb$a;Lcom/yandex/metrica/impl/ob/ct;Lcom/yandex/metrica/impl/ob/cs;)V

    .line 107
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/ec;Lcom/yandex/metrica/impl/ob/df;Lcom/yandex/metrica/impl/ob/eb$a;Lcom/yandex/metrica/impl/ob/ct;Lcom/yandex/metrica/impl/ob/cs;)V
    .locals 1

    .prologue
    .line 280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/yandex/metrica/impl/ob/eb;->g:Z

    .line 282
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/eb;->d:Landroid/content/Context;

    .line 283
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/eb;->e:Lcom/yandex/metrica/impl/ob/ec;

    .line 284
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/eb;->h:Lcom/yandex/metrica/impl/ob/df;

    .line 285
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->f:Ljava/util/WeakHashMap;

    .line 286
    iput-object p4, p0, Lcom/yandex/metrica/impl/ob/eb;->k:Lcom/yandex/metrica/impl/ob/eb$a;

    .line 287
    invoke-virtual {p3}, Lcom/yandex/metrica/impl/ob/df;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/yandex/metrica/impl/ob/eb;->g:Z

    .line 288
    iput-object p5, p0, Lcom/yandex/metrica/impl/ob/eb;->m:Lcom/yandex/metrica/impl/ob/ct;

    .line 289
    iput-object p6, p0, Lcom/yandex/metrica/impl/ob/eb;->n:Lcom/yandex/metrica/impl/ob/cs;

    .line 290
    return-void
.end method

.method public static a([B)Landroid/location/Location;
    .locals 3

    .prologue
    .line 219
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 222
    const/4 v0, 0x0

    :try_start_0
    array-length v2, p0

    invoke-virtual {v1, p0, v0, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 223
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 224
    const-class v0, Landroid/location/Location;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 231
    :goto_0
    return-object v0

    .line 228
    :catch_0
    move-exception v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 231
    const/4 v0, 0x0

    goto :goto_0

    .line 228
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 229
    throw v0
.end method

.method public static a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/eb;
    .locals 3

    .prologue
    .line 53
    sget-object v0, Lcom/yandex/metrica/impl/ob/eb;->b:Lcom/yandex/metrica/impl/ob/eb;

    if-nez v0, :cond_1

    .line 54
    sget-object v1, Lcom/yandex/metrica/impl/ob/eb;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 55
    :try_start_0
    sget-object v0, Lcom/yandex/metrica/impl/ob/eb;->b:Lcom/yandex/metrica/impl/ob/eb;

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Lcom/yandex/metrica/impl/ob/eb;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/yandex/metrica/impl/ob/eb;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/eb;->b:Lcom/yandex/metrica/impl/ob/eb;

    .line 58
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    :cond_1
    sget-object v0, Lcom/yandex/metrica/impl/ob/eb;->b:Lcom/yandex/metrica/impl/ob/eb;

    return-object v0

    .line 58
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/ob/eb;)Lcom/yandex/metrica/impl/ob/ej;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->j:Lcom/yandex/metrica/impl/ob/ej;

    return-object v0
.end method

.method public static a(Landroid/location/Location;)[B
    .locals 3

    .prologue
    .line 203
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 204
    const/4 v0, 0x0

    new-array v0, v0, [B

    .line 207
    :try_start_0
    invoke-virtual {v1, p0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 208
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 212
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 215
    :goto_0
    return-object v0

    .line 212
    :catch_0
    move-exception v2

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 213
    throw v0
.end method

.method static synthetic b(Lcom/yandex/metrica/impl/ob/eb;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/eb;->f()V

    return-void
.end method

.method static synthetic c(Lcom/yandex/metrica/impl/ob/eb;)Lcom/yandex/metrica/impl/ob/dv;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->i:Lcom/yandex/metrica/impl/ob/dv;

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 199
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->e:Lcom/yandex/metrica/impl/ob/ec;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/eb;->l:Ljava/lang/Runnable;

    sget-wide v2, Lcom/yandex/metrica/impl/ob/eb;->a:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/yandex/metrica/impl/ob/ec;->a(Ljava/lang/Runnable;J)V

    .line 200
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->e:Lcom/yandex/metrica/impl/ob/ec;

    new-instance v1, Lcom/yandex/metrica/impl/ob/eb$1;

    invoke-direct {v1, p0}, Lcom/yandex/metrica/impl/ob/eb$1;-><init>(Lcom/yandex/metrica/impl/ob/eb;)V

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/ec;->execute(Ljava/lang/Runnable;)V

    .line 126
    return-void
.end method

.method public a(Lcom/yandex/metrica/impl/ob/dv;)V
    .locals 2

    .prologue
    .line 259
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/eb;->i:Lcom/yandex/metrica/impl/ob/dv;

    .line 260
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->e:Lcom/yandex/metrica/impl/ob/ec;

    new-instance v1, Lcom/yandex/metrica/impl/ob/eb$3;

    invoke-direct {v1, p0}, Lcom/yandex/metrica/impl/ob/eb$3;-><init>(Lcom/yandex/metrica/impl/ob/eb;)V

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/ec;->execute(Ljava/lang/Runnable;)V

    .line 268
    return-void
.end method

.method public declared-synchronized a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 131
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/ob/eb;->g:Z

    if-eqz v0, :cond_1

    .line 132
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->f:Ljava/util/WeakHashMap;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->j:Lcom/yandex/metrica/impl/ob/ej;

    if-nez v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->k:Lcom/yandex/metrica/impl/ob/eb$a;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/eb;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/eb;->e:Lcom/yandex/metrica/impl/ob/ec;

    .line 135
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/ec;->a()Landroid/os/Looper;

    move-result-object v2

    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/eb;->i:Lcom/yandex/metrica/impl/ob/dv;

    iget-object v4, p0, Lcom/yandex/metrica/impl/ob/eb;->m:Lcom/yandex/metrica/impl/ob/ct;

    iget-object v5, p0, Lcom/yandex/metrica/impl/ob/eb;->n:Lcom/yandex/metrica/impl/ob/cs;

    .line 134
    invoke-virtual/range {v0 .. v5}, Lcom/yandex/metrica/impl/ob/eb$a;->a(Landroid/content/Context;Landroid/os/Looper;Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/ct;Lcom/yandex/metrica/impl/ob/cs;)Lcom/yandex/metrica/impl/ob/ej;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->j:Lcom/yandex/metrica/impl/ob/ej;

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->j:Lcom/yandex/metrica/impl/ob/ej;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/ej;->e()V

    .line 1178
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->l:Ljava/lang/Runnable;

    if-nez v0, :cond_1

    .line 1179
    new-instance v0, Lcom/yandex/metrica/impl/ob/eb$2;

    invoke-direct {v0, p0}, Lcom/yandex/metrica/impl/ob/eb$2;-><init>(Lcom/yandex/metrica/impl/ob/eb;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->l:Ljava/lang/Runnable;

    .line 1188
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/eb;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    :cond_1
    monitor-exit p0

    return-void

    .line 131
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/lang/Object;ZZ)V
    .locals 2

    .prologue
    .line 235
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/ob/eb;->g:Z

    if-eq v0, p3, :cond_0

    .line 236
    if-eqz p2, :cond_2

    .line 237
    iput-boolean p3, p0, Lcom/yandex/metrica/impl/ob/eb;->g:Z

    .line 238
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->h:Lcom/yandex/metrica/impl/ob/df;

    iget-boolean v1, p0, Lcom/yandex/metrica/impl/ob/eb;->g:Z

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/df;->a(Z)V

    .line 239
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/ob/eb;->g:Z

    if-eqz v0, :cond_1

    .line 241
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/ob/eb;->a(Ljava/lang/Object;)V

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 244
    :cond_1
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/eb;->b()V

    goto :goto_0

    .line 246
    :cond_2
    if-nez p3, :cond_3

    .line 248
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/ob/eb;->b(Ljava/lang/Object;)V

    goto :goto_0

    .line 249
    :cond_3
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/ob/eb;->g:Z

    if-eqz v0, :cond_0

    .line 251
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/ob/eb;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method declared-synchronized b()V
    .locals 1

    .prologue
    .line 150
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->f:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->clear()V

    .line 151
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/eb;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    monitor-exit p0

    return-void

    .line 150
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 143
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->f:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/eb;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    monitor-exit p0

    return-void

    .line 143
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method c()V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->j:Lcom/yandex/metrica/impl/ob/ej;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->f:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->j:Lcom/yandex/metrica/impl/ob/ej;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/ej;->f()V

    .line 1193
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->l:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1194
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->e:Lcom/yandex/metrica/impl/ob/ec;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/eb;->l:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/ec;->a(Ljava/lang/Runnable;)V

    .line 160
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->j:Lcom/yandex/metrica/impl/ob/ej;

    .line 162
    :cond_1
    return-void
.end method

.method public d()Landroid/location/Location;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->j:Lcom/yandex/metrica/impl/ob/ej;

    .line 167
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/ej;->b()Landroid/location/Location;

    move-result-object v0

    goto :goto_0
.end method

.method public e()Landroid/location/Location;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eb;->j:Lcom/yandex/metrica/impl/ob/ej;

    .line 174
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/ej;->c()Landroid/location/Location;

    move-result-object v0

    goto :goto_0
.end method
