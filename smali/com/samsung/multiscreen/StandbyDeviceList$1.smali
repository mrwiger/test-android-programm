.class Lcom/samsung/multiscreen/StandbyDeviceList$1;
.super Ljava/util/TimerTask;
.source "StandbyDeviceList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/multiscreen/StandbyDeviceList;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/multiscreen/StandbyDeviceList;


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/StandbyDeviceList;)V
    .locals 0
    .param p1, "this$0"    # Lcom/samsung/multiscreen/StandbyDeviceList;

    .prologue
    .line 203
    iput-object p1, p0, Lcom/samsung/multiscreen/StandbyDeviceList$1;->this$0:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 207
    iget-object v3, p0, Lcom/samsung/multiscreen/StandbyDeviceList$1;->this$0:Lcom/samsung/multiscreen/StandbyDeviceList;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$502(Lcom/samsung/multiscreen/StandbyDeviceList;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 208
    iget-object v3, p0, Lcom/samsung/multiscreen/StandbyDeviceList$1;->this$0:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-static {v3}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$600(Lcom/samsung/multiscreen/StandbyDeviceList;)Ljava/util/List;

    move-result-object v2

    .line 209
    .local v2, "standbyServices":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/multiscreen/Service;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 210
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/multiscreen/Service;

    .line 211
    .local v1, "standbyService":Lcom/samsung/multiscreen/Service;
    if-eqz v1, :cond_0

    .line 212
    iget-object v3, p0, Lcom/samsung/multiscreen/StandbyDeviceList$1;->this$0:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-static {v3}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$000(Lcom/samsung/multiscreen/StandbyDeviceList;)Lcom/samsung/multiscreen/Search$SearchListener;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/samsung/multiscreen/Search$SearchListener;->onFound(Lcom/samsung/multiscreen/Service;)V

    .line 209
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 215
    .end local v1    # "standbyService":Lcom/samsung/multiscreen/Service;
    :cond_1
    return-void
.end method
