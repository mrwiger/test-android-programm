.class Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;
.super Ljava/lang/Object;
.source "AudioPlayer.java"

# interfaces
.implements Lcom/samsung/multiscreen/Channel$OnMessageListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/multiscreen/AudioPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnAudioPlayerMessageListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/multiscreen/AudioPlayer;


# direct methods
.method private constructor <init>(Lcom/samsung/multiscreen/AudioPlayer;)V
    .locals 0

    .prologue
    .line 524
    iput-object p1, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/multiscreen/AudioPlayer;Lcom/samsung/multiscreen/AudioPlayer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/multiscreen/AudioPlayer;
    .param p2, "x1"    # Lcom/samsung/multiscreen/AudioPlayer$1;

    .prologue
    .line 524
    invoke-direct {p0, p1}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;-><init>(Lcom/samsung/multiscreen/AudioPlayer;)V

    return-void
.end method

.method private handlePlayerControlResponse(Ljava/lang/String;)V
    .locals 5
    .param p1, "response"    # Ljava/lang/String;

    .prologue
    .line 658
    :try_start_0
    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->play:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 659
    iget-object v2, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-static {v2}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onPlay()V

    .line 683
    :cond_0
    :goto_0
    return-void

    .line 660
    :cond_1
    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->pause:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 661
    iget-object v2, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-static {v2}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onPause()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 680
    :catch_0
    move-exception v0

    .line 681
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/AudioPlayer;->isDebug()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "AudioPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error while parsing control response : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 662
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    :try_start_1
    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->stop:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 663
    iget-object v2, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-static {v2}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onStop()V

    goto :goto_0

    .line 664
    :cond_3
    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->next:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 665
    iget-object v2, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-static {v2}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onNext()V

    goto :goto_0

    .line 666
    :cond_4
    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->previous:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 667
    iget-object v2, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-static {v2}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onPrevious()V

    goto :goto_0

    .line 668
    :cond_5
    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->repeat:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 669
    sget-object v2, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatAll:Lcom/samsung/multiscreen/Player$RepeatMode;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$RepeatMode;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 670
    iget-object v2, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-static {v2}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v2

    sget-object v3, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatAll:Lcom/samsung/multiscreen/Player$RepeatMode;

    invoke-interface {v2, v3}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onRepeat(Lcom/samsung/multiscreen/Player$RepeatMode;)V

    goto/16 :goto_0

    .line 671
    :cond_6
    sget-object v2, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatSingle:Lcom/samsung/multiscreen/Player$RepeatMode;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$RepeatMode;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 672
    iget-object v2, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-static {v2}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v2

    sget-object v3, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatSingle:Lcom/samsung/multiscreen/Player$RepeatMode;

    invoke-interface {v2, v3}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onRepeat(Lcom/samsung/multiscreen/Player$RepeatMode;)V

    goto/16 :goto_0

    .line 673
    :cond_7
    sget-object v2, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatOff:Lcom/samsung/multiscreen/Player$RepeatMode;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$RepeatMode;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 674
    iget-object v2, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-static {v2}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v2

    sget-object v3, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatOff:Lcom/samsung/multiscreen/Player$RepeatMode;

    invoke-interface {v2, v3}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onRepeat(Lcom/samsung/multiscreen/Player$RepeatMode;)V

    goto/16 :goto_0

    .line 676
    :cond_8
    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->shuffle:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 677
    const-string v2, "true"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 678
    .local v1, "shuffleState":Ljava/lang/Boolean;
    iget-object v2, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-static {v2}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onShuffle(Ljava/lang/Boolean;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private handlePlayerInternalResponse(Ljava/lang/String;)V
    .locals 6
    .param p1, "response"    # Ljava/lang/String;

    .prologue
    .line 689
    :try_start_0
    sget-object v3, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerInternalEvents;->bufferingstart:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerInternalEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerInternalEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 690
    iget-object v3, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onBufferingStart()V

    .line 711
    :cond_0
    :goto_0
    return-void

    .line 691
    :cond_1
    sget-object v3, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerInternalEvents;->bufferingcomplete:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerInternalEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerInternalEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 692
    iget-object v3, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onBufferingComplete()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 708
    :catch_0
    move-exception v0

    .line 709
    .local v0, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/AudioPlayer;->isDebug()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "AudioPlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error while parsing Internal Event response : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 693
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    :try_start_1
    sget-object v3, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerInternalEvents;->bufferingprogress:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerInternalEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerInternalEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 694
    const-string v3, ":"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 695
    .local v1, "index":I
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 696
    .local v2, "progress":I
    iget-object v3, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onBufferingProgress(I)V

    goto :goto_0

    .line 697
    .end local v1    # "index":I
    .end local v2    # "progress":I
    :cond_3
    sget-object v3, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerInternalEvents;->currentplaytime:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerInternalEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerInternalEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 698
    const-string v3, ":"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 699
    .restart local v1    # "index":I
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 700
    .restart local v2    # "progress":I
    iget-object v3, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onCurrentPlayTime(I)V

    goto/16 :goto_0

    .line 701
    .end local v1    # "index":I
    .end local v2    # "progress":I
    :cond_4
    sget-object v3, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerInternalEvents;->streamcompleted:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerInternalEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerInternalEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 702
    iget-object v3, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onStreamCompleted()V

    goto/16 :goto_0

    .line 703
    :cond_5
    sget-object v3, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerInternalEvents;->totalduration:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerInternalEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerInternalEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 704
    const-string v3, ":"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 705
    .restart local v1    # "index":I
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 706
    .restart local v2    # "progress":I
    iget-object v3, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onStreamingStarted(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private handlePlayerQueueEventResponse(Ljava/lang/String;)V
    .locals 6
    .param p1, "response"    # Ljava/lang/String;

    .prologue
    .line 715
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 718
    .local v2, "jsonObject":Lorg/json/JSONObject;
    const-string v3, "subEvent"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 719
    .local v1, "event":Ljava/lang/String;
    const-string v3, "subEvent"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 721
    if-nez v1, :cond_1

    .line 722
    iget-object v3, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/AudioPlayer;->isDebug()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "AudioPlayer"

    const-string v4, "Sub-Event key missing from message."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 739
    .end local v1    # "event":Ljava/lang/String;
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    return-void

    .line 726
    .restart local v1    # "event":Ljava/lang/String;
    .restart local v2    # "jsonObject":Lorg/json/JSONObject;
    :cond_1
    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->enqueue:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 727
    iget-object v3, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onAddToList(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 736
    .end local v1    # "event":Ljava/lang/String;
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 737
    .local v0, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/AudioPlayer;->isDebug()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "AudioPlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error while parsing list Event response : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 728
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "event":Ljava/lang/String;
    .restart local v2    # "jsonObject":Lorg/json/JSONObject;
    :cond_2
    :try_start_1
    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->dequeue:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 729
    iget-object v3, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onRemoveFromList(Lorg/json/JSONObject;)V

    goto :goto_0

    .line 730
    :cond_3
    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->clear:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 731
    iget-object v3, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onClearList()V

    goto :goto_0

    .line 732
    :cond_4
    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->fetch:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "data"

    .line 733
    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 734
    iget-object v3, p0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v3

    const-string v4, "data"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onGetList(Lorg/json/JSONArray;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method


# virtual methods
.method public onMessage(Lcom/samsung/multiscreen/Message;)V
    .locals 25
    .param p1, "message"    # Lcom/samsung/multiscreen/Message;

    .prologue
    .line 531
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/multiscreen/AudioPlayer;->isDebug()Z

    move-result v20

    if-eqz v20, :cond_0

    const-string v20, "AudioPlayer"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "onMessage : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v20

    if-nez v20, :cond_2

    .line 534
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/multiscreen/AudioPlayer;->isDebug()Z

    move-result v20

    if-eqz v20, :cond_1

    const-string v20, "AudioPlayer"

    const-string v21, "Unregistered PlayerListener."

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 654
    :cond_1
    :goto_0
    return-void

    .line 539
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/multiscreen/Message;->getEvent()Ljava/lang/String;

    move-result-object v20

    const-string v21, "playerNotice"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_3

    .line 540
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/multiscreen/AudioPlayer;->isDebug()Z

    move-result v20

    if-eqz v20, :cond_1

    const-string v20, "AudioPlayer"

    const-string v21, "In-Valid Player Message"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 547
    :cond_3
    :try_start_0
    new-instance v21, Lorg/json/JSONTokener;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/multiscreen/Message;->getData()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/String;

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/json/JSONObject;

    .line 549
    .local v15, "response":Lorg/json/JSONObject;
    if-nez v15, :cond_4

    .line 550
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/multiscreen/AudioPlayer;->isDebug()Z

    move-result v20

    if-eqz v20, :cond_1

    const-string v20, "AudioPlayer"

    const-string v21, "NULL Response - Unable to parse JSON string."

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 651
    .end local v15    # "response":Lorg/json/JSONObject;
    :catch_0
    move-exception v5

    .line 652
    .local v5, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/multiscreen/AudioPlayer;->isDebug()Z

    move-result v20

    if-eqz v20, :cond_1

    const-string v20, "AudioPlayer"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Error while parsing response : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 559
    .end local v5    # "e":Ljava/lang/Exception;
    .restart local v15    # "response":Lorg/json/JSONObject;
    :cond_4
    :try_start_1
    const-string v20, "subEvent"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 560
    const-string v20, "subEvent"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 561
    .local v17, "subEventData":Ljava/lang/String;
    const-string v20, "playerReady"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 563
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer;->mAdditionalData:Lorg/json/JSONObject;

    move-object/from16 v20, v0

    if-eqz v20, :cond_5

    .line 564
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer;->mAdditionalData:Lorg/json/JSONObject;

    move-object/from16 v20, v0

    const-string v21, "playerType"

    sget-object v22, Lcom/samsung/multiscreen/Player$ContentType;->audio:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 565
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer;->mAdditionalData:Lorg/json/JSONObject;

    move-object/from16 v20, v0

    const-string v21, "subEvent"

    sget-object v22, Lcom/samsung/multiscreen/Player$PlayerContentSubEvents;->ADDITIONALMEDIAINFO:Lcom/samsung/multiscreen/Player$PlayerContentSubEvents;

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/multiscreen/Player$PlayerContentSubEvents;->name()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 566
    sget-object v20, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v21, "playerContentChange"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer;->mAdditionalData:Lorg/json/JSONObject;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 569
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onPlayerInitialized()V

    goto/16 :goto_0

    .line 574
    :cond_6
    const-string v20, "playerChange"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 579
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v20

    sget-object v21, Lcom/samsung/multiscreen/Player$ContentType;->audio:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v21

    invoke-interface/range {v20 .. v21}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onPlayerChange(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 581
    .end local v17    # "subEventData":Ljava/lang/String;
    :cond_7
    const-string v20, "playerType"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_c

    .line 582
    const-string v20, "playerType"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 583
    .local v12, "playerType":Ljava/lang/String;
    sget-object v20, Lcom/samsung/multiscreen/Player$ContentType;->audio:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 585
    const-string v20, "state"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 586
    const-string v20, "state"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->handlePlayerControlResponse(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 587
    :cond_8
    const-string v20, "Audio State"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 588
    const-string v20, "Audio State"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->handlePlayerInternalResponse(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 589
    :cond_9
    const-string v20, "queue"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 590
    const-string v20, "queue"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->handlePlayerQueueEventResponse(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 591
    :cond_a
    const-string v20, "currentPlaying"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 592
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v20

    const-string v21, "currentPlaying"

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v0, v1, v12}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onCurrentPlaying(Lorg/json/JSONObject;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 593
    :cond_b
    const-string v20, "error"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 594
    const-string v20, "error"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v7

    .line 596
    .local v7, "errorMessage":Ljava/lang/String;
    :try_start_2
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 597
    .local v8, "errorValue":I
    new-instance v6, Lcom/samsung/multiscreen/ErrorCode;

    invoke-direct {v6, v8}, Lcom/samsung/multiscreen/ErrorCode;-><init>(I)V

    .line 598
    .local v6, "errorCode":Lcom/samsung/multiscreen/ErrorCode;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v20

    invoke-virtual {v6}, Lcom/samsung/multiscreen/ErrorCode;->value()I

    move-result v21

    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v22, v0

    invoke-virtual {v6}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v21

    invoke-virtual {v6}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v24

    move-wide/from16 v0, v22

    move-object/from16 v2, v21

    move-object/from16 v3, v24

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/multiscreen/Error;->create(JLjava/lang/String;Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v21

    invoke-interface/range {v20 .. v21}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onError(Lcom/samsung/multiscreen/Error;)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 599
    .end local v6    # "errorCode":Lcom/samsung/multiscreen/ErrorCode;
    .end local v8    # "errorValue":I
    :catch_1
    move-exception v5

    .line 600
    .local v5, "e":Ljava/lang/NumberFormatException;
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v20

    invoke-static {v7}, Lcom/samsung/multiscreen/Error;->create(Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v21

    invoke-interface/range {v20 .. v21}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onError(Lcom/samsung/multiscreen/Error;)V

    goto/16 :goto_0

    .line 605
    .end local v5    # "e":Ljava/lang/NumberFormatException;
    .end local v7    # "errorMessage":Ljava/lang/String;
    .end local v12    # "playerType":Ljava/lang/String;
    :cond_c
    const-string v20, "state"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_16

    .line 606
    invoke-virtual {v15}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    .line 607
    .local v4, "data":Ljava/lang/String;
    sget-object v20, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->getControlStatus:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_13

    .line 608
    new-instance v20, Lorg/json/JSONTokener;

    move-object/from16 v0, v20

    invoke-direct {v0, v4}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v20 .. v20}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/json/JSONObject;

    .line 609
    .local v10, "jsonData":Lorg/json/JSONObject;
    const/16 v18, 0x0

    .line 610
    .local v18, "volumeLevel":I
    const/16 v20, 0x0

    invoke-static/range {v20 .. v20}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    .local v11, "muteStatus":Ljava/lang/Boolean;
    const/16 v20, 0x0

    invoke-static/range {v20 .. v20}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v16

    .line 611
    .local v16, "shuffleStatus":Ljava/lang/Boolean;
    sget-object v13, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatOff:Lcom/samsung/multiscreen/Player$RepeatMode;

    .line 612
    .local v13, "repeatMode":Lcom/samsung/multiscreen/Player$RepeatMode;
    sget-object v20, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->volume:Lcom/samsung/multiscreen/Player$PlayerControlStatus;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_d

    .line 613
    sget-object v20, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->volume:Lcom/samsung/multiscreen/Player$PlayerControlStatus;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v18

    .line 615
    :cond_d
    sget-object v20, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->mute:Lcom/samsung/multiscreen/Player$PlayerControlStatus;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_e

    .line 616
    sget-object v20, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->mute:Lcom/samsung/multiscreen/Player$PlayerControlStatus;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    .line 618
    :cond_e
    sget-object v20, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->repeat:Lcom/samsung/multiscreen/Player$PlayerControlStatus;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_f

    .line 619
    sget-object v20, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->repeat:Lcom/samsung/multiscreen/Player$PlayerControlStatus;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 620
    .local v14, "repeatStr":Ljava/lang/String;
    const/4 v13, 0x0

    .line 621
    sget-object v20, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatAll:Lcom/samsung/multiscreen/Player$RepeatMode;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/multiscreen/Player$RepeatMode;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_11

    .line 622
    sget-object v13, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatAll:Lcom/samsung/multiscreen/Player$RepeatMode;

    .line 629
    .end local v14    # "repeatStr":Ljava/lang/String;
    :cond_f
    :goto_1
    sget-object v20, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->shuffle:Lcom/samsung/multiscreen/Player$PlayerControlStatus;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_10

    .line 630
    sget-object v20, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->shuffle:Lcom/samsung/multiscreen/Player$PlayerControlStatus;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v16

    .line 632
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v18

    move-object/from16 v2, v16

    invoke-interface {v0, v1, v11, v2, v13}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onControlStatus(ILjava/lang/Boolean;Ljava/lang/Boolean;Lcom/samsung/multiscreen/Player$RepeatMode;)V

    goto/16 :goto_0

    .line 623
    .restart local v14    # "repeatStr":Ljava/lang/String;
    :cond_11
    sget-object v20, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatSingle:Lcom/samsung/multiscreen/Player$RepeatMode;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/multiscreen/Player$RepeatMode;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_12

    .line 624
    sget-object v13, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatSingle:Lcom/samsung/multiscreen/Player$RepeatMode;

    goto :goto_1

    .line 625
    :cond_12
    sget-object v20, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatOff:Lcom/samsung/multiscreen/Player$RepeatMode;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/multiscreen/Player$RepeatMode;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_f

    .line 626
    sget-object v13, Lcom/samsung/multiscreen/Player$RepeatMode;->repeatOff:Lcom/samsung/multiscreen/Player$RepeatMode;

    goto :goto_1

    .line 633
    .end local v10    # "jsonData":Lorg/json/JSONObject;
    .end local v11    # "muteStatus":Ljava/lang/Boolean;
    .end local v13    # "repeatMode":Lcom/samsung/multiscreen/Player$RepeatMode;
    .end local v14    # "repeatStr":Ljava/lang/String;
    .end local v16    # "shuffleStatus":Ljava/lang/Boolean;
    .end local v18    # "volumeLevel":I
    :cond_13
    sget-object v20, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->mute:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_14

    .line 634
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onMute()V

    goto/16 :goto_0

    .line 635
    :cond_14
    sget-object v20, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->unMute:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_15

    .line 636
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onUnMute()V

    goto/16 :goto_0

    .line 637
    :cond_15
    sget-object v20, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->getVolume:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 638
    const-string v20, ":"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    .line 639
    .local v9, "index":I
    add-int/lit8 v20, v9, 0x1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v21

    add-int/lit8 v21, v21, -0x2

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v19

    .line 640
    .local v19, "volumeStr":Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    .line 641
    .restart local v18    # "volumeLevel":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-interface {v0, v1}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onVolumeChange(I)V

    goto/16 :goto_0

    .line 643
    .end local v4    # "data":Ljava/lang/String;
    .end local v9    # "index":I
    .end local v18    # "volumeLevel":I
    .end local v19    # "volumeStr":Ljava/lang/String;
    :cond_16
    const-string v20, "appStatus"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 644
    invoke-virtual {v15}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    .line 645
    .restart local v4    # "data":Ljava/lang/String;
    sget-object v20, Lcom/samsung/multiscreen/Player$PlayerApplicationStatusEvents;->suspend:Lcom/samsung/multiscreen/Player$PlayerApplicationStatusEvents;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/multiscreen/Player$PlayerApplicationStatusEvents;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_17

    .line 646
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onApplicationSuspend()V

    goto/16 :goto_0

    .line 647
    :cond_17
    sget-object v20, Lcom/samsung/multiscreen/Player$PlayerApplicationStatusEvents;->resume:Lcom/samsung/multiscreen/Player$PlayerApplicationStatusEvents;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/multiscreen/Player$PlayerApplicationStatusEvents;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 648
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/AudioPlayer;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/samsung/multiscreen/AudioPlayer;->access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onApplicationResume()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0
.end method
