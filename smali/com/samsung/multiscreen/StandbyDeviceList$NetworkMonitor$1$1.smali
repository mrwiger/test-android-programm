.class Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1$1;
.super Landroid/net/ConnectivityManager$NetworkCallback;
.source "StandbyDeviceList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;)V
    .locals 0
    .param p1, "this$2"    # Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1$1;->this$2:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;

    invoke-direct {p0}, Landroid/net/ConnectivityManager$NetworkCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onAvailable(Landroid/net/Network;)V
    .locals 6
    .param p1, "network"    # Landroid/net/Network;

    .prologue
    .line 95
    invoke-super {p0, p1}, Landroid/net/ConnectivityManager$NetworkCallback;->onAvailable(Landroid/net/Network;)V

    .line 96
    iget-object v4, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1$1;->this$2:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;

    iget-object v4, v4, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;->this$1:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;

    iget-object v5, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1$1;->this$2:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;

    iget-object v5, v5, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;->this$1:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;

    invoke-static {v5}, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->access$300(Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;)Landroid/net/ConnectivityManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->access$202(Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;Landroid/net/NetworkInfo;)Landroid/net/NetworkInfo;

    .line 97
    iget-object v4, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1$1;->this$2:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;

    iget-object v4, v4, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;->this$1:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;

    invoke-static {v4}, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->access$200(Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;)Landroid/net/NetworkInfo;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1$1;->this$2:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;

    iget-object v4, v4, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;->this$1:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;

    invoke-static {v4}, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->access$200(Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;)Landroid/net/NetworkInfo;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 98
    iget-object v4, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1$1;->this$2:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;

    iget-object v4, v4, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;->val$context:Landroid/content/Context;

    const-string v5, "wifi"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/WifiManager;

    .line 99
    .local v3, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    .line 100
    .local v2, "wifiInfo":Landroid/net/wifi/WifiInfo;
    iget-object v4, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1$1;->this$2:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;

    iget-object v4, v4, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;->this$1:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;

    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->access$402(Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;Ljava/lang/String;)Ljava/lang/String;

    .line 102
    iget-object v4, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1$1;->this$2:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;

    iget-object v4, v4, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;->this$1:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;

    iget-object v4, v4, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->this$0:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-static {v4}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$500(Lcom/samsung/multiscreen/StandbyDeviceList;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 103
    iget-object v4, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1$1;->this$2:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;

    iget-object v4, v4, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;->this$1:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;

    iget-object v4, v4, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->this$0:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-static {v4}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$600(Lcom/samsung/multiscreen/StandbyDeviceList;)Ljava/util/List;

    move-result-object v1

    .line 104
    .local v1, "lostStandbyServices":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/multiscreen/Service;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 105
    iget-object v4, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1$1;->this$2:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;

    iget-object v4, v4, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;->this$1:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;

    iget-object v4, v4, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->this$0:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-static {v4}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$000(Lcom/samsung/multiscreen/StandbyDeviceList;)Lcom/samsung/multiscreen/Search$SearchListener;

    move-result-object v5

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/multiscreen/Service;

    invoke-interface {v5, v4}, Lcom/samsung/multiscreen/Search$SearchListener;->onFound(Lcom/samsung/multiscreen/Service;)V

    .line 104
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 109
    .end local v0    # "i":I
    .end local v1    # "lostStandbyServices":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/multiscreen/Service;>;"
    .end local v2    # "wifiInfo":Landroid/net/wifi/WifiInfo;
    .end local v3    # "wifiManager":Landroid/net/wifi/WifiManager;
    :cond_0
    iget-object v4, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1$1;->this$2:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;

    iget-object v4, v4, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;->this$1:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;

    const-string v5, ""

    invoke-static {v4, v5}, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->access$402(Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;Ljava/lang/String;)Ljava/lang/String;

    .line 111
    :cond_1
    return-void
.end method

.method public onLost(Landroid/net/Network;)V
    .locals 4
    .param p1, "network"    # Landroid/net/Network;

    .prologue
    .line 115
    invoke-super {p0, p1}, Landroid/net/ConnectivityManager$NetworkCallback;->onLost(Landroid/net/Network;)V

    .line 117
    iget-object v2, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1$1;->this$2:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;

    iget-object v2, v2, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;->this$1:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;

    iget-object v2, v2, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->this$0:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-static {v2}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$600(Lcom/samsung/multiscreen/StandbyDeviceList;)Ljava/util/List;

    move-result-object v1

    .line 118
    .local v1, "lostStandbyServices":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/multiscreen/Service;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 119
    iget-object v2, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1$1;->this$2:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;

    iget-object v2, v2, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;->this$1:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;

    iget-object v2, v2, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->this$0:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-static {v2}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$000(Lcom/samsung/multiscreen/StandbyDeviceList;)Lcom/samsung/multiscreen/Search$SearchListener;

    move-result-object v3

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/multiscreen/Service;

    invoke-interface {v3, v2}, Lcom/samsung/multiscreen/Search$SearchListener;->onLost(Lcom/samsung/multiscreen/Service;)V

    .line 118
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 121
    :cond_0
    iget-object v2, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1$1;->this$2:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;

    iget-object v2, v2, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;->this$1:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;

    const-string v3, ""

    invoke-static {v2, v3}, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->access$402(Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;Ljava/lang/String;)Ljava/lang/String;

    .line 122
    return-void
.end method
