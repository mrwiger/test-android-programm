.class public Lcom/my/target/eq;
.super Lcom/my/target/es;
.source "MeasuringPromoLayoutManager.java"


# instance fields
.field private final df:I

.field dg:I

.field private dh:I

.field private di:I

.field private dj:F

.field private dk:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/my/target/es;-><init>(Landroid/content/Context;)V

    .line 20
    const/high16 v0, 0x3e000000    # 0.125f

    iput v0, p0, Lcom/my/target/eq;->dj:F

    .line 26
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    .line 27
    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iput v0, p0, Lcom/my/target/eq;->df:I

    .line 28
    return-void
.end method


# virtual methods
.method public final K()V
    .locals 1

    .prologue
    .line 37
    const v0, 0x3e333333    # 0.175f

    iput v0, p0, Lcom/my/target/eq;->dj:F

    .line 38
    return-void
.end method

.method public final f(I)V
    .locals 0

    .prologue
    .line 42
    iput p1, p0, Lcom/my/target/eq;->dk:I

    .line 43
    return-void
.end method

.method public measureChildWithMargins(Landroid/view/View;II)V
    .locals 12
    .param p1, "child"    # Landroid/view/View;
    .param p2, "widthUsed"    # I
    .param p3, "heightUsed"    # I

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    .line 48
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    .line 51
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->getWidth()I

    move-result v3

    .line 52
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->getHeight()I

    move-result v4

    .line 55
    const/4 v1, 0x0

    .line 57
    iget v2, p0, Lcom/my/target/eq;->dj:F

    const/4 v5, 0x0

    cmpg-float v2, v2, v5

    if-gtz v2, :cond_0

    .line 59
    if-ge v3, v4, :cond_4

    .line 61
    const/high16 v2, 0x3e000000    # 0.125f

    iput v2, p0, Lcom/my/target/eq;->dj:F

    .line 69
    :cond_0
    :goto_0
    iget v2, p0, Lcom/my/target/eq;->di:I

    if-gtz v2, :cond_1

    .line 71
    iput v4, p0, Lcom/my/target/eq;->di:I

    .line 73
    :cond_1
    iget v2, p0, Lcom/my/target/eq;->dh:I

    if-gtz v2, :cond_2

    .line 75
    int-to-float v2, v3

    int-to-float v5, v3

    iget v6, p0, Lcom/my/target/eq;->dj:F

    mul-float/2addr v6, v11

    mul-float/2addr v5, v6

    sub-float/2addr v2, v5

    float-to-int v2, v2

    iput v2, p0, Lcom/my/target/eq;->dh:I

    .line 78
    :cond_2
    if-lez v4, :cond_3

    if-lez v3, :cond_3

    iget v2, p0, Lcom/my/target/eq;->di:I

    if-lez v2, :cond_3

    iget v2, p0, Lcom/my/target/eq;->dh:I

    if-gtz v2, :cond_5

    .line 143
    :cond_3
    :goto_1
    return-void

    .line 65
    :cond_4
    const v2, 0x3d4ccccd    # 0.05f

    iput v2, p0, Lcom/my/target/eq;->dj:F

    goto :goto_0

    .line 83
    :cond_5
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->getPaddingLeft()I

    move-result v5

    .line 84
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->getPaddingRight()I

    move-result v6

    .line 85
    int-to-float v2, v3

    int-to-float v7, v4

    div-float/2addr v2, v7

    iget v7, p0, Lcom/my/target/eq;->dh:I

    int-to-float v7, v7

    iget v8, p0, Lcom/my/target/eq;->di:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    cmpl-float v2, v2, v7

    if-lez v2, :cond_7

    .line 87
    iget v1, p0, Lcom/my/target/eq;->dh:I

    mul-int/2addr v1, v4

    int-to-float v1, v1

    iget v2, p0, Lcom/my/target/eq;->di:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, p0, Lcom/my/target/eq;->df:I

    sub-int v2, v1, v2

    .line 88
    iget v1, p0, Lcom/my/target/eq;->dg:I

    if-lez v1, :cond_6

    .line 90
    iget v1, p0, Lcom/my/target/eq;->dg:I

    .line 103
    :goto_2
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;->getItemViewType(Landroid/view/View;)I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_9

    .line 105
    iget v7, p0, Lcom/my/target/eq;->dn:I

    if-lez v7, :cond_8

    .line 107
    iget v7, p0, Lcom/my/target/eq;->dn:I

    iput v7, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    .line 114
    :goto_3
    iput v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    .line 134
    :goto_4
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->getWidthMode()I

    move-result v1

    add-int/2addr v5, v6

    iget v6, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    add-int/2addr v5, v6

    iget v6, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    add-int/2addr v5, v6

    add-int/2addr v5, p2

    .line 136
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->canScrollHorizontally()Z

    move-result v6

    .line 134
    invoke-static {v3, v1, v5, v2, v6}, Landroid/support/v7/widget/LinearLayoutManager;->getChildMeasureSpec(IIIIZ)I

    move-result v1

    .line 138
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->getHeightMode()I

    move-result v2

    .line 139
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->getPaddingTop()I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->getPaddingBottom()I

    move-result v5

    add-int/2addr v3, v5

    iget v5, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->topMargin:I

    add-int/2addr v3, v5

    iget v5, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->bottomMargin:I

    add-int/2addr v3, v5

    add-int/2addr v3, p3

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->height:I

    .line 141
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->canScrollVertically()Z

    move-result v5

    .line 137
    invoke-static {v4, v2, v3, v0, v5}, Landroid/support/v7/widget/LinearLayoutManager;->getChildMeasureSpec(IIIIZ)I

    move-result v0

    .line 142
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    goto :goto_1

    .line 94
    :cond_6
    iget v1, p0, Lcom/my/target/eq;->dk:I

    int-to-float v7, v3

    add-int v8, v3, v2

    int-to-float v8, v8

    div-float/2addr v8, v11

    int-to-float v9, v3

    iget v10, p0, Lcom/my/target/eq;->dj:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    sub-float/2addr v7, v8

    float-to-int v7, v7

    int-to-float v7, v7

    div-float/2addr v7, v11

    float-to-int v7, v7

    invoke-static {v1, v7}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_2

    .line 100
    :cond_7
    int-to-float v2, v3

    const/high16 v7, 0x3f800000    # 1.0f

    iget v8, p0, Lcom/my/target/eq;->dj:F

    add-float/2addr v7, v8

    div-float/2addr v2, v7

    float-to-int v2, v2

    sub-int/2addr v2, v6

    sub-int/2addr v2, v5

    goto :goto_2

    .line 111
    :cond_8
    int-to-float v7, v3

    int-to-float v8, v5

    sub-float/2addr v7, v8

    int-to-float v8, v6

    sub-float/2addr v7, v8

    int-to-float v8, v2

    sub-float/2addr v7, v8

    div-float/2addr v7, v11

    float-to-int v7, v7

    iput v7, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    goto :goto_3

    .line 116
    :cond_9
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;->getItemViewType(Landroid/view/View;)I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_b

    .line 118
    iget v7, p0, Lcom/my/target/eq;->dn:I

    if-lez v7, :cond_a

    .line 120
    iget v7, p0, Lcom/my/target/eq;->dn:I

    iput v7, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    .line 126
    :goto_5
    iput v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    goto :goto_4

    .line 124
    :cond_a
    int-to-float v7, v3

    int-to-float v8, v5

    sub-float/2addr v7, v8

    int-to-float v8, v6

    sub-float/2addr v7, v8

    int-to-float v8, v2

    sub-float/2addr v7, v8

    div-float/2addr v7, v11

    float-to-int v7, v7

    iput v7, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    goto :goto_5

    .line 130
    :cond_b
    iput v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    .line 131
    iput v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    goto/16 :goto_4
.end method
