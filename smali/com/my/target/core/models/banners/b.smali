.class public final Lcom/my/target/core/models/banners/b;
.super Lcom/my/target/ah;
.source "NativeAdCard.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/my/target/ah;-><init>()V

    .line 29
    return-void
.end method

.method public static newCard(Lcom/my/target/core/models/banners/a;)Lcom/my/target/core/models/banners/b;
    .locals 2
    .param p0, "nativeAdBanner"    # Lcom/my/target/core/models/banners/a;

    .prologue
    .line 9
    new-instance v0, Lcom/my/target/core/models/banners/b;

    invoke-direct {v0}, Lcom/my/target/core/models/banners/b;-><init>()V

    .line 10
    iget-object v1, p0, Lcom/my/target/core/models/banners/a;->ctaText:Ljava/lang/String;

    iput-object v1, v0, Lcom/my/target/core/models/banners/b;->ctaText:Ljava/lang/String;

    .line 11
    iget-object v1, p0, Lcom/my/target/core/models/banners/a;->navigationType:Ljava/lang/String;

    iput-object v1, v0, Lcom/my/target/core/models/banners/b;->navigationType:Ljava/lang/String;

    .line 12
    iget-object v1, p0, Lcom/my/target/core/models/banners/a;->urlscheme:Ljava/lang/String;

    iput-object v1, v0, Lcom/my/target/core/models/banners/b;->urlscheme:Ljava/lang/String;

    .line 13
    iget-object v1, p0, Lcom/my/target/core/models/banners/a;->bundleId:Ljava/lang/String;

    iput-object v1, v0, Lcom/my/target/core/models/banners/b;->bundleId:Ljava/lang/String;

    .line 14
    iget-boolean v1, p0, Lcom/my/target/core/models/banners/a;->directLink:Z

    iput-boolean v1, v0, Lcom/my/target/core/models/banners/b;->directLink:Z

    .line 15
    iget-boolean v1, p0, Lcom/my/target/core/models/banners/a;->openInBrowser:Z

    iput-boolean v1, v0, Lcom/my/target/core/models/banners/b;->openInBrowser:Z

    .line 16
    iget-boolean v1, p0, Lcom/my/target/core/models/banners/a;->usePlayStoreAction:Z

    iput-boolean v1, v0, Lcom/my/target/core/models/banners/b;->usePlayStoreAction:Z

    .line 17
    iget-object v1, p0, Lcom/my/target/core/models/banners/a;->deeplink:Ljava/lang/String;

    iput-object v1, v0, Lcom/my/target/core/models/banners/b;->deeplink:Ljava/lang/String;

    .line 18
    iget-object v1, p0, Lcom/my/target/core/models/banners/a;->clickArea:Lcom/my/target/af;

    iput-object v1, v0, Lcom/my/target/core/models/banners/b;->clickArea:Lcom/my/target/af;

    .line 19
    iget v1, p0, Lcom/my/target/core/models/banners/a;->rating:F

    iput v1, v0, Lcom/my/target/core/models/banners/b;->rating:F

    .line 20
    iget v1, p0, Lcom/my/target/core/models/banners/a;->votes:I

    iput v1, v0, Lcom/my/target/core/models/banners/b;->votes:I

    .line 21
    iget-object v1, p0, Lcom/my/target/core/models/banners/a;->domain:Ljava/lang/String;

    iput-object v1, v0, Lcom/my/target/core/models/banners/b;->domain:Ljava/lang/String;

    .line 22
    iget-object v1, p0, Lcom/my/target/core/models/banners/a;->category:Ljava/lang/String;

    iput-object v1, v0, Lcom/my/target/core/models/banners/b;->category:Ljava/lang/String;

    .line 23
    iget-object v1, p0, Lcom/my/target/core/models/banners/a;->subCategory:Ljava/lang/String;

    iput-object v1, v0, Lcom/my/target/core/models/banners/b;->subCategory:Ljava/lang/String;

    .line 24
    return-object v0
.end method
