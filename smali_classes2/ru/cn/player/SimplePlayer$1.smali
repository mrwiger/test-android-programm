.class Lru/cn/player/SimplePlayer$1;
.super Ljava/lang/Object;
.source "SimplePlayer.java"

# interfaces
.implements Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/player/SimplePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/player/SimplePlayer;


# direct methods
.method constructor <init>(Lru/cn/player/SimplePlayer;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/player/SimplePlayer;

    .prologue
    .line 133
    iput-object p1, p0, Lru/cn/player/SimplePlayer$1;->this$0:Lru/cn/player/SimplePlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete()V
    .locals 6

    .prologue
    .line 189
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lru/cn/player/SimplePlayer$1;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v1}, Lru/cn/player/SimplePlayer;->access$000(Lru/cn/player/SimplePlayer;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 190
    .local v0, "fListeners":Ljava/util/List;, "Ljava/util/List<Lru/cn/player/SimplePlayer$Listener;>;"
    iget-object v1, p0, Lru/cn/player/SimplePlayer$1;->this$0:Lru/cn/player/SimplePlayer;

    new-instance v2, Lru/cn/player/SimplePlayer$1$2;

    invoke-direct {v2, p0, v0}, Lru/cn/player/SimplePlayer$1$2;-><init>(Lru/cn/player/SimplePlayer$1;Ljava/util/List;)V

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v2, v4, v5}, Lru/cn/player/SimplePlayer;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 199
    return-void
.end method

.method public onError(I)V
    .locals 6
    .param p1, "code"    # I

    .prologue
    .line 204
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lru/cn/player/SimplePlayer$1;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v2}, Lru/cn/player/SimplePlayer;->access$000(Lru/cn/player/SimplePlayer;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 205
    .local v0, "fListeners":Ljava/util/List;, "Ljava/util/List<Lru/cn/player/SimplePlayer$Listener;>;"
    iget-object v2, p0, Lru/cn/player/SimplePlayer$1;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v2}, Lru/cn/player/SimplePlayer;->access$800(Lru/cn/player/SimplePlayer;)I

    move-result v1

    .line 206
    .local v1, "type":I
    iget-object v2, p0, Lru/cn/player/SimplePlayer$1;->this$0:Lru/cn/player/SimplePlayer;

    new-instance v3, Lru/cn/player/SimplePlayer$1$3;

    invoke-direct {v3, p0, v0, v1, p1}, Lru/cn/player/SimplePlayer$1$3;-><init>(Lru/cn/player/SimplePlayer$1;Ljava/util/List;II)V

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lru/cn/player/SimplePlayer;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 216
    iget-object v2, p0, Lru/cn/player/SimplePlayer$1;->this$0:Lru/cn/player/SimplePlayer;

    iget-object v3, p0, Lru/cn/player/SimplePlayer$1;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v3}, Lru/cn/player/SimplePlayer;->access$300(Lru/cn/player/SimplePlayer;)Lru/cn/player/AbstractMediaPlayer;

    move-result-object v3

    invoke-virtual {v3}, Lru/cn/player/AbstractMediaPlayer;->getCurrentPosition()I

    move-result v3

    invoke-static {v2, v3}, Lru/cn/player/SimplePlayer;->access$202(Lru/cn/player/SimplePlayer;I)I

    .line 217
    return-void
.end method

.method public onStateChanged(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V
    .locals 6
    .param p1, "state"    # Lru/cn/player/AbstractMediaPlayer$PlayerState;

    .prologue
    .line 136
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lru/cn/player/SimplePlayer$1;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v1}, Lru/cn/player/SimplePlayer;->access$000(Lru/cn/player/SimplePlayer;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 137
    .local v0, "fListeners":Ljava/util/List;, "Ljava/util/List<Lru/cn/player/SimplePlayer$Listener;>;"
    iget-object v1, p0, Lru/cn/player/SimplePlayer$1;->this$0:Lru/cn/player/SimplePlayer;

    new-instance v2, Lru/cn/player/SimplePlayer$1$1;

    invoke-direct {v2, p0, v0, p1}, Lru/cn/player/SimplePlayer$1$1;-><init>(Lru/cn/player/SimplePlayer$1;Ljava/util/List;Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v2, v4, v5}, Lru/cn/player/SimplePlayer;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 146
    sget-object v1, Lru/cn/player/AbstractMediaPlayer$PlayerState;->STOPPED:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    if-ne p1, v1, :cond_0

    .line 147
    iget-object v1, p0, Lru/cn/player/SimplePlayer$1;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v1}, Lru/cn/player/SimplePlayer;->access$100(Lru/cn/player/SimplePlayer;)Lru/cn/utils/bytefog/BytefogSession;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 148
    iget-object v1, p0, Lru/cn/player/SimplePlayer$1;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v1}, Lru/cn/player/SimplePlayer;->access$100(Lru/cn/player/SimplePlayer;)Lru/cn/utils/bytefog/BytefogSession;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/utils/bytefog/BytefogSession;->stop()V

    .line 149
    iget-object v1, p0, Lru/cn/player/SimplePlayer$1;->this$0:Lru/cn/player/SimplePlayer;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lru/cn/player/SimplePlayer;->access$102(Lru/cn/player/SimplePlayer;Lru/cn/utils/bytefog/BytefogSession;)Lru/cn/utils/bytefog/BytefogSession;

    .line 153
    :cond_0
    iget-object v1, p0, Lru/cn/player/SimplePlayer$1;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v1}, Lru/cn/player/SimplePlayer;->access$200(Lru/cn/player/SimplePlayer;)I

    move-result v1

    if-lez v1, :cond_1

    .line 154
    sget-object v1, Lru/cn/player/SimplePlayer$6;->$SwitchMap$ru$cn$player$AbstractMediaPlayer$PlayerState:[I

    invoke-virtual {p1}, Lru/cn/player/AbstractMediaPlayer$PlayerState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 162
    :cond_1
    :goto_0
    return-void

    .line 157
    :pswitch_0
    iget-object v1, p0, Lru/cn/player/SimplePlayer$1;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v1}, Lru/cn/player/SimplePlayer;->access$300(Lru/cn/player/SimplePlayer;)Lru/cn/player/AbstractMediaPlayer;

    move-result-object v1

    iget-object v2, p0, Lru/cn/player/SimplePlayer$1;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v2}, Lru/cn/player/SimplePlayer;->access$200(Lru/cn/player/SimplePlayer;)I

    move-result v2

    invoke-virtual {v1, v2}, Lru/cn/player/AbstractMediaPlayer;->seekTo(I)V

    .line 158
    iget-object v1, p0, Lru/cn/player/SimplePlayer$1;->this$0:Lru/cn/player/SimplePlayer;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lru/cn/player/SimplePlayer;->access$202(Lru/cn/player/SimplePlayer;I)I

    goto :goto_0

    .line 154
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public videoSizeChanged(IIF)V
    .locals 5
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "pixelWidthHeightRatio"    # F

    .prologue
    const/4 v4, 0x0

    .line 166
    invoke-static {}, Lru/cn/player/SimplePlayer;->access$400()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Video size detected: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " widthHeightRatio="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    cmpl-float v1, p3, v4

    if-lez v1, :cond_1

    .line 169
    int-to-float v1, p1

    mul-float/2addr v1, p3

    float-to-int p1, v1

    .line 174
    :cond_0
    :goto_0
    invoke-static {}, Lru/cn/player/SimplePlayer;->access$400()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    iget-object v1, p0, Lru/cn/player/SimplePlayer$1;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v1}, Lru/cn/player/SimplePlayer;->access$600(Lru/cn/player/SimplePlayer;)Lru/cn/player/VideoSizeCalculator;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lru/cn/player/VideoSizeCalculator;->setVideoSize(II)V

    .line 177
    iget-object v1, p0, Lru/cn/player/SimplePlayer$1;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v1}, Lru/cn/player/SimplePlayer;->access$700(Lru/cn/player/SimplePlayer;)Landroid/view/SurfaceView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    .line 179
    iget-object v1, p0, Lru/cn/player/SimplePlayer$1;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v1}, Lru/cn/player/SimplePlayer;->access$000(Lru/cn/player/SimplePlayer;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/player/SimplePlayer$Listener;

    .line 180
    .local v0, "listener":Lru/cn/player/SimplePlayer$Listener;
    invoke-interface {v0, p1, p2}, Lru/cn/player/SimplePlayer$Listener;->videoSizeChanged(II)V

    goto :goto_1

    .line 170
    .end local v0    # "listener":Lru/cn/player/SimplePlayer$Listener;
    :cond_1
    iget-object v1, p0, Lru/cn/player/SimplePlayer$1;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v1}, Lru/cn/player/SimplePlayer;->access$500(Lru/cn/player/SimplePlayer;)F

    move-result v1

    cmpl-float v1, v1, v4

    if-lez v1, :cond_0

    .line 171
    iget-object v1, p0, Lru/cn/player/SimplePlayer$1;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v1}, Lru/cn/player/SimplePlayer;->access$500(Lru/cn/player/SimplePlayer;)F

    move-result v1

    int-to-float v2, p2

    mul-float/2addr v1, v2

    float-to-int p1, v1

    goto :goto_0

    .line 183
    :cond_2
    iget-object v1, p0, Lru/cn/player/SimplePlayer$1;->this$0:Lru/cn/player/SimplePlayer;

    invoke-virtual {v1}, Lru/cn/player/SimplePlayer;->requestLayout()V

    .line 184
    return-void
.end method
