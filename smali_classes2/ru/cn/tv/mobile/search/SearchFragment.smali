.class public Lru/cn/tv/mobile/search/SearchFragment;
.super Landroid/support/v4/app/Fragment;
.source "SearchFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/mobile/search/SearchFragment$SearchFragmentListener;
    }
.end annotation


# instance fields
.field private adapter:Lru/cn/tv/mobile/search/SearchResultAdapter;

.field private emptyView:Landroid/view/View;

.field private list:Landroid/widget/ListView;

.field private listener:Lru/cn/tv/mobile/search/SearchFragment$SearchFragmentListener;

.field private progress:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private initListView()V
    .locals 5

    .prologue
    .line 90
    new-instance v0, Lru/cn/tv/mobile/search/SearchResultAdapter;

    invoke-virtual {p0}, Lru/cn/tv/mobile/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0c00b7

    const v3, 0x7f0c009a

    const v4, 0x7f0c00ae

    invoke-direct {v0, v1, v2, v3, v4}, Lru/cn/tv/mobile/search/SearchResultAdapter;-><init>(Landroid/content/Context;III)V

    iput-object v0, p0, Lru/cn/tv/mobile/search/SearchFragment;->adapter:Lru/cn/tv/mobile/search/SearchResultAdapter;

    .line 94
    iget-object v0, p0, Lru/cn/tv/mobile/search/SearchFragment;->list:Landroid/widget/ListView;

    iget-object v1, p0, Lru/cn/tv/mobile/search/SearchFragment;->adapter:Lru/cn/tv/mobile/search/SearchResultAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 95
    iget-object v0, p0, Lru/cn/tv/mobile/search/SearchFragment;->list:Landroid/widget/ListView;

    new-instance v1, Lru/cn/tv/mobile/search/SearchFragment$$Lambda$1;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/search/SearchFragment$$Lambda$1;-><init>(Lru/cn/tv/mobile/search/SearchFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 111
    return-void
.end method

.method public static newInstance(Ljava/lang/String;J)Lru/cn/tv/mobile/search/SearchFragment;
    .locals 3
    .param p0, "query"    # Ljava/lang/String;
    .param p1, "rubricId"    # J

    .prologue
    .line 30
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 31
    .local v0, "args":Landroid/os/Bundle;
    new-instance v1, Lru/cn/tv/mobile/search/SearchFragment;

    invoke-direct {v1}, Lru/cn/tv/mobile/search/SearchFragment;-><init>()V

    .line 32
    .local v1, "fragment":Lru/cn/tv/mobile/search/SearchFragment;
    const-string v2, "query"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    const-string v2, "rubric_id"

    invoke-virtual {v0, v2, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 34
    invoke-virtual {v1, v0}, Lru/cn/tv/mobile/search/SearchFragment;->setArguments(Landroid/os/Bundle;)V

    .line 35
    return-object v1
.end method

.method private searchCompleted(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 82
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 83
    iget-object v0, p0, Lru/cn/tv/mobile/search/SearchFragment;->emptyView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 85
    :cond_0
    iget-object v0, p0, Lru/cn/tv/mobile/search/SearchFragment;->adapter:Lru/cn/tv/mobile/search/SearchResultAdapter;

    invoke-virtual {v0, p1}, Lru/cn/tv/mobile/search/SearchResultAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 86
    iget-object v0, p0, Lru/cn/tv/mobile/search/SearchFragment;->progress:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 87
    return-void
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$SearchFragment(Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/mobile/search/SearchFragment;->searchCompleted(Landroid/database/Cursor;)V

    return-void
.end method

.method public getQuery()Ljava/lang/String;
    .locals 2

    .prologue
    .line 74
    invoke-virtual {p0}, Lru/cn/tv/mobile/search/SearchFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final synthetic lambda$initListView$0$SearchFragment(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view1"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 96
    iget-object v6, p0, Lru/cn/tv/mobile/search/SearchFragment;->listener:Lru/cn/tv/mobile/search/SearchFragment$SearchFragmentListener;

    if-nez v6, :cond_1

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 99
    :cond_1
    iget-object v6, p0, Lru/cn/tv/mobile/search/SearchFragment;->adapter:Lru/cn/tv/mobile/search/SearchResultAdapter;

    invoke-virtual {v6, p3}, Lru/cn/tv/mobile/search/SearchResultAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 100
    .local v0, "c":Landroid/database/Cursor;
    iget-object v6, p0, Lru/cn/tv/mobile/search/SearchFragment;->adapter:Lru/cn/tv/mobile/search/SearchResultAdapter;

    invoke-virtual {v6, p3}, Lru/cn/tv/mobile/search/SearchResultAdapter;->getItemViewType(I)I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_2

    .line 101
    const-string v6, "telecastId"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 102
    .local v4, "telecastId":J
    const-string v6, "archive"

    invoke-static {v6}, Lru/cn/domain/statistics/AnalyticsManager;->view_search(Ljava/lang/String;)V

    .line 103
    iget-object v6, p0, Lru/cn/tv/mobile/search/SearchFragment;->listener:Lru/cn/tv/mobile/search/SearchFragment$SearchFragmentListener;

    invoke-interface {v6, v4, v5}, Lru/cn/tv/mobile/search/SearchFragment$SearchFragmentListener;->onRecordSelected(J)V

    goto :goto_0

    .line 104
    .end local v4    # "telecastId":J
    :cond_2
    iget-object v6, p0, Lru/cn/tv/mobile/search/SearchFragment;->adapter:Lru/cn/tv/mobile/search/SearchResultAdapter;

    invoke-virtual {v6, p3}, Lru/cn/tv/mobile/search/SearchResultAdapter;->getItemViewType(I)I

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    .line 105
    const-string v6, "cn_id"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 106
    .local v2, "channelId":J
    const-string v6, "title"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 107
    .local v1, "title":Ljava/lang/String;
    const-string v6, "channel"

    invoke-static {v6}, Lru/cn/domain/statistics/AnalyticsManager;->view_search(Ljava/lang/String;)V

    .line 108
    iget-object v6, p0, Lru/cn/tv/mobile/search/SearchFragment;->listener:Lru/cn/tv/mobile/search/SearchFragment$SearchFragmentListener;

    invoke-interface {v6, v2, v3, v1}, Lru/cn/tv/mobile/search/SearchFragment$SearchFragmentListener;->onChannelSelected(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 48
    invoke-virtual {p0}, Lru/cn/tv/mobile/search/SearchFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "query"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "query":Ljava/lang/String;
    invoke-virtual {p0}, Lru/cn/tv/mobile/search/SearchFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "rubric_id"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 51
    .local v2, "rubricId":J
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v4

    const-class v5, Lru/cn/tv/mobile/search/SearchViewModel;

    invoke-static {p0, v4, v5}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v1

    check-cast v1, Lru/cn/tv/mobile/search/SearchViewModel;

    .line 52
    .local v1, "viewModel":Lru/cn/tv/mobile/search/SearchViewModel;
    invoke-virtual {v1}, Lru/cn/tv/mobile/search/SearchViewModel;->searchResult()Landroid/arch/lifecycle/LiveData;

    move-result-object v4

    new-instance v5, Lru/cn/tv/mobile/search/SearchFragment$$Lambda$0;

    invoke-direct {v5, p0}, Lru/cn/tv/mobile/search/SearchFragment$$Lambda$0;-><init>(Lru/cn/tv/mobile/search/SearchFragment;)V

    invoke-virtual {v4, p0, v5}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 53
    invoke-virtual {v1, v0, v2, v3}, Lru/cn/tv/mobile/search/SearchViewModel;->setSearchQuery(Ljava/lang/String;J)V

    .line 54
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 59
    const v0, 0x7f0c00af

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 64
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 66
    const v0, 0x7f090175

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lru/cn/tv/mobile/search/SearchFragment;->progress:Landroid/widget/ProgressBar;

    .line 67
    const v0, 0x102000a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lru/cn/tv/mobile/search/SearchFragment;->list:Landroid/widget/ListView;

    .line 68
    const v0, 0x7f0900b1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/mobile/search/SearchFragment;->emptyView:Landroid/view/View;

    .line 70
    invoke-direct {p0}, Lru/cn/tv/mobile/search/SearchFragment;->initListView()V

    .line 71
    return-void
.end method

.method public setListener(Lru/cn/tv/mobile/search/SearchFragment$SearchFragmentListener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/tv/mobile/search/SearchFragment$SearchFragmentListener;

    .prologue
    .line 78
    iput-object p1, p0, Lru/cn/tv/mobile/search/SearchFragment;->listener:Lru/cn/tv/mobile/search/SearchFragment$SearchFragmentListener;

    .line 79
    return-void
.end method
