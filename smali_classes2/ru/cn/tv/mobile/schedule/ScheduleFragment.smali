.class public Lru/cn/tv/mobile/schedule/ScheduleFragment;
.super Lru/cn/view/CustomListFragment;
.source "ScheduleFragment.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;
    }
.end annotation


# instance fields
.field private adapter:Lru/cn/tv/mobile/schedule/ScheduleAdapter;

.field private currentChannelId:J

.field private currentDate:Ljava/util/Calendar;

.field private currentTelecastId:J

.field private goToAirButton:Landroid/view/View;

.field private listener:Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;

.field private mHandler:Landroid/os/Handler;

.field private noSchedule:Landroid/view/View;

.field private onAirTelecastId:J

.field private progressBar:Landroid/view/View;

.field private viewModel:Lru/cn/tv/mobile/schedule/ScheduleViewModel;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 25
    invoke-direct {p0}, Lru/cn/view/CustomListFragment;-><init>()V

    .line 34
    iput-wide v0, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->currentChannelId:J

    .line 35
    iput-wide v0, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->currentTelecastId:J

    .line 36
    iput-wide v0, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->onAirTelecastId:J

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->currentDate:Ljava/util/Calendar;

    return-void
.end method

.method public static newInstance(JLjava/util/Calendar;)Lru/cn/tv/mobile/schedule/ScheduleFragment;
    .locals 6
    .param p0, "channelId"    # J
    .param p2, "date"    # Ljava/util/Calendar;

    .prologue
    .line 53
    new-instance v1, Lru/cn/tv/mobile/schedule/ScheduleFragment;

    invoke-direct {v1}, Lru/cn/tv/mobile/schedule/ScheduleFragment;-><init>()V

    .line 55
    .local v1, "fragment":Lru/cn/tv/mobile/schedule/ScheduleFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 56
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "channelId"

    invoke-virtual {v0, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 57
    const-string v2, "timestampMs"

    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 58
    const-string v2, "timezoneOffset"

    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 59
    invoke-virtual {v1, v0}, Lru/cn/tv/mobile/schedule/ScheduleFragment;->setArguments(Landroid/os/Bundle;)V

    .line 61
    return-object v1
.end method

.method private setSchedule(Landroid/database/Cursor;)V
    .locals 24
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 231
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->progressBar:Landroid/view/View;

    move-object/from16 v18, v0

    const/16 v19, 0x8

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->setVisibility(I)V

    .line 233
    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v18

    if-nez v18, :cond_2

    .line 234
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->noSchedule:Landroid/view/View;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->setVisibility(I)V

    .line 236
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->currentDate:Ljava/util/Calendar;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lru/cn/utils/DateUtils;->isToday(Ljava/util/Calendar;)Z

    move-result v5

    .line 237
    .local v5, "isToday":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->goToAirButton:Landroid/view/View;

    move-object/from16 v19, v0

    if-eqz v5, :cond_1

    const/16 v18, 0x0

    :goto_0
    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 282
    .end local v5    # "isToday":Z
    .end local p1    # "cursor":Landroid/database/Cursor;
    :goto_1
    return-void

    .line 237
    .restart local v5    # "isToday":Z
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :cond_1
    const/16 v18, 0x8

    goto :goto_0

    .line 242
    .end local v5    # "isToday":Z
    :cond_2
    check-cast p1, Landroid/database/CursorWrapper;

    .line 243
    .end local p1    # "cursor":Landroid/database/Cursor;
    invoke-virtual/range {p1 .. p1}, Landroid/database/CursorWrapper;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v4

    check-cast v4, Lru/cn/api/provider/cursor/ScheduleItemCursor;

    .line 245
    .local v4, "c":Lru/cn/api/provider/cursor/ScheduleItemCursor;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 247
    .local v10, "nowMs":J
    invoke-virtual {v4}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->moveToFirst()Z

    .line 248
    :goto_2
    invoke-virtual {v4}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->isAfterLast()Z

    move-result v18

    if-nez v18, :cond_4

    .line 249
    invoke-virtual {v4, v10, v11}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->isOnTime(J)Z

    move-result v12

    .line 250
    .local v12, "onAir":Z
    if-eqz v12, :cond_6

    .line 251
    invoke-virtual {v4}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getTelecastId()J

    move-result-wide v8

    .line 254
    .local v8, "id":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->onAirTelecastId:J

    move-wide/from16 v18, v0

    cmp-long v18, v18, v8

    if-eqz v18, :cond_5

    .line 255
    move-object/from16 v0, p0

    iput-wide v8, v0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->onAirTelecastId:J

    .line 257
    invoke-virtual {v4}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getTime()J

    move-result-wide v16

    .line 258
    .local v16, "telecastTime":J
    invoke-virtual {v4}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getDuration()J

    move-result-wide v14

    .line 259
    .local v14, "telecastDuration":J
    new-instance v18, Ljava/util/Date;

    add-long v20, v16, v14

    const-wide/16 v22, 0x3e8

    mul-long v20, v20, v22

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-static/range {v18 .. v18}, Lru/cn/utils/Utils;->getCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v13

    .line 262
    .local v13, "telecastEnd":Ljava/util/Calendar;
    invoke-virtual {v13}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v18

    .line 263
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    sub-long v18, v18, v20

    const-wide/16 v20, 0x2710

    add-long v6, v18, v20

    .line 268
    .end local v13    # "telecastEnd":Ljava/util/Calendar;
    .end local v14    # "telecastDuration":J
    .end local v16    # "telecastTime":J
    .local v6, "delay":J
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 269
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->removeMessages(I)V

    .line 272
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 278
    .end local v6    # "delay":J
    .end local v8    # "id":J
    .end local v12    # "onAir":Z
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->noSchedule:Landroid/view/View;

    move-object/from16 v18, v0

    const/16 v19, 0x8

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->setVisibility(I)V

    .line 279
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->adapter:Lru/cn/tv/mobile/schedule/ScheduleAdapter;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lru/cn/tv/mobile/schedule/ScheduleAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 281
    move-object/from16 v0, p0

    iget-wide v0, v0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->currentTelecastId:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Lru/cn/tv/mobile/schedule/ScheduleFragment;->setCurrentTelecast(J)V

    goto/16 :goto_1

    .line 265
    .restart local v8    # "id":J
    .restart local v12    # "onAir":Z
    :cond_5
    const-wide/32 v6, 0xea60

    .restart local v6    # "delay":J
    goto :goto_3

    .line 275
    .end local v6    # "delay":J
    .end local v8    # "id":J
    :cond_6
    invoke-virtual {v4}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->moveToNext()Z

    goto/16 :goto_2
.end method

.method private updateData()V
    .locals 7

    .prologue
    .line 214
    invoke-virtual {p0}, Lru/cn/tv/mobile/schedule/ScheduleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 228
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    iget-object v0, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->adapter:Lru/cn/tv/mobile/schedule/ScheduleAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->currentDate:Ljava/util/Calendar;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->currentChannelId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->viewModel:Lru/cn/tv/mobile/schedule/ScheduleViewModel;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->adapter:Lru/cn/tv/mobile/schedule/ScheduleAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/schedule/ScheduleAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 222
    invoke-virtual {p0}, Lru/cn/tv/mobile/schedule/ScheduleFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lru/cn/tv/mobile/schedule/ScheduleFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->progressBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 225
    const-wide/16 v4, 0x0

    .line 226
    .local v4, "currentTerritoryId":J
    iget-object v1, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->viewModel:Lru/cn/tv/mobile/schedule/ScheduleViewModel;

    iget-wide v2, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->currentChannelId:J

    iget-object v6, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->currentDate:Ljava/util/Calendar;

    invoke-virtual/range {v1 .. v6}, Lru/cn/tv/mobile/schedule/ScheduleViewModel;->setChannel(JJLjava/util/Calendar;)V

    goto :goto_0
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$ScheduleFragment(Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/mobile/schedule/ScheduleFragment;->setSchedule(Landroid/database/Cursor;)V

    return-void
.end method

.method protected getItemHeight()I
    .locals 1

    .prologue
    .line 210
    const/16 v0, 0x30

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v0, 0x0

    .line 132
    invoke-virtual {p0}, Lru/cn/tv/mobile/schedule/ScheduleFragment;->isAdded()Z

    move-result v1

    if-nez v1, :cond_0

    .line 142
    :goto_0
    return v0

    .line 136
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 138
    :pswitch_0
    invoke-direct {p0}, Lru/cn/tv/mobile/schedule/ScheduleFragment;->updateData()V

    .line 139
    const/4 v0, 0x1

    goto :goto_0

    .line 136
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method final synthetic lambda$onViewCreated$0$ScheduleFragment(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 107
    iget-object v0, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->listener:Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->listener:Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;

    iget-wide v2, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->currentChannelId:J

    invoke-interface {v0, v2, v3}, Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;->onOnAirClicked(J)V

    .line 110
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    .line 66
    invoke-super {p0, p1}, Lru/cn/view/CustomListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 67
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v5, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->mHandler:Landroid/os/Handler;

    .line 69
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v5

    const-class v6, Lru/cn/tv/mobile/schedule/ScheduleViewModel;

    invoke-static {p0, v5, v6}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v5

    check-cast v5, Lru/cn/tv/mobile/schedule/ScheduleViewModel;

    iput-object v5, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->viewModel:Lru/cn/tv/mobile/schedule/ScheduleViewModel;

    .line 70
    iget-object v5, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->viewModel:Lru/cn/tv/mobile/schedule/ScheduleViewModel;

    .line 71
    invoke-virtual {v5}, Lru/cn/tv/mobile/schedule/ScheduleViewModel;->schedule()Landroid/arch/lifecycle/LiveData;

    move-result-object v5

    new-instance v6, Lru/cn/tv/mobile/schedule/ScheduleFragment$$Lambda$0;

    invoke-direct {v6, p0}, Lru/cn/tv/mobile/schedule/ScheduleFragment$$Lambda$0;-><init>(Lru/cn/tv/mobile/schedule/ScheduleFragment;)V

    .line 72
    invoke-virtual {v5, p0, v6}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 74
    invoke-virtual {p0}, Lru/cn/tv/mobile/schedule/ScheduleFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 75
    .local v0, "arguments":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 76
    const-string v5, "channelId"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->currentChannelId:J

    .line 77
    const-string v5, "timestampMs"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 79
    .local v2, "timestampMs":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 80
    .local v1, "date":Ljava/util/Calendar;
    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 81
    const-string v5, "timezoneOffset"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 82
    .local v4, "timezoneOffset":I
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/util/TimeZone;->setRawOffset(I)V

    .line 84
    const/16 v5, 0xa

    invoke-virtual {v1, v5, v8}, Ljava/util/Calendar;->set(II)V

    .line 85
    const/16 v5, 0xc

    invoke-virtual {v1, v5, v8}, Ljava/util/Calendar;->set(II)V

    .line 86
    const/16 v5, 0xd

    invoke-virtual {v1, v5, v8}, Ljava/util/Calendar;->set(II)V

    .line 87
    const/16 v5, 0xe

    invoke-virtual {v1, v5, v8}, Ljava/util/Calendar;->set(II)V

    .line 88
    iput-object v1, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->currentDate:Ljava/util/Calendar;

    .line 90
    .end local v1    # "date":Ljava/util/Calendar;
    .end local v2    # "timestampMs":J
    .end local v4    # "timezoneOffset":I
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 94
    const v0, 0x7f0c00ac

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 123
    invoke-super {p0}, Lru/cn/view/CustomListFragment;->onDestroyView()V

    .line 125
    iget-object v0, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 128
    :cond_0
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 151
    invoke-super/range {p0 .. p5}, Lru/cn/view/CustomListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 154
    invoke-virtual {p0}, Lru/cn/tv/mobile/schedule/ScheduleFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, p3, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 156
    iget-object v1, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->adapter:Lru/cn/tv/mobile/schedule/ScheduleAdapter;

    invoke-virtual {v1, p3}, Lru/cn/tv/mobile/schedule/ScheduleAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/cursor/ScheduleItemCursor;

    .line 158
    .local v0, "cursor":Lru/cn/api/provider/cursor/ScheduleItemCursor;
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getTelecastId()J

    move-result-wide v2

    .line 160
    .local v2, "telecastId":J
    iget-object v1, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->listener:Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;

    if-eqz v1, :cond_0

    .line 161
    iget-object v1, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->listener:Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;

    invoke-interface {v1, v2, v3, v0}, Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;->onScheduleItemClicked(JLru/cn/api/provider/cursor/ScheduleItemCursor;)V

    .line 163
    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 99
    invoke-super {p0, p1, p2}, Lru/cn/view/CustomListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 101
    const v0, 0x7f090175

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->progressBar:Landroid/view/View;

    .line 102
    const v0, 0x7f09014b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->noSchedule:Landroid/view/View;

    .line 104
    const v0, 0x7f0900e8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->goToAirButton:Landroid/view/View;

    .line 105
    iget-object v0, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->goToAirButton:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->goToAirButton:Landroid/view/View;

    new-instance v1, Lru/cn/tv/mobile/schedule/ScheduleFragment$$Lambda$1;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/schedule/ScheduleFragment$$Lambda$1;-><init>(Lru/cn/tv/mobile/schedule/ScheduleFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    :cond_0
    new-instance v0, Lru/cn/tv/mobile/schedule/ScheduleAdapter;

    invoke-virtual {p0}, Lru/cn/tv/mobile/schedule/ScheduleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lru/cn/tv/mobile/schedule/ScheduleAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->adapter:Lru/cn/tv/mobile/schedule/ScheduleAdapter;

    .line 115
    iget-object v0, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->adapter:Lru/cn/tv/mobile/schedule/ScheduleAdapter;

    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/schedule/ScheduleFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 116
    invoke-virtual {p0}, Lru/cn/tv/mobile/schedule/ScheduleFragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 117
    invoke-direct {p0}, Lru/cn/tv/mobile/schedule/ScheduleFragment;->updateData()V

    .line 119
    :cond_1
    return-void
.end method

.method public setCurrentTelecast(J)V
    .locals 11
    .param p1, "telecastId"    # J

    .prologue
    .line 166
    iput-wide p1, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->currentTelecastId:J

    .line 167
    iget-object v5, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->adapter:Lru/cn/tv/mobile/schedule/ScheduleAdapter;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->adapter:Lru/cn/tv/mobile/schedule/ScheduleAdapter;

    invoke-virtual {v5}, Lru/cn/tv/mobile/schedule/ScheduleAdapter;->getCount()I

    move-result v5

    if-lez v5, :cond_0

    .line 168
    iget-object v5, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->adapter:Lru/cn/tv/mobile/schedule/ScheduleAdapter;

    invoke-virtual {v5}, Lru/cn/tv/mobile/schedule/ScheduleAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/cursor/ScheduleItemCursor;

    .line 169
    .local v0, "c":Lru/cn/api/provider/cursor/ScheduleItemCursor;
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->isClosed()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 198
    .end local v0    # "c":Lru/cn/api/provider/cursor/ScheduleItemCursor;
    :cond_0
    :goto_0
    return-void

    .line 173
    .restart local v0    # "c":Lru/cn/api/provider/cursor/ScheduleItemCursor;
    :cond_1
    const/4 v1, -0x1

    .line 174
    .local v1, "currentPosition":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 176
    .local v2, "nowMs":J
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->moveToFirst()Z

    .line 177
    :goto_1
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_4

    .line 178
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getPosition()I

    move-result v4

    .line 181
    .local v4, "position":I
    invoke-virtual {v0, v2, v3}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->isOnTime(J)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 182
    move v1, v4

    .line 185
    :cond_2
    iget-wide v6, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->currentTelecastId:J

    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getTelecastId()J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-nez v5, :cond_3

    .line 186
    invoke-virtual {p0, v4}, Lru/cn/tv/mobile/schedule/ScheduleFragment;->selectPosition(I)V

    .line 188
    invoke-virtual {p0}, Lru/cn/tv/mobile/schedule/ScheduleFragment;->getListView()Landroid/widget/ListView;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v4, v6}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_0

    .line 191
    :cond_3
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->moveToNext()Z

    goto :goto_1

    .line 194
    .end local v4    # "position":I
    :cond_4
    const/4 v5, -0x1

    if-eq v1, v5, :cond_0

    .line 195
    invoke-virtual {p0, v1}, Lru/cn/tv/mobile/schedule/ScheduleFragment;->selectPosition(I)V

    goto :goto_0
.end method

.method public setListener(Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;

    .prologue
    .line 146
    iput-object p1, p0, Lru/cn/tv/mobile/schedule/ScheduleFragment;->listener:Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;

    .line 147
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 0
    .param p1, "isVisibleToUser"    # Z

    .prologue
    .line 202
    invoke-super {p0, p1}, Lru/cn/view/CustomListFragment;->setUserVisibleHint(Z)V

    .line 203
    if-eqz p1, :cond_0

    .line 204
    invoke-direct {p0}, Lru/cn/tv/mobile/schedule/ScheduleFragment;->updateData()V

    .line 206
    :cond_0
    return-void
.end method
