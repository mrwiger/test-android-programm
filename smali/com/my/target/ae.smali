.class public Lcom/my/target/ae;
.super Ljava/lang/Object;
.source "AdService.java"


# instance fields
.field private allowCloseDelay:F

.field private final bM:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/ae;",
            ">;"
        }
    .end annotation
.end field

.field private final bN:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/aq;",
            ">;"
        }
    .end annotation
.end field

.field private bO:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/aq;",
            ">;"
        }
    .end annotation
.end field

.field private bP:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/aq;",
            ">;"
        }
    .end annotation
.end field

.field private bQ:Lcom/my/target/ae;

.field private bR:Ljava/lang/String;

.field private bS:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/ap;",
            ">;"
        }
    .end annotation
.end field

.field private bT:I

.field private bU:I

.field private bV:Z

.field private bW:Z

.field private bX:Z

.field private bY:Ljava/lang/Boolean;

.field private bZ:Ljava/lang/Boolean;

.field private ca:Ljava/lang/Boolean;

.field private cb:Ljava/lang/Boolean;

.field private cc:Ljava/lang/Boolean;

.field private companionBanners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/ai;",
            ">;"
        }
    .end annotation
.end field

.field private ctaText:Ljava/lang/String;

.field private id:I

.field private point:F

.field private pointP:F

.field private position:I

.field private final url:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/high16 v1, -0x40800000    # -1.0f

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/ae;->bM:Ljava/util/ArrayList;

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/ae;->bN:Ljava/util/ArrayList;

    .line 38
    iput v2, p0, Lcom/my/target/ae;->id:I

    .line 39
    iput v2, p0, Lcom/my/target/ae;->position:I

    .line 40
    iput v2, p0, Lcom/my/target/ae;->bU:I

    .line 41
    iput v1, p0, Lcom/my/target/ae;->point:F

    .line 42
    iput v1, p0, Lcom/my/target/ae;->pointP:F

    .line 46
    iput v1, p0, Lcom/my/target/ae;->allowCloseDelay:F

    .line 55
    iput-object p1, p0, Lcom/my/target/ae;->url:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public static m(Ljava/lang/String;)Lcom/my/target/ae;
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/my/target/ae;

    invoke-direct {v0, p0}, Lcom/my/target/ae;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public A()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/my/target/ae;->bX:Z

    return v0
.end method

.method public B()Lcom/my/target/ae;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/my/target/ae;->bQ:Lcom/my/target/ae;

    return-object v0
.end method

.method public C()Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/my/target/ae;->bV:Z

    return v0
.end method

.method public D()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 119
    iget-object v0, p0, Lcom/my/target/ae;->bM:Ljava/util/ArrayList;

    return-object v0
.end method

.method public E()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lcom/my/target/ae;->bO:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 144
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/my/target/ae;->bO:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 146
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public F()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 151
    iget-object v0, p0, Lcom/my/target/ae;->bP:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 153
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/my/target/ae;->bP:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 155
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public G()I
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lcom/my/target/ae;->bT:I

    return v0
.end method

.method public H()Ljava/lang/String;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/my/target/ae;->bR:Ljava/lang/String;

    return-object v0
.end method

.method public I()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/ap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 284
    iget-object v0, p0, Lcom/my/target/ae;->bS:Ljava/util/ArrayList;

    return-object v0
.end method

.method public J()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/my/target/ae;->bY:Ljava/lang/Boolean;

    return-object v0
.end method

.method public K()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/my/target/ae;->bZ:Ljava/lang/Boolean;

    return-object v0
.end method

.method public L()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/my/target/ae;->ca:Ljava/lang/Boolean;

    return-object v0
.end method

.method public M()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/my/target/ae;->cb:Ljava/lang/Boolean;

    return-object v0
.end method

.method public N()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lcom/my/target/ae;->cc:Ljava/lang/Boolean;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 185
    iput p1, p0, Lcom/my/target/ae;->bT:I

    .line 186
    return-void
.end method

.method public a(Lcom/my/target/ae;)V
    .locals 1

    .prologue
    .line 90
    iput-object p1, p0, Lcom/my/target/ae;->bQ:Lcom/my/target/ae;

    .line 91
    if-eqz p1, :cond_0

    .line 93
    iget v0, p0, Lcom/my/target/ae;->position:I

    invoke-virtual {p1, v0}, Lcom/my/target/ae;->b(I)V

    .line 95
    :cond_0
    return-void
.end method

.method public a(Lcom/my/target/aq;)V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/my/target/ae;->bN:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    return-void
.end method

.method public a(Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 314
    iput-object p1, p0, Lcom/my/target/ae;->bY:Ljava/lang/Boolean;

    .line 315
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 215
    iput p1, p0, Lcom/my/target/ae;->position:I

    .line 216
    iget-object v0, p0, Lcom/my/target/ae;->bQ:Lcom/my/target/ae;

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/my/target/ae;->bQ:Lcom/my/target/ae;

    invoke-virtual {v0, p1}, Lcom/my/target/ae;->b(I)V

    .line 220
    :cond_0
    return-void
.end method

.method public b(Lcom/my/target/ae;)V
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/my/target/ae;->bM:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 245
    return-void
.end method

.method public b(Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 324
    iput-object p1, p0, Lcom/my/target/ae;->bZ:Ljava/lang/Boolean;

    .line 325
    return-void
.end method

.method public b(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/ai;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 165
    iput-object p1, p0, Lcom/my/target/ae;->companionBanners:Ljava/util/ArrayList;

    .line 166
    return-void
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 234
    iput p1, p0, Lcom/my/target/ae;->bU:I

    .line 235
    return-void
.end method

.method public c(Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 334
    iput-object p1, p0, Lcom/my/target/ae;->ca:Ljava/lang/Boolean;

    .line 335
    return-void
.end method

.method public c(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/aq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 170
    iput-object p1, p0, Lcom/my/target/ae;->bO:Ljava/util/ArrayList;

    .line 171
    return-void
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/my/target/ae;->bW:Z

    .line 66
    return-void
.end method

.method public d(Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lcom/my/target/ae;->cb:Ljava/lang/Boolean;

    .line 345
    return-void
.end method

.method public d(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/aq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 175
    iput-object p1, p0, Lcom/my/target/ae;->bP:Ljava/util/ArrayList;

    .line 176
    return-void
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 109
    iput-boolean p1, p0, Lcom/my/target/ae;->bV:Z

    .line 110
    return-void
.end method

.method public e(Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 354
    iput-object p1, p0, Lcom/my/target/ae;->cc:Ljava/lang/Boolean;

    .line 355
    return-void
.end method

.method public e(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/aq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 249
    iget-object v0, p0, Lcom/my/target/ae;->bP:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 251
    iput-object p1, p0, Lcom/my/target/ae;->bP:Ljava/util/ArrayList;

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    if-eqz p1, :cond_0

    .line 257
    iget-object v0, p0, Lcom/my/target/ae;->bP:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public e(Z)V
    .locals 0

    .prologue
    .line 239
    iput-boolean p1, p0, Lcom/my/target/ae;->bX:Z

    .line 240
    return-void
.end method

.method public f(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/aq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 264
    iget-object v0, p0, Lcom/my/target/ae;->bO:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 266
    iput-object p1, p0, Lcom/my/target/ae;->bO:Ljava/util/ArrayList;

    .line 275
    :cond_0
    :goto_0
    return-void

    .line 270
    :cond_1
    if-eqz p1, :cond_0

    .line 272
    iget-object v0, p0, Lcom/my/target/ae;->bO:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public g(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/ap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 279
    iput-object p1, p0, Lcom/my/target/ae;->bS:Ljava/util/ArrayList;

    .line 280
    return-void
.end method

.method public getAllowCloseDelay()F
    .locals 1

    .prologue
    .line 299
    iget v0, p0, Lcom/my/target/ae;->allowCloseDelay:F

    return v0
.end method

.method public getCompanionBanners()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/ai;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    iget-object v0, p0, Lcom/my/target/ae;->companionBanners:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCtaText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/my/target/ae;->ctaText:Ljava/lang/String;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/my/target/ae;->id:I

    return v0
.end method

.method public getPoint()F
    .locals 1

    .prologue
    .line 190
    iget v0, p0, Lcom/my/target/ae;->point:F

    return v0
.end method

.method public getPointP()F
    .locals 1

    .prologue
    .line 200
    iget v0, p0, Lcom/my/target/ae;->pointP:F

    return v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lcom/my/target/ae;->position:I

    return v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/my/target/ae;->url:Ljava/lang/String;

    return-object v0
.end method

.method public n(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 130
    iget-object v0, p0, Lcom/my/target/ae;->bN:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/aq;

    .line 132
    invoke-virtual {v0}, Lcom/my/target/aq;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 134
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 137
    :cond_1
    return-object v1
.end method

.method public o(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/my/target/ae;->bR:Ljava/lang/String;

    .line 230
    return-void
.end method

.method public setAllowCloseDelay(F)V
    .locals 0
    .param p1, "allowCloseDelay"    # F

    .prologue
    .line 304
    iput p1, p0, Lcom/my/target/ae;->allowCloseDelay:F

    .line 305
    return-void
.end method

.method public setCtaText(Ljava/lang/String;)V
    .locals 0
    .param p1, "ctaText"    # Ljava/lang/String;

    .prologue
    .line 294
    iput-object p1, p0, Lcom/my/target/ae;->ctaText:Ljava/lang/String;

    .line 295
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/my/target/ae;->id:I

    .line 86
    return-void
.end method

.method public setPoint(F)V
    .locals 0
    .param p1, "point"    # F

    .prologue
    .line 195
    iput p1, p0, Lcom/my/target/ae;->point:F

    .line 196
    return-void
.end method

.method public setPointP(F)V
    .locals 0
    .param p1, "pointP"    # F

    .prologue
    .line 205
    iput p1, p0, Lcom/my/target/ae;->pointP:F

    .line 206
    return-void
.end method

.method public y()Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/my/target/ae;->bW:Z

    return v0
.end method

.method public z()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/my/target/ae;->bU:I

    return v0
.end method
