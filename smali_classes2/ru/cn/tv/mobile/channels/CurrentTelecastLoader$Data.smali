.class Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;
.super Ljava/lang/Object;
.source "CurrentTelecastLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/mobile/channels/CurrentTelecastLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Data"
.end annotation


# instance fields
.field duration:J

.field time:J

.field title:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lru/cn/tv/mobile/channels/CurrentTelecastLoader$1;)V
    .locals 0
    .param p1, "x0"    # Lru/cn/tv/mobile/channels/CurrentTelecastLoader$1;

    .prologue
    .line 20
    invoke-direct {p0}, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;-><init>()V

    return-void
.end method


# virtual methods
.method public isOutdated()Z
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    .line 26
    iget-wide v2, p0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;->time:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;->duration:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 32
    :cond_0
    :goto_0
    return v0

    .line 29
    :cond_1
    iget-wide v2, p0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;->time:J

    iget-wide v4, p0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;->duration:J

    add-long/2addr v2, v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 32
    const/4 v0, 0x0

    goto :goto_0
.end method
