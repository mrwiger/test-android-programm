.class public Lru/cn/player/TimeoutHandler;
.super Ljava/lang/Object;
.source "TimeoutHandler.java"


# instance fields
.field private final timeoutHandler:Landroid/os/Handler;

.field private timeoutRunnable:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Landroid/os/Handler;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lru/cn/player/TimeoutHandler;->timeoutHandler:Landroid/os/Handler;

    .line 12
    return-void
.end method


# virtual methods
.method public reset()V
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/player/TimeoutHandler;->timeoutRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lru/cn/player/TimeoutHandler;->timeoutHandler:Landroid/os/Handler;

    iget-object v1, p0, Lru/cn/player/TimeoutHandler;->timeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/player/TimeoutHandler;->timeoutRunnable:Ljava/lang/Runnable;

    .line 31
    :cond_0
    return-void
.end method

.method public startTimer(Ljava/lang/Runnable;I)V
    .locals 4
    .param p1, "runnable"    # Ljava/lang/Runnable;
    .param p2, "timeoutMillis"    # I

    .prologue
    .line 18
    iget-object v0, p0, Lru/cn/player/TimeoutHandler;->timeoutRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 19
    invoke-virtual {p0}, Lru/cn/player/TimeoutHandler;->reset()V

    .line 22
    :cond_0
    iput-object p1, p0, Lru/cn/player/TimeoutHandler;->timeoutRunnable:Ljava/lang/Runnable;

    .line 23
    iget-object v0, p0, Lru/cn/player/TimeoutHandler;->timeoutHandler:Landroid/os/Handler;

    int-to-long v2, p2

    invoke-virtual {v0, p1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 24
    return-void
.end method
