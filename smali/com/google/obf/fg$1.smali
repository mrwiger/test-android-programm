.class Lcom/google/obf/fg$1;
.super Lcom/google/obf/ew;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/obf/fg;->a(Lcom/google/obf/eg;Lcom/google/obf/gd;)Lcom/google/obf/ew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/obf/ew",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:Z

.field final synthetic c:Lcom/google/obf/eg;

.field final synthetic d:Lcom/google/obf/gd;

.field final synthetic e:Lcom/google/obf/fg;

.field private f:Lcom/google/obf/ew;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/ew",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/obf/fg;ZZLcom/google/obf/eg;Lcom/google/obf/gd;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/obf/fg$1;->e:Lcom/google/obf/fg;

    iput-boolean p2, p0, Lcom/google/obf/fg$1;->a:Z

    iput-boolean p3, p0, Lcom/google/obf/fg$1;->b:Z

    iput-object p4, p0, Lcom/google/obf/fg$1;->c:Lcom/google/obf/eg;

    iput-object p5, p0, Lcom/google/obf/fg$1;->d:Lcom/google/obf/gd;

    invoke-direct {p0}, Lcom/google/obf/ew;-><init>()V

    return-void
.end method

.method private a()Lcom/google/obf/ew;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/obf/ew",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 11
    iget-object v0, p0, Lcom/google/obf/fg$1;->f:Lcom/google/obf/ew;

    .line 12
    if-eqz v0, :cond_0

    .line 13
    :goto_0
    return-object v0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/google/obf/fg$1;->c:Lcom/google/obf/eg;

    iget-object v1, p0, Lcom/google/obf/fg$1;->e:Lcom/google/obf/fg;

    iget-object v2, p0, Lcom/google/obf/fg$1;->d:Lcom/google/obf/gd;

    .line 13
    invoke-virtual {v0, v1, v2}, Lcom/google/obf/eg;->a(Lcom/google/obf/ex;Lcom/google/obf/gd;)Lcom/google/obf/ew;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/fg$1;->f:Lcom/google/obf/ew;

    goto :goto_0
.end method


# virtual methods
.method public read(Lcom/google/obf/ge;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/ge;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2
    iget-boolean v0, p0, Lcom/google/obf/fg$1;->a:Z

    if-eqz v0, :cond_0

    .line 3
    invoke-virtual {p1}, Lcom/google/obf/ge;->n()V

    .line 4
    const/4 v0, 0x0

    .line 5
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/obf/fg$1;->a()Lcom/google/obf/ew;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/obf/ew;->read(Lcom/google/obf/ge;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public write(Lcom/google/obf/gg;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/gg;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6
    iget-boolean v0, p0, Lcom/google/obf/fg$1;->b:Z

    if-eqz v0, :cond_0

    .line 7
    invoke-virtual {p1}, Lcom/google/obf/gg;->f()Lcom/google/obf/gg;

    .line 10
    :goto_0
    return-void

    .line 9
    :cond_0
    invoke-direct {p0}, Lcom/google/obf/fg$1;->a()Lcom/google/obf/ew;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/obf/ew;->write(Lcom/google/obf/gg;Ljava/lang/Object;)V

    goto :goto_0
.end method
