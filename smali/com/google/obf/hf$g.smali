.class public final enum Lcom/google/obf/hf$g;
.super Ljava/lang/Enum;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/hf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "g"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/obf/hf$g;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/obf/hf$g;

.field public static final enum b:Lcom/google/obf/hf$g;

.field private static final synthetic d:[Lcom/google/obf/hf$g;


# instance fields
.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 7
    new-instance v0, Lcom/google/obf/hf$g;

    const-string v1, "TYPE_VIDEO"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/obf/hf$g;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/obf/hf$g;->a:Lcom/google/obf/hf$g;

    .line 8
    new-instance v0, Lcom/google/obf/hf$g;

    const-string v1, "TYPE_AUDIO"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/obf/hf$g;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/obf/hf$g;->b:Lcom/google/obf/hf$g;

    .line 9
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/obf/hf$g;

    sget-object v1, Lcom/google/obf/hf$g;->a:Lcom/google/obf/hf$g;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/obf/hf$g;->b:Lcom/google/obf/hf$g;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/obf/hf$g;->d:[Lcom/google/obf/hf$g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 5
    iput p3, p0, Lcom/google/obf/hf$g;->c:I

    .line 6
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/obf/hf$g;
    .locals 1

    .prologue
    .line 2
    const-class v0, Lcom/google/obf/hf$g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/obf/hf$g;

    return-object v0
.end method

.method public static values()[Lcom/google/obf/hf$g;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lcom/google/obf/hf$g;->d:[Lcom/google/obf/hf$g;

    invoke-virtual {v0}, [Lcom/google/obf/hf$g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/obf/hf$g;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 3
    iget v0, p0, Lcom/google/obf/hf$g;->c:I

    return v0
.end method
