.class public interface abstract Lru/cn/api/catalogue/retrofit/CatalogueApi;
.super Ljava/lang/Object;
.source "CatalogueApi.java"


# virtual methods
.method public abstract items(JIILjava/lang/String;)Lio/reactivex/Single;
    .param p1    # J
        .annotation runtime Lretrofit2/http/Query;
            value = "rubric"
        .end annotation
    .end param
    .param p3    # I
        .annotation runtime Lretrofit2/http/Query;
            value = "count"
        .end annotation
    .end param
    .param p4    # I
        .annotation runtime Lretrofit2/http/Query;
            value = "skip"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Query;
            value = "rubric_options"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JII",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lru/cn/api/catalogue/replies/SelectionPage;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "select.json"
    .end annotation
.end method

.method public abstract items(JLjava/lang/String;)Lio/reactivex/Single;
    .param p1    # J
        .annotation runtime Lretrofit2/http/Query;
            value = "id"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Query;
            value = "fields"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lru/cn/api/catalogue/replies/SelectionPage;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "select.json"
    .end annotation
.end method

.method public abstract logo(JLjava/lang/String;)Lretrofit2/Call;
    .param p1    # J
        .annotation runtime Lretrofit2/http/Query;
            value = "rubric"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Query;
            value = "profile"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            ")",
            "Lretrofit2/Call",
            "<[B>;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "logo.png"
    .end annotation
.end method

.method public abstract rubricator(Ljava/lang/Long;)Lio/reactivex/Single;
    .param p1    # Ljava/lang/Long;
        .annotation runtime Lretrofit2/http/Query;
            value = "rubric"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lru/cn/api/catalogue/replies/Rubricator;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "rubricator.json"
    .end annotation
.end method
