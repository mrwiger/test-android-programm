.class Lcom/yandex/metrica/impl/ob/en;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lcom/yandex/metrica/impl/ob/dv;

.field private c:Lcom/yandex/metrica/impl/al;

.field private d:Lcom/yandex/metrica/impl/ob/ct;

.field private e:Lcom/yandex/metrica/impl/ob/cs;

.field private f:Lcom/yandex/metrica/impl/utils/q;

.field private g:Lcom/yandex/metrica/impl/ob/ei;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/al;Lcom/yandex/metrica/impl/ob/ct;Lcom/yandex/metrica/impl/ob/cs;Lcom/yandex/metrica/impl/utils/q;Lcom/yandex/metrica/impl/ob/ei;)V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/en;->a:Landroid/content/Context;

    .line 155
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/en;->b:Lcom/yandex/metrica/impl/ob/dv;

    .line 156
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/en;->c:Lcom/yandex/metrica/impl/al;

    .line 157
    iput-object p4, p0, Lcom/yandex/metrica/impl/ob/en;->d:Lcom/yandex/metrica/impl/ob/ct;

    .line 158
    iput-object p5, p0, Lcom/yandex/metrica/impl/ob/en;->e:Lcom/yandex/metrica/impl/ob/cs;

    .line 159
    iput-object p6, p0, Lcom/yandex/metrica/impl/ob/en;->f:Lcom/yandex/metrica/impl/utils/q;

    .line 160
    iput-object p7, p0, Lcom/yandex/metrica/impl/ob/en;->g:Lcom/yandex/metrica/impl/ob/ei;

    .line 161
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/ct;Lcom/yandex/metrica/impl/ob/cs;)V
    .locals 8

    .prologue
    .line 46
    new-instance v3, Lcom/yandex/metrica/impl/al;

    invoke-direct {v3}, Lcom/yandex/metrica/impl/al;-><init>()V

    new-instance v6, Lcom/yandex/metrica/impl/utils/p;

    invoke-direct {v6}, Lcom/yandex/metrica/impl/utils/p;-><init>()V

    new-instance v7, Lcom/yandex/metrica/impl/ob/ei;

    invoke-direct {v7}, Lcom/yandex/metrica/impl/ob/ei;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v7}, Lcom/yandex/metrica/impl/ob/en;-><init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/al;Lcom/yandex/metrica/impl/ob/ct;Lcom/yandex/metrica/impl/ob/cs;Lcom/yandex/metrica/impl/utils/q;Lcom/yandex/metrica/impl/ob/ei;)V

    .line 48
    return-void
.end method

.method private a(Lcom/yandex/metrica/impl/ob/cm;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 99
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/en;->b:Lcom/yandex/metrica/impl/ob/dv;

    if-eqz v2, :cond_5

    .line 2118
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/en;->b:Lcom/yandex/metrica/impl/ob/dv;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/en;->b:Lcom/yandex/metrica/impl/ob/dv;

    iget v2, v2, Lcom/yandex/metrica/impl/ob/dv;->i:I

    int-to-long v2, v2

    .line 2123
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/cm;->a()J

    move-result-wide v4

    cmp-long v2, v4, v2

    if-ltz v2, :cond_1

    move v2, v0

    .line 2119
    :goto_0
    if-eqz v2, :cond_2

    move v2, v0

    .line 100
    :goto_1
    if-nez v2, :cond_0

    .line 2127
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/en;->b:Lcom/yandex/metrica/impl/ob/dv;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/en;->b:Lcom/yandex/metrica/impl/ob/dv;

    iget-wide v2, v2, Lcom/yandex/metrica/impl/ob/dv;->k:J

    .line 2132
    iget-object v4, p0, Lcom/yandex/metrica/impl/ob/en;->f:Lcom/yandex/metrica/impl/utils/q;

    invoke-interface {v4}, Lcom/yandex/metrica/impl/utils/q;->a()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/cm;->b()J

    move-result-wide v6

    sub-long/2addr v4, v6

    cmp-long v2, v4, v2

    if-lez v2, :cond_3

    move v2, v0

    .line 2128
    :goto_2
    if-eqz v2, :cond_4

    move v2, v0

    .line 100
    :goto_3
    if-eqz v2, :cond_5

    .line 114
    :cond_0
    :goto_4
    return v0

    :cond_1
    move v2, v1

    .line 2123
    goto :goto_0

    :cond_2
    move v2, v1

    .line 2119
    goto :goto_1

    :cond_3
    move v2, v1

    .line 2132
    goto :goto_2

    :cond_4
    move v2, v1

    .line 2128
    goto :goto_3

    :cond_5
    move v0, v1

    .line 114
    goto :goto_4
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 51
    .line 1088
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/en;->d:Lcom/yandex/metrica/impl/ob/ct;

    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/ob/en;->a(Lcom/yandex/metrica/impl/ob/cm;)Z

    move-result v0

    .line 1089
    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/en;->e:Lcom/yandex/metrica/impl/ob/cs;

    invoke-direct {p0, v3}, Lcom/yandex/metrica/impl/ob/en;->a(Lcom/yandex/metrica/impl/ob/cm;)Z

    move-result v3

    .line 1090
    if-nez v0, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    move v0, v1

    .line 51
    :goto_0
    if-eqz v0, :cond_4

    .line 2058
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/en;->b:Lcom/yandex/metrica/impl/ob/dv;

    if-eqz v0, :cond_4

    .line 2059
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/en;->g:Lcom/yandex/metrica/impl/ob/ei;

    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/en;->a:Landroid/content/Context;

    iget-object v4, p0, Lcom/yandex/metrica/impl/ob/en;->b:Lcom/yandex/metrica/impl/ob/dv;

    .line 2060
    invoke-virtual {v0, v3, v4}, Lcom/yandex/metrica/impl/ob/ei;->a(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/dv;)Lcom/yandex/metrica/impl/ob/ee;

    move-result-object v3

    .line 2061
    invoke-virtual {v3}, Lcom/yandex/metrica/impl/ob/ee;->b()Z

    move-result v0

    .line 2063
    if-eqz v0, :cond_3

    .line 2064
    invoke-virtual {v3}, Lcom/yandex/metrica/impl/ob/ee;->d()Lcom/yandex/metrica/impl/ob/es;

    move-result-object v4

    invoke-virtual {v4}, Lcom/yandex/metrica/impl/ob/es;->b()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2068
    :goto_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v4

    if-nez v4, :cond_4

    if-eqz v0, :cond_4

    .line 2073
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/en;->c:Lcom/yandex/metrica/impl/al;

    invoke-virtual {v0, v3}, Lcom/yandex/metrica/impl/al;->a(Lcom/yandex/metrica/impl/ak;)V

    .line 2074
    invoke-virtual {v3}, Lcom/yandex/metrica/impl/ob/ee;->c()Z

    move-result v0

    .line 2075
    if-nez v0, :cond_2

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/ob/ee;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_1

    :cond_1
    move v0, v2

    .line 1090
    goto :goto_0

    :cond_2
    move v0, v2

    .line 2075
    goto :goto_1

    .line 2082
    :cond_3
    invoke-virtual {v3}, Lcom/yandex/metrica/impl/ob/ee;->y()V

    .line 2077
    :cond_4
    return-void
.end method

.method public a(Lcom/yandex/metrica/impl/ob/dv;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/en;->b:Lcom/yandex/metrica/impl/ob/dv;

    .line 138
    return-void
.end method
