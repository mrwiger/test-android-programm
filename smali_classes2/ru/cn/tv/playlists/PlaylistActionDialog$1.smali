.class Lru/cn/tv/playlists/PlaylistActionDialog$1;
.super Ljava/lang/Object;
.source "PlaylistActionDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/playlists/PlaylistActionDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/playlists/PlaylistActionDialog;

.field final synthetic val$arguments:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lru/cn/tv/playlists/PlaylistActionDialog;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/playlists/PlaylistActionDialog;

    .prologue
    .line 38
    iput-object p1, p0, Lru/cn/tv/playlists/PlaylistActionDialog$1;->this$0:Lru/cn/tv/playlists/PlaylistActionDialog;

    iput-object p2, p0, Lru/cn/tv/playlists/PlaylistActionDialog$1;->val$arguments:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 41
    iget-object v5, p0, Lru/cn/tv/playlists/PlaylistActionDialog$1;->val$arguments:Landroid/os/Bundle;

    const-string v6, "playlistId"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 42
    .local v2, "playlistId":J
    iget-object v5, p0, Lru/cn/tv/playlists/PlaylistActionDialog$1;->val$arguments:Landroid/os/Bundle;

    const-string v6, "title"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 43
    .local v4, "title":Ljava/lang/String;
    iget-object v5, p0, Lru/cn/tv/playlists/PlaylistActionDialog$1;->val$arguments:Landroid/os/Bundle;

    const-string v6, "location"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 44
    .local v1, "location":Ljava/lang/String;
    invoke-static {v2, v3, v4, v1}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->newInstance(JLjava/lang/String;Ljava/lang/String;)Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    move-result-object v0

    .line 46
    .local v0, "editDialog":Lru/cn/tv/playlists/UserPlaylistAddEditDialog;
    iget-object v5, p0, Lru/cn/tv/playlists/PlaylistActionDialog$1;->this$0:Lru/cn/tv/playlists/PlaylistActionDialog;

    invoke-virtual {v5}, Lru/cn/tv/playlists/PlaylistActionDialog;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 47
    return-void
.end method
