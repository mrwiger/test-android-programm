.class public final Lcom/my/target/ek;
.super Landroid/widget/LinearLayout;
.source "CardTabletView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static final az:I

.field private static final bC:I

.field private static final bD:I

.field private static final bE:I

.field private static final bF:I

.field private static final bf:I


# instance fields
.field private final aU:Lcom/my/target/ca;

.field private final aX:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final aw:Lcom/my/target/cm;

.field private final bL:Landroid/widget/Button;

.field private be:Landroid/view/View$OnClickListener;

.field private final cG:Lcom/my/target/bv;

.field private final cH:Landroid/widget/TextView;

.field private final cI:Landroid/widget/TextView;

.field private final cJ:Landroid/widget/LinearLayout;

.field private final cn:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ek;->bC:I

    .line 34
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ek;->bf:I

    .line 35
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ek;->az:I

    .line 36
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ek;->bD:I

    .line 37
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ek;->bF:I

    .line 38
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ek;->bE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v1, 0x0

    const/16 v8, 0xc

    const/4 v7, -0x2

    const/4 v6, 0x1

    .line 63
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 58
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/my/target/ek;->aX:Ljava/util/HashMap;

    .line 64
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ek;->cH:Landroid/widget/TextView;

    .line 65
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ek;->cI:Landroid/widget/TextView;

    .line 66
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ek;->bL:Landroid/widget/Button;

    .line 67
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    .line 68
    new-instance v0, Lcom/my/target/bv;

    invoke-direct {v0, p1}, Lcom/my/target/bv;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ek;->cG:Lcom/my/target/bv;

    .line 69
    new-instance v0, Lcom/my/target/ca;

    invoke-direct {v0, p1}, Lcom/my/target/ca;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ek;->aU:Lcom/my/target/ca;

    .line 70
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ek;->cn:Landroid/widget/TextView;

    .line 71
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ek;->cJ:Landroid/widget/LinearLayout;

    .line 1195
    invoke-virtual {p0, v6}, Lcom/my/target/ek;->setOrientation(I)V

    .line 1196
    invoke-virtual {p0, v6}, Lcom/my/target/ek;->setGravity(I)V

    .line 1197
    iget-object v0, p0, Lcom/my/target/ek;->cJ:Landroid/widget/LinearLayout;

    const v3, -0x333334

    iget-object v2, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    invoke-virtual {v2, v6}, Lcom/my/target/cm;->n(I)I

    move-result v4

    move v2, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Lcom/my/target/cm;->a(Landroid/view/View;IIIII)V

    .line 1199
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1200
    iget-object v1, p0, Lcom/my/target/ek;->cJ:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1201
    iget-object v0, p0, Lcom/my/target/ek;->cJ:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1203
    iget-object v0, p0, Lcom/my/target/ek;->cG:Lcom/my/target/bv;

    sget v1, Lcom/my/target/ek;->bf:I

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setId(I)V

    .line 1205
    iget-object v0, p0, Lcom/my/target/ek;->bL:Landroid/widget/Button;

    sget v1, Lcom/my/target/ek;->bC:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setId(I)V

    .line 1206
    iget-object v0, p0, Lcom/my/target/ek;->bL:Landroid/widget/Button;

    iget-object v1, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    const/16 v2, 0xf

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    const/16 v4, 0xf

    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Lcom/my/target/cm;->n(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/Button;->setPadding(IIII)V

    .line 1207
    iget-object v0, p0, Lcom/my/target/ek;->bL:Landroid/widget/Button;

    iget-object v1, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setMinimumWidth(I)V

    .line 1208
    iget-object v0, p0, Lcom/my/target/ek;->bL:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1209
    iget-object v0, p0, Lcom/my/target/ek;->bL:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->setSingleLine()V

    .line 1210
    iget-object v0, p0, Lcom/my/target/ek;->bL:Landroid/widget/Button;

    sget v1, Lcom/my/target/eo;->textSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextSize(F)V

    .line 1211
    iget-object v0, p0, Lcom/my/target/ek;->bL:Landroid/widget/Button;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1212
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 1214
    iget-object v0, p0, Lcom/my/target/ek;->bL:Landroid/widget/Button;

    iget-object v1, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v9}, Lcom/my/target/cm;->n(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setElevation(F)V

    .line 1217
    :cond_0
    iget-object v0, p0, Lcom/my/target/ek;->bL:Landroid/widget/Button;

    const v1, -0xff540e

    const v2, -0xff8957

    iget-object v3, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    invoke-virtual {v3, v9}, Lcom/my/target/cm;->n(I)I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/my/target/cm;->a(Landroid/view/View;III)V

    .line 1218
    iget-object v0, p0, Lcom/my/target/ek;->bL:Landroid/widget/Button;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 1220
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1222
    iget-object v1, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v8}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    .line 1223
    invoke-virtual {v2, v8}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    .line 1224
    invoke-virtual {v3, v8}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    .line 1225
    invoke-virtual {v4, v8}, Lcom/my/target/cm;->n(I)I

    move-result v4

    .line 1222
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1226
    iput v6, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1227
    iget-object v1, p0, Lcom/my/target/ek;->bL:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1229
    iget-object v0, p0, Lcom/my/target/ek;->cH:Landroid/widget/TextView;

    sget v1, Lcom/my/target/ek;->az:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 1230
    iget-object v0, p0, Lcom/my/target/ek;->cH:Landroid/widget/TextView;

    sget v1, Lcom/my/target/eo;->textSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 1231
    iget-object v0, p0, Lcom/my/target/ek;->cH:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1232
    iget-object v0, p0, Lcom/my/target/ek;->cH:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1233
    iget-object v0, p0, Lcom/my/target/ek;->cH:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setLines(I)V

    .line 1234
    iget-object v0, p0, Lcom/my/target/ek;->cH:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1235
    iget-object v0, p0, Lcom/my/target/ek;->cH:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v8}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    invoke-virtual {v3, v6}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    invoke-virtual {v4, v6}, Lcom/my/target/cm;->n(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1236
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1237
    iget-object v1, p0, Lcom/my/target/ek;->cH:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1239
    iget-object v0, p0, Lcom/my/target/ek;->cI:Landroid/widget/TextView;

    sget v1, Lcom/my/target/ek;->bD:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 1240
    iget-object v0, p0, Lcom/my/target/ek;->cI:Landroid/widget/TextView;

    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1241
    iget-object v0, p0, Lcom/my/target/ek;->cI:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setLines(I)V

    .line 1242
    iget-object v0, p0, Lcom/my/target/ek;->cI:Landroid/widget/TextView;

    sget v1, Lcom/my/target/eo;->textSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 1243
    iget-object v0, p0, Lcom/my/target/ek;->cI:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1244
    iget-object v0, p0, Lcom/my/target/ek;->cI:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v8}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    invoke-virtual {v2, v6}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    invoke-virtual {v3, v6}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    invoke-virtual {v4, v8}, Lcom/my/target/cm;->n(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1245
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1246
    iget-object v1, p0, Lcom/my/target/ek;->cI:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1248
    iget-object v0, p0, Lcom/my/target/ek;->aU:Lcom/my/target/ca;

    sget v1, Lcom/my/target/ek;->bF:I

    invoke-virtual {v0, v1}, Lcom/my/target/ca;->setId(I)V

    .line 1249
    iget-object v0, p0, Lcom/my/target/ek;->aU:Lcom/my/target/ca;

    iget-object v1, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/my/target/ca;->setStarsPadding(F)V

    .line 1250
    iget-object v0, p0, Lcom/my/target/ek;->aU:Lcom/my/target/ca;

    iget-object v1, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    sget v2, Lcom/my/target/eo;->cX:I

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/ca;->setStarSize(I)V

    .line 1251
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1252
    iput v6, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1253
    iget-object v1, p0, Lcom/my/target/ek;->aU:Lcom/my/target/ca;

    invoke-virtual {v1, v0}, Lcom/my/target/ca;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1255
    iget-object v0, p0, Lcom/my/target/ek;->cn:Landroid/widget/TextView;

    sget v1, Lcom/my/target/ek;->bE:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 1256
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1257
    iget-object v1, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 1258
    iget-object v1, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 1259
    iget-object v1, p0, Lcom/my/target/ek;->cn:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1261
    iget-object v0, p0, Lcom/my/target/ek;->cG:Lcom/my/target/bv;

    invoke-virtual {p0, v0}, Lcom/my/target/ek;->addView(Landroid/view/View;)V

    .line 1262
    iget-object v0, p0, Lcom/my/target/ek;->cJ:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/my/target/ek;->addView(Landroid/view/View;)V

    .line 1263
    iget-object v0, p0, Lcom/my/target/ek;->cJ:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/ek;->cH:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1264
    iget-object v0, p0, Lcom/my/target/ek;->cJ:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/ek;->cI:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1265
    iget-object v0, p0, Lcom/my/target/ek;->cJ:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/ek;->aU:Lcom/my/target/ca;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1266
    iget-object v0, p0, Lcom/my/target/ek;->cJ:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/ek;->cn:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1267
    iget-object v0, p0, Lcom/my/target/ek;->cJ:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/ek;->bL:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 73
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;Lcom/my/target/af;)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .prologue
    const v2, -0x3a1508

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 114
    iput-object p1, p0, Lcom/my/target/ek;->be:Landroid/view/View$OnClickListener;

    .line 115
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 117
    :cond_0
    invoke-super {p0, v4}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    iget-object v0, p0, Lcom/my/target/ek;->bL:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    :goto_0
    return-void

    .line 121
    :cond_1
    iget-boolean v0, p2, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_2

    .line 123
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    iget-object v0, p0, Lcom/my/target/ek;->cJ:Landroid/widget/LinearLayout;

    const v3, -0x333334

    iget-object v4, p0, Lcom/my/target/ek;->aw:Lcom/my/target/cm;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/my/target/cm;->n(I)I

    move-result v4

    move v5, v1

    invoke-static/range {v0 .. v5}, Lcom/my/target/cm;->a(Landroid/view/View;IIIII)V

    .line 125
    invoke-static {p0, v1, v2}, Lcom/my/target/cm;->a(Landroid/view/View;II)V

    .line 126
    iget-object v0, p0, Lcom/my/target/ek;->bL:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    iget-object v0, p0, Lcom/my/target/ek;->cG:Lcom/my/target/bv;

    invoke-virtual {v0, p0}, Lcom/my/target/bv;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 128
    iget-object v0, p0, Lcom/my/target/ek;->aX:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/my/target/ek;->cG:Lcom/my/target/bv;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 132
    :cond_2
    invoke-virtual {p0, p0}, Lcom/my/target/ek;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 133
    iget-object v0, p0, Lcom/my/target/ek;->cJ:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 134
    iget-object v0, p0, Lcom/my/target/ek;->cG:Lcom/my/target/bv;

    invoke-virtual {v0, p0}, Lcom/my/target/bv;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 135
    iget-object v0, p0, Lcom/my/target/ek;->cH:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 136
    iget-object v0, p0, Lcom/my/target/ek;->cI:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 137
    iget-object v0, p0, Lcom/my/target/ek;->aU:Lcom/my/target/ca;

    invoke-virtual {v0, p0}, Lcom/my/target/ca;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 138
    iget-object v0, p0, Lcom/my/target/ek;->cn:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 140
    iget-object v0, p0, Lcom/my/target/ek;->aX:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/my/target/ek;->cG:Lcom/my/target/bv;

    iget-boolean v3, p2, Lcom/my/target/af;->cv:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    iget-object v0, p0, Lcom/my/target/ek;->aX:Ljava/util/HashMap;

    iget-boolean v2, p2, Lcom/my/target/af;->cD:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, p0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    iget-object v0, p0, Lcom/my/target/ek;->aX:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/my/target/ek;->cJ:Landroid/widget/LinearLayout;

    iget-boolean v3, p2, Lcom/my/target/af;->cD:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    iget-object v0, p0, Lcom/my/target/ek;->aX:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/my/target/ek;->cH:Landroid/widget/TextView;

    iget-boolean v3, p2, Lcom/my/target/af;->cs:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    iget-object v0, p0, Lcom/my/target/ek;->aX:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/my/target/ek;->cI:Landroid/widget/TextView;

    iget-boolean v3, p2, Lcom/my/target/af;->ct:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    iget-object v0, p0, Lcom/my/target/ek;->aX:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/my/target/ek;->aU:Lcom/my/target/ca;

    iget-boolean v3, p2, Lcom/my/target/af;->cw:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    iget-object v0, p0, Lcom/my/target/ek;->aX:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/my/target/ek;->cn:Landroid/widget/TextView;

    iget-boolean v3, p2, Lcom/my/target/af;->cB:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    iget-boolean v0, p2, Lcom/my/target/af;->cy:Z

    if-eqz v0, :cond_3

    .line 150
    iget-object v0, p0, Lcom/my/target/ek;->bL:Landroid/widget/Button;

    iget-object v1, p0, Lcom/my/target/ek;->be:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 154
    :cond_3
    iget-object v0, p0, Lcom/my/target/ek;->bL:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    iget-object v0, p0, Lcom/my/target/ek;->bL:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_0
.end method

.method public final getCacheImageView()Lcom/my/target/bv;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/my/target/ek;->cG:Lcom/my/target/bv;

    return-object v0
.end method

.method public final getCtaButtonView()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/my/target/ek;->bL:Landroid/widget/Button;

    return-object v0
.end method

.method public final getDescriptionTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/my/target/ek;->cI:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getDomainTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/my/target/ek;->cn:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getRatingView()Lcom/my/target/ca;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/my/target/ek;->aU:Lcom/my/target/ca;

    return-object v0
.end method

.method public final getTitleTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/my/target/ek;->cH:Landroid/widget/TextView;

    return-object v0
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 162
    iget-object v0, p0, Lcom/my/target/ek;->aX:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 190
    :goto_0
    return v0

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/my/target/ek;->aX:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 168
    goto :goto_0

    .line 170
    :cond_1
    invoke-virtual {p1, v2}, Landroid/view/View;->setClickable(Z)V

    .line 171
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    move v0, v2

    .line 190
    goto :goto_0

    .line 174
    :pswitch_1
    const v0, -0x3a1508

    invoke-virtual {p0, v0}, Lcom/my/target/ek;->setBackgroundColor(I)V

    goto :goto_1

    .line 177
    :pswitch_2
    invoke-virtual {p0, v1}, Lcom/my/target/ek;->setBackgroundColor(I)V

    .line 178
    iget-object v0, p0, Lcom/my/target/ek;->be:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_2

    .line 180
    iget-object v0, p0, Lcom/my/target/ek;->be:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 182
    :cond_2
    invoke-virtual {p0, v1}, Lcom/my/target/ek;->setBackgroundColor(I)V

    goto :goto_1

    .line 185
    :pswitch_3
    invoke-virtual {p0, v1}, Lcom/my/target/ek;->setBackgroundColor(I)V

    goto :goto_1

    .line 171
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
