.class public Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;
.super Lcom/google/android/exoplayer2/trackselection/BaseTrackSelection;
.source "AdaptiveTrackSelection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection$Factory;
    }
.end annotation


# instance fields
.field private final bandwidthFraction:F

.field private final bandwidthMeter:Lcom/google/android/exoplayer2/upstream/BandwidthMeter;

.field private final bufferedFractionToLiveEdgeForQualityIncrease:F

.field private final clock:Lcom/google/android/exoplayer2/util/Clock;

.field private lastBufferEvaluationMs:J

.field private final maxDurationForQualityDecreaseUs:J

.field private final maxInitialBitrate:I

.field private final minDurationForQualityIncreaseUs:J

.field private final minDurationToRetainAfterDiscardUs:J

.field private final minTimeBetweenBufferReevaluationMs:J

.field private playbackSpeed:F

.field private reason:I

.field private selectedIndex:I


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/source/TrackGroup;[ILcom/google/android/exoplayer2/upstream/BandwidthMeter;IJJJFFJLcom/google/android/exoplayer2/util/Clock;)V
    .locals 5
    .param p1, "group"    # Lcom/google/android/exoplayer2/source/TrackGroup;
    .param p2, "tracks"    # [I
    .param p3, "bandwidthMeter"    # Lcom/google/android/exoplayer2/upstream/BandwidthMeter;
    .param p4, "maxInitialBitrate"    # I
    .param p5, "minDurationForQualityIncreaseMs"    # J
    .param p7, "maxDurationForQualityDecreaseMs"    # J
    .param p9, "minDurationToRetainAfterDiscardMs"    # J
    .param p11, "bandwidthFraction"    # F
    .param p12, "bufferedFractionToLiveEdgeForQualityIncrease"    # F
    .param p13, "minTimeBetweenBufferReevaluationMs"    # J
    .param p15, "clock"    # Lcom/google/android/exoplayer2/util/Clock;

    .prologue
    .line 248
    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/trackselection/BaseTrackSelection;-><init>(Lcom/google/android/exoplayer2/source/TrackGroup;[I)V

    .line 249
    iput-object p3, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->bandwidthMeter:Lcom/google/android/exoplayer2/upstream/BandwidthMeter;

    .line 250
    iput p4, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->maxInitialBitrate:I

    .line 251
    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p5

    iput-wide v2, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->minDurationForQualityIncreaseUs:J

    .line 252
    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p7

    iput-wide v2, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->maxDurationForQualityDecreaseUs:J

    .line 253
    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p9

    iput-wide v2, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->minDurationToRetainAfterDiscardUs:J

    .line 254
    move/from16 v0, p11

    iput v0, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->bandwidthFraction:F

    .line 255
    move/from16 v0, p12

    iput v0, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->bufferedFractionToLiveEdgeForQualityIncrease:F

    .line 257
    move-wide/from16 v0, p13

    iput-wide v0, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->minTimeBetweenBufferReevaluationMs:J

    .line 258
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->clock:Lcom/google/android/exoplayer2/util/Clock;

    .line 259
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->playbackSpeed:F

    .line 260
    const-wide/high16 v2, -0x8000000000000000L

    invoke-direct {p0, v2, v3}, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->determineIdealSelectedIndex(J)I

    move-result v2

    iput v2, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->selectedIndex:I

    .line 261
    const/4 v2, 0x1

    iput v2, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->reason:I

    .line 262
    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v2, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->lastBufferEvaluationMs:J

    .line 263
    return-void
.end method

.method private determineIdealSelectedIndex(J)I
    .locals 11
    .param p1, "nowMs"    # J

    .prologue
    .line 372
    iget-object v7, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->bandwidthMeter:Lcom/google/android/exoplayer2/upstream/BandwidthMeter;

    invoke-interface {v7}, Lcom/google/android/exoplayer2/upstream/BandwidthMeter;->getBitrateEstimate()J

    move-result-wide v0

    .line 373
    .local v0, "bitrateEstimate":J
    const-wide/16 v8, -0x1

    cmp-long v7, v0, v8

    if-nez v7, :cond_1

    iget v7, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->maxInitialBitrate:I

    int-to-long v2, v7

    .line 375
    .local v2, "effectiveBitrate":J
    :goto_0
    const/4 v6, 0x0

    .line 376
    .local v6, "lowestBitrateNonBlacklistedIndex":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    iget v7, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->length:I

    if-ge v5, v7, :cond_4

    .line 377
    const-wide/high16 v8, -0x8000000000000000L

    cmp-long v7, p1, v8

    if-eqz v7, :cond_0

    invoke-virtual {p0, v5, p1, p2}, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->isBlacklisted(IJ)Z

    move-result v7

    if-nez v7, :cond_3

    .line 378
    :cond_0
    invoke-virtual {p0, v5}, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->getFormat(I)Lcom/google/android/exoplayer2/Format;

    move-result-object v4

    .line 379
    .local v4, "format":Lcom/google/android/exoplayer2/Format;
    iget v7, v4, Lcom/google/android/exoplayer2/Format;->bitrate:I

    int-to-float v7, v7

    iget v8, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->playbackSpeed:F

    mul-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    int-to-long v8, v7

    cmp-long v7, v8, v2

    if-gtz v7, :cond_2

    .line 386
    .end local v4    # "format":Lcom/google/android/exoplayer2/Format;
    .end local v5    # "i":I
    :goto_2
    return v5

    .line 373
    .end local v2    # "effectiveBitrate":J
    .end local v6    # "lowestBitrateNonBlacklistedIndex":I
    :cond_1
    long-to-float v7, v0

    iget v8, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->bandwidthFraction:F

    mul-float/2addr v7, v8

    float-to-long v2, v7

    goto :goto_0

    .line 382
    .restart local v2    # "effectiveBitrate":J
    .restart local v4    # "format":Lcom/google/android/exoplayer2/Format;
    .restart local v5    # "i":I
    .restart local v6    # "lowestBitrateNonBlacklistedIndex":I
    :cond_2
    move v6, v5

    .line 376
    .end local v4    # "format":Lcom/google/android/exoplayer2/Format;
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_4
    move v5, v6

    .line 386
    goto :goto_2
.end method

.method private minDurationForQualityIncreaseUs(J)J
    .locals 5
    .param p1, "availableDurationUs"    # J

    .prologue
    .line 390
    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v1, p1, v2

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->minDurationForQualityIncreaseUs:J

    cmp-long v1, p1, v2

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    .line 392
    .local v0, "isAvailableDurationTooShort":Z
    :goto_0
    if-eqz v0, :cond_1

    long-to-float v1, p1

    iget v2, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->bufferedFractionToLiveEdgeForQualityIncrease:F

    mul-float/2addr v1, v2

    float-to-long v2, v1

    :goto_1
    return-wide v2

    .line 390
    .end local v0    # "isAvailableDurationTooShort":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 392
    .restart local v0    # "isAvailableDurationTooShort":Z
    :cond_1
    iget-wide v2, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->minDurationForQualityIncreaseUs:J

    goto :goto_1
.end method


# virtual methods
.method public enable()V
    .locals 2

    .prologue
    .line 267
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v0, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->lastBufferEvaluationMs:J

    .line 268
    return-void
.end method

.method public evaluateQueueSize(JLjava/util/List;)I
    .locals 25
    .param p1, "playbackPositionUs"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/exoplayer2/source/chunk/MediaChunk;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 325
    .local p3, "queue":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/exoplayer2/source/chunk/MediaChunk;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->clock:Lcom/google/android/exoplayer2/util/Clock;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/google/android/exoplayer2/util/Clock;->elapsedRealtime()J

    move-result-wide v12

    .line 326
    .local v12, "nowMs":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->lastBufferEvaluationMs:J

    move-wide/from16 v20, v0

    const-wide v22, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v19, v20, v22

    if-eqz v19, :cond_1

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->lastBufferEvaluationMs:J

    move-wide/from16 v20, v0

    sub-long v20, v12, v20

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->minTimeBetweenBufferReevaluationMs:J

    move-wide/from16 v22, v0

    cmp-long v19, v20, v22

    if-gez v19, :cond_1

    .line 328
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v18

    .line 362
    :cond_0
    :goto_0
    return v18

    .line 330
    :cond_1
    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->lastBufferEvaluationMs:J

    .line 331
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->isEmpty()Z

    move-result v19

    if-eqz v19, :cond_2

    .line 332
    const/16 v18, 0x0

    goto :goto_0

    .line 335
    :cond_2
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v18

    .line 336
    .local v18, "queueSize":I
    add-int/lit8 v19, v18, -0x1

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/exoplayer2/source/chunk/MediaChunk;

    .line 337
    .local v9, "lastChunk":Lcom/google/android/exoplayer2/source/chunk/MediaChunk;
    iget-wide v0, v9, Lcom/google/android/exoplayer2/source/chunk/MediaChunk;->startTimeUs:J

    move-wide/from16 v20, v0

    sub-long v20, v20, p1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->playbackSpeed:F

    move/from16 v19, v0

    .line 338
    move-wide/from16 v0, v20

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer2/util/Util;->getPlayoutDurationForMediaDuration(JF)J

    move-result-wide v14

    .line 340
    .local v14, "playoutBufferedDurationBeforeLastChunkUs":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->minDurationToRetainAfterDiscardUs:J

    move-wide/from16 v20, v0

    cmp-long v19, v14, v20

    if-ltz v19, :cond_0

    .line 343
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->determineIdealSelectedIndex(J)I

    move-result v8

    .line 344
    .local v8, "idealSelectedIndex":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->getFormat(I)Lcom/google/android/exoplayer2/Format;

    move-result-object v7

    .line 348
    .local v7, "idealFormat":Lcom/google/android/exoplayer2/Format;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    move/from16 v0, v18

    if-ge v6, v0, :cond_0

    .line 349
    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/exoplayer2/source/chunk/MediaChunk;

    .line 350
    .local v4, "chunk":Lcom/google/android/exoplayer2/source/chunk/MediaChunk;
    iget-object v5, v4, Lcom/google/android/exoplayer2/source/chunk/MediaChunk;->trackFormat:Lcom/google/android/exoplayer2/Format;

    .line 351
    .local v5, "format":Lcom/google/android/exoplayer2/Format;
    iget-wide v0, v4, Lcom/google/android/exoplayer2/source/chunk/MediaChunk;->startTimeUs:J

    move-wide/from16 v20, v0

    sub-long v10, v20, p1

    .line 352
    .local v10, "mediaDurationBeforeThisChunkUs":J
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->playbackSpeed:F

    move/from16 v19, v0

    .line 353
    move/from16 v0, v19

    invoke-static {v10, v11, v0}, Lcom/google/android/exoplayer2/util/Util;->getPlayoutDurationForMediaDuration(JF)J

    move-result-wide v16

    .line 354
    .local v16, "playoutDurationBeforeThisChunkUs":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->minDurationToRetainAfterDiscardUs:J

    move-wide/from16 v20, v0

    cmp-long v19, v16, v20

    if-ltz v19, :cond_3

    iget v0, v5, Lcom/google/android/exoplayer2/Format;->bitrate:I

    move/from16 v19, v0

    iget v0, v7, Lcom/google/android/exoplayer2/Format;->bitrate:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_3

    iget v0, v5, Lcom/google/android/exoplayer2/Format;->height:I

    move/from16 v19, v0

    const/16 v20, -0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_3

    iget v0, v5, Lcom/google/android/exoplayer2/Format;->height:I

    move/from16 v19, v0

    const/16 v20, 0x2d0

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_3

    iget v0, v5, Lcom/google/android/exoplayer2/Format;->width:I

    move/from16 v19, v0

    const/16 v20, -0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_3

    iget v0, v5, Lcom/google/android/exoplayer2/Format;->width:I

    move/from16 v19, v0

    const/16 v20, 0x500

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_3

    iget v0, v5, Lcom/google/android/exoplayer2/Format;->height:I

    move/from16 v19, v0

    iget v0, v7, Lcom/google/android/exoplayer2/Format;->height:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_3

    move/from16 v18, v6

    .line 359
    goto/16 :goto_0

    .line 348
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_1
.end method

.method public getSelectedIndex()I
    .locals 1

    .prologue
    .line 310
    iget v0, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->selectedIndex:I

    return v0
.end method

.method public getSelectionData()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 320
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSelectionReason()I
    .locals 1

    .prologue
    .line 315
    iget v0, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->reason:I

    return v0
.end method

.method public onPlaybackSpeed(F)V
    .locals 0
    .param p1, "playbackSpeed"    # F

    .prologue
    .line 272
    iput p1, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->playbackSpeed:F

    .line 273
    return-void
.end method

.method public updateSelectedTrack(JJJ)V
    .locals 9
    .param p1, "playbackPositionUs"    # J
    .param p3, "bufferedDurationUs"    # J
    .param p5, "availableDurationUs"    # J

    .prologue
    .line 278
    iget-object v5, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->clock:Lcom/google/android/exoplayer2/util/Clock;

    invoke-interface {v5}, Lcom/google/android/exoplayer2/util/Clock;->elapsedRealtime()J

    move-result-wide v2

    .line 280
    .local v2, "nowMs":J
    iget v1, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->selectedIndex:I

    .line 281
    .local v1, "currentSelectedIndex":I
    invoke-direct {p0, v2, v3}, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->determineIdealSelectedIndex(J)I

    move-result v5

    iput v5, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->selectedIndex:I

    .line 282
    iget v5, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->selectedIndex:I

    if-ne v5, v1, :cond_1

    .line 306
    :cond_0
    :goto_0
    return-void

    .line 286
    :cond_1
    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->isBlacklisted(IJ)Z

    move-result v5

    if-nez v5, :cond_2

    .line 288
    invoke-virtual {p0, v1}, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->getFormat(I)Lcom/google/android/exoplayer2/Format;

    move-result-object v0

    .line 289
    .local v0, "currentFormat":Lcom/google/android/exoplayer2/Format;
    iget v5, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->selectedIndex:I

    invoke-virtual {p0, v5}, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->getFormat(I)Lcom/google/android/exoplayer2/Format;

    move-result-object v4

    .line 290
    .local v4, "selectedFormat":Lcom/google/android/exoplayer2/Format;
    iget v5, v4, Lcom/google/android/exoplayer2/Format;->bitrate:I

    iget v6, v0, Lcom/google/android/exoplayer2/Format;->bitrate:I

    if-le v5, v6, :cond_3

    .line 291
    invoke-direct {p0, p5, p6}, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->minDurationForQualityIncreaseUs(J)J

    move-result-wide v6

    cmp-long v5, p3, v6

    if-gez v5, :cond_3

    .line 294
    iput v1, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->selectedIndex:I

    .line 303
    .end local v0    # "currentFormat":Lcom/google/android/exoplayer2/Format;
    .end local v4    # "selectedFormat":Lcom/google/android/exoplayer2/Format;
    :cond_2
    :goto_1
    iget v5, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->selectedIndex:I

    if-eq v5, v1, :cond_0

    .line 304
    const/4 v5, 0x3

    iput v5, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->reason:I

    goto :goto_0

    .line 295
    .restart local v0    # "currentFormat":Lcom/google/android/exoplayer2/Format;
    .restart local v4    # "selectedFormat":Lcom/google/android/exoplayer2/Format;
    :cond_3
    iget v5, v4, Lcom/google/android/exoplayer2/Format;->bitrate:I

    iget v6, v0, Lcom/google/android/exoplayer2/Format;->bitrate:I

    if-ge v5, v6, :cond_2

    iget-wide v6, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->maxDurationForQualityDecreaseUs:J

    cmp-long v5, p3, v6

    if-ltz v5, :cond_2

    .line 299
    iput v1, p0, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection;->selectedIndex:I

    goto :goto_1
.end method
