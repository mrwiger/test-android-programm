.class public Lru/cn/player/VideoTrackSelector;
.super Ljava/lang/Object;
.source "VideoTrackSelector.java"

# interfaces
.implements Lru/cn/player/ITrackSelector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lru/cn/player/TrackInfo;",
        ">",
        "Ljava/lang/Object;",
        "Lru/cn/player/ITrackSelector;"
    }
.end annotation


# instance fields
.field private final trackSelector:Lru/cn/player/Selector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lru/cn/player/Selector",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lru/cn/player/Selector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/player/Selector",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 12
    .local p0, "this":Lru/cn/player/VideoTrackSelector;, "Lru/cn/player/VideoTrackSelector<TT;>;"
    .local p1, "trackSelector":Lru/cn/player/Selector;, "Lru/cn/player/Selector<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lru/cn/player/VideoTrackSelector;->trackSelector:Lru/cn/player/Selector;

    .line 14
    return-void
.end method


# virtual methods
.method public adaptive()Z
    .locals 1

    .prologue
    .line 56
    .local p0, "this":Lru/cn/player/VideoTrackSelector;, "Lru/cn/player/VideoTrackSelector<TT;>;"
    iget-object v0, p0, Lru/cn/player/VideoTrackSelector;->trackSelector:Lru/cn/player/Selector;

    invoke-interface {v0}, Lru/cn/player/Selector;->canAdaptiveVideoStreaming()Z

    move-result v0

    return v0
.end method

.method public containsTracks()Z
    .locals 2

    .prologue
    .local p0, "this":Lru/cn/player/VideoTrackSelector;, "Lru/cn/player/VideoTrackSelector<TT;>;"
    const/4 v0, 0x1

    .line 36
    iget-object v1, p0, Lru/cn/player/VideoTrackSelector;->trackSelector:Lru/cn/player/Selector;

    invoke-interface {v1}, Lru/cn/player/Selector;->getVideoTracksCount()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public deactivatable()Z
    .locals 1

    .prologue
    .line 46
    .local p0, "this":Lru/cn/player/VideoTrackSelector;, "Lru/cn/player/VideoTrackSelector<TT;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public disable()V
    .locals 1

    .prologue
    .line 51
    .local p0, "this":Lru/cn/player/VideoTrackSelector;, "Lru/cn/player/VideoTrackSelector<TT;>;"
    iget-object v0, p0, Lru/cn/player/VideoTrackSelector;->trackSelector:Lru/cn/player/Selector;

    invoke-interface {v0}, Lru/cn/player/Selector;->disableVideo()V

    .line 52
    return-void
.end method

.method public getCurrentTrackIndex()I
    .locals 1

    .prologue
    .line 31
    .local p0, "this":Lru/cn/player/VideoTrackSelector;, "Lru/cn/player/VideoTrackSelector<TT;>;"
    iget-object v0, p0, Lru/cn/player/VideoTrackSelector;->trackSelector:Lru/cn/player/Selector;

    invoke-interface {v0}, Lru/cn/player/Selector;->getCurrentVideoTrackIndex()I

    move-result v0

    return v0
.end method

.method public getTracksName(Lru/cn/player/ITrackSelector$TrackNameGenerator;)Ljava/util/List;
    .locals 3
    .param p1, "nameGenerator"    # Lru/cn/player/ITrackSelector$TrackNameGenerator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/player/ITrackSelector$TrackNameGenerator;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18
    .local p0, "this":Lru/cn/player/VideoTrackSelector;, "Lru/cn/player/VideoTrackSelector<TT;>;"
    iget-object v1, p0, Lru/cn/player/VideoTrackSelector;->trackSelector:Lru/cn/player/Selector;

    invoke-interface {v1}, Lru/cn/player/Selector;->getVideoTracks()Ljava/util/List;

    move-result-object v0

    .line 20
    .local v0, "videoTracks":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    .line 21
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 24
    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lru/cn/player/VideoTrackSelector$$Lambda$0;->get$Lambda(Lru/cn/player/ITrackSelector$TrackNameGenerator;)Lcom/annimon/stream/function/Function;

    move-result-object v2

    .line 25
    invoke-virtual {v1, v2}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v1

    .line 26
    invoke-virtual {v1}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method public selectItem(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 41
    .local p0, "this":Lru/cn/player/VideoTrackSelector;, "Lru/cn/player/VideoTrackSelector<TT;>;"
    iget-object v0, p0, Lru/cn/player/VideoTrackSelector;->trackSelector:Lru/cn/player/Selector;

    invoke-interface {v0, p1}, Lru/cn/player/Selector;->selectVideoTrack(I)V

    .line 42
    return-void
.end method

.method public setAdaptive()V
    .locals 1

    .prologue
    .line 61
    .local p0, "this":Lru/cn/player/VideoTrackSelector;, "Lru/cn/player/VideoTrackSelector<TT;>;"
    iget-object v0, p0, Lru/cn/player/VideoTrackSelector;->trackSelector:Lru/cn/player/Selector;

    invoke-interface {v0}, Lru/cn/player/Selector;->setAdaptiveVideoStream()V

    .line 62
    return-void
.end method
