.class public Lcom/my/target/cl;
.super Ljava/lang/Object;
.source "StatResolver.java"


# static fields
.field private static final kr:Lcom/my/target/cl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/my/target/cl;

    invoke-direct {v0}, Lcom/my/target/cl;-><init>()V

    sput-object v0, Lcom/my/target/cl;->kr:Lcom/my/target/cl;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    return-void
.end method

.method private R(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 150
    :try_start_0
    const-string v0, "UTF-8"

    invoke-static {p1, v0}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 156
    :goto_0
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/webkit/URLUtil;->isNetworkUrl(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 164
    :goto_1
    return-object v0

    .line 152
    :catch_0
    move-exception v0

    .line 154
    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    move-object v0, p1

    goto :goto_0

    .line 162
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid stat url: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 164
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic a(Lcom/my/target/cl;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/my/target/cl;->R(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/my/target/aq;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/my/target/cl;->kr:Lcom/my/target/cl;

    invoke-virtual {v0, p0, p1}, Lcom/my/target/cl;->b(Lcom/my/target/aq;Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method static synthetic a(Lcom/my/target/cl;Lcom/my/target/aq;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/my/target/cl;->c(Lcom/my/target/aq;)V

    return-void
.end method

.method public static a(Ljava/util/List;Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/aq;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38
    sget-object v0, Lcom/my/target/cl;->kr:Lcom/my/target/cl;

    invoke-virtual {v0, p0, p1}, Lcom/my/target/cl;->c(Ljava/util/List;Landroid/content/Context;)V

    .line 39
    return-void
.end method

.method public static b(Ljava/util/List;Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 48
    sget-object v0, Lcom/my/target/cl;->kr:Lcom/my/target/cl;

    invoke-virtual {v0, p0, p1}, Lcom/my/target/cl;->d(Ljava/util/List;Landroid/content/Context;)V

    .line 49
    return-void
.end method

.method private c(Lcom/my/target/aq;)V
    .locals 5

    .prologue
    .line 169
    instance-of v0, p1, Lcom/my/target/ap;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 171
    check-cast v0, Lcom/my/target/ap;

    invoke-virtual {v0}, Lcom/my/target/ap;->Z()F

    move-result v0

    .line 172
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tracking progress stat value:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " url:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/my/target/aq;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 194
    :goto_0
    return-void

    .line 174
    :cond_0
    instance-of v0, p1, Lcom/my/target/ao;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 176
    check-cast v0, Lcom/my/target/ao;

    .line 177
    invoke-virtual {v0}, Lcom/my/target/ao;->ag()I

    move-result v1

    .line 178
    invoke-virtual {v0}, Lcom/my/target/ao;->Z()F

    move-result v2

    .line 179
    invoke-virtual {v0}, Lcom/my/target/ao;->Y()Z

    move-result v0

    .line 180
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "tracking ovv stat percent:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " value:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ovv:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " url:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/my/target/aq;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 182
    :cond_1
    instance-of v0, p1, Lcom/my/target/an;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 184
    check-cast v0, Lcom/my/target/an;

    .line 185
    invoke-virtual {v0}, Lcom/my/target/an;->ag()I

    move-result v1

    .line 186
    invoke-virtual {v0}, Lcom/my/target/an;->Z()F

    move-result v2

    .line 187
    invoke-virtual {v0}, Lcom/my/target/an;->getDuration()F

    move-result v0

    .line 188
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "tracking mrc stat percent: value:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " percent "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " duration:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " url:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/my/target/aq;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 192
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tracking stat type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/my/target/aq;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " url:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/my/target/aq;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static n(Ljava/lang/String;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/my/target/cl;->kr:Lcom/my/target/cl;

    invoke-virtual {v0, p0, p1}, Lcom/my/target/cl;->o(Ljava/lang/String;Landroid/content/Context;)V

    .line 44
    return-void
.end method


# virtual methods
.method b(Lcom/my/target/aq;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 58
    if-eqz p1, :cond_0

    .line 60
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 61
    new-instance v1, Lcom/my/target/cl$1;

    invoke-direct {v1, p0, p1, v0}, Lcom/my/target/cl$1;-><init>(Lcom/my/target/cl;Lcom/my/target/aq;Landroid/content/Context;)V

    invoke-static {v1}, Lcom/my/target/h;->b(Ljava/lang/Runnable;)V

    .line 75
    :cond_0
    return-void
.end method

.method c(Ljava/util/List;Landroid/content/Context;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/aq;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 79
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 81
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 82
    new-instance v1, Lcom/my/target/cl$2;

    invoke-direct {v1, p0, p1, v0}, Lcom/my/target/cl$2;-><init>(Lcom/my/target/cl;Ljava/util/List;Landroid/content/Context;)V

    invoke-static {v1}, Lcom/my/target/h;->b(Ljava/lang/Runnable;)V

    .line 100
    :cond_0
    return-void
.end method

.method d(Ljava/util/List;Landroid/content/Context;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 124
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 126
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 127
    new-instance v1, Lcom/my/target/cl$4;

    invoke-direct {v1, p0, p1, v0}, Lcom/my/target/cl$4;-><init>(Lcom/my/target/cl;Ljava/util/List;Landroid/content/Context;)V

    invoke-static {v1}, Lcom/my/target/h;->b(Ljava/lang/Runnable;)V

    .line 144
    :cond_0
    return-void
.end method

.method o(Ljava/lang/String;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 104
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 107
    new-instance v1, Lcom/my/target/cl$3;

    invoke-direct {v1, p0, p1, v0}, Lcom/my/target/cl$3;-><init>(Lcom/my/target/cl;Ljava/lang/String;Landroid/content/Context;)V

    invoke-static {v1}, Lcom/my/target/h;->b(Ljava/lang/Runnable;)V

    .line 120
    :cond_0
    return-void
.end method
