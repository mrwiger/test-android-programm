.class public Lcom/yandex/metrica/impl/ob/fy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/yandex/metrica/impl/ob/fz;


# static fields
.field static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/yandex/metrica/impl/ob/fx;",
            "Lcom/yandex/metrica/IIdentifierCallback$Reason;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/yandex/metrica/impl/az;

.field private final c:Lcom/yandex/metrica/impl/ob/ga;

.field private final d:Lcom/yandex/metrica/impl/ob/dc;

.field private final e:Ljava/lang/Object;

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/yandex/metrica/IIdentifierCallback;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/yandex/metrica/IIdentifierCallback;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/yandex/metrica/impl/ob/fy$1;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/ob/fy$1;-><init>()V

    .line 35
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/yandex/metrica/impl/ob/fy;->a:Ljava/util/Map;

    .line 34
    return-void
.end method

.method public constructor <init>(Lcom/yandex/metrica/impl/az;Ljava/lang/String;Lcom/yandex/metrica/impl/ob/dc;)V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->e:Ljava/lang/Object;

    .line 50
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->f:Ljava/util/Map;

    .line 52
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->g:Ljava/util/Map;

    .line 63
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/fy;->b:Lcom/yandex/metrica/impl/az;

    .line 64
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/fy;->d:Lcom/yandex/metrica/impl/ob/dc;

    .line 65
    new-instance v0, Lcom/yandex/metrica/impl/ob/ga;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fy;->d:Lcom/yandex/metrica/impl/ob/dc;

    invoke-direct {v0, v1, p2}, Lcom/yandex/metrica/impl/ob/ga;-><init>(Lcom/yandex/metrica/impl/ob/dc;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->c:Lcom/yandex/metrica/impl/ob/ga;

    .line 66
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/fy;->e()V

    .line 67
    return-void
.end method

.method private e()V
    .locals 7

    .prologue
    .line 177
    new-instance v1, Ljava/util/WeakHashMap;

    invoke-direct {v1}, Ljava/util/WeakHashMap;-><init>()V

    .line 178
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 180
    new-instance v3, Ljava/util/WeakHashMap;

    invoke-direct {v3}, Ljava/util/WeakHashMap;-><init>()V

    .line 181
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 183
    iget-object v5, p0, Lcom/yandex/metrica/impl/ob/fy;->e:Ljava/lang/Object;

    monitor-enter v5

    .line 184
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->c:Lcom/yandex/metrica/impl/ob/ga;

    sget-object v6, Lcom/yandex/metrica/impl/ob/ga$a;->a:Lcom/yandex/metrica/impl/ob/ga$a;

    invoke-virtual {v0, v6}, Lcom/yandex/metrica/impl/ob/ga;->a(Lcom/yandex/metrica/impl/ob/ga$a;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->f:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 186
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 187
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->c:Lcom/yandex/metrica/impl/ob/ga;

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/ga;->b(Ljava/util/Map;)V

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->c:Lcom/yandex/metrica/impl/ob/ga;

    sget-object v6, Lcom/yandex/metrica/impl/ob/ga$a;->c:Lcom/yandex/metrica/impl/ob/ga$a;

    invoke-virtual {v0, v6}, Lcom/yandex/metrica/impl/ob/ga;->a(Lcom/yandex/metrica/impl/ob/ga$a;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 196
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->g:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 197
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 198
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->c:Lcom/yandex/metrica/impl/ob/ga;

    invoke-virtual {v0, v4}, Lcom/yandex/metrica/impl/ob/ga;->a(Ljava/util/Map;)V

    .line 205
    :cond_1
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/IIdentifierCallback;

    .line 208
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-interface {v0, v6}, Lcom/yandex/metrica/IIdentifierCallback;->onReceive(Ljava/util/Map;)V

    goto :goto_0

    .line 205
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 211
    :cond_2
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/IIdentifierCallback;

    .line 212
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6, v4}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-interface {v0, v6}, Lcom/yandex/metrica/IIdentifierCallback;->onReceive(Ljava/util/Map;)V

    goto :goto_1

    .line 215
    :cond_3
    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 216
    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 218
    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 219
    invoke-interface {v4}, Ljava/util/Map;->clear()V

    .line 220
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->c:Lcom/yandex/metrica/impl/ob/ga;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/ga;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 125
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fy;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 126
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->c:Lcom/yandex/metrica/impl/ob/ga;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/ga;->a(Landroid/os/Bundle;)V

    .line 127
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->c:Lcom/yandex/metrica/impl/ob/ga;

    .line 2029
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 127
    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/ga;->a(J)V

    .line 128
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/fy;->e()V

    .line 131
    return-void

    .line 128
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lcom/yandex/metrica/IIdentifierCallback;)V
    .locals 3

    .prologue
    .line 105
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fy;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 106
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->g:Ljava/util/Map;

    const/4 v2, 0x0

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->c:Lcom/yandex/metrica/impl/ob/ga;

    sget-object v2, Lcom/yandex/metrica/impl/ob/ga$a;->c:Lcom/yandex/metrica/impl/ob/ga$a;

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/ga;->a(Lcom/yandex/metrica/impl/ob/ga$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1121
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->b:Lcom/yandex/metrica/impl/az;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/az;->c()V

    .line 115
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/fy;->e()V

    .line 118
    return-void

    .line 115
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->b:Lcom/yandex/metrica/impl/az;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/az;->c(Ljava/lang/String;)V

    .line 163
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 140
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->c:Lcom/yandex/metrica/impl/ob/ga;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/ga;->b()Ljava/util/List;

    move-result-object v0

    .line 142
    invoke-static {p1}, Lcom/yandex/metrica/impl/bl;->a(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 143
    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->c:Lcom/yandex/metrica/impl/ob/ga;

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/ga;->a(Ljava/util/List;)V

    .line 145
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->b:Lcom/yandex/metrica/impl/az;

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/az;->a(Ljava/util/List;)V

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    invoke-static {p1, v0}, Lcom/yandex/metrica/impl/bl;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 148
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->c:Lcom/yandex/metrica/impl/ob/ga;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/ga;->a(Ljava/util/List;)V

    .line 149
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->b:Lcom/yandex/metrica/impl/az;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/az;->a(Ljava/util/List;)V

    goto :goto_0

    .line 151
    :cond_2
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fy;->b:Lcom/yandex/metrica/impl/az;

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/az;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method public a(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 156
    invoke-static {p1}, Lcom/yandex/metrica/impl/utils/o;->c(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 158
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fy;->b:Lcom/yandex/metrica/impl/az;

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/az;->a(Ljava/util/Map;)V

    .line 159
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->d:Lcom/yandex/metrica/impl/ob/dc;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dc;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 223
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/fx;->b(Landroid/os/Bundle;)Lcom/yandex/metrica/impl/ob/fx;

    move-result-object v0

    .line 2228
    sget-object v1, Lcom/yandex/metrica/impl/ob/fy;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/IIdentifierCallback$Reason;

    .line 2230
    new-instance v2, Ljava/util/WeakHashMap;

    invoke-direct {v2}, Ljava/util/WeakHashMap;-><init>()V

    .line 2231
    new-instance v3, Ljava/util/WeakHashMap;

    invoke-direct {v3}, Ljava/util/WeakHashMap;-><init>()V

    .line 2233
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fy;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 2234
    :try_start_0
    iget-object v4, p0, Lcom/yandex/metrica/impl/ob/fy;->f:Ljava/util/Map;

    invoke-interface {v2, v4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2235
    iget-object v4, p0, Lcom/yandex/metrica/impl/ob/fy;->g:Ljava/util/Map;

    invoke-interface {v3, v4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2237
    iget-object v4, p0, Lcom/yandex/metrica/impl/ob/fy;->f:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    .line 2238
    iget-object v4, p0, Lcom/yandex/metrica/impl/ob/fy;->g:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    .line 2244
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2246
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/yandex/metrica/IIdentifierCallback;

    .line 2247
    invoke-interface {v1, v0}, Lcom/yandex/metrica/IIdentifierCallback;->onRequestError(Lcom/yandex/metrica/IIdentifierCallback$Reason;)V

    goto :goto_0

    .line 2244
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2250
    :cond_0
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/yandex/metrica/IIdentifierCallback;

    .line 2251
    invoke-interface {v1, v0}, Lcom/yandex/metrica/IIdentifierCallback;->onRequestError(Lcom/yandex/metrica/IIdentifierCallback$Reason;)V

    goto :goto_1

    .line 2254
    :cond_1
    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 2255
    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 225
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->c:Lcom/yandex/metrica/impl/ob/ga;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/ga;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->c:Lcom/yandex/metrica/impl/ob/ga;

    sget-object v1, Lcom/yandex/metrica/impl/ob/ga$a;->c:Lcom/yandex/metrica/impl/ob/ga$a;

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/ga;->a(Lcom/yandex/metrica/impl/ob/ga$a;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->c:Lcom/yandex/metrica/impl/ob/ga;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/ga;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2121
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fy;->b:Lcom/yandex/metrica/impl/az;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/az;->c()V

    .line 137
    :cond_1
    return-void
.end method
