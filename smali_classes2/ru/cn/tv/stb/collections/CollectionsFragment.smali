.class public Lru/cn/tv/stb/collections/CollectionsFragment;
.super Landroid/support/v4/app/Fragment;
.source "CollectionsFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/stb/collections/CollectionsFragment$CollectionListener;
    }
.end annotation


# instance fields
.field private collectionListener:Lru/cn/tv/stb/collections/CollectionsFragment$CollectionListener;

.field private collectionObjectMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lru/cn/tv/stb/collections/StbCollectionView;",
            ">;"
        }
    .end annotation
.end field

.field private emptyRubricText:Landroid/widget/TextView;

.field private isLoading:Z

.field private progressBar:Landroid/widget/ProgressBar;

.field private scrollView:Landroid/widget/ScrollView;

.field private scrollViewContainer:Landroid/widget/LinearLayout;

.field private viewModel:Lru/cn/tv/stb/collections/CollectionsViewModel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 45
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->collectionObjectMap:Ljava/util/Map;

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->isLoading:Z

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/stb/collections/CollectionsFragment;)Lru/cn/tv/stb/collections/CollectionsFragment$CollectionListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/collections/CollectionsFragment;

    .prologue
    .line 32
    iget-object v0, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->collectionListener:Lru/cn/tv/stb/collections/CollectionsFragment$CollectionListener;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/tv/stb/collections/CollectionsFragment;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/collections/CollectionsFragment;

    .prologue
    .line 32
    iget-boolean v0, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->isLoading:Z

    return v0
.end method

.method static synthetic access$102(Lru/cn/tv/stb/collections/CollectionsFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/stb/collections/CollectionsFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->isLoading:Z

    return p1
.end method

.method static synthetic access$200(Lru/cn/tv/stb/collections/CollectionsFragment;)Lru/cn/tv/stb/collections/CollectionsViewModel;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/collections/CollectionsFragment;

    .prologue
    .line 32
    iget-object v0, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->viewModel:Lru/cn/tv/stb/collections/CollectionsViewModel;

    return-object v0
.end method

.method private createCollectionView(Lru/cn/tv/stb/collections/CollectionInfo;)Lru/cn/tv/stb/collections/StbCollectionView;
    .locals 9
    .param p1, "collection"    # Lru/cn/tv/stb/collections/CollectionInfo;

    .prologue
    const/4 v5, 0x0

    .line 127
    iget-object v0, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 129
    invoke-virtual {p1}, Lru/cn/tv/stb/collections/CollectionInfo;->getRubric()Lru/cn/api/catalogue/replies/Rubric;

    move-result-object v8

    .line 131
    .local v8, "rubric":Lru/cn/api/catalogue/replies/Rubric;
    new-instance v4, Lru/cn/tv/stb/collections/StbCollectionView;

    invoke-virtual {p0}, Lru/cn/tv/stb/collections/CollectionsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {v4, v0, v1}, Lru/cn/tv/stb/collections/StbCollectionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 132
    .local v4, "collectionView":Lru/cn/tv/stb/collections/StbCollectionView;
    iget-object v0, v8, Lru/cn/api/catalogue/replies/Rubric;->title:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lru/cn/tv/stb/collections/StbCollectionView;->setTitle(Ljava/lang/String;)V

    .line 133
    invoke-virtual {v4}, Lru/cn/tv/stb/collections/StbCollectionView;->show()V

    .line 135
    invoke-virtual {v4}, Lru/cn/tv/stb/collections/StbCollectionView;->getCollectionRecycler()Landroid/support/v7/widget/RecyclerView;

    move-result-object v6

    .line 137
    .local v6, "collectionRecycler":Landroid/support/v7/widget/RecyclerView;
    invoke-virtual {p0}, Lru/cn/tv/stb/collections/CollectionsFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lru/cn/domain/KidsObject;->isKidsMode(Landroid/content/Context;)Z

    move-result v7

    .line 138
    .local v7, "kidsMode":Z
    new-instance v2, Lru/cn/tv/stb/collections/CollectionTelecastAdapter;

    const v0, 0x7f0c007b

    invoke-direct {v2, v0, v7}, Lru/cn/tv/stb/collections/CollectionTelecastAdapter;-><init>(IZ)V

    .line 139
    .local v2, "adapter":Lru/cn/tv/stb/collections/CollectionTelecastAdapter;
    invoke-virtual {v6, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 140
    invoke-virtual {p1}, Lru/cn/tv/stb/collections/CollectionInfo;->getElements()Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {v2, v0}, Lru/cn/tv/stb/collections/CollectionTelecastAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 142
    new-instance v3, Lru/cn/view/LinearLayoutManagerExt;

    invoke-virtual {p0}, Lru/cn/tv/stb/collections/CollectionsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v3, v0, v5, v5}, Lru/cn/view/LinearLayoutManagerExt;-><init>(Landroid/content/Context;IZ)V

    .line 144
    .local v3, "collectionLayout":Landroid/support/v7/widget/LinearLayoutManager;
    invoke-virtual {v6, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 146
    new-instance v0, Lru/cn/tv/stb/collections/CollectionsFragment$1;

    invoke-direct {v0, p0, v2, v8}, Lru/cn/tv/stb/collections/CollectionsFragment$1;-><init>(Lru/cn/tv/stb/collections/CollectionsFragment;Lru/cn/tv/stb/collections/CollectionTelecastAdapter;Lru/cn/api/catalogue/replies/Rubric;)V

    invoke-virtual {v2, v0}, Lru/cn/tv/stb/collections/CollectionTelecastAdapter;->setOnItemClickListener(Lru/cn/view/CursorRecyclerViewAdapter$OnItemClickListener;)V

    .line 155
    new-instance v0, Lru/cn/tv/stb/collections/CollectionsFragment$$Lambda$1;

    invoke-direct {v0, p0, v4}, Lru/cn/tv/stb/collections/CollectionsFragment$$Lambda$1;-><init>(Lru/cn/tv/stb/collections/CollectionsFragment;Lru/cn/tv/stb/collections/StbCollectionView;)V

    invoke-virtual {p1, v0}, Lru/cn/tv/stb/collections/CollectionInfo;->setOnUpdate(Lio/reactivex/functions/Consumer;)V

    .line 157
    new-instance v0, Lru/cn/tv/stb/collections/CollectionsFragment$2;

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lru/cn/tv/stb/collections/CollectionsFragment$2;-><init>(Lru/cn/tv/stb/collections/CollectionsFragment;Lru/cn/tv/stb/collections/CollectionTelecastAdapter;Landroid/support/v7/widget/LinearLayoutManager;Lru/cn/tv/stb/collections/StbCollectionView;Lru/cn/tv/stb/collections/CollectionInfo;)V

    invoke-virtual {v6, v0}, Landroid/support/v7/widget/RecyclerView;->addOnScrollListener(Landroid/support/v7/widget/RecyclerView$OnScrollListener;)V

    .line 181
    return-object v4
.end method

.method private setCollections(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/tv/stb/collections/CollectionInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 105
    .local p1, "collections":Ljava/util/List;, "Ljava/util/List<Lru/cn/tv/stb/collections/CollectionInfo;>;"
    iget-object v3, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->scrollViewContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 106
    iget-object v3, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->collectionObjectMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 108
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 109
    :cond_0
    iget-object v3, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->scrollView:Landroid/widget/ScrollView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 110
    iget-object v3, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->emptyRubricText:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 124
    :cond_1
    return-void

    .line 114
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/tv/stb/collections/CollectionInfo;

    .line 115
    .local v0, "collection":Lru/cn/tv/stb/collections/CollectionInfo;
    invoke-direct {p0, v0}, Lru/cn/tv/stb/collections/CollectionsFragment;->createCollectionView(Lru/cn/tv/stb/collections/CollectionInfo;)Lru/cn/tv/stb/collections/StbCollectionView;

    move-result-object v1

    .line 116
    .local v1, "collectionView":Lru/cn/tv/stb/collections/StbCollectionView;
    iget-object v4, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->scrollViewContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 118
    invoke-virtual {v0}, Lru/cn/tv/stb/collections/CollectionInfo;->getRubric()Lru/cn/api/catalogue/replies/Rubric;

    move-result-object v2

    .line 119
    .local v2, "rubric":Lru/cn/api/catalogue/replies/Rubric;
    iget-object v4, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->collectionObjectMap:Ljava/util/Map;

    iget-wide v6, v2, Lru/cn/api/catalogue/replies/Rubric;->id:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    iget-object v4, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->collectionObjectMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-eq v4, v5, :cond_3

    .line 121
    invoke-virtual {v1}, Lru/cn/tv/stb/collections/StbCollectionView;->showDivider()V

    goto :goto_0
.end method

.method private updateElements(Lru/cn/tv/stb/collections/StbCollectionView;Landroid/database/Cursor;)V
    .locals 7
    .param p1, "collectionView"    # Lru/cn/tv/stb/collections/StbCollectionView;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 185
    const/4 v6, 0x0

    iput-boolean v6, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->isLoading:Z

    .line 187
    invoke-virtual {p1}, Lru/cn/tv/stb/collections/StbCollectionView;->hideProgress()V

    .line 189
    invoke-virtual {p1}, Lru/cn/tv/stb/collections/StbCollectionView;->getCollectionRecycler()Landroid/support/v7/widget/RecyclerView;

    move-result-object v5

    .line 190
    .local v5, "recycler":Landroid/support/v7/widget/RecyclerView;
    invoke-virtual {v5}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    check-cast v0, Lru/cn/tv/stb/collections/CollectionTelecastAdapter;

    .line 191
    .local v0, "collectionAdapter":Lru/cn/tv/stb/collections/CollectionTelecastAdapter;
    invoke-virtual {v5}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v4

    check-cast v4, Landroid/support/v7/widget/LinearLayoutManager;

    .line 193
    .local v4, "layoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    invoke-virtual {v0}, Lru/cn/tv/stb/collections/CollectionTelecastAdapter;->getItemCount()I

    move-result v6

    add-int/lit8 v2, v6, -0x1

    .line 194
    .local v2, "itemCount":I
    invoke-virtual {v4}, Landroid/support/v7/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    move-result v3

    .line 196
    .local v3, "lastVisiblePosition":I
    invoke-virtual {v0, p2}, Lru/cn/tv/stb/collections/CollectionTelecastAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 198
    sub-int v1, v2, v3

    .line 199
    .local v1, "countToEndOfPage":I
    const/4 v6, 0x3

    if-ge v1, v6, :cond_0

    .line 200
    add-int/lit8 v2, v2, 0x3

    .line 203
    :cond_0
    const/4 v6, 0x5

    if-ne v1, v6, :cond_1

    .line 204
    add-int/lit8 v2, v2, -0x1

    .line 207
    :cond_1
    invoke-virtual {v5}, Landroid/support/v7/widget/RecyclerView;->isShown()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 208
    invoke-virtual {v5, v2}, Landroid/support/v7/widget/RecyclerView;->smoothScrollToPosition(I)V

    .line 210
    :cond_2
    return-void
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$CollectionsFragment(Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/stb/collections/CollectionsFragment;->setCollections(Ljava/util/List;)V

    return-void
.end method

.method final synthetic lambda$createCollectionView$0$CollectionsFragment(Lru/cn/tv/stb/collections/StbCollectionView;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "collectionView"    # Lru/cn/tv/stb/collections/StbCollectionView;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 155
    invoke-direct {p0, p1, p2}, Lru/cn/tv/stb/collections/CollectionsFragment;->updateElements(Lru/cn/tv/stb/collections/StbCollectionView;Landroid/database/Cursor;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 53
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 55
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v0

    const-class v1, Lru/cn/tv/stb/collections/CollectionsViewModel;

    invoke-static {p0, v0, v1}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lru/cn/tv/stb/collections/CollectionsViewModel;

    iput-object v0, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->viewModel:Lru/cn/tv/stb/collections/CollectionsViewModel;

    .line 56
    iget-object v0, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->viewModel:Lru/cn/tv/stb/collections/CollectionsViewModel;

    invoke-virtual {v0}, Lru/cn/tv/stb/collections/CollectionsViewModel;->collections()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/stb/collections/CollectionsFragment$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/collections/CollectionsFragment$$Lambda$0;-><init>(Lru/cn/tv/stb/collections/CollectionsFragment;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 57
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 62
    const v0, 0x7f0c0079

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 67
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 69
    const v0, 0x7f09007f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->scrollView:Landroid/widget/ScrollView;

    .line 70
    const v0, 0x7f090080

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->scrollViewContainer:Landroid/widget/LinearLayout;

    .line 71
    const v0, 0x7f0900b3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->emptyRubricText:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f090177

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 73
    iget-object v0, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->progressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 74
    return-void
.end method

.method public requestFocus()Z
    .locals 2

    .prologue
    .line 81
    iget-object v1, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->collectionObjectMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    const/4 v1, 0x0

    .line 89
    :goto_0
    return v1

    .line 85
    :cond_0
    iget-object v1, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->collectionObjectMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->enumeration(Ljava/util/Collection;)Ljava/util/Enumeration;

    move-result-object v1

    .line 86
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/tv/stb/collections/StbCollectionView;

    .line 87
    .local v0, "collectionView":Lru/cn/tv/stb/collections/StbCollectionView;
    invoke-virtual {v0}, Lru/cn/tv/stb/collections/StbCollectionView;->clearFocus()V

    .line 89
    invoke-virtual {v0}, Lru/cn/tv/stb/collections/StbCollectionView;->getCollectionRecycler()Landroid/support/v7/widget/RecyclerView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->requestFocus()Z

    move-result v1

    goto :goto_0
.end method

.method public scrollToStartPosition()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 97
    iget-object v1, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->collectionObjectMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/tv/stb/collections/StbCollectionView;

    .line 98
    .local v0, "view":Lru/cn/tv/stb/collections/StbCollectionView;
    invoke-virtual {v0}, Lru/cn/tv/stb/collections/StbCollectionView;->getCollectionRecycler()Landroid/support/v7/widget/RecyclerView;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->scrollToPosition(I)V

    goto :goto_0

    .line 101
    .end local v0    # "view":Lru/cn/tv/stb/collections/StbCollectionView;
    :cond_0
    iget-object v1, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->scrollView:Landroid/widget/ScrollView;

    invoke-virtual {v1, v3, v3}, Landroid/widget/ScrollView;->scrollTo(II)V

    .line 102
    return-void
.end method

.method public setCollectionListener(Lru/cn/tv/stb/collections/CollectionsFragment$CollectionListener;)V
    .locals 0
    .param p1, "collectionListener"    # Lru/cn/tv/stb/collections/CollectionsFragment$CollectionListener;

    .prologue
    .line 77
    iput-object p1, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->collectionListener:Lru/cn/tv/stb/collections/CollectionsFragment$CollectionListener;

    .line 78
    return-void
.end method

.method public updateIfNeeded()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lru/cn/tv/stb/collections/CollectionsFragment;->viewModel:Lru/cn/tv/stb/collections/CollectionsViewModel;

    invoke-virtual {v0}, Lru/cn/tv/stb/collections/CollectionsViewModel;->load()V

    .line 94
    return-void
.end method
