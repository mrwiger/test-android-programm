.class public final Lru/cn/player/exoplayer/ExoMediaPlayer;
.super Lru/cn/player/AbstractMediaPlayer;
.source "ExoMediaPlayer.java"

# interfaces
.implements Lcom/google/android/exoplayer2/Player$EventListener;
.implements Lcom/google/android/exoplayer2/SimpleExoPlayer$VideoListener;
.implements Lcom/google/android/exoplayer2/text/TextOutput;
.implements Lru/cn/player/Selector$TrackSelector;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/player/exoplayer/ExoMediaPlayer$RecoverHandler;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lru/cn/player/AbstractMediaPlayer;",
        "Lcom/google/android/exoplayer2/Player$EventListener;",
        "Lcom/google/android/exoplayer2/SimpleExoPlayer$VideoListener;",
        "Lcom/google/android/exoplayer2/text/TextOutput;",
        "Lru/cn/player/Selector$TrackSelector",
        "<",
        "Lru/cn/player/exoplayer/ExoTrackInfo;",
        ">;"
    }
.end annotation


# static fields
.field private static final BANDWIDTH_METER:Lcom/google/android/exoplayer2/upstream/DefaultBandwidthMeter;

.field private static final FIXED_FACTORY:Lcom/google/android/exoplayer2/trackselection/TrackSelection$Factory;

.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private contentUri:Landroid/net/Uri;

.field private final detailObserver:Lru/cn/player/exoplayer/DetailObserver;

.field private final exoTrackSelector:Lru/cn/player/exoplayer/ExoTrackSelector;

.field private isStaticWindow:Z

.field private final metadataExtractor:Lru/cn/player/exoplayer/MetadataExtractor;

.field private player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

.field private recoverCount:I

.field private final recoveryHandler:Landroid/os/Handler;

.field private subtitleView:Lcom/google/android/exoplayer2/ui/SubtitleView;

.field private trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

.field private wasReady:Z

.field private window:Lcom/google/android/exoplayer2/Timeline$Window;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const-class v0, Lru/cn/player/exoplayer/ExoMediaPlayer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lru/cn/player/exoplayer/ExoMediaPlayer;->LOG_TAG:Ljava/lang/String;

    .line 77
    new-instance v0, Lcom/google/android/exoplayer2/upstream/DefaultBandwidthMeter;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/upstream/DefaultBandwidthMeter;-><init>()V

    sput-object v0, Lru/cn/player/exoplayer/ExoMediaPlayer;->BANDWIDTH_METER:Lcom/google/android/exoplayer2/upstream/DefaultBandwidthMeter;

    .line 78
    new-instance v0, Lcom/google/android/exoplayer2/trackselection/FixedTrackSelection$Factory;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/trackselection/FixedTrackSelection$Factory;-><init>()V

    sput-object v0, Lru/cn/player/exoplayer/ExoMediaPlayer;->FIXED_FACTORY:Lcom/google/android/exoplayer2/trackselection/TrackSelection$Factory;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x0

    const/4 v7, 0x1

    .line 145
    invoke-direct {p0, p1}, Lru/cn/player/AbstractMediaPlayer;-><init>(Landroid/content/Context;)V

    .line 103
    iput v12, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->recoverCount:I

    .line 146
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xf

    if-gt v2, v3, :cond_0

    .line 147
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Exoplayer is not available on 4.0.x"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 149
    :cond_0
    new-instance v2, Lcom/google/android/exoplayer2/Timeline$Window;

    invoke-direct {v2}, Lcom/google/android/exoplayer2/Timeline$Window;-><init>()V

    iput-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->window:Lcom/google/android/exoplayer2/Timeline$Window;

    .line 150
    new-instance v2, Lru/cn/player/exoplayer/MetadataExtractor;

    invoke-direct {v2}, Lru/cn/player/exoplayer/MetadataExtractor;-><init>()V

    iput-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->metadataExtractor:Lru/cn/player/exoplayer/MetadataExtractor;

    .line 151
    new-instance v2, Lru/cn/player/exoplayer/ExoMediaPlayer$RecoverHandler;

    invoke-direct {v2, p0}, Lru/cn/player/exoplayer/ExoMediaPlayer$RecoverHandler;-><init>(Lru/cn/player/exoplayer/ExoMediaPlayer;)V

    iput-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->recoveryHandler:Landroid/os/Handler;

    .line 153
    new-instance v1, Lcom/google/android/exoplayer2/upstream/DefaultAllocator;

    const/high16 v2, 0x10000

    invoke-direct {v1, v7, v2}, Lcom/google/android/exoplayer2/upstream/DefaultAllocator;-><init>(ZI)V

    .line 154
    .local v1, "allocator":Lcom/google/android/exoplayer2/upstream/DefaultAllocator;
    new-instance v0, Lcom/google/android/exoplayer2/DefaultLoadControl;

    const/16 v2, 0x3a98

    const v3, 0xafc8

    .line 157
    invoke-static {p1}, Lru/cn/player/exoplayer/ExoPlayerUtils;->initialBufferDuration(Landroid/content/Context;)I

    move-result v4

    .line 158
    invoke-static {p1}, Lru/cn/player/exoplayer/ExoPlayerUtils;->rebufferDuration(Landroid/content/Context;)I

    move-result v5

    const/4 v6, -0x1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/exoplayer2/DefaultLoadControl;-><init>(Lcom/google/android/exoplayer2/upstream/DefaultAllocator;IIIIIZ)V

    .line 162
    .local v0, "loadControl":Lcom/google/android/exoplayer2/LoadControl;
    new-instance v11, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection$Factory;

    sget-object v2, Lru/cn/player/exoplayer/ExoMediaPlayer;->BANDWIDTH_METER:Lcom/google/android/exoplayer2/upstream/DefaultBandwidthMeter;

    invoke-direct {v11, v2}, Lcom/google/android/exoplayer2/trackselection/AdaptiveTrackSelection$Factory;-><init>(Lcom/google/android/exoplayer2/upstream/BandwidthMeter;)V

    .line 163
    .local v11, "videoTrackSelectionFactory":Lcom/google/android/exoplayer2/trackselection/TrackSelection$Factory;
    new-instance v2, Lru/cn/player/exoplayer/SpleenyTrackSelector;

    invoke-direct {v2, v11}, Lru/cn/player/exoplayer/SpleenyTrackSelector;-><init>(Lcom/google/android/exoplayer2/trackselection/TrackSelection$Factory;)V

    iput-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    .line 166
    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    invoke-virtual {v2}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->getParameters()Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector$Parameters;

    move-result-object v2

    .line 167
    invoke-virtual {v2}, Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector$Parameters;->buildUpon()Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector$ParametersBuilder;

    move-result-object v2

    const-string v3, "ru"

    .line 168
    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector$ParametersBuilder;->setPreferredAudioLanguage(Ljava/lang/String;)Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector$ParametersBuilder;

    move-result-object v2

    .line 169
    invoke-virtual {v2}, Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector$ParametersBuilder;->build()Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector$Parameters;

    move-result-object v8

    .line 171
    .local v8, "defaultLangParams":Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector$Parameters;
    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    invoke-virtual {v2, v8}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->setParameters(Lcom/google/android/exoplayer2/trackselection/DefaultTrackSelector$Parameters;)V

    .line 173
    const/4 v9, 0x1

    .line 174
    .local v9, "extensionMode":I
    const-string v2, "soft_decoders"

    invoke-static {v2}, Lru/cn/api/experiments/Experiments;->eligibleForExperiment(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 175
    const/4 v9, 0x2

    .line 179
    :cond_1
    const-string v2, "default_input_buffers"

    invoke-static {v2}, Lru/cn/api/experiments/Experiments;->eligibleForExperiment(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 180
    new-instance v10, Lru/cn/player/exoplayer/DefaultInputBuffersRendererFactory;

    invoke-direct {v10, p1, v13, v9}, Lru/cn/player/exoplayer/DefaultInputBuffersRendererFactory;-><init>(Landroid/content/Context;Lcom/google/android/exoplayer2/drm/DrmSessionManager;I)V

    .line 185
    .local v10, "renderersFactory":Lcom/google/android/exoplayer2/RenderersFactory;
    :goto_0
    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    invoke-static {v10, v2, v0}, Lcom/google/android/exoplayer2/ExoPlayerFactory;->newSimpleInstance(Lcom/google/android/exoplayer2/RenderersFactory;Lcom/google/android/exoplayer2/trackselection/TrackSelector;Lcom/google/android/exoplayer2/LoadControl;)Lcom/google/android/exoplayer2/SimpleExoPlayer;

    move-result-object v2

    iput-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    .line 186
    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v2, p0}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->addListener(Lcom/google/android/exoplayer2/Player$EventListener;)V

    .line 187
    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v2, p0}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->addVideoListener(Lcom/google/android/exoplayer2/video/VideoListener;)V

    .line 188
    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v2, p0}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->addTextOutput(Lcom/google/android/exoplayer2/text/TextOutput;)V

    .line 190
    new-instance v2, Lru/cn/player/exoplayer/DetailObserver;

    iget-object v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-direct {v2, v3}, Lru/cn/player/exoplayer/DetailObserver;-><init>(Lcom/google/android/exoplayer2/SimpleExoPlayer;)V

    iput-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->detailObserver:Lru/cn/player/exoplayer/DetailObserver;

    .line 192
    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v2, :cond_2

    .line 193
    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    iget-object v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->setVideoSurfaceHolder(Landroid/view/SurfaceHolder;)V

    .line 194
    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v2, v7}, Landroid/view/SurfaceHolder;->setKeepScreenOn(Z)V

    .line 197
    :cond_2
    new-instance v2, Lcom/google/android/exoplayer2/ui/SubtitleView;

    invoke-direct {v2, p1}, Lcom/google/android/exoplayer2/ui/SubtitleView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->subtitleView:Lcom/google/android/exoplayer2/ui/SubtitleView;

    .line 199
    new-instance v2, Lru/cn/player/exoplayer/ExoTrackSelector;

    iget-object v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    iget-object v4, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    iget-object v5, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->accessibility:Lru/cn/player/AbstractMediaPlayer$Accessibility;

    iget-boolean v5, v5, Lru/cn/player/AbstractMediaPlayer$Accessibility;->captioningEnable:Z

    if-nez v5, :cond_4

    :goto_1
    invoke-direct {v2, v3, v4, v7, p0}, Lru/cn/player/exoplayer/ExoTrackSelector;-><init>(Lcom/google/android/exoplayer2/SimpleExoPlayer;Lru/cn/player/exoplayer/SpleenyTrackSelector;ZLru/cn/player/Selector$TrackSelector;)V

    iput-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->exoTrackSelector:Lru/cn/player/exoplayer/ExoTrackSelector;

    .line 200
    return-void

    .line 182
    .end local v10    # "renderersFactory":Lcom/google/android/exoplayer2/RenderersFactory;
    :cond_3
    new-instance v10, Lcom/google/android/exoplayer2/DefaultRenderersFactory;

    invoke-direct {v10, p1, v13, v9}, Lcom/google/android/exoplayer2/DefaultRenderersFactory;-><init>(Landroid/content/Context;Lcom/google/android/exoplayer2/drm/DrmSessionManager;I)V

    .restart local v10    # "renderersFactory":Lcom/google/android/exoplayer2/RenderersFactory;
    goto :goto_0

    :cond_4
    move v7, v12

    .line 199
    goto :goto_1
.end method

.method static synthetic access$000(Lru/cn/player/exoplayer/ExoMediaPlayer;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/exoplayer/ExoMediaPlayer;

    .prologue
    .line 67
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->contentUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/player/exoplayer/ExoMediaPlayer;)Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/exoplayer/ExoMediaPlayer;

    .prologue
    .line 67
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/player/exoplayer/ExoMediaPlayer;)Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/exoplayer/ExoMediaPlayer;

    .prologue
    .line 67
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    return-object v0
.end method

.method private buildMediaSource()Lcom/google/android/exoplayer2/source/MediaSource;
    .locals 6

    .prologue
    .line 248
    sget-object v3, Lru/cn/player/exoplayer/ExoMediaPlayer;->LOG_TAG:Ljava/lang/String;

    const-string v4, "buildMediaSource()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    iget-object v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->context:Landroid/content/Context;

    const-string v4, "2.7.3"

    invoke-static {v3, v4}, Lru/cn/player/exoplayer/ExoPlayerUtils;->userAgent(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 252
    .local v2, "userAgent":Ljava/lang/String;
    iget-object v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->contentUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    const-string v4, "http"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->contentUri:Landroid/net/Uri;

    .line 253
    invoke-virtual {v3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 254
    new-instance v0, Lcom/google/android/exoplayer2/ext/okhttp/OkHttpDataSourceFactory;

    invoke-static {}, Lru/cn/player/exoplayer/ExoPlayerUtils;->defaultCallFactory()Lokhttp3/OkHttpClient;

    move-result-object v3

    sget-object v4, Lru/cn/player/exoplayer/ExoMediaPlayer;->BANDWIDTH_METER:Lcom/google/android/exoplayer2/upstream/DefaultBandwidthMeter;

    invoke-direct {v0, v3, v2, v4}, Lcom/google/android/exoplayer2/ext/okhttp/OkHttpDataSourceFactory;-><init>(Lokhttp3/Call$Factory;Ljava/lang/String;Lcom/google/android/exoplayer2/upstream/TransferListener;)V

    .line 257
    .local v0, "dataFactory":Lcom/google/android/exoplayer2/upstream/DataSource$Factory;
    iget-object v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->contentUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/exoplayer2/util/Util;->inferContentType(Ljava/lang/String;)I

    move-result v1

    .line 258
    .local v1, "type":I
    packed-switch v1, :pswitch_data_0

    .line 273
    .end local v0    # "dataFactory":Lcom/google/android/exoplayer2/upstream/DataSource$Factory;
    .end local v1    # "type":I
    :cond_0
    new-instance v0, Lru/cn/player/exoplayer/DataSourceFactory;

    iget-object v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->context:Landroid/content/Context;

    iget-object v4, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->contentUri:Landroid/net/Uri;

    sget-object v5, Lru/cn/player/exoplayer/ExoMediaPlayer;->BANDWIDTH_METER:Lcom/google/android/exoplayer2/upstream/DefaultBandwidthMeter;

    invoke-direct {v0, v3, v4, v2, v5}, Lru/cn/player/exoplayer/DataSourceFactory;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/exoplayer2/upstream/TransferListener;)V

    .line 274
    .restart local v0    # "dataFactory":Lcom/google/android/exoplayer2/upstream/DataSource$Factory;
    new-instance v3, Lcom/google/android/exoplayer2/source/ExtractorMediaSource$Factory;

    invoke-direct {v3, v0}, Lcom/google/android/exoplayer2/source/ExtractorMediaSource$Factory;-><init>(Lcom/google/android/exoplayer2/upstream/DataSource$Factory;)V

    iget-object v4, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->contentUri:Landroid/net/Uri;

    .line 275
    invoke-virtual {v3, v4}, Lcom/google/android/exoplayer2/source/ExtractorMediaSource$Factory;->createMediaSource(Landroid/net/Uri;)Lcom/google/android/exoplayer2/source/ExtractorMediaSource;

    move-result-object v3

    .line 274
    :goto_0
    return-object v3

    .line 260
    .restart local v1    # "type":I
    :pswitch_0
    new-instance v3, Lcom/google/android/exoplayer2/source/hls/HlsMediaSource$Factory;

    invoke-direct {v3, v0}, Lcom/google/android/exoplayer2/source/hls/HlsMediaSource$Factory;-><init>(Lcom/google/android/exoplayer2/upstream/DataSource$Factory;)V

    iget-object v4, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->contentUri:Landroid/net/Uri;

    .line 261
    invoke-virtual {v3, v4}, Lcom/google/android/exoplayer2/source/hls/HlsMediaSource$Factory;->createMediaSource(Landroid/net/Uri;)Lcom/google/android/exoplayer2/source/hls/HlsMediaSource;

    move-result-object v3

    goto :goto_0

    .line 264
    :pswitch_1
    new-instance v3, Lcom/google/android/exoplayer2/source/smoothstreaming/SsMediaSource$Factory;

    new-instance v4, Lcom/google/android/exoplayer2/source/smoothstreaming/DefaultSsChunkSource$Factory;

    invoke-direct {v4, v0}, Lcom/google/android/exoplayer2/source/smoothstreaming/DefaultSsChunkSource$Factory;-><init>(Lcom/google/android/exoplayer2/upstream/DataSource$Factory;)V

    invoke-direct {v3, v4, v0}, Lcom/google/android/exoplayer2/source/smoothstreaming/SsMediaSource$Factory;-><init>(Lcom/google/android/exoplayer2/source/smoothstreaming/SsChunkSource$Factory;Lcom/google/android/exoplayer2/upstream/DataSource$Factory;)V

    iget-object v4, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->contentUri:Landroid/net/Uri;

    .line 265
    invoke-virtual {v3, v4}, Lcom/google/android/exoplayer2/source/smoothstreaming/SsMediaSource$Factory;->createMediaSource(Landroid/net/Uri;)Lcom/google/android/exoplayer2/source/smoothstreaming/SsMediaSource;

    move-result-object v3

    goto :goto_0

    .line 268
    :pswitch_2
    new-instance v3, Lcom/google/android/exoplayer2/source/dash/DashMediaSource$Factory;

    new-instance v4, Lcom/google/android/exoplayer2/source/dash/DefaultDashChunkSource$Factory;

    invoke-direct {v4, v0}, Lcom/google/android/exoplayer2/source/dash/DefaultDashChunkSource$Factory;-><init>(Lcom/google/android/exoplayer2/upstream/DataSource$Factory;)V

    invoke-direct {v3, v4, v0}, Lcom/google/android/exoplayer2/source/dash/DashMediaSource$Factory;-><init>(Lcom/google/android/exoplayer2/source/dash/DashChunkSource$Factory;Lcom/google/android/exoplayer2/upstream/DataSource$Factory;)V

    iget-object v4, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->contentUri:Landroid/net/Uri;

    .line 269
    invoke-virtual {v3, v4}, Lcom/google/android/exoplayer2/source/dash/DashMediaSource$Factory;->createMediaSource(Landroid/net/Uri;)Lcom/google/android/exoplayer2/source/dash/DashMediaSource;

    move-result-object v3

    goto :goto_0

    .line 258
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static final synthetic lambda$onPlayerError$0$ExoMediaPlayer(Ljava/util/Map$Entry;)Ljava/lang/String;
    .locals 2
    .param p0, "entry"    # Ljava/util/Map$Entry;

    .prologue
    .line 561
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private overrideSelectedUserQualityIfNeeded()V
    .locals 15

    .prologue
    .line 501
    iget-object v12, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->context:Landroid/content/Context;

    invoke-static {v12}, Lru/cn/player/exoplayer/ExoPlayerUtils;->qualityRestriction(Landroid/content/Context;)I

    move-result v11

    .line 502
    .local v11, "userRestriction":I
    if-nez v11, :cond_0

    .line 546
    :goto_0
    return-void

    .line 506
    :cond_0
    iget-object v12, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    iget-object v13, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    const/4 v14, 0x2

    invoke-virtual {v12, v13, v14}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->getRendererIndex(Lcom/google/android/exoplayer2/SimpleExoPlayer;I)I

    move-result v4

    .line 507
    .local v4, "rendererIndex":I
    iget-object v12, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    iget-object v13, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    const/4 v14, 0x2

    invoke-virtual {v12, v13, v14}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->getGroupArrayByTrackType(Lcom/google/android/exoplayer2/SimpleExoPlayer;I)Lcom/google/android/exoplayer2/source/TrackGroupArray;

    move-result-object v10

    .line 509
    .local v10, "typeTrackGroups":Lcom/google/android/exoplayer2/source/TrackGroupArray;
    if-nez v10, :cond_1

    .line 510
    new-instance v12, Ljava/lang/IllegalStateException;

    const-string v13, "typeTrackGroups == null"

    invoke-direct {v12, v13}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 513
    :cond_1
    const/4 v5, 0x0

    .line 514
    .local v5, "selectedFormat":Lcom/google/android/exoplayer2/Format;
    const/4 v7, 0x0

    .line 515
    .local v7, "selectedTrackIndex":I
    const/4 v6, -0x1

    .line 516
    .local v6, "selectedGroupIndex":I
    const/4 v3, 0x0

    .line 517
    .local v3, "height":I
    const/4 v12, 0x1

    if-ne v11, v12, :cond_6

    .line 518
    const v3, 0x7fffffff

    .line 523
    :cond_2
    :goto_1
    const/4 v2, 0x0

    .local v2, "groupIndex":I
    :goto_2
    iget v12, v10, Lcom/google/android/exoplayer2/source/TrackGroupArray;->length:I

    if-ge v2, v12, :cond_8

    .line 524
    invoke-virtual {v10, v2}, Lcom/google/android/exoplayer2/source/TrackGroupArray;->get(I)Lcom/google/android/exoplayer2/source/TrackGroup;

    move-result-object v1

    .line 526
    .local v1, "group":Lcom/google/android/exoplayer2/source/TrackGroup;
    const/4 v8, 0x0

    .local v8, "trackIndex":I
    :goto_3
    iget v12, v1, Lcom/google/android/exoplayer2/source/TrackGroup;->length:I

    if-ge v8, v12, :cond_7

    .line 527
    invoke-virtual {v1, v8}, Lcom/google/android/exoplayer2/source/TrackGroup;->getFormat(I)Lcom/google/android/exoplayer2/Format;

    move-result-object v0

    .line 528
    .local v0, "format":Lcom/google/android/exoplayer2/Format;
    const/4 v12, 0x1

    if-ne v11, v12, :cond_3

    iget v12, v0, Lcom/google/android/exoplayer2/Format;->height:I

    if-lt v12, v3, :cond_4

    :cond_3
    const/4 v12, 0x2

    if-ne v11, v12, :cond_5

    iget v12, v0, Lcom/google/android/exoplayer2/Format;->height:I

    if-le v12, v3, :cond_5

    .line 530
    :cond_4
    iget v3, v0, Lcom/google/android/exoplayer2/Format;->height:I

    .line 531
    move-object v5, v0

    .line 532
    move v7, v8

    .line 533
    move v6, v2

    .line 526
    :cond_5
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 519
    .end local v0    # "format":Lcom/google/android/exoplayer2/Format;
    .end local v1    # "group":Lcom/google/android/exoplayer2/source/TrackGroup;
    .end local v2    # "groupIndex":I
    .end local v8    # "trackIndex":I
    :cond_6
    const/4 v12, 0x2

    if-ne v11, v12, :cond_2

    .line 520
    const/high16 v3, -0x80000000

    goto :goto_1

    .line 523
    .restart local v1    # "group":Lcom/google/android/exoplayer2/source/TrackGroup;
    .restart local v2    # "groupIndex":I
    .restart local v8    # "trackIndex":I
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 539
    .end local v1    # "group":Lcom/google/android/exoplayer2/source/TrackGroup;
    .end local v8    # "trackIndex":I
    :cond_8
    invoke-static {v5, v4, v6, v7}, Lru/cn/player/exoplayer/ExoTrackInfo;->createTrackInfo(Lcom/google/android/exoplayer2/Format;III)Lru/cn/player/exoplayer/ExoTrackInfo;

    move-result-object v9

    .line 545
    .local v9, "trackInfo":Lru/cn/player/exoplayer/ExoTrackInfo;
    invoke-direct {p0, v9}, Lru/cn/player/exoplayer/ExoMediaPlayer;->setTrack(Lru/cn/player/TrackInfo;)V

    goto :goto_0
.end method

.method private setKeepScreenOn(Z)V
    .locals 1
    .param p1, "keepScreenOn"    # Z

    .prologue
    .line 384
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-nez v0, :cond_0

    .line 388
    :goto_0
    return-void

    .line 387
    :cond_0
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p1}, Landroid/view/SurfaceHolder;->setKeepScreenOn(Z)V

    goto :goto_0
.end method

.method private setTrack(Lru/cn/player/TrackInfo;)V
    .locals 11
    .param p1, "info"    # Lru/cn/player/TrackInfo;

    .prologue
    const/4 v8, 0x1

    const/4 v10, 0x0

    .line 430
    move-object v2, p1

    check-cast v2, Lru/cn/player/exoplayer/ExoTrackInfo;

    .line 431
    .local v2, "trackInfo":Lru/cn/player/exoplayer/ExoTrackInfo;
    iget-object v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    invoke-virtual {v3}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->getCurrentMappedTrackInfo()Lcom/google/android/exoplayer2/trackselection/MappingTrackSelector$MappedTrackInfo;

    move-result-object v1

    .line 432
    .local v1, "mappedInfo":Lcom/google/android/exoplayer2/trackselection/MappingTrackSelector$MappedTrackInfo;
    if-nez v1, :cond_0

    .line 454
    :goto_0
    return-void

    .line 435
    :cond_0
    iget v3, v2, Lru/cn/player/exoplayer/ExoTrackInfo;->trackIndex:I

    if-gez v3, :cond_1

    .line 436
    iget-object v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    iget v4, v2, Lru/cn/player/exoplayer/ExoTrackInfo;->rendererIndex:I

    invoke-virtual {v3, v4, v8}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->setRendererDisabled(IZ)V

    goto :goto_0

    .line 438
    :cond_1
    iget-object v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    iget v4, v2, Lru/cn/player/exoplayer/ExoTrackInfo;->rendererIndex:I

    invoke-virtual {v3, v4}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->getRendererDisabled(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 439
    iget-object v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    iget v4, v2, Lru/cn/player/exoplayer/ExoTrackInfo;->rendererIndex:I

    invoke-virtual {v3, v4, v10}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->setRendererDisabled(IZ)V

    .line 442
    :cond_2
    iget v3, v2, Lru/cn/player/exoplayer/ExoTrackInfo;->rendererIndex:I

    invoke-virtual {v1, v3}, Lcom/google/android/exoplayer2/trackselection/MappingTrackSelector$MappedTrackInfo;->getTrackGroups(I)Lcom/google/android/exoplayer2/source/TrackGroupArray;

    move-result-object v0

    .line 443
    .local v0, "groups":Lcom/google/android/exoplayer2/source/TrackGroupArray;
    if-nez v0, :cond_3

    .line 444
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "groups == null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 447
    :cond_3
    iget-object v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    iget v4, v2, Lru/cn/player/exoplayer/ExoTrackInfo;->rendererIndex:I

    new-instance v5, Lcom/google/android/exoplayer2/trackselection/MappingTrackSelector$SelectionOverride;

    sget-object v6, Lru/cn/player/exoplayer/ExoMediaPlayer;->FIXED_FACTORY:Lcom/google/android/exoplayer2/trackselection/TrackSelection$Factory;

    iget v7, v2, Lru/cn/player/exoplayer/ExoTrackInfo;->groupIndex:I

    new-array v8, v8, [I

    iget v9, v2, Lru/cn/player/exoplayer/ExoTrackInfo;->trackIndex:I

    aput v9, v8, v10

    invoke-direct {v5, v6, v7, v8}, Lcom/google/android/exoplayer2/trackselection/MappingTrackSelector$SelectionOverride;-><init>(Lcom/google/android/exoplayer2/trackselection/TrackSelection$Factory;I[I)V

    invoke-virtual {v3, v4, v0, v5}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->setSelectionOverride(ILcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/MappingTrackSelector$SelectionOverride;)V

    goto :goto_0
.end method


# virtual methods
.method public destroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 314
    invoke-virtual {p0}, Lru/cn/player/exoplayer/ExoMediaPlayer;->stop()V

    .line 315
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    if-eqz v0, :cond_0

    .line 316
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->release()V

    .line 317
    iput-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    .line 318
    iput-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->contentUri:Landroid/net/Uri;

    .line 319
    iput v1, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->recoverCount:I

    .line 320
    iput-boolean v1, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->wasReady:Z

    .line 322
    :cond_0
    return-void
.end method

.method public getAudioTrackProvider()Lru/cn/player/AudioTrackSelector;
    .locals 2

    .prologue
    .line 411
    new-instance v0, Lru/cn/player/AudioTrackSelector;

    iget-object v1, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->exoTrackSelector:Lru/cn/player/exoplayer/ExoTrackSelector;

    invoke-direct {v0, v1}, Lru/cn/player/AudioTrackSelector;-><init>(Lru/cn/player/Selector;)V

    return-object v0
.end method

.method public getBufferPosition()J
    .locals 4

    .prologue
    .line 350
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    if-eqz v0, :cond_1

    .line 351
    invoke-virtual {p0}, Lru/cn/player/exoplayer/ExoMediaPlayer;->getDuration()I

    move-result v0

    if-lez v0, :cond_0

    .line 352
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->getBufferedPosition()J

    move-result-wide v0

    .line 357
    :goto_0
    return-wide v0

    .line 354
    :cond_0
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->getBufferedPosition()J

    move-result-wide v0

    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->getCurrentPosition()J

    move-result-wide v2

    sub-long/2addr v0, v2

    goto :goto_0

    .line 357
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentPosition()I
    .locals 2

    .prologue
    .line 341
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->getCurrentPosition()J

    move-result-wide v0

    long-to-int v0, v0

    .line 345
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDateTime()J
    .locals 4

    .prologue
    .line 362
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->window:Lcom/google/android/exoplayer2/Timeline$Window;

    if-eqz v0, :cond_0

    .line 363
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->window:Lcom/google/android/exoplayer2/Timeline$Window;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/Timeline$Window;->windowStartTimeMs:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->window:Lcom/google/android/exoplayer2/Timeline$Window;

    iget-wide v0, v0, Lcom/google/android/exoplayer2/Timeline$Window;->windowStartTimeMs:J

    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->getCurrentPosition()J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 368
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getDuration()I
    .locals 6

    .prologue
    const/4 v2, -0x1

    .line 326
    iget-object v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    if-eqz v3, :cond_0

    .line 327
    iget-boolean v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->isStaticWindow:Z

    if-nez v3, :cond_1

    .line 336
    :cond_0
    :goto_0
    return v2

    .line 330
    :cond_1
    iget-object v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->getDuration()J

    move-result-wide v0

    .line 331
    .local v0, "duration":J
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-lez v3, :cond_0

    .line 332
    long-to-int v2, v0

    goto :goto_0
.end method

.method public getSubtitleTracksProvider()Lru/cn/player/SubtitleTrackSelector;
    .locals 2

    .prologue
    .line 416
    new-instance v0, Lru/cn/player/SubtitleTrackSelector;

    iget-object v1, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->exoTrackSelector:Lru/cn/player/exoplayer/ExoTrackSelector;

    invoke-direct {v0, v1}, Lru/cn/player/SubtitleTrackSelector;-><init>(Lru/cn/player/Selector;)V

    return-object v0
.end method

.method public getSubtitleView()Landroid/view/View;
    .locals 1

    .prologue
    .line 599
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->subtitleView:Lcom/google/android/exoplayer2/ui/SubtitleView;

    return-object v0
.end method

.method public getVideoTracksProvider()Lru/cn/player/VideoTrackSelector;
    .locals 2

    .prologue
    .line 421
    new-instance v0, Lru/cn/player/VideoTrackSelector;

    iget-object v1, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->exoTrackSelector:Lru/cn/player/exoplayer/ExoTrackSelector;

    invoke-direct {v0, v1}, Lru/cn/player/VideoTrackSelector;-><init>(Lru/cn/player/Selector;)V

    return-object v0
.end method

.method public onCues(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/exoplayer2/text/Cue;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 204
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/exoplayer2/text/Cue;>;"
    sget-object v0, Lru/cn/player/exoplayer/ExoMediaPlayer;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onCues()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->subtitleView:Lcom/google/android/exoplayer2/ui/SubtitleView;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/ui/SubtitleView;->setCues(Ljava/util/List;)V

    .line 206
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->subtitleView:Lcom/google/android/exoplayer2/ui/SubtitleView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/ui/SubtitleView;->setVisibility(I)V

    .line 207
    return-void
.end method

.method public onLoadingChanged(Z)V
    .locals 0
    .param p1, "isLoading"    # Z

    .prologue
    .line 625
    return-void
.end method

.method public onPlaybackParametersChanged(Lcom/google/android/exoplayer2/PlaybackParameters;)V
    .locals 0
    .param p1, "playbackParameters"    # Lcom/google/android/exoplayer2/PlaybackParameters;

    .prologue
    .line 606
    return-void
.end method

.method public onPlayerError(Lcom/google/android/exoplayer2/ExoPlaybackException;)V
    .locals 8
    .param p1, "playbackError"    # Lcom/google/android/exoplayer2/ExoPlaybackException;

    .prologue
    .line 550
    sget-object v3, Lru/cn/player/exoplayer/ExoMediaPlayer;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ExoPlaybackException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lru/cn/player/exoplayer/ExoMediaPlayer;->setKeepScreenOn(Z)V

    .line 554
    new-instance v0, Lru/cn/player/exoplayer/ErrorClassifier;

    invoke-direct {v0, p1}, Lru/cn/player/exoplayer/ErrorClassifier;-><init>(Lcom/google/android/exoplayer2/ExoPlaybackException;)V

    .line 555
    .local v0, "classifier":Lru/cn/player/exoplayer/ErrorClassifier;
    iget-object v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->contentUri:Landroid/net/Uri;

    iget-object v4, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->detailObserver:Lru/cn/player/exoplayer/DetailObserver;

    invoke-virtual {v4}, Lru/cn/player/exoplayer/DetailObserver;->getCodecs()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lru/cn/player/exoplayer/ErrorClassifier;->classify(Landroid/net/Uri;Ljava/lang/String;)V

    .line 557
    invoke-virtual {v0}, Lru/cn/player/exoplayer/ErrorClassifier;->errorCode()I

    move-result v1

    .line 560
    .local v1, "errorCode":I
    invoke-virtual {v0}, Lru/cn/player/exoplayer/ErrorClassifier;->attributes()Ljava/util/Map;

    move-result-object v3

    invoke-static {v3}, Lcom/annimon/stream/Stream;->of(Ljava/util/Map;)Lcom/annimon/stream/Stream;

    move-result-object v3

    sget-object v4, Lru/cn/player/exoplayer/ExoMediaPlayer$$Lambda$0;->$instance:Lcom/annimon/stream/function/Function;

    .line 561
    invoke-virtual {v3, v4}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v3

    const-string v4, ","

    .line 562
    invoke-static {v4}, Lcom/annimon/stream/Collectors;->joining(Ljava/lang/CharSequence;)Lcom/annimon/stream/Collector;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/annimon/stream/Stream;->collect(Lcom/annimon/stream/Collector;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 564
    .local v2, "message":Ljava/lang/String;
    sget-object v3, Lru/cn/domain/statistics/inetra/ErrorCode;->UNKNOWN_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v4, "Log_ExoPlayer2Error"

    invoke-static {v3, v4, v1, v2}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    .line 566
    iget-boolean v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->wasReady:Z

    if-eqz v3, :cond_0

    iget v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->recoverCount:I

    const/4 v4, 0x5

    if-ge v3, v4, :cond_0

    invoke-virtual {v0}, Lru/cn/player/exoplayer/ErrorClassifier;->recoverable()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 567
    sget-object v3, Lru/cn/player/exoplayer/ExoMediaPlayer;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Attempt to recover from error "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    iget v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->recoverCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->recoverCount:I

    .line 570
    iget-object v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->recoveryHandler:Landroid/os/Handler;

    const/16 v4, 0x14d

    iget v5, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->recoverCount:I

    mul-int/lit8 v5, v5, 0x64

    int-to-long v6, v5

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 581
    :goto_0
    return-void

    .line 573
    :cond_0
    iget-object v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    if-eqz v3, :cond_1

    .line 574
    iget-object v3, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    invoke-interface {v3, v1}, Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;->onError(I)V

    .line 577
    :cond_1
    invoke-virtual {p0}, Lru/cn/player/exoplayer/ExoMediaPlayer;->stop()V

    .line 579
    invoke-static {p1}, Lru/cn/utils/Logger;->logException(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onPlayerStateChanged(ZI)V
    .locals 3
    .param p1, "playWhenReady"    # Z
    .param p2, "playbackState"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 458
    packed-switch p2, :pswitch_data_0

    .line 498
    :goto_0
    return-void

    .line 461
    :pswitch_0
    iput-boolean v1, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->wasReady:Z

    goto :goto_0

    .line 465
    :pswitch_1
    invoke-virtual {p0, v2}, Lru/cn/player/exoplayer/ExoMediaPlayer;->notifyBuffering(Z)V

    goto :goto_0

    .line 469
    :pswitch_2
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    if-eqz v0, :cond_0

    .line 470
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    invoke-interface {v0}, Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;->onComplete()V

    .line 473
    :cond_0
    invoke-direct {p0, v1}, Lru/cn/player/exoplayer/ExoMediaPlayer;->setKeepScreenOn(Z)V

    .line 474
    sget-object v0, Lru/cn/player/AbstractMediaPlayer$PlayerState;->STOPPED:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lru/cn/player/exoplayer/ExoMediaPlayer;->setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    goto :goto_0

    .line 480
    :pswitch_3
    iget-boolean v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->wasReady:Z

    if-nez v0, :cond_1

    .line 481
    invoke-direct {p0}, Lru/cn/player/exoplayer/ExoMediaPlayer;->overrideSelectedUserQualityIfNeeded()V

    .line 483
    :cond_1
    iput-boolean v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->wasReady:Z

    .line 484
    iput v1, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->recoverCount:I

    .line 485
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->playerTimeoutHandler:Lru/cn/player/TimeoutHandler;

    invoke-virtual {v0}, Lru/cn/player/TimeoutHandler;->reset()V

    .line 487
    invoke-virtual {p0, v1}, Lru/cn/player/exoplayer/ExoMediaPlayer;->notifyBuffering(Z)V

    .line 489
    invoke-virtual {p0}, Lru/cn/player/exoplayer/ExoMediaPlayer;->getState()Lru/cn/player/AbstractMediaPlayer$PlayerState;

    move-result-object v0

    sget-object v1, Lru/cn/player/AbstractMediaPlayer$PlayerState;->LOADING:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_2

    .line 490
    sget-object v0, Lru/cn/player/AbstractMediaPlayer$PlayerState;->LOADED:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lru/cn/player/exoplayer/ExoMediaPlayer;->setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    .line 493
    :cond_2
    invoke-direct {p0, p1}, Lru/cn/player/exoplayer/ExoMediaPlayer;->setKeepScreenOn(Z)V

    .line 495
    if-eqz p1, :cond_3

    sget-object v0, Lru/cn/player/AbstractMediaPlayer$PlayerState;->PLAYING:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    :goto_1
    invoke-virtual {p0, v0}, Lru/cn/player/exoplayer/ExoMediaPlayer;->setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    goto :goto_0

    :cond_3
    sget-object v0, Lru/cn/player/AbstractMediaPlayer$PlayerState;->PAUSED:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    goto :goto_1

    .line 458
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public onPositionDiscontinuity(I)V
    .locals 0
    .param p1, "i"    # I

    .prologue
    .line 586
    return-void
.end method

.method public onRenderedFirstFrame()V
    .locals 0

    .prologue
    .line 603
    return-void
.end method

.method public onRepeatModeChanged(I)V
    .locals 0
    .param p1, "i"    # I

    .prologue
    .line 628
    return-void
.end method

.method public onSeekProcessed()V
    .locals 0

    .prologue
    .line 610
    return-void
.end method

.method public onShuffleModeEnabledChanged(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 632
    return-void
.end method

.method public onTimelineChanged(Lcom/google/android/exoplayer2/Timeline;Ljava/lang/Object;I)V
    .locals 3
    .param p1, "timeline"    # Lcom/google/android/exoplayer2/Timeline;
    .param p2, "manifest"    # Ljava/lang/Object;
    .param p3, "i"    # I

    .prologue
    .line 614
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/Timeline;->getWindowCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 615
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/Timeline;->getWindowCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->window:Lcom/google/android/exoplayer2/Timeline$Window;

    invoke-virtual {p1, v1, v2}, Lcom/google/android/exoplayer2/Timeline;->getWindow(ILcom/google/android/exoplayer2/Timeline$Window;)Lcom/google/android/exoplayer2/Timeline$Window;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/exoplayer2/Timeline$Window;->isDynamic:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->isStaticWindow:Z

    .line 617
    iget-object v1, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->metadataExtractor:Lru/cn/player/exoplayer/MetadataExtractor;

    invoke-virtual {v1, p2}, Lru/cn/player/exoplayer/MetadataExtractor;->extract(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 618
    .local v0, "items":Ljava/util/List;, "Ljava/util/List<Lru/cn/player/metadata/MetadataItem;>;"
    invoke-virtual {p0, v0}, Lru/cn/player/exoplayer/ExoMediaPlayer;->onMetadataItems(Ljava/util/List;)V

    .line 619
    return-void

    .line 615
    .end local v0    # "items":Ljava/util/List;, "Ljava/util/List<Lru/cn/player/metadata/MetadataItem;>;"
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic onTrackSelected(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 67
    check-cast p1, Lru/cn/player/exoplayer/ExoTrackInfo;

    invoke-virtual {p0, p1}, Lru/cn/player/exoplayer/ExoMediaPlayer;->onTrackSelected(Lru/cn/player/exoplayer/ExoTrackInfo;)V

    return-void
.end method

.method public onTrackSelected(Lru/cn/player/exoplayer/ExoTrackInfo;)V
    .locals 0
    .param p1, "track"    # Lru/cn/player/exoplayer/ExoTrackInfo;

    .prologue
    .line 426
    invoke-direct {p0, p1}, Lru/cn/player/exoplayer/ExoMediaPlayer;->setTrack(Lru/cn/player/TrackInfo;)V

    .line 427
    return-void
.end method

.method public onTracksChanged(Lcom/google/android/exoplayer2/source/TrackGroupArray;Lcom/google/android/exoplayer2/trackselection/TrackSelectionArray;)V
    .locals 0
    .param p1, "trackGroupArray"    # Lcom/google/android/exoplayer2/source/TrackGroupArray;
    .param p2, "trackSelectionArray"    # Lcom/google/android/exoplayer2/trackselection/TrackSelectionArray;

    .prologue
    .line 622
    return-void
.end method

.method public onVideoSizeChanged(IIIF)V
    .locals 3
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "unappliedRotationDegrees"    # I
    .param p4, "pixelWidthHeightRatio"    # F

    .prologue
    .line 591
    sget-object v0, Lru/cn/player/exoplayer/ExoMediaPlayer;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "size changed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " widthHeightRatio="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 592
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    if-eqz v0, :cond_0

    .line 593
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    invoke-interface {v0, p1, p2, p4}, Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;->videoSizeChanged(IIF)V

    .line 595
    :cond_0
    return-void
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 280
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->setPlayWhenReady(Z)V

    .line 283
    :cond_0
    return-void
.end method

.method public play(Landroid/net/Uri;)V
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v3, 0x1

    .line 211
    sget-object v2, Lru/cn/player/exoplayer/ExoMediaPlayer;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "play() uri = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->contentUri:Landroid/net/Uri;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->contentUri:Landroid/net/Uri;

    invoke-virtual {v2, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 213
    :cond_0
    iput-object p1, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->contentUri:Landroid/net/Uri;

    .line 216
    :cond_1
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p0, v2}, Lru/cn/player/exoplayer/ExoMediaPlayer;->setVolume(F)V

    .line 217
    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->detailObserver:Lru/cn/player/exoplayer/DetailObserver;

    invoke-virtual {v2}, Lru/cn/player/exoplayer/DetailObserver;->reset()V

    .line 219
    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->playerTimeoutHandler:Lru/cn/player/TimeoutHandler;

    new-instance v4, Lru/cn/player/exoplayer/ExoMediaPlayer$1;

    invoke-direct {v4, p0}, Lru/cn/player/exoplayer/ExoMediaPlayer$1;-><init>(Lru/cn/player/exoplayer/ExoMediaPlayer;)V

    const/16 v5, 0x4e20

    invoke-virtual {v2, v4, v5}, Lru/cn/player/TimeoutHandler;->startTimer(Ljava/lang/Runnable;I)V

    .line 230
    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->audioFocus:Lru/cn/player/AudioFocus;

    invoke-virtual {v2}, Lru/cn/player/AudioFocus;->request()Z

    .line 233
    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    invoke-virtual {v2}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->clearSelectionOverrides()V

    .line 234
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->getRendererCount()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 235
    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v2, v0}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->getRendererType(I)I

    move-result v2

    const/4 v4, 0x3

    if-ne v2, v4, :cond_2

    .line 236
    iget-object v4, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->trackSelector:Lru/cn/player/exoplayer/SpleenyTrackSelector;

    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->accessibility:Lru/cn/player/AbstractMediaPlayer$Accessibility;

    iget-boolean v2, v2, Lru/cn/player/AbstractMediaPlayer$Accessibility;->captioningEnable:Z

    if-nez v2, :cond_3

    move v2, v3

    :goto_1
    invoke-virtual {v4, v0, v2}, Lru/cn/player/exoplayer/SpleenyTrackSelector;->setRendererDisabled(IZ)V

    .line 234
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 236
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 240
    :cond_4
    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->setPlayWhenReady(Z)V

    .line 241
    invoke-direct {p0}, Lru/cn/player/exoplayer/ExoMediaPlayer;->buildMediaSource()Lcom/google/android/exoplayer2/source/MediaSource;

    move-result-object v1

    .line 242
    .local v1, "mediaSource":Lcom/google/android/exoplayer2/source/MediaSource;
    iget-object v2, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v2, v1, v3, v3}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->prepare(Lcom/google/android/exoplayer2/source/MediaSource;ZZ)V

    .line 244
    sget-object v2, Lru/cn/player/AbstractMediaPlayer$PlayerState;->LOADING:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    invoke-virtual {p0, v2}, Lru/cn/player/exoplayer/ExoMediaPlayer;->setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    .line 245
    return-void
.end method

.method public resume()V
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->audioFocus:Lru/cn/player/AudioFocus;

    invoke-virtual {v0}, Lru/cn/player/AudioFocus;->request()Z

    .line 289
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->setPlayWhenReady(Z)V

    .line 291
    :cond_0
    return-void
.end method

.method public seekTo(I)V
    .locals 4
    .param p1, "millis"    # I

    .prologue
    .line 399
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    if-eqz v0, :cond_0

    .line 400
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->seekTo(J)V

    .line 402
    :cond_0
    return-void
.end method

.method public setChangeQualityListener(Lru/cn/player/ChangeQualityListener;)V
    .locals 1
    .param p1, "changeQualityListener"    # Lru/cn/player/ChangeQualityListener;

    .prologue
    .line 406
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->detailObserver:Lru/cn/player/exoplayer/DetailObserver;

    invoke-virtual {v0, p1}, Lru/cn/player/exoplayer/DetailObserver;->setQualityListener(Lru/cn/player/ChangeQualityListener;)V

    .line 407
    return-void
.end method

.method public setDisplay(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1, "surfaceHolder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 373
    invoke-super {p0, p1}, Lru/cn/player/AbstractMediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 374
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    if-eqz v0, :cond_1

    .line 375
    if-eqz p1, :cond_0

    .line 376
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->getPlayWhenReady()Z

    move-result v0

    invoke-direct {p0, v0}, Lru/cn/player/exoplayer/ExoMediaPlayer;->setKeepScreenOn(Z)V

    .line 379
    :cond_0
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->setVideoSurfaceHolder(Landroid/view/SurfaceHolder;)V

    .line 381
    :cond_1
    return-void
.end method

.method public setVolume(F)V
    .locals 1
    .param p1, "volume"    # F

    .prologue
    .line 392
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->setVolume(F)V

    .line 395
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    const/16 v2, 0x14d

    .line 295
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->detailObserver:Lru/cn/player/exoplayer/DetailObserver;

    iget-object v1, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->contentUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lru/cn/player/exoplayer/DetailObserver;->checkPlaybackDiscontinuities(Landroid/net/Uri;)V

    .line 298
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->player:Lcom/google/android/exoplayer2/SimpleExoPlayer;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/SimpleExoPlayer;->stop()V

    .line 301
    :cond_0
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->audioFocus:Lru/cn/player/AudioFocus;

    invoke-virtual {v0}, Lru/cn/player/AudioFocus;->abandon()V

    .line 303
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->playerTimeoutHandler:Lru/cn/player/TimeoutHandler;

    invoke-virtual {v0}, Lru/cn/player/TimeoutHandler;->reset()V

    .line 304
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->recoveryHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 305
    iget-object v0, p0, Lru/cn/player/exoplayer/ExoMediaPlayer;->recoveryHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 308
    :cond_1
    sget-object v0, Lru/cn/player/AbstractMediaPlayer$PlayerState;->STOPPED:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lru/cn/player/exoplayer/ExoMediaPlayer;->setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    .line 309
    invoke-virtual {p0}, Lru/cn/player/exoplayer/ExoMediaPlayer;->clearSurface()V

    .line 310
    return-void
.end method
