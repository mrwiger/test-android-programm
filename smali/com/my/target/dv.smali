.class public final Lcom/my/target/dv;
.super Lcom/my/target/ak;
.source "InterstitialAdSection.java"


# instance fields
.field private allowCloseDelay:F

.field private closeIcon:Lcom/my/target/common/models/ImageData;

.field private playIcon:Lcom/my/target/common/models/ImageData;

.field private final r:Lcom/my/target/am;

.field private s:Lcom/my/target/core/models/banners/d;

.field private storeIcon:Lcom/my/target/common/models/ImageData;

.field private style:I

.field private t:Z

.field private u:Ljava/lang/String;

.field private v:Lorg/json/JSONObject;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/my/target/ak;-><init>()V

    .line 30
    invoke-static {}, Lcom/my/target/am;->W()Lcom/my/target/am;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/dv;->r:Lcom/my/target/am;

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/dv;->t:Z

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/my/target/dv;->allowCloseDelay:F

    .line 43
    sget-object v0, Lcom/my/target/af;->ce:Lcom/my/target/af;

    iput-object v0, p0, Lcom/my/target/dv;->clickArea:Lcom/my/target/af;

    .line 44
    return-void
.end method

.method public static i()Lcom/my/target/dv;
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/my/target/dv;

    invoke-direct {v0}, Lcom/my/target/dv;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/my/target/core/models/banners/d;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/my/target/dv;->s:Lcom/my/target/core/models/banners/d;

    .line 54
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/my/target/dv;->t:Z

    .line 79
    return-void
.end method

.method public final getAllowCloseDelay()F
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/my/target/dv;->allowCloseDelay:F

    return v0
.end method

.method public final getBannersCount()I
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/my/target/dv;->s:Lcom/my/target/core/models/banners/d;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getCloseIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/my/target/dv;->closeIcon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public final getHtml()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/my/target/dv;->u:Ljava/lang/String;

    return-object v0
.end method

.method public final getPlayIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/my/target/dv;->playIcon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public final getRawData()Lorg/json/JSONObject;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/my/target/dv;->v:Lorg/json/JSONObject;

    return-object v0
.end method

.method public final getStoreIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/my/target/dv;->storeIcon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public final getStyle()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/my/target/dv;->style:I

    return v0
.end method

.method public final getVideoSettings()Lcom/my/target/am;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/my/target/dv;->r:Lcom/my/target/am;

    return-object v0
.end method

.method public final j()Lcom/my/target/core/models/banners/d;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/my/target/dv;->s:Lcom/my/target/core/models/banners/d;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/my/target/dv;->t:Z

    return v0
.end method

.method public final setAllowCloseDelay(F)V
    .locals 0
    .param p1, "allowCloseDelay"    # F

    .prologue
    .line 83
    iput p1, p0, Lcom/my/target/dv;->allowCloseDelay:F

    .line 84
    return-void
.end method

.method public final setCloseIcon(Lcom/my/target/common/models/ImageData;)V
    .locals 0
    .param p1, "closeIcon"    # Lcom/my/target/common/models/ImageData;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/my/target/dv;->closeIcon:Lcom/my/target/common/models/ImageData;

    .line 64
    return-void
.end method

.method public final setHtml(Ljava/lang/String;)V
    .locals 0
    .param p1, "html"    # Ljava/lang/String;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/my/target/dv;->u:Ljava/lang/String;

    .line 124
    return-void
.end method

.method public final setPlayIcon(Lcom/my/target/common/models/ImageData;)V
    .locals 0
    .param p1, "playIcon"    # Lcom/my/target/common/models/ImageData;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/my/target/dv;->playIcon:Lcom/my/target/common/models/ImageData;

    .line 69
    return-void
.end method

.method public final setRawData(Lorg/json/JSONObject;)V
    .locals 0
    .param p1, "rawData"    # Lorg/json/JSONObject;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/my/target/dv;->v:Lorg/json/JSONObject;

    .line 129
    return-void
.end method

.method public final setStoreIcon(Lcom/my/target/common/models/ImageData;)V
    .locals 0
    .param p1, "storeIcon"    # Lcom/my/target/common/models/ImageData;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/my/target/dv;->storeIcon:Lcom/my/target/common/models/ImageData;

    .line 74
    return-void
.end method

.method public final setStyle(I)V
    .locals 0
    .param p1, "style"    # I

    .prologue
    .line 88
    iput p1, p0, Lcom/my/target/dv;->style:I

    .line 89
    return-void
.end method
