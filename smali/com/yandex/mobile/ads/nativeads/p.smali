.class public Lcom/yandex/mobile/ads/nativeads/p;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/yandex/mobile/ads/nativeads/i;

.field private b:Lcom/yandex/mobile/ads/nativeads/q;


# direct methods
.method public constructor <init>(Lcom/yandex/mobile/ads/nativeads/i;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/yandex/mobile/ads/nativeads/p;->a:Lcom/yandex/mobile/ads/nativeads/i;

    .line 35
    return-void
.end method


# virtual methods
.method a(IZ)Lcom/yandex/mobile/ads/ax;
    .locals 3

    .prologue
    .line 44
    const/4 v1, 0x0

    .line 47
    if-eqz p2, :cond_1

    .line 48
    sget-object v0, Lcom/yandex/mobile/ads/ax$a;->c:Lcom/yandex/mobile/ads/ax$a;

    .line 65
    :goto_0
    new-instance v2, Lcom/yandex/mobile/ads/ax;

    invoke-direct {v2, v0}, Lcom/yandex/mobile/ads/ax;-><init>(Lcom/yandex/mobile/ads/ax$a;)V

    .line 66
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    invoke-virtual {v2, v1}, Lcom/yandex/mobile/ads/ax;->a(Ljava/lang/String;)V

    .line 70
    :cond_0
    return-object v2

    .line 50
    :cond_1
    invoke-virtual {p0}, Lcom/yandex/mobile/ads/nativeads/p;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 51
    sget-object v0, Lcom/yandex/mobile/ads/ax$a;->k:Lcom/yandex/mobile/ads/ax$a;

    goto :goto_0

    .line 53
    :cond_2
    invoke-virtual {p0}, Lcom/yandex/mobile/ads/nativeads/p;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 54
    sget-object v0, Lcom/yandex/mobile/ads/ax$a;->l:Lcom/yandex/mobile/ads/ax$a;

    goto :goto_0

    .line 56
    :cond_3
    invoke-virtual {p0, p1}, Lcom/yandex/mobile/ads/nativeads/p;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 57
    sget-object v0, Lcom/yandex/mobile/ads/ax$a;->h:Lcom/yandex/mobile/ads/ax$a;

    goto :goto_0

    .line 60
    :cond_4
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/p;->a:Lcom/yandex/mobile/ads/nativeads/i;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/i;->a()Lcom/yandex/mobile/ads/nativeads/i$a;

    move-result-object v1

    .line 61
    invoke-virtual {v1}, Lcom/yandex/mobile/ads/nativeads/i$a;->b()Lcom/yandex/mobile/ads/ax$a;

    move-result-object v0

    .line 62
    invoke-virtual {v1}, Lcom/yandex/mobile/ads/nativeads/i$a;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method a(Lcom/yandex/mobile/ads/nativeads/q;)V
    .locals 1

    .prologue
    .line 38
    iput-object p1, p0, Lcom/yandex/mobile/ads/nativeads/p;->b:Lcom/yandex/mobile/ads/nativeads/q;

    .line 39
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/p;->a:Lcom/yandex/mobile/ads/nativeads/i;

    invoke-virtual {v0, p1}, Lcom/yandex/mobile/ads/nativeads/i;->a(Lcom/yandex/mobile/ads/nativeads/q;)V

    .line 40
    return-void
.end method

.method a()Z
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/p;->b:Lcom/yandex/mobile/ads/nativeads/q;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/q;->a()Lcom/yandex/mobile/ads/nativeads/NativeAdView;

    move-result-object v0

    .line 77
    invoke-static {v0}, Lcom/yandex/mobile/ads/utils/k;->c(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method a(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 99
    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/p;->b:Lcom/yandex/mobile/ads/nativeads/q;

    if-eqz v1, :cond_0

    .line 100
    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/p;->b:Lcom/yandex/mobile/ads/nativeads/q;

    invoke-virtual {v1}, Lcom/yandex/mobile/ads/nativeads/q;->a()Lcom/yandex/mobile/ads/nativeads/NativeAdView;

    move-result-object v1

    .line 101
    invoke-static {v1, p1}, Lcom/yandex/mobile/ads/utils/k;->a(Landroid/view/View;I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 104
    :cond_0
    :goto_0
    return v0

    .line 101
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()Z
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 84
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/p;->b:Lcom/yandex/mobile/ads/nativeads/q;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/q;->a()Lcom/yandex/mobile/ads/nativeads/NativeAdView;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    if-lt v0, v2, :cond_0

    if-ge v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c()Z
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/p;->a:Lcom/yandex/mobile/ads/nativeads/i;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/i;->e()Z

    move-result v0

    return v0
.end method
