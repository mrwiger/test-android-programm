.class Lcom/samsung/multiscreen/BLESearchProvider$1;
.super Ljava/lang/Object;
.source "BLESearchProvider.java"

# interfaces
.implements Landroid/bluetooth/BluetoothAdapter$LeScanCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/multiscreen/BLESearchProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/multiscreen/BLESearchProvider;


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/BLESearchProvider;)V
    .locals 0
    .param p1, "this$0"    # Lcom/samsung/multiscreen/BLESearchProvider;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/samsung/multiscreen/BLESearchProvider$1;->this$0:Lcom/samsung/multiscreen/BLESearchProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private isTV([B)Z
    .locals 13
    .param p1, "advertisedData"    # [B

    .prologue
    const/4 v12, 0x1

    .line 98
    const/4 v6, 0x0

    .line 99
    .local v6, "isTV":Z
    invoke-static {p1}, Lcom/samsung/multiscreen/ble/adparser/AdParser;->parseAdData([B)Ljava/util/ArrayList;

    move-result-object v0

    .line 100
    .local v0, "ads":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/multiscreen/ble/adparser/AdElement;>;"
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 101
    .local v7, "sb":Ljava/lang/StringBuilder;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v5, v10, :cond_2

    .line 102
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/multiscreen/ble/adparser/AdElement;

    .line 103
    .local v3, "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    if-lez v5, :cond_0

    .line 104
    const-string v10, " ; "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    :cond_0
    invoke-virtual {v3}, Lcom/samsung/multiscreen/ble/adparser/AdElement;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    instance-of v10, v3, Lcom/samsung/multiscreen/ble/adparser/TypeManufacturerData;

    if-eqz v10, :cond_1

    move-object v4, v3

    .line 108
    check-cast v4, Lcom/samsung/multiscreen/ble/adparser/TypeManufacturerData;

    .line 110
    .local v4, "em":Lcom/samsung/multiscreen/ble/adparser/TypeManufacturerData;
    invoke-virtual {v4}, Lcom/samsung/multiscreen/ble/adparser/TypeManufacturerData;->getManufacturer()Ljava/lang/String;

    move-result-object v10

    const-string v11, "0075"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 112
    invoke-virtual {v4}, Lcom/samsung/multiscreen/ble/adparser/TypeManufacturerData;->getBytes()[B

    move-result-object v1

    .line 113
    .local v1, "data":[B
    const/4 v10, 0x0

    aget-byte v9, v1, v10

    .line 114
    .local v9, "version":B
    aget-byte v8, v1, v12

    .line 115
    .local v8, "serviceId":B
    const/4 v10, 0x2

    aget-byte v2, v1, v10

    .line 117
    .local v2, "deviceType":B
    sget-object v10, Lcom/samsung/multiscreen/BLESearchProvider$DeviceType;->TV:Lcom/samsung/multiscreen/BLESearchProvider$DeviceType;

    invoke-virtual {v10}, Lcom/samsung/multiscreen/BLESearchProvider$DeviceType;->getValue()I

    move-result v10

    if-ne v10, v2, :cond_1

    const/4 v10, 0x3

    aget-byte v10, v1, v10

    if-ne v10, v12, :cond_1

    .line 118
    const/4 v6, 0x1

    .line 101
    .end local v1    # "data":[B
    .end local v2    # "deviceType":B
    .end local v4    # "em":Lcom/samsung/multiscreen/ble/adparser/TypeManufacturerData;
    .end local v8    # "serviceId":B
    .end local v9    # "version":B
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 124
    .end local v3    # "e":Lcom/samsung/multiscreen/ble/adparser/AdElement;
    :cond_2
    return v6
.end method

.method private reapServices()V
    .locals 9

    .prologue
    .line 162
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 163
    .local v4, "now":J
    iget-object v6, p0, Lcom/samsung/multiscreen/BLESearchProvider$1;->this$0:Lcom/samsung/multiscreen/BLESearchProvider;

    invoke-static {v6}, Lcom/samsung/multiscreen/BLESearchProvider;->access$000(Lcom/samsung/multiscreen/BLESearchProvider;)Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;

    .line 164
    .local v0, "btService":Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;
    iget-object v6, p0, Lcom/samsung/multiscreen/BLESearchProvider$1;->this$0:Lcom/samsung/multiscreen/BLESearchProvider;

    invoke-static {v6}, Lcom/samsung/multiscreen/BLESearchProvider;->access$000(Lcom/samsung/multiscreen/BLESearchProvider;)Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 165
    .local v2, "expires":J
    cmp-long v6, v2, v4

    if-gez v6, :cond_0

    .line 166
    iget-object v6, p0, Lcom/samsung/multiscreen/BLESearchProvider$1;->this$0:Lcom/samsung/multiscreen/BLESearchProvider;

    invoke-virtual {v0}, Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/samsung/multiscreen/BLESearchProvider;->getServiceById(Ljava/lang/String;)Lcom/samsung/multiscreen/Service;

    move-result-object v1

    .line 167
    .local v1, "service":Lcom/samsung/multiscreen/Service;
    iget-object v6, p0, Lcom/samsung/multiscreen/BLESearchProvider$1;->this$0:Lcom/samsung/multiscreen/BLESearchProvider;

    invoke-static {v6}, Lcom/samsung/multiscreen/BLESearchProvider;->access$000(Lcom/samsung/multiscreen/BLESearchProvider;)Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    iget-object v6, p0, Lcom/samsung/multiscreen/BLESearchProvider$1;->this$0:Lcom/samsung/multiscreen/BLESearchProvider;

    invoke-virtual {v6, v1}, Lcom/samsung/multiscreen/BLESearchProvider;->removeServiceAndNotify(Lcom/samsung/multiscreen/Service;)V

    goto :goto_0

    .line 171
    .end local v0    # "btService":Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;
    .end local v1    # "service":Lcom/samsung/multiscreen/Service;
    .end local v2    # "expires":J
    :cond_1
    return-void
.end method


# virtual methods
.method public onLeScan(Landroid/bluetooth/BluetoothDevice;I[B)V
    .locals 12
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "rssi"    # I
    .param p3, "scanRecord"    # [B

    .prologue
    const-wide/16 v10, 0x1388

    const/4 v4, 0x0

    .line 133
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 134
    .local v2, "sb":Ljava/lang/StringBuilder;
    array-length v5, p3

    move v3, v4

    :goto_0
    if-ge v3, v5, :cond_0

    aget-byte v0, p3, v3

    .line 135
    .local v0, "b":B
    const-string v6, "%02x"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 138
    .end local v0    # "b":B
    :cond_0
    new-instance v1, Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;

    iget-object v3, p0, Lcom/samsung/multiscreen/BLESearchProvider$1;->this$0:Lcom/samsung/multiscreen/BLESearchProvider;

    invoke-direct {v1, v3, p1}, Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;-><init>(Lcom/samsung/multiscreen/BLESearchProvider;Landroid/bluetooth/BluetoothDevice;)V

    .line 140
    .local v1, "btService":Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;
    iget-object v3, p0, Lcom/samsung/multiscreen/BLESearchProvider$1;->this$0:Lcom/samsung/multiscreen/BLESearchProvider;

    invoke-static {v3}, Lcom/samsung/multiscreen/BLESearchProvider;->access$000(Lcom/samsung/multiscreen/BLESearchProvider;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 141
    iget-object v3, p0, Lcom/samsung/multiscreen/BLESearchProvider$1;->this$0:Lcom/samsung/multiscreen/BLESearchProvider;

    invoke-static {v3, v1, v10, v11}, Lcom/samsung/multiscreen/BLESearchProvider;->access$100(Lcom/samsung/multiscreen/BLESearchProvider;Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;J)V

    .line 143
    invoke-direct {p0, p3}, Lcom/samsung/multiscreen/BLESearchProvider$1;->isTV([B)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 144
    iget-object v3, p0, Lcom/samsung/multiscreen/BLESearchProvider$1;->this$0:Lcom/samsung/multiscreen/BLESearchProvider;

    invoke-static {v3}, Lcom/samsung/multiscreen/BLESearchProvider;->access$200(Lcom/samsung/multiscreen/BLESearchProvider;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 146
    iget-object v3, p0, Lcom/samsung/multiscreen/BLESearchProvider$1;->this$0:Lcom/samsung/multiscreen/BLESearchProvider;

    invoke-static {v3}, Lcom/samsung/multiscreen/BLESearchProvider;->access$200(Lcom/samsung/multiscreen/BLESearchProvider;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    const/16 v3, -0x64

    if-lt p2, v3, :cond_1

    .line 148
    iget-object v3, p0, Lcom/samsung/multiscreen/BLESearchProvider$1;->this$0:Lcom/samsung/multiscreen/BLESearchProvider;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/multiscreen/BLESearchProvider;->addTVOnlyBle(Ljava/lang/String;)V

    .line 156
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/samsung/multiscreen/BLESearchProvider$1;->reapServices()V

    .line 157
    return-void

    .line 153
    :cond_2
    iget-object v3, p0, Lcom/samsung/multiscreen/BLESearchProvider$1;->this$0:Lcom/samsung/multiscreen/BLESearchProvider;

    invoke-static {v3, v1, v10, v11}, Lcom/samsung/multiscreen/BLESearchProvider;->access$100(Lcom/samsung/multiscreen/BLESearchProvider;Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;J)V

    goto :goto_1
.end method
