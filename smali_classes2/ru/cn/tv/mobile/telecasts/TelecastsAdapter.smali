.class public Lru/cn/tv/mobile/telecasts/TelecastsAdapter;
.super Landroid/support/v4/widget/SimpleCursorAdapter;
.source "TelecastsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;
    }
.end annotation


# static fields
.field private static from:[Ljava/lang/String;

.field private static to:[I


# instance fields
.field private layout:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    new-array v0, v1, [Ljava/lang/String;

    sput-object v0, Lru/cn/tv/mobile/telecasts/TelecastsAdapter;->from:[Ljava/lang/String;

    .line 24
    new-array v0, v1, [I

    sput-object v0, Lru/cn/tv/mobile/telecasts/TelecastsAdapter;->to:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I

    .prologue
    const/4 v2, 0x0

    .line 38
    const/4 v3, 0x0

    sget-object v4, Lru/cn/tv/mobile/telecasts/TelecastsAdapter;->from:[Ljava/lang/String;

    sget-object v5, Lru/cn/tv/mobile/telecasts/TelecastsAdapter;->to:[I

    move-object v0, p0

    move-object v1, p1

    move v6, v2

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 39
    iput p2, p0, Lru/cn/tv/mobile/telecasts/TelecastsAdapter;->layout:I

    .line 40
    return-void
.end method

.method private calculateElapsed(J)I
    .locals 9
    .param p1, "time"    # J

    .prologue
    .line 113
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long v2, v4, v6

    .line 114
    .local v2, "timeNowSec":J
    sub-long v0, v2, p1

    .line 116
    .local v0, "timeElapsed":J
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-gez v4, :cond_0

    .line 117
    const-wide/16 v0, 0x0

    .line 120
    :cond_0
    long-to-int v4, v0

    return v4
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 22
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 63
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;

    .local v11, "holder":Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;
    move-object/from16 v4, p3

    .line 65
    check-cast v4, Lru/cn/api/provider/cursor/TelecastItemCursor;

    .line 67
    .local v4, "c":Lru/cn/api/provider/cursor/TelecastItemCursor;
    invoke-virtual {v4}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getTitle()Ljava/lang/String;

    move-result-object v16

    .line 68
    .local v16, "title":Ljava/lang/String;
    invoke-virtual {v4}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getChannelTitle()Ljava/lang/String;

    move-result-object v6

    .line 69
    .local v6, "channelTitle":Ljava/lang/String;
    invoke-virtual {v4}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getViewsCount()I

    move-result v17

    .line 70
    .local v17, "viewsCount":I
    invoke-virtual {v4}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getTime()J

    move-result-wide v14

    .line 71
    .local v14, "time":J
    invoke-virtual {v4}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getDuration()J

    move-result-wide v8

    .line 73
    .local v8, "duration":J
    iget-object v0, v11, Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v0, v11, Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;->channelTitle:Landroid/widget/TextView;

    move-object/from16 v18, v0

    if-eqz v18, :cond_0

    .line 76
    iget-object v0, v11, Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;->channelTitle:Landroid/widget/TextView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    :cond_0
    iget-object v0, v11, Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;->dateTime:Landroid/widget/TextView;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1

    .line 80
    new-instance v7, Ljava/sql/Date;

    const-wide/16 v18, 0x3e8

    mul-long v18, v18, v14

    move-wide/from16 v0, v18

    invoke-direct {v7, v0, v1}, Ljava/sql/Date;-><init>(J)V

    .line 81
    .local v7, "d":Ljava/sql/Date;
    invoke-static {v7}, Lru/cn/utils/Utils;->getCalendar(Ljava/util/Date;)Ljava/util/Calendar;

    move-result-object v5

    .line 82
    .local v5, "calendar":Ljava/util/Calendar;
    iget-object v0, v11, Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;->dateTime:Landroid/widget/TextView;

    move-object/from16 v18, v0

    const-string v19, "dd MMMM, HH:mm"

    move-object/from16 v0, v19

    invoke-static {v5, v0}, Lru/cn/utils/Utils;->format(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    .end local v5    # "calendar":Ljava/util/Calendar;
    .end local v7    # "d":Ljava/sql/Date;
    :cond_1
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lru/cn/tv/mobile/telecasts/TelecastsAdapter;->calculateElapsed(J)I

    move-result v10

    .line 86
    .local v10, "elapsedTime":I
    int-to-long v0, v10

    move-wide/from16 v18, v0

    cmp-long v18, v18, v8

    if-gez v18, :cond_3

    .line 87
    iget-object v0, v11, Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;->airProgress:Landroid/widget/ProgressBar;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 88
    iget-object v0, v11, Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;->airProgress:Landroid/widget/ProgressBar;

    move-object/from16 v18, v0

    long-to-int v0, v8

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 89
    iget-object v0, v11, Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;->airProgress:Landroid/widget/ProgressBar;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 94
    :goto_0
    invoke-virtual {v4}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getImage()Ljava/lang/String;

    move-result-object v12

    .line 95
    .local v12, "image":Ljava/lang/String;
    invoke-static/range {p2 .. p2}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v18

    .line 96
    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v18

    .line 97
    invoke-virtual/range {v18 .. v18}, Lcom/squareup/picasso/RequestCreator;->fit()Lcom/squareup/picasso/RequestCreator;

    move-result-object v18

    .line 98
    invoke-virtual/range {v18 .. v18}, Lcom/squareup/picasso/RequestCreator;->centerCrop()Lcom/squareup/picasso/RequestCreator;

    move-result-object v18

    const v19, 0x7f0800cd

    .line 99
    invoke-virtual/range {v18 .. v19}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v18

    const v19, 0x7f0800cd

    .line 100
    invoke-virtual/range {v18 .. v19}, Lcom/squareup/picasso/RequestCreator;->error(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v18

    iget-object v0, v11, Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    .line 101
    invoke-virtual/range {v18 .. v19}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 103
    iget-object v0, v11, Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;->accessIndicator:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    invoke-virtual {v4}, Lru/cn/api/provider/cursor/TelecastItemCursor;->isPaid()Z

    move-result v18

    if-eqz v18, :cond_4

    const/16 v18, 0x0

    :goto_1
    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 105
    if-lez v17, :cond_2

    .line 106
    const v18, 0x7f0e0073

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v19, v20

    move-object/from16 v0, p2

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    .line 107
    .local v13, "text":Ljava/lang/String;
    iget-object v0, v11, Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;->viewsCount:Landroid/widget/TextView;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setVisibility(I)V

    .line 108
    iget-object v0, v11, Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;->viewsCount:Landroid/widget/TextView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    .end local v13    # "text":Ljava/lang/String;
    :cond_2
    return-void

    .line 91
    .end local v12    # "image":Ljava/lang/String;
    :cond_3
    iget-object v0, v11, Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;->airProgress:Landroid/widget/ProgressBar;

    move-object/from16 v18, v0

    const/16 v19, 0x8

    invoke-virtual/range {v18 .. v19}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 103
    .restart local v12    # "image":Ljava/lang/String;
    :cond_4
    const/16 v18, 0x8

    goto :goto_1
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 44
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 46
    .local v1, "inflater":Landroid/view/LayoutInflater;
    iget v3, p0, Lru/cn/tv/mobile/telecasts/TelecastsAdapter;->layout:I

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 47
    .local v2, "view":Landroid/view/View;
    new-instance v0, Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;

    invoke-direct {v0, v5}, Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;-><init>(Lru/cn/tv/mobile/telecasts/TelecastsAdapter$1;)V

    .line 48
    .local v0, "holder":Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;
    const v3, 0x7f0900f3

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v0, Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    .line 49
    const v3, 0x7f0901d7

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 50
    const v3, 0x7f09006b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;->channelTitle:Landroid/widget/TextView;

    .line 51
    const v3, 0x7f090096

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;->dateTime:Landroid/widget/TextView;

    .line 52
    const v3, 0x7f0901df

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;->viewsCount:Landroid/widget/TextView;

    .line 53
    const v3, 0x7f09002c

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    iput-object v3, v0, Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;->airProgress:Landroid/widget/ProgressBar;

    .line 54
    const v3, 0x7f090006

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v0, Lru/cn/tv/mobile/telecasts/TelecastsAdapter$ViewHolder;->accessIndicator:Landroid/widget/ImageView;

    .line 56
    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 58
    return-object v2
.end method
