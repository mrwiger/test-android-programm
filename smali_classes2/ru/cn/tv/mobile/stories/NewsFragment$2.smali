.class Lru/cn/tv/mobile/stories/NewsFragment$2;
.super Ljava/lang/Object;
.source "NewsFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/mobile/stories/NewsFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/mobile/stories/NewsFragment;


# direct methods
.method constructor <init>(Lru/cn/tv/mobile/stories/NewsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/mobile/stories/NewsFragment;

    .prologue
    .line 76
    iput-object p1, p0, Lru/cn/tv/mobile/stories/NewsFragment$2;->this$0:Lru/cn/tv/mobile/stories/NewsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 79
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v5, p0, Lru/cn/tv/mobile/stories/NewsFragment$2;->this$0:Lru/cn/tv/mobile/stories/NewsFragment;

    invoke-virtual {v5}, Lru/cn/tv/mobile/stories/NewsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, p3, v6}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 81
    iget-object v5, p0, Lru/cn/tv/mobile/stories/NewsFragment$2;->this$0:Lru/cn/tv/mobile/stories/NewsFragment;

    invoke-static {v5}, Lru/cn/tv/mobile/stories/NewsFragment;->access$200(Lru/cn/tv/mobile/stories/NewsFragment;)Lru/cn/tv/mobile/stories/NewsFragment$NewsFragmentListener;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 82
    iget-object v5, p0, Lru/cn/tv/mobile/stories/NewsFragment$2;->this$0:Lru/cn/tv/mobile/stories/NewsFragment;

    invoke-static {v5}, Lru/cn/tv/mobile/stories/NewsFragment;->access$300(Lru/cn/tv/mobile/stories/NewsFragment;)Lru/cn/tv/mobile/stories/StoriesRubricAdapter;

    move-result-object v5

    invoke-virtual {v5, p3}, Lru/cn/tv/mobile/stories/StoriesRubricAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 84
    .local v0, "cursor":Landroid/database/Cursor;
    invoke-static {v0}, Lru/cn/api/provider/TvContentProviderContract$Helper;->getRubricId(Landroid/database/Cursor;)J

    move-result-wide v2

    .line 85
    .local v2, "rubricId":J
    invoke-static {v0}, Lru/cn/api/provider/TvContentProviderContract$Helper;->getRubricUiHint(Landroid/database/Cursor;)Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    move-result-object v1

    .line 87
    .local v1, "hint":Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    invoke-static {v0}, Lru/cn/api/provider/TvContentProviderContract$Helper;->getRubricTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    .line 89
    .local v4, "title":Ljava/lang/String;
    iget-object v5, p0, Lru/cn/tv/mobile/stories/NewsFragment$2;->this$0:Lru/cn/tv/mobile/stories/NewsFragment;

    invoke-static {v5}, Lru/cn/tv/mobile/stories/NewsFragment;->access$200(Lru/cn/tv/mobile/stories/NewsFragment;)Lru/cn/tv/mobile/stories/NewsFragment$NewsFragmentListener;

    move-result-object v5

    invoke-interface {v5, v2, v3, v1, v4}, Lru/cn/tv/mobile/stories/NewsFragment$NewsFragmentListener;->rubricClicked(JLru/cn/api/catalogue/replies/Rubric$UiHintType;Ljava/lang/String;)V

    .line 91
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v1    # "hint":Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    .end local v2    # "rubricId":J
    .end local v4    # "title":Ljava/lang/String;
    :cond_0
    return-void
.end method
