.class public Lru/cn/ad/MiddleRollFactory;
.super Ljava/lang/Object;
.source "MiddleRollFactory.java"

# interfaces
.implements Lru/cn/ad/AdAdapter$Factory;


# instance fields
.field private final context:Landroid/content/Context;

.field private final isTV:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Z)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isTV"    # Z

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lru/cn/ad/MiddleRollFactory;->context:Landroid/content/Context;

    .line 17
    iput-boolean p2, p0, Lru/cn/ad/MiddleRollFactory;->isTV:Z

    .line 18
    return-void
.end method

.method private createMobileAdapter(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)Lru/cn/ad/AdAdapter;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "system"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    const/4 v0, 0x0

    .line 29
    iget v1, p2, Lru/cn/api/money_miner/replies/AdSystem;->bannerType:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    .line 43
    :goto_0
    return-object v0

    .line 32
    :cond_0
    iget v1, p2, Lru/cn/api/money_miner/replies/AdSystem;->type:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 34
    :sswitch_0
    new-instance v0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    invoke-direct {v0, p1, p2}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    goto :goto_0

    .line 37
    :sswitch_1
    new-instance v0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;

    invoke-direct {v0, p1, p2}, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    goto :goto_0

    .line 40
    :sswitch_2
    new-instance v0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;

    invoke-direct {v0, p1, p2}, Lru/cn/ad/natives/adapters/FacebookNativeAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    goto :goto_0

    .line 32
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x8 -> :sswitch_1
        0xb -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public create(Lru/cn/api/money_miner/replies/AdSystem;)Lru/cn/ad/AdAdapter;
    .locals 1
    .param p1, "system"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 22
    iget-boolean v0, p0, Lru/cn/ad/MiddleRollFactory;->isTV:Z

    if-eqz v0, :cond_0

    .line 23
    const/4 v0, 0x0

    .line 25
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lru/cn/ad/MiddleRollFactory;->context:Landroid/content/Context;

    invoke-direct {p0, v0, p1}, Lru/cn/ad/MiddleRollFactory;->createMobileAdapter(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)Lru/cn/ad/AdAdapter;

    move-result-object v0

    goto :goto_0
.end method
