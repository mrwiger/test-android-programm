.class public interface abstract Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;
.super Ljava/lang/Object;
.source "StbPlayerSettingsPanel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/player/controller/StbPlayerSettingsPanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract audioTracks()V
.end method

.method public abstract fitModeChanged(Lru/cn/player/SimplePlayer$FitMode;)V
.end method

.method public abstract subtitle()V
.end method

.method public abstract volume()V
.end method

.method public abstract zoomIn()V
.end method

.method public abstract zoomOut()V
.end method
