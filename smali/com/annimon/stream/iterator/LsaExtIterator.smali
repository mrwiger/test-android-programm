.class public abstract Lcom/annimon/stream/iterator/LsaExtIterator;
.super Ljava/lang/Object;
.source "LsaExtIterator.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected hasNext:Z

.field protected isInit:Z

.field protected next:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    .local p0, "this":Lcom/annimon/stream/iterator/LsaExtIterator;, "Lcom/annimon/stream/iterator/LsaExtIterator<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 18
    .local p0, "this":Lcom/annimon/stream/iterator/LsaExtIterator;, "Lcom/annimon/stream/iterator/LsaExtIterator<TT;>;"
    iget-boolean v0, p0, Lcom/annimon/stream/iterator/LsaExtIterator;->isInit:Z

    if-nez v0, :cond_0

    .line 20
    invoke-virtual {p0}, Lcom/annimon/stream/iterator/LsaExtIterator;->nextIteration()V

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/annimon/stream/iterator/LsaExtIterator;->isInit:Z

    .line 23
    :cond_0
    iget-boolean v0, p0, Lcom/annimon/stream/iterator/LsaExtIterator;->hasNext:Z

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 28
    .local p0, "this":Lcom/annimon/stream/iterator/LsaExtIterator;, "Lcom/annimon/stream/iterator/LsaExtIterator<TT;>;"
    iget-boolean v1, p0, Lcom/annimon/stream/iterator/LsaExtIterator;->isInit:Z

    if-nez v1, :cond_0

    .line 30
    invoke-virtual {p0}, Lcom/annimon/stream/iterator/LsaExtIterator;->hasNext()Z

    .line 32
    :cond_0
    iget-boolean v1, p0, Lcom/annimon/stream/iterator/LsaExtIterator;->hasNext:Z

    if-nez v1, :cond_1

    .line 33
    new-instance v1, Ljava/util/NoSuchElementException;

    invoke-direct {v1}, Ljava/util/NoSuchElementException;-><init>()V

    throw v1

    .line 35
    :cond_1
    iget-object v0, p0, Lcom/annimon/stream/iterator/LsaExtIterator;->next:Ljava/lang/Object;

    .line 36
    .local v0, "result":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0}, Lcom/annimon/stream/iterator/LsaExtIterator;->nextIteration()V

    .line 37
    iget-boolean v1, p0, Lcom/annimon/stream/iterator/LsaExtIterator;->hasNext:Z

    if-nez v1, :cond_2

    .line 39
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/annimon/stream/iterator/LsaExtIterator;->next:Ljava/lang/Object;

    .line 41
    :cond_2
    return-object v0
.end method

.method protected abstract nextIteration()V
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 48
    .local p0, "this":Lcom/annimon/stream/iterator/LsaExtIterator;, "Lcom/annimon/stream/iterator/LsaExtIterator<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "remove not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
