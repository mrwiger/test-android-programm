.class Lru/cn/player/androidplayer/AndroidMediaPlayer$4;
.super Ljava/lang/Object;
.source "AndroidMediaPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/player/androidplayer/AndroidMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;


# direct methods
.method constructor <init>(Lru/cn/player/androidplayer/AndroidMediaPlayer;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/player/androidplayer/AndroidMediaPlayer;

    .prologue
    .line 99
    iput-object p1, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$4;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 3
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "framework_err"    # I
    .param p3, "impl_err"    # I

    .prologue
    .line 101
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$4;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    invoke-static {v0}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->access$800(Lru/cn/player/androidplayer/AndroidMediaPlayer;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$4;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    invoke-static {v0}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->access$900(Lru/cn/player/androidplayer/AndroidMediaPlayer;)Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$4;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    invoke-static {v0}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->access$1000(Lru/cn/player/androidplayer/AndroidMediaPlayer;)Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    move-result-object v0

    invoke-interface {v0, p3}, Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;->onError(I)V

    .line 107
    :cond_0
    const/16 v0, 0x64

    if-ne p2, v0, :cond_1

    .line 108
    sget-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->UNKNOWN_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v1, "MediaServerDied"

    const/4 v2, 0x0

    invoke-static {v0, v1, p3, v2}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    .line 112
    :cond_1
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$4;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    invoke-virtual {v0}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->stop()V

    .line 114
    const/4 v0, 0x1

    return v0
.end method
