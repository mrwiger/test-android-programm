.class public Lru/cn/player/AudioFocus;
.super Ljava/lang/Object;
.source "AudioFocus.java"


# instance fields
.field private final audioManager:Landroid/media/AudioManager;

.field private final context:Landroid/content/Context;

.field private hasAudioFocus:Z

.field private final headsetReceiver:Lru/cn/player/HeadsetReceiver;

.field private final onAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private final pauseOnTransientLoss:Z

.field private final player:Lru/cn/player/AbstractMediaPlayer;

.field private restoreInterrupted:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lru/cn/player/AbstractMediaPlayer;Lru/cn/player/HeadsetReceiver;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "player"    # Lru/cn/player/AbstractMediaPlayer;
    .param p3, "headsetReceiver"    # Lru/cn/player/HeadsetReceiver;
    .param p4, "pauseOnTransientLoss"    # Z

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Lru/cn/player/AudioFocus$1;

    invoke-direct {v0, p0}, Lru/cn/player/AudioFocus$1;-><init>(Lru/cn/player/AudioFocus;)V

    iput-object v0, p0, Lru/cn/player/AudioFocus;->onAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 22
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lru/cn/player/AudioFocus;->audioManager:Landroid/media/AudioManager;

    .line 23
    iput-object p1, p0, Lru/cn/player/AudioFocus;->context:Landroid/content/Context;

    .line 24
    iput-object p2, p0, Lru/cn/player/AudioFocus;->player:Lru/cn/player/AbstractMediaPlayer;

    .line 25
    iput-object p3, p0, Lru/cn/player/AudioFocus;->headsetReceiver:Lru/cn/player/HeadsetReceiver;

    .line 26
    iput-boolean p4, p0, Lru/cn/player/AudioFocus;->pauseOnTransientLoss:Z

    .line 27
    return-void
.end method

.method static synthetic access$000(Lru/cn/player/AudioFocus;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/player/AudioFocus;

    .prologue
    .line 7
    iget-boolean v0, p0, Lru/cn/player/AudioFocus;->restoreInterrupted:Z

    return v0
.end method

.method static synthetic access$002(Lru/cn/player/AudioFocus;Z)Z
    .locals 0
    .param p0, "x0"    # Lru/cn/player/AudioFocus;
    .param p1, "x1"    # Z

    .prologue
    .line 7
    iput-boolean p1, p0, Lru/cn/player/AudioFocus;->restoreInterrupted:Z

    return p1
.end method

.method static synthetic access$100(Lru/cn/player/AudioFocus;)Lru/cn/player/AbstractMediaPlayer;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/AudioFocus;

    .prologue
    .line 7
    iget-object v0, p0, Lru/cn/player/AudioFocus;->player:Lru/cn/player/AbstractMediaPlayer;

    return-object v0
.end method

.method static synthetic access$202(Lru/cn/player/AudioFocus;Z)Z
    .locals 0
    .param p0, "x0"    # Lru/cn/player/AudioFocus;
    .param p1, "x1"    # Z

    .prologue
    .line 7
    iput-boolean p1, p0, Lru/cn/player/AudioFocus;->hasAudioFocus:Z

    return p1
.end method

.method static synthetic access$300(Lru/cn/player/AudioFocus;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/player/AudioFocus;

    .prologue
    .line 7
    iget-boolean v0, p0, Lru/cn/player/AudioFocus;->pauseOnTransientLoss:Z

    return v0
.end method


# virtual methods
.method public final abandon()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 44
    iget-boolean v0, p0, Lru/cn/player/AudioFocus;->hasAudioFocus:Z

    if-nez v0, :cond_0

    .line 53
    :goto_0
    return-void

    .line 47
    :cond_0
    iget-object v0, p0, Lru/cn/player/AudioFocus;->audioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lru/cn/player/AudioFocus;->onAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 49
    iget-object v0, p0, Lru/cn/player/AudioFocus;->headsetReceiver:Lru/cn/player/HeadsetReceiver;

    iget-object v1, p0, Lru/cn/player/AudioFocus;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lru/cn/player/HeadsetReceiver;->unregister(Landroid/content/Context;)V

    .line 51
    iput-boolean v2, p0, Lru/cn/player/AudioFocus;->restoreInterrupted:Z

    .line 52
    iput-boolean v2, p0, Lru/cn/player/AudioFocus;->hasAudioFocus:Z

    goto :goto_0
.end method

.method public final request()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 30
    iget-boolean v2, p0, Lru/cn/player/AudioFocus;->hasAudioFocus:Z

    if-eqz v2, :cond_0

    .line 40
    :goto_0
    return v1

    .line 33
    :cond_0
    iget-object v2, p0, Lru/cn/player/AudioFocus;->audioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lru/cn/player/AudioFocus;->onAudioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4, v1}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 36
    .local v0, "result":I
    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lru/cn/player/AudioFocus;->hasAudioFocus:Z

    .line 38
    iget-object v1, p0, Lru/cn/player/AudioFocus;->headsetReceiver:Lru/cn/player/HeadsetReceiver;

    iget-object v2, p0, Lru/cn/player/AudioFocus;->context:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lru/cn/player/HeadsetReceiver;->register(Landroid/content/Context;)V

    .line 40
    iget-boolean v1, p0, Lru/cn/player/AudioFocus;->hasAudioFocus:Z

    goto :goto_0

    .line 36
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
