.class public abstract Lru/cn/ad/natives/adapters/NativeAdAdapter;
.super Lru/cn/ad/AdAdapter;
.source "NativeAdAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;,
        Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;
    }
.end annotation


# instance fields
.field private presenter:Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "system"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lru/cn/ad/AdAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    .line 60
    return-void
.end method


# virtual methods
.method public getPresenter()Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lru/cn/ad/natives/adapters/NativeAdAdapter;->presenter:Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    return-object v0
.end method

.method public setPresenter(Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;)V
    .locals 0
    .param p1, "presenter"    # Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    .prologue
    .line 63
    iput-object p1, p0, Lru/cn/ad/natives/adapters/NativeAdAdapter;->presenter:Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    .line 64
    return-void
.end method
