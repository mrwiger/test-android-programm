.class Lcom/yandex/metrica/impl/ob/du;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/du;->a:Landroid/content/Context;

    .line 30
    return-void
.end method


# virtual methods
.method public a(JLjava/lang/String;)Lcom/yandex/metrica/impl/ob/eh;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0, p3}, Lcom/yandex/metrica/impl/ob/du;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 45
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, p2, v0}, Lcom/yandex/metrica/impl/ob/eo;->a(JLjava/lang/String;)Lcom/yandex/metrica/impl/ob/eh;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/yandex/metrica/impl/ob/ea;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/eo;->a(Lcom/yandex/metrica/impl/ob/ea;)Ljava/lang/String;

    move-result-object v0

    .line 51
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 52
    :goto_0
    return-object v0

    .line 51
    :cond_0
    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/du;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/yandex/metrica/impl/ob/eh;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 34
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/eo;->a(Lcom/yandex/metrica/impl/ob/eh;)Ljava/lang/String;

    move-result-object v1

    .line 35
    const/4 v0, 0x0

    .line 36
    if-eqz v1, :cond_0

    .line 37
    invoke-virtual {p0, v1}, Lcom/yandex/metrica/impl/ob/du;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 39
    :cond_0
    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 62
    const/4 v0, 0x0

    .line 65
    :try_start_0
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/du;->a:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/yandex/metrica/impl/utils/m;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 69
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method b(JLjava/lang/String;)Lcom/yandex/metrica/impl/ob/ea;
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0, p3}, Lcom/yandex/metrica/impl/ob/du;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 57
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, p2, v0}, Lcom/yandex/metrica/impl/ob/eo;->b(JLjava/lang/String;)Lcom/yandex/metrica/impl/ob/ea;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 74
    const/4 v0, 0x0

    .line 76
    :try_start_0
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/du;->a:Landroid/content/Context;

    .line 1026
    const-string v2, "UTF-8"

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/utils/m;->a(Landroid/content/Context;[B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 80
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method
