.class public Lru/cn/tv/playlists/UserPlaylistAddEditDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "UserPlaylistAddEditDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;,
        Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;
    }
.end annotation


# instance fields
.field private currentDialogType:Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

.field private locationEditText:Landroid/widget/EditText;

.field private positiveButton:Landroid/widget/Button;

.field private progress:Landroid/view/View;

.field private titleEditText:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->progress:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->titleEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->locationEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->positiveButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$400(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;)Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->currentDialogType:Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

    return-object v0
.end method

.method static synthetic access$500(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/playlists/UserPlaylistAddEditDialog;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->addNewPlaylist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/playlists/UserPlaylistAddEditDialog;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->editPlaylist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    .prologue
    .line 27
    invoke-direct {p0}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->savePlaylist()V

    return-void
.end method

.method private addNewPlaylist(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "location"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 263
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 264
    .local v2, "values":Landroid/content/ContentValues;
    sget-object v4, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->title:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    invoke-virtual {v4}, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    sget-object v4, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->location:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    invoke-virtual {v4}, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    sget-object v4, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->contractor_id:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    invoke-virtual {v4}, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 267
    sget-object v4, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->priority:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    invoke-virtual {v4}, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 269
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->playlistUri()Landroid/net/Uri;

    move-result-object v0

    .line 270
    .local v0, "playlistUri":Landroid/net/Uri;
    invoke-virtual {p0}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 272
    .local v1, "uri":Landroid/net/Uri;
    if-eqz v1, :cond_0

    const/4 v3, 0x1

    :cond_0
    return v3
.end method

.method private editPlaylist(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "location"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 276
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 277
    .local v2, "values":Landroid/content/ContentValues;
    sget-object v5, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->title:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    invoke-virtual {v5}, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->playlistUri()Landroid/net/Uri;

    move-result-object v1

    .line 280
    .local v1, "playlistUri":Landroid/net/Uri;
    invoke-virtual {p0}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "location"

    new-array v7, v3, [Ljava/lang/String;

    aput-object p1, v7, v4

    invoke-virtual {v5, v1, v2, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 283
    .local v0, "count":I
    if-lez v0, :cond_0

    :goto_0
    return v3

    :cond_0
    move v3, v4

    goto :goto_0
.end method

.method public static newInstance()Lru/cn/tv/playlists/UserPlaylistAddEditDialog;
    .locals 2

    .prologue
    .line 41
    new-instance v1, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    invoke-direct {v1}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;-><init>()V

    .line 42
    .local v1, "frag":Lru/cn/tv/playlists/UserPlaylistAddEditDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 43
    .local v0, "args":Landroid/os/Bundle;
    invoke-virtual {v1, v0}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->setArguments(Landroid/os/Bundle;)V

    .line 44
    return-object v1
.end method

.method public static newInstance(JLjava/lang/String;Ljava/lang/String;)Lru/cn/tv/playlists/UserPlaylistAddEditDialog;
    .locals 4
    .param p0, "playlistId"    # J
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "location"    # Ljava/lang/String;

    .prologue
    .line 49
    new-instance v1, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    invoke-direct {v1}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;-><init>()V

    .line 50
    .local v1, "f":Lru/cn/tv/playlists/UserPlaylistAddEditDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 51
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "playlistId"

    invoke-virtual {v0, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 52
    const-string v2, "title"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-string v2, "location"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    invoke-virtual {v1, v0}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->setArguments(Landroid/os/Bundle;)V

    .line 55
    return-object v1
.end method

.method private savePlaylist()V
    .locals 5

    .prologue
    .line 202
    iget-object v2, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->titleEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 203
    .local v1, "title":Ljava/lang/String;
    iget-object v2, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->locationEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 204
    .local v0, "location":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 205
    :cond_0
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 206
    iget-object v2, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->titleEditText:Landroid/widget/EditText;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 207
    iget-object v2, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->titleEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    .line 213
    :goto_0
    invoke-virtual {p0}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 214
    invoke-virtual {p0}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0091

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 213
    invoke-static {v2, v3}, Lru/cn/tv/errors/ErrorDialog;->create(Landroid/content/Context;Ljava/lang/CharSequence;)Landroid/app/Dialog;

    move-result-object v2

    .line 214
    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    .line 219
    :goto_1
    return-void

    .line 209
    :cond_1
    iget-object v2, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->locationEditText:Landroid/widget/EditText;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 210
    iget-object v2, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->locationEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    .line 218
    :cond_2
    new-instance v2, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;

    invoke-direct {v2, p0, v1, v0}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;-><init>(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v2, v3}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$CheckPlaylistTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x0

    .line 60
    invoke-virtual {p0}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    .line 61
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v7, 0x7f0c00bc

    invoke-virtual {v3, v7, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 63
    .local v6, "view":Landroid/view/View;
    const v7, 0x7f0901d7

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    iput-object v7, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->titleEditText:Landroid/widget/EditText;

    .line 64
    const v7, 0x7f090114

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    iput-object v7, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->locationEditText:Landroid/widget/EditText;

    .line 65
    iget-object v7, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->locationEditText:Landroid/widget/EditText;

    iget-object v8, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->locationEditText:Landroid/widget/EditText;

    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->length()I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setSelection(I)V

    .line 66
    const v7, 0x7f090175

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->progress:Landroid/view/View;

    .line 68
    const/4 v2, 0x0

    .line 69
    .local v2, "dialogTitle":Ljava/lang/String;
    invoke-virtual {p0}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 70
    .local v0, "arguments":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 71
    const-string v7, "location"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 73
    .local v4, "location":Ljava/lang/String;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_0

    .line 74
    sget-object v7, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;->editplaylist:Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

    iput-object v7, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->currentDialogType:Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

    .line 75
    const v7, 0x7f0e017d

    invoke-virtual {p0, v7}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 76
    iget-object v7, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->locationEditText:Landroid/widget/EditText;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 77
    const-string v7, "title"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 78
    .local v5, "title":Ljava/lang/String;
    iget-object v7, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->locationEditText:Landroid/widget/EditText;

    invoke-virtual {v7, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 79
    iget-object v7, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->titleEditText:Landroid/widget/EditText;

    invoke-virtual {v7, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 83
    .end local v4    # "location":Ljava/lang/String;
    .end local v5    # "title":Ljava/lang/String;
    :cond_0
    if-eqz v0, :cond_1

    const-string v7, "location"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_2

    .line 84
    :cond_1
    sget-object v7, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;->newPlaylist:Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

    iput-object v7, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->currentDialogType:Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

    .line 85
    const v7, 0x7f0e017b

    invoke-virtual {p0, v7}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 88
    :cond_2
    new-instance v7, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 89
    invoke-virtual {v7, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v7

    .line 90
    invoke-virtual {v7, v6}, Landroid/support/v7/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v7

    const v8, 0x7f0e0129

    .line 91
    invoke-virtual {v7, v8, v9}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v7

    const v8, 0x7f0e0039

    .line 92
    invoke-virtual {v7, v8, v9}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v7

    .line 93
    invoke-virtual {v7}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v1

    .line 95
    .local v1, "d":Landroid/support/v7/app/AlertDialog;
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v7

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 96
    return-object v1
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 223
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onStart()V

    .line 224
    invoke-virtual {p0}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/AlertDialog;

    .line 225
    .local v0, "d":Landroid/support/v7/app/AlertDialog;
    if-eqz v0, :cond_0

    .line 226
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    iput-object v1, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->positiveButton:Landroid/widget/Button;

    .line 227
    iget-object v1, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->positiveButton:Landroid/widget/Button;

    new-instance v2, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$1;

    invoke-direct {v2, p0}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$1;-><init>(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 234
    iget-object v1, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->locationEditText:Landroid/widget/EditText;

    new-instance v2, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$2;

    invoke-direct {v2, p0}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$2;-><init>(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 246
    iget-object v1, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->currentDialogType:Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

    sget-object v2, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;->editplaylist:Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

    if-ne v1, v2, :cond_0

    .line 247
    iget-object v1, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->titleEditText:Landroid/widget/EditText;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 248
    iget-object v1, p0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->titleEditText:Landroid/widget/EditText;

    new-instance v2, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$3;

    invoke-direct {v2, p0}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$3;-><init>(Lru/cn/tv/playlists/UserPlaylistAddEditDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 260
    :cond_0
    return-void
.end method
