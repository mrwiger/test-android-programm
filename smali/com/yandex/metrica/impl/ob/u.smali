.class public Lcom/yandex/metrica/impl/ob/u;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ob/u$f;,
        Lcom/yandex/metrica/impl/ob/u$g;,
        Lcom/yandex/metrica/impl/ob/u$b;,
        Lcom/yandex/metrica/impl/ob/u$d;,
        Lcom/yandex/metrica/impl/ob/u$c;,
        Lcom/yandex/metrica/impl/ob/u$a;,
        Lcom/yandex/metrica/impl/ob/u$e;
    }
.end annotation


# instance fields
.field private final a:Lcom/yandex/metrica/impl/ob/v;

.field private final b:Lcom/yandex/metrica/impl/ob/fd;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/yandex/metrica/impl/ob/u$f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/fd;)V
    .locals 4

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/u;->a:Lcom/yandex/metrica/impl/ob/v;

    .line 36
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/u;->b:Lcom/yandex/metrica/impl/ob/fd;

    .line 1041
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/u;->c:Ljava/util/List;

    .line 1042
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u;->c:Ljava/util/List;

    new-instance v1, Lcom/yandex/metrica/impl/ob/u$b;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/u;->a:Lcom/yandex/metrica/impl/ob/v;

    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/u;->b:Lcom/yandex/metrica/impl/ob/fd;

    invoke-direct {v1, v2, v3}, Lcom/yandex/metrica/impl/ob/u$b;-><init>(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/fd;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1043
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u;->c:Ljava/util/List;

    new-instance v1, Lcom/yandex/metrica/impl/ob/u$d;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/u;->a:Lcom/yandex/metrica/impl/ob/v;

    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/u;->b:Lcom/yandex/metrica/impl/ob/fd;

    invoke-direct {v1, v2, v3}, Lcom/yandex/metrica/impl/ob/u$d;-><init>(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/fd;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1044
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u;->c:Ljava/util/List;

    new-instance v1, Lcom/yandex/metrica/impl/ob/u$c;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/u;->a:Lcom/yandex/metrica/impl/ob/v;

    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/u;->a:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/ob/v;->E()Lcom/yandex/metrica/impl/ob/fe;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/yandex/metrica/impl/ob/u$c;-><init>(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/fe;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1045
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u;->c:Ljava/util/List;

    new-instance v1, Lcom/yandex/metrica/impl/ob/u$a;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/u;->a:Lcom/yandex/metrica/impl/ob/v;

    invoke-direct {v1, v2}, Lcom/yandex/metrica/impl/ob/u$a;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1046
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u;->c:Ljava/util/List;

    new-instance v1, Lcom/yandex/metrica/impl/ob/u$e;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/u;->a:Lcom/yandex/metrica/impl/ob/v;

    invoke-direct {v1, v2}, Lcom/yandex/metrica/impl/ob/u$e;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    return-void
.end method


# virtual methods
.method a()V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u;->a:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/v;->m()Lcom/yandex/metrica/impl/ob/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/t;->a()Ljava/lang/String;

    move-result-object v0

    .line 1059
    sget-object v1, Lcom/yandex/metrica/impl/ob/fd;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 51
    if-nez v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/u$f;

    .line 53
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/u$f;->f()V

    goto :goto_0

    .line 56
    :cond_0
    return-void
.end method
