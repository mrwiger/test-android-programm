.class public Lru/cn/utils/validation/CompositeValidator;
.super Ljava/lang/Object;
.source "CompositeValidator.java"

# interfaces
.implements Lru/cn/utils/validation/Validator;


# instance fields
.field private description:Ljava/lang/String;

.field private final validators:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/utils/validation/Validator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>([Lru/cn/utils/validation/Validator;)V
    .locals 1
    .param p1, "validators"    # [Lru/cn/utils/validation/Validator;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lru/cn/utils/validation/CompositeValidator;->validators:Ljava/util/List;

    .line 14
    return-void
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lru/cn/utils/validation/CompositeValidator;->description:Ljava/lang/String;

    return-object v0
.end method

.method public validate()Z
    .locals 3

    .prologue
    .line 18
    iget-object v1, p0, Lru/cn/utils/validation/CompositeValidator;->validators:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/utils/validation/Validator;

    .line 19
    .local v0, "validator":Lru/cn/utils/validation/Validator;
    invoke-interface {v0}, Lru/cn/utils/validation/Validator;->validate()Z

    move-result v2

    if-nez v2, :cond_0

    .line 20
    invoke-interface {v0}, Lru/cn/utils/validation/Validator;->getDescription()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lru/cn/utils/validation/CompositeValidator;->description:Ljava/lang/String;

    .line 21
    const/4 v1, 0x0

    .line 24
    .end local v0    # "validator":Lru/cn/utils/validation/Validator;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method
