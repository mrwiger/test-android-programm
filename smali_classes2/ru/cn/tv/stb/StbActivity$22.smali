.class Lru/cn/tv/stb/StbActivity$22;
.super Lru/cn/tv/stb/ListKeyListener;
.source "StbActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/stb/StbActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/stb/StbActivity;


# direct methods
.method constructor <init>(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 1589
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity$22;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-direct {p0}, Lru/cn/tv/stb/ListKeyListener;-><init>()V

    return-void
.end method


# virtual methods
.method protected keyLeft()Z
    .locals 1

    .prologue
    .line 1592
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$22;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$300(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/categories/CategoryFragment;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/stb/categories/CategoryFragment;->isFolded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1593
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$22;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$300(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/categories/CategoryFragment;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/stb/categories/CategoryFragment;->expand()V

    .line 1598
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1595
    :cond_0
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$22;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$1100(Lru/cn/tv/stb/StbActivity;)V

    goto :goto_0
.end method

.method protected keyRight()Z
    .locals 2

    .prologue
    .line 1602
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$22;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$300(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/categories/CategoryFragment;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$22;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$2900(Lru/cn/tv/stb/StbActivity;)Lru/cn/domain/tv/CurrentCategory$Type;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/cn/tv/stb/categories/CategoryFragment;->setSelection(Lru/cn/domain/tv/CurrentCategory$Type;)V

    .line 1603
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$22;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$4800(Lru/cn/tv/stb/StbActivity;)V

    .line 1604
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$22;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$4900(Lru/cn/tv/stb/StbActivity;)V

    .line 1606
    const/4 v0, 0x1

    return v0
.end method
