.class public Lru/cn/api/billing/PinAuthorisationApi;
.super Lru/cn/api/BaseAPI;
.source "PinAuthorisationApi.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/api/billing/PinAuthorisationApi$PinCodeStatusDeserializer;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lru/cn/api/BaseAPI;-><init>()V

    return-void
.end method


# virtual methods
.method public getStatus(Ljava/lang/String;)Lru/cn/api/billing/PinAuthorisationStatus;
    .locals 7
    .param p1, "requestUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/URISyntaxException;,
            Ljava/io/IOException;,
            Lru/cn/api/BaseAPI$ParseException;
        }
    .end annotation

    .prologue
    .line 20
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "?inetra-visit-reason=check-parental-pincode-authorization"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lru/cn/api/BaseAPI;->getContent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 23
    .local v0, "content":Ljava/lang/String;
    new-instance v3, Lcom/google/gson/GsonBuilder;

    invoke-direct {v3}, Lcom/google/gson/GsonBuilder;-><init>()V

    const-class v4, Lru/cn/api/billing/PinAuthorisationStatus$Status;

    new-instance v5, Lru/cn/api/billing/PinAuthorisationApi$PinCodeStatusDeserializer;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lru/cn/api/billing/PinAuthorisationApi$PinCodeStatusDeserializer;-><init>(Lru/cn/api/billing/PinAuthorisationApi$1;)V

    .line 24
    invoke-virtual {v3, v4, v5}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v3

    .line 25
    invoke-virtual {v3}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v2

    .line 28
    .local v2, "gson":Lcom/google/gson/Gson;
    :try_start_0
    const-class v3, Lru/cn/api/billing/PinAuthorisationStatus;

    invoke-virtual {v2, v0, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lru/cn/api/billing/PinAuthorisationStatus;
    :try_end_0
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v3

    .line 30
    :catch_0
    move-exception v1

    .line 31
    .local v1, "e":Lcom/google/gson/JsonSyntaxException;
    invoke-static {v1}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .line 32
    new-instance v3, Lru/cn/api/BaseAPI$ParseException;

    const-string v4, "JsonSyntaxException"

    invoke-direct {v3, v4}, Lru/cn/api/BaseAPI$ParseException;-><init>(Ljava/lang/String;)V

    throw v3
.end method
