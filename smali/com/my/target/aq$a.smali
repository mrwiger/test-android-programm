.class public interface abstract Lcom/my/target/aq$a;
.super Ljava/lang/Object;
.source "Stat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/aq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "a"
.end annotation


# static fields
.field public static final ERROR:Ljava/lang/String; = "error"

.field public static final dA:Ljava/lang/String; = "playheadViewabilityValue"

.field public static final dB:Ljava/lang/String; = "ovvStat"

.field public static final dC:Ljava/lang/String; = "mrcStat"

.field public static final dD:Ljava/lang/String; = "creativeView"

.field public static final dE:Ljava/lang/String; = "skip"

.field public static final dF:Ljava/lang/String; = "serviceRequested"

.field public static final dG:Ljava/lang/String; = "serviceAnswerEmpty"

.field public static final dn:Ljava/lang/String; = "playbackStarted"

.field public static final do:Ljava/lang/String; = "playbackPaused"

.field public static final dp:Ljava/lang/String; = "playbackResumed"

.field public static final dq:Ljava/lang/String; = "playbackStopped"

.field public static final dr:Ljava/lang/String; = "closedByUser"

.field public static final ds:Ljava/lang/String; = "fullscreenOn"

.field public static final dt:Ljava/lang/String; = "fullscreenOff"

.field public static final du:Ljava/lang/String; = "impression"

.field public static final dv:Ljava/lang/String; = "volumeOn"

.field public static final dw:Ljava/lang/String; = "volumeOff"

.field public static final dx:Ljava/lang/String; = "click"

.field public static final dy:Ljava/lang/String; = "deeplinkClick"

.field public static final dz:Ljava/lang/String; = "playheadReachedValue"
