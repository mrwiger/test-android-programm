.class final enum Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;
.super Ljava/lang/Enum;
.source "Player.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/multiscreen/Player;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "PlayerQueueSubEvents"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

.field public static final enum clear:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

.field public static final enum dequeue:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

.field public static final enum enqueue:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

.field public static final enum fetch:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 110
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    const-string v1, "enqueue"

    invoke-direct {v0, v1, v2}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->enqueue:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    .line 111
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    const-string v1, "dequeue"

    invoke-direct {v0, v1, v3}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->dequeue:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    .line 112
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    const-string v1, "clear"

    invoke-direct {v0, v1, v4}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->clear:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    .line 113
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    const-string v1, "fetch"

    invoke-direct {v0, v1, v5}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->fetch:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    .line 109
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    sget-object v1, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->enqueue:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->dequeue:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->clear:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->fetch:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->$VALUES:[Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 109
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 109
    const-class v0, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    return-object v0
.end method

.method public static values()[Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;
    .locals 1

    .prologue
    .line 109
    sget-object v0, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->$VALUES:[Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v0}, [Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    return-object v0
.end method
