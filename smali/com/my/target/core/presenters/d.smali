.class public final Lcom/my/target/core/presenters/d;
.super Ljava/lang/Object;
.source "InterstitialHtmlPresenter.java"

# interfaces
.implements Lcom/my/target/bt$a;
.implements Lcom/my/target/core/presenters/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/core/presenters/d$a;,
        Lcom/my/target/core/presenters/d$b;
    }
.end annotation


# instance fields
.field private final D:Lcom/my/target/core/presenters/d$b;

.field private final E:Lcom/my/target/bt;

.field private final F:Lcom/my/target/by;

.field private final G:Landroid/widget/RelativeLayout;

.field private H:Lcom/my/target/core/presenters/d$a;

.field private I:J

.field private J:Lcom/my/target/core/presenters/h$a;

.field private K:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, -0x2

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Lcom/my/target/core/presenters/d$b;

    invoke-direct {v0, v4}, Lcom/my/target/core/presenters/d$b;-><init>(B)V

    iput-object v0, p0, Lcom/my/target/core/presenters/d;->D:Lcom/my/target/core/presenters/d$b;

    .line 64
    new-instance v0, Lcom/my/target/bt;

    invoke-direct {v0, p1}, Lcom/my/target/bt;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/core/presenters/d;->E:Lcom/my/target/bt;

    .line 65
    new-instance v0, Lcom/my/target/by;

    invoke-direct {v0, p1}, Lcom/my/target/by;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/core/presenters/d;->F:Lcom/my/target/by;

    .line 66
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/core/presenters/d;->G:Landroid/widget/RelativeLayout;

    .line 68
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->F:Lcom/my/target/by;

    const-string v1, "Close"

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 69
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 72
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 73
    iget-object v1, p0, Lcom/my/target/core/presenters/d;->F:Lcom/my/target/by;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/my/target/by;->setVisibility(I)V

    .line 74
    iget-object v1, p0, Lcom/my/target/core/presenters/d;->F:Lcom/my/target/by;

    invoke-virtual {v1, v0}, Lcom/my/target/by;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 76
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 78
    const/4 v1, 0x1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 80
    iget-object v1, p0, Lcom/my/target/core/presenters/d;->E:Lcom/my/target/bt;

    invoke-virtual {v1, v0}, Lcom/my/target/bt;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 82
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->G:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/my/target/core/presenters/d;->E:Lcom/my/target/bt;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 83
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->F:Lcom/my/target/by;

    invoke-virtual {v0}, Lcom/my/target/by;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->G:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/my/target/core/presenters/d;->F:Lcom/my/target/by;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 87
    :cond_0
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    .line 88
    const/16 v1, 0x1c

    invoke-virtual {v0, v1}, Lcom/my/target/cm;->n(I)I

    move-result v0

    invoke-static {v0}, Lcom/my/target/bq;->i(I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 89
    if-eqz v0, :cond_1

    .line 91
    iget-object v1, p0, Lcom/my/target/core/presenters/d;->F:Lcom/my/target/by;

    invoke-virtual {v1, v0, v4}, Lcom/my/target/by;->b(Landroid/graphics/Bitmap;Z)V

    .line 93
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/my/target/core/presenters/d;)Lcom/my/target/core/presenters/h$a;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->J:Lcom/my/target/core/presenters/h$a;

    return-object v0
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 393
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->E:Lcom/my/target/bt;

    iget-object v1, p0, Lcom/my/target/core/presenters/d;->H:Lcom/my/target/core/presenters/d$a;

    invoke-virtual {v0, v1}, Lcom/my/target/bt;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 394
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/my/target/core/presenters/d;->I:J

    .line 395
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->E:Lcom/my/target/bt;

    iget-object v1, p0, Lcom/my/target/core/presenters/d;->H:Lcom/my/target/core/presenters/d$a;

    invoke-virtual {v0, v1, p1, p2}, Lcom/my/target/bt;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 396
    return-void
.end method

.method static synthetic b(Lcom/my/target/core/presenters/d;)Lcom/my/target/by;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->F:Lcom/my/target/by;

    return-object v0
.end method

.method public static b(Landroid/content/Context;)Lcom/my/target/core/presenters/d;
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/my/target/core/presenters/d;

    invoke-direct {v0, p0}, Lcom/my/target/core/presenters/d;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private s()V
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->J:Lcom/my/target/core/presenters/h$a;

    if-eqz v0, :cond_0

    .line 402
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->J:Lcom/my/target/core/presenters/h$a;

    invoke-interface {v0}, Lcom/my/target/core/presenters/h$a;->g()V

    .line 404
    :cond_0
    return-void
.end method


# virtual methods
.method public final J(Ljava/lang/String;)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 385
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->J:Lcom/my/target/core/presenters/h$a;

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->J:Lcom/my/target/core/presenters/h$a;

    .line 3284
    iget-object v1, p0, Lcom/my/target/core/presenters/d;->G:Landroid/widget/RelativeLayout;

    .line 387
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/my/target/core/presenters/h$a;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 389
    :cond_0
    return-void
.end method

.method public final a(Lcom/my/target/core/presenters/h$a;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/my/target/core/presenters/d;->J:Lcom/my/target/core/presenters/h$a;

    .line 149
    return-void
.end method

.method public final a(Lcom/my/target/dv;Lcom/my/target/core/models/banners/f;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 98
    new-instance v0, Lcom/my/target/core/presenters/d$a;

    invoke-direct {v0, p0, v3}, Lcom/my/target/core/presenters/d$a;-><init>(Lcom/my/target/core/presenters/d;B)V

    iput-object v0, p0, Lcom/my/target/core/presenters/d;->H:Lcom/my/target/core/presenters/d$a;

    .line 100
    invoke-virtual {p1}, Lcom/my/target/dv;->getRawData()Lorg/json/JSONObject;

    move-result-object v0

    .line 101
    invoke-virtual {p1}, Lcom/my/target/dv;->getHtml()Ljava/lang/String;

    move-result-object v1

    .line 102
    if-nez v0, :cond_0

    .line 104
    invoke-direct {p0}, Lcom/my/target/core/presenters/d;->s()V

    .line 143
    :goto_0
    return-void

    .line 107
    :cond_0
    if-nez v1, :cond_1

    .line 109
    invoke-direct {p0}, Lcom/my/target/core/presenters/d;->s()V

    goto :goto_0

    .line 112
    :cond_1
    invoke-virtual {p2}, Lcom/my/target/core/models/banners/f;->getId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/my/target/core/presenters/d;->K:Ljava/lang/String;

    .line 113
    iget-object v2, p0, Lcom/my/target/core/presenters/d;->E:Lcom/my/target/bt;

    invoke-virtual {v2, p0}, Lcom/my/target/bt;->setBannerWebViewListener(Lcom/my/target/bt$a;)V

    .line 114
    iget-object v2, p0, Lcom/my/target/core/presenters/d;->E:Lcom/my/target/bt;

    invoke-virtual {v2, v0, v1}, Lcom/my/target/bt;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 115
    invoke-virtual {p2}, Lcom/my/target/core/models/banners/f;->getCloseIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 116
    if-eqz v0, :cond_2

    .line 118
    iget-object v1, p0, Lcom/my/target/core/presenters/d;->F:Lcom/my/target/by;

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0, v3}, Lcom/my/target/by;->b(Landroid/graphics/Bitmap;Z)V

    .line 120
    :cond_2
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->F:Lcom/my/target/by;

    new-instance v1, Lcom/my/target/core/presenters/d$1;

    invoke-direct {v1, p0}, Lcom/my/target/core/presenters/d$1;-><init>(Lcom/my/target/core/presenters/d;)V

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    invoke-virtual {p2}, Lcom/my/target/core/models/banners/f;->getAllowCloseDelay()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "banner will be allowed to close in "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/f;->getAllowCloseDelay()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " seconds"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 136
    invoke-virtual {p2}, Lcom/my/target/core/models/banners/f;->getAllowCloseDelay()F

    move-result v0

    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v1

    float-to-long v0, v0

    invoke-direct {p0, v0, v1}, Lcom/my/target/core/presenters/d;->a(J)V

    goto :goto_0

    .line 140
    :cond_3
    const-string v0, "banner is allowed to close"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->F:Lcom/my/target/by;

    invoke-virtual {v0, v3}, Lcom/my/target/by;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Lcom/my/target/v;)V
    .locals 6
    .param p1, "event"    # Lcom/my/target/v;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 290
    invoke-interface {p1}, Lcom/my/target/v;->getType()Ljava/lang/String;

    move-result-object v3

    const/4 v0, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 1173
    .end local p1    # "event":Lcom/my/target/v;
    :cond_1
    :goto_1
    :pswitch_0
    return-void

    .line 290
    .restart local p1    # "event":Lcom/my/target/v;
    :sswitch_0
    const-string v4, "onReady"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v4, "onExpand"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v2

    goto :goto_0

    :sswitch_2
    const-string v4, "onCollapse"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v4, "onError"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v4, "onAdError"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v4, "onCloseClick"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string v4, "onComplete"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x6

    goto :goto_0

    :sswitch_7
    const-string v4, "onNoAd"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x7

    goto :goto_0

    :sswitch_8
    const-string v4, "onAdStart"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0x8

    goto :goto_0

    :sswitch_9
    const-string v4, "onAdStop"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0x9

    goto :goto_0

    :sswitch_a
    const-string v4, "onAdPause"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0xa

    goto :goto_0

    :sswitch_b
    const-string v4, "onAdResume"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0xb

    goto :goto_0

    :sswitch_c
    const-string v4, "onAdComplete"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v4, "onAdClick"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v4, "onStat"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v4, "onSizeChange"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string v4, "onRequestNewAds"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v0, 0x10

    goto/16 :goto_0

    .line 293
    :pswitch_1
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->D:Lcom/my/target/core/presenters/d$b;

    invoke-virtual {v0}, Lcom/my/target/core/presenters/d$b;->t()V

    .line 1154
    move-object p1, p0

    iget-object v0, p0, Lcom/my/target/core/presenters/d;->D:Lcom/my/target/core/presenters/d$b;

    invoke-virtual {v0}, Lcom/my/target/core/presenters/d$b;->isReady()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1156
    iget-object v0, p1, Lcom/my/target/core/presenters/d;->D:Lcom/my/target/core/presenters/d$b;

    invoke-virtual {v0}, Lcom/my/target/core/presenters/d$b;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1158
    const-string v0, "already started"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1162
    :cond_2
    iget-object v0, p1, Lcom/my/target/core/presenters/d;->G:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 1165
    :try_start_0
    iget-object v1, p1, Lcom/my/target/core/presenters/d;->E:Lcom/my/target/bt;

    new-instance v3, Lcom/my/target/o;

    const-string v4, "fullscreen"

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5, v0}, Lcom/my/target/o;-><init>(Ljava/lang/String;[Ljava/lang/String;I)V

    invoke-virtual {v1, v3}, Lcom/my/target/bt;->a(Lcom/my/target/m;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1172
    iget-object v0, p1, Lcom/my/target/core/presenters/d;->D:Lcom/my/target/core/presenters/d$b;

    invoke-virtual {v0, v2}, Lcom/my/target/core/presenters/d$b;->setStarted(Z)V

    goto/16 :goto_1

    .line 1169
    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 1177
    :cond_3
    const-string v0, "not ready"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_2
    move-object v0, p1

    .line 302
    check-cast v0, Lcom/my/target/u;

    .line 303
    const-string v1, "JS error"

    .line 304
    invoke-virtual {v0}, Lcom/my/target/u;->j()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 306
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/my/target/u;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 308
    :goto_2
    iget-object v1, p0, Lcom/my/target/core/presenters/d;->E:Lcom/my/target/bt;

    invoke-virtual {v1}, Lcom/my/target/bt;->getUrl()Ljava/lang/String;

    move-result-object v1

    .line 309
    const-string v2, "JS error"

    invoke-static {v2}, Lcom/my/target/az;->y(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/my/target/az;->z(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    .line 310
    invoke-virtual {v0, v1}, Lcom/my/target/az;->A(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/presenters/d;->K:Ljava/lang/String;

    .line 311
    invoke-virtual {v0, v1}, Lcom/my/target/az;->B(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/presenters/d;->E:Lcom/my/target/bt;

    .line 312
    invoke-virtual {v1}, Lcom/my/target/bt;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/az;->e(Landroid/content/Context;)V

    .line 314
    invoke-interface {p1}, Lcom/my/target/v;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onError"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 316
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->D:Lcom/my/target/core/presenters/d$b;

    invoke-virtual {v0}, Lcom/my/target/core/presenters/d$b;->isReady()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 318
    invoke-direct {p0}, Lcom/my/target/core/presenters/d;->s()V

    goto/16 :goto_1

    .line 322
    :cond_4
    invoke-direct {p0}, Lcom/my/target/core/presenters/d;->s()V

    goto/16 :goto_1

    .line 329
    :pswitch_3
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->D:Lcom/my/target/core/presenters/d$b;

    invoke-virtual {v0, v1}, Lcom/my/target/core/presenters/d$b;->setStarted(Z)V

    .line 330
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->D:Lcom/my/target/core/presenters/d$b;

    invoke-virtual {v0, v1}, Lcom/my/target/core/presenters/d$b;->setPaused(Z)V

    .line 331
    invoke-direct {p0}, Lcom/my/target/core/presenters/d;->s()V

    goto/16 :goto_1

    .line 334
    :pswitch_4
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->D:Lcom/my/target/core/presenters/d$b;

    invoke-virtual {v0}, Lcom/my/target/core/presenters/d$b;->isReady()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 336
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->D:Lcom/my/target/core/presenters/d$b;

    invoke-virtual {v0, v1}, Lcom/my/target/core/presenters/d$b;->setStarted(Z)V

    .line 337
    invoke-direct {p0}, Lcom/my/target/core/presenters/d;->s()V

    goto/16 :goto_1

    .line 341
    :cond_5
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->D:Lcom/my/target/core/presenters/d$b;

    invoke-virtual {v0}, Lcom/my/target/core/presenters/d$b;->reset()V

    .line 342
    invoke-direct {p0}, Lcom/my/target/core/presenters/d;->s()V

    goto/16 :goto_1

    .line 346
    :pswitch_5
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->J:Lcom/my/target/core/presenters/h$a;

    if-eqz v0, :cond_1

    .line 348
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->J:Lcom/my/target/core/presenters/h$a;

    .line 1284
    iget-object v1, p0, Lcom/my/target/core/presenters/d;->G:Landroid/widget/RelativeLayout;

    .line 348
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/my/target/core/presenters/h$a;->a(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 360
    :pswitch_6
    check-cast p1, Lcom/my/target/s;

    .line 361
    .end local p1    # "event":Lcom/my/target/v;
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->J:Lcom/my/target/core/presenters/h$a;

    if-eqz v0, :cond_1

    .line 363
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->J:Lcom/my/target/core/presenters/h$a;

    invoke-virtual {p1}, Lcom/my/target/s;->getUrl()Ljava/lang/String;

    move-result-object v1

    .line 2284
    iget-object v2, p0, Lcom/my/target/core/presenters/d;->G:Landroid/widget/RelativeLayout;

    .line 363
    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/my/target/core/presenters/h$a;->a(Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_1

    .line 367
    .restart local p1    # "event":Lcom/my/target/v;
    :pswitch_7
    check-cast p1, Lcom/my/target/z;

    .end local p1    # "event":Lcom/my/target/v;
    invoke-virtual {p1}, Lcom/my/target/z;->k()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/presenters/d;->G:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/my/target/cl;->b(Ljava/util/List;Landroid/content/Context;)V

    goto/16 :goto_1

    .restart local p1    # "event":Lcom/my/target/v;
    :cond_6
    move-object v0, v1

    goto/16 :goto_2

    .line 290
    :sswitch_data_0
    .sparse-switch
        -0x7ea0abc8 -> :sswitch_6
        -0x50755897 -> :sswitch_3
        -0x4fc450fc -> :sswitch_0
        -0x3c62dbbd -> :sswitch_7
        -0x3c607f2d -> :sswitch_e
        0x969e846 -> :sswitch_d
        0x988f4c6 -> :sswitch_4
        0xa1c48b4 -> :sswitch_a
        0xa4ee720 -> :sswitch_8
        0x21cc2dbb -> :sswitch_c
        0x2c8d7a50 -> :sswitch_f
        0x3a239584 -> :sswitch_9
        0x3d0dfd2f -> :sswitch_b
        0x421db559 -> :sswitch_1
        0x4aae51af -> :sswitch_5
        0x7d5bcec0 -> :sswitch_10
        0x7f6d46ac -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final destroy()V
    .locals 2

    .prologue
    .line 277
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->G:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/my/target/core/presenters/d;->E:Lcom/my/target/bt;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 278
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->E:Lcom/my/target/bt;

    invoke-virtual {v0}, Lcom/my/target/bt;->destroy()V

    .line 279
    return-void
.end method

.method public final o()Landroid/view/View;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->G:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public final onError(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 379
    invoke-direct {p0}, Lcom/my/target/core/presenters/d;->s()V

    .line 380
    return-void
.end method

.method public final pause()V
    .locals 3

    .prologue
    .line 207
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->D:Lcom/my/target/core/presenters/d$b;

    invoke-virtual {v0}, Lcom/my/target/core/presenters/d$b;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 209
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->D:Lcom/my/target/core/presenters/d$b;

    invoke-virtual {v0}, Lcom/my/target/core/presenters/d$b;->isPaused()Z

    move-result v0

    if-nez v0, :cond_0

    .line 213
    :try_start_0
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->E:Lcom/my/target/bt;

    new-instance v1, Lcom/my/target/l;

    const-string v2, "pause"

    invoke-direct {v1, v2}, Lcom/my/target/l;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/my/target/bt;->a(Lcom/my/target/m;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->D:Lcom/my/target/core/presenters/d$b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/my/target/core/presenters/d$b;->setPaused(Z)V

    .line 231
    :goto_0
    return-void

    .line 217
    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 224
    :cond_0
    const-string v0, "already paused"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 229
    :cond_1
    const-string v0, "not started"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final resume()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 236
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->D:Lcom/my/target/core/presenters/d$b;

    invoke-virtual {v0}, Lcom/my/target/core/presenters/d$b;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 238
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->D:Lcom/my/target/core/presenters/d$b;

    invoke-virtual {v0}, Lcom/my/target/core/presenters/d$b;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 242
    :try_start_0
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->E:Lcom/my/target/bt;

    new-instance v1, Lcom/my/target/l;

    const-string v2, "resume"

    invoke-direct {v1, v2}, Lcom/my/target/l;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/my/target/bt;->a(Lcom/my/target/m;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 249
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->D:Lcom/my/target/core/presenters/d$b;

    invoke-virtual {v0, v6}, Lcom/my/target/core/presenters/d$b;->setPaused(Z)V

    .line 260
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 261
    iget-wide v2, p0, Lcom/my/target/core/presenters/d;->I:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 263
    iget-wide v2, p0, Lcom/my/target/core/presenters/d;->I:J

    cmp-long v2, v0, v2

    if-ltz v2, :cond_3

    .line 265
    iget-object v0, p0, Lcom/my/target/core/presenters/d;->F:Lcom/my/target/by;

    invoke-virtual {v0, v6}, Lcom/my/target/by;->setVisibility(I)V

    .line 272
    :cond_0
    :goto_1
    return-void

    .line 246
    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 253
    :cond_1
    const-string v0, "already started"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 258
    :cond_2
    const-string v0, "not started"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 269
    :cond_3
    iget-wide v2, p0, Lcom/my/target/core/presenters/d;->I:J

    sub-long v0, v2, v0

    invoke-direct {p0, v0, v1}, Lcom/my/target/core/presenters/d;->a(J)V

    goto :goto_1
.end method
