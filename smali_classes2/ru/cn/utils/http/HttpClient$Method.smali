.class public final enum Lru/cn/utils/http/HttpClient$Method;
.super Ljava/lang/Enum;
.source "HttpClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/utils/http/HttpClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Method"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/utils/http/HttpClient$Method;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/utils/http/HttpClient$Method;

.field public static final enum GET:Lru/cn/utils/http/HttpClient$Method;

.field public static final enum HEAD:Lru/cn/utils/http/HttpClient$Method;

.field public static final enum POST:Lru/cn/utils/http/HttpClient$Method;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    new-instance v0, Lru/cn/utils/http/HttpClient$Method;

    const-string v1, "GET"

    invoke-direct {v0, v1, v2}, Lru/cn/utils/http/HttpClient$Method;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/utils/http/HttpClient$Method;->GET:Lru/cn/utils/http/HttpClient$Method;

    new-instance v0, Lru/cn/utils/http/HttpClient$Method;

    const-string v1, "POST"

    invoke-direct {v0, v1, v3}, Lru/cn/utils/http/HttpClient$Method;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/utils/http/HttpClient$Method;->POST:Lru/cn/utils/http/HttpClient$Method;

    new-instance v0, Lru/cn/utils/http/HttpClient$Method;

    const-string v1, "HEAD"

    invoke-direct {v0, v1, v4}, Lru/cn/utils/http/HttpClient$Method;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/utils/http/HttpClient$Method;->HEAD:Lru/cn/utils/http/HttpClient$Method;

    .line 39
    const/4 v0, 0x3

    new-array v0, v0, [Lru/cn/utils/http/HttpClient$Method;

    sget-object v1, Lru/cn/utils/http/HttpClient$Method;->GET:Lru/cn/utils/http/HttpClient$Method;

    aput-object v1, v0, v2

    sget-object v1, Lru/cn/utils/http/HttpClient$Method;->POST:Lru/cn/utils/http/HttpClient$Method;

    aput-object v1, v0, v3

    sget-object v1, Lru/cn/utils/http/HttpClient$Method;->HEAD:Lru/cn/utils/http/HttpClient$Method;

    aput-object v1, v0, v4

    sput-object v0, Lru/cn/utils/http/HttpClient$Method;->$VALUES:[Lru/cn/utils/http/HttpClient$Method;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/utils/http/HttpClient$Method;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 39
    const-class v0, Lru/cn/utils/http/HttpClient$Method;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/utils/http/HttpClient$Method;

    return-object v0
.end method

.method public static values()[Lru/cn/utils/http/HttpClient$Method;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lru/cn/utils/http/HttpClient$Method;->$VALUES:[Lru/cn/utils/http/HttpClient$Method;

    invoke-virtual {v0}, [Lru/cn/utils/http/HttpClient$Method;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/utils/http/HttpClient$Method;

    return-object v0
.end method
