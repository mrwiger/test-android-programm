.class public Lcom/yandex/mobile/ads/nativeads/ac$a;
.super Lcom/yandex/mobile/ads/nativeads/ac;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/mobile/ads/nativeads/ac;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/yandex/mobile/ads/nativeads/ac",
        "<",
        "Landroid/widget/ImageView;",
        "Lcom/yandex/mobile/ads/nativeads/e;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/yandex/mobile/ads/nativeads/d;


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;Lcom/yandex/mobile/ads/nativeads/d;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/yandex/mobile/ads/nativeads/ac;-><init>(Landroid/view/View;)V

    .line 61
    iput-object p2, p0, Lcom/yandex/mobile/ads/nativeads/ac$a;->a:Lcom/yandex/mobile/ads/nativeads/d;

    .line 62
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Landroid/view/View;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 55
    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p0, p1}, Lcom/yandex/mobile/ads/nativeads/ac$a;->a(Landroid/widget/ImageView;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/widget/ImageView;)Ljava/lang/ref/WeakReference;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ImageView;",
            ")",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 67
    invoke-super {p0, p1}, Lcom/yandex/mobile/ads/nativeads/ac;->a(Landroid/view/View;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Landroid/view/View;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 55
    check-cast p1, Landroid/widget/ImageView;

    check-cast p2, Lcom/yandex/mobile/ads/nativeads/e;

    invoke-virtual {p0, p1, p2}, Lcom/yandex/mobile/ads/nativeads/ac$a;->a(Landroid/widget/ImageView;Lcom/yandex/mobile/ads/nativeads/e;)V

    return-void
.end method

.method public a(Landroid/widget/ImageView;Lcom/yandex/mobile/ads/nativeads/e;)V
    .locals 2

    .prologue
    .line 72
    invoke-virtual {p2}, Lcom/yandex/mobile/ads/nativeads/e;->a()Ljava/lang/String;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/ac$a;->a:Lcom/yandex/mobile/ads/nativeads/d;

    invoke-interface {v1, v0}, Lcom/yandex/mobile/ads/nativeads/d;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 74
    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 77
    :cond_0
    return-void
.end method

.method public bridge synthetic b(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 55
    check-cast p1, Landroid/widget/ImageView;

    check-cast p2, Lcom/yandex/mobile/ads/nativeads/e;

    invoke-virtual {p0, p1, p2}, Lcom/yandex/mobile/ads/nativeads/ac$a;->b(Landroid/widget/ImageView;Lcom/yandex/mobile/ads/nativeads/e;)Z

    move-result v0

    return v0
.end method

.method public b(Landroid/widget/ImageView;Lcom/yandex/mobile/ads/nativeads/e;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 82
    :try_start_0
    invoke-virtual {p2}, Lcom/yandex/mobile/ads/nativeads/e;->a()Ljava/lang/String;

    move-result-object v0

    .line 83
    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/ac$a;->a:Lcom/yandex/mobile/ads/nativeads/d;

    invoke-interface {v2, v0}, Lcom/yandex/mobile/ads/nativeads/d;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 85
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 86
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 88
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 86
    goto :goto_0

    .line 88
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0
.end method
