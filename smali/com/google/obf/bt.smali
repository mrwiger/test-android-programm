.class abstract Lcom/google/obf/bt;
.super Ljava/lang/Object;
.source "IMASDK"


# instance fields
.field protected final a:Lcom/google/obf/dw;

.field protected final b:Lcom/google/obf/bq;

.field protected c:Lcom/google/obf/ar;

.field protected d:Lcom/google/obf/al;


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/google/obf/dw;

    const v1, 0xfe01

    new-array v1, v1, [B

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/obf/dw;-><init>([BI)V

    iput-object v0, p0, Lcom/google/obf/bt;->a:Lcom/google/obf/dw;

    .line 3
    new-instance v0, Lcom/google/obf/bq;

    invoke-direct {v0}, Lcom/google/obf/bq;-><init>()V

    iput-object v0, p0, Lcom/google/obf/bt;->b:Lcom/google/obf/bq;

    return-void
.end method


# virtual methods
.method abstract a(Lcom/google/obf/ak;Lcom/google/obf/ao;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation
.end method

.method a(Lcom/google/obf/al;Lcom/google/obf/ar;)V
    .locals 0

    .prologue
    .line 4
    iput-object p1, p0, Lcom/google/obf/bt;->d:Lcom/google/obf/al;

    .line 5
    iput-object p2, p0, Lcom/google/obf/bt;->c:Lcom/google/obf/ar;

    .line 6
    return-void
.end method

.method b()V
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lcom/google/obf/bt;->b:Lcom/google/obf/bq;

    invoke-virtual {v0}, Lcom/google/obf/bq;->a()V

    .line 8
    iget-object v0, p0, Lcom/google/obf/bt;->a:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->a()V

    .line 9
    return-void
.end method
