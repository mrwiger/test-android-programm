.class public abstract Lcom/google/obf/js;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<TT;>;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/Comparator;)Lcom/google/obf/js;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<TT;>;)",
            "Lcom/google/obf/js",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1
    instance-of v0, p0, Lcom/google/obf/js;

    if-eqz v0, :cond_0

    .line 2
    check-cast p0, Lcom/google/obf/js;

    .line 4
    :goto_0
    return-object p0

    .line 3
    :cond_0
    new-instance v0, Lcom/google/obf/jh;

    invoke-direct {v0, p0}, Lcom/google/obf/jh;-><init>(Ljava/util/Comparator;)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/obf/iw;)Lcom/google/obf/js;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<F:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/obf/iw",
            "<TF;+TT;>;)",
            "Lcom/google/obf/js",
            "<TF;>;"
        }
    .end annotation

    .prologue
    .line 6
    new-instance v0, Lcom/google/obf/je;

    invoke-direct {v0, p1, p0}, Lcom/google/obf/je;-><init>(Lcom/google/obf/iw;Lcom/google/obf/js;)V

    return-object v0
.end method

.method public abstract compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)I"
        }
    .end annotation
.end method
