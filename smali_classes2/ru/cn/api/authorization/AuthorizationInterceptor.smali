.class public Lru/cn/api/authorization/AuthorizationInterceptor;
.super Ljava/lang/Object;
.source "AuthorizationInterceptor.java"

# interfaces
.implements Lokhttp3/Authenticator;
.implements Lokhttp3/Interceptor;


# instance fields
.field private final authApi:Lru/cn/api/authorization/retrofit/AuthApi;

.field private final context:Landroid/content/Context;

.field private final contractorId:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lru/cn/api/authorization/retrofit/AuthApi;J)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "authApi"    # Lru/cn/api/authorization/retrofit/AuthApi;
    .param p3, "contractorId"    # J

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lru/cn/api/authorization/AuthorizationInterceptor;->context:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lru/cn/api/authorization/AuthorizationInterceptor;->authApi:Lru/cn/api/authorization/retrofit/AuthApi;

    .line 29
    iput-wide p3, p0, Lru/cn/api/authorization/AuthorizationInterceptor;->contractorId:J

    .line 30
    return-void
.end method

.method private static getAuthorizedRequest(Lokhttp3/Request;Ljava/lang/String;)Lokhttp3/Request;
    .locals 3
    .param p0, "originalRequest"    # Lokhttp3/Request;
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 73
    invoke-virtual {p0}, Lokhttp3/Request;->url()Lokhttp3/HttpUrl;

    move-result-object v1

    .line 74
    invoke-virtual {v1}, Lokhttp3/HttpUrl;->newBuilder()Lokhttp3/HttpUrl$Builder;

    move-result-object v1

    const-string v2, "access_token"

    .line 75
    invoke-virtual {v1, v2, p1}, Lokhttp3/HttpUrl$Builder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/HttpUrl$Builder;

    move-result-object v1

    .line 76
    invoke-virtual {v1}, Lokhttp3/HttpUrl$Builder;->build()Lokhttp3/HttpUrl;

    move-result-object v0

    .line 78
    .local v0, "authorizedUrl":Lokhttp3/HttpUrl;
    invoke-virtual {p0}, Lokhttp3/Request;->newBuilder()Lokhttp3/Request$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lokhttp3/Request$Builder;->url(Lokhttp3/HttpUrl;)Lokhttp3/Request$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v1

    return-object v1
.end method

.method private retryCount(Lokhttp3/Response;)I
    .locals 2
    .param p1, "response"    # Lokhttp3/Response;

    .prologue
    .line 82
    if-nez p1, :cond_1

    .line 83
    const/4 v0, 0x0

    .line 91
    :cond_0
    return v0

    .line 85
    :cond_1
    const/4 v0, 0x0

    .line 86
    .local v0, "retryCount":I
    :goto_0
    invoke-virtual {p1}, Lokhttp3/Response;->priorResponse()Lokhttp3/Response;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 87
    invoke-virtual {p1}, Lokhttp3/Response;->priorResponse()Lokhttp3/Response;

    move-result-object p1

    .line 88
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public authenticate(Lokhttp3/Route;Lokhttp3/Response;)Lokhttp3/Request;
    .locals 8
    .param p1, "route"    # Lokhttp3/Route;
    .param p2, "response"    # Lokhttp3/Response;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 34
    invoke-direct {p0, p2}, Lru/cn/api/authorization/AuthorizationInterceptor;->retryCount(Lokhttp3/Response;)I

    move-result v4

    if-lez v4, :cond_1

    .line 52
    :cond_0
    :goto_0
    return-object v3

    .line 37
    :cond_1
    invoke-virtual {p2}, Lokhttp3/Response;->code()I

    move-result v4

    const/16 v5, 0x191

    if-ne v4, v5, :cond_0

    .line 40
    const-string v4, "WWW-Authenticate"

    invoke-virtual {p2, v4}, Lokhttp3/Response;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 41
    .local v1, "headerValue":Ljava/lang/String;
    const-string v4, "Bearer"

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 44
    iget-object v4, p0, Lru/cn/api/authorization/AuthorizationInterceptor;->context:Landroid/content/Context;

    iget-object v5, p0, Lru/cn/api/authorization/AuthorizationInterceptor;->authApi:Lru/cn/api/authorization/retrofit/AuthApi;

    iget-wide v6, p0, Lru/cn/api/authorization/AuthorizationInterceptor;->contractorId:J

    invoke-static {v4, v5, v6, v7}, Lru/cn/api/authorization/Authorization;->getAccount(Landroid/content/Context;Lru/cn/api/authorization/retrofit/AuthApi;J)Lru/cn/api/authorization/Account;

    move-result-object v0

    .line 45
    .local v0, "account":Lru/cn/api/authorization/Account;
    if-eqz v0, :cond_0

    .line 48
    iget-object v4, p0, Lru/cn/api/authorization/AuthorizationInterceptor;->context:Landroid/content/Context;

    iget-object v5, p0, Lru/cn/api/authorization/AuthorizationInterceptor;->authApi:Lru/cn/api/authorization/retrofit/AuthApi;

    iget-wide v6, p0, Lru/cn/api/authorization/AuthorizationInterceptor;->contractorId:J

    invoke-static {v4, v5, v0, v6, v7}, Lru/cn/api/authorization/Authorization;->renewToken(Landroid/content/Context;Lru/cn/api/authorization/retrofit/AuthApi;Lru/cn/api/authorization/Account;J)Ljava/lang/String;

    move-result-object v2

    .line 49
    .local v2, "updatedToken":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 52
    invoke-virtual {p2}, Lokhttp3/Response;->request()Lokhttp3/Request;

    move-result-object v3

    invoke-static {v3, v2}, Lru/cn/api/authorization/AuthorizationInterceptor;->getAuthorizedRequest(Lokhttp3/Request;Ljava/lang/String;)Lokhttp3/Request;

    move-result-object v3

    goto :goto_0
.end method

.method public intercept(Lokhttp3/Interceptor$Chain;)Lokhttp3/Response;
    .locals 8
    .param p1, "chain"    # Lokhttp3/Interceptor$Chain;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 57
    iget-object v4, p0, Lru/cn/api/authorization/AuthorizationInterceptor;->context:Landroid/content/Context;

    iget-object v5, p0, Lru/cn/api/authorization/AuthorizationInterceptor;->authApi:Lru/cn/api/authorization/retrofit/AuthApi;

    iget-wide v6, p0, Lru/cn/api/authorization/AuthorizationInterceptor;->contractorId:J

    invoke-static {v4, v5, v6, v7}, Lru/cn/api/authorization/Authorization;->getAccount(Landroid/content/Context;Lru/cn/api/authorization/retrofit/AuthApi;J)Lru/cn/api/authorization/Account;

    move-result-object v0

    .line 58
    .local v0, "account":Lru/cn/api/authorization/Account;
    if-nez v0, :cond_0

    .line 69
    :goto_0
    return-object v3

    .line 63
    :cond_0
    invoke-virtual {v0}, Lru/cn/api/authorization/Account;->getToken()Lru/cn/api/authorization/Account$Token;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Lru/cn/api/authorization/Account;->getToken()Lru/cn/api/authorization/Account$Token;

    move-result-object v3

    iget-object v2, v3, Lru/cn/api/authorization/Account$Token;->accessToken:Ljava/lang/String;

    .line 64
    .local v2, "token":Ljava/lang/String;
    :goto_1
    if-eqz v2, :cond_2

    .line 65
    invoke-interface {p1}, Lokhttp3/Interceptor$Chain;->request()Lokhttp3/Request;

    move-result-object v3

    invoke-static {v3, v2}, Lru/cn/api/authorization/AuthorizationInterceptor;->getAuthorizedRequest(Lokhttp3/Request;Ljava/lang/String;)Lokhttp3/Request;

    move-result-object v1

    .line 66
    .local v1, "authorizedRequest":Lokhttp3/Request;
    invoke-interface {p1, v1}, Lokhttp3/Interceptor$Chain;->proceed(Lokhttp3/Request;)Lokhttp3/Response;

    move-result-object v3

    goto :goto_0

    .end local v1    # "authorizedRequest":Lokhttp3/Request;
    .end local v2    # "token":Ljava/lang/String;
    :cond_1
    move-object v2, v3

    .line 63
    goto :goto_1

    .line 69
    .restart local v2    # "token":Ljava/lang/String;
    :cond_2
    invoke-interface {p1}, Lokhttp3/Interceptor$Chain;->request()Lokhttp3/Request;

    move-result-object v3

    invoke-interface {p1, v3}, Lokhttp3/Interceptor$Chain;->proceed(Lokhttp3/Request;)Lokhttp3/Response;

    move-result-object v3

    goto :goto_0
.end method
