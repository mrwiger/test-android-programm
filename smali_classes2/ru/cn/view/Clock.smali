.class public Lru/cn/view/Clock;
.super Lru/cn/view/CustomFontTextView;
.source "Clock.java"


# instance fields
.field private isKidsMode:Z

.field private mCalendar:Ljava/util/Calendar;

.field private mHandler:Landroid/os/Handler;

.field private mOffset:I

.field private mPaused:Z

.field private mStartTime:Ljava/util/Calendar;

.field private mTicker:Ljava/lang/Runnable;

.field private mTickerStopped:Z

.field private mTimerMode:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 44
    invoke-direct {p0, p1, p2}, Lru/cn/view/CustomFontTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lru/cn/view/Clock;->mCalendar:Ljava/util/Calendar;

    .line 29
    iput-boolean v1, p0, Lru/cn/view/Clock;->mTickerStopped:Z

    .line 31
    iput-boolean v1, p0, Lru/cn/view/Clock;->isKidsMode:Z

    .line 33
    iput-boolean v1, p0, Lru/cn/view/Clock;->mTimerMode:Z

    .line 34
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lru/cn/view/Clock;->mStartTime:Ljava/util/Calendar;

    .line 36
    iput v1, p0, Lru/cn/view/Clock;->mOffset:I

    .line 37
    iput-boolean v1, p0, Lru/cn/view/Clock;->mPaused:Z

    .line 45
    invoke-static {p1}, Lru/cn/domain/KidsObject;->isKidsMode(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lru/cn/view/Clock;->isKidsMode:Z

    .line 46
    return-void
.end method

.method static synthetic access$000(Lru/cn/view/Clock;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/view/Clock;

    .prologue
    .line 18
    iget-boolean v0, p0, Lru/cn/view/Clock;->mTickerStopped:Z

    return v0
.end method

.method static synthetic access$100(Lru/cn/view/Clock;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/view/Clock;

    .prologue
    .line 18
    invoke-direct {p0}, Lru/cn/view/Clock;->update()V

    return-void
.end method

.method static synthetic access$200(Lru/cn/view/Clock;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lru/cn/view/Clock;

    .prologue
    .line 18
    iget-object v0, p0, Lru/cn/view/Clock;->mTicker:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/view/Clock;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lru/cn/view/Clock;

    .prologue
    .line 18
    iget-object v0, p0, Lru/cn/view/Clock;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private update()V
    .locals 11

    .prologue
    const/16 v10, 0x8

    .line 102
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 106
    .local v0, "currentTimeMils":J
    iget-boolean v7, p0, Lru/cn/view/Clock;->mTimerMode:Z

    if-nez v7, :cond_2

    .line 107
    iget-boolean v7, p0, Lru/cn/view/Clock;->mPaused:Z

    if-nez v7, :cond_0

    .line 108
    iget-object v7, p0, Lru/cn/view/Clock;->mCalendar:Ljava/util/Calendar;

    iget v8, p0, Lru/cn/view/Clock;->mOffset:I

    mul-int/lit16 v8, v8, 0x3e8

    int-to-long v8, v8

    add-long/2addr v8, v0

    invoke-virtual {v7, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 110
    :cond_0
    const-string v2, "kk:mm:ss"

    .line 125
    .local v2, "format":Ljava/lang/String;
    :goto_0
    iget-object v7, p0, Lru/cn/view/Clock;->mCalendar:Ljava/util/Calendar;

    invoke-static {v2, v7}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 126
    .local v6, "string":Ljava/lang/String;
    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, v6}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 127
    .local v3, "ss":Landroid/text/SpannableString;
    iget-boolean v7, p0, Lru/cn/view/Clock;->isKidsMode:Z

    if-eqz v7, :cond_1

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v7, v10, :cond_1

    .line 128
    new-instance v7, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lru/cn/view/Clock;->getContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0f01c6

    invoke-direct {v7, v8, v9}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    const/4 v8, 0x5

    const/16 v9, 0x21

    invoke-virtual {v3, v7, v8, v10, v9}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 131
    :cond_1
    invoke-virtual {p0, v3}, Lru/cn/view/Clock;->setText(Ljava/lang/CharSequence;)V

    .line 132
    return-void

    .line 112
    .end local v2    # "format":Ljava/lang/String;
    .end local v3    # "ss":Landroid/text/SpannableString;
    .end local v6    # "string":Ljava/lang/String;
    :cond_2
    iget-boolean v7, p0, Lru/cn/view/Clock;->mPaused:Z

    if-eqz v7, :cond_3

    .line 113
    iget-object v7, p0, Lru/cn/view/Clock;->mStartTime:Ljava/util/Calendar;

    iget v8, p0, Lru/cn/view/Clock;->mOffset:I

    mul-int/lit16 v8, v8, 0x3e8

    int-to-long v8, v8

    add-long/2addr v8, v0

    invoke-virtual {v7, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 115
    :cond_3
    iget-object v7, p0, Lru/cn/view/Clock;->mStartTime:Ljava/util/Calendar;

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    sub-long v4, v0, v8

    .line 116
    .local v4, "ms":J
    iget-object v7, p0, Lru/cn/view/Clock;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v7, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 117
    iget-object v7, p0, Lru/cn/view/Clock;->mCalendar:Ljava/util/Calendar;

    const-string v8, "UTC"

    invoke-static {v8}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 119
    const-wide/32 v8, 0x36ee80

    cmp-long v7, v4, v8

    if-ltz v7, :cond_4

    .line 120
    const-string v2, "-kk:mm:ss"

    .restart local v2    # "format":Ljava/lang/String;
    goto :goto_0

    .line 122
    .end local v2    # "format":Ljava/lang/String;
    :cond_4
    const-string v2, "-mm:ss"

    .restart local v2    # "format":Ljava/lang/String;
    goto :goto_0
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/view/Clock;->mTickerStopped:Z

    .line 79
    invoke-super {p0}, Lru/cn/view/CustomFontTextView;->onAttachedToWindow()V

    .line 80
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lru/cn/view/Clock;->mHandler:Landroid/os/Handler;

    .line 85
    new-instance v0, Lru/cn/view/Clock$1;

    invoke-direct {v0, p0}, Lru/cn/view/Clock$1;-><init>(Lru/cn/view/Clock;)V

    iput-object v0, p0, Lru/cn/view/Clock;->mTicker:Ljava/lang/Runnable;

    .line 98
    iget-object v0, p0, Lru/cn/view/Clock;->mTicker:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 99
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 136
    invoke-super {p0}, Lru/cn/view/CustomFontTextView;->onDetachedFromWindow()V

    .line 137
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/view/Clock;->mTickerStopped:Z

    .line 138
    return-void
.end method

.method public setOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 49
    iput p1, p0, Lru/cn/view/Clock;->mOffset:I

    .line 50
    return-void
.end method

.method public setTimerMode(I)V
    .locals 6
    .param p1, "start"    # I

    .prologue
    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/view/Clock;->mTimerMode:Z

    .line 66
    iget-object v0, p0, Lru/cn/view/Clock;->mStartTime:Ljava/util/Calendar;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    mul-int/lit16 v1, p1, 0x3e8

    int-to-long v4, v1

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 67
    neg-int v0, p1

    iput v0, p0, Lru/cn/view/Clock;->mOffset:I

    .line 68
    invoke-direct {p0}, Lru/cn/view/Clock;->update()V

    .line 69
    return-void
.end method
