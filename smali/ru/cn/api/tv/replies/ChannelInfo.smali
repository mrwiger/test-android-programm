.class public Lru/cn/api/tv/replies/ChannelInfo;
.super Ljava/lang/Object;
.source "ChannelInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/api/tv/replies/ChannelInfo$Category;,
        Lru/cn/api/tv/replies/ChannelInfo$CategoryType;
    }
.end annotation


# instance fields
.field public ageRestriction:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ageRestriction"
    .end annotation
.end field

.field public categories:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/tv/replies/ChannelInfo$Category;",
            ">;"
        }
    .end annotation
.end field

.field public channelId:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "channelId"
    .end annotation
.end field

.field public currentTelecast:Lru/cn/api/tv/replies/Telecast;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "currentTelecast"
    .end annotation
.end field

.field public hasSchedule:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "hasSchedule"
    .end annotation
.end field

.field public logo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "logoURL"
    .end annotation
.end field

.field public title:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "title"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
