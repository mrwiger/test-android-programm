.class final Lcom/my/target/dj$b;
.super Ljava/lang/Object;
.source "StandardNativeHostFlipView.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/dj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic aB:Lcom/my/target/dj;


# direct methods
.method private constructor <init>(Lcom/my/target/dj;)V
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Lcom/my/target/dj$b;->aB:Lcom/my/target/dj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/my/target/dj;B)V
    .locals 0

    .prologue
    .line 214
    invoke-direct {p0, p1}, Lcom/my/target/dj$b;-><init>(Lcom/my/target/dj;)V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/my/target/dj$b;->aB:Lcom/my/target/dj;

    iget-object v1, p0, Lcom/my/target/dj$b;->aB:Lcom/my/target/dj;

    invoke-virtual {v1}, Lcom/my/target/dj;->getCurrentFlipper()Landroid/widget/ViewFlipper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v1

    invoke-static {v0, v1}, Lcom/my/target/dj;->a(Lcom/my/target/dj;I)I

    .line 226
    iget-object v0, p0, Lcom/my/target/dj$b;->aB:Lcom/my/target/dj;

    invoke-static {v0}, Lcom/my/target/dj;->a(Lcom/my/target/dj;)Lcom/my/target/dj$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/my/target/dj$b;->aB:Lcom/my/target/dj;

    invoke-static {v0}, Lcom/my/target/dj;->a(Lcom/my/target/dj;)Lcom/my/target/dj$a;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/dj$b;->aB:Lcom/my/target/dj;

    invoke-static {v1}, Lcom/my/target/dj;->b(Lcom/my/target/dj;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/my/target/dj$a;->j(I)V

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/my/target/dj$b;->aB:Lcom/my/target/dj;

    invoke-static {v0}, Lcom/my/target/dj;->c(Lcom/my/target/dj;)Z

    .line 231
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 237
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 220
    return-void
.end method
