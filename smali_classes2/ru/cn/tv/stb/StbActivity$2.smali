.class Lru/cn/tv/stb/StbActivity$2;
.super Ljava/lang/Object;
.source "StbActivity.java"

# interfaces
.implements Lru/cn/tv/player/controller/PlayerController$CompletionBehaviour;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/stb/StbActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/stb/StbActivity;


# direct methods
.method constructor <init>(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 229
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity$2;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Lru/cn/api/tv/replies/Telecast;)V
    .locals 4
    .param p1, "telecast"    # Lru/cn/api/tv/replies/Telecast;

    .prologue
    .line 233
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$2;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/StbPlayerController;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/player/controller/StbPlayerController;->hide()V

    .line 234
    if-eqz p1, :cond_0

    .line 235
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$2;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$200(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/StbNextTelecastView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lru/cn/tv/player/controller/StbNextTelecastView;->show(Lru/cn/api/tv/replies/Telecast;)V

    .line 244
    :goto_0
    return-void

    .line 239
    :cond_0
    invoke-static {}, Lru/cn/domain/statistics/inetra/InetraTracker;->getViewFrom()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    .line 238
    invoke-static {v0, v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->setSessionParams(II)V

    .line 242
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$2;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$000(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/SimplePlayerFragment;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$2;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$000(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/SimplePlayerFragment;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/tv/player/SimplePlayerFragment;->getChannelId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lru/cn/tv/player/SimplePlayerFragment;->playChannel(J)V

    goto :goto_0
.end method
