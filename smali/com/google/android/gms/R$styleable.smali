.class public final Lcom/google/android/gms/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final AdsAttrs:[I

.field public static final AdsAttrs_adSize:I = 0x0

.field public static final AdsAttrs_adSizes:I = 0x1

.field public static final AdsAttrs_adUnitId:I = 0x2

.field public static final CastExpandedController:[I

.field public static final CastExpandedController_castButtonColor:I = 0x0

.field public static final CastExpandedController_castClosedCaptionsButtonDrawable:I = 0x1

.field public static final CastExpandedController_castControlButtons:I = 0x2

.field public static final CastExpandedController_castForward30ButtonDrawable:I = 0x3

.field public static final CastExpandedController_castMuteToggleButtonDrawable:I = 0x4

.field public static final CastExpandedController_castPauseButtonDrawable:I = 0x5

.field public static final CastExpandedController_castPlayButtonDrawable:I = 0x6

.field public static final CastExpandedController_castRewind30ButtonDrawable:I = 0x7

.field public static final CastExpandedController_castSeekBarProgressDrawable:I = 0x8

.field public static final CastExpandedController_castSeekBarThumbDrawable:I = 0x9

.field public static final CastExpandedController_castSkipNextButtonDrawable:I = 0xa

.field public static final CastExpandedController_castSkipPreviousButtonDrawable:I = 0xb

.field public static final CastExpandedController_castStopButtonDrawable:I = 0xc

.field public static final CastIntroOverlay:[I

.field public static final CastIntroOverlay_castBackgroundColor:I = 0x0

.field public static final CastIntroOverlay_castButtonBackgroundColor:I = 0x1

.field public static final CastIntroOverlay_castButtonText:I = 0x2

.field public static final CastIntroOverlay_castButtonTextAppearance:I = 0x3

.field public static final CastIntroOverlay_castFocusRadius:I = 0x4

.field public static final CastIntroOverlay_castTitleTextAppearance:I = 0x5

.field public static final CastMiniController:[I

.field public static final CastMiniController_castBackground:I = 0x0

.field public static final CastMiniController_castButtonColor:I = 0x1

.field public static final CastMiniController_castClosedCaptionsButtonDrawable:I = 0x2

.field public static final CastMiniController_castControlButtons:I = 0x3

.field public static final CastMiniController_castForward30ButtonDrawable:I = 0x4

.field public static final CastMiniController_castLargePauseButtonDrawable:I = 0x5

.field public static final CastMiniController_castLargePlayButtonDrawable:I = 0x6

.field public static final CastMiniController_castLargeStopButtonDrawable:I = 0x7

.field public static final CastMiniController_castMuteToggleButtonDrawable:I = 0x8

.field public static final CastMiniController_castPauseButtonDrawable:I = 0x9

.field public static final CastMiniController_castPlayButtonDrawable:I = 0xa

.field public static final CastMiniController_castProgressBarColor:I = 0xb

.field public static final CastMiniController_castRewind30ButtonDrawable:I = 0xc

.field public static final CastMiniController_castShowImageThumbnail:I = 0xd

.field public static final CastMiniController_castSkipNextButtonDrawable:I = 0xe

.field public static final CastMiniController_castSkipPreviousButtonDrawable:I = 0xf

.field public static final CastMiniController_castStopButtonDrawable:I = 0x10

.field public static final CastMiniController_castSubtitleTextAppearance:I = 0x11

.field public static final CastMiniController_castTitleTextAppearance:I = 0x12

.field public static final CustomCastTheme:[I

.field public static final CustomCastTheme_castExpandedControllerStyle:I = 0x0

.field public static final CustomCastTheme_castIntroOverlayStyle:I = 0x1

.field public static final CustomCastTheme_castMiniControllerStyle:I = 0x2

.field public static final LoadingImageView:[I

.field public static final LoadingImageView_circleCrop:I = 0x0

.field public static final LoadingImageView_imageAspectRatio:I = 0x1

.field public static final LoadingImageView_imageAspectRatioAdjust:I = 0x2

.field public static final SignInButton:[I

.field public static final SignInButton_buttonSize:I = 0x0

.field public static final SignInButton_colorScheme:I = 0x1

.field public static final SignInButton_scopeUris:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 381
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/R$styleable;->AdsAttrs:[I

    .line 385
    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/gms/R$styleable;->CastExpandedController:[I

    .line 399
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/gms/R$styleable;->CastIntroOverlay:[I

    .line 406
    const/16 v0, 0x13

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/android/gms/R$styleable;->CastMiniController:[I

    .line 426
    new-array v0, v1, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/google/android/gms/R$styleable;->CustomCastTheme:[I

    .line 430
    new-array v0, v1, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/google/android/gms/R$styleable;->LoadingImageView:[I

    .line 434
    new-array v0, v1, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/google/android/gms/R$styleable;->SignInButton:[I

    return-void

    .line 381
    :array_0
    .array-data 4
        0x7f040025
        0x7f040026
        0x7f040027
    .end array-data

    .line 385
    :array_1
    .array-data 4
        0x7f040061
        0x7f040064
        0x7f040065
        0x7f040069
        0x7f04006f
        0x7f040070
        0x7f040071
        0x7f040073
        0x7f040074
        0x7f040075
        0x7f040077
        0x7f040078
        0x7f040079
    .end array-data

    .line 399
    :array_2
    .array-data 4
        0x7f04005f
        0x7f040060
        0x7f040062
        0x7f040063
        0x7f040068
        0x7f04007b
    .end array-data

    .line 406
    :array_3
    .array-data 4
        0x7f04005e
        0x7f040061
        0x7f040064
        0x7f040065
        0x7f040069
        0x7f04006b
        0x7f04006c
        0x7f04006d
        0x7f04006f
        0x7f040070
        0x7f040071
        0x7f040072
        0x7f040073
        0x7f040076
        0x7f040077
        0x7f040078
        0x7f040079
        0x7f04007a
        0x7f04007b
    .end array-data

    .line 426
    :array_4
    .array-data 4
        0x7f040066
        0x7f04006a
        0x7f04006e
    .end array-data

    .line 430
    :array_5
    .array-data 4
        0x7f040084
        0x7f040108
        0x7f040109
    .end array-data

    .line 434
    :array_6
    .array-data 4
        0x7f040057
        0x7f040095
        0x7f04018d
    .end array-data
.end method
