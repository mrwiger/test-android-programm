.class Lcom/google/obf/cl$a;
.super Lcom/google/obf/cl$d;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/cl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/obf/cl;

.field private final b:Lcom/google/obf/dw;

.field private final c:Lcom/google/obf/dv;

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(Lcom/google/obf/cl;)V
    .locals 2

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/obf/cl$a;->a:Lcom/google/obf/cl;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/obf/cl$d;-><init>(Lcom/google/obf/cl$1;)V

    .line 2
    new-instance v0, Lcom/google/obf/dw;

    invoke-direct {v0}, Lcom/google/obf/dw;-><init>()V

    iput-object v0, p0, Lcom/google/obf/cl$a;->b:Lcom/google/obf/dw;

    .line 3
    new-instance v0, Lcom/google/obf/dv;

    const/4 v1, 0x4

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Lcom/google/obf/dv;-><init>([B)V

    iput-object v0, p0, Lcom/google/obf/cl$a;->c:Lcom/google/obf/dv;

    .line 4
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 5
    return-void
.end method

.method public a(Lcom/google/obf/dw;ZLcom/google/obf/al;)V
    .locals 8

    .prologue
    const/16 v7, 0xd

    const/16 v2, 0xc

    const/4 v6, 0x3

    const/4 v0, 0x0

    .line 6
    if-eqz p2, :cond_0

    .line 7
    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v1

    .line 8
    invoke-virtual {p1, v1}, Lcom/google/obf/dw;->d(I)V

    .line 9
    iget-object v1, p0, Lcom/google/obf/cl$a;->c:Lcom/google/obf/dv;

    invoke-virtual {p1, v1, v6}, Lcom/google/obf/dw;->a(Lcom/google/obf/dv;I)V

    .line 10
    iget-object v1, p0, Lcom/google/obf/cl$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v1, v2}, Lcom/google/obf/dv;->b(I)V

    .line 11
    iget-object v1, p0, Lcom/google/obf/cl$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v1, v2}, Lcom/google/obf/dv;->c(I)I

    move-result v1

    iput v1, p0, Lcom/google/obf/cl$a;->d:I

    .line 12
    iput v0, p0, Lcom/google/obf/cl$a;->e:I

    .line 13
    iget-object v1, p0, Lcom/google/obf/cl$a;->c:Lcom/google/obf/dv;

    iget-object v1, v1, Lcom/google/obf/dv;->a:[B

    const/4 v2, -0x1

    invoke-static {v1, v0, v6, v2}, Lcom/google/obf/ea;->a([BIII)I

    move-result v1

    iput v1, p0, Lcom/google/obf/cl$a;->f:I

    .line 14
    iget-object v1, p0, Lcom/google/obf/cl$a;->b:Lcom/google/obf/dw;

    iget v2, p0, Lcom/google/obf/cl$a;->d:I

    invoke-virtual {v1, v2}, Lcom/google/obf/dw;->a(I)V

    .line 15
    :cond_0
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v1

    iget v2, p0, Lcom/google/obf/cl$a;->d:I

    iget v3, p0, Lcom/google/obf/cl$a;->e:I

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 16
    iget-object v2, p0, Lcom/google/obf/cl$a;->b:Lcom/google/obf/dw;

    iget-object v2, v2, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/cl$a;->e:I

    invoke-virtual {p1, v2, v3, v1}, Lcom/google/obf/dw;->a([BII)V

    .line 17
    iget v2, p0, Lcom/google/obf/cl$a;->e:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/obf/cl$a;->e:I

    .line 18
    iget v1, p0, Lcom/google/obf/cl$a;->e:I

    iget v2, p0, Lcom/google/obf/cl$a;->d:I

    if-ge v1, v2, :cond_2

    .line 33
    :cond_1
    return-void

    .line 20
    :cond_2
    iget-object v1, p0, Lcom/google/obf/cl$a;->b:Lcom/google/obf/dw;

    iget-object v1, v1, Lcom/google/obf/dw;->a:[B

    iget v2, p0, Lcom/google/obf/cl$a;->d:I

    iget v3, p0, Lcom/google/obf/cl$a;->f:I

    invoke-static {v1, v0, v2, v3}, Lcom/google/obf/ea;->a([BIII)I

    move-result v1

    if-nez v1, :cond_1

    .line 22
    iget-object v1, p0, Lcom/google/obf/cl$a;->b:Lcom/google/obf/dw;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/google/obf/dw;->d(I)V

    .line 23
    iget v1, p0, Lcom/google/obf/cl$a;->d:I

    add-int/lit8 v1, v1, -0x9

    div-int/lit8 v1, v1, 0x4

    .line 24
    :goto_0
    if-ge v0, v1, :cond_1

    .line 25
    iget-object v2, p0, Lcom/google/obf/cl$a;->b:Lcom/google/obf/dw;

    iget-object v3, p0, Lcom/google/obf/cl$a;->c:Lcom/google/obf/dv;

    const/4 v4, 0x4

    invoke-virtual {v2, v3, v4}, Lcom/google/obf/dw;->a(Lcom/google/obf/dv;I)V

    .line 26
    iget-object v2, p0, Lcom/google/obf/cl$a;->c:Lcom/google/obf/dv;

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Lcom/google/obf/dv;->c(I)I

    move-result v2

    .line 27
    iget-object v3, p0, Lcom/google/obf/cl$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v3, v6}, Lcom/google/obf/dv;->b(I)V

    .line 28
    if-nez v2, :cond_3

    .line 29
    iget-object v2, p0, Lcom/google/obf/cl$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v2, v7}, Lcom/google/obf/dv;->b(I)V

    .line 32
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 30
    :cond_3
    iget-object v2, p0, Lcom/google/obf/cl$a;->c:Lcom/google/obf/dv;

    invoke-virtual {v2, v7}, Lcom/google/obf/dv;->c(I)I

    move-result v2

    .line 31
    iget-object v3, p0, Lcom/google/obf/cl$a;->a:Lcom/google/obf/cl;

    iget-object v3, v3, Lcom/google/obf/cl;->a:Landroid/util/SparseArray;

    new-instance v4, Lcom/google/obf/cl$c;

    iget-object v5, p0, Lcom/google/obf/cl$a;->a:Lcom/google/obf/cl;

    invoke-direct {v4, v5, v2}, Lcom/google/obf/cl$c;-><init>(Lcom/google/obf/cl;I)V

    invoke-virtual {v3, v2, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_1
.end method
