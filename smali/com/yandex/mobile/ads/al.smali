.class public final enum Lcom/yandex/mobile/ads/al;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/yandex/mobile/ads/al;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/yandex/mobile/ads/al;

.field public static final enum b:Lcom/yandex/mobile/ads/al;

.field public static final enum c:Lcom/yandex/mobile/ads/al;

.field public static final enum d:Lcom/yandex/mobile/ads/al;

.field public static final enum e:Lcom/yandex/mobile/ads/al;

.field public static final enum f:Lcom/yandex/mobile/ads/al;

.field public static final enum g:Lcom/yandex/mobile/ads/al;

.field public static final enum h:Lcom/yandex/mobile/ads/al;

.field public static final enum i:Lcom/yandex/mobile/ads/al;

.field public static final enum j:Lcom/yandex/mobile/ads/al;

.field public static final enum k:Lcom/yandex/mobile/ads/al;

.field public static final enum l:Lcom/yandex/mobile/ads/al;

.field public static final enum m:Lcom/yandex/mobile/ads/al;

.field public static final enum n:Lcom/yandex/mobile/ads/al;

.field public static final enum o:Lcom/yandex/mobile/ads/al;

.field private static final synthetic q:[Lcom/yandex/mobile/ads/al;


# instance fields
.field private final p:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 21
    new-instance v0, Lcom/yandex/mobile/ads/al;

    const-string v1, "YMAD_HEADER_WIDTH"

    const-string v2, "YMAd-Width"

    invoke-direct {v0, v1, v4, v2}, Lcom/yandex/mobile/ads/al;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/yandex/mobile/ads/al;->a:Lcom/yandex/mobile/ads/al;

    .line 22
    new-instance v0, Lcom/yandex/mobile/ads/al;

    const-string v1, "YMAD_HEADER_HEIGHT"

    const-string v2, "YMAd-Height"

    invoke-direct {v0, v1, v5, v2}, Lcom/yandex/mobile/ads/al;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/yandex/mobile/ads/al;->b:Lcom/yandex/mobile/ads/al;

    .line 23
    new-instance v0, Lcom/yandex/mobile/ads/al;

    const-string v1, "YMAD_TYPE"

    const-string v2, "YMAd-Type"

    invoke-direct {v0, v1, v6, v2}, Lcom/yandex/mobile/ads/al;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/yandex/mobile/ads/al;->c:Lcom/yandex/mobile/ads/al;

    .line 24
    new-instance v0, Lcom/yandex/mobile/ads/al;

    const-string v1, "YMAD_ID"

    const-string v2, "YMAd-Id"

    invoke-direct {v0, v1, v7, v2}, Lcom/yandex/mobile/ads/al;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/yandex/mobile/ads/al;->d:Lcom/yandex/mobile/ads/al;

    .line 25
    new-instance v0, Lcom/yandex/mobile/ads/al;

    const-string v1, "YMAD_SHOW_NOTICE"

    const-string v2, "YMAd-ShowNotice"

    invoke-direct {v0, v1, v8, v2}, Lcom/yandex/mobile/ads/al;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/yandex/mobile/ads/al;->e:Lcom/yandex/mobile/ads/al;

    .line 26
    new-instance v0, Lcom/yandex/mobile/ads/al;

    const-string v1, "YMAD_NOTICE_DELAY"

    const/4 v2, 0x5

    const-string v3, "YMAd-NoticeDelay"

    invoke-direct {v0, v1, v2, v3}, Lcom/yandex/mobile/ads/al;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/yandex/mobile/ads/al;->f:Lcom/yandex/mobile/ads/al;

    .line 27
    new-instance v0, Lcom/yandex/mobile/ads/al;

    const-string v1, "YMAD_CLICK_THROUGH"

    const/4 v2, 0x6

    const-string v3, "YMAd-ClickThrough"

    invoke-direct {v0, v1, v2, v3}, Lcom/yandex/mobile/ads/al;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/yandex/mobile/ads/al;->g:Lcom/yandex/mobile/ads/al;

    .line 28
    new-instance v0, Lcom/yandex/mobile/ads/al;

    const-string v1, "YMAD_PREFETCH_COUNT"

    const/4 v2, 0x7

    const-string v3, "YMAd-PrefetchCount"

    invoke-direct {v0, v1, v2, v3}, Lcom/yandex/mobile/ads/al;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/yandex/mobile/ads/al;->h:Lcom/yandex/mobile/ads/al;

    .line 29
    new-instance v0, Lcom/yandex/mobile/ads/al;

    const-string v1, "YMAD_REFRESH_PERIOD"

    const/16 v2, 0x8

    const-string v3, "YMAd-RefreshPeriod"

    invoke-direct {v0, v1, v2, v3}, Lcom/yandex/mobile/ads/al;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/yandex/mobile/ads/al;->i:Lcom/yandex/mobile/ads/al;

    .line 30
    new-instance v0, Lcom/yandex/mobile/ads/al;

    const-string v1, "YMAD_RELOAD_TIMEOUT"

    const/16 v2, 0x9

    const-string v3, "YMAd-ReloadTimeout"

    invoke-direct {v0, v1, v2, v3}, Lcom/yandex/mobile/ads/al;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/yandex/mobile/ads/al;->j:Lcom/yandex/mobile/ads/al;

    .line 31
    new-instance v0, Lcom/yandex/mobile/ads/al;

    const-string v1, "YMAD_RENDERER"

    const/16 v2, 0xa

    const-string v3, "YMAd-Renderer"

    invoke-direct {v0, v1, v2, v3}, Lcom/yandex/mobile/ads/al;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/yandex/mobile/ads/al;->k:Lcom/yandex/mobile/ads/al;

    .line 32
    new-instance v0, Lcom/yandex/mobile/ads/al;

    const-string v1, "YMAD_SESSION_DATA"

    const/16 v2, 0xb

    const-string v3, "YMAd-SessionData"

    invoke-direct {v0, v1, v2, v3}, Lcom/yandex/mobile/ads/al;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/yandex/mobile/ads/al;->l:Lcom/yandex/mobile/ads/al;

    .line 33
    new-instance v0, Lcom/yandex/mobile/ads/al;

    const-string v1, "YMAD_VISIBILITY_PERCENT"

    const/16 v2, 0xc

    const-string v3, "YMAd-VisibilityPercent"

    invoke-direct {v0, v1, v2, v3}, Lcom/yandex/mobile/ads/al;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/yandex/mobile/ads/al;->m:Lcom/yandex/mobile/ads/al;

    .line 34
    new-instance v0, Lcom/yandex/mobile/ads/al;

    const-string v1, "LOCATION"

    const/16 v2, 0xd

    const-string v3, "Location"

    invoke-direct {v0, v1, v2, v3}, Lcom/yandex/mobile/ads/al;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/yandex/mobile/ads/al;->n:Lcom/yandex/mobile/ads/al;

    .line 35
    new-instance v0, Lcom/yandex/mobile/ads/al;

    const-string v1, "USER_AGENT"

    const/16 v2, 0xe

    const-string v3, "User-Agent"

    invoke-direct {v0, v1, v2, v3}, Lcom/yandex/mobile/ads/al;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/yandex/mobile/ads/al;->o:Lcom/yandex/mobile/ads/al;

    .line 19
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/yandex/mobile/ads/al;

    sget-object v1, Lcom/yandex/mobile/ads/al;->a:Lcom/yandex/mobile/ads/al;

    aput-object v1, v0, v4

    sget-object v1, Lcom/yandex/mobile/ads/al;->b:Lcom/yandex/mobile/ads/al;

    aput-object v1, v0, v5

    sget-object v1, Lcom/yandex/mobile/ads/al;->c:Lcom/yandex/mobile/ads/al;

    aput-object v1, v0, v6

    sget-object v1, Lcom/yandex/mobile/ads/al;->d:Lcom/yandex/mobile/ads/al;

    aput-object v1, v0, v7

    sget-object v1, Lcom/yandex/mobile/ads/al;->e:Lcom/yandex/mobile/ads/al;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/yandex/mobile/ads/al;->f:Lcom/yandex/mobile/ads/al;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/yandex/mobile/ads/al;->g:Lcom/yandex/mobile/ads/al;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/yandex/mobile/ads/al;->h:Lcom/yandex/mobile/ads/al;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/yandex/mobile/ads/al;->i:Lcom/yandex/mobile/ads/al;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/yandex/mobile/ads/al;->j:Lcom/yandex/mobile/ads/al;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/yandex/mobile/ads/al;->k:Lcom/yandex/mobile/ads/al;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/yandex/mobile/ads/al;->l:Lcom/yandex/mobile/ads/al;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/yandex/mobile/ads/al;->m:Lcom/yandex/mobile/ads/al;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/yandex/mobile/ads/al;->n:Lcom/yandex/mobile/ads/al;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/yandex/mobile/ads/al;->o:Lcom/yandex/mobile/ads/al;

    aput-object v2, v0, v1

    sput-object v0, Lcom/yandex/mobile/ads/al;->q:[Lcom/yandex/mobile/ads/al;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 44
    iput-object p3, p0, Lcom/yandex/mobile/ads/al;->p:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/yandex/mobile/ads/al;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lcom/yandex/mobile/ads/al;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/al;

    return-object v0
.end method

.method public static values()[Lcom/yandex/mobile/ads/al;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/yandex/mobile/ads/al;->q:[Lcom/yandex/mobile/ads/al;

    invoke-virtual {v0}, [Lcom/yandex/mobile/ads/al;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/yandex/mobile/ads/al;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/yandex/mobile/ads/al;->p:Ljava/lang/String;

    return-object v0
.end method
