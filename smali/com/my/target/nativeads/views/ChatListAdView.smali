.class public Lcom/my/target/nativeads/views/ChatListAdView;
.super Landroid/widget/RelativeLayout;
.source "ChatListAdView.java"


# static fields
.field private static final AD_ID:I

.field private static final AGE_ID:I

.field private static final COLOR_PLACEHOLDER_GRAY:I = -0x111112

.field private static final DESC_ID:I

.field private static final ICON_ID:I

.field private static final LABELS_ID:I

.field private static final LETTERS_GREY:I = -0x666667

.field private static final RATING_ID:I

.field private static final STARS_ID:I

.field private static final TITLE_2_ID:I

.field private static final TITLE_ID:I

.field private static final URL_ID:I

.field private static final VOTES_ID:I


# instance fields
.field private final advertisingLabel:Landroid/widget/TextView;

.field private final ageRestrictionLabel:Lcom/my/target/bu;

.field private banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

.field private final descriptionLabel:Landroid/widget/TextView;

.field private final disclaimerLabel:Landroid/widget/TextView;

.field private final iconImageView:Lcom/my/target/bv;

.field private final labelsLayout:Landroid/widget/LinearLayout;

.field private final ratingLayout:Landroid/widget/LinearLayout;

.field private final starsView:Lcom/my/target/ca;

.field private final titleLabel:Landroid/widget/TextView;

.field private final uiUtils:Lcom/my/target/cm;

.field private final urlLabel:Landroid/widget/TextView;

.field private final votesLabel:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/ChatListAdView;->AGE_ID:I

    .line 38
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/ChatListAdView;->ICON_ID:I

    .line 39
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/ChatListAdView;->LABELS_ID:I

    .line 40
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/ChatListAdView;->TITLE_ID:I

    .line 41
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/ChatListAdView;->URL_ID:I

    .line 42
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/ChatListAdView;->DESC_ID:I

    .line 43
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/ChatListAdView;->AD_ID:I

    .line 44
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/ChatListAdView;->TITLE_2_ID:I

    .line 45
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/ChatListAdView;->STARS_ID:I

    .line 46
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/ChatListAdView;->VOTES_ID:I

    .line 47
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/ChatListAdView;->RATING_ID:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/my/target/nativeads/views/ChatListAdView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/my/target/nativeads/views/ChatListAdView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 75
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 76
    new-instance v0, Lcom/my/target/bu;

    invoke-direct {v0, p1}, Lcom/my/target/bu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    .line 77
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->advertisingLabel:Landroid/widget/TextView;

    .line 78
    new-instance v0, Lcom/my/target/bv;

    invoke-direct {v0, p1}, Lcom/my/target/bv;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->iconImageView:Lcom/my/target/bv;

    .line 79
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->labelsLayout:Landroid/widget/LinearLayout;

    .line 80
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->titleLabel:Landroid/widget/TextView;

    .line 81
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->urlLabel:Landroid/widget/TextView;

    .line 82
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->descriptionLabel:Landroid/widget/TextView;

    .line 83
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ratingLayout:Landroid/widget/LinearLayout;

    .line 84
    new-instance v0, Lcom/my/target/ca;

    invoke-direct {v0, p1}, Lcom/my/target/ca;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->starsView:Lcom/my/target/ca;

    .line 85
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->votesLabel:Landroid/widget/TextView;

    .line 86
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->disclaimerLabel:Landroid/widget/TextView;

    .line 88
    const-string v0, "ad_view"

    invoke-static {p0, v0}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->titleLabel:Landroid/widget/TextView;

    const-string v1, "title_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->descriptionLabel:Landroid/widget/TextView;

    const-string v1, "description_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->disclaimerLabel:Landroid/widget/TextView;

    const-string v1, "disclaimer_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 93
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->uiUtils:Lcom/my/target/cm;

    .line 95
    invoke-direct {p0}, Lcom/my/target/nativeads/views/ChatListAdView;->initView()V

    .line 96
    return-void
.end method

.method private initView()V
    .locals 10

    .prologue
    const v9, -0x666667

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, -0x2

    .line 269
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0xc

    .line 270
    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/nativeads/views/ChatListAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v3, 0xc

    .line 271
    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/nativeads/views/ChatListAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v4, 0xc

    .line 272
    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    .line 269
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/my/target/nativeads/views/ChatListAdView;->setPadding(IIII)V

    .line 275
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    sget v1, Lcom/my/target/nativeads/views/ChatListAdView;->AGE_ID:I

    invoke-virtual {v0, v1}, Lcom/my/target/bu;->setId(I)V

    .line 276
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    const v1, -0x777778

    invoke-virtual {v0, v6, v1}, Lcom/my/target/bu;->c(II)V

    .line 277
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v1, v7}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v1, v8, v8, v8}, Lcom/my/target/bu;->setPadding(IIII)V

    .line 278
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 280
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 281
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    invoke-virtual {v1, v0}, Lcom/my/target/bu;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 283
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    invoke-virtual {v0, v9}, Lcom/my/target/bu;->setTextColor(I)V

    .line 284
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    invoke-virtual {v0, v6, v9}, Lcom/my/target/bu;->c(II)V

    .line 285
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    invoke-virtual {v0, v8}, Lcom/my/target/bu;->setBackgroundColor(I)V

    .line 287
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->advertisingLabel:Landroid/widget/TextView;

    sget v1, Lcom/my/target/nativeads/views/ChatListAdView;->AD_ID:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 288
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->advertisingLabel:Landroid/widget/TextView;

    const-string v1, "advertising_label"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 289
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 291
    sget v1, Lcom/my/target/nativeads/views/ChatListAdView;->AGE_ID:I

    invoke-virtual {v0, v6, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 292
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->advertisingLabel:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 293
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->advertisingLabel:Landroid/widget/TextView;

    const/high16 v1, 0x41600000    # 14.0f

    invoke-virtual {v0, v7, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 294
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->advertisingLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 297
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->iconImageView:Lcom/my/target/bv;

    sget v1, Lcom/my/target/nativeads/views/ChatListAdView;->ICON_ID:I

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setId(I)V

    .line 298
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->iconImageView:Lcom/my/target/bv;

    const-string v1, "icon_image"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 299
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0x36

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/nativeads/views/ChatListAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v3, 0x36

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 300
    const/4 v1, 0x3

    sget v2, Lcom/my/target/nativeads/views/ChatListAdView;->AD_ID:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 301
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v1, v7}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 302
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->iconImageView:Lcom/my/target/bv;

    invoke-virtual {v1, v0}, Lcom/my/target/bv;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 305
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->labelsLayout:Landroid/widget/LinearLayout;

    sget v1, Lcom/my/target/nativeads/views/ChatListAdView;->LABELS_ID:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setId(I)V

    .line 306
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->labelsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 307
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->labelsLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0x36

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setMinimumHeight(I)V

    .line 308
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 310
    const/4 v1, 0x3

    sget v2, Lcom/my/target/nativeads/views/ChatListAdView;->AD_ID:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 311
    sget v1, Lcom/my/target/nativeads/views/ChatListAdView;->ICON_ID:I

    invoke-virtual {v0, v6, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 312
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 313
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->uiUtils:Lcom/my/target/cm;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 314
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->labelsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 316
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->titleLabel:Landroid/widget/TextView;

    sget v1, Lcom/my/target/nativeads/views/ChatListAdView;->TITLE_ID:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 317
    const-string v0, "ad_view"

    invoke-static {p0, v0}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 318
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->titleLabel:Landroid/widget/TextView;

    const-string v1, "title_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 319
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 321
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->titleLabel:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 322
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->titleLabel:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 323
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->titleLabel:Landroid/widget/TextView;

    const/high16 v1, 0x41800000    # 16.0f

    invoke-virtual {v0, v7, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 324
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->titleLabel:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 326
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->urlLabel:Landroid/widget/TextView;

    sget v1, Lcom/my/target/nativeads/views/ChatListAdView;->URL_ID:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 327
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 329
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v1, v7}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 330
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->urlLabel:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 331
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->urlLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 332
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->urlLabel:Landroid/widget/TextView;

    const/high16 v1, 0x41600000    # 14.0f

    invoke-virtual {v0, v7, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 334
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->descriptionLabel:Landroid/widget/TextView;

    sget v1, Lcom/my/target/nativeads/views/ChatListAdView;->DESC_ID:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 335
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 337
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v1, v7}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 339
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->descriptionLabel:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 340
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->descriptionLabel:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 341
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->descriptionLabel:Landroid/widget/TextView;

    const/high16 v1, 0x41600000    # 14.0f

    invoke-virtual {v0, v7, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 343
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ratingLayout:Landroid/widget/LinearLayout;

    sget v1, Lcom/my/target/nativeads/views/ChatListAdView;->RATING_ID:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setId(I)V

    .line 344
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ratingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 345
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 347
    const/4 v1, 0x3

    sget v2, Lcom/my/target/nativeads/views/ChatListAdView;->TITLE_2_ID:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 348
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ratingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 350
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->starsView:Lcom/my/target/ca;

    sget v1, Lcom/my/target/nativeads/views/ChatListAdView;->STARS_ID:I

    invoke-virtual {v0, v1}, Lcom/my/target/ca;->setId(I)V

    .line 351
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0x49

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/nativeads/views/ChatListAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v3, 0xc

    .line 352
    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 353
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->uiUtils:Lcom/my/target/cm;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 354
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->uiUtils:Lcom/my/target/cm;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 355
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->starsView:Lcom/my/target/ca;

    invoke-virtual {v1, v0}, Lcom/my/target/ca;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 357
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->votesLabel:Landroid/widget/TextView;

    sget v1, Lcom/my/target/nativeads/views/ChatListAdView;->VOTES_ID:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 358
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->votesLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 359
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->votesLabel:Landroid/widget/TextView;

    const/high16 v1, 0x41600000    # 14.0f

    invoke-virtual {v0, v7, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 361
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 363
    const/4 v1, 0x3

    sget v2, Lcom/my/target/nativeads/views/ChatListAdView;->RATING_ID:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 365
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->disclaimerLabel:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 366
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->disclaimerLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 367
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->disclaimerLabel:Landroid/widget/TextView;

    const/high16 v1, 0x41400000    # 12.0f

    invoke-virtual {v0, v7, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 369
    const v0, -0x3a1508

    invoke-static {p0, v8, v0}, Lcom/my/target/cm;->a(Landroid/view/View;II)V

    .line 371
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v2, v7, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    .line 374
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v1, v6}, Lcom/my/target/cm;->n(I)I

    move-result v1

    const v2, -0x333334

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 375
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v1, v6}, Lcom/my/target/cm;->n(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 377
    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    sget-object v2, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v3, v7, [I

    fill-array-data v3, :array_1

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    .line 380
    iget-object v2, p0, Lcom/my/target/nativeads/views/ChatListAdView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v2, v6}, Lcom/my/target/cm;->n(I)I

    move-result v2

    const v3, -0x333334

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 381
    iget-object v2, p0, Lcom/my/target/nativeads/views/ChatListAdView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v2, v6}, Lcom/my/target/cm;->n(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 383
    new-instance v2, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 384
    new-array v3, v6, [I

    const v4, 0x10100a7

    aput v4, v3, v8

    invoke-virtual {v2, v3, v1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 386
    sget-object v1, Landroid/util/StateSet;->WILD_CARD:[I

    invoke-virtual {v2, v1, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 388
    invoke-virtual {p0, v6}, Lcom/my/target/nativeads/views/ChatListAdView;->setClickable(Z)V

    .line 391
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    invoke-virtual {p0, v0}, Lcom/my/target/nativeads/views/ChatListAdView;->addView(Landroid/view/View;)V

    .line 392
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->advertisingLabel:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/my/target/nativeads/views/ChatListAdView;->addView(Landroid/view/View;)V

    .line 393
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->iconImageView:Lcom/my/target/bv;

    invoke-virtual {p0, v0}, Lcom/my/target/nativeads/views/ChatListAdView;->addView(Landroid/view/View;)V

    .line 394
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->labelsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/my/target/nativeads/views/ChatListAdView;->addView(Landroid/view/View;)V

    .line 396
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->labelsLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->titleLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 397
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->labelsLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->urlLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 398
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->labelsLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->descriptionLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 399
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->labelsLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->disclaimerLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 401
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ratingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/my/target/nativeads/views/ChatListAdView;->addView(Landroid/view/View;)V

    .line 403
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ratingLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->starsView:Lcom/my/target/ca;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 404
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ratingLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->votesLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 405
    return-void

    .line 371
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 377
    :array_1
    .array-data 4
        -0x3a1508
        -0x3a1508
    .end array-data
.end method


# virtual methods
.method public getAdvertisingTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->advertisingLabel:Landroid/widget/TextView;

    return-object v0
.end method

.method public getAgeRestrictionTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    return-object v0
.end method

.method public getDescriptionTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->descriptionLabel:Landroid/widget/TextView;

    return-object v0
.end method

.method public getDisclaimerTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->disclaimerLabel:Landroid/widget/TextView;

    return-object v0
.end method

.method public getDomainOrCategoryTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->urlLabel:Landroid/widget/TextView;

    return-object v0
.end method

.method public getIconImageView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->iconImageView:Lcom/my/target/bv;

    return-object v0
.end method

.method public getStarsRatingView()Lcom/my/target/ca;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->starsView:Lcom/my/target/ca;

    return-object v0
.end method

.method public getTitleTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->titleLabel:Landroid/widget/TextView;

    return-object v0
.end method

.method public getVotesTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->votesLabel:Landroid/widget/TextView;

    return-object v0
.end method

.method public loadImages()V
    .locals 2

    .prologue
    .line 249
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

    if-nez v0, :cond_1

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 253
    :cond_1
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

    invoke-virtual {v0}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 254
    if-eqz v0, :cond_0

    .line 256
    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_2

    .line 258
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->iconImageView:Lcom/my/target/bv;

    invoke-static {v0, v1}, Lcom/my/target/ch;->a(Lcom/my/target/common/models/ImageData;Landroid/widget/ImageView;)V

    goto :goto_0

    .line 262
    :cond_2
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->iconImageView:Lcom/my/target/bv;

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/my/target/bv;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public setupView(Lcom/my/target/nativeads/banners/NativePromoBanner;)V
    .locals 6
    .param p1, "banner"    # Lcom/my/target/nativeads/banners/NativePromoBanner;

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 145
    if-nez p1, :cond_0

    .line 245
    :goto_0
    return-void

    .line 149
    :cond_0
    iput-object p1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

    .line 150
    const-string v0, "Setup banner"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 152
    const-string v0, "web"

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getNavigationType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 154
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->urlLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 155
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ratingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 156
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->urlLabel:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getDomain()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->urlLabel:Landroid/widget/TextView;

    const-string v1, "domain_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 207
    :cond_1
    :goto_1
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 208
    if-eqz v0, :cond_2

    .line 210
    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 212
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->iconImageView:Lcom/my/target/bv;

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/my/target/bv;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 222
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->titleLabel:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 223
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->descriptionLabel:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->advertisingLabel:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getAdvertisingLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 225
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getAgeRestrictions()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 227
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getAgeRestrictions()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/bu;->setText(Ljava/lang/CharSequence;)V

    .line 228
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    const-string v1, "age_bordered"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 235
    :goto_3
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getDisclaimer()Ljava/lang/String;

    move-result-object v0

    .line 237
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 239
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->disclaimerLabel:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 159
    :cond_3
    const-string v0, "store"

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getNavigationType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 161
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getCategory()Ljava/lang/String;

    move-result-object v1

    .line 162
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getSubCategory()Ljava/lang/String;

    move-result-object v2

    .line 164
    const-string v0, ""

    .line 165
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 167
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 170
    :cond_4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 172
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 175
    :cond_5
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 177
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 180
    :cond_6
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getRating()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_8

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getRating()F

    move-result v1

    const/high16 v2, 0x40a00000    # 5.0f

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_8

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getVotes()I

    move-result v1

    if-lez v1, :cond_8

    .line 182
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->urlLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 183
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ratingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 184
    if-eqz v0, :cond_7

    .line 186
    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ratingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 188
    :cond_7
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->labelsLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ratingLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 190
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ratingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 192
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->starsView:Lcom/my/target/ca;

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getRating()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/ca;->setRating(F)V

    .line 193
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->votesLabel:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getVotes()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 194
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->votesLabel:Landroid/widget/TextView;

    const-string v1, "votes_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 195
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->starsView:Lcom/my/target/ca;

    const-string v1, "rating_view"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 200
    :cond_8
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->urlLabel:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 201
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->urlLabel:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ratingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 203
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->urlLabel:Landroid/widget/TextView;

    const-string v1, "category_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 216
    :cond_9
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->iconImageView:Lcom/my/target/bv;

    const v2, -0x111112

    invoke-virtual {v1, v2}, Lcom/my/target/bv;->setBackgroundColor(I)V

    .line 217
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->iconImageView:Lcom/my/target/bv;

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/my/target/bv;->setPlaceholderWidth(I)V

    .line 218
    iget-object v1, p0, Lcom/my/target/nativeads/views/ChatListAdView;->iconImageView:Lcom/my/target/bv;

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getHeight()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/my/target/bv;->setPlaceholderHeight(I)V

    goto/16 :goto_2

    .line 232
    :cond_a
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    invoke-virtual {v0, v4}, Lcom/my/target/bu;->setVisibility(I)V

    goto/16 :goto_3

    .line 243
    :cond_b
    iget-object v0, p0, Lcom/my/target/nativeads/views/ChatListAdView;->disclaimerLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0
.end method
