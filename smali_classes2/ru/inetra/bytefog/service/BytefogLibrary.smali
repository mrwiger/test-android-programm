.class Lru/inetra/bytefog/service/BytefogLibrary;
.super Ljava/lang/Object;
.source "BytefogLibrary.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/inetra/bytefog/service/BytefogLibrary$EventHandler;
    }
.end annotation


# static fields
.field private static final HANDLE_MESSAGE_HLS_ERROR:I = 0x1

.field private static final HANDLE_MESSAGE_UPDATE_RESULT:I = 0x2

.field private static final LIBRARY_NAME:Ljava/lang/String; = "bytefog-node-service"

.field private static final TAG:Ljava/lang/String; = "BytefogLibrary"

.field public static context:Landroid/content/Context;

.field public static handler:Lru/inetra/bytefog/service/BytefogLibrary$EventHandler;

.field public static listener:Lru/inetra/bytefog/service/BytefogLibraryListener;


# direct methods
.method public constructor <init>(Lru/inetra/bytefog/service/BytefogLibraryListener;)V
    .locals 2
    .param p1, "listener"    # Lru/inetra/bytefog/service/BytefogLibraryListener;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    sput-object p1, Lru/inetra/bytefog/service/BytefogLibrary;->listener:Lru/inetra/bytefog/service/BytefogLibraryListener;

    .line 25
    const-string v0, "bytefog-node-service"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 26
    new-instance v0, Lru/inetra/bytefog/service/BytefogLibrary$EventHandler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lru/inetra/bytefog/service/BytefogLibrary$EventHandler;-><init>(Lru/inetra/bytefog/service/BytefogLibrary;Landroid/os/Looper;)V

    sput-object v0, Lru/inetra/bytefog/service/BytefogLibrary;->handler:Lru/inetra/bytefog/service/BytefogLibrary$EventHandler;

    .line 27
    const-string v0, "BytefogLibrary"

    const-string v1, "Library libbytefog-node-service.so is successfully loaded"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    return-void
.end method

.method private static getNetworkInterfaceInfoSerialized()[B
    .locals 2

    .prologue
    .line 66
    sget-object v1, Lru/inetra/bytefog/service/BytefogLibrary;->context:Landroid/content/Context;

    invoke-static {v1}, Lru/inetra/bytefog/service/NetworkUtils;->getNetworkInterfaceInfo(Landroid/content/Context;)[Lru/inetra/bytefog/service/NetworkInterfaceInfo;

    move-result-object v0

    .line 67
    .local v0, "networkInterfaceInfo":[Lru/inetra/bytefog/service/NetworkInterfaceInfo;
    invoke-static {v0}, Lru/inetra/bytefog/service/NetworkUtils;->getNetworkInterfaceInfoSerialized([Lru/inetra/bytefog/service/NetworkInterfaceInfo;)[B

    move-result-object v1

    return-object v1
.end method

.method private static hlsErrorCallback(I)V
    .locals 2
    .param p0, "statusCode"    # I

    .prologue
    .line 47
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 48
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 49
    iput p0, v0, Landroid/os/Message;->arg1:I

    .line 50
    sget-object v1, Lru/inetra/bytefog/service/BytefogLibrary;->handler:Lru/inetra/bytefog/service/BytefogLibrary$EventHandler;

    invoke-virtual {v1, v0}, Lru/inetra/bytefog/service/BytefogLibrary$EventHandler;->sendMessage(Landroid/os/Message;)Z

    .line 51
    return-void
.end method

.method private static updateResultCallback(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "result"    # I
    .param p1, "availableVersion"    # Ljava/lang/String;
    .param p2, "downloadUrl"    # Ljava/lang/String;
    .param p3, "changelog"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 55
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x2

    iput v2, v1, Landroid/os/Message;->what:I

    .line 56
    new-instance v0, Lru/inetra/bytefog/service/UpdateInfo;

    invoke-direct {v0}, Lru/inetra/bytefog/service/UpdateInfo;-><init>()V

    .line 57
    .local v0, "info":Lru/inetra/bytefog/service/UpdateInfo;
    invoke-static {p0}, Lru/inetra/bytefog/service/UpdateStatus;->fromInteger(I)Lru/inetra/bytefog/service/UpdateStatus;

    move-result-object v2

    invoke-virtual {v0, v2}, Lru/inetra/bytefog/service/UpdateInfo;->setStatus(Lru/inetra/bytefog/service/UpdateStatus;)V

    .line 58
    invoke-virtual {v0, p1}, Lru/inetra/bytefog/service/UpdateInfo;->setAvailableVersion(Ljava/lang/String;)V

    .line 59
    invoke-virtual {v0, p2}, Lru/inetra/bytefog/service/UpdateInfo;->setDownloadUrl(Ljava/lang/String;)V

    .line 60
    invoke-virtual {v0, p3}, Lru/inetra/bytefog/service/UpdateInfo;->setChangelog(Ljava/lang/String;)V

    .line 61
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 62
    sget-object v2, Lru/inetra/bytefog/service/BytefogLibrary;->handler:Lru/inetra/bytefog/service/BytefogLibrary$EventHandler;

    invoke-virtual {v2, v1}, Lru/inetra/bytefog/service/BytefogLibrary$EventHandler;->sendMessage(Landroid/os/Message;)Z

    .line 63
    return-void
.end method


# virtual methods
.method native cancelAllRequests()V
.end method

.method native convertBytefogMagnetToHlsPlaylistUri(Ljava/lang/String;)Ljava/lang/String;
.end method

.method native getApiVersion()Ljava/lang/String;
.end method

.method native notifyNetworkChanged()V
.end method

.method public scheduleFail()V
    .locals 4

    .prologue
    .line 71
    sget-object v0, Lru/inetra/bytefog/service/BytefogLibrary;->handler:Lru/inetra/bytefog/service/BytefogLibrary$EventHandler;

    new-instance v1, Lru/inetra/bytefog/service/BytefogLibrary$1;

    invoke-direct {v1, p0}, Lru/inetra/bytefog/service/BytefogLibrary$1;-><init>(Lru/inetra/bytefog/service/BytefogLibrary;)V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Lru/inetra/bytefog/service/BytefogLibrary$EventHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 77
    return-void
.end method

.method native start([Ljava/lang/String;)Z
.end method

.method native started()Z
.end method

.method native stop()V
.end method

.method native trollBreakpad()V
.end method
