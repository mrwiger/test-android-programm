.class Lru/cn/tv/player/SimplePlayerFragmentEx$5;
.super Ljava/lang/Object;
.source "SimplePlayerFragmentEx.java"

# interfaces
.implements Lru/cn/player/ITrackSelector$TrackNameGenerator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/player/SimplePlayerFragmentEx;->getSubtitlesTrackNameGenerator()Lru/cn/player/ITrackSelector$TrackNameGenerator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field count:I

.field final synthetic this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;


# direct methods
.method constructor <init>(Lru/cn/tv/player/SimplePlayerFragmentEx;)V
    .locals 1
    .param p1, "this$0"    # Lru/cn/tv/player/SimplePlayerFragmentEx;

    .prologue
    .line 646
    iput-object p1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$5;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 647
    const/4 v0, 0x1

    iput v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$5;->count:I

    return-void
.end method


# virtual methods
.method public getName(Lru/cn/player/TrackInfo;)Ljava/lang/String;
    .locals 6
    .param p1, "trackInfo"    # Lru/cn/player/TrackInfo;

    .prologue
    .line 650
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$5;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    const v5, 0x7f0e0162

    invoke-virtual {v4, v5}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$5;->count:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 651
    .local v1, "title":Ljava/lang/String;
    iget v3, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$5;->count:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$5;->count:I

    .line 652
    if-nez p1, :cond_0

    move-object v2, v1

    .line 659
    .end local v1    # "title":Ljava/lang/String;
    .local v2, "title":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 655
    .end local v2    # "title":Ljava/lang/String;
    .restart local v1    # "title":Ljava/lang/String;
    :cond_0
    iget-object v3, p1, Lru/cn/player/TrackInfo;->language:Ljava/lang/String;

    const-string v4, "und"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 656
    .local v0, "lang":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 657
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_1
    move-object v2, v1

    .line 659
    .end local v1    # "title":Ljava/lang/String;
    .restart local v2    # "title":Ljava/lang/String;
    goto :goto_0
.end method
