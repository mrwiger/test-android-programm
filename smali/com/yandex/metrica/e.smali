.class public Lcom/yandex/metrica/e;
.super Lcom/yandex/metrica/YandexMetricaConfig;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/e$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/yandex/metrica/a;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/lang/Integer;

.field private final g:Ljava/lang/Integer;

.field private final h:Ljava/lang/Integer;

.field private final i:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/lang/Boolean;

.field private final k:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Lcom/yandex/metrica/YandexMetricaConfig;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-direct {p0, p1}, Lcom/yandex/metrica/YandexMetricaConfig;-><init>(Lcom/yandex/metrica/YandexMetricaConfig;)V

    .line 53
    iput-object v0, p0, Lcom/yandex/metrica/e;->a:Lcom/yandex/metrica/a;

    .line 54
    iput-object v0, p0, Lcom/yandex/metrica/e;->b:Ljava/util/Map;

    .line 55
    iput-object v0, p0, Lcom/yandex/metrica/e;->d:Ljava/lang/String;

    .line 56
    iput-object v0, p0, Lcom/yandex/metrica/e;->f:Ljava/lang/Integer;

    .line 57
    iput-object v0, p0, Lcom/yandex/metrica/e;->g:Ljava/lang/Integer;

    .line 58
    iput-object v0, p0, Lcom/yandex/metrica/e;->h:Ljava/lang/Integer;

    .line 59
    iput-object v0, p0, Lcom/yandex/metrica/e;->c:Ljava/lang/String;

    .line 60
    iput-object v0, p0, Lcom/yandex/metrica/e;->i:Ljava/util/LinkedHashMap;

    .line 61
    iput-object v0, p0, Lcom/yandex/metrica/e;->j:Ljava/lang/Boolean;

    .line 62
    iput-object v0, p0, Lcom/yandex/metrica/e;->k:Ljava/lang/Boolean;

    .line 63
    iput-object v0, p0, Lcom/yandex/metrica/e;->e:Ljava/util/List;

    .line 64
    return-void
.end method

.method private constructor <init>(Lcom/yandex/metrica/e$a;)V
    .locals 2

    .prologue
    .line 559
    invoke-static {p1}, Lcom/yandex/metrica/e$a;->a(Lcom/yandex/metrica/e$a;)Lcom/yandex/metrica/YandexMetricaConfig$Builder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/yandex/metrica/YandexMetricaConfig;-><init>(Lcom/yandex/metrica/YandexMetricaConfig$Builder;)V

    .line 560
    invoke-static {p1}, Lcom/yandex/metrica/e$a;->b(Lcom/yandex/metrica/e$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/e;->d:Ljava/lang/String;

    .line 561
    invoke-static {p1}, Lcom/yandex/metrica/e$a;->c(Lcom/yandex/metrica/e$a;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/e;->f:Ljava/lang/Integer;

    .line 563
    invoke-static {p1}, Lcom/yandex/metrica/e$a;->d(Lcom/yandex/metrica/e$a;)Ljava/util/List;

    move-result-object v0

    .line 564
    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->a(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 565
    invoke-static {p1}, Lcom/yandex/metrica/e$a;->e(Lcom/yandex/metrica/e$a;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 566
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 567
    invoke-static {p1}, Lcom/yandex/metrica/e$a;->e(Lcom/yandex/metrica/e$a;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 571
    :cond_0
    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/yandex/metrica/e;->e:Ljava/util/List;

    .line 573
    invoke-static {p1}, Lcom/yandex/metrica/e$a;->f(Lcom/yandex/metrica/e$a;)Lcom/yandex/metrica/a;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/e;->a:Lcom/yandex/metrica/a;

    .line 574
    invoke-static {p1}, Lcom/yandex/metrica/e$a;->g(Lcom/yandex/metrica/e$a;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/e;->b:Ljava/util/Map;

    .line 575
    invoke-static {p1}, Lcom/yandex/metrica/e$a;->h(Lcom/yandex/metrica/e$a;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/e;->h:Ljava/lang/Integer;

    .line 576
    invoke-static {p1}, Lcom/yandex/metrica/e$a;->i(Lcom/yandex/metrica/e$a;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/e;->g:Ljava/lang/Integer;

    .line 577
    iget-object v0, p1, Lcom/yandex/metrica/e$a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/yandex/metrica/e;->c:Ljava/lang/String;

    .line 578
    invoke-static {p1}, Lcom/yandex/metrica/e$a;->j(Lcom/yandex/metrica/e$a;)Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/e;->i:Ljava/util/LinkedHashMap;

    .line 579
    invoke-static {p1}, Lcom/yandex/metrica/e$a;->k(Lcom/yandex/metrica/e$a;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/e;->j:Ljava/lang/Boolean;

    .line 580
    iget-object v0, p1, Lcom/yandex/metrica/e$a;->b:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/yandex/metrica/e;->k:Ljava/lang/Boolean;

    .line 581
    return-void

    .line 571
    :cond_1
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/yandex/metrica/e$a;B)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/yandex/metrica/e;-><init>(Lcom/yandex/metrica/e$a;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/yandex/metrica/e$a;
    .locals 1

    .prologue
    .line 86
    new-instance v0, Lcom/yandex/metrica/e$a;

    invoke-direct {v0, p0}, Lcom/yandex/metrica/e$a;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method static a(Lcom/yandex/metrica/YandexMetricaConfig;)Lcom/yandex/metrica/e;
    .locals 1

    .prologue
    .line 67
    instance-of v0, p0, Lcom/yandex/metrica/e;

    if-eqz v0, :cond_0

    .line 68
    check-cast p0, Lcom/yandex/metrica/e;

    .line 70
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lcom/yandex/metrica/e;

    invoke-direct {v0, p0}, Lcom/yandex/metrica/e;-><init>(Lcom/yandex/metrica/YandexMetricaConfig;)V

    move-object p0, v0

    goto :goto_0
.end method

.method static b(Lcom/yandex/metrica/YandexMetricaConfig;)Lcom/yandex/metrica/e$a;
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 105
    invoke-virtual {p0}, Lcom/yandex/metrica/YandexMetricaConfig;->getApiKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/yandex/metrica/e;->a(Ljava/lang/String;)Lcom/yandex/metrica/e$a;

    move-result-object v4

    .line 106
    invoke-virtual {p0}, Lcom/yandex/metrica/YandexMetricaConfig;->getAppVersion()Ljava/lang/String;

    move-result-object v0

    .line 1353
    if-eqz v0, :cond_9

    move v0, v2

    .line 106
    :goto_0
    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {p0}, Lcom/yandex/metrica/YandexMetricaConfig;->getAppVersion()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/yandex/metrica/e$a;->a(Ljava/lang/String;)Lcom/yandex/metrica/e$a;

    .line 109
    :cond_0
    invoke-virtual {p0}, Lcom/yandex/metrica/YandexMetricaConfig;->getSessionTimeout()Ljava/lang/Integer;

    move-result-object v0

    .line 2353
    if-eqz v0, :cond_a

    move v0, v2

    .line 109
    :goto_1
    if-eqz v0, :cond_1

    .line 110
    invoke-virtual {p0}, Lcom/yandex/metrica/YandexMetricaConfig;->getSessionTimeout()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/yandex/metrica/e$a;->a(I)Lcom/yandex/metrica/e$a;

    .line 112
    :cond_1
    invoke-virtual {p0}, Lcom/yandex/metrica/YandexMetricaConfig;->isReportCrashEnabled()Ljava/lang/Boolean;

    move-result-object v0

    .line 3353
    if-eqz v0, :cond_b

    move v0, v2

    .line 112
    :goto_2
    if-eqz v0, :cond_2

    .line 113
    invoke-virtual {p0}, Lcom/yandex/metrica/YandexMetricaConfig;->isReportCrashEnabled()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v4, v0}, Lcom/yandex/metrica/e$a;->a(Z)Lcom/yandex/metrica/e$a;

    .line 115
    :cond_2
    invoke-virtual {p0}, Lcom/yandex/metrica/YandexMetricaConfig;->isReportNativeCrashEnabled()Ljava/lang/Boolean;

    move-result-object v0

    .line 4353
    if-eqz v0, :cond_c

    move v0, v2

    .line 115
    :goto_3
    if-eqz v0, :cond_3

    .line 116
    invoke-virtual {p0}, Lcom/yandex/metrica/YandexMetricaConfig;->isReportNativeCrashEnabled()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v4, v0}, Lcom/yandex/metrica/e$a;->b(Z)Lcom/yandex/metrica/e$a;

    .line 118
    :cond_3
    invoke-virtual {p0}, Lcom/yandex/metrica/YandexMetricaConfig;->getLocation()Landroid/location/Location;

    move-result-object v0

    .line 5353
    if-eqz v0, :cond_d

    move v0, v2

    .line 118
    :goto_4
    if-eqz v0, :cond_4

    .line 119
    invoke-virtual {p0}, Lcom/yandex/metrica/YandexMetricaConfig;->getLocation()Landroid/location/Location;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/yandex/metrica/e$a;->a(Landroid/location/Location;)Lcom/yandex/metrica/e$a;

    .line 121
    :cond_4
    invoke-virtual {p0}, Lcom/yandex/metrica/YandexMetricaConfig;->isTrackLocationEnabled()Ljava/lang/Boolean;

    move-result-object v0

    .line 6353
    if-eqz v0, :cond_e

    move v0, v2

    .line 121
    :goto_5
    if-eqz v0, :cond_5

    .line 122
    invoke-virtual {p0}, Lcom/yandex/metrica/YandexMetricaConfig;->isTrackLocationEnabled()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v4, v0}, Lcom/yandex/metrica/e$a;->c(Z)Lcom/yandex/metrica/e$a;

    .line 124
    :cond_5
    invoke-virtual {p0}, Lcom/yandex/metrica/YandexMetricaConfig;->isCollectInstalledApps()Ljava/lang/Boolean;

    move-result-object v0

    .line 7353
    if-eqz v0, :cond_f

    move v0, v2

    .line 124
    :goto_6
    if-eqz v0, :cond_6

    .line 125
    invoke-virtual {p0}, Lcom/yandex/metrica/YandexMetricaConfig;->isCollectInstalledApps()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v4, v0}, Lcom/yandex/metrica/e$a;->d(Z)Lcom/yandex/metrica/e$a;

    .line 127
    :cond_6
    invoke-virtual {p0}, Lcom/yandex/metrica/YandexMetricaConfig;->isLogEnabled()Ljava/lang/Boolean;

    move-result-object v0

    .line 8353
    if-eqz v0, :cond_10

    move v0, v2

    .line 127
    :goto_7
    if-eqz v0, :cond_7

    .line 128
    invoke-virtual {p0}, Lcom/yandex/metrica/YandexMetricaConfig;->isLogEnabled()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 129
    invoke-virtual {v4}, Lcom/yandex/metrica/e$a;->a()Lcom/yandex/metrica/e$a;

    .line 132
    :cond_7
    invoke-virtual {p0}, Lcom/yandex/metrica/YandexMetricaConfig;->getPreloadInfo()Lcom/yandex/metrica/PreloadInfo;

    move-result-object v0

    .line 9353
    if-eqz v0, :cond_11

    move v0, v2

    .line 132
    :goto_8
    if-eqz v0, :cond_8

    .line 133
    invoke-virtual {p0}, Lcom/yandex/metrica/YandexMetricaConfig;->getPreloadInfo()Lcom/yandex/metrica/PreloadInfo;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/yandex/metrica/e$a;->a(Lcom/yandex/metrica/PreloadInfo;)Lcom/yandex/metrica/e$a;

    .line 135
    :cond_8
    invoke-virtual {p0}, Lcom/yandex/metrica/YandexMetricaConfig;->getErrorEnvironment()Ljava/util/Map;

    move-result-object v0

    .line 10353
    if-eqz v0, :cond_12

    move v0, v2

    .line 135
    :goto_9
    if-eqz v0, :cond_13

    .line 136
    invoke-virtual {p0}, Lcom/yandex/metrica/YandexMetricaConfig;->getErrorEnvironment()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_a
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 137
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v1, v0}, Lcom/yandex/metrica/e$a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/e$a;

    goto :goto_a

    :cond_9
    move v0, v3

    .line 1353
    goto/16 :goto_0

    :cond_a
    move v0, v3

    .line 2353
    goto/16 :goto_1

    :cond_b
    move v0, v3

    .line 3353
    goto/16 :goto_2

    :cond_c
    move v0, v3

    .line 4353
    goto/16 :goto_3

    :cond_d
    move v0, v3

    .line 5353
    goto/16 :goto_4

    :cond_e
    move v0, v3

    .line 6353
    goto/16 :goto_5

    :cond_f
    move v0, v3

    .line 7353
    goto :goto_6

    :cond_10
    move v0, v3

    .line 8353
    goto :goto_7

    :cond_11
    move v0, v3

    .line 9353
    goto :goto_8

    :cond_12
    move v0, v3

    .line 10353
    goto :goto_9

    .line 140
    :cond_13
    invoke-virtual {p0}, Lcom/yandex/metrica/YandexMetricaConfig;->isFirstActivationAsUpdate()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 141
    invoke-virtual {v4, v2}, Lcom/yandex/metrica/e$a;->e(Z)Lcom/yandex/metrica/e$a;

    .line 11150
    :cond_14
    instance-of v0, p0, Lcom/yandex/metrica/e;

    if-eqz v0, :cond_16

    .line 11151
    check-cast p0, Lcom/yandex/metrica/e;

    .line 11152
    invoke-virtual {p0}, Lcom/yandex/metrica/e;->b()Ljava/lang/String;

    move-result-object v0

    .line 11353
    if-eqz v0, :cond_17

    move v0, v2

    .line 11152
    :goto_b
    if-eqz v0, :cond_15

    .line 11153
    invoke-virtual {p0}, Lcom/yandex/metrica/e;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/yandex/metrica/e$a;->d(Ljava/lang/String;)Lcom/yandex/metrica/e$a;

    .line 11155
    :cond_15
    invoke-virtual {p0}, Lcom/yandex/metrica/e;->c()Ljava/util/List;

    move-result-object v0

    .line 12353
    if-eqz v0, :cond_18

    move v0, v2

    .line 11155
    :goto_c
    if-eqz v0, :cond_16

    .line 11156
    invoke-virtual {p0}, Lcom/yandex/metrica/e;->c()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/yandex/metrica/e$a;->a(Ljava/util/List;)Lcom/yandex/metrica/e$a;

    .line 146
    :cond_16
    return-object v4

    :cond_17
    move v0, v3

    .line 11353
    goto :goto_b

    :cond_18
    move v0, v3

    .line 12353
    goto :goto_c
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 592
    iget-object v0, p0, Lcom/yandex/metrica/e;->d:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 606
    iget-object v0, p0, Lcom/yandex/metrica/e;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/e;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/e;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 615
    iget-object v0, p0, Lcom/yandex/metrica/e;->e:Ljava/util/List;

    return-object v0
.end method

.method public d()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 627
    iget-object v0, p0, Lcom/yandex/metrica/e;->f:Ljava/lang/Integer;

    return-object v0
.end method

.method public e()Lcom/yandex/metrica/a;
    .locals 1

    .prologue
    .line 635
    iget-object v0, p0, Lcom/yandex/metrica/e;->a:Lcom/yandex/metrica/a;

    return-object v0
.end method

.method public f()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 644
    iget-object v0, p0, Lcom/yandex/metrica/e;->b:Ljava/util/Map;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 653
    iget-object v0, p0, Lcom/yandex/metrica/e;->c:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 664
    iget-object v0, p0, Lcom/yandex/metrica/e;->h:Ljava/lang/Integer;

    return-object v0
.end method

.method public i()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 675
    iget-object v0, p0, Lcom/yandex/metrica/e;->g:Ljava/lang/Integer;

    return-object v0
.end method

.method public j()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 687
    iget-object v0, p0, Lcom/yandex/metrica/e;->i:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method public k()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 695
    iget-object v0, p0, Lcom/yandex/metrica/e;->j:Ljava/lang/Boolean;

    return-object v0
.end method

.method public l()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 703
    iget-object v0, p0, Lcom/yandex/metrica/e;->k:Ljava/lang/Boolean;

    return-object v0
.end method
