.class public final Lcom/my/target/eh;
.super Landroid/widget/RelativeLayout;
.source "VerticalView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# static fields
.field private static final bC:I

.field private static final bZ:I

.field private static final ca:I

.field private static final cb:I


# instance fields
.field private final aW:Z

.field private final av:Lcom/my/target/bu;

.field private final aw:Lcom/my/target/cm;

.field private final bL:Landroid/widget/Button;

.field private final cc:Lcom/my/target/ea;

.field private final cd:Lcom/my/target/ec;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/eh;->bZ:I

    .line 35
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/eh;->ca:I

    .line 36
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/eh;->bC:I

    .line 37
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/eh;->cb:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/my/target/cm;Z)V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 48
    iput-object p2, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    .line 49
    iput-boolean p3, p0, Lcom/my/target/eh;->aW:Z

    .line 51
    new-instance v0, Lcom/my/target/ec;

    invoke-direct {v0, p1, p2, p3}, Lcom/my/target/ec;-><init>(Landroid/content/Context;Lcom/my/target/cm;Z)V

    iput-object v0, p0, Lcom/my/target/eh;->cd:Lcom/my/target/ec;

    .line 52
    iget-object v0, p0, Lcom/my/target/eh;->cd:Lcom/my/target/ec;

    const-string v1, "footer_layout"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 53
    new-instance v0, Lcom/my/target/ea;

    invoke-direct {v0, p1, p2, p3}, Lcom/my/target/ea;-><init>(Landroid/content/Context;Lcom/my/target/cm;Z)V

    iput-object v0, p0, Lcom/my/target/eh;->cc:Lcom/my/target/ea;

    .line 54
    iget-object v0, p0, Lcom/my/target/eh;->cc:Lcom/my/target/ea;

    const-string v1, "body_layout"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 55
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/eh;->bL:Landroid/widget/Button;

    .line 56
    iget-object v0, p0, Lcom/my/target/eh;->bL:Landroid/widget/Button;

    const-string v1, "cta_button"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 57
    new-instance v0, Lcom/my/target/bu;

    invoke-direct {v0, p1}, Lcom/my/target/bu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/eh;->av:Lcom/my/target/bu;

    .line 58
    iget-object v0, p0, Lcom/my/target/eh;->av:Lcom/my/target/bu;

    const-string v1, "age_bordering"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method static synthetic a(Lcom/my/target/eh;)Lcom/my/target/ea;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/my/target/eh;->cc:Lcom/my/target/ea;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/my/target/af;Landroid/view/View$OnClickListener;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .prologue
    .line 222
    iget-object v0, p0, Lcom/my/target/eh;->cc:Lcom/my/target/ea;

    invoke-virtual {v0, p1, p2}, Lcom/my/target/ea;->a(Lcom/my/target/af;Landroid/view/View$OnClickListener;)V

    .line 223
    iget-boolean v0, p1, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/my/target/eh;->bL:Landroid/widget/Button;

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 267
    :goto_0
    return-void

    .line 229
    :cond_0
    iget-boolean v0, p1, Lcom/my/target/af;->cy:Z

    if-eqz v0, :cond_1

    .line 231
    iget-object v0, p0, Lcom/my/target/eh;->bL:Landroid/widget/Button;

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 232
    iget-object v0, p0, Lcom/my/target/eh;->bL:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 239
    :goto_1
    iget-object v0, p0, Lcom/my/target/eh;->av:Lcom/my/target/bu;

    new-instance v1, Lcom/my/target/eh$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/my/target/eh$1;-><init>(Lcom/my/target/eh;Lcom/my/target/af;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Lcom/my/target/bu;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0

    .line 236
    :cond_1
    iget-object v0, p0, Lcom/my/target/eh;->bL:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 237
    iget-object v0, p0, Lcom/my/target/eh;->bL:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1
.end method

.method public final c(II)V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v10, 0x10

    const/4 v9, -0x1

    .line 63
    .line 64
    add-int v0, p1, p2

    const/16 v3, 0x500

    if-ge v0, v3, :cond_5

    move v0, v1

    .line 69
    :goto_0
    invoke-static {p2, p1}, Ljava/lang/Math;->max(II)I

    move-result v3

    div-int/lit8 v3, v3, 0x8

    .line 71
    iget-object v4, p0, Lcom/my/target/eh;->cc:Lcom/my/target/ea;

    invoke-virtual {v4, v0}, Lcom/my/target/ea;->e(Z)V

    .line 72
    iget-object v4, p0, Lcom/my/target/eh;->cd:Lcom/my/target/ec;

    invoke-virtual {v4}, Lcom/my/target/ec;->H()V

    .line 74
    new-instance v4, Landroid/view/View;

    invoke-virtual {p0}, Lcom/my/target/eh;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 75
    const v5, -0x555556

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundColor(I)V

    .line 76
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v5, v9, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 78
    invoke-virtual {v4, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 80
    iget-object v5, p0, Lcom/my/target/eh;->cd:Lcom/my/target/ec;

    sget v6, Lcom/my/target/eh;->ca:I

    invoke-virtual {v5, v6}, Lcom/my/target/ec;->setId(I)V

    .line 81
    iget-object v5, p0, Lcom/my/target/eh;->cd:Lcom/my/target/ec;

    invoke-virtual {v5, v3, v0}, Lcom/my/target/ec;->a(IZ)V

    .line 85
    iget-object v5, p0, Lcom/my/target/eh;->bL:Landroid/widget/Button;

    sget v6, Lcom/my/target/eh;->bC:I

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setId(I)V

    .line 86
    iget-object v5, p0, Lcom/my/target/eh;->bL:Landroid/widget/Button;

    iget-object v6, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    const/16 v7, 0xf

    invoke-virtual {v6, v7}, Lcom/my/target/cm;->n(I)I

    move-result v6

    iget-object v7, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    const/16 v8, 0xf

    invoke-virtual {v7, v8}, Lcom/my/target/cm;->n(I)I

    move-result v7

    invoke-virtual {v5, v6, v2, v7, v2}, Landroid/widget/Button;->setPadding(IIII)V

    .line 87
    iget-object v5, p0, Lcom/my/target/eh;->bL:Landroid/widget/Button;

    iget-object v6, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    const/16 v7, 0x64

    invoke-virtual {v6, v7}, Lcom/my/target/cm;->n(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setMinimumWidth(I)V

    .line 88
    iget-object v5, p0, Lcom/my/target/eh;->bL:Landroid/widget/Button;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 89
    iget-object v5, p0, Lcom/my/target/eh;->bL:Landroid/widget/Button;

    invoke-virtual {v5}, Landroid/widget/Button;->setSingleLine()V

    .line 90
    iget-object v5, p0, Lcom/my/target/eh;->bL:Landroid/widget/Button;

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 93
    iget-object v5, p0, Lcom/my/target/eh;->av:Lcom/my/target/bu;

    sget v6, Lcom/my/target/eh;->bZ:I

    invoke-virtual {v5, v6}, Lcom/my/target/bu;->setId(I)V

    .line 94
    iget-object v5, p0, Lcom/my/target/eh;->av:Lcom/my/target/bu;

    const v6, -0x777778

    invoke-virtual {v5, v1, v6}, Lcom/my/target/bu;->c(II)V

    .line 95
    iget-object v5, p0, Lcom/my/target/eh;->av:Lcom/my/target/bu;

    iget-object v6, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Lcom/my/target/cm;->n(I)I

    move-result v6

    invoke-virtual {v5, v6, v2, v2, v2}, Lcom/my/target/bu;->setPadding(IIII)V

    .line 96
    iget-object v2, p0, Lcom/my/target/eh;->av:Lcom/my/target/bu;

    const v5, -0x111112

    invoke-virtual {v2, v5}, Lcom/my/target/bu;->setTextColor(I)V

    .line 97
    iget-object v2, p0, Lcom/my/target/eh;->av:Lcom/my/target/bu;

    const/4 v5, 0x5

    invoke-virtual {v2, v5}, Lcom/my/target/bu;->setMaxEms(I)V

    .line 98
    iget-object v2, p0, Lcom/my/target/eh;->av:Lcom/my/target/bu;

    const v5, -0x111112

    iget-object v6, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Lcom/my/target/cm;->n(I)I

    move-result v6

    invoke-virtual {v2, v1, v5, v6}, Lcom/my/target/bu;->a(III)V

    .line 99
    iget-object v2, p0, Lcom/my/target/eh;->av:Lcom/my/target/bu;

    const/high16 v5, 0x66000000

    invoke-virtual {v2, v5}, Lcom/my/target/bu;->setBackgroundColor(I)V

    .line 101
    iget-object v2, p0, Lcom/my/target/eh;->cc:Lcom/my/target/ea;

    sget v5, Lcom/my/target/eh;->cb:I

    invoke-virtual {v2, v5}, Lcom/my/target/ea;->setId(I)V

    .line 102
    iget-object v2, p0, Lcom/my/target/eh;->cc:Lcom/my/target/ea;

    invoke-virtual {v2, v1}, Lcom/my/target/ea;->setOrientation(I)V

    .line 103
    iget-object v2, p0, Lcom/my/target/eh;->cc:Lcom/my/target/ea;

    const/16 v5, 0xe

    invoke-virtual {v2, v5}, Lcom/my/target/ea;->setGravity(I)V

    .line 104
    if-eqz v0, :cond_0

    .line 106
    iget-object v2, p0, Lcom/my/target/eh;->cc:Lcom/my/target/ea;

    iget-object v5, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    invoke-virtual {v5, v11}, Lcom/my/target/cm;->n(I)I

    move-result v5

    iget-object v6, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    .line 107
    invoke-virtual {v6, v11}, Lcom/my/target/cm;->n(I)I

    move-result v6

    iget-object v7, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    .line 108
    invoke-virtual {v7, v11}, Lcom/my/target/cm;->n(I)I

    move-result v7

    iget-object v8, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    .line 109
    invoke-virtual {v8, v11}, Lcom/my/target/cm;->n(I)I

    move-result v8

    .line 106
    invoke-virtual {v2, v5, v6, v7, v8}, Lcom/my/target/ea;->setPadding(IIII)V

    .line 118
    :goto_1
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 120
    const/4 v5, 0x2

    sget v6, Lcom/my/target/eh;->ca:I

    invoke-virtual {v2, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 121
    iget-object v5, p0, Lcom/my/target/eh;->cc:Lcom/my/target/ea;

    invoke-virtual {v5, v2}, Lcom/my/target/ea;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 123
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v5, -0x2

    const/4 v6, -0x2

    invoke-direct {v2, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 125
    iget-object v5, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    invoke-virtual {v5, v10}, Lcom/my/target/cm;->n(I)I

    move-result v5

    iget-object v6, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    .line 126
    invoke-virtual {v6, v10}, Lcom/my/target/cm;->n(I)I

    move-result v6

    iget-object v7, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    .line 127
    invoke-virtual {v7, v10}, Lcom/my/target/cm;->n(I)I

    move-result v7

    iget-object v8, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    .line 128
    invoke-virtual {v8, v11}, Lcom/my/target/cm;->n(I)I

    move-result v8

    .line 125
    invoke-virtual {v2, v5, v6, v7, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 129
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x11

    if-lt v5, v6, :cond_1

    .line 131
    const/16 v5, 0x15

    invoke-virtual {v2, v5, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 138
    :goto_2
    iget-object v5, p0, Lcom/my/target/eh;->av:Lcom/my/target/bu;

    invoke-virtual {v5, v2}, Lcom/my/target/bu;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 141
    iget-boolean v2, p0, Lcom/my/target/eh;->aW:Z

    if-eqz v2, :cond_2

    .line 143
    iget-object v2, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    const/16 v5, 0x40

    invoke-virtual {v2, v5}, Lcom/my/target/cm;->n(I)I

    move-result v2

    .line 149
    :goto_3
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x2

    invoke-direct {v5, v6, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 151
    const/16 v2, 0xe

    invoke-virtual {v5, v2, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 152
    const/16 v2, 0x8

    sget v6, Lcom/my/target/eh;->cb:I

    invoke-virtual {v5, v2, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 153
    if-eqz v0, :cond_3

    .line 155
    iget-object v0, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    const/16 v2, 0x34

    .line 156
    invoke-virtual {v0, v2}, Lcom/my/target/cm;->n(I)I

    move-result v0

    neg-int v0, v0

    iget-object v2, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    .line 157
    invoke-virtual {v2, v11}, Lcom/my/target/cm;->n(I)I

    move-result v2

    sub-int/2addr v0, v2

    iput v0, v5, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 164
    :goto_4
    iget-object v0, p0, Lcom/my/target/eh;->bL:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 166
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v9, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 168
    const/16 v2, 0xc

    invoke-virtual {v0, v2, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 169
    iget-object v2, p0, Lcom/my/target/eh;->cd:Lcom/my/target/ec;

    invoke-virtual {v2, v0}, Lcom/my/target/ec;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 171
    iget-object v0, p0, Lcom/my/target/eh;->cc:Lcom/my/target/ea;

    invoke-virtual {p0, v0}, Lcom/my/target/eh;->addView(Landroid/view/View;)V

    .line 172
    invoke-virtual {p0, v4}, Lcom/my/target/eh;->addView(Landroid/view/View;)V

    .line 174
    iget-object v0, p0, Lcom/my/target/eh;->av:Lcom/my/target/bu;

    invoke-virtual {p0, v0}, Lcom/my/target/eh;->addView(Landroid/view/View;)V

    .line 175
    iget-object v0, p0, Lcom/my/target/eh;->cd:Lcom/my/target/ec;

    invoke-virtual {p0, v0}, Lcom/my/target/eh;->addView(Landroid/view/View;)V

    .line 176
    iget-object v0, p0, Lcom/my/target/eh;->bL:Landroid/widget/Button;

    invoke-virtual {p0, v0}, Lcom/my/target/eh;->addView(Landroid/view/View;)V

    .line 178
    invoke-virtual {p0, v1}, Lcom/my/target/eh;->setClickable(Z)V

    .line 180
    iget-boolean v0, p0, Lcom/my/target/eh;->aW:Z

    if-eqz v0, :cond_4

    .line 182
    iget-object v0, p0, Lcom/my/target/eh;->bL:Landroid/widget/Button;

    const/4 v1, 0x2

    const/high16 v2, 0x42000000    # 32.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/Button;->setTextSize(IF)V

    .line 188
    :goto_5
    return-void

    .line 113
    :cond_0
    iget-object v2, p0, Lcom/my/target/eh;->cc:Lcom/my/target/ea;

    iget-object v5, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    invoke-virtual {v5, v10}, Lcom/my/target/cm;->n(I)I

    move-result v5

    iget-object v6, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    .line 114
    invoke-virtual {v6, v10}, Lcom/my/target/cm;->n(I)I

    move-result v6

    iget-object v7, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    .line 115
    invoke-virtual {v7, v10}, Lcom/my/target/cm;->n(I)I

    move-result v7

    iget-object v8, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    .line 116
    invoke-virtual {v8, v10}, Lcom/my/target/cm;->n(I)I

    move-result v8

    .line 113
    invoke-virtual {v2, v5, v6, v7, v8}, Lcom/my/target/ea;->setPadding(IIII)V

    goto/16 :goto_1

    .line 135
    :cond_1
    const/16 v5, 0xb

    invoke-virtual {v2, v5, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto/16 :goto_2

    .line 147
    :cond_2
    iget-object v2, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    const/16 v5, 0x34

    invoke-virtual {v2, v5}, Lcom/my/target/cm;->n(I)I

    move-result v2

    goto/16 :goto_3

    .line 161
    :cond_3
    iget-object v0, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    const/16 v2, 0x34

    invoke-virtual {v0, v2}, Lcom/my/target/cm;->n(I)I

    move-result v0

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, v5, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto :goto_4

    .line 186
    :cond_4
    iget-object v0, p0, Lcom/my/target/eh;->bL:Landroid/widget/Button;

    const/4 v1, 0x2

    const/high16 v2, 0x41b00000    # 22.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/Button;->setTextSize(IF)V

    goto :goto_5

    :cond_5
    move v0, v2

    goto/16 :goto_0
.end method

.method public final setBanner(Lcom/my/target/core/models/banners/h;)V
    .locals 6
    .param p1, "banner"    # Lcom/my/target/core/models/banners/h;

    .prologue
    .line 192
    iget-object v0, p0, Lcom/my/target/eh;->cc:Lcom/my/target/ea;

    invoke-virtual {v0, p1}, Lcom/my/target/ea;->setBanner(Lcom/my/target/core/models/banners/h;)V

    .line 193
    iget-object v0, p0, Lcom/my/target/eh;->cd:Lcom/my/target/ec;

    invoke-virtual {v0, p1}, Lcom/my/target/ec;->setBanner(Lcom/my/target/core/models/banners/h;)V

    .line 195
    iget-object v0, p0, Lcom/my/target/eh;->bL:Landroid/widget/Button;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getCtaText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v0, p0, Lcom/my/target/eh;->cd:Lcom/my/target/ec;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getFooterColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/ec;->setBackgroundColor(I)V

    .line 199
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getAgeRestrictions()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/my/target/eh;->av:Lcom/my/target/bu;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/my/target/bu;->setVisibility(I)V

    .line 208
    :goto_0
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getCtaButtonColor()I

    move-result v0

    .line 209
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getCtaButtonTouchColor()I

    move-result v1

    .line 210
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getCtaButtonTextColor()I

    move-result v2

    .line 212
    iget-object v3, p0, Lcom/my/target/eh;->bL:Landroid/widget/Button;

    iget-object v4, p0, Lcom/my/target/eh;->aw:Lcom/my/target/cm;

    const/4 v5, 0x2

    .line 215
    invoke-virtual {v4, v5}, Lcom/my/target/cm;->n(I)I

    move-result v4

    .line 212
    invoke-static {v3, v0, v1, v4}, Lcom/my/target/cm;->a(Landroid/view/View;III)V

    .line 216
    iget-object v0, p0, Lcom/my/target/eh;->bL:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTextColor(I)V

    .line 217
    return-void

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/my/target/eh;->av:Lcom/my/target/bu;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getAgeRestrictions()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/bu;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
