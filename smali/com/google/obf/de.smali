.class public final Lcom/google/obf/de;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/dj;


# instance fields
.field private final a:Lcom/google/obf/dj;

.field private final b:Lcom/google/obf/dj;

.field private final c:Lcom/google/obf/dj;

.field private final d:Lcom/google/obf/dj;

.field private e:Lcom/google/obf/dj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/obf/di;Lcom/google/obf/dj;)V
    .locals 1

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    invoke-static {p3}, Lcom/google/obf/dl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/dj;

    iput-object v0, p0, Lcom/google/obf/de;->a:Lcom/google/obf/dj;

    .line 5
    new-instance v0, Lcom/google/obf/df;

    invoke-direct {v0, p2}, Lcom/google/obf/df;-><init>(Lcom/google/obf/di;)V

    iput-object v0, p0, Lcom/google/obf/de;->b:Lcom/google/obf/dj;

    .line 6
    new-instance v0, Lcom/google/obf/cy;

    invoke-direct {v0, p1, p2}, Lcom/google/obf/cy;-><init>(Landroid/content/Context;Lcom/google/obf/di;)V

    iput-object v0, p0, Lcom/google/obf/de;->c:Lcom/google/obf/dj;

    .line 7
    new-instance v0, Lcom/google/obf/cz;

    invoke-direct {v0, p1, p2}, Lcom/google/obf/cz;-><init>(Landroid/content/Context;Lcom/google/obf/di;)V

    iput-object v0, p0, Lcom/google/obf/de;->d:Lcom/google/obf/dj;

    .line 8
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/obf/di;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    const/16 v4, 0x1f40

    .line 1
    new-instance v0, Lcom/google/obf/dd;

    const/4 v2, 0x0

    move-object v1, p3

    move-object v3, p2

    move v5, v4

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/obf/dd;-><init>(Ljava/lang/String;Lcom/google/obf/dx;Lcom/google/obf/di;IIZ)V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/obf/de;-><init>(Landroid/content/Context;Lcom/google/obf/di;Lcom/google/obf/dj;)V

    .line 2
    return-void
.end method


# virtual methods
.method public a([BII)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/obf/de;->e:Lcom/google/obf/dj;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/obf/dj;->a([BII)I

    move-result v0

    return v0
.end method

.method public a(Lcom/google/obf/db;)J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9
    iget-object v0, p0, Lcom/google/obf/de;->e:Lcom/google/obf/dj;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 10
    iget-object v0, p1, Lcom/google/obf/db;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 11
    iget-object v1, p1, Lcom/google/obf/db;->a:Landroid/net/Uri;

    invoke-static {v1}, Lcom/google/obf/ea;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 12
    iget-object v0, p1, Lcom/google/obf/db;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/android_asset/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 13
    iget-object v0, p0, Lcom/google/obf/de;->c:Lcom/google/obf/dj;

    iput-object v0, p0, Lcom/google/obf/de;->e:Lcom/google/obf/dj;

    .line 20
    :goto_1
    iget-object v0, p0, Lcom/google/obf/de;->e:Lcom/google/obf/dj;

    invoke-interface {v0, p1}, Lcom/google/obf/dj;->a(Lcom/google/obf/db;)J

    move-result-wide v0

    return-wide v0

    .line 9
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 14
    :cond_1
    iget-object v0, p0, Lcom/google/obf/de;->b:Lcom/google/obf/dj;

    iput-object v0, p0, Lcom/google/obf/de;->e:Lcom/google/obf/dj;

    goto :goto_1

    .line 15
    :cond_2
    const-string v1, "asset"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 16
    iget-object v0, p0, Lcom/google/obf/de;->c:Lcom/google/obf/dj;

    iput-object v0, p0, Lcom/google/obf/de;->e:Lcom/google/obf/dj;

    goto :goto_1

    .line 17
    :cond_3
    const-string v1, "content"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 18
    iget-object v0, p0, Lcom/google/obf/de;->d:Lcom/google/obf/dj;

    iput-object v0, p0, Lcom/google/obf/de;->e:Lcom/google/obf/dj;

    goto :goto_1

    .line 19
    :cond_4
    iget-object v0, p0, Lcom/google/obf/de;->a:Lcom/google/obf/dj;

    iput-object v0, p0, Lcom/google/obf/de;->e:Lcom/google/obf/dj;

    goto :goto_1
.end method

.method public a()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 22
    iget-object v0, p0, Lcom/google/obf/de;->e:Lcom/google/obf/dj;

    if-eqz v0, :cond_0

    .line 23
    :try_start_0
    iget-object v0, p0, Lcom/google/obf/de;->e:Lcom/google/obf/dj;

    invoke-interface {v0}, Lcom/google/obf/dj;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 24
    iput-object v1, p0, Lcom/google/obf/de;->e:Lcom/google/obf/dj;

    .line 27
    :cond_0
    return-void

    .line 26
    :catchall_0
    move-exception v0

    iput-object v1, p0, Lcom/google/obf/de;->e:Lcom/google/obf/dj;

    throw v0
.end method
