.class public Lru/cn/api/money_miner/replies/AdPredicate;
.super Ljava/lang/Object;
.source "AdPredicate.java"


# instance fields
.field public form:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "form"
    .end annotation
.end field

.field public parts:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "parts"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/money_miner/replies/AdPredicatePart;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static final synthetic lambda$matches$0$AdPredicate(Lru/cn/api/money_miner/replies/AdPredicatePart;)Z
    .locals 5
    .param p0, "part"    # Lru/cn/api/money_miner/replies/AdPredicatePart;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 30
    iget-object v4, p0, Lru/cn/api/money_miner/replies/AdPredicatePart;->negative:Ljava/util/List;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lru/cn/api/money_miner/replies/AdPredicatePart;->negative:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-eqz v4, :cond_2

    move v0, v2

    .line 31
    .local v0, "hasNegative":Z
    :goto_0
    iget-object v4, p0, Lru/cn/api/money_miner/replies/AdPredicatePart;->positive:Ljava/util/List;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lru/cn/api/money_miner/replies/AdPredicatePart;->positive:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-eqz v4, :cond_3

    move v1, v2

    .line 33
    .local v1, "hasPositive":Z
    :goto_1
    if-nez v1, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    move v3, v2

    :cond_1
    return v3

    .end local v0    # "hasNegative":Z
    .end local v1    # "hasPositive":Z
    :cond_2
    move v0, v3

    .line 30
    goto :goto_0

    .restart local v0    # "hasNegative":Z
    :cond_3
    move v1, v3

    .line 31
    goto :goto_1
.end method

.method static final synthetic lambda$matches$1$AdPredicate(Ljava/util/List;Lru/cn/api/money_miner/replies/AdPredicatePart;)Z
    .locals 1
    .param p0, "tags"    # Ljava/util/List;
    .param p1, "part"    # Lru/cn/api/money_miner/replies/AdPredicatePart;

    .prologue
    .line 43
    invoke-virtual {p1, p0}, Lru/cn/api/money_miner/replies/AdPredicatePart;->matchesConjuctive(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method static final synthetic lambda$matches$2$AdPredicate(Ljava/util/List;Lru/cn/api/money_miner/replies/AdPredicatePart;)Z
    .locals 1
    .param p0, "tags"    # Ljava/util/List;
    .param p1, "part"    # Lru/cn/api/money_miner/replies/AdPredicatePart;

    .prologue
    .line 47
    invoke-virtual {p1, p0}, Lru/cn/api/money_miner/replies/AdPredicatePart;->matchesDisjunctive(Ljava/util/List;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 55
    instance-of v3, p1, Lru/cn/api/money_miner/replies/AdPredicate;

    if-nez v3, :cond_1

    move v1, v2

    .line 62
    :cond_0
    :goto_0
    return v1

    .line 58
    :cond_1
    if-eq p0, p1, :cond_0

    move-object v0, p1

    .line 61
    check-cast v0, Lru/cn/api/money_miner/replies/AdPredicate;

    .line 62
    .local v0, "predicate":Lru/cn/api/money_miner/replies/AdPredicate;
    iget v3, v0, Lru/cn/api/money_miner/replies/AdPredicate;->form:I

    iget v4, p0, Lru/cn/api/money_miner/replies/AdPredicate;->form:I

    if-ne v3, v4, :cond_3

    invoke-virtual {v0}, Lru/cn/api/money_miner/replies/AdPredicate;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lru/cn/api/money_miner/replies/AdPredicate;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    iget-object v3, v0, Lru/cn/api/money_miner/replies/AdPredicate;->parts:Ljava/util/List;

    iget-object v4, p0, Lru/cn/api/money_miner/replies/AdPredicate;->parts:Ljava/util/List;

    .line 63
    invoke-interface {v3, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lru/cn/api/money_miner/replies/AdPredicate;->parts:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/api/money_miner/replies/AdPredicate;->parts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public matches(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/4 v1, 0x1

    .line 25
    invoke-virtual {p0}, Lru/cn/api/money_miner/replies/AdPredicate;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 50
    :cond_0
    :goto_0
    return v1

    .line 28
    :cond_1
    iget-object v2, p0, Lru/cn/api/money_miner/replies/AdPredicate;->parts:Ljava/util/List;

    invoke-static {v2}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v2

    sget-object v3, Lru/cn/api/money_miner/replies/AdPredicate$$Lambda$0;->$instance:Lcom/annimon/stream/function/Predicate;

    .line 29
    invoke-virtual {v2, v3}, Lcom/annimon/stream/Stream;->filter(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;

    move-result-object v2

    .line 35
    invoke-virtual {v2}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object v0

    .line 37
    .local v0, "predicateParts":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/money_miner/replies/AdPredicatePart;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 40
    iget v2, p0, Lru/cn/api/money_miner/replies/AdPredicate;->form:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 42
    :pswitch_0
    invoke-static {v0}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v1

    new-instance v2, Lru/cn/api/money_miner/replies/AdPredicate$$Lambda$1;

    invoke-direct {v2, p1}, Lru/cn/api/money_miner/replies/AdPredicate$$Lambda$1;-><init>(Ljava/util/List;)V

    .line 43
    invoke-virtual {v1, v2}, Lcom/annimon/stream/Stream;->allMatch(Lcom/annimon/stream/function/Predicate;)Z

    move-result v1

    goto :goto_0

    .line 46
    :pswitch_1
    invoke-static {v0}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v1

    new-instance v2, Lru/cn/api/money_miner/replies/AdPredicate$$Lambda$2;

    invoke-direct {v2, p1}, Lru/cn/api/money_miner/replies/AdPredicate$$Lambda$2;-><init>(Ljava/util/List;)V

    .line 47
    invoke-virtual {v1, v2}, Lcom/annimon/stream/Stream;->anyMatch(Lcom/annimon/stream/function/Predicate;)Z

    move-result v1

    goto :goto_0

    .line 40
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
