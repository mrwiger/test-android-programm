.class public final Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "PinCodeDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$PinCodeDialogListener;,
        Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;,
        Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$PinCodeDialogCallbacks;
    }
.end annotation


# instance fields
.field private currentPinCodeEditView:Landroid/widget/EditText;

.field private currentRealPinCode:Ljava/lang/String;

.field private dialogType:Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

.field private invalidCurrentPinView:Landroid/widget/TextView;

.field private listener:Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$PinCodeDialogListener;

.field private newPinView:Landroid/widget/TextView;

.field private okButtonView:Landroid/widget/Button;

.field private viewModel:Lru/cn/tv/stb/parental/pincode/PinCodeViewModel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->currentRealPinCode:Ljava/lang/String;

    return-void
.end method

.method private notifyCancel()V
    .locals 1

    .prologue
    .line 192
    invoke-virtual {p0}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->dismiss()V

    .line 193
    iget-object v0, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->listener:Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$PinCodeDialogListener;

    invoke-interface {v0}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$PinCodeDialogListener;->noButtonClick()V

    .line 194
    return-void
.end method

.method private notifySuccess()V
    .locals 1

    .prologue
    .line 187
    invoke-virtual {p0}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->dismiss()V

    .line 188
    iget-object v0, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->listener:Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$PinCodeDialogListener;

    invoke-interface {v0}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$PinCodeDialogListener;->okButtonClickDataValid()V

    .line 189
    return-void
.end method

.method private setPin(Landroid/util/Pair;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Throwable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Throwable;>;"
    const/4 v5, 0x1

    .line 197
    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->currentRealPinCode:Ljava/lang/String;

    .line 199
    iget-object v1, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->okButtonView:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 200
    sget-object v1, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$1;->$SwitchMap$ru$cn$tv$stb$parental$pincode$PinCodeDialogFragment$DialogType:[I

    iget-object v2, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->dialogType:Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    invoke-virtual {v2}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 206
    :goto_0
    return-void

    .line 202
    :pswitch_0
    const v1, 0x7f0e0107

    invoke-virtual {p0, v1}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 203
    .local v0, "newPin":Ljava/lang/String;
    iget-object v1, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->newPinView:Landroid/widget/TextView;

    const-string v2, "%1$s %2$s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    iget-object v4, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->currentRealPinCode:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 200
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$PinCodeDialogFragment(Landroid/util/Pair;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->setPin(Landroid/util/Pair;)V

    return-void
.end method

.method final synthetic lambda$onViewCreated$0$PinCodeDialogFragment(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 96
    invoke-direct {p0}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->notifyCancel()V

    return-void
.end method

.method final synthetic lambda$onViewCreated$1$PinCodeDialogFragment(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 105
    iget-object v1, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->currentPinCodeEditView:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 106
    .local v0, "enteredPinCode":Ljava/lang/String;
    const-string v1, "2712"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->currentRealPinCode:Ljava/lang/String;

    .line 107
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 108
    :cond_0
    invoke-direct {p0}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->notifySuccess()V

    .line 116
    :goto_0
    return-void

    .line 111
    :cond_1
    iget-object v1, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->invalidCurrentPinView:Landroid/widget/TextView;

    const v2, 0x7f0e0101

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 112
    iget-object v1, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->currentPinCodeEditView:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v1, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->invalidCurrentPinView:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 114
    iget-object v1, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->currentPinCodeEditView:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0
.end method

.method final synthetic lambda$onViewCreated$2$PinCodeDialogFragment(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 125
    invoke-direct {p0}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->notifySuccess()V

    return-void
.end method

.method final synthetic lambda$onViewCreated$3$PinCodeDialogFragment(Landroid/widget/EditText;Landroid/widget/EditText;Landroid/widget/EditText;Landroid/view/View;)V
    .locals 6
    .param p1, "currentPinEditText"    # Landroid/widget/EditText;
    .param p2, "newPinEditText"    # Landroid/widget/EditText;
    .param p3, "repeatNewEditText"    # Landroid/widget/EditText;
    .param p4, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    .line 137
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 138
    .local v0, "currentPinValue":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 139
    .local v2, "newPinValue":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 141
    .local v1, "newPinRepeatValue":Ljava/lang/String;
    iget-object v3, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->currentRealPinCode:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 142
    iget-object v3, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->invalidCurrentPinView:Landroid/widget/TextView;

    const v4, 0x7f0e0101

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 143
    const-string v3, ""

    invoke-virtual {p1, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 144
    iget-object v3, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->invalidCurrentPinView:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 145
    invoke-virtual {p1}, Landroid/widget/EditText;->requestFocus()Z

    .line 169
    :goto_0
    return-void

    .line 149
    :cond_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    if-ge v3, v4, :cond_1

    .line 150
    iget-object v3, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->invalidCurrentPinView:Landroid/widget/TextView;

    const v4, 0x7f0e0102

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 151
    const-string v3, ""

    invoke-virtual {p2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 152
    const-string v3, ""

    invoke-virtual {p3, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 153
    iget-object v3, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->invalidCurrentPinView:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 154
    invoke-virtual {p2}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    .line 158
    :cond_1
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 159
    iget-object v3, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->invalidCurrentPinView:Landroid/widget/TextView;

    const v4, 0x7f0e0103

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 160
    iget-object v3, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->invalidCurrentPinView:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 161
    const-string v3, ""

    invoke-virtual {p2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 162
    const-string v3, ""

    invoke-virtual {p3, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 163
    invoke-virtual {p2}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    .line 166
    :cond_2
    iget-object v3, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->viewModel:Lru/cn/tv/stb/parental/pincode/PinCodeViewModel;

    invoke-virtual {v3, v2}, Lru/cn/tv/stb/parental/pincode/PinCodeViewModel;->changePin(Ljava/lang/String;)V

    .line 167
    invoke-direct {p0}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->notifySuccess()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 58
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 60
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v0

    const-class v1, Lru/cn/tv/stb/parental/pincode/PinCodeViewModel;

    invoke-static {p0, v0, v1}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lru/cn/tv/stb/parental/pincode/PinCodeViewModel;

    iput-object v0, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->viewModel:Lru/cn/tv/stb/parental/pincode/PinCodeViewModel;

    .line 61
    iget-object v0, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->viewModel:Lru/cn/tv/stb/parental/pincode/PinCodeViewModel;

    invoke-virtual {v0}, Lru/cn/tv/stb/parental/pincode/PinCodeViewModel;->pinCode()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$$Lambda$0;-><init>(Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 62
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 66
    iget-object v1, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->dialogType:Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    if-nez v1, :cond_0

    .line 67
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/DialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 86
    :goto_0
    return-object v1

    .line 70
    :cond_0
    invoke-virtual {p0}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 72
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 73
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 75
    sget-object v1, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$1;->$SwitchMap$ru$cn$tv$stb$parental$pincode$PinCodeDialogFragment$DialogType:[I

    iget-object v2, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->dialogType:Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    invoke-virtual {v2}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 86
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/DialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    .line 77
    :pswitch_0
    const v1, 0x7f0c0084

    invoke-virtual {p1, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    .line 80
    :pswitch_1
    const v1, 0x7f0c0082

    invoke-virtual {p1, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    .line 83
    :pswitch_2
    const v1, 0x7f0c0083

    invoke-virtual {p1, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    .line 75
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v7, 0x7f0900fc

    const v6, 0x7f09008d

    .line 91
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/DialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 92
    const v4, 0x7f090156

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->okButtonView:Landroid/widget/Button;

    .line 93
    iget-object v4, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->okButtonView:Landroid/widget/Button;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 95
    const v4, 0x7f09014a

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 96
    .local v2, "noButtonView":Landroid/widget/Button;
    new-instance v4, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$$Lambda$1;

    invoke-direct {v4, p0}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$$Lambda$1;-><init>(Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;)V

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    sget-object v4, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$1;->$SwitchMap$ru$cn$tv$stb$parental$pincode$PinCodeDialogFragment$DialogType:[I

    iget-object v5, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->dialogType:Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    invoke-virtual {v5}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 176
    :goto_0
    return-void

    .line 100
    :pswitch_0
    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    iput-object v4, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->currentPinCodeEditView:Landroid/widget/EditText;

    .line 101
    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->invalidCurrentPinView:Landroid/widget/TextView;

    .line 103
    iget-object v4, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->okButtonView:Landroid/widget/Button;

    new-instance v5, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$$Lambda$2;

    invoke-direct {v5, p0}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$$Lambda$2;-><init>(Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 122
    :pswitch_1
    const v4, 0x7f0901a6

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->newPinView:Landroid/widget/TextView;

    .line 124
    iget-object v4, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->okButtonView:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->requestFocus()Z

    .line 125
    iget-object v4, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->okButtonView:Landroid/widget/Button;

    new-instance v5, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$$Lambda$3;

    invoke-direct {v5, p0}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$$Lambda$3;-><init>(Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 130
    :pswitch_2
    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 131
    .local v0, "currentPinEditText":Landroid/widget/EditText;
    const v4, 0x7f09013c

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 132
    .local v1, "newPinEditText":Landroid/widget/EditText;
    const v4, 0x7f09013d

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 133
    .local v3, "repeatNewEditText":Landroid/widget/EditText;
    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->invalidCurrentPinView:Landroid/widget/TextView;

    .line 135
    iget-object v4, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->okButtonView:Landroid/widget/Button;

    new-instance v5, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$$Lambda$4;

    invoke-direct {v5, p0, v0, v1, v3}, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$$Lambda$4;-><init>(Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;Landroid/widget/EditText;Landroid/widget/EditText;Landroid/widget/EditText;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 98
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setDialogType(Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;)V
    .locals 0
    .param p1, "dialogType"    # Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    .prologue
    .line 179
    iput-object p1, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->dialogType:Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$DialogType;

    .line 180
    return-void
.end method

.method public setPinCodeDialogListener(Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$PinCodeDialogListener;)V
    .locals 0
    .param p1, "l"    # Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$PinCodeDialogListener;

    .prologue
    .line 183
    iput-object p1, p0, Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment;->listener:Lru/cn/tv/stb/parental/pincode/PinCodeDialogFragment$PinCodeDialogListener;

    .line 184
    return-void
.end method
