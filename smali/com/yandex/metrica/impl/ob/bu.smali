.class public Lcom/yandex/metrica/impl/ob/bu;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ob/bu$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 74
    new-instance v0, Lcom/yandex/metrica/impl/utils/j;

    const-string v1, "YMM-CSE"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/utils/j;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/ob/bu;-><init>(Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 76
    return-void
.end method

.method constructor <init>(Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/bu;->a:Ljava/util/concurrent/ScheduledExecutorService;

    .line 81
    return-void
.end method


# virtual methods
.method public a()Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bu;->a:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method

.method public a(Lcom/yandex/metrica/impl/ob/cd;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bu;->a:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/yandex/metrica/impl/ob/bu$a;

    invoke-direct {v1, p1, p2}, Lcom/yandex/metrica/impl/ob/bu$a;-><init>(Lcom/yandex/metrica/impl/ob/cd;Landroid/os/Bundle;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 87
    return-void
.end method

.method public a(Lcom/yandex/metrica/impl/ob/cd;Landroid/os/Bundle;Lcom/yandex/metrica/impl/ob/cc;)V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bu;->a:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/yandex/metrica/impl/ob/bu$a;

    invoke-direct {v1, p1, p2, p3}, Lcom/yandex/metrica/impl/ob/bu$a;-><init>(Lcom/yandex/metrica/impl/ob/cd;Landroid/os/Bundle;Lcom/yandex/metrica/impl/ob/cc;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 94
    return-void
.end method
