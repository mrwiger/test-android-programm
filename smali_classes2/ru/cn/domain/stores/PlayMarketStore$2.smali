.class Lru/cn/domain/stores/PlayMarketStore$2;
.super Ljava/lang/Object;
.source "PlayMarketStore.java"

# interfaces
.implements Lru/cn/domain/stores/iabhelper/IabHelper$QueryInventoryFinishedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/domain/stores/PlayMarketStore;->queryInventory(Lru/cn/domain/stores/iabhelper/IabHelper;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/domain/stores/PlayMarketStore;

.field final synthetic val$inventoryQuery:Lru/cn/domain/stores/iabhelper/IabHelper;


# direct methods
.method constructor <init>(Lru/cn/domain/stores/PlayMarketStore;Lru/cn/domain/stores/iabhelper/IabHelper;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/domain/stores/PlayMarketStore;

    .prologue
    .line 47
    iput-object p1, p0, Lru/cn/domain/stores/PlayMarketStore$2;->this$0:Lru/cn/domain/stores/PlayMarketStore;

    iput-object p2, p0, Lru/cn/domain/stores/PlayMarketStore$2;->val$inventoryQuery:Lru/cn/domain/stores/iabhelper/IabHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method final synthetic lambda$onQueryInventoryFinished$0$PlayMarketStore$2(Ljava/util/List;)V
    .locals 1
    .param p1, "purchases"    # Ljava/util/List;

    .prologue
    .line 54
    iget-object v0, p0, Lru/cn/domain/stores/PlayMarketStore$2;->this$0:Lru/cn/domain/stores/PlayMarketStore;

    iget-object v0, v0, Lru/cn/domain/stores/PlayMarketStore;->persistor:Lru/cn/domain/stores/PurchasePersistor;

    invoke-virtual {v0, p1}, Lru/cn/domain/stores/PurchasePersistor;->savePurchases(Ljava/util/List;)V

    return-void
.end method

.method public onQueryInventoryFinished(Lru/cn/domain/stores/iabhelper/IabResult;Lru/cn/domain/stores/iabhelper/Inventory;)V
    .locals 3
    .param p1, "result"    # Lru/cn/domain/stores/iabhelper/IabResult;
    .param p2, "inventory"    # Lru/cn/domain/stores/iabhelper/Inventory;

    .prologue
    .line 50
    if-eqz p2, :cond_0

    .line 51
    invoke-virtual {p2}, Lru/cn/domain/stores/iabhelper/Inventory;->getAllPurchases()Ljava/util/List;

    move-result-object v0

    .line 52
    .local v0, "purchases":Ljava/util/List;, "Ljava/util/List<Lru/cn/domain/stores/iabhelper/Purchase;>;"
    iget-object v1, p0, Lru/cn/domain/stores/PlayMarketStore$2;->this$0:Lru/cn/domain/stores/PlayMarketStore;

    iget-object v1, v1, Lru/cn/domain/stores/PlayMarketStore;->LOG_TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-instance v2, Lru/cn/domain/stores/PlayMarketStore$2$$Lambda$0;

    invoke-direct {v2, p0, v0}, Lru/cn/domain/stores/PlayMarketStore$2$$Lambda$0;-><init>(Lru/cn/domain/stores/PlayMarketStore$2;Ljava/util/List;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 58
    .end local v0    # "purchases":Ljava/util/List;, "Ljava/util/List<Lru/cn/domain/stores/iabhelper/Purchase;>;"
    :cond_0
    iget-object v1, p0, Lru/cn/domain/stores/PlayMarketStore$2;->val$inventoryQuery:Lru/cn/domain/stores/iabhelper/IabHelper;

    invoke-virtual {v1}, Lru/cn/domain/stores/iabhelper/IabHelper;->dispose()V

    .line 59
    return-void
.end method
