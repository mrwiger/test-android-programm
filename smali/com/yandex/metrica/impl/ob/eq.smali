.class public Lcom/yandex/metrica/impl/ob/eq;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ob/eq$a;
    }
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljavax/net/ssl/SSLSocketFactory;

.field private d:Lcom/yandex/metrica/impl/ob/hm;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Lcom/yandex/metrica/impl/ob/eq$1;

    invoke-direct {v0, p0}, Lcom/yandex/metrica/impl/ob/eq$1;-><init>(Lcom/yandex/metrica/impl/ob/eq;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/eq;->d:Lcom/yandex/metrica/impl/ob/hm;

    .line 47
    return-void
.end method

.method public static a()Lcom/yandex/metrica/impl/ob/eq;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/yandex/metrica/impl/ob/eq$a;->a:Lcom/yandex/metrica/impl/ob/eq;

    return-object v0
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/ob/eq;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eq;->b:Ljava/lang/String;

    return-object v0
.end method

.method private static d()Ljava/security/cert/X509Certificate;
    .locals 2

    .prologue
    .line 113
    :try_start_0
    invoke-static {}, Lcom/yandex/metrica/impl/ob/a;->a()[Ljava/lang/String;

    move-result-object v0

    .line 114
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 115
    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/ha;->a(Ljava/lang/String;)Ljava/security/cert/X509Certificate;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 120
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 69
    monitor-enter p0

    .line 1094
    :try_start_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1098
    const-string v2, "https://certificate.mobile.yandex.net/api/v1/pins"

    move-object v3, v2

    .line 1107
    :goto_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/eq;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/eq;->a:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 70
    :goto_2
    if-eqz v0, :cond_0

    .line 71
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/eq;->b:Ljava/lang/String;

    .line 72
    iput-object v3, p0, Lcom/yandex/metrica/impl/ob/eq;->a:Ljava/lang/String;

    .line 73
    new-instance v0, Lcom/yandex/metrica/impl/ob/hg;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/eq;->d:Lcom/yandex/metrica/impl/ob/hm;

    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/yandex/metrica/impl/ob/hg;-><init>(Lcom/yandex/metrica/impl/ob/hm;ZZ)V

    .line 74
    invoke-static {}, Lcom/yandex/metrica/impl/ob/eq;->d()Ljava/security/cert/X509Certificate;

    move-result-object v1

    .line 76
    if-eqz v1, :cond_0

    .line 77
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 78
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/eq;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/hg;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 81
    new-instance v1, Lcom/yandex/metrica/impl/ob/gz;

    invoke-direct {v1, p1, v0}, Lcom/yandex/metrica/impl/ob/gz;-><init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/hg;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    :try_start_1
    new-instance v0, Lcom/yandex/metrica/impl/ob/hc;

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/hc;-><init>(Lcom/yandex/metrica/impl/ob/hn;)V

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/hc;->a()Ljavax/net/ssl/SSLContext;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/eq;->c:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90
    :cond_0
    :goto_3
    monitor-exit p0

    return-void

    .line 1101
    :cond_1
    :try_start_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/api/v1/pins"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    move-object v3, v2

    goto :goto_0

    :cond_2
    move v2, v1

    .line 1107
    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_3
.end method

.method public declared-synchronized b()Ljavax/net/ssl/SSLSocketFactory;
    .locals 1

    .prologue
    .line 61
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eq;->c:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()Z
    .locals 1

    .prologue
    .line 65
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eq;->c:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
