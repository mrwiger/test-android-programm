.class final Lcom/my/target/core/engines/i$a;
.super Ljava/lang/Object;
.source "InstreamAudioAdEngine.java"

# interfaces
.implements Lcom/my/target/core/controllers/c$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/core/engines/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic D:Lcom/my/target/core/engines/i;


# direct methods
.method private constructor <init>(Lcom/my/target/core/engines/i;)V
    .locals 0

    .prologue
    .line 496
    iput-object p1, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/my/target/core/engines/i;B)V
    .locals 0

    .prologue
    .line 496
    invoke-direct {p0, p1}, Lcom/my/target/core/engines/i$a;-><init>(Lcom/my/target/core/engines/i;)V

    return-void
.end method


# virtual methods
.method public final onBannerCompleted(Lcom/my/target/aj;)V
    .locals 3
    .param p1, "banner"    # Lcom/my/target/aj;

    .prologue
    .line 543
    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->a(Lcom/my/target/core/engines/i;)Lcom/my/target/al;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->b(Lcom/my/target/core/engines/i;)Lcom/my/target/aj;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->c(Lcom/my/target/core/engines/i;)Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;

    move-result-object v0

    if-nez v0, :cond_1

    .line 553
    :cond_0
    :goto_0
    return-void

    .line 547
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->g(Lcom/my/target/core/engines/i;)Lcom/my/target/instreamads/InstreamAudioAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/instreamads/InstreamAudioAd;->getListener()Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;

    move-result-object v0

    .line 548
    if-eqz v0, :cond_2

    .line 550
    iget-object v1, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v1}, Lcom/my/target/core/engines/i;->g(Lcom/my/target/core/engines/i;)Lcom/my/target/instreamads/InstreamAudioAd;

    move-result-object v1

    iget-object v2, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v2}, Lcom/my/target/core/engines/i;->c(Lcom/my/target/core/engines/i;)Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;->onBannerComplete(Lcom/my/target/instreamads/InstreamAudioAd;Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;)V

    .line 552
    :cond_2
    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->h(Lcom/my/target/core/engines/i;)V

    goto :goto_0
.end method

.method public final onBannerError(Ljava/lang/String;Lcom/my/target/aj;)V
    .locals 2
    .param p1, "error"    # Ljava/lang/String;
    .param p2, "banner"    # Lcom/my/target/aj;

    .prologue
    .line 572
    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->a(Lcom/my/target/core/engines/i;)Lcom/my/target/al;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->b(Lcom/my/target/core/engines/i;)Lcom/my/target/aj;

    move-result-object v0

    if-eq v0, p2, :cond_1

    .line 582
    :cond_0
    :goto_0
    return-void

    .line 576
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->g(Lcom/my/target/core/engines/i;)Lcom/my/target/instreamads/InstreamAudioAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/instreamads/InstreamAudioAd;->getListener()Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;

    move-result-object v0

    .line 577
    if-eqz v0, :cond_2

    .line 579
    iget-object v1, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v1}, Lcom/my/target/core/engines/i;->g(Lcom/my/target/core/engines/i;)Lcom/my/target/instreamads/InstreamAudioAd;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;->onError(Ljava/lang/String;Lcom/my/target/instreamads/InstreamAudioAd;)V

    .line 581
    :cond_2
    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->h(Lcom/my/target/core/engines/i;)V

    goto :goto_0
.end method

.method public final onBannerProgressChanged(FFLcom/my/target/aj;)V
    .locals 2
    .param p1, "timeLeft"    # F
    .param p2, "duration"    # F
    .param p3, "banner"    # Lcom/my/target/aj;

    .prologue
    .line 558
    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->a(Lcom/my/target/core/engines/i;)Lcom/my/target/al;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->b(Lcom/my/target/core/engines/i;)Lcom/my/target/aj;

    move-result-object v0

    if-ne v0, p3, :cond_0

    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->c(Lcom/my/target/core/engines/i;)Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;

    move-result-object v0

    if-nez v0, :cond_1

    .line 567
    :cond_0
    :goto_0
    return-void

    .line 562
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->g(Lcom/my/target/core/engines/i;)Lcom/my/target/instreamads/InstreamAudioAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/instreamads/InstreamAudioAd;->getListener()Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;

    move-result-object v0

    .line 563
    if-eqz v0, :cond_0

    .line 565
    iget-object v1, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v1}, Lcom/my/target/core/engines/i;->g(Lcom/my/target/core/engines/i;)Lcom/my/target/instreamads/InstreamAudioAd;

    move-result-object v1

    invoke-interface {v0, p1, p2, v1}, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;->onBannerTimeLeftChange(FFLcom/my/target/instreamads/InstreamAudioAd;)V

    goto :goto_0
.end method

.method public final onBannerStarted(Lcom/my/target/aj;)V
    .locals 3
    .param p1, "banner"    # Lcom/my/target/aj;

    .prologue
    .line 501
    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->a(Lcom/my/target/core/engines/i;)Lcom/my/target/al;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->b(Lcom/my/target/core/engines/i;)Lcom/my/target/aj;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->c(Lcom/my/target/core/engines/i;)Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;

    move-result-object v0

    if-nez v0, :cond_1

    .line 524
    :cond_0
    :goto_0
    return-void

    .line 505
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->d(Lcom/my/target/core/engines/i;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 507
    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->e(Lcom/my/target/core/engines/i;)Z

    .line 508
    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->f(Lcom/my/target/core/engines/i;)Lcom/my/target/core/controllers/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/core/controllers/c;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 509
    if-nez v0, :cond_3

    .line 511
    const-string v0, "can\'t send stat: context is null"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 519
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->g(Lcom/my/target/core/engines/i;)Lcom/my/target/instreamads/InstreamAudioAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/instreamads/InstreamAudioAd;->getListener()Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;

    move-result-object v0

    .line 520
    if-eqz v0, :cond_0

    .line 522
    iget-object v1, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v1}, Lcom/my/target/core/engines/i;->g(Lcom/my/target/core/engines/i;)Lcom/my/target/instreamads/InstreamAudioAd;

    move-result-object v1

    iget-object v2, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v2}, Lcom/my/target/core/engines/i;->c(Lcom/my/target/core/engines/i;)Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;->onBannerStart(Lcom/my/target/instreamads/InstreamAudioAd;Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;)V

    goto :goto_0

    .line 515
    :cond_3
    iget-object v1, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v1}, Lcom/my/target/core/engines/i;->a(Lcom/my/target/core/engines/i;)Lcom/my/target/al;

    move-result-object v1

    const-string v2, "impression"

    invoke-virtual {v1, v2}, Lcom/my/target/al;->p(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    goto :goto_1
.end method

.method public final onBannerStopped(Lcom/my/target/aj;)V
    .locals 3
    .param p1, "banner"    # Lcom/my/target/aj;

    .prologue
    .line 529
    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->a(Lcom/my/target/core/engines/i;)Lcom/my/target/al;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->b(Lcom/my/target/core/engines/i;)Lcom/my/target/aj;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->c(Lcom/my/target/core/engines/i;)Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;

    move-result-object v0

    if-nez v0, :cond_1

    .line 538
    :cond_0
    :goto_0
    return-void

    .line 533
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v0}, Lcom/my/target/core/engines/i;->g(Lcom/my/target/core/engines/i;)Lcom/my/target/instreamads/InstreamAudioAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/instreamads/InstreamAudioAd;->getListener()Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;

    move-result-object v0

    .line 534
    if-eqz v0, :cond_0

    .line 536
    iget-object v1, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v1}, Lcom/my/target/core/engines/i;->g(Lcom/my/target/core/engines/i;)Lcom/my/target/instreamads/InstreamAudioAd;

    move-result-object v1

    iget-object v2, p0, Lcom/my/target/core/engines/i$a;->D:Lcom/my/target/core/engines/i;

    invoke-static {v2}, Lcom/my/target/core/engines/i;->c(Lcom/my/target/core/engines/i;)Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdListener;->onBannerComplete(Lcom/my/target/instreamads/InstreamAudioAd;Lcom/my/target/instreamads/InstreamAudioAd$InstreamAudioAdBanner;)V

    goto :goto_0
.end method
