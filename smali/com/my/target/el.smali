.class public final Lcom/my/target/el;
.super Landroid/widget/RelativeLayout;
.source "CardVerticalView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static final az:I

.field private static final bC:I

.field private static final bD:I

.field private static final bE:I

.field private static final bF:I

.field private static final bf:I


# instance fields
.field private final aU:Lcom/my/target/ca;

.field private final aX:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final aw:Lcom/my/target/cm;

.field private final bL:Landroid/widget/Button;

.field private be:Landroid/view/View$OnClickListener;

.field private final cG:Lcom/my/target/bv;

.field private final cH:Landroid/widget/TextView;

.field private final cI:Landroid/widget/TextView;

.field private final cK:Landroid/widget/RelativeLayout;

.field private final cn:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/el;->bC:I

    .line 31
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/el;->bf:I

    .line 32
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/el;->az:I

    .line 33
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/el;->bD:I

    .line 34
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/el;->bF:I

    .line 35
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/el;->bE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/16 v9, 0xc

    const/4 v8, -0x1

    const/4 v7, 0x1

    const/4 v6, -0x2

    .line 52
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/my/target/el;->aX:Ljava/util/HashMap;

    .line 53
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    .line 54
    new-instance v0, Lcom/my/target/bv;

    invoke-direct {v0, p1}, Lcom/my/target/bv;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/el;->cG:Lcom/my/target/bv;

    .line 55
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/el;->cH:Landroid/widget/TextView;

    .line 56
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/el;->cI:Landroid/widget/TextView;

    .line 57
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/el;->bL:Landroid/widget/Button;

    .line 58
    new-instance v0, Lcom/my/target/ca;

    invoke-direct {v0, p1}, Lcom/my/target/ca;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/el;->aU:Lcom/my/target/ca;

    .line 59
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/el;->cn:Landroid/widget/TextView;

    .line 60
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/el;->cK:Landroid/widget/RelativeLayout;

    .line 1195
    const/4 v1, 0x0

    const/4 v2, 0x0

    const v3, -0x333334

    iget-object v0, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    invoke-virtual {v0, v7}, Lcom/my/target/cm;->n(I)I

    move-result v4

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/my/target/cm;->a(Landroid/view/View;IIIII)V

    .line 1196
    iget-object v0, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    invoke-virtual {v0, v7}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iget-object v1, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v7}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    invoke-virtual {v2, v7}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/my/target/el;->setPadding(IIII)V

    .line 1198
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1199
    const/4 v1, 0x3

    sget v2, Lcom/my/target/el;->bf:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1200
    iget-object v1, p0, Lcom/my/target/el;->cK:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1202
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v8, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1203
    iget-object v1, p0, Lcom/my/target/el;->cG:Lcom/my/target/bv;

    invoke-virtual {v1, v0}, Lcom/my/target/bv;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1204
    iget-object v0, p0, Lcom/my/target/el;->cG:Lcom/my/target/bv;

    sget v1, Lcom/my/target/el;->bf:I

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setId(I)V

    .line 1206
    iget-object v0, p0, Lcom/my/target/el;->bL:Landroid/widget/Button;

    sget v1, Lcom/my/target/el;->bC:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setId(I)V

    .line 1207
    iget-object v0, p0, Lcom/my/target/el;->bL:Landroid/widget/Button;

    iget-object v1, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    const/16 v2, 0xf

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    const/16 v4, 0xf

    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Lcom/my/target/cm;->n(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/Button;->setPadding(IIII)V

    .line 1208
    iget-object v0, p0, Lcom/my/target/el;->bL:Landroid/widget/Button;

    iget-object v1, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setMinimumWidth(I)V

    .line 1209
    iget-object v0, p0, Lcom/my/target/el;->bL:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1210
    iget-object v0, p0, Lcom/my/target/el;->bL:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->setSingleLine()V

    .line 1211
    iget-object v0, p0, Lcom/my/target/el;->bL:Landroid/widget/Button;

    const/high16 v1, 0x41900000    # 18.0f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextSize(F)V

    .line 1212
    iget-object v0, p0, Lcom/my/target/el;->bL:Landroid/widget/Button;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1213
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 1215
    iget-object v0, p0, Lcom/my/target/el;->bL:Landroid/widget/Button;

    iget-object v1, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v10}, Lcom/my/target/cm;->n(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setElevation(F)V

    .line 1218
    :cond_0
    iget-object v0, p0, Lcom/my/target/el;->bL:Landroid/widget/Button;

    const v1, -0xff540e

    const v2, -0xff8957

    iget-object v3, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    invoke-virtual {v3, v10}, Lcom/my/target/cm;->n(I)I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/my/target/cm;->a(Landroid/view/View;III)V

    .line 1219
    iget-object v0, p0, Lcom/my/target/el;->bL:Landroid/widget/Button;

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setTextColor(I)V

    .line 1221
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1223
    invoke-virtual {v0, v9, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1224
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1226
    iget-object v1, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v9}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    .line 1227
    invoke-virtual {v2, v9}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    .line 1228
    invoke-virtual {v3, v9}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    .line 1229
    invoke-virtual {v4, v9}, Lcom/my/target/cm;->n(I)I

    move-result v4

    .line 1226
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1230
    iget-object v1, p0, Lcom/my/target/el;->bL:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1232
    iget-object v0, p0, Lcom/my/target/el;->cH:Landroid/widget/TextView;

    sget v1, Lcom/my/target/el;->az:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 1233
    iget-object v0, p0, Lcom/my/target/el;->cH:Landroid/widget/TextView;

    const/high16 v1, 0x41900000    # 18.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 1234
    iget-object v0, p0, Lcom/my/target/el;->cH:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1235
    iget-object v0, p0, Lcom/my/target/el;->cH:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1236
    iget-object v0, p0, Lcom/my/target/el;->cH:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1237
    iget-object v0, p0, Lcom/my/target/el;->cH:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1238
    iget-object v0, p0, Lcom/my/target/el;->cH:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v9}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    invoke-virtual {v4, v7}, Lcom/my/target/cm;->n(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1239
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1240
    iget-object v1, p0, Lcom/my/target/el;->cH:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1242
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/my/target/el;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1243
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1244
    const/4 v2, 0x3

    sget v3, Lcom/my/target/el;->az:I

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1245
    sget v2, Lcom/my/target/el;->bF:I

    invoke-virtual {v1, v10, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1246
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1248
    iget-object v1, p0, Lcom/my/target/el;->cI:Landroid/widget/TextView;

    sget v2, Lcom/my/target/el;->bD:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setId(I)V

    .line 1249
    iget-object v1, p0, Lcom/my/target/el;->cI:Landroid/widget/TextView;

    const v2, -0x777778

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1250
    iget-object v1, p0, Lcom/my/target/el;->cI:Landroid/widget/TextView;

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1251
    iget-object v1, p0, Lcom/my/target/el;->cI:Landroid/widget/TextView;

    const/high16 v2, 0x41900000    # 18.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 1252
    iget-object v1, p0, Lcom/my/target/el;->cI:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1253
    iget-object v1, p0, Lcom/my/target/el;->cI:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    invoke-virtual {v2, v9}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    invoke-virtual {v4, v7}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v5, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    invoke-virtual {v5, v9}, Lcom/my/target/cm;->n(I)I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1254
    iget-object v1, p0, Lcom/my/target/el;->cI:Landroid/widget/TextView;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1256
    iget-object v1, p0, Lcom/my/target/el;->aU:Lcom/my/target/ca;

    sget v2, Lcom/my/target/el;->bF:I

    invoke-virtual {v1, v2}, Lcom/my/target/ca;->setId(I)V

    .line 1257
    iget-object v1, p0, Lcom/my/target/el;->aU:Lcom/my/target/ca;

    iget-object v2, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    const/16 v3, 0x12

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/my/target/ca;->setStarSize(I)V

    .line 1258
    iget-object v1, p0, Lcom/my/target/el;->aU:Lcom/my/target/ca;

    iget-object v2, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/my/target/ca;->setStarsPadding(F)V

    .line 1259
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1260
    sget v2, Lcom/my/target/el;->bC:I

    invoke-virtual {v1, v10, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1261
    const/16 v2, 0xe

    invoke-virtual {v1, v2, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1262
    iget-object v2, p0, Lcom/my/target/el;->aU:Lcom/my/target/ca;

    invoke-virtual {v2, v1}, Lcom/my/target/ca;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1264
    iget-object v1, p0, Lcom/my/target/el;->cn:Landroid/widget/TextView;

    sget v2, Lcom/my/target/el;->bE:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setId(I)V

    .line 1265
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1267
    iget-object v2, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1268
    iget-object v2, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1269
    sget v2, Lcom/my/target/el;->bC:I

    invoke-virtual {v1, v10, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1270
    const/16 v2, 0xe

    invoke-virtual {v1, v2, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1271
    iget-object v2, p0, Lcom/my/target/el;->cn:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1273
    iget-object v1, p0, Lcom/my/target/el;->cG:Lcom/my/target/bv;

    invoke-virtual {p0, v1}, Lcom/my/target/el;->addView(Landroid/view/View;)V

    .line 1274
    iget-object v1, p0, Lcom/my/target/el;->cK:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v1}, Lcom/my/target/el;->addView(Landroid/view/View;)V

    .line 1275
    iget-object v1, p0, Lcom/my/target/el;->cK:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/my/target/el;->cH:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1276
    iget-object v1, p0, Lcom/my/target/el;->cI:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1277
    iget-object v1, p0, Lcom/my/target/el;->cK:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1278
    iget-object v0, p0, Lcom/my/target/el;->cK:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/my/target/el;->bL:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1279
    iget-object v0, p0, Lcom/my/target/el;->cK:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/my/target/el;->aU:Lcom/my/target/ca;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1280
    iget-object v0, p0, Lcom/my/target/el;->cK:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/my/target/el;->cn:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 62
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;Lcom/my/target/af;)V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 103
    iput-object p1, p0, Lcom/my/target/el;->be:Landroid/view/View$OnClickListener;

    .line 104
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 106
    :cond_0
    invoke-super {p0, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    iget-object v0, p0, Lcom/my/target/el;->bL:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    :goto_0
    return-void

    .line 111
    :cond_1
    invoke-virtual {p0, p0}, Lcom/my/target/el;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 112
    iget-object v0, p0, Lcom/my/target/el;->cK:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 113
    iget-object v0, p0, Lcom/my/target/el;->cG:Lcom/my/target/bv;

    invoke-virtual {v0, p0}, Lcom/my/target/bv;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 114
    iget-object v0, p0, Lcom/my/target/el;->cH:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 115
    iget-object v0, p0, Lcom/my/target/el;->cI:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 116
    iget-object v0, p0, Lcom/my/target/el;->aU:Lcom/my/target/ca;

    invoke-virtual {v0, p0}, Lcom/my/target/ca;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 117
    iget-object v0, p0, Lcom/my/target/el;->cn:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 118
    iget-object v0, p0, Lcom/my/target/el;->bL:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 120
    iget-object v3, p0, Lcom/my/target/el;->aX:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/my/target/el;->cG:Lcom/my/target/bv;

    iget-boolean v0, p2, Lcom/my/target/af;->cv:Z

    if-nez v0, :cond_2

    iget-boolean v0, p2, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_b

    :cond_2
    move v0, v2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    iget-object v3, p0, Lcom/my/target/el;->aX:Ljava/util/HashMap;

    iget-boolean v0, p2, Lcom/my/target/af;->cD:Z

    if-nez v0, :cond_3

    iget-boolean v0, p2, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_c

    :cond_3
    move v0, v2

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    iget-object v3, p0, Lcom/my/target/el;->aX:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/my/target/el;->cK:Landroid/widget/RelativeLayout;

    iget-boolean v0, p2, Lcom/my/target/af;->cD:Z

    if-nez v0, :cond_4

    iget-boolean v0, p2, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_d

    :cond_4
    move v0, v2

    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    iget-object v3, p0, Lcom/my/target/el;->aX:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/my/target/el;->cH:Landroid/widget/TextView;

    iget-boolean v0, p2, Lcom/my/target/af;->cs:Z

    if-nez v0, :cond_5

    iget-boolean v0, p2, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_e

    :cond_5
    move v0, v2

    :goto_4
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    iget-object v3, p0, Lcom/my/target/el;->aX:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/my/target/el;->cI:Landroid/widget/TextView;

    iget-boolean v0, p2, Lcom/my/target/af;->ct:Z

    if-nez v0, :cond_6

    iget-boolean v0, p2, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_f

    :cond_6
    move v0, v2

    :goto_5
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    iget-object v3, p0, Lcom/my/target/el;->aX:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/my/target/el;->aU:Lcom/my/target/ca;

    iget-boolean v0, p2, Lcom/my/target/af;->cw:Z

    if-nez v0, :cond_7

    iget-boolean v0, p2, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_10

    :cond_7
    move v0, v2

    :goto_6
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    iget-object v3, p0, Lcom/my/target/el;->aX:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/my/target/el;->cn:Landroid/widget/TextView;

    iget-boolean v0, p2, Lcom/my/target/af;->cB:Z

    if-nez v0, :cond_8

    iget-boolean v0, p2, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_11

    :cond_8
    move v0, v2

    :goto_7
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    iget-object v0, p0, Lcom/my/target/el;->aX:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/my/target/el;->bL:Landroid/widget/Button;

    iget-boolean v4, p2, Lcom/my/target/af;->cy:Z

    if-nez v4, :cond_9

    iget-boolean v4, p2, Lcom/my/target/af;->cE:Z

    if-eqz v4, :cond_a

    :cond_9
    move v1, v2

    :cond_a
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_b
    move v0, v1

    .line 120
    goto/16 :goto_1

    :cond_c
    move v0, v1

    .line 121
    goto/16 :goto_2

    :cond_d
    move v0, v1

    .line 122
    goto :goto_3

    :cond_e
    move v0, v1

    .line 123
    goto :goto_4

    :cond_f
    move v0, v1

    .line 124
    goto :goto_5

    :cond_10
    move v0, v1

    .line 125
    goto :goto_6

    :cond_11
    move v0, v1

    .line 126
    goto :goto_7
.end method

.method public final getCacheImageView()Lcom/my/target/bv;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/my/target/el;->cG:Lcom/my/target/bv;

    return-object v0
.end method

.method public final getCtaButtonView()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/my/target/el;->bL:Landroid/widget/Button;

    return-object v0
.end method

.method public final getDescriptionTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/my/target/el;->cI:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getDomainTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/my/target/el;->cn:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getRatingView()Lcom/my/target/ca;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/my/target/el;->aU:Lcom/my/target/ca;

    return-object v0
.end method

.method public final getTitleTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/my/target/el;->cH:Landroid/widget/TextView;

    return-object v0
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const v3, -0x333334

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 133
    iget-object v0, p0, Lcom/my/target/el;->aX:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 190
    :goto_0
    return v1

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/my/target/el;->aX:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 139
    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    .line 141
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_1
    :goto_1
    :pswitch_0
    move v1, v6

    .line 190
    goto :goto_0

    .line 144
    :pswitch_1
    if-eqz v0, :cond_1

    .line 146
    iget-object v0, p0, Lcom/my/target/el;->bL:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    .line 148
    iget-object v0, p0, Lcom/my/target/el;->bL:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setPressed(Z)V

    goto :goto_1

    .line 152
    :cond_2
    const v0, -0x3a1508

    invoke-virtual {p0, v0}, Lcom/my/target/el;->setBackgroundColor(I)V

    goto :goto_1

    .line 157
    :pswitch_2
    iget-object v2, p0, Lcom/my/target/el;->be:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_3

    .line 159
    iget-object v2, p0, Lcom/my/target/el;->be:Landroid/view/View$OnClickListener;

    invoke-interface {v2, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 161
    :cond_3
    if-eqz v0, :cond_1

    .line 163
    iget-object v0, p0, Lcom/my/target/el;->bL:Landroid/widget/Button;

    if-ne p1, v0, :cond_4

    .line 165
    iget-object v0, p0, Lcom/my/target/el;->bL:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setPressed(Z)V

    goto :goto_1

    .line 169
    :cond_4
    iget-object v0, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    invoke-virtual {v0, v6}, Lcom/my/target/cm;->n(I)I

    move-result v4

    move-object v0, p0

    move v2, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Lcom/my/target/cm;->a(Landroid/view/View;IIIII)V

    goto :goto_1

    .line 174
    :pswitch_3
    if-eqz v0, :cond_1

    .line 176
    iget-object v0, p0, Lcom/my/target/el;->bL:Landroid/widget/Button;

    if-ne p1, v0, :cond_5

    .line 178
    iget-object v0, p0, Lcom/my/target/el;->bL:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setPressed(Z)V

    goto :goto_1

    .line 182
    :cond_5
    iget-object v0, p0, Lcom/my/target/el;->aw:Lcom/my/target/cm;

    invoke-virtual {v0, v6}, Lcom/my/target/cm;->n(I)I

    move-result v4

    move-object v0, p0

    move v2, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Lcom/my/target/cm;->a(Landroid/view/View;IIIII)V

    goto :goto_1

    .line 141
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
