.class final Lru/cn/ad/video/yandex/YandexVideoAdapter$YMAMediaPredicate;
.super Ljava/lang/Object;
.source "YandexVideoAdapter.java"

# interfaces
.implements Lcom/android/internal/util/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/ad/video/yandex/YandexVideoAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "YMAMediaPredicate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/internal/util/Predicate",
        "<",
        "Lcom/yandex/mobile/ads/video/models/ad/MediaFile;",
        ">;"
    }
.end annotation


# instance fields
.field private final maxDimension:I

.field private final mimeTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;I)V
    .locals 0
    .param p2, "maxDimension"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 367
    .local p1, "mimeTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 368
    iput-object p1, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$YMAMediaPredicate;->mimeTypes:Ljava/util/List;

    .line 369
    iput p2, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$YMAMediaPredicate;->maxDimension:I

    .line 370
    return-void
.end method


# virtual methods
.method public apply(Lcom/yandex/mobile/ads/video/models/ad/MediaFile;)Z
    .locals 3
    .param p1, "file"    # Lcom/yandex/mobile/ads/video/models/ad/MediaFile;

    .prologue
    const/4 v0, 0x0

    .line 374
    iget-object v1, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$YMAMediaPredicate;->mimeTypes:Ljava/util/List;

    invoke-virtual {p1}, Lcom/yandex/mobile/ads/video/models/ad/MediaFile;->getMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 380
    :cond_0
    :goto_0
    return v0

    .line 377
    :cond_1
    invoke-virtual {p1}, Lcom/yandex/mobile/ads/video/models/ad/MediaFile;->getHeight()I

    move-result v1

    iget v2, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$YMAMediaPredicate;->maxDimension:I

    if-gt v1, v2, :cond_0

    .line 380
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 363
    check-cast p1, Lcom/yandex/mobile/ads/video/models/ad/MediaFile;

    invoke-virtual {p0, p1}, Lru/cn/ad/video/yandex/YandexVideoAdapter$YMAMediaPredicate;->apply(Lcom/yandex/mobile/ads/video/models/ad/MediaFile;)Z

    move-result v0

    return v0
.end method
