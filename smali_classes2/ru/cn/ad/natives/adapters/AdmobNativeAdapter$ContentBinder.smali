.class final Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;
.super Lru/cn/ad/natives/adapters/AdmobNativeAdapter$AdmobBinder;
.source "AdmobNativeAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/ad/natives/adapters/AdmobNativeAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ContentBinder"
.end annotation


# instance fields
.field private adView:Lcom/google/android/gms/ads/formats/NativeContentAdView;

.field private final contentAd:Lcom/google/android/gms/ads/formats/NativeContentAd;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/formats/NativeContentAd;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentAd"    # Lcom/google/android/gms/ads/formats/NativeContentAd;

    .prologue
    .line 236
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$AdmobBinder;-><init>(Lru/cn/ad/natives/adapters/AdmobNativeAdapter$1;)V

    .line 237
    iput-object p2, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->contentAd:Lcom/google/android/gms/ads/formats/NativeContentAd;

    .line 239
    new-instance v0, Lcom/google/android/gms/ads/formats/NativeContentAdView;

    invoke-direct {v0, p1}, Lcom/google/android/gms/ads/formats/NativeContentAdView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->adView:Lcom/google/android/gms/ads/formats/NativeContentAdView;

    .line 240
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->adView:Lcom/google/android/gms/ads/formats/NativeContentAdView;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/ads/formats/NativeContentAdView;->setNativeAd(Lcom/google/android/gms/ads/formats/NativeAd;)V

    .line 241
    return-void
.end method


# virtual methods
.method public bind(Landroid/widget/Button;I)Z
    .locals 1
    .param p1, "view"    # Landroid/widget/Button;
    .param p2, "componentType"    # I

    .prologue
    .line 309
    packed-switch p2, :pswitch_data_0

    .line 316
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 311
    :pswitch_0
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->adView:Lcom/google/android/gms/ads/formats/NativeContentAdView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/ads/formats/NativeContentAdView;->setCallToActionView(Landroid/view/View;)V

    .line 312
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->contentAd:Lcom/google/android/gms/ads/formats/NativeContentAd;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/formats/NativeContentAd;->getCallToAction()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 313
    const/4 v0, 0x1

    goto :goto_0

    .line 309
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public bind(Landroid/widget/ImageView;I)Z
    .locals 3
    .param p1, "view"    # Landroid/widget/ImageView;
    .param p2, "componentType"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 284
    packed-switch p2, :pswitch_data_0

    .line 304
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 286
    :pswitch_1
    iget-object v2, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->contentAd:Lcom/google/android/gms/ads/formats/NativeContentAd;

    invoke-virtual {v2}, Lcom/google/android/gms/ads/formats/NativeContentAd;->getLogo()Lcom/google/android/gms/ads/formats/NativeAd$Image;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 290
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->adView:Lcom/google/android/gms/ads/formats/NativeContentAdView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/ads/formats/NativeContentAdView;->setLogoView(Landroid/view/View;)V

    .line 291
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->contentAd:Lcom/google/android/gms/ads/formats/NativeContentAd;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/formats/NativeContentAd;->getLogo()Lcom/google/android/gms/ads/formats/NativeAd$Image;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/ads/formats/NativeAd$Image;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move v0, v1

    .line 292
    goto :goto_0

    .line 295
    :pswitch_2
    iget-object v2, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->contentAd:Lcom/google/android/gms/ads/formats/NativeContentAd;

    invoke-virtual {v2}, Lcom/google/android/gms/ads/formats/NativeContentAd;->getImages()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_0

    .line 299
    iget-object v2, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->adView:Lcom/google/android/gms/ads/formats/NativeContentAdView;

    invoke-virtual {v2, p1}, Lcom/google/android/gms/ads/formats/NativeContentAdView;->setImageView(Landroid/view/View;)V

    .line 300
    iget-object v2, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->contentAd:Lcom/google/android/gms/ads/formats/NativeContentAd;

    invoke-virtual {v2}, Lcom/google/android/gms/ads/formats/NativeContentAd;->getImages()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/formats/NativeAd$Image;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/formats/NativeAd$Image;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move v0, v1

    .line 301
    goto :goto_0

    .line 284
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public bind(Landroid/widget/TextView;I)Z
    .locals 3
    .param p1, "view"    # Landroid/widget/TextView;
    .param p2, "componentType"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 250
    sparse-switch p2, :sswitch_data_0

    move v0, v1

    .line 279
    :goto_0
    return v0

    .line 252
    :sswitch_0
    iget-object v1, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->adView:Lcom/google/android/gms/ads/formats/NativeContentAdView;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/ads/formats/NativeContentAdView;->setHeadlineView(Landroid/view/View;)V

    .line 253
    iget-object v1, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->contentAd:Lcom/google/android/gms/ads/formats/NativeContentAd;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/formats/NativeContentAd;->getHeadline()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 257
    :sswitch_1
    iget-object v1, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->adView:Lcom/google/android/gms/ads/formats/NativeContentAdView;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/ads/formats/NativeContentAdView;->setCallToActionView(Landroid/view/View;)V

    .line 258
    iget-object v1, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->contentAd:Lcom/google/android/gms/ads/formats/NativeContentAd;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/formats/NativeContentAd;->getCallToAction()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 262
    :sswitch_2
    iget-object v2, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->contentAd:Lcom/google/android/gms/ads/formats/NativeContentAd;

    invoke-virtual {v2}, Lcom/google/android/gms/ads/formats/NativeContentAd;->getBody()Ljava/lang/CharSequence;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    .line 263
    goto :goto_0

    .line 266
    :cond_0
    iget-object v1, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->adView:Lcom/google/android/gms/ads/formats/NativeContentAdView;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/ads/formats/NativeContentAdView;->setBodyView(Landroid/view/View;)V

    .line 267
    iget-object v1, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->contentAd:Lcom/google/android/gms/ads/formats/NativeContentAd;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/formats/NativeContentAd;->getBody()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 271
    :sswitch_3
    iget-object v2, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->contentAd:Lcom/google/android/gms/ads/formats/NativeContentAd;

    invoke-virtual {v2}, Lcom/google/android/gms/ads/formats/NativeContentAd;->getAdvertiser()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 272
    goto :goto_0

    .line 275
    :cond_1
    iget-object v1, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->contentAd:Lcom/google/android/gms/ads/formats/NativeContentAd;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/formats/NativeContentAd;->getAdvertiser()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 250
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x4 -> :sswitch_1
        0xe -> :sswitch_3
    .end sparse-switch
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->contentAd:Lcom/google/android/gms/ads/formats/NativeContentAd;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/formats/NativeContentAd;->destroy()V

    .line 328
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->adView:Lcom/google/android/gms/ads/formats/NativeContentAdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/formats/NativeContentAdView;->removeAllViews()V

    .line 329
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->adView:Lcom/google/android/gms/ads/formats/NativeContentAdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/formats/NativeContentAdView;->destroy()V

    .line 330
    return-void
.end method

.method public getAdType()I
    .locals 1

    .prologue
    .line 245
    const/4 v0, 0x1

    return v0
.end method

.method public registerTemplate(Landroid/view/View;Ljava/util/List;)Landroid/view/View;
    .locals 1
    .param p1, "templateView"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 321
    .local p2, "clickableViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->adView:Lcom/google/android/gms/ads/formats/NativeContentAdView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/ads/formats/NativeContentAdView;->addView(Landroid/view/View;)V

    .line 322
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;->adView:Lcom/google/android/gms/ads/formats/NativeContentAdView;

    return-object v0
.end method
