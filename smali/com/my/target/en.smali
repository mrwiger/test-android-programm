.class public final Lcom/my/target/en;
.super Lcom/my/target/ee;
.source "CarouselHorizontalView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static final az:I

.field private static final bC:I

.field private static final bi:I

.field private static final cO:I

.field private static final cP:I

.field private static final cQ:I

.field private static final cR:I

.field private static final cS:I


# instance fields
.field private final F:Lcom/my/target/by;

.field private final aX:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final aw:Lcom/my/target/cm;

.field private final bL:Landroid/widget/Button;

.field private final bk:Lcom/my/target/bv;

.field private final bp:Lcom/my/target/by;

.field private final cT:Landroid/widget/TextView;

.field private final cU:Lcom/my/target/eu;

.field private final cV:Landroid/widget/RelativeLayout;

.field private final ch:Landroid/widget/TextView;

.field private s:Lcom/my/target/core/models/banners/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/en;->cO:I

    .line 42
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/en;->bi:I

    .line 43
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/en;->cP:I

    .line 44
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/en;->az:I

    .line 45
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/en;->cQ:I

    .line 46
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/en;->bC:I

    .line 47
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/en;->cR:I

    .line 48
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/en;->cS:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 10

    .prologue
    const/16 v9, 0xa

    const/16 v8, 0xc

    const/4 v7, -0x1

    const/4 v6, 0x0

    const/4 v5, -0x2

    .line 79
    invoke-direct {p0, p1, v6}, Lcom/my/target/ee;-><init>(Landroid/content/Context;I)V

    .line 73
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/my/target/en;->aX:Ljava/util/HashMap;

    .line 80
    const v0, -0x3a1508

    invoke-static {p0, v7, v0}, Lcom/my/target/cm;->a(Landroid/view/View;II)V

    .line 82
    new-instance v0, Lcom/my/target/by;

    invoke-direct {v0, p1}, Lcom/my/target/by;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/en;->F:Lcom/my/target/by;

    .line 83
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/en;->aw:Lcom/my/target/cm;

    .line 84
    new-instance v0, Lcom/my/target/by;

    invoke-direct {v0, p1}, Lcom/my/target/by;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/en;->bp:Lcom/my/target/by;

    .line 85
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/en;->ch:Landroid/widget/TextView;

    .line 86
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/en;->cT:Landroid/widget/TextView;

    .line 87
    new-instance v0, Lcom/my/target/bv;

    invoke-direct {v0, p1}, Lcom/my/target/bv;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/en;->bk:Lcom/my/target/bv;

    .line 88
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/en;->bL:Landroid/widget/Button;

    .line 90
    iget-object v0, p0, Lcom/my/target/en;->bL:Landroid/widget/Button;

    sget v1, Lcom/my/target/en;->bC:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setId(I)V

    .line 91
    iget-object v0, p0, Lcom/my/target/en;->bL:Landroid/widget/Button;

    iget-object v1, p0, Lcom/my/target/en;->aw:Lcom/my/target/cm;

    const/16 v2, 0xf

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/en;->aw:Lcom/my/target/cm;

    invoke-virtual {v2, v9}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/en;->aw:Lcom/my/target/cm;

    const/16 v4, 0xf

    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/en;->aw:Lcom/my/target/cm;

    invoke-virtual {v4, v9}, Lcom/my/target/cm;->n(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/Button;->setPadding(IIII)V

    .line 92
    iget-object v0, p0, Lcom/my/target/en;->bL:Landroid/widget/Button;

    iget-object v1, p0, Lcom/my/target/en;->aw:Lcom/my/target/cm;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setMinimumWidth(I)V

    .line 93
    iget-object v0, p0, Lcom/my/target/en;->bL:Landroid/widget/Button;

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setMaxEms(I)V

    .line 94
    iget-object v0, p0, Lcom/my/target/en;->bL:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 95
    iget-object v0, p0, Lcom/my/target/en;->bL:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->setSingleLine()V

    .line 96
    iget-object v0, p0, Lcom/my/target/en;->bL:Landroid/widget/Button;

    const/high16 v1, 0x41900000    # 18.0f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextSize(F)V

    .line 97
    iget-object v0, p0, Lcom/my/target/en;->bL:Landroid/widget/Button;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 98
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 100
    iget-object v0, p0, Lcom/my/target/en;->bL:Landroid/widget/Button;

    iget-object v1, p0, Lcom/my/target/en;->aw:Lcom/my/target/cm;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setElevation(F)V

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/my/target/en;->bL:Landroid/widget/Button;

    const v1, -0xff540e

    const v2, -0xff8957

    iget-object v3, p0, Lcom/my/target/en;->aw:Lcom/my/target/cm;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/my/target/cm;->a(Landroid/view/View;III)V

    .line 104
    iget-object v0, p0, Lcom/my/target/en;->bL:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setTextColor(I)V

    .line 106
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 108
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 110
    iget-object v1, p0, Lcom/my/target/en;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v8}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/en;->aw:Lcom/my/target/cm;

    invoke-virtual {v2, v8}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/en;->aw:Lcom/my/target/cm;

    invoke-virtual {v3, v8}, Lcom/my/target/cm;->n(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v6, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 111
    iget-object v1, p0, Lcom/my/target/en;->bL:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 113
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/en;->cV:Landroid/widget/RelativeLayout;

    .line 114
    iget-object v0, p0, Lcom/my/target/en;->cV:Landroid/widget/RelativeLayout;

    sget v1, Lcom/my/target/en;->cQ:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 115
    iget-object v0, p0, Lcom/my/target/en;->cV:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/my/target/en;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v9}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/en;->aw:Lcom/my/target/cm;

    .line 116
    invoke-virtual {v2, v6}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/en;->aw:Lcom/my/target/cm;

    .line 117
    invoke-virtual {v3, v9}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/en;->aw:Lcom/my/target/cm;

    .line 118
    invoke-virtual {v4, v6}, Lcom/my/target/cm;->n(I)I

    move-result v4

    .line 115
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 120
    iget-object v0, p0, Lcom/my/target/en;->F:Lcom/my/target/by;

    sget v1, Lcom/my/target/en;->cO:I

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setId(I)V

    .line 121
    iget-object v0, p0, Lcom/my/target/en;->F:Lcom/my/target/by;

    const-string v1, "close"

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 122
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 123
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 124
    iget-object v1, p0, Lcom/my/target/en;->F:Lcom/my/target/by;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/my/target/by;->setVisibility(I)V

    .line 125
    iget-object v1, p0, Lcom/my/target/en;->F:Lcom/my/target/by;

    invoke-virtual {v1, v0}, Lcom/my/target/by;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 127
    iget-object v0, p0, Lcom/my/target/en;->bk:Lcom/my/target/bv;

    sget v1, Lcom/my/target/en;->bi:I

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setId(I)V

    .line 128
    iget-object v0, p0, Lcom/my/target/en;->bk:Lcom/my/target/bv;

    const-string v1, "icon"

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 130
    iget-object v0, p0, Lcom/my/target/en;->ch:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLines(I)V

    .line 131
    iget-object v0, p0, Lcom/my/target/en;->ch:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 132
    iget-object v0, p0, Lcom/my/target/en;->ch:Landroid/widget/TextView;

    const/high16 v1, 0x41b00000    # 22.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 134
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/my/target/en;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 135
    sget v1, Lcom/my/target/en;->cP:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setId(I)V

    .line 136
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 137
    const/4 v2, 0x1

    sget v3, Lcom/my/target/en;->bi:I

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 138
    sget v2, Lcom/my/target/en;->bC:I

    invoke-virtual {v1, v6, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 139
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 141
    iget-object v1, p0, Lcom/my/target/en;->cT:Landroid/widget/TextView;

    sget v2, Lcom/my/target/en;->cS:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setId(I)V

    .line 142
    iget-object v1, p0, Lcom/my/target/en;->cT:Landroid/widget/TextView;

    const/high16 v2, 0x41900000    # 18.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 143
    iget-object v1, p0, Lcom/my/target/en;->cT:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLines(I)V

    .line 144
    iget-object v1, p0, Lcom/my/target/en;->cT:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 146
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/my/target/en;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 147
    sget v2, Lcom/my/target/en;->cR:I

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setId(I)V

    .line 148
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 149
    const/4 v3, 0x1

    sget v4, Lcom/my/target/en;->bi:I

    invoke-virtual {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 150
    const/4 v3, 0x3

    sget v4, Lcom/my/target/en;->cP:I

    invoke-virtual {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 151
    sget v3, Lcom/my/target/en;->bC:I

    invoke-virtual {v2, v6, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 152
    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 154
    new-instance v2, Lcom/my/target/eu;

    invoke-direct {v2, p1}, Lcom/my/target/eu;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/my/target/en;->cU:Lcom/my/target/eu;

    .line 156
    iget-object v2, p0, Lcom/my/target/en;->cU:Lcom/my/target/eu;

    iget-object v3, p0, Lcom/my/target/en;->aw:Lcom/my/target/cm;

    const/16 v4, 0x10

    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/en;->aw:Lcom/my/target/cm;

    invoke-virtual {v4, v8}, Lcom/my/target/cm;->n(I)I

    move-result v4

    invoke-virtual {v2, v6, v3, v6, v4}, Landroid/support/v7/widget/RecyclerView;->setPadding(IIII)V

    .line 157
    iget-object v2, p0, Lcom/my/target/en;->cU:Lcom/my/target/eu;

    iget-object v3, p0, Lcom/my/target/en;->aw:Lcom/my/target/cm;

    invoke-virtual {v3, v9}, Lcom/my/target/cm;->n(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/my/target/eu;->setSideSlidesMargins(I)V

    .line 159
    iget-object v2, p0, Lcom/my/target/en;->cV:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/my/target/en;->bL:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 160
    iget-object v2, p0, Lcom/my/target/en;->cV:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/my/target/en;->bk:Lcom/my/target/bv;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 161
    iget-object v2, p0, Lcom/my/target/en;->ch:Landroid/widget/TextView;

    sget v3, Lcom/my/target/en;->az:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setId(I)V

    .line 162
    iget-object v2, p0, Lcom/my/target/en;->cV:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 163
    iget-object v2, p0, Lcom/my/target/en;->ch:Landroid/widget/TextView;

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 164
    iget-object v0, p0, Lcom/my/target/en;->cV:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 165
    iget-object v0, p0, Lcom/my/target/en;->cT:Landroid/widget/TextView;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 166
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v7, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 167
    invoke-virtual {v0, v8, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 168
    iget-object v1, p0, Lcom/my/target/en;->cV:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 169
    iget-object v0, p0, Lcom/my/target/en;->cV:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v0}, Lcom/my/target/en;->addView(Landroid/view/View;)V

    .line 172
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 173
    const/4 v1, 0x2

    sget v2, Lcom/my/target/en;->cQ:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 175
    iget-object v1, p0, Lcom/my/target/en;->cU:Lcom/my/target/eu;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 176
    iget-object v0, p0, Lcom/my/target/en;->cU:Lcom/my/target/eu;

    invoke-virtual {p0, v0}, Lcom/my/target/en;->addView(Landroid/view/View;)V

    .line 177
    iget-object v0, p0, Lcom/my/target/en;->F:Lcom/my/target/by;

    invoke-virtual {p0, v0}, Lcom/my/target/en;->addView(Landroid/view/View;)V

    .line 178
    return-void
.end method


# virtual methods
.method public final G()V
    .locals 2

    .prologue
    .line 297
    iget-object v0, p0, Lcom/my/target/en;->F:Lcom/my/target/by;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setVisibility(I)V

    .line 298
    return-void
.end method

.method public final b(Lcom/my/target/core/models/banners/h;)V
    .locals 0

    .prologue
    .line 293
    return-void
.end method

.method public final e(I)V
    .locals 0

    .prologue
    .line 427
    return-void
.end method

.method public final f(Z)V
    .locals 0

    .prologue
    .line 417
    return-void
.end method

.method public final finish()V
    .locals 0

    .prologue
    .line 332
    return-void
.end method

.method public final getCloseButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/my/target/en;->F:Lcom/my/target/by;

    return-object v0
.end method

.method public final getNumbersOfCurrentShowingCards()[I
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 183
    iget-object v0, p0, Lcom/my/target/en;->cU:Lcom/my/target/eu;

    invoke-virtual {v0}, Lcom/my/target/eu;->getCardLayoutManager()Lcom/my/target/es;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/es;->findFirstVisibleItemPosition()I

    move-result v2

    .line 184
    iget-object v0, p0, Lcom/my/target/en;->cU:Lcom/my/target/eu;

    invoke-virtual {v0}, Lcom/my/target/eu;->getCardLayoutManager()Lcom/my/target/es;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/es;->findLastCompletelyVisibleItemPosition()I

    move-result v0

    .line 185
    if-eq v2, v3, :cond_0

    if-ne v0, v3, :cond_2

    .line 187
    :cond_0
    new-array v0, v1, [I

    .line 195
    :cond_1
    return-object v0

    .line 189
    :cond_2
    sub-int/2addr v0, v2

    add-int/lit8 v4, v0, 0x1

    .line 190
    new-array v0, v4, [I

    .line 191
    :goto_0
    if-ge v1, v4, :cond_1

    .line 193
    add-int/lit8 v3, v2, 0x1

    aput v2, v0, v1

    .line 191
    add-int/lit8 v1, v1, 0x1

    move v2, v3

    goto :goto_0
.end method

.method public final getSoundButton()Lcom/my/target/by;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/my/target/en;->bp:Lcom/my/target/by;

    return-object v0
.end method

.method public final isPaused()Z
    .locals 1

    .prologue
    .line 307
    const/4 v0, 0x0

    return v0
.end method

.method public final isPlaying()Z
    .locals 1

    .prologue
    .line 302
    const/4 v0, 0x0

    return v0
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v2, -0x1

    .line 388
    iget-object v0, p0, Lcom/my/target/en;->aX:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 390
    const/4 v0, 0x0

    .line 411
    :goto_0
    return v0

    .line 392
    :cond_0
    iget-object v0, p0, Lcom/my/target/en;->aX:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 394
    goto :goto_0

    .line 396
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    move v0, v1

    .line 411
    goto :goto_0

    .line 399
    :pswitch_1
    const v0, -0x3a1508

    invoke-virtual {p0, v0}, Lcom/my/target/en;->setBackgroundColor(I)V

    goto :goto_1

    .line 402
    :pswitch_2
    invoke-virtual {p0, v2}, Lcom/my/target/en;->setBackgroundColor(I)V

    .line 403
    iget-object v0, p0, Lcom/my/target/en;->bx:Lcom/my/target/ee$a;

    invoke-virtual {v0, p1}, Lcom/my/target/ee$a;->onClick(Landroid/view/View;)V

    goto :goto_1

    .line 406
    :pswitch_3
    invoke-virtual {p0, v2}, Lcom/my/target/en;->setBackgroundColor(I)V

    goto :goto_1

    .line 396
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final pause()V
    .locals 0

    .prologue
    .line 422
    return-void
.end method

.method public final play()V
    .locals 0

    .prologue
    .line 326
    return-void
.end method

.method public final resume()V
    .locals 0

    .prologue
    .line 346
    return-void
.end method

.method public final setBanner(Lcom/my/target/core/models/banners/h;)V
    .locals 7
    .param p1, "banner"    # Lcom/my/target/core/models/banners/h;

    .prologue
    const/16 v6, 0x40

    const/4 v4, -0x2

    const/4 v1, 0x0

    .line 201
    invoke-super {p0, p1}, Lcom/my/target/ee;->setBanner(Lcom/my/target/core/models/banners/h;)V

    .line 202
    iput-object p1, p0, Lcom/my/target/en;->s:Lcom/my/target/core/models/banners/d;

    .line 203
    iget-object v0, p0, Lcom/my/target/en;->bL:Landroid/widget/Button;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getCtaText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 205
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getCloseIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 206
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 208
    iget-object v2, p0, Lcom/my/target/en;->F:Lcom/my/target/by;

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Lcom/my/target/by;->b(Landroid/graphics/Bitmap;Z)V

    .line 219
    :cond_0
    :goto_0
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 222
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v4

    .line 223
    if-eqz v4, :cond_8

    .line 225
    invoke-virtual {v4}, Lcom/my/target/common/models/ImageData;->getWidth()I

    move-result v2

    .line 226
    invoke-virtual {v4}, Lcom/my/target/common/models/ImageData;->getHeight()I

    move-result v0

    .line 229
    :goto_1
    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    .line 231
    int-to-float v0, v0

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 233
    iget-object v2, p0, Lcom/my/target/en;->aw:Lcom/my/target/cm;

    invoke-virtual {v2, v6}, Lcom/my/target/cm;->n(I)I

    move-result v2

    .line 234
    iget-object v5, p0, Lcom/my/target/en;->aw:Lcom/my/target/cm;

    invoke-virtual {v5, v6}, Lcom/my/target/cm;->n(I)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v0, v5

    float-to-int v0, v0

    .line 236
    iput v2, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 237
    iput v0, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 240
    :cond_1
    iget-object v0, p0, Lcom/my/target/en;->bk:Lcom/my/target/bv;

    invoke-virtual {v0, v3}, Lcom/my/target/bv;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 242
    if-eqz v4, :cond_2

    .line 244
    iget-object v0, p0, Lcom/my/target/en;->bk:Lcom/my/target/bv;

    invoke-virtual {v4}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/my/target/bv;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 247
    :cond_2
    iget-object v0, p0, Lcom/my/target/en;->ch:Landroid/widget/TextView;

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 248
    iget-object v0, p0, Lcom/my/target/en;->ch:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getCategory()Ljava/lang/String;

    move-result-object v2

    .line 251
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getSubCategory()Ljava/lang/String;

    move-result-object v3

    .line 253
    const-string v0, ""

    .line 254
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 256
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 259
    :cond_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 261
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 264
    :cond_4
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 266
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 269
    :cond_5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 271
    iget-object v2, p0, Lcom/my/target/en;->cT:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 272
    iget-object v0, p0, Lcom/my/target/en;->cT:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 279
    :goto_2
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getInterstitialAdCards()Ljava/util/List;

    move-result-object v0

    .line 280
    iget-object v1, p0, Lcom/my/target/en;->cU:Lcom/my/target/eu;

    invoke-virtual {v1, v0}, Lcom/my/target/eu;->c(Ljava/util/List;)V

    .line 281
    return-void

    .line 212
    :cond_6
    iget-object v0, p0, Lcom/my/target/en;->aw:Lcom/my/target/cm;

    const/16 v2, 0x1c

    invoke-virtual {v0, v2}, Lcom/my/target/cm;->n(I)I

    move-result v0

    invoke-static {v0}, Lcom/my/target/bq;->i(I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 213
    if-eqz v0, :cond_0

    .line 215
    iget-object v2, p0, Lcom/my/target/en;->F:Lcom/my/target/by;

    invoke-virtual {v2, v0, v1}, Lcom/my/target/by;->b(Landroid/graphics/Bitmap;Z)V

    goto/16 :goto_0

    .line 276
    :cond_7
    iget-object v0, p0, Lcom/my/target/en;->cT:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_8
    move v0, v1

    move v2, v1

    goto/16 :goto_1
.end method

.method public final setClickArea(Lcom/my/target/af;)V
    .locals 3
    .param p1, "area"    # Lcom/my/target/af;

    .prologue
    .line 351
    iget-boolean v0, p1, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/my/target/en;->bx:Lcom/my/target/ee$a;

    invoke-virtual {p0, v0}, Lcom/my/target/en;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 354
    const/4 v0, -0x1

    const v1, -0x3a1508

    invoke-static {p0, v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;II)V

    .line 355
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/my/target/en;->setClickable(Z)V

    .line 382
    :goto_0
    iget-object v0, p0, Lcom/my/target/en;->cU:Lcom/my/target/eu;

    iget-object v1, p0, Lcom/my/target/en;->ad:Lcom/my/target/ee$b;

    invoke-virtual {v0, v1}, Lcom/my/target/eu;->setOnPromoCardListener(Lcom/my/target/ee$b;)V

    .line 383
    return-void

    .line 359
    :cond_0
    iget-object v0, p0, Lcom/my/target/en;->ch:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 360
    iget-object v0, p0, Lcom/my/target/en;->cT:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 361
    iget-object v0, p0, Lcom/my/target/en;->bk:Lcom/my/target/bv;

    invoke-virtual {v0, p0}, Lcom/my/target/bv;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 362
    invoke-virtual {p0, p0}, Lcom/my/target/en;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 363
    iget-object v0, p0, Lcom/my/target/en;->cV:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 365
    iget-object v0, p0, Lcom/my/target/en;->aX:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/my/target/en;->ch:Landroid/widget/TextView;

    iget-boolean v2, p1, Lcom/my/target/af;->cs:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    iget-object v0, p0, Lcom/my/target/en;->aX:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/my/target/en;->cT:Landroid/widget/TextView;

    iget-boolean v2, p1, Lcom/my/target/af;->cC:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 367
    iget-object v0, p0, Lcom/my/target/en;->aX:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/my/target/en;->bk:Lcom/my/target/bv;

    iget-boolean v2, p1, Lcom/my/target/af;->cu:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 368
    iget-object v0, p0, Lcom/my/target/en;->aX:Ljava/util/HashMap;

    iget-boolean v1, p1, Lcom/my/target/af;->cD:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 369
    iget-object v0, p0, Lcom/my/target/en;->aX:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/my/target/en;->cV:Landroid/widget/RelativeLayout;

    iget-boolean v2, p1, Lcom/my/target/af;->cD:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 371
    iget-boolean v0, p1, Lcom/my/target/af;->cy:Z

    if-eqz v0, :cond_1

    .line 373
    iget-object v0, p0, Lcom/my/target/en;->bL:Landroid/widget/Button;

    iget-object v1, p0, Lcom/my/target/en;->bx:Lcom/my/target/ee$a;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 377
    :cond_1
    iget-object v0, p0, Lcom/my/target/en;->bL:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 378
    iget-object v0, p0, Lcom/my/target/en;->bL:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public final setTimeChanged(F)V
    .locals 0

    .prologue
    .line 337
    return-void
.end method

.method public final setVideoListener(Lcom/my/target/cc$a;)V
    .locals 0

    .prologue
    .line 341
    return-void
.end method
