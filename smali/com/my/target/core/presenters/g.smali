.class public final Lcom/my/target/core/presenters/g;
.super Ljava/lang/Object;
.source "InterstitialSliderPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/core/presenters/g$a;
    }
.end annotation


# instance fields
.field private final ao:Lcom/my/target/ey;

.field private ap:Lcom/my/target/core/presenters/g$a;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Lcom/my/target/ey;

    invoke-direct {v0, p1}, Lcom/my/target/ey;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/core/presenters/g;->ao:Lcom/my/target/ey;

    .line 27
    new-instance v0, Lcom/my/target/core/presenters/g$1;

    invoke-direct {v0, p0}, Lcom/my/target/core/presenters/g$1;-><init>(Lcom/my/target/core/presenters/g;)V

    .line 50
    iget-object v1, p0, Lcom/my/target/core/presenters/g;->ao:Lcom/my/target/ey;

    invoke-virtual {v1, v0}, Lcom/my/target/ey;->setFSSliderCardListener(Lcom/my/target/fb$c;)V

    .line 51
    new-instance v0, Lcom/my/target/core/presenters/g$2;

    invoke-direct {v0, p0}, Lcom/my/target/core/presenters/g$2;-><init>(Lcom/my/target/core/presenters/g;)V

    .line 62
    iget-object v1, p0, Lcom/my/target/core/presenters/g;->ao:Lcom/my/target/ey;

    invoke-virtual {v1, v0}, Lcom/my/target/ey;->setCloseClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    return-void
.end method

.method static synthetic a(Lcom/my/target/core/presenters/g;)Lcom/my/target/core/presenters/g$a;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/my/target/core/presenters/g;->ap:Lcom/my/target/core/presenters/g$a;

    return-object v0
.end method

.method static synthetic b(Lcom/my/target/core/presenters/g;)Lcom/my/target/ey;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/my/target/core/presenters/g;->ao:Lcom/my/target/ey;

    return-object v0
.end method

.method public static f(Landroid/content/Context;)Lcom/my/target/core/presenters/g;
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/my/target/core/presenters/g;

    invoke-direct {v0, p0}, Lcom/my/target/core/presenters/g;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/my/target/core/presenters/g$a;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/my/target/core/presenters/g;->ap:Lcom/my/target/core/presenters/g$a;

    .line 78
    return-void
.end method

.method public final a(Lcom/my/target/dw;)V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/my/target/core/presenters/g;->ao:Lcom/my/target/ey;

    invoke-virtual {p1}, Lcom/my/target/dw;->R()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/my/target/ey;->a(Lcom/my/target/dw;Ljava/util/List;)V

    .line 68
    return-void
.end method

.method public final getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/my/target/core/presenters/g;->ao:Lcom/my/target/ey;

    return-object v0
.end method
