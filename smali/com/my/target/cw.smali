.class public final Lcom/my/target/cw;
.super Landroid/view/ViewGroup;
.source "VideoDialogView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/cw$c;,
        Lcom/my/target/cw$b;,
        Lcom/my/target/cw$a;,
        Lcom/my/target/cw$d;
    }
.end annotation


# static fields
.field private static final SOUND_BUTTON_ID:I

.field private static final TITLE_ID:I

.field private static final ab:I

.field private static final ac:I

.field private static final ad:I

.field private static final ae:I

.field private static final af:I

.field private static final ag:I

.field private static final ah:I

.field private static final ai:I

.field private static final aj:I


# instance fields
.field private final aA:Landroid/graphics/Bitmap;

.field private aB:I

.field private final aC:I

.field private aD:Z

.field private aE:Ljava/lang/String;

.field private aF:Lcom/my/target/cw$d;

.field private final ak:Landroid/widget/TextView;

.field private final al:Lcom/my/target/ca;

.field private final am:Landroid/widget/Button;

.field private final an:Landroid/widget/LinearLayout;

.field private final ao:Landroid/widget/TextView;

.field private final ap:Landroid/widget/FrameLayout;

.field private final aq:Landroid/widget/TextView;

.field private final ar:Lcom/my/target/cx;

.field private final as:Lcom/my/target/by;

.field private final at:Lcom/my/target/cu;

.field private final au:Lcom/my/target/cu;

.field private final av:Lcom/my/target/cu;

.field private final aw:Ljava/lang/Runnable;

.field private final ax:Lcom/my/target/cw$c;

.field private final ay:Landroid/view/View$OnClickListener;

.field private final az:Landroid/graphics/Bitmap;

.field private final ctaButton:Landroid/widget/Button;

.field private final mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

.field private final padding:I

.field private final uiUtils:Lcom/my/target/cm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/cw;->ab:I

    .line 38
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/cw;->ac:I

    .line 39
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/cw;->ad:I

    .line 40
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/cw;->ae:I

    .line 41
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/cw;->af:I

    .line 42
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/cw;->ag:I

    .line 43
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/cw;->TITLE_ID:I

    .line 44
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/cw;->ah:I

    .line 45
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/cw;->ai:I

    .line 46
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/cw;->SOUND_BUTTON_ID:I

    .line 47
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/cw;->aj:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v9, 0x0

    const/16 v8, 0x10

    const/4 v7, 0x1

    const/4 v2, -0x1

    .line 86
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 88
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cw;->am:Landroid/widget/Button;

    .line 89
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    .line 90
    new-instance v0, Lcom/my/target/ca;

    invoke-direct {v0, p1}, Lcom/my/target/ca;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    .line 91
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    .line 92
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cw;->ao:Landroid/widget/TextView;

    .line 93
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cw;->ap:Landroid/widget/FrameLayout;

    .line 94
    new-instance v0, Lcom/my/target/cu;

    invoke-direct {v0, p1}, Lcom/my/target/cu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cw;->at:Lcom/my/target/cu;

    .line 95
    new-instance v0, Lcom/my/target/cu;

    invoke-direct {v0, p1}, Lcom/my/target/cu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cw;->au:Lcom/my/target/cu;

    .line 96
    new-instance v0, Lcom/my/target/cu;

    invoke-direct {v0, p1}, Lcom/my/target/cu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cw;->av:Lcom/my/target/cu;

    .line 97
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    .line 98
    new-instance v0, Lcom/my/target/nativeads/views/MediaAdView;

    invoke-direct {v0, p1}, Lcom/my/target/nativeads/views/MediaAdView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    .line 99
    new-instance v0, Lcom/my/target/cx;

    invoke-direct {v0, p1}, Lcom/my/target/cx;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cw;->ar:Lcom/my/target/cx;

    .line 100
    new-instance v0, Lcom/my/target/by;

    invoke-direct {v0, p1}, Lcom/my/target/by;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    .line 101
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cw;->an:Landroid/widget/LinearLayout;

    .line 102
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    .line 103
    new-instance v0, Lcom/my/target/cw$b;

    invoke-direct {v0, p0, v9}, Lcom/my/target/cw$b;-><init>(Lcom/my/target/cw;B)V

    iput-object v0, p0, Lcom/my/target/cw;->aw:Ljava/lang/Runnable;

    .line 104
    new-instance v0, Lcom/my/target/cw$c;

    invoke-direct {v0, p0, v9}, Lcom/my/target/cw$c;-><init>(Lcom/my/target/cw;B)V

    iput-object v0, p0, Lcom/my/target/cw;->ax:Lcom/my/target/cw$c;

    .line 105
    new-instance v0, Lcom/my/target/cw$a;

    invoke-direct {v0, p0, v9}, Lcom/my/target/cw$a;-><init>(Lcom/my/target/cw;B)V

    iput-object v0, p0, Lcom/my/target/cw;->ay:Landroid/view/View$OnClickListener;

    .line 107
    iget-object v0, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    const/16 v1, 0x1c

    invoke-virtual {v0, v1}, Lcom/my/target/cm;->n(I)I

    move-result v0

    invoke-static {v0}, Lcom/my/target/core/resources/a;->getVolumeOnIcon(I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/cw;->az:Landroid/graphics/Bitmap;

    .line 108
    iget-object v0, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    const/16 v1, 0x1c

    invoke-virtual {v0, v1}, Lcom/my/target/cm;->n(I)I

    move-result v0

    invoke-static {v0}, Lcom/my/target/core/resources/a;->getVolumeOffIcon(I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/cw;->aA:Landroid/graphics/Bitmap;

    .line 110
    iget-object v0, p0, Lcom/my/target/cw;->am:Landroid/widget/Button;

    const-string v1, "dismiss_button"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    const-string v1, "title_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    const-string v1, "stars_view"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    const-string v1, "cta_button"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/my/target/cw;->ao:Landroid/widget/TextView;

    const-string v1, "replay_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lcom/my/target/cw;->ap:Landroid/widget/FrameLayout;

    const-string v1, "shadow"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/my/target/cw;->at:Lcom/my/target/cu;

    const-string v1, "pause_button"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/my/target/cw;->au:Lcom/my/target/cu;

    const-string v1, "play_button"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/my/target/cw;->av:Lcom/my/target/cu;

    const-string v1, "replay_button"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    const-string v1, "domain_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    const-string v1, "media_view"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/my/target/cw;->ar:Lcom/my/target/cx;

    const-string v1, "video_progress_wheel"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    const-string v1, "sound_button"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    const/16 v1, 0x1c

    invoke-virtual {v0, v1}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iput v0, p0, Lcom/my/target/cw;->aC:I

    .line 124
    iget-object v0, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v0, v8}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iput v0, p0, Lcom/my/target/cw;->padding:I

    .line 1514
    const/high16 v0, -0x1000000

    invoke-virtual {p0, v0}, Lcom/my/target/cw;->setBackgroundColor(I)V

    .line 1515
    iget v6, p0, Lcom/my/target/cw;->padding:I

    .line 1517
    iget-object v0, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    sget v1, Lcom/my/target/cw;->SOUND_BUTTON_ID:I

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setId(I)V

    .line 1519
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v3, -0x2

    invoke-direct {v0, v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1521
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1522
    iget-object v1, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v1, v0}, Lcom/my/target/nativeads/views/MediaAdView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1523
    iget-object v0, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    sget v1, Lcom/my/target/cw;->ai:I

    invoke-virtual {v0, v1}, Lcom/my/target/nativeads/views/MediaAdView;->setId(I)V

    .line 1524
    iget-object v0, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    iget-object v1, p0, Lcom/my/target/cw;->ax:Lcom/my/target/cw$c;

    invoke-virtual {v0, v1}, Lcom/my/target/nativeads/views/MediaAdView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1525
    iget-object v0, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/my/target/nativeads/views/MediaAdView;->setBackgroundColor(I)V

    .line 1527
    iget-object v0, p0, Lcom/my/target/cw;->ap:Landroid/widget/FrameLayout;

    const/high16 v1, -0x67000000

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 1528
    iget-object v0, p0, Lcom/my/target/cw;->ap:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v10}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1530
    iget-object v0, p0, Lcom/my/target/cw;->am:Landroid/widget/Button;

    sget v1, Lcom/my/target/cw;->ab:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setId(I)V

    .line 1532
    iget-object v0, p0, Lcom/my/target/cw;->am:Landroid/widget/Button;

    const/4 v1, 0x2

    const/high16 v3, 0x41800000    # 16.0f

    invoke-virtual {v0, v1, v3}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1533
    iget-object v0, p0, Lcom/my/target/cw;->am:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1534
    iget-object v0, p0, Lcom/my/target/cw;->am:Landroid/widget/Button;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1535
    iget-object v0, p0, Lcom/my/target/cw;->am:Landroid/widget/Button;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setMaxLines(I)V

    .line 1536
    iget-object v0, p0, Lcom/my/target/cw;->am:Landroid/widget/Button;

    invoke-virtual {v0, v6, v6, v6, v6}, Landroid/widget/Button;->setPadding(IIII)V

    .line 1538
    iget-object v0, p0, Lcom/my/target/cw;->am:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTextColor(I)V

    .line 1539
    iget-object v0, p0, Lcom/my/target/cw;->am:Landroid/widget/Button;

    const/high16 v1, -0x78000000

    iget-object v3, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    .line 1540
    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v3, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    const/4 v5, 0x4

    invoke-virtual {v3, v5}, Lcom/my/target/cm;->n(I)I

    move-result v5

    move v3, v2

    .line 1539
    invoke-static/range {v0 .. v5}, Lcom/my/target/cm;->a(Landroid/view/View;IIIII)V

    .line 1542
    iget-object v0, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    sget v1, Lcom/my/target/cw;->TITLE_ID:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 1543
    iget-object v0, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1544
    iget-object v0, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1545
    iget-object v0, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    const/4 v1, 0x2

    const/high16 v3, 0x41900000    # 18.0f

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1546
    iget-object v0, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1548
    iget-object v0, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    const/high16 v1, -0x78000000

    iget-object v3, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    .line 1549
    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v3, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    const/4 v5, 0x4

    invoke-virtual {v3, v5}, Lcom/my/target/cm;->n(I)I

    move-result v5

    move v3, v2

    .line 1548
    invoke-static/range {v0 .. v5}, Lcom/my/target/cm;->a(Landroid/view/View;IIIII)V

    .line 1551
    iget-object v0, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    sget v1, Lcom/my/target/cw;->ac:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setId(I)V

    .line 1552
    iget-object v0, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTextColor(I)V

    .line 1553
    iget-object v0, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1554
    iget-object v0, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setGravity(I)V

    .line 1555
    iget-object v0, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    const/4 v1, 0x2

    const/high16 v3, 0x41800000    # 16.0f

    invoke-virtual {v0, v1, v3}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1556
    iget-object v0, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    const/16 v3, 0x64

    invoke-virtual {v1, v3}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setMinimumWidth(I)V

    .line 1557
    iget-object v0, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    invoke-virtual {v0, v6, v6, v6, v6}, Landroid/widget/Button;->setPadding(IIII)V

    .line 1558
    iget-object v0, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v1, v7}, Lcom/my/target/cm;->n(I)I

    move-result v1

    int-to-float v1, v1

    iget-object v3, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    .line 1559
    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    .line 1560
    invoke-virtual {v4, v7}, Lcom/my/target/cm;->n(I)I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, -0x1000000

    .line 1558
    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 1563
    iget-object v0, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    sget v1, Lcom/my/target/cw;->ah:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 1564
    iget-object v0, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    const v1, -0x333334

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1565
    iget-object v0, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxEms(I)V

    .line 1566
    iget-object v0, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v1, v7}, Lcom/my/target/cm;->n(I)I

    move-result v1

    int-to-float v1, v1

    iget-object v3, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    .line 1567
    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    .line 1568
    invoke-virtual {v4, v7}, Lcom/my/target/cm;->n(I)I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, -0x1000000

    .line 1566
    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 1571
    iget-object v0, p0, Lcom/my/target/cw;->an:Landroid/widget/LinearLayout;

    sget v1, Lcom/my/target/cw;->ad:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setId(I)V

    .line 1572
    iget-object v0, p0, Lcom/my/target/cw;->an:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/cw;->ay:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1573
    iget-object v0, p0, Lcom/my/target/cw;->an:Landroid/widget/LinearLayout;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 1574
    iget-object v0, p0, Lcom/my/target/cw;->an:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1575
    iget-object v0, p0, Lcom/my/target/cw;->an:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v1, v10}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v3, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v3, v10}, Lcom/my/target/cm;->n(I)I

    move-result v3

    invoke-virtual {v0, v1, v9, v3, v9}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 1577
    iget-object v0, p0, Lcom/my/target/cw;->ao:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 1578
    iget-object v0, p0, Lcom/my/target/cw;->ao:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1579
    iget-object v0, p0, Lcom/my/target/cw;->ao:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/cw;->ao:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1580
    iget-object v0, p0, Lcom/my/target/cw;->ao:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1581
    iget-object v0, p0, Lcom/my/target/cw;->ao:Landroid/widget/TextView;

    const/4 v1, 0x2

    const/high16 v3, 0x41800000    # 16.0f

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1582
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v0, -0x2

    const/4 v1, -0x2

    invoke-direct {v6, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1583
    iget-object v0, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iput v0, v6, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 1585
    iget-object v0, p0, Lcom/my/target/cw;->av:Lcom/my/target/cu;

    iget-object v1, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v1, v8}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v3, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    .line 1586
    invoke-virtual {v3, v8}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    .line 1587
    invoke-virtual {v4, v8}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v5, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    .line 1588
    invoke-virtual {v5, v8}, Lcom/my/target/cm;->n(I)I

    move-result v5

    .line 1585
    invoke-virtual {v0, v1, v3, v4, v5}, Lcom/my/target/cu;->setPadding(IIII)V

    .line 1590
    iget-object v0, p0, Lcom/my/target/cw;->at:Lcom/my/target/cu;

    sget v1, Lcom/my/target/cw;->af:I

    invoke-virtual {v0, v1}, Lcom/my/target/cu;->setId(I)V

    .line 1591
    iget-object v0, p0, Lcom/my/target/cw;->at:Lcom/my/target/cu;

    iget-object v1, p0, Lcom/my/target/cw;->ay:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/my/target/cu;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1592
    iget-object v0, p0, Lcom/my/target/cw;->at:Lcom/my/target/cu;

    invoke-virtual {v0, v10}, Lcom/my/target/cu;->setVisibility(I)V

    .line 1593
    iget-object v0, p0, Lcom/my/target/cw;->at:Lcom/my/target/cu;

    iget-object v1, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v1, v8}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v3, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    .line 1594
    invoke-virtual {v3, v8}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    .line 1595
    invoke-virtual {v4, v8}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v5, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    .line 1596
    invoke-virtual {v5, v8}, Lcom/my/target/cm;->n(I)I

    move-result v5

    .line 1593
    invoke-virtual {v0, v1, v3, v4, v5}, Lcom/my/target/cu;->setPadding(IIII)V

    .line 1598
    iget-object v0, p0, Lcom/my/target/cw;->au:Lcom/my/target/cu;

    sget v1, Lcom/my/target/cw;->ae:I

    invoke-virtual {v0, v1}, Lcom/my/target/cu;->setId(I)V

    .line 1599
    iget-object v0, p0, Lcom/my/target/cw;->au:Lcom/my/target/cu;

    iget-object v1, p0, Lcom/my/target/cw;->ay:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/my/target/cu;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1600
    iget-object v0, p0, Lcom/my/target/cw;->au:Lcom/my/target/cu;

    invoke-virtual {v0, v10}, Lcom/my/target/cu;->setVisibility(I)V

    .line 1601
    iget-object v0, p0, Lcom/my/target/cw;->au:Lcom/my/target/cu;

    iget-object v1, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v1, v8}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v3, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    .line 1602
    invoke-virtual {v3, v8}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    .line 1603
    invoke-virtual {v4, v8}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v5, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    .line 1604
    invoke-virtual {v5, v8}, Lcom/my/target/cm;->n(I)I

    move-result v5

    .line 1601
    invoke-virtual {v0, v1, v3, v4, v5}, Lcom/my/target/cu;->setPadding(IIII)V

    .line 1606
    iget-object v0, p0, Lcom/my/target/cw;->ap:Landroid/widget/FrameLayout;

    sget v1, Lcom/my/target/cw;->aj:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setId(I)V

    .line 1608
    invoke-virtual {p0}, Lcom/my/target/cw;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2132
    const-string v1, "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AMXCy8fw79+rQAAAhVJREFUeNrt2y9IXlEYx3H3ooIiiCAIC4JgMRgsCyaLwWaxLK0srZhWVtYWVtYWlpYMNsvK0sKKRTANBivDIIggIiLiZ+URDncHFgzbznN+8d77nvPwvec99zz/xsa6uv4oPMWjzADgK55kBnCvj3icGQBc4hWmsgK41w/sZAPwswLiC9ayAJjGa1wNrt/hAxaaBlBcW8ReZTVc4CUmmwZQ3FvHYQXEd2w3DyDuj/AMJxUQn7HaNIDiuRm8wfUAwi3eY75pAMXzS9ivrIZz7GKiaQDF7zZwVAHxDVvNAyj2h+c4rYD4hJWmARRjzOItbir7wzvMNQ2gGGsZB5XVcIYXGG8aQDHmJo4rII6x2TyAGHc83vpZBcQBlpsGUIw/F/vA7QDCTewbs00DKOZZiS/DUKfxJRk1DaCYbyvOCkMdYaN5ADHnRJwazysg9rHUNIBi7vnwI4b7w3X4HTNNAyhsWA3PcqiT8ERHTQMobNmOWMNQh1hvHkDYMxlRp4sKiD0sNg2gsGsh4pB3AwhXEbecbhpAYd9aRKZVItgPT+v96wAKO3ciVzHUw9J6/wuAsHUqslaXFRC/pfVGY139L9A3wf4Z7AehfhTuzlB3h3tApIfE/jqAtEHRtGHx1ImRtKmxtMnRtOnx1AUSaUtk0hZJpS2TS10ombZUNm2xdPpy+d4w0VtmetNU2ra51I2TuVtnuxrWL/YiKQ6CN9uRAAAAAElFTkSuQmCC"

    invoke-static {v1, v9}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    .line 2133
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 2134
    const/16 v4, 0x1a4

    iput v4, v3, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    .line 2135
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2136
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v0, v3, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    .line 2137
    array-length v0, v1

    invoke-static {v1, v9, v0, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1609
    if-eqz v0, :cond_0

    .line 1611
    iget-object v1, p0, Lcom/my/target/cw;->au:Lcom/my/target/cu;

    invoke-virtual {v1, v0}, Lcom/my/target/cu;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1613
    :cond_0
    invoke-virtual {p0}, Lcom/my/target/cw;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2142
    const-string v1, "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AMXCjITNKc0rQAAAJFJREFUeNrt2tENgCAMQEEwLuD+QzpC3cBURWLsvV+JNRfhi9YkSSpbP3sYETF0WO89s27m3KX6H1AeYL2wdrs5Y3/4ja/OTZ8B2f074h0z5zoDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/lr6rvDoK+xfmWsLNEmSVLUD47EiX/OuE8UAAAAASUVORK5CYII="

    invoke-static {v1, v9}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    .line 2143
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 2144
    const/16 v4, 0x1a4

    iput v4, v3, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    .line 2145
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2146
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v0, v3, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    .line 2147
    array-length v0, v1

    invoke-static {v1, v9, v0, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1614
    if-eqz v0, :cond_1

    .line 1616
    iget-object v1, p0, Lcom/my/target/cw;->at:Lcom/my/target/cu;

    invoke-virtual {v1, v0}, Lcom/my/target/cu;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1619
    :cond_1
    iget-object v0, p0, Lcom/my/target/cw;->at:Lcom/my/target/cu;

    const/high16 v1, -0x78000000

    iget-object v3, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    .line 1623
    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v3, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    const/4 v5, 0x4

    .line 1624
    invoke-virtual {v3, v5}, Lcom/my/target/cm;->n(I)I

    move-result v5

    move v3, v2

    .line 1619
    invoke-static/range {v0 .. v5}, Lcom/my/target/cm;->a(Landroid/view/View;IIIII)V

    .line 1625
    iget-object v0, p0, Lcom/my/target/cw;->au:Lcom/my/target/cu;

    const/high16 v1, -0x78000000

    iget-object v3, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    .line 1629
    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v3, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    const/4 v5, 0x4

    .line 1630
    invoke-virtual {v3, v5}, Lcom/my/target/cm;->n(I)I

    move-result v5

    move v3, v2

    .line 1625
    invoke-static/range {v0 .. v5}, Lcom/my/target/cm;->a(Landroid/view/View;IIIII)V

    .line 1631
    iget-object v0, p0, Lcom/my/target/cw;->av:Lcom/my/target/cu;

    const/high16 v1, -0x78000000

    iget-object v3, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    .line 1635
    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v3, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    const/4 v5, 0x4

    .line 1636
    invoke-virtual {v3, v5}, Lcom/my/target/cm;->n(I)I

    move-result v5

    move v3, v2

    .line 1631
    invoke-static/range {v0 .. v5}, Lcom/my/target/cm;->a(Landroid/view/View;IIIII)V

    .line 1638
    iget-object v0, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    iget-object v1, p0, Lcom/my/target/cw;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/ca;->setStarSize(I)V

    .line 1640
    iget-object v0, p0, Lcom/my/target/cw;->ar:Lcom/my/target/cx;

    sget v1, Lcom/my/target/cw;->ag:I

    invoke-virtual {v0, v1}, Lcom/my/target/cx;->setId(I)V

    .line 1641
    iget-object v0, p0, Lcom/my/target/cw;->ar:Lcom/my/target/cx;

    invoke-virtual {v0, v10}, Lcom/my/target/cx;->setVisibility(I)V

    .line 1643
    iget-object v0, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {p0, v0}, Lcom/my/target/cw;->addView(Landroid/view/View;)V

    .line 1644
    iget-object v0, p0, Lcom/my/target/cw;->ap:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/my/target/cw;->addView(Landroid/view/View;)V

    .line 1645
    iget-object v0, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    invoke-virtual {p0, v0}, Lcom/my/target/cw;->addView(Landroid/view/View;)V

    .line 1646
    iget-object v0, p0, Lcom/my/target/cw;->am:Landroid/widget/Button;

    invoke-virtual {p0, v0}, Lcom/my/target/cw;->addView(Landroid/view/View;)V

    .line 1647
    iget-object v0, p0, Lcom/my/target/cw;->ar:Lcom/my/target/cx;

    invoke-virtual {p0, v0}, Lcom/my/target/cw;->addView(Landroid/view/View;)V

    .line 1648
    iget-object v0, p0, Lcom/my/target/cw;->an:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/my/target/cw;->addView(Landroid/view/View;)V

    .line 1649
    iget-object v0, p0, Lcom/my/target/cw;->at:Lcom/my/target/cu;

    invoke-virtual {p0, v0}, Lcom/my/target/cw;->addView(Landroid/view/View;)V

    .line 1650
    iget-object v0, p0, Lcom/my/target/cw;->au:Lcom/my/target/cu;

    invoke-virtual {p0, v0}, Lcom/my/target/cw;->addView(Landroid/view/View;)V

    .line 1651
    iget-object v0, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    invoke-virtual {p0, v0}, Lcom/my/target/cw;->addView(Landroid/view/View;)V

    .line 1652
    iget-object v0, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/my/target/cw;->addView(Landroid/view/View;)V

    .line 1653
    iget-object v0, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    invoke-virtual {p0, v0}, Lcom/my/target/cw;->addView(Landroid/view/View;)V

    .line 1654
    iget-object v0, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/my/target/cw;->addView(Landroid/view/View;)V

    .line 1656
    iget-object v0, p0, Lcom/my/target/cw;->an:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/cw;->av:Lcom/my/target/cu;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1657
    iget-object v0, p0, Lcom/my/target/cw;->an:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/cw;->ao:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1659
    iget-object v0, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/my/target/cw;->ay:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1660
    iget-object v0, p0, Lcom/my/target/cw;->am:Landroid/widget/Button;

    iget-object v1, p0, Lcom/my/target/cw;->ay:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1661
    iget-object v0, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    iget-object v1, p0, Lcom/my/target/cw;->ay:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    return-void
.end method

.method static synthetic E()I
    .locals 1

    .prologue
    .line 33
    sget v0, Lcom/my/target/cw;->ac:I

    return v0
.end method

.method static synthetic F()I
    .locals 1

    .prologue
    .line 33
    sget v0, Lcom/my/target/cw;->ad:I

    return v0
.end method

.method static synthetic G()I
    .locals 1

    .prologue
    .line 33
    sget v0, Lcom/my/target/cw;->af:I

    return v0
.end method

.method static synthetic H()I
    .locals 1

    .prologue
    .line 33
    sget v0, Lcom/my/target/cw;->ae:I

    return v0
.end method

.method static synthetic I()I
    .locals 1

    .prologue
    .line 33
    sget v0, Lcom/my/target/cw;->ab:I

    return v0
.end method

.method static synthetic J()I
    .locals 1

    .prologue
    .line 33
    sget v0, Lcom/my/target/cw;->SOUND_BUTTON_ID:I

    return v0
.end method

.method static synthetic a(Lcom/my/target/cw;)Lcom/my/target/cw$d;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/my/target/cw;->aF:Lcom/my/target/cw$d;

    return-object v0
.end method

.method static synthetic b(Lcom/my/target/cw;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/my/target/cw;->aB:I

    return v0
.end method

.method static synthetic c(Lcom/my/target/cw;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 33
    .line 3479
    iget v0, p0, Lcom/my/target/cw;->aB:I

    if-eqz v0, :cond_0

    .line 3481
    const/4 v0, 0x0

    iput v0, p0, Lcom/my/target/cw;->aB:I

    .line 3482
    iget-object v0, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    .line 3483
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3484
    iget-object v0, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getProgressBarView()Landroid/widget/ProgressBar;

    move-result-object v0

    .line 3485
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 3486
    iget-object v0, p0, Lcom/my/target/cw;->an:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3487
    iget-object v0, p0, Lcom/my/target/cw;->au:Lcom/my/target/cu;

    invoke-virtual {v0, v1}, Lcom/my/target/cu;->setVisibility(I)V

    .line 3488
    iget-object v0, p0, Lcom/my/target/cw;->at:Lcom/my/target/cu;

    invoke-virtual {v0, v1}, Lcom/my/target/cu;->setVisibility(I)V

    .line 3489
    iget-object v0, p0, Lcom/my/target/cw;->ap:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 33
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/my/target/cw;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/my/target/cw;->aw:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic e(Lcom/my/target/cw;)V
    .locals 3

    .prologue
    const/4 v1, 0x2

    const/16 v2, 0x8

    .line 33
    .line 3498
    iget v0, p0, Lcom/my/target/cw;->aB:I

    if-eq v0, v1, :cond_0

    .line 3500
    iput v1, p0, Lcom/my/target/cw;->aB:I

    .line 3501
    iget-object v0, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    .line 3502
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3503
    iget-object v0, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getProgressBarView()Landroid/widget/ProgressBar;

    move-result-object v0

    .line 3504
    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 3505
    iget-object v0, p0, Lcom/my/target/cw;->an:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3506
    iget-object v0, p0, Lcom/my/target/cw;->au:Lcom/my/target/cu;

    invoke-virtual {v0, v2}, Lcom/my/target/cu;->setVisibility(I)V

    .line 3507
    iget-object v0, p0, Lcom/my/target/cw;->at:Lcom/my/target/cu;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/my/target/cu;->setVisibility(I)V

    .line 3508
    iget-object v0, p0, Lcom/my/target/cw;->ap:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 33
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()V
    .locals 3

    .prologue
    const/4 v1, 0x3

    const/16 v2, 0x8

    .line 234
    iget v0, p0, Lcom/my/target/cw;->aB:I

    if-eq v0, v1, :cond_0

    .line 236
    iput v1, p0, Lcom/my/target/cw;->aB:I

    .line 237
    iget-object v0, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    .line 238
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 239
    iget-object v0, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getProgressBarView()Landroid/widget/ProgressBar;

    move-result-object v0

    .line 240
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 241
    iget-object v0, p0, Lcom/my/target/cw;->an:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 242
    iget-object v0, p0, Lcom/my/target/cw;->au:Lcom/my/target/cu;

    invoke-virtual {v0, v2}, Lcom/my/target/cu;->setVisibility(I)V

    .line 243
    iget-object v0, p0, Lcom/my/target/cw;->at:Lcom/my/target/cu;

    invoke-virtual {v0, v2}, Lcom/my/target/cu;->setVisibility(I)V

    .line 244
    iget-object v0, p0, Lcom/my/target/cw;->ap:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 246
    :cond_0
    return-void
.end method

.method public final B()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 253
    iget v0, p0, Lcom/my/target/cw;->aB:I

    if-eq v0, v3, :cond_0

    .line 255
    iput v3, p0, Lcom/my/target/cw;->aB:I

    .line 256
    iget-object v0, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    .line 257
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 258
    iget-object v0, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getProgressBarView()Landroid/widget/ProgressBar;

    move-result-object v0

    .line 259
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 260
    iget-object v0, p0, Lcom/my/target/cw;->an:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 261
    iget-object v0, p0, Lcom/my/target/cw;->au:Lcom/my/target/cu;

    invoke-virtual {v0, v2}, Lcom/my/target/cu;->setVisibility(I)V

    .line 262
    iget-object v0, p0, Lcom/my/target/cw;->at:Lcom/my/target/cu;

    invoke-virtual {v0, v1}, Lcom/my/target/cu;->setVisibility(I)V

    .line 263
    iget-object v0, p0, Lcom/my/target/cw;->ap:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 265
    :cond_0
    return-void
.end method

.method public final C()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/16 v1, 0x8

    .line 273
    iget v0, p0, Lcom/my/target/cw;->aB:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/my/target/cw;->aB:I

    if-eq v0, v2, :cond_0

    .line 275
    const/4 v0, 0x0

    iput v0, p0, Lcom/my/target/cw;->aB:I

    .line 276
    iget-object v0, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    .line 277
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 278
    iget-object v0, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getProgressBarView()Landroid/widget/ProgressBar;

    move-result-object v0

    .line 279
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 280
    iget-object v0, p0, Lcom/my/target/cw;->an:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 281
    iget-object v0, p0, Lcom/my/target/cw;->au:Lcom/my/target/cu;

    invoke-virtual {v0, v1}, Lcom/my/target/cu;->setVisibility(I)V

    .line 282
    iget v0, p0, Lcom/my/target/cw;->aB:I

    if-eq v0, v2, :cond_0

    .line 284
    iget-object v0, p0, Lcom/my/target/cw;->at:Lcom/my/target/cu;

    invoke-virtual {v0, v1}, Lcom/my/target/cu;->setVisibility(I)V

    .line 287
    :cond_0
    return-void
.end method

.method public final D()V
    .locals 2

    .prologue
    .line 305
    iget-object v0, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    .line 306
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 307
    return-void
.end method

.method public final a(Lcom/my/target/core/models/banners/a;Lcom/my/target/common/models/VideoData;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v4, 0x0

    .line 131
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getVideoBanner()Lcom/my/target/aj;

    move-result-object v0

    .line 132
    if-nez v0, :cond_1

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    iget-object v1, p0, Lcom/my/target/cw;->ar:Lcom/my/target/cx;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getDuration()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/my/target/cx;->setMax(F)V

    .line 139
    invoke-virtual {v0}, Lcom/my/target/aj;->isAllowReplay()Z

    move-result v1

    iput-boolean v1, p0, Lcom/my/target/cw;->aD:Z

    .line 141
    iget-object v1, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getCtaText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v1, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    const-string v1, "store"

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getNavigationType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 146
    iget-object v1, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 147
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getVotes()I

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getRating()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    .line 149
    iget-object v1, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    invoke-virtual {v1, v4}, Lcom/my/target/ca;->setVisibility(I)V

    .line 150
    iget-object v1, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getRating()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/my/target/ca;->setRating(F)V

    .line 164
    :goto_1
    invoke-virtual {v0}, Lcom/my/target/aj;->getCloseActionText()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/my/target/cw;->aE:Ljava/lang/String;

    .line 165
    iget-object v1, p0, Lcom/my/target/cw;->am:Landroid/widget/Button;

    iget-object v2, p0, Lcom/my/target/cw;->aE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 167
    iget-object v1, p0, Lcom/my/target/cw;->ao:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/my/target/aj;->getReplayActionText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    invoke-virtual {p0}, Lcom/my/target/cw;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 3122
    const-string v1, "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AMXCjM59gfMOgAAA59JREFUeNrtmkloFEEUhl/N6KgxriiJOHEXF4gENYlgRFA8uyAoCNGggl68CCJ6EQx6cCFqUFzABQQRMYh4EfSi4IJbiFERQVxRgxuRMWri5yE1UBY9yWh6Znq6+z+96a6ZV/8/tbz3qkRChAgRwiMAFgJngWgQyVcDHXTiFKCCRH4Df6MFKAkK+W0W+VfAxCAQV8A+i/wTIB4E8lHgpEX+LjA8COQHAM8t8o3ATKC338kXA59IjXbgEVAPLAb6+k2AHRbhVrrGV+AwMNVPIpx3INncjRi/gTPAWL+IcMIieBGIAXFgGXAU+OggRALY7ItIEdhvkTsNRIz3MWCp3h1sXAGK/RAHnLKIHUzRdgnwwmr7Epjsh3jggkWsNkXbQuC4Q8g8Ld9F6ANctYht7KL9GiNpAnib94ujDo5uaUIdwMo00uafhghNQL98F2EYcA9YlGb75daoOeqHnSHyj+23WyLMC1oRJQpct7LJXkETYSrwyxBhlQQNwAFrFKigCTDK2hXmBnEUnDMEOBZEARYaArwLogAF1jRIq4YQ8YsASqmEiNw2HpUHSgCNB4Y9KYgCPDXs8UEU4LNhF6bzhW7DRmCEiFTqjx+VUtc8LECrYfd3RQA9lxq03SgiZR4WIGbYv9yaAt8Me5DHp8DAFP3ukQDvDTvu8WxrnGG/dUUApdQrEUkYU2a8hwWYnGJH6PEu0GzYczwaCSoRqTIeNbkpwFXDnu/Rf79URJKnyz9E5Kab6i4wYuxvQKEHR8BO8+DE7R+PAu8MB6s9Rj4GvDH6tzYTTvZaVZeIhwSoMfr2HRiUCSd21aXGI+T7WRcu6jLp7Ijh6AMw1AMCmKXxtozeLgOKgC+Gw4ZcFiCB2fpWSRLbsuF0nXUYsSlH5EfqE+IknmblSo0+1m6wRKjOMvnB+jzQHPpl2ezAEOCx0YEOYH2WfMct8rlZkIES4LXVkXqgTwZ9Vjn43JrLFXgC8Mzq0H2g0mU/BTrSa7d8bfHCPlwE3HC41XWyp9fbdIS3Tt8fxprzq7wUivYGdmnithCXgRXpxgw67K4A9ljhdxKPgVK3+q5cFqJcRA6JyAyn1zpFfaBz9RZdtYnpStNonc/PEpEhDt9vE5FaEdmtlPrh2YoEEAEWpbje9j9o1aNrhOQbgOlAncNC2R0SwCVgZaZTb5VFMUpEpEJEpojIGBEZLJ21+zbpLGe3iMgTEXkoIneUUj8lRIgQIUKECJFJ/AEepzU1TSID5QAAAABJRU5ErkJggg=="

    invoke-static {v1, v4}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    .line 3123
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 3124
    const/16 v3, 0x1a4

    iput v3, v2, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    .line 3125
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 3126
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v0, v2, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    .line 3127
    array-length v0, v1

    invoke-static {v1, v4, v0, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 169
    if-eqz v0, :cond_2

    .line 171
    iget-object v1, p0, Lcom/my/target/cw;->av:Lcom/my/target/cu;

    invoke-virtual {v1, v0}, Lcom/my/target/cu;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 174
    :cond_2
    iget-object v0, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {p2}, Lcom/my/target/common/models/VideoData;->getWidth()I

    move-result v1

    .line 175
    invoke-virtual {p2}, Lcom/my/target/common/models/VideoData;->getHeight()I

    move-result v2

    .line 174
    invoke-virtual {v0, v1, v2}, Lcom/my/target/nativeads/views/MediaAdView;->setPlaceHolderDimension(II)V

    .line 176
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 177
    if-eqz v0, :cond_0

    .line 179
    iget-object v1, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v1}, Lcom/my/target/nativeads/views/MediaAdView;->getImageView()Landroid/widget/ImageView;

    move-result-object v1

    .line 180
    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 154
    :cond_3
    iget-object v1, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    invoke-virtual {v1, v3}, Lcom/my/target/ca;->setVisibility(I)V

    goto :goto_1

    .line 159
    :cond_4
    iget-object v1, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    invoke-virtual {v1, v3}, Lcom/my/target/ca;->setVisibility(I)V

    .line 160
    iget-object v1, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 161
    iget-object v1, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getDomain()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final b(FF)V
    .locals 4
    .param p1, "progress"    # F
    .param p2, "videoBannerDuration"    # F

    .prologue
    .line 196
    iget-object v0, p0, Lcom/my/target/cw;->ar:Lcom/my/target/cx;

    invoke-virtual {v0}, Lcom/my/target/cx;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/my/target/cw;->ar:Lcom/my/target/cx;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/my/target/cx;->setVisibility(I)V

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/my/target/cw;->ar:Lcom/my/target/cx;

    div-float v1, p1, p2

    invoke-virtual {v0, v1}, Lcom/my/target/cx;->setProgress(F)V

    .line 201
    iget-object v0, p0, Lcom/my/target/cw;->ar:Lcom/my/target/cx;

    sub-float v1, p2, p1

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/my/target/cx;->setDigit(I)V

    .line 202
    return-void
.end method

.method public final b(Lcom/my/target/cv;)V
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/my/target/nativeads/views/MediaAdView;->addView(Landroid/view/View;I)V

    .line 192
    return-void
.end method

.method public final c(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 291
    if-eqz p1, :cond_0

    .line 293
    iget-object v0, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    iget-object v1, p0, Lcom/my/target/cw;->aA:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2}, Lcom/my/target/by;->b(Landroid/graphics/Bitmap;Z)V

    .line 294
    iget-object v0, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    const-string v1, "sound off"

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 301
    :goto_0
    return-void

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    iget-object v1, p0, Lcom/my/target/cw;->az:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2}, Lcom/my/target/by;->b(Landroid/graphics/Bitmap;Z)V

    .line 299
    iget-object v0, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    const-string v1, "sound on"

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected final onLayout(ZIIII)V
    .locals 8
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 366
    sub-int v0, p4, p2

    .line 367
    sub-int v1, p5, p3

    .line 369
    iget-object v2, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v2}, Lcom/my/target/nativeads/views/MediaAdView;->getMeasuredWidth()I

    move-result v2

    .line 370
    iget-object v3, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v3}, Lcom/my/target/nativeads/views/MediaAdView;->getMeasuredHeight()I

    move-result v3

    .line 372
    sub-int v4, v0, v2

    shr-int/lit8 v4, v4, 0x1

    .line 373
    sub-int v5, v1, v3

    shr-int/lit8 v5, v5, 0x1

    .line 374
    iget-object v6, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    add-int/2addr v2, v4

    add-int/2addr v3, v5

    invoke-virtual {v6, v4, v5, v2, v3}, Lcom/my/target/nativeads/views/MediaAdView;->layout(IIII)V

    .line 375
    iget-object v2, p0, Lcom/my/target/cw;->ap:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v3}, Lcom/my/target/nativeads/views/MediaAdView;->getLeft()I

    move-result v3

    iget-object v4, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v4}, Lcom/my/target/nativeads/views/MediaAdView;->getTop()I

    move-result v4

    iget-object v5, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v5}, Lcom/my/target/nativeads/views/MediaAdView;->getRight()I

    move-result v5

    iget-object v6, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v6}, Lcom/my/target/nativeads/views/MediaAdView;->getBottom()I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/widget/FrameLayout;->layout(IIII)V

    .line 377
    iget-object v2, p0, Lcom/my/target/cw;->au:Lcom/my/target/cu;

    invoke-virtual {v2}, Lcom/my/target/cu;->getMeasuredWidth()I

    move-result v2

    .line 378
    iget-object v3, p0, Lcom/my/target/cw;->au:Lcom/my/target/cu;

    invoke-virtual {v3}, Lcom/my/target/cu;->getMeasuredHeight()I

    move-result v3

    .line 379
    iget-object v4, p0, Lcom/my/target/cw;->au:Lcom/my/target/cu;

    shr-int/lit8 v5, p4, 0x1

    shr-int/lit8 v6, v2, 0x1

    sub-int/2addr v5, v6

    shr-int/lit8 v6, p5, 0x1

    shr-int/lit8 v7, v3, 0x1

    sub-int/2addr v6, v7

    shr-int/lit8 v7, p4, 0x1

    shr-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v7

    shr-int/lit8 v7, p5, 0x1

    shr-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v7

    invoke-virtual {v4, v5, v6, v2, v3}, Lcom/my/target/cu;->layout(IIII)V

    .line 384
    iget-object v2, p0, Lcom/my/target/cw;->at:Lcom/my/target/cu;

    invoke-virtual {v2}, Lcom/my/target/cu;->getMeasuredWidth()I

    move-result v2

    .line 385
    iget-object v3, p0, Lcom/my/target/cw;->at:Lcom/my/target/cu;

    invoke-virtual {v3}, Lcom/my/target/cu;->getMeasuredHeight()I

    move-result v3

    .line 386
    iget-object v4, p0, Lcom/my/target/cw;->at:Lcom/my/target/cu;

    shr-int/lit8 v5, p4, 0x1

    shr-int/lit8 v6, v2, 0x1

    sub-int/2addr v5, v6

    shr-int/lit8 v6, p5, 0x1

    shr-int/lit8 v7, v3, 0x1

    sub-int/2addr v6, v7

    shr-int/lit8 v7, p4, 0x1

    shr-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v7

    shr-int/lit8 v7, p5, 0x1

    shr-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v7

    invoke-virtual {v4, v5, v6, v2, v3}, Lcom/my/target/cu;->layout(IIII)V

    .line 391
    iget-object v2, p0, Lcom/my/target/cw;->an:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v2

    .line 392
    iget-object v3, p0, Lcom/my/target/cw;->an:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v3

    .line 393
    iget-object v4, p0, Lcom/my/target/cw;->an:Landroid/widget/LinearLayout;

    shr-int/lit8 v5, p4, 0x1

    shr-int/lit8 v6, v2, 0x1

    sub-int/2addr v5, v6

    shr-int/lit8 v6, p5, 0x1

    shr-int/lit8 v7, v3, 0x1

    sub-int/2addr v6, v7

    shr-int/lit8 v7, p4, 0x1

    shr-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v7

    shr-int/lit8 v7, p5, 0x1

    shr-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v7

    invoke-virtual {v4, v5, v6, v2, v3}, Landroid/widget/LinearLayout;->layout(IIII)V

    .line 398
    iget-object v2, p0, Lcom/my/target/cw;->am:Landroid/widget/Button;

    iget v3, p0, Lcom/my/target/cw;->padding:I

    iget v4, p0, Lcom/my/target/cw;->padding:I

    iget v5, p0, Lcom/my/target/cw;->padding:I

    iget-object v6, p0, Lcom/my/target/cw;->am:Landroid/widget/Button;

    .line 400
    invoke-virtual {v6}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v5, v6

    iget v6, p0, Lcom/my/target/cw;->padding:I

    iget-object v7, p0, Lcom/my/target/cw;->am:Landroid/widget/Button;

    .line 401
    invoke-virtual {v7}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v6, v7

    .line 398
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/widget/Button;->layout(IIII)V

    .line 403
    if-le v0, v1, :cond_0

    .line 405
    iget-object v2, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v2

    iget-object v3, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    .line 406
    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    iget-object v4, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    .line 407
    invoke-virtual {v4}, Lcom/my/target/ca;->getMeasuredHeight()I

    move-result v4

    .line 406
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 405
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 410
    iget-object v3, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    iget v4, p0, Lcom/my/target/cw;->padding:I

    sub-int v4, v0, v4

    iget-object v5, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    invoke-virtual {v5}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v5

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/my/target/cw;->padding:I

    sub-int v5, v1, v5

    iget-object v6, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    .line 411
    invoke-virtual {v6}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v6

    sub-int/2addr v5, v6

    iget-object v6, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    invoke-virtual {v6}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v6

    sub-int v6, v2, v6

    shr-int/lit8 v6, v6, 0x1

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/my/target/cw;->padding:I

    sub-int/2addr v0, v6

    iget v6, p0, Lcom/my/target/cw;->padding:I

    sub-int v6, v1, v6

    iget-object v7, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    .line 413
    invoke-virtual {v7}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v7

    sub-int v7, v2, v7

    shr-int/lit8 v7, v7, 0x1

    sub-int/2addr v6, v7

    .line 410
    invoke-virtual {v3, v4, v5, v0, v6}, Landroid/widget/Button;->layout(IIII)V

    .line 414
    iget-object v0, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    iget-object v3, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getRight()I

    move-result v3

    iget-object v4, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    invoke-virtual {v4}, Lcom/my/target/by;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    invoke-virtual {v4}, Lcom/my/target/by;->getPadding()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    .line 415
    invoke-virtual {v4}, Lcom/my/target/nativeads/views/MediaAdView;->getBottom()I

    move-result v4

    iget v5, p0, Lcom/my/target/cw;->padding:I

    shl-int/lit8 v5, v5, 0x1

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    invoke-virtual {v5}, Lcom/my/target/by;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v4, v5

    sub-int/2addr v4, v2

    iget-object v5, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    invoke-virtual {v5}, Lcom/my/target/by;->getPadding()I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    .line 416
    invoke-virtual {v5}, Landroid/widget/Button;->getRight()I

    move-result v5

    iget-object v6, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    invoke-virtual {v6}, Lcom/my/target/by;->getPadding()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    .line 417
    invoke-virtual {v6}, Lcom/my/target/nativeads/views/MediaAdView;->getBottom()I

    move-result v6

    iget v7, p0, Lcom/my/target/cw;->padding:I

    shl-int/lit8 v7, v7, 0x1

    sub-int/2addr v6, v7

    sub-int/2addr v6, v2

    iget-object v7, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    invoke-virtual {v7}, Lcom/my/target/by;->getPadding()I

    move-result v7

    add-int/2addr v6, v7

    .line 414
    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/my/target/by;->layout(IIII)V

    .line 419
    iget-object v0, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    iget-object v3, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getLeft()I

    move-result v3

    iget v4, p0, Lcom/my/target/cw;->padding:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    invoke-virtual {v4}, Lcom/my/target/ca;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/my/target/cw;->padding:I

    sub-int v4, v1, v4

    iget-object v5, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    .line 420
    invoke-virtual {v5}, Lcom/my/target/ca;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    invoke-virtual {v5}, Lcom/my/target/ca;->getMeasuredHeight()I

    move-result v5

    sub-int v5, v2, v5

    shr-int/lit8 v5, v5, 0x1

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    .line 421
    invoke-virtual {v5}, Landroid/widget/Button;->getLeft()I

    move-result v5

    iget v6, p0, Lcom/my/target/cw;->padding:I

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/my/target/cw;->padding:I

    sub-int v6, v1, v6

    iget-object v7, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    .line 422
    invoke-virtual {v7}, Lcom/my/target/ca;->getMeasuredHeight()I

    move-result v7

    sub-int v7, v2, v7

    shr-int/lit8 v7, v7, 0x1

    sub-int/2addr v6, v7

    .line 419
    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/my/target/ca;->layout(IIII)V

    .line 423
    iget-object v0, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getLeft()I

    move-result v3

    iget v4, p0, Lcom/my/target/cw;->padding:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/my/target/cw;->padding:I

    sub-int v4, v1, v4

    iget-object v5, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    .line 424
    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    sub-int v5, v2, v5

    shr-int/lit8 v5, v5, 0x1

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    .line 425
    invoke-virtual {v5}, Landroid/widget/Button;->getLeft()I

    move-result v5

    iget v6, p0, Lcom/my/target/cw;->padding:I

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/my/target/cw;->padding:I

    sub-int v6, v1, v6

    iget-object v7, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    .line 426
    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v7

    sub-int v7, v2, v7

    shr-int/lit8 v7, v7, 0x1

    sub-int/2addr v6, v7

    .line 423
    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/widget/TextView;->layout(IIII)V

    .line 428
    iget-object v0, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    invoke-virtual {v0}, Lcom/my/target/ca;->getLeft()I

    move-result v0

    iget-object v3, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getLeft()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 429
    iget-object v3, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    iget v4, p0, Lcom/my/target/cw;->padding:I

    sub-int v4, v0, v4

    iget-object v5, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/my/target/cw;->padding:I

    sub-int v5, v1, v5

    iget-object v6, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    .line 430
    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    sub-int/2addr v5, v6

    iget-object v6, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    sub-int v6, v2, v6

    shr-int/lit8 v6, v6, 0x1

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/my/target/cw;->padding:I

    sub-int/2addr v0, v6

    iget v6, p0, Lcom/my/target/cw;->padding:I

    sub-int v6, v1, v6

    iget-object v7, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    .line 432
    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v7

    sub-int v7, v2, v7

    shr-int/lit8 v7, v7, 0x1

    sub-int/2addr v6, v7

    .line 429
    invoke-virtual {v3, v4, v5, v0, v6}, Landroid/widget/TextView;->layout(IIII)V

    .line 434
    iget-object v0, p0, Lcom/my/target/cw;->ar:Lcom/my/target/cx;

    iget v3, p0, Lcom/my/target/cw;->padding:I

    iget v4, p0, Lcom/my/target/cw;->padding:I

    sub-int v4, v1, v4

    iget-object v5, p0, Lcom/my/target/cw;->ar:Lcom/my/target/cx;

    .line 435
    invoke-virtual {v5}, Lcom/my/target/cx;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/cw;->ar:Lcom/my/target/cx;

    invoke-virtual {v5}, Lcom/my/target/cx;->getMeasuredHeight()I

    move-result v5

    sub-int v5, v2, v5

    shr-int/lit8 v5, v5, 0x1

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/my/target/cw;->padding:I

    iget-object v6, p0, Lcom/my/target/cw;->ar:Lcom/my/target/cx;

    .line 436
    invoke-virtual {v6}, Lcom/my/target/cx;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v5, v6

    iget v6, p0, Lcom/my/target/cw;->padding:I

    sub-int/2addr v1, v6

    iget-object v6, p0, Lcom/my/target/cw;->ar:Lcom/my/target/cx;

    .line 437
    invoke-virtual {v6}, Lcom/my/target/cx;->getMeasuredHeight()I

    move-result v6

    sub-int/2addr v2, v6

    shr-int/lit8 v2, v2, 0x1

    sub-int/2addr v1, v2

    .line 434
    invoke-virtual {v0, v3, v4, v5, v1}, Lcom/my/target/cx;->layout(IIII)V

    .line 472
    :goto_0
    return-void

    .line 442
    :cond_0
    iget-object v1, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    iget-object v2, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v2}, Lcom/my/target/nativeads/views/MediaAdView;->getRight()I

    move-result v2

    iget v3, p0, Lcom/my/target/cw;->padding:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    invoke-virtual {v3}, Lcom/my/target/by;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    invoke-virtual {v3}, Lcom/my/target/by;->getPadding()I

    move-result v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    .line 443
    invoke-virtual {v3}, Lcom/my/target/nativeads/views/MediaAdView;->getBottom()I

    move-result v3

    iget v4, p0, Lcom/my/target/cw;->padding:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    invoke-virtual {v4}, Lcom/my/target/by;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    invoke-virtual {v4}, Lcom/my/target/by;->getPadding()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    .line 444
    invoke-virtual {v4}, Lcom/my/target/nativeads/views/MediaAdView;->getRight()I

    move-result v4

    iget v5, p0, Lcom/my/target/cw;->padding:I

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    invoke-virtual {v5}, Lcom/my/target/by;->getPadding()I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    .line 445
    invoke-virtual {v5}, Lcom/my/target/nativeads/views/MediaAdView;->getBottom()I

    move-result v5

    iget v6, p0, Lcom/my/target/cw;->padding:I

    sub-int/2addr v5, v6

    iget-object v6, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    invoke-virtual {v6}, Lcom/my/target/by;->getPadding()I

    move-result v6

    add-int/2addr v5, v6

    .line 442
    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/my/target/by;->layout(IIII)V

    .line 447
    iget-object v1, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    shr-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    shr-int/lit8 v3, v3, 0x1

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    .line 448
    invoke-virtual {v3}, Lcom/my/target/nativeads/views/MediaAdView;->getBottom()I

    move-result v3

    iget v4, p0, Lcom/my/target/cw;->padding:I

    add-int/2addr v3, v4

    shr-int/lit8 v4, v0, 0x1

    iget-object v5, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    .line 449
    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    shr-int/lit8 v5, v5, 0x1

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    .line 450
    invoke-virtual {v5}, Lcom/my/target/nativeads/views/MediaAdView;->getBottom()I

    move-result v5

    iget v6, p0, Lcom/my/target/cw;->padding:I

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    .line 447
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/TextView;->layout(IIII)V

    .line 453
    iget-object v1, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    shr-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    invoke-virtual {v3}, Lcom/my/target/ca;->getMeasuredWidth()I

    move-result v3

    shr-int/lit8 v3, v3, 0x1

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    .line 454
    invoke-virtual {v3}, Landroid/widget/TextView;->getBottom()I

    move-result v3

    iget v4, p0, Lcom/my/target/cw;->padding:I

    add-int/2addr v3, v4

    shr-int/lit8 v4, v0, 0x1

    iget-object v5, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    .line 455
    invoke-virtual {v5}, Lcom/my/target/ca;->getMeasuredWidth()I

    move-result v5

    shr-int/lit8 v5, v5, 0x1

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    .line 456
    invoke-virtual {v5}, Landroid/widget/TextView;->getBottom()I

    move-result v5

    iget v6, p0, Lcom/my/target/cw;->padding:I

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    invoke-virtual {v6}, Lcom/my/target/ca;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    .line 453
    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/my/target/ca;->layout(IIII)V

    .line 458
    iget-object v1, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    shr-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    shr-int/lit8 v3, v3, 0x1

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    .line 459
    invoke-virtual {v3}, Landroid/widget/TextView;->getBottom()I

    move-result v3

    iget v4, p0, Lcom/my/target/cw;->padding:I

    add-int/2addr v3, v4

    shr-int/lit8 v4, v0, 0x1

    iget-object v5, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    .line 460
    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    shr-int/lit8 v5, v5, 0x1

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    .line 461
    invoke-virtual {v5}, Landroid/widget/TextView;->getBottom()I

    move-result v5

    iget v6, p0, Lcom/my/target/cw;->padding:I

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    .line 458
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/TextView;->layout(IIII)V

    .line 463
    iget-object v1, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    shr-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v3

    shr-int/lit8 v3, v3, 0x1

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    .line 464
    invoke-virtual {v3}, Lcom/my/target/ca;->getBottom()I

    move-result v3

    iget v4, p0, Lcom/my/target/cw;->padding:I

    add-int/2addr v3, v4

    shr-int/lit8 v0, v0, 0x1

    iget-object v4, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    .line 465
    invoke-virtual {v4}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v4

    shr-int/lit8 v4, v4, 0x1

    add-int/2addr v0, v4

    iget-object v4, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    .line 466
    invoke-virtual {v4}, Lcom/my/target/ca;->getBottom()I

    move-result v4

    iget v5, p0, Lcom/my/target/cw;->padding:I

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    invoke-virtual {v5}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 463
    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/widget/Button;->layout(IIII)V

    .line 467
    iget-object v0, p0, Lcom/my/target/cw;->ar:Lcom/my/target/cx;

    iget v1, p0, Lcom/my/target/cw;->padding:I

    iget-object v2, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    .line 468
    invoke-virtual {v2}, Lcom/my/target/nativeads/views/MediaAdView;->getBottom()I

    move-result v2

    iget v3, p0, Lcom/my/target/cw;->padding:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/my/target/cw;->ar:Lcom/my/target/cx;

    invoke-virtual {v3}, Lcom/my/target/cx;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/my/target/cw;->padding:I

    iget-object v4, p0, Lcom/my/target/cw;->ar:Lcom/my/target/cx;

    .line 469
    invoke-virtual {v4}, Lcom/my/target/cx;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    .line 470
    invoke-virtual {v4}, Lcom/my/target/nativeads/views/MediaAdView;->getBottom()I

    move-result v4

    iget v5, p0, Lcom/my/target/cw;->padding:I

    sub-int/2addr v4, v5

    .line 467
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/my/target/cx;->layout(IIII)V

    goto/16 :goto_0
.end method

.method protected final onMeasure(II)V
    .locals 9
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v8, -0x80000000

    .line 313
    iget-object v0, p0, Lcom/my/target/cw;->as:Lcom/my/target/by;

    iget v1, p0, Lcom/my/target/cw;->aC:I

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget v2, p0, Lcom/my/target/cw;->aC:I

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/my/target/by;->measure(II)V

    .line 314
    iget-object v0, p0, Lcom/my/target/cw;->ar:Lcom/my/target/cx;

    iget v1, p0, Lcom/my/target/cw;->aC:I

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget v2, p0, Lcom/my/target/cw;->aC:I

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/my/target/cx;->measure(II)V

    .line 316
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 317
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 320
    iget-object v2, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/my/target/nativeads/views/MediaAdView;->measure(II)V

    .line 322
    iget v2, p0, Lcom/my/target/cw;->padding:I

    shl-int/lit8 v2, v2, 0x1

    sub-int v2, v0, v2

    .line 323
    iget v3, p0, Lcom/my/target/cw;->padding:I

    shl-int/lit8 v3, v3, 0x1

    sub-int v3, v1, v3

    .line 325
    iget-object v4, p0, Lcom/my/target/cw;->am:Landroid/widget/Button;

    div-int/lit8 v5, v2, 0x2

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/Button;->measure(II)V

    .line 326
    iget-object v4, p0, Lcom/my/target/cw;->at:Lcom/my/target/cu;

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/my/target/cu;->measure(II)V

    .line 327
    iget-object v4, p0, Lcom/my/target/cw;->au:Lcom/my/target/cu;

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/my/target/cu;->measure(II)V

    .line 329
    iget-object v4, p0, Lcom/my/target/cw;->an:Landroid/widget/LinearLayout;

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/LinearLayout;->measure(II)V

    .line 330
    iget-object v4, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/my/target/ca;->measure(II)V

    .line 333
    iget-object v4, p0, Lcom/my/target/cw;->ap:Landroid/widget/FrameLayout;

    iget-object v5, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v5}, Lcom/my/target/nativeads/views/MediaAdView;->getMeasuredWidth()I

    move-result v5

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    iget-object v6, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v6}, Lcom/my/target/nativeads/views/MediaAdView;->getMeasuredHeight()I

    move-result v6

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/FrameLayout;->measure(II)V

    .line 335
    iget-object v4, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/Button;->measure(II)V

    .line 336
    iget-object v4, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->measure(II)V

    .line 337
    iget-object v4, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->measure(II)V

    .line 339
    if-le v0, v1, :cond_0

    .line 341
    iget-object v4, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v4

    .line 342
    iget-object v5, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    .line 343
    iget-object v6, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    invoke-virtual {v6}, Lcom/my/target/ca;->getMeasuredWidth()I

    move-result v6

    iget-object v7, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 344
    iget-object v7, p0, Lcom/my/target/cw;->ar:Lcom/my/target/cx;

    invoke-virtual {v7}, Lcom/my/target/cx;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v5, v7

    add-int/2addr v5, v6

    add-int/2addr v4, v5

    iget v5, p0, Lcom/my/target/cw;->padding:I

    mul-int/lit8 v5, v5, 0x3

    add-int/2addr v4, v5

    .line 349
    if-le v4, v2, :cond_0

    .line 351
    iget-object v4, p0, Lcom/my/target/cw;->ar:Lcom/my/target/cx;

    invoke-virtual {v4}, Lcom/my/target/cx;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v2, v4

    iget v4, p0, Lcom/my/target/cw;->padding:I

    mul-int/lit8 v4, v4, 0x3

    sub-int/2addr v2, v4

    .line 352
    iget-object v4, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    div-int/lit8 v5, v2, 0x3

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/Button;->measure(II)V

    .line 353
    iget-object v4, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    div-int/lit8 v5, v2, 0x3

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/my/target/ca;->measure(II)V

    .line 354
    iget-object v4, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    div-int/lit8 v5, v2, 0x3

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->measure(II)V

    .line 356
    iget-object v4, p0, Lcom/my/target/cw;->ctaButton:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v2, v4

    iget-object v4, p0, Lcom/my/target/cw;->aq:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v2, v4

    iget-object v4, p0, Lcom/my/target/cw;->al:Lcom/my/target/ca;

    invoke-virtual {v4}, Lcom/my/target/ca;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v2, v4

    .line 357
    iget-object v4, p0, Lcom/my/target/cw;->ak:Landroid/widget/TextView;

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v4, v2, v3}, Landroid/widget/TextView;->measure(II)V

    .line 360
    :cond_0
    invoke-virtual {p0, v0, v1}, Lcom/my/target/cw;->setMeasuredDimension(II)V

    .line 361
    return-void
.end method

.method public final setVideoDialogViewListener(Lcom/my/target/cw$d;)V
    .locals 0
    .param p1, "videoDialogViewListener"    # Lcom/my/target/cw$d;

    .prologue
    .line 186
    iput-object p1, p0, Lcom/my/target/cw;->aF:Lcom/my/target/cw$d;

    .line 187
    return-void
.end method

.method public final z()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 209
    iget v0, p0, Lcom/my/target/cw;->aB:I

    if-eq v0, v3, :cond_1

    .line 211
    iput v3, p0, Lcom/my/target/cw;->aB:I

    .line 212
    iget-object v0, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    .line 213
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 214
    iget-object v0, p0, Lcom/my/target/cw;->mediaAdView:Lcom/my/target/nativeads/views/MediaAdView;

    invoke-virtual {v0}, Lcom/my/target/nativeads/views/MediaAdView;->getProgressBarView()Landroid/widget/ProgressBar;

    move-result-object v0

    .line 215
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 217
    iget-boolean v0, p0, Lcom/my/target/cw;->aD:Z

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/my/target/cw;->an:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 220
    iget-object v0, p0, Lcom/my/target/cw;->ap:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/my/target/cw;->au:Lcom/my/target/cu;

    invoke-virtual {v0, v1}, Lcom/my/target/cu;->setVisibility(I)V

    .line 224
    iget-object v0, p0, Lcom/my/target/cw;->at:Lcom/my/target/cu;

    invoke-virtual {v0, v1}, Lcom/my/target/cu;->setVisibility(I)V

    .line 225
    iget-object v0, p0, Lcom/my/target/cw;->ar:Lcom/my/target/cx;

    invoke-virtual {v0, v1}, Lcom/my/target/cx;->setVisibility(I)V

    .line 227
    :cond_1
    return-void
.end method
