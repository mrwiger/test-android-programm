.class public final Lru/cn/ad/video/ui/VastPresenter;
.super Ljava/lang/Object;
.source "VastPresenter.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lru/cn/ad/video/ui/MediaControl$Listener;
.implements Lru/cn/player/SimplePlayer$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/ad/video/ui/VastPresenter$PresenterListener;,
        Lru/cn/ad/video/ui/VastPresenter$Tracker;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private adClickUri:Ljava/lang/String;

.field private final adContainer:Landroid/view/ViewGroup;

.field private adReady:Z

.field private adStarted:Z

.field private final contentUri:Ljava/lang/String;

.field private final context:Landroid/content/Context;

.field private firstQuartileReady:Z

.field private final handler:Landroid/os/Handler;

.field private final listener:Lru/cn/ad/video/ui/VastPresenter$PresenterListener;

.field private mediaControl:Lru/cn/ad/video/ui/MediaControl;

.field private midpointReady:Z

.field private final player:Lru/cn/player/SimplePlayer;

.field private skipOffset:Lru/cn/ad/video/VAST/parser/SkipOffset;

.field private thirdQuartileReady:Z

.field private final tracker:Lru/cn/ad/video/ui/VastPresenter$Tracker;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lru/cn/ad/video/ui/VastPresenter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lru/cn/ad/video/ui/VastPresenter;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lru/cn/ad/video/RenderingSettings;Ljava/lang/String;Lru/cn/ad/video/ui/VastPresenter$Tracker;Lru/cn/ad/video/ui/VastPresenter$PresenterListener;)V
    .locals 1
    .param p1, "settings"    # Lru/cn/ad/video/RenderingSettings;
    .param p2, "contentURI"    # Ljava/lang/String;
    .param p3, "tracker"    # Lru/cn/ad/video/ui/VastPresenter$Tracker;
    .param p4, "listener"    # Lru/cn/ad/video/ui/VastPresenter$PresenterListener;

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iget-object v0, p1, Lru/cn/ad/video/RenderingSettings;->player:Lru/cn/player/SimplePlayer;

    iput-object v0, p0, Lru/cn/ad/video/ui/VastPresenter;->player:Lru/cn/player/SimplePlayer;

    .line 86
    iget-object v0, p1, Lru/cn/ad/video/RenderingSettings;->container:Landroid/view/ViewGroup;

    iput-object v0, p0, Lru/cn/ad/video/ui/VastPresenter;->adContainer:Landroid/view/ViewGroup;

    .line 87
    iput-object p3, p0, Lru/cn/ad/video/ui/VastPresenter;->tracker:Lru/cn/ad/video/ui/VastPresenter$Tracker;

    .line 88
    iput-object p4, p0, Lru/cn/ad/video/ui/VastPresenter;->listener:Lru/cn/ad/video/ui/VastPresenter$PresenterListener;

    .line 89
    iput-object p2, p0, Lru/cn/ad/video/ui/VastPresenter;->contentUri:Ljava/lang/String;

    .line 91
    iget-object v0, p0, Lru/cn/ad/video/ui/VastPresenter;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/video/ui/VastPresenter;->context:Landroid/content/Context;

    .line 92
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lru/cn/ad/video/ui/VastPresenter;->handler:Landroid/os/Handler;

    .line 93
    return-void
.end method

.method private getVastError(II)I
    .locals 3
    .param p1, "playerType"    # I
    .param p2, "playerErrorCode"    # I

    .prologue
    const/16 v0, 0x192

    const/16 v1, 0x191

    .line 165
    const/4 v2, 0x5

    if-ne p1, v2, :cond_1

    .line 166
    sparse-switch p2, :sswitch_data_0

    .line 187
    :cond_0
    :goto_0
    const/16 v0, 0x190

    :goto_1
    :sswitch_0
    return v0

    :sswitch_1
    move v0, v1

    .line 171
    goto :goto_1

    .line 175
    :sswitch_2
    const/16 v0, 0x195

    goto :goto_1

    .line 177
    :cond_1
    if-nez p1, :cond_0

    .line 178
    sparse-switch p2, :sswitch_data_1

    goto :goto_0

    :sswitch_3
    move v0, v1

    .line 183
    goto :goto_1

    .line 166
    :sswitch_data_0
    .sparse-switch
        0x2f5 -> :sswitch_1
        0x2f8 -> :sswitch_2
        0x2fb -> :sswitch_2
        0x3e9 -> :sswitch_0
    .end sparse-switch

    .line 178
    :sswitch_data_1
    .sparse-switch
        -0x3ec -> :sswitch_3
        0x3e9 -> :sswitch_0
    .end sparse-switch
.end method

.method private onReady()V
    .locals 9

    .prologue
    .line 220
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 221
    new-instance v6, Lru/cn/ad/video/ui/TVVideoMediaControl;

    iget-object v7, p0, Lru/cn/ad/video/ui/VastPresenter;->context:Landroid/content/Context;

    invoke-direct {v6, v7}, Lru/cn/ad/video/ui/TVVideoMediaControl;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lru/cn/ad/video/ui/VastPresenter;->mediaControl:Lru/cn/ad/video/ui/MediaControl;

    .line 226
    :goto_0
    iget-object v7, p0, Lru/cn/ad/video/ui/VastPresenter;->mediaControl:Lru/cn/ad/video/ui/MediaControl;

    iget-object v6, p0, Lru/cn/ad/video/ui/VastPresenter;->adClickUri:Ljava/lang/String;

    if-eqz v6, :cond_4

    const/4 v6, 0x1

    :goto_1
    invoke-interface {v7, v6}, Lru/cn/ad/video/ui/MediaControl;->setClickable(Z)V

    .line 227
    iget-object v6, p0, Lru/cn/ad/video/ui/VastPresenter;->mediaControl:Lru/cn/ad/video/ui/MediaControl;

    invoke-interface {v6, p0}, Lru/cn/ad/video/ui/MediaControl;->setListener(Lru/cn/ad/video/ui/MediaControl$Listener;)V

    .line 229
    iget-object v6, p0, Lru/cn/ad/video/ui/VastPresenter;->mediaControl:Lru/cn/ad/video/ui/MediaControl;

    invoke-interface {v6}, Lru/cn/ad/video/ui/MediaControl;->getView()Landroid/view/View;

    move-result-object v0

    .line 230
    .local v0, "adWrapper":Landroid/view/View;
    iget-object v6, p0, Lru/cn/ad/video/ui/VastPresenter;->adContainer:Landroid/view/ViewGroup;

    if-eqz v6, :cond_5

    iget-object v1, p0, Lru/cn/ad/video/ui/VastPresenter;->adContainer:Landroid/view/ViewGroup;

    .line 231
    .local v1, "parentLayout":Landroid/view/ViewGroup;
    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 233
    iget-object v6, p0, Lru/cn/ad/video/ui/VastPresenter;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v6}, Lru/cn/player/SimplePlayer;->getDuration()I

    move-result v6

    int-to-long v2, v6

    .line 234
    .local v2, "duration":J
    iget-object v6, p0, Lru/cn/ad/video/ui/VastPresenter;->skipOffset:Lru/cn/ad/video/VAST/parser/SkipOffset;

    if-nez v6, :cond_0

    .line 235
    const-wide/16 v6, 0x4e20

    cmp-long v6, v2, v6

    if-lez v6, :cond_0

    .line 236
    new-instance v6, Lru/cn/ad/video/VAST/parser/SkipOffset;

    sget-object v7, Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;->SkipTypeTime:Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;

    const/high16 v8, 0x41700000    # 15.0f

    invoke-direct {v6, v7, v8}, Lru/cn/ad/video/VAST/parser/SkipOffset;-><init>(Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;F)V

    iput-object v6, p0, Lru/cn/ad/video/ui/VastPresenter;->skipOffset:Lru/cn/ad/video/VAST/parser/SkipOffset;

    .line 240
    :cond_0
    iget-object v6, p0, Lru/cn/ad/video/ui/VastPresenter;->skipOffset:Lru/cn/ad/video/VAST/parser/SkipOffset;

    if-eqz v6, :cond_2

    .line 241
    const-wide/16 v4, 0x0

    .line 242
    .local v4, "skipPositionMillis":J
    sget-object v6, Lru/cn/ad/video/ui/VastPresenter$1;->$SwitchMap$ru$cn$ad$video$VAST$parser$SkipOffset$SkipType:[I

    iget-object v7, p0, Lru/cn/ad/video/ui/VastPresenter;->skipOffset:Lru/cn/ad/video/VAST/parser/SkipOffset;

    iget-object v7, v7, Lru/cn/ad/video/VAST/parser/SkipOffset;->type:Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;

    invoke-virtual {v7}, Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 251
    :goto_3
    const-wide/16 v6, 0x3a98

    cmp-long v6, v4, v6

    if-lez v6, :cond_1

    .line 252
    const-wide/16 v4, 0x3a98

    .line 255
    :cond_1
    iget-object v6, p0, Lru/cn/ad/video/ui/VastPresenter;->mediaControl:Lru/cn/ad/video/ui/MediaControl;

    invoke-interface {v6, v4, v5}, Lru/cn/ad/video/ui/MediaControl;->setSkipPositionMs(J)V

    .line 258
    .end local v4    # "skipPositionMillis":J
    :cond_2
    iget-object v6, p0, Lru/cn/ad/video/ui/VastPresenter;->mediaControl:Lru/cn/ad/video/ui/MediaControl;

    invoke-interface {v6, v2, v3}, Lru/cn/ad/video/ui/MediaControl;->setDuration(J)V

    .line 260
    iget-object v6, p0, Lru/cn/ad/video/ui/VastPresenter;->tracker:Lru/cn/ad/video/ui/VastPresenter$Tracker;

    iget-object v7, p0, Lru/cn/ad/video/ui/VastPresenter;->context:Landroid/content/Context;

    invoke-interface {v6, v7}, Lru/cn/ad/video/ui/VastPresenter$Tracker;->trackImpression(Landroid/content/Context;)V

    .line 261
    return-void

    .line 223
    .end local v0    # "adWrapper":Landroid/view/View;
    .end local v1    # "parentLayout":Landroid/view/ViewGroup;
    .end local v2    # "duration":J
    :cond_3
    new-instance v6, Lru/cn/ad/video/ui/MobileVideoMediaControl;

    iget-object v7, p0, Lru/cn/ad/video/ui/VastPresenter;->context:Landroid/content/Context;

    invoke-direct {v6, v7}, Lru/cn/ad/video/ui/MobileVideoMediaControl;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lru/cn/ad/video/ui/VastPresenter;->mediaControl:Lru/cn/ad/video/ui/MediaControl;

    goto :goto_0

    .line 226
    :cond_4
    const/4 v6, 0x0

    goto :goto_1

    .line 230
    .restart local v0    # "adWrapper":Landroid/view/View;
    :cond_5
    iget-object v6, p0, Lru/cn/ad/video/ui/VastPresenter;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v6}, Lru/cn/player/SimplePlayer;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    move-object v1, v6

    goto :goto_2

    .line 244
    .restart local v1    # "parentLayout":Landroid/view/ViewGroup;
    .restart local v2    # "duration":J
    .restart local v4    # "skipPositionMillis":J
    :pswitch_0
    long-to-float v6, v2

    iget-object v7, p0, Lru/cn/ad/video/ui/VastPresenter;->skipOffset:Lru/cn/ad/video/VAST/parser/SkipOffset;

    iget v7, v7, Lru/cn/ad/video/VAST/parser/SkipOffset;->value:F

    mul-float/2addr v6, v7

    float-to-long v4, v6

    .line 245
    goto :goto_3

    .line 247
    :pswitch_1
    iget-object v6, p0, Lru/cn/ad/video/ui/VastPresenter;->skipOffset:Lru/cn/ad/video/VAST/parser/SkipOffset;

    iget v6, v6, Lru/cn/ad/video/VAST/parser/SkipOffset;->value:F

    const/high16 v7, 0x447a0000    # 1000.0f

    mul-float/2addr v6, v7

    float-to-long v4, v6

    goto :goto_3

    .line 242
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private onStarted()V
    .locals 2

    .prologue
    .line 211
    sget-object v0, Lru/cn/ad/video/ui/VastPresenter;->LOG_TAG:Ljava/lang/String;

    const-string v1, "Started ad"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    const-string v0, "start"

    invoke-direct {p0, v0}, Lru/cn/ad/video/ui/VastPresenter;->trackEvent(Ljava/lang/String;)V

    .line 214
    const-string v0, "creativeView"

    invoke-direct {p0, v0}, Lru/cn/ad/video/ui/VastPresenter;->trackEvent(Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lru/cn/ad/video/ui/VastPresenter;->listener:Lru/cn/ad/video/ui/VastPresenter$PresenterListener;

    invoke-interface {v0}, Lru/cn/ad/video/ui/VastPresenter$PresenterListener;->adStarted()V

    .line 217
    return-void
.end method

.method private onStopped()V
    .locals 5

    .prologue
    .line 264
    sget-object v2, Lru/cn/ad/video/ui/VastPresenter;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Stopped ad"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    iget-object v2, p0, Lru/cn/ad/video/ui/VastPresenter;->mediaControl:Lru/cn/ad/video/ui/MediaControl;

    if-eqz v2, :cond_0

    .line 267
    iget-object v2, p0, Lru/cn/ad/video/ui/VastPresenter;->mediaControl:Lru/cn/ad/video/ui/MediaControl;

    invoke-interface {v2}, Lru/cn/ad/video/ui/MediaControl;->getView()Landroid/view/View;

    move-result-object v0

    .line 268
    .local v0, "adWrapper":Landroid/view/View;
    iget-object v2, p0, Lru/cn/ad/video/ui/VastPresenter;->adContainer:Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    iget-object v1, p0, Lru/cn/ad/video/ui/VastPresenter;->adContainer:Landroid/view/ViewGroup;

    .line 269
    .local v1, "parentLayout":Landroid/view/ViewGroup;
    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 272
    .end local v0    # "adWrapper":Landroid/view/View;
    .end local v1    # "parentLayout":Landroid/view/ViewGroup;
    :cond_0
    iget-object v2, p0, Lru/cn/ad/video/ui/VastPresenter;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v2, p0}, Lru/cn/player/SimplePlayer;->removeListener(Lru/cn/player/SimplePlayer$Listener;)V

    .line 274
    iget-object v2, p0, Lru/cn/ad/video/ui/VastPresenter;->listener:Lru/cn/ad/video/ui/VastPresenter$PresenterListener;

    invoke-interface {v2}, Lru/cn/ad/video/ui/VastPresenter$PresenterListener;->adCompleted()V

    .line 276
    invoke-static {}, Lru/cn/peersay/controllers/DialogsRemoteController;->sharedInstance()Lru/cn/peersay/controllers/DialogsRemoteController;

    move-result-object v2

    iget-object v3, p0, Lru/cn/ad/video/ui/VastPresenter;->context:Landroid/content/Context;

    const-string v4, "ad_skip"

    invoke-virtual {v2, v3, v4}, Lru/cn/peersay/controllers/DialogsRemoteController;->onDialogDismiss(Landroid/content/Context;Ljava/lang/String;)V

    .line 277
    return-void

    .line 268
    .restart local v0    # "adWrapper":Landroid/view/View;
    :cond_1
    iget-object v2, p0, Lru/cn/ad/video/ui/VastPresenter;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v2}, Lru/cn/player/SimplePlayer;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    move-object v1, v2

    goto :goto_0
.end method

.method private trackClick()V
    .locals 3

    .prologue
    .line 344
    sget-object v0, Lru/cn/ad/video/ui/VastPresenter;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Track click "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lru/cn/ad/video/ui/VastPresenter;->adClickUri:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    iget-object v0, p0, Lru/cn/ad/video/ui/VastPresenter;->tracker:Lru/cn/ad/video/ui/VastPresenter$Tracker;

    iget-object v1, p0, Lru/cn/ad/video/ui/VastPresenter;->context:Landroid/content/Context;

    invoke-interface {v0, v1}, Lru/cn/ad/video/ui/VastPresenter$Tracker;->trackClick(Landroid/content/Context;)V

    .line 347
    return-void
.end method

.method private trackEvent(Ljava/lang/String;)V
    .locals 3
    .param p1, "eventName"    # Ljava/lang/String;

    .prologue
    .line 338
    sget-object v0, Lru/cn/ad/video/ui/VastPresenter;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Track event "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    iget-object v0, p0, Lru/cn/ad/video/ui/VastPresenter;->tracker:Lru/cn/ad/video/ui/VastPresenter$Tracker;

    iget-object v1, p0, Lru/cn/ad/video/ui/VastPresenter;->context:Landroid/content/Context;

    invoke-interface {v0, v1, p1}, Lru/cn/ad/video/ui/VastPresenter$Tracker;->trackEvent(Landroid/content/Context;Ljava/lang/String;)V

    .line 341
    return-void
.end method

.method private updateProgress()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 298
    iget-object v4, p0, Lru/cn/ad/video/ui/VastPresenter;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v4}, Lru/cn/player/SimplePlayer;->getCurrentPosition()I

    move-result v1

    .line 299
    .local v1, "position":I
    iget-object v4, p0, Lru/cn/ad/video/ui/VastPresenter;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v4}, Lru/cn/player/SimplePlayer;->getDuration()I

    move-result v0

    .line 300
    .local v0, "duration":I
    if-gtz v0, :cond_1

    .line 322
    :cond_0
    :goto_0
    return-void

    .line 303
    :cond_1
    mul-int/lit8 v4, v1, 0x64

    div-int/2addr v4, v0

    int-to-double v2, v4

    .line 304
    .local v2, "progress":D
    const-wide/high16 v4, 0x4039000000000000L    # 25.0

    cmpl-double v4, v2, v4

    if-ltz v4, :cond_2

    iget-boolean v4, p0, Lru/cn/ad/video/ui/VastPresenter;->firstQuartileReady:Z

    if-nez v4, :cond_2

    .line 305
    iput-boolean v6, p0, Lru/cn/ad/video/ui/VastPresenter;->firstQuartileReady:Z

    .line 306
    const-string v4, "firstQuartile"

    invoke-direct {p0, v4}, Lru/cn/ad/video/ui/VastPresenter;->trackEvent(Ljava/lang/String;)V

    .line 309
    :cond_2
    const-wide/high16 v4, 0x4049000000000000L    # 50.0

    cmpl-double v4, v2, v4

    if-ltz v4, :cond_3

    iget-boolean v4, p0, Lru/cn/ad/video/ui/VastPresenter;->midpointReady:Z

    if-nez v4, :cond_3

    .line 310
    iput-boolean v6, p0, Lru/cn/ad/video/ui/VastPresenter;->midpointReady:Z

    .line 311
    const-string v4, "midpoint"

    invoke-direct {p0, v4}, Lru/cn/ad/video/ui/VastPresenter;->trackEvent(Ljava/lang/String;)V

    .line 314
    :cond_3
    const-wide v4, 0x4052c00000000000L    # 75.0

    cmpl-double v4, v2, v4

    if-ltz v4, :cond_4

    iget-boolean v4, p0, Lru/cn/ad/video/ui/VastPresenter;->thirdQuartileReady:Z

    if-nez v4, :cond_4

    .line 315
    iput-boolean v6, p0, Lru/cn/ad/video/ui/VastPresenter;->thirdQuartileReady:Z

    .line 316
    const-string v4, "thirdQuartile"

    invoke-direct {p0, v4}, Lru/cn/ad/video/ui/VastPresenter;->trackEvent(Ljava/lang/String;)V

    .line 319
    :cond_4
    iget-object v4, p0, Lru/cn/ad/video/ui/VastPresenter;->mediaControl:Lru/cn/ad/video/ui/MediaControl;

    if-eqz v4, :cond_0

    .line 320
    iget-object v4, p0, Lru/cn/ad/video/ui/VastPresenter;->mediaControl:Lru/cn/ad/video/ui/MediaControl;

    int-to-long v6, v1

    invoke-interface {v4, v6, v7}, Lru/cn/ad/video/ui/MediaControl;->setPosition(J)V

    goto :goto_0
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lru/cn/ad/video/ui/VastPresenter;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 112
    iget-object v0, p0, Lru/cn/ad/video/ui/VastPresenter;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->stop()V

    .line 114
    invoke-direct {p0}, Lru/cn/ad/video/ui/VastPresenter;->onStopped()V

    .line 115
    return-void
.end method

.method public endBuffering()V
    .locals 0

    .prologue
    .line 203
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v0, 0x1

    .line 326
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 334
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 328
    :pswitch_0
    invoke-direct {p0}, Lru/cn/ad/video/ui/VastPresenter;->updateProgress()V

    .line 329
    iget-object v1, p0, Lru/cn/ad/video/ui/VastPresenter;->handler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 326
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onClicked()V
    .locals 3

    .prologue
    .line 289
    invoke-direct {p0}, Lru/cn/ad/video/ui/VastPresenter;->trackClick()V

    .line 291
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lru/cn/ad/video/ui/VastPresenter;->adClickUri:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 292
    .local v0, "browserIntent":Landroid/content/Intent;
    iget-object v1, p0, Lru/cn/ad/video/ui/VastPresenter;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 295
    return-void
.end method

.method public onComplete()V
    .locals 1

    .prologue
    .line 151
    const-string v0, "complete"

    invoke-direct {p0, v0}, Lru/cn/ad/video/ui/VastPresenter;->trackEvent(Ljava/lang/String;)V

    .line 152
    return-void
.end method

.method public onError(II)V
    .locals 3
    .param p1, "playerType"    # I
    .param p2, "code"    # I

    .prologue
    .line 156
    invoke-direct {p0, p1, p2}, Lru/cn/ad/video/ui/VastPresenter;->getVastError(II)I

    move-result v0

    .line 157
    .local v0, "vastErrorCode":I
    iget-object v1, p0, Lru/cn/ad/video/ui/VastPresenter;->tracker:Lru/cn/ad/video/ui/VastPresenter$Tracker;

    iget-object v2, p0, Lru/cn/ad/video/ui/VastPresenter;->context:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, Lru/cn/ad/video/ui/VastPresenter$Tracker;->trackError(Landroid/content/Context;I)V

    .line 158
    return-void
.end method

.method public onMetadata(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/player/metadata/MetadataItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 162
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lru/cn/player/metadata/MetadataItem;>;"
    return-void
.end method

.method public onSkipped()V
    .locals 1

    .prologue
    .line 281
    const-string v0, "skip"

    invoke-direct {p0, v0}, Lru/cn/ad/video/ui/VastPresenter;->trackEvent(Ljava/lang/String;)V

    .line 284
    invoke-virtual {p0}, Lru/cn/ad/video/ui/VastPresenter;->destroy()V

    .line 285
    return-void
.end method

.method public play()V
    .locals 3

    .prologue
    .line 104
    sget-object v0, Lru/cn/ad/video/ui/VastPresenter;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Play advertisement "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lru/cn/ad/video/ui/VastPresenter;->contentUri:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lru/cn/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lru/cn/ad/video/ui/VastPresenter;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0, p0}, Lru/cn/player/SimplePlayer;->addListener(Lru/cn/player/SimplePlayer$Listener;)V

    .line 107
    iget-object v0, p0, Lru/cn/ad/video/ui/VastPresenter;->player:Lru/cn/player/SimplePlayer;

    iget-object v1, p0, Lru/cn/ad/video/ui/VastPresenter;->contentUri:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lru/cn/player/SimplePlayer;->play(Ljava/lang/String;)V

    .line 108
    return-void
.end method

.method public qualityChanged(I)V
    .locals 0
    .param p1, "bitrate"    # I

    .prologue
    .line 208
    return-void
.end method

.method public setAdClickUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "clickUri"    # Ljava/lang/String;

    .prologue
    .line 100
    iput-object p1, p0, Lru/cn/ad/video/ui/VastPresenter;->adClickUri:Ljava/lang/String;

    .line 101
    return-void
.end method

.method public setSkipOffset(Lru/cn/ad/video/VAST/parser/SkipOffset;)V
    .locals 0
    .param p1, "skipOffset"    # Lru/cn/ad/video/VAST/parser/SkipOffset;

    .prologue
    .line 96
    iput-object p1, p0, Lru/cn/ad/video/ui/VastPresenter;->skipOffset:Lru/cn/ad/video/VAST/parser/SkipOffset;

    .line 97
    return-void
.end method

.method public startBuffering()V
    .locals 0

    .prologue
    .line 198
    return-void
.end method

.method public stateChanged(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V
    .locals 4
    .param p1, "state"    # Lru/cn/player/AbstractMediaPlayer$PlayerState;

    .prologue
    const/4 v3, 0x1

    .line 119
    sget-object v0, Lru/cn/ad/video/ui/VastPresenter;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "state changed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    sget-object v0, Lru/cn/ad/video/ui/VastPresenter$1;->$SwitchMap$ru$cn$player$AbstractMediaPlayer$PlayerState:[I

    invoke-virtual {p1}, Lru/cn/player/AbstractMediaPlayer$PlayerState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 122
    :pswitch_0
    iget-boolean v0, p0, Lru/cn/ad/video/ui/VastPresenter;->adReady:Z

    if-nez v0, :cond_0

    .line 123
    iput-boolean v3, p0, Lru/cn/ad/video/ui/VastPresenter;->adReady:Z

    .line 124
    invoke-direct {p0}, Lru/cn/ad/video/ui/VastPresenter;->onReady()V

    goto :goto_0

    .line 130
    :pswitch_1
    iget-boolean v0, p0, Lru/cn/ad/video/ui/VastPresenter;->adReady:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lru/cn/ad/video/ui/VastPresenter;->adStarted:Z

    if-nez v0, :cond_1

    .line 131
    iput-boolean v3, p0, Lru/cn/ad/video/ui/VastPresenter;->adStarted:Z

    .line 132
    invoke-direct {p0}, Lru/cn/ad/video/ui/VastPresenter;->onStarted()V

    .line 134
    :cond_1
    iget-object v0, p0, Lru/cn/ad/video/ui/VastPresenter;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 138
    :pswitch_2
    iget-object v0, p0, Lru/cn/ad/video/ui/VastPresenter;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 142
    :pswitch_3
    iget-object v0, p0, Lru/cn/ad/video/ui/VastPresenter;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 144
    invoke-direct {p0}, Lru/cn/ad/video/ui/VastPresenter;->onStopped()V

    goto :goto_0

    .line 120
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public videoSizeChanged(II)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 193
    return-void
.end method
