.class public Lcom/yandex/metrica/impl/bh$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/impl/bh;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/bh$a$a;,
        Lcom/yandex/metrica/impl/bh$a$b;,
        Lcom/yandex/metrica/impl/bh$a$c;
    }
.end annotation


# instance fields
.field private a:Lcom/yandex/metrica/impl/bh$a$c;

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Z

.field private i:Ljava/lang/String;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Lcom/yandex/metrica/impl/ob/fw;

.field private s:Lcom/yandex/metrica/impl/ob/fv;

.field private t:Lcom/yandex/metrica/impl/bh$a$b;

.field private u:Lcom/yandex/metrica/impl/bh$a$a;

.field private v:Z

.field private w:Z

.field private x:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    new-instance v0, Lcom/yandex/metrica/impl/ob/fw;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/ob/fw;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/bh$a;->r:Lcom/yandex/metrica/impl/ob/fw;

    .line 137
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/yandex/metrica/impl/bh$a;->s:Lcom/yandex/metrica/impl/ob/fv;

    return-void
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/bh$a;Lcom/yandex/metrica/impl/bh$a$a;)Lcom/yandex/metrica/impl/bh$a$a;
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/yandex/metrica/impl/bh$a;->u:Lcom/yandex/metrica/impl/bh$a$a;

    return-object p1
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/bh$a;Lcom/yandex/metrica/impl/bh$a$b;)Lcom/yandex/metrica/impl/bh$a$b;
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/yandex/metrica/impl/bh$a;->t:Lcom/yandex/metrica/impl/bh$a$b;

    return-object p1
.end method


# virtual methods
.method a(Lcom/yandex/metrica/impl/bh$a$c;)V
    .locals 0

    .prologue
    .line 316
    iput-object p1, p0, Lcom/yandex/metrica/impl/bh$a;->a:Lcom/yandex/metrica/impl/bh$a$c;

    .line 317
    return-void
.end method

.method a(Lcom/yandex/metrica/impl/ob/fv;)V
    .locals 0

    .prologue
    .line 348
    iput-object p1, p0, Lcom/yandex/metrica/impl/bh$a;->s:Lcom/yandex/metrica/impl/ob/fv;

    .line 349
    return-void
.end method

.method public a(Lcom/yandex/metrica/impl/ob/fw;)V
    .locals 0

    .prologue
    .line 324
    iput-object p1, p0, Lcom/yandex/metrica/impl/bh$a;->r:Lcom/yandex/metrica/impl/ob/fw;

    .line 325
    return-void
.end method

.method public a(Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 380
    iput-object p1, p0, Lcom/yandex/metrica/impl/bh$a;->x:Ljava/lang/Long;

    .line 381
    return-void
.end method

.method a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 252
    iput-object p1, p0, Lcom/yandex/metrica/impl/bh$a;->i:Ljava/lang/String;

    .line 253
    return-void
.end method

.method a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 244
    iput-object p1, p0, Lcom/yandex/metrica/impl/bh$a;->g:Ljava/util/List;

    .line 245
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 204
    iput-boolean p1, p0, Lcom/yandex/metrica/impl/bh$a;->h:Z

    .line 205
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 208
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/bh$a;->h:Z

    return v0
.end method

.method b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 268
    iput-object p1, p0, Lcom/yandex/metrica/impl/bh$a;->k:Ljava/lang/String;

    .line 269
    return-void
.end method

.method b(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 260
    iput-object p1, p0, Lcom/yandex/metrica/impl/bh$a;->j:Ljava/util/List;

    .line 261
    return-void
.end method

.method b(Z)V
    .locals 0

    .prologue
    .line 212
    iput-boolean p1, p0, Lcom/yandex/metrica/impl/bh$a;->b:Z

    .line 213
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 216
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/bh$a;->b:Z

    return v0
.end method

.method c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 276
    iput-object p1, p0, Lcom/yandex/metrica/impl/bh$a;->m:Ljava/lang/String;

    .line 277
    return-void
.end method

.method public c(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 284
    iput-object p1, p0, Lcom/yandex/metrica/impl/bh$a;->n:Ljava/util/List;

    .line 285
    return-void
.end method

.method c(Z)V
    .locals 0

    .prologue
    .line 220
    iput-boolean p1, p0, Lcom/yandex/metrica/impl/bh$a;->c:Z

    .line 221
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 224
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/bh$a;->c:Z

    return v0
.end method

.method d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 292
    iput-object p1, p0, Lcom/yandex/metrica/impl/bh$a;->o:Ljava/lang/String;

    .line 293
    return-void
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 232
    iput-boolean p1, p0, Lcom/yandex/metrica/impl/bh$a;->e:Z

    .line 233
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 228
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/bh$a;->e:Z

    return v0
.end method

.method e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 300
    iput-object p1, p0, Lcom/yandex/metrica/impl/bh$a;->p:Ljava/lang/String;

    .line 301
    return-void
.end method

.method public e(Z)V
    .locals 0

    .prologue
    .line 240
    iput-boolean p1, p0, Lcom/yandex/metrica/impl/bh$a;->f:Z

    .line 241
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 236
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/bh$a;->f:Z

    return v0
.end method

.method f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 248
    iget-object v0, p0, Lcom/yandex/metrica/impl/bh$a;->g:Ljava/util/List;

    return-object v0
.end method

.method f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 308
    iput-object p1, p0, Lcom/yandex/metrica/impl/bh$a;->q:Ljava/lang/String;

    .line 309
    return-void
.end method

.method public f(Z)V
    .locals 0

    .prologue
    .line 344
    iput-boolean p1, p0, Lcom/yandex/metrica/impl/bh$a;->d:Z

    .line 345
    return-void
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/yandex/metrica/impl/bh$a;->i:Ljava/lang/String;

    return-object v0
.end method

.method public g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 336
    iput-object p1, p0, Lcom/yandex/metrica/impl/bh$a;->l:Ljava/lang/String;

    .line 337
    return-void
.end method

.method public g(Z)V
    .locals 0

    .prologue
    .line 364
    iput-boolean p1, p0, Lcom/yandex/metrica/impl/bh$a;->v:Z

    .line 365
    return-void
.end method

.method public h()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 264
    iget-object v0, p0, Lcom/yandex/metrica/impl/bh$a;->j:Ljava/util/List;

    return-object v0
.end method

.method public h(Z)V
    .locals 0

    .prologue
    .line 372
    iput-boolean p1, p0, Lcom/yandex/metrica/impl/bh$a;->w:Z

    .line 373
    return-void
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/yandex/metrica/impl/bh$a;->k:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/yandex/metrica/impl/bh$a;->m:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 288
    iget-object v0, p0, Lcom/yandex/metrica/impl/bh$a;->n:Ljava/util/List;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/yandex/metrica/impl/bh$a;->o:Ljava/lang/String;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/yandex/metrica/impl/bh$a;->p:Ljava/lang/String;

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/yandex/metrica/impl/bh$a;->q:Ljava/lang/String;

    return-object v0
.end method

.method public o()Lcom/yandex/metrica/impl/bh$a$c;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/yandex/metrica/impl/bh$a;->a:Lcom/yandex/metrica/impl/bh$a$c;

    return-object v0
.end method

.method public p()Lcom/yandex/metrica/impl/ob/fw;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/yandex/metrica/impl/bh$a;->r:Lcom/yandex/metrica/impl/ob/fw;

    return-object v0
.end method

.method public q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/yandex/metrica/impl/bh$a;->l:Ljava/lang/String;

    return-object v0
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 340
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/bh$a;->d:Z

    return v0
.end method

.method public s()Lcom/yandex/metrica/impl/ob/fv;
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/yandex/metrica/impl/bh$a;->s:Lcom/yandex/metrica/impl/ob/fv;

    return-object v0
.end method

.method public t()Lcom/yandex/metrica/impl/bh$a$b;
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/yandex/metrica/impl/bh$a;->t:Lcom/yandex/metrica/impl/bh$a$b;

    return-object v0
.end method

.method public u()Lcom/yandex/metrica/impl/bh$a$a;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/yandex/metrica/impl/bh$a;->u:Lcom/yandex/metrica/impl/bh$a$a;

    return-object v0
.end method

.method public v()Z
    .locals 1

    .prologue
    .line 368
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/bh$a;->v:Z

    return v0
.end method

.method public w()Z
    .locals 1

    .prologue
    .line 376
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/bh$a;->w:Z

    return v0
.end method

.method public x()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/yandex/metrica/impl/bh$a;->x:Ljava/lang/Long;

    return-object v0
.end method
