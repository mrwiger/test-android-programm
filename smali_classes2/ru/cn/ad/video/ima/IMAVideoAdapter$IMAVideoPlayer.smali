.class Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;
.super Ljava/lang/Object;
.source "IMAVideoAdapter.java"

# interfaces
.implements Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;
.implements Lru/cn/player/SimplePlayer$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/ad/video/ima/IMAVideoAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IMAVideoPlayer"
.end annotation


# instance fields
.field private final callbacks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;",
            ">;"
        }
    .end annotation
.end field

.field private loader:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/util/List",
            "<",
            "Lru/cn/player/youtube/YoutubeVideo;",
            ">;>;"
        }
    .end annotation
.end field

.field private player:Lru/cn/player/SimplePlayer;

.field private source:Ljava/lang/String;

.field final synthetic this$0:Lru/cn/ad/video/ima/IMAVideoAdapter;


# direct methods
.method constructor <init>(Lru/cn/ad/video/ima/IMAVideoAdapter;)V
    .locals 1

    .prologue
    .line 286
    iput-object p1, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->this$0:Lru/cn/ad/video/ima/IMAVideoAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 281
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->callbacks:Ljava/util/List;

    .line 286
    return-void
.end method

.method static synthetic access$000(Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;

    .prologue
    .line 279
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->source:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;)Lru/cn/player/SimplePlayer;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;

    .prologue
    .line 279
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;

    .prologue
    .line 279
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->callbacks:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public addCallback(Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;)V
    .locals 1
    .param p1, "videoAdPlayerCallback"    # Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;

    .prologue
    .line 375
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->callbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 376
    return-void
.end method

.method public attachPlayer(Lru/cn/player/SimplePlayer;)V
    .locals 0
    .param p1, "player"    # Lru/cn/player/SimplePlayer;

    .prologue
    .line 289
    iput-object p1, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    .line 290
    invoke-virtual {p1, p0}, Lru/cn/player/SimplePlayer;->addListener(Lru/cn/player/SimplePlayer$Listener;)V

    .line 291
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    if-eqz v0, :cond_0

    .line 366
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0, p0}, Lru/cn/player/SimplePlayer;->removeListener(Lru/cn/player/SimplePlayer$Listener;)V

    .line 368
    :cond_0
    invoke-virtual {p0}, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->stopAd()V

    .line 370
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    .line 371
    return-void
.end method

.method public endBuffering()V
    .locals 0

    .prologue
    .line 449
    return-void
.end method

.method public getAdProgress()Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;
    .locals 6

    .prologue
    .line 385
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    .line 386
    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getState()Lru/cn/player/AbstractMediaPlayer$PlayerState;

    move-result-object v0

    sget-object v1, Lru/cn/player/AbstractMediaPlayer$PlayerState;->STOPPED:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    .line 387
    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getDuration()I

    move-result v0

    if-gtz v0, :cond_1

    .line 388
    :cond_0
    sget-object v0, Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;->VIDEO_TIME_NOT_READY:Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;

    .line 391
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;

    iget-object v1, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v1}, Lru/cn/player/SimplePlayer;->getCurrentPosition()I

    move-result v1

    int-to-long v2, v1

    iget-object v1, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v1}, Lru/cn/player/SimplePlayer;->getDuration()I

    move-result v1

    int-to-long v4, v1

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;-><init>(JJ)V

    goto :goto_0
.end method

.method public loadAd(Ljava/lang/String;)V
    .locals 0
    .param p1, "adUri"    # Ljava/lang/String;

    .prologue
    .line 338
    iput-object p1, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->source:Ljava/lang/String;

    .line 339
    return-void
.end method

.method public onComplete()V
    .locals 3

    .prologue
    .line 421
    iget-object v1, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->callbacks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;

    .line 422
    .local v0, "callback":Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;
    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;->onEnded()V

    goto :goto_0

    .line 424
    .end local v0    # "callback":Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;
    :cond_0
    return-void
.end method

.method public onError(II)V
    .locals 3
    .param p1, "playerType"    # I
    .param p2, "code"    # I

    .prologue
    .line 428
    iget-object v1, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->this$0:Lru/cn/ad/video/ima/IMAVideoAdapter;

    sget-object v2, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;->VIDEO_PLAY_ERROR:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;

    invoke-static {v1, v2}, Lru/cn/ad/video/ima/IMAVideoAdapter;->access$200(Lru/cn/ad/video/ima/IMAVideoAdapter;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;)V

    .line 430
    iget-object v1, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->callbacks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;

    .line 431
    .local v0, "callback":Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;
    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;->onError()V

    goto :goto_0

    .line 433
    .end local v0    # "callback":Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;
    :cond_0
    return-void
.end method

.method public onMetadata(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/player/metadata/MetadataItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 437
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lru/cn/player/metadata/MetadataItem;>;"
    return-void
.end method

.method public pauseAd()V
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->pause()V

    .line 356
    :cond_0
    return-void
.end method

.method public playAd()V
    .locals 3

    .prologue
    .line 295
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->source:Ljava/lang/String;

    invoke-static {v0}, Lru/cn/player/youtube/YoutubeParser;->isYoutubeURL(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297
    new-instance v0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer$1;

    invoke-direct {v0, p0}, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer$1;-><init>(Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 326
    invoke-virtual {v0, v1}, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->loader:Landroid/os/AsyncTask;

    .line 334
    :cond_0
    :goto_0
    return-void

    .line 330
    :cond_1
    const-string v0, "IMAVideoAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Play advertisement "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->source:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lru/cn/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    iget-object v1, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->source:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lru/cn/player/SimplePlayer;->play(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public qualityChanged(I)V
    .locals 0
    .param p1, "bitrate"    # I

    .prologue
    .line 453
    return-void
.end method

.method public removeCallback(Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;)V
    .locals 1
    .param p1, "videoAdPlayerCallback"    # Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;

    .prologue
    .line 380
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->callbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 381
    return-void
.end method

.method public resumeAd()V
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->resume()V

    .line 362
    :cond_0
    return-void
.end method

.method public startBuffering()V
    .locals 0

    .prologue
    .line 445
    return-void
.end method

.method public stateChanged(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V
    .locals 3
    .param p1, "state"    # Lru/cn/player/AbstractMediaPlayer$PlayerState;

    .prologue
    .line 396
    sget-object v1, Lru/cn/ad/video/ima/IMAVideoAdapter$1;->$SwitchMap$ru$cn$player$AbstractMediaPlayer$PlayerState:[I

    invoke-virtual {p1}, Lru/cn/player/AbstractMediaPlayer$PlayerState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 417
    :cond_0
    :goto_0
    return-void

    .line 398
    :pswitch_0
    iget-object v1, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->callbacks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;

    .line 399
    .local v0, "callback":Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;
    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;->onResume()V

    goto :goto_1

    .line 403
    .end local v0    # "callback":Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;
    :pswitch_1
    iget-object v1, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->callbacks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;

    .line 404
    .restart local v0    # "callback":Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;
    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;->onPause()V

    goto :goto_2

    .line 408
    .end local v0    # "callback":Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;
    :pswitch_2
    iget-object v1, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->callbacks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;

    .line 409
    .restart local v0    # "callback":Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;
    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;->onPlay()V

    goto :goto_3

    .line 414
    .end local v0    # "callback":Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;
    :pswitch_3
    iget-object v1, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->this$0:Lru/cn/ad/video/ima/IMAVideoAdapter;

    invoke-virtual {v1}, Lru/cn/ad/video/ima/IMAVideoAdapter;->destroy()V

    goto :goto_0

    .line 396
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public stopAd()V
    .locals 2

    .prologue
    .line 343
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->stop()V

    .line 346
    :cond_0
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->loader:Landroid/os/AsyncTask;

    if-eqz v0, :cond_1

    .line 347
    iget-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->loader:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 348
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/ad/video/ima/IMAVideoAdapter$IMAVideoPlayer;->loader:Landroid/os/AsyncTask;

    .line 350
    :cond_1
    return-void
.end method

.method public videoSizeChanged(II)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 441
    return-void
.end method
