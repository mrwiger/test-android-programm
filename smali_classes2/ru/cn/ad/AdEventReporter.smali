.class public Lru/cn/ad/AdEventReporter;
.super Ljava/lang/Object;
.source "AdEventReporter.java"


# instance fields
.field private final placeId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "placeId"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lru/cn/ad/AdEventReporter;->placeId:Ljava/lang/String;

    .line 21
    return-void
.end method

.method static final synthetic lambda$reportError$0$AdEventReporter(Ljava/util/Map$Entry;)Ljava/lang/String;
    .locals 2
    .param p0, "entry"    # Ljava/util/Map$Entry;

    .prologue
    .line 42
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final reportError(Lru/cn/domain/statistics/inetra/ErrorCode;Lru/cn/api/money_miner/replies/AdSystem;Ljava/lang/String;ILjava/util/Map;)V
    .locals 4
    .param p1, "code"    # Lru/cn/domain/statistics/inetra/ErrorCode;
    .param p2, "adSystem"    # Lru/cn/api/money_miner/replies/AdSystem;
    .param p3, "domain"    # Ljava/lang/String;
    .param p4, "errorCode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/domain/statistics/inetra/ErrorCode;",
            "Lru/cn/api/money_miner/replies/AdSystem;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p5, "extra":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 35
    .local v1, "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "place_id"

    iget-object v3, p0, Lru/cn/ad/AdEventReporter;->placeId:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    const-string v2, "network_id"

    iget v3, p2, Lru/cn/api/money_miner/replies/AdSystem;->id:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    if-eqz p5, :cond_0

    .line 38
    invoke-interface {v1, p5}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 41
    :cond_0
    invoke-static {v1}, Lcom/annimon/stream/Stream;->of(Ljava/util/Map;)Lcom/annimon/stream/Stream;

    move-result-object v2

    sget-object v3, Lru/cn/ad/AdEventReporter$$Lambda$0;->$instance:Lcom/annimon/stream/function/Function;

    .line 42
    invoke-virtual {v2, v3}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v2

    const-string v3, ";"

    .line 43
    invoke-static {v3}, Lcom/annimon/stream/Collectors;->joining(Ljava/lang/CharSequence;)Lcom/annimon/stream/Collector;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/annimon/stream/Stream;->collect(Lcom/annimon/stream/Collector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 45
    .local v0, "combinedMessage":Ljava/lang/String;
    invoke-static {p1, p3, p4, v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    .line 46
    return-void
.end method

.method public final reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Lru/cn/api/money_miner/replies/AdSystem;Ljava/util/Map;)V
    .locals 1
    .param p1, "eventId"    # Lru/cn/domain/statistics/inetra/AdvEvent;
    .param p2, "adSystem"    # Lru/cn/api/money_miner/replies/AdSystem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/domain/statistics/inetra/AdvEvent;",
            "Lru/cn/api/money_miner/replies/AdSystem;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p3, "extra":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lru/cn/ad/AdEventReporter;->placeId:Ljava/lang/String;

    invoke-static {p1, v0, p2, p3}, Lru/cn/domain/statistics/inetra/InetraTracker;->advEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/lang/String;Lru/cn/api/money_miner/replies/AdSystem;Ljava/util/Map;)V

    .line 28
    return-void
.end method
