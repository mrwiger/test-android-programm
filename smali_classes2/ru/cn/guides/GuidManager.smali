.class public Lru/cn/guides/GuidManager;
.super Ljava/lang/Object;
.source "GuidManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/guides/GuidManager$GuidGroup;,
        Lru/cn/guides/GuidManager$GuidItem;,
        Lru/cn/guides/GuidManager$GuidManagerListener;
    }
.end annotation


# static fields
.field static groups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/guides/GuidManager$GuidGroup;",
            ">;"
        }
    .end annotation
.end field

.field private static window:Landroid/widget/PopupWindow;


# instance fields
.field private c:Landroid/content/Context;

.field private listener:Lru/cn/guides/GuidManager$GuidManagerListener;

.field private windowView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    sput-object v0, Lru/cn/guides/GuidManager;->window:Landroid/widget/PopupWindow;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lru/cn/guides/GuidManager;->groups:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lru/cn/guides/GuidManager;->window:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$002(Landroid/widget/PopupWindow;)Landroid/widget/PopupWindow;
    .locals 0
    .param p0, "x0"    # Landroid/widget/PopupWindow;

    .prologue
    .line 16
    sput-object p0, Lru/cn/guides/GuidManager;->window:Landroid/widget/PopupWindow;

    return-object p0
.end method

.method static synthetic access$100(Lru/cn/guides/GuidManager;)Lru/cn/guides/GuidManager$GuidManagerListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/guides/GuidManager;

    .prologue
    .line 16
    iget-object v0, p0, Lru/cn/guides/GuidManager;->listener:Lru/cn/guides/GuidManager$GuidManagerListener;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/guides/GuidManager;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lru/cn/guides/GuidManager;

    .prologue
    .line 16
    iget-object v0, p0, Lru/cn/guides/GuidManager;->windowView:Landroid/view/View;

    return-object v0
.end method

.method static closeGuide()V
    .locals 1

    .prologue
    .line 133
    sget-object v0, Lru/cn/guides/GuidManager;->window:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 134
    sget-object v0, Lru/cn/guides/GuidManager;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 136
    :cond_0
    return-void
.end method

.method private loadGroups()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 96
    sget-object v3, Lru/cn/guides/GuidManager;->groups:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 98
    iget-object v3, p0, Lru/cn/guides/GuidManager;->c:Landroid/content/Context;

    const-string v4, "GuidManager_groups"

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 102
    .local v1, "preferences":Landroid/content/SharedPreferences;
    const-string v3, "0"

    invoke-interface {v1, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 103
    .local v2, "watched":Z
    if-nez v2, :cond_0

    .line 105
    new-instance v0, Lru/cn/guides/GuidManager$GuidGroup;

    invoke-direct {v0}, Lru/cn/guides/GuidManager$GuidGroup;-><init>()V

    .line 106
    .local v0, "g":Lru/cn/guides/GuidManager$GuidGroup;
    iput v5, v0, Lru/cn/guides/GuidManager$GuidGroup;->version:I

    .line 107
    sget v3, Lru/cn/guides/R$string;->ug_air_title:I

    sget v4, Lru/cn/guides/R$string;->ug_air_description:I

    sget v5, Lru/cn/guides/R$drawable;->ug_air:I

    invoke-virtual {v0, v3, v4, v5}, Lru/cn/guides/GuidManager$GuidGroup;->addItem(III)V

    .line 109
    sget v3, Lru/cn/guides/R$string;->ug_timeshift_title:I

    sget v4, Lru/cn/guides/R$string;->ug_timeshift_description:I

    sget v5, Lru/cn/guides/R$drawable;->ug_timeshift:I

    invoke-virtual {v0, v3, v4, v5}, Lru/cn/guides/GuidManager$GuidGroup;->addItem(III)V

    .line 112
    sget v3, Lru/cn/guides/R$string;->ug_news_title:I

    sget v4, Lru/cn/guides/R$string;->ug_news_description:I

    sget v5, Lru/cn/guides/R$drawable;->ug_news:I

    invoke-virtual {v0, v3, v4, v5}, Lru/cn/guides/GuidManager$GuidGroup;->addItem(III)V

    .line 113
    sget v3, Lru/cn/guides/R$string;->ug_top_title:I

    sget v4, Lru/cn/guides/R$string;->ug_top_description:I

    sget v5, Lru/cn/guides/R$drawable;->ug_top:I

    invoke-virtual {v0, v3, v4, v5}, Lru/cn/guides/GuidManager$GuidGroup;->addItem(III)V

    .line 116
    sget-object v3, Lru/cn/guides/GuidManager;->groups:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    .end local v0    # "g":Lru/cn/guides/GuidManager$GuidGroup;
    :cond_0
    return-void
.end method


# virtual methods
.method public Init(Landroid/content/Context;Landroid/view/ViewGroup;Lru/cn/guides/GuidManager$GuidManagerListener;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "listener"    # Lru/cn/guides/GuidManager$GuidManagerListener;

    .prologue
    const/4 v3, -0x1

    .line 54
    iput-object p1, p0, Lru/cn/guides/GuidManager;->c:Landroid/content/Context;

    .line 55
    iput-object p3, p0, Lru/cn/guides/GuidManager;->listener:Lru/cn/guides/GuidManager$GuidManagerListener;

    .line 56
    invoke-direct {p0}, Lru/cn/guides/GuidManager;->loadGroups()V

    .line 58
    sget-object v0, Lru/cn/guides/GuidManager;->groups:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 59
    const-string v0, "layout_inflater"

    .line 60
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 62
    .local v6, "inflater":Landroid/view/LayoutInflater;
    sget v0, Lru/cn/guides/R$layout;->guides_window:I

    const/4 v1, 0x0

    invoke-virtual {v6, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/guides/GuidManager;->windowView:Landroid/view/View;

    .line 64
    new-instance v0, Lru/cn/guides/GuidManager$1;

    iget-object v2, p0, Lru/cn/guides/GuidManager;->windowView:Landroid/view/View;

    const/4 v5, 0x1

    move-object v1, p0

    move v4, v3

    invoke-direct/range {v0 .. v5}, Lru/cn/guides/GuidManager$1;-><init>(Lru/cn/guides/GuidManager;Landroid/view/View;IIZ)V

    sput-object v0, Lru/cn/guides/GuidManager;->window:Landroid/widget/PopupWindow;

    .line 68
    sget-object v0, Lru/cn/guides/GuidManager;->window:Landroid/widget/PopupWindow;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 69
    sget-object v0, Lru/cn/guides/GuidManager;->window:Landroid/widget/PopupWindow;

    new-instance v1, Lru/cn/guides/GuidManager$2;

    invoke-direct {v1, p0}, Lru/cn/guides/GuidManager$2;-><init>(Lru/cn/guides/GuidManager;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 81
    new-instance v0, Lru/cn/guides/GuidManager$3;

    invoke-direct {v0, p0}, Lru/cn/guides/GuidManager$3;-><init>(Lru/cn/guides/GuidManager;)V

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    .line 92
    .end local v6    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    if-eqz p3, :cond_0

    .line 89
    const/4 v0, 0x0

    invoke-interface {p3, v0}, Lru/cn/guides/GuidManager$GuidManagerListener;->onClosed(Z)V

    goto :goto_0
.end method

.method saveGroups()V
    .locals 6

    .prologue
    .line 122
    iget-object v3, p0, Lru/cn/guides/GuidManager;->c:Landroid/content/Context;

    const-string v4, "GuidManager_groups"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 124
    .local v2, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 125
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    sget-object v3, Lru/cn/guides/GuidManager;->groups:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/guides/GuidManager$GuidGroup;

    .line 126
    .local v1, "group":Lru/cn/guides/GuidManager$GuidGroup;
    iget v4, v1, Lru/cn/guides/GuidManager$GuidGroup;->version:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 128
    .end local v1    # "group":Lru/cn/guides/GuidManager$GuidGroup;
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 129
    sget-object v3, Lru/cn/guides/GuidManager;->groups:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 130
    return-void
.end method
