.class public Lcom/google/obf/ah;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/ar;


# instance fields
.field private final a:Lcom/google/obf/ap;

.field private final b:Lcom/google/obf/t;

.field private c:Z

.field private d:J

.field private e:J

.field private volatile f:J

.field private volatile g:Lcom/google/obf/q;


# direct methods
.method public constructor <init>(Lcom/google/obf/cx;)V
    .locals 4

    .prologue
    const-wide/high16 v2, -0x8000000000000000L

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/google/obf/ap;

    invoke-direct {v0, p1}, Lcom/google/obf/ap;-><init>(Lcom/google/obf/cx;)V

    iput-object v0, p0, Lcom/google/obf/ah;->a:Lcom/google/obf/ap;

    .line 3
    new-instance v0, Lcom/google/obf/t;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/obf/t;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/ah;->b:Lcom/google/obf/t;

    .line 4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/ah;->c:Z

    .line 5
    iput-wide v2, p0, Lcom/google/obf/ah;->d:J

    .line 6
    iput-wide v2, p0, Lcom/google/obf/ah;->e:J

    .line 7
    iput-wide v2, p0, Lcom/google/obf/ah;->f:J

    .line 8
    return-void
.end method

.method private f()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 32
    iget-object v1, p0, Lcom/google/obf/ah;->a:Lcom/google/obf/ap;

    iget-object v2, p0, Lcom/google/obf/ah;->b:Lcom/google/obf/t;

    invoke-virtual {v1, v2}, Lcom/google/obf/ap;->a(Lcom/google/obf/t;)Z

    move-result v1

    .line 33
    iget-boolean v2, p0, Lcom/google/obf/ah;->c:Z

    if-eqz v2, :cond_0

    .line 34
    :goto_0
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/obf/ah;->b:Lcom/google/obf/t;

    invoke-virtual {v2}, Lcom/google/obf/t;->c()Z

    move-result v2

    if-nez v2, :cond_0

    .line 35
    iget-object v1, p0, Lcom/google/obf/ah;->a:Lcom/google/obf/ap;

    invoke-virtual {v1}, Lcom/google/obf/ap;->b()V

    .line 36
    iget-object v1, p0, Lcom/google/obf/ah;->a:Lcom/google/obf/ap;

    iget-object v2, p0, Lcom/google/obf/ah;->b:Lcom/google/obf/t;

    invoke-virtual {v1, v2}, Lcom/google/obf/ap;->a(Lcom/google/obf/t;)Z

    move-result v1

    goto :goto_0

    .line 37
    :cond_0
    if-nez v1, :cond_2

    .line 41
    :cond_1
    :goto_1
    return v0

    .line 39
    :cond_2
    iget-wide v2, p0, Lcom/google/obf/ah;->e:J

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/obf/ah;->b:Lcom/google/obf/t;

    iget-wide v2, v1, Lcom/google/obf/t;->e:J

    iget-wide v4, p0, Lcom/google/obf/ah;->e:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 41
    :cond_3
    const/4 v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public a(Lcom/google/obf/ak;IZ)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/obf/ah;->a:Lcom/google/obf/ap;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/obf/ap;->a(Lcom/google/obf/ak;IZ)I

    move-result v0

    return v0
.end method

.method public a()V
    .locals 4

    .prologue
    const-wide/high16 v2, -0x8000000000000000L

    .line 9
    iget-object v0, p0, Lcom/google/obf/ah;->a:Lcom/google/obf/ap;

    invoke-virtual {v0}, Lcom/google/obf/ap;->a()V

    .line 10
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/ah;->c:Z

    .line 11
    iput-wide v2, p0, Lcom/google/obf/ah;->d:J

    .line 12
    iput-wide v2, p0, Lcom/google/obf/ah;->e:J

    .line 13
    iput-wide v2, p0, Lcom/google/obf/ah;->f:J

    .line 14
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 26
    :goto_0
    iget-object v0, p0, Lcom/google/obf/ah;->a:Lcom/google/obf/ap;

    iget-object v1, p0, Lcom/google/obf/ah;->b:Lcom/google/obf/t;

    invoke-virtual {v0, v1}, Lcom/google/obf/ap;->a(Lcom/google/obf/t;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/ah;->b:Lcom/google/obf/t;

    iget-wide v0, v0, Lcom/google/obf/t;->e:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_0

    .line 27
    iget-object v0, p0, Lcom/google/obf/ah;->a:Lcom/google/obf/ap;

    invoke-virtual {v0}, Lcom/google/obf/ap;->b()V

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/ah;->c:Z

    goto :goto_0

    .line 29
    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/obf/ah;->d:J

    .line 30
    return-void
.end method

.method public a(JIII[B)V
    .locals 9

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/google/obf/ah;->f:J

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/obf/ah;->f:J

    .line 48
    iget-object v0, p0, Lcom/google/obf/ah;->a:Lcom/google/obf/ap;

    iget-object v1, p0, Lcom/google/obf/ah;->a:Lcom/google/obf/ap;

    invoke-virtual {v1}, Lcom/google/obf/ap;->c()J

    move-result-wide v2

    int-to-long v4, p4

    sub-long/2addr v2, v4

    int-to-long v4, p5

    sub-long v4, v2, v4

    move-wide v1, p1

    move v3, p3

    move v6, p4

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/google/obf/ap;->a(JIJI[B)V

    .line 49
    return-void
.end method

.method public a(Lcom/google/obf/dw;I)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/obf/ah;->a:Lcom/google/obf/ap;

    invoke-virtual {v0, p1, p2}, Lcom/google/obf/ap;->a(Lcom/google/obf/dw;I)V

    .line 46
    return-void
.end method

.method public a(Lcom/google/obf/q;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/obf/ah;->g:Lcom/google/obf/q;

    .line 43
    return-void
.end method

.method public a(Lcom/google/obf/t;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Lcom/google/obf/ah;->f()Z

    move-result v1

    .line 20
    if-nez v1, :cond_0

    .line 25
    :goto_0
    return v0

    .line 22
    :cond_0
    iget-object v1, p0, Lcom/google/obf/ah;->a:Lcom/google/obf/ap;

    invoke-virtual {v1, p1}, Lcom/google/obf/ap;->b(Lcom/google/obf/t;)Z

    .line 23
    iput-boolean v0, p0, Lcom/google/obf/ah;->c:Z

    .line 24
    iget-wide v0, p1, Lcom/google/obf/t;->e:J

    iput-wide v0, p0, Lcom/google/obf/ah;->d:J

    .line 25
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/google/obf/ah;->g:Lcom/google/obf/q;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(J)Z
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/obf/ah;->a:Lcom/google/obf/ap;

    invoke-virtual {v0, p1, p2}, Lcom/google/obf/ap;->a(J)Z

    move-result v0

    return v0
.end method

.method public c()Lcom/google/obf/q;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/obf/ah;->g:Lcom/google/obf/q;

    return-object v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 17
    iget-wide v0, p0, Lcom/google/obf/ah;->f:J

    return-wide v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/obf/ah;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
