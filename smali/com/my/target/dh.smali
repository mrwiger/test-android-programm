.class public final Lcom/my/target/dh;
.super Lcom/my/target/ak;
.source "StandardAdSection.java"


# instance fields
.field private final D:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/core/models/banners/c;",
            ">;"
        }
    .end annotation
.end field

.field private final E:Lcom/my/target/dg;

.field private F:I

.field private G:Z

.field private H:Z

.field private I:I

.field private J:Ljava/lang/String;

.field private K:Lorg/json/JSONObject;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 46
    invoke-direct {p0}, Lcom/my/target/ak;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/dh;->D:Ljava/util/ArrayList;

    .line 38
    const/16 v0, 0x3c

    iput v0, p0, Lcom/my/target/dh;->F:I

    .line 39
    iput-boolean v1, p0, Lcom/my/target/dh;->G:Z

    .line 40
    iput-boolean v1, p0, Lcom/my/target/dh;->H:Z

    .line 41
    iput v1, p0, Lcom/my/target/dh;->I:I

    .line 47
    new-instance v0, Lcom/my/target/dg;

    invoke-direct {v0}, Lcom/my/target/dg;-><init>()V

    iput-object v0, p0, Lcom/my/target/dh;->E:Lcom/my/target/dg;

    .line 48
    return-void
.end method

.method public static v()Lcom/my/target/dh;
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/my/target/dh;

    invoke-direct {v0}, Lcom/my/target/dh;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final A()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/my/target/dh;->I:I

    return v0
.end method

.method public final R()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/my/target/dh;->D:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final a(Lcom/my/target/core/models/banners/c;)V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/my/target/dh;->D:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    return-void
.end method

.method public final b(Lcom/my/target/core/models/banners/c;)V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/my/target/dh;->D:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 123
    return-void
.end method

.method public final g(Z)V
    .locals 0

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/my/target/dh;->G:Z

    .line 63
    return-void
.end method

.method public final getBannersCount()I
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/my/target/dh;->D:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getHtml()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/my/target/dh;->J:Ljava/lang/String;

    return-object v0
.end method

.method public final getRawData()Lorg/json/JSONObject;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/my/target/dh;->K:Lorg/json/JSONObject;

    return-object v0
.end method

.method public final h(Z)V
    .locals 0

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/my/target/dh;->H:Z

    .line 68
    return-void
.end method

.method public final i(I)V
    .locals 0

    .prologue
    .line 57
    iput p1, p0, Lcom/my/target/dh;->F:I

    .line 58
    return-void
.end method

.method public final setAnimationType(I)V
    .locals 0
    .param p1, "animationType"    # I

    .prologue
    .line 72
    iput p1, p0, Lcom/my/target/dh;->I:I

    .line 73
    return-void
.end method

.method public final setHtml(Ljava/lang/String;)V
    .locals 0
    .param p1, "html"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/my/target/dh;->J:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public final setRawData(Lorg/json/JSONObject;)V
    .locals 0
    .param p1, "rawData"    # Lorg/json/JSONObject;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/my/target/dh;->K:Lorg/json/JSONObject;

    .line 108
    return-void
.end method

.method public final w()Lcom/my/target/dg;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/my/target/dh;->E:Lcom/my/target/dg;

    return-object v0
.end method

.method public final x()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/my/target/dh;->F:I

    return v0
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/my/target/dh;->G:Z

    return v0
.end method

.method public final z()Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/my/target/dh;->H:Z

    return v0
.end method
