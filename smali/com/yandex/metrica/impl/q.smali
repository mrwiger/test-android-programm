.class public final Lcom/yandex/metrica/impl/q;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/q$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/yandex/metrica/impl/q$a;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/yandex/metrica/impl/q$a;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/yandex/metrica/impl/q$a;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/yandex/metrica/impl/q$a;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/yandex/metrica/impl/q$a;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/yandex/metrica/impl/q$a;",
            ">;"
        }
    .end annotation
.end field

.field private static final g:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/yandex/metrica/impl/q$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 112
    sget-object v0, Lcom/yandex/metrica/impl/q$a;->a:Lcom/yandex/metrica/impl/q$a;

    const/16 v1, 0x9

    new-array v1, v1, [Lcom/yandex/metrica/impl/q$a;

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->m:Lcom/yandex/metrica/impl/q$a;

    aput-object v2, v1, v4

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->j:Lcom/yandex/metrica/impl/q$a;

    aput-object v2, v1, v5

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->t:Lcom/yandex/metrica/impl/q$a;

    aput-object v2, v1, v6

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->u:Lcom/yandex/metrica/impl/q$a;

    aput-object v2, v1, v7

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->v:Lcom/yandex/metrica/impl/q$a;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, Lcom/yandex/metrica/impl/q$a;->h:Lcom/yandex/metrica/impl/q$a;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/yandex/metrica/impl/q$a;->w:Lcom/yandex/metrica/impl/q$a;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/yandex/metrica/impl/q$a;->x:Lcom/yandex/metrica/impl/q$a;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, Lcom/yandex/metrica/impl/q$a;->z:Lcom/yandex/metrica/impl/q$a;

    aput-object v3, v1, v2

    .line 113
    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;[Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/yandex/metrica/impl/q;->b:Ljava/util/EnumSet;

    .line 126
    sget-object v0, Lcom/yandex/metrica/impl/q$a;->k:Lcom/yandex/metrica/impl/q$a;

    const/16 v1, 0xd

    new-array v1, v1, [Lcom/yandex/metrica/impl/q$a;

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->l:Lcom/yandex/metrica/impl/q$a;

    aput-object v2, v1, v4

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->r:Lcom/yandex/metrica/impl/q$a;

    aput-object v2, v1, v5

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->j:Lcom/yandex/metrica/impl/q$a;

    aput-object v2, v1, v6

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->a:Lcom/yandex/metrica/impl/q$a;

    aput-object v2, v1, v7

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->b:Lcom/yandex/metrica/impl/q$a;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, Lcom/yandex/metrica/impl/q$a;->E:Lcom/yandex/metrica/impl/q$a;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/yandex/metrica/impl/q$a;->h:Lcom/yandex/metrica/impl/q$a;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/yandex/metrica/impl/q$a;->i:Lcom/yandex/metrica/impl/q$a;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, Lcom/yandex/metrica/impl/q$a;->p:Lcom/yandex/metrica/impl/q$a;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, Lcom/yandex/metrica/impl/q$a;->q:Lcom/yandex/metrica/impl/q$a;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    sget-object v3, Lcom/yandex/metrica/impl/q$a;->w:Lcom/yandex/metrica/impl/q$a;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    sget-object v3, Lcom/yandex/metrica/impl/q$a;->x:Lcom/yandex/metrica/impl/q$a;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    sget-object v3, Lcom/yandex/metrica/impl/q$a;->z:Lcom/yandex/metrica/impl/q$a;

    aput-object v3, v1, v2

    .line 127
    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;[Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/yandex/metrica/impl/q;->c:Ljava/util/EnumSet;

    .line 144
    sget-object v0, Lcom/yandex/metrica/impl/q$a;->e:Lcom/yandex/metrica/impl/q$a;

    sget-object v1, Lcom/yandex/metrica/impl/q$a;->k:Lcom/yandex/metrica/impl/q$a;

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->l:Lcom/yandex/metrica/impl/q$a;

    .line 145
    invoke-static {v0, v1, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/yandex/metrica/impl/q;->d:Ljava/util/EnumSet;

    .line 151
    sget-object v0, Lcom/yandex/metrica/impl/q$a;->b:Lcom/yandex/metrica/impl/q$a;

    const/4 v1, 0x6

    new-array v1, v1, [Lcom/yandex/metrica/impl/q$a;

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->p:Lcom/yandex/metrica/impl/q$a;

    aput-object v2, v1, v4

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->C:Lcom/yandex/metrica/impl/q$a;

    aput-object v2, v1, v5

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->A:Lcom/yandex/metrica/impl/q$a;

    aput-object v2, v1, v6

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->t:Lcom/yandex/metrica/impl/q$a;

    aput-object v2, v1, v7

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->h:Lcom/yandex/metrica/impl/q$a;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, Lcom/yandex/metrica/impl/q$a;->E:Lcom/yandex/metrica/impl/q$a;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;[Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/yandex/metrica/impl/q;->a:Ljava/util/EnumSet;

    .line 161
    sget-object v0, Lcom/yandex/metrica/impl/q$a;->q:Lcom/yandex/metrica/impl/q$a;

    const/16 v1, 0xa

    new-array v1, v1, [Lcom/yandex/metrica/impl/q$a;

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->j:Lcom/yandex/metrica/impl/q$a;

    aput-object v2, v1, v4

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->t:Lcom/yandex/metrica/impl/q$a;

    aput-object v2, v1, v5

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->h:Lcom/yandex/metrica/impl/q$a;

    aput-object v2, v1, v6

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->u:Lcom/yandex/metrica/impl/q$a;

    aput-object v2, v1, v7

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->v:Lcom/yandex/metrica/impl/q$a;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, Lcom/yandex/metrica/impl/q$a;->a:Lcom/yandex/metrica/impl/q$a;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/yandex/metrica/impl/q$a;->i:Lcom/yandex/metrica/impl/q$a;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/yandex/metrica/impl/q$a;->p:Lcom/yandex/metrica/impl/q$a;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, Lcom/yandex/metrica/impl/q$a;->w:Lcom/yandex/metrica/impl/q$a;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, Lcom/yandex/metrica/impl/q$a;->x:Lcom/yandex/metrica/impl/q$a;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;[Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/yandex/metrica/impl/q;->e:Ljava/util/EnumSet;

    .line 175
    sget-object v0, Lcom/yandex/metrica/impl/q$a;->f:Lcom/yandex/metrica/impl/q$a;

    sget-object v1, Lcom/yandex/metrica/impl/q$a;->y:Lcom/yandex/metrica/impl/q$a;

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->g:Lcom/yandex/metrica/impl/q$a;

    sget-object v3, Lcom/yandex/metrica/impl/q$a;->o:Lcom/yandex/metrica/impl/q$a;

    sget-object v4, Lcom/yandex/metrica/impl/q$a;->c:Lcom/yandex/metrica/impl/q$a;

    invoke-static {v0, v1, v2, v3, v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/yandex/metrica/impl/q;->f:Ljava/util/EnumSet;

    .line 183
    sget-object v0, Lcom/yandex/metrica/impl/q$a;->c:Lcom/yandex/metrica/impl/q$a;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/yandex/metrica/impl/q;->g:Ljava/util/EnumSet;

    return-void
.end method

.method static a()Lcom/yandex/metrica/impl/i;
    .locals 3

    .prologue
    .line 286
    new-instance v0, Lcom/yandex/metrica/impl/f;

    sget-object v1, Lcom/yandex/metrica/impl/q$a;->j:Lcom/yandex/metrica/impl/q$a;

    .line 287
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/q$a;->b()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->j:Lcom/yandex/metrica/impl/q$a;

    .line 288
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/f;-><init>(Ljava/lang/String;I)V

    .line 286
    return-object v0
.end method

.method static a(ILjava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lcom/yandex/metrica/impl/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/yandex/metrica/impl/i;"
        }
    .end annotation

    .prologue
    .line 310
    new-instance v0, Lcom/yandex/metrica/impl/f;

    sget-object v1, Lcom/yandex/metrica/impl/q$a;->C:Lcom/yandex/metrica/impl/q$a;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v1

    invoke-direct {v0, p2, p1, v1, p0}, Lcom/yandex/metrica/impl/f;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 311
    invoke-static {p3}, Lcom/yandex/metrica/impl/utils/g;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/f;->e(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    .line 310
    return-object v0
.end method

.method public static a(Lcom/yandex/metrica/impl/ao;)Lcom/yandex/metrica/impl/i;
    .locals 4

    .prologue
    .line 280
    if-nez p0, :cond_0

    const-string v0, ""

    .line 281
    :goto_0
    new-instance v1, Lcom/yandex/metrica/impl/f;

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->z:Lcom/yandex/metrica/impl/q$a;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/q$a;->b()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/yandex/metrica/impl/q$a;->z:Lcom/yandex/metrica/impl/q$a;

    .line 282
    invoke-virtual {v3}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v3

    invoke-direct {v1, v0, v2, v3}, Lcom/yandex/metrica/impl/f;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 281
    return-object v1

    .line 280
    :cond_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ao;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static a(Lcom/yandex/metrica/impl/q$a;Ljava/lang/String;)Lcom/yandex/metrica/impl/i;
    .locals 3

    .prologue
    .line 224
    new-instance v0, Lcom/yandex/metrica/impl/f;

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/q$a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v2

    invoke-direct {v0, p1, v1, v2}, Lcom/yandex/metrica/impl/f;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;
    .locals 2

    .prologue
    .line 232
    new-instance v0, Lcom/yandex/metrica/impl/f;

    sget-object v1, Lcom/yandex/metrica/impl/q$a;->c:Lcom/yandex/metrica/impl/q$a;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/yandex/metrica/impl/f;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method static a(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/i;
    .locals 2

    .prologue
    .line 236
    new-instance v0, Lcom/yandex/metrica/impl/f;

    sget-object v1, Lcom/yandex/metrica/impl/q$a;->c:Lcom/yandex/metrica/impl/q$a;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v1

    invoke-direct {v0, p1, p0, v1}, Lcom/yandex/metrica/impl/f;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static a(I)Z
    .locals 2

    .prologue
    .line 202
    sget-object v0, Lcom/yandex/metrica/impl/q;->d:Ljava/util/EnumSet;

    invoke-static {p0}, Lcom/yandex/metrica/impl/q$a;->a(I)Lcom/yandex/metrica/impl/q$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static a(Lcom/yandex/metrica/impl/i;)Z
    .locals 2

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/i;->c()I

    move-result v0

    sget-object v1, Lcom/yandex/metrica/impl/q$a;->k:Lcom/yandex/metrica/impl/q$a;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 207
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/i;->c()I

    move-result v0

    sget-object v1, Lcom/yandex/metrica/impl/q$a;->l:Lcom/yandex/metrica/impl/q$a;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 208
    :cond_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/i;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    .line 206
    goto :goto_0
.end method

.method public static a(Lcom/yandex/metrica/impl/q$a;)Z
    .locals 1

    .prologue
    .line 194
    sget-object v0, Lcom/yandex/metrica/impl/q;->b:Ljava/util/EnumSet;

    invoke-virtual {v0, p0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static b(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;
    .locals 2

    .prologue
    .line 248
    new-instance v0, Lcom/yandex/metrica/impl/f;

    sget-object v1, Lcom/yandex/metrica/impl/q$a;->B:Lcom/yandex/metrica/impl/q$a;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/yandex/metrica/impl/f;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method static b(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/i;
    .locals 2

    .prologue
    .line 244
    new-instance v0, Lcom/yandex/metrica/impl/f;

    sget-object v1, Lcom/yandex/metrica/impl/q$a;->g:Lcom/yandex/metrica/impl/q$a;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v1

    invoke-direct {v0, p1, p0, v1}, Lcom/yandex/metrica/impl/f;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static b(I)Z
    .locals 2

    .prologue
    .line 216
    sget-object v0, Lcom/yandex/metrica/impl/q;->f:Ljava/util/EnumSet;

    invoke-static {p0}, Lcom/yandex/metrica/impl/q$a;->a(I)Lcom/yandex/metrica/impl/q$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static b(Lcom/yandex/metrica/impl/q$a;)Z
    .locals 1

    .prologue
    .line 198
    sget-object v0, Lcom/yandex/metrica/impl/q;->c:Ljava/util/EnumSet;

    invoke-virtual {v0, p0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static c(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;
    .locals 2

    .prologue
    .line 252
    new-instance v0, Lcom/yandex/metrica/impl/f;

    sget-object v1, Lcom/yandex/metrica/impl/q$a;->e:Lcom/yandex/metrica/impl/q$a;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/yandex/metrica/impl/f;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method static c(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/i;
    .locals 2

    .prologue
    .line 256
    new-instance v0, Lcom/yandex/metrica/impl/f;

    sget-object v1, Lcom/yandex/metrica/impl/q$a;->y:Lcom/yandex/metrica/impl/q$a;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v1

    invoke-direct {v0, p1, p0, v1}, Lcom/yandex/metrica/impl/f;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static c(I)Z
    .locals 2

    .prologue
    .line 220
    sget-object v0, Lcom/yandex/metrica/impl/q;->g:Ljava/util/EnumSet;

    invoke-static {p0}, Lcom/yandex/metrica/impl/q$a;->a(I)Lcom/yandex/metrica/impl/q$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static c(Lcom/yandex/metrica/impl/q$a;)Z
    .locals 1

    .prologue
    .line 212
    sget-object v0, Lcom/yandex/metrica/impl/q;->e:Ljava/util/EnumSet;

    invoke-virtual {v0, p0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/yandex/metrica/impl/q$a;)Lcom/yandex/metrica/impl/i;
    .locals 3

    .prologue
    .line 228
    new-instance v0, Lcom/yandex/metrica/impl/f;

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/q$a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/f;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method public static d(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;
    .locals 3

    .prologue
    .line 260
    new-instance v0, Lcom/yandex/metrica/impl/f;

    const-string v1, ""

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->t:Lcom/yandex/metrica/impl/q$a;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v2

    invoke-direct {v0, v1, p0, v2}, Lcom/yandex/metrica/impl/f;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method static d(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/i;
    .locals 4

    .prologue
    .line 272
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 273
    const-string v1, "type"

    invoke-virtual {v0, v1, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    const-string v1, "link"

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    new-instance v1, Lcom/yandex/metrica/impl/f;

    invoke-static {v0}, Lcom/yandex/metrica/impl/utils/g;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    sget-object v3, Lcom/yandex/metrica/impl/q$a;->D:Lcom/yandex/metrica/impl/q$a;

    .line 276
    invoke-virtual {v3}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v3

    invoke-direct {v1, v0, v2, v3}, Lcom/yandex/metrica/impl/f;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 275
    return-object v1
.end method

.method static e(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;
    .locals 1

    .prologue
    .line 264
    const-string v0, "open"

    invoke-static {v0, p0}, Lcom/yandex/metrica/impl/q;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    return-object v0
.end method

.method static f(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;
    .locals 1

    .prologue
    .line 268
    const-string v0, "referral"

    invoke-static {v0, p0}, Lcom/yandex/metrica/impl/q;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    return-object v0
.end method
