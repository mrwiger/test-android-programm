.class public Lcom/samsung/multiscreen/BLESearchProvider;
.super Lcom/samsung/multiscreen/SearchProvider;
.source "BLESearchProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;,
        Lcom/samsung/multiscreen/BLESearchProvider$DeviceType;
    }
.end annotation


# static fields
.field private static final BLE_NETWORK_TYPE:Ljava/lang/String; = "BLE"

.field private static final BLE_NOT_SUPPORTED:Ljava/lang/String; = "BLE is not supported"

.field private static final BLE_RSSI_MIMIMUM:I = -0x64

.field private static final BLUETOOTH_NOT_SUPPORTED:Ljava/lang/String; = "Bluetooth not supported"

.field private static final DEFAULT_TTL:J = 0x1388L

.field private static final SAMSUNG_DEVICE_STATUS:B = 0x14t

.field private static final SAMSUNG_MANUFACTURER_ID:Ljava/lang/String; = "0075"

.field private static final TAG:Ljava/lang/String; = "BLESearchProvider"


# instance fields
.field private final BT_devices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private final context:Landroid/content/Context;

.field private final devices:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final leScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 183
    invoke-direct {p0}, Lcom/samsung/multiscreen/SearchProvider;-><init>()V

    .line 93
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/multiscreen/BLESearchProvider;->devices:Ljava/util/Map;

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/multiscreen/BLESearchProvider;->BT_devices:Ljava/util/ArrayList;

    .line 95
    new-instance v0, Lcom/samsung/multiscreen/BLESearchProvider$1;

    invoke-direct {v0, p0}, Lcom/samsung/multiscreen/BLESearchProvider$1;-><init>(Lcom/samsung/multiscreen/BLESearchProvider;)V

    iput-object v0, p0, Lcom/samsung/multiscreen/BLESearchProvider;->leScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    .line 185
    iput-object p1, p0, Lcom/samsung/multiscreen/BLESearchProvider;->context:Landroid/content/Context;

    .line 187
    invoke-direct {p0}, Lcom/samsung/multiscreen/BLESearchProvider;->setupBluetoothAdapter()V

    .line 188
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/samsung/multiscreen/Search$SearchListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchListener"    # Lcom/samsung/multiscreen/Search$SearchListener;

    .prologue
    .line 191
    invoke-direct {p0, p2}, Lcom/samsung/multiscreen/SearchProvider;-><init>(Lcom/samsung/multiscreen/Search$SearchListener;)V

    .line 93
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/multiscreen/BLESearchProvider;->devices:Ljava/util/Map;

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/multiscreen/BLESearchProvider;->BT_devices:Ljava/util/ArrayList;

    .line 95
    new-instance v0, Lcom/samsung/multiscreen/BLESearchProvider$1;

    invoke-direct {v0, p0}, Lcom/samsung/multiscreen/BLESearchProvider$1;-><init>(Lcom/samsung/multiscreen/BLESearchProvider;)V

    iput-object v0, p0, Lcom/samsung/multiscreen/BLESearchProvider;->leScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    .line 193
    iput-object p1, p0, Lcom/samsung/multiscreen/BLESearchProvider;->context:Landroid/content/Context;

    .line 195
    invoke-direct {p0}, Lcom/samsung/multiscreen/BLESearchProvider;->setupBluetoothAdapter()V

    .line 196
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/multiscreen/BLESearchProvider;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/BLESearchProvider;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/multiscreen/BLESearchProvider;->devices:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/multiscreen/BLESearchProvider;Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;J)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/BLESearchProvider;
    .param p1, "x1"    # Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;
    .param p2, "x2"    # J

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/multiscreen/BLESearchProvider;->updateAlive(Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;J)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/multiscreen/BLESearchProvider;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/BLESearchProvider;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/multiscreen/BLESearchProvider;->BT_devices:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static create(Landroid/content/Context;)Lcom/samsung/multiscreen/SearchProvider;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 256
    new-instance v0, Lcom/samsung/multiscreen/BLESearchProvider;

    invoke-direct {v0, p0}, Lcom/samsung/multiscreen/BLESearchProvider;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method static create(Landroid/content/Context;Lcom/samsung/multiscreen/Search$SearchListener;)Lcom/samsung/multiscreen/SearchProvider;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "searchListener"    # Lcom/samsung/multiscreen/Search$SearchListener;

    .prologue
    .line 261
    new-instance v0, Lcom/samsung/multiscreen/BLESearchProvider;

    invoke-direct {v0, p0, p1}, Lcom/samsung/multiscreen/BLESearchProvider;-><init>(Landroid/content/Context;Lcom/samsung/multiscreen/Search$SearchListener;)V

    return-object v0
.end method

.method private setupBluetoothAdapter()V
    .locals 3

    .prologue
    .line 201
    iget-object v1, p0, Lcom/samsung/multiscreen/BLESearchProvider;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "android.hardware.bluetooth_le"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 202
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string v2, "BLE is not supported"

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 207
    :cond_0
    iget-object v1, p0, Lcom/samsung/multiscreen/BLESearchProvider;->context:Landroid/content/Context;

    const-string v2, "bluetooth"

    .line 208
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothManager;

    .line 209
    .local v0, "bluetoothManager":Landroid/bluetooth/BluetoothManager;
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothManager;->getAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/multiscreen/BLESearchProvider;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 212
    iget-object v1, p0, Lcom/samsung/multiscreen/BLESearchProvider;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v1, :cond_1

    .line 213
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string v2, "Bluetooth not supported"

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 215
    :cond_1
    return-void
.end method

.method private updateAlive(Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;J)V
    .locals 6
    .param p1, "btService"    # Lcom/samsung/multiscreen/BLESearchProvider$BluetoothService;
    .param p2, "ttl"    # J

    .prologue
    .line 176
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 177
    .local v2, "now":J
    add-long v0, v2, p2

    .line 178
    .local v0, "expires":J
    iget-object v4, p0, Lcom/samsung/multiscreen/BLESearchProvider;->devices:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, p1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    return-void
.end method


# virtual methods
.method public start()V
    .locals 2

    .prologue
    .line 220
    const-string v0, "BLESearchProvider"

    const-string v1, "Start BLE search"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    iget-boolean v0, p0, Lcom/samsung/multiscreen/BLESearchProvider;->searching:Z

    if-eqz v0, :cond_0

    .line 223
    invoke-virtual {p0}, Lcom/samsung/multiscreen/BLESearchProvider;->stop()Z

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/samsung/multiscreen/BLESearchProvider;->devices:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 227
    iget-object v0, p0, Lcom/samsung/multiscreen/BLESearchProvider;->BT_devices:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 228
    iget-object v0, p0, Lcom/samsung/multiscreen/BLESearchProvider;->TVListOnlyBle:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 229
    invoke-virtual {p0}, Lcom/samsung/multiscreen/BLESearchProvider;->clearServices()V

    .line 233
    iget-object v0, p0, Lcom/samsung/multiscreen/BLESearchProvider;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/samsung/multiscreen/BLESearchProvider;->leScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->startLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/multiscreen/BLESearchProvider;->searching:Z

    .line 234
    return-void
.end method

.method public stop()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 238
    const-string v1, "BLESearchProvider"

    const-string v2, "Stop BLE search"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    iget-boolean v1, p0, Lcom/samsung/multiscreen/BLESearchProvider;->searching:Z

    if-nez v1, :cond_0

    .line 246
    :goto_0
    return v0

    .line 243
    :cond_0
    iput-boolean v0, p0, Lcom/samsung/multiscreen/BLESearchProvider;->searching:Z

    .line 244
    iget-object v0, p0, Lcom/samsung/multiscreen/BLESearchProvider;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/samsung/multiscreen/BLESearchProvider;->leScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->stopLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)V

    .line 246
    const/4 v0, 0x1

    goto :goto_0
.end method
