.class final Lcom/google/obf/fp$a;
.super Lcom/google/obf/ew;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/fp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/obf/ew",
        "<",
        "Ljava/util/Collection",
        "<TE;>;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/obf/ew;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/ew",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/obf/fk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/fk",
            "<+",
            "Ljava/util/Collection",
            "<TE;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/obf/eg;Ljava/lang/reflect/Type;Lcom/google/obf/ew;Lcom/google/obf/fk;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/eg;",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/obf/ew",
            "<TE;>;",
            "Lcom/google/obf/fk",
            "<+",
            "Ljava/util/Collection",
            "<TE;>;>;)V"
        }
    .end annotation

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/obf/ew;-><init>()V

    .line 2
    new-instance v0, Lcom/google/obf/ga;

    invoke-direct {v0, p1, p3, p2}, Lcom/google/obf/ga;-><init>(Lcom/google/obf/eg;Lcom/google/obf/ew;Ljava/lang/reflect/Type;)V

    iput-object v0, p0, Lcom/google/obf/fp$a;->a:Lcom/google/obf/ew;

    .line 3
    iput-object p4, p0, Lcom/google/obf/fp$a;->b:Lcom/google/obf/fk;

    .line 4
    return-void
.end method


# virtual methods
.method public a(Lcom/google/obf/ge;)Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/ge;",
            ")",
            "Ljava/util/Collection",
            "<TE;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p1}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    move-result-object v0

    sget-object v1, Lcom/google/obf/gf;->i:Lcom/google/obf/gf;

    if-ne v0, v1, :cond_0

    .line 6
    invoke-virtual {p1}, Lcom/google/obf/ge;->j()V

    .line 7
    const/4 v0, 0x0

    .line 15
    :goto_0
    return-object v0

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/google/obf/fp$a;->b:Lcom/google/obf/fk;

    invoke-interface {v0}, Lcom/google/obf/fk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 9
    invoke-virtual {p1}, Lcom/google/obf/ge;->a()V

    .line 10
    :goto_1
    invoke-virtual {p1}, Lcom/google/obf/ge;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 11
    iget-object v1, p0, Lcom/google/obf/fp$a;->a:Lcom/google/obf/ew;

    invoke-virtual {v1, p1}, Lcom/google/obf/ew;->read(Lcom/google/obf/ge;)Ljava/lang/Object;

    move-result-object v1

    .line 12
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 14
    :cond_1
    invoke-virtual {p1}, Lcom/google/obf/ge;->b()V

    goto :goto_0
.end method

.method public a(Lcom/google/obf/gg;Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/gg;",
            "Ljava/util/Collection",
            "<TE;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16
    if-nez p2, :cond_0

    .line 17
    invoke-virtual {p1}, Lcom/google/obf/gg;->f()Lcom/google/obf/gg;

    .line 24
    :goto_0
    return-void

    .line 19
    :cond_0
    invoke-virtual {p1}, Lcom/google/obf/gg;->b()Lcom/google/obf/gg;

    .line 20
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 21
    iget-object v2, p0, Lcom/google/obf/fp$a;->a:Lcom/google/obf/ew;

    invoke-virtual {v2, p1, v1}, Lcom/google/obf/ew;->write(Lcom/google/obf/gg;Ljava/lang/Object;)V

    goto :goto_1

    .line 23
    :cond_1
    invoke-virtual {p1}, Lcom/google/obf/gg;->c()Lcom/google/obf/gg;

    goto :goto_0
.end method

.method public synthetic read(Lcom/google/obf/ge;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/google/obf/fp$a;->a(Lcom/google/obf/ge;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public synthetic write(Lcom/google/obf/gg;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26
    check-cast p2, Ljava/util/Collection;

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/fp$a;->a(Lcom/google/obf/gg;Ljava/util/Collection;)V

    return-void
.end method
