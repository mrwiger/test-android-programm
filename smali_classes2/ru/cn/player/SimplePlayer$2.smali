.class Lru/cn/player/SimplePlayer$2;
.super Ljava/lang/Object;
.source "SimplePlayer.java"

# interfaces
.implements Lru/cn/player/BufferingListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/player/SimplePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/player/SimplePlayer;


# direct methods
.method constructor <init>(Lru/cn/player/SimplePlayer;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/player/SimplePlayer;

    .prologue
    .line 220
    iput-object p1, p0, Lru/cn/player/SimplePlayer$2;->this$0:Lru/cn/player/SimplePlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public endBuffering()V
    .locals 3

    .prologue
    .line 228
    iget-object v1, p0, Lru/cn/player/SimplePlayer$2;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v1}, Lru/cn/player/SimplePlayer;->access$000(Lru/cn/player/SimplePlayer;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/player/SimplePlayer$Listener;

    .line 229
    .local v0, "listener":Lru/cn/player/SimplePlayer$Listener;
    invoke-interface {v0}, Lru/cn/player/SimplePlayer$Listener;->endBuffering()V

    goto :goto_0

    .line 231
    .end local v0    # "listener":Lru/cn/player/SimplePlayer$Listener;
    :cond_0
    return-void
.end method

.method public startBuffering()V
    .locals 3

    .prologue
    .line 222
    iget-object v1, p0, Lru/cn/player/SimplePlayer$2;->this$0:Lru/cn/player/SimplePlayer;

    invoke-static {v1}, Lru/cn/player/SimplePlayer;->access$000(Lru/cn/player/SimplePlayer;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/player/SimplePlayer$Listener;

    .line 223
    .local v0, "listener":Lru/cn/player/SimplePlayer$Listener;
    invoke-interface {v0}, Lru/cn/player/SimplePlayer$Listener;->startBuffering()V

    goto :goto_0

    .line 225
    .end local v0    # "listener":Lru/cn/player/SimplePlayer$Listener;
    :cond_0
    return-void
.end method
