.class Lru/cn/tv/stb/StbActivity$11;
.super Ljava/lang/Object;
.source "StbActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/stb/StbActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/stb/StbActivity;


# direct methods
.method constructor <init>(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 459
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity$11;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "index"    # I
    .param p4, "l"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 463
    .local p1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$11;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$2100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/TracksAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 464
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$11;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$2200(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector;

    move-result-object v0

    invoke-interface {v0, p3}, Lru/cn/player/ITrackSelector;->selectItem(I)V

    .line 465
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$11;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$2100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/TracksAdapter;

    move-result-object v0

    invoke-virtual {v0, p3}, Lru/cn/tv/player/controller/TracksAdapter;->setSelectedPosition(I)V

    .line 479
    :cond_0
    :goto_0
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$11;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$2500(Lru/cn/tv/stb/StbActivity;)V

    .line 480
    return-void

    .line 466
    :cond_1
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$11;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$2300(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/TracksAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 467
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$11;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$2400(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/player/ITrackSelector;->deactivatable()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 468
    if-nez p3, :cond_2

    .line 469
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$11;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$2400(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/player/ITrackSelector;->disable()V

    .line 476
    :goto_1
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$11;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$2300(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/TracksAdapter;

    move-result-object v0

    invoke-virtual {v0, p3}, Lru/cn/tv/player/controller/TracksAdapter;->setSelectedPosition(I)V

    goto :goto_0

    .line 471
    :cond_2
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$11;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$2400(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector;

    move-result-object v0

    add-int/lit8 v1, p3, -0x1

    invoke-interface {v0, v1}, Lru/cn/player/ITrackSelector;->selectItem(I)V

    goto :goto_1

    .line 474
    :cond_3
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$11;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$2400(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector;

    move-result-object v0

    invoke-interface {v0, p3}, Lru/cn/player/ITrackSelector;->selectItem(I)V

    goto :goto_1
.end method
