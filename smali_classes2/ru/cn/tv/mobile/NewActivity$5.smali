.class Lru/cn/tv/mobile/NewActivity$5;
.super Ljava/lang/Object;
.source "NewActivity.java"

# interfaces
.implements Lru/cn/tv/mobile/ChannelsScheduleFragment$ChannelsScheduleFragmentListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/mobile/NewActivity;->openChannels(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/mobile/NewActivity;

.field final synthetic val$f:Lru/cn/tv/mobile/ChannelsScheduleFragment;


# direct methods
.method constructor <init>(Lru/cn/tv/mobile/NewActivity;Lru/cn/tv/mobile/ChannelsScheduleFragment;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/mobile/NewActivity;

    .prologue
    .line 391
    iput-object p1, p0, Lru/cn/tv/mobile/NewActivity$5;->this$0:Lru/cn/tv/mobile/NewActivity;

    iput-object p2, p0, Lru/cn/tv/mobile/NewActivity$5;->val$f:Lru/cn/tv/mobile/ChannelsScheduleFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onListOpened()V
    .locals 2

    .prologue
    .line 395
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity$5;->val$f:Lru/cn/tv/mobile/ChannelsScheduleFragment;

    invoke-virtual {v0}, Lru/cn/tv/mobile/ChannelsScheduleFragment;->getViewMode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 406
    :goto_0
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity$5;->this$0:Lru/cn/tv/mobile/NewActivity;

    iget-object v0, v0, Lru/cn/tv/mobile/NewActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBarDrawerToggle;->setDrawerIndicatorEnabled(Z)V

    .line 407
    return-void

    .line 397
    :pswitch_0
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity$5;->this$0:Lru/cn/tv/mobile/NewActivity;

    const v1, 0x7f0e008f

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/NewActivity;->setDefaultTitle(I)V

    goto :goto_0

    .line 400
    :pswitch_1
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity$5;->this$0:Lru/cn/tv/mobile/NewActivity;

    const v1, 0x7f0e008d

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/NewActivity;->setDefaultTitle(I)V

    goto :goto_0

    .line 403
    :pswitch_2
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity$5;->this$0:Lru/cn/tv/mobile/NewActivity;

    const v1, 0x7f0e008e

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/NewActivity;->setDefaultTitle(I)V

    goto :goto_0

    .line 395
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onScheduleOpened(Ljava/lang/String;)V
    .locals 2
    .param p1, "channelTitle"    # Ljava/lang/String;

    .prologue
    .line 411
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity$5;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-virtual {v0, p1}, Lru/cn/tv/mobile/NewActivity;->setDefaultTitle(Ljava/lang/String;)V

    .line 412
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity$5;->this$0:Lru/cn/tv/mobile/NewActivity;

    iget-object v0, v0, Lru/cn/tv/mobile/NewActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBarDrawerToggle;->setDrawerIndicatorEnabled(Z)V

    .line 413
    return-void
.end method
