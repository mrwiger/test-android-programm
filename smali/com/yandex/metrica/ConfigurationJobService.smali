.class public Lcom/yandex/metrica/ConfigurationJobService;
.super Landroid/app/job/JobService;
.source "SourceFile"


# instance fields
.field private a:Lcom/yandex/metrica/impl/ob/bu;

.field private b:Lcom/yandex/metrica/impl/ob/ce;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 6

    .prologue
    .line 46
    invoke-super {p0}, Landroid/app/job/JobService;->onCreate()V

    .line 48
    invoke-virtual {p0}, Lcom/yandex/metrica/ConfigurationJobService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 49
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "[ConfigurationJobService:%s]"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 53
    new-instance v1, Lcom/yandex/metrica/impl/ob/bu;

    invoke-direct {v1}, Lcom/yandex/metrica/impl/ob/bu;-><init>()V

    iput-object v1, p0, Lcom/yandex/metrica/ConfigurationJobService;->a:Lcom/yandex/metrica/impl/ob/bu;

    .line 54
    new-instance v1, Lcom/yandex/metrica/impl/ob/ca;

    invoke-virtual {p0}, Lcom/yandex/metrica/ConfigurationJobService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/yandex/metrica/ConfigurationJobService;->a:Lcom/yandex/metrica/impl/ob/bu;

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/ob/bu;->a()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    new-instance v4, Lcom/yandex/metrica/impl/ob/bv;

    invoke-direct {v4, v0}, Lcom/yandex/metrica/impl/ob/bv;-><init>(Landroid/content/Context;)V

    invoke-direct {v1, v2, v3, v4}, Lcom/yandex/metrica/impl/ob/ca;-><init>(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;Lcom/yandex/metrica/impl/ob/cb;)V

    .line 56
    new-instance v0, Lcom/yandex/metrica/impl/ob/ce;

    invoke-virtual {p0}, Lcom/yandex/metrica/ConfigurationJobService;->getApplicationContext()Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/ce;-><init>(Lcom/yandex/metrica/impl/ob/ca;)V

    iput-object v0, p0, Lcom/yandex/metrica/ConfigurationJobService;->b:Lcom/yandex/metrica/impl/ob/ce;

    .line 58
    return-void
.end method

.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 5
    .param p1, "params"    # Landroid/app/job/JobParameters;

    .prologue
    const/4 v0, 0x0

    .line 63
    if-eqz p1, :cond_0

    .line 65
    :try_start_0
    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I

    move-result v1

    const v2, 0x5a23e709

    if-ne v1, v2, :cond_0

    .line 66
    iget-object v1, p0, Lcom/yandex/metrica/ConfigurationJobService;->a:Lcom/yandex/metrica/impl/ob/bu;

    iget-object v2, p0, Lcom/yandex/metrica/ConfigurationJobService;->b:Lcom/yandex/metrica/impl/ob/ce;

    const/4 v3, 0x0

    new-instance v4, Lcom/yandex/metrica/ConfigurationJobService$1;

    invoke-direct {v4, p0, p1}, Lcom/yandex/metrica/ConfigurationJobService$1;-><init>(Lcom/yandex/metrica/ConfigurationJobService;Landroid/app/job/JobParameters;)V

    invoke-virtual {v1, v2, v3, v4}, Lcom/yandex/metrica/impl/ob/bu;->a(Lcom/yandex/metrica/impl/ob/cd;Landroid/os/Bundle;Lcom/yandex/metrica/impl/ob/cc;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    const/4 v0, 0x1

    .line 83
    :cond_0
    :goto_0
    return v0

    .line 81
    :catch_0
    move-exception v1

    invoke-virtual {p0, p1, v0}, Lcom/yandex/metrica/ConfigurationJobService;->jobFinished(Landroid/app/job/JobParameters;Z)V

    goto :goto_0
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 1
    .param p1, "params"    # Landroid/app/job/JobParameters;

    .prologue
    .line 91
    const/4 v0, 0x0

    return v0
.end method
