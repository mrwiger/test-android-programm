.class public final Lcom/google/obf/fr;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/ex;


# instance fields
.field private final a:Lcom/google/obf/ff;


# direct methods
.method public constructor <init>(Lcom/google/obf/ff;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/obf/fr;->a:Lcom/google/obf/ff;

    .line 3
    return-void
.end method


# virtual methods
.method public a(Lcom/google/obf/eg;Lcom/google/obf/gd;)Lcom/google/obf/ew;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/obf/eg;",
            "Lcom/google/obf/gd",
            "<TT;>;)",
            "Lcom/google/obf/ew",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 4
    invoke-virtual {p2}, Lcom/google/obf/gd;->a()Ljava/lang/Class;

    move-result-object v0

    .line 5
    const-class v1, Lcom/google/obf/ez;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/google/obf/ez;

    .line 6
    if-nez v0, :cond_0

    .line 7
    const/4 v0, 0x0

    .line 8
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/obf/fr;->a:Lcom/google/obf/ff;

    invoke-virtual {p0, v1, p1, p2, v0}, Lcom/google/obf/fr;->a(Lcom/google/obf/ff;Lcom/google/obf/eg;Lcom/google/obf/gd;Lcom/google/obf/ez;)Lcom/google/obf/ew;

    move-result-object v0

    goto :goto_0
.end method

.method a(Lcom/google/obf/ff;Lcom/google/obf/eg;Lcom/google/obf/gd;Lcom/google/obf/ez;)Lcom/google/obf/ew;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/ff;",
            "Lcom/google/obf/eg;",
            "Lcom/google/obf/gd",
            "<*>;",
            "Lcom/google/obf/ez;",
            ")",
            "Lcom/google/obf/ew",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 9
    invoke-interface {p4}, Lcom/google/obf/ez;->a()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/obf/gd;->b(Ljava/lang/Class;)Lcom/google/obf/gd;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/obf/ff;->a(Lcom/google/obf/gd;)Lcom/google/obf/fk;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/obf/fk;->a()Ljava/lang/Object;

    move-result-object v0

    .line 10
    instance-of v1, v0, Lcom/google/obf/ew;

    if-eqz v1, :cond_1

    .line 11
    check-cast v0, Lcom/google/obf/ew;

    .line 20
    :goto_0
    if-eqz v0, :cond_0

    .line 21
    invoke-virtual {v0}, Lcom/google/obf/ew;->nullSafe()Lcom/google/obf/ew;

    move-result-object v0

    .line 22
    :cond_0
    return-object v0

    .line 12
    :cond_1
    instance-of v1, v0, Lcom/google/obf/ex;

    if-eqz v1, :cond_2

    .line 13
    check-cast v0, Lcom/google/obf/ex;

    invoke-interface {v0, p2, p3}, Lcom/google/obf/ex;->a(Lcom/google/obf/eg;Lcom/google/obf/gd;)Lcom/google/obf/ew;

    move-result-object v0

    goto :goto_0

    .line 14
    :cond_2
    instance-of v1, v0, Lcom/google/obf/et;

    if-nez v1, :cond_3

    instance-of v1, v0, Lcom/google/obf/el;

    if-eqz v1, :cond_6

    .line 15
    :cond_3
    instance-of v1, v0, Lcom/google/obf/et;

    if-eqz v1, :cond_4

    move-object v1, v0

    check-cast v1, Lcom/google/obf/et;

    .line 16
    :goto_1
    instance-of v2, v0, Lcom/google/obf/el;

    if-eqz v2, :cond_5

    check-cast v0, Lcom/google/obf/el;

    move-object v2, v0

    .line 17
    :goto_2
    new-instance v0, Lcom/google/obf/fz;

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/obf/fz;-><init>(Lcom/google/obf/et;Lcom/google/obf/el;Lcom/google/obf/eg;Lcom/google/obf/gd;Lcom/google/obf/ex;)V

    goto :goto_0

    :cond_4
    move-object v1, v5

    .line 15
    goto :goto_1

    :cond_5
    move-object v2, v5

    .line 16
    goto :goto_2

    .line 19
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "@JsonAdapter value must be TypeAdapter, TypeAdapterFactory, JsonSerializer or JsonDeserializer reference."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
