.class final Lcom/yandex/metrica/impl/ag$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/impl/ag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/yandex/metrica/impl/ag;

.field private final b:I

.field private final c:Lcom/yandex/metrica/impl/i;

.field private final d:Landroid/os/Bundle;

.field private final e:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/yandex/metrica/impl/ag;Landroid/content/Context;Lcom/yandex/metrica/impl/i;Landroid/os/Bundle;I)V
    .locals 1

    .prologue
    .line 370
    iput-object p1, p0, Lcom/yandex/metrica/impl/ag$a;->a:Lcom/yandex/metrica/impl/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 371
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ag$a;->e:Landroid/content/Context;

    .line 372
    iput p5, p0, Lcom/yandex/metrica/impl/ag$a;->b:I

    .line 373
    iput-object p3, p0, Lcom/yandex/metrica/impl/ag$a;->c:Lcom/yandex/metrica/impl/i;

    .line 374
    iput-object p4, p0, Lcom/yandex/metrica/impl/ag$a;->d:Landroid/os/Bundle;

    .line 375
    return-void
.end method

.method private static a(Landroid/os/Bundle;)Lcom/yandex/metrica/CounterConfiguration;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 443
    .line 445
    if-eqz p0, :cond_0

    .line 447
    :try_start_0
    const-string v0, "COUNTER_MIGRATION_CFG_OBJ"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/CounterConfiguration;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    move-object v1, v0

    .line 452
    :goto_1
    return-object v1

    .line 449
    :catch_0
    move-exception v0

    goto :goto_1

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 401
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag$a;->d:Landroid/os/Bundle;

    invoke-static {v0}, Lcom/yandex/metrica/CounterConfiguration;->b(Landroid/os/Bundle;)Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    .line 402
    invoke-static {v0}, Lcom/yandex/metrica/impl/ag;->b(Lcom/yandex/metrica/CounterConfiguration;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 439
    :cond_0
    :goto_0
    return-void

    .line 406
    :cond_1
    iget-object v1, p0, Lcom/yandex/metrica/impl/ag$a;->a:Lcom/yandex/metrica/impl/ag;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ag$a;->c:Lcom/yandex/metrica/impl/i;

    iget v3, p0, Lcom/yandex/metrica/impl/ag$a;->b:I

    invoke-virtual {v1, v2, v0, v3}, Lcom/yandex/metrica/impl/ag;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/CounterConfiguration;I)Lcom/yandex/metrica/impl/ob/t;

    move-result-object v1

    .line 410
    if-eqz v1, :cond_0

    .line 414
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/t;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/yandex/metrica/impl/ag;->a(Lcom/yandex/metrica/CounterConfiguration;Ljava/lang/String;)V

    .line 416
    invoke-static {}, Lcom/yandex/metrica/impl/ag;->c()Ljava/util/Map;

    move-result-object v2

    monitor-enter v2

    .line 418
    :try_start_0
    iget-object v3, p0, Lcom/yandex/metrica/impl/ag$a;->a:Lcom/yandex/metrica/impl/ag;

    invoke-virtual {v3, v0}, Lcom/yandex/metrica/impl/ag;->a(Lcom/yandex/metrica/CounterConfiguration;)V

    .line 419
    iget-object v3, p0, Lcom/yandex/metrica/impl/ag$a;->a:Lcom/yandex/metrica/impl/ag;

    iget-object v4, p0, Lcom/yandex/metrica/impl/ag$a;->c:Lcom/yandex/metrica/impl/i;

    invoke-virtual {v3, v1, v0, v4}, Lcom/yandex/metrica/impl/ag;->a(Lcom/yandex/metrica/impl/ob/t;Lcom/yandex/metrica/CounterConfiguration;Lcom/yandex/metrica/impl/i;)Lcom/yandex/metrica/impl/ob/v;

    move-result-object v3

    .line 420
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/t;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/ob/v;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 422
    iget-object v1, p0, Lcom/yandex/metrica/impl/ag$a;->a:Lcom/yandex/metrica/impl/ag;

    iget-object v4, p0, Lcom/yandex/metrica/impl/ag$a;->e:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 423
    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->m()Z

    move-result v5

    .line 422
    invoke-virtual {v1, v4, v5}, Lcom/yandex/metrica/impl/ag;->a(ZZ)V

    .line 426
    :cond_2
    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->j()Ljava/lang/String;

    move-result-object v1

    .line 1378
    iget-object v4, p0, Lcom/yandex/metrica/impl/ag$a;->d:Landroid/os/Bundle;

    const-string v5, "COUNTER_MIGRATION_CFG_OBJ"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1379
    iget-object v4, p0, Lcom/yandex/metrica/impl/ag$a;->d:Landroid/os/Bundle;

    invoke-static {v4}, Lcom/yandex/metrica/impl/ag$a;->a(Landroid/os/Bundle;)Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v4

    .line 1380
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/yandex/metrica/CounterConfiguration;->D()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1381
    iget-object v5, p0, Lcom/yandex/metrica/impl/ag$a;->e:Landroid/content/Context;

    iget v6, p0, Lcom/yandex/metrica/impl/ag$a;->b:I

    .line 1384
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    .line 1381
    invoke-static {v5, v4, v6, v7}, Lcom/yandex/metrica/impl/ob/t;->a(Landroid/content/Context;Lcom/yandex/metrica/CounterConfiguration;Ljava/lang/Integer;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/t;

    move-result-object v5

    .line 1386
    invoke-static {}, Lcom/yandex/metrica/impl/ag;->c()Ljava/util/Map;

    move-result-object v6

    invoke-virtual {v5}, Lcom/yandex/metrica/impl/ob/t;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 1388
    new-instance v6, Lcom/yandex/metrica/CounterConfiguration;

    invoke-direct {v6, v4}, Lcom/yandex/metrica/CounterConfiguration;-><init>(Lcom/yandex/metrica/CounterConfiguration;)V

    .line 1389
    invoke-virtual {v6, v1}, Lcom/yandex/metrica/CounterConfiguration;->a(Ljava/lang/String;)V

    .line 1390
    iget-object v1, p0, Lcom/yandex/metrica/impl/ag$a;->a:Lcom/yandex/metrica/impl/ag;

    const/4 v4, 0x0

    invoke-virtual {v1, v5, v6, v4}, Lcom/yandex/metrica/impl/ag;->a(Lcom/yandex/metrica/impl/ob/t;Lcom/yandex/metrica/CounterConfiguration;Lcom/yandex/metrica/impl/i;)Lcom/yandex/metrica/impl/ob/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/v;->g()V

    .line 428
    :cond_3
    invoke-static {v3}, Lcom/yandex/metrica/impl/ag;->a(Lcom/yandex/metrica/impl/ob/v;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 429
    monitor-exit v2

    goto/16 :goto_0

    .line 439
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 432
    :cond_4
    :try_start_1
    iget-object v1, p0, Lcom/yandex/metrica/impl/ag$a;->c:Lcom/yandex/metrica/impl/i;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/i;->c()I

    move-result v1

    invoke-static {v1}, Lcom/yandex/metrica/impl/q;->a(I)Z

    move-result v1

    if-nez v1, :cond_5

    .line 433
    invoke-virtual {v3, v0}, Lcom/yandex/metrica/impl/ob/v;->a(Lcom/yandex/metrica/CounterConfiguration;)V

    .line 436
    :cond_5
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag$a;->c:Lcom/yandex/metrica/impl/i;

    invoke-static {v3, v0}, Lcom/yandex/metrica/impl/ag;->a(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/i;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 437
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag$a;->c:Lcom/yandex/metrica/impl/i;

    invoke-virtual {v3, v0}, Lcom/yandex/metrica/impl/ob/v;->a(Lcom/yandex/metrica/impl/i;)V

    .line 439
    :cond_6
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method
