.class public abstract Lcom/google/obf/x;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/h$a;


# instance fields
.field private a:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(IJZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 11
    return-void
.end method

.method public a(ILjava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 32
    return-void
.end method

.method protected b()Lcom/google/obf/k;
    .locals 1

    .prologue
    .line 2
    const/4 v0, 0x0

    return-object v0
.end method

.method protected abstract b(I)Lcom/google/obf/q;
.end method

.method final b(IJZ)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 7
    iget v1, p0, Lcom/google/obf/x;->a:I

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 8
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/obf/x;->a:I

    .line 9
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/obf/x;->a(IJZ)V

    .line 10
    return-void

    .line 7
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract b(JJ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation
.end method

.method protected c()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 16
    return-void
.end method

.method protected abstract c(J)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation
.end method

.method protected d()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 21
    return-void
.end method

.method protected abstract d(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation
.end method

.method protected abstract e()Z
.end method

.method final f(J)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 4
    iget v0, p0, Lcom/google/obf/x;->a:I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 5
    invoke-virtual {p0, p1, p2}, Lcom/google/obf/x;->c(J)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput v1, p0, Lcom/google/obf/x;->a:I

    .line 6
    iget v0, p0, Lcom/google/obf/x;->a:I

    return v0

    :cond_0
    move v0, v2

    .line 4
    goto :goto_0

    :cond_1
    move v1, v2

    .line 5
    goto :goto_1
.end method

.method protected abstract f()Z
.end method

.method protected g()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 26
    return-void
.end method

.method protected abstract q()J
.end method

.method protected abstract r()J
.end method

.method protected abstract s()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation
.end method

.method protected t()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 31
    return-void
.end method

.method protected abstract u()I
.end method

.method protected final v()I
    .locals 1

    .prologue
    .line 3
    iget v0, p0, Lcom/google/obf/x;->a:I

    return v0
.end method

.method final w()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 12
    iget v0, p0, Lcom/google/obf/x;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 13
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/obf/x;->a:I

    .line 14
    invoke-virtual {p0}, Lcom/google/obf/x;->c()V

    .line 15
    return-void

    .line 12
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final x()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 17
    iget v0, p0, Lcom/google/obf/x;->a:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 18
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/obf/x;->a:I

    .line 19
    invoke-virtual {p0}, Lcom/google/obf/x;->d()V

    .line 20
    return-void

    .line 17
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final y()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 22
    iget v0, p0, Lcom/google/obf/x;->a:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 23
    iput v1, p0, Lcom/google/obf/x;->a:I

    .line 24
    invoke-virtual {p0}, Lcom/google/obf/x;->g()V

    .line 25
    return-void

    .line 22
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final z()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    const/4 v2, -0x1

    .line 27
    iget v0, p0, Lcom/google/obf/x;->a:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/obf/x;->a:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/obf/x;->a:I

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 28
    iput v2, p0, Lcom/google/obf/x;->a:I

    .line 29
    invoke-virtual {p0}, Lcom/google/obf/x;->t()V

    .line 30
    return-void

    .line 27
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
