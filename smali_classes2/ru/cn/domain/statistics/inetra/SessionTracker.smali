.class final Lru/cn/domain/statistics/inetra/SessionTracker;
.super Ljava/lang/Object;
.source "SessionTracker.java"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/domain/statistics/inetra/SessionTracker$OnSessionChangeListener;
    }
.end annotation


# instance fields
.field private counter:I

.field private lastActive:J

.field private final listener:Lru/cn/domain/statistics/inetra/SessionTracker$OnSessionChangeListener;

.field private final preferences:Landroid/content/SharedPreferences;

.field private sid:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lru/cn/domain/statistics/inetra/SessionTracker$OnSessionChangeListener;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lru/cn/domain/statistics/inetra/SessionTracker$OnSessionChangeListener;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 33
    .local v0, "appContext":Landroid/content/Context;
    iput-object p2, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->listener:Lru/cn/domain/statistics/inetra/SessionTracker$OnSessionChangeListener;

    .line 34
    const-string v2, "statistics"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->preferences:Landroid/content/SharedPreferences;

    move-object v1, v0

    .line 36
    check-cast v1, Landroid/app/Application;

    .line 37
    .local v1, "currentApp":Landroid/app/Application;
    invoke-virtual {v1, p0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 39
    iget-object v2, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->preferences:Landroid/content/SharedPreferences;

    const-string v3, "interrupted"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->lastActive:J

    .line 40
    iget-object v2, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->preferences:Landroid/content/SharedPreferences;

    const-string v3, "sid"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->sid:Ljava/lang/String;

    .line 41
    return-void
.end method

.method private checkExpired()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 59
    iget-wide v0, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->lastActive:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->lastActive:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x124f80

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->sid:Ljava/lang/String;

    .line 61
    iput-wide v4, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->lastActive:J

    .line 63
    :cond_0
    return-void
.end method

.method private renewSid()V
    .locals 3

    .prologue
    .line 50
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->sid:Ljava/lang/String;

    .line 52
    iget-object v1, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 53
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "sid"

    iget-object v2, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->sid:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 54
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 55
    return-void
.end method


# virtual methods
.method getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lru/cn/domain/statistics/inetra/SessionTracker;->checkExpired()V

    .line 46
    iget-object v0, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->sid:Ljava/lang/String;

    return-object v0
.end method

.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 115
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 111
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 89
    const-string v1, "TrackingSession"

    const-string v2, "Session finished"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    iget v1, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->counter:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->counter:I

    .line 92
    iget v1, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->counter:I

    if-gtz v1, :cond_0

    .line 93
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->lastActive:J

    .line 95
    iget-object v1, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 96
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "interrupted"

    iget-wide v2, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->lastActive:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 97
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 99
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 67
    const-string v1, "TrackingSession"

    const-string v2, "Session started"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    iget v1, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->counter:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->counter:I

    .line 70
    invoke-direct {p0}, Lru/cn/domain/statistics/inetra/SessionTracker;->checkExpired()V

    .line 71
    iget-object v1, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->sid:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 72
    invoke-direct {p0}, Lru/cn/domain/statistics/inetra/SessionTracker;->renewSid()V

    .line 74
    iget-object v1, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->listener:Lru/cn/domain/statistics/inetra/SessionTracker$OnSessionChangeListener;

    if-eqz v1, :cond_0

    .line 75
    iget-object v1, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->listener:Lru/cn/domain/statistics/inetra/SessionTracker$OnSessionChangeListener;

    iget-object v2, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->sid:Ljava/lang/String;

    invoke-interface {v1, v2}, Lru/cn/domain/statistics/inetra/SessionTracker$OnSessionChangeListener;->onSessionChange(Ljava/lang/String;)V

    .line 78
    :cond_0
    iget v1, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->counter:I

    if-lez v1, :cond_1

    .line 79
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->lastActive:J

    .line 81
    iget-object v1, p0, Lru/cn/domain/statistics/inetra/SessionTracker;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 82
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "interrupted"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 83
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 85
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 107
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 119
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 103
    return-void
.end method
