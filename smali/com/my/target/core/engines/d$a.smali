.class public final Lcom/my/target/core/engines/d$a;
.super Ljava/lang/Object;
.source "InterstitialAdHtmlEngine.java"

# interfaces
.implements Lcom/my/target/core/presenters/h$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/core/engines/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/my/target/ads/InterstitialAd;

.field private final d:Lcom/my/target/core/models/banners/f;

.field private final g:Lcom/my/target/core/engines/d;


# direct methods
.method constructor <init>(Lcom/my/target/core/engines/d;Lcom/my/target/ads/InterstitialAd;Lcom/my/target/core/models/banners/f;)V
    .locals 0

    .prologue
    .line 197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 198
    iput-object p1, p0, Lcom/my/target/core/engines/d$a;->g:Lcom/my/target/core/engines/d;

    .line 199
    iput-object p2, p0, Lcom/my/target/core/engines/d$a;->a:Lcom/my/target/ads/InterstitialAd;

    .line 200
    iput-object p3, p0, Lcom/my/target/core/engines/d$a;->d:Lcom/my/target/core/models/banners/f;

    .line 201
    return-void
.end method


# virtual methods
.method public final a(FFLandroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 242
    iget-object v0, p0, Lcom/my/target/core/engines/d$a;->d:Lcom/my/target/core/models/banners/f;

    invoke-virtual {v0}, Lcom/my/target/core/models/banners/f;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/ar;->ae()Ljava/util/Set;

    move-result-object v0

    .line 243
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 245
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 246
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/ap;

    .line 248
    sub-float v4, p2, p1

    .line 249
    invoke-virtual {v0}, Lcom/my/target/ap;->Z()F

    move-result v1

    .line 251
    cmpg-float v5, v1, v6

    if-gez v5, :cond_1

    invoke-virtual {v0}, Lcom/my/target/ap;->aa()F

    move-result v5

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_1

    .line 253
    const/high16 v1, 0x42c80000    # 100.0f

    div-float v1, p2, v1

    invoke-virtual {v0}, Lcom/my/target/ap;->aa()F

    move-result v5

    mul-float/2addr v1, v5

    .line 255
    :cond_1
    cmpl-float v5, v1, v6

    if-ltz v5, :cond_0

    cmpg-float v1, v1, v4

    if-gez v1, :cond_0

    .line 257
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 260
    :cond_2
    invoke-static {v2, p3}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 262
    :cond_3
    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/my/target/core/engines/d$a;->d:Lcom/my/target/core/models/banners/f;

    invoke-virtual {v0}, Lcom/my/target/core/models/banners/f;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    const-string v1, "playbackStarted"

    invoke-virtual {v0, v1}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 231
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 206
    invoke-static {}, Lcom/my/target/ce;->bw()Lcom/my/target/ce;

    move-result-object v0

    .line 207
    iget-object v1, p0, Lcom/my/target/core/engines/d$a;->d:Lcom/my/target/core/models/banners/f;

    invoke-virtual {v0, v1, p1, p2}, Lcom/my/target/ce;->a(Lcom/my/target/ah;Ljava/lang/String;Landroid/content/Context;)V

    .line 208
    iget-object v0, p0, Lcom/my/target/core/engines/d$a;->a:Lcom/my/target/ads/InterstitialAd;

    invoke-virtual {v0}, Lcom/my/target/ads/InterstitialAd;->getListener()Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;

    move-result-object v0

    .line 209
    if-eqz v0, :cond_0

    .line 211
    iget-object v1, p0, Lcom/my/target/core/engines/d$a;->a:Lcom/my/target/ads/InterstitialAd;

    invoke-interface {v0, v1}, Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;->onClick(Lcom/my/target/ads/InterstitialAd;)V

    .line 213
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/my/target/core/engines/d$a;->d:Lcom/my/target/core/models/banners/f;

    invoke-virtual {v0}, Lcom/my/target/core/models/banners/f;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 237
    return-void
.end method

.method public final bh()V
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/my/target/core/engines/d$a;->g:Lcom/my/target/core/engines/d;

    invoke-virtual {v0}, Lcom/my/target/core/engines/d;->dismiss()V

    .line 225
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/my/target/core/engines/d$a;->g:Lcom/my/target/core/engines/d;

    invoke-virtual {v0}, Lcom/my/target/core/engines/d;->dismiss()V

    .line 219
    return-void
.end method
