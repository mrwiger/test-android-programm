.class Lcom/yandex/mobile/ads/ar$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/yandex/mobile/ads/nativeads/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/yandex/mobile/ads/ar;->a(Lcom/yandex/mobile/ads/nativeads/o;Lcom/yandex/mobile/ads/nativeads/d;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/yandex/mobile/ads/nativeads/h;

.field final synthetic b:Lcom/yandex/mobile/ads/nativeads/d;

.field final synthetic c:Lcom/yandex/mobile/ads/nativeads/o;

.field final synthetic d:Lcom/yandex/mobile/ads/ar;


# direct methods
.method constructor <init>(Lcom/yandex/mobile/ads/ar;Lcom/yandex/mobile/ads/nativeads/h;Lcom/yandex/mobile/ads/nativeads/d;Lcom/yandex/mobile/ads/nativeads/o;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/yandex/mobile/ads/ar$1;->d:Lcom/yandex/mobile/ads/ar;

    iput-object p2, p0, Lcom/yandex/mobile/ads/ar$1;->a:Lcom/yandex/mobile/ads/nativeads/h;

    iput-object p3, p0, Lcom/yandex/mobile/ads/ar$1;->b:Lcom/yandex/mobile/ads/nativeads/d;

    iput-object p4, p0, Lcom/yandex/mobile/ads/ar$1;->c:Lcom/yandex/mobile/ads/nativeads/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 101
    iget-object v0, p0, Lcom/yandex/mobile/ads/ar$1;->a:Lcom/yandex/mobile/ads/nativeads/h;

    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/h;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/b;

    const-string v1, "image"

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/b;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/b;->a()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    check-cast v1, Lcom/yandex/mobile/ads/nativeads/e;

    invoke-virtual {v1}, Lcom/yandex/mobile/ads/nativeads/e;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    if-le v5, v2, :cond_1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-le v1, v2, :cond_1

    move v1, v2

    :goto_1
    if-eqz v1, :cond_0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 102
    :cond_3
    iget-object v0, p0, Lcom/yandex/mobile/ads/ar$1;->a:Lcom/yandex/mobile/ads/nativeads/h;

    invoke-virtual {v0, v3}, Lcom/yandex/mobile/ads/nativeads/h;->a(Ljava/util/List;)V

    .line 104
    iget-object v0, p0, Lcom/yandex/mobile/ads/ar$1;->b:Lcom/yandex/mobile/ads/nativeads/d;

    invoke-interface {v0, p1}, Lcom/yandex/mobile/ads/nativeads/d;->a(Ljava/util/Map;)V

    .line 105
    iget-object v0, p0, Lcom/yandex/mobile/ads/ar$1;->d:Lcom/yandex/mobile/ads/ar;

    iget-object v1, p0, Lcom/yandex/mobile/ads/ar$1;->c:Lcom/yandex/mobile/ads/nativeads/o;

    iget-object v2, p0, Lcom/yandex/mobile/ads/ar$1;->b:Lcom/yandex/mobile/ads/nativeads/d;

    invoke-static {v0, v1, v2}, Lcom/yandex/mobile/ads/ar;->a(Lcom/yandex/mobile/ads/ar;Lcom/yandex/mobile/ads/nativeads/o;Lcom/yandex/mobile/ads/nativeads/d;)V

    .line 106
    return-void
.end method
