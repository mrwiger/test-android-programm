.class public Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;
.super Lru/cn/ad/AdAdapter;
.source "MyTargetInterstitial.java"


# instance fields
.field private interstitial:Lcom/my/target/ads/InterstitialAd;

.field private slotId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "adSystem"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lru/cn/ad/AdAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    .line 21
    const-string v1, "slot_id"

    invoke-virtual {p2, v1}, Lru/cn/api/money_miner/replies/AdSystem;->getParamOrThrow(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 22
    .local v0, "s":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->slotId:I

    .line 23
    return-void
.end method

.method static synthetic access$000(Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;

    .prologue
    .line 13
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;

    .prologue
    .line 13
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;

    .prologue
    .line 13
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;

    .prologue
    .line 13
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$400(Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;

    .prologue
    .line 13
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$500(Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;

    .prologue
    .line 13
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 83
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->interstitial:Lcom/my/target/ads/InterstitialAd;

    invoke-virtual {v0, v1}, Lcom/my/target/ads/InterstitialAd;->setListener(Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;)V

    .line 84
    iput-object v1, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->interstitial:Lcom/my/target/ads/InterstitialAd;

    .line 85
    return-void
.end method

.method protected onLoad()V
    .locals 3

    .prologue
    .line 27
    new-instance v0, Lcom/my/target/ads/InterstitialAd;

    iget v1, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->slotId:I

    iget-object v2, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->context:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/my/target/ads/InterstitialAd;-><init>(ILandroid/content/Context;)V

    iput-object v0, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->interstitial:Lcom/my/target/ads/InterstitialAd;

    .line 28
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->interstitial:Lcom/my/target/ads/InterstitialAd;

    new-instance v1, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial$1;

    invoke-direct {v1, p0}, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial$1;-><init>(Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;)V

    invoke-virtual {v0, v1}, Lcom/my/target/ads/InterstitialAd;->setListener(Lcom/my/target/ads/InterstitialAd$InterstitialAdListener;)V

    .line 71
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->interstitial:Lcom/my/target/ads/InterstitialAd;

    invoke-virtual {v0}, Lcom/my/target/ads/InterstitialAd;->load()V

    .line 72
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->interstitial:Lcom/my/target/ads/InterstitialAd;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/MyTargetInterstitial;->interstitial:Lcom/my/target/ads/InterstitialAd;

    invoke-virtual {v0}, Lcom/my/target/ads/InterstitialAd;->show()V

    .line 79
    :cond_0
    return-void
.end method
