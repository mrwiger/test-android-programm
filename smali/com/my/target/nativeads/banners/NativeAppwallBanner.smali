.class public Lcom/my/target/nativeads/banners/NativeAppwallBanner;
.super Ljava/lang/Object;
.source "NativeAppwallBanner.java"


# instance fields
.field private final appInstalled:Z

.field private final bubbleIcon:Lcom/my/target/common/models/ImageData;

.field private final bubbleId:Ljava/lang/String;

.field private final coins:I

.field private final coinsIcon:Lcom/my/target/common/models/ImageData;

.field private final coinsIconBgColor:I

.field private final coinsIconTextColor:I

.field private final crossNotifIcon:Lcom/my/target/common/models/ImageData;

.field private final description:Ljava/lang/String;

.field private final gotoAppIcon:Lcom/my/target/common/models/ImageData;

.field private hasNotification:Z

.field private final icon:Lcom/my/target/common/models/ImageData;

.field private final id:Ljava/lang/String;

.field private final isBanner:Z

.field private final isItemHighlight:Z

.field private final isMain:Z

.field private final isRequireCategoryHighlight:Z

.field private final isRequireWifi:Z

.field private final isSubItem:Z

.field private final itemHighlightIcon:Lcom/my/target/common/models/ImageData;

.field private final labelIcon:Lcom/my/target/common/models/ImageData;

.field private final labelType:Ljava/lang/String;

.field private final mrgsId:I

.field private final paidType:Ljava/lang/String;

.field private final rating:F

.field private final status:Ljava/lang/String;

.field private final statusIcon:Lcom/my/target/common/models/ImageData;

.field private final title:Ljava/lang/String;

.field private final votes:I


# direct methods
.method private constructor <init>(Lcom/my/target/core/models/banners/i;)V
    .locals 1
    .param p1, "nativeBanner"    # Lcom/my/target/core/models/banners/i;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->id:Ljava/lang/String;

    .line 50
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->getDescription()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->description:Ljava/lang/String;

    .line 51
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->title:Ljava/lang/String;

    .line 52
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->getBubbleId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->bubbleId:Ljava/lang/String;

    .line 53
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->getLabelType()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->labelType:Ljava/lang/String;

    .line 54
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->getStatus()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->status:Ljava/lang/String;

    .line 55
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->getPaidType()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->paidType:Ljava/lang/String;

    .line 56
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->getMrgsId()I

    move-result v0

    iput v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->mrgsId:I

    .line 57
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->getCoins()I

    move-result v0

    iput v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->coins:I

    .line 58
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->getCoinsIconBgColor()I

    move-result v0

    iput v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->coinsIconBgColor:I

    .line 59
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->getCoinsIconTextColor()I

    move-result v0

    iput v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->coinsIconTextColor:I

    .line 60
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->getVotes()I

    move-result v0

    iput v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->votes:I

    .line 61
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->getRating()F

    move-result v0

    iput v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->rating:F

    .line 62
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->isHasNotification()Z

    move-result v0

    iput-boolean v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->hasNotification:Z

    .line 63
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->isMain()Z

    move-result v0

    iput-boolean v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->isMain:Z

    .line 64
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->isRequireCategoryHighlight()Z

    move-result v0

    iput-boolean v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->isRequireCategoryHighlight:Z

    .line 65
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->isItemHighlight()Z

    move-result v0

    iput-boolean v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->isItemHighlight:Z

    .line 66
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->isBanner()Z

    move-result v0

    iput-boolean v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->isBanner:Z

    .line 67
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->isRequireWifi()Z

    move-result v0

    iput-boolean v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->isRequireWifi:Z

    .line 68
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->isSubItem()Z

    move-result v0

    iput-boolean v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->isSubItem:Z

    .line 69
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->isAppInstalled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->appInstalled:Z

    .line 70
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->getIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->icon:Lcom/my/target/common/models/ImageData;

    .line 71
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->getCoinsIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->coinsIcon:Lcom/my/target/common/models/ImageData;

    .line 72
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->getLabelIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->labelIcon:Lcom/my/target/common/models/ImageData;

    .line 73
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->getGotoAppIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->gotoAppIcon:Lcom/my/target/common/models/ImageData;

    .line 74
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->getStatusIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->statusIcon:Lcom/my/target/common/models/ImageData;

    .line 75
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->getBubbleIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->bubbleIcon:Lcom/my/target/common/models/ImageData;

    .line 76
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->getItemHighlightIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->itemHighlightIcon:Lcom/my/target/common/models/ImageData;

    .line 77
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/i;->getCrossNotifIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->crossNotifIcon:Lcom/my/target/common/models/ImageData;

    .line 78
    return-void
.end method

.method public static newBanner(Lcom/my/target/core/models/banners/i;)Lcom/my/target/nativeads/banners/NativeAppwallBanner;
    .locals 1
    .param p0, "nativeBanner"    # Lcom/my/target/core/models/banners/i;

    .prologue
    .line 13
    new-instance v0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;

    invoke-direct {v0, p0}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;-><init>(Lcom/my/target/core/models/banners/i;)V

    return-object v0
.end method


# virtual methods
.method public getBubbleIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->bubbleIcon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public getBubbleId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->bubbleId:Ljava/lang/String;

    return-object v0
.end method

.method public getCoins()I
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->coins:I

    return v0
.end method

.method public getCoinsIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->coinsIcon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public getCoinsIconBgColor()I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->coinsIconBgColor:I

    return v0
.end method

.method public getCoinsIconTextColor()I
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->coinsIconTextColor:I

    return v0
.end method

.method public getCrossNotifIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->crossNotifIcon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getGotoAppIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->gotoAppIcon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public getIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->icon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getItemHighlightIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->itemHighlightIcon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public getLabelIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->labelIcon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public getLabelType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->labelType:Ljava/lang/String;

    return-object v0
.end method

.method public getMrgsId()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->mrgsId:I

    return v0
.end method

.method public getPaidType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->paidType:Ljava/lang/String;

    return-object v0
.end method

.method public getRating()F
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->rating:F

    return v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->status:Ljava/lang/String;

    return-object v0
.end method

.method public getStatusIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->statusIcon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getVotes()I
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->votes:I

    return v0
.end method

.method public isAppInstalled()Z
    .locals 1

    .prologue
    .line 182
    iget-boolean v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->appInstalled:Z

    return v0
.end method

.method public isBanner()Z
    .locals 1

    .prologue
    .line 167
    iget-boolean v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->isBanner:Z

    return v0
.end method

.method public isHasNotification()Z
    .locals 1

    .prologue
    .line 147
    iget-boolean v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->hasNotification:Z

    return v0
.end method

.method public isItemHighlight()Z
    .locals 1

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->isItemHighlight:Z

    return v0
.end method

.method public isMain()Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->isMain:Z

    return v0
.end method

.method public isRequireCategoryHighlight()Z
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->isRequireCategoryHighlight:Z

    return v0
.end method

.method public isRequireWifi()Z
    .locals 1

    .prologue
    .line 172
    iget-boolean v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->isRequireWifi:Z

    return v0
.end method

.method public isSubItem()Z
    .locals 1

    .prologue
    .line 177
    iget-boolean v0, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->isSubItem:Z

    return v0
.end method

.method public setHasNotification(Z)V
    .locals 0
    .param p1, "hasNotification"    # Z

    .prologue
    .line 227
    iput-boolean p1, p0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->hasNotification:Z

    .line 228
    return-void
.end method
