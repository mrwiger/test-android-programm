.class public final Lru/cn/tv/stb/StbViewModel$$Factory;
.super Ljava/lang/Object;
.source "StbViewModel$$Factory.java"

# interfaces
.implements Ltoothpick/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ltoothpick/Factory",
        "<",
        "Lru/cn/tv/stb/StbViewModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic createInstance(Ltoothpick/Scope;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0, p1}, Lru/cn/tv/stb/StbViewModel$$Factory;->createInstance(Ltoothpick/Scope;)Lru/cn/tv/stb/StbViewModel;

    move-result-object v0

    return-object v0
.end method

.method public createInstance(Ltoothpick/Scope;)Lru/cn/tv/stb/StbViewModel;
    .locals 5
    .param p1, "scope"    # Ltoothpick/Scope;

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lru/cn/tv/stb/StbViewModel$$Factory;->getTargetScope(Ltoothpick/Scope;)Ltoothpick/Scope;

    move-result-object p1

    .line 14
    const-class v4, Landroid/content/Context;

    invoke-interface {p1, v4}, Ltoothpick/Scope;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 15
    .local v0, "param1":Landroid/content/Context;
    const-class v4, Lru/cn/mvvm/RxLoader;

    invoke-interface {p1, v4}, Ltoothpick/Scope;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/mvvm/RxLoader;

    .line 16
    .local v1, "param2":Lru/cn/mvvm/RxLoader;
    const-class v4, Lru/cn/domain/tv/CurrentCategory;

    invoke-interface {p1, v4}, Ltoothpick/Scope;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/domain/tv/CurrentCategory;

    .line 17
    .local v2, "param3":Lru/cn/domain/tv/CurrentCategory;
    new-instance v3, Lru/cn/tv/stb/StbViewModel;

    invoke-direct {v3, v0, v1, v2}, Lru/cn/tv/stb/StbViewModel;-><init>(Landroid/content/Context;Lru/cn/mvvm/RxLoader;Lru/cn/domain/tv/CurrentCategory;)V

    .line 18
    .local v3, "stbViewModel":Lru/cn/tv/stb/StbViewModel;
    return-object v3
.end method

.method public getTargetScope(Ltoothpick/Scope;)Ltoothpick/Scope;
    .locals 0
    .param p1, "scope"    # Ltoothpick/Scope;

    .prologue
    .line 23
    return-object p1
.end method

.method public hasProvidesSingletonInScopeAnnotation()Z
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    return v0
.end method

.method public hasScopeAnnotation()Z
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    return v0
.end method
