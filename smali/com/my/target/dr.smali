.class public final Lcom/my/target/dr;
.super Lcom/my/target/e;
.source "InterstitialAdResultProcessor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/my/target/e",
        "<",
        "Lcom/my/target/dv;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/my/target/e;-><init>()V

    .line 43
    return-void
.end method

.method private static a(Ljava/util/List;II)Lcom/my/target/common/models/ImageData;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/common/models/ImageData;",
            ">;II)",
            "Lcom/my/target/common/models/ImageData;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 201
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 261
    :goto_0
    return-object v0

    .line 205
    :cond_0
    if-eqz p2, :cond_1

    if-nez p1, :cond_2

    .line 207
    :cond_1
    const-string v1, "[InterstitialAdResultProcessor] display size is zero"

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 211
    :cond_2
    int-to-float v1, p1

    int-to-float v2, p2

    div-float v5, v1, v2

    .line 212
    const/4 v1, 0x0

    .line 215
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v1

    move-object v1, v0

    :cond_3
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/common/models/ImageData;

    .line 217
    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getWidth()I

    move-result v3

    if-lez v3, :cond_3

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getHeight()I

    move-result v3

    if-lez v3, :cond_3

    .line 223
    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float v4, v3, v4

    .line 225
    cmpg-float v3, v5, v4

    if-gez v3, :cond_5

    .line 227
    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getWidth()I

    move-result v3

    int-to-float v3, v3

    .line 228
    int-to-float v7, p1

    cmpl-float v7, v3, v7

    if-lez v7, :cond_4

    .line 230
    int-to-float v3, p1

    .line 233
    :cond_4
    div-float v4, v3, v4

    move v8, v4

    move v4, v3

    move v3, v8

    .line 246
    :goto_2
    mul-float/2addr v3, v4

    .line 248
    cmpl-float v2, v3, v2

    if-lez v2, :cond_7

    move-object v1, v0

    move v2, v3

    .line 257
    goto :goto_1

    .line 237
    :cond_5
    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getHeight()I

    move-result v3

    int-to-float v3, v3

    .line 238
    int-to-float v7, p2

    cmpl-float v7, v3, v7

    if-lez v7, :cond_6

    .line 240
    int-to-float v3, p2

    .line 243
    :cond_6
    mul-float/2addr v4, v3

    goto :goto_2

    :cond_7
    move-object v0, v1

    .line 261
    goto :goto_0
.end method

.method public static e()Lcom/my/target/dr;
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/my/target/dr;

    invoke-direct {v0}, Lcom/my/target/dr;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Lcom/my/target/ak;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/ak;
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 33
    check-cast p1, Lcom/my/target/dv;

    .line 1050
    invoke-virtual {p1}, Lcom/my/target/dv;->j()Lcom/my/target/core/models/banners/d;

    move-result-object v0

    .line 1051
    if-eqz v0, :cond_15

    .line 1057
    instance-of v1, v0, Lcom/my/target/core/models/banners/h;

    if-eqz v1, :cond_a

    .line 1059
    check-cast v0, Lcom/my/target/core/models/banners/h;

    .line 1083
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1084
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/h;->getVideoBanner()Lcom/my/target/aj;

    move-result-object v1

    .line 1085
    if-eqz v1, :cond_1

    .line 1087
    invoke-virtual {v1}, Lcom/my/target/aj;->getPreview()Lcom/my/target/common/models/ImageData;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1089
    invoke-virtual {v1}, Lcom/my/target/aj;->getPreview()Lcom/my/target/common/models/ImageData;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1092
    :cond_0
    invoke-virtual {v1}, Lcom/my/target/aj;->getMediaData()Lcom/my/target/ag;

    move-result-object v1

    check-cast v1, Lcom/my/target/common/models/VideoData;

    .line 1093
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/my/target/common/models/VideoData;->isCacheable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1095
    invoke-static {}, Lcom/my/target/ay;->an()Lcom/my/target/ay;

    move-result-object v2

    invoke-virtual {v1}, Lcom/my/target/common/models/VideoData;->getUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6, p3}, Lcom/my/target/ay;->f(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1096
    invoke-virtual {v1, v2}, Lcom/my/target/common/models/VideoData;->setData(Ljava/lang/Object;)V

    .line 1100
    :cond_1
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/h;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1102
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/h;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1104
    :cond_2
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/h;->getIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1106
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/h;->getIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1109
    :cond_3
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/h;->getCloseIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1111
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/h;->getCloseIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1114
    :cond_4
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/h;->getPlayIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 1116
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/h;->getPlayIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1119
    :cond_5
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/h;->getStoreIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 1121
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/h;->getStoreIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1124
    :cond_6
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/h;->getInterstitialAdCards()Ljava/util/List;

    move-result-object v0

    .line 1125
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_8

    .line 1127
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_7
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/e;

    .line 1129
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/e;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 1130
    if-eqz v0, :cond_7

    .line 1132
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1137
    :cond_8
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_9

    .line 1139
    invoke-static {v5}, Lcom/my/target/ch;->a(Ljava/util/List;)Lcom/my/target/ch;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/my/target/ch;->v(Landroid/content/Context;)V

    .line 1074
    :cond_9
    :goto_1
    if-eqz v4, :cond_15

    .line 1076
    :goto_2
    return-object p1

    .line 1061
    :cond_a
    instance-of v1, v0, Lcom/my/target/core/models/banners/g;

    if-eqz v1, :cond_13

    .line 1063
    check-cast v0, Lcom/my/target/core/models/banners/g;

    .line 1145
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1266
    const-string v1, "window"

    invoke-virtual {p3, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 1268
    if-eqz v1, :cond_16

    .line 1270
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 1273
    :goto_3
    new-instance v6, Landroid/graphics/Point;

    invoke-direct {v6}, Landroid/graphics/Point;-><init>()V

    .line 1275
    if-eqz v1, :cond_b

    .line 1277
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x11

    if-lt v7, v8, :cond_10

    .line 1279
    invoke-virtual {v1, v6}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 1148
    :cond_b
    :goto_4
    iget v1, v6, Landroid/graphics/Point;->x:I

    .line 1149
    iget v6, v6, Landroid/graphics/Point;->y:I

    .line 1151
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/g;->getPortraitImages()Ljava/util/List;

    move-result-object v7

    invoke-static {v1, v6}, Ljava/lang/Math;->min(II)I

    move-result v8

    invoke-static {v1, v6}, Ljava/lang/Math;->max(II)I

    move-result v9

    invoke-static {v7, v8, v9}, Lcom/my/target/dr;->a(Ljava/util/List;II)Lcom/my/target/common/models/ImageData;

    move-result-object v7

    .line 1152
    if-eqz v7, :cond_c

    .line 1154
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1155
    invoke-virtual {v0, v7}, Lcom/my/target/core/models/banners/g;->setOptimalPortraitImage(Lcom/my/target/common/models/ImageData;)V

    .line 1158
    :cond_c
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/g;->getLandscapeImages()Ljava/util/List;

    move-result-object v8

    invoke-static {v1, v6}, Ljava/lang/Math;->max(II)I

    move-result v9

    invoke-static {v1, v6}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v8, v9, v1}, Lcom/my/target/dr;->a(Ljava/util/List;II)Lcom/my/target/common/models/ImageData;

    move-result-object v1

    .line 1159
    if-eqz v1, :cond_d

    .line 1161
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1162
    invoke-virtual {v0, v1}, Lcom/my/target/core/models/banners/g;->setOptimalLandscapeImage(Lcom/my/target/common/models/ImageData;)V

    .line 1165
    :cond_d
    if-nez v7, :cond_e

    if-eqz v1, :cond_f

    .line 1167
    :cond_e
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/g;->getCloseIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 1168
    if-eqz v0, :cond_f

    .line 1170
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1174
    :cond_f
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_12

    .line 1176
    invoke-static {v5}, Lcom/my/target/ch;->a(Ljava/util/List;)Lcom/my/target/ch;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/my/target/ch;->v(Landroid/content/Context;)V

    .line 1177
    if-eqz v7, :cond_11

    invoke-virtual {v7}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_11

    move v0, v4

    :goto_5
    move v4, v0

    .line 1063
    goto/16 :goto_1

    .line 1283
    :cond_10
    invoke-virtual {v1, v6}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    goto :goto_4

    .line 1181
    :cond_11
    if-eqz v1, :cond_12

    invoke-virtual {v1}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_12

    move v0, v4

    .line 1183
    goto :goto_5

    :cond_12
    move v0, v2

    .line 1186
    goto :goto_5

    .line 1065
    :cond_13
    instance-of v1, v0, Lcom/my/target/core/models/banners/f;

    if-eqz v1, :cond_14

    .line 1067
    check-cast v0, Lcom/my/target/core/models/banners/f;

    .line 2191
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/f;->getCloseIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 2193
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/f;->getCloseIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/ch;->a(Lcom/my/target/common/models/ImageData;)Lcom/my/target/ch;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/my/target/ch;->v(Landroid/content/Context;)V

    goto/16 :goto_1

    :cond_14
    move v4, v2

    .line 1071
    goto/16 :goto_1

    :cond_15
    move-object p1, v3

    .line 33
    goto/16 :goto_2

    :cond_16
    move-object v1, v3

    goto/16 :goto_3
.end method
