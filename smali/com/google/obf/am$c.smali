.class final Lcom/google/obf/am$c;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/am;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "c"
.end annotation


# instance fields
.field private final a:[Lcom/google/obf/aj;

.field private final b:Lcom/google/obf/al;

.field private c:Lcom/google/obf/aj;


# direct methods
.method public constructor <init>([Lcom/google/obf/aj;Lcom/google/obf/al;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/obf/am$c;->a:[Lcom/google/obf/aj;

    .line 3
    iput-object p2, p0, Lcom/google/obf/am$c;->b:Lcom/google/obf/al;

    .line 4
    return-void
.end method


# virtual methods
.method public a(Lcom/google/obf/ak;)Lcom/google/obf/aj;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/am$e;,
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/obf/am$c;->c:Lcom/google/obf/aj;

    if-eqz v0, :cond_0

    .line 6
    iget-object v0, p0, Lcom/google/obf/am$c;->c:Lcom/google/obf/aj;

    .line 20
    :goto_0
    return-object v0

    .line 7
    :cond_0
    iget-object v1, p0, Lcom/google/obf/am$c;->a:[Lcom/google/obf/aj;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 8
    :try_start_0
    invoke-interface {v3, p1}, Lcom/google/obf/aj;->a(Lcom/google/obf/ak;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 9
    iput-object v3, p0, Lcom/google/obf/am$c;->c:Lcom/google/obf/aj;
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10
    invoke-interface {p1}, Lcom/google/obf/ak;->a()V

    .line 17
    :cond_1
    iget-object v0, p0, Lcom/google/obf/am$c;->c:Lcom/google/obf/aj;

    if-nez v0, :cond_3

    .line 18
    new-instance v0, Lcom/google/obf/am$e;

    iget-object v1, p0, Lcom/google/obf/am$c;->a:[Lcom/google/obf/aj;

    invoke-direct {v0, v1}, Lcom/google/obf/am$e;-><init>([Lcom/google/obf/aj;)V

    throw v0

    .line 10
    :cond_2
    invoke-interface {p1}, Lcom/google/obf/ak;->a()V

    .line 16
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 12
    :catch_0
    move-exception v3

    .line 13
    invoke-interface {p1}, Lcom/google/obf/ak;->a()V

    goto :goto_2

    .line 15
    :catchall_0
    move-exception v0

    invoke-interface {p1}, Lcom/google/obf/ak;->a()V

    throw v0

    .line 19
    :cond_3
    iget-object v0, p0, Lcom/google/obf/am$c;->c:Lcom/google/obf/aj;

    iget-object v1, p0, Lcom/google/obf/am$c;->b:Lcom/google/obf/al;

    invoke-interface {v0, v1}, Lcom/google/obf/aj;->a(Lcom/google/obf/al;)V

    .line 20
    iget-object v0, p0, Lcom/google/obf/am$c;->c:Lcom/google/obf/aj;

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/obf/am$c;->c:Lcom/google/obf/aj;

    if-eqz v0, :cond_0

    .line 22
    iget-object v0, p0, Lcom/google/obf/am$c;->c:Lcom/google/obf/aj;

    invoke-interface {v0}, Lcom/google/obf/aj;->c()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/am$c;->c:Lcom/google/obf/aj;

    .line 24
    :cond_0
    return-void
.end method
