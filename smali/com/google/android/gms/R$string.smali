.class public final Lcom/google/android/gms/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final cast_ad_label:I = 0x7f0e003a

.field public static final cast_casting_to_device:I = 0x7f0e003b

.field public static final cast_closed_captions:I = 0x7f0e003c

.field public static final cast_closed_captions_unavailable:I = 0x7f0e003d

.field public static final cast_disconnect:I = 0x7f0e003e

.field public static final cast_expanded_controller_ad_image_description:I = 0x7f0e003f

.field public static final cast_expanded_controller_ad_in_progress:I = 0x7f0e0040

.field public static final cast_expanded_controller_background_image:I = 0x7f0e0041

.field public static final cast_expanded_controller_live_stream_indicator:I = 0x7f0e0042

.field public static final cast_expanded_controller_loading:I = 0x7f0e0043

.field public static final cast_expanded_controller_skip_ad_label:I = 0x7f0e0044

.field public static final cast_forward:I = 0x7f0e0045

.field public static final cast_forward_10:I = 0x7f0e0046

.field public static final cast_forward_30:I = 0x7f0e0047

.field public static final cast_intro_overlay_button_text:I = 0x7f0e0048

.field public static final cast_invalid_stream_duration_text:I = 0x7f0e0049

.field public static final cast_invalid_stream_position_text:I = 0x7f0e004a

.field public static final cast_mute:I = 0x7f0e004b

.field public static final cast_notification_connected_message:I = 0x7f0e004c

.field public static final cast_notification_connecting_message:I = 0x7f0e004d

.field public static final cast_notification_disconnect:I = 0x7f0e004e

.field public static final cast_pause:I = 0x7f0e004f

.field public static final cast_play:I = 0x7f0e0050

.field public static final cast_rewind:I = 0x7f0e0051

.field public static final cast_rewind_10:I = 0x7f0e0052

.field public static final cast_rewind_30:I = 0x7f0e0053

.field public static final cast_seek_bar:I = 0x7f0e0054

.field public static final cast_skip_next:I = 0x7f0e0055

.field public static final cast_skip_prev:I = 0x7f0e0056

.field public static final cast_stop:I = 0x7f0e0057

.field public static final cast_stop_live_stream:I = 0x7f0e0058

.field public static final cast_tracks_chooser_dialog_audio:I = 0x7f0e0059

.field public static final cast_tracks_chooser_dialog_cancel:I = 0x7f0e005a

.field public static final cast_tracks_chooser_dialog_closed_captions:I = 0x7f0e005b

.field public static final cast_tracks_chooser_dialog_default_track_name:I = 0x7f0e005c

.field public static final cast_tracks_chooser_dialog_none:I = 0x7f0e005d

.field public static final cast_tracks_chooser_dialog_ok:I = 0x7f0e005e

.field public static final cast_tracks_chooser_dialog_subtitles:I = 0x7f0e005f

.field public static final cast_unmute:I = 0x7f0e0060

.field public static final common_google_play_services_enable_button:I = 0x7f0e0076

.field public static final common_google_play_services_enable_text:I = 0x7f0e0077

.field public static final common_google_play_services_enable_title:I = 0x7f0e0078

.field public static final common_google_play_services_install_button:I = 0x7f0e0079

.field public static final common_google_play_services_install_text:I = 0x7f0e007a

.field public static final common_google_play_services_install_title:I = 0x7f0e007b

.field public static final common_google_play_services_notification_ticker:I = 0x7f0e007c

.field public static final common_google_play_services_unknown_issue:I = 0x7f0e007d

.field public static final common_google_play_services_unsupported_text:I = 0x7f0e007e

.field public static final common_google_play_services_update_button:I = 0x7f0e007f

.field public static final common_google_play_services_update_text:I = 0x7f0e0080

.field public static final common_google_play_services_update_title:I = 0x7f0e0081

.field public static final common_google_play_services_updating_text:I = 0x7f0e0082

.field public static final common_google_play_services_wear_update_text:I = 0x7f0e0083

.field public static final common_open_on_phone:I = 0x7f0e0084

.field public static final common_signin_button_text:I = 0x7f0e0085

.field public static final common_signin_button_text_long:I = 0x7f0e0086

.field public static final s1:I = 0x7f0e0123

.field public static final s2:I = 0x7f0e0124

.field public static final s3:I = 0x7f0e0125

.field public static final s4:I = 0x7f0e0126

.field public static final s5:I = 0x7f0e0127

.field public static final s6:I = 0x7f0e0128
