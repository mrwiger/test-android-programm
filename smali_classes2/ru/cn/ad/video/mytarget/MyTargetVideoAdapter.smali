.class public Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;
.super Lru/cn/ad/video/VideoAdAdapter;
.source "MyTargetVideoAdapter.java"

# interfaces
.implements Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;
.implements Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$PresenterListener;
.implements Lru/cn/ad/video/mytarget/Tracker;


# instance fields
.field private instreamAd:Lcom/my/target/instreamads/InstreamAd;

.field private presenter:Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;

.field private final slotId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "system"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lru/cn/ad/video/VideoAdAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    .line 30
    iget-object v1, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->adSystem:Lru/cn/api/money_miner/replies/AdSystem;

    const-string v2, "slot_id"

    invoke-virtual {v1, v2}, Lru/cn/api/money_miner/replies/AdSystem;->getParamOrThrow(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 31
    .local v0, "slotParam":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->slotId:I

    .line 32
    return-void
.end method

.method private trackError(I)V
    .locals 3
    .param p1, "errorCode"    # I

    .prologue
    .line 150
    const-string v0, "MyTargetVideo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Track error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    sget-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->ADV_ERROR_PLAY:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v1, "MyTargetError"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, p1, v2}, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->reportError(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/util/Map;)V

    .line 154
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->instreamAd:Lcom/my/target/instreamads/InstreamAd;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->instreamAd:Lcom/my/target/instreamads/InstreamAd;

    invoke-virtual {v0}, Lcom/my/target/instreamads/InstreamAd;->destroy()V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->instreamAd:Lcom/my/target/instreamads/InstreamAd;

    .line 59
    :cond_0
    invoke-virtual {p0}, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->stop()V

    .line 60
    return-void
.end method

.method public onBannerComplete(Lcom/my/target/instreamads/InstreamAd;Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;)V
    .locals 1
    .param p1, "instreamAd"    # Lcom/my/target/instreamads/InstreamAd;
    .param p2, "instreamAdBanner"    # Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;

    .prologue
    .line 115
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_COMPLETE:Lru/cn/domain/statistics/inetra/AdvEvent;

    invoke-virtual {p0, v0}, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->trackEvent(Lru/cn/domain/statistics/inetra/AdvEvent;)V

    .line 118
    invoke-virtual {p0}, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->destroy()V

    .line 120
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdEnded()V

    .line 122
    :cond_0
    return-void
.end method

.method public onBannerStart(Lcom/my/target/instreamads/InstreamAd;Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;)V
    .locals 1
    .param p1, "instreamAd"    # Lcom/my/target/instreamads/InstreamAd;
    .param p2, "instreamAdBanner"    # Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;

    .prologue
    .line 107
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->presenter:Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;

    invoke-virtual {v0, p2}, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->setBanner(Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;)V

    .line 109
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdStarted()V

    .line 111
    :cond_0
    return-void
.end method

.method public onBannerTimeLeftChange(FFLcom/my/target/instreamads/InstreamAd;)V
    .locals 0
    .param p1, "v"    # F
    .param p2, "v1"    # F
    .param p3, "instreamAd"    # Lcom/my/target/instreamads/InstreamAd;

    .prologue
    .line 126
    return-void
.end method

.method public onComplete()V
    .locals 1

    .prologue
    .line 135
    invoke-virtual {p0}, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->destroy()V

    .line 137
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdEnded()V

    .line 139
    :cond_0
    return-void
.end method

.method public onComplete(Ljava/lang/String;Lcom/my/target/instreamads/InstreamAd;)V
    .locals 0
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "instreamAd"    # Lcom/my/target/instreamads/InstreamAd;

    .prologue
    .line 130
    return-void
.end method

.method public onError(Ljava/lang/String;Lcom/my/target/instreamads/InstreamAd;)V
    .locals 3
    .param p1, "code"    # Ljava/lang/String;
    .param p2, "instreamAd"    # Lcom/my/target/instreamads/InstreamAd;

    .prologue
    .line 90
    const/4 v1, 0x0

    .line 92
    .local v1, "errorCode":I
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 96
    :goto_0
    invoke-direct {p0, v1}, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->trackError(I)V

    .line 99
    invoke-virtual {p0}, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->destroy()V

    .line 101
    iget-object v2, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    if-eqz v2, :cond_0

    .line 102
    iget-object v2, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    invoke-interface {v2}, Lru/cn/ad/AdAdapter$Listener;->onAdEnded()V

    .line 103
    :cond_0
    return-void

    .line 93
    :catch_0
    move-exception v0

    .line 94
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected onLoad()V
    .locals 3

    .prologue
    .line 36
    new-instance v0, Lcom/my/target/instreamads/InstreamAd;

    iget v1, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->slotId:I

    iget-object v2, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->context:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/my/target/instreamads/InstreamAd;-><init>(ILandroid/content/Context;)V

    iput-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->instreamAd:Lcom/my/target/instreamads/InstreamAd;

    .line 37
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->instreamAd:Lcom/my/target/instreamads/InstreamAd;

    invoke-virtual {v0, p0}, Lcom/my/target/instreamads/InstreamAd;->setListener(Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;)V

    .line 38
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->instreamAd:Lcom/my/target/instreamads/InstreamAd;

    invoke-virtual {v0}, Lcom/my/target/instreamads/InstreamAd;->load()V

    .line 39
    return-void
.end method

.method public onLoad(Lcom/my/target/instreamads/InstreamAd;)V
    .locals 1
    .param p1, "instreamAd"    # Lcom/my/target/instreamads/InstreamAd;

    .prologue
    .line 78
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdLoaded()V

    .line 80
    :cond_0
    return-void
.end method

.method public onNoAd(Ljava/lang/String;Lcom/my/target/instreamads/InstreamAd;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "instreamAd"    # Lcom/my/target/instreamads/InstreamAd;

    .prologue
    .line 84
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onError()V

    .line 86
    :cond_0
    return-void
.end method

.method public show()V
    .locals 3

    .prologue
    .line 43
    iget-object v1, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->presenter:Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;

    if-nez v1, :cond_0

    .line 44
    invoke-virtual {p0}, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->getRenderingSettings()Lru/cn/ad/video/RenderingSettings;

    move-result-object v0

    .line 45
    .local v0, "renderingSettings":Lru/cn/ad/video/RenderingSettings;
    new-instance v1, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;

    iget-object v2, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->instreamAd:Lcom/my/target/instreamads/InstreamAd;

    invoke-direct {v1, v0, v2, p0}, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;-><init>(Lru/cn/ad/video/RenderingSettings;Lcom/my/target/instreamads/InstreamAd;Lru/cn/ad/video/mytarget/Tracker;)V

    iput-object v1, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->presenter:Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;

    .line 46
    iget-object v1, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->presenter:Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;

    invoke-virtual {v1, p0}, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->setListener(Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$PresenterListener;)V

    .line 49
    .end local v0    # "renderingSettings":Lru/cn/ad/video/RenderingSettings;
    :cond_0
    iget-object v1, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->presenter:Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;

    invoke-virtual {v1}, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->play()V

    .line 50
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 64
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->presenter:Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->presenter:Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;

    invoke-virtual {v0, v1}, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->setListener(Lru/cn/ad/video/mytarget/MyTargetVideoPresenter$PresenterListener;)V

    .line 66
    iget-object v0, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->presenter:Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;

    invoke-virtual {v0}, Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;->destroy()V

    .line 67
    iput-object v1, p0, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->presenter:Lru/cn/ad/video/mytarget/MyTargetVideoPresenter;

    .line 69
    :cond_0
    return-void
.end method

.method public trackEvent(Lru/cn/domain/statistics/inetra/AdvEvent;)V
    .locals 3
    .param p1, "event"    # Lru/cn/domain/statistics/inetra/AdvEvent;

    .prologue
    .line 143
    const-string v0, "MyTargetVideo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Track event "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lru/cn/ad/video/mytarget/MyTargetVideoAdapter;->reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/util/Map;)V

    .line 147
    return-void
.end method
