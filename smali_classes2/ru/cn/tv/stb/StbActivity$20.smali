.class Lru/cn/tv/stb/StbActivity$20;
.super Ljava/lang/Object;
.source "StbActivity.java"

# interfaces
.implements Lru/cn/player/ITrackSelector$TrackNameGenerator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/stb/StbActivity;->getSubtitlesTrackNameGenerator()Lru/cn/player/ITrackSelector$TrackNameGenerator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field count:I

.field final synthetic this$0:Lru/cn/tv/stb/StbActivity;


# direct methods
.method constructor <init>(Lru/cn/tv/stb/StbActivity;)V
    .locals 1
    .param p1, "this$0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 1547
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity$20;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1548
    const/4 v0, 0x1

    iput v0, p0, Lru/cn/tv/stb/StbActivity$20;->count:I

    return-void
.end method


# virtual methods
.method public getName(Lru/cn/player/TrackInfo;)Ljava/lang/String;
    .locals 6
    .param p1, "trackInfo"    # Lru/cn/player/TrackInfo;

    .prologue
    .line 1552
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lru/cn/tv/stb/StbActivity$20;->this$0:Lru/cn/tv/stb/StbActivity;

    const v5, 0x7f0e0162

    invoke-virtual {v4, v5}, Lru/cn/tv/stb/StbActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lru/cn/tv/stb/StbActivity$20;->count:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1553
    .local v1, "title":Ljava/lang/String;
    iget v3, p0, Lru/cn/tv/stb/StbActivity$20;->count:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lru/cn/tv/stb/StbActivity$20;->count:I

    .line 1554
    if-nez p1, :cond_0

    move-object v2, v1

    .line 1561
    .end local v1    # "title":Ljava/lang/String;
    .local v2, "title":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 1557
    .end local v2    # "title":Ljava/lang/String;
    .restart local v1    # "title":Ljava/lang/String;
    :cond_0
    iget-object v3, p1, Lru/cn/player/TrackInfo;->language:Ljava/lang/String;

    const-string v4, "und"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1558
    .local v0, "lang":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1559
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_1
    move-object v2, v1

    .line 1561
    .end local v1    # "title":Ljava/lang/String;
    .restart local v2    # "title":Ljava/lang/String;
    goto :goto_0
.end method
