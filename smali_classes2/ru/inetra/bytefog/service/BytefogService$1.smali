.class Lru/inetra/bytefog/service/BytefogService$1;
.super Lru/inetra/bytefog/service/IService$Stub;
.source "BytefogService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/inetra/bytefog/service/BytefogService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/inetra/bytefog/service/BytefogService;


# direct methods
.method constructor <init>(Lru/inetra/bytefog/service/BytefogService;)V
    .locals 0
    .param p1, "this$0"    # Lru/inetra/bytefog/service/BytefogService;

    .prologue
    .line 54
    iput-object p1, p0, Lru/inetra/bytefog/service/BytefogService$1;->this$0:Lru/inetra/bytefog/service/BytefogService;

    invoke-direct {p0}, Lru/inetra/bytefog/service/IService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelAllRequests()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 73
    :try_start_0
    iget-object v1, p0, Lru/inetra/bytefog/service/BytefogService$1;->this$0:Lru/inetra/bytefog/service/BytefogService;

    invoke-static {v1}, Lru/inetra/bytefog/service/BytefogService;->access$100(Lru/inetra/bytefog/service/BytefogService;)Lru/inetra/bytefog/service/BytefogLibrary;

    move-result-object v1

    invoke-virtual {v1}, Lru/inetra/bytefog/service/BytefogLibrary;->cancelAllRequests()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    return-void

    .line 74
    :catch_0
    move-exception v0

    .line 75
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "BytefogService"

    const-string v2, "cancelAllRequests causes error"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 76
    new-instance v1, Landroid/os/RemoteException;

    invoke-direct {v1}, Landroid/os/RemoteException;-><init>()V

    throw v1
.end method

.method public convertBytefogMagnetToHlsPlaylistUri(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "magnet"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 63
    :try_start_0
    iget-object v1, p0, Lru/inetra/bytefog/service/BytefogService$1;->this$0:Lru/inetra/bytefog/service/BytefogService;

    invoke-static {v1}, Lru/inetra/bytefog/service/BytefogService;->access$100(Lru/inetra/bytefog/service/BytefogService;)Lru/inetra/bytefog/service/BytefogLibrary;

    move-result-object v1

    invoke-virtual {v1, p1}, Lru/inetra/bytefog/service/BytefogLibrary;->convertBytefogMagnetToHlsPlaylistUri(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 64
    :catch_0
    move-exception v0

    .line 65
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "BytefogService"

    const-string v2, "Magnet conversion error"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 66
    new-instance v1, Landroid/os/RemoteException;

    invoke-direct {v1}, Landroid/os/RemoteException;-><init>()V

    throw v1
.end method

.method public setListener(Lru/inetra/bytefog/service/IServiceListener;)V
    .locals 1
    .param p1, "listener"    # Lru/inetra/bytefog/service/IServiceListener;

    .prologue
    .line 57
    iget-object v0, p0, Lru/inetra/bytefog/service/BytefogService$1;->this$0:Lru/inetra/bytefog/service/BytefogService;

    invoke-static {v0, p1}, Lru/inetra/bytefog/service/BytefogService;->access$002(Lru/inetra/bytefog/service/BytefogService;Lru/inetra/bytefog/service/IServiceListener;)Lru/inetra/bytefog/service/IServiceListener;

    .line 58
    return-void
.end method
