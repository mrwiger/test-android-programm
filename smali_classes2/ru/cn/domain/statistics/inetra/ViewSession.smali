.class final Lru/cn/domain/statistics/inetra/ViewSession;
.super Ljava/lang/Object;
.source "ViewSession.java"


# instance fields
.field bitrate:I

.field final collectionId:J

.field contentUri:Ljava/lang/String;

.field contractorId:J

.field currentProtocol:Ljava/lang/Integer;

.field locationTitle:Ljava/lang/String;

.field playerType:I

.field private playingStartTimeMs:J

.field sourceUri:Ljava/lang/String;

.field startMode:Ljava/lang/Integer;

.field final viewFrom:Ljava/lang/Integer;

.field final viewMode:I

.field private watchedTimeMs:J


# direct methods
.method private constructor <init>()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 23
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, Lru/cn/domain/statistics/inetra/ViewSession;-><init>(ILjava/lang/Integer;Ljava/lang/Integer;J)V

    .line 24
    return-void
.end method

.method constructor <init>(ILjava/lang/Integer;Ljava/lang/Integer;J)V
    .locals 2
    .param p1, "viewMode"    # I
    .param p2, "viewFrom"    # Ljava/lang/Integer;
    .param p3, "startMode"    # Ljava/lang/Integer;
    .param p4, "collectionId"    # J

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lru/cn/domain/statistics/inetra/ViewSession;->currentProtocol:Ljava/lang/Integer;

    .line 27
    iput p1, p0, Lru/cn/domain/statistics/inetra/ViewSession;->viewMode:I

    .line 28
    iput-object p3, p0, Lru/cn/domain/statistics/inetra/ViewSession;->startMode:Ljava/lang/Integer;

    .line 29
    iput-object p2, p0, Lru/cn/domain/statistics/inetra/ViewSession;->viewFrom:Ljava/lang/Integer;

    .line 30
    iput-wide p4, p0, Lru/cn/domain/statistics/inetra/ViewSession;->collectionId:J

    .line 31
    return-void
.end method


# virtual methods
.method getWatchedTime()J
    .locals 4

    .prologue
    .line 54
    iget-wide v0, p0, Lru/cn/domain/statistics/inetra/ViewSession;->watchedTimeMs:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0
.end method

.method pauseCounting()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 41
    iget-wide v2, p0, Lru/cn/domain/statistics/inetra/ViewSession;->playingStartTimeMs:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_0

    .line 42
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lru/cn/domain/statistics/inetra/ViewSession;->playingStartTimeMs:J

    sub-long v0, v2, v4

    .line 43
    .local v0, "timeElapsed":J
    iget-wide v2, p0, Lru/cn/domain/statistics/inetra/ViewSession;->watchedTimeMs:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lru/cn/domain/statistics/inetra/ViewSession;->watchedTimeMs:J

    .line 45
    iput-wide v6, p0, Lru/cn/domain/statistics/inetra/ViewSession;->playingStartTimeMs:J

    .line 47
    .end local v0    # "timeElapsed":J
    :cond_0
    return-void
.end method

.method reset()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 58
    iput-wide v0, p0, Lru/cn/domain/statistics/inetra/ViewSession;->watchedTimeMs:J

    .line 59
    iput-wide v0, p0, Lru/cn/domain/statistics/inetra/ViewSession;->playingStartTimeMs:J

    .line 60
    return-void
.end method

.method startCounting()V
    .locals 4

    .prologue
    .line 34
    iget-wide v0, p0, Lru/cn/domain/statistics/inetra/ViewSession;->playingStartTimeMs:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 38
    :goto_0
    return-void

    .line 37
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lru/cn/domain/statistics/inetra/ViewSession;->playingStartTimeMs:J

    goto :goto_0
.end method
