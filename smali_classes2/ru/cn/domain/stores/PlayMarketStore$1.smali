.class Lru/cn/domain/stores/PlayMarketStore$1;
.super Ljava/lang/Object;
.source "PlayMarketStore.java"

# interfaces
.implements Lru/cn/domain/stores/iabhelper/IabHelper$OnIabSetupFinishedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/domain/stores/PlayMarketStore;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/domain/stores/PlayMarketStore;

.field final synthetic val$inventoryQuery:Lru/cn/domain/stores/iabhelper/IabHelper;


# direct methods
.method constructor <init>(Lru/cn/domain/stores/PlayMarketStore;Lru/cn/domain/stores/iabhelper/IabHelper;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/domain/stores/PlayMarketStore;

    .prologue
    .line 29
    iput-object p1, p0, Lru/cn/domain/stores/PlayMarketStore$1;->this$0:Lru/cn/domain/stores/PlayMarketStore;

    iput-object p2, p0, Lru/cn/domain/stores/PlayMarketStore$1;->val$inventoryQuery:Lru/cn/domain/stores/iabhelper/IabHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onIabSetupFinished(Lru/cn/domain/stores/iabhelper/IabResult;)V
    .locals 3
    .param p1, "result"    # Lru/cn/domain/stores/iabhelper/IabResult;

    .prologue
    .line 31
    invoke-virtual {p1}, Lru/cn/domain/stores/iabhelper/IabResult;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lru/cn/domain/stores/PlayMarketStore$1;->this$0:Lru/cn/domain/stores/PlayMarketStore;

    sget-object v1, Lru/cn/domain/stores/PurchaseStore$Status;->AVAILABLE:Lru/cn/domain/stores/PurchaseStore$Status;

    iput-object v1, v0, Lru/cn/domain/stores/PlayMarketStore;->status:Lru/cn/domain/stores/PurchaseStore$Status;

    .line 33
    iget-object v0, p0, Lru/cn/domain/stores/PlayMarketStore$1;->this$0:Lru/cn/domain/stores/PlayMarketStore;

    iget-object v1, p0, Lru/cn/domain/stores/PlayMarketStore$1;->val$inventoryQuery:Lru/cn/domain/stores/iabhelper/IabHelper;

    invoke-static {v0, v1}, Lru/cn/domain/stores/PlayMarketStore;->access$000(Lru/cn/domain/stores/PlayMarketStore;Lru/cn/domain/stores/iabhelper/IabHelper;)V

    .line 42
    :goto_0
    return-void

    .line 36
    :cond_0
    invoke-virtual {p1}, Lru/cn/domain/stores/iabhelper/IabResult;->getResponse()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 37
    iget-object v0, p0, Lru/cn/domain/stores/PlayMarketStore$1;->this$0:Lru/cn/domain/stores/PlayMarketStore;

    sget-object v1, Lru/cn/domain/stores/PurchaseStore$Status;->NOT_AVAILABLE:Lru/cn/domain/stores/PurchaseStore$Status;

    iput-object v1, v0, Lru/cn/domain/stores/PlayMarketStore;->status:Lru/cn/domain/stores/PurchaseStore$Status;

    .line 40
    :cond_1
    iget-object v0, p0, Lru/cn/domain/stores/PlayMarketStore$1;->this$0:Lru/cn/domain/stores/PlayMarketStore;

    iget-object v0, v0, Lru/cn/domain/stores/PlayMarketStore;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Problem setting up In-app Billing: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
