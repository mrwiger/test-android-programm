.class final Lcom/google/obf/cu$b;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/cu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation


# instance fields
.field public A:F

.field public B:F

.field public C:F

.field public D:F

.field public E:F

.field public F:I

.field public G:I

.field public H:I

.field public I:J

.field public J:J

.field public K:Lcom/google/obf/ar;

.field public L:I

.field private M:Ljava/lang/String;

.field public a:Ljava/lang/String;

.field public b:I

.field public c:I

.field public d:I

.field public e:Z

.field public f:[B

.field public g:[B

.field public h:[B

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:[B

.field public o:I

.field public p:Z

.field public q:I

.field public r:I

.field public s:I

.field public t:I

.field public u:I

.field public v:F

.field public w:F

.field public x:F

.field public y:F

.field public z:F


# direct methods
.method private constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/high16 v1, -0x40800000    # -1.0f

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput v2, p0, Lcom/google/obf/cu$b;->i:I

    .line 3
    iput v2, p0, Lcom/google/obf/cu$b;->j:I

    .line 4
    iput v2, p0, Lcom/google/obf/cu$b;->k:I

    .line 5
    iput v2, p0, Lcom/google/obf/cu$b;->l:I

    .line 6
    iput v3, p0, Lcom/google/obf/cu$b;->m:I

    .line 7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/cu$b;->n:[B

    .line 8
    iput v2, p0, Lcom/google/obf/cu$b;->o:I

    .line 9
    iput-boolean v3, p0, Lcom/google/obf/cu$b;->p:Z

    .line 10
    iput v2, p0, Lcom/google/obf/cu$b;->q:I

    .line 11
    iput v2, p0, Lcom/google/obf/cu$b;->r:I

    .line 12
    iput v2, p0, Lcom/google/obf/cu$b;->s:I

    .line 13
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/google/obf/cu$b;->t:I

    .line 14
    const/16 v0, 0xc8

    iput v0, p0, Lcom/google/obf/cu$b;->u:I

    .line 15
    iput v1, p0, Lcom/google/obf/cu$b;->v:F

    .line 16
    iput v1, p0, Lcom/google/obf/cu$b;->w:F

    .line 17
    iput v1, p0, Lcom/google/obf/cu$b;->x:F

    .line 18
    iput v1, p0, Lcom/google/obf/cu$b;->y:F

    .line 19
    iput v1, p0, Lcom/google/obf/cu$b;->z:F

    .line 20
    iput v1, p0, Lcom/google/obf/cu$b;->A:F

    .line 21
    iput v1, p0, Lcom/google/obf/cu$b;->B:F

    .line 22
    iput v1, p0, Lcom/google/obf/cu$b;->C:F

    .line 23
    iput v1, p0, Lcom/google/obf/cu$b;->D:F

    .line 24
    iput v1, p0, Lcom/google/obf/cu$b;->E:F

    .line 25
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/obf/cu$b;->F:I

    .line 26
    iput v2, p0, Lcom/google/obf/cu$b;->G:I

    .line 27
    const/16 v0, 0x1f40

    iput v0, p0, Lcom/google/obf/cu$b;->H:I

    .line 28
    iput-wide v4, p0, Lcom/google/obf/cu$b;->I:J

    .line 29
    iput-wide v4, p0, Lcom/google/obf/cu$b;->J:J

    .line 30
    const-string v0, "eng"

    iput-object v0, p0, Lcom/google/obf/cu$b;->M:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/obf/cu$1;)V
    .locals 0

    .prologue
    .line 255
    invoke-direct {p0}, Lcom/google/obf/cu$b;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/obf/cu$b;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 256
    iput-object p1, p0, Lcom/google/obf/cu$b;->M:Ljava/lang/String;

    return-object p1
.end method

.method private static a(Lcom/google/obf/dw;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/dw;",
            ")",
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    .line 150
    const/16 v0, 0x10

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->d(I)V

    .line 151
    invoke-virtual {p0}, Lcom/google/obf/dw;->l()J

    move-result-wide v0

    .line 152
    const-wide/32 v2, 0x31435657

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 153
    new-instance v2, Lcom/google/obf/s;

    const/16 v3, 0x39

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unsupported FourCC compression type: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    :catch_0
    move-exception v0

    .line 163
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Error parsing FourCC VC1 codec private"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 154
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/google/obf/dw;->d()I

    move-result v0

    add-int/lit8 v0, v0, 0x14

    .line 155
    iget-object v1, p0, Lcom/google/obf/dw;->a:[B

    .line 156
    :goto_0
    array-length v2, v1

    add-int/lit8 v2, v2, -0x4

    if-ge v0, v2, :cond_2

    .line 157
    aget-byte v2, v1, v0

    if-nez v2, :cond_1

    add-int/lit8 v2, v0, 0x1

    aget-byte v2, v1, v2

    if-nez v2, :cond_1

    add-int/lit8 v2, v0, 0x2

    aget-byte v2, v1, v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    add-int/lit8 v2, v0, 0x3

    aget-byte v2, v1, v2

    const/16 v3, 0xf

    if-ne v2, v3, :cond_1

    .line 158
    array-length v2, v1

    invoke-static {v1, v0, v2}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    .line 159
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 160
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 161
    :cond_2
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Failed to find FourCC VC1 initialization data"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
.end method

.method private static a([B)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v5, -0x1

    const/4 v0, 0x0

    .line 213
    const/4 v1, 0x0

    :try_start_0
    aget-byte v1, p0, v1

    if-eq v1, v2, :cond_0

    .line 214
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    :catch_0
    move-exception v0

    .line 243
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v2, v0

    move v3, v4

    .line 217
    :goto_0
    :try_start_1
    aget-byte v1, p0, v3

    if-ne v1, v5, :cond_1

    .line 218
    add-int/lit16 v1, v2, 0xff

    .line 219
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_0

    .line 220
    :cond_1
    add-int/lit8 v1, v3, 0x1

    aget-byte v3, p0, v3

    add-int/2addr v2, v3

    .line 222
    :goto_1
    aget-byte v3, p0, v1

    if-ne v3, v5, :cond_2

    .line 223
    add-int/lit16 v0, v0, 0xff

    .line 224
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 225
    :cond_2
    add-int/lit8 v3, v1, 0x1

    aget-byte v1, p0, v1

    add-int/2addr v0, v1

    .line 226
    aget-byte v1, p0, v3

    if-eq v1, v4, :cond_3

    .line 227
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 228
    :cond_3
    new-array v1, v2, [B

    .line 229
    const/4 v4, 0x0

    invoke-static {p0, v3, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 230
    add-int/2addr v2, v3

    .line 231
    aget-byte v3, p0, v2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_4

    .line 232
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 233
    :cond_4
    add-int/2addr v0, v2

    .line 234
    aget-byte v2, p0, v0

    const/4 v3, 0x5

    if-eq v2, v3, :cond_5

    .line 235
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 236
    :cond_5
    array-length v2, p0

    sub-int/2addr v2, v0

    new-array v2, v2, [B

    .line 237
    const/4 v3, 0x0

    array-length v4, p0

    sub-int/2addr v4, v0

    invoke-static {p0, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 238
    new-instance v0, Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 239
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 240
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    .line 241
    return-object v0
.end method

.method private a()[B
    .locals 5

    .prologue
    const v4, 0x47435000    # 50000.0f

    const/high16 v3, 0x3f000000    # 0.5f

    const/high16 v1, -0x40800000    # -1.0f

    .line 132
    iget v0, p0, Lcom/google/obf/cu$b;->v:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/obf/cu$b;->w:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/obf/cu$b;->x:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/obf/cu$b;->y:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/obf/cu$b;->z:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/obf/cu$b;->A:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/obf/cu$b;->B:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/obf/cu$b;->C:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/obf/cu$b;->D:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/obf/cu$b;->E:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 133
    :cond_0
    const/4 v0, 0x0

    .line 149
    :goto_0
    return-object v0

    .line 134
    :cond_1
    const/16 v0, 0x19

    new-array v0, v0, [B

    .line 135
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 136
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 137
    iget v2, p0, Lcom/google/obf/cu$b;->v:F

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 138
    iget v2, p0, Lcom/google/obf/cu$b;->w:F

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 139
    iget v2, p0, Lcom/google/obf/cu$b;->x:F

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 140
    iget v2, p0, Lcom/google/obf/cu$b;->y:F

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 141
    iget v2, p0, Lcom/google/obf/cu$b;->z:F

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 142
    iget v2, p0, Lcom/google/obf/cu$b;->A:F

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 143
    iget v2, p0, Lcom/google/obf/cu$b;->B:F

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 144
    iget v2, p0, Lcom/google/obf/cu$b;->C:F

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 145
    iget v2, p0, Lcom/google/obf/cu$b;->D:F

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 146
    iget v2, p0, Lcom/google/obf/cu$b;->E:F

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 147
    iget v2, p0, Lcom/google/obf/cu$b;->t:I

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 148
    iget v2, p0, Lcom/google/obf/cu$b;->u:I

    int-to-short v2, v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    goto :goto_0
.end method

.method private static b(Lcom/google/obf/dw;)Landroid/util/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/dw;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/List",
            "<[B>;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 164
    const/4 v1, 0x4

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/google/obf/dw;->c(I)V

    .line 165
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v1

    and-int/lit8 v1, v1, 0x3

    add-int/lit8 v2, v1, 0x1

    .line 166
    const/4 v1, 0x3

    if-ne v2, v1, :cond_0

    .line 167
    new-instance v0, Lcom/google/obf/s;

    invoke-direct {v0}, Lcom/google/obf/s;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    :catch_0
    move-exception v0

    .line 179
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Error parsing AVC codec private"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 168
    :cond_0
    :try_start_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 169
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v1

    and-int/lit8 v4, v1, 0x1f

    move v1, v0

    .line 170
    :goto_0
    if-ge v1, v4, :cond_1

    .line 171
    invoke-static {p0}, Lcom/google/obf/du;->a(Lcom/google/obf/dw;)[B

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 173
    :cond_1
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v1

    .line 174
    :goto_1
    if-ge v0, v1, :cond_2

    .line 175
    invoke-static {p0}, Lcom/google/obf/du;->a(Lcom/google/obf/dw;)[B

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 177
    :cond_2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    return-object v0
.end method

.method private static c(Lcom/google/obf/dw;)Landroid/util/Pair;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/dw;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/List",
            "<[B>;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 180
    const/16 v0, 0x15

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->c(I)V

    .line 181
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v0

    and-int/lit8 v5, v0, 0x3

    .line 182
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v6

    .line 184
    invoke-virtual {p0}, Lcom/google/obf/dw;->d()I

    move-result v7

    move v3, v1

    move v4, v1

    .line 185
    :goto_0
    if-ge v3, v6, :cond_1

    .line 186
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->d(I)V

    .line 187
    invoke-virtual {p0}, Lcom/google/obf/dw;->g()I

    move-result v8

    move v0, v1

    move v2, v4

    .line 188
    :goto_1
    if-ge v0, v8, :cond_0

    .line 189
    invoke-virtual {p0}, Lcom/google/obf/dw;->g()I

    move-result v4

    .line 190
    add-int/lit8 v9, v4, 0x4

    add-int/2addr v2, v9

    .line 191
    invoke-virtual {p0, v4}, Lcom/google/obf/dw;->d(I)V

    .line 192
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 193
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v4, v2

    goto :goto_0

    .line 194
    :cond_1
    invoke-virtual {p0, v7}, Lcom/google/obf/dw;->c(I)V

    .line 195
    new-array v7, v4, [B

    move v3, v1

    move v0, v1

    .line 197
    :goto_2
    if-ge v3, v6, :cond_3

    .line 198
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/obf/dw;->d(I)V

    .line 199
    invoke-virtual {p0}, Lcom/google/obf/dw;->g()I

    move-result v8

    move v2, v0

    move v0, v1

    .line 200
    :goto_3
    if-ge v0, v8, :cond_2

    .line 201
    invoke-virtual {p0}, Lcom/google/obf/dw;->g()I

    move-result v9

    .line 202
    sget-object v10, Lcom/google/obf/du;->a:[B

    const/4 v11, 0x0

    sget-object v12, Lcom/google/obf/du;->a:[B

    array-length v12, v12

    invoke-static {v10, v11, v7, v2, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 203
    sget-object v10, Lcom/google/obf/du;->a:[B

    array-length v10, v10

    add-int/2addr v2, v10

    .line 204
    iget-object v10, p0, Lcom/google/obf/dw;->a:[B

    invoke-virtual {p0}, Lcom/google/obf/dw;->d()I

    move-result v11

    invoke-static {v10, v11, v7, v2, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 205
    add-int/2addr v2, v9

    .line 206
    invoke-virtual {p0, v9}, Lcom/google/obf/dw;->d(I)V

    .line 207
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 208
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    goto :goto_2

    .line 209
    :cond_3
    if-nez v4, :cond_4

    const/4 v0, 0x0

    .line 210
    :goto_4
    add-int/lit8 v1, v5, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 209
    :cond_4
    invoke-static {v7}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_4

    .line 211
    :catch_0
    move-exception v0

    .line 212
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Error parsing HEVC codec private"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static d(Lcom/google/obf/dw;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 244
    :try_start_0
    invoke-virtual {p0}, Lcom/google/obf/dw;->h()I

    move-result v2

    .line 245
    if-ne v2, v0, :cond_1

    .line 252
    :cond_0
    :goto_0
    return v0

    .line 247
    :cond_1
    const v3, 0xfffe

    if-ne v2, v3, :cond_3

    .line 248
    const/16 v2, 0x18

    invoke-virtual {p0, v2}, Lcom/google/obf/dw;->c(I)V

    .line 249
    invoke-virtual {p0}, Lcom/google/obf/dw;->o()J

    move-result-wide v2

    invoke-static {}, Lcom/google/obf/cu;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->getMostSignificantBits()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    .line 250
    invoke-virtual {p0}, Lcom/google/obf/dw;->o()J

    move-result-wide v2

    invoke-static {}, Lcom/google/obf/cu;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->getLeastSignificantBits()J
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 252
    goto :goto_0

    .line 253
    :catch_0
    move-exception v0

    .line 254
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Error parsing MS/ACM codec private"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a(Lcom/google/obf/al;IJ)V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    .line 31
    const/4 v5, -0x1

    .line 32
    const/4 v12, -0x1

    .line 33
    const/4 v2, 0x0

    .line 34
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cu$b;->a:Ljava/lang/String;

    const/4 v3, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v3, :pswitch_data_0

    .line 108
    new-instance v2, Lcom/google/obf/s;

    const-string v3, "Unrecognized codec identifier."

    invoke-direct {v2, v3}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v2

    .line 34
    :sswitch_0
    const-string v6, "V_VP8"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :sswitch_1
    const-string v6, "V_VP9"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :sswitch_2
    const-string v6, "V_MPEG2"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x2

    goto :goto_0

    :sswitch_3
    const-string v6, "V_MPEG4/ISO/SP"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x3

    goto :goto_0

    :sswitch_4
    const-string v6, "V_MPEG4/ISO/ASP"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x4

    goto :goto_0

    :sswitch_5
    const-string v6, "V_MPEG4/ISO/AP"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x5

    goto :goto_0

    :sswitch_6
    const-string v6, "V_MPEG4/ISO/AVC"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x6

    goto :goto_0

    :sswitch_7
    const-string v6, "V_MPEGH/ISO/HEVC"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x7

    goto :goto_0

    :sswitch_8
    const-string v6, "V_MS/VFW/FOURCC"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v3, 0x8

    goto :goto_0

    :sswitch_9
    const-string v6, "A_VORBIS"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v3, 0x9

    goto :goto_0

    :sswitch_a
    const-string v6, "A_OPUS"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v3, 0xa

    goto :goto_0

    :sswitch_b
    const-string v6, "A_AAC"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v3, 0xb

    goto/16 :goto_0

    :sswitch_c
    const-string v6, "A_MPEG/L3"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v3, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v6, "A_AC3"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v3, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v6, "A_EAC3"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v3, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v6, "A_TRUEHD"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v3, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string v6, "A_DTS"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v3, 0x10

    goto/16 :goto_0

    :sswitch_11
    const-string v6, "A_DTS/EXPRESS"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v3, 0x11

    goto/16 :goto_0

    :sswitch_12
    const-string v6, "A_DTS/LOSSLESS"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v3, 0x12

    goto/16 :goto_0

    :sswitch_13
    const-string v6, "A_FLAC"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v3, 0x13

    goto/16 :goto_0

    :sswitch_14
    const-string v6, "A_MS/ACM"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v3, 0x14

    goto/16 :goto_0

    :sswitch_15
    const-string v6, "A_PCM/INT/LIT"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v3, 0x15

    goto/16 :goto_0

    :sswitch_16
    const-string v6, "S_TEXT/UTF8"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v3, 0x16

    goto/16 :goto_0

    :sswitch_17
    const-string v6, "S_VOBSUB"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v3, 0x17

    goto/16 :goto_0

    :sswitch_18
    const-string v6, "S_HDMV/PGS"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v3, 0x18

    goto/16 :goto_0

    .line 35
    :pswitch_0
    const-string v3, "video/x-vnd.on2.vp8"

    move-object v10, v2

    .line 109
    :goto_1
    invoke-static {v3}, Lcom/google/obf/ds;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 110
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, -0x1

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/obf/cu$b;->F:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/obf/cu$b;->H:I

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/obf/cu$b;->M:Ljava/lang/String;

    move-wide/from16 v6, p3

    invoke-static/range {v2 .. v12}, Lcom/google/obf/q;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;I)Lcom/google/obf/q;

    move-result-object v2

    .line 129
    :goto_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/obf/cu$b;->b:I

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/obf/cu$b;->K:Lcom/google/obf/ar;

    .line 130
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/obf/cu$b;->K:Lcom/google/obf/ar;

    invoke-interface {v3, v2}, Lcom/google/obf/ar;->a(Lcom/google/obf/q;)V

    .line 131
    return-void

    .line 37
    :pswitch_1
    const-string v3, "video/x-vnd.on2.vp9"

    move-object v10, v2

    .line 38
    goto :goto_1

    .line 39
    :pswitch_2
    const-string v3, "video/mpeg2"

    move-object v10, v2

    .line 40
    goto :goto_1

    .line 41
    :pswitch_3
    const-string v3, "video/mp4v-es"

    .line 42
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cu$b;->h:[B

    if-nez v2, :cond_1

    const/4 v2, 0x0

    :goto_3
    move-object v10, v2

    .line 43
    goto :goto_1

    .line 42
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cu$b;->h:[B

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    goto :goto_3

    .line 44
    :pswitch_4
    const-string v4, "video/avc"

    .line 45
    new-instance v2, Lcom/google/obf/dw;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/obf/cu$b;->h:[B

    invoke-direct {v2, v3}, Lcom/google/obf/dw;-><init>([B)V

    invoke-static {v2}, Lcom/google/obf/cu$b;->b(Lcom/google/obf/dw;)Landroid/util/Pair;

    move-result-object v3

    .line 46
    iget-object v2, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    .line 47
    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/obf/cu$b;->L:I

    move-object v3, v4

    move-object v10, v2

    .line 48
    goto :goto_1

    .line 49
    :pswitch_5
    const-string v4, "video/hevc"

    .line 50
    new-instance v2, Lcom/google/obf/dw;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/obf/cu$b;->h:[B

    invoke-direct {v2, v3}, Lcom/google/obf/dw;-><init>([B)V

    invoke-static {v2}, Lcom/google/obf/cu$b;->c(Lcom/google/obf/dw;)Landroid/util/Pair;

    move-result-object v3

    .line 51
    iget-object v2, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    .line 52
    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/obf/cu$b;->L:I

    move-object v3, v4

    move-object v10, v2

    .line 53
    goto/16 :goto_1

    .line 54
    :pswitch_6
    const-string v3, "video/wvc1"

    .line 55
    new-instance v2, Lcom/google/obf/dw;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cu$b;->h:[B

    invoke-direct {v2, v4}, Lcom/google/obf/dw;-><init>([B)V

    invoke-static {v2}, Lcom/google/obf/cu$b;->a(Lcom/google/obf/dw;)Ljava/util/List;

    move-result-object v2

    move-object v10, v2

    .line 56
    goto/16 :goto_1

    .line 57
    :pswitch_7
    const-string v3, "audio/vorbis"

    .line 58
    const/16 v5, 0x2000

    .line 59
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cu$b;->h:[B

    invoke-static {v2}, Lcom/google/obf/cu$b;->a([B)Ljava/util/List;

    move-result-object v2

    move-object v10, v2

    .line 60
    goto/16 :goto_1

    .line 61
    :pswitch_8
    const-string v3, "audio/opus"

    .line 62
    const/16 v5, 0x1680

    .line 63
    new-instance v2, Ljava/util/ArrayList;

    const/4 v4, 0x3

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 64
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cu$b;->h:[B

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    const/16 v4, 0x8

    .line 66
    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/obf/cu$b;->I:J

    invoke-virtual {v4, v6, v7}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    .line 67
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    const/16 v4, 0x8

    .line 69
    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/obf/cu$b;->J:J

    invoke-virtual {v4, v6, v7}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    .line 70
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v10, v2

    .line 71
    goto/16 :goto_1

    .line 72
    :pswitch_9
    const-string v3, "audio/mp4a-latm"

    .line 73
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cu$b;->h:[B

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    move-object v10, v2

    .line 74
    goto/16 :goto_1

    .line 75
    :pswitch_a
    const-string v3, "audio/mpeg"

    .line 76
    const/16 v5, 0x1000

    move-object v10, v2

    .line 77
    goto/16 :goto_1

    .line 78
    :pswitch_b
    const-string v3, "audio/ac3"

    move-object v10, v2

    .line 79
    goto/16 :goto_1

    .line 80
    :pswitch_c
    const-string v3, "audio/eac3"

    move-object v10, v2

    .line 81
    goto/16 :goto_1

    .line 82
    :pswitch_d
    const-string v3, "audio/true-hd"

    move-object v10, v2

    .line 83
    goto/16 :goto_1

    .line 84
    :pswitch_e
    const-string v3, "audio/vnd.dts"

    move-object v10, v2

    .line 85
    goto/16 :goto_1

    .line 86
    :pswitch_f
    const-string v3, "audio/vnd.dts.hd"

    move-object v10, v2

    .line 87
    goto/16 :goto_1

    .line 88
    :pswitch_10
    const-string v3, "audio/x-flac"

    .line 89
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cu$b;->h:[B

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    move-object v10, v2

    .line 90
    goto/16 :goto_1

    .line 91
    :pswitch_11
    const-string v3, "audio/raw"

    .line 92
    new-instance v4, Lcom/google/obf/dw;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/obf/cu$b;->h:[B

    invoke-direct {v4, v6}, Lcom/google/obf/dw;-><init>([B)V

    invoke-static {v4}, Lcom/google/obf/cu$b;->d(Lcom/google/obf/dw;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 93
    new-instance v2, Lcom/google/obf/s;

    const-string v3, "Non-PCM MS/ACM is unsupported"

    invoke-direct {v2, v3}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v2

    .line 94
    :cond_2
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/obf/cu$b;->G:I

    invoke-static {v4}, Lcom/google/obf/ea;->a(I)I

    move-result v12

    .line 95
    if-nez v12, :cond_d

    .line 96
    new-instance v2, Lcom/google/obf/s;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/obf/cu$b;->G:I

    const/16 v4, 0x26

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unsupported PCM bit depth: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v2

    .line 97
    :pswitch_12
    const-string v3, "audio/raw"

    .line 98
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/obf/cu$b;->G:I

    invoke-static {v4}, Lcom/google/obf/ea;->a(I)I

    move-result v12

    .line 99
    if-nez v12, :cond_d

    .line 100
    new-instance v2, Lcom/google/obf/s;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/obf/cu$b;->G:I

    const/16 v4, 0x26

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unsupported PCM bit depth: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v2

    .line 101
    :pswitch_13
    const-string v3, "application/x-subrip"

    move-object v10, v2

    .line 102
    goto/16 :goto_1

    .line 103
    :pswitch_14
    const-string v3, "application/vobsub"

    .line 104
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cu$b;->h:[B

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    move-object v10, v2

    .line 105
    goto/16 :goto_1

    .line 106
    :pswitch_15
    const-string v3, "application/pgs"

    move-object v10, v2

    .line 107
    goto/16 :goto_1

    .line 111
    :cond_3
    invoke-static {v3}, Lcom/google/obf/ds;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 112
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/obf/cu$b;->m:I

    if-nez v2, :cond_4

    .line 113
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/obf/cu$b;->k:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_7

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/obf/cu$b;->i:I

    :goto_4
    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/obf/cu$b;->k:I

    .line 114
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/obf/cu$b;->l:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_8

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/obf/cu$b;->j:I

    :goto_5
    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/obf/cu$b;->l:I

    .line 115
    :cond_4
    const/high16 v12, -0x40800000    # -1.0f

    .line 116
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/obf/cu$b;->k:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_5

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/obf/cu$b;->l:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_5

    .line 117
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/obf/cu$b;->j:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/obf/cu$b;->k:I

    mul-int/2addr v2, v4

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/obf/cu$b;->i:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/obf/cu$b;->l:I

    mul-int/2addr v4, v6

    int-to-float v4, v4

    div-float v12, v2, v4

    .line 118
    :cond_5
    const/4 v15, 0x0

    .line 119
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/obf/cu$b;->p:Z

    if-eqz v2, :cond_6

    .line 120
    invoke-direct/range {p0 .. p0}, Lcom/google/obf/cu$b;->a()[B

    move-result-object v2

    .line 121
    new-instance v15, Lcom/google/obf/d;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/obf/cu$b;->q:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/obf/cu$b;->s:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/obf/cu$b;->r:I

    invoke-direct {v15, v4, v6, v7, v2}, Lcom/google/obf/d;-><init>(III[B)V

    .line 122
    :cond_6
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, -0x1

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/obf/cu$b;->i:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/obf/cu$b;->j:I

    const/4 v11, -0x1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/obf/cu$b;->n:[B

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/obf/cu$b;->o:I

    move-wide/from16 v6, p3

    invoke-static/range {v2 .. v15}, Lcom/google/obf/q;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;IF[BILcom/google/obf/d;)Lcom/google/obf/q;

    move-result-object v2

    goto/16 :goto_2

    .line 113
    :cond_7
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/obf/cu$b;->k:I

    goto :goto_4

    .line 114
    :cond_8
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/obf/cu$b;->l:I

    goto :goto_5

    .line 123
    :cond_9
    const-string v2, "application/x-subrip"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 124
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, -0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/obf/cu$b;->M:Ljava/lang/String;

    move-wide/from16 v5, p3

    invoke-static/range {v2 .. v7}, Lcom/google/obf/q;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)Lcom/google/obf/q;

    move-result-object v2

    goto/16 :goto_2

    .line 125
    :cond_a
    const-string v2, "application/vobsub"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    const-string v2, "application/pgs"

    .line 126
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 127
    :cond_b
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, -0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/obf/cu$b;->M:Ljava/lang/String;

    move-wide/from16 v5, p3

    move-object v7, v10

    invoke-static/range {v2 .. v8}, Lcom/google/obf/q;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/util/List;Ljava/lang/String;)Lcom/google/obf/q;

    move-result-object v2

    goto/16 :goto_2

    .line 128
    :cond_c
    new-instance v2, Lcom/google/obf/s;

    const-string v3, "Unexpected MIME type."

    invoke-direct {v2, v3}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_d
    move-object v10, v2

    goto/16 :goto_1

    .line 34
    nop

    :sswitch_data_0
    .sparse-switch
        -0x7ce7f5de -> :sswitch_5
        -0x7ce7f3b0 -> :sswitch_3
        -0x76567dc0 -> :sswitch_14
        -0x6a615338 -> :sswitch_f
        -0x672350af -> :sswitch_9
        -0x585f4fcd -> :sswitch_c
        -0x51dc40b2 -> :sswitch_8
        -0x2016c535 -> :sswitch_4
        -0x2016c4e5 -> :sswitch_6
        -0x19552dbd -> :sswitch_17
        -0x1538b2ba -> :sswitch_12
        0x3c02325 -> :sswitch_b
        0x3c02353 -> :sswitch_d
        0x3c030c5 -> :sswitch_10
        0x4e86155 -> :sswitch_0
        0x4e86156 -> :sswitch_1
        0x5e8da3e -> :sswitch_18
        0x2056f406 -> :sswitch_11
        0x2b453ce4 -> :sswitch_15
        0x32fdf009 -> :sswitch_7
        0x54c61e47 -> :sswitch_16
        0x6bd6c624 -> :sswitch_2
        0x7446132a -> :sswitch_e
        0x7446b0a6 -> :sswitch_13
        0x744ad97d -> :sswitch_a
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
    .end packed-switch
.end method
