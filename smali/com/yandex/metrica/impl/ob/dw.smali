.class Lcom/yandex/metrica/impl/ob/dw;
.super Lcom/yandex/metrica/impl/ob/dt;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ob/dw$a;
    }
.end annotation


# static fields
.field public static final a:J

.field public static final b:J


# instance fields
.field private c:Lcom/yandex/metrica/impl/e$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/yandex/metrica/impl/e$a",
            "<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/yandex/metrica/impl/ob/dw$a;

.field private e:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 25
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/yandex/metrica/impl/ob/dw;->a:J

    .line 28
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/yandex/metrica/impl/ob/dw;->b:J

    return-void
.end method

.method public constructor <init>(Lcom/yandex/metrica/impl/ob/ds;)V
    .locals 6

    .prologue
    .line 49
    new-instance v0, Lcom/yandex/metrica/impl/ob/dw$a;

    sget-wide v2, Lcom/yandex/metrica/impl/ob/dw;->a:J

    const-wide/16 v4, 0xc8

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/yandex/metrica/impl/ob/dw$a;-><init>(JJ)V

    sget-wide v2, Lcom/yandex/metrica/impl/ob/dw;->b:J

    invoke-direct {p0, p1, v0, v2, v3}, Lcom/yandex/metrica/impl/ob/dw;-><init>(Lcom/yandex/metrica/impl/ob/ds;Lcom/yandex/metrica/impl/ob/dw$a;J)V

    .line 53
    return-void
.end method

.method constructor <init>(Lcom/yandex/metrica/impl/ob/ds;Lcom/yandex/metrica/impl/ob/dw$a;J)V
    .locals 1

    .prologue
    .line 143
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/dt;-><init>(Lcom/yandex/metrica/impl/ob/ds;)V

    .line 144
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/dw;->d:Lcom/yandex/metrica/impl/ob/dw$a;

    .line 145
    iput-wide p3, p0, Lcom/yandex/metrica/impl/ob/dw;->e:J

    .line 146
    return-void
.end method

.method public static a(Landroid/location/Location;Landroid/location/Location;JJ)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 91
    if-nez p1, :cond_1

    .line 134
    :cond_0
    :goto_0
    return v1

    .line 96
    :cond_1
    if-nez p0, :cond_2

    move v1, v2

    .line 98
    goto :goto_0

    .line 102
    :cond_2
    invoke-virtual {p0}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 103
    cmp-long v0, v4, p2

    if-lez v0, :cond_3

    move v3, v1

    .line 104
    :goto_1
    neg-long v6, p2

    cmp-long v0, v4, v6

    if-gez v0, :cond_4

    move v0, v1

    .line 105
    :goto_2
    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_5

    move v6, v1

    .line 107
    :goto_3
    if-nez v3, :cond_0

    .line 111
    if-eqz v0, :cond_6

    move v1, v2

    .line 113
    goto :goto_0

    :cond_3
    move v3, v2

    .line 103
    goto :goto_1

    :cond_4
    move v0, v2

    .line 104
    goto :goto_2

    :cond_5
    move v6, v2

    .line 105
    goto :goto_3

    .line 117
    :cond_6
    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    sub-float/2addr v0, v3

    float-to-int v0, v0

    .line 118
    if-lez v0, :cond_9

    move v5, v1

    .line 119
    :goto_4
    if-gez v0, :cond_a

    move v4, v1

    .line 120
    :goto_5
    int-to-long v8, v0

    cmp-long v0, v8, p4

    if-lez v0, :cond_b

    move v0, v1

    .line 123
    :goto_6
    invoke-virtual {p0}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v7

    .line 1138
    if-nez v3, :cond_d

    if-nez v7, :cond_c

    move v3, v1

    .line 126
    :goto_7
    if-nez v4, :cond_0

    .line 128
    if-eqz v6, :cond_7

    if-eqz v5, :cond_0

    .line 130
    :cond_7
    if-eqz v6, :cond_8

    if-nez v0, :cond_8

    if-nez v3, :cond_0

    :cond_8
    move v1, v2

    .line 134
    goto :goto_0

    :cond_9
    move v5, v2

    .line 118
    goto :goto_4

    :cond_a
    move v4, v2

    .line 119
    goto :goto_5

    :cond_b
    move v0, v2

    .line 120
    goto :goto_6

    :cond_c
    move v3, v2

    .line 1138
    goto :goto_7

    :cond_d
    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_7
.end method


# virtual methods
.method public a()Landroid/location/Location;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/dw;->c:Lcom/yandex/metrica/impl/e$a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/dw;->c:Lcom/yandex/metrica/impl/e$a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/e$a;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    goto :goto_0
.end method

.method public a(Landroid/location/Location;Lcom/yandex/metrica/impl/ob/dv;)V
    .locals 6

    .prologue
    .line 58
    if-eqz p1, :cond_1

    .line 1065
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/dw;->c:Lcom/yandex/metrica/impl/e$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/dw;->c:Lcom/yandex/metrica/impl/e$a;

    iget-wide v2, p0, Lcom/yandex/metrica/impl/ob/dw;->e:J

    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/e$a;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/dw;->c:Lcom/yandex/metrica/impl/e$a;

    .line 1066
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/e$a;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/Location;

    .line 1083
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/dw;->d:Lcom/yandex/metrica/impl/ob/dw$a;

    iget-wide v2, v0, Lcom/yandex/metrica/impl/ob/dw$a;->a:J

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/dw;->d:Lcom/yandex/metrica/impl/ob/dw$a;

    iget-wide v4, v0, Lcom/yandex/metrica/impl/ob/dw$a;->b:J

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/yandex/metrica/impl/ob/dw;->a(Landroid/location/Location;Landroid/location/Location;JJ)Z

    move-result v0

    .line 1066
    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 1068
    :goto_0
    if-eqz v0, :cond_1

    .line 1069
    new-instance v0, Landroid/location/Location;

    invoke-direct {v0, p1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    .line 1070
    new-instance v1, Lcom/yandex/metrica/impl/e$a;

    invoke-direct {v1}, Lcom/yandex/metrica/impl/e$a;-><init>()V

    .line 1071
    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/e$a;->a(Ljava/lang/Object;)V

    .line 1072
    iput-object v1, p0, Lcom/yandex/metrica/impl/ob/dw;->c:Lcom/yandex/metrica/impl/e$a;

    .line 61
    :cond_1
    return-void

    .line 1066
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
