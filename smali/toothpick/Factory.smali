.class public interface abstract Ltoothpick/Factory;
.super Ljava/lang/Object;
.source "Factory.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract createInstance(Ltoothpick/Scope;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltoothpick/Scope;",
            ")TT;"
        }
    .end annotation
.end method

.method public abstract getTargetScope(Ltoothpick/Scope;)Ltoothpick/Scope;
.end method

.method public abstract hasProvidesSingletonInScopeAnnotation()Z
.end method

.method public abstract hasScopeAnnotation()Z
.end method
