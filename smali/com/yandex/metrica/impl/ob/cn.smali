.class public final Lcom/yandex/metrica/impl/ob/cn;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ob/cn$c;,
        Lcom/yandex/metrica/impl/ob/cn$b;,
        Lcom/yandex/metrica/impl/ob/cn$g;,
        Lcom/yandex/metrica/impl/ob/cn$f;,
        Lcom/yandex/metrica/impl/ob/cn$e;,
        Lcom/yandex/metrica/impl/ob/cn$d;,
        Lcom/yandex/metrica/impl/ob/cn$x;,
        Lcom/yandex/metrica/impl/ob/cn$v;,
        Lcom/yandex/metrica/impl/ob/cn$u;,
        Lcom/yandex/metrica/impl/ob/cn$t;,
        Lcom/yandex/metrica/impl/ob/cn$s;,
        Lcom/yandex/metrica/impl/ob/cn$r;,
        Lcom/yandex/metrica/impl/ob/cn$l;,
        Lcom/yandex/metrica/impl/ob/cn$n;,
        Lcom/yandex/metrica/impl/ob/cn$m;,
        Lcom/yandex/metrica/impl/ob/cn$k;,
        Lcom/yandex/metrica/impl/ob/cn$j;,
        Lcom/yandex/metrica/impl/ob/cn$i;,
        Lcom/yandex/metrica/impl/ob/cn$h;,
        Lcom/yandex/metrica/impl/ob/cn$q;,
        Lcom/yandex/metrica/impl/ob/cn$p;,
        Lcom/yandex/metrica/impl/ob/cn$y;,
        Lcom/yandex/metrica/impl/ob/cn$w;,
        Lcom/yandex/metrica/impl/ob/cn$o;,
        Lcom/yandex/metrica/impl/ob/cn$z;,
        Lcom/yandex/metrica/impl/ob/cn$a;,
        Lcom/yandex/metrica/impl/ob/cn$ae;,
        Lcom/yandex/metrica/impl/ob/cn$aa;,
        Lcom/yandex/metrica/impl/ob/cn$ab;,
        Lcom/yandex/metrica/impl/ob/cn$ad;,
        Lcom/yandex/metrica/impl/ob/cn$ac;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/Boolean;

.field public static final b:I

.field static final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/yandex/metrica/impl/ob/cn$o;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/yandex/metrica/impl/ob/cn$o;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/16 v7, 0x3c

    const/16 v6, 0x32

    const/16 v5, 0x2f

    const/16 v4, 0x1d

    const/4 v3, 0x0

    .line 49
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/yandex/metrica/impl/ob/cn;->a:Ljava/lang/Boolean;

    .line 52
    invoke-static {}, Lcom/yandex/metrica/YandexMetrica;->getLibraryApiLevel()I

    move-result v0

    sput v0, Lcom/yandex/metrica/impl/ob/cn;->b:I

    .line 537
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 538
    sput-object v0, Lcom/yandex/metrica/impl/ob/cn;->c:Landroid/util/SparseArray;

    const/4 v1, 0x6

    new-instance v2, Lcom/yandex/metrica/impl/ob/cn$w;

    invoke-direct {v2, v3}, Lcom/yandex/metrica/impl/ob/cn$w;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 539
    sget-object v0, Lcom/yandex/metrica/impl/ob/cn;->c:Landroid/util/SparseArray;

    const/4 v1, 0x7

    new-instance v2, Lcom/yandex/metrica/impl/ob/cn$y;

    invoke-direct {v2, v3}, Lcom/yandex/metrica/impl/ob/cn$y;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 540
    sget-object v0, Lcom/yandex/metrica/impl/ob/cn;->c:Landroid/util/SparseArray;

    const/16 v1, 0xe

    new-instance v2, Lcom/yandex/metrica/impl/ob/cn$p;

    invoke-direct {v2, v3}, Lcom/yandex/metrica/impl/ob/cn$p;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 541
    sget-object v0, Lcom/yandex/metrica/impl/ob/cn;->c:Landroid/util/SparseArray;

    new-instance v1, Lcom/yandex/metrica/impl/ob/cn$q;

    invoke-direct {v1, v3}, Lcom/yandex/metrica/impl/ob/cn$q;-><init>(B)V

    invoke-virtual {v0, v4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 542
    sget-object v0, Lcom/yandex/metrica/impl/ob/cn;->c:Landroid/util/SparseArray;

    const/16 v1, 0x25

    new-instance v2, Lcom/yandex/metrica/impl/ob/cn$r;

    invoke-direct {v2, v3}, Lcom/yandex/metrica/impl/ob/cn$r;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 543
    sget-object v0, Lcom/yandex/metrica/impl/ob/cn;->c:Landroid/util/SparseArray;

    const/16 v1, 0x27

    new-instance v2, Lcom/yandex/metrica/impl/ob/cn$s;

    invoke-direct {v2, v3}, Lcom/yandex/metrica/impl/ob/cn$s;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 544
    sget-object v0, Lcom/yandex/metrica/impl/ob/cn;->c:Landroid/util/SparseArray;

    const/16 v1, 0x2d

    new-instance v2, Lcom/yandex/metrica/impl/ob/cn$t;

    invoke-direct {v2, v3}, Lcom/yandex/metrica/impl/ob/cn$t;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 545
    sget-object v0, Lcom/yandex/metrica/impl/ob/cn;->c:Landroid/util/SparseArray;

    new-instance v1, Lcom/yandex/metrica/impl/ob/cn$u;

    invoke-direct {v1, v3}, Lcom/yandex/metrica/impl/ob/cn$u;-><init>(B)V

    invoke-virtual {v0, v5, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 546
    sget-object v0, Lcom/yandex/metrica/impl/ob/cn;->c:Landroid/util/SparseArray;

    new-instance v1, Lcom/yandex/metrica/impl/ob/cn$v;

    invoke-direct {v1, v3}, Lcom/yandex/metrica/impl/ob/cn$v;-><init>(B)V

    invoke-virtual {v0, v6, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 547
    sget-object v0, Lcom/yandex/metrica/impl/ob/cn;->c:Landroid/util/SparseArray;

    new-instance v1, Lcom/yandex/metrica/impl/ob/cn$x;

    invoke-direct {v1, v3}, Lcom/yandex/metrica/impl/ob/cn$x;-><init>(B)V

    invoke-virtual {v0, v7, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 549
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 550
    sput-object v0, Lcom/yandex/metrica/impl/ob/cn;->d:Landroid/util/SparseArray;

    const/16 v1, 0xc

    new-instance v2, Lcom/yandex/metrica/impl/ob/cn$h;

    invoke-direct {v2, v3}, Lcom/yandex/metrica/impl/ob/cn$h;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 551
    sget-object v0, Lcom/yandex/metrica/impl/ob/cn;->d:Landroid/util/SparseArray;

    new-instance v1, Lcom/yandex/metrica/impl/ob/cn$i;

    invoke-direct {v1, v3}, Lcom/yandex/metrica/impl/ob/cn$i;-><init>(B)V

    invoke-virtual {v0, v4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 552
    sget-object v0, Lcom/yandex/metrica/impl/ob/cn;->d:Landroid/util/SparseArray;

    new-instance v1, Lcom/yandex/metrica/impl/ob/cn$j;

    invoke-direct {v1, v3}, Lcom/yandex/metrica/impl/ob/cn$j;-><init>(B)V

    invoke-virtual {v0, v5, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 553
    sget-object v0, Lcom/yandex/metrica/impl/ob/cn;->d:Landroid/util/SparseArray;

    new-instance v1, Lcom/yandex/metrica/impl/ob/cn$k;

    invoke-direct {v1, v3}, Lcom/yandex/metrica/impl/ob/cn$k;-><init>(B)V

    invoke-virtual {v0, v6, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 554
    sget-object v0, Lcom/yandex/metrica/impl/ob/cn;->d:Landroid/util/SparseArray;

    const/16 v1, 0x37

    new-instance v2, Lcom/yandex/metrica/impl/ob/cn$l;

    invoke-direct {v2, v3}, Lcom/yandex/metrica/impl/ob/cn$l;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 555
    sget-object v0, Lcom/yandex/metrica/impl/ob/cn;->d:Landroid/util/SparseArray;

    new-instance v1, Lcom/yandex/metrica/impl/ob/cn$m;

    invoke-direct {v1, v3}, Lcom/yandex/metrica/impl/ob/cn$m;-><init>(B)V

    invoke-virtual {v0, v7, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 556
    sget-object v0, Lcom/yandex/metrica/impl/ob/cn;->d:Landroid/util/SparseArray;

    const/16 v1, 0x3f

    new-instance v2, Lcom/yandex/metrica/impl/ob/cn$n;

    invoke-direct {v2, v3}, Lcom/yandex/metrica/impl/ob/cn$n;-><init>(B)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 559
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 560
    sput-object v0, Lcom/yandex/metrica/impl/ob/cn;->e:Ljava/util/HashMap;

    const-string v1, "reports"

    sget-object v2, Lcom/yandex/metrica/impl/ob/cn$ac;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 561
    sget-object v0, Lcom/yandex/metrica/impl/ob/cn;->e:Ljava/util/HashMap;

    const-string v1, "sessions"

    sget-object v2, Lcom/yandex/metrica/impl/ob/cn$ad;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 562
    sget-object v0, Lcom/yandex/metrica/impl/ob/cn;->e:Ljava/util/HashMap;

    const-string v1, "preferences"

    sget-object v2, Lcom/yandex/metrica/impl/ob/cn$ab;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 563
    return-void
.end method

.method public static a()Lcom/yandex/metrica/impl/ob/cv;
    .locals 6

    .prologue
    .line 566
    new-instance v0, Lcom/yandex/metrica/impl/ob/cv;

    new-instance v1, Lcom/yandex/metrica/impl/ob/cn$d;

    invoke-direct {v1}, Lcom/yandex/metrica/impl/ob/cn$d;-><init>()V

    new-instance v2, Lcom/yandex/metrica/impl/ob/cn$e;

    invoke-direct {v2}, Lcom/yandex/metrica/impl/ob/cn$e;-><init>()V

    sget-object v3, Lcom/yandex/metrica/impl/ob/cn;->c:Landroid/util/SparseArray;

    new-instance v4, Lcom/yandex/metrica/impl/ob/cx;

    sget-object v5, Lcom/yandex/metrica/impl/ob/cn;->e:Ljava/util/HashMap;

    invoke-direct {v4, v5}, Lcom/yandex/metrica/impl/ob/cx;-><init>(Ljava/util/HashMap;)V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/yandex/metrica/impl/ob/cv;-><init>(Lcom/yandex/metrica/impl/ob/cn$o;Lcom/yandex/metrica/impl/ob/cn$o;Landroid/util/SparseArray;Lcom/yandex/metrica/impl/ob/cw;)V

    return-object v0
.end method

.method public static b()Lcom/yandex/metrica/impl/ob/cv;
    .locals 6

    .prologue
    .line 576
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 577
    const-string v1, "preferences"

    sget-object v2, Lcom/yandex/metrica/impl/ob/cn$ab;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 578
    const-string v1, "startup"

    sget-object v2, Lcom/yandex/metrica/impl/ob/cn$ae;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 579
    const-string v1, "l_dat"

    sget-object v2, Lcom/yandex/metrica/impl/ob/cn$a;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 580
    const-string v1, "lbs_dat"

    sget-object v2, Lcom/yandex/metrica/impl/ob/cn$a;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 581
    const-string v1, "permissions"

    sget-object v2, Lcom/yandex/metrica/impl/ob/cn$aa;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 583
    new-instance v1, Lcom/yandex/metrica/impl/ob/cv;

    new-instance v2, Lcom/yandex/metrica/impl/ob/cn$f;

    invoke-direct {v2}, Lcom/yandex/metrica/impl/ob/cn$f;-><init>()V

    new-instance v3, Lcom/yandex/metrica/impl/ob/cn$g;

    invoke-direct {v3}, Lcom/yandex/metrica/impl/ob/cn$g;-><init>()V

    sget-object v4, Lcom/yandex/metrica/impl/ob/cn;->d:Landroid/util/SparseArray;

    new-instance v5, Lcom/yandex/metrica/impl/ob/cx;

    invoke-direct {v5, v0}, Lcom/yandex/metrica/impl/ob/cx;-><init>(Ljava/util/HashMap;)V

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/yandex/metrica/impl/ob/cv;-><init>(Lcom/yandex/metrica/impl/ob/cn$o;Lcom/yandex/metrica/impl/ob/cn$o;Landroid/util/SparseArray;Lcom/yandex/metrica/impl/ob/cw;)V

    return-object v1
.end method

.method public static c()Lcom/yandex/metrica/impl/ob/cv;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 593
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 594
    const-string v1, "preferences"

    sget-object v2, Lcom/yandex/metrica/impl/ob/cn$ab;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 596
    new-instance v1, Lcom/yandex/metrica/impl/ob/cv;

    new-instance v2, Lcom/yandex/metrica/impl/ob/cn$b;

    invoke-direct {v2, v4}, Lcom/yandex/metrica/impl/ob/cn$b;-><init>(B)V

    new-instance v3, Lcom/yandex/metrica/impl/ob/cn$c;

    invoke-direct {v3, v4}, Lcom/yandex/metrica/impl/ob/cn$c;-><init>(B)V

    new-instance v4, Landroid/util/SparseArray;

    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    new-instance v5, Lcom/yandex/metrica/impl/ob/cx;

    invoke-direct {v5, v0}, Lcom/yandex/metrica/impl/ob/cx;-><init>(Ljava/util/HashMap;)V

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/yandex/metrica/impl/ob/cv;-><init>(Lcom/yandex/metrica/impl/ob/cn$o;Lcom/yandex/metrica/impl/ob/cn$o;Landroid/util/SparseArray;Lcom/yandex/metrica/impl/ob/cw;)V

    return-object v1
.end method
