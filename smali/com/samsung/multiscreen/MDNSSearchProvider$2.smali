.class final Lcom/samsung/multiscreen/MDNSSearchProvider$2;
.super Ljava/lang/Object;
.source "MDNSSearchProvider.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/multiscreen/MDNSSearchProvider;->getById(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/multiscreen/Result;)Lcom/samsung/multiscreen/ProviderThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$id:Ljava/lang/String;

.field final synthetic val$result:Lcom/samsung/multiscreen/Result;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/samsung/multiscreen/Result;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/samsung/multiscreen/MDNSSearchProvider$2;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/samsung/multiscreen/MDNSSearchProvider$2;->val$result:Lcom/samsung/multiscreen/Result;

    iput-object p3, p0, Lcom/samsung/multiscreen/MDNSSearchProvider$2;->val$id:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    .line 166
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    check-cast v6, Lcom/samsung/multiscreen/ProviderThread;

    .line 168
    .local v6, "currentThread":Lcom/samsung/multiscreen/ProviderThread;
    iget-object v3, p0, Lcom/samsung/multiscreen/MDNSSearchProvider$2;->val$context:Landroid/content/Context;

    const-string v4, "MDNSSearchProvider"

    invoke-static {v3, v4}, Lcom/samsung/multiscreen/util/NetUtil;->acquireMulticastLock(Landroid/content/Context;Ljava/lang/String;)Landroid/net/wifi/WifiManager$MulticastLock;

    move-result-object v10

    .line 170
    .local v10, "multicastLock":Landroid/net/wifi/WifiManager$MulticastLock;
    const/4 v0, 0x0

    .line 171
    .local v0, "jmdns":Ljavax/jmdns/JmDNS;
    const/4 v13, 0x0

    .line 173
    .local v13, "runnable":Ljava/lang/Runnable;
    :try_start_0
    iget-object v3, p0, Lcom/samsung/multiscreen/MDNSSearchProvider$2;->val$context:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/multiscreen/util/NetUtil;->getDeviceIpAddress(Landroid/content/Context;)Ljava/net/InetAddress;

    move-result-object v7

    .line 174
    .local v7, "deviceIpAddress":Ljava/net/InetAddress;
    invoke-static {v7}, Ljavax/jmdns/JmDNS;->create(Ljava/net/InetAddress;)Ljavax/jmdns/JmDNS;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 185
    .end local v7    # "deviceIpAddress":Ljava/net/InetAddress;
    :goto_0
    if-eqz v0, :cond_3

    .line 186
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/samsung/multiscreen/MDNSSearchProvider$2;->val$id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_samsungmsf._tcp.local."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 187
    .local v1, "type":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/multiscreen/MDNSSearchProvider$2;->val$id:Ljava/lang/String;

    .line 189
    .local v2, "name":Ljava/lang/String;
    const/4 v11, 0x2

    .line 190
    .local v11, "retry":I
    const/4 v9, 0x0

    .line 191
    .local v9, "info":Ljavax/jmdns/ServiceInfo;
    :goto_1
    invoke-virtual {v6}, Lcom/samsung/multiscreen/ProviderThread;->isTerminate()Z

    move-result v3

    if-nez v3, :cond_1

    if-nez v9, :cond_1

    add-int/lit8 v12, v11, -0x1

    .end local v11    # "retry":I
    .local v12, "retry":I
    if-ltz v11, :cond_0

    .line 192
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v3

    if-nez v3, :cond_0

    .line 193
    const/4 v3, 0x0

    const-wide/16 v4, 0x1388

    invoke-virtual/range {v0 .. v5}, Ljavax/jmdns/JmDNS;->getServiceInfo(Ljava/lang/String;Ljava/lang/String;ZJ)Ljavax/jmdns/ServiceInfo;

    move-result-object v9

    move v11, v12

    .end local v12    # "retry":I
    .restart local v11    # "retry":I
    goto :goto_1

    .line 175
    .end local v1    # "type":Ljava/lang/String;
    .end local v2    # "name":Ljava/lang/String;
    .end local v9    # "info":Ljavax/jmdns/ServiceInfo;
    .end local v11    # "retry":I
    :catch_0
    move-exception v8

    .line 176
    .local v8, "e":Ljava/io/IOException;
    invoke-static {v8}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .line 177
    new-instance v13, Lcom/samsung/multiscreen/MDNSSearchProvider$2$1;

    .end local v13    # "runnable":Ljava/lang/Runnable;
    invoke-direct {v13, p0, v8}, Lcom/samsung/multiscreen/MDNSSearchProvider$2$1;-><init>(Lcom/samsung/multiscreen/MDNSSearchProvider$2;Ljava/io/IOException;)V

    .restart local v13    # "runnable":Ljava/lang/Runnable;
    goto :goto_0

    .end local v8    # "e":Ljava/io/IOException;
    .restart local v1    # "type":Ljava/lang/String;
    .restart local v2    # "name":Ljava/lang/String;
    .restart local v9    # "info":Ljavax/jmdns/ServiceInfo;
    .restart local v12    # "retry":I
    :cond_0
    move v11, v12

    .line 196
    .end local v12    # "retry":I
    .restart local v11    # "retry":I
    :cond_1
    invoke-virtual {v6}, Lcom/samsung/multiscreen/ProviderThread;->isTerminate()Z

    move-result v3

    if-nez v3, :cond_2

    .line 197
    if-nez v9, :cond_5

    .line 198
    new-instance v13, Lcom/samsung/multiscreen/MDNSSearchProvider$2$2;

    .end local v13    # "runnable":Ljava/lang/Runnable;
    invoke-direct {v13, p0}, Lcom/samsung/multiscreen/MDNSSearchProvider$2$2;-><init>(Lcom/samsung/multiscreen/MDNSSearchProvider$2;)V

    .line 216
    .restart local v13    # "runnable":Ljava/lang/Runnable;
    :cond_2
    :goto_2
    :try_start_1
    invoke-virtual {v0}, Ljavax/jmdns/JmDNS;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 222
    .end local v1    # "type":Ljava/lang/String;
    .end local v2    # "name":Ljava/lang/String;
    .end local v9    # "info":Ljavax/jmdns/ServiceInfo;
    .end local v11    # "retry":I
    :cond_3
    :goto_3
    invoke-static {v10}, Lcom/samsung/multiscreen/util/NetUtil;->releaseMulticastLock(Landroid/net/wifi/WifiManager$MulticastLock;)V

    .line 224
    if-eqz v13, :cond_4

    .line 225
    invoke-static {v13}, Lcom/samsung/multiscreen/util/RunUtil;->runOnUI(Ljava/lang/Runnable;)V

    .line 227
    :cond_4
    return-void

    .line 205
    .restart local v1    # "type":Ljava/lang/String;
    .restart local v2    # "name":Ljava/lang/String;
    .restart local v9    # "info":Ljavax/jmdns/ServiceInfo;
    .restart local v11    # "retry":I
    :cond_5
    invoke-static {v9}, Lcom/samsung/multiscreen/Service;->create(Ljavax/jmdns/ServiceInfo;)Lcom/samsung/multiscreen/Service;

    move-result-object v14

    .line 206
    .local v14, "service":Lcom/samsung/multiscreen/Service;
    new-instance v13, Lcom/samsung/multiscreen/MDNSSearchProvider$2$3;

    .end local v13    # "runnable":Ljava/lang/Runnable;
    invoke-direct {v13, p0, v14}, Lcom/samsung/multiscreen/MDNSSearchProvider$2$3;-><init>(Lcom/samsung/multiscreen/MDNSSearchProvider$2;Lcom/samsung/multiscreen/Service;)V

    .restart local v13    # "runnable":Ljava/lang/Runnable;
    goto :goto_2

    .line 217
    .end local v14    # "service":Lcom/samsung/multiscreen/Service;
    :catch_1
    move-exception v8

    .line 218
    .restart local v8    # "e":Ljava/io/IOException;
    const-string v3, "MDNSSearchProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getById error: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method
