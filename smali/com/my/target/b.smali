.class public final Lcom/my/target/b;
.super Ljava/lang/Object;
.source "AdConfig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/b$a;
    }
.end annotation


# static fields
.field private static final DEFAULT_VIDEO_QUALITY:I = 0x168


# instance fields
.field private autoLoadImages:Z

.field private autoLoadVideo:Z

.field private bannersCount:I

.field private cachePeriod:J

.field private final customParams:Lcom/my/target/common/CustomParams;

.field private final format:Ljava/lang/String;

.field private refreshAd:Z

.field private final slotId:I

.field private trackingEnvironmentEnabled:Z

.field private trackingLocationEnabled:Z

.field private videoQuality:I


# direct methods
.method private constructor <init>(ILjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Lcom/my/target/common/CustomParams;

    invoke-direct {v0}, Lcom/my/target/common/CustomParams;-><init>()V

    iput-object v0, p0, Lcom/my/target/b;->customParams:Lcom/my/target/common/CustomParams;

    .line 26
    const-wide/32 v0, 0x5265c00

    iput-wide v0, p0, Lcom/my/target/b;->cachePeriod:J

    .line 27
    iput-boolean v2, p0, Lcom/my/target/b;->trackingLocationEnabled:Z

    .line 28
    iput-boolean v2, p0, Lcom/my/target/b;->trackingEnvironmentEnabled:Z

    .line 29
    iput-boolean v2, p0, Lcom/my/target/b;->refreshAd:Z

    .line 30
    iput-boolean v3, p0, Lcom/my/target/b;->autoLoadImages:Z

    .line 31
    iput-boolean v3, p0, Lcom/my/target/b;->autoLoadVideo:Z

    .line 32
    const/16 v0, 0x168

    iput v0, p0, Lcom/my/target/b;->videoQuality:I

    .line 139
    iput p1, p0, Lcom/my/target/b;->slotId:I

    .line 140
    iput-object p2, p0, Lcom/my/target/b;->format:Ljava/lang/String;

    .line 141
    return-void
.end method

.method public static newConfig(ILjava/lang/String;)Lcom/my/target/b;
    .locals 1
    .param p0, "slotId"    # I
    .param p1, "format"    # Ljava/lang/String;

    .prologue
    .line 20
    new-instance v0, Lcom/my/target/b;

    invoke-direct {v0, p0, p1}, Lcom/my/target/b;-><init>(ILjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getBannersCount()I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/my/target/b;->bannersCount:I

    return v0
.end method

.method public getCachePeriod()J
    .locals 2

    .prologue
    .line 37
    iget-wide v0, p0, Lcom/my/target/b;->cachePeriod:J

    return-wide v0
.end method

.method public getCustomParams()Lcom/my/target/common/CustomParams;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/my/target/b;->customParams:Lcom/my/target/common/CustomParams;

    return-object v0
.end method

.method public getFormat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/my/target/b;->format:Ljava/lang/String;

    return-object v0
.end method

.method public getSlotId()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/my/target/b;->slotId:I

    return v0
.end method

.method public getVideoQuality()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/my/target/b;->videoQuality:I

    return v0
.end method

.method public isAutoLoadImages()Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/my/target/b;->autoLoadImages:Z

    return v0
.end method

.method public isAutoLoadVideo()Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/my/target/b;->autoLoadVideo:Z

    return v0
.end method

.method public isRefreshAd()Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/my/target/b;->refreshAd:Z

    return v0
.end method

.method public isTrackingEnvironmentEnabled()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/my/target/b;->trackingEnvironmentEnabled:Z

    return v0
.end method

.method public isTrackingLocationEnabled()Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/my/target/b;->trackingLocationEnabled:Z

    return v0
.end method

.method public setAutoLoadImages(Z)V
    .locals 0
    .param p1, "autoLoadImages"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/my/target/b;->autoLoadImages:Z

    .line 75
    return-void
.end method

.method public setAutoLoadVideo(Z)V
    .locals 0
    .param p1, "autoLoadVideo"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/my/target/b;->autoLoadVideo:Z

    .line 85
    return-void
.end method

.method public setBannersCount(I)V
    .locals 0
    .param p1, "bannersCount"    # I

    .prologue
    .line 134
    iput p1, p0, Lcom/my/target/b;->bannersCount:I

    .line 135
    return-void
.end method

.method public setCachePeriod(J)V
    .locals 5
    .param p1, "cachePeriod"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 42
    cmp-long v0, p1, v2

    if-gez v0, :cond_0

    .line 44
    iput-wide v2, p0, Lcom/my/target/b;->cachePeriod:J

    .line 50
    :goto_0
    return-void

    .line 48
    :cond_0
    iput-wide p1, p0, Lcom/my/target/b;->cachePeriod:J

    goto :goto_0
.end method

.method public setRefreshAd(Z)V
    .locals 0
    .param p1, "refreshAd"    # Z

    .prologue
    .line 124
    iput-boolean p1, p0, Lcom/my/target/b;->refreshAd:Z

    .line 125
    return-void
.end method

.method public setTrackingEnvironmentEnabled(Z)V
    .locals 0
    .param p1, "trackingEnvironmentEnabled"    # Z

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/my/target/b;->trackingEnvironmentEnabled:Z

    .line 95
    return-void
.end method

.method public setTrackingLocationEnabled(Z)V
    .locals 0
    .param p1, "trackingLocationEnabled"    # Z

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/my/target/b;->trackingLocationEnabled:Z

    .line 105
    return-void
.end method

.method public setVideoQuality(I)V
    .locals 0
    .param p1, "videoQuality"    # I

    .prologue
    .line 114
    iput p1, p0, Lcom/my/target/b;->videoQuality:I

    .line 115
    return-void
.end method
