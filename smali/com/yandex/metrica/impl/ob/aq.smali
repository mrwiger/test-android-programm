.class public Lcom/yandex/metrica/impl/ob/aq;
.super Lcom/yandex/metrica/impl/ob/ah;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/yandex/metrica/impl/ob/ah;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/yandex/metrica/impl/ob/v;)V
    .locals 4

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/ah;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/aq;->a:Ljava/util/ArrayList;

    .line 30
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/aq;->a:Ljava/util/ArrayList;

    new-instance v1, Lcom/yandex/metrica/impl/ob/ak;

    invoke-direct {v1, p1}, Lcom/yandex/metrica/impl/ob/ak;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 32
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->m()Lcom/yandex/metrica/impl/ob/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/t;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->m()Lcom/yandex/metrica/impl/ob/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/t;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/aq;->a:Ljava/util/ArrayList;

    new-instance v1, Lcom/yandex/metrica/impl/ob/an;

    .line 36
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/aq;->a()Lcom/yandex/metrica/impl/ob/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/v;->n()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/yandex/metrica/impl/ob/cq;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/cq;

    move-result-object v2

    new-instance v3, Lcom/yandex/metrica/impl/ob/ez;

    invoke-direct {v3}, Lcom/yandex/metrica/impl/ob/ez;-><init>()V

    invoke-direct {v1, p1, v2, v3}, Lcom/yandex/metrica/impl/ob/an;-><init>(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/cq;Lcom/yandex/metrica/impl/ob/ez;)V

    .line 34
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/aq;->a:Ljava/util/ArrayList;

    new-instance v1, Lcom/yandex/metrica/impl/ob/ai;

    invoke-direct {v1, p1}, Lcom/yandex/metrica/impl/ob/ai;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/yandex/metrica/impl/i;)Z
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/aq;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/ah;

    .line 45
    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/ah;->a(Lcom/yandex/metrica/impl/i;)Z

    goto :goto_0

    .line 47
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
