.class public Lcom/my/target/common/MyTargetActivity;
.super Landroid/app/Activity;
.source "MyTargetActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/common/MyTargetActivity$ActivityEngine;
    }
.end annotation


# static fields
.field public static activityEngine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;


# instance fields
.field private engine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

.field private rootLayout:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/my/target/common/MyTargetActivity;->engine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/common/MyTargetActivity;->engine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

    invoke-interface {v0}, Lcom/my/target/common/MyTargetActivity$ActivityEngine;->onActivityBackPressed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 32
    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 49
    invoke-virtual {p0}, Lcom/my/target/common/MyTargetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 50
    sget-object v1, Lcom/my/target/common/MyTargetActivity;->activityEngine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

    iput-object v1, p0, Lcom/my/target/common/MyTargetActivity;->engine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

    .line 51
    const/4 v1, 0x0

    sput-object v1, Lcom/my/target/common/MyTargetActivity;->activityEngine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

    .line 52
    iget-object v1, p0, Lcom/my/target/common/MyTargetActivity;->engine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 54
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-direct {v1, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/my/target/common/MyTargetActivity;->rootLayout:Landroid/widget/FrameLayout;

    .line 55
    iget-object v1, p0, Lcom/my/target/common/MyTargetActivity;->engine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

    iget-object v2, p0, Lcom/my/target/common/MyTargetActivity;->rootLayout:Landroid/widget/FrameLayout;

    invoke-interface {v1, p0, v0, v2}, Lcom/my/target/common/MyTargetActivity$ActivityEngine;->onActivityCreate(Lcom/my/target/common/MyTargetActivity;Landroid/content/Intent;Landroid/widget/FrameLayout;)V

    .line 56
    iget-object v0, p0, Lcom/my/target/common/MyTargetActivity;->rootLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/my/target/common/MyTargetActivity;->setContentView(Landroid/view/View;)V

    .line 62
    :goto_0
    return-void

    .line 60
    :cond_0
    invoke-virtual {p0}, Lcom/my/target/common/MyTargetActivity;->finish()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 107
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 108
    iget-object v0, p0, Lcom/my/target/common/MyTargetActivity;->engine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/my/target/common/MyTargetActivity;->engine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

    invoke-interface {v0}, Lcom/my/target/common/MyTargetActivity$ActivityEngine;->onActivityDestroy()V

    .line 112
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/my/target/common/MyTargetActivity;->engine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/common/MyTargetActivity;->engine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

    invoke-interface {v0, p1}, Lcom/my/target/common/MyTargetActivity$ActivityEngine;->onActivityOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    const/4 v0, 0x1

    .line 41
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 87
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 88
    iget-object v0, p0, Lcom/my/target/common/MyTargetActivity;->engine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/my/target/common/MyTargetActivity;->engine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

    invoke-interface {v0}, Lcom/my/target/common/MyTargetActivity$ActivityEngine;->onActivityPause()V

    .line 92
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 77
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 78
    iget-object v0, p0, Lcom/my/target/common/MyTargetActivity;->engine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/my/target/common/MyTargetActivity;->engine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

    invoke-interface {v0}, Lcom/my/target/common/MyTargetActivity$ActivityEngine;->onActivityResume()V

    .line 82
    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 67
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 68
    iget-object v0, p0, Lcom/my/target/common/MyTargetActivity;->engine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/my/target/common/MyTargetActivity;->engine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

    invoke-interface {v0}, Lcom/my/target/common/MyTargetActivity$ActivityEngine;->onActivityStart()V

    .line 72
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 97
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 98
    iget-object v0, p0, Lcom/my/target/common/MyTargetActivity;->engine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/my/target/common/MyTargetActivity;->engine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

    invoke-interface {v0}, Lcom/my/target/common/MyTargetActivity$ActivityEngine;->onActivityStop()V

    .line 102
    :cond_0
    return-void
.end method
