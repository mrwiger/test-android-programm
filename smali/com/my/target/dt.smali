.class public final Lcom/my/target/dt;
.super Lcom/my/target/d;
.source "InterstitialSliderAdResponseParser.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/my/target/d",
        "<",
        "Lcom/my/target/dw;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/my/target/d;-><init>()V

    .line 34
    return-void
.end method

.method public static newParser()Lcom/my/target/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/my/target/d",
            "<",
            "Lcom/my/target/dw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    new-instance v0, Lcom/my/target/dt;

    invoke-direct {v0}, Lcom/my/target/dt;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;Lcom/my/target/ae;Lcom/my/target/ak;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/ak;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 24
    check-cast p3, Lcom/my/target/dw;

    .line 1043
    invoke-virtual {p0, p1, p5}, Lcom/my/target/dt;->a(Ljava/lang/String;Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object v0

    .line 1044
    if-eqz v0, :cond_6

    .line 1049
    invoke-virtual {p4}, Lcom/my/target/b;->getFormat()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 1050
    if-eqz v0, :cond_6

    .line 1055
    if-nez p3, :cond_0

    .line 1057
    invoke-static {}, Lcom/my/target/dw;->l()Lcom/my/target/dw;

    move-result-object p3

    .line 1060
    :cond_0
    invoke-static {p2, p4, p5}, Lcom/my/target/core/parsers/g;->c(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/g;

    move-result-object v2

    invoke-virtual {v2, v0, p3}, Lcom/my/target/core/parsers/g;->a(Lorg/json/JSONObject;Lcom/my/target/dw;)V

    .line 1062
    const-string v2, "banners"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 1063
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-gtz v0, :cond_3

    :cond_1
    move-object p3, v1

    .line 1088
    :cond_2
    :goto_0
    return-object p3

    .line 1068
    :cond_3
    invoke-static {p3, p2, p4, p5}, Lcom/my/target/core/parsers/f;->a(Lcom/my/target/dw;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/f;

    move-result-object v3

    .line 1072
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v0, v4, :cond_5

    .line 1074
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 1076
    if-eqz v4, :cond_4

    .line 1078
    invoke-static {}, Lcom/my/target/core/models/banners/g;->newBanner()Lcom/my/target/core/models/banners/g;

    move-result-object v5

    .line 1079
    invoke-virtual {v3, v4, v5}, Lcom/my/target/core/parsers/f;->a(Lorg/json/JSONObject;Lcom/my/target/core/models/banners/g;)Z

    move-result v4

    .line 1080
    if-eqz v4, :cond_4

    .line 1082
    invoke-virtual {p3, v5}, Lcom/my/target/dw;->c(Lcom/my/target/core/models/banners/g;)V

    .line 1072
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1086
    :cond_5
    invoke-virtual {p3}, Lcom/my/target/dw;->getBannersCount()I

    move-result v0

    if-gtz v0, :cond_2

    :cond_6
    move-object p3, v1

    .line 24
    goto :goto_0
.end method
