.class public Lru/cn/ad/MiddleRollBanner;
.super Ljava/lang/Object;
.source "MiddleRollBanner.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lru/cn/ad/AdapterLoader$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/ad/MiddleRollBanner$Listener;
    }
.end annotation


# instance fields
.field private adCuePoints:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private adapter:Lru/cn/ad/AdAdapter;

.field private blockingPresenter:Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

.field private contentProgress:Lru/cn/ad/ContentProgress;

.field private contextPresenter:Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

.field private final factory:Lru/cn/ad/TagsAwareFactory;

.field private final handler:Landroid/os/Handler;

.field private isPresenting:Z

.field private listener:Lru/cn/ad/MiddleRollBanner$Listener;

.field private metadata:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/player/metadata/MetadataItem;",
            ">;"
        }
    .end annotation
.end field

.field private metadataAdPresenter:Lru/cn/ad/metadata/MetadataAdPresenter;

.field private final placeId:Ljava/lang/String;

.field private final placeLoader:Lru/cn/ad/PlaceLoader;

.field private settings:Lru/cn/ad/video/RenderingSettings;

.field private final viewContainer:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/String;Lru/cn/ad/AdAdapter$Factory;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "viewContainer"    # Landroid/view/ViewGroup;
    .param p3, "placeId"    # Ljava/lang/String;
    .param p4, "factory"    # Lru/cn/ad/AdAdapter$Factory;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p2, p0, Lru/cn/ad/MiddleRollBanner;->viewContainer:Landroid/view/ViewGroup;

    .line 60
    iput-object p3, p0, Lru/cn/ad/MiddleRollBanner;->placeId:Ljava/lang/String;

    .line 62
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lru/cn/ad/MiddleRollBanner;->handler:Landroid/os/Handler;

    .line 64
    new-instance v0, Lru/cn/ad/TagsAwareFactory;

    invoke-direct {v0, p4}, Lru/cn/ad/TagsAwareFactory;-><init>(Lru/cn/ad/AdAdapter$Factory;)V

    iput-object v0, p0, Lru/cn/ad/MiddleRollBanner;->factory:Lru/cn/ad/TagsAwareFactory;

    .line 66
    new-instance v0, Lru/cn/ad/PlaceLoader;

    iget-object v1, p0, Lru/cn/ad/MiddleRollBanner;->factory:Lru/cn/ad/TagsAwareFactory;

    invoke-direct {v0, p1, p3, v1}, Lru/cn/ad/PlaceLoader;-><init>(Landroid/content/Context;Ljava/lang/String;Lru/cn/ad/AdAdapter$Factory;)V

    iput-object v0, p0, Lru/cn/ad/MiddleRollBanner;->placeLoader:Lru/cn/ad/PlaceLoader;

    .line 67
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner;->placeLoader:Lru/cn/ad/PlaceLoader;

    invoke-virtual {v0, p0}, Lru/cn/ad/PlaceLoader;->setListener(Lru/cn/ad/AdapterLoader$Listener;)V

    .line 68
    return-void
.end method

.method static synthetic access$002(Lru/cn/ad/MiddleRollBanner;Z)Z
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/MiddleRollBanner;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lru/cn/ad/MiddleRollBanner;->isPresenting:Z

    return p1
.end method

.method static synthetic access$100(Lru/cn/ad/MiddleRollBanner;)Lru/cn/ad/MiddleRollBanner$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/MiddleRollBanner;

    .prologue
    .line 25
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner;->listener:Lru/cn/ad/MiddleRollBanner$Listener;

    return-object v0
.end method

.method private adCueTrigger()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 281
    iget-object v3, p0, Lru/cn/ad/MiddleRollBanner;->adCuePoints:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lru/cn/ad/MiddleRollBanner;->adCuePoints:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 292
    :cond_0
    :goto_0
    return v2

    .line 284
    :cond_1
    iget-object v3, p0, Lru/cn/ad/MiddleRollBanner;->contentProgress:Lru/cn/ad/ContentProgress;

    invoke-interface {v3}, Lru/cn/ad/ContentProgress;->getCurrentPosition()I

    move-result v1

    .line 286
    .local v1, "position":I
    invoke-direct {p0, v1}, Lru/cn/ad/MiddleRollBanner;->nextCue(I)Ljava/lang/Integer;

    move-result-object v0

    .line 287
    .local v0, "nearestAdCuePoint":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 290
    iget-object v2, p0, Lru/cn/ad/MiddleRollBanner;->adCuePoints:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 292
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private static currentMetadata(Ljava/util/List;J)Lru/cn/player/metadata/MetadataItem;
    .locals 5
    .param p1, "dateTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/player/metadata/MetadataItem;",
            ">;J)",
            "Lru/cn/player/metadata/MetadataItem;"
        }
    .end annotation

    .prologue
    .line 242
    .local p0, "metadata":Ljava/util/List;, "Ljava/util/List<Lru/cn/player/metadata/MetadataItem;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/player/metadata/MetadataItem;

    .line 243
    .local v0, "item":Lru/cn/player/metadata/MetadataItem;
    iget-wide v2, v0, Lru/cn/player/metadata/MetadataItem;->startTimeMs:J

    cmp-long v2, p1, v2

    if-ltz v2, :cond_0

    iget-wide v2, v0, Lru/cn/player/metadata/MetadataItem;->endTimeMs:J

    cmp-long v2, p1, v2

    if-gez v2, :cond_0

    .line 247
    .end local v0    # "item":Lru/cn/player/metadata/MetadataItem;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hideMetadata()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 251
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner;->metadataAdPresenter:Lru/cn/ad/metadata/MetadataAdPresenter;

    if-eqz v0, :cond_0

    .line 252
    iput-boolean v2, p0, Lru/cn/ad/MiddleRollBanner;->isPresenting:Z

    .line 253
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner;->metadataAdPresenter:Lru/cn/ad/metadata/MetadataAdPresenter;

    invoke-virtual {v0}, Lru/cn/ad/metadata/MetadataAdPresenter;->hide()V

    .line 254
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/ad/MiddleRollBanner;->metadataAdPresenter:Lru/cn/ad/metadata/MetadataAdPresenter;

    .line 255
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner;->viewContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 257
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner;->listener:Lru/cn/ad/MiddleRollBanner$Listener;

    invoke-interface {v0, v2}, Lru/cn/ad/MiddleRollBanner$Listener;->adCompleted(Z)V

    .line 259
    :cond_0
    return-void
.end method

.method private metadataTrigger()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 215
    iget-object v2, p0, Lru/cn/ad/MiddleRollBanner;->metadata:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lru/cn/ad/MiddleRollBanner;->metadata:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 237
    :cond_0
    :goto_0
    return v1

    .line 218
    :cond_1
    iget-object v2, p0, Lru/cn/ad/MiddleRollBanner;->contentProgress:Lru/cn/ad/ContentProgress;

    if-eqz v2, :cond_0

    .line 221
    iget-object v2, p0, Lru/cn/ad/MiddleRollBanner;->metadata:Ljava/util/List;

    iget-object v3, p0, Lru/cn/ad/MiddleRollBanner;->contentProgress:Lru/cn/ad/ContentProgress;

    invoke-interface {v3}, Lru/cn/ad/ContentProgress;->getDateTime()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lru/cn/ad/MiddleRollBanner;->currentMetadata(Ljava/util/List;J)Lru/cn/player/metadata/MetadataItem;

    move-result-object v0

    .line 222
    .local v0, "item":Lru/cn/player/metadata/MetadataItem;
    if-nez v0, :cond_2

    .line 223
    iget-object v2, p0, Lru/cn/ad/MiddleRollBanner;->metadataAdPresenter:Lru/cn/ad/metadata/MetadataAdPresenter;

    if-eqz v2, :cond_0

    .line 224
    invoke-direct {p0}, Lru/cn/ad/MiddleRollBanner;->hideMetadata()V

    goto :goto_0

    .line 231
    :cond_2
    iget-boolean v2, p0, Lru/cn/ad/MiddleRollBanner;->isPresenting:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lru/cn/ad/MiddleRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    if-nez v2, :cond_0

    .line 235
    :cond_3
    invoke-direct {p0, v0}, Lru/cn/ad/MiddleRollBanner;->showMetadata(Lru/cn/player/metadata/MetadataItem;)V

    .line 237
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private nextCue(I)Ljava/lang/Integer;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 296
    iget-object v1, p0, Lru/cn/ad/MiddleRollBanner;->adCuePoints:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 297
    .local v0, "point":Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-le p1, v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit16 v2, v2, 0x4e20

    if-ge p1, v2, :cond_0

    .line 302
    .end local v0    # "point":Ljava/lang/Integer;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private populateCuePoints()V
    .locals 8

    .prologue
    .line 164
    iget-object v4, p0, Lru/cn/ad/MiddleRollBanner;->adCuePoints:Ljava/util/List;

    if-eqz v4, :cond_1

    .line 188
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    iget-object v4, p0, Lru/cn/ad/MiddleRollBanner;->contentProgress:Lru/cn/ad/ContentProgress;

    invoke-interface {v4}, Lru/cn/ad/ContentProgress;->getDuration()I

    move-result v0

    .line 168
    .local v0, "duration":I
    if-lez v0, :cond_0

    .line 173
    iget-object v4, p0, Lru/cn/ad/MiddleRollBanner;->placeId:Ljava/lang/String;

    invoke-static {v4}, Lru/cn/ad/AdsManager;->getStoredPlace(Ljava/lang/String;)Lru/cn/api/money_miner/replies/AdPlace;

    move-result-object v2

    .line 174
    .local v2, "place":Lru/cn/api/money_miner/replies/AdPlace;
    if-eqz v2, :cond_0

    .line 178
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lru/cn/ad/MiddleRollBanner;->adCuePoints:Ljava/util/List;

    .line 180
    iget v4, v2, Lru/cn/api/money_miner/replies/AdPlace;->requestDelay:I

    const/16 v5, 0x258

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    mul-int/lit16 v1, v4, 0x3e8

    .line 181
    .local v1, "frequencyMs":I
    move v3, v1

    .line 182
    .local v3, "positionMs":I
    :goto_1
    if-ge v3, v0, :cond_2

    .line 183
    iget-object v4, p0, Lru/cn/ad/MiddleRollBanner;->adCuePoints:Ljava/util/List;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    add-int/2addr v3, v1

    goto :goto_1

    .line 187
    :cond_2
    iget-object v4, p0, Lru/cn/ad/MiddleRollBanner;->handler:Landroid/os/Handler;

    const/4 v5, 0x1

    const-wide/16 v6, 0x3e8

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private presenter()Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner;->contentProgress:Lru/cn/ad/ContentProgress;

    invoke-interface {v0}, Lru/cn/ad/ContentProgress;->allowBlocking()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner;->blockingPresenter:Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner;->blockingPresenter:Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    .line 351
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner;->contextPresenter:Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    goto :goto_0
.end method

.method private show(Z)V
    .locals 4
    .param p1, "blocking"    # Z

    .prologue
    .line 306
    invoke-direct {p0}, Lru/cn/ad/MiddleRollBanner;->presenter()Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    move-result-object v0

    .line 308
    .local v0, "presenter":Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;
    iget-object v1, p0, Lru/cn/ad/MiddleRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    new-instance v2, Lru/cn/ad/MiddleRollBanner$1;

    invoke-direct {v2, p0, p1, v0}, Lru/cn/ad/MiddleRollBanner$1;-><init>(Lru/cn/ad/MiddleRollBanner;ZLru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;)V

    invoke-virtual {v1, v2}, Lru/cn/ad/AdAdapter;->setListener(Lru/cn/ad/AdAdapter$Listener;)V

    .line 336
    iget-object v1, p0, Lru/cn/ad/MiddleRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    instance-of v1, v1, Lru/cn/ad/video/VideoAdAdapter;

    if-eqz v1, :cond_1

    .line 337
    iget-object v1, p0, Lru/cn/ad/MiddleRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    check-cast v1, Lru/cn/ad/video/VideoAdAdapter;

    iget-object v2, p0, Lru/cn/ad/MiddleRollBanner;->settings:Lru/cn/ad/video/RenderingSettings;

    invoke-virtual {v1, v2}, Lru/cn/ad/video/VideoAdAdapter;->setRenderingSettings(Lru/cn/ad/video/RenderingSettings;)V

    .line 343
    :cond_0
    :goto_0
    iget-object v1, p0, Lru/cn/ad/MiddleRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    new-instance v2, Lru/cn/ad/AdEventReporter;

    iget-object v3, p0, Lru/cn/ad/MiddleRollBanner;->placeId:Ljava/lang/String;

    invoke-direct {v2, v3}, Lru/cn/ad/AdEventReporter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lru/cn/ad/AdAdapter;->setReporter(Lru/cn/ad/AdEventReporter;)V

    .line 344
    iget-object v1, p0, Lru/cn/ad/MiddleRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    invoke-virtual {v1}, Lru/cn/ad/AdAdapter;->show()V

    .line 345
    return-void

    .line 339
    :cond_1
    iget-object v1, p0, Lru/cn/ad/MiddleRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    instance-of v1, v1, Lru/cn/ad/natives/adapters/NativeAdAdapter;

    if-eqz v1, :cond_0

    .line 340
    iget-object v1, p0, Lru/cn/ad/MiddleRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    check-cast v1, Lru/cn/ad/natives/adapters/NativeAdAdapter;

    invoke-virtual {v1, v0}, Lru/cn/ad/natives/adapters/NativeAdAdapter;->setPresenter(Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;)V

    goto :goto_0
.end method

.method private showMetadata(Lru/cn/player/metadata/MetadataItem;)V
    .locals 3
    .param p1, "item"    # Lru/cn/player/metadata/MetadataItem;

    .prologue
    const/4 v2, 0x0

    .line 262
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner;->metadataAdPresenter:Lru/cn/ad/metadata/MetadataAdPresenter;

    if-eqz v0, :cond_2

    .line 263
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner;->metadataAdPresenter:Lru/cn/ad/metadata/MetadataAdPresenter;

    iget-object v0, v0, Lru/cn/ad/metadata/MetadataAdPresenter;->item:Lru/cn/player/metadata/MetadataItem;

    iget-object v0, v0, Lru/cn/player/metadata/MetadataItem;->uniqueId:Ljava/lang/String;

    iget-object v1, p1, Lru/cn/player/metadata/MetadataItem;->uniqueId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 278
    :cond_0
    :goto_0
    return-void

    .line 266
    :cond_1
    invoke-direct {p0}, Lru/cn/ad/MiddleRollBanner;->hideMetadata()V

    .line 269
    :cond_2
    iget-boolean v0, p0, Lru/cn/ad/MiddleRollBanner;->isPresenting:Z

    if-nez v0, :cond_0

    .line 272
    new-instance v0, Lru/cn/ad/metadata/MetadataAdPresenter;

    iget-object v1, p0, Lru/cn/ad/MiddleRollBanner;->viewContainer:Landroid/view/ViewGroup;

    invoke-direct {v0, v1, p1}, Lru/cn/ad/metadata/MetadataAdPresenter;-><init>(Landroid/view/ViewGroup;Lru/cn/player/metadata/MetadataItem;)V

    iput-object v0, p0, Lru/cn/ad/MiddleRollBanner;->metadataAdPresenter:Lru/cn/ad/metadata/MetadataAdPresenter;

    .line 273
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner;->metadataAdPresenter:Lru/cn/ad/metadata/MetadataAdPresenter;

    invoke-virtual {v0}, Lru/cn/ad/metadata/MetadataAdPresenter;->show()V

    .line 274
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner;->viewContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 275
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/ad/MiddleRollBanner;->isPresenting:Z

    .line 277
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner;->listener:Lru/cn/ad/MiddleRollBanner$Listener;

    iget-object v1, p0, Lru/cn/ad/MiddleRollBanner;->metadataAdPresenter:Lru/cn/ad/metadata/MetadataAdPresenter;

    invoke-virtual {v1}, Lru/cn/ad/metadata/MetadataAdPresenter;->presentingView()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lru/cn/ad/MiddleRollBanner$Listener;->adStarted(ZLandroid/view/View;)V

    goto :goto_0
.end method

.method private trackProgressChange()V
    .locals 5

    .prologue
    .line 191
    iget-object v2, p0, Lru/cn/ad/MiddleRollBanner;->contentProgress:Lru/cn/ad/ContentProgress;

    if-nez v2, :cond_1

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    iget-object v2, p0, Lru/cn/ad/MiddleRollBanner;->contentProgress:Lru/cn/ad/ContentProgress;

    invoke-interface {v2}, Lru/cn/ad/ContentProgress;->allowBlocking()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lru/cn/ad/MiddleRollBanner;->blockingPresenter:Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    if-eqz v2, :cond_4

    const/4 v0, 0x1

    .line 196
    .local v0, "blockingPresenting":Z
    :goto_1
    invoke-direct {p0}, Lru/cn/ad/MiddleRollBanner;->metadataTrigger()Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v0, :cond_0

    .line 199
    :cond_2
    invoke-direct {p0}, Lru/cn/ad/MiddleRollBanner;->adCueTrigger()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 202
    const/4 v1, 0x0

    .line 203
    .local v1, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lru/cn/ad/MiddleRollBanner;->blockingPresenter:Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    if-eqz v2, :cond_3

    .line 204
    new-instance v1, Ljava/util/HashMap;

    .end local v1    # "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 205
    .restart local v1    # "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "blocking"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    :cond_3
    sget-object v2, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_OPPORTUNITY:Lru/cn/domain/statistics/inetra/AdvEvent;

    iget-object v3, p0, Lru/cn/ad/MiddleRollBanner;->placeId:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v2, v3, v4, v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->advEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/lang/String;Lru/cn/api/money_miner/replies/AdSystem;Ljava/util/Map;)V

    .line 210
    iget-object v2, p0, Lru/cn/ad/MiddleRollBanner;->placeLoader:Lru/cn/ad/PlaceLoader;

    invoke-virtual {v2}, Lru/cn/ad/PlaceLoader;->load()V

    goto :goto_0

    .line 194
    .end local v0    # "blockingPresenting":Z
    .end local v1    # "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 116
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 117
    iput-object v1, p0, Lru/cn/ad/MiddleRollBanner;->adCuePoints:Ljava/util/List;

    .line 118
    invoke-virtual {p0}, Lru/cn/ad/MiddleRollBanner;->dismiss()V

    .line 119
    return-void
.end method

.method public dismiss()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 104
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/ad/MiddleRollBanner;->isPresenting:Z

    .line 105
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    invoke-virtual {v0, v1}, Lru/cn/ad/AdAdapter;->setListener(Lru/cn/ad/AdAdapter$Listener;)V

    .line 108
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    invoke-virtual {v0}, Lru/cn/ad/AdAdapter;->destroy()V

    .line 109
    iput-object v1, p0, Lru/cn/ad/MiddleRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    .line 112
    :cond_0
    invoke-direct {p0}, Lru/cn/ad/MiddleRollBanner;->hideMetadata()V

    .line 113
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v0, 0x1

    .line 72
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 83
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 74
    :pswitch_0
    iget-object v1, p0, Lru/cn/ad/MiddleRollBanner;->adCuePoints:Ljava/util/List;

    if-nez v1, :cond_0

    .line 75
    invoke-direct {p0}, Lru/cn/ad/MiddleRollBanner;->populateCuePoints()V

    .line 78
    :cond_0
    invoke-direct {p0}, Lru/cn/ad/MiddleRollBanner;->trackProgressChange()V

    .line 79
    iget-object v1, p0, Lru/cn/ad/MiddleRollBanner;->handler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 72
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public isPresenting()Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lru/cn/ad/MiddleRollBanner;->isPresenting:Z

    return v0
.end method

.method public onError()V
    .locals 0

    .prologue
    .line 101
    return-void
.end method

.method public onLoaded(Lru/cn/ad/AdAdapter;)V
    .locals 2
    .param p1, "adAdapter"    # Lru/cn/ad/AdAdapter;

    .prologue
    .line 88
    iget-object v1, p0, Lru/cn/ad/MiddleRollBanner;->contentProgress:Lru/cn/ad/ContentProgress;

    if-nez v1, :cond_0

    .line 96
    :goto_0
    return-void

    .line 91
    :cond_0
    iput-object p1, p0, Lru/cn/ad/MiddleRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    .line 93
    iget-object v1, p0, Lru/cn/ad/MiddleRollBanner;->contentProgress:Lru/cn/ad/ContentProgress;

    invoke-interface {v1}, Lru/cn/ad/ContentProgress;->allowBlocking()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lru/cn/ad/MiddleRollBanner;->blockingPresenter:Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 95
    .local v0, "blocking":Z
    :goto_1
    invoke-direct {p0, v0}, Lru/cn/ad/MiddleRollBanner;->show(Z)V

    goto :goto_0

    .line 93
    .end local v0    # "blocking":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setBlockingPresenter(Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;)V
    .locals 0
    .param p1, "presenter"    # Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    .prologue
    .line 130
    iput-object p1, p0, Lru/cn/ad/MiddleRollBanner;->blockingPresenter:Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    .line 131
    return-void
.end method

.method public setContentProgress(Lru/cn/ad/ContentProgress;)V
    .locals 0
    .param p1, "progress"    # Lru/cn/ad/ContentProgress;

    .prologue
    .line 150
    iput-object p1, p0, Lru/cn/ad/MiddleRollBanner;->contentProgress:Lru/cn/ad/ContentProgress;

    .line 152
    if-nez p1, :cond_0

    .line 153
    invoke-virtual {p0}, Lru/cn/ad/MiddleRollBanner;->destroy()V

    .line 157
    :goto_0
    return-void

    .line 155
    :cond_0
    invoke-direct {p0}, Lru/cn/ad/MiddleRollBanner;->populateCuePoints()V

    goto :goto_0
.end method

.method public setContextPresenter(Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;)V
    .locals 0
    .param p1, "presenter"    # Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    .prologue
    .line 126
    iput-object p1, p0, Lru/cn/ad/MiddleRollBanner;->contextPresenter:Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    .line 127
    return-void
.end method

.method public setListener(Lru/cn/ad/MiddleRollBanner$Listener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/ad/MiddleRollBanner$Listener;

    .prologue
    .line 122
    iput-object p1, p0, Lru/cn/ad/MiddleRollBanner;->listener:Lru/cn/ad/MiddleRollBanner$Listener;

    .line 123
    return-void
.end method

.method public setMetaTags(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 138
    .local p1, "metaTags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner;->factory:Lru/cn/ad/TagsAwareFactory;

    invoke-virtual {v0, p1}, Lru/cn/ad/TagsAwareFactory;->setTags(Ljava/util/List;)V

    .line 139
    return-void
.end method

.method public setMetadata(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/player/metadata/MetadataItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 142
    .local p1, "metadata":Ljava/util/List;, "Ljava/util/List<Lru/cn/player/metadata/MetadataItem;>;"
    iput-object p1, p0, Lru/cn/ad/MiddleRollBanner;->metadata:Ljava/util/List;

    .line 143
    iget-object v0, p0, Lru/cn/ad/MiddleRollBanner;->metadataAdPresenter:Lru/cn/ad/metadata/MetadataAdPresenter;

    if-eqz v0, :cond_0

    .line 144
    invoke-direct {p0}, Lru/cn/ad/MiddleRollBanner;->hideMetadata()V

    .line 146
    :cond_0
    invoke-direct {p0}, Lru/cn/ad/MiddleRollBanner;->trackProgressChange()V

    .line 147
    return-void
.end method
