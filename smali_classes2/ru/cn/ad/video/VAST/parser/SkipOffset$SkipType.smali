.class public final enum Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;
.super Ljava/lang/Enum;
.source "SkipOffset.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/ad/video/VAST/parser/SkipOffset;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SkipType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;

.field public static final enum SkipTypePercent:Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;

.field public static final enum SkipTypeTime:Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 10
    new-instance v0, Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;

    const-string v1, "SkipTypeTime"

    invoke-direct {v0, v1, v2}, Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;->SkipTypeTime:Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;

    .line 11
    new-instance v0, Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;

    const-string v1, "SkipTypePercent"

    invoke-direct {v0, v1, v3}, Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;->SkipTypePercent:Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;

    .line 9
    const/4 v0, 0x2

    new-array v0, v0, [Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;

    sget-object v1, Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;->SkipTypeTime:Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;

    aput-object v1, v0, v2

    sget-object v1, Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;->SkipTypePercent:Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;

    aput-object v1, v0, v3

    sput-object v0, Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;->$VALUES:[Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 9
    const-class v0, Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;

    return-object v0
.end method

.method public static values()[Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;->$VALUES:[Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;

    invoke-virtual {v0}, [Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/ad/video/VAST/parser/SkipOffset$SkipType;

    return-object v0
.end method
