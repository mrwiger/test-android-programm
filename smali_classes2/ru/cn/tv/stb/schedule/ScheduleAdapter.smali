.class public Lru/cn/tv/stb/schedule/ScheduleAdapter;
.super Landroid/widget/SimpleCursorAdapter;
.source "ScheduleAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;
    }
.end annotation


# static fields
.field private static from:[Ljava/lang/String;

.field private static to:[I


# instance fields
.field private isKidsMode:Z

.field private launcherIsInstall:Z

.field private layout:I

.field private textColor:Landroid/content/res/ColorStateList;

.field private textColorOnAir:Landroid/content/res/ColorStateList;

.field private textColorWithRecord:Landroid/content/res/ColorStateList;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    new-array v0, v1, [Ljava/lang/String;

    sput-object v0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->from:[Ljava/lang/String;

    .line 30
    new-array v0, v1, [I

    sput-object v0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->to:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "launcherIsInstall"    # Z

    .prologue
    const/4 v2, 0x0

    .line 42
    sget-object v4, Lru/cn/tv/stb/schedule/ScheduleAdapter;->from:[Ljava/lang/String;

    sget-object v5, Lru/cn/tv/stb/schedule/ScheduleAdapter;->to:[I

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v6, v2

    invoke-direct/range {v0 .. v6}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 37
    iput-boolean v2, p0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->isKidsMode:Z

    .line 38
    iput-boolean v2, p0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->launcherIsInstall:Z

    .line 44
    const/4 v0, 0x3

    new-array v8, v0, [I

    fill-array-data v8, :array_0

    .line 49
    .local v8, "textSizeAttr":[I
    invoke-virtual {p1, v8}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v7

    .line 51
    .local v7, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v7, v2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->textColor:Landroid/content/res/ColorStateList;

    .line 52
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->textColorOnAir:Landroid/content/res/ColorStateList;

    .line 53
    const/4 v0, 0x2

    invoke-virtual {v7, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->textColorWithRecord:Landroid/content/res/ColorStateList;

    .line 55
    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    .line 57
    const v0, 0x7f040189

    invoke-static {p1, v0}, Lru/cn/utils/Utils;->resolveResourse(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->layout:I

    .line 58
    invoke-static {p1}, Lru/cn/domain/KidsObject;->isKidsMode(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->isKidsMode:Z

    .line 59
    iput-boolean p3, p0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->launcherIsInstall:Z

    .line 60
    return-void

    .line 44
    nop

    :array_0
    .array-data 4
        0x7f04018a
        0x7f04018b
        0x7f04018c
    .end array-data
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 16
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 89
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;

    .local v5, "holder":Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;
    move-object/from16 v3, p3

    .line 91
    check-cast v3, Lru/cn/api/provider/cursor/ScheduleItemCursor;

    .line 93
    .local v3, "c":Lru/cn/api/provider/cursor/ScheduleItemCursor;
    invoke-virtual {v3}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getTime()J

    move-result-wide v10

    .line 94
    .local v10, "time":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 96
    .local v6, "currentTimeMs":J
    invoke-virtual {v3}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->hasRecords()Z

    move-result v12

    if-nez v12, :cond_4

    .line 97
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->isKidsMode:Z

    if-eqz v12, :cond_3

    .line 98
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 99
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    const v13, 0x7f080257

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 100
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f06005e

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 121
    :goto_0
    invoke-virtual {v3, v6, v7}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->isOnTime(J)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 122
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->onAirIndicator:Landroid/view/View;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/view/View;->setVisibility(I)V

    .line 123
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->isKidsMode:Z

    if-eqz v12, :cond_6

    .line 124
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->onAirIndicator:Landroid/view/View;

    const v13, 0x7f080256

    invoke-virtual {v12, v13}, Landroid/view/View;->setBackgroundResource(I)V

    .line 125
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 141
    :goto_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 142
    .local v4, "calendar":Ljava/util/Calendar;
    const-wide/16 v12, 0x3e8

    mul-long/2addr v12, v10

    invoke-virtual {v4, v12, v13}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 144
    const-string v12, "HH:mm"

    invoke-static {v4, v12}, Lru/cn/utils/Utils;->format(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 145
    .local v8, "s":Ljava/lang/String;
    new-instance v9, Landroid/text/SpannableString;

    invoke-direct {v9, v8}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 147
    .local v9, "ss":Landroid/text/SpannableString;
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->isKidsMode:Z

    if-eqz v12, :cond_0

    .line 148
    new-instance v12, Landroid/text/style/TextAppearanceSpan;

    const v13, 0x7f0f01c5

    move-object/from16 v0, p2

    invoke-direct {v12, v0, v13}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    const/4 v13, 0x0

    const/4 v14, 0x1

    const/16 v15, 0x21

    invoke-virtual {v9, v12, v13, v14, v15}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 150
    new-instance v12, Landroid/text/style/TextAppearanceSpan;

    const v13, 0x7f0f01c6

    move-object/from16 v0, p2

    invoke-direct {v12, v0, v13}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    const/4 v13, 0x2

    const/4 v14, 0x5

    const/16 v15, 0x21

    invoke-virtual {v9, v12, v13, v14, v15}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 154
    :cond_0
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    invoke-virtual {v12, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v3}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getTitle()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->launcherIsInstall:Z

    if-eqz v12, :cond_9

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->isKidsMode:Z

    if-nez v12, :cond_9

    const/4 v2, 0x1

    .line 159
    .local v2, "allowNotification":Z
    :goto_2
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->notificationIndicator:Landroid/widget/ImageView;

    if-eqz v12, :cond_2

    .line 160
    const-wide/16 v12, 0x3e8

    mul-long/2addr v12, v10

    cmp-long v12, v12, v6

    if-lez v12, :cond_c

    if-eqz v2, :cond_c

    .line 161
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->rootView:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->isActivated()Z

    move-result v12

    if-eqz v12, :cond_b

    .line 162
    invoke-virtual {v3}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getTelecastId()J

    move-result-wide v12

    move-object/from16 v0, p2

    invoke-static {v0, v12, v13}, Lru/cn/api/provider/NotificationIdsStorage;->isNotificationIdExist(Landroid/content/Context;J)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 163
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->notificationIndicator:Landroid/widget/ImageView;

    const v13, 0x7f08017b

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 171
    :goto_3
    invoke-virtual {v3}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getTelecastId()J

    move-result-wide v12

    move-object/from16 v0, p2

    invoke-static {v0, v12, v13}, Lru/cn/api/provider/NotificationIdsStorage;->isNotificationIdExist(Landroid/content/Context;J)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 172
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->notificationIndicator:Landroid/widget/ImageView;

    const v13, 0x7f08017b

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 175
    :cond_1
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->notificationIndicator:Landroid/widget/ImageView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 180
    :cond_2
    :goto_4
    return-void

    .line 102
    .end local v2    # "allowNotification":Z
    .end local v4    # "calendar":Ljava/util/Calendar;
    .end local v8    # "s":Ljava/lang/String;
    .end local v9    # "ss":Landroid/text/SpannableString;
    :cond_3
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->textColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 103
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->textColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 104
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v13, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v13

    and-int/lit8 v13, v13, -0x9

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setPaintFlags(I)V

    goto/16 :goto_0

    .line 108
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->isKidsMode:Z

    if-eqz v12, :cond_5

    .line 109
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 110
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    const v13, 0x7f080254

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 111
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    .line 112
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f06004a

    .line 113
    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v13

    .line 111
    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto/16 :goto_0

    .line 115
    :cond_5
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->textColorWithRecord:Landroid/content/res/ColorStateList;

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 116
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->textColorWithRecord:Landroid/content/res/ColorStateList;

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 117
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v13, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v13

    or-int/lit8 v13, v13, 0x8

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setPaintFlags(I)V

    goto/16 :goto_0

    .line 127
    :cond_6
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->textColorOnAir:Landroid/content/res/ColorStateList;

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 128
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->textColorOnAir:Landroid/content/res/ColorStateList;

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 129
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    sget-object v13, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 130
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->title:Landroid/widget/TextView;

    sget-object v13, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto/16 :goto_1

    .line 134
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->isKidsMode:Z

    if-nez v12, :cond_8

    .line 135
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    sget-object v13, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 136
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->title:Landroid/widget/TextView;

    sget-object v13, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 138
    :cond_8
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->onAirIndicator:Landroid/view/View;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 157
    .restart local v4    # "calendar":Ljava/util/Calendar;
    .restart local v8    # "s":Ljava/lang/String;
    .restart local v9    # "ss":Landroid/text/SpannableString;
    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 165
    .restart local v2    # "allowNotification":Z
    :cond_a
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->notificationIndicator:Landroid/widget/ImageView;

    const v13, 0x7f08017c

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_3

    .line 168
    :cond_b
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->notificationIndicator:Landroid/widget/ImageView;

    const v13, 0x7f08017c

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_3

    .line 177
    :cond_c
    iget-object v12, v5, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->notificationIndicator:Landroid/widget/ImageView;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_4
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 72
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 74
    .local v1, "inflater":Landroid/view/LayoutInflater;
    iget v3, p0, Lru/cn/tv/stb/schedule/ScheduleAdapter;->layout:I

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 76
    .local v2, "view":Landroid/view/View;
    new-instance v0, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;

    invoke-direct {v0, v5}, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;-><init>(Lru/cn/tv/stb/schedule/ScheduleAdapter$1;)V

    .line 77
    .local v0, "holder":Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;
    const v3, 0x7f090184

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v0, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->rootView:Landroid/view/View;

    .line 78
    const v3, 0x7f0901d4

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->time:Landroid/widget/TextView;

    .line 79
    const v3, 0x7f0901d7

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 80
    const v3, 0x7f090157

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v0, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->onAirIndicator:Landroid/view/View;

    .line 81
    const v3, 0x7f090150

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v0, Lru/cn/tv/stb/schedule/ScheduleAdapter$ViewHolder;->notificationIndicator:Landroid/widget/ImageView;

    .line 82
    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 84
    return-object v2
.end method
