.class public Lcom/my/target/bd;
.super Ljava/lang/Object;
.source "AdditionalDataParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/bd$a;
    }
.end annotation


# static fields
.field public static final dQ:I = 0x5


# instance fields
.field private final adConfig:Lcom/my/target/b;

.field private final bJ:Landroid/content/Context;

.field private final eo:Lcom/my/target/ae;


# direct methods
.method private constructor <init>(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/my/target/bd;->eo:Lcom/my/target/ae;

    .line 35
    iput-object p2, p0, Lcom/my/target/bd;->adConfig:Lcom/my/target/b;

    .line 36
    iput-object p3, p0, Lcom/my/target/bd;->bJ:Landroid/content/Context;

    .line 37
    return-void
.end method

.method public static a(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/bd;
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/my/target/bd;

    invoke-direct {v0, p0, p1, p2}, Lcom/my/target/bd;-><init>(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V

    return-object v0
.end method

.method private f(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 145
    invoke-static {p1}, Lcom/my/target/az;->y(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/my/target/az;->z(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/bd;->adConfig:Lcom/my/target/b;

    .line 146
    invoke-virtual {v1}, Lcom/my/target/b;->getSlotId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/az;->h(I)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/bd;->eo:Lcom/my/target/ae;

    .line 147
    invoke-virtual {v1}, Lcom/my/target/ae;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/az;->A(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/bd;->bJ:Landroid/content/Context;

    .line 148
    invoke-virtual {v0, v1}, Lcom/my/target/az;->e(Landroid/content/Context;)V

    .line 149
    return-void
.end method


# virtual methods
.method public c(Lorg/json/JSONObject;)Lcom/my/target/ae;
    .locals 14

    .prologue
    const/4 v0, 0x0

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    const-wide/16 v12, 0x0

    .line 41
    iget-object v1, p0, Lcom/my/target/bd;->eo:Lcom/my/target/ae;

    invoke-virtual {v1}, Lcom/my/target/ae;->G()I

    move-result v1

    .line 42
    const/4 v4, 0x5

    if-lt v1, v4, :cond_0

    .line 44
    const-string v1, "got additional data, but max redirects limit exceeded"

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 140
    :goto_0
    return-object v0

    .line 47
    :cond_0
    const-string v4, "id"

    iget-object v5, p0, Lcom/my/target/bd;->eo:Lcom/my/target/ae;

    invoke-virtual {v5}, Lcom/my/target/ae;->getId()I

    move-result v5

    invoke-virtual {p1, v4, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    .line 48
    const-string v5, "url"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 49
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 51
    const-string v1, "Required field"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No url in additionalData Id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/my/target/bd;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 54
    :cond_1
    invoke-static {v5}, Lcom/my/target/ae;->m(Ljava/lang/String;)Lcom/my/target/ae;

    move-result-object v6

    .line 55
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {v6, v0}, Lcom/my/target/ae;->a(I)V

    .line 56
    invoke-virtual {v6, v4}, Lcom/my/target/ae;->setId(I)V

    .line 57
    const-string v0, "doAfter"

    invoke-virtual {v6}, Lcom/my/target/ae;->y()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v6, v0}, Lcom/my/target/ae;->c(Z)V

    .line 58
    const-string v0, "doOnEmptyResponseFromId"

    invoke-virtual {v6}, Lcom/my/target/ae;->z()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v6, v0}, Lcom/my/target/ae;->c(I)V

    .line 59
    const-string v0, "isMidrollPoint"

    invoke-virtual {v6}, Lcom/my/target/ae;->A()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v7

    .line 60
    invoke-virtual {v6, v7}, Lcom/my/target/ae;->e(Z)V

    .line 61
    const-string v0, "allowCloseDelay"

    invoke-virtual {v6}, Lcom/my/target/ae;->getAllowCloseDelay()F

    move-result v1

    float-to-double v4, v1

    invoke-virtual {p1, v0, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-virtual {v6, v0}, Lcom/my/target/ae;->setAllowCloseDelay(F)V

    .line 63
    const-string v0, "allowClose"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 65
    const-string v0, "allowClose"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/my/target/ae;->a(Ljava/lang/Boolean;)V

    .line 67
    :cond_2
    const-string v0, "hasPause"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 69
    const-string v0, "hasPause"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/my/target/ae;->b(Ljava/lang/Boolean;)V

    .line 71
    :cond_3
    const-string v0, "allowSeek"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 73
    const-string v0, "allowSeek"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/my/target/ae;->c(Ljava/lang/Boolean;)V

    .line 75
    :cond_4
    const-string v0, "allowSkip"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 77
    const-string v0, "allowSkip"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/my/target/ae;->d(Ljava/lang/Boolean;)V

    .line 79
    :cond_5
    const-string v0, "allowTrackChange"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 81
    const-string v0, "allowTrackChange"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/my/target/ae;->e(Ljava/lang/Boolean;)V

    .line 84
    :cond_6
    const-string v0, "point"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 85
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v4

    if-eqz v4, :cond_a

    move-wide v0, v2

    .line 97
    :cond_7
    :goto_1
    const-string v4, "pointP"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 98
    invoke-static {v4, v5}, Ljava/lang/Double;->isNaN(D)Z

    move-result v8

    if-eqz v8, :cond_b

    move-wide v4, v2

    .line 109
    :cond_8
    :goto_2
    if-eqz v7, :cond_d

    .line 111
    cmpg-double v7, v0, v12

    if-gez v7, :cond_d

    cmpg-double v7, v4, v12

    if-gez v7, :cond_d

    .line 114
    const-wide/high16 v0, 0x4049000000000000L    # 50.0

    .line 118
    :goto_3
    double-to-float v2, v2

    invoke-virtual {v6, v2}, Lcom/my/target/ae;->setPoint(F)V

    .line 119
    double-to-float v0, v0

    invoke-virtual {v6, v0}, Lcom/my/target/ae;->setPointP(F)V

    .line 121
    const-string v0, "serviceStatistics"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 122
    if-eqz v1, :cond_c

    .line 124
    const/4 v0, 0x0

    :goto_4
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_c

    .line 126
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 127
    if-eqz v2, :cond_9

    .line 129
    const-string v3, "type"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 130
    const-string v4, "url"

    const-string v5, ""

    invoke-virtual {v2, v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 131
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 133
    invoke-static {v3, v2}, Lcom/my/target/aq;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/my/target/aq;

    move-result-object v2

    invoke-virtual {v6, v2}, Lcom/my/target/ae;->a(Lcom/my/target/aq;)V

    .line 124
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 91
    :cond_a
    cmpg-double v4, v0, v12

    if-gez v4, :cond_7

    .line 93
    const-string v4, "Bad value"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Wrong value "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " for point in additionalData object"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/my/target/bd;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 104
    :cond_b
    cmpg-double v8, v4, v12

    if-gez v8, :cond_8

    .line 106
    const-string v8, "Bad value"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Wrong value "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " for pointP in additionalData object"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v8, v9}, Lcom/my/target/bd;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 138
    :cond_c
    iget-object v0, p0, Lcom/my/target/bd;->eo:Lcom/my/target/ae;

    invoke-virtual {v0}, Lcom/my/target/ae;->F()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/my/target/ae;->e(Ljava/util/ArrayList;)V

    .line 139
    iget-object v0, p0, Lcom/my/target/bd;->eo:Lcom/my/target/ae;

    invoke-virtual {v0}, Lcom/my/target/ae;->E()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/my/target/ae;->f(Ljava/util/ArrayList;)V

    move-object v0, v6

    .line 140
    goto/16 :goto_0

    :cond_d
    move-wide v2, v0

    move-wide v0, v4

    goto/16 :goto_3
.end method
