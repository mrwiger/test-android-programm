.class Lru/cn/tv/billing/BillingViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "BillingViewModel.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private final loader:Lru/cn/mvvm/RxLoader;

.field private final privateOfficeUrl:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lru/cn/mvvm/RxLoader;Landroid/app/Application;)V
    .locals 1
    .param p1, "loader"    # Lru/cn/mvvm/RxLoader;
    .param p2, "application"    # Landroid/app/Application;

    .prologue
    .line 32
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 33
    iput-object p1, p0, Lru/cn/tv/billing/BillingViewModel;->loader:Lru/cn/mvvm/RxLoader;

    .line 34
    iput-object p2, p0, Lru/cn/tv/billing/BillingViewModel;->context:Landroid/content/Context;

    .line 36
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/billing/BillingViewModel;->privateOfficeUrl:Landroid/arch/lifecycle/MutableLiveData;

    .line 37
    return-void
.end method

.method private authorizationToken(J)Lio/reactivex/Observable;
    .locals 3
    .param p1, "contractorId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    new-instance v0, Lru/cn/tv/billing/BillingViewModel$$Lambda$4;

    invoke-direct {v0, p0, p1, p2}, Lru/cn/tv/billing/BillingViewModel$$Lambda$4;-><init>(Lru/cn/tv/billing/BillingViewModel;J)V

    .line 96
    invoke-static {v0}, Lio/reactivex/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 103
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 95
    return-object v0
.end method

.method static final synthetic lambda$officeBaseUrl$3$BillingViewModel(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 82
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 83
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 84
    const-string v1, "private_office_uri"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 85
    .local v0, "baseUrl":Ljava/lang/String;
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    .line 87
    return-object v0

    .line 90
    .end local v0    # "baseUrl":Ljava/lang/String;
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Missing private office URL"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method static final synthetic lambda$setBillingParams$0$BillingViewModel(Ljava/lang/String;JZZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "callback"    # Ljava/lang/String;
    .param p1, "channelId"    # J
    .param p3, "peersTvPlus"    # Z
    .param p4, "pinAuthorize"    # Z
    .param p5, "officeUrl"    # Ljava/lang/String;
    .param p6, "token"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 50
    new-instance v1, Lru/cn/domain/PrivateOffice$UriBuilder;

    invoke-direct {v1, p5}, Lru/cn/domain/PrivateOffice$UriBuilder;-><init>(Ljava/lang/String;)V

    .line 51
    invoke-virtual {v1, p0}, Lru/cn/domain/PrivateOffice$UriBuilder;->callback(Ljava/lang/String;)Lru/cn/domain/PrivateOffice$UriBuilder;

    move-result-object v0

    .line 53
    .local v0, "builder":Lru/cn/domain/PrivateOffice$UriBuilder;
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 54
    invoke-virtual {v0, p6}, Lru/cn/domain/PrivateOffice$UriBuilder;->token(Ljava/lang/String;)Lru/cn/domain/PrivateOffice$UriBuilder;

    .line 57
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-lez v1, :cond_2

    .line 58
    invoke-virtual {v0, p1, p2}, Lru/cn/domain/PrivateOffice$UriBuilder;->channel(J)Lru/cn/domain/PrivateOffice$UriBuilder;

    .line 65
    :cond_1
    :goto_0
    invoke-virtual {v0}, Lru/cn/domain/PrivateOffice$UriBuilder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 59
    :cond_2
    if-eqz p3, :cond_3

    .line 60
    invoke-virtual {v0}, Lru/cn/domain/PrivateOffice$UriBuilder;->peersTVPlus()Lru/cn/domain/PrivateOffice$UriBuilder;

    goto :goto_0

    .line 61
    :cond_3
    if-eqz p4, :cond_1

    .line 62
    invoke-virtual {v0}, Lru/cn/domain/PrivateOffice$UriBuilder;->pinAuthorize()Lru/cn/domain/PrivateOffice$UriBuilder;

    goto :goto_0
.end method

.method static final synthetic lambda$setBillingParams$2$BillingViewModel(Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "throwable"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 70
    const-string v0, "BillingViewModel"

    const-string v1, "BillingViewModel setBillingParams: "

    invoke-static {v0, v1, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 71
    return-void
.end method

.method private officeBaseUrl(J)Lio/reactivex/Observable;
    .locals 3
    .param p1, "contractorId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    invoke-static {p1, p2}, Lru/cn/api/provider/TvContentProviderContract;->contractor(J)Landroid/net/Uri;

    move-result-object v0

    .line 79
    .local v0, "contractorUri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/billing/BillingViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v1

    .line 80
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lru/cn/tv/billing/BillingViewModel$$Lambda$3;->$instance:Lio/reactivex/functions/Function;

    .line 81
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 79
    return-object v1
.end method


# virtual methods
.method final synthetic lambda$authorizationToken$4$BillingViewModel(J)Ljava/lang/String;
    .locals 3
    .param p1, "contractorId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 97
    iget-object v1, p0, Lru/cn/tv/billing/BillingViewModel;->context:Landroid/content/Context;

    invoke-static {v1, p1, p2}, Lru/cn/api/authorization/Authorization;->getAuthToken(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 98
    .local v0, "authToken":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 99
    const-string v0, ""

    .line 101
    .end local v0    # "authToken":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method final synthetic lambda$setBillingParams$1$BillingViewModel(Ljava/lang/String;)V
    .locals 1
    .param p1, "it"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lru/cn/tv/billing/BillingViewModel;->privateOfficeUrl:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v0, p1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method privateOfficeUrl()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lru/cn/tv/billing/BillingViewModel;->privateOfficeUrl:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method setBillingParams(JJZZLjava/lang/String;)V
    .locals 9
    .param p1, "contractorId"    # J
    .param p3, "channelId"    # J
    .param p5, "peersTvPlus"    # Z
    .param p6, "pinAuthorize"    # Z
    .param p7, "callback"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-virtual {p0}, Lru/cn/tv/billing/BillingViewModel;->unbindAll()V

    .line 47
    invoke-direct {p0, p1, p2}, Lru/cn/tv/billing/BillingViewModel;->officeBaseUrl(J)Lio/reactivex/Observable;

    move-result-object v7

    .line 48
    invoke-direct {p0, p1, p2}, Lru/cn/tv/billing/BillingViewModel;->authorizationToken(J)Lio/reactivex/Observable;

    move-result-object v8

    new-instance v0, Lru/cn/tv/billing/BillingViewModel$$Lambda$0;

    move-object/from16 v1, p7

    move-wide v2, p3

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lru/cn/tv/billing/BillingViewModel$$Lambda$0;-><init>(Ljava/lang/String;JZZ)V

    .line 46
    invoke-static {v7, v8, v0}, Lio/reactivex/Observable;->zip(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    .line 67
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lru/cn/tv/billing/BillingViewModel$$Lambda$1;

    invoke-direct {v1, p0}, Lru/cn/tv/billing/BillingViewModel$$Lambda$1;-><init>(Lru/cn/tv/billing/BillingViewModel;)V

    sget-object v2, Lru/cn/tv/billing/BillingViewModel$$Lambda$2;->$instance:Lio/reactivex/functions/Consumer;

    .line 68
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v6

    .line 73
    .local v6, "disposable":Lio/reactivex/disposables/Disposable;
    invoke-virtual {p0, v6}, Lru/cn/tv/billing/BillingViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 74
    return-void
.end method
