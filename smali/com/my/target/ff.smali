.class public final Lcom/my/target/ff;
.super Lcom/my/target/e;
.source "NativeAppwallAdResultProcessor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/my/target/e",
        "<",
        "Lcom/my/target/fg;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/my/target/e;-><init>()V

    .line 32
    return-void
.end method

.method public static e()Lcom/my/target/ff;
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/my/target/ff;

    invoke-direct {v0}, Lcom/my/target/ff;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Lcom/my/target/ak;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/ak;
    .locals 10

    .prologue
    .line 22
    check-cast p1, Lcom/my/target/fg;

    .line 1039
    invoke-virtual {p2}, Lcom/my/target/b;->getCachePeriod()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-virtual {p1}, Lcom/my/target/fg;->C()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/my/target/fg;->getRawData()Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1041
    invoke-static {p3}, Lcom/my/target/cf;->t(Landroid/content/Context;)Lcom/my/target/cf;

    move-result-object v0

    .line 1042
    invoke-virtual {p2}, Lcom/my/target/b;->getSlotId()I

    move-result v1

    .line 1043
    if-eqz v0, :cond_9

    .line 1045
    invoke-virtual {p1}, Lcom/my/target/fg;->getRawData()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1046
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/my/target/cf;->a(ILjava/lang/String;Z)Ljava/io/File;

    .line 1054
    :cond_0
    :goto_0
    invoke-virtual {p2}, Lcom/my/target/b;->isAutoLoadImages()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1056
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1058
    invoke-virtual {p1}, Lcom/my/target/fg;->R()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/i;

    .line 1060
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/i;->getStatusIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v3

    .line 1061
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/i;->getCoinsIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v4

    .line 1062
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/i;->getGotoAppIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v5

    .line 1063
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/i;->getIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v6

    .line 1064
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/i;->getLabelIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v7

    .line 1065
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/i;->getBubbleIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v8

    .line 1066
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/i;->getItemHighlightIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v9

    .line 1067
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/i;->getCrossNotifIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 1069
    if-eqz v3, :cond_2

    .line 1071
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1074
    :cond_2
    if-eqz v4, :cond_3

    .line 1076
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1079
    :cond_3
    if-eqz v5, :cond_4

    .line 1081
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1084
    :cond_4
    if-eqz v6, :cond_5

    .line 1086
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1089
    :cond_5
    if-eqz v7, :cond_6

    .line 1091
    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1094
    :cond_6
    if-eqz v8, :cond_7

    .line 1096
    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1099
    :cond_7
    if-eqz v9, :cond_8

    .line 1101
    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1104
    :cond_8
    if-eqz v0, :cond_1

    .line 1106
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1050
    :cond_9
    const-string v0, "unable to open disk cache and save data for slotId "

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1110
    :cond_a
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_b

    .line 1112
    invoke-static {v1}, Lcom/my/target/ch;->a(Ljava/util/List;)Lcom/my/target/ch;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/my/target/ch;->v(Landroid/content/Context;)V

    .line 22
    :cond_b
    return-object p1
.end method
