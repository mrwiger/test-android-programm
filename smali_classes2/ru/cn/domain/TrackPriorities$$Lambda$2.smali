.class final synthetic Lru/cn/domain/TrackPriorities$$Lambda$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/annimon/stream/function/Function;


# instance fields
.field private final arg$1:Ljava/util/Map;

.field private final arg$2:J


# direct methods
.method constructor <init>(Ljava/util/Map;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lru/cn/domain/TrackPriorities$$Lambda$2;->arg$1:Ljava/util/Map;

    iput-wide p2, p0, Lru/cn/domain/TrackPriorities$$Lambda$2;->arg$2:J

    return-void
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lru/cn/domain/TrackPriorities$$Lambda$2;->arg$1:Ljava/util/Map;

    iget-wide v2, p0, Lru/cn/domain/TrackPriorities$$Lambda$2;->arg$2:J

    check-cast p1, Ljava/lang/Long;

    invoke-static {v0, v2, v3, p1}, Lru/cn/domain/TrackPriorities;->lambda$byTimezones$2$TrackPriorities(Ljava/util/Map;JLjava/lang/Long;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
