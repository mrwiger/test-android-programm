.class final Lcom/google/obf/cd$a;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/cd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/cd$a$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/obf/ar;

.field private final b:Z

.field private final c:Z

.field private final d:Lcom/google/obf/dv;

.field private final e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/obf/du$b;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/obf/du$a;",
            ">;"
        }
    .end annotation
.end field

.field private g:[B

.field private h:I

.field private i:I

.field private j:J

.field private k:Z

.field private l:J

.field private m:Lcom/google/obf/cd$a$a;

.field private n:Lcom/google/obf/cd$a$a;

.field private o:Z

.field private p:J

.field private q:J

.field private r:Z


# direct methods
.method public constructor <init>(Lcom/google/obf/ar;ZZ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/obf/cd$a;->a:Lcom/google/obf/ar;

    .line 3
    iput-boolean p2, p0, Lcom/google/obf/cd$a;->b:Z

    .line 4
    iput-boolean p3, p0, Lcom/google/obf/cd$a;->c:Z

    .line 5
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/obf/cd$a;->e:Landroid/util/SparseArray;

    .line 6
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/obf/cd$a;->f:Landroid/util/SparseArray;

    .line 7
    new-instance v0, Lcom/google/obf/cd$a$a;

    invoke-direct {v0, v1}, Lcom/google/obf/cd$a$a;-><init>(Lcom/google/obf/cd$1;)V

    iput-object v0, p0, Lcom/google/obf/cd$a;->m:Lcom/google/obf/cd$a$a;

    .line 8
    new-instance v0, Lcom/google/obf/cd$a$a;

    invoke-direct {v0, v1}, Lcom/google/obf/cd$a$a;-><init>(Lcom/google/obf/cd$1;)V

    iput-object v0, p0, Lcom/google/obf/cd$a;->n:Lcom/google/obf/cd$a$a;

    .line 9
    new-instance v0, Lcom/google/obf/dv;

    invoke-direct {v0}, Lcom/google/obf/dv;-><init>()V

    iput-object v0, p0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    .line 10
    const/16 v0, 0x80

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/obf/cd$a;->g:[B

    .line 11
    invoke-virtual {p0}, Lcom/google/obf/cd$a;->b()V

    .line 12
    return-void
.end method

.method private a(I)V
    .locals 8

    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/google/obf/cd$a;->r:Z

    if-eqz v0, :cond_0

    const/4 v4, 0x1

    .line 125
    :goto_0
    iget-wide v0, p0, Lcom/google/obf/cd$a;->j:J

    iget-wide v2, p0, Lcom/google/obf/cd$a;->p:J

    sub-long/2addr v0, v2

    long-to-int v5, v0

    .line 126
    iget-object v1, p0, Lcom/google/obf/cd$a;->a:Lcom/google/obf/ar;

    iget-wide v2, p0, Lcom/google/obf/cd$a;->q:J

    const/4 v7, 0x0

    move v6, p1

    invoke-interface/range {v1 .. v7}, Lcom/google/obf/ar;->a(JIII[B)V

    .line 127
    return-void

    .line 124
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(JI)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 112
    iget v2, p0, Lcom/google/obf/cd$a;->i:I

    const/16 v3, 0x9

    if-eq v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/obf/cd$a;->c:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/obf/cd$a;->n:Lcom/google/obf/cd$a$a;

    iget-object v3, p0, Lcom/google/obf/cd$a;->m:Lcom/google/obf/cd$a$a;

    .line 113
    invoke-static {v2, v3}, Lcom/google/obf/cd$a$a;->a(Lcom/google/obf/cd$a$a;Lcom/google/obf/cd$a$a;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 114
    :cond_0
    iget-boolean v2, p0, Lcom/google/obf/cd$a;->o:Z

    if-eqz v2, :cond_1

    .line 115
    iget-wide v2, p0, Lcom/google/obf/cd$a;->j:J

    sub-long v2, p1, v2

    long-to-int v2, v2

    .line 116
    add-int/2addr v2, p3

    invoke-direct {p0, v2}, Lcom/google/obf/cd$a;->a(I)V

    .line 117
    :cond_1
    iget-wide v2, p0, Lcom/google/obf/cd$a;->j:J

    iput-wide v2, p0, Lcom/google/obf/cd$a;->p:J

    .line 118
    iget-wide v2, p0, Lcom/google/obf/cd$a;->l:J

    iput-wide v2, p0, Lcom/google/obf/cd$a;->q:J

    .line 119
    iput-boolean v0, p0, Lcom/google/obf/cd$a;->r:Z

    .line 120
    iput-boolean v1, p0, Lcom/google/obf/cd$a;->o:Z

    .line 121
    :cond_2
    iget-boolean v2, p0, Lcom/google/obf/cd$a;->r:Z

    iget v3, p0, Lcom/google/obf/cd$a;->i:I

    const/4 v4, 0x5

    if-eq v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/obf/cd$a;->b:Z

    if-eqz v3, :cond_4

    iget v3, p0, Lcom/google/obf/cd$a;->i:I

    if-ne v3, v1, :cond_4

    iget-object v3, p0, Lcom/google/obf/cd$a;->n:Lcom/google/obf/cd$a$a;

    .line 122
    invoke-virtual {v3}, Lcom/google/obf/cd$a$a;->b()Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    move v0, v1

    :cond_4
    or-int/2addr v0, v2

    iput-boolean v0, p0, Lcom/google/obf/cd$a;->r:Z

    .line 123
    return-void
.end method

.method public a(JIJ)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 22
    iput p3, p0, Lcom/google/obf/cd$a;->i:I

    .line 23
    iput-wide p4, p0, Lcom/google/obf/cd$a;->l:J

    .line 24
    iput-wide p1, p0, Lcom/google/obf/cd$a;->j:J

    .line 25
    iget-boolean v0, p0, Lcom/google/obf/cd$a;->b:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/obf/cd$a;->i:I

    if-eq v0, v2, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/obf/cd$a;->c:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/obf/cd$a;->i:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/google/obf/cd$a;->i:I

    if-eq v0, v2, :cond_1

    iget v0, p0, Lcom/google/obf/cd$a;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 26
    :cond_1
    iget-object v0, p0, Lcom/google/obf/cd$a;->m:Lcom/google/obf/cd$a$a;

    .line 27
    iget-object v1, p0, Lcom/google/obf/cd$a;->n:Lcom/google/obf/cd$a$a;

    iput-object v1, p0, Lcom/google/obf/cd$a;->m:Lcom/google/obf/cd$a$a;

    .line 28
    iput-object v0, p0, Lcom/google/obf/cd$a;->n:Lcom/google/obf/cd$a$a;

    .line 29
    iget-object v0, p0, Lcom/google/obf/cd$a;->n:Lcom/google/obf/cd$a$a;

    invoke-virtual {v0}, Lcom/google/obf/cd$a$a;->a()V

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/cd$a;->h:I

    .line 31
    iput-boolean v2, p0, Lcom/google/obf/cd$a;->k:Z

    .line 32
    :cond_2
    return-void
.end method

.method public a(Lcom/google/obf/du$a;)V
    .locals 2

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/obf/cd$a;->f:Landroid/util/SparseArray;

    iget v1, p1, Lcom/google/obf/du$a;->a:I

    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 17
    return-void
.end method

.method public a(Lcom/google/obf/du$b;)V
    .locals 2

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/obf/cd$a;->e:Landroid/util/SparseArray;

    iget v1, p1, Lcom/google/obf/du$b;->a:I

    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 15
    return-void
.end method

.method public a([BII)V
    .locals 19

    .prologue
    .line 33
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/obf/cd$a;->k:Z

    if-nez v2, :cond_1

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 35
    :cond_1
    sub-int v2, p3, p2

    .line 36
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/obf/cd$a;->g:[B

    array-length v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/obf/cd$a;->h:I

    add-int/2addr v4, v2

    if-ge v3, v4, :cond_2

    .line 37
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/obf/cd$a;->g:[B

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/obf/cd$a;->h:I

    add-int/2addr v4, v2

    mul-int/lit8 v4, v4, 0x2

    invoke-static {v3, v4}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/obf/cd$a;->g:[B

    .line 38
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/obf/cd$a;->g:[B

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/obf/cd$a;->h:I

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v0, v1, v3, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 39
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/obf/cd$a;->h:I

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/obf/cd$a;->h:I

    .line 40
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/obf/cd$a;->g:[B

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/obf/cd$a;->h:I

    invoke-virtual {v2, v3, v4}, Lcom/google/obf/dv;->a([BI)V

    .line 41
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    invoke-virtual {v2}, Lcom/google/obf/dv;->a()I

    move-result v2

    const/16 v3, 0x8

    if-lt v2, v3, :cond_0

    .line 43
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/obf/dv;->b(I)V

    .line 44
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/obf/dv;->c(I)I

    move-result v4

    .line 45
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/google/obf/dv;->b(I)V

    .line 46
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    invoke-virtual {v2}, Lcom/google/obf/dv;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 48
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    invoke-virtual {v2}, Lcom/google/obf/dv;->d()I

    .line 49
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    invoke-virtual {v2}, Lcom/google/obf/dv;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 51
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    invoke-virtual {v2}, Lcom/google/obf/dv;->d()I

    move-result v5

    .line 52
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/obf/cd$a;->c:Z

    if-nez v2, :cond_3

    .line 53
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/obf/cd$a;->k:Z

    .line 54
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cd$a;->n:Lcom/google/obf/cd$a$a;

    invoke-virtual {v2, v5}, Lcom/google/obf/cd$a$a;->a(I)V

    goto/16 :goto_0

    .line 56
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    invoke-virtual {v2}, Lcom/google/obf/dv;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 58
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    invoke-virtual {v2}, Lcom/google/obf/dv;->d()I

    move-result v7

    .line 59
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cd$a;->f:Landroid/util/SparseArray;

    invoke-virtual {v2, v7}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v2

    if-gez v2, :cond_4

    .line 60
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/obf/cd$a;->k:Z

    goto/16 :goto_0

    .line 62
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cd$a;->f:Landroid/util/SparseArray;

    invoke-virtual {v2, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/obf/du$a;

    .line 63
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/obf/cd$a;->e:Landroid/util/SparseArray;

    iget v6, v2, Lcom/google/obf/du$a;->b:I

    invoke-virtual {v3, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/obf/du$b;

    .line 64
    iget-boolean v6, v3, Lcom/google/obf/du$b;->e:Z

    if-eqz v6, :cond_5

    .line 65
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    invoke-virtual {v6}, Lcom/google/obf/dv;->a()I

    move-result v6

    const/4 v8, 0x2

    if-lt v6, v8, :cond_0

    .line 67
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    const/4 v8, 0x2

    invoke-virtual {v6, v8}, Lcom/google/obf/dv;->b(I)V

    .line 68
    :cond_5
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    invoke-virtual {v6}, Lcom/google/obf/dv;->a()I

    move-result v6

    iget v8, v3, Lcom/google/obf/du$b;->g:I

    if-lt v6, v8, :cond_0

    .line 70
    const/4 v8, 0x0

    .line 71
    const/4 v9, 0x0

    .line 72
    const/4 v10, 0x0

    .line 73
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    iget v11, v3, Lcom/google/obf/du$b;->g:I

    invoke-virtual {v6, v11}, Lcom/google/obf/dv;->c(I)I

    move-result v6

    .line 74
    iget-boolean v11, v3, Lcom/google/obf/du$b;->f:Z

    if-nez v11, :cond_6

    .line 75
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    invoke-virtual {v8}, Lcom/google/obf/dv;->a()I

    move-result v8

    const/4 v11, 0x1

    if-lt v8, v11, :cond_0

    .line 77
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    invoke-virtual {v8}, Lcom/google/obf/dv;->b()Z

    move-result v8

    .line 78
    if-eqz v8, :cond_6

    .line 79
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    invoke-virtual {v9}, Lcom/google/obf/dv;->a()I

    move-result v9

    const/4 v10, 0x1

    if-lt v9, v10, :cond_0

    .line 81
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    invoke-virtual {v9}, Lcom/google/obf/dv;->b()Z

    move-result v10

    .line 82
    const/4 v9, 0x1

    .line 83
    :cond_6
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/obf/cd$a;->i:I

    const/4 v12, 0x5

    if-ne v11, v12, :cond_9

    const/4 v11, 0x1

    .line 84
    :goto_1
    const/4 v12, 0x0

    .line 85
    if-eqz v11, :cond_7

    .line 86
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    invoke-virtual {v12}, Lcom/google/obf/dv;->c()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 88
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    invoke-virtual {v12}, Lcom/google/obf/dv;->d()I

    move-result v12

    .line 89
    :cond_7
    const/4 v13, 0x0

    .line 90
    const/4 v14, 0x0

    .line 91
    const/4 v15, 0x0

    .line 92
    const/16 v16, 0x0

    .line 93
    iget v0, v3, Lcom/google/obf/du$b;->h:I

    move/from16 v17, v0

    if-nez v17, :cond_a

    .line 94
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    invoke-virtual {v13}, Lcom/google/obf/dv;->a()I

    move-result v13

    iget v0, v3, Lcom/google/obf/du$b;->i:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-lt v13, v0, :cond_0

    .line 96
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    iget v0, v3, Lcom/google/obf/du$b;->i:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Lcom/google/obf/dv;->c(I)I

    move-result v13

    .line 97
    iget-boolean v2, v2, Lcom/google/obf/du$a;->c:Z

    if-eqz v2, :cond_8

    if-nez v8, :cond_8

    .line 98
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    invoke-virtual {v2}, Lcom/google/obf/dv;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 100
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    invoke-virtual {v2}, Lcom/google/obf/dv;->e()I

    move-result v14

    .line 109
    :cond_8
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cd$a;->n:Lcom/google/obf/cd$a$a;

    invoke-virtual/range {v2 .. v16}, Lcom/google/obf/cd$a$a;->a(Lcom/google/obf/du$b;IIIIZZZZIIIII)V

    .line 110
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/obf/cd$a;->k:Z

    goto/16 :goto_0

    .line 83
    :cond_9
    const/4 v11, 0x0

    goto :goto_1

    .line 101
    :cond_a
    iget v0, v3, Lcom/google/obf/du$b;->h:I

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_8

    iget-boolean v0, v3, Lcom/google/obf/du$b;->j:Z

    move/from16 v17, v0

    if-nez v17, :cond_8

    .line 102
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    invoke-virtual {v15}, Lcom/google/obf/dv;->c()Z

    move-result v15

    if-eqz v15, :cond_0

    .line 104
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    invoke-virtual {v15}, Lcom/google/obf/dv;->e()I

    move-result v15

    .line 105
    iget-boolean v2, v2, Lcom/google/obf/du$a;->c:Z

    if-eqz v2, :cond_8

    if-nez v8, :cond_8

    .line 106
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    invoke-virtual {v2}, Lcom/google/obf/dv;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 108
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/cd$a;->d:Lcom/google/obf/dv;

    invoke-virtual {v2}, Lcom/google/obf/dv;->e()I

    move-result v16

    goto :goto_2
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 13
    iget-boolean v0, p0, Lcom/google/obf/cd$a;->c:Z

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18
    iput-boolean v0, p0, Lcom/google/obf/cd$a;->k:Z

    .line 19
    iput-boolean v0, p0, Lcom/google/obf/cd$a;->o:Z

    .line 20
    iget-object v0, p0, Lcom/google/obf/cd$a;->n:Lcom/google/obf/cd$a$a;

    invoke-virtual {v0}, Lcom/google/obf/cd$a$a;->a()V

    .line 21
    return-void
.end method
