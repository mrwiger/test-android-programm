.class Lcom/samsung/multiscreen/Channel$3$2;
.super Ljava/lang/Object;
.source "Channel.java"

# interfaces
.implements Lcom/koushikdutta/async/http/WebSocket$StringCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/multiscreen/Channel$3;->onCompleted(Ljava/lang/Exception;Lcom/koushikdutta/async/http/WebSocket;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/multiscreen/Channel$3;


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/Channel$3;)V
    .locals 0
    .param p1, "this$1"    # Lcom/samsung/multiscreen/Channel$3;

    .prologue
    .line 514
    iput-object p1, p0, Lcom/samsung/multiscreen/Channel$3$2;->this$1:Lcom/samsung/multiscreen/Channel$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStringAvailable(Ljava/lang/String;)V
    .locals 6
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 517
    iget-object v3, p0, Lcom/samsung/multiscreen/Channel$3$2;->this$1:Lcom/samsung/multiscreen/Channel$3;

    iget-object v3, v3, Lcom/samsung/multiscreen/Channel$3;->this$0:Lcom/samsung/multiscreen/Channel;

    invoke-static {v3}, Lcom/samsung/multiscreen/Channel;->access$400(Lcom/samsung/multiscreen/Channel;)Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->resetLastPingReceived()V

    .line 520
    :try_start_0
    invoke-static {p1}, Lcom/samsung/multiscreen/util/JSONUtil;->parse(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    .line 522
    .local v2, "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v3, "event"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 528
    .local v1, "event":Ljava/lang/String;
    const-string v3, "ms.channel.connect"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 529
    iget-object v3, p0, Lcom/samsung/multiscreen/Channel$3$2;->this$1:Lcom/samsung/multiscreen/Channel$3;

    iget-object v3, v3, Lcom/samsung/multiscreen/Channel$3;->this$0:Lcom/samsung/multiscreen/Channel;

    iget-object v4, p0, Lcom/samsung/multiscreen/Channel$3$2;->this$1:Lcom/samsung/multiscreen/Channel$3;

    iget-object v4, v4, Lcom/samsung/multiscreen/Channel$3;->val$id:Ljava/lang/String;

    invoke-static {v3, v2, v4}, Lcom/samsung/multiscreen/Channel;->access$500(Lcom/samsung/multiscreen/Channel;Ljava/util/Map;Ljava/lang/String;)V

    .line 536
    .end local v1    # "event":Ljava/lang/String;
    .end local v2    # "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :goto_0
    return-void

    .line 531
    .restart local v1    # "event":Ljava/lang/String;
    .restart local v2    # "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_0
    iget-object v3, p0, Lcom/samsung/multiscreen/Channel$3$2;->this$1:Lcom/samsung/multiscreen/Channel$3;

    iget-object v3, v3, Lcom/samsung/multiscreen/Channel$3;->this$0:Lcom/samsung/multiscreen/Channel;

    iget-object v4, p0, Lcom/samsung/multiscreen/Channel$3$2;->this$1:Lcom/samsung/multiscreen/Channel$3;

    iget-object v4, v4, Lcom/samsung/multiscreen/Channel$3;->val$id:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v2, v5}, Lcom/samsung/multiscreen/Channel;->handleMessage(Ljava/lang/String;Ljava/util/Map;[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 533
    .end local v1    # "event":Ljava/lang/String;
    .end local v2    # "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :catch_0
    move-exception v0

    .line 534
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "Channel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "connect error: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
