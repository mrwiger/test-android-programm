.class public final Lcom/my/target/dn;
.super Landroid/widget/RelativeLayout;
.source "StandardNativeWideView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/my/target/dm;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# static fields
.field private static final aD:I

.field private static final aE:I

.field private static final bo:I

.field private static final bp:I


# instance fields
.field private final P:Lcom/my/target/cm;

.field private final aJ:Landroid/widget/TextView;

.field private final aL:Landroid/widget/TextView;

.field private final aN:Landroid/widget/Button;

.field private final aP:Lcom/my/target/bx;

.field private final aT:Landroid/widget/FrameLayout;

.field private final aX:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private a_:Landroid/view/View$OnClickListener;

.field private backgroundColor:I

.field private bi:I

.field private final bq:Landroid/widget/RelativeLayout;

.field private final br:Lcom/my/target/bx;

.field private final bs:Landroid/widget/LinearLayout;

.field private final bt:Landroid/widget/TextView;

.field private final bu:Lcom/my/target/ca;

.field private final bv:Lcom/my/target/bx;

.field private final bw:Landroid/widget/FrameLayout;

.field private final bx:Lcom/my/target/bu;

.field private final height:I

.field private final width:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/dn;->bo:I

    .line 37
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/dn;->aD:I

    .line 38
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/dn;->bp:I

    .line 39
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/dn;->aE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/my/target/dn;-><init>(Landroid/content/Context;B)V

    .line 65
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, -0x2

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, -0x1

    .line 69
    invoke-direct {p0, p1, v10}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/my/target/dn;->aX:Ljava/util/HashMap;

    .line 70
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dn;->bq:Landroid/widget/RelativeLayout;

    .line 71
    new-instance v0, Lcom/my/target/bx;

    invoke-direct {v0, p1}, Lcom/my/target/bx;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dn;->aP:Lcom/my/target/bx;

    .line 72
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dn;->aJ:Landroid/widget/TextView;

    .line 73
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dn;->aL:Landroid/widget/TextView;

    .line 74
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dn;->aN:Landroid/widget/Button;

    .line 75
    new-instance v0, Lcom/my/target/bx;

    invoke-direct {v0, p1}, Lcom/my/target/bx;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dn;->br:Lcom/my/target/bx;

    .line 76
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dn;->aT:Landroid/widget/FrameLayout;

    .line 77
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    .line 78
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dn;->bs:Landroid/widget/LinearLayout;

    .line 79
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dn;->bt:Landroid/widget/TextView;

    .line 80
    new-instance v0, Lcom/my/target/ca;

    invoke-direct {v0, p1}, Lcom/my/target/ca;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dn;->bu:Lcom/my/target/ca;

    .line 81
    new-instance v0, Lcom/my/target/bx;

    invoke-direct {v0, p1}, Lcom/my/target/bx;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dn;->bv:Lcom/my/target/bx;

    .line 82
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dn;->bw:Landroid/widget/FrameLayout;

    .line 83
    new-instance v0, Lcom/my/target/bu;

    invoke-direct {v0, p1}, Lcom/my/target/bu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dn;->bx:Lcom/my/target/bu;

    .line 85
    const-string v0, "ad_view"

    invoke-static {p0, v0}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/my/target/dn;->aP:Lcom/my/target/bx;

    const-string v1, "icon_image"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/my/target/dn;->aJ:Landroid/widget/TextView;

    const-string v1, "title_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/my/target/dn;->aL:Landroid/widget/TextView;

    const-string v1, "domain_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/my/target/dn;->aN:Landroid/widget/Button;

    const-string v1, "cta_button"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/my/target/dn;->br:Lcom/my/target/bx;

    const-string v1, "main_image"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/my/target/dn;->aT:Landroid/widget/FrameLayout;

    const-string v1, "icon_layout"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/my/target/dn;->bt:Landroid/widget/TextView;

    const-string v1, "votes_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/my/target/dn;->bu:Lcom/my/target/ca;

    const-string v1, "rating_view"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/my/target/dn;->bv:Lcom/my/target/bx;

    const-string v1, "banner_image"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/my/target/dn;->bx:Lcom/my/target/bu;

    const-string v1, "age_border"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 97
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    .line 98
    const/16 v1, 0xfa

    invoke-virtual {v0, v1}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, p0, Lcom/my/target/dn;->height:I

    .line 99
    const/16 v1, 0x12c

    invoke-virtual {v0, v1}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iput v0, p0, Lcom/my/target/dn;->width:I

    .line 1343
    invoke-virtual {p0}, Lcom/my/target/dn;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1344
    if-eqz v0, :cond_1

    .line 1348
    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-direct {v1, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1350
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    const/16 v4, 0x2a

    .line 1352
    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    invoke-direct {v2, v6, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1353
    iget-object v3, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1354
    iget-object v3, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1355
    iget-object v3, p0, Lcom/my/target/dn;->bq:Landroid/widget/RelativeLayout;

    sget v4, Lcom/my/target/dn;->bo:I

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 1356
    iget-object v3, p0, Lcom/my/target/dn;->bq:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1358
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    const/16 v4, 0x26

    .line 1359
    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    const/16 v5, 0x26

    invoke-virtual {v4, v5}, Lcom/my/target/cm;->n(I)I

    move-result v4

    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1360
    iget-object v3, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1361
    iget-object v3, p0, Lcom/my/target/dn;->aT:Landroid/widget/FrameLayout;

    sget v4, Lcom/my/target/dn;->aD:I

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setId(I)V

    .line 1362
    iget-object v3, p0, Lcom/my/target/dn;->aT:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1364
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1366
    const/16 v3, 0x11

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1367
    iget-object v3, p0, Lcom/my/target/dn;->aP:Lcom/my/target/bx;

    invoke-virtual {v3, v2}, Lcom/my/target/bx;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1369
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1372
    const/4 v3, 0x1

    sget v4, Lcom/my/target/dn;->aD:I

    invoke-virtual {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1373
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1375
    iget-object v2, p0, Lcom/my/target/dn;->aJ:Landroid/widget/TextView;

    const/high16 v3, 0x41900000    # 18.0f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 1376
    iget-object v2, p0, Lcom/my/target/dn;->aJ:Landroid/widget/TextView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1377
    iget-object v2, p0, Lcom/my/target/dn;->aJ:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1378
    iget-object v2, p0, Lcom/my/target/dn;->aJ:Landroid/widget/TextView;

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1379
    iget-object v2, p0, Lcom/my/target/dn;->aJ:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 1380
    iget-object v2, p0, Lcom/my/target/dn;->aJ:Landroid/widget/TextView;

    sget v3, Lcom/my/target/dn;->aE:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setId(I)V

    .line 1382
    iget-object v2, p0, Lcom/my/target/dn;->aL:Landroid/widget/TextView;

    const/high16 v3, 0x41600000    # 14.0f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 1383
    iget-object v2, p0, Lcom/my/target/dn;->aL:Landroid/widget/TextView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1384
    iget-object v2, p0, Lcom/my/target/dn;->aL:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1385
    iget-object v2, p0, Lcom/my/target/dn;->aL:Landroid/widget/TextView;

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1386
    iget-object v2, p0, Lcom/my/target/dn;->aL:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 1387
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1390
    const/4 v3, 0x3

    sget v4, Lcom/my/target/dn;->aE:I

    invoke-virtual {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1391
    iget-object v3, p0, Lcom/my/target/dn;->aL:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1393
    iget-object v2, p0, Lcom/my/target/dn;->aN:Landroid/widget/Button;

    sget v3, Lcom/my/target/dn;->bp:I

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setId(I)V

    .line 1394
    iget-object v2, p0, Lcom/my/target/dn;->aN:Landroid/widget/Button;

    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setTextSize(F)V

    .line 1395
    iget-object v2, p0, Lcom/my/target/dn;->aN:Landroid/widget/Button;

    iget-object v3, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/my/target/cm;->n(I)I

    move-result v4

    invoke-virtual {v2, v3, v8, v4, v8}, Landroid/widget/Button;->setPadding(IIII)V

    .line 1396
    iget-object v2, p0, Lcom/my/target/dn;->aN:Landroid/widget/Button;

    invoke-virtual {v2, v10}, Landroid/widget/Button;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1397
    iget-object v2, p0, Lcom/my/target/dn;->aN:Landroid/widget/Button;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setLines(I)V

    .line 1398
    iget-object v2, p0, Lcom/my/target/dn;->aN:Landroid/widget/Button;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1399
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    const/16 v4, 0x24

    .line 1400
    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    invoke-direct {v2, v6, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1401
    iget-object v3, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1402
    iget-object v3, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1403
    iget-object v3, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 1404
    const/16 v3, 0xc

    invoke-virtual {v2, v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1405
    invoke-virtual {p0}, Lcom/my/target/dn;->getCtaButton()Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1406
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_0

    .line 1408
    invoke-virtual {p0}, Lcom/my/target/dn;->getCtaButton()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/widget/Button;->setStateListAnimator(Landroid/animation/StateListAnimator;)V

    .line 1411
    :cond_0
    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1413
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    const/16 v4, 0x128

    .line 1414
    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    const/16 v5, 0xa8

    invoke-virtual {v4, v5}, Lcom/my/target/cm;->n(I)I

    move-result v4

    invoke-direct {v0, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1415
    const/4 v3, 0x3

    sget v4, Lcom/my/target/dn;->bo:I

    invoke-virtual {v0, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1416
    sget v3, Lcom/my/target/dn;->bp:I

    invoke-virtual {v0, v7, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1417
    const/16 v3, 0xe

    invoke-virtual {v0, v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1418
    iget-object v3, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 1419
    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1421
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1423
    const/16 v3, 0x11

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1424
    iget-object v3, p0, Lcom/my/target/dn;->br:Lcom/my/target/bx;

    invoke-virtual {v3, v0}, Lcom/my/target/bx;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1426
    iget-object v0, p0, Lcom/my/target/dn;->bt:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1427
    iget-object v0, p0, Lcom/my/target/dn;->bt:Landroid/widget/TextView;

    const/high16 v3, 0x41600000    # 14.0f

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 1428
    iget-object v0, p0, Lcom/my/target/dn;->bt:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 1430
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v6, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1432
    const/4 v3, 0x3

    sget v4, Lcom/my/target/dn;->aE:I

    invoke-virtual {v0, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1433
    iget-object v3, p0, Lcom/my/target/dn;->bs:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1435
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v3, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    const/16 v4, 0x49

    .line 1436
    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    const/16 v5, 0xa

    .line 1437
    invoke-virtual {v4, v5}, Lcom/my/target/cm;->n(I)I

    move-result v4

    invoke-direct {v0, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1438
    iget-object v3, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 1439
    iget-object v3, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 1440
    iget-object v3, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 1441
    const/16 v3, 0x30

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1442
    iget-object v3, p0, Lcom/my/target/dn;->bu:Lcom/my/target/ca;

    invoke-virtual {v3, v0}, Lcom/my/target/ca;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1444
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1447
    const/16 v3, 0xb

    invoke-virtual {v0, v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1448
    iget-object v3, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1449
    iget-object v3, p0, Lcom/my/target/dn;->bx:Lcom/my/target/bu;

    invoke-virtual {v3, v0}, Lcom/my/target/bu;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1450
    iget-object v0, p0, Lcom/my/target/dn;->bx:Lcom/my/target/bu;

    iget-object v3, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    invoke-virtual {v3, v7}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/dn;->P:Lcom/my/target/cm;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/my/target/cm;->n(I)I

    move-result v4

    invoke-virtual {v0, v3, v4, v8, v8}, Lcom/my/target/bu;->setPadding(IIII)V

    .line 1451
    iget-object v0, p0, Lcom/my/target/dn;->bx:Lcom/my/target/bu;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/my/target/bu;->setLines(I)V

    .line 1452
    iget-object v0, p0, Lcom/my/target/dn;->bx:Lcom/my/target/bu;

    const/high16 v3, 0x41400000    # 12.0f

    invoke-virtual {v0, v7, v3}, Lcom/my/target/bu;->setTextSize(IF)V

    .line 1454
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1456
    const/16 v3, 0xd

    invoke-virtual {v0, v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1457
    iget-object v3, p0, Lcom/my/target/dn;->bv:Lcom/my/target/bx;

    invoke-virtual {p0, v3, v0}, Lcom/my/target/dn;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1458
    iget-object v0, p0, Lcom/my/target/dn;->bw:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0, v6, v6}, Lcom/my/target/dn;->addView(Landroid/view/View;II)V

    .line 1460
    iget-object v0, p0, Lcom/my/target/dn;->bs:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/my/target/dn;->bu:Lcom/my/target/ca;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1461
    iget-object v0, p0, Lcom/my/target/dn;->bs:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/my/target/dn;->bt:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1463
    iget-object v0, p0, Lcom/my/target/dn;->aJ:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1464
    iget-object v0, p0, Lcom/my/target/dn;->aL:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1465
    iget-object v0, p0, Lcom/my/target/dn;->bs:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1466
    iget-object v0, p0, Lcom/my/target/dn;->aT:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/my/target/dn;->aP:Lcom/my/target/bx;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1467
    iget-object v0, p0, Lcom/my/target/dn;->bq:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/my/target/dn;->aT:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1468
    iget-object v0, p0, Lcom/my/target/dn;->bq:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1469
    iget-object v0, p0, Lcom/my/target/dn;->bq:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v0}, Lcom/my/target/dn;->addView(Landroid/view/View;)V

    .line 1470
    invoke-virtual {p0}, Lcom/my/target/dn;->getCtaButton()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/my/target/dn;->addView(Landroid/view/View;)V

    .line 1471
    iget-object v0, p0, Lcom/my/target/dn;->br:Lcom/my/target/bx;

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1472
    invoke-virtual {p0, v2}, Lcom/my/target/dn;->addView(Landroid/view/View;)V

    .line 1473
    iget-object v0, p0, Lcom/my/target/dn;->bx:Lcom/my/target/bu;

    invoke-virtual {p0, v0}, Lcom/my/target/dn;->addView(Landroid/view/View;)V

    .line 101
    :cond_1
    return-void
.end method


# virtual methods
.method public final H()Z
    .locals 1

    .prologue
    .line 225
    const/4 v0, 0x0

    return v0
.end method

.method public final O()Landroid/view/View;
    .locals 0

    .prologue
    .line 200
    return-object p0
.end method

.method public final P()V
    .locals 0

    .prologue
    .line 232
    return-void
.end method

.method public final a(Lcom/my/target/af;ZLandroid/view/View$OnClickListener;)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 239
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Apply click area "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/my/target/af;->O()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " to view"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 240
    iput-object p3, p0, Lcom/my/target/dn;->a_:Landroid/view/View$OnClickListener;

    .line 241
    if-nez p2, :cond_0

    iget-boolean v0, p1, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_b

    :cond_0
    move v0, v2

    .line 242
    :goto_0
    invoke-virtual {p0, p0}, Lcom/my/target/dn;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 244
    iget-object v3, p0, Lcom/my/target/dn;->aJ:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 245
    iget-object v3, p0, Lcom/my/target/dn;->aP:Lcom/my/target/bx;

    invoke-virtual {v3, p0}, Lcom/my/target/bx;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 246
    iget-object v3, p0, Lcom/my/target/dn;->br:Lcom/my/target/bx;

    invoke-virtual {v3, p0}, Lcom/my/target/bx;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 247
    iget-object v3, p0, Lcom/my/target/dn;->bu:Lcom/my/target/ca;

    invoke-virtual {v3, p0}, Lcom/my/target/ca;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 248
    iget-object v3, p0, Lcom/my/target/dn;->bt:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 249
    iget-object v3, p0, Lcom/my/target/dn;->aL:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 250
    iget-object v3, p0, Lcom/my/target/dn;->bx:Lcom/my/target/bu;

    invoke-virtual {v3, p0}, Lcom/my/target/bu;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 251
    iget-object v3, p0, Lcom/my/target/dn;->aN:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 253
    iget-object v4, p0, Lcom/my/target/dn;->aX:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/my/target/dn;->aJ:Landroid/widget/TextView;

    iget-boolean v3, p1, Lcom/my/target/af;->cs:Z

    if-nez v3, :cond_1

    if-eqz v0, :cond_c

    :cond_1
    move v3, v2

    :goto_1
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    iget-object v4, p0, Lcom/my/target/dn;->aX:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/my/target/dn;->aP:Lcom/my/target/bx;

    iget-boolean v3, p1, Lcom/my/target/af;->cu:Z

    if-nez v3, :cond_2

    if-eqz v0, :cond_d

    :cond_2
    move v3, v2

    :goto_2
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    iget-object v4, p0, Lcom/my/target/dn;->aX:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/my/target/dn;->br:Lcom/my/target/bx;

    iget-boolean v3, p1, Lcom/my/target/af;->cv:Z

    if-nez v3, :cond_3

    if-eqz v0, :cond_e

    :cond_3
    move v3, v2

    :goto_3
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    iget-object v4, p0, Lcom/my/target/dn;->aX:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/my/target/dn;->bu:Lcom/my/target/ca;

    iget-boolean v3, p1, Lcom/my/target/af;->cw:Z

    if-nez v3, :cond_4

    if-eqz v0, :cond_f

    :cond_4
    move v3, v2

    :goto_4
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    iget-object v4, p0, Lcom/my/target/dn;->aX:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/my/target/dn;->bt:Landroid/widget/TextView;

    iget-boolean v3, p1, Lcom/my/target/af;->cx:Z

    if-nez v3, :cond_5

    if-eqz v0, :cond_10

    :cond_5
    move v3, v2

    :goto_5
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    iget-object v4, p0, Lcom/my/target/dn;->aX:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/my/target/dn;->bx:Lcom/my/target/bu;

    iget-boolean v3, p1, Lcom/my/target/af;->cz:Z

    if-nez v3, :cond_6

    if-eqz v0, :cond_11

    :cond_6
    move v3, v2

    :goto_6
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    iget-object v4, p0, Lcom/my/target/dn;->aX:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/my/target/dn;->aL:Landroid/widget/TextView;

    iget-boolean v3, p1, Lcom/my/target/af;->cB:Z

    if-nez v3, :cond_7

    if-eqz v0, :cond_12

    :cond_7
    move v3, v2

    :goto_7
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    iget-object v4, p0, Lcom/my/target/dn;->aX:Ljava/util/HashMap;

    iget-boolean v3, p1, Lcom/my/target/af;->cD:Z

    if-nez v3, :cond_8

    if-eqz v0, :cond_13

    :cond_8
    move v3, v2

    :goto_8
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v4, p0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    iget-object v3, p0, Lcom/my/target/dn;->aX:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/my/target/dn;->aN:Landroid/widget/Button;

    iget-boolean v5, p1, Lcom/my/target/af;->cy:Z

    if-nez v5, :cond_9

    if-eqz v0, :cond_a

    :cond_9
    move v1, v2

    :cond_a
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    return-void

    :cond_b
    move v0, v1

    .line 241
    goto/16 :goto_0

    :cond_c
    move v3, v1

    .line 253
    goto/16 :goto_1

    :cond_d
    move v3, v1

    .line 254
    goto/16 :goto_2

    :cond_e
    move v3, v1

    .line 255
    goto :goto_3

    :cond_f
    move v3, v1

    .line 256
    goto :goto_4

    :cond_10
    move v3, v1

    .line 257
    goto :goto_5

    :cond_11
    move v3, v1

    .line 258
    goto :goto_6

    :cond_12
    move v3, v1

    .line 259
    goto :goto_7

    :cond_13
    move v3, v1

    .line 260
    goto :goto_8
.end method

.method public final b(II)V
    .locals 0

    .prologue
    .line 206
    iput p1, p0, Lcom/my/target/dn;->backgroundColor:I

    .line 207
    iput p2, p0, Lcom/my/target/dn;->bi:I

    .line 208
    return-void
.end method

.method public final b(Lcom/my/target/ah;)V
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v7, 0x3

    const/4 v1, 0x0

    const/16 v6, 0x8

    .line 109
    const-string v0, "teaser"

    invoke-virtual {p1}, Lcom/my/target/ah;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 111
    new-array v2, v7, [Landroid/view/View;

    iget-object v0, p0, Lcom/my/target/dn;->aT:Landroid/widget/FrameLayout;

    aput-object v0, v2, v1

    iget-object v0, p0, Lcom/my/target/dn;->bw:Landroid/widget/FrameLayout;

    aput-object v0, v2, v3

    aput-object p0, v2, v4

    move v0, v1

    :goto_0
    if-ge v0, v7, :cond_0

    aget-object v3, v2, v0

    .line 113
    iget v4, p0, Lcom/my/target/dn;->backgroundColor:I

    iget v5, p0, Lcom/my/target/dn;->bi:I

    invoke-static {v3, v4, v5}, Lcom/my/target/cm;->a(Landroid/view/View;II)V

    .line 111
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/my/target/dn;->bv:Lcom/my/target/bx;

    invoke-virtual {v0, v6}, Lcom/my/target/bx;->setVisibility(I)V

    .line 117
    iget-object v0, p0, Lcom/my/target/dn;->bw:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 118
    const-string v0, "store"

    invoke-virtual {p1}, Lcom/my/target/ah;->getNavigationType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 120
    iget-object v0, p0, Lcom/my/target/dn;->aL:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 121
    iget-object v0, p0, Lcom/my/target/dn;->bs:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 122
    invoke-virtual {p1}, Lcom/my/target/ah;->getRating()F

    move-result v0

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_2

    .line 124
    iget-object v0, p0, Lcom/my/target/dn;->bu:Lcom/my/target/ca;

    invoke-virtual {v0, v1}, Lcom/my/target/ca;->setVisibility(I)V

    .line 125
    invoke-virtual {p1}, Lcom/my/target/ah;->getVotes()I

    move-result v0

    if-lez v0, :cond_1

    .line 127
    iget-object v0, p0, Lcom/my/target/dn;->bt:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 139
    :goto_1
    iget-object v0, p0, Lcom/my/target/dn;->aL:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 161
    :goto_2
    return-void

    .line 131
    :cond_1
    iget-object v0, p0, Lcom/my/target/dn;->bt:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 136
    :cond_2
    iget-object v0, p0, Lcom/my/target/dn;->bu:Lcom/my/target/ca;

    invoke-virtual {v0, v6}, Lcom/my/target/ca;->setVisibility(I)V

    .line 137
    iget-object v0, p0, Lcom/my/target/dn;->bt:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 143
    :cond_3
    iget-object v0, p0, Lcom/my/target/dn;->aL:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 144
    iget-object v0, p0, Lcom/my/target/dn;->bs:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_2

    .line 149
    :cond_4
    new-array v2, v7, [Landroid/view/View;

    iget-object v0, p0, Lcom/my/target/dn;->aT:Landroid/widget/FrameLayout;

    aput-object v0, v2, v1

    iget-object v0, p0, Lcom/my/target/dn;->bw:Landroid/widget/FrameLayout;

    aput-object v0, v2, v3

    aput-object p0, v2, v4

    move v0, v1

    :goto_3
    if-ge v0, v7, :cond_5

    aget-object v3, v2, v0

    .line 151
    iget v4, p0, Lcom/my/target/dn;->bi:I

    invoke-static {v4}, Lcom/my/target/cm;->m(I)I

    move-result v4

    invoke-static {v3, v1, v4}, Lcom/my/target/cm;->a(Landroid/view/View;II)V

    .line 149
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 154
    :cond_5
    iget-object v0, p0, Lcom/my/target/dn;->bx:Lcom/my/target/bu;

    invoke-virtual {v0, v6}, Lcom/my/target/bu;->setVisibility(I)V

    .line 155
    iget-object v0, p0, Lcom/my/target/dn;->bq:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 156
    invoke-virtual {p0}, Lcom/my/target/dn;->getCtaButton()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 157
    iget-object v0, p0, Lcom/my/target/dn;->br:Lcom/my/target/bx;

    invoke-virtual {v0, v6}, Lcom/my/target/bx;->setVisibility(I)V

    .line 158
    iget-object v0, p0, Lcom/my/target/dn;->bw:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 159
    iget-object v0, p0, Lcom/my/target/dn;->bv:Lcom/my/target/bx;

    invoke-virtual {v0, v1}, Lcom/my/target/bx;->setVisibility(I)V

    goto :goto_2
.end method

.method public final getAgeRestrictionsView()Lcom/my/target/bu;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/my/target/dn;->bx:Lcom/my/target/bu;

    return-object v0
.end method

.method public final getBannerImage()Lcom/my/target/bx;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/my/target/dn;->bv:Lcom/my/target/bx;

    return-object v0
.end method

.method public final getCtaButton()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/my/target/dn;->aN:Landroid/widget/Button;

    return-object v0
.end method

.method public final getDescriptionTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getDisclaimerTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getDomainTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/my/target/dn;->aL:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getIconImage()Lcom/my/target/bx;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/my/target/dn;->aP:Lcom/my/target/bx;

    return-object v0
.end method

.method public final getMainImage()Lcom/my/target/bx;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/my/target/dn;->br:Lcom/my/target/bx;

    return-object v0
.end method

.method public final getRatingTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/my/target/dn;->bt:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getStarsRatingView()Lcom/my/target/ca;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/my/target/dn;->bu:Lcom/my/target/ca;

    return-object v0
.end method

.method public final getTitleTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/my/target/dn;->aJ:Landroid/widget/TextView;

    return-object v0
.end method

.method protected final onMeasure(II)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 336
    iget v0, p0, Lcom/my/target/dn;->width:I

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iget v1, p0, Lcom/my/target/dn;->height:I

    .line 337
    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 336
    invoke-super {p0, v0, v1}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 339
    return-void
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 267
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    move v0, v2

    .line 328
    :goto_1
    return v0

    .line 270
    :pswitch_1
    iget-object v0, p0, Lcom/my/target/dn;->aX:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 272
    goto :goto_1

    .line 274
    :cond_1
    iget-object v0, p0, Lcom/my/target/dn;->aX:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 276
    goto :goto_1

    .line 278
    :cond_2
    iget-object v0, p0, Lcom/my/target/dn;->aN:Landroid/widget/Button;

    if-ne p1, v0, :cond_3

    .line 280
    invoke-virtual {p1, v2}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 284
    :cond_3
    iget v0, p0, Lcom/my/target/dn;->bi:I

    invoke-virtual {p0, v0}, Lcom/my/target/dn;->setBackgroundColor(I)V

    .line 285
    iget-object v0, p0, Lcom/my/target/dn;->aP:Lcom/my/target/bx;

    iget v1, p0, Lcom/my/target/dn;->backgroundColor:I

    invoke-virtual {v0, v1}, Lcom/my/target/bx;->setBackgroundColor(I)V

    goto :goto_0

    .line 290
    :pswitch_2
    iget-object v0, p0, Lcom/my/target/dn;->aX:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 292
    goto :goto_1

    .line 294
    :cond_4
    iget-object v0, p0, Lcom/my/target/dn;->aX:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    .line 296
    goto :goto_1

    .line 299
    :cond_5
    iget-object v0, p0, Lcom/my/target/dn;->aN:Landroid/widget/Button;

    if-ne p1, v0, :cond_6

    .line 301
    invoke-virtual {p1, v1}, Landroid/view/View;->setPressed(Z)V

    .line 308
    :goto_2
    invoke-virtual {p0}, Lcom/my/target/dn;->performClick()Z

    .line 309
    iget-object v0, p0, Lcom/my/target/dn;->a_:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/my/target/dn;->a_:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 305
    :cond_6
    iget v0, p0, Lcom/my/target/dn;->backgroundColor:I

    invoke-virtual {p0, v0}, Lcom/my/target/dn;->setBackgroundColor(I)V

    .line 306
    iget-object v0, p0, Lcom/my/target/dn;->aP:Lcom/my/target/bx;

    iget v1, p0, Lcom/my/target/dn;->backgroundColor:I

    invoke-virtual {v0, v1}, Lcom/my/target/bx;->setBackgroundColor(I)V

    goto :goto_2

    .line 315
    :pswitch_3
    iget-object v0, p0, Lcom/my/target/dn;->aN:Landroid/widget/Button;

    if-ne p1, v0, :cond_7

    .line 317
    invoke-virtual {p1, v1}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 321
    :cond_7
    iget v0, p0, Lcom/my/target/dn;->backgroundColor:I

    invoke-virtual {p0, v0}, Lcom/my/target/dn;->setBackgroundColor(I)V

    .line 322
    iget-object v0, p0, Lcom/my/target/dn;->aP:Lcom/my/target/bx;

    iget v1, p0, Lcom/my/target/dn;->backgroundColor:I

    invoke-virtual {v0, v1}, Lcom/my/target/bx;->setBackgroundColor(I)V

    goto :goto_0

    .line 267
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final start()V
    .locals 0

    .prologue
    .line 214
    return-void
.end method

.method public final stop()V
    .locals 0

    .prologue
    .line 220
    return-void
.end method
