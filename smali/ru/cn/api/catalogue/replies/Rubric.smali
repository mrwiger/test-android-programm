.class public Lru/cn/api/catalogue/replies/Rubric;
.super Ljava/lang/Object;
.source "Rubric.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    }
.end annotation


# instance fields
.field public description:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "description"
    .end annotation
.end field

.field public have_subitems:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "have_subitems"
    .end annotation
.end field

.field public id:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id"
    .end annotation
.end field

.field public logoUrl:Ljava/lang/String;

.field public options:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "options"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/catalogue/replies/RubricOption;",
            ">;"
        }
    .end annotation
.end field

.field public subitems:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "subitems"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;"
        }
    .end annotation
.end field

.field public title:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "title"
    .end annotation
.end field

.field public uiHint:Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ui_hint"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fromJson(Ljava/lang/String;)Lru/cn/api/catalogue/replies/Rubric;
    .locals 3
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 15
    :try_start_0
    new-instance v1, Lcom/google/gson/Gson;

    invoke-direct {v1}, Lcom/google/gson/Gson;-><init>()V

    const-class v2, Lru/cn/api/catalogue/replies/Rubric;

    invoke-virtual {v1, p0, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/catalogue/replies/Rubric;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 17
    :goto_0
    return-object v1

    .line 16
    :catch_0
    move-exception v0

    .line 17
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public defaultFilter()Landroid/support/v4/util/LongSparseArray;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 74
    iget-object v3, p0, Lru/cn/api/catalogue/replies/Rubric;->options:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lru/cn/api/catalogue/replies/Rubric;->options:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 89
    :cond_0
    :goto_0
    return-object v0

    .line 77
    :cond_1
    iget-object v3, p0, Lru/cn/api/catalogue/replies/Rubric;->options:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/catalogue/replies/RubricOption;

    .line 78
    .local v1, "option":Lru/cn/api/catalogue/replies/RubricOption;
    iget-object v4, v1, Lru/cn/api/catalogue/replies/RubricOption;->type:Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

    sget-object v5, Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;->SWITCH:Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

    if-ne v4, v5, :cond_2

    .line 79
    iget-object v4, v1, Lru/cn/api/catalogue/replies/RubricOption;->values:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/api/catalogue/replies/RubricOptionValue;

    .line 80
    .local v2, "value":Lru/cn/api/catalogue/replies/RubricOptionValue;
    iget-boolean v5, v2, Lru/cn/api/catalogue/replies/RubricOptionValue;->selectedByDefault:Z

    if-eqz v5, :cond_3

    .line 81
    new-instance v0, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v0}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    .line 82
    .local v0, "filter":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Ljava/lang/String;>;"
    iget-wide v4, v1, Lru/cn/api/catalogue/replies/RubricOption;->id:J

    iget-object v3, v2, Lru/cn/api/catalogue/replies/RubricOptionValue;->value:Ljava/lang/String;

    invoke-virtual {v0, v4, v5, v3}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    goto :goto_0
.end method

.method public haveSubrubrics()Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lru/cn/api/catalogue/replies/Rubric;->have_subitems:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lru/cn/api/catalogue/replies/Rubric;->subitems:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lru/cn/api/catalogue/replies/Rubric;->subitems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
