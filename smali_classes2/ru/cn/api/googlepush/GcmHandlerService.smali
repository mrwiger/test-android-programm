.class public Lru/cn/api/googlepush/GcmHandlerService;
.super Lcom/google/android/gms/gcm/GcmListenerService;
.source "GcmHandlerService.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/gms/gcm/GcmListenerService;-><init>()V

    return-void
.end method

.method private showNotification(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 47
    const-string v9, "gcm_message"

    invoke-virtual {p2, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 50
    .local v3, "message":Ljava/lang/String;
    if-nez v3, :cond_0

    .line 81
    :goto_0
    return-void

    .line 53
    :cond_0
    const v9, 0x7f0e002b

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 54
    .local v0, "appName":Ljava/lang/String;
    const-string v9, "gcm_title"

    invoke-virtual {p2, v9, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 56
    .local v8, "title":Ljava/lang/String;
    new-instance v5, Landroid/content/Intent;

    const-class v9, Lru/cn/tv/StartActivity;

    invoke-direct {v5, p1, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 57
    .local v5, "resultIntent":Landroid/content/Intent;
    invoke-virtual {v5, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 59
    invoke-static {p1}, Landroid/support/v4/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/support/v4/app/TaskStackBuilder;

    move-result-object v7

    .line 60
    .local v7, "stackBuilder":Landroid/support/v4/app/TaskStackBuilder;
    const-class v9, Lru/cn/tv/StartActivity;

    invoke-virtual {v7, v9}, Landroid/support/v4/app/TaskStackBuilder;->addParentStack(Ljava/lang/Class;)Landroid/support/v4/app/TaskStackBuilder;

    .line 61
    invoke-virtual {v7, v5}, Landroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/support/v4/app/TaskStackBuilder;

    .line 62
    const/4 v9, 0x0

    const/high16 v10, 0x8000000

    invoke-virtual {v7, v9, v10}, Landroid/support/v4/app/TaskStackBuilder;->getPendingIntent(II)Landroid/app/PendingIntent;

    move-result-object v6

    .line 65
    .local v6, "resultPendingIntent":Landroid/app/PendingIntent;
    new-instance v9, Landroid/support/v4/app/NotificationCompat$Builder;

    const-string v10, "PUSH_CHANNEL_ID"

    invoke-direct {v9, p1, v10}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const v10, 0x7f0800f1

    .line 66
    invoke-virtual {v9, v10}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v9

    const/4 v10, 0x1

    .line 67
    invoke-virtual {v9, v10}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v9

    .line 68
    invoke-virtual {v9, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v9

    .line 69
    invoke-virtual {v9, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v9

    .line 70
    invoke-virtual {v9, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v9

    .line 71
    invoke-virtual {v9}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v4

    .line 73
    .local v4, "notification":Landroid/app/Notification;
    const-string v9, "notification"

    invoke-virtual {p1, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 75
    .local v2, "manager":Landroid/app/NotificationManager;
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v10, 0x1a

    if-lt v9, v10, :cond_1

    .line 76
    new-instance v1, Landroid/app/NotificationChannel;

    const-string v9, "PUSH_CHANNEL_ID"

    const/4 v10, 0x4

    invoke-direct {v1, v9, v0, v10}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    .line 77
    .local v1, "channel":Landroid/app/NotificationChannel;
    invoke-virtual {v2, v1}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    .line 80
    .end local v1    # "channel":Landroid/app/NotificationChannel;
    :cond_1
    const/16 v9, 0x7b

    invoke-virtual {v2, v9, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method


# virtual methods
.method public onMessageReceived(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "from"    # Ljava/lang/String;
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 31
    if-nez p2, :cond_1

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 35
    :cond_1
    invoke-virtual {p0}, Lru/cn/api/googlepush/GcmHandlerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 36
    .local v0, "context":Landroid/content/Context;
    const-string v1, "pdid"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 37
    const-string v1, "pdid"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 38
    .local v2, "pdid":J
    invoke-static {v2, v3}, Lru/cn/domain/statistics/inetra/InetraTracker;->pushView(J)V

    .line 41
    .end local v2    # "pdid":J
    :cond_2
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v1

    if-nez v1, :cond_0

    .line 42
    invoke-direct {p0, v0, p2}, Lru/cn/api/googlepush/GcmHandlerService;->showNotification(Landroid/content/Context;Landroid/os/Bundle;)V

    goto :goto_0
.end method
