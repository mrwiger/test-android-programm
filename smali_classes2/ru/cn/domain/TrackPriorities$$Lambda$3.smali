.class final synthetic Lru/cn/domain/TrackPriorities$$Lambda$3;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field private final arg$1:J

.field private final arg$2:Lcom/annimon/stream/function/Function;


# direct methods
.method constructor <init>(JLcom/annimon/stream/function/Function;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lru/cn/domain/TrackPriorities$$Lambda$3;->arg$1:J

    iput-object p3, p0, Lru/cn/domain/TrackPriorities$$Lambda$3;->arg$2:Lcom/annimon/stream/function/Function;

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3

    iget-wide v0, p0, Lru/cn/domain/TrackPriorities$$Lambda$3;->arg$1:J

    iget-object v2, p0, Lru/cn/domain/TrackPriorities$$Lambda$3;->arg$2:Lcom/annimon/stream/function/Function;

    check-cast p1, Lru/cn/api/iptv/replies/MediaLocation;

    check-cast p2, Lru/cn/api/iptv/replies/MediaLocation;

    invoke-static {v0, v1, v2, p1, p2}, Lru/cn/domain/TrackPriorities;->lambda$byTimezones$3$TrackPriorities(JLcom/annimon/stream/function/Function;Lru/cn/api/iptv/replies/MediaLocation;Lru/cn/api/iptv/replies/MediaLocation;)I

    move-result v0

    return v0
.end method
