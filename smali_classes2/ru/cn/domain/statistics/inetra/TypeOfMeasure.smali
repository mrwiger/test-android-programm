.class public final enum Lru/cn/domain/statistics/inetra/TypeOfMeasure;
.super Ljava/lang/Enum;
.source "TypeOfMeasure.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/domain/statistics/inetra/TypeOfMeasure;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/domain/statistics/inetra/TypeOfMeasure;

.field public static final enum CHANNELS_LIST_LOAD_AND_DISPLAYING:Lru/cn/domain/statistics/inetra/TypeOfMeasure;

.field public static final enum PRELOADER_BANNER_LOAD:Lru/cn/domain/statistics/inetra/TypeOfMeasure;

.field public static final enum TIME_OF_CONTENT_LOADING:Lru/cn/domain/statistics/inetra/TypeOfMeasure;

.field public static final enum VIDEO_BANNER_LOAD:Lru/cn/domain/statistics/inetra/TypeOfMeasure;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 5
    new-instance v0, Lru/cn/domain/statistics/inetra/TypeOfMeasure;

    const-string v1, "PRELOADER_BANNER_LOAD"

    invoke-direct {v0, v1, v5, v2}, Lru/cn/domain/statistics/inetra/TypeOfMeasure;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/domain/statistics/inetra/TypeOfMeasure;->PRELOADER_BANNER_LOAD:Lru/cn/domain/statistics/inetra/TypeOfMeasure;

    .line 7
    new-instance v0, Lru/cn/domain/statistics/inetra/TypeOfMeasure;

    const-string v1, "VIDEO_BANNER_LOAD"

    invoke-direct {v0, v1, v2, v3}, Lru/cn/domain/statistics/inetra/TypeOfMeasure;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/domain/statistics/inetra/TypeOfMeasure;->VIDEO_BANNER_LOAD:Lru/cn/domain/statistics/inetra/TypeOfMeasure;

    .line 9
    new-instance v0, Lru/cn/domain/statistics/inetra/TypeOfMeasure;

    const-string v1, "CHANNELS_LIST_LOAD_AND_DISPLAYING"

    invoke-direct {v0, v1, v3, v4}, Lru/cn/domain/statistics/inetra/TypeOfMeasure;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/domain/statistics/inetra/TypeOfMeasure;->CHANNELS_LIST_LOAD_AND_DISPLAYING:Lru/cn/domain/statistics/inetra/TypeOfMeasure;

    .line 11
    new-instance v0, Lru/cn/domain/statistics/inetra/TypeOfMeasure;

    const-string v1, "TIME_OF_CONTENT_LOADING"

    invoke-direct {v0, v1, v4, v6}, Lru/cn/domain/statistics/inetra/TypeOfMeasure;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/domain/statistics/inetra/TypeOfMeasure;->TIME_OF_CONTENT_LOADING:Lru/cn/domain/statistics/inetra/TypeOfMeasure;

    .line 3
    new-array v0, v6, [Lru/cn/domain/statistics/inetra/TypeOfMeasure;

    sget-object v1, Lru/cn/domain/statistics/inetra/TypeOfMeasure;->PRELOADER_BANNER_LOAD:Lru/cn/domain/statistics/inetra/TypeOfMeasure;

    aput-object v1, v0, v5

    sget-object v1, Lru/cn/domain/statistics/inetra/TypeOfMeasure;->VIDEO_BANNER_LOAD:Lru/cn/domain/statistics/inetra/TypeOfMeasure;

    aput-object v1, v0, v2

    sget-object v1, Lru/cn/domain/statistics/inetra/TypeOfMeasure;->CHANNELS_LIST_LOAD_AND_DISPLAYING:Lru/cn/domain/statistics/inetra/TypeOfMeasure;

    aput-object v1, v0, v3

    sget-object v1, Lru/cn/domain/statistics/inetra/TypeOfMeasure;->TIME_OF_CONTENT_LOADING:Lru/cn/domain/statistics/inetra/TypeOfMeasure;

    aput-object v1, v0, v4

    sput-object v0, Lru/cn/domain/statistics/inetra/TypeOfMeasure;->$VALUES:[Lru/cn/domain/statistics/inetra/TypeOfMeasure;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 16
    iput p3, p0, Lru/cn/domain/statistics/inetra/TypeOfMeasure;->value:I

    .line 17
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/domain/statistics/inetra/TypeOfMeasure;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lru/cn/domain/statistics/inetra/TypeOfMeasure;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/domain/statistics/inetra/TypeOfMeasure;

    return-object v0
.end method

.method public static values()[Lru/cn/domain/statistics/inetra/TypeOfMeasure;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lru/cn/domain/statistics/inetra/TypeOfMeasure;->$VALUES:[Lru/cn/domain/statistics/inetra/TypeOfMeasure;

    invoke-virtual {v0}, [Lru/cn/domain/statistics/inetra/TypeOfMeasure;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/domain/statistics/inetra/TypeOfMeasure;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lru/cn/domain/statistics/inetra/TypeOfMeasure;->value:I

    return v0
.end method
