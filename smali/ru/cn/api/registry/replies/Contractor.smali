.class public Lru/cn/api/registry/replies/Contractor;
.super Ljava/lang/Object;
.source "Contractor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;
    }
.end annotation


# instance fields
.field public brandName:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "brandName"
    .end annotation
.end field

.field public contractorId:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "contractorId"
    .end annotation
.end field

.field private images:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "images"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/registry/replies/ContractorImage;",
            ">;"
        }
    .end annotation
.end field

.field public name:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "name"
    .end annotation
.end field

.field private partnerTags:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "partnerTags"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public privateOfficeURL:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "privateOfficeURL"
    .end annotation
.end field

.field public supportedOfficeIdioms:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "supportedOfficeIdioms"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/api/registry/replies/Contractor;->images:Ljava/util/List;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/api/registry/replies/Contractor;->partnerTags:Ljava/util/List;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/api/registry/replies/Contractor;->supportedOfficeIdioms:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getImage(Lru/cn/api/registry/replies/ContractorImage$ProfileType;)Lru/cn/api/registry/replies/ContractorImage;
    .locals 4
    .param p1, "profile"    # Lru/cn/api/registry/replies/ContractorImage$ProfileType;

    .prologue
    const/4 v1, 0x0

    .line 65
    iget-object v2, p0, Lru/cn/api/registry/replies/Contractor;->images:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lru/cn/api/registry/replies/Contractor;->images:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 73
    :goto_0
    return-object v0

    .line 68
    :cond_1
    iget-object v2, p0, Lru/cn/api/registry/replies/Contractor;->images:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/registry/replies/ContractorImage;

    .line 69
    .local v0, "image":Lru/cn/api/registry/replies/ContractorImage;
    invoke-virtual {v0}, Lru/cn/api/registry/replies/ContractorImage;->getProfileType()Lru/cn/api/registry/replies/ContractorImage$ProfileType;

    move-result-object v3

    if-ne v3, p1, :cond_2

    goto :goto_0

    .end local v0    # "image":Lru/cn/api/registry/replies/ContractorImage;
    :cond_3
    move-object v0, v1

    .line 73
    goto :goto_0
.end method

.method public hasPartnerTag(Ljava/lang/String;)Z
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 77
    iget-object v0, p0, Lru/cn/api/registry/replies/Contractor;->partnerTags:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/api/registry/replies/Contractor;->partnerTags:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    :cond_0
    const/4 v0, 0x0

    .line 80
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lru/cn/api/registry/replies/Contractor;->partnerTags:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
