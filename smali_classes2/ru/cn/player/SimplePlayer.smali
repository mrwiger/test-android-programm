.class public Lru/cn/player/SimplePlayer;
.super Landroid/widget/FrameLayout;
.source "SimplePlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/player/SimplePlayer$Listener;,
        Lru/cn/player/SimplePlayer$FitMode;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private aspectRatio:F

.field private bufferingListener:Lru/cn/player/BufferingListener;

.field private bytefogSession:Lru/cn/utils/bytefog/BytefogSession;

.field private castSession:Lcom/google/android/gms/cast/framework/CastSession;

.field private listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/player/SimplePlayer$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private mSeekWhenReady:I

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

.field private mediaPlayerListener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

.field private needsHandleHeadsetUnplug:Z

.field private playerType:I

.field private sizeCalculator:Lru/cn/player/VideoSizeCalculator;

.field private source:Landroid/net/Uri;

.field private subtitleView:Landroid/view/View;

.field surfaceHolderCallback:Landroid/view/SurfaceHolder$Callback;

.field private sv:Landroid/view/SurfaceView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lru/cn/player/SimplePlayer;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lru/cn/player/SimplePlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 91
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lru/cn/player/SimplePlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 92
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 95
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 72
    const/4 v0, 0x0

    iput v0, p0, Lru/cn/player/SimplePlayer;->playerType:I

    .line 73
    iput-object v1, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/player/SimplePlayer;->needsHandleHeadsetUnplug:Z

    .line 77
    iput-object v1, p0, Lru/cn/player/SimplePlayer;->source:Landroid/net/Uri;

    .line 79
    const/4 v0, 0x0

    iput v0, p0, Lru/cn/player/SimplePlayer;->aspectRatio:F

    .line 80
    new-instance v0, Lru/cn/player/VideoSizeCalculator;

    invoke-direct {v0}, Lru/cn/player/VideoSizeCalculator;-><init>()V

    iput-object v0, p0, Lru/cn/player/SimplePlayer;->sizeCalculator:Lru/cn/player/VideoSizeCalculator;

    .line 84
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/player/SimplePlayer;->listeners:Ljava/util/List;

    .line 133
    new-instance v0, Lru/cn/player/SimplePlayer$1;

    invoke-direct {v0, p0}, Lru/cn/player/SimplePlayer$1;-><init>(Lru/cn/player/SimplePlayer;)V

    iput-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayerListener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    .line 220
    new-instance v0, Lru/cn/player/SimplePlayer$2;

    invoke-direct {v0, p0}, Lru/cn/player/SimplePlayer$2;-><init>(Lru/cn/player/SimplePlayer;)V

    iput-object v0, p0, Lru/cn/player/SimplePlayer;->bufferingListener:Lru/cn/player/BufferingListener;

    .line 429
    new-instance v0, Lru/cn/player/SimplePlayer$5;

    invoke-direct {v0, p0}, Lru/cn/player/SimplePlayer$5;-><init>(Lru/cn/player/SimplePlayer;)V

    iput-object v0, p0, Lru/cn/player/SimplePlayer;->surfaceHolderCallback:Landroid/view/SurfaceHolder$Callback;

    .line 97
    new-instance v0, Landroid/view/SurfaceView;

    invoke-virtual {p0}, Lru/cn/player/SimplePlayer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lru/cn/player/SimplePlayer;->sv:Landroid/view/SurfaceView;

    .line 98
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->sv:Landroid/view/SurfaceView;

    invoke-virtual {p0, v0}, Lru/cn/player/SimplePlayer;->addView(Landroid/view/View;)V

    .line 99
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->sv:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lru/cn/player/SimplePlayer;->surfaceHolderCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 100
    return-void
.end method

.method static synthetic access$000(Lru/cn/player/SimplePlayer;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/SimplePlayer;

    .prologue
    .line 30
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->listeners:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/player/SimplePlayer;)Lru/cn/utils/bytefog/BytefogSession;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/SimplePlayer;

    .prologue
    .line 30
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->bytefogSession:Lru/cn/utils/bytefog/BytefogSession;

    return-object v0
.end method

.method static synthetic access$1000(Lru/cn/player/SimplePlayer;)Landroid/view/SurfaceHolder;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/SimplePlayer;

    .prologue
    .line 30
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    return-object v0
.end method

.method static synthetic access$1002(Lru/cn/player/SimplePlayer;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .locals 0
    .param p0, "x0"    # Lru/cn/player/SimplePlayer;
    .param p1, "x1"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 30
    iput-object p1, p0, Lru/cn/player/SimplePlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    return-object p1
.end method

.method static synthetic access$102(Lru/cn/player/SimplePlayer;Lru/cn/utils/bytefog/BytefogSession;)Lru/cn/utils/bytefog/BytefogSession;
    .locals 0
    .param p0, "x0"    # Lru/cn/player/SimplePlayer;
    .param p1, "x1"    # Lru/cn/utils/bytefog/BytefogSession;

    .prologue
    .line 30
    iput-object p1, p0, Lru/cn/player/SimplePlayer;->bytefogSession:Lru/cn/utils/bytefog/BytefogSession;

    return-object p1
.end method

.method static synthetic access$200(Lru/cn/player/SimplePlayer;)I
    .locals 1
    .param p0, "x0"    # Lru/cn/player/SimplePlayer;

    .prologue
    .line 30
    iget v0, p0, Lru/cn/player/SimplePlayer;->mSeekWhenReady:I

    return v0
.end method

.method static synthetic access$202(Lru/cn/player/SimplePlayer;I)I
    .locals 0
    .param p0, "x0"    # Lru/cn/player/SimplePlayer;
    .param p1, "x1"    # I

    .prologue
    .line 30
    iput p1, p0, Lru/cn/player/SimplePlayer;->mSeekWhenReady:I

    return p1
.end method

.method static synthetic access$300(Lru/cn/player/SimplePlayer;)Lru/cn/player/AbstractMediaPlayer;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/SimplePlayer;

    .prologue
    .line 30
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    return-object v0
.end method

.method static synthetic access$400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lru/cn/player/SimplePlayer;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lru/cn/player/SimplePlayer;)F
    .locals 1
    .param p0, "x0"    # Lru/cn/player/SimplePlayer;

    .prologue
    .line 30
    iget v0, p0, Lru/cn/player/SimplePlayer;->aspectRatio:F

    return v0
.end method

.method static synthetic access$600(Lru/cn/player/SimplePlayer;)Lru/cn/player/VideoSizeCalculator;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/SimplePlayer;

    .prologue
    .line 30
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->sizeCalculator:Lru/cn/player/VideoSizeCalculator;

    return-object v0
.end method

.method static synthetic access$700(Lru/cn/player/SimplePlayer;)Landroid/view/SurfaceView;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/SimplePlayer;

    .prologue
    .line 30
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->sv:Landroid/view/SurfaceView;

    return-object v0
.end method

.method static synthetic access$800(Lru/cn/player/SimplePlayer;)I
    .locals 1
    .param p0, "x0"    # Lru/cn/player/SimplePlayer;

    .prologue
    .line 30
    iget v0, p0, Lru/cn/player/SimplePlayer;->playerType:I

    return v0
.end method

.method static synthetic access$900(Lru/cn/player/SimplePlayer;)Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/SimplePlayer;

    .prologue
    .line 30
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayerListener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    return-object v0
.end method

.method private isCurrentType(ILru/cn/player/AbstractMediaPlayer;)Z
    .locals 1
    .param p1, "type"    # I
    .param p2, "player"    # Lru/cn/player/AbstractMediaPlayer;

    .prologue
    .line 545
    sparse-switch p1, :sswitch_data_0

    .line 556
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 547
    :sswitch_0
    instance-of v0, p2, Lru/cn/player/androidplayer/AndroidMediaPlayer;

    goto :goto_0

    .line 550
    :sswitch_1
    instance-of v0, p2, Lru/cn/player/exoplayer/ExoMediaPlayer;

    goto :goto_0

    .line 553
    :sswitch_2
    instance-of v0, p2, Lru/cn/player/chromecast/ChromecastPlayer;

    goto :goto_0

    .line 545
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5 -> :sswitch_1
        0x69 -> :sswitch_2
    .end sparse-switch
.end method

.method private isInPlaybackState()Z
    .locals 2

    .prologue
    .line 455
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    .line 456
    invoke-virtual {v0}, Lru/cn/player/AbstractMediaPlayer;->getState()Lru/cn/player/AbstractMediaPlayer$PlayerState;

    move-result-object v0

    sget-object v1, Lru/cn/player/AbstractMediaPlayer$PlayerState;->PLAYING:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    .line 457
    invoke-virtual {v0}, Lru/cn/player/AbstractMediaPlayer;->getState()Lru/cn/player/AbstractMediaPlayer$PlayerState;

    move-result-object v0

    sget-object v1, Lru/cn/player/AbstractMediaPlayer$PlayerState;->PAUSED:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    .line 458
    invoke-virtual {v0}, Lru/cn/player/AbstractMediaPlayer;->getState()Lru/cn/player/AbstractMediaPlayer$PlayerState;

    move-result-object v0

    sget-object v1, Lru/cn/player/AbstractMediaPlayer$PlayerState;->LOADED:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 455
    :goto_0
    return v0

    .line 458
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addListener(Lru/cn/player/SimplePlayer$Listener;)V
    .locals 1
    .param p1, "listener"    # Lru/cn/player/SimplePlayer$Listener;

    .prologue
    .line 235
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239
    :goto_0
    return-void

    .line 238
    :cond_0
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getAudioTrackProvider()Lru/cn/player/ITrackSelector;
    .locals 1

    .prologue
    .line 530
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    if-nez v0, :cond_0

    .line 531
    const/4 v0, 0x0

    .line 533
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    invoke-virtual {v0}, Lru/cn/player/AbstractMediaPlayer;->getAudioTrackProvider()Lru/cn/player/AudioTrackSelector;

    move-result-object v0

    goto :goto_0
.end method

.method public getBufferPosition()J
    .locals 2

    .prologue
    .line 500
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    if-eqz v0, :cond_0

    .line 501
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    invoke-virtual {v0}, Lru/cn/player/AbstractMediaPlayer;->getBufferPosition()J

    move-result-wide v0

    .line 503
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 481
    invoke-direct {p0}, Lru/cn/player/SimplePlayer;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 482
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    invoke-virtual {v0}, Lru/cn/player/AbstractMediaPlayer;->getCurrentPosition()I

    move-result v0

    .line 484
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDateTime()J
    .locals 2

    .prologue
    .line 492
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    if-eqz v0, :cond_0

    .line 493
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    invoke-virtual {v0}, Lru/cn/player/AbstractMediaPlayer;->getDateTime()J

    move-result-wide v0

    .line 496
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 474
    invoke-direct {p0}, Lru/cn/player/SimplePlayer;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 475
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    invoke-virtual {v0}, Lru/cn/player/AbstractMediaPlayer;->getDuration()I

    move-result v0

    .line 477
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getFitMode()Lru/cn/player/SimplePlayer$FitMode;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->sizeCalculator:Lru/cn/player/VideoSizeCalculator;

    invoke-virtual {v0}, Lru/cn/player/VideoSizeCalculator;->getMode()Lru/cn/player/SimplePlayer$FitMode;

    move-result-object v0

    return-object v0
.end method

.method public getPlayerType()I
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lru/cn/player/SimplePlayer;->playerType:I

    return v0
.end method

.method public getState()Lru/cn/player/AbstractMediaPlayer$PlayerState;
    .locals 1

    .prologue
    .line 513
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    if-eqz v0, :cond_0

    .line 514
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    invoke-virtual {v0}, Lru/cn/player/AbstractMediaPlayer;->getState()Lru/cn/player/AbstractMediaPlayer$PlayerState;

    move-result-object v0

    .line 517
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lru/cn/player/AbstractMediaPlayer$PlayerState;->STOPPED:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    goto :goto_0
.end method

.method public getSubtitlesTrackProvider()Lru/cn/player/ITrackSelector;
    .locals 1

    .prologue
    .line 538
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    if-nez v0, :cond_0

    .line 539
    const/4 v0, 0x0

    .line 541
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    invoke-virtual {v0}, Lru/cn/player/AbstractMediaPlayer;->getSubtitleTracksProvider()Lru/cn/player/SubtitleTrackSelector;

    move-result-object v0

    goto :goto_0
.end method

.method public getUserZoom()F
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->sizeCalculator:Lru/cn/player/VideoSizeCalculator;

    invoke-virtual {v0}, Lru/cn/player/VideoSizeCalculator;->getZoom()F

    move-result v0

    return v0
.end method

.method public getVideoTracksProvider()Lru/cn/player/ITrackSelector;
    .locals 1

    .prologue
    .line 522
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    if-nez v0, :cond_0

    .line 523
    const/4 v0, 0x0

    .line 525
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    invoke-virtual {v0}, Lru/cn/player/AbstractMediaPlayer;->getVideoTracksProvider()Lru/cn/player/VideoTrackSelector;

    move-result-object v0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 2

    .prologue
    .line 488
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    invoke-virtual {v0}, Lru/cn/player/AbstractMediaPlayer;->getState()Lru/cn/player/AbstractMediaPlayer$PlayerState;

    move-result-object v0

    sget-object v1, Lru/cn/player/AbstractMediaPlayer$PlayerState;->PLAYING:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final synthetic lambda$play$0$SimplePlayer(I)V
    .locals 3
    .param p1, "bitrate"    # I

    .prologue
    .line 349
    iget-object v1, p0, Lru/cn/player/SimplePlayer;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/player/SimplePlayer$Listener;

    .line 350
    .local v0, "listener":Lru/cn/player/SimplePlayer$Listener;
    invoke-interface {v0, p1}, Lru/cn/player/SimplePlayer$Listener;->qualityChanged(I)V

    goto :goto_0

    .line 352
    .end local v0    # "listener":Lru/cn/player/SimplePlayer$Listener;
    :cond_0
    return-void
.end method

.method final synthetic lambda$play$1$SimplePlayer(Ljava/util/List;)V
    .locals 3
    .param p1, "items"    # Ljava/util/List;

    .prologue
    .line 355
    iget-object v1, p0, Lru/cn/player/SimplePlayer;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/player/SimplePlayer$Listener;

    .line 356
    .local v0, "listener":Lru/cn/player/SimplePlayer$Listener;
    invoke-interface {v0, p1}, Lru/cn/player/SimplePlayer$Listener;->onMetadata(Ljava/util/List;)V

    goto :goto_0

    .line 358
    .end local v0    # "listener":Lru/cn/player/SimplePlayer$Listener;
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 465
    iget-object v1, p0, Lru/cn/player/SimplePlayer;->sizeCalculator:Lru/cn/player/VideoSizeCalculator;

    invoke-virtual {v1, p2, p3, p4, p5}, Lru/cn/player/VideoSizeCalculator;->calculate(IIII)Landroid/graphics/Rect;

    move-result-object v0

    .line 466
    .local v0, "surfaceSize":Landroid/graphics/Rect;
    iget-object v1, p0, Lru/cn/player/SimplePlayer;->sv:Landroid/view/SurfaceView;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, v0, Landroid/graphics/Rect;->top:I

    iget v4, v0, Landroid/graphics/Rect;->right:I

    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/view/SurfaceView;->layout(IIII)V

    .line 468
    iget-object v1, p0, Lru/cn/player/SimplePlayer;->subtitleView:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 469
    iget-object v1, p0, Lru/cn/player/SimplePlayer;->subtitleView:Landroid/view/View;

    invoke-virtual {v1, p2, p3, p4, p5}, Landroid/view/View;->layout(IIII)V

    .line 471
    :cond_0
    return-void
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 418
    invoke-virtual {p0}, Lru/cn/player/SimplePlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 419
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    invoke-virtual {v0}, Lru/cn/player/AbstractMediaPlayer;->pause()V

    .line 421
    :cond_0
    return-void
.end method

.method public play(Landroid/net/Uri;)V
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 323
    iget-object v2, p0, Lru/cn/player/SimplePlayer;->source:Landroid/net/Uri;

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lru/cn/player/SimplePlayer;->source:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 324
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lru/cn/player/SimplePlayer;->source:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 325
    :cond_0
    const/4 v2, 0x0

    iput v2, p0, Lru/cn/player/SimplePlayer;->mSeekWhenReady:I

    .line 328
    :cond_1
    iput-object p1, p0, Lru/cn/player/SimplePlayer;->source:Landroid/net/Uri;

    .line 331
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.music.musicservicecommand"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 332
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "command"

    const-string v3, "pause"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 333
    invoke-virtual {p0}, Lru/cn/player/SimplePlayer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 335
    iget v2, p0, Lru/cn/player/SimplePlayer;->playerType:I

    iget-object v3, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    invoke-direct {p0, v2, v3}, Lru/cn/player/SimplePlayer;->isCurrentType(ILru/cn/player/AbstractMediaPlayer;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 336
    iget-object v2, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    if-eqz v2, :cond_2

    .line 337
    iget-object v2, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    invoke-virtual {v2}, Lru/cn/player/AbstractMediaPlayer;->destroy()V

    .line 338
    const/4 v2, 0x0

    iput-object v2, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    .line 341
    :cond_2
    iget v2, p0, Lru/cn/player/SimplePlayer;->playerType:I

    sparse-switch v2, :sswitch_data_0

    .line 366
    :goto_0
    iget-object v2, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    if-eqz v2, :cond_5

    .line 367
    iget-object v2, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    iget-object v3, p0, Lru/cn/player/SimplePlayer;->mediaPlayerListener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    invoke-virtual {v2, v3}, Lru/cn/player/AbstractMediaPlayer;->setListener(Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;)V

    .line 368
    iget-object v2, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    iget-object v3, p0, Lru/cn/player/SimplePlayer;->bufferingListener:Lru/cn/player/BufferingListener;

    invoke-virtual {v2, v3}, Lru/cn/player/AbstractMediaPlayer;->setBufferingListener(Lru/cn/player/BufferingListener;)V

    .line 370
    iget-object v2, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    invoke-virtual {v2}, Lru/cn/player/AbstractMediaPlayer;->getSubtitleView()Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lru/cn/player/SimplePlayer;->subtitleView:Landroid/view/View;

    .line 371
    iget-object v2, p0, Lru/cn/player/SimplePlayer;->subtitleView:Landroid/view/View;

    if-eqz v2, :cond_3

    .line 372
    iget-object v2, p0, Lru/cn/player/SimplePlayer;->subtitleView:Landroid/view/View;

    invoke-virtual {p0, v2}, Lru/cn/player/SimplePlayer;->addView(Landroid/view/View;)V

    .line 375
    :cond_3
    iget-object v2, p0, Lru/cn/player/SimplePlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v2, :cond_4

    .line 376
    iget-object v2, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    iget-object v3, p0, Lru/cn/player/SimplePlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v2, v3}, Lru/cn/player/AbstractMediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 379
    :cond_4
    iget-object v2, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    iget-boolean v3, p0, Lru/cn/player/SimplePlayer;->needsHandleHeadsetUnplug:Z

    invoke-virtual {v2, v3}, Lru/cn/player/AbstractMediaPlayer;->setNeedHeadsetUnpluggedHandle(Z)V

    .line 380
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_5

    .line 382
    invoke-virtual {p0}, Lru/cn/player/SimplePlayer;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "captioning"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/CaptioningManager;

    .line 383
    .local v0, "captioningManager":Landroid/view/accessibility/CaptioningManager;
    if-eqz v0, :cond_5

    .line 384
    iget-object v2, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    iget-object v2, v2, Lru/cn/player/AbstractMediaPlayer;->accessibility:Lru/cn/player/AbstractMediaPlayer$Accessibility;

    invoke-virtual {v0}, Landroid/view/accessibility/CaptioningManager;->isEnabled()Z

    move-result v3

    iput-boolean v3, v2, Lru/cn/player/AbstractMediaPlayer$Accessibility;->captioningEnable:Z

    .line 390
    .end local v0    # "captioningManager":Landroid/view/accessibility/CaptioningManager;
    :cond_5
    iget-object v2, p0, Lru/cn/player/SimplePlayer;->source:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string v3, "magnet"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 391
    iget-object v2, p0, Lru/cn/player/SimplePlayer;->bytefogSession:Lru/cn/utils/bytefog/BytefogSession;

    if-eqz v2, :cond_6

    .line 392
    iget-object v2, p0, Lru/cn/player/SimplePlayer;->bytefogSession:Lru/cn/utils/bytefog/BytefogSession;

    invoke-virtual {v2}, Lru/cn/utils/bytefog/BytefogSession;->stop()V

    .line 395
    :cond_6
    new-instance v2, Lru/cn/utils/bytefog/BytefogSession;

    invoke-virtual {p0}, Lru/cn/player/SimplePlayer;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lru/cn/player/SimplePlayer;->source:Landroid/net/Uri;

    invoke-direct {v2, v3, v4}, Lru/cn/utils/bytefog/BytefogSession;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    iput-object v2, p0, Lru/cn/player/SimplePlayer;->bytefogSession:Lru/cn/utils/bytefog/BytefogSession;

    .line 396
    iget-object v2, p0, Lru/cn/player/SimplePlayer;->bytefogSession:Lru/cn/utils/bytefog/BytefogSession;

    new-instance v3, Lru/cn/player/SimplePlayer$3;

    invoke-direct {v3, p0}, Lru/cn/player/SimplePlayer$3;-><init>(Lru/cn/player/SimplePlayer;)V

    invoke-virtual {v2, v3}, Lru/cn/utils/bytefog/BytefogSession;->setOnErrorListener(Lru/cn/utils/bytefog/BytefogSession$OnErrorListener;)V

    .line 404
    iget-object v2, p0, Lru/cn/player/SimplePlayer;->bytefogSession:Lru/cn/utils/bytefog/BytefogSession;

    new-instance v3, Lru/cn/player/SimplePlayer$4;

    invoke-direct {v3, p0}, Lru/cn/player/SimplePlayer$4;-><init>(Lru/cn/player/SimplePlayer;)V

    invoke-virtual {v2, v3}, Lru/cn/utils/bytefog/BytefogSession;->start(Lru/cn/utils/bytefog/BytefogSession$OnReadyListener;)V

    .line 415
    :goto_1
    return-void

    .line 343
    :sswitch_0
    new-instance v2, Lru/cn/player/androidplayer/AndroidMediaPlayer;

    invoke-virtual {p0}, Lru/cn/player/SimplePlayer;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lru/cn/player/androidplayer/AndroidMediaPlayer;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    goto/16 :goto_0

    .line 347
    :sswitch_1
    new-instance v2, Lru/cn/player/exoplayer/ExoMediaPlayer;

    invoke-virtual {p0}, Lru/cn/player/SimplePlayer;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lru/cn/player/exoplayer/ExoMediaPlayer;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    .line 348
    iget-object v2, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    new-instance v3, Lru/cn/player/SimplePlayer$$Lambda$0;

    invoke-direct {v3, p0}, Lru/cn/player/SimplePlayer$$Lambda$0;-><init>(Lru/cn/player/SimplePlayer;)V

    invoke-virtual {v2, v3}, Lru/cn/player/AbstractMediaPlayer;->setChangeQualityListener(Lru/cn/player/ChangeQualityListener;)V

    .line 354
    iget-object v2, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    new-instance v3, Lru/cn/player/SimplePlayer$$Lambda$1;

    invoke-direct {v3, p0}, Lru/cn/player/SimplePlayer$$Lambda$1;-><init>(Lru/cn/player/SimplePlayer;)V

    invoke-virtual {v2, v3}, Lru/cn/player/AbstractMediaPlayer;->setMetadataListener(Lru/cn/player/metadata/MetadataListener;)V

    goto/16 :goto_0

    .line 362
    :sswitch_2
    new-instance v2, Lru/cn/player/chromecast/ChromecastPlayer;

    invoke-virtual {p0}, Lru/cn/player/SimplePlayer;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lru/cn/player/SimplePlayer;->castSession:Lcom/google/android/gms/cast/framework/CastSession;

    invoke-direct {v2, v3, v4}, Lru/cn/player/chromecast/ChromecastPlayer;-><init>(Landroid/content/Context;Lcom/google/android/gms/cast/framework/CastSession;)V

    iput-object v2, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    goto/16 :goto_0

    .line 414
    :cond_7
    iget-object v2, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    iget-object v3, p0, Lru/cn/player/SimplePlayer;->source:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lru/cn/player/AbstractMediaPlayer;->play(Landroid/net/Uri;)V

    goto :goto_1

    .line 341
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5 -> :sswitch_1
        0x69 -> :sswitch_2
    .end sparse-switch
.end method

.method public play(Ljava/lang/String;)V
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 317
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lru/cn/player/SimplePlayer;->play(Landroid/net/Uri;)V

    .line 318
    return-void
.end method

.method public removeListener(Lru/cn/player/SimplePlayer$Listener;)V
    .locals 1
    .param p1, "listener"    # Lru/cn/player/SimplePlayer$Listener;

    .prologue
    .line 242
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 243
    return-void
.end method

.method public resume()V
    .locals 2

    .prologue
    .line 311
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    invoke-virtual {v0}, Lru/cn/player/AbstractMediaPlayer;->getState()Lru/cn/player/AbstractMediaPlayer$PlayerState;

    move-result-object v0

    sget-object v1, Lru/cn/player/AbstractMediaPlayer$PlayerState;->PAUSED:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    if-ne v0, v1, :cond_0

    .line 312
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    invoke-virtual {v0}, Lru/cn/player/AbstractMediaPlayer;->resume()V

    .line 314
    :cond_0
    return-void
.end method

.method public seekTo(I)V
    .locals 1
    .param p1, "msec"    # I

    .prologue
    .line 424
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    invoke-virtual {v0, p1}, Lru/cn/player/AbstractMediaPlayer;->seekTo(I)V

    .line 427
    :cond_0
    return-void
.end method

.method public setAspectRatio(F)V
    .locals 5
    .param p1, "ratio"    # F

    .prologue
    const/high16 v4, 0x40400000    # 3.0f

    const/high16 v3, 0x3f000000    # 0.5f

    .line 273
    sget-object v0, Lru/cn/player/SimplePlayer;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ratio "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    iput p1, p0, Lru/cn/player/SimplePlayer;->aspectRatio:F

    .line 275
    iget v0, p0, Lru/cn/player/SimplePlayer;->aspectRatio:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 276
    iget v0, p0, Lru/cn/player/SimplePlayer;->aspectRatio:F

    cmpl-float v0, v0, v4

    if-lez v0, :cond_1

    .line 277
    iput v4, p0, Lru/cn/player/SimplePlayer;->aspectRatio:F

    .line 282
    :cond_0
    :goto_0
    return-void

    .line 278
    :cond_1
    iget v0, p0, Lru/cn/player/SimplePlayer;->aspectRatio:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_0

    .line 279
    iput v3, p0, Lru/cn/player/SimplePlayer;->aspectRatio:F

    goto :goto_0
.end method

.method public setAutoCrop(II)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 266
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->sizeCalculator:Lru/cn/player/VideoSizeCalculator;

    invoke-virtual {v0, p1, p2}, Lru/cn/player/VideoSizeCalculator;->setCrop(II)V

    .line 267
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->sizeCalculator:Lru/cn/player/VideoSizeCalculator;

    invoke-virtual {v0}, Lru/cn/player/VideoSizeCalculator;->getMode()Lru/cn/player/SimplePlayer$FitMode;

    move-result-object v0

    sget-object v1, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_SIZE:Lru/cn/player/SimplePlayer$FitMode;

    if-ne v0, v1, :cond_0

    .line 268
    invoke-virtual {p0}, Lru/cn/player/SimplePlayer;->requestLayout()V

    .line 270
    :cond_0
    return-void
.end method

.method public final setCastSession(Lcom/google/android/gms/cast/framework/CastSession;)V
    .locals 0
    .param p1, "session"    # Lcom/google/android/gms/cast/framework/CastSession;

    .prologue
    .line 246
    iput-object p1, p0, Lru/cn/player/SimplePlayer;->castSession:Lcom/google/android/gms/cast/framework/CastSession;

    .line 247
    return-void
.end method

.method public setFitMode(Lru/cn/player/SimplePlayer$FitMode;)V
    .locals 1
    .param p1, "mode"    # Lru/cn/player/SimplePlayer$FitMode;

    .prologue
    .line 254
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->sizeCalculator:Lru/cn/player/VideoSizeCalculator;

    invoke-virtual {v0, p1}, Lru/cn/player/VideoSizeCalculator;->setMode(Lru/cn/player/SimplePlayer$FitMode;)V

    .line 255
    invoke-virtual {p0}, Lru/cn/player/SimplePlayer;->requestLayout()V

    .line 256
    return-void
.end method

.method public setNeedHeadsetUnpluggedHandle(Z)V
    .locals 1
    .param p1, "needHandle"    # Z

    .prologue
    .line 103
    iput-boolean p1, p0, Lru/cn/player/SimplePlayer;->needsHandleHeadsetUnplug:Z

    .line 104
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    invoke-virtual {v0, p1}, Lru/cn/player/AbstractMediaPlayer;->setNeedHeadsetUnpluggedHandle(Z)V

    .line 107
    :cond_0
    return-void
.end method

.method public setPlayerType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 114
    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-gt v0, v1, :cond_0

    .line 116
    const/4 p1, -0x1

    .line 119
    :cond_0
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 120
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->castSession:Lcom/google/android/gms/cast/framework/CastSession;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lru/cn/player/SimplePlayer;->castSession:Lcom/google/android/gms/cast/framework/CastSession;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/framework/CastSession;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 121
    const/16 p1, 0x69

    .line 130
    :cond_1
    :goto_0
    iput p1, p0, Lru/cn/player/SimplePlayer;->playerType:I

    .line 131
    return-void

    .line 122
    :cond_2
    invoke-virtual {p0}, Lru/cn/player/SimplePlayer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lru/cn/player/exoplayer/ExoPlayerUtils;->isUsable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 123
    const/4 p1, 0x5

    goto :goto_0

    .line 126
    :cond_3
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public setVolume(F)V
    .locals 1
    .param p1, "volume"    # F

    .prologue
    .line 507
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    if-eqz v0, :cond_0

    .line 508
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    invoke-virtual {v0, p1}, Lru/cn/player/AbstractMediaPlayer;->setVolume(F)V

    .line 510
    :cond_0
    return-void
.end method

.method public setZoom(F)V
    .locals 2
    .param p1, "zoom"    # F

    .prologue
    .line 259
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->sizeCalculator:Lru/cn/player/VideoSizeCalculator;

    invoke-virtual {v0, p1}, Lru/cn/player/VideoSizeCalculator;->setZoom(F)V

    .line 260
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->sizeCalculator:Lru/cn/player/VideoSizeCalculator;

    invoke-virtual {v0}, Lru/cn/player/VideoSizeCalculator;->getMode()Lru/cn/player/SimplePlayer$FitMode;

    move-result-object v0

    sget-object v1, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_USER:Lru/cn/player/SimplePlayer$FitMode;

    if-ne v0, v1, :cond_0

    .line 261
    invoke-virtual {p0}, Lru/cn/player/SimplePlayer;->requestLayout()V

    .line 263
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    if-eqz v0, :cond_0

    .line 450
    iget-object v0, p0, Lru/cn/player/SimplePlayer;->mediaPlayer:Lru/cn/player/AbstractMediaPlayer;

    invoke-virtual {v0}, Lru/cn/player/AbstractMediaPlayer;->stop()V

    .line 452
    :cond_0
    return-void
.end method

.method public zoomIn()V
    .locals 3

    .prologue
    .line 285
    iget-object v1, p0, Lru/cn/player/SimplePlayer;->sizeCalculator:Lru/cn/player/VideoSizeCalculator;

    invoke-virtual {v1}, Lru/cn/player/VideoSizeCalculator;->getMode()Lru/cn/player/SimplePlayer$FitMode;

    move-result-object v1

    sget-object v2, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_USER:Lru/cn/player/SimplePlayer$FitMode;

    if-eq v1, v2, :cond_1

    .line 293
    :cond_0
    :goto_0
    return-void

    .line 288
    :cond_1
    iget-object v1, p0, Lru/cn/player/SimplePlayer;->sizeCalculator:Lru/cn/player/VideoSizeCalculator;

    invoke-virtual {v1}, Lru/cn/player/VideoSizeCalculator;->getZoom()F

    move-result v0

    .line 289
    .local v0, "zoom":F
    const/high16 v1, 0x3fc00000    # 1.5f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    .line 290
    iget-object v1, p0, Lru/cn/player/SimplePlayer;->sizeCalculator:Lru/cn/player/VideoSizeCalculator;

    const v2, 0x3dcccccd    # 0.1f

    add-float/2addr v2, v0

    invoke-virtual {v1, v2}, Lru/cn/player/VideoSizeCalculator;->setZoom(F)V

    .line 291
    invoke-virtual {p0}, Lru/cn/player/SimplePlayer;->requestLayout()V

    goto :goto_0
.end method

.method public zoomOut()V
    .locals 3

    .prologue
    .line 296
    iget-object v1, p0, Lru/cn/player/SimplePlayer;->sizeCalculator:Lru/cn/player/VideoSizeCalculator;

    invoke-virtual {v1}, Lru/cn/player/VideoSizeCalculator;->getMode()Lru/cn/player/SimplePlayer$FitMode;

    move-result-object v1

    sget-object v2, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_USER:Lru/cn/player/SimplePlayer$FitMode;

    if-eq v1, v2, :cond_1

    .line 304
    :cond_0
    :goto_0
    return-void

    .line 299
    :cond_1
    iget-object v1, p0, Lru/cn/player/SimplePlayer;->sizeCalculator:Lru/cn/player/VideoSizeCalculator;

    invoke-virtual {v1}, Lru/cn/player/VideoSizeCalculator;->getZoom()F

    move-result v0

    .line 300
    .local v0, "zoom":F
    const/high16 v1, 0x3f000000    # 0.5f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    .line 301
    iget-object v1, p0, Lru/cn/player/SimplePlayer;->sizeCalculator:Lru/cn/player/VideoSizeCalculator;

    const v2, 0x3dcccccd    # 0.1f

    sub-float v2, v0, v2

    invoke-virtual {v1, v2}, Lru/cn/player/VideoSizeCalculator;->setZoom(F)V

    .line 302
    invoke-virtual {p0}, Lru/cn/player/SimplePlayer;->requestLayout()V

    goto :goto_0
.end method
