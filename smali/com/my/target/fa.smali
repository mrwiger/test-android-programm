.class final Lcom/my/target/fa;
.super Landroid/support/v7/widget/LinearLayoutManager;
.source "SliderLayoutManager.java"


# instance fields
.field private dZ:I

.field private final df:I

.field private ea:I


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0, p1, v0, v0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 25
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    .line 26
    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iput v0, p0, Lcom/my/target/fa;->df:I

    .line 27
    return-void
.end method


# virtual methods
.method final d(II)V
    .locals 0

    .prologue
    .line 114
    iput p1, p0, Lcom/my/target/fa;->dZ:I

    .line 115
    iput p2, p0, Lcom/my/target/fa;->ea:I

    .line 116
    return-void
.end method

.method public final measureChildWithMargins(Landroid/view/View;II)V
    .locals 12
    .param p1, "child"    # Landroid/view/View;
    .param p2, "widthUsed"    # I
    .param p3, "heightUsed"    # I

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    .line 32
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    .line 34
    invoke-virtual {p0}, Lcom/my/target/fa;->getWidth()I

    move-result v3

    .line 35
    invoke-virtual {p0}, Lcom/my/target/fa;->getHeight()I

    move-result v4

    .line 38
    const/4 v2, 0x0

    .line 40
    if-eqz v4, :cond_0

    if-eqz v3, :cond_0

    iget v1, p0, Lcom/my/target/fa;->ea:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/my/target/fa;->dZ:I

    if-nez v1, :cond_1

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 46
    :cond_1
    if-ge v3, v4, :cond_2

    .line 48
    const/high16 v1, 0x3e000000    # 0.125f

    .line 55
    :goto_1
    invoke-virtual {p0}, Lcom/my/target/fa;->getPaddingRight()I

    move-result v5

    .line 56
    invoke-virtual {p0}, Lcom/my/target/fa;->getPaddingLeft()I

    move-result v6

    .line 58
    int-to-float v7, v3

    int-to-float v8, v4

    div-float/2addr v7, v8

    iget v8, p0, Lcom/my/target/fa;->dZ:I

    iget v9, p0, Lcom/my/target/fa;->df:I

    mul-int/lit8 v9, v9, 0x2

    add-int/2addr v8, v9

    int-to-float v8, v8

    iget v9, p0, Lcom/my/target/fa;->ea:I

    int-to-float v9, v9

    div-float/2addr v8, v9

    cmpl-float v7, v7, v8

    if-lez v7, :cond_3

    .line 60
    iget v2, p0, Lcom/my/target/fa;->dZ:I

    mul-int/2addr v2, v4

    int-to-float v2, v2

    iget v4, p0, Lcom/my/target/fa;->ea:I

    int-to-float v4, v4

    div-float/2addr v2, v4

    float-to-int v2, v2

    iget v4, p0, Lcom/my/target/fa;->df:I

    mul-int/lit8 v4, v4, 0x2

    sub-int/2addr v2, v4

    .line 61
    int-to-float v4, v3

    add-int v7, v3, v2

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    int-to-float v8, v3

    mul-float/2addr v1, v8

    add-float/2addr v1, v7

    sub-float v1, v4, v1

    float-to-int v1, v1

    div-int/lit8 v1, v1, 0x2

    .line 68
    :goto_2
    invoke-virtual {p0, p1}, Lcom/my/target/fa;->getItemViewType(Landroid/view/View;)I

    move-result v4

    const/4 v7, 0x1

    if-ne v4, v7, :cond_4

    .line 70
    int-to-float v4, v3

    int-to-float v7, v6

    sub-float/2addr v4, v7

    int-to-float v7, v5

    sub-float/2addr v4, v7

    int-to-float v7, v2

    sub-float/2addr v4, v7

    div-float/2addr v4, v10

    float-to-int v4, v4

    iput v4, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    .line 72
    iput v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    .line 88
    :goto_3
    invoke-virtual {p0}, Lcom/my/target/fa;->getWidthMode()I

    move-result v1

    add-int v4, v6, v5

    iget v5, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    add-int/2addr v4, v5

    iget v5, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    add-int/2addr v4, v5

    add-int/2addr v4, p2

    .line 92
    invoke-virtual {p0}, Lcom/my/target/fa;->canScrollHorizontally()Z

    move-result v5

    .line 87
    invoke-static {v3, v1, v4, v2, v5}, Lcom/my/target/fa;->getChildMeasureSpec(IIIIZ)I

    move-result v1

    .line 93
    invoke-virtual {p0}, Lcom/my/target/fa;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/my/target/fa;->getHeightMode()I

    move-result v3

    .line 94
    invoke-virtual {p0}, Lcom/my/target/fa;->getPaddingTop()I

    move-result v4

    invoke-virtual {p0}, Lcom/my/target/fa;->getPaddingBottom()I

    move-result v5

    add-int/2addr v4, v5

    iget v5, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->topMargin:I

    add-int/2addr v4, v5

    iget v5, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->bottomMargin:I

    add-int/2addr v4, v5

    add-int/2addr v4, p3

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->height:I

    .line 96
    invoke-virtual {p0}, Lcom/my/target/fa;->canScrollVertically()Z

    move-result v5

    .line 93
    invoke-static {v2, v3, v4, v0, v5}, Lcom/my/target/fa;->getChildMeasureSpec(IIIIZ)I

    move-result v0

    .line 97
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    goto/16 :goto_0

    .line 52
    :cond_2
    const v1, 0x3d4ccccd    # 0.05f

    goto/16 :goto_1

    .line 65
    :cond_3
    int-to-float v4, v3

    const/high16 v7, 0x3f800000    # 1.0f

    mul-float/2addr v1, v10

    sub-float v1, v7, v1

    mul-float/2addr v1, v4

    float-to-int v1, v1

    sub-int/2addr v1, v5

    sub-int/2addr v1, v6

    move v11, v2

    move v2, v1

    move v1, v11

    goto :goto_2

    .line 74
    :cond_4
    invoke-virtual {p0, p1}, Lcom/my/target/fa;->getItemViewType(Landroid/view/View;)I

    move-result v4

    const/4 v7, 0x2

    if-ne v4, v7, :cond_5

    .line 76
    int-to-float v4, v3

    int-to-float v7, v6

    sub-float/2addr v4, v7

    int-to-float v7, v5

    sub-float/2addr v4, v7

    int-to-float v7, v2

    sub-float/2addr v4, v7

    div-float/2addr v4, v10

    float-to-int v4, v4

    iput v4, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    .line 78
    iput v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    goto :goto_3

    .line 82
    :cond_5
    iput v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    .line 83
    iput v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    goto :goto_3
.end method
