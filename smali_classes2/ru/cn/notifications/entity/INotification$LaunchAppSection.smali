.class public Lru/cn/notifications/entity/INotification$LaunchAppSection;
.super Ljava/lang/Object;
.source "INotification.java"

# interfaces
.implements Lru/cn/notifications/entity/INotification;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/notifications/entity/INotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LaunchAppSection"
.end annotation


# instance fields
.field private final createTime:J

.field private final host:Ljava/lang/String;

.field private final message:Ljava/lang/String;

.field private final scheme:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "scheme"    # Ljava/lang/String;
    .param p3, "host"    # Ljava/lang/String;
    .param p4, "createTime"    # J

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    iput-object p1, p0, Lru/cn/notifications/entity/INotification$LaunchAppSection;->message:Ljava/lang/String;

    .line 120
    iput-object p2, p0, Lru/cn/notifications/entity/INotification$LaunchAppSection;->scheme:Ljava/lang/String;

    .line 121
    iput-object p3, p0, Lru/cn/notifications/entity/INotification$LaunchAppSection;->host:Ljava/lang/String;

    .line 122
    iput-wide p4, p0, Lru/cn/notifications/entity/INotification$LaunchAppSection;->createTime:J

    .line 123
    return-void
.end method


# virtual methods
.method public createTime()J
    .locals 2

    .prologue
    .line 140
    iget-wide v0, p0, Lru/cn/notifications/entity/INotification$LaunchAppSection;->createTime:J

    return-wide v0
.end method

.method public host()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lru/cn/notifications/entity/INotification$LaunchAppSection;->host:Ljava/lang/String;

    return-object v0
.end method

.method public message()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lru/cn/notifications/entity/INotification$LaunchAppSection;->message:Ljava/lang/String;

    return-object v0
.end method

.method public scheme()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lru/cn/notifications/entity/INotification$LaunchAppSection;->scheme:Ljava/lang/String;

    return-object v0
.end method
