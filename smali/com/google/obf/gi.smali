.class public Lcom/google/obf/gi;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/hj$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/gi$a;,
        Lcom/google/obf/gi$c;,
        Lcom/google/obf/gi$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/obf/hj;

.field private b:Ljava/lang/String;

.field private c:Landroid/view/View;

.field private d:Lcom/google/obf/gi$b;

.field private e:Lcom/google/obf/gi$a;

.field private f:Landroid/app/Activity;

.field private g:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/obf/hj;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1
    new-instance v0, Lcom/google/obf/gi$c;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/obf/gi$c;-><init>(Lcom/google/obf/gi$1;)V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/obf/gi;-><init>(Ljava/lang/String;Lcom/google/obf/hj;Landroid/view/View;Lcom/google/obf/gi$b;)V

    .line 2
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Lcom/google/obf/hj;Landroid/view/View;Lcom/google/obf/gi$b;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object p1, p0, Lcom/google/obf/gi;->b:Ljava/lang/String;

    .line 5
    iput-object p2, p0, Lcom/google/obf/gi;->a:Lcom/google/obf/hj;

    .line 6
    iput-object p3, p0, Lcom/google/obf/gi;->c:Landroid/view/View;

    .line 7
    iput-object p4, p0, Lcom/google/obf/gi;->d:Lcom/google/obf/gi$b;

    .line 8
    iput-object v0, p0, Lcom/google/obf/gi;->f:Landroid/app/Activity;

    .line 9
    iput-object v0, p0, Lcom/google/obf/gi;->e:Lcom/google/obf/gi$a;

    .line 10
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/gi;->g:Z

    .line 11
    return-void
.end method

.method static synthetic a(Lcom/google/obf/gi;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/obf/gi;->f:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic a(Lcom/google/obf/gi;Landroid/app/Activity;)Landroid/app/Activity;
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/obf/gi;->f:Landroid/app/Activity;

    return-object p1
.end method

.method static synthetic b(Lcom/google/obf/gi;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/obf/gi;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/obf/gi;)Lcom/google/obf/hj;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/obf/gi;->a:Lcom/google/obf/hj;

    return-object v0
.end method

.method static synthetic d(Lcom/google/obf/gi;)Landroid/app/Application;
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/google/obf/gi;->i()Landroid/app/Application;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/google/obf/gi;)Lcom/google/obf/gi$a;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/obf/gi;->e:Lcom/google/obf/gi$a;

    return-object v0
.end method

.method private i()Landroid/app/Application;
    .locals 2

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/obf/gi;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 15
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 16
    instance-of v1, v0, Landroid/app/Application;

    if-eqz v1, :cond_0

    .line 17
    check-cast v0, Landroid/app/Application;

    .line 18
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/ads/interactivemedia/v3/impl/data/a;
    .locals 9

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/google/obf/gi;->g()Lcom/google/ads/interactivemedia/v3/impl/data/a$a;

    move-result-object v0

    .line 55
    invoke-virtual {p0}, Lcom/google/obf/gi;->h()Lcom/google/ads/interactivemedia/v3/impl/data/a$a;

    move-result-object v1

    .line 56
    iget-object v2, p0, Lcom/google/obf/gi;->c:Landroid/view/View;

    invoke-static {v2}, Landroid/support/v4/view/ViewCompat;->isAttachedToWindow(Landroid/view/View;)Z

    move-result v2

    .line 57
    invoke-virtual {p0}, Lcom/google/obf/gi;->f()Z

    move-result v3

    .line 58
    invoke-virtual {p0}, Lcom/google/obf/gi;->e()D

    move-result-wide v4

    .line 59
    iget-object v6, p0, Lcom/google/obf/gi;->d:Lcom/google/obf/gi$b;

    invoke-interface {v6}, Lcom/google/obf/gi$b;->a()J

    move-result-wide v6

    .line 60
    invoke-static {}, Lcom/google/ads/interactivemedia/v3/impl/data/a;->builder()Lcom/google/ads/interactivemedia/v3/impl/data/a$b;

    move-result-object v8

    invoke-interface {v8, p1}, Lcom/google/ads/interactivemedia/v3/impl/data/a$b;->queryId(Ljava/lang/String;)Lcom/google/ads/interactivemedia/v3/impl/data/a$b;

    move-result-object v8

    invoke-interface {v8, p2}, Lcom/google/ads/interactivemedia/v3/impl/data/a$b;->eventId(Ljava/lang/String;)Lcom/google/ads/interactivemedia/v3/impl/data/a$b;

    move-result-object v8

    .line 61
    invoke-interface {v8, p3}, Lcom/google/ads/interactivemedia/v3/impl/data/a$b;->vastEvent(Ljava/lang/String;)Lcom/google/ads/interactivemedia/v3/impl/data/a$b;

    move-result-object v8

    invoke-interface {v8, p4}, Lcom/google/ads/interactivemedia/v3/impl/data/a$b;->appState(Ljava/lang/String;)Lcom/google/ads/interactivemedia/v3/impl/data/a$b;

    move-result-object v8

    invoke-interface {v8, v6, v7}, Lcom/google/ads/interactivemedia/v3/impl/data/a$b;->nativeTime(J)Lcom/google/ads/interactivemedia/v3/impl/data/a$b;

    move-result-object v6

    invoke-interface {v6, v4, v5}, Lcom/google/ads/interactivemedia/v3/impl/data/a$b;->nativeVolume(D)Lcom/google/ads/interactivemedia/v3/impl/data/a$b;

    move-result-object v4

    .line 62
    invoke-interface {v4, v2}, Lcom/google/ads/interactivemedia/v3/impl/data/a$b;->nativeViewAttached(Z)Lcom/google/ads/interactivemedia/v3/impl/data/a$b;

    move-result-object v2

    invoke-interface {v2, v3}, Lcom/google/ads/interactivemedia/v3/impl/data/a$b;->nativeViewHidden(Z)Lcom/google/ads/interactivemedia/v3/impl/data/a$b;

    move-result-object v2

    .line 63
    invoke-interface {v2, v0}, Lcom/google/ads/interactivemedia/v3/impl/data/a$b;->nativeViewBounds(Lcom/google/ads/interactivemedia/v3/impl/data/a$a;)Lcom/google/ads/interactivemedia/v3/impl/data/a$b;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/ads/interactivemedia/v3/impl/data/a$b;->nativeViewVisibleBounds(Lcom/google/ads/interactivemedia/v3/impl/data/a$a;)Lcom/google/ads/interactivemedia/v3/impl/data/a$b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/impl/data/a$b;->build()Lcom/google/ads/interactivemedia/v3/impl/data/a;

    move-result-object v0

    .line 64
    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/obf/gi;->a:Lcom/google/obf/hj;

    iget-object v1, p0, Lcom/google/obf/gi;->b:Ljava/lang/String;

    invoke-virtual {v0, p0, v1}, Lcom/google/obf/hj;->a(Lcom/google/obf/hj$a;Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 65
    const-string v0, ""

    const-string v1, ""

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/obf/gi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/ads/interactivemedia/v3/impl/data/a;

    move-result-object v0

    .line 66
    iget-object v1, p0, Lcom/google/obf/gi;->a:Lcom/google/obf/hj;

    new-instance v2, Lcom/google/obf/hi;

    sget-object v3, Lcom/google/obf/hi$b;->activityMonitor:Lcom/google/obf/hi$b;

    sget-object v4, Lcom/google/obf/hi$c;->viewability:Lcom/google/obf/hi$c;

    iget-object v5, p0, Lcom/google/obf/gi;->b:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/google/obf/hi;-><init>(Lcom/google/obf/hi$b;Lcom/google/obf/hi$c;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Lcom/google/obf/hj;->b(Lcom/google/obf/hi;)V

    .line 67
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 68
    const-string v0, ""

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/obf/gi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/ads/interactivemedia/v3/impl/data/a;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lcom/google/obf/gi;->a:Lcom/google/obf/hj;

    new-instance v2, Lcom/google/obf/hi;

    sget-object v3, Lcom/google/obf/hi$b;->activityMonitor:Lcom/google/obf/hi$b;

    sget-object v4, Lcom/google/obf/hi$c;->viewability:Lcom/google/obf/hi$c;

    iget-object v5, p0, Lcom/google/obf/gi;->b:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/google/obf/hi;-><init>(Lcom/google/obf/hi$b;Lcom/google/obf/hi$c;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Lcom/google/obf/hj;->b(Lcom/google/obf/hi;)V

    .line 70
    return-void
.end method

.method protected a(Z)V
    .locals 0

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/google/obf/gi;->g:Z

    .line 13
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/obf/gi;->a:Lcom/google/obf/hj;

    iget-object v1, p0, Lcom/google/obf/gi;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/obf/hj;->b(Ljava/lang/String;)V

    .line 22
    return-void
.end method

.method public c()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 23
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/obf/gi;->g:Z

    if-eqz v0, :cond_0

    .line 24
    invoke-direct {p0}, Lcom/google/obf/gi;->i()Landroid/app/Application;

    move-result-object v0

    .line 25
    if-eqz v0, :cond_0

    .line 26
    new-instance v1, Lcom/google/obf/gi$a;

    invoke-direct {v1, p0}, Lcom/google/obf/gi$a;-><init>(Lcom/google/obf/gi;)V

    iput-object v1, p0, Lcom/google/obf/gi;->e:Lcom/google/obf/gi$a;

    .line 27
    iget-object v1, p0, Lcom/google/obf/gi;->e:Lcom/google/obf/gi$a;

    invoke-virtual {v0, v1}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 28
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 29
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 30
    invoke-direct {p0}, Lcom/google/obf/gi;->i()Landroid/app/Application;

    move-result-object v0

    .line 31
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/obf/gi;->e:Lcom/google/obf/gi$a;

    if-eqz v1, :cond_0

    .line 32
    iget-object v1, p0, Lcom/google/obf/gi;->e:Lcom/google/obf/gi$a;

    invoke-virtual {v0, v1}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 33
    :cond_0
    return-void
.end method

.method public e()D
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 34
    const-wide/16 v2, 0x0

    .line 35
    iget-object v0, p0, Lcom/google/obf/gi;->c:Landroid/view/View;

    .line 36
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 37
    if-eqz v0, :cond_0

    .line 38
    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    .line 39
    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    .line 40
    int-to-double v2, v1

    int-to-double v0, v0

    div-double v0, v2, v0

    .line 41
    :goto_0
    return-wide v0

    :cond_0
    move-wide v0, v2

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 42
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 43
    iget-object v1, p0, Lcom/google/obf/gi;->c:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    .line 44
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/gi;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lcom/google/ads/interactivemedia/v3/impl/data/a$a;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/obf/gi;->c:Landroid/view/View;

    invoke-static {v0}, Lcom/google/ads/interactivemedia/v3/impl/data/a$a;->createFromLocationOnScreen(Landroid/view/View;)Lcom/google/ads/interactivemedia/v3/impl/data/a$a;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/google/ads/interactivemedia/v3/impl/data/a$a;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 46
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 47
    iget-object v0, p0, Lcom/google/obf/gi;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v3

    .line 48
    iget-object v0, p0, Lcom/google/obf/gi;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 49
    :goto_0
    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/gi;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_1

    .line 50
    :cond_0
    invoke-virtual {v2, v1, v1, v1, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 51
    :cond_1
    iget v0, v2, Landroid/graphics/Rect;->left:I

    iget v1, v2, Landroid/graphics/Rect;->top:I

    .line 52
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 53
    invoke-static {v0, v1, v3, v2}, Lcom/google/ads/interactivemedia/v3/impl/data/a$a;->create(IIII)Lcom/google/ads/interactivemedia/v3/impl/data/a$a;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v1

    .line 48
    goto :goto_0
.end method
