.class Lru/cn/tv/mobile/linkdevice/LinkDeviceViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "LinkDeviceViewModel.java"


# instance fields
.field private final linkUri:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 25
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/mobile/linkdevice/LinkDeviceViewModel;->linkUri:Landroid/arch/lifecycle/MutableLiveData;

    .line 27
    invoke-direct {p0, p1}, Lru/cn/tv/mobile/linkdevice/LinkDeviceViewModel;->pinUri(Landroid/content/Context;)Lio/reactivex/Observable;

    move-result-object v0

    .line 28
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lru/cn/tv/mobile/linkdevice/LinkDeviceViewModel$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/linkdevice/LinkDeviceViewModel$$Lambda$0;-><init>(Lru/cn/tv/mobile/linkdevice/LinkDeviceViewModel;)V

    new-instance v2, Lru/cn/tv/mobile/linkdevice/LinkDeviceViewModel$$Lambda$1;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/linkdevice/LinkDeviceViewModel$$Lambda$1;-><init>(Lru/cn/tv/mobile/linkdevice/LinkDeviceViewModel;)V

    .line 29
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 27
    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/linkdevice/LinkDeviceViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 31
    return-void
.end method

.method static final synthetic lambda$pinUri$2$LinkDeviceViewModel(Landroid/content/Context;)Landroid/net/Uri;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 38
    const-wide/16 v0, 0x2

    const-string v2, "ptv29783051://callback"

    invoke-static {p0, v0, v1, v2}, Lru/cn/api/authorization/Authorization;->pinAuthorizationUri(Landroid/content/Context;JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private pinUri(Landroid/content/Context;)Lio/reactivex/Observable;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    new-instance v0, Lru/cn/tv/mobile/linkdevice/LinkDeviceViewModel$$Lambda$2;

    invoke-direct {v0, p1}, Lru/cn/tv/mobile/linkdevice/LinkDeviceViewModel$$Lambda$2;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lio/reactivex/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method final synthetic lambda$new$0$LinkDeviceViewModel(Landroid/net/Uri;)V
    .locals 1
    .param p1, "it"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lru/cn/tv/mobile/linkdevice/LinkDeviceViewModel;->linkUri:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v0, p1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method final synthetic lambda$new$1$LinkDeviceViewModel(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "throwable"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lru/cn/tv/mobile/linkdevice/LinkDeviceViewModel;->linkUri:Landroid/arch/lifecycle/MutableLiveData;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method linkUri()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lru/cn/tv/mobile/linkdevice/LinkDeviceViewModel;->linkUri:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method
