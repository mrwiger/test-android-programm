.class public Lcom/my/target/bx;
.super Landroid/widget/ImageView;
.source "FitBitmapImageView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "AppCompatCustomView"
    }
.end annotation


# instance fields
.field private iq:Landroid/graphics/Bitmap;

.field private is:I

.field private it:I

.field private maxHeight:I

.field private maxWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 78
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 79
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 82
    iget v2, p0, Lcom/my/target/bx;->it:I

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/my/target/bx;->is:I

    if-eqz v2, :cond_1

    .line 84
    iget v3, p0, Lcom/my/target/bx;->is:I

    .line 85
    iget v2, p0, Lcom/my/target/bx;->it:I

    .line 98
    :goto_0
    if-eqz v3, :cond_0

    if-nez v2, :cond_3

    .line 100
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onMeasure(II)V

    .line 127
    :goto_1
    return-void

    .line 87
    :cond_1
    iget-object v2, p0, Lcom/my/target/bx;->iq:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_2

    .line 89
    iget-object v2, p0, Lcom/my/target/bx;->iq:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 90
    iget-object v2, p0, Lcom/my/target/bx;->iq:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    goto :goto_0

    .line 94
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onMeasure(II)V

    goto :goto_1

    .line 104
    :cond_3
    int-to-float v4, v3

    int-to-float v5, v2

    div-float/2addr v4, v5

    .line 106
    iget v5, p0, Lcom/my/target/bx;->maxHeight:I

    if-lez v5, :cond_4

    .line 108
    iget v5, p0, Lcom/my/target/bx;->maxHeight:I

    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 110
    :cond_4
    iget v5, p0, Lcom/my/target/bx;->maxWidth:I

    if-lez v5, :cond_5

    .line 112
    iget v5, p0, Lcom/my/target/bx;->maxWidth:I

    invoke-static {v5, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 115
    :cond_5
    int-to-float v5, v1

    int-to-float v3, v3

    div-float v3, v5, v3

    .line 116
    int-to-float v5, v0

    int-to-float v2, v2

    div-float v2, v5, v2

    .line 117
    invoke-static {v3, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    cmpl-float v2, v2, v3

    if-nez v2, :cond_6

    const/4 v2, 0x0

    cmpl-float v2, v4, v2

    if-lez v2, :cond_6

    .line 119
    int-to-float v0, v1

    div-float/2addr v0, v4

    float-to-int v0, v0

    .line 126
    :goto_2
    invoke-virtual {p0, v1, v0}, Lcom/my/target/bx;->setMeasuredDimension(II)V

    goto :goto_1

    .line 123
    :cond_6
    int-to-float v1, v0

    mul-float/2addr v1, v4

    float-to-int v1, v1

    goto :goto_2
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    .line 47
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/my/target/bx;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 48
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/my/target/bx;->setAdjustViewBounds(Z)V

    .line 49
    iput-object p1, p0, Lcom/my/target/bx;->iq:Landroid/graphics/Bitmap;

    .line 50
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 51
    return-void
.end method

.method public setImageData(Lcom/my/target/common/models/ImageData;)V
    .locals 1
    .param p1, "imageData"    # Lcom/my/target/common/models/ImageData;

    .prologue
    .line 39
    invoke-virtual {p1}, Lcom/my/target/common/models/ImageData;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/my/target/bx;->it:I

    .line 40
    invoke-virtual {p1}, Lcom/my/target/common/models/ImageData;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/my/target/bx;->is:I

    .line 41
    invoke-virtual {p1}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/my/target/bx;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 42
    return-void
.end method

.method public setMaxHeight(I)V
    .locals 0
    .param p1, "maxHeight"    # I

    .prologue
    .line 72
    iput p1, p0, Lcom/my/target/bx;->maxHeight:I

    .line 73
    return-void
.end method

.method public setMaxWidth(I)V
    .locals 0
    .param p1, "maxWidth"    # I

    .prologue
    .line 66
    iput p1, p0, Lcom/my/target/bx;->maxWidth:I

    .line 67
    return-void
.end method

.method public setPlaceholderHeight(I)V
    .locals 0
    .param p1, "placeholderHeight"    # I

    .prologue
    .line 55
    iput p1, p0, Lcom/my/target/bx;->it:I

    .line 56
    return-void
.end method

.method public setPlaceholderWidth(I)V
    .locals 0
    .param p1, "placeholderWidth"    # I

    .prologue
    .line 60
    iput p1, p0, Lcom/my/target/bx;->is:I

    .line 61
    return-void
.end method
