.class Lcom/yandex/metrica/impl/ob/bs$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/impl/ob/bs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ob/bs$a$a;
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/yandex/metrica/impl/ob/bs$a$a;

.field private final c:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/bs$a$a;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Lcom/yandex/metrica/impl/ob/bs$a$1;

    invoke-direct {v0, p0}, Lcom/yandex/metrica/impl/ob/bs$a$1;-><init>(Lcom/yandex/metrica/impl/ob/bs$a;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bs$a;->c:Landroid/content/ServiceConnection;

    .line 59
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/bs$a;->a:Landroid/content/Context;

    .line 60
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/bs$a;->b:Lcom/yandex/metrica/impl/ob/bs$a$a;

    .line 61
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bs$a;->b:Lcom/yandex/metrica/impl/ob/bs$a$a;

    invoke-interface {v0}, Lcom/yandex/metrica/impl/ob/bs$a$a;->b()V

    .line 111
    return-void
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/ob/bs$a;)V
    .locals 1

    .prologue
    .line 43
    .line 1106
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bs$a;->b:Lcom/yandex/metrica/impl/ob/bs$a$a;

    invoke-interface {v0}, Lcom/yandex/metrica/impl/ob/bs$a$a;->a()V

    .line 43
    return-void
.end method

.method static synthetic b(Lcom/yandex/metrica/impl/ob/bs$a;)V
    .locals 2

    .prologue
    .line 43
    .line 2099
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bs$a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/bs$a;->c:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2102
    :goto_0
    return-void

    .line 43
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 79
    .line 1085
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bs$a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/bs$a;->c:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 1086
    if-nez v0, :cond_0

    .line 1088
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/bs$a;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1093
    :cond_0
    :goto_0
    return-void

    .line 1092
    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/bs$a;->a()V

    goto :goto_0
.end method
