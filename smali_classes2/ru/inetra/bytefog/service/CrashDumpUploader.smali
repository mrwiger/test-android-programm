.class final Lru/inetra/bytefog/service/CrashDumpUploader;
.super Ljava/lang/Object;
.source "CrashDumpUploader.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final URL:Ljava/lang/String; = "http://crashes.cn.ru/crashes/upload-minidump/"

.field private static final dumpExtension:Ljava/lang/String; = ".dmp"


# instance fields
.field private dumpsPath:Ljava/lang/String;

.field private userId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "dumpsPath"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lru/inetra/bytefog/service/CrashDumpUploader;->userId:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lru/inetra/bytefog/service/CrashDumpUploader;->dumpsPath:Ljava/lang/String;

    .line 20
    return-void
.end method

.method private deleteDumps(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p1, "dumps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 65
    .local v0, "dump":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_0

    .line 66
    invoke-virtual {v0}, Ljava/io/File;->deleteOnExit()V

    goto :goto_0

    .line 69
    .end local v0    # "dump":Ljava/io/File;
    :cond_1
    return-void
.end method

.method private findDumpsInPath(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 8
    .param p1, "dumpsPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 43
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    .line 44
    .local v4, "files":[Ljava/io/File;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 46
    .local v1, "dumps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    array-length v6, v4

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v6, :cond_1

    aget-object v2, v4, v5

    .line 47
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 48
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 49
    .local v3, "filename":Ljava/lang/String;
    const-string v7, ".dmp"

    invoke-virtual {v3, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 50
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    .end local v3    # "filename":Ljava/lang/String;
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 54
    .end local v2    # "file":Ljava/io/File;
    :cond_1
    return-object v1
.end method

.method private sendDumps(Lru/inetra/bytefog/service/HttpClient;Ljava/util/ArrayList;)V
    .locals 3
    .param p1, "client"    # Lru/inetra/bytefog/service/HttpClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/inetra/bytefog/service/HttpClient;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 58
    .local p2, "dumps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 59
    .local v0, "dump":Ljava/io/File;
    const-string v2, "dump"

    invoke-virtual {p1, v2, v0}, Lru/inetra/bytefog/service/HttpClient;->addFilePart(Ljava/lang/String;Ljava/io/File;)V

    goto :goto_0

    .line 61
    .end local v0    # "dump":Ljava/io/File;
    :cond_0
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 23
    iget-object v3, p0, Lru/inetra/bytefog/service/CrashDumpUploader;->dumpsPath:Ljava/lang/String;

    invoke-direct {p0, v3}, Lru/inetra/bytefog/service/CrashDumpUploader;->findDumpsInPath(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 25
    .local v1, "dumps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 26
    const-string v3, "bytefog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Found "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " crash dumps. Sending..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    new-instance v0, Lru/inetra/bytefog/service/HttpClient;

    const-string v3, "http://crashes.cn.ru/crashes/upload-minidump/"

    invoke-direct {v0, v3}, Lru/inetra/bytefog/service/HttpClient;-><init>(Ljava/lang/String;)V

    .line 29
    .local v0, "client":Lru/inetra/bytefog/service/HttpClient;
    :try_start_0
    invoke-virtual {v0}, Lru/inetra/bytefog/service/HttpClient;->connectForMultipart()V

    .line 30
    const-string v3, "uid"

    iget-object v4, p0, Lru/inetra/bytefog/service/CrashDumpUploader;->userId:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lru/inetra/bytefog/service/HttpClient;->addFormPart(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0, v0, v1}, Lru/inetra/bytefog/service/CrashDumpUploader;->sendDumps(Lru/inetra/bytefog/service/HttpClient;Ljava/util/ArrayList;)V

    .line 32
    invoke-virtual {v0}, Lru/inetra/bytefog/service/HttpClient;->finishMultipart()V

    .line 33
    invoke-virtual {v0}, Lru/inetra/bytefog/service/HttpClient;->getResponse()Ljava/lang/String;

    move-result-object v2

    .line 34
    .local v2, "response":Ljava/lang/String;
    invoke-direct {p0, v1}, Lru/inetra/bytefog/service/CrashDumpUploader;->deleteDumps(Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 39
    .end local v0    # "client":Lru/inetra/bytefog/service/HttpClient;
    .end local v2    # "response":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 36
    .restart local v0    # "client":Lru/inetra/bytefog/service/HttpClient;
    :catch_0
    move-exception v3

    goto :goto_0
.end method
