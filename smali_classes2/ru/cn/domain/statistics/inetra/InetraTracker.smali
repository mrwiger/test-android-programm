.class public Lru/cn/domain/statistics/inetra/InetraTracker;
.super Ljava/lang/Object;
.source "InetraTracker.java"

# interfaces
.implements Lru/cn/domain/statistics/inetra/SessionTracker$OnSessionChangeListener;


# static fields
.field private static INSTANCE:Lru/cn/domain/statistics/inetra/InetraTracker;

.field private static _duid:Ljava/lang/String;

.field private static bufferingCounter:Lru/cn/domain/statistics/inetra/BufferingCounter;

.field static currentAppMode:Ljava/lang/Integer;

.field private static currentChannelId:Ljava/lang/Long;

.field private static currentChannelTitle:Ljava/lang/String;

.field private static currentCollectionId:J

.field private static currentStartMode:Ljava/lang/Integer;

.field private static currentTelecastId:Ljava/lang/Long;

.field private static currentViewFrom:Ljava/lang/Integer;

.field private static currentViewScreen:Ljava/lang/String;

.field private static viewSession:Lru/cn/domain/statistics/inetra/ViewSession;


# instance fields
.field private final sender:Lru/cn/domain/statistics/inetra/TrackingSender;

.field private final sessionTracker:Lru/cn/domain/statistics/inetra/SessionTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 59
    sput-object v2, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    .line 60
    new-instance v0, Lru/cn/domain/statistics/inetra/BufferingCounter;

    invoke-direct {v0}, Lru/cn/domain/statistics/inetra/BufferingCounter;-><init>()V

    sput-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->bufferingCounter:Lru/cn/domain/statistics/inetra/BufferingCounter;

    .line 62
    sput-object v2, Lru/cn/domain/statistics/inetra/InetraTracker;->currentViewFrom:Ljava/lang/Integer;

    .line 63
    sput-object v2, Lru/cn/domain/statistics/inetra/InetraTracker;->currentStartMode:Ljava/lang/Integer;

    .line 64
    const-string v0, "0"

    sput-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->currentViewScreen:Ljava/lang/String;

    .line 66
    const-wide/16 v0, 0x0

    sput-wide v0, Lru/cn/domain/statistics/inetra/InetraTracker;->currentCollectionId:J

    .line 68
    sput-object v2, Lru/cn/domain/statistics/inetra/InetraTracker;->currentTelecastId:Ljava/lang/Long;

    .line 69
    sput-object v2, Lru/cn/domain/statistics/inetra/InetraTracker;->currentChannelId:Ljava/lang/Long;

    .line 70
    sput-object v2, Lru/cn/domain/statistics/inetra/InetraTracker;->currentChannelTitle:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Lru/cn/domain/statistics/inetra/TrackingSender;

    invoke-static {p1}, Lru/cn/domain/statistics/inetra/InetraTracker;->getDuid(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lru/cn/domain/statistics/inetra/TrackingSender;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lru/cn/domain/statistics/inetra/InetraTracker;->sender:Lru/cn/domain/statistics/inetra/TrackingSender;

    .line 81
    new-instance v0, Lru/cn/domain/statistics/inetra/SessionTracker;

    invoke-direct {v0, p1, p0}, Lru/cn/domain/statistics/inetra/SessionTracker;-><init>(Landroid/content/Context;Lru/cn/domain/statistics/inetra/SessionTracker$OnSessionChangeListener;)V

    iput-object v0, p0, Lru/cn/domain/statistics/inetra/InetraTracker;->sessionTracker:Lru/cn/domain/statistics/inetra/SessionTracker;

    .line 82
    return-void
.end method

.method public static advEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/lang/String;Lru/cn/api/money_miner/replies/AdSystem;Ljava/util/Map;)V
    .locals 5
    .param p0, "type"    # Lru/cn/domain/statistics/inetra/AdvEvent;
    .param p1, "placeId"    # Ljava/lang/String;
    .param p2, "adSystem"    # Lru/cn/api/money_miner/replies/AdSystem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/domain/statistics/inetra/AdvEvent;",
            "Ljava/lang/String;",
            "Lru/cn/api/money_miner/replies/AdSystem;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 475
    .local p3, "extraParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "adv/event"

    invoke-static {v3}, Lru/cn/domain/statistics/inetra/InetraTracker;->createEvent(Ljava/lang/String;)Lru/cn/domain/statistics/inetra/TrackingEvent;

    move-result-object v0

    .line 476
    .local v0, "event":Lru/cn/domain/statistics/inetra/TrackingEvent;
    const-string v3, "_type"

    iget-object v4, p0, Lru/cn/domain/statistics/inetra/AdvEvent;->value:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    const-string v3, "_place_id"

    invoke-virtual {v0, v3, p1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    .line 480
    .local v2, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    .line 481
    const-string v3, "network_id"

    iget v4, p2, Lru/cn/api/money_miner/replies/AdSystem;->id:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 483
    sget-object v3, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_START:Lru/cn/domain/statistics/inetra/AdvEvent;

    invoke-virtual {p0, v3}, Lru/cn/domain/statistics/inetra/AdvEvent;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, p2, Lru/cn/api/money_miner/replies/AdSystem;->price:I

    if-lez v3, :cond_0

    .line 484
    const-string v3, "price"

    iget v4, p2, Lru/cn/api/money_miner/replies/AdSystem;->price:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 488
    :cond_0
    if-eqz p3, :cond_1

    .line 489
    invoke-interface {v2, p3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 492
    :cond_1
    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 493
    invoke-static {v2}, Lcom/annimon/stream/Stream;->of(Ljava/util/Map;)Lcom/annimon/stream/Stream;

    move-result-object v3

    sget-object v4, Lru/cn/domain/statistics/inetra/InetraTracker$$Lambda$1;->$instance:Lcom/annimon/stream/function/Function;

    .line 494
    invoke-virtual {v3, v4}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v3

    const-string v4, ";"

    .line 495
    invoke-static {v4}, Lcom/annimon/stream/Collectors;->joining(Ljava/lang/CharSequence;)Lcom/annimon/stream/Collector;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/annimon/stream/Stream;->collect(Lcom/annimon/stream/Collector;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 497
    .local v1, "message":Ljava/lang/String;
    const-string v3, "_message"

    invoke-virtual {v0, v3, v1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    .end local v1    # "message":Ljava/lang/String;
    :cond_2
    invoke-static {v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->sendEvent(Lru/cn/domain/statistics/inetra/TrackingEvent;)V

    .line 501
    return-void
.end method

.method private static bufferingError(I)V
    .locals 10
    .param p0, "mode"    # I

    .prologue
    .line 559
    sget-object v5, Lru/cn/domain/statistics/inetra/InetraTracker;->bufferingCounter:Lru/cn/domain/statistics/inetra/BufferingCounter;

    iget v0, v5, Lru/cn/domain/statistics/inetra/BufferingCounter;->bufferingCount:I

    .line 560
    .local v0, "bufferingCount":I
    sget-object v5, Lru/cn/domain/statistics/inetra/InetraTracker;->bufferingCounter:Lru/cn/domain/statistics/inetra/BufferingCounter;

    iget v5, v5, Lru/cn/domain/statistics/inetra/BufferingCounter;->bufferingCount:I

    if-nez v5, :cond_0

    .line 602
    :goto_0
    return-void

    .line 564
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "events="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ";"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 565
    .local v3, "message":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "stalling_time="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lru/cn/domain/statistics/inetra/InetraTracker;->bufferingCounter:Lru/cn/domain/statistics/inetra/BufferingCounter;

    iget-wide v6, v6, Lru/cn/domain/statistics/inetra/BufferingCounter;->totalBufferingTimeMs:J

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ";"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 566
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "playback_time="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lru/cn/domain/statistics/inetra/InetraTracker;->bufferingCounter:Lru/cn/domain/statistics/inetra/BufferingCounter;

    iget-wide v6, v6, Lru/cn/domain/statistics/inetra/BufferingCounter;->totalPlayingContentTime:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ";"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 568
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "view_mode="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget v6, v6, Lru/cn/domain/statistics/inetra/ViewSession;->viewMode:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ";"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 570
    sget-object v5, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget v5, v5, Lru/cn/domain/statistics/inetra/ViewSession;->bitrate:I

    if-lez v5, :cond_1

    .line 571
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "bandwidth="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget v6, v6, Lru/cn/domain/statistics/inetra/ViewSession;->bitrate:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ";"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 574
    :cond_1
    sget-object v5, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget v4, v5, Lru/cn/domain/statistics/inetra/ViewSession;->playerType:I

    .line 575
    .local v4, "player":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "player="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ";"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 576
    sget-object v5, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget-wide v6, v5, Lru/cn/domain/statistics/inetra/ViewSession;->contractorId:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_2

    .line 577
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "contractor="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget-wide v6, v6, Lru/cn/domain/statistics/inetra/ViewSession;->contractorId:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ";"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 580
    :cond_2
    sget-object v5, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget-object v5, v5, Lru/cn/domain/statistics/inetra/ViewSession;->sourceUri:Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 581
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "url="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget-object v6, v6, Lru/cn/domain/statistics/inetra/ViewSession;->sourceUri:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ";"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 583
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "content_url="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget-object v6, v6, Lru/cn/domain/statistics/inetra/ViewSession;->contentUri:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 586
    sget-object v1, Lru/cn/domain/statistics/inetra/ErrorCode;->LOW_DOWNLOAD_SPEED:Lru/cn/domain/statistics/inetra/ErrorCode;

    .line 587
    .local v1, "errorCode":Lru/cn/domain/statistics/inetra/ErrorCode;
    sget-object v5, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget-object v5, v5, Lru/cn/domain/statistics/inetra/ViewSession;->currentProtocol:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_4

    .line 588
    sget-object v1, Lru/cn/domain/statistics/inetra/ErrorCode;->LOW_DOWNLOAD_SPEED_P2P:Lru/cn/domain/statistics/inetra/ErrorCode;

    .line 591
    :cond_4
    const/16 v2, 0x321

    .line 592
    .local v2, "internalCode":I
    sparse-switch p0, :sswitch_data_0

    .line 601
    :goto_1
    const-string v5, "BufferingStatistic"

    invoke-static {v1, v5, v2, v3}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 594
    :sswitch_0
    const/16 v2, 0x325

    .line 595
    goto :goto_1

    .line 597
    :sswitch_1
    const/16 v2, 0x324

    goto :goto_1

    .line 592
    :sswitch_data_0
    .sparse-switch
        0x4b0 -> :sswitch_0
        0x514 -> :sswitch_1
    .end sparse-switch
.end method

.method public static channelStart(JLjava/lang/String;I)V
    .locals 2
    .param p0, "cnId"    # J
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "player"    # I

    .prologue
    .line 153
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    invoke-virtual {v0}, Lru/cn/domain/statistics/inetra/ViewSession;->startCounting()V

    .line 154
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iput p3, v0, Lru/cn/domain/statistics/inetra/ViewSession;->playerType:I

    .line 156
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->currentChannelId:Ljava/lang/Long;

    .line 157
    sput-object p2, Lru/cn/domain/statistics/inetra/InetraTracker;->currentChannelTitle:Ljava/lang/String;

    .line 158
    const/4 v0, 0x0

    sput-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->currentTelecastId:Ljava/lang/Long;

    .line 159
    return-void
.end method

.method private static createEvent(Ljava/lang/String;)Lru/cn/domain/statistics/inetra/TrackingEvent;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 605
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->INSTANCE:Lru/cn/domain/statistics/inetra/InetraTracker;

    iget-object v0, v0, Lru/cn/domain/statistics/inetra/InetraTracker;->sender:Lru/cn/domain/statistics/inetra/TrackingSender;

    sget-object v1, Lru/cn/domain/statistics/inetra/InetraTracker;->INSTANCE:Lru/cn/domain/statistics/inetra/InetraTracker;

    iget-object v1, v1, Lru/cn/domain/statistics/inetra/InetraTracker;->sessionTracker:Lru/cn/domain/statistics/inetra/SessionTracker;

    invoke-virtual {v1}, Lru/cn/domain/statistics/inetra/SessionTracker;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lru/cn/domain/statistics/inetra/TrackingSender;->createEvent(Ljava/lang/String;Ljava/lang/String;)Lru/cn/domain/statistics/inetra/TrackingEvent;

    move-result-object v0

    return-object v0
.end method

.method public static createTrackingSession(IJLjava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p0, "viewMode"    # I
    .param p1, "contractorId"    # J
    .param p3, "sourceUri"    # Ljava/lang/String;
    .param p4, "contentUri"    # Ljava/lang/String;

    .prologue
    .line 125
    const/4 v6, 0x0

    move v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v1 .. v6}, Lru/cn/domain/statistics/inetra/InetraTracker;->createTrackingSession(IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    return-void
.end method

.method public static createTrackingSession(IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p0, "viewMode"    # I
    .param p1, "contractorId"    # J
    .param p3, "sourceUri"    # Ljava/lang/String;
    .param p4, "contentUri"    # Ljava/lang/String;
    .param p5, "locationTitle"    # Ljava/lang/String;

    .prologue
    .line 131
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    if-eqz v0, :cond_0

    .line 132
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    invoke-virtual {v0}, Lru/cn/domain/statistics/inetra/ViewSession;->pauseCounting()V

    .line 133
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    invoke-virtual {v0}, Lru/cn/domain/statistics/inetra/ViewSession;->getWatchedTime()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 134
    const/16 v0, 0x3e8

    invoke-static {v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->watch(I)V

    .line 136
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Watch time tracked in unexpected way"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lru/cn/utils/Logger;->logException(Ljava/lang/Throwable;)V

    .line 140
    :cond_0
    new-instance v0, Lru/cn/domain/statistics/inetra/ViewSession;

    sget-object v2, Lru/cn/domain/statistics/inetra/InetraTracker;->currentViewFrom:Ljava/lang/Integer;

    sget-object v3, Lru/cn/domain/statistics/inetra/InetraTracker;->currentStartMode:Ljava/lang/Integer;

    sget-wide v4, Lru/cn/domain/statistics/inetra/InetraTracker;->currentCollectionId:J

    move v1, p0

    invoke-direct/range {v0 .. v5}, Lru/cn/domain/statistics/inetra/ViewSession;-><init>(ILjava/lang/Integer;Ljava/lang/Integer;J)V

    sput-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    .line 141
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iput-wide p1, v0, Lru/cn/domain/statistics/inetra/ViewSession;->contractorId:J

    .line 142
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iput-object p3, v0, Lru/cn/domain/statistics/inetra/ViewSession;->sourceUri:Ljava/lang/String;

    .line 143
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iput-object p4, v0, Lru/cn/domain/statistics/inetra/ViewSession;->contentUri:Ljava/lang/String;

    .line 144
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iput-object p5, v0, Lru/cn/domain/statistics/inetra/ViewSession;->locationTitle:Ljava/lang/String;

    .line 146
    const-string v0, "magnet:"

    invoke-virtual {p4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 147
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lru/cn/domain/statistics/inetra/ViewSession;->currentProtocol:Ljava/lang/Integer;

    .line 149
    :cond_1
    return-void
.end method

.method public static error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V
    .locals 3
    .param p0, "code"    # Lru/cn/domain/statistics/inetra/ErrorCode;
    .param p1, "domain"    # Ljava/lang/String;
    .param p2, "internalCode"    # I
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 431
    const-string v1, "error"

    invoke-static {v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->createEvent(Ljava/lang/String;)Lru/cn/domain/statistics/inetra/TrackingEvent;

    move-result-object v0

    .line 432
    .local v0, "event":Lru/cn/domain/statistics/inetra/TrackingEvent;
    const-string v1, "_internal_code"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    sget-object v1, Lru/cn/domain/statistics/inetra/ErrorCode;->UNKNOWN_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    if-eq p0, v1, :cond_0

    .line 435
    const-string v1, "_code"

    invoke-virtual {p0}, Lru/cn/domain/statistics/inetra/ErrorCode;->getValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    :cond_0
    if-eqz p1, :cond_1

    .line 439
    const-string v1, "_domain"

    invoke-virtual {v0, v1, p1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    :cond_1
    if-eqz p3, :cond_2

    .line 443
    const-string v1, "_message"

    invoke-virtual {v0, v1, p3}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    :cond_2
    invoke-static {v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->sendEvent(Lru/cn/domain/statistics/inetra/TrackingEvent;)V

    .line 447
    return-void
.end method

.method public static getCollectionId()J
    .locals 2

    .prologue
    .line 510
    sget-wide v0, Lru/cn/domain/statistics/inetra/InetraTracker;->currentCollectionId:J

    return-wide v0
.end method

.method public static getDuid(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 522
    sget-object v1, Lru/cn/domain/statistics/inetra/InetraTracker;->_duid:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 523
    sget-object v1, Lru/cn/domain/statistics/inetra/InetraTracker;->_duid:Ljava/lang/String;

    .line 531
    :goto_0
    return-object v1

    .line 525
    :cond_0
    invoke-static {p0}, Lru/cn/utils/Utils;->getAndroidId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 526
    .local v0, "androidId":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 527
    const/4 v1, 0x0

    goto :goto_0

    .line 529
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-static {v1}, Ljava/util/UUID;->nameUUIDFromBytes([B)Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lru/cn/domain/statistics/inetra/InetraTracker;->_duid:Ljava/lang/String;

    .line 531
    sget-object v1, Lru/cn/domain/statistics/inetra/InetraTracker;->_duid:Ljava/lang/String;

    goto :goto_0
.end method

.method public static getViewFrom()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 505
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->currentViewFrom:Ljava/lang/Integer;

    return-object v0
.end method

.method static final synthetic lambda$advEvent$1$InetraTracker(Ljava/util/Map$Entry;)Ljava/lang/String;
    .locals 2
    .param p0, "entry"    # Ljava/util/Map$Entry;

    .prologue
    .line 494
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static final synthetic lambda$tracking$0$InetraTracker(Ljava/util/Map$Entry;)Ljava/lang/String;
    .locals 2
    .param p0, "entry"    # Ljava/util/Map$Entry;

    .prologue
    .line 460
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static playbackBuffering(ZZ)V
    .locals 1
    .param p0, "isBuffering"    # Z
    .param p1, "playing"    # Z

    .prologue
    .line 98
    if-eqz p0, :cond_1

    .line 99
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->bufferingCounter:Lru/cn/domain/statistics/inetra/BufferingCounter;

    invoke-virtual {v0}, Lru/cn/domain/statistics/inetra/BufferingCounter;->startCountBuffering()V

    .line 101
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    if-eqz v0, :cond_0

    .line 102
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    invoke-virtual {v0}, Lru/cn/domain/statistics/inetra/ViewSession;->pauseCounting()V

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->bufferingCounter:Lru/cn/domain/statistics/inetra/BufferingCounter;

    invoke-virtual {v0}, Lru/cn/domain/statistics/inetra/BufferingCounter;->stopCountBuffering()V

    .line 107
    if-eqz p1, :cond_0

    .line 108
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    invoke-virtual {v0}, Lru/cn/domain/statistics/inetra/ViewSession;->startCounting()V

    goto :goto_0
.end method

.method public static playbackError(ZIIJLjava/lang/String;J)V
    .locals 8
    .param p0, "playbackStarted"    # Z
    .param p1, "playerType"    # I
    .param p2, "code"    # I
    .param p3, "channelId"    # J
    .param p5, "channelTitle"    # Ljava/lang/String;
    .param p6, "telecastId"    # J

    .prologue
    const/4 v4, 0x2

    const-wide/16 v6, 0x0

    .line 314
    const-string v1, "unknown"

    .line 315
    .local v1, "errorDomain":Ljava/lang/String;
    sparse-switch p1, :sswitch_data_0

    .line 329
    :goto_0
    sget-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->UNABLE_START_VIDEO:Lru/cn/domain/statistics/inetra/ErrorCode;

    .line 330
    .local v0, "errorCode":Lru/cn/domain/statistics/inetra/ErrorCode;
    if-eqz p0, :cond_6

    .line 331
    sget-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->PLAY_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    .line 332
    sget-object v3, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget-object v3, v3, Lru/cn/domain/statistics/inetra/ViewSession;->currentProtocol:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, v4, :cond_0

    .line 333
    sget-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->PLAY_ERROR_P2P:Lru/cn/domain/statistics/inetra/ErrorCode;

    .line 341
    :cond_0
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "content_url="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget-object v4, v4, Lru/cn/domain/statistics/inetra/ViewSession;->contentUri:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 342
    .local v2, "message":Ljava/lang/String;
    cmp-long v3, p3, v6

    if-lez v3, :cond_1

    .line 343
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";channel_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 346
    :cond_1
    if-eqz p5, :cond_2

    .line 347
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";channel_title="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 350
    :cond_2
    cmp-long v3, p6, v6

    if-lez v3, :cond_3

    .line 351
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";telecast_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p6, p7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 354
    :cond_3
    sget-object v3, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget-wide v4, v3, Lru/cn/domain/statistics/inetra/ViewSession;->contractorId:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_4

    .line 355
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";contractor="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget-wide v4, v4, Lru/cn/domain/statistics/inetra/ViewSession;->contractorId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 358
    :cond_4
    sget-object v3, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget-object v3, v3, Lru/cn/domain/statistics/inetra/ViewSession;->sourceUri:Ljava/lang/String;

    if-eqz v3, :cond_5

    .line 359
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";url="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget-object v4, v4, Lru/cn/domain/statistics/inetra/ViewSession;->sourceUri:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 362
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";view_mode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget v4, v4, Lru/cn/domain/statistics/inetra/ViewSession;->viewMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 363
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";player="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 365
    invoke-static {v0, v1, p2, v2}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    .line 366
    return-void

    .line 317
    .end local v0    # "errorCode":Lru/cn/domain/statistics/inetra/ErrorCode;
    .end local v2    # "message":Ljava/lang/String;
    :sswitch_0
    const-string v1, "MediaPlayerError"

    .line 318
    goto/16 :goto_0

    .line 321
    :sswitch_1
    const-string v1, "ChromecastPlayerError"

    .line 322
    goto/16 :goto_0

    .line 325
    :sswitch_2
    const-string v1, "ExoPlayer2Error"

    goto/16 :goto_0

    .line 336
    .restart local v0    # "errorCode":Lru/cn/domain/statistics/inetra/ErrorCode;
    :cond_6
    sget-object v3, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget-object v3, v3, Lru/cn/domain/statistics/inetra/ViewSession;->currentProtocol:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, v4, :cond_0

    .line 337
    sget-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->UNABLE_START_VIDEO_P2P:Lru/cn/domain/statistics/inetra/ErrorCode;

    goto/16 :goto_1

    .line 315
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5 -> :sswitch_2
        0x69 -> :sswitch_1
    .end sparse-switch
.end method

.method public static pushOpen(J)V
    .locals 4
    .param p0, "pdid"    # J

    .prologue
    .line 369
    const-string v1, "push/open"

    invoke-static {v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->createEvent(Ljava/lang/String;)Lru/cn/domain/statistics/inetra/TrackingEvent;

    move-result-object v0

    .line 370
    .local v0, "event":Lru/cn/domain/statistics/inetra/TrackingEvent;
    const-string v1, "_pdid"

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    invoke-static {v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->sendEvent(Lru/cn/domain/statistics/inetra/TrackingEvent;)V

    .line 372
    return-void
.end method

.method public static pushView(J)V
    .locals 4
    .param p0, "pdid"    # J

    .prologue
    .line 375
    const-string v1, "push/view"

    invoke-static {v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->createEvent(Ljava/lang/String;)Lru/cn/domain/statistics/inetra/TrackingEvent;

    move-result-object v0

    .line 376
    .local v0, "event":Lru/cn/domain/statistics/inetra/TrackingEvent;
    const-string v1, "_pdid"

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    invoke-static {v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->sendEvent(Lru/cn/domain/statistics/inetra/TrackingEvent;)V

    .line 378
    return-void
.end method

.method public static qualityChanged(I)V
    .locals 1
    .param p0, "bitrate"    # I

    .prologue
    .line 114
    const/16 v0, 0x514

    invoke-static {v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->watch(I)V

    .line 116
    if-lez p0, :cond_0

    .line 117
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iput p0, v0, Lru/cn/domain/statistics/inetra/ViewSession;->bitrate:I

    .line 120
    :cond_0
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    invoke-virtual {v0}, Lru/cn/domain/statistics/inetra/ViewSession;->startCounting()V

    .line 121
    return-void
.end method

.method public static restoreSessionParams(Landroid/os/Bundle;)V
    .locals 2
    .param p0, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 392
    const-string v0, "tracking_view_from"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->currentViewFrom:Ljava/lang/Integer;

    .line 393
    const-string v0, "tracking_start_mode"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->currentStartMode:Ljava/lang/Integer;

    .line 394
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->currentViewFrom:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 395
    const-string v0, "tracking_collection_id"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    sput-wide v0, Lru/cn/domain/statistics/inetra/InetraTracker;->currentCollectionId:J

    .line 397
    :cond_0
    return-void
.end method

.method public static saveSessionParams(Landroid/os/Bundle;)V
    .locals 4
    .param p0, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 401
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->currentViewFrom:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->currentStartMode:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 409
    :cond_0
    :goto_0
    return-void

    .line 404
    :cond_1
    const-string v0, "tracking_view_from"

    sget-object v1, Lru/cn/domain/statistics/inetra/InetraTracker;->currentViewFrom:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 405
    const-string v0, "tracking_start_mode"

    sget-object v1, Lru/cn/domain/statistics/inetra/InetraTracker;->currentStartMode:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 406
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->currentViewFrom:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 407
    const-string v0, "tracking_collection_id"

    sget-wide v2, Lru/cn/domain/statistics/inetra/InetraTracker;->currentCollectionId:J

    invoke-virtual {p0, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_0
.end method

.method private static sendEvent(Lru/cn/domain/statistics/inetra/TrackingEvent;)V
    .locals 1
    .param p0, "event"    # Lru/cn/domain/statistics/inetra/TrackingEvent;

    .prologue
    .line 609
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->INSTANCE:Lru/cn/domain/statistics/inetra/InetraTracker;

    iget-object v0, v0, Lru/cn/domain/statistics/inetra/InetraTracker;->sender:Lru/cn/domain/statistics/inetra/TrackingSender;

    invoke-virtual {v0, p0}, Lru/cn/domain/statistics/inetra/TrackingSender;->sendEvent(Lru/cn/domain/statistics/inetra/TrackingEvent;)V

    .line 610
    return-void
.end method

.method static sessionStart()V
    .locals 2

    .prologue
    .line 554
    const-string v1, "session/start"

    invoke-static {v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->createEvent(Ljava/lang/String;)Lru/cn/domain/statistics/inetra/TrackingEvent;

    move-result-object v0

    .line 555
    .local v0, "event":Lru/cn/domain/statistics/inetra/TrackingEvent;
    invoke-static {v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->sendEvent(Lru/cn/domain/statistics/inetra/TrackingEvent;)V

    .line 556
    return-void
.end method

.method public static setAppMode(I)V
    .locals 1
    .param p0, "appMode"    # I

    .prologue
    .line 94
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->currentAppMode:Ljava/lang/Integer;

    .line 95
    return-void
.end method

.method public static setCurrentViewScreen(Ljava/lang/String;)V
    .locals 0
    .param p0, "viewScreen"    # Ljava/lang/String;

    .prologue
    .line 426
    sput-object p0, Lru/cn/domain/statistics/inetra/InetraTracker;->currentViewScreen:Ljava/lang/String;

    .line 428
    return-void
.end method

.method public static setSessionParams(II)V
    .locals 2
    .param p0, "viewFrom"    # I
    .param p1, "startMode"    # I

    .prologue
    .line 412
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->currentViewFrom:Ljava/lang/Integer;

    .line 413
    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    .line 414
    const-wide/16 v0, 0x0

    sput-wide v0, Lru/cn/domain/statistics/inetra/InetraTracker;->currentCollectionId:J

    .line 416
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->currentStartMode:Ljava/lang/Integer;

    .line 420
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    if-eqz v0, :cond_1

    .line 421
    sget-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lru/cn/domain/statistics/inetra/ViewSession;->startMode:Ljava/lang/Integer;

    .line 423
    :cond_1
    return-void
.end method

.method public static setSessionParams(IIJ)V
    .locals 0
    .param p0, "viewFrom"    # I
    .param p1, "startMode"    # I
    .param p2, "collectionId"    # J

    .prologue
    .line 387
    invoke-static {p0, p1}, Lru/cn/domain/statistics/inetra/InetraTracker;->setSessionParams(II)V

    .line 388
    sput-wide p2, Lru/cn/domain/statistics/inetra/InetraTracker;->currentCollectionId:J

    .line 389
    return-void
.end method

.method public static singletonize(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 90
    new-instance v0, Lru/cn/domain/statistics/inetra/InetraTracker;

    invoke-direct {v0, p0}, Lru/cn/domain/statistics/inetra/InetraTracker;-><init>(Landroid/content/Context;)V

    sput-object v0, Lru/cn/domain/statistics/inetra/InetraTracker;->INSTANCE:Lru/cn/domain/statistics/inetra/InetraTracker;

    .line 91
    return-void
.end method

.method public static startoverUsed(J)V
    .locals 4
    .param p0, "telecastId"    # J

    .prologue
    .line 381
    const-string v1, "startover/use"

    invoke-static {v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->createEvent(Ljava/lang/String;)Lru/cn/domain/statistics/inetra/TrackingEvent;

    move-result-object v0

    .line 382
    .local v0, "event":Lru/cn/domain/statistics/inetra/TrackingEvent;
    const-string v1, "_telecast_id"

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    invoke-static {v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->sendEvent(Lru/cn/domain/statistics/inetra/TrackingEvent;)V

    .line 384
    return-void
.end method

.method public static telecastStart(Landroid/content/Context;JI)V
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "telecastId"    # J
    .param p3, "player"    # I

    .prologue
    .line 163
    sget-object v9, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    if-eqz v9, :cond_0

    .line 164
    sget-object v9, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    invoke-virtual {v9}, Lru/cn/domain/statistics/inetra/ViewSession;->pauseCounting()V

    .line 165
    sget-object v9, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    invoke-virtual {v9}, Lru/cn/domain/statistics/inetra/ViewSession;->getWatchedTime()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-lez v9, :cond_0

    .line 166
    const/16 v9, 0x3e8

    invoke-static {v9}, Lru/cn/domain/statistics/inetra/InetraTracker;->watch(I)V

    .line 168
    new-instance v9, Ljava/lang/IllegalStateException;

    const-string v10, "Try to telecast/start before sending previous watch activity"

    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v9}, Lru/cn/utils/Logger;->logException(Ljava/lang/Throwable;)V

    .line 172
    :cond_0
    sget-object v9, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    invoke-virtual {v9}, Lru/cn/domain/statistics/inetra/ViewSession;->startCounting()V

    .line 173
    sget-object v9, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    move/from16 v0, p3

    iput v0, v9, Lru/cn/domain/statistics/inetra/ViewSession;->playerType:I

    .line 175
    sget-object v9, Lru/cn/domain/statistics/inetra/InetraTracker;->currentTelecastId:Ljava/lang/Long;

    if-eqz v9, :cond_1

    sget-object v9, Lru/cn/domain/statistics/inetra/InetraTracker;->currentTelecastId:Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v9, p1, v10

    if-eqz v9, :cond_6

    .line 176
    :cond_1
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    sput-object v9, Lru/cn/domain/statistics/inetra/InetraTracker;->currentTelecastId:Ljava/lang/Long;

    .line 177
    const/4 v9, 0x0

    sput-object v9, Lru/cn/domain/statistics/inetra/InetraTracker;->currentChannelId:Ljava/lang/Long;

    .line 178
    const/4 v9, 0x0

    sput-object v9, Lru/cn/domain/statistics/inetra/InetraTracker;->currentChannelTitle:Ljava/lang/String;

    .line 179
    sget-object v9, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget-object v9, v9, Lru/cn/domain/statistics/inetra/ViewSession;->viewFrom:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 180
    .local v6, "viewFrom":I
    sget-object v9, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget-object v9, v9, Lru/cn/domain/statistics/inetra/ViewSession;->startMode:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 181
    .local v5, "startMode":I
    sget-object v8, Lru/cn/domain/statistics/inetra/InetraTracker;->currentViewScreen:Ljava/lang/String;

    .line 182
    .local v8, "viewScreen":Ljava/lang/String;
    sget-object v9, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget v7, v9, Lru/cn/domain/statistics/inetra/ViewSession;->viewMode:I

    .line 183
    .local v7, "viewMode":I
    sget-object v9, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget-wide v2, v9, Lru/cn/domain/statistics/inetra/ViewSession;->collectionId:J

    .line 185
    .local v2, "collectionId":J
    const-string v9, "telecast/start"

    invoke-static {v9}, Lru/cn/domain/statistics/inetra/InetraTracker;->createEvent(Ljava/lang/String;)Lru/cn/domain/statistics/inetra/TrackingEvent;

    move-result-object v4

    .line 186
    .local v4, "event":Lru/cn/domain/statistics/inetra/TrackingEvent;
    const-string v9, "_telecast_id"

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    const-string v9, "_view_mode"

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const-string v9, "_view_from"

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    const-string v9, "_start_mode"

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const-string v9, "_view_screen"

    invoke-virtual {v4, v9, v8}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    const-string v9, "_player"

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    sget-object v9, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget-wide v10, v9, Lru/cn/domain/statistics/inetra/ViewSession;->contractorId:J

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-lez v9, :cond_2

    .line 194
    const-string v9, "_contractor_id"

    sget-object v10, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget-wide v10, v10, Lru/cn/domain/statistics/inetra/ViewSession;->contractorId:J

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    :cond_2
    sget-object v9, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget-object v9, v9, Lru/cn/domain/statistics/inetra/ViewSession;->sourceUri:Ljava/lang/String;

    if-eqz v9, :cond_3

    .line 198
    const-string v9, "_url"

    sget-object v10, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget-object v10, v10, Lru/cn/domain/statistics/inetra/ViewSession;->sourceUri:Ljava/lang/String;

    invoke-virtual {v4, v9, v10}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    :cond_3
    const-string v9, "_content_url"

    sget-object v10, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget-object v10, v10, Lru/cn/domain/statistics/inetra/ViewSession;->contentUri:Ljava/lang/String;

    invoke-virtual {v4, v9, v10}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    const-wide/16 v10, 0x0

    cmp-long v9, v2, v10

    if-lez v9, :cond_4

    .line 204
    const-string v9, "_collection_id"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    :cond_4
    sget-object v9, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget-object v9, v9, Lru/cn/domain/statistics/inetra/ViewSession;->locationTitle:Ljava/lang/String;

    if-eqz v9, :cond_5

    .line 208
    const-string v9, "_channel"

    sget-object v10, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    iget-object v10, v10, Lru/cn/domain/statistics/inetra/ViewSession;->locationTitle:Ljava/lang/String;

    invoke-virtual {v4, v9, v10}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    :cond_5
    invoke-static {v4}, Lru/cn/domain/statistics/inetra/InetraTracker;->sendEvent(Lru/cn/domain/statistics/inetra/TrackingEvent;)V

    .line 213
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v9

    if-nez v9, :cond_6

    const/4 v9, 0x1

    if-eq v5, v9, :cond_6

    .line 214
    invoke-static {p0}, Lru/cn/domain/statistics/AdCounter;->Logging(Landroid/content/Context;)V

    .line 217
    .end local v2    # "collectionId":J
    .end local v4    # "event":Lru/cn/domain/statistics/inetra/TrackingEvent;
    .end local v5    # "startMode":I
    .end local v6    # "viewFrom":I
    .end local v7    # "viewMode":I
    .end local v8    # "viewScreen":Ljava/lang/String;
    :cond_6
    return-void
.end method

.method public static timeMeasure(Lru/cn/domain/statistics/inetra/TypeOfMeasure;Lru/cn/domain/statistics/inetra/TimeMeasure;)V
    .locals 6
    .param p0, "_type"    # Lru/cn/domain/statistics/inetra/TypeOfMeasure;
    .param p1, "measure"    # Lru/cn/domain/statistics/inetra/TimeMeasure;

    .prologue
    .line 536
    invoke-virtual {p1}, Lru/cn/domain/statistics/inetra/TimeMeasure;->getTotalTime()J

    move-result-wide v2

    .line 537
    .local v2, "_time":J
    invoke-virtual {p1, p0}, Lru/cn/domain/statistics/inetra/TimeMeasure;->getMessage(Lru/cn/domain/statistics/inetra/TypeOfMeasure;)Ljava/lang/String;

    move-result-object v0

    .line 539
    .local v0, "_message":Ljava/lang/String;
    const-string v4, "time/measure"

    invoke-static {v4}, Lru/cn/domain/statistics/inetra/InetraTracker;->createEvent(Ljava/lang/String;)Lru/cn/domain/statistics/inetra/TrackingEvent;

    move-result-object v1

    .line 540
    .local v1, "event":Lru/cn/domain/statistics/inetra/TrackingEvent;
    const-string v4, "_type"

    invoke-virtual {p0}, Lru/cn/domain/statistics/inetra/TypeOfMeasure;->getValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-ltz v4, :cond_0

    .line 543
    const-string v4, "_time"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    :cond_0
    if-eqz v0, :cond_1

    .line 547
    const-string v4, "_message"

    invoke-virtual {v1, v4, v0}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    :cond_1
    invoke-static {v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->sendEvent(Lru/cn/domain/statistics/inetra/TrackingEvent;)V

    .line 551
    return-void
.end method

.method public static tracking(Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .param p0, "domain"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 455
    .local p1, "extras":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "tracking"

    invoke-static {v2}, Lru/cn/domain/statistics/inetra/InetraTracker;->createEvent(Ljava/lang/String;)Lru/cn/domain/statistics/inetra/TrackingEvent;

    move-result-object v0

    .line 456
    .local v0, "event":Lru/cn/domain/statistics/inetra/TrackingEvent;
    const-string v2, "_domain"

    invoke-virtual {v0, v2, p0}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    if-eqz p1, :cond_0

    .line 459
    invoke-static {p1}, Lcom/annimon/stream/Stream;->of(Ljava/util/Map;)Lcom/annimon/stream/Stream;

    move-result-object v2

    sget-object v3, Lru/cn/domain/statistics/inetra/InetraTracker$$Lambda$0;->$instance:Lcom/annimon/stream/function/Function;

    .line 460
    invoke-virtual {v2, v3}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v2

    const-string v3, ";"

    .line 461
    invoke-static {v3}, Lcom/annimon/stream/Collectors;->joining(Ljava/lang/CharSequence;)Lcom/annimon/stream/Collector;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/annimon/stream/Stream;->collect(Lcom/annimon/stream/Collector;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 463
    .local v1, "message":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 464
    const-string v2, "_message"

    invoke-virtual {v0, v2, v1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    .end local v1    # "message":Ljava/lang/String;
    :cond_0
    invoke-static {v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->sendEvent(Lru/cn/domain/statistics/inetra/TrackingEvent;)V

    .line 469
    return-void
.end method

.method public static watch(I)V
    .locals 28
    .param p0, "mode"    # I

    .prologue
    .line 221
    sget-object v24, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    invoke-virtual/range {v24 .. v24}, Lru/cn/domain/statistics/inetra/ViewSession;->pauseCounting()V

    .line 222
    sget-object v24, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    invoke-virtual/range {v24 .. v24}, Lru/cn/domain/statistics/inetra/ViewSession;->getWatchedTime()J

    move-result-wide v18

    .line 224
    .local v18, "time":J
    sget-object v24, Lru/cn/domain/statistics/inetra/InetraTracker;->bufferingCounter:Lru/cn/domain/statistics/inetra/BufferingCounter;

    move-object/from16 v0, v24

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Lru/cn/domain/statistics/inetra/BufferingCounter;->addWatchedTime(J)V

    .line 225
    const/16 v24, 0x44c

    move/from16 v0, p0

    move/from16 v1, v24

    if-eq v0, v1, :cond_0

    .line 226
    invoke-static/range {p0 .. p0}, Lru/cn/domain/statistics/inetra/InetraTracker;->bufferingError(I)V

    .line 227
    sget-object v24, Lru/cn/domain/statistics/inetra/InetraTracker;->bufferingCounter:Lru/cn/domain/statistics/inetra/BufferingCounter;

    invoke-virtual/range {v24 .. v24}, Lru/cn/domain/statistics/inetra/BufferingCounter;->reset()V

    .line 230
    :cond_0
    const-wide/16 v24, 0x0

    cmp-long v24, v18, v24

    if-nez v24, :cond_1

    .line 310
    :goto_0
    return-void

    .line 233
    :cond_1
    sget-object v24, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    invoke-virtual/range {v24 .. v24}, Lru/cn/domain/statistics/inetra/ViewSession;->reset()V

    .line 235
    sget-object v24, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    move-object/from16 v0, v24

    iget v14, v0, Lru/cn/domain/statistics/inetra/ViewSession;->playerType:I

    .line 236
    .local v14, "player":I
    sget-object v24, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    move-object/from16 v0, v24

    iget-wide v10, v0, Lru/cn/domain/statistics/inetra/ViewSession;->contractorId:J

    .line 237
    .local v10, "contractorId":J
    sget-object v24, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    move-object/from16 v0, v24

    iget-object v15, v0, Lru/cn/domain/statistics/inetra/ViewSession;->sourceUri:Ljava/lang/String;

    .line 238
    .local v15, "sourceUri":Ljava/lang/String;
    sget-object v24, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    move-object/from16 v0, v24

    iget-object v8, v0, Lru/cn/domain/statistics/inetra/ViewSession;->contentUri:Ljava/lang/String;

    .line 239
    .local v8, "contentUri":Ljava/lang/String;
    sget-object v24, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    move-object/from16 v0, v24

    iget-object v9, v0, Lru/cn/domain/statistics/inetra/ViewSession;->currentProtocol:Ljava/lang/Integer;

    .line 240
    .local v9, "currentProtocol":Ljava/lang/Integer;
    sget-object v23, Lru/cn/domain/statistics/inetra/InetraTracker;->currentViewScreen:Ljava/lang/String;

    .line 243
    .local v23, "viewScreen":Ljava/lang/String;
    rem-int/lit8 v12, p0, 0xa

    .line 245
    .local v12, "endMode":I
    sget-object v24, Lru/cn/domain/statistics/inetra/InetraTracker;->currentTelecastId:Ljava/lang/Long;

    if-eqz v24, :cond_7

    .line 246
    sget-object v24, Lru/cn/domain/statistics/inetra/InetraTracker;->currentTelecastId:Ljava/lang/Long;

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 247
    .local v16, "telecastId":J
    sget-object v24, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    move-object/from16 v0, v24

    iget v0, v0, Lru/cn/domain/statistics/inetra/ViewSession;->viewMode:I

    move/from16 v22, v0

    .line 248
    .local v22, "viewMode":I
    sget-object v24, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    move-object/from16 v0, v24

    iget-object v0, v0, Lru/cn/domain/statistics/inetra/ViewSession;->viewFrom:Ljava/lang/Integer;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v21

    .line 249
    .local v21, "viewFrom":I
    sget-object v24, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    move-object/from16 v0, v24

    iget-wide v6, v0, Lru/cn/domain/statistics/inetra/ViewSession;->collectionId:J

    .line 251
    .local v6, "collectionId":J
    const-string v24, "telecast/watch"

    invoke-static/range {v24 .. v24}, Lru/cn/domain/statistics/inetra/InetraTracker;->createEvent(Ljava/lang/String;)Lru/cn/domain/statistics/inetra/TrackingEvent;

    move-result-object v13

    .line 252
    .local v13, "event":Lru/cn/domain/statistics/inetra/TrackingEvent;
    const-string v24, "_telecast_id"

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v13, v0, v1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    const-string v24, "_time"

    invoke-static/range {v18 .. v19}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v13, v0, v1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    const-string v24, "_view_mode"

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v13, v0, v1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const-string v24, "_view_from"

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v13, v0, v1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const-string v24, "_end_mode"

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v13, v0, v1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    const-string v24, "_view_screen"

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v13, v0, v1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const-string v24, "_player"

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v13, v0, v1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    const-wide/16 v24, 0x0

    cmp-long v24, v10, v24

    if-lez v24, :cond_2

    .line 261
    const-string v24, "_contractor_id"

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v13, v0, v1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    :cond_2
    if-eqz v15, :cond_3

    .line 265
    const-string v24, "_url"

    sget-object v25, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    move-object/from16 v0, v25

    iget-object v0, v0, Lru/cn/domain/statistics/inetra/ViewSession;->sourceUri:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v13, v0, v1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    :cond_3
    const-string v24, "_content_url"

    move-object/from16 v0, v24

    invoke-virtual {v13, v0, v8}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    if-eqz v9, :cond_4

    .line 271
    const-string v24, "_protocol"

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v13, v0, v1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    :cond_4
    const-wide/16 v24, 0x0

    cmp-long v24, v6, v24

    if-lez v24, :cond_5

    .line 274
    const-string v24, "_collection_id"

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v13, v0, v1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    :cond_5
    sget-object v24, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    move-object/from16 v0, v24

    iget-object v0, v0, Lru/cn/domain/statistics/inetra/ViewSession;->locationTitle:Ljava/lang/String;

    move-object/from16 v24, v0

    if-eqz v24, :cond_6

    .line 278
    const-string v24, "_channel"

    sget-object v25, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    move-object/from16 v0, v25

    iget-object v0, v0, Lru/cn/domain/statistics/inetra/ViewSession;->locationTitle:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v13, v0, v1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    :cond_6
    invoke-static {v13}, Lru/cn/domain/statistics/inetra/InetraTracker;->sendEvent(Lru/cn/domain/statistics/inetra/TrackingEvent;)V

    goto/16 :goto_0

    .line 283
    .end local v6    # "collectionId":J
    .end local v13    # "event":Lru/cn/domain/statistics/inetra/TrackingEvent;
    .end local v16    # "telecastId":J
    .end local v21    # "viewFrom":I
    .end local v22    # "viewMode":I
    :cond_7
    sget-object v24, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    move-object/from16 v0, v24

    iget-object v0, v0, Lru/cn/domain/statistics/inetra/ViewSession;->locationTitle:Ljava/lang/String;

    move-object/from16 v24, v0

    if-eqz v24, :cond_b

    sget-object v24, Lru/cn/domain/statistics/inetra/InetraTracker;->viewSession:Lru/cn/domain/statistics/inetra/ViewSession;

    move-object/from16 v0, v24

    iget-object v0, v0, Lru/cn/domain/statistics/inetra/ViewSession;->locationTitle:Ljava/lang/String;

    move-object/from16 v20, v0

    .line 285
    .local v20, "title":Ljava/lang/String;
    :goto_1
    sget-object v24, Lru/cn/domain/statistics/inetra/InetraTracker;->currentChannelId:Ljava/lang/Long;

    if-eqz v24, :cond_c

    sget-object v24, Lru/cn/domain/statistics/inetra/InetraTracker;->currentChannelId:Ljava/lang/Long;

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Long;->longValue()J

    move-result-wide v24

    const-wide/16 v26, 0x0

    cmp-long v24, v24, v26

    if-lez v24, :cond_c

    sget-object v24, Lru/cn/domain/statistics/inetra/InetraTracker;->currentChannelId:Ljava/lang/Long;

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 287
    .local v4, "channelId":J
    :goto_2
    const-string v24, "channel/watch"

    invoke-static/range {v24 .. v24}, Lru/cn/domain/statistics/inetra/InetraTracker;->createEvent(Ljava/lang/String;)Lru/cn/domain/statistics/inetra/TrackingEvent;

    move-result-object v13

    .line 288
    .restart local v13    # "event":Lru/cn/domain/statistics/inetra/TrackingEvent;
    const-string v24, "_channel"

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v13, v0, v1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    const-wide/16 v24, 0x0

    cmp-long v24, v10, v24

    if-lez v24, :cond_8

    .line 291
    const-string v24, "_contractor_id"

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v13, v0, v1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    :cond_8
    const-string v24, "_view_screen"

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v13, v0, v1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    const-string v24, "_player"

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v13, v0, v1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    const-string v24, "_url"

    move-object/from16 v0, v24

    invoke-virtual {v13, v0, v15}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    const-string v24, "_content_url"

    move-object/from16 v0, v24

    invoke-virtual {v13, v0, v8}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    const-wide/16 v24, 0x0

    cmp-long v24, v4, v24

    if-lez v24, :cond_9

    .line 300
    const-string v24, "_channel_id"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v13, v0, v1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    :cond_9
    if-eqz v9, :cond_a

    .line 304
    const-string v24, "_protocol"

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v13, v0, v1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    :cond_a
    const-string v24, "_time"

    invoke-static/range {v18 .. v19}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v13, v0, v1}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    invoke-static {v13}, Lru/cn/domain/statistics/inetra/InetraTracker;->sendEvent(Lru/cn/domain/statistics/inetra/TrackingEvent;)V

    goto/16 :goto_0

    .line 283
    .end local v4    # "channelId":J
    .end local v13    # "event":Lru/cn/domain/statistics/inetra/TrackingEvent;
    .end local v20    # "title":Ljava/lang/String;
    :cond_b
    sget-object v20, Lru/cn/domain/statistics/inetra/InetraTracker;->currentChannelTitle:Ljava/lang/String;

    goto/16 :goto_1

    .line 285
    .restart local v20    # "title":Ljava/lang/String;
    :cond_c
    const-wide/16 v4, 0x0

    goto :goto_2
.end method


# virtual methods
.method public onSessionChange(Ljava/lang/String;)V
    .locals 0
    .param p1, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 86
    invoke-static {}, Lru/cn/domain/statistics/inetra/InetraTracker;->sessionStart()V

    .line 87
    return-void
.end method
