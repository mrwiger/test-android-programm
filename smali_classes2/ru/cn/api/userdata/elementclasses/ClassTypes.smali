.class public final Lru/cn/api/userdata/elementclasses/ClassTypes;
.super Ljava/lang/Object;
.source "ClassTypes.java"


# instance fields
.field private final types:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Type;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lru/cn/api/userdata/elementclasses/ClassTypes;->types:Ljava/util/Map;

    .line 15
    iget-object v0, p0, Lru/cn/api/userdata/elementclasses/ClassTypes;->types:Ljava/util/Map;

    const-string v1, "application"

    const-class v2, Lru/cn/api/userdata/elementclasses/ApplicationClass;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    iget-object v0, p0, Lru/cn/api/userdata/elementclasses/ClassTypes;->types:Ljava/util/Map;

    const-string v1, "bookmark"

    const-class v2, Lru/cn/api/userdata/elementclasses/BookmarkClass;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    iget-object v0, p0, Lru/cn/api/userdata/elementclasses/ClassTypes;->types:Ljava/util/Map;

    const-string v1, "catalogue_item"

    const-class v2, Lru/cn/api/userdata/elementclasses/CatalogueItemClass;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    iget-object v0, p0, Lru/cn/api/userdata/elementclasses/ClassTypes;->types:Ljava/util/Map;

    const-string v1, "device"

    const-class v2, Lru/cn/api/userdata/elementclasses/DeviceClass;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    iget-object v0, p0, Lru/cn/api/userdata/elementclasses/ClassTypes;->types:Ljava/util/Map;

    const-string v1, "purchase"

    const-class v2, Lru/cn/api/userdata/elementclasses/PurchaseClass;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    return-void
.end method


# virtual methods
.method public classType(Ljava/lang/String;)Ljava/lang/reflect/Type;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    iget-object v0, p0, Lru/cn/api/userdata/elementclasses/ClassTypes;->types:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    return-object v0
.end method
