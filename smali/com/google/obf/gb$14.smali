.class final Lcom/google/obf/gb$14;
.super Lcom/google/obf/ew;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/gb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/obf/ew",
        "<",
        "Ljava/net/URL;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/obf/ew;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/obf/ge;)Ljava/net/URL;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p1}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    move-result-object v1

    sget-object v2, Lcom/google/obf/gf;->i:Lcom/google/obf/gf;

    if-ne v1, v2, :cond_1

    .line 3
    invoke-virtual {p1}, Lcom/google/obf/ge;->j()V

    .line 6
    :cond_0
    :goto_0
    return-object v0

    .line 5
    :cond_1
    invoke-virtual {p1}, Lcom/google/obf/ge;->h()Ljava/lang/String;

    move-result-object v1

    .line 6
    const-string v2, "null"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/google/obf/gg;Ljava/net/URL;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7
    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/obf/gg;->b(Ljava/lang/String;)Lcom/google/obf/gg;

    .line 8
    return-void

    .line 7
    :cond_0
    invoke-virtual {p2}, Ljava/net/URL;->toExternalForm()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public synthetic read(Lcom/google/obf/ge;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/google/obf/gb$14;->a(Lcom/google/obf/ge;)Ljava/net/URL;

    move-result-object v0

    return-object v0
.end method

.method public synthetic write(Lcom/google/obf/gg;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 10
    check-cast p2, Ljava/net/URL;

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/gb$14;->a(Lcom/google/obf/gg;Ljava/net/URL;)V

    return-void
.end method
