.class public final Lcom/my/target/cs;
.super Lcom/my/target/ak;
.source "NativeAdSection.java"


# static fields
.field private static final O:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final P:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/core/models/banners/a;",
            ">;"
        }
    .end annotation
.end field

.field private final Q:Lcom/my/target/am;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/my/target/cs;->O:Landroid/util/LruCache;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/my/target/ak;-><init>()V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/cs;->P:Ljava/util/ArrayList;

    .line 35
    invoke-static {}, Lcom/my/target/am;->W()Lcom/my/target/am;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/cs;->Q:Lcom/my/target/am;

    .line 40
    return-void
.end method

.method public static u()Landroid/util/LruCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    sget-object v0, Lcom/my/target/cs;->O:Landroid/util/LruCache;

    return-object v0
.end method

.method public static v()Lcom/my/target/cs;
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/my/target/cs;

    invoke-direct {v0}, Lcom/my/target/cs;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final R()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/my/target/cs;->P:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final a(Lcom/my/target/core/models/banners/a;)V
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Lcom/my/target/cs;->P:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    sget-object v0, Lcom/my/target/cs;->O:Landroid/util/LruCache;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/a;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    return-void
.end method

.method public final getBannersCount()I
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/my/target/cs;->P:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getVideoSettings()Lcom/my/target/am;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/my/target/cs;->Q:Lcom/my/target/am;

    return-object v0
.end method

.method public final w()Lcom/my/target/core/models/banners/a;
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/my/target/cs;->P:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/my/target/cs;->P:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/a;

    .line 48
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
