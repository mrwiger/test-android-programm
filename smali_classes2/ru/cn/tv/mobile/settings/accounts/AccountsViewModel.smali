.class Lru/cn/tv/mobile/settings/accounts/AccountsViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "AccountsViewModel.java"


# instance fields
.field private final contractors:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final loader:Lru/cn/mvvm/RxLoader;


# direct methods
.method constructor <init>(Lru/cn/mvvm/RxLoader;)V
    .locals 3
    .param p1, "loader"    # Lru/cn/mvvm/RxLoader;

    .prologue
    .line 24
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 25
    iput-object p1, p0, Lru/cn/tv/mobile/settings/accounts/AccountsViewModel;->loader:Lru/cn/mvvm/RxLoader;

    .line 27
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/mobile/settings/accounts/AccountsViewModel;->contractors:Landroid/arch/lifecycle/MutableLiveData;

    .line 29
    invoke-direct {p0}, Lru/cn/tv/mobile/settings/accounts/AccountsViewModel;->contentProviders()Lio/reactivex/Observable;

    move-result-object v0

    .line 30
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lru/cn/tv/mobile/settings/accounts/AccountsViewModel$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/settings/accounts/AccountsViewModel$$Lambda$0;-><init>(Lru/cn/tv/mobile/settings/accounts/AccountsViewModel;)V

    new-instance v2, Lru/cn/tv/mobile/settings/accounts/AccountsViewModel$$Lambda$1;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/settings/accounts/AccountsViewModel$$Lambda$1;-><init>(Lru/cn/tv/mobile/settings/accounts/AccountsViewModel;)V

    .line 31
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 29
    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/settings/accounts/AccountsViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 33
    return-void
.end method

.method private contentProviders()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->contentProviders()Landroid/net/Uri;

    move-result-object v0

    .line 42
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/mobile/settings/accounts/AccountsViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v1

    .line 43
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 42
    return-object v1
.end method


# virtual methods
.method public contractors()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lru/cn/tv/mobile/settings/accounts/AccountsViewModel;->contractors:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method final synthetic lambda$new$0$AccountsViewModel(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "it"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lru/cn/tv/mobile/settings/accounts/AccountsViewModel;->contractors:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v0, p1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method final synthetic lambda$new$1$AccountsViewModel(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "throwable"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lru/cn/tv/mobile/settings/accounts/AccountsViewModel;->contractors:Landroid/arch/lifecycle/MutableLiveData;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method
