.class public Lru/cn/ad/video/VAST/VastTracker;
.super Ljava/lang/Object;
.source "VastTracker.java"

# interfaces
.implements Lru/cn/ad/video/ui/VastPresenter$Tracker;


# instance fields
.field private final parsers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/ad/video/VAST/parser/VastParser;",
            ">;"
        }
    .end annotation
.end field

.field private final userAgent:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .param p2, "userAgent"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/ad/video/VAST/parser/VastParser;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 22
    .local p1, "parsers":Ljava/util/List;, "Ljava/util/List<Lru/cn/ad/video/VAST/parser/VastParser;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lru/cn/ad/video/VAST/VastTracker;->parsers:Ljava/util/List;

    .line 25
    iput-object p2, p0, Lru/cn/ad/video/VAST/VastTracker;->userAgent:Ljava/lang/String;

    .line 26
    return-void
.end method


# virtual methods
.method public trackClick(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    iget-object v3, p0, Lru/cn/ad/video/VAST/VastTracker;->parsers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/ad/video/VAST/parser/VastParser;

    .line 62
    .local v2, "vasObj":Lru/cn/ad/video/VAST/parser/VastParser;
    invoke-virtual {v2}, Lru/cn/ad/video/VAST/parser/VastParser;->getClickTrackingUrls()Ljava/util/List;

    move-result-object v0

    .line 63
    .local v0, "clickTrackingList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 64
    .local v1, "clickTrackingUrl":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_1

    .line 65
    const-string v5, "VastTracker"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Track clicking url "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    iget-object v5, p0, Lru/cn/ad/video/VAST/VastTracker;->userAgent:Ljava/lang/String;

    invoke-static {v1, v5}, Lru/cn/utils/http/HttpClient;->sendRequestAsync(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 71
    .end local v0    # "clickTrackingList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "clickTrackingUrl":Ljava/lang/String;
    .end local v2    # "vasObj":Lru/cn/ad/video/VAST/parser/VastParser;
    :cond_2
    return-void
.end method

.method public trackError(Landroid/content/Context;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "errorCode"    # I

    .prologue
    .line 75
    iget-object v3, p0, Lru/cn/ad/video/VAST/VastTracker;->parsers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/ad/video/VAST/parser/VastParser;

    .line 77
    .local v1, "vasObj":Lru/cn/ad/video/VAST/parser/VastParser;
    packed-switch p2, :pswitch_data_0

    .line 91
    invoke-virtual {v1}, Lru/cn/ad/video/VAST/parser/VastParser;->getErrorUri()Ljava/lang/String;

    move-result-object v0

    .line 92
    .local v0, "errorUri":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 93
    invoke-static {v0, p2}, Lru/cn/ad/video/VAST/VASTUtils;->resolveErrorUri(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 94
    invoke-virtual {v1}, Lru/cn/ad/video/VAST/parser/VastParser;->getBaseUri()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Lru/cn/ad/video/util/UriUtil;->resolve(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 96
    const-string v4, "VastTracker"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Track error "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " by URL "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget-object v4, p0, Lru/cn/ad/video/VAST/VastTracker;->userAgent:Ljava/lang/String;

    invoke-static {v0, v4}, Lru/cn/utils/http/HttpClient;->sendRequestAsync(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 79
    .end local v0    # "errorUri":Ljava/lang/String;
    :pswitch_0
    invoke-virtual {v1}, Lru/cn/ad/video/VAST/parser/VastParser;->getVastErrorUri()Ljava/lang/String;

    move-result-object v2

    .line 80
    .local v2, "vastErrorUri":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 81
    invoke-static {v2, p2}, Lru/cn/ad/video/VAST/VASTUtils;->resolveErrorUri(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 82
    invoke-virtual {v1}, Lru/cn/ad/video/VAST/parser/VastParser;->getBaseUri()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v2}, Lru/cn/ad/video/util/UriUtil;->resolve(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 84
    const-string v4, "VastTracker"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Track VAST error "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " by URL "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    iget-object v4, p0, Lru/cn/ad/video/VAST/VastTracker;->userAgent:Ljava/lang/String;

    invoke-static {v2, v4}, Lru/cn/utils/http/HttpClient;->sendRequestAsync(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 104
    .end local v1    # "vasObj":Lru/cn/ad/video/VAST/parser/VastParser;
    .end local v2    # "vastErrorUri":Ljava/lang/String;
    :cond_1
    return-void

    .line 77
    nop

    :pswitch_data_0
    .packed-switch 0x12f
        :pswitch_0
    .end packed-switch
.end method

.method public trackEvent(Landroid/content/Context;Ljava/lang/String;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "eventName"    # Ljava/lang/String;

    .prologue
    .line 44
    iget-object v3, p0, Lru/cn/ad/video/VAST/VastTracker;->parsers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/ad/video/VAST/parser/VastParser;

    .line 45
    .local v2, "vasObj":Lru/cn/ad/video/VAST/parser/VastParser;
    invoke-virtual {v2, p2}, Lru/cn/ad/video/VAST/parser/VastParser;->getEventTrackingUrls(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 46
    .local v0, "eventTrackingList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .line 49
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 50
    .local v1, "eventTrackingUrl":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_1

    .line 51
    const-string v5, "VastTracker"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Track event \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\' by url "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    iget-object v5, p0, Lru/cn/ad/video/VAST/VastTracker;->userAgent:Ljava/lang/String;

    invoke-static {v1, v5}, Lru/cn/utils/http/HttpClient;->sendRequestAsync(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 57
    .end local v0    # "eventTrackingList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "eventTrackingUrl":Ljava/lang/String;
    .end local v2    # "vasObj":Lru/cn/ad/video/VAST/parser/VastParser;
    :cond_2
    return-void
.end method

.method public trackImpression(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    iget-object v3, p0, Lru/cn/ad/video/VAST/VastTracker;->parsers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/ad/video/VAST/parser/VastParser;

    .line 31
    .local v2, "vasObj":Lru/cn/ad/video/VAST/parser/VastParser;
    invoke-virtual {v2}, Lru/cn/ad/video/VAST/parser/VastParser;->getImpressionTrackerUrls()Ljava/util/List;

    move-result-object v1

    .line 32
    .local v1, "impressionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 33
    .local v0, "impression":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_1

    .line 34
    const-string v5, "VastTracker"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Track impression url "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    iget-object v5, p0, Lru/cn/ad/video/VAST/VastTracker;->userAgent:Ljava/lang/String;

    invoke-static {v0, v5}, Lru/cn/utils/http/HttpClient;->sendRequestAsync(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 40
    .end local v0    # "impression":Ljava/lang/String;
    .end local v1    # "impressionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "vasObj":Lru/cn/ad/video/VAST/parser/VastParser;
    :cond_2
    return-void
.end method
