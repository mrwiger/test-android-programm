.class public Lru/cn/domain/stores/iabhelper/IabException;
.super Ljava/lang/Exception;
.source "IabException.java"


# instance fields
.field mResult:Lru/cn/domain/stores/iabhelper/IabResult;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "response"    # I
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 32
    new-instance v0, Lru/cn/domain/stores/iabhelper/IabResult;

    invoke-direct {v0, p1, p2}, Lru/cn/domain/stores/iabhelper/IabResult;-><init>(ILjava/lang/String;)V

    invoke-direct {p0, v0}, Lru/cn/domain/stores/iabhelper/IabException;-><init>(Lru/cn/domain/stores/iabhelper/IabResult;)V

    .line 33
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "response"    # I
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "cause"    # Ljava/lang/Exception;

    .prologue
    .line 41
    new-instance v0, Lru/cn/domain/stores/iabhelper/IabResult;

    invoke-direct {v0, p1, p2}, Lru/cn/domain/stores/iabhelper/IabResult;-><init>(ILjava/lang/String;)V

    invoke-direct {p0, v0, p3}, Lru/cn/domain/stores/iabhelper/IabException;-><init>(Lru/cn/domain/stores/iabhelper/IabResult;Ljava/lang/Exception;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Lru/cn/domain/stores/iabhelper/IabResult;)V
    .locals 1
    .param p1, "r"    # Lru/cn/domain/stores/iabhelper/IabResult;

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lru/cn/domain/stores/iabhelper/IabException;-><init>(Lru/cn/domain/stores/iabhelper/IabResult;Ljava/lang/Exception;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Lru/cn/domain/stores/iabhelper/IabResult;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "r"    # Lru/cn/domain/stores/iabhelper/IabResult;
    .param p2, "cause"    # Ljava/lang/Exception;

    .prologue
    .line 36
    invoke-virtual {p1}, Lru/cn/domain/stores/iabhelper/IabResult;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 37
    iput-object p1, p0, Lru/cn/domain/stores/iabhelper/IabException;->mResult:Lru/cn/domain/stores/iabhelper/IabResult;

    .line 38
    return-void
.end method


# virtual methods
.method public getResult()Lru/cn/domain/stores/iabhelper/IabResult;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lru/cn/domain/stores/iabhelper/IabException;->mResult:Lru/cn/domain/stores/iabhelper/IabResult;

    return-object v0
.end method
