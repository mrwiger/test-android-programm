.class public final Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;
.super Ljava/lang/Object;
.source "InstreamAudioAd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/instreamads/InstreamAudioAd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InstreamAdCompanionBanner"
.end annotation


# instance fields
.field public final adSlotID:Ljava/lang/String;

.field public final apiFramework:Ljava/lang/String;

.field public final assetHeight:I

.field public final assetWidth:I

.field public final expandedHeight:I

.field public final expandedWidth:I

.field public final height:I

.field public final htmlResource:Ljava/lang/String;

.field public final iframeResource:Ljava/lang/String;

.field public final required:Ljava/lang/String;

.field public final staticResource:Ljava/lang/String;

.field public final width:I


# direct methods
.method private constructor <init>(IIIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "assetWidth"    # I
    .param p4, "assetHeight"    # I
    .param p5, "expandedWidth"    # I
    .param p6, "expandedHeight"    # I
    .param p7, "staticResource"    # Ljava/lang/String;
    .param p8, "iframeResource"    # Ljava/lang/String;
    .param p9, "htmlResource"    # Ljava/lang/String;
    .param p10, "apiFramework"    # Ljava/lang/String;
    .param p11, "adSlotID"    # Ljava/lang/String;
    .param p12, "required"    # Ljava/lang/String;

    .prologue
    .line 403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 404
    iput p1, p0, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;->width:I

    .line 405
    iput p2, p0, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;->height:I

    .line 406
    iput p3, p0, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;->assetWidth:I

    .line 407
    iput p4, p0, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;->assetHeight:I

    .line 408
    iput p5, p0, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;->expandedWidth:I

    .line 409
    iput p6, p0, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;->expandedHeight:I

    .line 410
    iput-object p7, p0, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;->staticResource:Ljava/lang/String;

    .line 411
    iput-object p8, p0, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;->iframeResource:Ljava/lang/String;

    .line 412
    iput-object p9, p0, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;->htmlResource:Ljava/lang/String;

    .line 413
    iput-object p10, p0, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;->apiFramework:Ljava/lang/String;

    .line 414
    iput-object p11, p0, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;->adSlotID:Ljava/lang/String;

    .line 415
    iput-object p12, p0, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;->required:Ljava/lang/String;

    .line 416
    return-void
.end method

.method public static newBanner(Lcom/my/target/ai;)Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;
    .locals 13
    .param p0, "companionBanner"    # Lcom/my/target/ai;

    .prologue
    .line 364
    new-instance v0, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;

    invoke-virtual {p0}, Lcom/my/target/ai;->getWidth()I

    move-result v1

    .line 365
    invoke-virtual {p0}, Lcom/my/target/ai;->getHeight()I

    move-result v2

    .line 366
    invoke-virtual {p0}, Lcom/my/target/ai;->getAssetWidth()I

    move-result v3

    .line 367
    invoke-virtual {p0}, Lcom/my/target/ai;->getAssetHeight()I

    move-result v4

    .line 368
    invoke-virtual {p0}, Lcom/my/target/ai;->getExpandedWidth()I

    move-result v5

    .line 369
    invoke-virtual {p0}, Lcom/my/target/ai;->getExpandedHeight()I

    move-result v6

    .line 370
    invoke-virtual {p0}, Lcom/my/target/ai;->getStaticResource()Ljava/lang/String;

    move-result-object v7

    .line 371
    invoke-virtual {p0}, Lcom/my/target/ai;->getIframeResource()Ljava/lang/String;

    move-result-object v8

    .line 372
    invoke-virtual {p0}, Lcom/my/target/ai;->getHtmlResource()Ljava/lang/String;

    move-result-object v9

    .line 373
    invoke-virtual {p0}, Lcom/my/target/ai;->getApiFramework()Ljava/lang/String;

    move-result-object v10

    .line 374
    invoke-virtual {p0}, Lcom/my/target/ai;->getAdSlotID()Ljava/lang/String;

    move-result-object v11

    .line 375
    invoke-virtual {p0}, Lcom/my/target/ai;->getRequired()Ljava/lang/String;

    move-result-object v12

    invoke-direct/range {v0 .. v12}, Lcom/my/target/instreamads/InstreamAudioAd$InstreamAdCompanionBanner;-><init>(IIIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    return-object v0
.end method
