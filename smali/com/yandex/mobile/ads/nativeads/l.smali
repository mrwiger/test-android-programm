.class public Lcom/yandex/mobile/ads/nativeads/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/yandex/mobile/ads/j;
.implements Lcom/yandex/mobile/ads/nativeads/NativeGenericAd;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/yandex/mobile/ads/nativeads/d;

.field private final c:Lcom/yandex/mobile/ads/nativeads/h;

.field private final d:Lcom/yandex/mobile/ads/n;

.field private final e:Lcom/yandex/mobile/ads/e;

.field private final f:Lcom/yandex/mobile/ads/i;

.field private final g:Lcom/yandex/mobile/ads/ao;

.field private final h:Lcom/yandex/mobile/ads/background/a;

.field private final i:Lcom/yandex/mobile/ads/a;

.field private final j:Lcom/yandex/mobile/ads/report/b$a;

.field private k:Lcom/yandex/mobile/ads/nativeads/a;

.field private l:Lcom/yandex/mobile/ads/nativeads/NativeAdEventListener;

.field private m:Lcom/yandex/mobile/ads/nativeads/r;

.field private n:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/yandex/mobile/ads/nativeads/NativeAdImageLoadingListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/yandex/mobile/ads/nativeads/o;Lcom/yandex/mobile/ads/nativeads/d;Lcom/yandex/mobile/ads/report/b$a;)V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->n:Ljava/util/Set;

    .line 67
    iput-object p1, p0, Lcom/yandex/mobile/ads/nativeads/l;->a:Landroid/content/Context;

    .line 68
    iput-object p3, p0, Lcom/yandex/mobile/ads/nativeads/l;->b:Lcom/yandex/mobile/ads/nativeads/d;

    .line 69
    iput-object p4, p0, Lcom/yandex/mobile/ads/nativeads/l;->j:Lcom/yandex/mobile/ads/report/b$a;

    .line 71
    invoke-virtual {p2}, Lcom/yandex/mobile/ads/nativeads/o;->d()Lcom/yandex/mobile/ads/nativeads/h;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->c:Lcom/yandex/mobile/ads/nativeads/h;

    .line 72
    invoke-virtual {p2}, Lcom/yandex/mobile/ads/nativeads/o;->a()Lcom/yandex/mobile/ads/n;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->d:Lcom/yandex/mobile/ads/n;

    .line 73
    invoke-virtual {p2}, Lcom/yandex/mobile/ads/nativeads/o;->b()Lcom/yandex/mobile/ads/e;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->e:Lcom/yandex/mobile/ads/e;

    .line 75
    new-instance v0, Lcom/yandex/mobile/ads/i;

    invoke-direct {v0, p0}, Lcom/yandex/mobile/ads/i;-><init>(Lcom/yandex/mobile/ads/j;)V

    iput-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->f:Lcom/yandex/mobile/ads/i;

    .line 76
    new-instance v0, Lcom/yandex/mobile/ads/ao;

    invoke-direct {v0, p1}, Lcom/yandex/mobile/ads/ao;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->g:Lcom/yandex/mobile/ads/ao;

    .line 77
    new-instance v0, Lcom/yandex/mobile/ads/a;

    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/l;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/yandex/mobile/ads/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->i:Lcom/yandex/mobile/ads/a;

    .line 78
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/mobile/ads/background/b;->a(Landroid/content/Context;)Lcom/yandex/mobile/ads/background/a;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->h:Lcom/yandex/mobile/ads/background/a;

    .line 79
    return-void
.end method

.method static synthetic a(Lcom/yandex/mobile/ads/nativeads/l;)Lcom/yandex/mobile/ads/a;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->i:Lcom/yandex/mobile/ads/a;

    return-object v0
.end method

.method static a(Lcom/yandex/mobile/ads/nativeads/b;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/yandex/mobile/ads/nativeads/b",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 222
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/yandex/mobile/ads/nativeads/b;->a()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/yandex/mobile/ads/nativeads/l;)Lcom/yandex/mobile/ads/background/a;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->h:Lcom/yandex/mobile/ads/background/a;

    return-object v0
.end method

.method static synthetic c(Lcom/yandex/mobile/ads/nativeads/l;)Lcom/yandex/mobile/ads/nativeads/r;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->m:Lcom/yandex/mobile/ads/nativeads/r;

    return-object v0
.end method

.method static synthetic d(Lcom/yandex/mobile/ads/nativeads/l;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->n:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 2

    .prologue
    .line 236
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->h:Lcom/yandex/mobile/ads/background/a;

    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/l;->a:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/yandex/mobile/ads/background/a;->a(Landroid/content/Context;)V

    .line 237
    return-void
.end method

.method public a(Lcom/yandex/mobile/ads/nativeads/r;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/yandex/mobile/ads/nativeads/l;->m:Lcom/yandex/mobile/ads/nativeads/r;

    .line 150
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 144
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/l;->d:Lcom/yandex/mobile/ads/n;

    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/l;->e:Lcom/yandex/mobile/ads/e;

    invoke-virtual {v1}, Lcom/yandex/mobile/ads/e;->i()Z

    move-result v3

    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/l;->f:Lcom/yandex/mobile/ads/i;

    invoke-virtual {v1}, Lcom/yandex/mobile/ads/i;->a()Lcom/yandex/mobile/ads/o;

    move-result-object v4

    iget-object v5, p0, Lcom/yandex/mobile/ads/nativeads/l;->k:Lcom/yandex/mobile/ads/nativeads/a;

    iget-object v6, p0, Lcom/yandex/mobile/ads/nativeads/l;->j:Lcom/yandex/mobile/ads/report/b$a;

    move-object v1, p1

    invoke-static/range {v0 .. v6}, Lcom/yandex/mobile/ads/q;->a(Landroid/content/Context;Ljava/lang/String;Lcom/yandex/mobile/ads/n;ZLandroid/os/ResultReceiver;Lcom/yandex/mobile/ads/nativeads/a;Lcom/yandex/mobile/ads/report/b$a;)V

    .line 146
    return-void
.end method

.method public declared-synchronized addImageLoadingListener(Lcom/yandex/mobile/ads/nativeads/NativeAdImageLoadingListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/yandex/mobile/ads/nativeads/NativeAdImageLoadingListener;

    .prologue
    .line 183
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->n:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->n:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 186
    :cond_0
    monitor-exit p0

    return-void

    .line 183
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getAdAssets()Lcom/yandex/mobile/ads/nativeads/NativeAdAssets;
    .locals 5

    .prologue
    .line 195
    new-instance v1, Lcom/yandex/mobile/ads/nativeads/NativeAdAssets;

    invoke-direct {v1}, Lcom/yandex/mobile/ads/nativeads/NativeAdAssets;-><init>()V

    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->c:Lcom/yandex/mobile/ads/nativeads/h;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/h;->c()Ljava/util/List;

    move-result-object v0

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/b;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/b;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const-string v0, "age"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/b;

    invoke-static {v0}, Lcom/yandex/mobile/ads/nativeads/l;->a(Lcom/yandex/mobile/ads/nativeads/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/yandex/mobile/ads/nativeads/NativeAdAssets;->a(Ljava/lang/String;)V

    const-string v0, "body"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/b;

    invoke-static {v0}, Lcom/yandex/mobile/ads/nativeads/l;->a(Lcom/yandex/mobile/ads/nativeads/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/yandex/mobile/ads/nativeads/NativeAdAssets;->b(Ljava/lang/String;)V

    const-string v0, "call_to_action"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/b;

    invoke-static {v0}, Lcom/yandex/mobile/ads/nativeads/l;->a(Lcom/yandex/mobile/ads/nativeads/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/yandex/mobile/ads/nativeads/NativeAdAssets;->c(Ljava/lang/String;)V

    const-string v0, "domain"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/b;

    invoke-static {v0}, Lcom/yandex/mobile/ads/nativeads/l;->a(Lcom/yandex/mobile/ads/nativeads/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/yandex/mobile/ads/nativeads/NativeAdAssets;->d(Ljava/lang/String;)V

    const-string v0, "favicon"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/b;

    invoke-static {v0}, Lcom/yandex/mobile/ads/nativeads/l;->a(Lcom/yandex/mobile/ads/nativeads/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/e;

    iget-object v3, p0, Lcom/yandex/mobile/ads/nativeads/l;->b:Lcom/yandex/mobile/ads/nativeads/d;

    invoke-virtual {v1, v0, v3}, Lcom/yandex/mobile/ads/nativeads/NativeAdAssets;->a(Lcom/yandex/mobile/ads/nativeads/e;Lcom/yandex/mobile/ads/nativeads/d;)V

    const-string v0, "icon"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/b;

    invoke-static {v0}, Lcom/yandex/mobile/ads/nativeads/l;->a(Lcom/yandex/mobile/ads/nativeads/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/e;

    iget-object v3, p0, Lcom/yandex/mobile/ads/nativeads/l;->b:Lcom/yandex/mobile/ads/nativeads/d;

    invoke-virtual {v1, v0, v3}, Lcom/yandex/mobile/ads/nativeads/NativeAdAssets;->b(Lcom/yandex/mobile/ads/nativeads/e;Lcom/yandex/mobile/ads/nativeads/d;)V

    const-string v0, "image"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/b;

    invoke-static {v0}, Lcom/yandex/mobile/ads/nativeads/l;->a(Lcom/yandex/mobile/ads/nativeads/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/e;

    iget-object v3, p0, Lcom/yandex/mobile/ads/nativeads/l;->b:Lcom/yandex/mobile/ads/nativeads/d;

    invoke-virtual {v1, v0, v3}, Lcom/yandex/mobile/ads/nativeads/NativeAdAssets;->c(Lcom/yandex/mobile/ads/nativeads/e;Lcom/yandex/mobile/ads/nativeads/d;)V

    const-string v0, "price"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/b;

    invoke-static {v0}, Lcom/yandex/mobile/ads/nativeads/l;->a(Lcom/yandex/mobile/ads/nativeads/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/yandex/mobile/ads/nativeads/NativeAdAssets;->e(Ljava/lang/String;)V

    const-string v0, "rating"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/b;

    invoke-static {v0}, Lcom/yandex/mobile/ads/nativeads/l;->a(Lcom/yandex/mobile/ads/nativeads/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/yandex/mobile/ads/nativeads/NativeAdAssets;->f(Ljava/lang/String;)V

    const-string v0, "review_count"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/b;

    invoke-static {v0}, Lcom/yandex/mobile/ads/nativeads/l;->a(Lcom/yandex/mobile/ads/nativeads/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/yandex/mobile/ads/nativeads/NativeAdAssets;->g(Ljava/lang/String;)V

    const-string v0, "sponsored"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/b;

    invoke-static {v0}, Lcom/yandex/mobile/ads/nativeads/l;->a(Lcom/yandex/mobile/ads/nativeads/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/yandex/mobile/ads/nativeads/NativeAdAssets;->h(Ljava/lang/String;)V

    const-string v0, "title"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/b;

    invoke-static {v0}, Lcom/yandex/mobile/ads/nativeads/l;->a(Lcom/yandex/mobile/ads/nativeads/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/yandex/mobile/ads/nativeads/NativeAdAssets;->i(Ljava/lang/String;)V

    const-string v0, "warning"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/b;

    invoke-static {v0}, Lcom/yandex/mobile/ads/nativeads/l;->a(Lcom/yandex/mobile/ads/nativeads/b;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/yandex/mobile/ads/nativeads/NativeAdAssets;->j(Ljava/lang/String;)V

    return-object v1
.end method

.method public getAdType()Lcom/yandex/mobile/ads/nativeads/NativeAdType;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->c:Lcom/yandex/mobile/ads/nativeads/h;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/h;->b()Lcom/yandex/mobile/ads/nativeads/NativeAdType;

    move-result-object v0

    return-object v0
.end method

.method public loadImages()V
    .locals 3

    .prologue
    .line 154
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->g:Lcom/yandex/mobile/ads/ao;

    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/l;->c:Lcom/yandex/mobile/ads/nativeads/h;

    new-instance v2, Lcom/yandex/mobile/ads/nativeads/l$2;

    invoke-direct {v2, p0}, Lcom/yandex/mobile/ads/nativeads/l$2;-><init>(Lcom/yandex/mobile/ads/nativeads/l;)V

    invoke-virtual {v0, v1, v2}, Lcom/yandex/mobile/ads/ao;->a(Lcom/yandex/mobile/ads/nativeads/h;Lcom/yandex/mobile/ads/nativeads/f;)V

    .line 179
    return-void
.end method

.method public onAdClosed()V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->l:Lcom/yandex/mobile/ads/nativeads/NativeAdEventListener;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->l:Lcom/yandex/mobile/ads/nativeads/NativeAdEventListener;

    invoke-interface {v0}, Lcom/yandex/mobile/ads/nativeads/NativeAdEventListener;->onAdClosed()V

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->i:Lcom/yandex/mobile/ads/a;

    sget-object v1, Lcom/yandex/mobile/ads/a$a;->b:Lcom/yandex/mobile/ads/a$a;

    invoke-virtual {v0, v1}, Lcom/yandex/mobile/ads/a;->b(Lcom/yandex/mobile/ads/a$a;)V

    .line 112
    return-void
.end method

.method public onAdLeftApplication()V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->l:Lcom/yandex/mobile/ads/nativeads/NativeAdEventListener;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->l:Lcom/yandex/mobile/ads/nativeads/NativeAdEventListener;

    invoke-interface {v0}, Lcom/yandex/mobile/ads/nativeads/NativeAdEventListener;->onAdLeftApplication()V

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->h:Lcom/yandex/mobile/ads/background/a;

    invoke-interface {v0}, Lcom/yandex/mobile/ads/background/a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->i:Lcom/yandex/mobile/ads/a;

    sget-object v1, Lcom/yandex/mobile/ads/a$a;->a:Lcom/yandex/mobile/ads/a$a;

    invoke-virtual {v0, v1}, Lcom/yandex/mobile/ads/a;->a(Lcom/yandex/mobile/ads/a$a;)V

    .line 122
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->h:Lcom/yandex/mobile/ads/background/a;

    new-instance v1, Lcom/yandex/mobile/ads/nativeads/l$1;

    invoke-direct {v1, p0}, Lcom/yandex/mobile/ads/nativeads/l$1;-><init>(Lcom/yandex/mobile/ads/nativeads/l;)V

    invoke-interface {v0, v1}, Lcom/yandex/mobile/ads/background/a;->a(Lcom/yandex/mobile/ads/background/d;)V

    .line 132
    :goto_0
    return-void

    .line 130
    :cond_1
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->i:Lcom/yandex/mobile/ads/a;

    sget-object v1, Lcom/yandex/mobile/ads/a$a;->a:Lcom/yandex/mobile/ads/a$a;

    invoke-virtual {v0, v1}, Lcom/yandex/mobile/ads/a;->c(Lcom/yandex/mobile/ads/a$a;)V

    goto :goto_0
.end method

.method public onAdOpened()V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->l:Lcom/yandex/mobile/ads/nativeads/NativeAdEventListener;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->l:Lcom/yandex/mobile/ads/nativeads/NativeAdEventListener;

    invoke-interface {v0}, Lcom/yandex/mobile/ads/nativeads/NativeAdEventListener;->onAdOpened()V

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->i:Lcom/yandex/mobile/ads/a;

    sget-object v1, Lcom/yandex/mobile/ads/a$a;->b:Lcom/yandex/mobile/ads/a$a;

    invoke-virtual {v0, v1}, Lcom/yandex/mobile/ads/a;->a(Lcom/yandex/mobile/ads/a$a;)V

    .line 141
    return-void
.end method

.method public declared-synchronized removeImageLoadingListener(Lcom/yandex/mobile/ads/nativeads/NativeAdImageLoadingListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/yandex/mobile/ads/nativeads/NativeAdImageLoadingListener;

    .prologue
    .line 190
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->n:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    monitor-exit p0

    return-void

    .line 190
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setAdEventListener(Lcom/yandex/mobile/ads/nativeads/NativeAdEventListener;)V
    .locals 0
    .param p1, "adEventListener"    # Lcom/yandex/mobile/ads/nativeads/NativeAdEventListener;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/yandex/mobile/ads/nativeads/l;->l:Lcom/yandex/mobile/ads/nativeads/NativeAdEventListener;

    .line 84
    return-void
.end method

.method public shouldOpenLinksInApp(Z)V
    .locals 1
    .param p1, "shouldOpenLinksInApp"    # Z

    .prologue
    .line 93
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/l;->e:Lcom/yandex/mobile/ads/e;

    invoke-virtual {v0, p1}, Lcom/yandex/mobile/ads/e;->a(Z)V

    .line 94
    return-void
.end method
