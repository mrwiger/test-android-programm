.class public Lru/cn/ad/video/VAST/MediaComparator;
.super Ljava/lang/Object;
.source "MediaComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lru/cn/ad/video/VAST/parser/MediaFile;",
        ">;"
    }
.end annotation


# instance fields
.field private final mimeTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final optimalDimension:I


# direct methods
.method public constructor <init>(Ljava/util/List;I)V
    .locals 0
    .param p2, "optimalDimension"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 13
    .local p1, "mimeTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lru/cn/ad/video/VAST/MediaComparator;->mimeTypes:Ljava/util/List;

    .line 15
    iput p2, p0, Lru/cn/ad/video/VAST/MediaComparator;->optimalDimension:I

    .line 16
    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 9
    check-cast p1, Lru/cn/ad/video/VAST/parser/MediaFile;

    check-cast p2, Lru/cn/ad/video/VAST/parser/MediaFile;

    invoke-virtual {p0, p1, p2}, Lru/cn/ad/video/VAST/MediaComparator;->compare(Lru/cn/ad/video/VAST/parser/MediaFile;Lru/cn/ad/video/VAST/parser/MediaFile;)I

    move-result v0

    return v0
.end method

.method public compare(Lru/cn/ad/video/VAST/parser/MediaFile;Lru/cn/ad/video/VAST/parser/MediaFile;)I
    .locals 5
    .param p1, "lhs"    # Lru/cn/ad/video/VAST/parser/MediaFile;
    .param p2, "rhs"    # Lru/cn/ad/video/VAST/parser/MediaFile;

    .prologue
    .line 20
    iget-object v2, p0, Lru/cn/ad/video/VAST/MediaComparator;->mimeTypes:Ljava/util/List;

    iget-object v3, p1, Lru/cn/ad/video/VAST/parser/MediaFile;->type:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 21
    .local v0, "leftMimeIndex":I
    iget-object v2, p0, Lru/cn/ad/video/VAST/MediaComparator;->mimeTypes:Ljava/util/List;

    iget-object v3, p2, Lru/cn/ad/video/VAST/parser/MediaFile;->type:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 23
    .local v1, "rightMimeIndex":I
    if-eq v0, v1, :cond_0

    .line 24
    sub-int v2, v0, v1

    .line 31
    :goto_0
    return v2

    .line 27
    :cond_0
    iget v2, p1, Lru/cn/ad/video/VAST/parser/MediaFile;->height:I

    iget v3, p2, Lru/cn/ad/video/VAST/parser/MediaFile;->height:I

    if-eq v2, v3, :cond_1

    .line 28
    iget v2, p1, Lru/cn/ad/video/VAST/parser/MediaFile;->height:I

    iget v3, p0, Lru/cn/ad/video/VAST/MediaComparator;->optimalDimension:I

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iget v3, p2, Lru/cn/ad/video/VAST/parser/MediaFile;->height:I

    iget v4, p0, Lru/cn/ad/video/VAST/MediaComparator;->optimalDimension:I

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    sub-int/2addr v2, v3

    goto :goto_0

    .line 31
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
