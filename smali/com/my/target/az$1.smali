.class Lcom/my/target/az$1;
.super Ljava/lang/Object;
.source "LogMessage.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/my/target/az;->e(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic dW:Landroid/content/Context;

.field final synthetic dX:Lcom/my/target/az;


# direct methods
.method constructor <init>(Lcom/my/target/az;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/my/target/az$1;->dX:Lcom/my/target/az;

    iput-object p2, p0, Lcom/my/target/az$1;->dW:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 88
    iget-object v0, p0, Lcom/my/target/az$1;->dX:Lcom/my/target/az;

    invoke-virtual {v0}, Lcom/my/target/az;->ao()Ljava/lang/String;

    move-result-object v0

    .line 89
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "send message to log:\n "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 90
    sget-boolean v1, Lcom/my/target/az;->dS:Z

    if-eqz v1, :cond_0

    .line 92
    const-string v1, "UTF-8"

    invoke-static {v1}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 93
    invoke-static {}, Lcom/my/target/av;->aj()Lcom/my/target/av;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/my/target/av;->x(Ljava/lang/String;)Lcom/my/target/av;

    move-result-object v0

    invoke-static {}, Lcom/my/target/az;->ap()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/my/target/az$1;->dW:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, Lcom/my/target/av;->f(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/Object;

    .line 95
    :cond_0
    return-void
.end method
