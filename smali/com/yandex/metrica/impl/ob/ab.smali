.class public Lcom/yandex/metrica/impl/ob/ab;
.super Lcom/yandex/metrica/impl/ob/ac;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/yandex/metrica/impl/ob/ac",
        "<",
        "Lcom/yandex/metrica/impl/ob/ah;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/yandex/metrica/impl/ob/bc;

.field private final b:Lcom/yandex/metrica/impl/ob/bb;

.field private final c:Lcom/yandex/metrica/impl/ob/ar;

.field private final d:Lcom/yandex/metrica/impl/ob/az;

.field private final e:Lcom/yandex/metrica/impl/ob/am;


# direct methods
.method public constructor <init>(Lcom/yandex/metrica/impl/ob/v;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ac;-><init>()V

    .line 35
    new-instance v0, Lcom/yandex/metrica/impl/ob/bc;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/bc;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ab;->a:Lcom/yandex/metrica/impl/ob/bc;

    .line 36
    new-instance v0, Lcom/yandex/metrica/impl/ob/bb;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/bb;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ab;->b:Lcom/yandex/metrica/impl/ob/bb;

    .line 37
    new-instance v0, Lcom/yandex/metrica/impl/ob/ar;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/ar;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ab;->c:Lcom/yandex/metrica/impl/ob/ar;

    .line 38
    new-instance v0, Lcom/yandex/metrica/impl/ob/az;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/az;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ab;->d:Lcom/yandex/metrica/impl/ob/az;

    .line 39
    new-instance v0, Lcom/yandex/metrica/impl/ob/am;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/am;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ab;->e:Lcom/yandex/metrica/impl/ob/am;

    .line 40
    return-void
.end method


# virtual methods
.method a(I)Lcom/yandex/metrica/impl/ob/z;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/yandex/metrica/impl/ob/z",
            "<",
            "Lcom/yandex/metrica/impl/ob/ah;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 45
    invoke-static {p1}, Lcom/yandex/metrica/impl/q$a;->a(I)Lcom/yandex/metrica/impl/q$a;

    move-result-object v1

    .line 46
    sget-object v2, Lcom/yandex/metrica/impl/ob/ab$1;->a:[I

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/q$a;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 69
    :goto_0
    new-instance v1, Lcom/yandex/metrica/impl/ob/y;

    invoke-direct {v1, v0}, Lcom/yandex/metrica/impl/ob/y;-><init>(Ljava/util/List;)V

    return-object v1

    .line 48
    :pswitch_0
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ab;->d:Lcom/yandex/metrica/impl/ob/az;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 51
    :pswitch_1
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ab;->d:Lcom/yandex/metrica/impl/ob/az;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 52
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ab;->c:Lcom/yandex/metrica/impl/ob/ar;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 55
    :pswitch_2
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ab;->a:Lcom/yandex/metrica/impl/ob/bc;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 56
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ab;->b:Lcom/yandex/metrica/impl/ob/bb;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 60
    :pswitch_3
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ab;->c:Lcom/yandex/metrica/impl/ob/ar;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 63
    :pswitch_4
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ab;->e:Lcom/yandex/metrica/impl/ob/am;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 46
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
