.class public final Lcom/my/target/fk;
.super Lcom/my/target/d;
.source "InstreamAudioAdResponseParser.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/my/target/d",
        "<",
        "Lcom/my/target/fm;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/my/target/d;-><init>()V

    return-void
.end method

.method private static a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/ae;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/ae;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 322
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/ae;

    .line 324
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/my/target/ae;

    .line 326
    invoke-virtual {v0}, Lcom/my/target/ae;->z()I

    move-result v4

    invoke-virtual {v1}, Lcom/my/target/ae;->getId()I

    move-result v5

    if-ne v4, v5, :cond_1

    .line 328
    invoke-virtual {v1, v0}, Lcom/my/target/ae;->a(Lcom/my/target/ae;)V

    goto :goto_0

    .line 333
    :cond_2
    return-void
.end method

.method private static a(Lorg/json/JSONObject;Lcom/my/target/bd;Lcom/my/target/al;Lcom/my/target/core/parsers/j;Lcom/my/target/ae;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Lcom/my/target/bd;",
            "Lcom/my/target/al",
            "<",
            "Lcom/my/target/common/models/AudioData;",
            ">;",
            "Lcom/my/target/core/parsers/j;",
            "Lcom/my/target/ae;",
            ")V"
        }
    .end annotation

    .prologue
    .line 237
    invoke-virtual {p2}, Lcom/my/target/al;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 238
    if-nez v3, :cond_0

    .line 282
    :goto_0
    return-void

    .line 243
    :cond_0
    invoke-virtual {p4}, Lcom/my/target/ae;->getPosition()I

    move-result v1

    .line 244
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 245
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 246
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_8

    .line 248
    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 249
    if-eqz v2, :cond_1

    .line 254
    const-string v6, "type"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 255
    const-string v7, "additionalData"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1291
    invoke-virtual {p1, v2}, Lcom/my/target/bd;->c(Lorg/json/JSONObject;)Lcom/my/target/ae;

    move-result-object v2

    .line 1292
    if-eqz v2, :cond_1

    .line 1296
    invoke-virtual {p2}, Lcom/my/target/al;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/my/target/ae;->o(Ljava/lang/String;)V

    .line 1297
    invoke-virtual {v2}, Lcom/my/target/ae;->z()I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_2

    .line 1299
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 246
    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1302
    :cond_2
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1303
    invoke-virtual {v2}, Lcom/my/target/ae;->A()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v2}, Lcom/my/target/ae;->y()Z

    move-result v6

    if-nez v6, :cond_3

    .line 1305
    invoke-virtual {p4, v2}, Lcom/my/target/ae;->b(Lcom/my/target/ae;)V

    .line 1306
    invoke-virtual {p4}, Lcom/my/target/ae;->getPosition()I

    move-result v6

    .line 1307
    if-ltz v6, :cond_4

    .line 1309
    invoke-virtual {v2, v6}, Lcom/my/target/ae;->b(I)V

    .line 1316
    :cond_3
    :goto_3
    invoke-virtual {p2, v2}, Lcom/my/target/al;->c(Lcom/my/target/ae;)V

    goto :goto_2

    .line 1313
    :cond_4
    invoke-virtual {p2}, Lcom/my/target/al;->getBannersCount()I

    move-result v6

    invoke-virtual {v2, v6}, Lcom/my/target/ae;->b(I)V

    goto :goto_3

    .line 261
    :cond_5
    invoke-static {}, Lcom/my/target/aj;->newAudioBanner()Lcom/my/target/aj;

    move-result-object v6

    .line 262
    invoke-virtual {p3, v2, v6}, Lcom/my/target/core/parsers/j;->a(Lorg/json/JSONObject;Lcom/my/target/aj;)Z

    move-result v2

    .line 263
    if-eqz v2, :cond_1

    .line 265
    invoke-virtual {p4}, Lcom/my/target/ae;->A()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 267
    invoke-virtual {p4}, Lcom/my/target/ae;->getPoint()F

    move-result v2

    invoke-virtual {v6, v2}, Lcom/my/target/aj;->setPoint(F)V

    .line 268
    invoke-virtual {p4}, Lcom/my/target/ae;->getPointP()F

    move-result v2

    invoke-virtual {v6, v2}, Lcom/my/target/aj;->setPointP(F)V

    .line 270
    :cond_6
    if-ltz v1, :cond_7

    .line 272
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p2, v6, v1}, Lcom/my/target/al;->a(Lcom/my/target/aj;I)V

    move v1, v2

    goto :goto_2

    .line 276
    :cond_7
    invoke-virtual {p2, v6}, Lcom/my/target/al;->a(Lcom/my/target/aj;)V

    goto :goto_2

    .line 281
    :cond_8
    invoke-static {v5, v4}, Lcom/my/target/fk;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto/16 :goto_0
.end method

.method public static newParser()Lcom/my/target/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/my/target/d",
            "<",
            "Lcom/my/target/fm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    new-instance v0, Lcom/my/target/fk;

    invoke-direct {v0}, Lcom/my/target/fk;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;Lcom/my/target/ae;Lcom/my/target/ak;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/ak;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 32
    check-cast p3, Lcom/my/target/fm;

    .line 2046
    invoke-static {p1}, Lcom/my/target/fk;->isVast(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2060
    invoke-static {p4, p2, p5}, Lcom/my/target/bi;->a(Lcom/my/target/b;Lcom/my/target/ae;Landroid/content/Context;)Lcom/my/target/bi;

    move-result-object v1

    .line 2061
    invoke-virtual {v1, p1}, Lcom/my/target/bi;->F(Ljava/lang/String;)V

    .line 2063
    invoke-virtual {p2}, Lcom/my/target/ae;->H()Ljava/lang/String;

    move-result-object v0

    .line 2064
    if-eqz v0, :cond_7

    .line 2066
    :goto_0
    if-nez p3, :cond_0

    .line 2068
    invoke-static {}, Lcom/my/target/fm;->h()Lcom/my/target/fm;

    move-result-object p3

    .line 2071
    :cond_0
    invoke-virtual {p3, v0}, Lcom/my/target/fm;->a(Ljava/lang/String;)Lcom/my/target/al;

    move-result-object v3

    .line 2072
    if-eqz v3, :cond_c

    .line 2077
    invoke-virtual {v1}, Lcom/my/target/bi;->as()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 2108
    invoke-virtual {v1}, Lcom/my/target/bi;->F()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/my/target/al;->e(Ljava/util/ArrayList;)V

    .line 2110
    invoke-virtual {p2}, Lcom/my/target/ae;->getPosition()I

    move-result v0

    .line 2111
    invoke-virtual {v1}, Lcom/my/target/bi;->as()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/aj;

    .line 2113
    invoke-virtual {p2}, Lcom/my/target/ae;->J()Ljava/lang/Boolean;

    move-result-object v2

    .line 2114
    if-eqz v2, :cond_8

    .line 2116
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/my/target/aj;->setAllowClose(Z)V

    .line 2123
    :goto_2
    invoke-virtual {p2}, Lcom/my/target/ae;->getAllowCloseDelay()F

    move-result v2

    .line 2124
    cmpl-float v5, v2, v6

    if-lez v5, :cond_9

    .line 2126
    invoke-virtual {v0, v2}, Lcom/my/target/aj;->setAllowCloseDelay(F)V

    .line 2133
    :goto_3
    invoke-virtual {p2}, Lcom/my/target/ae;->K()Ljava/lang/Boolean;

    move-result-object v2

    .line 2134
    if-eqz v2, :cond_1

    .line 2136
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/my/target/aj;->setAllowPause(Z)V

    .line 2139
    :cond_1
    invoke-virtual {p2}, Lcom/my/target/ae;->L()Ljava/lang/Boolean;

    move-result-object v2

    .line 2140
    if-eqz v2, :cond_2

    .line 2142
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/my/target/aj;->setAllowSeek(Z)V

    .line 2145
    :cond_2
    invoke-virtual {p2}, Lcom/my/target/ae;->M()Ljava/lang/Boolean;

    move-result-object v2

    .line 2146
    if-eqz v2, :cond_3

    .line 2148
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/my/target/aj;->setAllowSkip(Z)V

    .line 2151
    :cond_3
    invoke-virtual {p2}, Lcom/my/target/ae;->N()Ljava/lang/Boolean;

    move-result-object v2

    .line 2152
    if-eqz v2, :cond_4

    .line 2154
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/my/target/aj;->setAllowTrackChange(Z)V

    .line 2157
    :cond_4
    const-string v2, "Close"

    invoke-virtual {v0, v2}, Lcom/my/target/aj;->setCloseActionText(Ljava/lang/String;)V

    .line 2159
    invoke-virtual {p2}, Lcom/my/target/ae;->getPoint()F

    move-result v2

    .line 2160
    cmpl-float v5, v2, v6

    if-ltz v5, :cond_5

    .line 2162
    invoke-virtual {v0, v2}, Lcom/my/target/aj;->setPoint(F)V

    .line 2164
    :cond_5
    invoke-virtual {p2}, Lcom/my/target/ae;->getPointP()F

    move-result v2

    .line 2165
    cmpl-float v5, v2, v6

    if-ltz v5, :cond_6

    .line 2167
    invoke-virtual {v0, v2}, Lcom/my/target/aj;->setPointP(F)V

    .line 2169
    :cond_6
    if-ltz v1, :cond_a

    .line 2171
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v3, v0, v1}, Lcom/my/target/al;->a(Lcom/my/target/aj;I)V

    move v1, v2

    goto :goto_1

    .line 2064
    :cond_7
    const-string v0, "preroll"

    goto/16 :goto_0

    .line 2120
    :cond_8
    invoke-virtual {v3}, Lcom/my/target/al;->isAllowClose()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/my/target/aj;->setAllowClose(Z)V

    goto :goto_2

    .line 2130
    :cond_9
    invoke-virtual {v3}, Lcom/my/target/al;->getAllowCloseDelay()F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/my/target/aj;->setAllowCloseDelay(F)V

    goto :goto_3

    .line 2175
    :cond_a
    invoke-virtual {v3, v0}, Lcom/my/target/al;->a(Lcom/my/target/aj;)V

    goto/16 :goto_1

    .line 2083
    :cond_b
    invoke-virtual {v1}, Lcom/my/target/bi;->at()Lcom/my/target/ae;

    move-result-object v0

    .line 2084
    if-eqz v0, :cond_c

    .line 2086
    invoke-virtual {v3}, Lcom/my/target/al;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ae;->o(Ljava/lang/String;)V

    .line 2087
    invoke-virtual {p2}, Lcom/my/target/ae;->getPosition()I

    move-result v1

    .line 2088
    if-ltz v1, :cond_d

    .line 2090
    invoke-virtual {v0, v1}, Lcom/my/target/ae;->b(I)V

    .line 2096
    :goto_4
    invoke-virtual {v3, v0}, Lcom/my/target/al;->c(Lcom/my/target/ae;)V

    .line 2048
    :cond_c
    :goto_5
    return-object p3

    .line 2094
    :cond_d
    invoke-virtual {v3}, Lcom/my/target/al;->getBannersCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/ae;->b(I)V

    goto :goto_4

    .line 2186
    :cond_e
    invoke-virtual {p0, p1, p5}, Lcom/my/target/fk;->a(Ljava/lang/String;Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object v0

    .line 2187
    if-eqz v0, :cond_c

    .line 2192
    invoke-virtual {p4}, Lcom/my/target/b;->getFormat()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 2193
    if-eqz v0, :cond_c

    .line 2198
    if-nez p3, :cond_f

    .line 2200
    invoke-static {}, Lcom/my/target/fm;->h()Lcom/my/target/fm;

    move-result-object p3

    .line 2202
    :cond_f
    invoke-static {p2, p4, p5}, Lcom/my/target/core/parsers/k;->a(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/k;

    move-result-object v1

    invoke-virtual {v1, v0, p3}, Lcom/my/target/core/parsers/k;->a(Lorg/json/JSONObject;Lcom/my/target/fm;)V

    .line 2204
    invoke-static {p2, p4, p5}, Lcom/my/target/bd;->a(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/bd;

    move-result-object v1

    .line 2206
    const-string v2, "sections"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 2207
    if-eqz v2, :cond_c

    .line 2209
    invoke-virtual {p2}, Lcom/my/target/ae;->H()Ljava/lang/String;

    move-result-object v0

    .line 2210
    if-eqz v0, :cond_10

    .line 2212
    invoke-virtual {p3, v0}, Lcom/my/target/fm;->a(Ljava/lang/String;)Lcom/my/target/al;

    move-result-object v0

    .line 2213
    if-eqz v0, :cond_c

    .line 2215
    invoke-static {v0, p2, p4, p5}, Lcom/my/target/core/parsers/j;->a(Lcom/my/target/al;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/j;

    move-result-object v3

    .line 2216
    invoke-static {v2, v1, v0, v3, p2}, Lcom/my/target/fk;->a(Lorg/json/JSONObject;Lcom/my/target/bd;Lcom/my/target/al;Lcom/my/target/core/parsers/j;Lcom/my/target/ae;)V

    goto :goto_5

    .line 2221
    :cond_10
    invoke-virtual {p3}, Lcom/my/target/fm;->i()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/al;

    .line 2223
    invoke-static {v0, p2, p4, p5}, Lcom/my/target/core/parsers/j;->a(Lcom/my/target/al;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/j;

    move-result-object v4

    .line 2224
    invoke-static {v2, v1, v0, v4, p2}, Lcom/my/target/fk;->a(Lorg/json/JSONObject;Lcom/my/target/bd;Lcom/my/target/al;Lcom/my/target/core/parsers/j;Lcom/my/target/ae;)V

    goto :goto_6
.end method
