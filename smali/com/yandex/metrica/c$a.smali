.class public final Lcom/yandex/metrica/c$a;
.super Lcom/yandex/metrica/impl/ob/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# static fields
.field private static volatile k:[Lcom/yandex/metrica/c$a;


# instance fields
.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:Ljava/lang/String;

.field public h:Z

.field public i:I

.field public j:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3214
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/d;-><init>()V

    .line 3215
    invoke-virtual {p0}, Lcom/yandex/metrica/c$a;->e()Lcom/yandex/metrica/c$a;

    .line 3216
    return-void
.end method

.method public static d()[Lcom/yandex/metrica/c$a;
    .locals 2

    .prologue
    .line 3174
    sget-object v0, Lcom/yandex/metrica/c$a;->k:[Lcom/yandex/metrica/c$a;

    if-nez v0, :cond_1

    .line 3175
    sget-object v1, Lcom/yandex/metrica/impl/ob/c;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 3177
    :try_start_0
    sget-object v0, Lcom/yandex/metrica/c$a;->k:[Lcom/yandex/metrica/c$a;

    if-nez v0, :cond_0

    .line 3178
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/yandex/metrica/c$a;

    sput-object v0, Lcom/yandex/metrica/c$a;->k:[Lcom/yandex/metrica/c$a;

    .line 3180
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3182
    :cond_1
    sget-object v0, Lcom/yandex/metrica/c$a;->k:[Lcom/yandex/metrica/c$a;

    return-object v0

    .line 3180
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(Lcom/yandex/metrica/impl/ob/b;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, -0x1

    .line 3235
    iget v0, p0, Lcom/yandex/metrica/c$a;->b:I

    if-eq v0, v2, :cond_0

    .line 3236
    const/4 v0, 0x1

    iget v1, p0, Lcom/yandex/metrica/c$a;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->b(II)V

    .line 3238
    :cond_0
    iget v0, p0, Lcom/yandex/metrica/c$a;->c:I

    if-eqz v0, :cond_1

    .line 3239
    const/4 v0, 0x2

    iget v1, p0, Lcom/yandex/metrica/c$a;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->c(II)V

    .line 3241
    :cond_1
    iget v0, p0, Lcom/yandex/metrica/c$a;->d:I

    if-eq v0, v2, :cond_2

    .line 3242
    const/4 v0, 0x3

    iget v1, p0, Lcom/yandex/metrica/c$a;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->b(II)V

    .line 3244
    :cond_2
    iget v0, p0, Lcom/yandex/metrica/c$a;->e:I

    if-eq v0, v2, :cond_3

    .line 3245
    const/4 v0, 0x4

    iget v1, p0, Lcom/yandex/metrica/c$a;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->b(II)V

    .line 3247
    :cond_3
    iget v0, p0, Lcom/yandex/metrica/c$a;->f:I

    if-eq v0, v2, :cond_4

    .line 3248
    const/4 v0, 0x5

    iget v1, p0, Lcom/yandex/metrica/c$a;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->b(II)V

    .line 3250
    :cond_4
    iget-object v0, p0, Lcom/yandex/metrica/c$a;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 3251
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/yandex/metrica/c$a;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(ILjava/lang/String;)V

    .line 3253
    :cond_5
    iget-boolean v0, p0, Lcom/yandex/metrica/c$a;->h:Z

    if-eqz v0, :cond_6

    .line 3254
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/yandex/metrica/c$a;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(IZ)V

    .line 3256
    :cond_6
    iget v0, p0, Lcom/yandex/metrica/c$a;->i:I

    if-eqz v0, :cond_7

    .line 3257
    const/16 v0, 0x8

    iget v1, p0, Lcom/yandex/metrica/c$a;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(II)V

    .line 3259
    :cond_7
    iget v0, p0, Lcom/yandex/metrica/c$a;->j:I

    if-eq v0, v2, :cond_8

    .line 3260
    const/16 v0, 0x9

    iget v1, p0, Lcom/yandex/metrica/c$a;->j:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->b(II)V

    .line 3262
    :cond_8
    invoke-super {p0, p1}, Lcom/yandex/metrica/impl/ob/d;->a(Lcom/yandex/metrica/impl/ob/b;)V

    .line 3263
    return-void
.end method

.method protected c()I
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 3267
    invoke-super {p0}, Lcom/yandex/metrica/impl/ob/d;->c()I

    move-result v0

    .line 3268
    iget v1, p0, Lcom/yandex/metrica/c$a;->b:I

    if-eq v1, v3, :cond_0

    .line 3269
    const/4 v1, 0x1

    iget v2, p0, Lcom/yandex/metrica/c$a;->b:I

    .line 3270
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3272
    :cond_0
    iget v1, p0, Lcom/yandex/metrica/c$a;->c:I

    if-eqz v1, :cond_1

    .line 3273
    const/4 v1, 0x2

    iget v2, p0, Lcom/yandex/metrica/c$a;->c:I

    .line 3274
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3276
    :cond_1
    iget v1, p0, Lcom/yandex/metrica/c$a;->d:I

    if-eq v1, v3, :cond_2

    .line 3277
    const/4 v1, 0x3

    iget v2, p0, Lcom/yandex/metrica/c$a;->d:I

    .line 3278
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3280
    :cond_2
    iget v1, p0, Lcom/yandex/metrica/c$a;->e:I

    if-eq v1, v3, :cond_3

    .line 3281
    const/4 v1, 0x4

    iget v2, p0, Lcom/yandex/metrica/c$a;->e:I

    .line 3282
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3284
    :cond_3
    iget v1, p0, Lcom/yandex/metrica/c$a;->f:I

    if-eq v1, v3, :cond_4

    .line 3285
    const/4 v1, 0x5

    iget v2, p0, Lcom/yandex/metrica/c$a;->f:I

    .line 3286
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3288
    :cond_4
    iget-object v1, p0, Lcom/yandex/metrica/c$a;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 3289
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/yandex/metrica/c$a;->g:Ljava/lang/String;

    .line 3290
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3292
    :cond_5
    iget-boolean v1, p0, Lcom/yandex/metrica/c$a;->h:Z

    if-eqz v1, :cond_6

    .line 3293
    const/4 v1, 0x7

    .line 3294
    invoke-static {v1}, Lcom/yandex/metrica/impl/ob/b;->e(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 3296
    :cond_6
    iget v1, p0, Lcom/yandex/metrica/c$a;->i:I

    if-eqz v1, :cond_7

    .line 3297
    const/16 v1, 0x8

    iget v2, p0, Lcom/yandex/metrica/c$a;->i:I

    .line 3298
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3300
    :cond_7
    iget v1, p0, Lcom/yandex/metrica/c$a;->j:I

    if-eq v1, v3, :cond_8

    .line 3301
    const/16 v1, 0x9

    iget v2, p0, Lcom/yandex/metrica/c$a;->j:I

    .line 3302
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3304
    :cond_8
    return v0
.end method

.method public e()Lcom/yandex/metrica/c$a;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 3219
    iput v1, p0, Lcom/yandex/metrica/c$a;->b:I

    .line 3220
    iput v2, p0, Lcom/yandex/metrica/c$a;->c:I

    .line 3221
    iput v1, p0, Lcom/yandex/metrica/c$a;->d:I

    .line 3222
    iput v1, p0, Lcom/yandex/metrica/c$a;->e:I

    .line 3223
    iput v1, p0, Lcom/yandex/metrica/c$a;->f:I

    .line 3224
    const-string v0, ""

    iput-object v0, p0, Lcom/yandex/metrica/c$a;->g:Ljava/lang/String;

    .line 3225
    iput-boolean v2, p0, Lcom/yandex/metrica/c$a;->h:Z

    .line 3226
    iput v2, p0, Lcom/yandex/metrica/c$a;->i:I

    .line 3227
    iput v1, p0, Lcom/yandex/metrica/c$a;->j:I

    .line 3228
    iput v1, p0, Lcom/yandex/metrica/c$a;->a:I

    .line 3229
    return-object p0
.end method
