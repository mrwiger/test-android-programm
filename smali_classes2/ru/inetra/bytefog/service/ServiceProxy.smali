.class Lru/inetra/bytefog/service/ServiceProxy;
.super Ljava/lang/Object;
.source "ServiceProxy.java"

# interfaces
.implements Lru/inetra/bytefog/service/Instance;


# static fields
.field private static final TAG:Ljava/lang/String; = "ServiceProxy"


# instance fields
.field private bytefogConfig:Lru/inetra/bytefog/service/BytefogConfig;

.field private context:Landroid/content/Context;

.field private crashDumpUpdloadExecutor:Ljava/util/concurrent/Executor;

.field private handler:Landroid/os/Handler;

.field private isStarting:Z

.field private listener:Lru/inetra/bytefog/service/InstanceListener;

.field private mConnection:Landroid/content/ServiceConnection;

.field private mService:Lru/inetra/bytefog/service/IService;

.field private final restartServiceRunnable:Ljava/lang/Runnable;

.field serviceListener:Lru/inetra/bytefog/service/IServiceListener$Stub;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lru/inetra/bytefog/service/BytefogConfig;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "config"    # Lru/inetra/bytefog/service/BytefogConfig;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V

    iput-object v0, p0, Lru/inetra/bytefog/service/ServiceProxy;->crashDumpUpdloadExecutor:Ljava/util/concurrent/Executor;

    .line 28
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lru/inetra/bytefog/service/ServiceProxy;->handler:Landroid/os/Handler;

    .line 29
    new-instance v0, Lru/inetra/bytefog/service/ServiceProxy$1;

    invoke-direct {v0, p0}, Lru/inetra/bytefog/service/ServiceProxy$1;-><init>(Lru/inetra/bytefog/service/ServiceProxy;)V

    iput-object v0, p0, Lru/inetra/bytefog/service/ServiceProxy;->restartServiceRunnable:Ljava/lang/Runnable;

    .line 108
    new-instance v0, Lru/inetra/bytefog/service/ServiceProxy$2;

    invoke-direct {v0, p0}, Lru/inetra/bytefog/service/ServiceProxy$2;-><init>(Lru/inetra/bytefog/service/ServiceProxy;)V

    iput-object v0, p0, Lru/inetra/bytefog/service/ServiceProxy;->mConnection:Landroid/content/ServiceConnection;

    .line 140
    new-instance v0, Lru/inetra/bytefog/service/ServiceProxy$3;

    invoke-direct {v0, p0}, Lru/inetra/bytefog/service/ServiceProxy$3;-><init>(Lru/inetra/bytefog/service/ServiceProxy;)V

    iput-object v0, p0, Lru/inetra/bytefog/service/ServiceProxy;->serviceListener:Lru/inetra/bytefog/service/IServiceListener$Stub;

    .line 38
    iput-object p1, p0, Lru/inetra/bytefog/service/ServiceProxy;->context:Landroid/content/Context;

    .line 39
    iput-object p2, p0, Lru/inetra/bytefog/service/ServiceProxy;->bytefogConfig:Lru/inetra/bytefog/service/BytefogConfig;

    .line 40
    invoke-virtual {p0}, Lru/inetra/bytefog/service/ServiceProxy;->scheduleCrashDumpsUpload()V

    .line 41
    invoke-virtual {p0}, Lru/inetra/bytefog/service/ServiceProxy;->startService()V

    .line 42
    return-void
.end method

.method static synthetic access$000(Lru/inetra/bytefog/service/ServiceProxy;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lru/inetra/bytefog/service/ServiceProxy;

    .prologue
    .line 19
    iget-object v0, p0, Lru/inetra/bytefog/service/ServiceProxy;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lru/inetra/bytefog/service/ServiceProxy;)Lru/inetra/bytefog/service/IService;
    .locals 1
    .param p0, "x0"    # Lru/inetra/bytefog/service/ServiceProxy;

    .prologue
    .line 19
    iget-object v0, p0, Lru/inetra/bytefog/service/ServiceProxy;->mService:Lru/inetra/bytefog/service/IService;

    return-object v0
.end method

.method static synthetic access$102(Lru/inetra/bytefog/service/ServiceProxy;Lru/inetra/bytefog/service/IService;)Lru/inetra/bytefog/service/IService;
    .locals 0
    .param p0, "x0"    # Lru/inetra/bytefog/service/ServiceProxy;
    .param p1, "x1"    # Lru/inetra/bytefog/service/IService;

    .prologue
    .line 19
    iput-object p1, p0, Lru/inetra/bytefog/service/ServiceProxy;->mService:Lru/inetra/bytefog/service/IService;

    return-object p1
.end method

.method static synthetic access$202(Lru/inetra/bytefog/service/ServiceProxy;Z)Z
    .locals 0
    .param p0, "x0"    # Lru/inetra/bytefog/service/ServiceProxy;
    .param p1, "x1"    # Z

    .prologue
    .line 19
    iput-boolean p1, p0, Lru/inetra/bytefog/service/ServiceProxy;->isStarting:Z

    return p1
.end method

.method static synthetic access$300(Lru/inetra/bytefog/service/ServiceProxy;)Lru/inetra/bytefog/service/InstanceListener;
    .locals 1
    .param p0, "x0"    # Lru/inetra/bytefog/service/ServiceProxy;

    .prologue
    .line 19
    iget-object v0, p0, Lru/inetra/bytefog/service/ServiceProxy;->listener:Lru/inetra/bytefog/service/InstanceListener;

    return-object v0
.end method

.method static synthetic access$400(Lru/inetra/bytefog/service/ServiceProxy;)V
    .locals 0
    .param p0, "x0"    # Lru/inetra/bytefog/service/ServiceProxy;

    .prologue
    .line 19
    invoke-direct {p0}, Lru/inetra/bytefog/service/ServiceProxy;->scheduleServiceRestart()V

    return-void
.end method

.method private scheduleServiceRestart()V
    .locals 2

    .prologue
    .line 103
    const-string v0, "ServiceProxy"

    const-string v1, "Scheduling bytefog service restart..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    iget-object v0, p0, Lru/inetra/bytefog/service/ServiceProxy;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lru/inetra/bytefog/service/ServiceProxy;->restartServiceRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 105
    iget-object v0, p0, Lru/inetra/bytefog/service/ServiceProxy;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lru/inetra/bytefog/service/ServiceProxy;->restartServiceRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 106
    return-void
.end method


# virtual methods
.method public cancelAllRequests()V
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lru/inetra/bytefog/service/ServiceProxy;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    :try_start_0
    iget-object v0, p0, Lru/inetra/bytefog/service/ServiceProxy;->mService:Lru/inetra/bytefog/service/IService;

    invoke-interface {v0}, Lru/inetra/bytefog/service/IService;->cancelAllRequests()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 96
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public convertBytefogMagnetToHlsPlaylistUri(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "magnet"    # Ljava/lang/String;

    .prologue
    .line 69
    :try_start_0
    iget-object v1, p0, Lru/inetra/bytefog/service/ServiceProxy;->mService:Lru/inetra/bytefog/service/IService;

    invoke-interface {v1, p1}, Lru/inetra/bytefog/service/IService;->convertBytefogMagnetToHlsPlaylistUri(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 72
    :goto_0
    return-object v1

    .line 70
    :catch_0
    move-exception v0

    .line 71
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "ServiceProxy"

    const-string v2, "Magnet conversion error"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 72
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isReady()Z
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lru/inetra/bytefog/service/ServiceProxy;->mService:Lru/inetra/bytefog/service/IService;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStarting()Z
    .locals 1

    .prologue
    .line 83
    iget-boolean v0, p0, Lru/inetra/bytefog/service/ServiceProxy;->isStarting:Z

    return v0
.end method

.method public scheduleCrashDumpsUpload()V
    .locals 3

    .prologue
    .line 46
    iget-object v2, p0, Lru/inetra/bytefog/service/ServiceProxy;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-object v0, v2, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    .line 47
    .local v0, "crashDumpPath":Ljava/lang/String;
    new-instance v1, Lru/inetra/bytefog/service/CrashDumpUploader;

    iget-object v2, p0, Lru/inetra/bytefog/service/ServiceProxy;->bytefogConfig:Lru/inetra/bytefog/service/BytefogConfig;

    invoke-virtual {v2}, Lru/inetra/bytefog/service/BytefogConfig;->getUid()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lru/inetra/bytefog/service/CrashDumpUploader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .local v1, "uploader":Lru/inetra/bytefog/service/CrashDumpUploader;
    iget-object v2, p0, Lru/inetra/bytefog/service/ServiceProxy;->crashDumpUpdloadExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v2, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 49
    return-void
.end method

.method public setListener(Lru/inetra/bytefog/service/InstanceListener;)V
    .locals 0
    .param p1, "listener"    # Lru/inetra/bytefog/service/InstanceListener;

    .prologue
    .line 62
    iput-object p1, p0, Lru/inetra/bytefog/service/ServiceProxy;->listener:Lru/inetra/bytefog/service/InstanceListener;

    .line 63
    return-void
.end method

.method public start()V
    .locals 0

    .prologue
    .line 88
    invoke-virtual {p0}, Lru/inetra/bytefog/service/ServiceProxy;->startService()V

    .line 89
    return-void
.end method

.method public startService()V
    .locals 4

    .prologue
    .line 53
    invoke-virtual {p0}, Lru/inetra/bytefog/service/ServiceProxy;->isReady()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lru/inetra/bytefog/service/ServiceProxy;->isStarting:Z

    if-nez v1, :cond_0

    .line 54
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lru/inetra/bytefog/service/ServiceProxy;->context:Landroid/content/Context;

    const-class v2, Lru/inetra/bytefog/service/BytefogService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 55
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "EXTRA_UID"

    iget-object v2, p0, Lru/inetra/bytefog/service/ServiceProxy;->bytefogConfig:Lru/inetra/bytefog/service/BytefogConfig;

    invoke-virtual {v2}, Lru/inetra/bytefog/service/BytefogConfig;->getUid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    iget-object v1, p0, Lru/inetra/bytefog/service/ServiceProxy;->context:Landroid/content/Context;

    iget-object v2, p0, Lru/inetra/bytefog/service/ServiceProxy;->mConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 58
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
