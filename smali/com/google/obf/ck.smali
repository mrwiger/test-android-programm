.class final Lcom/google/obf/ck;
.super Ljava/lang/Object;
.source "IMASDK"


# instance fields
.field private final a:Lcom/google/obf/ar;


# direct methods
.method public constructor <init>(Lcom/google/obf/ar;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/obf/ck;->a:Lcom/google/obf/ar;

    .line 3
    const-string v2, "application/eia-608"

    const/4 v3, -0x1

    const-wide/16 v4, -0x1

    move-object v6, v1

    invoke-static/range {v1 .. v6}, Lcom/google/obf/q;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)Lcom/google/obf/q;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/obf/ar;->a(Lcom/google/obf/q;)V

    .line 4
    return-void
.end method


# virtual methods
.method public a(JLcom/google/obf/dw;)V
    .locals 9

    .prologue
    const/16 v8, 0xff

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 5
    :goto_0
    invoke-virtual {p3}, Lcom/google/obf/dw;->b()I

    move-result v0

    if-le v0, v4, :cond_3

    move v0, v6

    .line 7
    :cond_0
    invoke-virtual {p3}, Lcom/google/obf/dw;->f()I

    move-result v1

    .line 8
    add-int/2addr v0, v1

    .line 9
    if-eq v1, v8, :cond_0

    move v5, v6

    .line 11
    :cond_1
    invoke-virtual {p3}, Lcom/google/obf/dw;->f()I

    move-result v1

    .line 12
    add-int/2addr v5, v1

    .line 13
    if-eq v1, v8, :cond_1

    .line 14
    invoke-static {v0, v5, p3}, Lcom/google/obf/cv;->a(IILcom/google/obf/dw;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 15
    iget-object v0, p0, Lcom/google/obf/ck;->a:Lcom/google/obf/ar;

    invoke-interface {v0, p3, v5}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 16
    iget-object v1, p0, Lcom/google/obf/ck;->a:Lcom/google/obf/ar;

    const/4 v7, 0x0

    move-wide v2, p1

    invoke-interface/range {v1 .. v7}, Lcom/google/obf/ar;->a(JIII[B)V

    goto :goto_0

    .line 17
    :cond_2
    invoke-virtual {p3, v5}, Lcom/google/obf/dw;->d(I)V

    goto :goto_0

    .line 19
    :cond_3
    return-void
.end method
