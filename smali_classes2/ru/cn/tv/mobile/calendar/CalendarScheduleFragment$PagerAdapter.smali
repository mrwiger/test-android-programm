.class final Lru/cn/tv/mobile/calendar/CalendarScheduleFragment$PagerAdapter;
.super Lru/cn/tv/mobile/calendar/FragmentStatePagerAdapterEx;
.source "CalendarScheduleFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PagerAdapter"
.end annotation


# instance fields
.field private dates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Calendar;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;


# direct methods
.method constructor <init>(Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;Landroid/support/v4/app/FragmentManager;)V
    .locals 1
    .param p2, "fm"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 157
    iput-object p1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment$PagerAdapter;->this$0:Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;

    .line 158
    invoke-direct {p0, p2}, Lru/cn/tv/mobile/calendar/FragmentStatePagerAdapterEx;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 155
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment$PagerAdapter;->dates:Ljava/util/List;

    .line 159
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment$PagerAdapter;->dates:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 4
    .param p1, "i"    # I

    .prologue
    .line 163
    iget-object v1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment$PagerAdapter;->this$0:Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;

    invoke-static {v1}, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->access$100(Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;)J

    move-result-wide v2

    iget-object v1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment$PagerAdapter;->dates:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Calendar;

    invoke-static {v2, v3, v1}, Lru/cn/tv/mobile/schedule/ScheduleFragment;->newInstance(JLjava/util/Calendar;)Lru/cn/tv/mobile/schedule/ScheduleFragment;

    move-result-object v0

    .line 164
    .local v0, "fragment":Lru/cn/tv/mobile/schedule/ScheduleFragment;
    iget-object v1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment$PagerAdapter;->this$0:Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;

    invoke-static {v1}, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->access$000(Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;)Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/schedule/ScheduleFragment;->setListener(Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;)V

    .line 166
    return-object v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 199
    const/4 v0, -0x2

    return v0
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .locals 9
    .param p1, "position"    # I

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 181
    iget-object v4, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment$PagerAdapter;->dates:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 182
    .local v0, "c":Ljava/util/Calendar;
    invoke-static {}, Lru/cn/utils/Utils;->getCalendar()Ljava/util/Calendar;

    move-result-object v2

    .line 183
    .local v2, "now":Ljava/util/Calendar;
    const/4 v4, 0x7

    .line 184
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    .line 183
    invoke-virtual {v0, v4, v6, v5}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 185
    .local v1, "dayOfWeek":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "d.MM "

    invoke-static {v0, v5}, Lru/cn/utils/Utils;->format(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 187
    .local v3, "text":Ljava/lang/String;
    invoke-virtual {v2, v6}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 188
    invoke-virtual {v2, v7}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 189
    invoke-virtual {v2, v8}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 190
    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 191
    iget-object v4, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment$PagerAdapter;->this$0:Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;

    const v5, 0x7f0e008a

    invoke-virtual {v4, v5}, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 194
    :cond_0
    return-object v3
.end method

.method setDates(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/Calendar;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 175
    .local p1, "dates":Ljava/util/List;, "Ljava/util/List<Ljava/util/Calendar;>;"
    iput-object p1, p0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment$PagerAdapter;->dates:Ljava/util/List;

    .line 176
    invoke-virtual {p0}, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment$PagerAdapter;->notifyDataSetChanged()V

    .line 177
    return-void
.end method
