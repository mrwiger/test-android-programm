.class public Lcom/my/target/bz;
.super Landroid/webkit/WebView;
.source "MraidWebView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/bz$b;,
        Lcom/my/target/bz$a;
    }
.end annotation


# instance fields
.field private iP:Lcom/my/target/bz$a;

.field private iQ:Z

.field private iR:Z

.field private orientation:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 29
    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 30
    invoke-virtual {p0}, Lcom/my/target/bz;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/my/target/bz;->iQ:Z

    .line 31
    invoke-virtual {p0}, Lcom/my/target/bz;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 32
    invoke-virtual {p0}, Lcom/my/target/bz;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 33
    invoke-virtual {p0}, Lcom/my/target/bz;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    .line 34
    invoke-virtual {p0}, Lcom/my/target/bz;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {p0}, Lcom/my/target/bz;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setAppCachePath(Ljava/lang/String;)V

    .line 36
    invoke-virtual {p0}, Lcom/my/target/bz;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 37
    invoke-virtual {p0}, Lcom/my/target/bz;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setAllowContentAccess(Z)V

    .line 38
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 40
    invoke-virtual {p0}, Lcom/my/target/bz;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setAllowFileAccessFromFileURLs(Z)V

    .line 41
    invoke-virtual {p0}, Lcom/my/target/bz;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setAllowUniversalAccessFromFileURLs(Z)V

    .line 44
    :cond_0
    new-instance v0, Lcom/my/target/bz$b;

    invoke-virtual {p0}, Lcom/my/target/bz;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/my/target/bz$b;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 45
    new-instance v1, Lcom/my/target/bz$1;

    invoke-direct {v1, p0}, Lcom/my/target/bz$1;-><init>(Lcom/my/target/bz;)V

    invoke-virtual {v0, v1}, Lcom/my/target/bz$b;->a(Lcom/my/target/bz$b$a;)V

    .line 53
    new-instance v1, Lcom/my/target/bz$2;

    invoke-direct {v1, p0, v0}, Lcom/my/target/bz$2;-><init>(Lcom/my/target/bz;Lcom/my/target/bz$b;)V

    invoke-virtual {p0, v1}, Lcom/my/target/bz;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 63
    return-void

    :cond_1
    move v0, v2

    .line 30
    goto :goto_0
.end method

.method static synthetic a(Lcom/my/target/bz;Z)Z
    .locals 0

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/my/target/bz;->iR:Z

    return p1
.end method

.method private d(II)V
    .locals 2

    .prologue
    .line 123
    int-to-float v0, p1

    int-to-float v1, p2

    div-float/2addr v0, v1

    .line 126
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 128
    const/4 v0, 0x2

    .line 135
    :goto_0
    iget v1, p0, Lcom/my/target/bz;->orientation:I

    if-eq v0, v1, :cond_0

    .line 137
    iput v0, p0, Lcom/my/target/bz;->orientation:I

    .line 138
    iget-object v0, p0, Lcom/my/target/bz;->iP:Lcom/my/target/bz$a;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/my/target/bz;->iP:Lcom/my/target/bz$a;

    invoke-interface {v0}, Lcom/my/target/bz$a;->q()V

    .line 143
    :cond_0
    return-void

    .line 132
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public bd()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/my/target/bz;->iR:Z

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/my/target/bz;->iQ:Z

    return v0
.end method

.method public j(Z)V
    .locals 2

    .prologue
    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MraidWebView: pause, finishing "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 73
    if-eqz p1, :cond_0

    .line 75
    invoke-virtual {p0}, Lcom/my/target/bz;->stopLoading()V

    .line 76
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/my/target/bz;->loadUrl(Ljava/lang/String;)V

    .line 79
    :cond_0
    invoke-virtual {p0}, Lcom/my/target/bz;->onPause()V

    .line 80
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 101
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 102
    invoke-direct {p0, v0, v1}, Lcom/my/target/bz;->d(II)V

    .line 103
    invoke-super {p0, p1, p2}, Landroid/webkit/WebView;->onMeasure(II)V

    .line 104
    return-void
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 2
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 109
    invoke-super {p0, p1, p2}, Landroid/webkit/WebView;->onVisibilityChanged(Landroid/view/View;I)V

    .line 110
    if-nez p2, :cond_1

    const/4 v0, 0x1

    .line 111
    :goto_0
    iget-boolean v1, p0, Lcom/my/target/bz;->iQ:Z

    if-eq v0, v1, :cond_0

    .line 113
    iput-boolean v0, p0, Lcom/my/target/bz;->iQ:Z

    .line 114
    iget-object v0, p0, Lcom/my/target/bz;->iP:Lcom/my/target/bz$a;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/my/target/bz;->iP:Lcom/my/target/bz$a;

    iget-boolean v1, p0, Lcom/my/target/bz;->iQ:Z

    invoke-interface {v0, v1}, Lcom/my/target/bz$a;->onVisibilityChanged(Z)V

    .line 119
    :cond_0
    return-void

    .line 110
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setClicked(Z)V
    .locals 0
    .param p1, "clicked"    # Z

    .prologue
    .line 95
    iput-boolean p1, p0, Lcom/my/target/bz;->iR:Z

    .line 96
    return-void
.end method

.method public setVisibilityChangedListener(Lcom/my/target/bz$a;)V
    .locals 0
    .param p1, "listener"    # Lcom/my/target/bz$a;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/my/target/bz;->iP:Lcom/my/target/bz$a;

    .line 85
    return-void
.end method
