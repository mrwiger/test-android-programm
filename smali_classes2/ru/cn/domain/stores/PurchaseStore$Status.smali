.class public final enum Lru/cn/domain/stores/PurchaseStore$Status;
.super Ljava/lang/Enum;
.source "PurchaseStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/domain/stores/PurchaseStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/domain/stores/PurchaseStore$Status;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/domain/stores/PurchaseStore$Status;

.field public static final enum AVAILABLE:Lru/cn/domain/stores/PurchaseStore$Status;

.field public static final enum NOT_AVAILABLE:Lru/cn/domain/stores/PurchaseStore$Status;

.field public static final enum UNKNOWN:Lru/cn/domain/stores/PurchaseStore$Status;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18
    new-instance v0, Lru/cn/domain/stores/PurchaseStore$Status;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Lru/cn/domain/stores/PurchaseStore$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/domain/stores/PurchaseStore$Status;->UNKNOWN:Lru/cn/domain/stores/PurchaseStore$Status;

    .line 19
    new-instance v0, Lru/cn/domain/stores/PurchaseStore$Status;

    const-string v1, "AVAILABLE"

    invoke-direct {v0, v1, v3}, Lru/cn/domain/stores/PurchaseStore$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/domain/stores/PurchaseStore$Status;->AVAILABLE:Lru/cn/domain/stores/PurchaseStore$Status;

    .line 20
    new-instance v0, Lru/cn/domain/stores/PurchaseStore$Status;

    const-string v1, "NOT_AVAILABLE"

    invoke-direct {v0, v1, v4}, Lru/cn/domain/stores/PurchaseStore$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/domain/stores/PurchaseStore$Status;->NOT_AVAILABLE:Lru/cn/domain/stores/PurchaseStore$Status;

    .line 17
    const/4 v0, 0x3

    new-array v0, v0, [Lru/cn/domain/stores/PurchaseStore$Status;

    sget-object v1, Lru/cn/domain/stores/PurchaseStore$Status;->UNKNOWN:Lru/cn/domain/stores/PurchaseStore$Status;

    aput-object v1, v0, v2

    sget-object v1, Lru/cn/domain/stores/PurchaseStore$Status;->AVAILABLE:Lru/cn/domain/stores/PurchaseStore$Status;

    aput-object v1, v0, v3

    sget-object v1, Lru/cn/domain/stores/PurchaseStore$Status;->NOT_AVAILABLE:Lru/cn/domain/stores/PurchaseStore$Status;

    aput-object v1, v0, v4

    sput-object v0, Lru/cn/domain/stores/PurchaseStore$Status;->$VALUES:[Lru/cn/domain/stores/PurchaseStore$Status;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/domain/stores/PurchaseStore$Status;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    const-class v0, Lru/cn/domain/stores/PurchaseStore$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/domain/stores/PurchaseStore$Status;

    return-object v0
.end method

.method public static values()[Lru/cn/domain/stores/PurchaseStore$Status;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lru/cn/domain/stores/PurchaseStore$Status;->$VALUES:[Lru/cn/domain/stores/PurchaseStore$Status;

    invoke-virtual {v0}, [Lru/cn/domain/stores/PurchaseStore$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/domain/stores/PurchaseStore$Status;

    return-object v0
.end method
