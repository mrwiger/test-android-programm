.class public Lru/cn/tv/player/controller/PanelAnimator;
.super Ljava/lang/Object;
.source "PanelAnimator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;
    }
.end annotation


# instance fields
.field private animationAlphaAppearance:Landroid/view/animation/Animation;

.field private animationAlphaDisappear:Landroid/view/animation/Animation;

.field private animationTranslateDown:Landroid/view/animation/Animation;

.field private animationTranslateUp:Landroid/view/animation/Animation;

.field private animationTranslateUpLastPanel:Landroid/view/animation/Animation;

.field private panelIndex:I

.field private panelTitleView:Landroid/widget/TextView;

.field private panels:[Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/TextView;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "panelTitleView"    # Landroid/widget/TextView;

    .prologue
    const/4 v1, 0x1

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p2, p0, Lru/cn/tv/player/controller/PanelAnimator;->panelTitleView:Landroid/widget/TextView;

    .line 36
    const v0, 0x7f010012

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/player/controller/PanelAnimator;->animationTranslateDown:Landroid/view/animation/Animation;

    .line 37
    iget-object v0, p0, Lru/cn/tv/player/controller/PanelAnimator;->animationTranslateDown:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 39
    const v0, 0x7f010013

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/player/controller/PanelAnimator;->animationTranslateUp:Landroid/view/animation/Animation;

    .line 40
    iget-object v0, p0, Lru/cn/tv/player/controller/PanelAnimator;->animationTranslateUp:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 42
    const v0, 0x7f010014

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/player/controller/PanelAnimator;->animationTranslateUpLastPanel:Landroid/view/animation/Animation;

    .line 43
    iget-object v0, p0, Lru/cn/tv/player/controller/PanelAnimator;->animationTranslateUpLastPanel:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 45
    const v0, 0x7f01000d

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/player/controller/PanelAnimator;->animationAlphaDisappear:Landroid/view/animation/Animation;

    .line 46
    iget-object v0, p0, Lru/cn/tv/player/controller/PanelAnimator;->animationAlphaDisappear:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 48
    const v0, 0x7f01000c

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/player/controller/PanelAnimator;->animationAlphaAppearance:Landroid/view/animation/Animation;

    .line 49
    iget-object v0, p0, Lru/cn/tv/player/controller/PanelAnimator;->animationAlphaAppearance:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 50
    return-void
.end method

.method private cancelAnimations()V
    .locals 5

    .prologue
    .line 140
    iget-object v1, p0, Lru/cn/tv/player/controller/PanelAnimator;->panels:[Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    if-nez v1, :cond_1

    .line 147
    :cond_0
    return-void

    .line 143
    :cond_1
    iget-object v1, p0, Lru/cn/tv/player/controller/PanelAnimator;->panelTitleView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->clearAnimation()V

    .line 144
    iget-object v2, p0, Lru/cn/tv/player/controller/PanelAnimator;->panels:[Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 145
    .local v0, "info":Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;
    iget-object v4, v0, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;->panel:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->clearAnimation()V

    .line 144
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private showNextPanelAnimated(Landroid/view/animation/Animation;)V
    .locals 6
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v4, 0x0

    .line 119
    iget-object v3, p0, Lru/cn/tv/player/controller/PanelAnimator;->panels:[Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    iget v5, p0, Lru/cn/tv/player/controller/PanelAnimator;->panelIndex:I

    aget-object v0, v3, v5

    .line 121
    .local v0, "next":Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;
    iget-object v2, v0, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;->title:Ljava/lang/String;

    .line 122
    .local v2, "title":Ljava/lang/String;
    iget-object v3, p0, Lru/cn/tv/player/controller/PanelAnimator;->panelTitleView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    iget-object v5, p0, Lru/cn/tv/player/controller/PanelAnimator;->panelTitleView:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    move v3, v4

    :goto_0
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 124
    iget-object v5, p0, Lru/cn/tv/player/controller/PanelAnimator;->panelTitleView:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    iget-object v3, p0, Lru/cn/tv/player/controller/PanelAnimator;->animationAlphaAppearance:Landroid/view/animation/Animation;

    :goto_1
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 127
    iget-object v3, v0, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;->panel:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 128
    iget-object v3, v0, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;->panel:Landroid/view/View;

    invoke-virtual {v3, p1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 130
    iget v3, p0, Lru/cn/tv/player/controller/PanelAnimator;->panelIndex:I

    add-int/lit8 v3, v3, 0x1

    iget-object v5, p0, Lru/cn/tv/player/controller/PanelAnimator;->panels:[Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    array-length v5, v5

    if-ge v3, v5, :cond_0

    .line 131
    iget-object v3, p0, Lru/cn/tv/player/controller/PanelAnimator;->panels:[Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    iget v5, p0, Lru/cn/tv/player/controller/PanelAnimator;->panelIndex:I

    add-int/lit8 v5, v5, 0x1

    aget-object v1, v3, v5

    .line 132
    .local v1, "overNext":Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;
    iget-object v3, v1, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;->panel:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 133
    iget-object v3, v1, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;->panel:Landroid/view/View;

    iget-object v4, p0, Lru/cn/tv/player/controller/PanelAnimator;->animationAlphaAppearance:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 136
    .end local v1    # "overNext":Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;
    :cond_0
    iget-object v3, v0, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;->panel:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    .line 137
    return-void

    .line 123
    :cond_1
    const/16 v3, 0x8

    goto :goto_0

    .line 124
    :cond_2
    iget-object v3, p0, Lru/cn/tv/player/controller/PanelAnimator;->animationAlphaDisappear:Landroid/view/animation/Animation;

    goto :goto_1
.end method


# virtual methods
.method public down()Z
    .locals 3

    .prologue
    .line 102
    invoke-direct {p0}, Lru/cn/tv/player/controller/PanelAnimator;->cancelAnimations()V

    .line 104
    iget-object v1, p0, Lru/cn/tv/player/controller/PanelAnimator;->panels:[Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    iget v2, p0, Lru/cn/tv/player/controller/PanelAnimator;->panelIndex:I

    aget-object v0, v1, v2

    .line 105
    .local v0, "current":Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;
    iget-object v1, v0, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;->panel:Landroid/view/View;

    iget-object v2, p0, Lru/cn/tv/player/controller/PanelAnimator;->animationAlphaDisappear:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 106
    iget-object v1, v0, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;->panel:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 108
    iget v1, p0, Lru/cn/tv/player/controller/PanelAnimator;->panelIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lru/cn/tv/player/controller/PanelAnimator;->panelIndex:I

    .line 109
    iget v1, p0, Lru/cn/tv/player/controller/PanelAnimator;->panelIndex:I

    iget-object v2, p0, Lru/cn/tv/player/controller/PanelAnimator;->panels:[Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 110
    const/4 v1, 0x0

    iput v1, p0, Lru/cn/tv/player/controller/PanelAnimator;->panelIndex:I

    .line 113
    :cond_0
    iget v1, p0, Lru/cn/tv/player/controller/PanelAnimator;->panelIndex:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lru/cn/tv/player/controller/PanelAnimator;->panels:[Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    array-length v2, v2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lru/cn/tv/player/controller/PanelAnimator;->animationTranslateUpLastPanel:Landroid/view/animation/Animation;

    :goto_0
    invoke-direct {p0, v1}, Lru/cn/tv/player/controller/PanelAnimator;->showNextPanelAnimated(Landroid/view/animation/Animation;)V

    .line 115
    const/4 v1, 0x1

    return v1

    .line 113
    :cond_1
    iget-object v1, p0, Lru/cn/tv/player/controller/PanelAnimator;->animationTranslateUp:Landroid/view/animation/Animation;

    goto :goto_0
.end method

.method public reset()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 64
    iput v4, p0, Lru/cn/tv/player/controller/PanelAnimator;->panelIndex:I

    .line 66
    invoke-direct {p0}, Lru/cn/tv/player/controller/PanelAnimator;->cancelAnimations()V

    .line 68
    iget-object v3, p0, Lru/cn/tv/player/controller/PanelAnimator;->panelTitleView:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 69
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    iget-object v3, p0, Lru/cn/tv/player/controller/PanelAnimator;->panels:[Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 70
    iget-object v3, p0, Lru/cn/tv/player/controller/PanelAnimator;->panels:[Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    aget-object v2, v3, v1

    .line 71
    .local v2, "info":Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;
    iget v3, p0, Lru/cn/tv/player/controller/PanelAnimator;->panelIndex:I

    if-eq v1, v3, :cond_0

    iget v3, p0, Lru/cn/tv/player/controller/PanelAnimator;->panelIndex:I

    add-int/lit8 v3, v3, 0x1

    if-ne v1, v3, :cond_1

    .line 72
    :cond_0
    iget-object v3, v2, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;->panel:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 69
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 74
    :cond_1
    iget-object v3, v2, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;->panel:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 80
    .end local v2    # "info":Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;
    :cond_2
    iget-object v3, p0, Lru/cn/tv/player/controller/PanelAnimator;->panels:[Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    iget v4, p0, Lru/cn/tv/player/controller/PanelAnimator;->panelIndex:I

    aget-object v0, v3, v4

    .line 81
    .local v0, "current":Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;
    iget-object v3, v0, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;->panel:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    .line 82
    return-void
.end method

.method public varargs setPanels([Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;)V
    .locals 6
    .param p1, "panels"    # [Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    .prologue
    .line 53
    invoke-direct {p0}, Lru/cn/tv/player/controller/PanelAnimator;->cancelAnimations()V

    .line 54
    iget-object v1, p0, Lru/cn/tv/player/controller/PanelAnimator;->panels:[Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    if-eqz v1, :cond_0

    .line 55
    iget-object v2, p0, Lru/cn/tv/player/controller/PanelAnimator;->panels:[Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 56
    .local v0, "info":Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;
    iget-object v4, v0, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;->panel:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 55
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 60
    .end local v0    # "info":Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;
    :cond_0
    iput-object p1, p0, Lru/cn/tv/player/controller/PanelAnimator;->panels:[Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    .line 61
    return-void
.end method

.method public up()Z
    .locals 3

    .prologue
    .line 85
    iget v1, p0, Lru/cn/tv/player/controller/PanelAnimator;->panelIndex:I

    if-nez v1, :cond_0

    .line 86
    const/4 v1, 0x0

    .line 98
    :goto_0
    return v1

    .line 88
    :cond_0
    invoke-direct {p0}, Lru/cn/tv/player/controller/PanelAnimator;->cancelAnimations()V

    .line 90
    iget-object v1, p0, Lru/cn/tv/player/controller/PanelAnimator;->panels:[Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;

    iget v2, p0, Lru/cn/tv/player/controller/PanelAnimator;->panelIndex:I

    aget-object v0, v1, v2

    .line 91
    .local v0, "current":Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;
    iget-object v1, v0, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;->panel:Landroid/view/View;

    iget-object v2, p0, Lru/cn/tv/player/controller/PanelAnimator;->animationAlphaDisappear:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 92
    iget-object v1, v0, Lru/cn/tv/player/controller/PanelAnimator$PanelInfo;->panel:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 94
    iget v1, p0, Lru/cn/tv/player/controller/PanelAnimator;->panelIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lru/cn/tv/player/controller/PanelAnimator;->panelIndex:I

    .line 96
    iget-object v1, p0, Lru/cn/tv/player/controller/PanelAnimator;->animationTranslateDown:Landroid/view/animation/Animation;

    invoke-direct {p0, v1}, Lru/cn/tv/player/controller/PanelAnimator;->showNextPanelAnimated(Landroid/view/animation/Animation;)V

    .line 98
    const/4 v1, 0x1

    goto :goto_0
.end method
