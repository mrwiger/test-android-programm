.class public final Lcom/my/target/ev;
.super Lcom/my/target/et;
.source "RecyclerTabletView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/ev$b;,
        Lcom/my/target/ev$a;
    }
.end annotation


# instance fields
.field private final dm:Landroid/view/View$OnClickListener;

.field private final dy:Lcom/my/target/eo$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/my/target/ev;-><init>(Landroid/content/Context;B)V

    .line 51
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/my/target/ev;-><init>(Landroid/content/Context;C)V

    .line 56
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;C)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/my/target/et;-><init>(Landroid/content/Context;)V

    .line 29
    new-instance v0, Lcom/my/target/ev$1;

    invoke-direct {v0, p0}, Lcom/my/target/ev$1;-><init>(Lcom/my/target/ev;)V

    iput-object v0, p0, Lcom/my/target/ev;->dm:Landroid/view/View$OnClickListener;

    .line 61
    new-instance v0, Lcom/my/target/eo$a;

    invoke-direct {v0, p1}, Lcom/my/target/eo$a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ev;->dy:Lcom/my/target/eo$a;

    .line 63
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 64
    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 89
    return-void
.end method

.method public final c(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    iput-object p1, p0, Lcom/my/target/ev;->dp:Ljava/util/List;

    .line 75
    new-instance v0, Lcom/my/target/ev$b;

    .line 76
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/my/target/ev$b;-><init>(Ljava/util/List;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ev;->dr:Lcom/my/target/er;

    .line 77
    iget-object v0, p0, Lcom/my/target/ev;->dr:Lcom/my/target/er;

    iget-object v1, p0, Lcom/my/target/ev;->dl:Landroid/view/View$OnClickListener;

    .line 1069
    iput-object v1, v0, Lcom/my/target/er;->dl:Landroid/view/View$OnClickListener;

    .line 78
    iget-object v0, p0, Lcom/my/target/ev;->dr:Lcom/my/target/er;

    iget-object v1, p0, Lcom/my/target/ev;->dm:Landroid/view/View$OnClickListener;

    .line 2058
    iput-object v1, v0, Lcom/my/target/er;->dm:Landroid/view/View$OnClickListener;

    .line 79
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    .line 80
    iget-object v1, p0, Lcom/my/target/ev;->dy:Lcom/my/target/eo$a;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/my/target/cm;->n(I)I

    move-result v0

    .line 3032
    iput v0, v1, Lcom/my/target/eq;->dg:I

    .line 81
    iget-object v0, p0, Lcom/my/target/ev;->dy:Lcom/my/target/eo$a;

    invoke-virtual {p0, v0}, Lcom/my/target/ev;->setCardLayoutManager(Lcom/my/target/es;)V

    .line 82
    iget-object v0, p0, Lcom/my/target/ev;->dr:Lcom/my/target/er;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 83
    return-void
.end method

.method protected final getCardLayoutManager()Lcom/my/target/eq;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/my/target/ev;->dy:Lcom/my/target/eo$a;

    return-object v0
.end method

.method protected final bridge synthetic getCardLayoutManager()Lcom/my/target/es;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/my/target/ev;->getCardLayoutManager()Lcom/my/target/eq;

    move-result-object v0

    return-object v0
.end method

.method public final setSideSlidesMargins(I)V
    .locals 1
    .param p1, "sideSlidesMargins"    # I

    .prologue
    .line 68
    iget-object v0, p0, Lcom/my/target/ev;->dy:Lcom/my/target/eo$a;

    .line 1038
    iput p1, v0, Lcom/my/target/es;->dn:I

    .line 69
    return-void
.end method
