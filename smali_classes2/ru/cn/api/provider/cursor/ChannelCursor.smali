.class public Lru/cn/api/provider/cursor/ChannelCursor;
.super Lru/cn/api/provider/cursor/MatrixCursorEx;
.source "ChannelCursor.java"


# static fields
.field private static columnNames:[Ljava/lang/String;


# instance fields
.field private final allowPurchase_Index:I

.field private final cn_idIndex:I

.field private final contractorId_Index:I

.field private final currentTelecastId_Index:I

.field private final favouriteIndex:I

.field private final has_scheduleIndex:I

.field private final imageIndex:I

.field private final is_deniedIndex:I

.field private final is_intersectionsIndex:I

.field private final is_pornoIndex:I

.field private final numberIndex:I

.field private final recordableIndex:I

.field private final territoryIdIndex:I

.field private final titleIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 10
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "cn_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "image"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "number"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "has_schedule"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "favourite"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "recordable"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "is_denied"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "territory_id"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "current_telecast_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "is_porno"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "is_intersections"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "contractor_id"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "allow_purchase"

    aput-object v2, v0, v1

    sput-object v0, Lru/cn/api/provider/cursor/ChannelCursor;->columnNames:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lru/cn/api/provider/cursor/ChannelCursor;->columnNames:[Ljava/lang/String;

    invoke-direct {p0, v0}, Lru/cn/api/provider/cursor/MatrixCursorEx;-><init>([Ljava/lang/String;)V

    .line 32
    const-string v0, "cn_id"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->cn_idIndex:I

    .line 33
    const-string v0, "title"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->titleIndex:I

    .line 34
    const-string v0, "image"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->imageIndex:I

    .line 35
    const-string v0, "number"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->numberIndex:I

    .line 36
    const-string v0, "has_schedule"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->has_scheduleIndex:I

    .line 37
    const-string v0, "favourite"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->favouriteIndex:I

    .line 38
    const-string v0, "recordable"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->recordableIndex:I

    .line 39
    const-string v0, "is_denied"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->is_deniedIndex:I

    .line 40
    const-string v0, "territory_id"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->territoryIdIndex:I

    .line 41
    const-string v0, "current_telecast_id"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->currentTelecastId_Index:I

    .line 42
    const-string v0, "is_porno"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->is_pornoIndex:I

    .line 43
    const-string v0, "is_intersections"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->is_intersectionsIndex:I

    .line 44
    const-string v0, "contractor_id"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->contractorId_Index:I

    .line 45
    const-string v0, "allow_purchase"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->allowPurchase_Index:I

    .line 46
    return-void
.end method


# virtual methods
.method public addRow(JLjava/lang/String;Ljava/lang/String;IIIIIJJIIJZ)V
    .locals 4
    .param p1, "cnId"    # J
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "image"    # Ljava/lang/String;
    .param p5, "number"    # I
    .param p6, "hasSchedule"    # I
    .param p7, "favourite"    # I
    .param p8, "recordable"    # I
    .param p9, "isDenied"    # I
    .param p10, "territoryId"    # J
    .param p12, "currentTelecastId"    # J
    .param p14, "isPorno"    # I
    .param p15, "isIntersections"    # I
    .param p16, "contractorId"    # J
    .param p18, "allowPurchase"    # Z

    .prologue
    .line 51
    const/16 v0, 0xf

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x2

    aput-object p3, v1, v0

    const/4 v0, 0x3

    aput-object p4, v1, v0

    const/4 v0, 0x4

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x5

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x6

    .line 52
    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x7

    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    const/16 v0, 0x8

    invoke-static {p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    const/16 v0, 0x9

    invoke-static {p10, p11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    const/16 v0, 0xa

    invoke-static/range {p12 .. p13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    const/16 v0, 0xb

    invoke-static/range {p14 .. p14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    const/16 v0, 0xc

    .line 53
    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    const/16 v0, 0xd

    invoke-static/range {p16 .. p17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    const/16 v2, 0xe

    if-eqz p18, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v2

    .line 51
    invoke-virtual {p0, v1}, Lru/cn/api/provider/cursor/ChannelCursor;->addRow([Ljava/lang/Object;)V

    .line 54
    return-void

    .line 53
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addRow(Lru/cn/api/provider/Channel;Lru/cn/api/tv/replies/ChannelInfo;Lru/cn/api/tv/replies/Telecast;ZZ)V
    .locals 26
    .param p1, "channel"    # Lru/cn/api/provider/Channel;
    .param p2, "info"    # Lru/cn/api/tv/replies/ChannelInfo;
    .param p3, "currentTelecast"    # Lru/cn/api/tv/replies/Telecast;
    .param p4, "isFavourite"    # Z
    .param p5, "allowPurchase"    # Z

    .prologue
    .line 111
    if-nez p1, :cond_0

    .line 163
    :goto_0
    return-void

    .line 115
    :cond_0
    move-object/from16 v0, p1

    iget-object v2, v0, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lru/cn/api/iptv/replies/MediaLocation;

    .line 117
    .local v21, "defaultLocation":Lru/cn/api/iptv/replies/MediaLocation;
    const/4 v8, 0x0

    .line 118
    .local v8, "hasSchedule":I
    const/4 v10, 0x0

    .line 119
    .local v10, "recordable":I
    const/4 v11, 0x0

    .line 120
    .local v11, "isDenied":I
    const/16 v16, 0x0

    .line 121
    .local v16, "isPornoChannel":I
    const/16 v17, 0x0

    .line 122
    .local v17, "isIntersectionChannel":I
    move-object/from16 v0, v21

    iget-wide v0, v0, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    move-wide/from16 v18, v0

    .line 124
    .local v18, "contractorId":J
    move-object/from16 v0, p1

    iget-object v5, v0, Lru/cn/api/provider/Channel;->title:Ljava/lang/String;

    .line 125
    .local v5, "title":Ljava/lang/String;
    const/4 v6, 0x0

    .line 127
    .local v6, "image":Ljava/lang/String;
    move-object/from16 v0, v21

    iget-wide v12, v0, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    .line 128
    .local v12, "territoryId":J
    const-wide/16 v14, -0x1

    .line 130
    .local v14, "telecastId":J
    move-object/from16 v0, p1

    iget-wide v2, v0, Lru/cn/api/provider/Channel;->channelId:J

    const-wide/16 v24, 0x0

    cmp-long v2, v2, v24

    if-lez v2, :cond_4

    .line 131
    if-eqz p2, :cond_1

    .line 132
    move-object/from16 v0, p2

    iget-object v5, v0, Lru/cn/api/tv/replies/ChannelInfo;->title:Ljava/lang/String;

    .line 133
    move-object/from16 v0, p2

    iget v8, v0, Lru/cn/api/tv/replies/ChannelInfo;->hasSchedule:I

    .line 134
    move-object/from16 v0, p2

    iget-object v6, v0, Lru/cn/api/tv/replies/ChannelInfo;->logo:Ljava/lang/String;

    .line 137
    :cond_1
    if-eqz p3, :cond_2

    .line 138
    move-object/from16 v0, p3

    iget-wide v14, v0, Lru/cn/api/tv/replies/Telecast;->id:J

    .line 141
    :cond_2
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lru/cn/api/provider/Channel;->isPornoChannel:Z

    if-eqz v2, :cond_3

    .line 142
    const/16 v16, 0x1

    .line 145
    :cond_3
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lru/cn/api/provider/Channel;->isIntersectionChannel:Z

    if-eqz v2, :cond_4

    .line 146
    const/16 v17, 0x1

    .line 150
    :cond_4
    move-object/from16 v0, p1

    iget-object v2, v0, Lru/cn/api/provider/Channel;->locations:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lru/cn/api/iptv/replies/MediaLocation;

    .line 151
    .local v22, "l":Lru/cn/api/iptv/replies/MediaLocation;
    move-object/from16 v0, v22

    iget-boolean v3, v0, Lru/cn/api/iptv/replies/MediaLocation;->hasRecords:Z

    if-eqz v3, :cond_5

    .line 152
    const/4 v10, 0x1

    goto :goto_1

    .line 156
    .end local v22    # "l":Lru/cn/api/iptv/replies/MediaLocation;
    :cond_6
    move-object/from16 v0, v21

    iget-object v2, v0, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;

    sget-object v3, Lru/cn/api/iptv/replies/MediaLocation$Access;->denied:Lru/cn/api/iptv/replies/MediaLocation$Access;

    if-ne v2, v3, :cond_7

    .line 157
    const/4 v11, 0x1

    .line 160
    :cond_7
    move-object/from16 v0, p1

    iget-wide v3, v0, Lru/cn/api/provider/Channel;->channelId:J

    move-object/from16 v0, p1

    iget v7, v0, Lru/cn/api/provider/Channel;->number:I

    if-eqz p4, :cond_8

    const/4 v9, 0x1

    :goto_2
    move-object/from16 v2, p0

    move/from16 v20, p5

    invoke-virtual/range {v2 .. v20}, Lru/cn/api/provider/cursor/ChannelCursor;->addRow(JLjava/lang/String;Ljava/lang/String;IIIIIJJIIJZ)V

    goto/16 :goto_0

    :cond_8
    const/4 v9, 0x0

    goto :goto_2
.end method

.method public allowPurchase()Z
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->allowPurchase_Index:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getChannelId()J
    .locals 2

    .prologue
    .line 57
    iget v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->cn_idIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getContractorId()J
    .locals 2

    .prologue
    .line 103
    iget v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->contractorId_Index:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getCurrentTelecastId()J
    .locals 2

    .prologue
    .line 93
    iget v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->currentTelecastId_Index:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getFavourite()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->favouriteIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getHasSchedule()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->has_scheduleIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->imageIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIsDenied()Z
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->is_deniedIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIsIntersections()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->is_intersectionsIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getIsPorno()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->is_pornoIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getNumber()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->numberIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getRecordable()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->recordableIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getTerritoryId()J
    .locals 2

    .prologue
    .line 89
    iget v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->territoryIdIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lru/cn/api/provider/cursor/ChannelCursor;->titleIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ChannelCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
