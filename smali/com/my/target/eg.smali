.class public final Lcom/my/target/eg;
.super Landroid/widget/RelativeLayout;
.source "PromoMediaAdView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/eg$a;
    }
.end annotation


# instance fields
.field private final aW:Z

.field private ad:Lcom/my/target/ee$b;

.field private final aw:Lcom/my/target/cm;

.field private final bQ:Lcom/my/target/by;

.field private final bR:Lcom/my/target/eg$a;

.field private final bS:Lcom/my/target/fc;

.field private final bT:Landroid/widget/FrameLayout;

.field private bU:Lcom/my/target/common/models/VideoData;

.field private bV:I

.field private bW:I

.field private bX:Landroid/graphics/Bitmap;

.field private final bv:Lcom/my/target/bv;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/my/target/cm;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 50
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 51
    iput-object p2, p0, Lcom/my/target/eg;->aw:Lcom/my/target/cm;

    .line 52
    iput-boolean p3, p0, Lcom/my/target/eg;->aW:Z

    .line 54
    new-instance v0, Lcom/my/target/bv;

    invoke-direct {v0, p1}, Lcom/my/target/bv;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/eg;->bv:Lcom/my/target/bv;

    .line 55
    new-instance v0, Lcom/my/target/by;

    invoke-direct {v0, p1}, Lcom/my/target/by;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/eg;->bQ:Lcom/my/target/by;

    .line 56
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/eg;->bT:Landroid/widget/FrameLayout;

    .line 57
    iget-object v0, p0, Lcom/my/target/eg;->bT:Landroid/widget/FrameLayout;

    const v1, 0x33c5eaf8

    invoke-static {v0, v2, v1}, Lcom/my/target/cm;->a(Landroid/view/View;II)V

    .line 59
    new-instance v0, Lcom/my/target/fc;

    invoke-direct {v0, p1}, Lcom/my/target/fc;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/eg;->bS:Lcom/my/target/fc;

    .line 60
    new-instance v0, Lcom/my/target/eg$a;

    invoke-direct {v0, p0, v2}, Lcom/my/target/eg$a;-><init>(Lcom/my/target/eg;B)V

    iput-object v0, p0, Lcom/my/target/eg;->bR:Lcom/my/target/eg$a;

    .line 61
    return-void
.end method

.method static synthetic a(Lcom/my/target/eg;)Lcom/my/target/ee$b;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/my/target/eg;->ad:Lcom/my/target/ee$b;

    return-object v0
.end method

.method private c(Lcom/my/target/core/models/banners/h;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 401
    iget-object v0, p0, Lcom/my/target/eg;->bT:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 402
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/my/target/eg;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 403
    iget-object v0, p0, Lcom/my/target/eg;->bQ:Lcom/my/target/by;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setVisibility(I)V

    .line 405
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 406
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 408
    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/my/target/eg;->bW:I

    .line 409
    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/my/target/eg;->bV:I

    .line 411
    iget v1, p0, Lcom/my/target/eg;->bW:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/my/target/eg;->bV:I

    if-nez v1, :cond_1

    .line 413
    :cond_0
    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/my/target/eg;->bW:I

    .line 414
    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/my/target/eg;->bV:I

    .line 417
    :cond_1
    iget-object v1, p0, Lcom/my/target/eg;->bv:Lcom/my/target/bv;

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/my/target/bv;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 418
    iget-object v0, p0, Lcom/my/target/eg;->bv:Lcom/my/target/bv;

    invoke-virtual {v0, v2}, Lcom/my/target/bv;->setClickable(Z)V

    .line 420
    :cond_2
    return-void
.end method


# virtual methods
.method public final H()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 65
    iget-object v0, p0, Lcom/my/target/eg;->bQ:Lcom/my/target/by;

    const-string v1, "play_button"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/my/target/eg;->bv:Lcom/my/target/bv;

    const-string v1, "media_image"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/my/target/eg;->bS:Lcom/my/target/fc;

    const-string v1, "video_texture"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 69
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 71
    iget-object v1, p0, Lcom/my/target/eg;->bv:Lcom/my/target/bv;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Lcom/my/target/bv;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 72
    iget-object v1, p0, Lcom/my/target/eg;->bv:Lcom/my/target/bv;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/my/target/bv;->setAdjustViewBounds(Z)V

    .line 73
    iget-object v1, p0, Lcom/my/target/eg;->bv:Lcom/my/target/bv;

    invoke-virtual {v1, v0}, Lcom/my/target/bv;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 75
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 77
    iget-object v1, p0, Lcom/my/target/eg;->bS:Lcom/my/target/fc;

    invoke-virtual {v1, v0}, Lcom/my/target/fc;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 78
    iget-object v0, p0, Lcom/my/target/eg;->bS:Lcom/my/target/fc;

    invoke-virtual {p0, v0}, Lcom/my/target/eg;->addView(Landroid/view/View;)V

    .line 80
    iget-object v0, p0, Lcom/my/target/eg;->bv:Lcom/my/target/bv;

    invoke-virtual {p0, v0}, Lcom/my/target/eg;->addView(Landroid/view/View;)V

    .line 81
    iget-object v0, p0, Lcom/my/target/eg;->bQ:Lcom/my/target/by;

    invoke-virtual {p0, v0}, Lcom/my/target/eg;->addView(Landroid/view/View;)V

    .line 82
    iget-object v0, p0, Lcom/my/target/eg;->bT:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/my/target/eg;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 83
    return-void
.end method

.method final I()V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 210
    iget-object v0, p0, Lcom/my/target/eg;->bQ:Lcom/my/target/by;

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setVisibility(I)V

    .line 211
    iget-object v0, p0, Lcom/my/target/eg;->bv:Lcom/my/target/bv;

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setVisibility(I)V

    .line 213
    iget-object v0, p0, Lcom/my/target/eg;->bU:Lcom/my/target/common/models/VideoData;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/my/target/eg;->bS:Lcom/my/target/fc;

    iget-object v1, p0, Lcom/my/target/eg;->bU:Lcom/my/target/common/models/VideoData;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/my/target/fc;->a(Lcom/my/target/common/models/VideoData;Z)V

    .line 217
    :cond_0
    return-void
.end method

.method final a(Lcom/my/target/core/models/banners/h;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x2

    .line 181
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getVideoBanner()Lcom/my/target/aj;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1333
    iget-object v0, p0, Lcom/my/target/eg;->bT:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1334
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getVideoBanner()Lcom/my/target/aj;

    move-result-object v1

    .line 1335
    if-eqz v1, :cond_1

    .line 1340
    invoke-virtual {v1}, Lcom/my/target/aj;->getMediaData()Lcom/my/target/ag;

    move-result-object v0

    check-cast v0, Lcom/my/target/common/models/VideoData;

    iput-object v0, p0, Lcom/my/target/eg;->bU:Lcom/my/target/common/models/VideoData;

    .line 1342
    iget-object v0, p0, Lcom/my/target/eg;->bU:Lcom/my/target/common/models/VideoData;

    if-eqz v0, :cond_1

    .line 1347
    iget-object v0, p0, Lcom/my/target/eg;->bU:Lcom/my/target/common/models/VideoData;

    invoke-virtual {v0}, Lcom/my/target/common/models/VideoData;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/my/target/eg;->bW:I

    .line 1348
    iget-object v0, p0, Lcom/my/target/eg;->bU:Lcom/my/target/common/models/VideoData;

    invoke-virtual {v0}, Lcom/my/target/common/models/VideoData;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/my/target/eg;->bV:I

    .line 1350
    invoke-virtual {v1}, Lcom/my/target/aj;->getPreview()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 1352
    if-eqz v0, :cond_2

    .line 1354
    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/eg;->bX:Landroid/graphics/Bitmap;

    .line 1356
    iget-object v0, p0, Lcom/my/target/eg;->bv:Lcom/my/target/bv;

    iget-object v1, p0, Lcom/my/target/eg;->bX:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1369
    :cond_0
    :goto_0
    if-eq p2, v3, :cond_1

    .line 1371
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1373
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1374
    iget-object v1, p0, Lcom/my/target/eg;->bQ:Lcom/my/target/by;

    invoke-virtual {v1, v0}, Lcom/my/target/by;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1376
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getPlayIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 1378
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1380
    iget-object v1, p0, Lcom/my/target/eg;->bQ:Lcom/my/target/by;

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0, v3}, Lcom/my/target/by;->b(Landroid/graphics/Bitmap;Z)V

    :cond_1
    :goto_1
    return-void

    .line 1360
    :cond_2
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 1362
    if-eqz v0, :cond_0

    .line 1364
    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/eg;->bX:Landroid/graphics/Bitmap;

    .line 1365
    iget-object v0, p0, Lcom/my/target/eg;->bv:Lcom/my/target/bv;

    iget-object v1, p0, Lcom/my/target/eg;->bX:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 1385
    :cond_3
    iget-boolean v0, p0, Lcom/my/target/eg;->aW:Z

    if-eqz v0, :cond_4

    .line 1387
    iget-object v0, p0, Lcom/my/target/eg;->aw:Lcom/my/target/cm;

    const/16 v1, 0x8c

    invoke-virtual {v0, v1}, Lcom/my/target/cm;->n(I)I

    move-result v0

    .line 1394
    :goto_2
    iget-object v1, p0, Lcom/my/target/eg;->bQ:Lcom/my/target/by;

    invoke-static {v0}, Lcom/my/target/core/resources/b;->getPlayIcon(I)Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/my/target/by;->b(Landroid/graphics/Bitmap;Z)V

    goto :goto_1

    .line 1391
    :cond_4
    iget-object v0, p0, Lcom/my/target/eg;->aw:Lcom/my/target/cm;

    const/16 v1, 0x60

    invoke-virtual {v0, v1}, Lcom/my/target/cm;->n(I)I

    move-result v0

    goto :goto_2

    .line 187
    :cond_5
    invoke-direct {p0, p1}, Lcom/my/target/eg;->c(Lcom/my/target/core/models/banners/h;)V

    goto :goto_1
.end method

.method public final b(Lcom/my/target/core/models/banners/h;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/my/target/eg;->c(Lcom/my/target/core/models/banners/h;)V

    .line 88
    return-void
.end method

.method public final e(I)V
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/my/target/eg;->bS:Lcom/my/target/fc;

    invoke-virtual {v0}, Lcom/my/target/fc;->bl()V

    .line 154
    :goto_0
    return-void

    .line 146
    :cond_0
    if-nez p1, :cond_1

    .line 148
    iget-object v0, p0, Lcom/my/target/eg;->bS:Lcom/my/target/fc;

    invoke-virtual {v0}, Lcom/my/target/fc;->bj()V

    goto :goto_0

    .line 152
    :cond_1
    iget-object v0, p0, Lcom/my/target/eg;->bS:Lcom/my/target/fc;

    invoke-virtual {v0}, Lcom/my/target/fc;->bk()V

    goto :goto_0
.end method

.method final h(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 193
    iget-object v0, p0, Lcom/my/target/eg;->bS:Lcom/my/target/fc;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/my/target/fc;->k(Z)V

    .line 194
    iget-object v0, p0, Lcom/my/target/eg;->bv:Lcom/my/target/bv;

    invoke-virtual {v0, v3}, Lcom/my/target/bv;->setVisibility(I)V

    .line 195
    iget-object v0, p0, Lcom/my/target/eg;->bv:Lcom/my/target/bv;

    iget-object v1, p0, Lcom/my/target/eg;->bX:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 196
    if-eqz p1, :cond_0

    .line 198
    iget-object v0, p0, Lcom/my/target/eg;->bQ:Lcom/my/target/by;

    invoke-virtual {v0, v3}, Lcom/my/target/by;->setVisibility(I)V

    .line 206
    :goto_0
    return-void

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/my/target/eg;->bv:Lcom/my/target/bv;

    invoke-virtual {v0, v2}, Lcom/my/target/bv;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    iget-object v0, p0, Lcom/my/target/eg;->bQ:Lcom/my/target/by;

    invoke-virtual {v0, v2}, Lcom/my/target/by;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 204
    invoke-virtual {p0, v2}, Lcom/my/target/eg;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final isPaused()Z
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/my/target/eg;->bS:Lcom/my/target/fc;

    invoke-virtual {v0}, Lcom/my/target/fc;->getVideoState()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isPlaying()Z
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/my/target/eg;->bS:Lcom/my/target/fc;

    invoke-virtual {v0}, Lcom/my/target/fc;->getVideoState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/my/target/eg;->ad:Lcom/my/target/ee$b;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/my/target/eg;->ad:Lcom/my/target/ee$b;

    invoke-interface {v0}, Lcom/my/target/ee$b;->E()V

    .line 176
    :cond_0
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 10
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v3, 0x0

    const/high16 v1, -0x80000000

    const/high16 v9, 0x40000000    # 2.0f

    .line 222
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 223
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 224
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 225
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 227
    if-nez v5, :cond_d

    move v6, v1

    .line 232
    :goto_0
    if-nez v0, :cond_0

    move v0, v1

    .line 237
    :cond_0
    iget v5, p0, Lcom/my/target/eg;->bV:I

    if-eqz v5, :cond_1

    iget v5, p0, Lcom/my/target/eg;->bW:I

    if-nez v5, :cond_2

    .line 239
    :cond_1
    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 240
    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 241
    invoke-super {p0, v0, v1}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 328
    :goto_1
    return-void

    .line 245
    :cond_2
    iget v5, p0, Lcom/my/target/eg;->bW:I

    int-to-float v5, v5

    iget v7, p0, Lcom/my/target/eg;->bV:I

    int-to-float v7, v7

    div-float v7, v5, v7

    .line 247
    const/4 v5, 0x0

    .line 248
    if-eqz v4, :cond_3

    .line 250
    int-to-float v5, v2

    int-to-float v8, v4

    div-float/2addr v5, v8

    .line 256
    :cond_3
    if-ne v6, v9, :cond_4

    if-ne v0, v9, :cond_4

    move v0, v2

    move v3, v4

    .line 325
    :goto_2
    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 326
    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 327
    invoke-super {p0, v0, v1}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    goto :goto_1

    .line 261
    :cond_4
    if-ne v6, v1, :cond_8

    if-ne v0, v1, :cond_8

    .line 263
    cmpg-float v0, v7, v5

    if-gez v0, :cond_6

    .line 265
    int-to-float v0, v4

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 267
    if-lez v2, :cond_5

    if-le v0, v2, :cond_5

    .line 269
    int-to-float v0, v2

    div-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    move v0, v2

    .line 270
    goto :goto_2

    :cond_5
    move v3, v4

    .line 277
    goto :goto_2

    .line 280
    :cond_6
    int-to-float v0, v2

    div-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 282
    if-lez v4, :cond_7

    if-le v3, v4, :cond_7

    .line 284
    int-to-float v0, v4

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    move v3, v4

    .line 285
    goto :goto_2

    :cond_7
    move v0, v2

    .line 292
    goto :goto_2

    .line 294
    :cond_8
    if-ne v6, v1, :cond_a

    if-ne v0, v9, :cond_a

    .line 296
    int-to-float v0, v4

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 298
    if-lez v2, :cond_9

    if-le v0, v2, :cond_9

    .line 300
    int-to-float v0, v2

    div-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    move v0, v2

    .line 301
    goto :goto_2

    :cond_9
    move v3, v4

    .line 308
    goto :goto_2

    .line 309
    :cond_a
    if-ne v6, v9, :cond_c

    if-ne v0, v1, :cond_c

    .line 311
    int-to-float v0, v2

    div-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 313
    if-lez v4, :cond_b

    if-le v3, v4, :cond_b

    .line 315
    int-to-float v0, v4

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    move v3, v4

    .line 316
    goto :goto_2

    :cond_b
    move v0, v2

    .line 321
    goto :goto_2

    :cond_c
    move v0, v3

    goto :goto_2

    :cond_d
    move v6, v5

    goto/16 :goto_0
.end method

.method public final pause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 126
    iget-object v0, p0, Lcom/my/target/eg;->bS:Lcom/my/target/fc;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/my/target/fc;->j(Z)V

    .line 127
    iget-object v0, p0, Lcom/my/target/eg;->bQ:Lcom/my/target/by;

    invoke-virtual {v0, v2}, Lcom/my/target/by;->setVisibility(I)V

    .line 128
    iget-object v0, p0, Lcom/my/target/eg;->bS:Lcom/my/target/fc;

    invoke-virtual {v0}, Lcom/my/target/fc;->getScreenShot()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/my/target/eg;->bv:Lcom/my/target/bv;

    invoke-virtual {v0, v2}, Lcom/my/target/bv;->setVisibility(I)V

    .line 131
    iget-object v0, p0, Lcom/my/target/eg;->bv:Lcom/my/target/bv;

    iget-object v1, p0, Lcom/my/target/eg;->bS:Lcom/my/target/fc;

    invoke-virtual {v1}, Lcom/my/target/fc;->getScreenShot()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 133
    :cond_0
    return-void
.end method

.method public final resume()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 116
    iget-object v0, p0, Lcom/my/target/eg;->bU:Lcom/my/target/common/models/VideoData;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/my/target/eg;->bS:Lcom/my/target/fc;

    invoke-virtual {v0}, Lcom/my/target/fc;->resume()V

    .line 119
    iget-object v0, p0, Lcom/my/target/eg;->bv:Lcom/my/target/bv;

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setVisibility(I)V

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/my/target/eg;->bQ:Lcom/my/target/by;

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setVisibility(I)V

    .line 122
    return-void
.end method

.method public final setImageClickable(Z)V
    .locals 2
    .param p1, "imageClickable"    # Z

    .prologue
    .line 158
    if-eqz p1, :cond_0

    .line 160
    iget-object v0, p0, Lcom/my/target/eg;->bT:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    :goto_0
    return-void

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/my/target/eg;->bT:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    iget-object v0, p0, Lcom/my/target/eg;->bT:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    goto :goto_0
.end method

.method public final setInterstitialPromoViewListener(Lcom/my/target/ee$b;)V
    .locals 2
    .param p1, "interstitialPromoViewListener"    # Lcom/my/target/ee$b;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/my/target/eg;->ad:Lcom/my/target/ee$b;

    .line 98
    iget-object v0, p0, Lcom/my/target/eg;->bS:Lcom/my/target/fc;

    invoke-virtual {v0, p1}, Lcom/my/target/fc;->setVideoListener(Lcom/my/target/cc$a;)V

    .line 99
    iget-object v0, p0, Lcom/my/target/eg;->bv:Lcom/my/target/bv;

    iget-object v1, p0, Lcom/my/target/eg;->bR:Lcom/my/target/eg$a;

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    iget-object v0, p0, Lcom/my/target/eg;->bQ:Lcom/my/target/by;

    iget-object v1, p0, Lcom/my/target/eg;->bR:Lcom/my/target/eg$a;

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    iget-object v0, p0, Lcom/my/target/eg;->bR:Lcom/my/target/eg$a;

    invoke-virtual {p0, v0}, Lcom/my/target/eg;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    return-void
.end method

.method public final setVideoListener(Lcom/my/target/cc$a;)V
    .locals 1
    .param p1, "videoListener"    # Lcom/my/target/cc$a;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/my/target/eg;->bS:Lcom/my/target/fc;

    invoke-virtual {v0, p1}, Lcom/my/target/fc;->setVideoListener(Lcom/my/target/cc$a;)V

    .line 138
    return-void
.end method
