.class Lcom/yandex/metrica/impl/ob/dr;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/yandex/metrica/impl/ob/dv;

.field private final b:Lcom/yandex/metrica/impl/ob/ct;

.field private final c:Lcom/yandex/metrica/impl/ob/cs;


# direct methods
.method public constructor <init>(Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/ct;Lcom/yandex/metrica/impl/ob/cs;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/dr;->a:Lcom/yandex/metrica/impl/ob/dv;

    .line 35
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/dr;->b:Lcom/yandex/metrica/impl/ob/ct;

    .line 36
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/dr;->c:Lcom/yandex/metrica/impl/ob/cs;

    .line 37
    return-void
.end method


# virtual methods
.method public a()V
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const v8, 0x3dcccccd    # 0.1f

    .line 40
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/dr;->a:Lcom/yandex/metrica/impl/ob/dv;

    if-eqz v2, :cond_1

    .line 41
    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/dr;->a:Lcom/yandex/metrica/impl/ob/dv;

    .line 1049
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/dr;->b:Lcom/yandex/metrica/impl/ob/ct;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/ct;->a()J

    move-result-wide v4

    iget v2, v3, Lcom/yandex/metrica/impl/ob/dv;->l:I

    int-to-long v6, v2

    cmp-long v2, v4, v6

    if-lez v2, :cond_2

    move v2, v0

    .line 1052
    :goto_0
    if-eqz v2, :cond_0

    .line 1053
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/dr;->b:Lcom/yandex/metrica/impl/ob/ct;

    iget v3, v3, Lcom/yandex/metrica/impl/ob/dv;->l:I

    int-to-float v3, v3

    mul-float/2addr v3, v8

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Lcom/yandex/metrica/impl/ob/ct;->c(I)I

    .line 42
    :cond_0
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/dr;->a:Lcom/yandex/metrica/impl/ob/dv;

    .line 1059
    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/dr;->c:Lcom/yandex/metrica/impl/ob/cs;

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/ob/cs;->a()J

    move-result-wide v4

    iget v3, v2, Lcom/yandex/metrica/impl/ob/dv;->l:I

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-lez v3, :cond_3

    .line 1062
    :goto_1
    if-eqz v0, :cond_1

    .line 1063
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/dr;->c:Lcom/yandex/metrica/impl/ob/cs;

    iget v1, v2, Lcom/yandex/metrica/impl/ob/dv;->l:I

    int-to-float v1, v1

    mul-float/2addr v1, v8

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/cs;->c(I)I

    .line 46
    :cond_1
    return-void

    :cond_2
    move v2, v1

    .line 1049
    goto :goto_0

    :cond_3
    move v0, v1

    .line 1059
    goto :goto_1
.end method

.method public a(Lcom/yandex/metrica/impl/ob/dv;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/dr;->a:Lcom/yandex/metrica/impl/ob/dv;

    .line 70
    return-void
.end method
