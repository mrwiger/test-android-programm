.class public Lru/cn/tv/stb/login/PinAuthorizationDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "PinAuthorizationDialog.java"


# instance fields
.field private viewModel:Lru/cn/tv/stb/login/PinAuthorizationViewModel;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method private completed(Z)V
    .locals 4
    .param p1, "completed"    # Z

    .prologue
    .line 50
    if-eqz p1, :cond_0

    .line 51
    invoke-virtual {p0}, Lru/cn/tv/stb/login/PinAuthorizationDialog;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {p0}, Lru/cn/tv/stb/login/PinAuthorizationDialog;->getTargetRequestCode()I

    move-result v1

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 52
    invoke-virtual {p0}, Lru/cn/tv/stb/login/PinAuthorizationDialog;->dismiss()V

    .line 54
    :cond_0
    return-void
.end method

.method private expired(Z)V
    .locals 4
    .param p1, "expired"    # Z

    .prologue
    .line 57
    invoke-virtual {p0}, Lru/cn/tv/stb/login/PinAuthorizationDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 58
    .local v0, "dialog":Landroid/app/Dialog;
    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    .line 60
    new-instance v1, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lru/cn/tv/stb/login/PinAuthorizationDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0e00a4

    .line 61
    invoke-virtual {v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e0151

    .line 62
    invoke-virtual {v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e0038

    new-instance v3, Lru/cn/tv/stb/login/PinAuthorizationDialog$$Lambda$3;

    invoke-direct {v3, p0, v0}, Lru/cn/tv/stb/login/PinAuthorizationDialog$$Lambda$3;-><init>(Lru/cn/tv/stb/login/PinAuthorizationDialog;Landroid/app/Dialog;)V

    .line 63
    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e0030

    new-instance v3, Lru/cn/tv/stb/login/PinAuthorizationDialog$$Lambda$4;

    invoke-direct {v3, p0}, Lru/cn/tv/stb/login/PinAuthorizationDialog$$Lambda$4;-><init>(Lru/cn/tv/stb/login/PinAuthorizationDialog;)V

    .line 67
    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    .line 68
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 69
    return-void
.end method

.method private setPinCode(Ljava/lang/String;)V
    .locals 3
    .param p1, "pinCode"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-virtual {p0}, Lru/cn/tv/stb/login/PinAuthorizationDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    const v2, 0x7f090162

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 46
    .local v0, "pinCodeView":Landroid/widget/TextView;
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    return-void
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$PinAuthorizationDialog(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/stb/login/PinAuthorizationDialog;->setPinCode(Ljava/lang/String;)V

    return-void
.end method

.method final bridge synthetic bridge$lambda$1$PinAuthorizationDialog(Z)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/stb/login/PinAuthorizationDialog;->completed(Z)V

    return-void
.end method

.method final bridge synthetic bridge$lambda$2$PinAuthorizationDialog(Z)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/stb/login/PinAuthorizationDialog;->expired(Z)V

    return-void
.end method

.method final synthetic lambda$expired$0$PinAuthorizationDialog(Landroid/app/Dialog;Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/app/Dialog;
    .param p2, "d"    # Landroid/content/DialogInterface;
    .param p3, "which"    # I

    .prologue
    .line 64
    iget-object v0, p0, Lru/cn/tv/stb/login/PinAuthorizationDialog;->viewModel:Lru/cn/tv/stb/login/PinAuthorizationViewModel;

    invoke-virtual {p0}, Lru/cn/tv/stb/login/PinAuthorizationDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/cn/tv/stb/login/PinAuthorizationViewModel;->start(Landroid/content/Context;)V

    .line 65
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    .line 66
    return-void
.end method

.method final synthetic lambda$expired$1$PinAuthorizationDialog(Landroid/content/DialogInterface;I)V
    .locals 0
    .param p1, "dialog1"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 67
    invoke-virtual {p0}, Lru/cn/tv/stb/login/PinAuthorizationDialog;->dismiss()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 24
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 26
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v0

    const-class v1, Lru/cn/tv/stb/login/PinAuthorizationViewModel;

    invoke-static {p0, v0, v1}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lru/cn/tv/stb/login/PinAuthorizationViewModel;

    iput-object v0, p0, Lru/cn/tv/stb/login/PinAuthorizationDialog;->viewModel:Lru/cn/tv/stb/login/PinAuthorizationViewModel;

    .line 27
    iget-object v0, p0, Lru/cn/tv/stb/login/PinAuthorizationDialog;->viewModel:Lru/cn/tv/stb/login/PinAuthorizationViewModel;

    invoke-virtual {v0}, Lru/cn/tv/stb/login/PinAuthorizationViewModel;->pinCode()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/stb/login/PinAuthorizationDialog$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/login/PinAuthorizationDialog$$Lambda$0;-><init>(Lru/cn/tv/stb/login/PinAuthorizationDialog;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 28
    iget-object v0, p0, Lru/cn/tv/stb/login/PinAuthorizationDialog;->viewModel:Lru/cn/tv/stb/login/PinAuthorizationViewModel;

    invoke-virtual {v0}, Lru/cn/tv/stb/login/PinAuthorizationViewModel;->completed()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/stb/login/PinAuthorizationDialog$$Lambda$1;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/login/PinAuthorizationDialog$$Lambda$1;-><init>(Lru/cn/tv/stb/login/PinAuthorizationDialog;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 29
    iget-object v0, p0, Lru/cn/tv/stb/login/PinAuthorizationDialog;->viewModel:Lru/cn/tv/stb/login/PinAuthorizationViewModel;

    invoke-virtual {v0}, Lru/cn/tv/stb/login/PinAuthorizationViewModel;->expired()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/stb/login/PinAuthorizationDialog$$Lambda$2;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/login/PinAuthorizationDialog$$Lambda$2;-><init>(Lru/cn/tv/stb/login/PinAuthorizationDialog;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 31
    iget-object v0, p0, Lru/cn/tv/stb/login/PinAuthorizationDialog;->viewModel:Lru/cn/tv/stb/login/PinAuthorizationViewModel;

    invoke-virtual {p0}, Lru/cn/tv/stb/login/PinAuthorizationDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/cn/tv/stb/login/PinAuthorizationViewModel;->start(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 37
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lru/cn/tv/stb/login/PinAuthorizationDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0c00bd

    .line 38
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setView(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e0154

    .line 39
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e0030

    const/4 v2, 0x0

    .line 40
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    .line 37
    return-object v0
.end method
