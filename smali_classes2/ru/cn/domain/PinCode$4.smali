.class Lru/cn/domain/PinCode$4;
.super Landroid/os/AsyncTask;
.source "PinCode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/domain/PinCode;->checkAuthoriseStatus(Lru/cn/domain/PinCode$PinOperationCallbacks;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lru/cn/api/billing/PinAuthorisationStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/domain/PinCode;

.field final synthetic val$callbacks:Lru/cn/domain/PinCode$PinOperationCallbacks;


# direct methods
.method constructor <init>(Lru/cn/domain/PinCode;Lru/cn/domain/PinCode$PinOperationCallbacks;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/domain/PinCode;

    .prologue
    .line 129
    iput-object p1, p0, Lru/cn/domain/PinCode$4;->this$0:Lru/cn/domain/PinCode;

    iput-object p2, p0, Lru/cn/domain/PinCode$4;->val$callbacks:Lru/cn/domain/PinCode$PinOperationCallbacks;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 129
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lru/cn/domain/PinCode$4;->doInBackground([Ljava/lang/Void;)Lru/cn/api/billing/PinAuthorisationStatus;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Lru/cn/api/billing/PinAuthorisationStatus;
    .locals 4
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    .line 133
    :try_start_0
    iget-object v3, p0, Lru/cn/domain/PinCode$4;->this$0:Lru/cn/domain/PinCode;

    invoke-static {v3}, Lru/cn/domain/PinCode;->access$200(Lru/cn/domain/PinCode;)Ljava/lang/String;

    move-result-object v2

    .line 135
    .local v2, "requestUrl":Ljava/lang/String;
    new-instance v1, Lru/cn/api/billing/PinAuthorisationApi;

    invoke-direct {v1}, Lru/cn/api/billing/PinAuthorisationApi;-><init>()V

    .line 136
    .local v1, "pinCodeStatusApi":Lru/cn/api/billing/PinAuthorisationApi;
    invoke-virtual {v1, v2}, Lru/cn/api/billing/PinAuthorisationApi;->getStatus(Ljava/lang/String;)Lru/cn/api/billing/PinAuthorisationStatus;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 141
    .end local v1    # "pinCodeStatusApi":Lru/cn/api/billing/PinAuthorisationApi;
    .end local v2    # "requestUrl":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 137
    :catch_0
    move-exception v0

    .line 138
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .line 141
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 129
    check-cast p1, Lru/cn/api/billing/PinAuthorisationStatus;

    invoke-virtual {p0, p1}, Lru/cn/domain/PinCode$4;->onPostExecute(Lru/cn/api/billing/PinAuthorisationStatus;)V

    return-void
.end method

.method protected onPostExecute(Lru/cn/api/billing/PinAuthorisationStatus;)V
    .locals 2
    .param p1, "pinAuthorisationStatus"    # Lru/cn/api/billing/PinAuthorisationStatus;

    .prologue
    .line 146
    if-nez p1, :cond_0

    .line 147
    iget-object v0, p0, Lru/cn/domain/PinCode$4;->val$callbacks:Lru/cn/domain/PinCode$PinOperationCallbacks;

    invoke-interface {v0}, Lru/cn/domain/PinCode$PinOperationCallbacks;->onError()V

    .line 167
    :goto_0
    return-void

    .line 151
    :cond_0
    sget-object v0, Lru/cn/domain/PinCode$6;->$SwitchMap$ru$cn$api$billing$PinAuthorisationStatus$Status:[I

    iget-object v1, p1, Lru/cn/api/billing/PinAuthorisationStatus;->status:Lru/cn/api/billing/PinAuthorisationStatus$Status;

    invoke-virtual {v1}, Lru/cn/api/billing/PinAuthorisationStatus$Status;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 165
    iget-object v0, p0, Lru/cn/domain/PinCode$4;->val$callbacks:Lru/cn/domain/PinCode$PinOperationCallbacks;

    invoke-interface {v0}, Lru/cn/domain/PinCode$PinOperationCallbacks;->onError()V

    goto :goto_0

    .line 153
    :pswitch_0
    iget-object v0, p0, Lru/cn/domain/PinCode$4;->val$callbacks:Lru/cn/domain/PinCode$PinOperationCallbacks;

    invoke-interface {v0}, Lru/cn/domain/PinCode$PinOperationCallbacks;->onSuccess()V

    goto :goto_0

    .line 157
    :pswitch_1
    iget-object v0, p0, Lru/cn/domain/PinCode$4;->val$callbacks:Lru/cn/domain/PinCode$PinOperationCallbacks;

    invoke-interface {v0}, Lru/cn/domain/PinCode$PinOperationCallbacks;->onError()V

    goto :goto_0

    .line 161
    :pswitch_2
    iget-object v0, p0, Lru/cn/domain/PinCode$4;->val$callbacks:Lru/cn/domain/PinCode$PinOperationCallbacks;

    invoke-interface {v0}, Lru/cn/domain/PinCode$PinOperationCallbacks;->onError()V

    goto :goto_0

    .line 151
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
