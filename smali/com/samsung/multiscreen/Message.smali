.class public Lcom/samsung/multiscreen/Message;
.super Ljava/lang/Object;
.source "Message.java"


# static fields
.field static final MEHOD_APPLICATION_GET:Ljava/lang/String; = "ms.application.get"

.field static final MEHOD_APPLICATION_INSTALL:Ljava/lang/String; = "ms.application.install"

.field static final MEHOD_APPLICATION_START:Ljava/lang/String; = "ms.application.start"

.field static final MEHOD_APPLICATION_STOP:Ljava/lang/String; = "ms.application.stop"

.field static final MEHOD_WEB_APPLICATION_GET:Ljava/lang/String; = "ms.webapplication.get"

.field static final MEHOD_WEB_APPLICATION_START:Ljava/lang/String; = "ms.webapplication.start"

.field static final MEHOD_WEB_APPLICATION_STOP:Ljava/lang/String; = "ms.webapplication.stop"

.field static final METHOD_EMIT:Ljava/lang/String; = "ms.channel.emit"

.field static final PROPERTY_APP_NAME:Ljava/lang/String; = "appName"

.field static final PROPERTY_ARGS:Ljava/lang/String; = "args"

.field static final PROPERTY_CLIENTS:Ljava/lang/String; = "clients"

.field static final PROPERTY_DATA:Ljava/lang/String; = "data"

.field static final PROPERTY_ERROR:Ljava/lang/String; = "error"

.field static final PROPERTY_EVENT:Ljava/lang/String; = "event"

.field static final PROPERTY_FROM:Ljava/lang/String; = "from"

.field static final PROPERTY_ID:Ljava/lang/String; = "id"

.field static final PROPERTY_LIBRARY:Ljava/lang/String; = "library"

.field static final PROPERTY_MESSAGE:Ljava/lang/String; = "message"

.field public static final PROPERTY_MESSAGE_ID:Ljava/lang/String; = "id"

.field static final PROPERTY_METHOD:Ljava/lang/String; = "method"

.field static final PROPERTY_MOD_NUMBER:Ljava/lang/String; = "modelNumber"

.field static final PROPERTY_OS:Ljava/lang/String; = "os"

.field static final PROPERTY_PARAMS:Ljava/lang/String; = "params"

.field static final PROPERTY_RESULT:Ljava/lang/String; = "result"

.field static final PROPERTY_TO:Ljava/lang/String; = "to"

.field static final PROPERTY_URL:Ljava/lang/String; = "url"

.field static final PROPERTY_VERSION:Ljava/lang/String; = "version"

.field public static final TARGET_ALL:Ljava/lang/String; = "all"

.field public static final TARGET_BROADCAST:Ljava/lang/String; = "broadcast"

.field public static final TARGET_HOST:Ljava/lang/String; = "host"


# instance fields
.field private final channel:Lcom/samsung/multiscreen/Channel;
    .annotation build Llombok/NonNull;
    .end annotation
.end field

.field private final data:Ljava/lang/Object;

.field private final event:Ljava/lang/String;
    .annotation build Llombok/NonNull;
    .end annotation
.end field

.field private final from:Lcom/samsung/multiscreen/Client;
    .annotation build Llombok/NonNull;
    .end annotation
.end field

.field private final payload:[B


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/Channel;Ljava/lang/String;Ljava/lang/Object;Lcom/samsung/multiscreen/Client;[B)V
    .locals 2
    .param p1, "channel"    # Lcom/samsung/multiscreen/Channel;
        .annotation build Llombok/NonNull;
        .end annotation
    .end param
    .param p2, "event"    # Ljava/lang/String;
        .annotation build Llombok/NonNull;
        .end annotation
    .end param
    .param p3, "data"    # Ljava/lang/Object;
    .param p4, "from"    # Lcom/samsung/multiscreen/Client;
        .annotation build Llombok/NonNull;
        .end annotation
    .end param
    .param p5, "payload"    # [B

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "channel"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "event"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p4, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "from"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iput-object p1, p0, Lcom/samsung/multiscreen/Message;->channel:Lcom/samsung/multiscreen/Channel;

    iput-object p2, p0, Lcom/samsung/multiscreen/Message;->event:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsung/multiscreen/Message;->data:Ljava/lang/Object;

    iput-object p4, p0, Lcom/samsung/multiscreen/Message;->from:Lcom/samsung/multiscreen/Client;

    iput-object p5, p0, Lcom/samsung/multiscreen/Message;->payload:[B

    return-void
.end method


# virtual methods
.method protected canEqual(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 40
    instance-of v0, p1, Lcom/samsung/multiscreen/Message;

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 13
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 40
    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v9

    :cond_1
    instance-of v11, p1, Lcom/samsung/multiscreen/Message;

    if-nez v11, :cond_2

    move v9, v10

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/samsung/multiscreen/Message;

    .local v0, "other":Lcom/samsung/multiscreen/Message;
    invoke-virtual {v0, p0}, Lcom/samsung/multiscreen/Message;->canEqual(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_3

    move v9, v10

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Message;->getChannel()Lcom/samsung/multiscreen/Channel;

    move-result-object v5

    .local v5, "this$channel":Lcom/samsung/multiscreen/Channel;
    invoke-virtual {v0}, Lcom/samsung/multiscreen/Message;->getChannel()Lcom/samsung/multiscreen/Channel;

    move-result-object v1

    .local v1, "other$channel":Lcom/samsung/multiscreen/Channel;
    if-nez v5, :cond_5

    if-eqz v1, :cond_6

    :cond_4
    move v9, v10

    goto :goto_0

    :cond_5
    invoke-virtual {v5, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    :cond_6
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Message;->getEvent()Ljava/lang/String;

    move-result-object v7

    .local v7, "this$event":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/samsung/multiscreen/Message;->getEvent()Ljava/lang/String;

    move-result-object v3

    .local v3, "other$event":Ljava/lang/String;
    if-nez v7, :cond_8

    if-eqz v3, :cond_9

    :cond_7
    move v9, v10

    goto :goto_0

    :cond_8
    invoke-virtual {v7, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    :cond_9
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Message;->getData()Ljava/lang/Object;

    move-result-object v6

    .local v6, "this$data":Ljava/lang/Object;
    invoke-virtual {v0}, Lcom/samsung/multiscreen/Message;->getData()Ljava/lang/Object;

    move-result-object v2

    .local v2, "other$data":Ljava/lang/Object;
    if-nez v6, :cond_b

    if-eqz v2, :cond_c

    :cond_a
    move v9, v10

    goto :goto_0

    :cond_b
    invoke-virtual {v6, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    :cond_c
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Message;->getFrom()Lcom/samsung/multiscreen/Client;

    move-result-object v8

    .local v8, "this$from":Lcom/samsung/multiscreen/Client;
    invoke-virtual {v0}, Lcom/samsung/multiscreen/Message;->getFrom()Lcom/samsung/multiscreen/Client;

    move-result-object v4

    .local v4, "other$from":Lcom/samsung/multiscreen/Client;
    if-nez v8, :cond_e

    if-eqz v4, :cond_f

    :cond_d
    move v9, v10

    goto :goto_0

    :cond_e
    invoke-virtual {v8, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_d

    :cond_f
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Message;->getPayload()[B

    move-result-object v11

    invoke-virtual {v0}, Lcom/samsung/multiscreen/Message;->getPayload()[B

    move-result-object v12

    invoke-static {v11, v12}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v11

    if-nez v11, :cond_0

    move v9, v10

    goto :goto_0
.end method

.method public getChannel()Lcom/samsung/multiscreen/Channel;
    .locals 1
    .annotation build Llombok/NonNull;
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/multiscreen/Message;->channel:Lcom/samsung/multiscreen/Channel;

    return-object v0
.end method

.method public getData()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/multiscreen/Message;->data:Ljava/lang/Object;

    return-object v0
.end method

.method public getEvent()Ljava/lang/String;
    .locals 1
    .annotation build Llombok/NonNull;
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/multiscreen/Message;->event:Ljava/lang/String;

    return-object v0
.end method

.method public getFrom()Lcom/samsung/multiscreen/Client;
    .locals 1
    .annotation build Llombok/NonNull;
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/multiscreen/Message;->from:Lcom/samsung/multiscreen/Client;

    return-object v0
.end method

.method public getPayload()[B
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/samsung/multiscreen/Message;->payload:[B

    return-object v0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 40
    const/16 v4, 0x3b

    .local v4, "PRIME":I
    const/4 v5, 0x1

    .local v5, "result":I
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Message;->getChannel()Lcom/samsung/multiscreen/Channel;

    move-result-object v0

    .local v0, "$channel":Lcom/samsung/multiscreen/Channel;
    if-nez v0, :cond_0

    move v6, v7

    :goto_0
    add-int/lit8 v5, v6, 0x3b

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Message;->getEvent()Ljava/lang/String;

    move-result-object v2

    .local v2, "$event":Ljava/lang/String;
    mul-int/lit8 v8, v5, 0x3b

    if-nez v2, :cond_1

    move v6, v7

    :goto_1
    add-int v5, v8, v6

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Message;->getData()Ljava/lang/Object;

    move-result-object v1

    .local v1, "$data":Ljava/lang/Object;
    mul-int/lit8 v8, v5, 0x3b

    if-nez v1, :cond_2

    move v6, v7

    :goto_2
    add-int v5, v8, v6

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Message;->getFrom()Lcom/samsung/multiscreen/Client;

    move-result-object v3

    .local v3, "$from":Lcom/samsung/multiscreen/Client;
    mul-int/lit8 v6, v5, 0x3b

    if-nez v3, :cond_3

    :goto_3
    add-int v5, v6, v7

    mul-int/lit8 v6, v5, 0x3b

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Message;->getPayload()[B

    move-result-object v7

    invoke-static {v7}, Ljava/util/Arrays;->hashCode([B)I

    move-result v7

    add-int v5, v6, v7

    return v5

    .end local v1    # "$data":Ljava/lang/Object;
    .end local v2    # "$event":Ljava/lang/String;
    .end local v3    # "$from":Lcom/samsung/multiscreen/Client;
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v6

    goto :goto_0

    .restart local v2    # "$event":Ljava/lang/String;
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v6

    goto :goto_1

    .restart local v1    # "$data":Ljava/lang/Object;
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v6

    goto :goto_2

    .restart local v3    # "$from":Lcom/samsung/multiscreen/Client;
    :cond_3
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v7

    goto :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Message(event="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Message;->getEvent()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Message;->getData()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", from="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Message;->getFrom()Lcom/samsung/multiscreen/Client;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
