.class public final enum Lru/cn/domain/statistics/inetra/ErrorCode;
.super Ljava/lang/Enum;
.source "ErrorCode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/domain/statistics/inetra/ErrorCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/domain/statistics/inetra/ErrorCode;

.field public static final enum ADV_ERROR_LOAD_BANNER:Lru/cn/domain/statistics/inetra/ErrorCode;

.field public static final enum ADV_ERROR_PLAY:Lru/cn/domain/statistics/inetra/ErrorCode;

.field public static final enum CHANNELS_CONNECTION_TIMEOUT:Lru/cn/domain/statistics/inetra/ErrorCode;

.field public static final enum CHANNELS_DOMAIN_TIMEOUT:Lru/cn/domain/statistics/inetra/ErrorCode;

.field public static final enum CHANNELS_FORMAT_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

.field public static final enum CHANNELS_HTTP_RESPONSE_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

.field public static final enum CHANNELS_INVALID_URL:Lru/cn/domain/statistics/inetra/ErrorCode;

.field public static final enum CHANNELS_SOCKET_TIMEOUT:Lru/cn/domain/statistics/inetra/ErrorCode;

.field public static final enum CHROMECAST_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

.field public static final enum LOW_DOWNLOAD_SPEED:Lru/cn/domain/statistics/inetra/ErrorCode;

.field public static final enum LOW_DOWNLOAD_SPEED_P2P:Lru/cn/domain/statistics/inetra/ErrorCode;

.field public static final enum PLAY_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

.field public static final enum PLAY_ERROR_P2P:Lru/cn/domain/statistics/inetra/ErrorCode;

.field public static final enum UNABLE_START_VIDEO:Lru/cn/domain/statistics/inetra/ErrorCode;

.field public static final enum UNABLE_START_VIDEO_P2P:Lru/cn/domain/statistics/inetra/ErrorCode;

.field public static final enum UNKNOWN_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0xd

    const/16 v7, 0xc

    const/16 v6, 0xb

    const/16 v5, 0xa

    const/4 v4, 0x0

    .line 5
    new-instance v0, Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v1, "UNKNOWN_ERROR"

    invoke-direct {v0, v1, v4, v4}, Lru/cn/domain/statistics/inetra/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->UNKNOWN_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    .line 8
    new-instance v0, Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v1, "CHANNELS_DOMAIN_TIMEOUT"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, v5}, Lru/cn/domain/statistics/inetra/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->CHANNELS_DOMAIN_TIMEOUT:Lru/cn/domain/statistics/inetra/ErrorCode;

    .line 11
    new-instance v0, Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v1, "CHANNELS_CONNECTION_TIMEOUT"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2, v6}, Lru/cn/domain/statistics/inetra/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->CHANNELS_CONNECTION_TIMEOUT:Lru/cn/domain/statistics/inetra/ErrorCode;

    .line 14
    new-instance v0, Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v1, "CHANNELS_HTTP_RESPONSE_ERROR"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2, v7}, Lru/cn/domain/statistics/inetra/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->CHANNELS_HTTP_RESPONSE_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    .line 17
    new-instance v0, Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v1, "CHANNELS_SOCKET_TIMEOUT"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2, v8}, Lru/cn/domain/statistics/inetra/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->CHANNELS_SOCKET_TIMEOUT:Lru/cn/domain/statistics/inetra/ErrorCode;

    .line 20
    new-instance v0, Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v1, "CHANNELS_FORMAT_ERROR"

    const/4 v2, 0x5

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lru/cn/domain/statistics/inetra/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->CHANNELS_FORMAT_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    .line 23
    new-instance v0, Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v1, "CHANNELS_INVALID_URL"

    const/4 v2, 0x6

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lru/cn/domain/statistics/inetra/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->CHANNELS_INVALID_URL:Lru/cn/domain/statistics/inetra/ErrorCode;

    .line 26
    new-instance v0, Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v1, "UNABLE_START_VIDEO"

    const/4 v2, 0x7

    const/16 v3, 0x1e

    invoke-direct {v0, v1, v2, v3}, Lru/cn/domain/statistics/inetra/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->UNABLE_START_VIDEO:Lru/cn/domain/statistics/inetra/ErrorCode;

    .line 28
    new-instance v0, Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v1, "UNABLE_START_VIDEO_P2P"

    const/16 v2, 0x8

    const/16 v3, 0x1f

    invoke-direct {v0, v1, v2, v3}, Lru/cn/domain/statistics/inetra/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->UNABLE_START_VIDEO_P2P:Lru/cn/domain/statistics/inetra/ErrorCode;

    .line 30
    new-instance v0, Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v1, "LOW_DOWNLOAD_SPEED"

    const/16 v2, 0x9

    const/16 v3, 0x28

    invoke-direct {v0, v1, v2, v3}, Lru/cn/domain/statistics/inetra/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->LOW_DOWNLOAD_SPEED:Lru/cn/domain/statistics/inetra/ErrorCode;

    .line 32
    new-instance v0, Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v1, "LOW_DOWNLOAD_SPEED_P2P"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v5, v2}, Lru/cn/domain/statistics/inetra/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->LOW_DOWNLOAD_SPEED_P2P:Lru/cn/domain/statistics/inetra/ErrorCode;

    .line 34
    new-instance v0, Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v1, "PLAY_ERROR"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v6, v2}, Lru/cn/domain/statistics/inetra/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->PLAY_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    .line 36
    new-instance v0, Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v1, "PLAY_ERROR_P2P"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v7, v2}, Lru/cn/domain/statistics/inetra/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->PLAY_ERROR_P2P:Lru/cn/domain/statistics/inetra/ErrorCode;

    .line 38
    new-instance v0, Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v1, "ADV_ERROR_PLAY"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v8, v2}, Lru/cn/domain/statistics/inetra/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->ADV_ERROR_PLAY:Lru/cn/domain/statistics/inetra/ErrorCode;

    .line 40
    new-instance v0, Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v1, "ADV_ERROR_LOAD_BANNER"

    const/16 v2, 0xe

    const/16 v3, 0x47

    invoke-direct {v0, v1, v2, v3}, Lru/cn/domain/statistics/inetra/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->ADV_ERROR_LOAD_BANNER:Lru/cn/domain/statistics/inetra/ErrorCode;

    .line 42
    new-instance v0, Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v1, "CHROMECAST_ERROR"

    const/16 v2, 0xf

    const/16 v3, 0x50

    invoke-direct {v0, v1, v2, v3}, Lru/cn/domain/statistics/inetra/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->CHROMECAST_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    .line 3
    const/16 v0, 0x10

    new-array v0, v0, [Lru/cn/domain/statistics/inetra/ErrorCode;

    sget-object v1, Lru/cn/domain/statistics/inetra/ErrorCode;->UNKNOWN_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    aput-object v1, v0, v4

    const/4 v1, 0x1

    sget-object v2, Lru/cn/domain/statistics/inetra/ErrorCode;->CHANNELS_DOMAIN_TIMEOUT:Lru/cn/domain/statistics/inetra/ErrorCode;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lru/cn/domain/statistics/inetra/ErrorCode;->CHANNELS_CONNECTION_TIMEOUT:Lru/cn/domain/statistics/inetra/ErrorCode;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lru/cn/domain/statistics/inetra/ErrorCode;->CHANNELS_HTTP_RESPONSE_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lru/cn/domain/statistics/inetra/ErrorCode;->CHANNELS_SOCKET_TIMEOUT:Lru/cn/domain/statistics/inetra/ErrorCode;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lru/cn/domain/statistics/inetra/ErrorCode;->CHANNELS_FORMAT_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lru/cn/domain/statistics/inetra/ErrorCode;->CHANNELS_INVALID_URL:Lru/cn/domain/statistics/inetra/ErrorCode;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lru/cn/domain/statistics/inetra/ErrorCode;->UNABLE_START_VIDEO:Lru/cn/domain/statistics/inetra/ErrorCode;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lru/cn/domain/statistics/inetra/ErrorCode;->UNABLE_START_VIDEO_P2P:Lru/cn/domain/statistics/inetra/ErrorCode;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lru/cn/domain/statistics/inetra/ErrorCode;->LOW_DOWNLOAD_SPEED:Lru/cn/domain/statistics/inetra/ErrorCode;

    aput-object v2, v0, v1

    sget-object v1, Lru/cn/domain/statistics/inetra/ErrorCode;->LOW_DOWNLOAD_SPEED_P2P:Lru/cn/domain/statistics/inetra/ErrorCode;

    aput-object v1, v0, v5

    sget-object v1, Lru/cn/domain/statistics/inetra/ErrorCode;->PLAY_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/statistics/inetra/ErrorCode;->PLAY_ERROR_P2P:Lru/cn/domain/statistics/inetra/ErrorCode;

    aput-object v1, v0, v7

    sget-object v1, Lru/cn/domain/statistics/inetra/ErrorCode;->ADV_ERROR_PLAY:Lru/cn/domain/statistics/inetra/ErrorCode;

    aput-object v1, v0, v8

    const/16 v1, 0xe

    sget-object v2, Lru/cn/domain/statistics/inetra/ErrorCode;->ADV_ERROR_LOAD_BANNER:Lru/cn/domain/statistics/inetra/ErrorCode;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lru/cn/domain/statistics/inetra/ErrorCode;->CHROMECAST_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    aput-object v2, v0, v1

    sput-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->$VALUES:[Lru/cn/domain/statistics/inetra/ErrorCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 47
    iput p3, p0, Lru/cn/domain/statistics/inetra/ErrorCode;->value:I

    .line 48
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/domain/statistics/inetra/ErrorCode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lru/cn/domain/statistics/inetra/ErrorCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/domain/statistics/inetra/ErrorCode;

    return-object v0
.end method

.method public static values()[Lru/cn/domain/statistics/inetra/ErrorCode;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->$VALUES:[Lru/cn/domain/statistics/inetra/ErrorCode;

    invoke-virtual {v0}, [Lru/cn/domain/statistics/inetra/ErrorCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/domain/statistics/inetra/ErrorCode;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lru/cn/domain/statistics/inetra/ErrorCode;->value:I

    return v0
.end method
