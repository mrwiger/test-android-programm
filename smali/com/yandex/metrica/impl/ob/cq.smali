.class public Lcom/yandex/metrica/impl/ob/cq;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static volatile a:Lcom/yandex/metrica/impl/ob/cq;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/yandex/metrica/impl/ob/cp;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/yandex/metrica/impl/ob/cr;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/content/Context;

.field private e:Lcom/yandex/metrica/impl/ob/cp;

.field private f:Lcom/yandex/metrica/impl/ob/cr;

.field private g:Lcom/yandex/metrica/impl/ob/cr;

.field private h:Lcom/yandex/metrica/impl/ob/cr;

.field private i:Lcom/yandex/metrica/impl/ob/ct;

.field private j:Lcom/yandex/metrica/impl/ob/cs;

.field private k:Lcom/yandex/metrica/impl/ob/cu;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->b:Ljava/util/Map;

    .line 53
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->c:Ljava/util/Map;

    .line 65
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/cq;->d:Landroid/content/Context;

    .line 66
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/cq;
    .locals 3

    .prologue
    .line 42
    sget-object v0, Lcom/yandex/metrica/impl/ob/cq;->a:Lcom/yandex/metrica/impl/ob/cq;

    if-nez v0, :cond_1

    .line 43
    const-class v1, Lcom/yandex/metrica/impl/ob/cq;

    monitor-enter v1

    .line 44
    :try_start_0
    sget-object v0, Lcom/yandex/metrica/impl/ob/cq;->a:Lcom/yandex/metrica/impl/ob/cq;

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Lcom/yandex/metrica/impl/ob/cq;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/yandex/metrica/impl/ob/cq;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/cq;->a:Lcom/yandex/metrica/impl/ob/cq;

    .line 47
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    :cond_1
    sget-object v0, Lcom/yandex/metrica/impl/ob/cq;->a:Lcom/yandex/metrica/impl/ob/cq;

    return-object v0

    .line 47
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/cq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 154
    :cond_0
    return-object p1
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 161
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getNoBackupFilesDir()Ljava/io/File;

    move-result-object v0

    .line 162
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 163
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 164
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/cq;->d:Landroid/content/Context;

    invoke-virtual {v2, p1}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 165
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 167
    invoke-virtual {v2, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v2

    .line 169
    if-eqz v2, :cond_0

    .line 170
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-journal"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 171
    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/cq;->d:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 172
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 176
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 180
    :goto_0
    return-object p1

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized a()Lcom/yandex/metrica/impl/ob/cp;
    .locals 2

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->e:Lcom/yandex/metrica/impl/ob/cp;

    if-nez v0, :cond_0

    .line 85
    const-string v0, "metrica_data.db"

    invoke-static {}, Lcom/yandex/metrica/impl/ob/cn;->b()Lcom/yandex/metrica/impl/ob/cv;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/cq;->a(Ljava/lang/String;Lcom/yandex/metrica/impl/ob/cv;)Lcom/yandex/metrica/impl/ob/cp;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->e:Lcom/yandex/metrica/impl/ob/cp;

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->e:Lcom/yandex/metrica/impl/ob/cp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/yandex/metrica/impl/ob/t;)Lcom/yandex/metrica/impl/ob/cp;
    .locals 3

    .prologue
    .line 69
    monitor-enter p0

    .line 1184
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "db_metrica_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 70
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/cp;

    .line 72
    if-nez v0, :cond_0

    .line 76
    invoke-static {}, Lcom/yandex/metrica/impl/ob/cn;->a()Lcom/yandex/metrica/impl/ob/cv;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/yandex/metrica/impl/ob/cq;->a(Ljava/lang/String;Lcom/yandex/metrica/impl/ob/cv;)Lcom/yandex/metrica/impl/ob/cp;

    move-result-object v0

    .line 77
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/cq;->b:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    :cond_0
    monitor-exit p0

    return-object v0

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method a(Ljava/lang/String;Lcom/yandex/metrica/impl/ob/cv;)Lcom/yandex/metrica/impl/ob/cp;
    .locals 3

    .prologue
    .line 147
    new-instance v0, Lcom/yandex/metrica/impl/ob/cp;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cq;->d:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/cq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p2}, Lcom/yandex/metrica/impl/ob/cp;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/yandex/metrica/impl/ob/cv;)V

    return-object v0
.end method

.method public declared-synchronized b()Lcom/yandex/metrica/impl/ob/cr;
    .locals 3

    .prologue
    .line 102
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->f:Lcom/yandex/metrica/impl/ob/cr;

    if-nez v0, :cond_0

    .line 103
    new-instance v0, Lcom/yandex/metrica/impl/ob/cr;

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/cq;->a()Lcom/yandex/metrica/impl/ob/cp;

    move-result-object v1

    const-string v2, "preferences"

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/cr;-><init>(Lcom/yandex/metrica/impl/ob/cp;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->f:Lcom/yandex/metrica/impl/ob/cr;

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->f:Lcom/yandex/metrica/impl/ob/cr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 102
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Lcom/yandex/metrica/impl/ob/t;)Lcom/yandex/metrica/impl/ob/cr;
    .locals 4

    .prologue
    .line 91
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/t;->toString()Ljava/lang/String;

    move-result-object v1

    .line 92
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/cr;

    .line 93
    if-nez v0, :cond_0

    .line 94
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/ob/cq;->a(Lcom/yandex/metrica/impl/ob/t;)Lcom/yandex/metrica/impl/ob/cp;

    move-result-object v2

    .line 95
    new-instance v0, Lcom/yandex/metrica/impl/ob/cr;

    const-string v3, "preferences"

    invoke-direct {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/cr;-><init>(Lcom/yandex/metrica/impl/ob/cp;Ljava/lang/String;)V

    .line 96
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/cq;->c:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    :cond_0
    monitor-exit p0

    return-object v0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()Lcom/yandex/metrica/impl/ob/cu;
    .locals 3

    .prologue
    .line 109
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->k:Lcom/yandex/metrica/impl/ob/cu;

    if-nez v0, :cond_0

    .line 110
    new-instance v0, Lcom/yandex/metrica/impl/ob/cu;

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/cq;->a()Lcom/yandex/metrica/impl/ob/cp;

    move-result-object v1

    const-string v2, "permissions"

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/cu;-><init>(Lcom/yandex/metrica/impl/ob/cp;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->k:Lcom/yandex/metrica/impl/ob/cu;

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->k:Lcom/yandex/metrica/impl/ob/cu;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 109
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()Lcom/yandex/metrica/impl/ob/cr;
    .locals 3

    .prologue
    .line 116
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->g:Lcom/yandex/metrica/impl/ob/cr;

    if-nez v0, :cond_0

    .line 117
    new-instance v0, Lcom/yandex/metrica/impl/ob/cr;

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/cq;->a()Lcom/yandex/metrica/impl/ob/cp;

    move-result-object v1

    const-string v2, "startup"

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/cr;-><init>(Lcom/yandex/metrica/impl/ob/cp;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->g:Lcom/yandex/metrica/impl/ob/cr;

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->g:Lcom/yandex/metrica/impl/ob/cr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 116
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e()Lcom/yandex/metrica/impl/ob/cr;
    .locals 3

    .prologue
    .line 123
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->h:Lcom/yandex/metrica/impl/ob/cr;

    if-nez v0, :cond_0

    .line 124
    const-string v0, "metrica_client_data.db"

    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/ob/cq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 125
    new-instance v1, Lcom/yandex/metrica/impl/ob/cz;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/cq;->d:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/yandex/metrica/impl/ob/cz;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 126
    new-instance v0, Lcom/yandex/metrica/impl/ob/cr;

    const-string v2, "preferences"

    invoke-direct {v0, v2, v1}, Lcom/yandex/metrica/impl/ob/cr;-><init>(Ljava/lang/String;Lcom/yandex/metrica/impl/ob/cy;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->h:Lcom/yandex/metrica/impl/ob/cr;

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->h:Lcom/yandex/metrica/impl/ob/cr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized f()Lcom/yandex/metrica/impl/ob/ct;
    .locals 3

    .prologue
    .line 132
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->i:Lcom/yandex/metrica/impl/ob/ct;

    if-nez v0, :cond_0

    .line 133
    new-instance v0, Lcom/yandex/metrica/impl/ob/ct;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cq;->d:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/cq;->a()Lcom/yandex/metrica/impl/ob/cp;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/ct;-><init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/cp;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->i:Lcom/yandex/metrica/impl/ob/ct;

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->i:Lcom/yandex/metrica/impl/ob/ct;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized g()Lcom/yandex/metrica/impl/ob/cs;
    .locals 3

    .prologue
    .line 139
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->j:Lcom/yandex/metrica/impl/ob/cs;

    if-nez v0, :cond_0

    .line 140
    new-instance v0, Lcom/yandex/metrica/impl/ob/cs;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cq;->d:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/cq;->a()Lcom/yandex/metrica/impl/ob/cp;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/cs;-><init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/cp;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->j:Lcom/yandex/metrica/impl/ob/cs;

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cq;->j:Lcom/yandex/metrica/impl/ob/cs;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 139
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
