.class Lcom/yandex/metrica/impl/ob/u$c;
.super Lcom/yandex/metrica/impl/ob/u$f;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/impl/ob/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "c"
.end annotation


# instance fields
.field private final a:Lcom/yandex/metrica/impl/ob/fe;

.field private final b:Lcom/yandex/metrica/impl/ob/db;


# direct methods
.method constructor <init>(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/fe;)V
    .locals 1

    .prologue
    .line 289
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/u$f;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    .line 290
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/u$c;->a:Lcom/yandex/metrica/impl/ob/fe;

    .line 291
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->D()Lcom/yandex/metrica/impl/ob/db;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/u$c;->b:Lcom/yandex/metrica/impl/ob/db;

    .line 292
    return-void
.end method


# virtual methods
.method protected a()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 296
    const-string v0, "DONE"

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/u$c;->a:Lcom/yandex/metrica/impl/ob/fe;

    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/ob/fe;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "DONE"

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/u$c;->a:Lcom/yandex/metrica/impl/ob/fe;

    .line 297
    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/ob/fe;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    .line 296
    goto :goto_0
.end method

.method protected b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 303
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$c;->a:Lcom/yandex/metrica/impl/ob/fe;

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/fe;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 304
    const-string v1, "DONE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$c;->b:Lcom/yandex/metrica/impl/ob/db;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/db;->b()V

    .line 308
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$c;->a:Lcom/yandex/metrica/impl/ob/fe;

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/fe;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 309
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 310
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/u$c;->b:Lcom/yandex/metrica/impl/ob/db;

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/ob/db;->c(Ljava/lang/String;)V

    .line 313
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$c;->a:Lcom/yandex/metrica/impl/ob/fe;

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/fe;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 314
    const-string v1, "DONE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 315
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$c;->b:Lcom/yandex/metrica/impl/ob/db;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/db;->a()V

    .line 318
    :cond_2
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$c;->a:Lcom/yandex/metrica/impl/ob/fe;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fe;->d()V

    .line 319
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$c;->a:Lcom/yandex/metrica/impl/ob/fe;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fe;->e()V

    .line 320
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$c;->a:Lcom/yandex/metrica/impl/ob/fe;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fe;->c()V

    .line 321
    return-void
.end method
