.class final Lcom/google/obf/eg$5;
.super Lcom/google/obf/ew;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/obf/eg;->a(Lcom/google/obf/ew;)Lcom/google/obf/ew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/obf/ew",
        "<",
        "Ljava/util/concurrent/atomic/AtomicLong;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/obf/ew;


# direct methods
.method constructor <init>(Lcom/google/obf/ew;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/obf/eg$5;->a:Lcom/google/obf/ew;

    invoke-direct {p0}, Lcom/google/obf/ew;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/obf/ge;)Ljava/util/concurrent/atomic/AtomicLong;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4
    iget-object v0, p0, Lcom/google/obf/eg$5;->a:Lcom/google/obf/ew;

    invoke-virtual {v0, p1}, Lcom/google/obf/ew;->read(Lcom/google/obf/ge;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    .line 5
    new-instance v1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    return-object v1
.end method

.method public a(Lcom/google/obf/gg;Ljava/util/concurrent/atomic/AtomicLong;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2
    iget-object v0, p0, Lcom/google/obf/eg$5;->a:Lcom/google/obf/ew;

    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/obf/ew;->write(Lcom/google/obf/gg;Ljava/lang/Object;)V

    .line 3
    return-void
.end method

.method public synthetic read(Lcom/google/obf/ge;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lcom/google/obf/eg$5;->a(Lcom/google/obf/ge;)Ljava/util/concurrent/atomic/AtomicLong;

    move-result-object v0

    return-object v0
.end method

.method public synthetic write(Lcom/google/obf/gg;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7
    check-cast p2, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/eg$5;->a(Lcom/google/obf/gg;Ljava/util/concurrent/atomic/AtomicLong;)V

    return-void
.end method
