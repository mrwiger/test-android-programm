.class public final Lcom/yandex/metrica/c$c$d$a;
.super Lcom/yandex/metrica/impl/ob/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/c$c$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/c$c$d$a$a;,
        Lcom/yandex/metrica/c$c$d$a$b;
    }
.end annotation


# static fields
.field private static volatile n:[Lcom/yandex/metrica/c$c$d$a;


# instance fields
.field public b:J

.field public c:J

.field public d:I

.field public e:Ljava/lang/String;

.field public f:[B

.field public g:Lcom/yandex/metrica/c$c$b;

.field public h:Lcom/yandex/metrica/c$c$d$a$b;

.field public i:Ljava/lang/String;

.field public j:Lcom/yandex/metrica/c$c$d$a$a;

.field public k:I

.field public l:I

.field public m:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1222
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/d;-><init>()V

    .line 1223
    invoke-virtual {p0}, Lcom/yandex/metrica/c$c$d$a;->e()Lcom/yandex/metrica/c$c$d$a;

    .line 1224
    return-void
.end method

.method public static d()[Lcom/yandex/metrica/c$c$d$a;
    .locals 2

    .prologue
    .line 1173
    sget-object v0, Lcom/yandex/metrica/c$c$d$a;->n:[Lcom/yandex/metrica/c$c$d$a;

    if-nez v0, :cond_1

    .line 1174
    sget-object v1, Lcom/yandex/metrica/impl/ob/c;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1176
    :try_start_0
    sget-object v0, Lcom/yandex/metrica/c$c$d$a;->n:[Lcom/yandex/metrica/c$c$d$a;

    if-nez v0, :cond_0

    .line 1177
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/yandex/metrica/c$c$d$a;

    sput-object v0, Lcom/yandex/metrica/c$c$d$a;->n:[Lcom/yandex/metrica/c$c$d$a;

    .line 1179
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1181
    :cond_1
    sget-object v0, Lcom/yandex/metrica/c$c$d$a;->n:[Lcom/yandex/metrica/c$c$d$a;

    return-object v0

    .line 1179
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(Lcom/yandex/metrica/impl/ob/b;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1246
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/yandex/metrica/c$c$d$a;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/yandex/metrica/impl/ob/b;->a(IJ)V

    .line 1247
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/yandex/metrica/c$c$d$a;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/yandex/metrica/impl/ob/b;->a(IJ)V

    .line 1248
    const/4 v0, 0x3

    iget v1, p0, Lcom/yandex/metrica/c$c$d$a;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->b(II)V

    .line 1249
    iget-object v0, p0, Lcom/yandex/metrica/c$c$d$a;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1250
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/yandex/metrica/c$c$d$a;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(ILjava/lang/String;)V

    .line 1252
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/c$c$d$a;->f:[B

    sget-object v1, Lcom/yandex/metrica/impl/ob/f;->b:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1253
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/yandex/metrica/c$c$d$a;->f:[B

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(I[B)V

    .line 1255
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/c$c$d$a;->g:Lcom/yandex/metrica/c$c$b;

    if-eqz v0, :cond_2

    .line 1256
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/yandex/metrica/c$c$d$a;->g:Lcom/yandex/metrica/c$c$b;

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(ILcom/yandex/metrica/impl/ob/d;)V

    .line 1258
    :cond_2
    iget-object v0, p0, Lcom/yandex/metrica/c$c$d$a;->h:Lcom/yandex/metrica/c$c$d$a$b;

    if-eqz v0, :cond_3

    .line 1259
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/yandex/metrica/c$c$d$a;->h:Lcom/yandex/metrica/c$c$d$a$b;

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(ILcom/yandex/metrica/impl/ob/d;)V

    .line 1261
    :cond_3
    iget-object v0, p0, Lcom/yandex/metrica/c$c$d$a;->i:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1262
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/yandex/metrica/c$c$d$a;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(ILjava/lang/String;)V

    .line 1264
    :cond_4
    iget-object v0, p0, Lcom/yandex/metrica/c$c$d$a;->j:Lcom/yandex/metrica/c$c$d$a$a;

    if-eqz v0, :cond_5

    .line 1265
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/yandex/metrica/c$c$d$a;->j:Lcom/yandex/metrica/c$c$d$a$a;

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(ILcom/yandex/metrica/impl/ob/d;)V

    .line 1267
    :cond_5
    iget v0, p0, Lcom/yandex/metrica/c$c$d$a;->k:I

    if-eqz v0, :cond_6

    .line 1268
    const/16 v0, 0xa

    iget v1, p0, Lcom/yandex/metrica/c$c$d$a;->k:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->b(II)V

    .line 1270
    :cond_6
    iget v0, p0, Lcom/yandex/metrica/c$c$d$a;->l:I

    if-eqz v0, :cond_7

    .line 1271
    const/16 v0, 0xc

    iget v1, p0, Lcom/yandex/metrica/c$c$d$a;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(II)V

    .line 1273
    :cond_7
    iget v0, p0, Lcom/yandex/metrica/c$c$d$a;->m:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_8

    .line 1274
    const/16 v0, 0xd

    iget v1, p0, Lcom/yandex/metrica/c$c$d$a;->m:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(II)V

    .line 1276
    :cond_8
    invoke-super {p0, p1}, Lcom/yandex/metrica/impl/ob/d;->a(Lcom/yandex/metrica/impl/ob/b;)V

    .line 1277
    return-void
.end method

.method protected c()I
    .locals 4

    .prologue
    .line 1281
    invoke-super {p0}, Lcom/yandex/metrica/impl/ob/d;->c()I

    move-result v0

    .line 1282
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/yandex/metrica/c$c$d$a;->b:J

    .line 1283
    invoke-static {v1, v2, v3}, Lcom/yandex/metrica/impl/ob/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1284
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/yandex/metrica/c$c$d$a;->c:J

    .line 1285
    invoke-static {v1, v2, v3}, Lcom/yandex/metrica/impl/ob/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1286
    const/4 v1, 0x3

    iget v2, p0, Lcom/yandex/metrica/c$c$d$a;->d:I

    .line 1287
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1288
    iget-object v1, p0, Lcom/yandex/metrica/c$c$d$a;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1289
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/yandex/metrica/c$c$d$a;->e:Ljava/lang/String;

    .line 1290
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1292
    :cond_0
    iget-object v1, p0, Lcom/yandex/metrica/c$c$d$a;->f:[B

    sget-object v2, Lcom/yandex/metrica/impl/ob/f;->b:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1293
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/yandex/metrica/c$c$d$a;->f:[B

    .line 1294
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 1296
    :cond_1
    iget-object v1, p0, Lcom/yandex/metrica/c$c$d$a;->g:Lcom/yandex/metrica/c$c$b;

    if-eqz v1, :cond_2

    .line 1297
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/yandex/metrica/c$c$d$a;->g:Lcom/yandex/metrica/c$c$b;

    .line 1298
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->b(ILcom/yandex/metrica/impl/ob/d;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1300
    :cond_2
    iget-object v1, p0, Lcom/yandex/metrica/c$c$d$a;->h:Lcom/yandex/metrica/c$c$d$a$b;

    if-eqz v1, :cond_3

    .line 1301
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/yandex/metrica/c$c$d$a;->h:Lcom/yandex/metrica/c$c$d$a$b;

    .line 1302
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->b(ILcom/yandex/metrica/impl/ob/d;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1304
    :cond_3
    iget-object v1, p0, Lcom/yandex/metrica/c$c$d$a;->i:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1305
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/yandex/metrica/c$c$d$a;->i:Ljava/lang/String;

    .line 1306
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1308
    :cond_4
    iget-object v1, p0, Lcom/yandex/metrica/c$c$d$a;->j:Lcom/yandex/metrica/c$c$d$a$a;

    if-eqz v1, :cond_5

    .line 1309
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/yandex/metrica/c$c$d$a;->j:Lcom/yandex/metrica/c$c$d$a$a;

    .line 1310
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->b(ILcom/yandex/metrica/impl/ob/d;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1312
    :cond_5
    iget v1, p0, Lcom/yandex/metrica/c$c$d$a;->k:I

    if-eqz v1, :cond_6

    .line 1313
    const/16 v1, 0xa

    iget v2, p0, Lcom/yandex/metrica/c$c$d$a;->k:I

    .line 1314
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1316
    :cond_6
    iget v1, p0, Lcom/yandex/metrica/c$c$d$a;->l:I

    if-eqz v1, :cond_7

    .line 1317
    const/16 v1, 0xc

    iget v2, p0, Lcom/yandex/metrica/c$c$d$a;->l:I

    .line 1318
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1320
    :cond_7
    iget v1, p0, Lcom/yandex/metrica/c$c$d$a;->m:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_8

    .line 1321
    const/16 v1, 0xd

    iget v2, p0, Lcom/yandex/metrica/c$c$d$a;->m:I

    .line 1322
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1324
    :cond_8
    return v0
.end method

.method public e()Lcom/yandex/metrica/c$c$d$a;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1227
    iput-wide v4, p0, Lcom/yandex/metrica/c$c$d$a;->b:J

    .line 1228
    iput-wide v4, p0, Lcom/yandex/metrica/c$c$d$a;->c:J

    .line 1229
    iput v1, p0, Lcom/yandex/metrica/c$c$d$a;->d:I

    .line 1230
    const-string v0, ""

    iput-object v0, p0, Lcom/yandex/metrica/c$c$d$a;->e:Ljava/lang/String;

    .line 1231
    sget-object v0, Lcom/yandex/metrica/impl/ob/f;->b:[B

    iput-object v0, p0, Lcom/yandex/metrica/c$c$d$a;->f:[B

    .line 1232
    iput-object v2, p0, Lcom/yandex/metrica/c$c$d$a;->g:Lcom/yandex/metrica/c$c$b;

    .line 1233
    iput-object v2, p0, Lcom/yandex/metrica/c$c$d$a;->h:Lcom/yandex/metrica/c$c$d$a$b;

    .line 1234
    const-string v0, ""

    iput-object v0, p0, Lcom/yandex/metrica/c$c$d$a;->i:Ljava/lang/String;

    .line 1235
    iput-object v2, p0, Lcom/yandex/metrica/c$c$d$a;->j:Lcom/yandex/metrica/c$c$d$a$a;

    .line 1236
    iput v1, p0, Lcom/yandex/metrica/c$c$d$a;->k:I

    .line 1237
    iput v1, p0, Lcom/yandex/metrica/c$c$d$a;->l:I

    .line 1238
    iput v3, p0, Lcom/yandex/metrica/c$c$d$a;->m:I

    .line 1239
    iput v3, p0, Lcom/yandex/metrica/c$c$d$a;->a:I

    .line 1240
    return-object p0
.end method
