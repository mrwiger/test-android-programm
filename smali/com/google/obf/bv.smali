.class final Lcom/google/obf/bv;
.super Lcom/google/obf/bt;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/aq;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/bv$a;
    }
.end annotation


# instance fields
.field private e:Lcom/google/obf/bv$a;

.field private g:I

.field private h:J

.field private i:Z

.field private final j:Lcom/google/obf/br;

.field private k:J

.field private l:Lcom/google/obf/bw$d;

.field private m:Lcom/google/obf/bw$b;

.field private n:J

.field private o:J

.field private p:J

.field private q:J


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/obf/bt;-><init>()V

    .line 2
    new-instance v0, Lcom/google/obf/br;

    invoke-direct {v0}, Lcom/google/obf/br;-><init>()V

    iput-object v0, p0, Lcom/google/obf/bv;->j:Lcom/google/obf/br;

    .line 3
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/obf/bv;->k:J

    return-void
.end method

.method private static a(BLcom/google/obf/bv$a;)I
    .locals 2

    .prologue
    .line 80
    iget v0, p1, Lcom/google/obf/bv$a;->e:I

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/google/obf/bs;->a(BII)I

    move-result v0

    .line 81
    iget-object v1, p1, Lcom/google/obf/bv$a;->d:[Lcom/google/obf/bw$c;

    aget-object v0, v1, v0

    iget-boolean v0, v0, Lcom/google/obf/bw$c;->a:Z

    if-nez v0, :cond_0

    .line 82
    iget-object v0, p1, Lcom/google/obf/bv$a;->a:Lcom/google/obf/bw$d;

    iget v0, v0, Lcom/google/obf/bw$d;->g:I

    .line 84
    :goto_0
    return v0

    .line 83
    :cond_0
    iget-object v0, p1, Lcom/google/obf/bv$a;->a:Lcom/google/obf/bw$d;

    iget v0, v0, Lcom/google/obf/bw$d;->h:I

    goto :goto_0
.end method

.method static a(Lcom/google/obf/dw;J)V
    .locals 7

    .prologue
    const-wide/16 v4, 0xff

    .line 74
    invoke-virtual {p0}, Lcom/google/obf/dw;->c()I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->b(I)V

    .line 75
    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    invoke-virtual {p0}, Lcom/google/obf/dw;->c()I

    move-result v1

    add-int/lit8 v1, v1, -0x4

    and-long v2, p1, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 76
    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    invoke-virtual {p0}, Lcom/google/obf/dw;->c()I

    move-result v1

    add-int/lit8 v1, v1, -0x3

    const/16 v2, 0x8

    ushr-long v2, p1, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 77
    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    invoke-virtual {p0}, Lcom/google/obf/dw;->c()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    const/16 v2, 0x10

    ushr-long v2, p1, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 78
    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    invoke-virtual {p0}, Lcom/google/obf/dw;->c()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/16 v2, 0x18

    ushr-long v2, p1, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 79
    return-void
.end method

.method static a(Lcom/google/obf/dw;)Z
    .locals 2

    .prologue
    .line 4
    const/4 v0, 0x1

    const/4 v1, 0x1

    :try_start_0
    invoke-static {v0, p0, v1}, Lcom/google/obf/bw;->a(ILcom/google/obf/dw;Z)Z
    :try_end_0
    .catch Lcom/google/obf/s; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 6
    :goto_0
    return v0

    .line 5
    :catch_0
    move-exception v0

    .line 6
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/obf/ak;Lcom/google/obf/ao;)I
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 12
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/obf/bv;->p:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    .line 13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/bv;->e:Lcom/google/obf/bv$a;

    if-nez v2, :cond_0

    .line 14
    invoke-interface/range {p1 .. p1}, Lcom/google/obf/ak;->d()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/obf/bv;->n:J

    .line 15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/bv;->a:Lcom/google/obf/dw;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Lcom/google/obf/bv;->a(Lcom/google/obf/ak;Lcom/google/obf/dw;)Lcom/google/obf/bv$a;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/obf/bv;->e:Lcom/google/obf/bv$a;

    .line 16
    invoke-interface/range {p1 .. p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/obf/bv;->o:J

    .line 17
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/bv;->d:Lcom/google/obf/al;

    move-object/from16 v0, p0

    invoke-interface {v2, v0}, Lcom/google/obf/al;->a(Lcom/google/obf/aq;)V

    .line 18
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/obf/bv;->n:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 19
    const-wide/16 v2, 0x0

    invoke-interface/range {p1 .. p1}, Lcom/google/obf/ak;->d()J

    move-result-wide v4

    const-wide/16 v6, 0x1f40

    sub-long/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    move-object/from16 v0, p2

    iput-wide v2, v0, Lcom/google/obf/ao;->a:J

    .line 20
    const/4 v2, 0x1

    .line 58
    :goto_0
    return v2

    .line 21
    :cond_0
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/obf/bv;->n:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    const-wide/16 v2, -0x1

    .line 22
    :goto_1
    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/obf/bv;->p:J

    .line 23
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 24
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/bv;->e:Lcom/google/obf/bv$a;

    iget-object v2, v2, Lcom/google/obf/bv$a;->a:Lcom/google/obf/bw$d;

    iget-object v2, v2, Lcom/google/obf/bw$d;->j:[B

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 25
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/bv;->e:Lcom/google/obf/bv$a;

    iget-object v2, v2, Lcom/google/obf/bv$a;->c:[B

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 26
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/obf/bv;->n:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    const-wide/16 v2, -0x1

    .line 27
    :goto_2
    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/obf/bv;->q:J

    .line 28
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/obf/bv;->c:Lcom/google/obf/ar;

    const/4 v2, 0x0

    const-string v3, "audio/vorbis"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/bv;->e:Lcom/google/obf/bv$a;

    iget-object v4, v4, Lcom/google/obf/bv$a;->a:Lcom/google/obf/bw$d;

    iget v4, v4, Lcom/google/obf/bw$d;->e:I

    const v5, 0xfe01

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/obf/bv;->q:J

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/obf/bv;->e:Lcom/google/obf/bv$a;

    iget-object v8, v8, Lcom/google/obf/bv$a;->a:Lcom/google/obf/bw$d;

    iget v8, v8, Lcom/google/obf/bw$d;->b:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/obf/bv;->e:Lcom/google/obf/bv$a;

    iget-object v9, v9, Lcom/google/obf/bv$a;->a:Lcom/google/obf/bw$d;

    iget-wide v14, v9, Lcom/google/obf/bw$d;->c:J

    long-to-int v9, v14

    const/4 v11, 0x0

    invoke-static/range {v2 .. v11}, Lcom/google/obf/q;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)Lcom/google/obf/q;

    move-result-object v2

    invoke-interface {v12, v2}, Lcom/google/obf/ar;->a(Lcom/google/obf/q;)V

    .line 29
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/obf/bv;->n:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 30
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/bv;->j:Lcom/google/obf/br;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/obf/bv;->n:J

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/obf/bv;->o:J

    sub-long/2addr v4, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/obf/bv;->p:J

    invoke-virtual {v2, v4, v5, v6, v7}, Lcom/google/obf/br;->a(JJ)V

    .line 31
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/obf/bv;->o:J

    move-object/from16 v0, p2

    iput-wide v2, v0, Lcom/google/obf/ao;->a:J

    .line 32
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 22
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/bv;->b:Lcom/google/obf/bq;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/obf/bq;->a(Lcom/google/obf/ak;)J

    move-result-wide v2

    goto/16 :goto_1

    .line 27
    :cond_2
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/obf/bv;->p:J

    const-wide/32 v4, 0xf4240

    mul-long/2addr v2, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/bv;->e:Lcom/google/obf/bv$a;

    iget-object v4, v4, Lcom/google/obf/bv$a;->a:Lcom/google/obf/bw$d;

    iget-wide v4, v4, Lcom/google/obf/bw$d;->c:J

    div-long/2addr v2, v4

    goto :goto_2

    .line 33
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/obf/bv;->i:Z

    if-nez v2, :cond_5

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/obf/bv;->k:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-lez v2, :cond_5

    .line 34
    invoke-static/range {p1 .. p1}, Lcom/google/obf/bs;->a(Lcom/google/obf/ak;)V

    .line 35
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/bv;->j:Lcom/google/obf/br;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/obf/bv;->k:J

    move-object/from16 v0, p1

    invoke-virtual {v2, v4, v5, v0}, Lcom/google/obf/br;->a(JLcom/google/obf/ak;)J

    move-result-wide v2

    .line 36
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_4

    .line 37
    move-object/from16 v0, p2

    iput-wide v2, v0, Lcom/google/obf/ao;->a:J

    .line 38
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 39
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/bv;->b:Lcom/google/obf/bq;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/obf/bv;->k:J

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v4, v5}, Lcom/google/obf/bq;->a(Lcom/google/obf/ak;J)J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/obf/bv;->h:J

    .line 40
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/bv;->l:Lcom/google/obf/bw$d;

    iget v2, v2, Lcom/google/obf/bw$d;->g:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/obf/bv;->g:I

    .line 41
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/obf/bv;->i:Z

    .line 42
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/bv;->b:Lcom/google/obf/bq;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/obf/bv;->a:Lcom/google/obf/dw;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v3}, Lcom/google/obf/bq;->a(Lcom/google/obf/ak;Lcom/google/obf/dw;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 43
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/bv;->a:Lcom/google/obf/dw;

    iget-object v2, v2, Lcom/google/obf/dw;->a:[B

    const/4 v3, 0x0

    aget-byte v2, v2, v3

    and-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    if-eq v2, v3, :cond_7

    .line 44
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/bv;->a:Lcom/google/obf/dw;

    iget-object v2, v2, Lcom/google/obf/dw;->a:[B

    const/4 v3, 0x0

    aget-byte v2, v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/obf/bv;->e:Lcom/google/obf/bv$a;

    invoke-static {v2, v3}, Lcom/google/obf/bv;->a(BLcom/google/obf/bv$a;)I

    move-result v10

    .line 45
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/obf/bv;->i:Z

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/obf/bv;->g:I

    add-int/2addr v2, v10

    div-int/lit8 v2, v2, 0x4

    .line 47
    :goto_3
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/obf/bv;->h:J

    int-to-long v6, v2

    add-long/2addr v4, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/obf/bv;->k:J

    cmp-long v3, v4, v6

    if-ltz v3, :cond_6

    .line 48
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/obf/bv;->a:Lcom/google/obf/dw;

    int-to-long v4, v2

    invoke-static {v3, v4, v5}, Lcom/google/obf/bv;->a(Lcom/google/obf/dw;J)V

    .line 49
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/obf/bv;->h:J

    const-wide/32 v6, 0xf4240

    mul-long/2addr v4, v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/obf/bv;->e:Lcom/google/obf/bv$a;

    iget-object v3, v3, Lcom/google/obf/bv$a;->a:Lcom/google/obf/bw$d;

    iget-wide v6, v3, Lcom/google/obf/bw$d;->c:J

    div-long/2addr v4, v6

    .line 50
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/obf/bv;->c:Lcom/google/obf/ar;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/obf/bv;->a:Lcom/google/obf/dw;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/obf/bv;->a:Lcom/google/obf/dw;

    invoke-virtual {v7}, Lcom/google/obf/dw;->c()I

    move-result v7

    invoke-interface {v3, v6, v7}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 51
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/obf/bv;->c:Lcom/google/obf/ar;

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/obf/bv;->a:Lcom/google/obf/dw;

    invoke-virtual {v7}, Lcom/google/obf/dw;->c()I

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-interface/range {v3 .. v9}, Lcom/google/obf/ar;->a(JIII[B)V

    .line 52
    const-wide/16 v4, -0x1

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/google/obf/bv;->k:J

    .line 53
    :cond_6
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/obf/bv;->i:Z

    .line 54
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/obf/bv;->h:J

    int-to-long v2, v2

    add-long/2addr v2, v4

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/obf/bv;->h:J

    .line 55
    move-object/from16 v0, p0

    iput v10, v0, Lcom/google/obf/bv;->g:I

    .line 56
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/bv;->a:Lcom/google/obf/dw;

    invoke-virtual {v2}, Lcom/google/obf/dw;->a()V

    .line 57
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 46
    :cond_8
    const/4 v2, 0x0

    goto :goto_3

    .line 58
    :cond_9
    const/4 v2, -0x1

    goto/16 :goto_0
.end method

.method a(Lcom/google/obf/ak;Lcom/google/obf/dw;)Lcom/google/obf/bv$a;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 59
    iget-object v0, p0, Lcom/google/obf/bv;->l:Lcom/google/obf/bw$d;

    if-nez v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/google/obf/bv;->b:Lcom/google/obf/bq;

    invoke-virtual {v0, p1, p2}, Lcom/google/obf/bq;->a(Lcom/google/obf/ak;Lcom/google/obf/dw;)Z

    .line 61
    invoke-static {p2}, Lcom/google/obf/bw;->a(Lcom/google/obf/dw;)Lcom/google/obf/bw$d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/bv;->l:Lcom/google/obf/bw$d;

    .line 62
    invoke-virtual {p2}, Lcom/google/obf/dw;->a()V

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/google/obf/bv;->m:Lcom/google/obf/bw$b;

    if-nez v0, :cond_1

    .line 64
    iget-object v0, p0, Lcom/google/obf/bv;->b:Lcom/google/obf/bq;

    invoke-virtual {v0, p1, p2}, Lcom/google/obf/bq;->a(Lcom/google/obf/ak;Lcom/google/obf/dw;)Z

    .line 65
    invoke-static {p2}, Lcom/google/obf/bw;->b(Lcom/google/obf/dw;)Lcom/google/obf/bw$b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/bv;->m:Lcom/google/obf/bw$b;

    .line 66
    invoke-virtual {p2}, Lcom/google/obf/dw;->a()V

    .line 67
    :cond_1
    iget-object v0, p0, Lcom/google/obf/bv;->b:Lcom/google/obf/bq;

    invoke-virtual {v0, p1, p2}, Lcom/google/obf/bq;->a(Lcom/google/obf/ak;Lcom/google/obf/dw;)Z

    .line 68
    invoke-virtual {p2}, Lcom/google/obf/dw;->c()I

    move-result v0

    new-array v3, v0, [B

    .line 69
    iget-object v0, p2, Lcom/google/obf/dw;->a:[B

    invoke-virtual {p2}, Lcom/google/obf/dw;->c()I

    move-result v1

    invoke-static {v0, v2, v3, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 70
    iget-object v0, p0, Lcom/google/obf/bv;->l:Lcom/google/obf/bw$d;

    iget v0, v0, Lcom/google/obf/bw$d;->b:I

    invoke-static {p2, v0}, Lcom/google/obf/bw;->a(Lcom/google/obf/dw;I)[Lcom/google/obf/bw$c;

    move-result-object v4

    .line 71
    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Lcom/google/obf/bw;->a(I)I

    move-result v5

    .line 72
    invoke-virtual {p2}, Lcom/google/obf/dw;->a()V

    .line 73
    new-instance v0, Lcom/google/obf/bv$a;

    iget-object v1, p0, Lcom/google/obf/bv;->l:Lcom/google/obf/bw$d;

    iget-object v2, p0, Lcom/google/obf/bv;->m:Lcom/google/obf/bw$b;

    invoke-direct/range {v0 .. v5}, Lcom/google/obf/bv$a;-><init>(Lcom/google/obf/bw$d;Lcom/google/obf/bw$b;[B[Lcom/google/obf/bw$c;I)V

    return-object v0
.end method

.method public a()Z
    .locals 4

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/obf/bv;->e:Lcom/google/obf/bv$a;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/obf/bv;->n:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(J)J
    .locals 7

    .prologue
    .line 86
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 87
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/obf/bv;->k:J

    .line 88
    iget-wide v0, p0, Lcom/google/obf/bv;->o:J

    .line 90
    :goto_0
    return-wide v0

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/google/obf/bv;->e:Lcom/google/obf/bv$a;

    iget-object v0, v0, Lcom/google/obf/bv$a;->a:Lcom/google/obf/bw$d;

    iget-wide v0, v0, Lcom/google/obf/bw$d;->c:J

    mul-long/2addr v0, p1

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/obf/bv;->k:J

    .line 90
    iget-wide v0, p0, Lcom/google/obf/bv;->o:J

    iget-wide v2, p0, Lcom/google/obf/bv;->n:J

    iget-wide v4, p0, Lcom/google/obf/bv;->o:J

    sub-long/2addr v2, v4

    mul-long/2addr v2, p1

    iget-wide v4, p0, Lcom/google/obf/bv;->q:J

    div-long/2addr v2, v4

    const-wide/16 v4, 0xfa0

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 7
    invoke-super {p0}, Lcom/google/obf/bt;->b()V

    .line 8
    iput v2, p0, Lcom/google/obf/bv;->g:I

    .line 9
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/obf/bv;->h:J

    .line 10
    iput-boolean v2, p0, Lcom/google/obf/bv;->i:Z

    .line 11
    return-void
.end method
