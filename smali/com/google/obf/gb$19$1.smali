.class Lcom/google/obf/gb$19$1;
.super Lcom/google/obf/ew;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/obf/gb$19;->a(Lcom/google/obf/eg;Lcom/google/obf/gd;)Lcom/google/obf/ew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/obf/ew",
        "<",
        "Ljava/sql/Timestamp;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/obf/ew;

.field final synthetic b:Lcom/google/obf/gb$19;


# direct methods
.method constructor <init>(Lcom/google/obf/gb$19;Lcom/google/obf/ew;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/obf/gb$19$1;->b:Lcom/google/obf/gb$19;

    iput-object p2, p0, Lcom/google/obf/gb$19$1;->a:Lcom/google/obf/ew;

    invoke-direct {p0}, Lcom/google/obf/ew;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/obf/ge;)Ljava/sql/Timestamp;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2
    iget-object v0, p0, Lcom/google/obf/gb$19$1;->a:Lcom/google/obf/ew;

    invoke-virtual {v0, p1}, Lcom/google/obf/ew;->read(Lcom/google/obf/ge;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    .line 3
    if-eqz v0, :cond_0

    new-instance v1, Ljava/sql/Timestamp;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/sql/Timestamp;-><init>(J)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/obf/gg;Ljava/sql/Timestamp;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4
    iget-object v0, p0, Lcom/google/obf/gb$19$1;->a:Lcom/google/obf/ew;

    invoke-virtual {v0, p1, p2}, Lcom/google/obf/ew;->write(Lcom/google/obf/gg;Ljava/lang/Object;)V

    .line 5
    return-void
.end method

.method public synthetic read(Lcom/google/obf/ge;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lcom/google/obf/gb$19$1;->a(Lcom/google/obf/ge;)Ljava/sql/Timestamp;

    move-result-object v0

    return-object v0
.end method

.method public synthetic write(Lcom/google/obf/gg;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7
    check-cast p2, Ljava/sql/Timestamp;

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/gb$19$1;->a(Lcom/google/obf/gg;Ljava/sql/Timestamp;)V

    return-void
.end method
