.class public Lru/cn/domain/KidsObject;
.super Ljava/lang/Object;
.source "KidsObject.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/domain/KidsObject$KidsModeListener;
    }
.end annotation


# static fields
.field private static ageChanged:Z

.field private static checkTimerIsInit:Z

.field private static final handler:Landroid/os/Handler;

.field private static kidsListener:Lru/cn/domain/KidsObject$KidsModeListener;


# instance fields
.field private birthYear:I

.field private isAllowByLimits:Z

.field private isKidsMode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    const/4 v0, 0x0

    sput-boolean v0, Lru/cn/domain/KidsObject;->checkTimerIsInit:Z

    .line 32
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lru/cn/domain/KidsObject;->handler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;)Z
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-static {p0}, Lru/cn/domain/KidsObject;->isAllowByLimits(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100()Lru/cn/domain/KidsObject$KidsModeListener;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lru/cn/domain/KidsObject;->kidsListener:Lru/cn/domain/KidsObject$KidsModeListener;

    return-object v0
.end method

.method static synthetic access$200()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lru/cn/domain/KidsObject;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$302(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 18
    sput-boolean p0, Lru/cn/domain/KidsObject;->checkTimerIsInit:Z

    return p0
.end method

.method public static checkPassword(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 106
    if-eqz p0, :cond_3

    if-eqz p1, :cond_3

    .line 107
    const/4 v6, 0x0

    .line 109
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://ru.cn.launcher/check_password"

    .line 110
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 115
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 116
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 117
    const-string v0, "is_correct_password"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-ne v0, v7, :cond_1

    move v0, v7

    .line 120
    :goto_0
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 123
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_0
    :goto_1
    return v0

    .restart local v6    # "c":Landroid/database/Cursor;
    :cond_1
    move v0, v8

    .line 117
    goto :goto_0

    .line 120
    :cond_2
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v6    # "c":Landroid/database/Cursor;
    :cond_3
    move v0, v8

    .line 123
    goto :goto_1

    .line 120
    .restart local v6    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 121
    :cond_4
    throw v0
.end method

.method public static getAgeFilterIsNeed(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 127
    invoke-static {p0}, Lru/cn/domain/KidsObject;->isKidsMode(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 128
    const/4 v1, 0x0

    .line 132
    :goto_0
    return-object v1

    .line 131
    :cond_0
    invoke-static {}, Lru/cn/utils/Utils;->getCalendar()Ljava/util/Calendar;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {p0}, Lru/cn/domain/KidsObject;->getBirthYear(Landroid/content/Context;)I

    move-result v2

    sub-int v0, v1, v2

    .line 132
    .local v0, "age":I
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static getBirthYear(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 182
    invoke-static {p0}, Lru/cn/domain/KidsObject;->getKidsObject(Landroid/content/Context;)Lru/cn/domain/KidsObject;

    move-result-object v0

    .line 183
    .local v0, "o":Lru/cn/domain/KidsObject;
    if-eqz v0, :cond_0

    .line 184
    iget v1, v0, Lru/cn/domain/KidsObject;->birthYear:I

    .line 187
    :goto_0
    return v1

    :cond_0
    invoke-static {}, Lru/cn/utils/Utils;->getCalendar()Ljava/util/Calendar;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    goto :goto_0
.end method

.method private static getKidsObject(Landroid/content/Context;)Lru/cn/domain/KidsObject;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 56
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://ru.cn.launcher/kids_mode"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 57
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    .line 58
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 60
    new-instance v9, Lru/cn/domain/KidsObject;

    invoke-direct {v9}, Lru/cn/domain/KidsObject;-><init>()V

    .line 61
    .local v9, "ret":Lru/cn/domain/KidsObject;
    const-string v0, "is_kids_mode"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v11, v0, :cond_1

    move v0, v11

    :goto_0
    iput-boolean v0, v9, Lru/cn/domain/KidsObject;->isKidsMode:Z

    .line 62
    const-string v0, "is_allow_by_limits"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v11, v0, :cond_2

    move v0, v11

    :goto_1
    iput-boolean v0, v9, Lru/cn/domain/KidsObject;->isAllowByLimits:Z

    .line 63
    const-string v0, "birth_year"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v9, Lru/cn/domain/KidsObject;->birthYear:I

    .line 65
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 67
    iget-boolean v0, v9, Lru/cn/domain/KidsObject;->isKidsMode:Z

    if-eqz v0, :cond_3

    .line 68
    const/4 v0, 0x1

    invoke-static {v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->setAppMode(I)V

    .line 73
    :goto_2
    invoke-static {p0, v9}, Lru/cn/domain/KidsObject;->isKidsModeChanged(Landroid/content/Context;Lru/cn/domain/KidsObject;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "content"

    .line 75
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "ru.cn.api.tv"

    .line 76
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "clear_cache"

    .line 77
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v8

    .line 79
    .local v8, "reloadUri":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v8, v1, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 87
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v8    # "reloadUri":Landroid/net/Uri;
    .end local v9    # "ret":Lru/cn/domain/KidsObject;
    :cond_0
    :goto_3
    return-object v9

    .restart local v6    # "c":Landroid/database/Cursor;
    .restart local v9    # "ret":Lru/cn/domain/KidsObject;
    :cond_1
    move v0, v12

    .line 61
    goto :goto_0

    :cond_2
    move v0, v12

    .line 62
    goto :goto_1

    .line 70
    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->setAppMode(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 85
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v9    # "ret":Lru/cn/domain/KidsObject;
    :catch_0
    move-exception v7

    .line 86
    .local v7, "e":Ljava/lang/Exception;
    invoke-static {v7}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    move-object v9, v10

    .line 87
    goto :goto_3

    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v6    # "c":Landroid/database/Cursor;
    :cond_4
    move-object v9, v10

    .line 84
    goto :goto_3
.end method

.method private static initCheckTimeLimit(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 156
    sget-boolean v1, Lru/cn/domain/KidsObject;->checkTimerIsInit:Z

    if-nez v1, :cond_0

    .line 157
    const/16 v0, 0x1388

    .line 158
    .local v0, "delay":I
    sget-object v1, Lru/cn/domain/KidsObject;->handler:Landroid/os/Handler;

    new-instance v2, Lru/cn/domain/KidsObject$1;

    invoke-direct {v2, p0}, Lru/cn/domain/KidsObject$1;-><init>(Landroid/content/Context;)V

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 177
    const/4 v1, 0x1

    sput-boolean v1, Lru/cn/domain/KidsObject;->checkTimerIsInit:Z

    .line 179
    .end local v0    # "delay":I
    :cond_0
    return-void
.end method

.method public static isAgeChanged()Z
    .locals 1

    .prologue
    .line 140
    sget-boolean v0, Lru/cn/domain/KidsObject;->ageChanged:Z

    return v0
.end method

.method private static isAllowByLimits(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 148
    invoke-static {p0}, Lru/cn/domain/KidsObject;->getKidsObject(Landroid/content/Context;)Lru/cn/domain/KidsObject;

    move-result-object v0

    .line 149
    .local v0, "o":Lru/cn/domain/KidsObject;
    if-eqz v0, :cond_0

    .line 150
    iget-boolean v1, v0, Lru/cn/domain/KidsObject;->isAllowByLimits:Z

    .line 152
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isKidsMode(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 92
    invoke-static {}, Lru/cn/utils/Utils;->isLauncherInstalled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 102
    :cond_0
    :goto_0
    return v1

    .line 95
    :cond_1
    invoke-static {p0}, Lru/cn/domain/KidsObject;->getKidsObject(Landroid/content/Context;)Lru/cn/domain/KidsObject;

    move-result-object v0

    .line 96
    .local v0, "o":Lru/cn/domain/KidsObject;
    if-eqz v0, :cond_0

    .line 97
    iget-boolean v1, v0, Lru/cn/domain/KidsObject;->isKidsMode:Z

    if-eqz v1, :cond_2

    .line 98
    invoke-static {p0}, Lru/cn/domain/KidsObject;->initCheckTimeLimit(Landroid/content/Context;)V

    .line 100
    :cond_2
    iget-boolean v1, v0, Lru/cn/domain/KidsObject;->isKidsMode:Z

    goto :goto_0
.end method

.method private static isKidsModeChanged(Landroid/content/Context;Lru/cn/domain/KidsObject;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "obj"    # Lru/cn/domain/KidsObject;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 37
    const-string v6, "kids_object"

    invoke-virtual {p0, v6, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 39
    .local v3, "preferences":Landroid/content/SharedPreferences;
    const-string v6, "isKidsMode"

    iget-boolean v7, p1, Lru/cn/domain/KidsObject;->isKidsMode:Z

    invoke-interface {v3, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 40
    .local v2, "isKidsMode":Z
    const-string v6, "birthYear"

    iget v7, p1, Lru/cn/domain/KidsObject;->birthYear:I

    invoke-interface {v3, v6, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 42
    .local v0, "birthYear":I
    iget v6, p1, Lru/cn/domain/KidsObject;->birthYear:I

    if-eq v0, v6, :cond_0

    .line 43
    invoke-static {v5}, Lru/cn/domain/KidsObject;->setAgeChanged(Z)V

    .line 46
    :cond_0
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 47
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v6, "isKidsMode"

    iget-boolean v7, p1, Lru/cn/domain/KidsObject;->isKidsMode:Z

    invoke-interface {v1, v6, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 48
    const-string v6, "birthYear"

    iget v7, p1, Lru/cn/domain/KidsObject;->birthYear:I

    invoke-interface {v1, v6, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 49
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 51
    iget-boolean v6, p1, Lru/cn/domain/KidsObject;->isKidsMode:Z

    if-ne v2, v6, :cond_1

    iget v6, p1, Lru/cn/domain/KidsObject;->birthYear:I

    if-eq v0, v6, :cond_2

    :cond_1
    move v4, v5

    :cond_2
    return v4
.end method

.method public static setAgeChanged(Z)V
    .locals 0
    .param p0, "ageChanged"    # Z

    .prologue
    .line 144
    sput-boolean p0, Lru/cn/domain/KidsObject;->ageChanged:Z

    .line 145
    return-void
.end method

.method public static setKidsModeListener(Lru/cn/domain/KidsObject$KidsModeListener;)V
    .locals 0
    .param p0, "listener"    # Lru/cn/domain/KidsObject$KidsModeListener;

    .prologue
    .line 136
    sput-object p0, Lru/cn/domain/KidsObject;->kidsListener:Lru/cn/domain/KidsObject$KidsModeListener;

    .line 137
    return-void
.end method
