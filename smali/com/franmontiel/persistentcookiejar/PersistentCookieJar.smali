.class public Lcom/franmontiel/persistentcookiejar/PersistentCookieJar;
.super Ljava/lang/Object;
.source "PersistentCookieJar.java"

# interfaces
.implements Lcom/franmontiel/persistentcookiejar/ClearableCookieJar;


# instance fields
.field private cache:Lcom/franmontiel/persistentcookiejar/cache/CookieCache;

.field private persistor:Lcom/franmontiel/persistentcookiejar/persistence/CookiePersistor;


# direct methods
.method public constructor <init>(Lcom/franmontiel/persistentcookiejar/cache/CookieCache;Lcom/franmontiel/persistentcookiejar/persistence/CookiePersistor;)V
    .locals 2
    .param p1, "cache"    # Lcom/franmontiel/persistentcookiejar/cache/CookieCache;
    .param p2, "persistor"    # Lcom/franmontiel/persistentcookiejar/persistence/CookiePersistor;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/franmontiel/persistentcookiejar/PersistentCookieJar;->cache:Lcom/franmontiel/persistentcookiejar/cache/CookieCache;

    .line 36
    iput-object p2, p0, Lcom/franmontiel/persistentcookiejar/PersistentCookieJar;->persistor:Lcom/franmontiel/persistentcookiejar/persistence/CookiePersistor;

    .line 38
    iget-object v0, p0, Lcom/franmontiel/persistentcookiejar/PersistentCookieJar;->cache:Lcom/franmontiel/persistentcookiejar/cache/CookieCache;

    invoke-interface {p2}, Lcom/franmontiel/persistentcookiejar/persistence/CookiePersistor;->loadAll()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/franmontiel/persistentcookiejar/cache/CookieCache;->addAll(Ljava/util/Collection;)V

    .line 39
    return-void
.end method

.method private static isCookieExpired(Lokhttp3/Cookie;)Z
    .locals 4
    .param p0, "cookie"    # Lokhttp3/Cookie;

    .prologue
    .line 70
    invoke-virtual {p0}, Lokhttp3/Cookie;->expiresAt()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized loadForRequest(Lokhttp3/HttpUrl;)Ljava/util/List;
    .locals 5
    .param p1, "url"    # Lokhttp3/HttpUrl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lokhttp3/HttpUrl;",
            ")",
            "Ljava/util/List",
            "<",
            "Lokhttp3/Cookie;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 50
    .local v2, "removedCookies":Ljava/util/List;, "Ljava/util/List<Lokhttp3/Cookie;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 52
    .local v3, "validCookies":Ljava/util/List;, "Ljava/util/List<Lokhttp3/Cookie;>;"
    iget-object v4, p0, Lcom/franmontiel/persistentcookiejar/PersistentCookieJar;->cache:Lcom/franmontiel/persistentcookiejar/cache/CookieCache;

    invoke-interface {v4}, Lcom/franmontiel/persistentcookiejar/cache/CookieCache;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lokhttp3/Cookie;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 53
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lokhttp3/Cookie;

    .line 55
    .local v0, "currentCookie":Lokhttp3/Cookie;
    invoke-static {v0}, Lcom/franmontiel/persistentcookiejar/PersistentCookieJar;->isCookieExpired(Lokhttp3/Cookie;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 56
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 49
    .end local v0    # "currentCookie":Lokhttp3/Cookie;
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lokhttp3/Cookie;>;"
    .end local v2    # "removedCookies":Ljava/util/List;, "Ljava/util/List<Lokhttp3/Cookie;>;"
    .end local v3    # "validCookies":Ljava/util/List;, "Ljava/util/List<Lokhttp3/Cookie;>;"
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 59
    .restart local v0    # "currentCookie":Lokhttp3/Cookie;
    .restart local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lokhttp3/Cookie;>;"
    .restart local v2    # "removedCookies":Ljava/util/List;, "Ljava/util/List<Lokhttp3/Cookie;>;"
    .restart local v3    # "validCookies":Ljava/util/List;, "Ljava/util/List<Lokhttp3/Cookie;>;"
    :cond_1
    :try_start_1
    invoke-virtual {v0, p1}, Lokhttp3/Cookie;->matches(Lokhttp3/HttpUrl;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 60
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 64
    .end local v0    # "currentCookie":Lokhttp3/Cookie;
    :cond_2
    iget-object v4, p0, Lcom/franmontiel/persistentcookiejar/PersistentCookieJar;->persistor:Lcom/franmontiel/persistentcookiejar/persistence/CookiePersistor;

    invoke-interface {v4, v2}, Lcom/franmontiel/persistentcookiejar/persistence/CookiePersistor;->removeAll(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 66
    monitor-exit p0

    return-object v3
.end method

.method public declared-synchronized saveFromResponse(Lokhttp3/HttpUrl;Ljava/util/List;)V
    .locals 1
    .param p1, "url"    # Lokhttp3/HttpUrl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lokhttp3/HttpUrl;",
            "Ljava/util/List",
            "<",
            "Lokhttp3/Cookie;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p2, "cookies":Ljava/util/List;, "Ljava/util/List<Lokhttp3/Cookie;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/franmontiel/persistentcookiejar/PersistentCookieJar;->cache:Lcom/franmontiel/persistentcookiejar/cache/CookieCache;

    invoke-interface {v0, p2}, Lcom/franmontiel/persistentcookiejar/cache/CookieCache;->addAll(Ljava/util/Collection;)V

    .line 44
    iget-object v0, p0, Lcom/franmontiel/persistentcookiejar/PersistentCookieJar;->persistor:Lcom/franmontiel/persistentcookiejar/persistence/CookiePersistor;

    invoke-interface {v0, p2}, Lcom/franmontiel/persistentcookiejar/persistence/CookiePersistor;->saveAll(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    monitor-exit p0

    return-void

    .line 43
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
