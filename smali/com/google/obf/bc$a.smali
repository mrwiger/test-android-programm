.class final Lcom/google/obf/bc$a;
.super Lcom/google/obf/bc;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/bc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field public final aP:J

.field public final aQ:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/obf/bc$b;",
            ">;"
        }
    .end annotation
.end field

.field public final aR:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/obf/bc$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(IJ)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lcom/google/obf/bc;-><init>(I)V

    .line 2
    iput-wide p2, p0, Lcom/google/obf/bc$a;->aP:J

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/obf/bc$a;->aQ:Ljava/util/List;

    .line 4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/obf/bc$a;->aR:Ljava/util/List;

    .line 5
    return-void
.end method


# virtual methods
.method public a(Lcom/google/obf/bc$a;)V
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lcom/google/obf/bc$a;->aR:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 9
    return-void
.end method

.method public a(Lcom/google/obf/bc$b;)V
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lcom/google/obf/bc$a;->aQ:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 7
    return-void
.end method

.method public d(I)Lcom/google/obf/bc$b;
    .locals 4

    .prologue
    .line 10
    iget-object v0, p0, Lcom/google/obf/bc$a;->aQ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 11
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 12
    iget-object v0, p0, Lcom/google/obf/bc$a;->aQ:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bc$b;

    .line 13
    iget v3, v0, Lcom/google/obf/bc$b;->aO:I

    if-ne v3, p1, :cond_0

    .line 16
    :goto_1
    return-object v0

    .line 15
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 16
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public e(I)Lcom/google/obf/bc$a;
    .locals 4

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/obf/bc$a;->aR:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 18
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 19
    iget-object v0, p0, Lcom/google/obf/bc$a;->aR:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bc$a;

    .line 20
    iget v3, v0, Lcom/google/obf/bc$a;->aO:I

    if-ne v3, p1, :cond_0

    .line 23
    :goto_1
    return-object v0

    .line 22
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 23
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public f(I)I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 24
    .line 25
    iget-object v0, p0, Lcom/google/obf/bc$a;->aQ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    move v1, v2

    .line 26
    :goto_0
    if-ge v3, v4, :cond_0

    .line 27
    iget-object v0, p0, Lcom/google/obf/bc$a;->aQ:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bc$b;

    .line 28
    iget v0, v0, Lcom/google/obf/bc$b;->aO:I

    if-ne v0, p1, :cond_3

    .line 29
    add-int/lit8 v0, v1, 0x1

    .line 30
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    .line 31
    :cond_0
    iget-object v0, p0, Lcom/google/obf/bc$a;->aR:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 32
    :goto_2
    if-ge v2, v3, :cond_1

    .line 33
    iget-object v0, p0, Lcom/google/obf/bc$a;->aR:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bc$a;

    .line 34
    iget v0, v0, Lcom/google/obf/bc$a;->aO:I

    if-ne v0, p1, :cond_2

    .line 35
    add-int/lit8 v0, v1, 0x1

    .line 36
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_2

    .line 37
    :cond_1
    return v1

    :cond_2
    move v0, v1

    goto :goto_3

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 38
    iget v0, p0, Lcom/google/obf/bc$a;->aO:I

    invoke-static {v0}, Lcom/google/obf/bc$a;->c(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/obf/bc$a;->aQ:Ljava/util/List;

    new-array v2, v3, [Lcom/google/obf/bc$b;

    .line 39
    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/obf/bc$a;->aR:Ljava/util/List;

    new-array v3, v3, [Lcom/google/obf/bc$a;

    .line 40
    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x16

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " leaves: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " containers: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 41
    return-object v0
.end method
