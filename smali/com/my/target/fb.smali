.class public final Lcom/my/target/fb;
.super Landroid/support/v7/widget/RecyclerView;
.source "SliderRecyclerView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/fb$a;,
        Lcom/my/target/fb$d;,
        Lcom/my/target/fb$b;,
        Lcom/my/target/fb$c;
    }
.end annotation


# instance fields
.field private final dl:Landroid/view/View$OnClickListener;

.field private ds:Z

.field private final eb:Lcom/my/target/fa;

.field private final ec:Landroid/support/v7/widget/PagerSnapHelper;

.field private ed:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/g;",
            ">;"
        }
    .end annotation
.end field

.field private ee:Lcom/my/target/fb$c;

.field private ef:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    .line 32
    new-instance v0, Lcom/my/target/fb$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/my/target/fb$a;-><init>(Lcom/my/target/fb;B)V

    iput-object v0, p0, Lcom/my/target/fb;->dl:Landroid/view/View$OnClickListener;

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/my/target/fb;->ef:I

    .line 43
    new-instance v0, Lcom/my/target/fa;

    invoke-virtual {p0}, Lcom/my/target/fb;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/my/target/fa;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/fb;->eb:Lcom/my/target/fa;

    .line 44
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/my/target/fb;->setHasFixedSize(Z)V

    .line 45
    new-instance v0, Landroid/support/v7/widget/PagerSnapHelper;

    invoke-direct {v0}, Landroid/support/v7/widget/PagerSnapHelper;-><init>()V

    iput-object v0, p0, Lcom/my/target/fb;->ec:Landroid/support/v7/widget/PagerSnapHelper;

    .line 46
    iget-object v0, p0, Lcom/my/target/fb;->ec:Landroid/support/v7/widget/PagerSnapHelper;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/PagerSnapHelper;->attachToRecyclerView(Landroid/support/v7/widget/RecyclerView;)V

    .line 47
    return-void
.end method

.method static synthetic a(Lcom/my/target/fb;)I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/my/target/fb;->ef:I

    return v0
.end method

.method static synthetic b(Lcom/my/target/fb;)Lcom/my/target/fa;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/my/target/fb;->eb:Lcom/my/target/fa;

    return-object v0
.end method

.method static synthetic c(Lcom/my/target/fb;)Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/my/target/fb;->ds:Z

    return v0
.end method

.method static synthetic d(Lcom/my/target/fb;)Lcom/my/target/fb$c;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/my/target/fb;->ee:Lcom/my/target/fb$c;

    return-object v0
.end method

.method static synthetic e(Lcom/my/target/fb;)Ljava/util/List;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/my/target/fb;->ed:Ljava/util/List;

    return-object v0
.end method

.method static synthetic f(Lcom/my/target/fb;)Landroid/support/v7/widget/PagerSnapHelper;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/my/target/fb;->ec:Landroid/support/v7/widget/PagerSnapHelper;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/List;I)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/g;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 69
    iput-object p1, p0, Lcom/my/target/fb;->ed:Ljava/util/List;

    .line 73
    invoke-virtual {p0}, Lcom/my/target/fb;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 75
    if-eqz v0, :cond_6

    .line 77
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 79
    :goto_0
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 80
    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {v0, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 84
    :cond_0
    iget v3, v2, Landroid/graphics/Point;->x:I

    .line 85
    iget v2, v2, Landroid/graphics/Point;->y:I

    .line 87
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 89
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/g;

    .line 90
    if-le v3, v2, :cond_4

    .line 92
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/g;->getOptimalLandscapeImage()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 94
    if-eqz v0, :cond_1

    .line 96
    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 99
    :cond_1
    if-eqz v1, :cond_2

    .line 101
    iget-object v0, p0, Lcom/my/target/fb;->eb:Lcom/my/target/fa;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Lcom/my/target/fa;->d(II)V

    .line 120
    :cond_2
    :goto_1
    new-instance v0, Lcom/my/target/fb$b;

    invoke-virtual {p0}, Lcom/my/target/fb;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, p1, p2, v1}, Lcom/my/target/fb$b;-><init>(Ljava/util/List;ILandroid/content/res/Resources;)V

    .line 121
    iget-object v1, p0, Lcom/my/target/fb;->dl:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/my/target/fb$b;->setClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v1, p0, Lcom/my/target/fb;->eb:Lcom/my/target/fa;

    invoke-virtual {p0, v1}, Lcom/my/target/fb;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 123
    invoke-super {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 124
    iget-object v0, p0, Lcom/my/target/fb;->ee:Lcom/my/target/fb$c;

    if-eqz v0, :cond_3

    .line 126
    iget-object v1, p0, Lcom/my/target/fb;->ee:Lcom/my/target/fb$c;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/g;

    invoke-interface {v1, v4, v0}, Lcom/my/target/fb$c;->a(ILcom/my/target/core/models/banners/g;)V

    .line 128
    :cond_3
    return-void

    .line 106
    :cond_4
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/g;->getOptimalPortraitImage()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 108
    if-eqz v0, :cond_5

    .line 110
    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 113
    :cond_5
    if-eqz v1, :cond_2

    .line 115
    iget-object v0, p0, Lcom/my/target/fb;->eb:Lcom/my/target/fa;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Lcom/my/target/fa;->d(II)V

    goto :goto_1

    :cond_6
    move-object v0, v1

    goto :goto_0
.end method

.method protected final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v1, 0x0

    .line 152
    iget-object v0, p0, Lcom/my/target/fb;->ed:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/my/target/fb;->ed:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 154
    iget-object v0, p0, Lcom/my/target/fb;->ed:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/g;

    .line 155
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 157
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/g;->getOptimalLandscapeImage()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 159
    if-eqz v0, :cond_0

    .line 161
    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 164
    :cond_0
    if-eqz v1, :cond_1

    .line 166
    iget-object v0, p0, Lcom/my/target/fb;->eb:Lcom/my/target/fa;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Lcom/my/target/fa;->d(II)V

    .line 185
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 187
    invoke-virtual {p0}, Lcom/my/target/fb;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 189
    new-instance v0, Lcom/my/target/fb$1;

    invoke-direct {v0, p0}, Lcom/my/target/fb$1;-><init>(Lcom/my/target/fb;)V

    const-wide/16 v2, 0x64

    invoke-virtual {p0, v0, v2, v3}, Lcom/my/target/fb;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 199
    return-void

    .line 171
    :cond_2
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/g;->getOptimalPortraitImage()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 173
    if-eqz v0, :cond_3

    .line 175
    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 178
    :goto_1
    if-eqz v0, :cond_1

    .line 180
    iget-object v1, p0, Lcom/my/target/fb;->eb:Lcom/my/target/fa;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/my/target/fa;->d(II)V

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public final onScrollStateChanged(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    .line 138
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->onScrollStateChanged(I)V

    .line 140
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/my/target/fb;->ds:Z

    .line 142
    iget-boolean v0, p0, Lcom/my/target/fb;->ds:Z

    if-nez v0, :cond_0

    .line 1203
    iget-object v0, p0, Lcom/my/target/fb;->eb:Lcom/my/target/fa;

    invoke-virtual {v0}, Lcom/my/target/fa;->findFirstCompletelyVisibleItemPosition()I

    move-result v0

    .line 1204
    if-ltz v0, :cond_0

    .line 1208
    iget v1, p0, Lcom/my/target/fb;->ef:I

    if-eq v1, v0, :cond_0

    .line 1210
    iput v0, p0, Lcom/my/target/fb;->ef:I

    .line 1211
    iget-object v0, p0, Lcom/my/target/fb;->ee:Lcom/my/target/fb$c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/fb;->ed:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1213
    iget-object v1, p0, Lcom/my/target/fb;->ee:Lcom/my/target/fb$c;

    iget v2, p0, Lcom/my/target/fb;->ef:I

    iget-object v0, p0, Lcom/my/target/fb;->ed:Ljava/util/List;

    iget v3, p0, Lcom/my/target/fb;->ef:I

    .line 1214
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/g;

    .line 1213
    invoke-interface {v1, v2, v0}, Lcom/my/target/fb$c;->a(ILcom/my/target/core/models/banners/g;)V

    .line 146
    :cond_0
    return-void

    .line 140
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setSliderCardListener(Lcom/my/target/fb$c;)V
    .locals 0
    .param p1, "sliderCardListener"    # Lcom/my/target/fb$c;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/my/target/fb;->ee:Lcom/my/target/fb$c;

    .line 133
    return-void
.end method
