.class Lru/cn/ad/PreRollBanner$2;
.super Ljava/lang/Object;
.source "PreRollBanner.java"

# interfaces
.implements Lru/cn/ad/AdapterLoader$LoadStepListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/ad/PreRollBanner;->load()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/ad/PreRollBanner;

.field private timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;


# direct methods
.method constructor <init>(Lru/cn/ad/PreRollBanner;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/ad/PreRollBanner;

    .prologue
    .line 86
    iput-object p1, p0, Lru/cn/ad/PreRollBanner$2;->this$0:Lru/cn/ad/PreRollBanner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoadEnded()V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lru/cn/ad/PreRollBanner$2;->timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lru/cn/ad/PreRollBanner$2;->timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

    invoke-virtual {v0}, Lru/cn/domain/statistics/inetra/TimeMeasure;->measureEnd()V

    .line 103
    sget-object v0, Lru/cn/domain/statistics/inetra/TypeOfMeasure;->VIDEO_BANNER_LOAD:Lru/cn/domain/statistics/inetra/TypeOfMeasure;

    iget-object v1, p0, Lru/cn/ad/PreRollBanner$2;->timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

    invoke-static {v0, v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->timeMeasure(Lru/cn/domain/statistics/inetra/TypeOfMeasure;Lru/cn/domain/statistics/inetra/TimeMeasure;)V

    .line 106
    :cond_0
    return-void
.end method

.method public onLoadStarted(Lru/cn/api/money_miner/replies/AdSystem;)V
    .locals 2
    .param p1, "system"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 91
    iget-object v0, p0, Lru/cn/ad/PreRollBanner$2;->timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

    if-nez v0, :cond_0

    .line 92
    new-instance v0, Lru/cn/domain/statistics/inetra/TimeMeasure;

    invoke-direct {v0}, Lru/cn/domain/statistics/inetra/TimeMeasure;-><init>()V

    iput-object v0, p0, Lru/cn/ad/PreRollBanner$2;->timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

    .line 93
    iget-object v0, p0, Lru/cn/ad/PreRollBanner$2;->timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

    iget-object v1, p1, Lru/cn/api/money_miner/replies/AdSystem;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lru/cn/domain/statistics/inetra/TimeMeasure;->measureStart(Ljava/lang/String;)V

    .line 97
    :goto_0
    return-void

    .line 95
    :cond_0
    iget-object v0, p0, Lru/cn/ad/PreRollBanner$2;->timeMeasure:Lru/cn/domain/statistics/inetra/TimeMeasure;

    iget-object v1, p1, Lru/cn/api/money_miner/replies/AdSystem;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lru/cn/domain/statistics/inetra/TimeMeasure;->measureContinue(Ljava/lang/String;)V

    goto :goto_0
.end method
