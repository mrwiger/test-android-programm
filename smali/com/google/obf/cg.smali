.class final Lcom/google/obf/cg;
.super Lcom/google/obf/cb;
.source "IMASDK"


# instance fields
.field private final b:Lcom/google/obf/dw;

.field private final c:Lcom/google/obf/dt;

.field private d:I

.field private e:I

.field private f:Z

.field private g:Z

.field private h:J

.field private i:I

.field private j:J


# direct methods
.method public constructor <init>(Lcom/google/obf/ar;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/obf/cb;-><init>(Lcom/google/obf/ar;)V

    .line 2
    iput v2, p0, Lcom/google/obf/cg;->d:I

    .line 3
    new-instance v0, Lcom/google/obf/dw;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/cg;->b:Lcom/google/obf/dw;

    .line 4
    iget-object v0, p0, Lcom/google/obf/cg;->b:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    const/4 v1, -0x1

    aput-byte v1, v0, v2

    .line 5
    new-instance v0, Lcom/google/obf/dt;

    invoke-direct {v0}, Lcom/google/obf/dt;-><init>()V

    iput-object v0, p0, Lcom/google/obf/cg;->c:Lcom/google/obf/dt;

    .line 6
    return-void
.end method

.method private b(Lcom/google/obf/dw;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 23
    iget-object v5, p1, Lcom/google/obf/dw;->a:[B

    .line 24
    invoke-virtual {p1}, Lcom/google/obf/dw;->d()I

    move-result v0

    .line 25
    invoke-virtual {p1}, Lcom/google/obf/dw;->c()I

    move-result v6

    move v4, v0

    .line 26
    :goto_0
    if-ge v4, v6, :cond_3

    .line 27
    aget-byte v0, v5, v4

    and-int/lit16 v0, v0, 0xff

    const/16 v3, 0xff

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 28
    :goto_1
    iget-boolean v3, p0, Lcom/google/obf/cg;->g:Z

    if-eqz v3, :cond_1

    aget-byte v3, v5, v4

    and-int/lit16 v3, v3, 0xe0

    const/16 v7, 0xe0

    if-ne v3, v7, :cond_1

    move v3, v1

    .line 29
    :goto_2
    iput-boolean v0, p0, Lcom/google/obf/cg;->g:Z

    .line 30
    if-eqz v3, :cond_2

    .line 31
    add-int/lit8 v0, v4, 0x1

    invoke-virtual {p1, v0}, Lcom/google/obf/dw;->c(I)V

    .line 32
    iput-boolean v2, p0, Lcom/google/obf/cg;->g:Z

    .line 33
    iget-object v0, p0, Lcom/google/obf/cg;->b:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    aget-byte v2, v5, v4

    aput-byte v2, v0, v1

    .line 34
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/obf/cg;->e:I

    .line 35
    iput v1, p0, Lcom/google/obf/cg;->d:I

    .line 39
    :goto_3
    return-void

    :cond_0
    move v0, v2

    .line 27
    goto :goto_1

    :cond_1
    move v3, v2

    .line 28
    goto :goto_2

    .line 37
    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 38
    :cond_3
    invoke-virtual {p1, v6}, Lcom/google/obf/dw;->c(I)V

    goto :goto_3
.end method

.method private c(Lcom/google/obf/dw;)V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x1

    const/4 v0, 0x0

    const/4 v10, 0x0

    .line 40
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v1

    iget v2, p0, Lcom/google/obf/cg;->e:I

    rsub-int/lit8 v2, v2, 0x4

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 41
    iget-object v2, p0, Lcom/google/obf/cg;->b:Lcom/google/obf/dw;

    iget-object v2, v2, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/cg;->e:I

    invoke-virtual {p1, v2, v3, v1}, Lcom/google/obf/dw;->a([BII)V

    .line 42
    iget v2, p0, Lcom/google/obf/cg;->e:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/obf/cg;->e:I

    .line 43
    iget v1, p0, Lcom/google/obf/cg;->e:I

    if-ge v1, v12, :cond_0

    .line 60
    :goto_0
    return-void

    .line 45
    :cond_0
    iget-object v1, p0, Lcom/google/obf/cg;->b:Lcom/google/obf/dw;

    invoke-virtual {v1, v10}, Lcom/google/obf/dw;->c(I)V

    .line 46
    iget-object v1, p0, Lcom/google/obf/cg;->b:Lcom/google/obf/dw;

    invoke-virtual {v1}, Lcom/google/obf/dw;->m()I

    move-result v1

    iget-object v2, p0, Lcom/google/obf/cg;->c:Lcom/google/obf/dt;

    invoke-static {v1, v2}, Lcom/google/obf/dt;->a(ILcom/google/obf/dt;)Z

    move-result v1

    .line 47
    if-nez v1, :cond_1

    .line 48
    iput v10, p0, Lcom/google/obf/cg;->e:I

    .line 49
    iput v11, p0, Lcom/google/obf/cg;->d:I

    goto :goto_0

    .line 51
    :cond_1
    iget-object v1, p0, Lcom/google/obf/cg;->c:Lcom/google/obf/dt;

    iget v1, v1, Lcom/google/obf/dt;->c:I

    iput v1, p0, Lcom/google/obf/cg;->i:I

    .line 52
    iget-boolean v1, p0, Lcom/google/obf/cg;->f:Z

    if-nez v1, :cond_2

    .line 53
    const-wide/32 v2, 0xf4240

    iget-object v1, p0, Lcom/google/obf/cg;->c:Lcom/google/obf/dt;

    iget v1, v1, Lcom/google/obf/dt;->g:I

    int-to-long v4, v1

    mul-long/2addr v2, v4

    iget-object v1, p0, Lcom/google/obf/cg;->c:Lcom/google/obf/dt;

    iget v1, v1, Lcom/google/obf/dt;->d:I

    int-to-long v4, v1

    div-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/obf/cg;->h:J

    .line 54
    iget-object v1, p0, Lcom/google/obf/cg;->c:Lcom/google/obf/dt;

    iget-object v1, v1, Lcom/google/obf/dt;->b:Ljava/lang/String;

    const/4 v2, -0x1

    const/16 v3, 0x1000

    const-wide/16 v4, -0x1

    iget-object v6, p0, Lcom/google/obf/cg;->c:Lcom/google/obf/dt;

    iget v6, v6, Lcom/google/obf/dt;->e:I

    iget-object v7, p0, Lcom/google/obf/cg;->c:Lcom/google/obf/dt;

    iget v7, v7, Lcom/google/obf/dt;->d:I

    move-object v8, v0

    move-object v9, v0

    invoke-static/range {v0 .. v9}, Lcom/google/obf/q;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)Lcom/google/obf/q;

    move-result-object v0

    .line 55
    iget-object v1, p0, Lcom/google/obf/cg;->a:Lcom/google/obf/ar;

    invoke-interface {v1, v0}, Lcom/google/obf/ar;->a(Lcom/google/obf/q;)V

    .line 56
    iput-boolean v11, p0, Lcom/google/obf/cg;->f:Z

    .line 57
    :cond_2
    iget-object v0, p0, Lcom/google/obf/cg;->b:Lcom/google/obf/dw;

    invoke-virtual {v0, v10}, Lcom/google/obf/dw;->c(I)V

    .line 58
    iget-object v0, p0, Lcom/google/obf/cg;->a:Lcom/google/obf/ar;

    iget-object v1, p0, Lcom/google/obf/cg;->b:Lcom/google/obf/dw;

    invoke-interface {v0, v1, v12}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 59
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/obf/cg;->d:I

    goto :goto_0
.end method

.method private d(Lcom/google/obf/dw;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 61
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v0

    iget v1, p0, Lcom/google/obf/cg;->i:I

    iget v2, p0, Lcom/google/obf/cg;->e:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 62
    iget-object v1, p0, Lcom/google/obf/cg;->a:Lcom/google/obf/ar;

    invoke-interface {v1, p1, v0}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 63
    iget v1, p0, Lcom/google/obf/cg;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/cg;->e:I

    .line 64
    iget v0, p0, Lcom/google/obf/cg;->e:I

    iget v1, p0, Lcom/google/obf/cg;->i:I

    if-ge v0, v1, :cond_0

    .line 70
    :goto_0
    return-void

    .line 66
    :cond_0
    iget-object v1, p0, Lcom/google/obf/cg;->a:Lcom/google/obf/ar;

    iget-wide v2, p0, Lcom/google/obf/cg;->j:J

    const/4 v4, 0x1

    iget v5, p0, Lcom/google/obf/cg;->i:I

    const/4 v7, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/google/obf/ar;->a(JIII[B)V

    .line 67
    iget-wide v0, p0, Lcom/google/obf/cg;->j:J

    iget-wide v2, p0, Lcom/google/obf/cg;->h:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/obf/cg;->j:J

    .line 68
    iput v6, p0, Lcom/google/obf/cg;->e:I

    .line 69
    iput v6, p0, Lcom/google/obf/cg;->d:I

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7
    iput v0, p0, Lcom/google/obf/cg;->d:I

    .line 8
    iput v0, p0, Lcom/google/obf/cg;->e:I

    .line 9
    iput-boolean v0, p0, Lcom/google/obf/cg;->g:Z

    .line 10
    return-void
.end method

.method public a(JZ)V
    .locals 1

    .prologue
    .line 11
    iput-wide p1, p0, Lcom/google/obf/cg;->j:J

    .line 12
    return-void
.end method

.method public a(Lcom/google/obf/dw;)V
    .locals 1

    .prologue
    .line 13
    :goto_0
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v0

    if-lez v0, :cond_0

    .line 14
    iget v0, p0, Lcom/google/obf/cg;->d:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 15
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/obf/cg;->b(Lcom/google/obf/dw;)V

    goto :goto_0

    .line 17
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/obf/cg;->c(Lcom/google/obf/dw;)V

    goto :goto_0

    .line 19
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/obf/cg;->d(Lcom/google/obf/dw;)V

    goto :goto_0

    .line 21
    :cond_0
    return-void

    .line 14
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public b()V
    .locals 0

    .prologue
    .line 22
    return-void
.end method
