.class public Lru/cn/player/chromecast/CastOptionsProvider;
.super Ljava/lang/Object;
.source "CastOptionsProvider.java"

# interfaces
.implements Lcom/google/android/gms/cast/framework/OptionsProvider;


# static fields
.field private static final CHROMECAST_APP_ID:Ljava/lang/String; = "B0D83EF3"

.field private static final DEFAULT_SKIP_STEP:J = 0x7530L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAdditionalSessionProviders(Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/cast/framework/SessionProvider;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCastOptions(Landroid/content/Context;)Lcom/google/android/gms/cast/framework/CastOptions;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 27
    .local v0, "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v3, "com.google.android.gms.cast.framework.action.TOGGLE_PLAYBACK"

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    const-string v3, "com.google.android.gms.cast.framework.action.STOP_CASTING"

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    new-instance v3, Lcom/google/android/gms/cast/framework/media/NotificationOptions$Builder;

    invoke-direct {v3}, Lcom/google/android/gms/cast/framework/media/NotificationOptions$Builder;-><init>()V

    const/4 v4, 0x2

    new-array v4, v4, [I

    fill-array-data v4, :array_0

    .line 35
    invoke-virtual {v3, v0, v4}, Lcom/google/android/gms/cast/framework/media/NotificationOptions$Builder;->setActions(Ljava/util/List;[I)Lcom/google/android/gms/cast/framework/media/NotificationOptions$Builder;

    move-result-object v3

    const-wide/16 v4, 0x7530

    .line 36
    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/cast/framework/media/NotificationOptions$Builder;->setSkipStepMs(J)Lcom/google/android/gms/cast/framework/media/NotificationOptions$Builder;

    move-result-object v3

    const-class v4, Lru/cn/tv/mobile/NewActivity;

    .line 37
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/cast/framework/media/NotificationOptions$Builder;->setTargetActivityClassName(Ljava/lang/String;)Lcom/google/android/gms/cast/framework/media/NotificationOptions$Builder;

    move-result-object v3

    .line 38
    invoke-virtual {v3}, Lcom/google/android/gms/cast/framework/media/NotificationOptions$Builder;->build()Lcom/google/android/gms/cast/framework/media/NotificationOptions;

    move-result-object v2

    .line 40
    .local v2, "notificationOptions":Lcom/google/android/gms/cast/framework/media/NotificationOptions;
    new-instance v3, Lcom/google/android/gms/cast/framework/media/CastMediaOptions$Builder;

    invoke-direct {v3}, Lcom/google/android/gms/cast/framework/media/CastMediaOptions$Builder;-><init>()V

    .line 41
    invoke-virtual {v3, v2}, Lcom/google/android/gms/cast/framework/media/CastMediaOptions$Builder;->setNotificationOptions(Lcom/google/android/gms/cast/framework/media/NotificationOptions;)Lcom/google/android/gms/cast/framework/media/CastMediaOptions$Builder;

    move-result-object v3

    .line 42
    invoke-virtual {v3}, Lcom/google/android/gms/cast/framework/media/CastMediaOptions$Builder;->build()Lcom/google/android/gms/cast/framework/media/CastMediaOptions;

    move-result-object v1

    .line 44
    .local v1, "mediaOptions":Lcom/google/android/gms/cast/framework/media/CastMediaOptions;
    new-instance v3, Lcom/google/android/gms/cast/framework/CastOptions$Builder;

    invoke-direct {v3}, Lcom/google/android/gms/cast/framework/CastOptions$Builder;-><init>()V

    const-string v4, "B0D83EF3"

    .line 45
    invoke-virtual {v3, v4}, Lcom/google/android/gms/cast/framework/CastOptions$Builder;->setReceiverApplicationId(Ljava/lang/String;)Lcom/google/android/gms/cast/framework/CastOptions$Builder;

    move-result-object v3

    .line 46
    invoke-virtual {v3, v1}, Lcom/google/android/gms/cast/framework/CastOptions$Builder;->setCastMediaOptions(Lcom/google/android/gms/cast/framework/media/CastMediaOptions;)Lcom/google/android/gms/cast/framework/CastOptions$Builder;

    move-result-object v3

    .line 47
    invoke-virtual {v3}, Lcom/google/android/gms/cast/framework/CastOptions$Builder;->build()Lcom/google/android/gms/cast/framework/CastOptions;

    move-result-object v3

    .line 44
    return-object v3

    .line 34
    nop

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data
.end method
