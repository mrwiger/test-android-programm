.class public Lru/cn/notifications/entity/INotification$NewFilmMessage;
.super Ljava/lang/Object;
.source "INotification.java"

# interfaces
.implements Lru/cn/notifications/entity/INotification;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/notifications/entity/INotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NewFilmMessage"
.end annotation


# instance fields
.field private final createTime:J

.field private final filmId:J

.field private final message:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;JJ)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "filmId"    # J
    .param p4, "createTime"    # J

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lru/cn/notifications/entity/INotification$NewFilmMessage;->message:Ljava/lang/String;

    .line 35
    iput-wide p2, p0, Lru/cn/notifications/entity/INotification$NewFilmMessage;->filmId:J

    .line 36
    iput-wide p4, p0, Lru/cn/notifications/entity/INotification$NewFilmMessage;->createTime:J

    .line 37
    return-void
.end method


# virtual methods
.method public createTime()J
    .locals 2

    .prologue
    .line 50
    iget-wide v0, p0, Lru/cn/notifications/entity/INotification$NewFilmMessage;->createTime:J

    return-wide v0
.end method

.method public filmId()J
    .locals 2

    .prologue
    .line 45
    iget-wide v0, p0, Lru/cn/notifications/entity/INotification$NewFilmMessage;->filmId:J

    return-wide v0
.end method

.method public message()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lru/cn/notifications/entity/INotification$NewFilmMessage;->message:Ljava/lang/String;

    return-object v0
.end method
