.class public Lcom/my/target/nativeads/views/NewsFeedAdView;
.super Landroid/widget/RelativeLayout;
.source "NewsFeedAdView.java"


# static fields
.field private static final AD_ID:I

.field private static final AGE_ID:I

.field private static final COLOR_PLACEHOLDER_GRAY:I = -0x111112

.field private static final CTA_ID:I

.field private static final ICON_ID:I

.field private static final LABELS_ID:I

.field private static final RATING_ID:I

.field private static final STANDARD_BLUE:I = -0xff912c

.field private static final STANDARD_GREY:I = -0x666667

.field private static final STARS_ID:I

.field private static final TITLE_2_ID:I

.field private static final TITLE_ID:I

.field private static final URL_ID:I

.field private static final VOTES_ID:I


# instance fields
.field private final advertisingLabel:Landroid/widget/TextView;

.field private final ageRestrictionLabel:Lcom/my/target/bu;

.field private banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

.field private final ctaButton:Landroid/widget/Button;

.field private ctaParams:Landroid/widget/RelativeLayout$LayoutParams;

.field private final disclaimerLabel:Landroid/widget/TextView;

.field private disclaimerParams:Landroid/widget/RelativeLayout$LayoutParams;

.field private final iconImageView:Lcom/my/target/bv;

.field private final labelsLayout:Landroid/widget/LinearLayout;

.field private final ratingLayout:Landroid/widget/LinearLayout;

.field private final starsView:Lcom/my/target/ca;

.field private final titleLabel:Landroid/widget/TextView;

.field private final uiUtils:Lcom/my/target/cm;

.field private final urlLabel:Landroid/widget/TextView;

.field private urlLabelParams:Landroid/widget/RelativeLayout$LayoutParams;

.field private final votesLabel:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/NewsFeedAdView;->AGE_ID:I

    .line 43
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ICON_ID:I

    .line 44
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/NewsFeedAdView;->LABELS_ID:I

    .line 45
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/NewsFeedAdView;->TITLE_ID:I

    .line 46
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/NewsFeedAdView;->URL_ID:I

    .line 47
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/NewsFeedAdView;->AD_ID:I

    .line 48
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/NewsFeedAdView;->TITLE_2_ID:I

    .line 49
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/NewsFeedAdView;->CTA_ID:I

    .line 50
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/NewsFeedAdView;->STARS_ID:I

    .line 51
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/NewsFeedAdView;->VOTES_ID:I

    .line 52
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/nativeads/views/NewsFeedAdView;->RATING_ID:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 74
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/my/target/nativeads/views/NewsFeedAdView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/my/target/nativeads/views/NewsFeedAdView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 84
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 85
    new-instance v0, Lcom/my/target/bu;

    invoke-direct {v0, p1}, Lcom/my/target/bu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    .line 86
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->advertisingLabel:Landroid/widget/TextView;

    .line 87
    new-instance v0, Lcom/my/target/bv;

    invoke-direct {v0, p1}, Lcom/my/target/bv;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->iconImageView:Lcom/my/target/bv;

    .line 88
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->labelsLayout:Landroid/widget/LinearLayout;

    .line 89
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->titleLabel:Landroid/widget/TextView;

    .line 90
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabel:Landroid/widget/TextView;

    .line 91
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ratingLayout:Landroid/widget/LinearLayout;

    .line 92
    new-instance v0, Lcom/my/target/ca;

    invoke-direct {v0, p1}, Lcom/my/target/ca;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->starsView:Lcom/my/target/ca;

    .line 93
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->votesLabel:Landroid/widget/TextView;

    .line 94
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->disclaimerLabel:Landroid/widget/TextView;

    .line 95
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    .line 97
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    .line 99
    invoke-direct {p0}, Lcom/my/target/nativeads/views/NewsFeedAdView;->initView()V

    .line 100
    return-void
.end method

.method private initView()V
    .locals 9

    .prologue
    const/16 v8, 0xc

    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, -0x2

    .line 348
    const-string v0, "ad_view"

    invoke-static {p0, v0}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 350
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v0, v8}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    .line 351
    invoke-virtual {v1, v8}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    .line 352
    invoke-virtual {v2, v8}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    .line 353
    invoke-virtual {v3, v8}, Lcom/my/target/cm;->n(I)I

    move-result v3

    .line 350
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/my/target/nativeads/views/NewsFeedAdView;->setPadding(IIII)V

    .line 356
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    sget v1, Lcom/my/target/nativeads/views/NewsFeedAdView;->AGE_ID:I

    invoke-virtual {v0, v1}, Lcom/my/target/bu;->setId(I)V

    .line 357
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    const v1, -0x777778

    invoke-virtual {v0, v5, v1}, Lcom/my/target/bu;->c(II)V

    .line 358
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v1, v6, v6, v6}, Lcom/my/target/bu;->setPadding(IIII)V

    .line 359
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 361
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 362
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    invoke-virtual {v1, v0}, Lcom/my/target/bu;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 364
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->advertisingLabel:Landroid/widget/TextView;

    sget v1, Lcom/my/target/nativeads/views/NewsFeedAdView;->AD_ID:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 365
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 367
    sget v1, Lcom/my/target/nativeads/views/NewsFeedAdView;->AGE_ID:I

    invoke-virtual {v0, v5, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 368
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->advertisingLabel:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 369
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->advertisingLabel:Landroid/widget/TextView;

    const-string v1, "advertising_label"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 372
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->iconImageView:Lcom/my/target/bv;

    sget v1, Lcom/my/target/nativeads/views/NewsFeedAdView;->ICON_ID:I

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setId(I)V

    .line 373
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0x36

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v3, 0x36

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 374
    sget v1, Lcom/my/target/nativeads/views/NewsFeedAdView;->AD_ID:I

    invoke-virtual {v0, v7, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 375
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 376
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->iconImageView:Lcom/my/target/bv;

    invoke-virtual {v1, v0}, Lcom/my/target/bv;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 377
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->iconImageView:Lcom/my/target/bv;

    const-string v1, "icon_image"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 380
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->labelsLayout:Landroid/widget/LinearLayout;

    sget v1, Lcom/my/target/nativeads/views/NewsFeedAdView;->LABELS_ID:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setId(I)V

    .line 381
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->labelsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 382
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->labelsLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0x36

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setMinimumHeight(I)V

    .line 383
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 385
    sget v1, Lcom/my/target/nativeads/views/NewsFeedAdView;->AD_ID:I

    invoke-virtual {v0, v7, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 386
    sget v1, Lcom/my/target/nativeads/views/NewsFeedAdView;->ICON_ID:I

    invoke-virtual {v0, v5, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 387
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 388
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v1, v7}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 389
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->labelsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 391
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->titleLabel:Landroid/widget/TextView;

    sget v1, Lcom/my/target/nativeads/views/NewsFeedAdView;->TITLE_ID:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 392
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->titleLabel:Landroid/widget/TextView;

    const-string v1, "title_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 393
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 395
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->titleLabel:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 397
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabel:Landroid/widget/TextView;

    sget v1, Lcom/my/target/nativeads/views/NewsFeedAdView;->URL_ID:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 398
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabelParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 400
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabelParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 401
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabel:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabelParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 403
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ratingLayout:Landroid/widget/LinearLayout;

    sget v1, Lcom/my/target/nativeads/views/NewsFeedAdView;->RATING_ID:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setId(I)V

    .line 404
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ratingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 405
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 407
    sget v1, Lcom/my/target/nativeads/views/NewsFeedAdView;->TITLE_2_ID:I

    invoke-virtual {v0, v7, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 408
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ratingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 410
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->starsView:Lcom/my/target/ca;

    sget v1, Lcom/my/target/nativeads/views/NewsFeedAdView;->STARS_ID:I

    invoke-virtual {v0, v1}, Lcom/my/target/ca;->setId(I)V

    .line 411
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0x49

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    .line 412
    invoke-virtual {v2, v8}, Lcom/my/target/cm;->n(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 413
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 414
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 415
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->starsView:Lcom/my/target/ca;

    invoke-virtual {v1, v0}, Lcom/my/target/ca;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 417
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->votesLabel:Landroid/widget/TextView;

    sget v1, Lcom/my/target/nativeads/views/NewsFeedAdView;->VOTES_ID:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 419
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->disclaimerParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 421
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->disclaimerParams:Landroid/widget/RelativeLayout$LayoutParams;

    sget v1, Lcom/my/target/nativeads/views/NewsFeedAdView;->RATING_ID:I

    invoke-virtual {v0, v7, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 422
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->disclaimerLabel:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->disclaimerParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 424
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    sget v1, Lcom/my/target/nativeads/views/NewsFeedAdView;->CTA_ID:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setId(I)V

    .line 425
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    invoke-virtual {v0, v1, v6, v2, v6}, Landroid/widget/Button;->setPadding(IIII)V

    .line 426
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 427
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setMaxEms(I)V

    .line 428
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setLines(I)V

    .line 429
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 430
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0x1e

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-direct {v0, v4, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 431
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaParams:Landroid/widget/RelativeLayout$LayoutParams;

    sget v1, Lcom/my/target/nativeads/views/NewsFeedAdView;->LABELS_ID:I

    invoke-virtual {v0, v7, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 432
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaParams:Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 433
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0x17

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    neg-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 434
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 436
    const v0, -0x3a1508

    invoke-static {p0, v6, v0}, Lcom/my/target/cm;->a(Landroid/view/View;II)V

    .line 438
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    .line 441
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v1, v5}, Lcom/my/target/cm;->n(I)I

    move-result v1

    const v2, -0xff912c

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 442
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v1, v5}, Lcom/my/target/cm;->n(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 444
    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    sget-object v2, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_1

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    .line 447
    iget-object v2, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v2, v5}, Lcom/my/target/cm;->n(I)I

    move-result v2

    const v3, -0xff912c

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 448
    iget-object v2, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v2, v5}, Lcom/my/target/cm;->n(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 450
    new-instance v2, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 451
    new-array v3, v5, [I

    const v4, 0x10100a7

    aput v4, v3, v6

    invoke-virtual {v2, v3, v1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 453
    sget-object v1, Landroid/util/StateSet;->WILD_CARD:[I

    invoke-virtual {v2, v1, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 455
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 457
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 465
    :goto_0
    invoke-virtual {p0, v5}, Lcom/my/target/nativeads/views/NewsFeedAdView;->setClickable(Z)V

    .line 468
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    invoke-virtual {p0, v0}, Lcom/my/target/nativeads/views/NewsFeedAdView;->addView(Landroid/view/View;)V

    .line 469
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->advertisingLabel:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/my/target/nativeads/views/NewsFeedAdView;->addView(Landroid/view/View;)V

    .line 470
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->iconImageView:Lcom/my/target/bv;

    invoke-virtual {p0, v0}, Lcom/my/target/nativeads/views/NewsFeedAdView;->addView(Landroid/view/View;)V

    .line 471
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->labelsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/my/target/nativeads/views/NewsFeedAdView;->addView(Landroid/view/View;)V

    .line 473
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->labelsLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->titleLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 474
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->labelsLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 476
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    invoke-virtual {p0, v0}, Lcom/my/target/nativeads/views/NewsFeedAdView;->addView(Landroid/view/View;)V

    .line 477
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ratingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/my/target/nativeads/views/NewsFeedAdView;->addView(Landroid/view/View;)V

    .line 478
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->disclaimerLabel:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/my/target/nativeads/views/NewsFeedAdView;->addView(Landroid/view/View;)V

    .line 480
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ratingLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->starsView:Lcom/my/target/ca;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 481
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ratingLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->votesLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 483
    invoke-direct {p0}, Lcom/my/target/nativeads/views/NewsFeedAdView;->updateDefaultParams()V

    .line 484
    return-void

    .line 462
    :cond_0
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 438
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 444
    :array_1
    .array-data 4
        -0x3a1508
        -0x3a1508
    .end array-data
.end method

.method private updateDefaultParams()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x41600000    # 14.0f

    const/4 v3, 0x2

    const v2, -0x666667

    .line 488
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    invoke-virtual {v0, v2}, Lcom/my/target/bu;->setTextColor(I)V

    .line 489
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    invoke-virtual {v0, v5, v2}, Lcom/my/target/bu;->c(II)V

    .line 490
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/my/target/bu;->setBackgroundColor(I)V

    .line 492
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->advertisingLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 493
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->advertisingLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 495
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->titleLabel:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 496
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->titleLabel:Landroid/widget/TextView;

    const/high16 v1, 0x41800000    # 16.0f

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 497
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->titleLabel:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 499
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 500
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 502
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->votesLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 503
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->votesLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 505
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->disclaimerLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 506
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->disclaimerLabel:Landroid/widget/TextView;

    const/high16 v1, 0x41400000    # 12.0f

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 508
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    const v1, -0xff912c

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 509
    return-void
.end method


# virtual methods
.method public getAdvertisingTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->advertisingLabel:Landroid/widget/TextView;

    return-object v0
.end method

.method public getAgeRestrictionTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    return-object v0
.end method

.method public getCtaButtonView()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    return-object v0
.end method

.method public getDisclaimerTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->disclaimerLabel:Landroid/widget/TextView;

    return-object v0
.end method

.method public getDomainOrCategoryTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabel:Landroid/widget/TextView;

    return-object v0
.end method

.method public getIconImageView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->iconImageView:Lcom/my/target/bv;

    return-object v0
.end method

.method public getStarsRatingView()Lcom/my/target/ca;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->starsView:Lcom/my/target/ca;

    return-object v0
.end method

.method public getTitleTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->titleLabel:Landroid/widget/TextView;

    return-object v0
.end method

.method public getVotesTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->votesLabel:Landroid/widget/TextView;

    return-object v0
.end method

.method public loadImages()V
    .locals 2

    .prologue
    .line 327
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

    if-nez v0, :cond_1

    .line 344
    :cond_0
    :goto_0
    return-void

    .line 332
    :cond_1
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

    invoke-virtual {v0}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 333
    if-eqz v0, :cond_0

    .line 335
    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_2

    .line 337
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->iconImageView:Lcom/my/target/bv;

    invoke-static {v0, v1}, Lcom/my/target/ch;->a(Lcom/my/target/common/models/ImageData;Landroid/widget/ImageView;)V

    goto :goto_0

    .line 341
    :cond_2
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->iconImageView:Lcom/my/target/bv;

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/my/target/bv;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public setupView(Lcom/my/target/nativeads/banners/NativePromoBanner;)V
    .locals 9
    .param p1, "banner"    # Lcom/my/target/nativeads/banners/NativePromoBanner;

    .prologue
    const/16 v8, 0x9

    const/4 v2, 0x1

    const/16 v7, 0x8

    const/4 v3, 0x0

    const/4 v6, -0x2

    .line 149
    if-nez p1, :cond_1

    .line 323
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    iput-object p1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

    .line 154
    const-string v0, "Setup banner"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 155
    const-string v0, "web"

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getNavigationType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 157
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 158
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ratingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 159
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabel:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getDomain()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabel:Landroid/widget/TextView;

    const-string v1, "domain_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 212
    :cond_2
    :goto_1
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 214
    if-eqz v0, :cond_d

    .line 216
    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 218
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->iconImageView:Lcom/my/target/bv;

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/my/target/bv;->setImageBitmap(Landroid/graphics/Bitmap;)V

    move v0, v2

    .line 229
    :goto_2
    if-nez v0, :cond_3

    .line 231
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->iconImageView:Lcom/my/target/bv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setImageData(Lcom/my/target/common/models/ImageData;)V

    .line 232
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->iconImageView:Lcom/my/target/bv;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 235
    :cond_3
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->titleLabel:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 236
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->advertisingLabel:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getAdvertisingLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 237
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getCtaText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 238
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    const-string v1, "cta_button"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 240
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getAgeRestrictions()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 242
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getAgeRestrictions()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/bu;->setText(Ljava/lang/CharSequence;)V

    .line 243
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    const-string v1, "age_bordered"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 250
    :goto_3
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getDisclaimer()Ljava/lang/String;

    move-result-object v4

    .line 251
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 252
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 254
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->disclaimerLabel:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 255
    if-eqz v0, :cond_4

    .line 257
    check-cast v0, Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->disclaimerLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 259
    :cond_4
    if-eqz v1, :cond_5

    move-object v0, v1

    .line 261
    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 264
    :cond_5
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/my/target/nativeads/views/NewsFeedAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 265
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 267
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->disclaimerLabel:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 268
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->disclaimerParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 269
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->disclaimerParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v1, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 270
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->disclaimerParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getId()I

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 271
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->disclaimerParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 272
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->disclaimerLabel:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->disclaimerParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 273
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->disclaimerLabel:Landroid/widget/TextView;

    const-string v2, "disclaimer_text"

    invoke-static {v1, v2}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 275
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v3, 0x1e

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    invoke-direct {v1, v6, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 276
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaParams:Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 277
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 279
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->disclaimerLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 280
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 282
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->labelsLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    goto/16 :goto_0

    .line 162
    :cond_6
    const-string v0, "store"

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getNavigationType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 164
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getCategory()Ljava/lang/String;

    move-result-object v1

    .line 165
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getSubCategory()Ljava/lang/String;

    move-result-object v4

    .line 167
    const-string v0, ""

    .line 168
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 170
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 173
    :cond_7
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 175
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 178
    :cond_8
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 180
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 183
    :cond_9
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getRating()F

    move-result v1

    const/4 v4, 0x0

    cmpl-float v1, v1, v4

    if-lez v1, :cond_b

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getRating()F

    move-result v1

    const/high16 v4, 0x40a00000    # 5.0f

    cmpg-float v1, v1, v4

    if-gtz v1, :cond_b

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getVotes()I

    move-result v1

    if-lez v1, :cond_b

    .line 185
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 186
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ratingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 187
    if-eqz v0, :cond_a

    .line 189
    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ratingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 191
    :cond_a
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->labelsLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ratingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 193
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ratingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 195
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->starsView:Lcom/my/target/ca;

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getRating()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/ca;->setRating(F)V

    .line 196
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->votesLabel:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getVotes()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->votesLabel:Landroid/widget/TextView;

    const-string v1, "votes_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->starsView:Lcom/my/target/ca;

    const-string v1, "rating_view"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 203
    :cond_b
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabel:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 204
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabel:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 205
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabel:Landroid/widget/TextView;

    const-string v1, "category_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 207
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ratingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 223
    :cond_c
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->iconImageView:Lcom/my/target/bv;

    const v4, -0x111112

    invoke-virtual {v1, v4}, Lcom/my/target/bv;->setBackgroundColor(I)V

    .line 224
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->iconImageView:Lcom/my/target/bv;

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getWidth()I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/my/target/bv;->setPlaceholderWidth(I)V

    .line 225
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->iconImageView:Lcom/my/target/bv;

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getHeight()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/my/target/bv;->setPlaceholderHeight(I)V

    :cond_d
    move v0, v3

    goto/16 :goto_2

    .line 247
    :cond_e
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ageRestrictionLabel:Lcom/my/target/bu;

    invoke-virtual {v0, v7}, Lcom/my/target/bu;->setVisibility(I)V

    goto/16 :goto_3

    .line 286
    :cond_f
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->disclaimerLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 287
    const-string v0, "web"

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getNavigationType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289
    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getDomain()Ljava/lang/String;

    move-result-object v0

    .line 290
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabel:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 293
    if-eqz v0, :cond_10

    .line 295
    check-cast v0, Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 297
    :cond_10
    if-eqz v1, :cond_11

    .line 299
    check-cast v1, Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 302
    :cond_11
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/my/target/nativeads/views/NewsFeedAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 303
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    invoke-direct {v1, v4, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 305
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabelParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 306
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabelParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v1, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 307
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabelParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v4, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getId()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 308
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabelParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 309
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabel:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabelParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 311
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    const/16 v4, 0x1e

    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    invoke-direct {v1, v6, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 312
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaParams:Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v3, 0xb

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 313
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v3, v8}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 314
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 316
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->urlLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 317
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->ctaButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 319
    iget-object v1, p0, Lcom/my/target/nativeads/views/NewsFeedAdView;->labelsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    goto/16 :goto_0
.end method
