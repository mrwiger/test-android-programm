.class public final Lru/cn/tv/stb/calendar/CalendarFragment;
.super Lru/cn/view/CustomListFragment;
.source "CalendarFragment.java"


# instance fields
.field private adapter:Lru/cn/tv/stb/calendar/CalendarAdapter;

.field private currentChannelId:J

.field private selectedDate:Ljava/util/Calendar;

.field private viewModel:Lru/cn/tv/stb/calendar/CalendarViewModel;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lru/cn/view/CustomListFragment;-><init>()V

    return-void
.end method

.method private setDates(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 116
    iget-object v0, p0, Lru/cn/tv/stb/calendar/CalendarFragment;->adapter:Lru/cn/tv/stb/calendar/CalendarAdapter;

    invoke-virtual {v0, p1}, Lru/cn/tv/stb/calendar/CalendarAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 117
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 118
    iget-object v0, p0, Lru/cn/tv/stb/calendar/CalendarFragment;->selectedDate:Ljava/util/Calendar;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lru/cn/tv/stb/calendar/CalendarFragment;->selectedDate:Ljava/util/Calendar;

    invoke-virtual {p0, v0}, Lru/cn/tv/stb/calendar/CalendarFragment;->selectDate(Ljava/util/Calendar;)V

    .line 121
    :cond_0
    return-void
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$CalendarFragment(Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/stb/calendar/CalendarFragment;->setDates(Landroid/database/Cursor;)V

    return-void
.end method

.method public getDate(I)Ljava/util/Calendar;
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 65
    iget-object v4, p0, Lru/cn/tv/stb/calendar/CalendarFragment;->adapter:Lru/cn/tv/stb/calendar/CalendarAdapter;

    invoke-virtual {v4, p1}, Lru/cn/tv/stb/calendar/CalendarAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 67
    .local v0, "date":Landroid/database/Cursor;
    sget-object v4, Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;->year:Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;

    invoke-virtual {v4}, Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 68
    .local v3, "year":I
    sget-object v4, Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;->month:Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;

    invoke-virtual {v4}, Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 69
    .local v2, "month":I
    sget-object v4, Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;->day:Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;

    invoke-virtual {v4}, Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 71
    .local v1, "day":I
    invoke-static {v3, v2, v1}, Lru/cn/utils/Utils;->getCalendar(III)Ljava/util/Calendar;

    move-result-object v4

    return-object v4
.end method

.method protected getItemHeight()I
    .locals 1

    .prologue
    .line 61
    const/16 v0, 0x40

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 32
    invoke-super {p0, p1}, Lru/cn/view/CustomListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 33
    new-instance v0, Lru/cn/tv/stb/calendar/CalendarAdapter;

    invoke-virtual {p0}, Lru/cn/tv/stb/calendar/CalendarFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    .line 34
    invoke-virtual {p0}, Lru/cn/tv/stb/calendar/CalendarFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f04005d

    invoke-static {v3, v4}, Lru/cn/utils/Utils;->resolveResourse(Landroid/content/Context;I)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lru/cn/tv/stb/calendar/CalendarAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    iput-object v0, p0, Lru/cn/tv/stb/calendar/CalendarFragment;->adapter:Lru/cn/tv/stb/calendar/CalendarAdapter;

    .line 36
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v0

    const-class v1, Lru/cn/tv/stb/calendar/CalendarViewModel;

    invoke-static {p0, v0, v1}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lru/cn/tv/stb/calendar/CalendarViewModel;

    iput-object v0, p0, Lru/cn/tv/stb/calendar/CalendarFragment;->viewModel:Lru/cn/tv/stb/calendar/CalendarViewModel;

    .line 37
    iget-object v0, p0, Lru/cn/tv/stb/calendar/CalendarFragment;->viewModel:Lru/cn/tv/stb/calendar/CalendarViewModel;

    .line 38
    invoke-virtual {v0}, Lru/cn/tv/stb/calendar/CalendarViewModel;->dates()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/stb/calendar/CalendarFragment$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/calendar/CalendarFragment$$Lambda$0;-><init>(Lru/cn/tv/stb/calendar/CalendarFragment;)V

    .line 39
    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 40
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 45
    const v0, 0x7f0c006c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 2
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 56
    invoke-virtual {p0}, Lru/cn/tv/stb/calendar/CalendarFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p3, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 57
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 50
    invoke-super {p0, p1, p2}, Lru/cn/view/CustomListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 51
    iget-object v0, p0, Lru/cn/tv/stb/calendar/CalendarFragment;->adapter:Lru/cn/tv/stb/calendar/CalendarAdapter;

    invoke-virtual {p0, v0}, Lru/cn/tv/stb/calendar/CalendarFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 52
    return-void
.end method

.method public selectDate(Ljava/util/Calendar;)V
    .locals 13
    .param p1, "date"    # Ljava/util/Calendar;

    .prologue
    const/4 v12, 0x1

    .line 83
    iput-object p1, p0, Lru/cn/tv/stb/calendar/CalendarFragment;->selectedDate:Ljava/util/Calendar;

    .line 84
    iget-object v10, p0, Lru/cn/tv/stb/calendar/CalendarFragment;->adapter:Lru/cn/tv/stb/calendar/CalendarAdapter;

    invoke-virtual {v10}, Lru/cn/tv/stb/calendar/CalendarAdapter;->getCount()I

    move-result v10

    if-lez v10, :cond_0

    .line 85
    iget-object v10, p0, Lru/cn/tv/stb/calendar/CalendarFragment;->adapter:Lru/cn/tv/stb/calendar/CalendarAdapter;

    invoke-virtual {v10}, Lru/cn/tv/stb/calendar/CalendarAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 86
    .local v0, "c":Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 88
    const-string v10, "year"

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 89
    .local v9, "yearIndex":I
    const-string v10, "month"

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 90
    .local v7, "monthIndex":I
    const-string v10, "day"

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 92
    .local v5, "dayIndex":I
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v10

    if-nez v10, :cond_0

    .line 93
    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 94
    .local v8, "year":I
    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    add-int/lit8 v6, v10, -0x1

    .line 95
    .local v6, "month":I
    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 97
    .local v4, "day":I
    iget-object v10, p0, Lru/cn/tv/stb/calendar/CalendarFragment;->selectedDate:Ljava/util/Calendar;

    invoke-virtual {v10, v12}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 98
    .local v3, "currentYear":I
    iget-object v10, p0, Lru/cn/tv/stb/calendar/CalendarFragment;->selectedDate:Ljava/util/Calendar;

    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 99
    .local v2, "currentMonth":I
    iget-object v10, p0, Lru/cn/tv/stb/calendar/CalendarFragment;->selectedDate:Ljava/util/Calendar;

    const/4 v11, 0x5

    invoke-virtual {v10, v11}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 101
    .local v1, "currentDay":I
    if-ne v3, v8, :cond_1

    if-ne v2, v6, :cond_1

    if-ne v1, v4, :cond_1

    .line 105
    invoke-virtual {p0}, Lru/cn/tv/stb/calendar/CalendarFragment;->getListView()Landroid/widget/ListView;

    move-result-object v10

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v11

    invoke-virtual {v10, v11, v12}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 106
    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v10

    invoke-virtual {p0, v10}, Lru/cn/tv/stb/calendar/CalendarFragment;->selectPosition(I)V

    .line 113
    .end local v0    # "c":Landroid/database/Cursor;
    .end local v1    # "currentDay":I
    .end local v2    # "currentMonth":I
    .end local v3    # "currentYear":I
    .end local v4    # "day":I
    .end local v5    # "dayIndex":I
    .end local v6    # "month":I
    .end local v7    # "monthIndex":I
    .end local v8    # "year":I
    .end local v9    # "yearIndex":I
    :cond_0
    return-void

    .line 110
    .restart local v0    # "c":Landroid/database/Cursor;
    .restart local v1    # "currentDay":I
    .restart local v2    # "currentMonth":I
    .restart local v3    # "currentYear":I
    .restart local v4    # "day":I
    .restart local v5    # "dayIndex":I
    .restart local v6    # "month":I
    .restart local v7    # "monthIndex":I
    .restart local v8    # "year":I
    .restart local v9    # "yearIndex":I
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0
.end method

.method public setChannelId(J)V
    .locals 3
    .param p1, "channelId"    # J

    .prologue
    .line 75
    iget-wide v0, p0, Lru/cn/tv/stb/calendar/CalendarFragment;->currentChannelId:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    .line 76
    iput-wide p1, p0, Lru/cn/tv/stb/calendar/CalendarFragment;->currentChannelId:J

    .line 78
    iget-object v0, p0, Lru/cn/tv/stb/calendar/CalendarFragment;->viewModel:Lru/cn/tv/stb/calendar/CalendarViewModel;

    invoke-virtual {v0, p1, p2}, Lru/cn/tv/stb/calendar/CalendarViewModel;->setChannelId(J)V

    .line 80
    :cond_0
    return-void
.end method
