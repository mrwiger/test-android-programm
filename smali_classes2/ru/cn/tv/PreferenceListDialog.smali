.class public Lru/cn/tv/PreferenceListDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "PreferenceListDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/PreferenceListDialog$PreferenceListListener;
    }
.end annotation


# instance fields
.field private listener:Lru/cn/tv/PreferenceListDialog$PreferenceListListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lru/cn/tv/PreferenceListDialog;
    .locals 3
    .param p0, "title"    # Ljava/lang/String;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "options"    # [Ljava/lang/String;

    .prologue
    .line 27
    new-instance v1, Lru/cn/tv/PreferenceListDialog;

    invoke-direct {v1}, Lru/cn/tv/PreferenceListDialog;-><init>()V

    .line 29
    .local v1, "fragment":Lru/cn/tv/PreferenceListDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 30
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "title"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    const-string v2, "key"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    const-string v2, "options_array"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 33
    invoke-virtual {v1, v0}, Lru/cn/tv/PreferenceListDialog;->setArguments(Landroid/os/Bundle;)V

    .line 35
    return-object v1
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    invoke-virtual {p0}, Lru/cn/tv/PreferenceListDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "title"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 41
    .local v3, "title":Ljava/lang/String;
    invoke-virtual {p0}, Lru/cn/tv/PreferenceListDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 43
    new-instance v1, Landroid/widget/ListView;

    invoke-virtual {p0}, Lru/cn/tv/PreferenceListDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 45
    .local v1, "listView":Landroid/widget/ListView;
    invoke-virtual {p0}, Lru/cn/tv/PreferenceListDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "options_array"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 46
    .local v2, "options":[Ljava/lang/String;
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lru/cn/tv/PreferenceListDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x1090003

    invoke-direct {v0, v4, v5, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 47
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 48
    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 50
    return-object v1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p0}, Lru/cn/tv/PreferenceListDialog;->dismiss()V

    .line 56
    iget-object v0, p0, Lru/cn/tv/PreferenceListDialog;->listener:Lru/cn/tv/PreferenceListDialog$PreferenceListListener;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lru/cn/tv/PreferenceListDialog;->listener:Lru/cn/tv/PreferenceListDialog$PreferenceListListener;

    invoke-virtual {p0}, Lru/cn/tv/PreferenceListDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "key"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p3, v1}, Lru/cn/tv/PreferenceListDialog$PreferenceListListener;->onListItemSelected(ILjava/lang/String;)V

    .line 59
    :cond_0
    return-void
.end method

.method public setListener(Lru/cn/tv/PreferenceListDialog$PreferenceListListener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/tv/PreferenceListDialog$PreferenceListListener;

    .prologue
    .line 62
    iput-object p1, p0, Lru/cn/tv/PreferenceListDialog;->listener:Lru/cn/tv/PreferenceListDialog$PreferenceListListener;

    .line 63
    return-void
.end method
