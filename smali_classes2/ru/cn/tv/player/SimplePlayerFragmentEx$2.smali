.class Lru/cn/tv/player/SimplePlayerFragmentEx$2;
.super Ljava/lang/Object;
.source "SimplePlayerFragmentEx.java"

# interfaces
.implements Lru/cn/tv/player/controller/PlayerController$CompletionBehaviour;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/player/SimplePlayerFragmentEx;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;


# direct methods
.method constructor <init>(Lru/cn/tv/player/SimplePlayerFragmentEx;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/player/SimplePlayerFragmentEx;

    .prologue
    .line 130
    iput-object p1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$2;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Lru/cn/api/tv/replies/Telecast;)V
    .locals 2
    .param p1, "telecast"    # Lru/cn/api/tv/replies/Telecast;

    .prologue
    .line 134
    if-nez p1, :cond_1

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$2;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->access$100(Lru/cn/tv/player/SimplePlayerFragmentEx;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 138
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$2;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->access$000(Lru/cn/tv/player/SimplePlayerFragmentEx;)Lru/cn/tv/player/controller/TouchPlayerController;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/player/controller/TouchPlayerController;->hide()V

    .line 139
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$2;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->access$200(Lru/cn/tv/player/SimplePlayerFragmentEx;)Lru/cn/tv/player/controller/TouchNextProgramView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lru/cn/tv/player/controller/TouchNextProgramView;->show(Lru/cn/api/tv/replies/Telecast;)V

    .line 142
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$2;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->access$000(Lru/cn/tv/player/SimplePlayerFragmentEx;)Lru/cn/tv/player/controller/TouchPlayerController;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lru/cn/tv/player/controller/TouchPlayerController;->setTouchesEnabled(Z)V

    .line 144
    const-string v0, "show"

    invoke-static {v0}, Lru/cn/domain/statistics/AnalyticsManager;->recommend_next(Ljava/lang/String;)V

    goto :goto_0
.end method
