.class public final Lcom/my/target/core/parsers/a;
.super Ljava/lang/Object;
.source "NativeAdBannerParser.java"


# instance fields
.field private final R:Lcom/my/target/cs;

.field private final S:Lcom/my/target/ae;

.field private final T:Lcom/my/target/be;

.field private U:Ljava/lang/String;

.field private final adConfig:Lcom/my/target/b;

.field private final context:Landroid/content/Context;


# direct methods
.method private constructor <init>(Lcom/my/target/cs;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/my/target/core/parsers/a;->R:Lcom/my/target/cs;

    .line 49
    iput-object p2, p0, Lcom/my/target/core/parsers/a;->S:Lcom/my/target/ae;

    .line 50
    iput-object p3, p0, Lcom/my/target/core/parsers/a;->adConfig:Lcom/my/target/b;

    .line 51
    iput-object p4, p0, Lcom/my/target/core/parsers/a;->context:Landroid/content/Context;

    .line 52
    invoke-static {p1, p2, p3, p4}, Lcom/my/target/be;->a(Lcom/my/target/ak;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/be;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/parsers/a;->T:Lcom/my/target/be;

    .line 53
    return-void
.end method

.method public static a(Lcom/my/target/cs;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/a;
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/my/target/core/parsers/a;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/my/target/core/parsers/a;-><init>(Lcom/my/target/cs;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 119
    invoke-static {p1}, Lcom/my/target/az;->y(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/my/target/az;->z(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/parsers/a;->adConfig:Lcom/my/target/b;

    .line 120
    invoke-virtual {v1}, Lcom/my/target/b;->getSlotId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/az;->h(I)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/parsers/a;->U:Ljava/lang/String;

    .line 121
    invoke-virtual {v0, v1}, Lcom/my/target/az;->B(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/parsers/a;->S:Lcom/my/target/ae;

    .line 122
    invoke-virtual {v1}, Lcom/my/target/ae;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/az;->A(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/parsers/a;->context:Landroid/content/Context;

    .line 123
    invoke-virtual {v0, v1}, Lcom/my/target/az;->e(Landroid/content/Context;)V

    .line 124
    return-void
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;Lcom/my/target/core/models/banners/a;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 57
    iget-object v0, p0, Lcom/my/target/core/parsers/a;->T:Lcom/my/target/be;

    invoke-virtual {v0, p1, p2, v1}, Lcom/my/target/be;->a(Lorg/json/JSONObject;Lcom/my/target/ah;Lcom/my/target/af;)V

    .line 58
    invoke-virtual {p2}, Lcom/my/target/core/models/banners/a;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/parsers/a;->U:Ljava/lang/String;

    .line 60
    const-string v0, "cards"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 61
    if-eqz v3, :cond_3

    .line 63
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    .line 64
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_3

    .line 66
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 67
    if-eqz v5, :cond_0

    .line 1097
    invoke-static {p2}, Lcom/my/target/core/models/banners/b;->newCard(Lcom/my/target/core/models/banners/a;)Lcom/my/target/core/models/banners/b;

    move-result-object v0

    .line 1098
    iget-object v6, p0, Lcom/my/target/core/parsers/a;->T:Lcom/my/target/be;

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/a;->getClickArea()Lcom/my/target/af;

    move-result-object v7

    invoke-virtual {v6, v5, v0, v7}, Lcom/my/target/be;->a(Lorg/json/JSONObject;Lcom/my/target/ah;Lcom/my/target/af;)V

    .line 1100
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/b;->getTrackingLink()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1102
    const-string v0, "Required field"

    const-string v5, "no tracking link in nativeAdCard"

    invoke-direct {p0, v0, v5}, Lcom/my/target/core/parsers/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 70
    :goto_1
    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/a;->addNativeAdCard(Lcom/my/target/core/models/banners/b;)V

    .line 64
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1106
    :cond_1
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/b;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v6

    if-nez v6, :cond_2

    .line 1108
    const-string v0, "Required field"

    const-string v5, "no image in nativeAdCard"

    invoke-direct {p0, v0, v5}, Lcom/my/target/core/parsers/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 1109
    goto :goto_1

    .line 1112
    :cond_2
    const-string v6, "cardID"

    invoke-virtual {v0}, Lcom/my/target/core/models/banners/b;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/my/target/core/models/banners/b;->setId(Ljava/lang/String;)V

    goto :goto_1

    .line 78
    :cond_3
    invoke-virtual {p2}, Lcom/my/target/core/models/banners/a;->getNativeAdCards()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 80
    const-string v0, "video"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 81
    if-eqz v0, :cond_4

    .line 83
    invoke-static {}, Lcom/my/target/aj;->newVideoBanner()Lcom/my/target/aj;

    move-result-object v1

    .line 84
    invoke-virtual {p2}, Lcom/my/target/core/models/banners/a;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/my/target/aj;->setId(Ljava/lang/String;)V

    .line 85
    iget-object v2, p0, Lcom/my/target/core/parsers/a;->R:Lcom/my/target/cs;

    iget-object v3, p0, Lcom/my/target/core/parsers/a;->S:Lcom/my/target/ae;

    iget-object v4, p0, Lcom/my/target/core/parsers/a;->adConfig:Lcom/my/target/b;

    iget-object v5, p0, Lcom/my/target/core/parsers/a;->context:Landroid/content/Context;

    invoke-static {v2, v3, v4, v5}, Lcom/my/target/bg;->b(Lcom/my/target/ak;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/bg;

    move-result-object v2

    .line 86
    iget-object v3, p0, Lcom/my/target/core/parsers/a;->R:Lcom/my/target/cs;

    invoke-virtual {v3}, Lcom/my/target/cs;->getVideoSettings()Lcom/my/target/am;

    move-result-object v3

    invoke-virtual {v2, v0, v1, v3}, Lcom/my/target/bg;->a(Lorg/json/JSONObject;Lcom/my/target/aj;Lcom/my/target/am;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 88
    invoke-virtual {p2, v1}, Lcom/my/target/core/models/banners/a;->setVideoBanner(Lcom/my/target/aj;)V

    .line 92
    :cond_4
    return-void
.end method
