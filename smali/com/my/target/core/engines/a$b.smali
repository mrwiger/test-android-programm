.class public final Lcom/my/target/core/engines/a$b;
.super Ljava/lang/Object;
.source "NativeAdEngine.java"

# interfaces
.implements Lcom/my/target/core/controllers/b$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/core/engines/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field final synthetic N:Lcom/my/target/core/engines/a;


# direct methods
.method public constructor <init>(Lcom/my/target/core/engines/a;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/my/target/core/engines/a$b;->N:Lcom/my/target/core/engines/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 201
    const-string v0, "Click on native card received"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 203
    iget-object v0, p0, Lcom/my/target/core/engines/a$b;->N:Lcom/my/target/core/engines/a;

    invoke-static {v0}, Lcom/my/target/core/engines/a;->e(Lcom/my/target/core/engines/a;)Lcom/my/target/core/models/banners/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/core/models/banners/a;->getNativeAdCards()Ljava/util/List;

    move-result-object v0

    .line 205
    if-ltz p2, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge p2, v1, :cond_0

    .line 207
    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/b;

    .line 208
    iget-object v1, p0, Lcom/my/target/core/engines/a$b;->N:Lcom/my/target/core/engines/a;

    invoke-static {v1, v0, p1}, Lcom/my/target/core/engines/a;->a(Lcom/my/target/core/engines/a;Lcom/my/target/ah;Landroid/view/View;)V

    .line 212
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/engines/a$b;->N:Lcom/my/target/core/engines/a;

    invoke-static {v0}, Lcom/my/target/core/engines/a;->e(Lcom/my/target/core/engines/a;)Lcom/my/target/core/models/banners/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/core/models/banners/a;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    .line 213
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 214
    if-eqz v1, :cond_1

    .line 216
    const-string v2, "click"

    invoke-virtual {v0, v2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 218
    :cond_1
    return-void
.end method

.method public final a(Landroid/view/View;[I)V
    .locals 6

    .prologue
    .line 223
    array-length v2, p2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    aget v0, p2, v1

    .line 225
    iget-object v3, p0, Lcom/my/target/core/engines/a$b;->N:Lcom/my/target/core/engines/a;

    invoke-static {v3}, Lcom/my/target/core/engines/a;->e(Lcom/my/target/core/engines/a;)Lcom/my/target/core/models/banners/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/my/target/core/models/banners/a;->getNativeAdCards()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/b;

    .line 226
    iget-object v3, p0, Lcom/my/target/core/engines/a$b;->N:Lcom/my/target/core/engines/a;

    invoke-static {v3}, Lcom/my/target/core/engines/a;->c(Lcom/my/target/core/engines/a;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/my/target/core/engines/a$b;->N:Lcom/my/target/core/engines/a;

    invoke-static {v3}, Lcom/my/target/core/engines/a;->g(Lcom/my/target/core/engines/a;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 229
    if-eqz v0, :cond_0

    .line 231
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/b;->getStatHolder()Lcom/my/target/ar;

    move-result-object v3

    .line 232
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 233
    if-eqz v4, :cond_0

    .line 235
    const-string v5, "playbackStarted"

    invoke-virtual {v3, v5}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v3, v4}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 238
    :cond_0
    iget-object v3, p0, Lcom/my/target/core/engines/a$b;->N:Lcom/my/target/core/engines/a;

    invoke-static {v3}, Lcom/my/target/core/engines/a;->g(Lcom/my/target/core/engines/a;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 223
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 241
    :cond_2
    return-void
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/my/target/core/engines/a$b;->N:Lcom/my/target/core/engines/a;

    invoke-static {v0}, Lcom/my/target/core/engines/a;->f(Lcom/my/target/core/engines/a;)Lcom/my/target/nativeads/NativeAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/nativeads/NativeAd;->getListener()Lcom/my/target/nativeads/NativeAd$NativeAdListener;

    move-result-object v0

    .line 172
    if-eqz v0, :cond_0

    .line 174
    iget-object v1, p0, Lcom/my/target/core/engines/a$b;->N:Lcom/my/target/core/engines/a;

    invoke-static {v1}, Lcom/my/target/core/engines/a;->f(Lcom/my/target/core/engines/a;)Lcom/my/target/nativeads/NativeAd;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/my/target/nativeads/NativeAd$NativeAdListener;->onVideoPlay(Lcom/my/target/nativeads/NativeAd;)V

    .line 176
    :cond_0
    return-void
.end method

.method public final o()V
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lcom/my/target/core/engines/a$b;->N:Lcom/my/target/core/engines/a;

    invoke-static {v0}, Lcom/my/target/core/engines/a;->f(Lcom/my/target/core/engines/a;)Lcom/my/target/nativeads/NativeAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/nativeads/NativeAd;->getListener()Lcom/my/target/nativeads/NativeAd$NativeAdListener;

    move-result-object v0

    .line 182
    if-eqz v0, :cond_0

    .line 184
    iget-object v1, p0, Lcom/my/target/core/engines/a$b;->N:Lcom/my/target/core/engines/a;

    invoke-static {v1}, Lcom/my/target/core/engines/a;->f(Lcom/my/target/core/engines/a;)Lcom/my/target/nativeads/NativeAd;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/my/target/nativeads/NativeAd$NativeAdListener;->onVideoPause(Lcom/my/target/nativeads/NativeAd;)V

    .line 186
    :cond_0
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 161
    const-string v0, "Click received by native ad"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 162
    if-eqz p1, :cond_0

    .line 164
    iget-object v0, p0, Lcom/my/target/core/engines/a$b;->N:Lcom/my/target/core/engines/a;

    iget-object v1, p0, Lcom/my/target/core/engines/a$b;->N:Lcom/my/target/core/engines/a;

    invoke-static {v1}, Lcom/my/target/core/engines/a;->e(Lcom/my/target/core/engines/a;)Lcom/my/target/core/models/banners/a;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/my/target/core/engines/a;->a(Lcom/my/target/core/engines/a;Lcom/my/target/ah;Landroid/view/View;)V

    .line 166
    :cond_0
    return-void
.end method

.method public final p()V
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/my/target/core/engines/a$b;->N:Lcom/my/target/core/engines/a;

    invoke-static {v0}, Lcom/my/target/core/engines/a;->f(Lcom/my/target/core/engines/a;)Lcom/my/target/nativeads/NativeAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/nativeads/NativeAd;->getListener()Lcom/my/target/nativeads/NativeAd$NativeAdListener;

    move-result-object v0

    .line 192
    if-eqz v0, :cond_0

    .line 194
    iget-object v1, p0, Lcom/my/target/core/engines/a$b;->N:Lcom/my/target/core/engines/a;

    invoke-static {v1}, Lcom/my/target/core/engines/a;->f(Lcom/my/target/core/engines/a;)Lcom/my/target/nativeads/NativeAd;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/my/target/nativeads/NativeAd$NativeAdListener;->onVideoComplete(Lcom/my/target/nativeads/NativeAd;)V

    .line 196
    :cond_0
    return-void
.end method
