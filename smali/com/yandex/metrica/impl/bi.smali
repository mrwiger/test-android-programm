.class Lcom/yandex/metrica/impl/bi;
.super Lcom/yandex/metrica/impl/ak;
.source "SourceFile"


# instance fields
.field private a:Lcom/yandex/metrica/impl/bb;

.field private b:Landroid/content/Context;

.field private c:Lcom/yandex/metrica/impl/ob/v;

.field private m:Lcom/yandex/metrica/impl/ob/dg;

.field private n:Z

.field private o:Lcom/yandex/metrica/impl/ob/fx;


# direct methods
.method public constructor <init>(Lcom/yandex/metrica/impl/ob/v;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ak;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/yandex/metrica/impl/bi;->n:Z

    .line 52
    iput-object p1, p0, Lcom/yandex/metrica/impl/bi;->c:Lcom/yandex/metrica/impl/ob/v;

    .line 53
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->n()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/bi;->b:Landroid/content/Context;

    .line 54
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->i()Lcom/yandex/metrica/impl/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    .line 55
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->F()Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/bi;->m:Lcom/yandex/metrica/impl/ob/dg;

    .line 56
    return-void
.end method

.method private static a(Lcom/yandex/metrica/impl/bb;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/bb;->p()Ljava/lang/String;

    move-result-object v1

    .line 110
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/bb;->J()Ljava/lang/String;

    move-result-object v0

    .line 111
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, ""

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method private static a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 173
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 174
    invoke-virtual {p0, p1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 176
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 339
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Startup task for component: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->c:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/v;->m()Lcom/yandex/metrica/impl/ob/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/t;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method declared-synchronized a(J)V
    .locals 1

    .prologue
    .line 316
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/bi;->m:Lcom/yandex/metrica/impl/ob/dg;

    invoke-virtual {v0, p1, p2}, Lcom/yandex/metrica/impl/ob/dg;->b(J)Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dg;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 317
    monitor-exit p0

    return-void

    .line 316
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method a(Landroid/net/Uri$Builder;)V
    .locals 4

    .prologue
    .line 115
    const-string v0, "analytics/startup"

    invoke-virtual {p1, v0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 118
    const-string v0, "deviceid"

    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-static {v1}, Lcom/yandex/metrica/impl/bi;->a(Lcom/yandex/metrica/impl/bb;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 120
    const-string v0, "app_platform"

    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 121
    const-string v0, "protocol_version"

    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 122
    const-string v0, "analytics_sdk_version"

    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 123
    const-string v0, "analytics_sdk_version_name"

    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 124
    const-string v0, "model"

    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 125
    const-string v0, "manufacturer"

    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 126
    const-string v0, "os_version"

    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 127
    const-string v0, "screen_width"

    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->u()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 128
    const-string v0, "screen_height"

    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->v()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 129
    const-string v0, "screen_dpi"

    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->w()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 130
    const-string v0, "scalefactor"

    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->x()F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 131
    const-string v0, "locale"

    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->y()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 132
    const-string v0, "device_type"

    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 133
    const-string v0, "query_hosts"

    const-string v1, "2"

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 134
    const-string v0, "features"

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "easy_collecting"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "package_info"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "socket"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "permissions_collecting"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "features_collecting"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "foreground_location_collection"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "background_location_collection"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "telephony_restricted_to_location_tracking"

    aput-object v3, v1, v2

    .line 135
    invoke-static {v1}, Lcom/yandex/metrica/impl/bj;->b([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 134
    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 146
    const-string v0, "browsers"

    const-string v1, "1"

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 147
    const-string v0, "socket"

    const-string v1, "1"

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 148
    const-string v0, "app_id"

    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->c:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/v;->m()Lcom/yandex/metrica/impl/ob/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/t;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 149
    const-string v0, "foreground_location_collection"

    const-string v1, "1"

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 150
    const-string v0, "app_debuggable"

    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->T()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 151
    iget-object v0, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/bb;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    const-string v0, "background_location_collection"

    const-string v1, "1"

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/bi;->c:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/v;->k()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->u()Ljava/util/Map;

    move-result-object v1

    .line 156
    iget-object v0, p0, Lcom/yandex/metrica/impl/bi;->c:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/v;->k()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->v()Ljava/lang/String;

    move-result-object v0

    .line 157
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 158
    iget-object v0, p0, Lcom/yandex/metrica/impl/bi;->m:Lcom/yandex/metrica/impl/ob/dg;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dg;->e()Ljava/lang/String;

    move-result-object v0

    .line 160
    :cond_1
    invoke-static {v1}, Lcom/yandex/metrica/impl/bl;->a(Ljava/util/Map;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 161
    const-string v2, "distribution_customization"

    const-string v3, "1"

    invoke-virtual {p1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 162
    const-string v2, "clids_set"

    invoke-static {v1}, Lcom/yandex/metrica/impl/utils/o;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v2, v1}, Lcom/yandex/metrica/impl/bi;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 164
    const-string v1, "install_referrer"

    invoke-virtual {p1, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 168
    :cond_2
    const-string v0, "uuid"

    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->q()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/yandex/metrica/impl/bi;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const-string v0, "time"

    const-string v1, "1"

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 170
    return-void
.end method

.method declared-synchronized a(Lcom/yandex/metrica/impl/bb;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 245
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/bi;->z()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 261
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2264
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/bi;->m:Lcom/yandex/metrica/impl/ob/dg;

    .line 2265
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bb;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/dg;->l(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v0

    .line 2266
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bb;->G()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/dg;->c(Ljava/util/List;)Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v0

    .line 2267
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bb;->B()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/dg;->a(Ljava/util/List;)Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v0

    .line 2268
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bb;->C()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/dg;->b(Ljava/util/List;)Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v0

    .line 2269
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bb;->E()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/dg;->o(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v0

    .line 2270
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bb;->D()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/dg;->p(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v0

    .line 2271
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bb;->Q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/dg;->k(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v0

    .line 2272
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bb;->L()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/dg;->a(Z)Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v0

    .line 2273
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bb;->M()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/dg;->b(Z)Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v0

    .line 2274
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bb;->N()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/dg;->c(Z)Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v0

    .line 2275
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bb;->O()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/dg;->d(Z)Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v0

    .line 2276
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bb;->P()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/dg;->f(Z)Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v0

    .line 2277
    invoke-virtual {v0, p2}, Lcom/yandex/metrica/impl/ob/dg;->v(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v0

    .line 2278
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bb;->o()Lcom/yandex/metrica/impl/ob/dv;

    move-result-object v2

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/dv;->a()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/dg;->x(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dg;

    move-result-object v0

    .line 2279
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bb;->S()Lcom/yandex/metrica/impl/ob/dq;

    move-result-object v2

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/dq;->a()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/dg;->y(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dg;

    .line 2281
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bb;->K()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/yandex/metrica/impl/bi;->m:Lcom/yandex/metrica/impl/ob/dg;

    .line 3061
    invoke-static {v0}, Lcom/yandex/metrica/impl/utils/o;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    .line 3062
    invoke-static {v3}, Lcom/yandex/metrica/impl/utils/o;->b(Ljava/util/Map;)Z

    move-result v3

    .line 2288
    if-eqz v3, :cond_3

    .line 2296
    :cond_2
    :goto_1
    invoke-virtual {v2, v0}, Lcom/yandex/metrica/impl/ob/dg;->t(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dg;

    .line 2283
    iget-object v0, p0, Lcom/yandex/metrica/impl/bi;->m:Lcom/yandex/metrica/impl/ob/dg;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dg;->g()V

    .line 5029
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 254
    invoke-virtual {p0, v0, v1}, Lcom/yandex/metrica/impl/bi;->a(J)V

    .line 256
    invoke-static {}, Lcom/yandex/metrica/impl/ob/eq;->a()Lcom/yandex/metrica/impl/ob/eq;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    .line 257
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bb;->Q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/yandex/metrica/impl/ob/eq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 5305
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bb;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/yandex/metrica/impl/bj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5306
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.yandex.metrica.intent.action.SYNC"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5307
    const-string v1, "CAUSE"

    const-string v2, "CAUSE_DEVICE_ID"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5308
    const-string v1, "SYNC_TO_PKG"

    iget-object v2, p0, Lcom/yandex/metrica/impl/bi;->c:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/v;->m()Lcom/yandex/metrica/impl/ob/t;

    move-result-object v2

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/t;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5309
    const-string v1, "SYNC_DATA"

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bb;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5310
    const-string v1, "SYNC_DATA_2"

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/bb;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5311
    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 245
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2291
    :cond_3
    const/4 v0, 0x0

    :try_start_2
    invoke-virtual {v2, v0}, Lcom/yandex/metrica/impl/ob/dg;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4061
    invoke-static {v0}, Lcom/yandex/metrica/impl/utils/o;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    .line 4062
    invoke-static {v3}, Lcom/yandex/metrica/impl/utils/o;->b(Ljava/util/Map;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    .line 2292
    if-nez v3, :cond_2

    move-object v0, v1

    goto :goto_1
.end method

.method declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 320
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/yandex/metrica/impl/bi;->n:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 321
    monitor-exit p0

    return-void

    .line 320
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bi;->a(Z)V

    .line 63
    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    iget-object v2, p0, Lcom/yandex/metrica/impl/bi;->c:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/bb;->b(Lcom/yandex/metrica/impl/ob/v;)V

    .line 64
    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->F()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/yandex/metrica/impl/bi;->a(Ljava/util/List;)V

    .line 66
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/bi;->y()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    const/4 v0, 0x1

    .line 70
    :cond_0
    return v0
.end method

.method public c()Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 180
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/yandex/metrica/impl/bi;->k:Z

    .line 182
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/bi;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 183
    iput-boolean v8, p0, Lcom/yandex/metrica/impl/bi;->k:Z

    .line 217
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/bi;->k:Z

    return v0

    .line 184
    :cond_1
    const/16 v0, 0xc8

    iget v1, p0, Lcom/yandex/metrica/impl/bi;->h:I

    if-ne v0, v1, :cond_0

    .line 185
    iget-object v0, p0, Lcom/yandex/metrica/impl/bi;->c:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/v;->k()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->u()Ljava/util/Map;

    move-result-object v1

    .line 187
    iget-object v0, p0, Lcom/yandex/metrica/impl/bi;->i:[B

    .line 188
    invoke-static {v0}, Lcom/yandex/metrica/impl/bh;->a([B)Lcom/yandex/metrica/impl/bh$a;

    move-result-object v2

    .line 190
    sget-object v0, Lcom/yandex/metrica/impl/bh$a$c;->b:Lcom/yandex/metrica/impl/bh$a$c;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bh$a;->o()Lcom/yandex/metrica/impl/bh$a$c;

    move-result-object v3

    if-ne v0, v3, :cond_4

    .line 191
    iget-object v0, p0, Lcom/yandex/metrica/impl/bi;->m:Lcom/yandex/metrica/impl/ob/dg;

    iget-object v3, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/bb;->K()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/yandex/metrica/impl/ob/dg;->w(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dg;

    .line 192
    iget-object v0, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/bb;->a(Lcom/yandex/metrica/impl/bh$a;)V

    .line 193
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bh$a;->x()Ljava/lang/Long;

    move-result-object v0

    .line 1238
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/bi;->l()Ljava/util/Map;

    move-result-object v3

    invoke-static {v3}, Lcom/yandex/metrica/impl/bh;->a(Ljava/util/Map;)Ljava/lang/Long;

    move-result-object v3

    .line 1239
    if-eqz v3, :cond_2

    .line 1240
    invoke-static {}, Lcom/yandex/metrica/impl/utils/n;->a()Lcom/yandex/metrica/impl/utils/n;

    move-result-object v4

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7, v0}, Lcom/yandex/metrica/impl/utils/n;->a(JLjava/lang/Long;)V

    .line 194
    :cond_2
    iget-object v0, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-static {}, Lcom/yandex/metrica/impl/ob/dl;->a()Lcom/yandex/metrica/impl/ob/dl;

    move-result-object v3

    iget-object v4, p0, Lcom/yandex/metrica/impl/bi;->b:Landroid/content/Context;

    iget-object v5, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    .line 195
    invoke-virtual {v5}, Lcom/yandex/metrica/impl/bb;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/yandex/metrica/impl/ob/dl;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 194
    invoke-virtual {v0, v3}, Lcom/yandex/metrica/impl/bb;->i(Ljava/lang/String;)V

    .line 196
    const-string v0, ""

    .line 197
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bh$a;->s()Lcom/yandex/metrica/impl/ob/fv;

    move-result-object v3

    if-nez v3, :cond_3

    .line 198
    invoke-static {}, Lcom/yandex/metrica/impl/ob/g;->a()Lcom/yandex/metrica/impl/ob/g;

    move-result-object v3

    const-class v4, Lcom/yandex/metrica/impl/ob/p;

    invoke-virtual {v3, v4}, Lcom/yandex/metrica/impl/ob/g;->a(Ljava/lang/Class;)V

    .line 206
    :goto_1
    iget-object v3, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {p0, v3, v0}, Lcom/yandex/metrica/impl/bi;->a(Lcom/yandex/metrica/impl/bb;Ljava/lang/String;)V

    .line 1300
    iget-object v0, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/bb;->K()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/yandex/metrica/impl/utils/o;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 1301
    iget-object v3, p0, Lcom/yandex/metrica/impl/bi;->c:Lcom/yandex/metrica/impl/ob/v;

    invoke-interface {v0, v1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v3, v0}, Lcom/yandex/metrica/impl/ob/v;->a(Z)V

    .line 209
    iget-object v0, p0, Lcom/yandex/metrica/impl/bi;->c:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/v;->l()Landroid/os/ResultReceiver;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-static {v0, v1, v2}, Lcom/yandex/metrica/impl/k;->a(Landroid/os/ResultReceiver;Lcom/yandex/metrica/impl/bb;Lcom/yandex/metrica/impl/bh$a;)V

    .line 210
    invoke-static {}, Lcom/yandex/metrica/impl/ob/g;->a()Lcom/yandex/metrica/impl/ob/g;

    move-result-object v0

    new-instance v1, Lcom/yandex/metrica/impl/ob/o;

    iget-object v2, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-direct {v1, v2}, Lcom/yandex/metrica/impl/ob/o;-><init>(Lcom/yandex/metrica/impl/bb;)V

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/g;->a(Lcom/yandex/metrica/impl/ob/i;)V

    .line 211
    iput-boolean v8, p0, Lcom/yandex/metrica/impl/bi;->k:Z

    goto/16 :goto_0

    .line 201
    :cond_3
    :try_start_0
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bh$a;->s()Lcom/yandex/metrica/impl/ob/fv;

    move-result-object v3

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/ob/fv;->d()Ljava/lang/String;

    move-result-object v0

    .line 202
    invoke-static {}, Lcom/yandex/metrica/impl/ob/g;->a()Lcom/yandex/metrica/impl/ob/g;

    move-result-object v3

    new-instance v4, Lcom/yandex/metrica/impl/ob/p;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bh$a;->s()Lcom/yandex/metrica/impl/ob/fv;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/yandex/metrica/impl/ob/p;-><init>(Lcom/yandex/metrica/impl/ob/fv;)V

    invoke-virtual {v3, v4}, Lcom/yandex/metrica/impl/ob/g;->b(Lcom/yandex/metrica/impl/ob/i;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v3

    goto :goto_1

    .line 213
    :cond_4
    sget-object v0, Lcom/yandex/metrica/impl/ob/fx;->c:Lcom/yandex/metrica/impl/ob/fx;

    iput-object v0, p0, Lcom/yandex/metrica/impl/bi;->o:Lcom/yandex/metrica/impl/ob/fx;

    goto/16 :goto_0
.end method

.method protected d()Lcom/yandex/metrica/impl/ob/es;
    .locals 2

    .prologue
    .line 81
    new-instance v0, Lcom/yandex/metrica/impl/ob/ew;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/ob/ew;-><init>()V

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/bi;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/ew;->a(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/es;

    move-result-object v0

    return-object v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 333
    invoke-super {p0}, Lcom/yandex/metrica/impl/ak;->e()V

    .line 6074
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/bi;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 6075
    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bi;->a(Landroid/net/Uri$Builder;)V

    .line 6076
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/bi;->a(Ljava/lang/String;)V

    .line 335
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 229
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/bi;->k:Z

    if-nez v0, :cond_1

    .line 230
    iget-object v0, p0, Lcom/yandex/metrica/impl/bi;->o:Lcom/yandex/metrica/impl/ob/fx;

    if-nez v0, :cond_0

    .line 231
    sget-object v0, Lcom/yandex/metrica/impl/ob/fx;->a:Lcom/yandex/metrica/impl/ob/fx;

    iput-object v0, p0, Lcom/yandex/metrica/impl/bi;->o:Lcom/yandex/metrica/impl/ob/fx;

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/bi;->c:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/v;->l()Landroid/os/ResultReceiver;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/bi;->o:Lcom/yandex/metrica/impl/ob/fx;

    invoke-static {v0, v1}, Lcom/yandex/metrica/impl/k;->a(Landroid/os/ResultReceiver;Lcom/yandex/metrica/impl/ob/fx;)V

    .line 235
    :cond_1
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 222
    sget-object v0, Lcom/yandex/metrica/impl/ob/fx;->b:Lcom/yandex/metrica/impl/ob/fx;

    iput-object v0, p0, Lcom/yandex/metrica/impl/bi;->o:Lcom/yandex/metrica/impl/ob/fx;

    .line 223
    return-void
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 328
    const/4 v0, 0x1

    return v0
.end method

.method y()Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 85
    iget-object v0, p0, Lcom/yandex/metrica/impl/bi;->m:Lcom/yandex/metrica/impl/ob/dg;

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v4, v5}, Lcom/yandex/metrica/impl/ob/dg;->a(J)J

    move-result-wide v4

    .line 86
    iget-object v0, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v0, v4, v5}, Lcom/yandex/metrica/impl/bb;->a(J)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 87
    :goto_0
    iget-object v3, p0, Lcom/yandex/metrica/impl/bi;->c:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/ob/v;->k()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/yandex/metrica/CounterConfiguration;->u()Ljava/util/Map;

    move-result-object v3

    invoke-static {v3}, Lcom/yandex/metrica/impl/utils/o;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v3

    .line 88
    if-nez v0, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 89
    iget-object v0, p0, Lcom/yandex/metrica/impl/bi;->m:Lcom/yandex/metrica/impl/ob/dg;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dg;->o()Ljava/lang/String;

    move-result-object v0

    .line 90
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 97
    :cond_0
    :goto_1
    if-nez v0, :cond_7

    .line 98
    invoke-static {}, Lcom/yandex/metrica/impl/ob/dl;->a()Lcom/yandex/metrica/impl/ob/dl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dl;->d()Ljava/lang/String;

    move-result-object v0

    .line 99
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 100
    iget-object v0, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-static {v0}, Lcom/yandex/metrica/impl/bi;->a(Lcom/yandex/metrica/impl/bb;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 105
    :cond_1
    :goto_2
    return v1

    :cond_2
    move v0, v2

    .line 86
    goto :goto_0

    .line 93
    :cond_3
    iget-object v0, p0, Lcom/yandex/metrica/impl/bi;->m:Lcom/yandex/metrica/impl/ob/dg;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dg;->p()J

    move-result-wide v4

    .line 94
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    const-wide/32 v6, 0x15180

    cmp-long v0, v4, v6

    if-lez v0, :cond_4

    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v1, v2

    .line 100
    goto :goto_2

    .line 102
    :cond_6
    iget-object v3, p0, Lcom/yandex/metrica/impl/bi;->a:Lcom/yandex/metrica/impl/bb;

    invoke-static {v3}, Lcom/yandex/metrica/impl/bi;->a(Lcom/yandex/metrica/impl/bb;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v2

    goto :goto_2

    :cond_7
    move v1, v0

    goto :goto_2
.end method

.method declared-synchronized z()Z
    .locals 1

    .prologue
    .line 324
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/bi;->n:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
