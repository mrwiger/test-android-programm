.class public Lru/cn/api/catalogue/replies/RubricOptionValue;
.super Ljava/lang/Object;
.source "RubricOptionValue.java"


# instance fields
.field public selectedByDefault:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "selected_by_default"
    .end annotation
.end field

.field public title:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "title"
    .end annotation
.end field

.field public value:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "value"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/api/catalogue/replies/RubricOptionValue;->selectedByDefault:Z

    return-void
.end method
