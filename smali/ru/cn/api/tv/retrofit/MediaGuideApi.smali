.class public interface abstract Lru/cn/api/tv/retrofit/MediaGuideApi;
.super Ljava/lang/Object;
.source "MediaGuideApi.java"


# virtual methods
.method public abstract channelsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Lio/reactivex/Single;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Query;
            value = "channel"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Query;
            value = "fields"
        .end annotation
    .end param
    .param p3    # Ljava/lang/Long;
        .annotation runtime Lretrofit2/http/Query;
            value = "t"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lru/cn/api/tv/replies/ChannelInfoResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "channels.json"
    .end annotation
.end method

.method public abstract infosByTitles(Lru/cn/api/tv/TitlesRequest;)Lio/reactivex/Single;
    .param p1    # Lru/cn/api/tv/TitlesRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/api/tv/TitlesRequest;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lru/cn/api/tv/replies/ChannelInfoResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "idbytitle.json"
    .end annotation
.end method

.method public abstract schedule(JLjava/lang/String;J)Lio/reactivex/Single;
    .param p1    # J
        .annotation runtime Lretrofit2/http/Query;
            value = "channel"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Query;
            value = "dates"
        .end annotation
    .end param
    .param p4    # J
        .annotation runtime Lretrofit2/http/Query;
            value = "t"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "J)",
            "Lio/reactivex/Single",
            "<",
            "Lru/cn/api/tv/replies/TelecastsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "schedule.json"
    .end annotation
.end method

.method public abstract telecasts(Ljava/lang/String;)Lio/reactivex/Single;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Query;
            value = "telecast"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lru/cn/api/tv/replies/TelecastsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "telecastInfo.json"
    .end annotation
.end method
