.class public Lcom/my/target/br;
.super Landroid/app/Dialog;
.source "AdDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/br$a;
    }
.end annotation


# instance fields
.field private final hM:Lcom/my/target/br$a;


# direct methods
.method private constructor <init>(Lcom/my/target/br$a;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 32
    iput-object p1, p0, Lcom/my/target/br;->hM:Lcom/my/target/br$a;

    .line 33
    return-void
.end method

.method public static a(Lcom/my/target/br$a;Landroid/content/Context;)Lcom/my/target/br;
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/my/target/br;

    invoke-direct {v0, p0, p1}, Lcom/my/target/br;-><init>(Lcom/my/target/br$a;Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public aT()V
    .locals 2

    .prologue
    const/16 v1, 0x400

    .line 37
    invoke-virtual {p0}, Lcom/my/target/br;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 38
    if-eqz v0, :cond_0

    .line 40
    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 42
    :cond_0
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 59
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 60
    iget-object v0, p0, Lcom/my/target/br;->hM:Lcom/my/target/br$a;

    invoke-interface {v0}, Lcom/my/target/br$a;->aU()V

    .line 61
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, -0x1

    .line 66
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/my/target/br;->requestWindowFeature(I)Z

    .line 67
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/my/target/br;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 68
    invoke-virtual {p0, v0}, Lcom/my/target/br;->setContentView(Landroid/view/View;)V

    .line 69
    invoke-virtual {p0}, Lcom/my/target/br;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 70
    if-eqz v1, :cond_0

    .line 72
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 73
    invoke-virtual {v1, v4, v4}, Landroid/view/Window;->setLayout(II)V

    .line 75
    :cond_0
    iget-object v1, p0, Lcom/my/target/br;->hM:Lcom/my/target/br$a;

    invoke-interface {v1, p0, v0}, Lcom/my/target/br$a;->a(Lcom/my/target/br;Landroid/widget/FrameLayout;)V

    .line 76
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 77
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasFocus"    # Z

    .prologue
    .line 82
    iget-object v0, p0, Lcom/my/target/br;->hM:Lcom/my/target/br$a;

    invoke-interface {v0, p1}, Lcom/my/target/br$a;->i(Z)V

    .line 83
    invoke-super {p0, p1}, Landroid/app/Dialog;->onWindowFocusChanged(Z)V

    .line 84
    return-void
.end method

.method public setDimAmount(F)V
    .locals 2
    .param p1, "dimAmount"    # F

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/my/target/br;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 47
    if-eqz v0, :cond_0

    .line 49
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 50
    iput p1, v1, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 51
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 52
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 54
    :cond_0
    return-void
.end method
