.class public Lcom/google/obf/gr;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/ads/interactivemedia/v3/api/AdsManagerLoadedEvent;


# instance fields
.field private final a:Lcom/google/ads/interactivemedia/v3/api/AdsManager;

.field private final b:Lcom/google/ads/interactivemedia/v3/api/StreamManager;

.field private final c:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/google/ads/interactivemedia/v3/api/AdsManager;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/obf/gr;->a:Lcom/google/ads/interactivemedia/v3/api/AdsManager;

    .line 3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/gr;->b:Lcom/google/ads/interactivemedia/v3/api/StreamManager;

    .line 4
    iput-object p2, p0, Lcom/google/obf/gr;->c:Ljava/lang/Object;

    .line 5
    return-void
.end method

.method constructor <init>(Lcom/google/ads/interactivemedia/v3/api/StreamManager;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/gr;->a:Lcom/google/ads/interactivemedia/v3/api/AdsManager;

    .line 8
    iput-object p1, p0, Lcom/google/obf/gr;->b:Lcom/google/ads/interactivemedia/v3/api/StreamManager;

    .line 9
    iput-object p2, p0, Lcom/google/obf/gr;->c:Ljava/lang/Object;

    .line 10
    return-void
.end method


# virtual methods
.method public getAdsManager()Lcom/google/ads/interactivemedia/v3/api/AdsManager;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcom/google/obf/gr;->a:Lcom/google/ads/interactivemedia/v3/api/AdsManager;

    return-object v0
.end method
