.class Lru/cn/tv/playlists/UserPlaylistActionDialog$ChannelActionsItemClickListener;
.super Ljava/lang/Object;
.source "UserPlaylistActionDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/playlists/UserPlaylistActionDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChannelActionsItemClickListener"
.end annotation


# instance fields
.field private final location:Ljava/lang/String;

.field private final playlistId:J

.field final synthetic this$0:Lru/cn/tv/playlists/UserPlaylistActionDialog;

.field private final title:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lru/cn/tv/playlists/UserPlaylistActionDialog;JLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "playlistId"    # J
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "location"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lru/cn/tv/playlists/UserPlaylistActionDialog$ChannelActionsItemClickListener;->this$0:Lru/cn/tv/playlists/UserPlaylistActionDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-wide p2, p0, Lru/cn/tv/playlists/UserPlaylistActionDialog$ChannelActionsItemClickListener;->playlistId:J

    .line 68
    iput-object p4, p0, Lru/cn/tv/playlists/UserPlaylistActionDialog$ChannelActionsItemClickListener;->title:Ljava/lang/String;

    .line 69
    iput-object p5, p0, Lru/cn/tv/playlists/UserPlaylistActionDialog$ChannelActionsItemClickListener;->location:Ljava/lang/String;

    .line 70
    return-void
.end method

.method synthetic constructor <init>(Lru/cn/tv/playlists/UserPlaylistActionDialog;JLjava/lang/String;Ljava/lang/String;Lru/cn/tv/playlists/UserPlaylistActionDialog$1;)V
    .locals 0
    .param p1, "x0"    # Lru/cn/tv/playlists/UserPlaylistActionDialog;
    .param p2, "x1"    # J
    .param p4, "x2"    # Ljava/lang/String;
    .param p5, "x3"    # Ljava/lang/String;
    .param p6, "x4"    # Lru/cn/tv/playlists/UserPlaylistActionDialog$1;

    .prologue
    .line 59
    invoke-direct/range {p0 .. p5}, Lru/cn/tv/playlists/UserPlaylistActionDialog$ChannelActionsItemClickListener;-><init>(Lru/cn/tv/playlists/UserPlaylistActionDialog;JLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 74
    packed-switch p2, :pswitch_data_0

    .line 91
    :goto_0
    return-void

    .line 76
    :pswitch_0
    iget-wide v2, p0, Lru/cn/tv/playlists/UserPlaylistActionDialog$ChannelActionsItemClickListener;->playlistId:J

    iget-object v1, p0, Lru/cn/tv/playlists/UserPlaylistActionDialog$ChannelActionsItemClickListener;->title:Ljava/lang/String;

    iget-object v4, p0, Lru/cn/tv/playlists/UserPlaylistActionDialog$ChannelActionsItemClickListener;->location:Ljava/lang/String;

    invoke-static {v2, v3, v1, v4}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->newInstance(JLjava/lang/String;Ljava/lang/String;)Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    move-result-object v0

    .line 82
    .local v0, "editDialog":Lru/cn/tv/playlists/UserPlaylistAddEditDialog;
    iget-object v1, p0, Lru/cn/tv/playlists/UserPlaylistActionDialog$ChannelActionsItemClickListener;->this$0:Lru/cn/tv/playlists/UserPlaylistActionDialog;

    invoke-virtual {v1}, Lru/cn/tv/playlists/UserPlaylistActionDialog;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 85
    .end local v0    # "editDialog":Lru/cn/tv/playlists/UserPlaylistAddEditDialog;
    :pswitch_1
    iget-object v1, p0, Lru/cn/tv/playlists/UserPlaylistActionDialog$ChannelActionsItemClickListener;->this$0:Lru/cn/tv/playlists/UserPlaylistActionDialog;

    invoke-static {v1}, Lru/cn/tv/playlists/UserPlaylistActionDialog;->access$100(Lru/cn/tv/playlists/UserPlaylistActionDialog;)Lru/cn/tv/playlists/UserPlaylistActionViewModel;

    move-result-object v1

    iget-object v2, p0, Lru/cn/tv/playlists/UserPlaylistActionDialog$ChannelActionsItemClickListener;->location:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lru/cn/tv/playlists/UserPlaylistActionViewModel;->removePlaylist(Ljava/lang/String;)V

    goto :goto_0

    .line 74
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
