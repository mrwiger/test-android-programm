.class Lcom/google/obf/fj$a;
.super Ljava/util/AbstractSet;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/fj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractSet",
        "<",
        "Ljava/util/Map$Entry",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/obf/fj;


# direct methods
.method constructor <init>(Lcom/google/obf/fj;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/obf/fj$a;->a:Lcom/google/obf/fj;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lcom/google/obf/fj$a;->a:Lcom/google/obf/fj;

    invoke-virtual {v0}, Lcom/google/obf/fj;->clear()V

    .line 13
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 4
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/fj$a;->a:Lcom/google/obf/fj;

    check-cast p1, Ljava/util/Map$Entry;

    invoke-virtual {v0, p1}, Lcom/google/obf/fj;->a(Ljava/util/Map$Entry;)Lcom/google/obf/fj$d;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 3
    new-instance v0, Lcom/google/obf/fj$a$1;

    invoke-direct {v0, p0}, Lcom/google/obf/fj$a$1;-><init>(Lcom/google/obf/fj$a;)V

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 5
    instance-of v2, p1, Ljava/util/Map$Entry;

    if-nez v2, :cond_1

    .line 11
    :cond_0
    :goto_0
    return v0

    .line 7
    :cond_1
    iget-object v2, p0, Lcom/google/obf/fj$a;->a:Lcom/google/obf/fj;

    check-cast p1, Ljava/util/Map$Entry;

    invoke-virtual {v2, p1}, Lcom/google/obf/fj;->a(Ljava/util/Map$Entry;)Lcom/google/obf/fj$d;

    move-result-object v2

    .line 8
    if-eqz v2, :cond_0

    .line 10
    iget-object v0, p0, Lcom/google/obf/fj$a;->a:Lcom/google/obf/fj;

    invoke-virtual {v0, v2, v1}, Lcom/google/obf/fj;->a(Lcom/google/obf/fj$d;Z)V

    move v0, v1

    .line 11
    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 2
    iget-object v0, p0, Lcom/google/obf/fj$a;->a:Lcom/google/obf/fj;

    iget v0, v0, Lcom/google/obf/fj;->c:I

    return v0
.end method
