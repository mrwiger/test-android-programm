.class public Lru/cn/api/provider/cursor/ScheduleItemCursor;
.super Lru/cn/api/provider/cursor/MatrixCursorEx;
.source "ScheduleItemCursor.java"


# static fields
.field private static columnNames:[Ljava/lang/String;


# instance fields
.field private final channelIdIndex:I

.field private final channelTitleIndex:I

.field private final durationIndex:I

.field private final has_recordIndex:I

.field private final isDenied_Index:I

.field private final telecastIdIndex:I

.field private final timeIndex:I

.field private final titleIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 5
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "telecastId"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "cnId"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "channelTitle"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "time"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "has_record"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "is_denied"

    aput-object v2, v0, v1

    sput-object v0, Lru/cn/api/provider/cursor/ScheduleItemCursor;->columnNames:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lru/cn/api/provider/cursor/ScheduleItemCursor;->columnNames:[Ljava/lang/String;

    invoke-direct {p0, v0}, Lru/cn/api/provider/cursor/MatrixCursorEx;-><init>([Ljava/lang/String;)V

    .line 19
    const-string v0, "cnId"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/ScheduleItemCursor;->channelIdIndex:I

    .line 20
    const-string v0, "telecastId"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/ScheduleItemCursor;->telecastIdIndex:I

    .line 21
    const-string v0, "channelTitle"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/ScheduleItemCursor;->channelTitleIndex:I

    .line 22
    const-string v0, "time"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/ScheduleItemCursor;->timeIndex:I

    .line 23
    const-string v0, "duration"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/ScheduleItemCursor;->durationIndex:I

    .line 24
    const-string v0, "title"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/ScheduleItemCursor;->titleIndex:I

    .line 25
    const-string v0, "has_record"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/ScheduleItemCursor;->has_recordIndex:I

    .line 26
    const-string v0, "is_denied"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/ScheduleItemCursor;->isDenied_Index:I

    .line 27
    return-void
.end method


# virtual methods
.method public addRow(JJLjava/lang/String;JJLjava/lang/String;ZZ)V
    .locals 3
    .param p1, "telecastId"    # J
    .param p3, "cnId"    # J
    .param p5, "channelTitle"    # Ljava/lang/String;
    .param p6, "time"    # J
    .param p8, "duration"    # J
    .param p10, "title"    # Ljava/lang/String;
    .param p11, "hasRecord"    # Z
    .param p12, "isDenied"    # Z

    .prologue
    .line 31
    const/16 v0, 0x9

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x2

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x3

    aput-object p5, v1, v0

    const/4 v0, 0x4

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x5

    invoke-static {p8, p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x6

    aput-object p10, v1, v0

    const/4 v2, 0x7

    if-eqz p11, :cond_0

    const/4 v0, 0x1

    .line 32
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v2

    const/16 v2, 0x8

    if-eqz p12, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v2

    .line 31
    invoke-virtual {p0, v1}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->addRow([Ljava/lang/Object;)V

    .line 33
    return-void

    .line 31
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 32
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getChannelId()J
    .locals 2

    .prologue
    .line 36
    iget v0, p0, Lru/cn/api/provider/cursor/ScheduleItemCursor;->channelIdIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getChannelTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lru/cn/api/provider/cursor/ScheduleItemCursor;->channelTitleIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 52
    iget v0, p0, Lru/cn/api/provider/cursor/ScheduleItemCursor;->durationIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getTelecastId()J
    .locals 2

    .prologue
    .line 44
    iget v0, p0, Lru/cn/api/provider/cursor/ScheduleItemCursor;->telecastIdIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getTime()J
    .locals 2

    .prologue
    .line 48
    iget v0, p0, Lru/cn/api/provider/cursor/ScheduleItemCursor;->timeIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lru/cn/api/provider/cursor/ScheduleItemCursor;->titleIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasRecords()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 72
    iget v1, p0, Lru/cn/api/provider/cursor/ScheduleItemCursor;->has_recordIndex:I

    invoke-virtual {p0, v1}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getInt(I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDenied()Z
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lru/cn/api/provider/cursor/ScheduleItemCursor;->isDenied_Index:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOnTime(J)Z
    .locals 9
    .param p1, "timeMillis"    # J

    .prologue
    .line 64
    invoke-virtual {p0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getTime()J

    move-result-wide v2

    .line 65
    .local v2, "startTime":J
    invoke-virtual {p0}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getDuration()J

    move-result-wide v0

    .line 67
    .local v0, "duration":J
    const-wide/16 v6, 0x3e8

    div-long v4, p1, v6

    .line 68
    .local v4, "time":J
    cmp-long v6, v4, v2

    if-ltz v6, :cond_0

    add-long v6, v2, v0

    cmp-long v6, v4, v6

    if-gez v6, :cond_0

    const/4 v6, 0x1

    :goto_0
    return v6

    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method
