.class public abstract Lcom/my/target/c;
.super Ljava/lang/Object;
.source "AdFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/c$b;,
        Lcom/my/target/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/my/target/ak;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected final adConfig:Lcom/my/target/b;

.field protected final r:Lcom/my/target/c$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/my/target/c$a",
            "<TT;>;"
        }
    .end annotation
.end field

.field protected s:Ljava/lang/String;

.field private u:Lcom/my/target/c$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/my/target/c$b",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/my/target/c$a;Lcom/my/target/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/c$a",
            "<TT;>;",
            "Lcom/my/target/b;",
            ")V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/my/target/c;->r:Lcom/my/target/c$a;

    .line 36
    iput-object p2, p0, Lcom/my/target/c;->adConfig:Lcom/my/target/b;

    .line 37
    return-void
.end method

.method static synthetic a(Lcom/my/target/c;)Lcom/my/target/c$b;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/my/target/c;->u:Lcom/my/target/c$b;

    return-object v0
.end method

.method static synthetic a(Lcom/my/target/c;Lcom/my/target/c$b;)Lcom/my/target/c$b;
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lcom/my/target/c;->u:Lcom/my/target/c$b;

    return-object p1
.end method


# virtual methods
.method protected a(Lcom/my/target/ae;Lcom/my/target/ak;Lcom/my/target/d;Lcom/my/target/at;Landroid/content/Context;)Lcom/my/target/ak;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/ae;",
            "TT;",
            "Lcom/my/target/d",
            "<TT;>;",
            "Lcom/my/target/at;",
            "Landroid/content/Context;",
            ")TT;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 140
    invoke-virtual {p1}, Lcom/my/target/ae;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0, p5}, Lcom/my/target/at;->f(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/Object;

    .line 141
    invoke-virtual {p4}, Lcom/my/target/at;->ak()Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    :goto_0
    return-object p2

    .line 146
    :cond_0
    const-string v0, "serviceRequested"

    invoke-virtual {p1, v0}, Lcom/my/target/ae;->n(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0, p5}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 150
    if-eqz p2, :cond_4

    .line 152
    invoke-virtual {p2}, Lcom/my/target/ak;->getBannersCount()I

    move-result v0

    move v6, v0

    .line 155
    :goto_1
    invoke-virtual {p4}, Lcom/my/target/at;->al()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 156
    if-eqz v1, :cond_3

    .line 158
    iget-object v4, p0, Lcom/my/target/c;->adConfig:Lcom/my/target/b;

    move-object v0, p3

    move-object v2, p1

    move-object v3, p2

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/my/target/d;->a(Ljava/lang/String;Lcom/my/target/ae;Lcom/my/target/ak;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/ak;

    move-result-object v2

    .line 159
    invoke-virtual {p1}, Lcom/my/target/ae;->D()Ljava/util/ArrayList;

    move-result-object v1

    move-object v0, p0

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/my/target/c;->a(Ljava/util/List;Lcom/my/target/ak;Lcom/my/target/d;Lcom/my/target/at;Landroid/content/Context;)Lcom/my/target/ak;

    move-result-object v2

    .line 162
    :goto_2
    if-eqz v2, :cond_1

    .line 164
    invoke-virtual {v2}, Lcom/my/target/ak;->getBannersCount()I

    move-result v7

    .line 167
    :cond_1
    if-ne v6, v7, :cond_2

    .line 169
    const-string v0, "serviceAnswerEmpty"

    invoke-virtual {p1, v0}, Lcom/my/target/ae;->n(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0, p5}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 170
    invoke-virtual {p1}, Lcom/my/target/ae;->B()Lcom/my/target/ae;

    move-result-object v1

    .line 171
    if-eqz v1, :cond_2

    move-object v0, p0

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 173
    invoke-virtual/range {v0 .. v5}, Lcom/my/target/c;->a(Lcom/my/target/ae;Lcom/my/target/ak;Lcom/my/target/d;Lcom/my/target/at;Landroid/content/Context;)Lcom/my/target/ak;

    move-result-object v2

    :cond_2
    move-object p2, v2

    .line 176
    goto :goto_0

    :cond_3
    move-object v2, p2

    goto :goto_2

    :cond_4
    move v6, v7

    goto :goto_1
.end method

.method protected a(Ljava/util/List;Lcom/my/target/ak;Lcom/my/target/d;Lcom/my/target/at;Landroid/content/Context;)Lcom/my/target/ak;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/ae;",
            ">;TT;",
            "Lcom/my/target/d",
            "<TT;>;",
            "Lcom/my/target/at;",
            "Landroid/content/Context;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 124
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 126
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v2, p2

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/my/target/ae;

    move-object v0, p0

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 128
    invoke-virtual/range {v0 .. v5}, Lcom/my/target/c;->a(Lcom/my/target/ae;Lcom/my/target/ak;Lcom/my/target/d;Lcom/my/target/at;Landroid/content/Context;)Lcom/my/target/ak;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object v2, p2

    .line 131
    :cond_1
    return-object v2
.end method

.method public final a(Landroid/content/Context;)Lcom/my/target/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/my/target/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 50
    new-instance v1, Lcom/my/target/c$1;

    invoke-direct {v1, p0, v0}, Lcom/my/target/c$1;-><init>(Lcom/my/target/c;Landroid/content/Context;)V

    invoke-static {v1}, Lcom/my/target/h;->a(Ljava/lang/Runnable;)V

    .line 59
    return-object p0
.end method

.method public final a(Lcom/my/target/c$b;)Lcom/my/target/c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/c$b",
            "<TT;>;)",
            "Lcom/my/target/c",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 42
    iput-object p1, p0, Lcom/my/target/c;->u:Lcom/my/target/c$b;

    .line 43
    return-object p0
.end method

.method protected a(Lcom/my/target/ae;Lcom/my/target/at;Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p1}, Lcom/my/target/ae;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0, p3}, Lcom/my/target/at;->f(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/Object;

    .line 107
    invoke-virtual {p2}, Lcom/my/target/at;->ak()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    invoke-virtual {p2}, Lcom/my/target/at;->al()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 115
    :goto_0
    return-object v0

    .line 113
    :cond_0
    invoke-virtual {p2}, Lcom/my/target/at;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/c;->s:Ljava/lang/String;

    .line 115
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/my/target/ak;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, Lcom/my/target/c;->u:Lcom/my/target/c$b;

    if-nez v0, :cond_0

    .line 206
    :goto_0
    return-void

    .line 186
    :cond_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 188
    iget-object v0, p0, Lcom/my/target/c;->u:Lcom/my/target/c$b;

    invoke-interface {v0, p1, p2}, Lcom/my/target/c$b;->b(Lcom/my/target/ak;Ljava/lang/String;)V

    .line 189
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/c;->u:Lcom/my/target/c$b;

    goto :goto_0

    .line 193
    :cond_1
    new-instance v0, Lcom/my/target/c$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/my/target/c$2;-><init>(Lcom/my/target/c;Lcom/my/target/ak;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/my/target/h;->c(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;)Lcom/my/target/ak;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 65
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 67
    const-string v0, "[AdFactory] method getAdSync called from main thread"

    invoke-static {v0}, Lcom/my/target/g;->b(Ljava/lang/String;)V

    .line 68
    const/4 v0, 0x0

    .line 72
    :goto_0
    return-object v0

    .line 70
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 71
    invoke-virtual {p0, v0}, Lcom/my/target/c;->c(Landroid/content/Context;)Lcom/my/target/ak;

    move-result-object v0

    goto :goto_0
.end method

.method protected c(Landroid/content/Context;)Lcom/my/target/ak;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")TT;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 77
    iget-object v0, p0, Lcom/my/target/c;->r:Lcom/my/target/c$a;

    invoke-interface {v0}, Lcom/my/target/c$a;->d()Lcom/my/target/f;

    move-result-object v0

    .line 78
    iget-object v1, p0, Lcom/my/target/c;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0, v1, p1}, Lcom/my/target/f;->a(Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/ae;

    move-result-object v2

    .line 79
    invoke-static {}, Lcom/my/target/at;->ah()Lcom/my/target/at;

    move-result-object v6

    .line 80
    invoke-virtual {p0, v2, v6, p1}, Lcom/my/target/c;->a(Lcom/my/target/ae;Lcom/my/target/at;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 81
    if-nez v1, :cond_0

    .line 99
    :goto_0
    return-object v3

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/my/target/c;->r:Lcom/my/target/c$a;

    invoke-interface {v0}, Lcom/my/target/c$a;->b()Lcom/my/target/d;

    move-result-object v0

    .line 86
    iget-object v4, p0, Lcom/my/target/c;->adConfig:Lcom/my/target/b;

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/my/target/d;->a(Ljava/lang/String;Lcom/my/target/ae;Lcom/my/target/ak;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/ak;

    move-result-object v3

    .line 87
    iget-object v1, p0, Lcom/my/target/c;->r:Lcom/my/target/c$a;

    invoke-interface {v1}, Lcom/my/target/c$a;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 89
    invoke-virtual {v2}, Lcom/my/target/ae;->D()Ljava/util/ArrayList;

    move-result-object v2

    move-object v1, p0

    move-object v4, v0

    move-object v5, v6

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/my/target/c;->a(Ljava/util/List;Lcom/my/target/ak;Lcom/my/target/d;Lcom/my/target/at;Landroid/content/Context;)Lcom/my/target/ak;

    move-result-object v0

    .line 91
    :goto_1
    if-eqz v0, :cond_1

    .line 93
    iget-object v1, p0, Lcom/my/target/c;->r:Lcom/my/target/c$a;

    invoke-interface {v1}, Lcom/my/target/c$a;->c()Lcom/my/target/e;

    move-result-object v1

    .line 94
    if-eqz v1, :cond_1

    .line 96
    iget-object v2, p0, Lcom/my/target/c;->adConfig:Lcom/my/target/b;

    invoke-virtual {v1, v0, v2, p1}, Lcom/my/target/e;->a(Lcom/my/target/ak;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/ak;

    move-result-object v0

    :cond_1
    move-object v3, v0

    .line 99
    goto :goto_0

    :cond_2
    move-object v0, v3

    goto :goto_1
.end method
