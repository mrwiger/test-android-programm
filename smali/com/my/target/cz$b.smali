.class final Lcom/my/target/cz$b;
.super Landroid/support/v7/widget/LinearLayoutManager;
.source "PromoCardImageRecyclerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/cz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation


# instance fields
.field private aX:Lcom/my/target/ct$a;

.field private dividerPadding:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 155
    invoke-direct {p0, p1, v0, v0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 156
    return-void
.end method


# virtual methods
.method public final a(Lcom/my/target/ct$a;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/my/target/cz$b;->aX:Lcom/my/target/ct$a;

    .line 192
    return-void
.end method

.method public final measureChildWithMargins(Landroid/view/View;II)V
    .locals 3
    .param p1, "child"    # Landroid/view/View;
    .param p2, "widthUsed"    # I
    .param p3, "heightUsed"    # I

    .prologue
    .line 161
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    .line 164
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->getWidth()I

    move-result v1

    .line 165
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->getHeight()I

    move-result v2

    .line 167
    if-lez v2, :cond_0

    if-gtz v1, :cond_1

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 172
    :cond_1
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;->getItemViewType(Landroid/view/View;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 174
    iget v1, p0, Lcom/my/target/cz$b;->dividerPadding:I

    iput v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    .line 186
    :goto_1
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/LinearLayoutManager;->measureChildWithMargins(Landroid/view/View;II)V

    goto :goto_0

    .line 176
    :cond_2
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;->getItemViewType(Landroid/view/View;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 178
    iget v1, p0, Lcom/my/target/cz$b;->dividerPadding:I

    iput v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    goto :goto_1

    .line 182
    :cond_3
    iget v1, p0, Lcom/my/target/cz$b;->dividerPadding:I

    iput v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    .line 183
    iget v1, p0, Lcom/my/target/cz$b;->dividerPadding:I

    iput v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    goto :goto_1
.end method

.method public final onLayoutCompleted(Landroid/support/v7/widget/RecyclerView$State;)V
    .locals 1
    .param p1, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .prologue
    .line 197
    invoke-super {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;->onLayoutCompleted(Landroid/support/v7/widget/RecyclerView$State;)V

    .line 198
    iget-object v0, p0, Lcom/my/target/cz$b;->aX:Lcom/my/target/ct$a;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/my/target/cz$b;->aX:Lcom/my/target/ct$a;

    invoke-interface {v0}, Lcom/my/target/ct$a;->onLayoutCompleted()V

    .line 202
    :cond_0
    return-void
.end method

.method public final setDividerPadding(I)V
    .locals 0
    .param p1, "padding"    # I

    .prologue
    .line 206
    iput p1, p0, Lcom/my/target/cz$b;->dividerPadding:I

    .line 207
    return-void
.end method
