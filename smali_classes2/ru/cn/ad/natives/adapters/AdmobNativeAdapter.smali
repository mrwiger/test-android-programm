.class public Lru/cn/ad/natives/adapters/AdmobNativeAdapter;
.super Lru/cn/ad/natives/adapters/NativeAdAdapter;
.source "AdmobNativeAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;,
        Lru/cn/ad/natives/adapters/AdmobNativeAdapter$InstallBinder;,
        Lru/cn/ad/natives/adapters/AdmobNativeAdapter$AdmobBinder;
    }
.end annotation


# instance fields
.field private final bannerId:Ljava/lang/String;

.field private viewBinder:Lru/cn/ad/natives/adapters/AdmobNativeAdapter$AdmobBinder;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "system"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lru/cn/ad/natives/adapters/NativeAdAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    .line 37
    const-string v0, "banner_id"

    invoke-virtual {p2, v0}, Lru/cn/api/money_miner/replies/AdSystem;->getParamOrThrow(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->bannerId:Ljava/lang/String;

    .line 38
    return-void
.end method

.method static synthetic access$000(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$1000(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$1100(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$402(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;Lru/cn/ad/natives/adapters/AdmobNativeAdapter$AdmobBinder;)Lru/cn/ad/natives/adapters/AdmobNativeAdapter$AdmobBinder;
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/AdmobNativeAdapter;
    .param p1, "x1"    # Lru/cn/ad/natives/adapters/AdmobNativeAdapter$AdmobBinder;

    .prologue
    .line 27
    iput-object p1, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->viewBinder:Lru/cn/ad/natives/adapters/AdmobNativeAdapter$AdmobBinder;

    return-object p1
.end method

.method static synthetic access$500(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$700(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$800(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$900(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 131
    invoke-virtual {p0}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->getPresenter()Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    move-result-object v0

    .line 132
    .local v0, "presenter":Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;
    if-eqz v0, :cond_0

    .line 133
    invoke-interface {v0}, Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;->destroy()V

    .line 136
    :cond_0
    iget-object v1, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->viewBinder:Lru/cn/ad/natives/adapters/AdmobNativeAdapter$AdmobBinder;

    if-eqz v1, :cond_1

    .line 137
    iget-object v1, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->viewBinder:Lru/cn/ad/natives/adapters/AdmobNativeAdapter$AdmobBinder;

    invoke-virtual {v1}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$AdmobBinder;->destroy()V

    .line 139
    :cond_1
    return-void
.end method

.method protected onLoad()V
    .locals 6

    .prologue
    .line 42
    new-instance v3, Lcom/google/android/gms/ads/AdLoader$Builder;

    iget-object v4, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->context:Landroid/content/Context;

    iget-object v5, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->bannerId:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/ads/AdLoader$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v4, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$3;

    invoke-direct {v4, p0}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$3;-><init>(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)V

    .line 43
    invoke-virtual {v3, v4}, Lcom/google/android/gms/ads/AdLoader$Builder;->forAppInstallAd(Lcom/google/android/gms/ads/formats/NativeAppInstallAd$OnAppInstallAdLoadedListener;)Lcom/google/android/gms/ads/AdLoader$Builder;

    move-result-object v3

    new-instance v4, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$2;

    invoke-direct {v4, p0}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$2;-><init>(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)V

    .line 53
    invoke-virtual {v3, v4}, Lcom/google/android/gms/ads/AdLoader$Builder;->forContentAd(Lcom/google/android/gms/ads/formats/NativeContentAd$OnContentAdLoadedListener;)Lcom/google/android/gms/ads/AdLoader$Builder;

    move-result-object v3

    new-instance v4, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$1;

    invoke-direct {v4, p0}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$1;-><init>(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)V

    .line 63
    invoke-virtual {v3, v4}, Lcom/google/android/gms/ads/AdLoader$Builder;->withAdListener(Lcom/google/android/gms/ads/AdListener;)Lcom/google/android/gms/ads/AdLoader$Builder;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/ads/formats/NativeAdOptions$Builder;

    invoke-direct {v4}, Lcom/google/android/gms/ads/formats/NativeAdOptions$Builder;-><init>()V

    const/4 v5, 0x2

    .line 99
    invoke-virtual {v4, v5}, Lcom/google/android/gms/ads/formats/NativeAdOptions$Builder;->setAdChoicesPlacement(I)Lcom/google/android/gms/ads/formats/NativeAdOptions$Builder;

    move-result-object v4

    .line 100
    invoke-virtual {v4}, Lcom/google/android/gms/ads/formats/NativeAdOptions$Builder;->build()Lcom/google/android/gms/ads/formats/NativeAdOptions;

    move-result-object v4

    .line 98
    invoke-virtual {v3, v4}, Lcom/google/android/gms/ads/AdLoader$Builder;->withNativeAdOptions(Lcom/google/android/gms/ads/formats/NativeAdOptions;)Lcom/google/android/gms/ads/AdLoader$Builder;

    move-result-object v3

    .line 101
    invoke-virtual {v3}, Lcom/google/android/gms/ads/AdLoader$Builder;->build()Lcom/google/android/gms/ads/AdLoader;

    move-result-object v0

    .line 103
    .local v0, "adLoader":Lcom/google/android/gms/ads/AdLoader;
    new-instance v2, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v2}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    .line 104
    .local v2, "adRequestBuilder":Lcom/google/android/gms/ads/AdRequest$Builder;
    iget-object v3, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->context:Landroid/content/Context;

    invoke-static {v3}, Lru/cn/ad/AdsManager;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 105
    iget-object v3, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->context:Landroid/content/Context;

    invoke-static {v3}, Lru/cn/ad/AdsManager;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/ads/AdRequest$Builder;->addTestDevice(Ljava/lang/String;)Lcom/google/android/gms/ads/AdRequest$Builder;

    .line 107
    :cond_0
    invoke-virtual {v2}, Lcom/google/android/gms/ads/AdRequest$Builder;->build()Lcom/google/android/gms/ads/AdRequest;

    move-result-object v1

    .line 109
    .local v1, "adRequest":Lcom/google/android/gms/ads/AdRequest;
    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/AdLoader;->loadAd(Lcom/google/android/gms/ads/AdRequest;)V

    .line 110
    return-void
.end method

.method public show()V
    .locals 3

    .prologue
    .line 114
    invoke-virtual {p0}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->getPresenter()Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    move-result-object v0

    .line 115
    .local v0, "presenter":Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;
    iget-object v1, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->viewBinder:Lru/cn/ad/natives/adapters/AdmobNativeAdapter$AdmobBinder;

    new-instance v2, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$4;

    invoke-direct {v2, p0}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$4;-><init>(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)V

    invoke-interface {v0, v1, v2}, Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;->show(Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;Ljava/lang/Runnable;)V

    .line 122
    iget-object v1, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    if-eqz v1, :cond_0

    .line 123
    iget-object v1, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    invoke-interface {v1}, Lru/cn/ad/AdAdapter$Listener;->onAdStarted()V

    .line 126
    :cond_0
    sget-object v1, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_START:Lru/cn/domain/statistics/inetra/AdvEvent;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/util/Map;)V

    .line 127
    return-void
.end method
