.class public Lcom/yandex/metrica/impl/ob/ci;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ob/ci$a;
    }
.end annotation


# static fields
.field private static volatile a:Lcom/yandex/metrica/impl/ob/ci;

.field private static final b:Ljava/lang/Object;


# instance fields
.field private c:Lcom/yandex/metrica/impl/ob/cj;

.field private d:Lcom/yandex/metrica/impl/ob/ci$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/yandex/metrica/impl/ob/ci;->b:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 92
    new-instance v0, Lcom/yandex/metrica/impl/ob/cj;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/cj;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/ob/ci;-><init>(Lcom/yandex/metrica/impl/ob/cj;)V

    .line 93
    return-void
.end method

.method constructor <init>(Lcom/yandex/metrica/impl/ob/cj;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/ci;->c:Lcom/yandex/metrica/impl/ob/cj;

    .line 114
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ci;->c:Lcom/yandex/metrica/impl/ob/cj;

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/cj;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 116
    if-eqz v0, :cond_1

    .line 118
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 119
    new-instance v0, Lcom/yandex/metrica/impl/ob/ci$a;

    invoke-direct {v0, v2}, Lcom/yandex/metrica/impl/ob/ci$a;-><init>(Lorg/json/JSONArray;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    :goto_0
    if-nez v0, :cond_0

    .line 125
    new-instance v0, Lcom/yandex/metrica/impl/ob/ci$a;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/ob/ci$a;-><init>()V

    .line 127
    :cond_0
    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ci;->d:Lcom/yandex/metrica/impl/ob/ci$a;

    .line 128
    return-void

    :catch_0
    move-exception v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/ci;
    .locals 2

    .prologue
    .line 75
    sget-object v0, Lcom/yandex/metrica/impl/ob/ci;->a:Lcom/yandex/metrica/impl/ob/ci;

    if-nez v0, :cond_1

    .line 76
    sget-object v1, Lcom/yandex/metrica/impl/ob/ci;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 77
    :try_start_0
    sget-object v0, Lcom/yandex/metrica/impl/ob/ci;->a:Lcom/yandex/metrica/impl/ob/ci;

    if-nez v0, :cond_0

    .line 78
    new-instance v0, Lcom/yandex/metrica/impl/ob/ci;

    invoke-direct {v0, p0}, Lcom/yandex/metrica/impl/ob/ci;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/ci;->a:Lcom/yandex/metrica/impl/ob/ci;

    .line 80
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    :cond_1
    sget-object v0, Lcom/yandex/metrica/impl/ob/ci;->a:Lcom/yandex/metrica/impl/ob/ci;

    return-object v0

    .line 80
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/ch;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ci;->d:Lcom/yandex/metrica/impl/ob/ci$a;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/ci$a;->a(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/ch;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/yandex/metrica/impl/ob/ch;)V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ci;->d:Lcom/yandex/metrica/impl/ob/ci$a;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/ci$a;->a(Lcom/yandex/metrica/impl/ob/ch;)V

    .line 1106
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ci;->d:Lcom/yandex/metrica/impl/ob/ci$a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/ci$a;->a()Lorg/json/JSONArray;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1108
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ci;->c:Lcom/yandex/metrica/impl/ob/cj;

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/ob/cj;->a(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/cj;

    .line 98
    return-void
.end method
