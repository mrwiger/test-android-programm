.class public final Lcom/my/target/core/parsers/d;
.super Ljava/lang/Object;
.source "InterstitialAdImageBannerParser.java"


# instance fields
.field private final adConfig:Lcom/my/target/b;

.field private final context:Landroid/content/Context;

.field private final z:Lcom/my/target/ae;


# direct methods
.method private constructor <init>(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/my/target/core/parsers/d;->z:Lcom/my/target/ae;

    .line 41
    iput-object p2, p0, Lcom/my/target/core/parsers/d;->adConfig:Lcom/my/target/b;

    .line 42
    iput-object p3, p0, Lcom/my/target/core/parsers/d;->context:Landroid/content/Context;

    .line 43
    return-void
.end method

.method private a(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/my/target/common/models/ImageData;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 98
    const-string v1, "imageLink"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 99
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 101
    const-string v1, "InterstitialAdImageBanner no imageLink for image"

    const-string v2, "Required field"

    invoke-direct {p0, v1, v2, p2}, Lcom/my/target/core/parsers/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    :goto_0
    return-object v0

    .line 105
    :cond_0
    const-string v2, "width"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    .line 106
    const-string v3, "height"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    .line 107
    if-lez v2, :cond_1

    if-gtz v3, :cond_2

    .line 109
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "InterstitialAdImageBanner  image has wrong dimensions, w = "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", h = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Required field"

    invoke-direct {p0, v1, v2, p2}, Lcom/my/target/core/parsers/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 113
    :cond_2
    invoke-static {v1, v2, v3}, Lcom/my/target/common/models/ImageData;->newImageData(Ljava/lang/String;II)Lcom/my/target/common/models/ImageData;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/d;
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/my/target/core/parsers/d;

    invoke-direct {v0, p0, p1, p2}, Lcom/my/target/core/parsers/d;-><init>(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 118
    invoke-static {p2}, Lcom/my/target/az;->y(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/my/target/az;->z(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    .line 119
    invoke-virtual {v0, p3}, Lcom/my/target/az;->B(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/parsers/d;->adConfig:Lcom/my/target/b;

    .line 120
    invoke-virtual {v1}, Lcom/my/target/b;->getSlotId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/az;->h(I)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/parsers/d;->z:Lcom/my/target/ae;

    .line 121
    invoke-virtual {v1}, Lcom/my/target/ae;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/az;->A(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/parsers/d;->context:Landroid/content/Context;

    .line 122
    invoke-virtual {v0, v1}, Lcom/my/target/az;->e(Landroid/content/Context;)V

    .line 123
    return-void
.end method


# virtual methods
.method public final b(Lorg/json/JSONObject;Lcom/my/target/core/models/banners/g;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 47
    const-string v1, "portrait"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 48
    const-string v1, "landscape"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 50
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-gtz v1, :cond_3

    :cond_0
    if-eqz v3, :cond_1

    .line 51
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-gtz v1, :cond_3

    .line 53
    :cond_1
    const-string v1, "No images in InterstitialAdImageBanner"

    const-string v2, "Required field"

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/g;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/my/target/core/parsers/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    :cond_2
    :goto_0
    return v0

    .line 59
    :cond_3
    if-eqz v2, :cond_5

    .line 61
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    move v1, v0

    .line 62
    :goto_1
    if-ge v1, v4, :cond_5

    .line 64
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 65
    if-eqz v5, :cond_4

    .line 67
    invoke-virtual {p2}, Lcom/my/target/core/models/banners/g;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v5, v6}, Lcom/my/target/core/parsers/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/my/target/common/models/ImageData;

    move-result-object v5

    .line 68
    if-eqz v5, :cond_4

    .line 70
    invoke-virtual {p2, v5}, Lcom/my/target/core/models/banners/g;->addPortraitImage(Lcom/my/target/common/models/ImageData;)V

    .line 62
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 76
    :cond_5
    if-eqz v3, :cond_7

    .line 78
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v2

    move v1, v0

    .line 79
    :goto_2
    if-ge v1, v2, :cond_7

    .line 81
    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 82
    if-eqz v4, :cond_6

    .line 84
    invoke-virtual {p2}, Lcom/my/target/core/models/banners/g;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/my/target/core/parsers/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/my/target/common/models/ImageData;

    move-result-object v4

    .line 85
    if-eqz v4, :cond_6

    .line 87
    invoke-virtual {p2, v4}, Lcom/my/target/core/models/banners/g;->addLandscapeImage(Lcom/my/target/common/models/ImageData;)V

    .line 79
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 93
    :cond_7
    invoke-virtual {p2}, Lcom/my/target/core/models/banners/g;->getLandscapeImages()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {p2}, Lcom/my/target/core/models/banners/g;->getPortraitImages()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_8
    const/4 v0, 0x1

    goto :goto_0
.end method
