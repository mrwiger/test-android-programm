.class public Lcom/yandex/metrica/impl/ob/ga;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ob/ga$a;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:J

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private final j:Lcom/yandex/metrica/impl/ob/dc;


# direct methods
.method public constructor <init>(Lcom/yandex/metrica/impl/ob/dc;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/ga;->j:Lcom/yandex/metrica/impl/ob/dc;

    .line 44
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->j:Lcom/yandex/metrica/impl/ob/dc;

    invoke-virtual {v0, v4}, Lcom/yandex/metrica/impl/ob/dc;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->b:Ljava/lang/String;

    .line 45
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->j:Lcom/yandex/metrica/impl/ob/dc;

    invoke-virtual {v0, v4}, Lcom/yandex/metrica/impl/ob/dc;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->c:Ljava/lang/String;

    .line 46
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->j:Lcom/yandex/metrica/impl/ob/dc;

    invoke-virtual {v0, v4}, Lcom/yandex/metrica/impl/ob/dc;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->d:Ljava/lang/String;

    .line 47
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->j:Lcom/yandex/metrica/impl/ob/dc;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dc;->b()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->f:Ljava/util/List;

    .line 48
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->j:Lcom/yandex/metrica/impl/ob/dc;

    invoke-virtual {v0, v4}, Lcom/yandex/metrica/impl/ob/dc;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->a:Ljava/lang/String;

    .line 49
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->j:Lcom/yandex/metrica/impl/ob/dc;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/dc;->a(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/yandex/metrica/impl/ob/ga;->g:J

    .line 50
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->j:Lcom/yandex/metrica/impl/ob/dc;

    invoke-virtual {v0, v4}, Lcom/yandex/metrica/impl/ob/dc;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->h:Ljava/lang/String;

    .line 51
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->j:Lcom/yandex/metrica/impl/ob/dc;

    invoke-virtual {v0, v4}, Lcom/yandex/metrica/impl/ob/dc;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->i:Ljava/lang/String;

    .line 52
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->j:Lcom/yandex/metrica/impl/ob/dc;

    invoke-virtual {v0, v4}, Lcom/yandex/metrica/impl/ob/dc;->n(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->e:Ljava/lang/String;

    .line 54
    invoke-direct {p0, p2}, Lcom/yandex/metrica/impl/ob/ga;->b(Ljava/lang/String;)V

    .line 55
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ga;->e()V

    .line 56
    return-void
.end method

.method private declared-synchronized b(J)V
    .locals 1

    .prologue
    .line 217
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lcom/yandex/metrica/impl/ob/ga;->g:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    monitor-exit p0

    return-void

    .line 217
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 176
    monitor-enter p0

    :try_start_0
    const-string v0, "UuId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 177
    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/ob/ga;->b(Ljava/lang/String;)V

    .line 179
    const-string v0, "DeviceId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 180
    invoke-static {v0}, Lcom/yandex/metrica/impl/bj;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 181
    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/ga;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    :cond_0
    monitor-exit p0

    return-void

    .line 176
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/yandex/metrica/impl/bj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/yandex/metrica/impl/bj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 60
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/ga;->a:Ljava/lang/String;

    .line 62
    :cond_0
    return-void
.end method

.method private declared-synchronized c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 186
    monitor-enter p0

    :try_start_0
    const-string v0, "AdUrlGet"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 187
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 188
    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/ob/ga;->c(Ljava/lang/String;)V

    .line 190
    :cond_0
    const-string v0, "AdUrlReport"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 191
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 192
    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/ob/ga;->d(Ljava/lang/String;)V

    .line 194
    :cond_1
    const-string v0, "BindIdUrl"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 195
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 196
    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/ob/ga;->e(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 198
    :cond_2
    monitor-exit p0

    return-void

    .line 186
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 205
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/ga;->c:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 206
    monitor-exit p0

    return-void

    .line 205
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 209
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/ga;->d:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    monitor-exit p0

    return-void

    .line 209
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private e()V
    .locals 4

    .prologue
    .line 112
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->j:Lcom/yandex/metrica/impl/ob/dc;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ga;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dc;->f(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dc;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ga;->b:Ljava/lang/String;

    .line 113
    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dc;->g(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dc;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ga;->c:Ljava/lang/String;

    .line 114
    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dc;->h(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dc;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ga;->d:Ljava/lang/String;

    .line 115
    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dc;->i(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dc;

    move-result-object v0

    iget-wide v2, p0, Lcom/yandex/metrica/impl/ob/ga;->g:J

    .line 116
    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/dc;->d(J)Lcom/yandex/metrica/impl/ob/dc;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ga;->h:Ljava/lang/String;

    .line 117
    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dc;->j(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dc;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ga;->i:Ljava/lang/String;

    .line 118
    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dc;->m(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dc;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ga;->e:Ljava/lang/String;

    .line 119
    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dc;->o(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dc;

    move-result-object v0

    .line 120
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dc;->g()V

    .line 121
    return-void
.end method

.method private declared-synchronized e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 213
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/ga;->e:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 214
    monitor-exit p0

    return-void

    .line 213
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized f()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 162
    monitor-enter p0

    const/4 v2, 0x2

    :try_start_0
    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/yandex/metrica/impl/ob/ga;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/yandex/metrica/impl/ob/ga;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/yandex/metrica/impl/bj;->a([Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized g()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 168
    monitor-enter p0

    const/4 v2, 0x1

    :try_start_0
    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/yandex/metrica/impl/ob/ga;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/yandex/metrica/impl/bj;->a([Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized h()Z
    .locals 1

    .prologue
    .line 172
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ga;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ga;->g()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method a(J)V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->j:Lcom/yandex/metrica/impl/ob/dc;

    invoke-virtual {v0, p1, p2}, Lcom/yandex/metrica/impl/ob/dc;->e(J)Lcom/yandex/metrica/impl/ob/dc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dc;->g()V

    .line 139
    return-void
.end method

.method declared-synchronized a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 102
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/ga;->b(Landroid/os/Bundle;)V

    .line 103
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/ga;->c(Landroid/os/Bundle;)V

    .line 1156
    const-string v0, "ServerTimeOffset"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/ga;->b(J)V

    .line 2124
    const-string v0, "Clids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2125
    invoke-static {v0}, Lcom/yandex/metrica/impl/bj;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2126
    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->h:Ljava/lang/String;

    .line 2131
    :cond_0
    const-string v0, "CookieBrowsers"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2132
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2133
    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->i:Ljava/lang/String;

    .line 108
    :cond_1
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ga;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    monitor-exit p0

    return-void

    .line 102
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 201
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/ga;->b:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    monitor-exit p0

    return-void

    .line 201
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 151
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/ga;->f:Ljava/util/List;

    .line 152
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->j:Lcom/yandex/metrica/impl/ob/dc;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ga;->f:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dc;->a(Ljava/util/List;)Lcom/yandex/metrica/impl/ob/dc;

    .line 153
    return-void
.end method

.method declared-synchronized a(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 65
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/ob/ga;->b(Ljava/util/Map;)V

    .line 66
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/ob/ga;->c(Ljava/util/Map;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    monitor-exit p0

    return-void

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method a()Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 3029
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 142
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ga;->j:Lcom/yandex/metrica/impl/ob/dc;

    invoke-virtual {v2, v4, v5}, Lcom/yandex/metrica/impl/ob/dc;->b(J)J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 143
    const-wide/32 v2, 0x15180

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    cmp-long v0, v0, v4

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method declared-synchronized a(Lcom/yandex/metrica/impl/ob/ga$a;)Z
    .locals 1

    .prologue
    .line 70
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/yandex/metrica/impl/ob/ga$a;->c:Lcom/yandex/metrica/impl/ob/ga$a;

    if-ne v0, p1, :cond_0

    .line 71
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ga;->h()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 78
    :goto_0
    monitor-exit p0

    return v0

    .line 72
    :cond_0
    :try_start_1
    sget-object v0, Lcom/yandex/metrica/impl/ob/ga$a;->a:Lcom/yandex/metrica/impl/ob/ga$a;

    if-ne v0, p1, :cond_1

    .line 73
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ga;->f()Z

    move-result v0

    goto :goto_0

    .line 74
    :cond_1
    sget-object v0, Lcom/yandex/metrica/impl/ob/ga$a;->b:Lcom/yandex/metrica/impl/ob/ga$a;

    if-ne v0, p1, :cond_2

    .line 75
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ga;->g()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 78
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->f:Ljava/util/List;

    return-object v0
.end method

.method declared-synchronized b(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 82
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/yandex/metrica/impl/bj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    const-string v0, "yandex_mobile_metrica_uuid"

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ga;->a:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/yandex/metrica/impl/bj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 87
    const-string v0, "yandex_mobile_metrica_device_id"

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ga;->b:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    :cond_1
    monitor-exit p0

    return-void

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->a:Ljava/lang/String;

    return-object v0
.end method

.method declared-synchronized c(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 92
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/yandex/metrica/impl/bj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    const-string v0, "yandex_mobile_metrica_get_ad_url"

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ga;->c:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/yandex/metrica/impl/bj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 97
    const-string v0, "yandex_mobile_metrica_report_ad_url"

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ga;->d:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    :cond_1
    monitor-exit p0

    return-void

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ga;->b:Ljava/lang/String;

    return-object v0
.end method
