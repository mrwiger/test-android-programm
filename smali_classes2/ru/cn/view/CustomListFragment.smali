.class public abstract Lru/cn/view/CustomListFragment;
.super Landroid/support/v4/app/ListFragment;
.source "CustomListFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/view/CustomListFragment$DelayFixPosition;
    }
.end annotation


# instance fields
.field private delayFixPosition:Lru/cn/view/CustomListFragment$DelayFixPosition;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    .line 33
    new-instance v0, Lru/cn/view/CustomListFragment$DelayFixPosition;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lru/cn/view/CustomListFragment$DelayFixPosition;-><init>(Lru/cn/view/CustomListFragment;Lru/cn/view/CustomListFragment$1;)V

    iput-object v0, p0, Lru/cn/view/CustomListFragment;->delayFixPosition:Lru/cn/view/CustomListFragment$DelayFixPosition;

    return-void
.end method

.method static synthetic access$000(Lru/cn/view/CustomListFragment;)Lru/cn/view/CustomListFragment$DelayFixPosition;
    .locals 1
    .param p0, "x0"    # Lru/cn/view/CustomListFragment;

    .prologue
    .line 11
    iget-object v0, p0, Lru/cn/view/CustomListFragment;->delayFixPosition:Lru/cn/view/CustomListFragment$DelayFixPosition;

    return-object v0
.end method


# virtual methods
.method protected abstract getItemHeight()I
.end method

.method public selectNext()Z
    .locals 2

    .prologue
    .line 50
    invoke-virtual {p0}, Lru/cn/view/CustomListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v0

    .line 51
    .local v0, "position":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 52
    const/4 v0, -0x1

    .line 55
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 56
    :goto_0
    invoke-virtual {p0}, Lru/cn/view/CustomListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-gt v0, v1, :cond_2

    .line 57
    invoke-virtual {p0}, Lru/cn/view/CustomListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 58
    invoke-virtual {p0, v0}, Lru/cn/view/CustomListFragment;->selectPosition(I)V

    .line 59
    const/4 v1, 0x1

    .line 63
    :goto_1
    return v1

    .line 61
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 63
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public selectPosition(I)V
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 36
    invoke-virtual {p0}, Lru/cn/view/CustomListFragment;->getView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 37
    .local v0, "h1":I
    if-lez v0, :cond_0

    .line 38
    invoke-virtual {p0}, Lru/cn/view/CustomListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {p0}, Lru/cn/view/CustomListFragment;->getItemHeight()I

    move-result v4

    invoke-static {v3, v4}, Lru/cn/utils/Utils;->dpToPx(Landroid/content/Context;I)I

    move-result v1

    .line 39
    .local v1, "h2":I
    div-int/lit8 v3, v0, 0x2

    div-int/lit8 v4, v1, 0x2

    sub-int v2, v3, v4

    .line 40
    .local v2, "offset":I
    invoke-virtual {p0}, Lru/cn/view/CustomListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, p1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 47
    .end local v1    # "h2":I
    .end local v2    # "offset":I
    :goto_0
    return-void

    .line 43
    :cond_0
    iget-object v3, p0, Lru/cn/view/CustomListFragment;->delayFixPosition:Lru/cn/view/CustomListFragment$DelayFixPosition;

    iput p1, v3, Lru/cn/view/CustomListFragment$DelayFixPosition;->position:I

    .line 45
    invoke-virtual {p0}, Lru/cn/view/CustomListFragment;->getView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    iget-object v4, p0, Lru/cn/view/CustomListFragment;->delayFixPosition:Lru/cn/view/CustomListFragment$DelayFixPosition;

    invoke-virtual {v3, v4}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method

.method public selectPrev()Z
    .locals 2

    .prologue
    .line 67
    invoke-virtual {p0}, Lru/cn/view/CustomListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v0

    .line 68
    .local v0, "position":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 69
    invoke-virtual {p0}, Lru/cn/view/CustomListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getCount()I

    move-result v0

    .line 72
    :cond_0
    add-int/lit8 v0, v0, -0x1

    .line 73
    :goto_0
    if-ltz v0, :cond_2

    .line 74
    invoke-virtual {p0}, Lru/cn/view/CustomListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 75
    invoke-virtual {p0, v0}, Lru/cn/view/CustomListFragment;->selectPosition(I)V

    .line 76
    const/4 v1, 0x1

    .line 80
    :goto_1
    return v1

    .line 78
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 80
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method
