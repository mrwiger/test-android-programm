.class public final Lcom/google/obf/by;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/aj;


# static fields
.field private static final a:I


# instance fields
.field private final b:J

.field private final c:Lcom/google/obf/dw;

.field private d:Lcom/google/obf/bz;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-string v0, "ID3"

    invoke-static {v0}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/obf/by;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/obf/by;-><init>(J)V

    .line 2
    return-void
.end method

.method public constructor <init>(J)V
    .locals 3

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-wide p1, p0, Lcom/google/obf/by;->b:J

    .line 5
    new-instance v0, Lcom/google/obf/dw;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/by;->c:Lcom/google/obf/dw;

    .line 6
    return-void
.end method


# virtual methods
.method public a(Lcom/google/obf/ak;Lcom/google/obf/ao;)I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 51
    iget-object v2, p0, Lcom/google/obf/by;->c:Lcom/google/obf/dw;

    iget-object v2, v2, Lcom/google/obf/dw;->a:[B

    const/16 v3, 0xc8

    invoke-interface {p1, v2, v1, v3}, Lcom/google/obf/ak;->a([BII)I

    move-result v2

    .line 52
    if-ne v2, v0, :cond_0

    .line 60
    :goto_0
    return v0

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/google/obf/by;->c:Lcom/google/obf/dw;

    invoke-virtual {v0, v1}, Lcom/google/obf/dw;->c(I)V

    .line 55
    iget-object v0, p0, Lcom/google/obf/by;->c:Lcom/google/obf/dw;

    invoke-virtual {v0, v2}, Lcom/google/obf/dw;->b(I)V

    .line 56
    iget-boolean v0, p0, Lcom/google/obf/by;->e:Z

    if-nez v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/google/obf/by;->d:Lcom/google/obf/bz;

    iget-wide v2, p0, Lcom/google/obf/by;->b:J

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/obf/bz;->a(JZ)V

    .line 58
    iput-boolean v4, p0, Lcom/google/obf/by;->e:Z

    .line 59
    :cond_1
    iget-object v0, p0, Lcom/google/obf/by;->d:Lcom/google/obf/bz;

    iget-object v2, p0, Lcom/google/obf/by;->c:Lcom/google/obf/dw;

    invoke-virtual {v0, v2}, Lcom/google/obf/bz;->a(Lcom/google/obf/dw;)V

    move v0, v1

    .line 60
    goto :goto_0
.end method

.method public a(Lcom/google/obf/al;)V
    .locals 3

    .prologue
    .line 43
    new-instance v0, Lcom/google/obf/bz;

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {p1, v2}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/obf/bz;-><init>(Lcom/google/obf/ar;Lcom/google/obf/ar;)V

    iput-object v0, p0, Lcom/google/obf/by;->d:Lcom/google/obf/bz;

    .line 44
    invoke-interface {p1}, Lcom/google/obf/al;->f()V

    .line 45
    sget-object v0, Lcom/google/obf/aq;->f:Lcom/google/obf/aq;

    invoke-interface {p1, v0}, Lcom/google/obf/al;->a(Lcom/google/obf/aq;)V

    .line 46
    return-void
.end method

.method public a(Lcom/google/obf/ak;)Z
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/16 v7, 0xa

    const/4 v10, 0x6

    const/4 v9, 0x4

    const/4 v1, 0x0

    .line 7
    new-instance v5, Lcom/google/obf/dw;

    invoke-direct {v5, v7}, Lcom/google/obf/dw;-><init>(I)V

    .line 8
    new-instance v6, Lcom/google/obf/dv;

    iget-object v0, v5, Lcom/google/obf/dw;->a:[B

    invoke-direct {v6, v0}, Lcom/google/obf/dv;-><init>([B)V

    move v0, v1

    .line 10
    :goto_0
    iget-object v2, v5, Lcom/google/obf/dw;->a:[B

    invoke-interface {p1, v2, v1, v7}, Lcom/google/obf/ak;->c([BII)V

    .line 11
    invoke-virtual {v5, v1}, Lcom/google/obf/dw;->c(I)V

    .line 12
    invoke-virtual {v5}, Lcom/google/obf/dw;->j()I

    move-result v2

    sget v3, Lcom/google/obf/by;->a:I

    if-eq v2, v3, :cond_1

    .line 18
    invoke-interface {p1}, Lcom/google/obf/ak;->a()V

    .line 19
    invoke-interface {p1, v0}, Lcom/google/obf/ak;->c(I)V

    move v2, v1

    move v3, v1

    move v4, v0

    .line 23
    :goto_1
    iget-object v7, v5, Lcom/google/obf/dw;->a:[B

    const/4 v8, 0x2

    invoke-interface {p1, v7, v1, v8}, Lcom/google/obf/ak;->c([BII)V

    .line 24
    invoke-virtual {v5, v1}, Lcom/google/obf/dw;->c(I)V

    .line 25
    invoke-virtual {v5}, Lcom/google/obf/dw;->g()I

    move-result v7

    .line 26
    const v8, 0xfff6

    and-int/2addr v7, v8

    const v8, 0xfff0

    if-eq v7, v8, :cond_3

    .line 29
    invoke-interface {p1}, Lcom/google/obf/ak;->a()V

    .line 30
    add-int/lit8 v4, v4, 0x1

    sub-int v2, v4, v0

    const/16 v3, 0x2000

    if-lt v2, v3, :cond_2

    .line 39
    :cond_0
    :goto_2
    return v1

    .line 14
    :cond_1
    iget-object v2, v5, Lcom/google/obf/dw;->a:[B

    aget-byte v2, v2, v10

    and-int/lit8 v2, v2, 0x7f

    shl-int/lit8 v2, v2, 0x15

    iget-object v3, v5, Lcom/google/obf/dw;->a:[B

    const/4 v4, 0x7

    aget-byte v3, v3, v4

    and-int/lit8 v3, v3, 0x7f

    shl-int/lit8 v3, v3, 0xe

    or-int/2addr v2, v3

    iget-object v3, v5, Lcom/google/obf/dw;->a:[B

    const/16 v4, 0x8

    aget-byte v3, v3, v4

    and-int/lit8 v3, v3, 0x7f

    shl-int/lit8 v3, v3, 0x7

    or-int/2addr v2, v3

    iget-object v3, v5, Lcom/google/obf/dw;->a:[B

    const/16 v4, 0x9

    aget-byte v3, v3, v4

    and-int/lit8 v3, v3, 0x7f

    or-int/2addr v2, v3

    .line 15
    add-int/lit8 v3, v2, 0xa

    add-int/2addr v0, v3

    .line 16
    invoke-interface {p1, v2}, Lcom/google/obf/ak;->c(I)V

    goto :goto_0

    .line 32
    :cond_2
    invoke-interface {p1, v4}, Lcom/google/obf/ak;->c(I)V

    move v2, v1

    move v3, v1

    goto :goto_1

    .line 33
    :cond_3
    add-int/lit8 v2, v2, 0x1

    if-lt v2, v9, :cond_4

    const/16 v7, 0xbc

    if-le v3, v7, :cond_4

    .line 34
    const/4 v1, 0x1

    goto :goto_2

    .line 35
    :cond_4
    iget-object v7, v5, Lcom/google/obf/dw;->a:[B

    invoke-interface {p1, v7, v1, v9}, Lcom/google/obf/ak;->c([BII)V

    .line 36
    const/16 v7, 0xe

    invoke-virtual {v6, v7}, Lcom/google/obf/dv;->a(I)V

    .line 37
    const/16 v7, 0xd

    invoke-virtual {v6, v7}, Lcom/google/obf/dv;->c(I)I

    move-result v7

    .line 38
    if-le v7, v10, :cond_0

    .line 40
    add-int/lit8 v8, v7, -0x6

    invoke-interface {p1, v8}, Lcom/google/obf/ak;->c(I)V

    .line 41
    add-int/2addr v3, v7

    goto :goto_1
.end method

.method public b()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/by;->e:Z

    .line 48
    iget-object v0, p0, Lcom/google/obf/by;->d:Lcom/google/obf/bz;

    invoke-virtual {v0}, Lcom/google/obf/bz;->a()V

    .line 49
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 50
    return-void
.end method
