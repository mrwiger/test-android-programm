.class public abstract Lcom/my/target/d;
.super Ljava/lang/Object;
.source "AdResponseParser.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/my/target/ak;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final z:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method private a(Lorg/json/JSONObject;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 84
    :try_start_0
    const-string v1, "version"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 85
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "json version: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 86
    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 87
    if-lez v2, :cond_0

    .line 89
    const/4 v3, 0x0

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 92
    const/16 v2, 0xa

    :try_start_1
    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    .line 93
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 95
    const/4 v0, 0x1

    .line 107
    :cond_0
    :goto_0
    return v0

    .line 98
    :catch_0
    move-exception v1

    goto :goto_0

    .line 103
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method static isVast(Ljava/lang/String;)Z
    .locals 2
    .param p0, "data"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 28
    const-string v1, "<VAST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "<?xml"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(Ljava/lang/String;Lcom/my/target/ae;Lcom/my/target/ak;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/ak;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/my/target/ae;",
            "TT;",
            "Lcom/my/target/b;",
            "Landroid/content/Context;",
            ")TT;"
        }
    .end annotation
.end method

.method protected a(Ljava/lang/String;Landroid/content/Context;)Lorg/json/JSONObject;
    .locals 2

    .prologue
    .line 44
    if-eqz p1, :cond_0

    .line 46
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 48
    :cond_0
    if-eqz p1, :cond_2

    const-string v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 50
    const-string v0, "Converting to JSON..."

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 53
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 54
    const-string v1, "done"

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 55
    invoke-direct {p0, v0}, Lcom/my/target/d;->a(Lorg/json/JSONObject;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 77
    :goto_0
    return-object v0

    .line 61
    :cond_1
    const-string v0, "invalid json version"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 75
    :cond_2
    const-string v0, "parsing ad response: empty data"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 65
    :catch_0
    move-exception v0

    goto :goto_1
.end method
