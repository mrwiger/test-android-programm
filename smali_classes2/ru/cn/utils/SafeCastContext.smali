.class public Lru/cn/utils/SafeCastContext;
.super Ljava/lang/Object;
.source "SafeCastContext.java"


# direct methods
.method public static getSharedContext(Landroid/content/Context;)Lcom/google/android/gms/cast/framework/CastContext;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/cast/framework/CastContext;->getSharedInstance(Landroid/content/Context;)Lcom/google/android/gms/cast/framework/CastContext;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 26
    :goto_0
    return-object v1

    .line 22
    :catch_0
    move-exception v0

    .line 23
    .local v0, "error":Ljava/lang/Throwable;
    const-string v1, "SafeCastContext"

    const-string v2, "Fail to get shared context"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 26
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static onDispatchVolumeKeyEventBeforeJellyBean(Landroid/content/Context;Landroid/view/KeyEvent;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 40
    invoke-static {p0}, Lru/cn/utils/SafeCastContext;->getSharedContext(Landroid/content/Context;)Lcom/google/android/gms/cast/framework/CastContext;

    move-result-object v0

    .line 41
    .local v0, "castContext":Lcom/google/android/gms/cast/framework/CastContext;
    if-eqz v0, :cond_0

    .line 42
    invoke-virtual {v0, p1}, Lcom/google/android/gms/cast/framework/CastContext;->onDispatchVolumeKeyEventBeforeJellyBean(Landroid/view/KeyEvent;)Z

    move-result v1

    .line 45
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static setUpMediaRouteButton(Landroid/content/Context;Landroid/view/Menu;I)Landroid/view/MenuItem;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "itemId"    # I

    .prologue
    .line 31
    :try_start_0
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/cast/framework/CastButtonFactory;->setUpMediaRouteButton(Landroid/content/Context;Landroid/view/Menu;I)Landroid/view/MenuItem;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    :goto_0
    const/4 v1, 0x0

    return-object v1

    .line 32
    :catch_0
    move-exception v0

    .line 33
    .local v0, "error":Ljava/lang/Throwable;
    const-string v1, "SafeCastContext"

    const-string v2, "Fail to set up menu item for cast"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
