.class final Lcom/google/obf/fz$b;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/ex;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/fz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation


# instance fields
.field private final a:Lcom/google/obf/gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/gd",
            "<*>;"
        }
    .end annotation
.end field

.field private final b:Z

.field private final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/obf/et;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/et",
            "<*>;"
        }
    .end annotation
.end field

.field private final e:Lcom/google/obf/el;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/el",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Object;Lcom/google/obf/gd;ZLjava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lcom/google/obf/gd",
            "<*>;Z",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    instance-of v0, p1, Lcom/google/obf/et;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/google/obf/et;

    :goto_0
    iput-object v0, p0, Lcom/google/obf/fz$b;->d:Lcom/google/obf/et;

    .line 3
    instance-of v0, p1, Lcom/google/obf/el;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/google/obf/el;

    :goto_1
    iput-object p1, p0, Lcom/google/obf/fz$b;->e:Lcom/google/obf/el;

    .line 4
    iget-object v0, p0, Lcom/google/obf/fz$b;->d:Lcom/google/obf/et;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/fz$b;->e:Lcom/google/obf/el;

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Lcom/google/obf/fd;->a(Z)V

    .line 5
    iput-object p2, p0, Lcom/google/obf/fz$b;->a:Lcom/google/obf/gd;

    .line 6
    iput-boolean p3, p0, Lcom/google/obf/fz$b;->b:Z

    .line 7
    iput-object p4, p0, Lcom/google/obf/fz$b;->c:Ljava/lang/Class;

    .line 8
    return-void

    :cond_1
    move-object v0, v1

    .line 2
    goto :goto_0

    :cond_2
    move-object p1, v1

    .line 3
    goto :goto_1

    .line 4
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public a(Lcom/google/obf/eg;Lcom/google/obf/gd;)Lcom/google/obf/ew;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/obf/eg;",
            "Lcom/google/obf/gd",
            "<TT;>;)",
            "Lcom/google/obf/ew",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 9
    iget-object v0, p0, Lcom/google/obf/fz$b;->a:Lcom/google/obf/gd;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/obf/fz$b;->a:Lcom/google/obf/gd;

    .line 10
    invoke-virtual {v0, p2}, Lcom/google/obf/gd;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/obf/fz$b;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/obf/fz$b;->a:Lcom/google/obf/gd;

    invoke-virtual {v0}, Lcom/google/obf/gd;->b()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/obf/gd;->a()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 12
    :goto_0
    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/obf/fz;

    iget-object v1, p0, Lcom/google/obf/fz$b;->d:Lcom/google/obf/et;

    iget-object v2, p0, Lcom/google/obf/fz$b;->e:Lcom/google/obf/el;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/obf/fz;-><init>(Lcom/google/obf/et;Lcom/google/obf/el;Lcom/google/obf/eg;Lcom/google/obf/gd;Lcom/google/obf/ex;)V

    :goto_1
    return-object v0

    .line 10
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/obf/fz$b;->c:Ljava/lang/Class;

    .line 11
    invoke-virtual {p2}, Lcom/google/obf/gd;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    goto :goto_0

    .line 12
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
