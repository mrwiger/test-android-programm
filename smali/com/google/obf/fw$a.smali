.class public final Lcom/google/obf/fw$a;
.super Lcom/google/obf/ew;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/fw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/obf/ew",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/obf/fk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/fk",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/obf/fw$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/obf/fk;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/fk",
            "<TT;>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/obf/fw$b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/obf/ew;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/obf/fw$a;->a:Lcom/google/obf/fk;

    .line 3
    iput-object p2, p0, Lcom/google/obf/fw$a;->b:Ljava/util/Map;

    .line 4
    return-void
.end method


# virtual methods
.method public read(Lcom/google/obf/ge;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/ge;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p1}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    move-result-object v0

    sget-object v1, Lcom/google/obf/gf;->i:Lcom/google/obf/gf;

    if-ne v0, v1, :cond_0

    .line 6
    invoke-virtual {p1}, Lcom/google/obf/ge;->j()V

    .line 7
    const/4 v0, 0x0

    .line 23
    :goto_0
    return-object v0

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/google/obf/fw$a;->a:Lcom/google/obf/fk;

    invoke-interface {v0}, Lcom/google/obf/fk;->a()Ljava/lang/Object;

    move-result-object v1

    .line 9
    :try_start_0
    invoke-virtual {p1}, Lcom/google/obf/ge;->c()V

    .line 10
    :goto_1
    invoke-virtual {p1}, Lcom/google/obf/ge;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 11
    invoke-virtual {p1}, Lcom/google/obf/ge;->g()Ljava/lang/String;

    move-result-object v0

    .line 12
    iget-object v2, p0, Lcom/google/obf/fw$a;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/fw$b;

    .line 13
    if-eqz v0, :cond_1

    iget-boolean v2, v0, Lcom/google/obf/fw$b;->j:Z

    if-nez v2, :cond_2

    .line 14
    :cond_1
    invoke-virtual {p1}, Lcom/google/obf/ge;->n()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 18
    :catch_0
    move-exception v0

    .line 19
    new-instance v1, Lcom/google/obf/eu;

    invoke-direct {v1, v0}, Lcom/google/obf/eu;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 15
    :cond_2
    :try_start_1
    invoke-virtual {v0, p1, v1}, Lcom/google/obf/fw$b;->a(Lcom/google/obf/ge;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 20
    :catch_1
    move-exception v0

    .line 21
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 22
    :cond_3
    invoke-virtual {p1}, Lcom/google/obf/ge;->d()V

    move-object v0, v1

    .line 23
    goto :goto_0
.end method

.method public write(Lcom/google/obf/gg;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/gg;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    if-nez p2, :cond_0

    .line 25
    invoke-virtual {p1}, Lcom/google/obf/gg;->f()Lcom/google/obf/gg;

    .line 37
    :goto_0
    return-void

    .line 27
    :cond_0
    invoke-virtual {p1}, Lcom/google/obf/gg;->d()Lcom/google/obf/gg;

    .line 28
    :try_start_0
    iget-object v0, p0, Lcom/google/obf/fw$a;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/fw$b;

    .line 29
    invoke-virtual {v0, p2}, Lcom/google/obf/fw$b;->a(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 30
    iget-object v2, v0, Lcom/google/obf/fw$b;->h:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/google/obf/gg;->a(Ljava/lang/String;)Lcom/google/obf/gg;

    .line 31
    invoke-virtual {v0, p1, p2}, Lcom/google/obf/fw$b;->a(Lcom/google/obf/gg;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 34
    :catch_0
    move-exception v0

    .line 35
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 36
    :cond_2
    invoke-virtual {p1}, Lcom/google/obf/gg;->e()Lcom/google/obf/gg;

    goto :goto_0
.end method
