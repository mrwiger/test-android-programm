.class final Lcom/google/ads/interactivemedia/v3/impl/data/h$a;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/ads/interactivemedia/v3/impl/data/k$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ads/interactivemedia/v3/impl/data/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field private adContainerBounds:Lcom/google/ads/interactivemedia/v3/impl/data/a$a;

.field private adTagParameters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private adTagUrl:Ljava/lang/String;

.field private adsResponse:Ljava/lang/String;

.field private apiKey:Ljava/lang/String;

.field private assetKey:Ljava/lang/String;

.field private authToken:Ljava/lang/String;

.field private companionSlots:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private contentDuration:Ljava/lang/Float;

.field private contentKeywords:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private contentSourceId:Ljava/lang/String;

.field private contentTitle:Ljava/lang/String;

.field private env:Ljava/lang/String;

.field private extraParameters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private idType:Ljava/lang/String;

.field private isAdContainerAttachedToWindow:Ljava/lang/Boolean;

.field private isLat:Ljava/lang/String;

.field private isTv:Ljava/lang/Boolean;

.field private marketAppInfo:Lcom/google/obf/gp$b;

.field private msParameter:Ljava/lang/String;

.field private network:Ljava/lang/String;

.field private rdid:Ljava/lang/String;

.field private settings:Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;

.field private streamActivityMonitorId:Ljava/lang/String;

.field private vastLoadTimeout:Ljava/lang/Float;

.field private videoId:Ljava/lang/String;

.field private videoPlayActivation:Lcom/google/obf/gu$a;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    return-void
.end method


# virtual methods
.method public adContainerBounds(Lcom/google/ads/interactivemedia/v3/impl/data/a$a;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->adContainerBounds:Lcom/google/ads/interactivemedia/v3/impl/data/a$a;

    .line 48
    return-object p0
.end method

.method public adTagUrl(Ljava/lang/String;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;
    .locals 0

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->adTagUrl:Ljava/lang/String;

    .line 6
    return-object p0
.end method

.method public adsResponse(Ljava/lang/String;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;
    .locals 0

    .prologue
    .line 3
    iput-object p1, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->adsResponse:Ljava/lang/String;

    .line 4
    return-object p0
.end method

.method public build()Lcom/google/ads/interactivemedia/v3/impl/data/k;
    .locals 30

    .prologue
    .line 57
    new-instance v1, Lcom/google/ads/interactivemedia/v3/impl/data/h;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->adsResponse:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->adTagUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->assetKey:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->authToken:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->contentSourceId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->videoId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->apiKey:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->adTagParameters:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->env:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->network:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->videoPlayActivation:Lcom/google/obf/gu$a;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->contentDuration:Ljava/lang/Float;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->contentKeywords:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->contentTitle:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->vastLoadTimeout:Ljava/lang/Float;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->companionSlots:Ljava/util/Map;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->extraParameters:Ljava/util/Map;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->settings:Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->marketAppInfo:Lcom/google/obf/gp$b;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->isTv:Ljava/lang/Boolean;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->msParameter:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->isAdContainerAttachedToWindow:Ljava/lang/Boolean;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->adContainerBounds:Lcom/google/ads/interactivemedia/v3/impl/data/a$a;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->streamActivityMonitorId:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->rdid:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->idType:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->isLat:Ljava/lang/String;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    invoke-direct/range {v1 .. v29}, Lcom/google/ads/interactivemedia/v3/impl/data/h;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/google/obf/gu$a;Ljava/lang/Float;Ljava/util/List;Ljava/lang/String;Ljava/lang/Float;Ljava/util/Map;Ljava/util/Map;Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;Lcom/google/obf/gp$b;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Lcom/google/ads/interactivemedia/v3/impl/data/a$a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/impl/data/h$1;)V

    return-object v1
.end method

.method public companionSlots(Ljava/util/Map;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/ads/interactivemedia/v3/impl/data/k$a;"
        }
    .end annotation

    .prologue
    .line 33
    iput-object p1, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->companionSlots:Ljava/util/Map;

    .line 34
    return-object p0
.end method

.method public contentDuration(Ljava/lang/Float;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->contentDuration:Ljava/lang/Float;

    .line 26
    return-object p0
.end method

.method public contentKeywords(Ljava/util/List;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/ads/interactivemedia/v3/impl/data/k$a;"
        }
    .end annotation

    .prologue
    .line 27
    iput-object p1, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->contentKeywords:Ljava/util/List;

    .line 28
    return-object p0
.end method

.method public contentTitle(Ljava/lang/String;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->contentTitle:Ljava/lang/String;

    .line 30
    return-object p0
.end method

.method public env(Ljava/lang/String;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;
    .locals 0

    .prologue
    .line 19
    iput-object p1, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->env:Ljava/lang/String;

    .line 20
    return-object p0
.end method

.method public extraParameters(Ljava/util/Map;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/ads/interactivemedia/v3/impl/data/k$a;"
        }
    .end annotation

    .prologue
    .line 35
    iput-object p1, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->extraParameters:Ljava/util/Map;

    .line 36
    return-object p0
.end method

.method public isAdContainerAttachedToWindow(Ljava/lang/Boolean;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->isAdContainerAttachedToWindow:Ljava/lang/Boolean;

    .line 46
    return-object p0
.end method

.method public isTv(Ljava/lang/Boolean;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->isTv:Ljava/lang/Boolean;

    .line 42
    return-object p0
.end method

.method public marketAppInfo(Lcom/google/obf/gp$b;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->marketAppInfo:Lcom/google/obf/gp$b;

    .line 40
    return-object p0
.end method

.method public network(Ljava/lang/String;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->network:Ljava/lang/String;

    .line 22
    return-object p0
.end method

.method public settings(Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->settings:Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;

    .line 38
    return-object p0
.end method

.method public vastLoadTimeout(Ljava/lang/Float;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->vastLoadTimeout:Ljava/lang/Float;

    .line 32
    return-object p0
.end method

.method public videoPlayActivation(Lcom/google/obf/gu$a;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;
    .locals 0

    .prologue
    .line 23
    iput-object p1, p0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;->videoPlayActivation:Lcom/google/obf/gu$a;

    .line 24
    return-object p0
.end method
