.class public Lru/cn/domain/tv/CurrentCategory;
.super Ljava/lang/Object;
.source "CurrentCategory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/domain/tv/CurrentCategory$Type;
    }
.end annotation


# instance fields
.field private final category:Lio/reactivex/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/BehaviorSubject",
            "<",
            "Lru/cn/domain/tv/CurrentCategory$Type;",
            ">;"
        }
    .end annotation
.end field

.field private final categoryIn:Lio/reactivex/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/PublishSubject",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lru/cn/domain/tv/CurrentCategory$Type;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    sget-object v0, Lru/cn/domain/tv/CurrentCategory$Type;->all:Lru/cn/domain/tv/CurrentCategory$Type;

    invoke-static {v0}, Lio/reactivex/subjects/BehaviorSubject;->createDefault(Ljava/lang/Object;)Lio/reactivex/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lru/cn/domain/tv/CurrentCategory;->category:Lio/reactivex/subjects/BehaviorSubject;

    .line 35
    invoke-static {}, Lio/reactivex/subjects/PublishSubject;->create()Lio/reactivex/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lru/cn/domain/tv/CurrentCategory;->categoryIn:Lio/reactivex/subjects/PublishSubject;

    .line 37
    iget-object v0, p0, Lru/cn/domain/tv/CurrentCategory;->categoryIn:Lio/reactivex/subjects/PublishSubject;

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$$Lambda$0;->$instance:Lio/reactivex/functions/Function;

    .line 38
    invoke-virtual {v0, v1}, Lio/reactivex/subjects/PublishSubject;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lru/cn/domain/tv/CurrentCategory;->category:Lio/reactivex/subjects/BehaviorSubject;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lru/cn/domain/tv/CurrentCategory$$Lambda$1;->get$Lambda(Lio/reactivex/subjects/BehaviorSubject;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 42
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 43
    return-void
.end method

.method static final synthetic lambda$new$1$CurrentCategory(Landroid/util/Pair;)Lio/reactivex/ObservableSource;
    .locals 3
    .param p0, "action"    # Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    .line 39
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->timer(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lru/cn/domain/tv/CurrentCategory$$Lambda$2;

    invoke-direct {v1, p0}, Lru/cn/domain/tv/CurrentCategory$$Lambda$2;-><init>(Landroid/util/Pair;)V

    .line 40
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 38
    return-object v0
.end method

.method static final synthetic lambda$null$0$CurrentCategory(Landroid/util/Pair;Ljava/lang/Long;)Lru/cn/domain/tv/CurrentCategory$Type;
    .locals 1
    .param p0, "action"    # Landroid/util/Pair;
    .param p1, "it"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lru/cn/domain/tv/CurrentCategory$Type;

    return-object v0
.end method


# virtual methods
.method public category()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lru/cn/domain/tv/CurrentCategory$Type;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lru/cn/domain/tv/CurrentCategory;->category:Lio/reactivex/subjects/BehaviorSubject;

    return-object v0
.end method

.method public selectCategory(Lru/cn/domain/tv/CurrentCategory$Type;)V
    .locals 4
    .param p1, "type"    # Lru/cn/domain/tv/CurrentCategory$Type;

    .prologue
    .line 50
    iget-object v0, p0, Lru/cn/domain/tv/CurrentCategory;->categoryIn:Lio/reactivex/subjects/PublishSubject;

    const-wide/16 v2, 0x190

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 51
    return-void
.end method

.method public setCategory(Lru/cn/domain/tv/CurrentCategory$Type;)V
    .locals 4
    .param p1, "type"    # Lru/cn/domain/tv/CurrentCategory$Type;

    .prologue
    .line 54
    iget-object v0, p0, Lru/cn/domain/tv/CurrentCategory;->categoryIn:Lio/reactivex/subjects/PublishSubject;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 55
    return-void
.end method
