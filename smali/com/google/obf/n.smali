.class public abstract Lcom/google/obf/n;
.super Lcom/google/obf/v;
.source "IMASDK"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/n$a;,
        Lcom/google/obf/n$b;
    }
.end annotation


# static fields
.field private static final c:[B


# instance fields
.field private A:[Ljava/nio/ByteBuffer;

.field private B:J

.field private C:I

.field private D:I

.field private E:Z

.field private F:Z

.field private G:I

.field private H:I

.field private I:Z

.field private J:Z

.field private K:I

.field private L:Z

.field private M:Z

.field private N:Z

.field private O:Z

.field public final a:Lcom/google/obf/c;

.field protected final b:Landroid/os/Handler;

.field private final d:Lcom/google/obf/m;

.field private final e:Lcom/google/obf/ac;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/ac",
            "<",
            "Lcom/google/obf/ae;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Z

.field private final g:Lcom/google/obf/t;

.field private final h:Lcom/google/obf/r;

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Landroid/media/MediaCodec$BufferInfo;

.field private final k:Lcom/google/obf/n$b;

.field private final l:Z

.field private m:Lcom/google/obf/q;

.field private n:Lcom/google/obf/ab;

.field private o:Landroid/media/MediaCodec;

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:[Ljava/nio/ByteBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 437
    const-string v0, "0000016742C00BDA259000000168CE0F13200000016588840DCE7118A0002FBF1C31C3275D78"

    invoke-static {v0}, Lcom/google/obf/ea;->d(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/obf/n;->c:[B

    return-void
.end method

.method public constructor <init>(Lcom/google/obf/u;Lcom/google/obf/m;Lcom/google/obf/ac;ZLandroid/os/Handler;Lcom/google/obf/n$b;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/u;",
            "Lcom/google/obf/m;",
            "Lcom/google/obf/ac",
            "<",
            "Lcom/google/obf/ae;",
            ">;Z",
            "Landroid/os/Handler;",
            "Lcom/google/obf/n$b;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1
    const/4 v0, 0x1

    new-array v1, v0, [Lcom/google/obf/u;

    const/4 v0, 0x0

    aput-object p1, v1, v0

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/obf/n;-><init>([Lcom/google/obf/u;Lcom/google/obf/m;Lcom/google/obf/ac;ZLandroid/os/Handler;Lcom/google/obf/n$b;)V

    .line 2
    return-void
.end method

.method public constructor <init>([Lcom/google/obf/u;Lcom/google/obf/m;Lcom/google/obf/ac;ZLandroid/os/Handler;Lcom/google/obf/n$b;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/obf/u;",
            "Lcom/google/obf/m;",
            "Lcom/google/obf/ac",
            "<",
            "Lcom/google/obf/ae;",
            ">;Z",
            "Landroid/os/Handler;",
            "Lcom/google/obf/n$b;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 3
    invoke-direct {p0, p1}, Lcom/google/obf/v;-><init>([Lcom/google/obf/u;)V

    .line 4
    sget v0, Lcom/google/obf/ea;->a:I

    const/16 v2, 0x10

    if-lt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 5
    invoke-static {p2}, Lcom/google/obf/dl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/m;

    iput-object v0, p0, Lcom/google/obf/n;->d:Lcom/google/obf/m;

    .line 6
    iput-object p3, p0, Lcom/google/obf/n;->e:Lcom/google/obf/ac;

    .line 7
    iput-boolean p4, p0, Lcom/google/obf/n;->f:Z

    .line 8
    iput-object p5, p0, Lcom/google/obf/n;->b:Landroid/os/Handler;

    .line 9
    iput-object p6, p0, Lcom/google/obf/n;->k:Lcom/google/obf/n$b;

    .line 10
    invoke-static {}, Lcom/google/obf/n;->B()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/obf/n;->l:Z

    .line 11
    new-instance v0, Lcom/google/obf/c;

    invoke-direct {v0}, Lcom/google/obf/c;-><init>()V

    iput-object v0, p0, Lcom/google/obf/n;->a:Lcom/google/obf/c;

    .line 12
    new-instance v0, Lcom/google/obf/t;

    invoke-direct {v0, v1}, Lcom/google/obf/t;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/n;->g:Lcom/google/obf/t;

    .line 13
    new-instance v0, Lcom/google/obf/r;

    invoke-direct {v0}, Lcom/google/obf/r;-><init>()V

    iput-object v0, p0, Lcom/google/obf/n;->h:Lcom/google/obf/r;

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/obf/n;->i:Ljava/util/List;

    .line 15
    new-instance v0, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v0}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    iput-object v0, p0, Lcom/google/obf/n;->j:Landroid/media/MediaCodec$BufferInfo;

    .line 16
    iput v1, p0, Lcom/google/obf/n;->G:I

    .line 17
    iput v1, p0, Lcom/google/obf/n;->H:I

    .line 18
    return-void

    :cond_0
    move v0, v1

    .line 4
    goto :goto_0
.end method

.method private A()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 386
    iget v0, p0, Lcom/google/obf/n;->H:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 387
    invoke-virtual {p0}, Lcom/google/obf/n;->m()V

    .line 388
    invoke-virtual {p0}, Lcom/google/obf/n;->j()V

    .line 391
    :goto_0
    return-void

    .line 389
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/n;->M:Z

    .line 390
    invoke-virtual {p0}, Lcom/google/obf/n;->h()V

    goto :goto_0
.end method

.method private static B()Z
    .locals 2

    .prologue
    .line 435
    sget v0, Lcom/google/obf/ea;->a:I

    const/16 v1, 0x16

    if-gt v0, v1, :cond_0

    const-string v0, "foster"

    sget-object v1, Lcom/google/obf/ea;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "NVIDIA"

    sget-object v1, Lcom/google/obf/ea;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/google/obf/t;I)Landroid/media/MediaCodec$CryptoInfo;
    .locals 4

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/obf/t;->a:Lcom/google/obf/e;

    invoke-virtual {v0}, Lcom/google/obf/e;->a()Landroid/media/MediaCodec$CryptoInfo;

    move-result-object v0

    .line 276
    if-nez p1, :cond_0

    .line 281
    :goto_0
    return-object v0

    .line 278
    :cond_0
    iget-object v1, v0, Landroid/media/MediaCodec$CryptoInfo;->numBytesOfClearData:[I

    if-nez v1, :cond_1

    .line 279
    const/4 v1, 0x1

    new-array v1, v1, [I

    iput-object v1, v0, Landroid/media/MediaCodec$CryptoInfo;->numBytesOfClearData:[I

    .line 280
    :cond_1
    iget-object v1, v0, Landroid/media/MediaCodec$CryptoInfo;->numBytesOfClearData:[I

    const/4 v2, 0x0

    aget v3, v1, v2

    add-int/2addr v3, p1

    aput v3, v1, v2

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/obf/n;)Lcom/google/obf/n$b;
    .locals 1

    .prologue
    .line 436
    iget-object v0, p0, Lcom/google/obf/n;->k:Lcom/google/obf/n$b;

    return-object v0
.end method

.method private a(Landroid/media/MediaCodec$CryptoException;)V
    .locals 2

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/obf/n;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/n;->k:Lcom/google/obf/n$b;

    if-eqz v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/google/obf/n;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/obf/n$2;

    invoke-direct {v1, p0, p1}, Lcom/google/obf/n$2;-><init>(Lcom/google/obf/n;Landroid/media/MediaCodec$CryptoException;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 397
    :cond_0
    return-void
.end method

.method private a(Lcom/google/obf/n$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/google/obf/n;->b(Lcom/google/obf/n$a;)V

    .line 80
    new-instance v0, Lcom/google/obf/g;

    invoke-direct {v0, p1}, Lcom/google/obf/g;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private a(Ljava/lang/String;JJ)V
    .locals 8

    .prologue
    .line 398
    iget-object v0, p0, Lcom/google/obf/n;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/n;->k:Lcom/google/obf/n$b;

    if-eqz v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/google/obf/n;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/obf/n$3;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/obf/n$3;-><init>(Lcom/google/obf/n;Ljava/lang/String;JJ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 400
    :cond_0
    return-void
.end method

.method private a()Z
    .locals 6

    .prologue
    .line 320
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/obf/n;->B:J

    const-wide/16 v4, 0x3e8

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(JJ)Z
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 322
    iget-boolean v0, p0, Lcom/google/obf/n;->M:Z

    if-eqz v0, :cond_0

    .line 323
    const/4 v0, 0x0

    .line 374
    :goto_0
    return v0

    .line 324
    :cond_0
    iget v0, p0, Lcom/google/obf/n;->D:I

    if-gez v0, :cond_1

    .line 325
    iget-boolean v0, p0, Lcom/google/obf/n;->v:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/obf/n;->J:Z

    if-eqz v0, :cond_3

    .line 326
    :try_start_0
    iget-object v0, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    iget-object v1, p0, Lcom/google/obf/n;->j:Landroid/media/MediaCodec$BufferInfo;

    .line 327
    invoke-virtual {p0}, Lcom/google/obf/n;->p()J

    move-result-wide v2

    .line 328
    invoke-virtual {v0, v1, v2, v3}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v0

    iput v0, p0, Lcom/google/obf/n;->D:I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 338
    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/obf/n;->D:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_4

    .line 339
    invoke-direct {p0}, Lcom/google/obf/n;->i()V

    .line 340
    const/4 v0, 0x1

    goto :goto_0

    .line 330
    :catch_0
    move-exception v0

    .line 331
    invoke-direct {p0}, Lcom/google/obf/n;->A()V

    .line 332
    iget-boolean v0, p0, Lcom/google/obf/n;->M:Z

    if-eqz v0, :cond_2

    .line 333
    invoke-virtual {p0}, Lcom/google/obf/n;->m()V

    .line 334
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 335
    :cond_3
    iget-object v0, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    iget-object v1, p0, Lcom/google/obf/n;->j:Landroid/media/MediaCodec$BufferInfo;

    .line 336
    invoke-virtual {p0}, Lcom/google/obf/n;->p()J

    move-result-wide v2

    .line 337
    invoke-virtual {v0, v1, v2, v3}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v0

    iput v0, p0, Lcom/google/obf/n;->D:I

    goto :goto_1

    .line 341
    :cond_4
    iget v0, p0, Lcom/google/obf/n;->D:I

    const/4 v1, -0x3

    if-ne v0, v1, :cond_5

    .line 342
    iget-object v0, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/n;->A:[Ljava/nio/ByteBuffer;

    .line 343
    iget-object v0, p0, Lcom/google/obf/n;->a:Lcom/google/obf/c;

    iget v1, v0, Lcom/google/obf/c;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/obf/c;->e:I

    .line 344
    const/4 v0, 0x1

    goto :goto_0

    .line 345
    :cond_5
    iget v0, p0, Lcom/google/obf/n;->D:I

    if-gez v0, :cond_8

    .line 346
    iget-boolean v0, p0, Lcom/google/obf/n;->t:Z

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/google/obf/n;->L:Z

    if-nez v0, :cond_6

    iget v0, p0, Lcom/google/obf/n;->H:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    .line 347
    :cond_6
    invoke-direct {p0}, Lcom/google/obf/n;->A()V

    .line 348
    const/4 v0, 0x1

    goto :goto_0

    .line 349
    :cond_7
    const/4 v0, 0x0

    goto :goto_0

    .line 350
    :cond_8
    iget-boolean v0, p0, Lcom/google/obf/n;->y:Z

    if-eqz v0, :cond_9

    .line 351
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/n;->y:Z

    .line 352
    iget-object v0, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    iget v1, p0, Lcom/google/obf/n;->D:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 353
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/obf/n;->D:I

    .line 354
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 355
    :cond_9
    iget-object v0, p0, Lcom/google/obf/n;->j:Landroid/media/MediaCodec$BufferInfo;

    iget v0, v0, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_a

    .line 356
    invoke-direct {p0}, Lcom/google/obf/n;->A()V

    .line 357
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 358
    :cond_a
    iget-object v0, p0, Lcom/google/obf/n;->j:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v0, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-direct {p0, v0, v1}, Lcom/google/obf/n;->h(J)I

    move-result v11

    .line 359
    iget-boolean v0, p0, Lcom/google/obf/n;->v:Z

    if-eqz v0, :cond_e

    iget-boolean v0, p0, Lcom/google/obf/n;->J:Z

    if-eqz v0, :cond_e

    .line 360
    :try_start_1
    iget-object v6, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    iget-object v0, p0, Lcom/google/obf/n;->A:[Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/google/obf/n;->D:I

    aget-object v7, v0, v1

    iget-object v8, p0, Lcom/google/obf/n;->j:Landroid/media/MediaCodec$BufferInfo;

    iget v9, p0, Lcom/google/obf/n;->D:I

    const/4 v0, -0x1

    if-eq v11, v0, :cond_c

    const/4 v10, 0x1

    :goto_2
    move-object v1, p0

    move-wide v2, p1

    move-wide/from16 v4, p3

    invoke-virtual/range {v1 .. v10}, Lcom/google/obf/n;->a(JJLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;IZ)Z
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    .line 368
    :goto_3
    if-eqz v0, :cond_10

    .line 369
    iget-object v0, p0, Lcom/google/obf/n;->j:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v0, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-virtual {p0, v0, v1}, Lcom/google/obf/n;->b(J)V

    .line 370
    const/4 v0, -0x1

    if-eq v11, v0, :cond_b

    .line 371
    iget-object v0, p0, Lcom/google/obf/n;->i:Ljava/util/List;

    invoke-interface {v0, v11}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 372
    :cond_b
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/obf/n;->D:I

    .line 373
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 360
    :cond_c
    const/4 v10, 0x0

    goto :goto_2

    .line 362
    :catch_1
    move-exception v0

    .line 363
    invoke-direct {p0}, Lcom/google/obf/n;->A()V

    .line 364
    iget-boolean v0, p0, Lcom/google/obf/n;->M:Z

    if-eqz v0, :cond_d

    .line 365
    invoke-virtual {p0}, Lcom/google/obf/n;->m()V

    .line 366
    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 367
    :cond_e
    iget-object v6, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    iget-object v0, p0, Lcom/google/obf/n;->A:[Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/google/obf/n;->D:I

    aget-object v7, v0, v1

    iget-object v8, p0, Lcom/google/obf/n;->j:Landroid/media/MediaCodec$BufferInfo;

    iget v9, p0, Lcom/google/obf/n;->D:I

    const/4 v0, -0x1

    if-eq v11, v0, :cond_f

    const/4 v10, 0x1

    :goto_4
    move-object v1, p0

    move-wide v2, p1

    move-wide/from16 v4, p3

    invoke-virtual/range {v1 .. v10}, Lcom/google/obf/n;->a(JJLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;IZ)Z

    move-result v0

    goto :goto_3

    :cond_f
    const/4 v10, 0x0

    goto :goto_4

    .line 374
    :cond_10
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private a(JZ)Z
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 181
    iget-boolean v0, p0, Lcom/google/obf/n;->L:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/obf/n;->H:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 182
    :cond_0
    const/4 v0, 0x0

    .line 274
    :goto_0
    return v0

    .line 183
    :cond_1
    iget v0, p0, Lcom/google/obf/n;->C:I

    if-gez v0, :cond_3

    .line 184
    iget-object v0, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v0

    iput v0, p0, Lcom/google/obf/n;->C:I

    .line 185
    iget v0, p0, Lcom/google/obf/n;->C:I

    if-gez v0, :cond_2

    .line 186
    const/4 v0, 0x0

    goto :goto_0

    .line 187
    :cond_2
    iget-object v0, p0, Lcom/google/obf/n;->g:Lcom/google/obf/t;

    iget-object v1, p0, Lcom/google/obf/n;->z:[Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/google/obf/n;->C:I

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/google/obf/t;->b:Ljava/nio/ByteBuffer;

    .line 188
    iget-object v0, p0, Lcom/google/obf/n;->g:Lcom/google/obf/t;

    invoke-virtual {v0}, Lcom/google/obf/t;->d()V

    .line 189
    :cond_3
    iget v0, p0, Lcom/google/obf/n;->H:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 190
    iget-boolean v0, p0, Lcom/google/obf/n;->t:Z

    if-eqz v0, :cond_4

    .line 194
    :goto_1
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/obf/n;->H:I

    .line 195
    const/4 v0, 0x0

    goto :goto_0

    .line 191
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/n;->J:Z

    .line 192
    iget-object v0, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    iget v1, p0, Lcom/google/obf/n;->C:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v6, 0x4

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 193
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/obf/n;->C:I

    goto :goto_1

    .line 196
    :cond_5
    iget-boolean v0, p0, Lcom/google/obf/n;->x:Z

    if-eqz v0, :cond_6

    .line 197
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/n;->x:Z

    .line 198
    iget-object v0, p0, Lcom/google/obf/n;->g:Lcom/google/obf/t;

    iget-object v0, v0, Lcom/google/obf/t;->b:Ljava/nio/ByteBuffer;

    sget-object v1, Lcom/google/obf/n;->c:[B

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 199
    iget-object v0, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    iget v1, p0, Lcom/google/obf/n;->C:I

    const/4 v2, 0x0

    sget-object v3, Lcom/google/obf/n;->c:[B

    array-length v3, v3

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 200
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/obf/n;->C:I

    .line 201
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/n;->I:Z

    .line 202
    const/4 v0, 0x1

    goto :goto_0

    .line 203
    :cond_6
    iget-boolean v0, p0, Lcom/google/obf/n;->N:Z

    if-eqz v0, :cond_8

    .line 204
    const/4 v0, -0x3

    .line 214
    :cond_7
    :goto_2
    const/4 v1, -0x2

    if-ne v0, v1, :cond_b

    .line 215
    const/4 v0, 0x0

    goto :goto_0

    .line 205
    :cond_8
    iget v0, p0, Lcom/google/obf/n;->G:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_a

    .line 206
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    iget-object v0, p0, Lcom/google/obf/n;->m:Lcom/google/obf/q;

    iget-object v0, v0, Lcom/google/obf/q;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 207
    iget-object v0, p0, Lcom/google/obf/n;->m:Lcom/google/obf/q;

    iget-object v0, v0, Lcom/google/obf/q;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 208
    iget-object v2, p0, Lcom/google/obf/n;->g:Lcom/google/obf/t;

    iget-object v2, v2, Lcom/google/obf/t;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 209
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 210
    :cond_9
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/obf/n;->G:I

    .line 211
    :cond_a
    iget-object v0, p0, Lcom/google/obf/n;->h:Lcom/google/obf/r;

    iget-object v1, p0, Lcom/google/obf/n;->g:Lcom/google/obf/t;

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/obf/n;->a(JLcom/google/obf/r;Lcom/google/obf/t;)I

    move-result v0

    .line 212
    if-eqz p3, :cond_7

    iget v1, p0, Lcom/google/obf/n;->K:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_7

    const/4 v1, -0x2

    if-ne v0, v1, :cond_7

    .line 213
    const/4 v1, 0x2

    iput v1, p0, Lcom/google/obf/n;->K:I

    goto :goto_2

    .line 216
    :cond_b
    const/4 v1, -0x4

    if-ne v0, v1, :cond_d

    .line 217
    iget v0, p0, Lcom/google/obf/n;->G:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_c

    .line 218
    iget-object v0, p0, Lcom/google/obf/n;->g:Lcom/google/obf/t;

    invoke-virtual {v0}, Lcom/google/obf/t;->d()V

    .line 219
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/obf/n;->G:I

    .line 220
    :cond_c
    iget-object v0, p0, Lcom/google/obf/n;->h:Lcom/google/obf/r;

    invoke-virtual {p0, v0}, Lcom/google/obf/n;->a(Lcom/google/obf/r;)V

    .line 221
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 222
    :cond_d
    const/4 v1, -0x1

    if-ne v0, v1, :cond_11

    .line 223
    iget v0, p0, Lcom/google/obf/n;->G:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_e

    .line 224
    iget-object v0, p0, Lcom/google/obf/n;->g:Lcom/google/obf/t;

    invoke-virtual {v0}, Lcom/google/obf/t;->d()V

    .line 225
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/obf/n;->G:I

    .line 226
    :cond_e
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/n;->L:Z

    .line 227
    iget-boolean v0, p0, Lcom/google/obf/n;->I:Z

    if-nez v0, :cond_f

    .line 228
    invoke-direct {p0}, Lcom/google/obf/n;->A()V

    .line 229
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 230
    :cond_f
    :try_start_0
    iget-boolean v0, p0, Lcom/google/obf/n;->t:Z

    if-eqz v0, :cond_10

    .line 238
    :goto_4
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 231
    :cond_10
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/n;->J:Z

    .line 232
    iget-object v0, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    iget v1, p0, Lcom/google/obf/n;->C:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v6, 0x4

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 233
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/obf/n;->C:I
    :try_end_0
    .catch Landroid/media/MediaCodec$CryptoException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    .line 235
    :catch_0
    move-exception v0

    .line 236
    invoke-direct {p0, v0}, Lcom/google/obf/n;->a(Landroid/media/MediaCodec$CryptoException;)V

    .line 237
    new-instance v1, Lcom/google/obf/g;

    invoke-direct {v1, v0}, Lcom/google/obf/g;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 239
    :cond_11
    iget-boolean v0, p0, Lcom/google/obf/n;->O:Z

    if-eqz v0, :cond_14

    .line 240
    iget-object v0, p0, Lcom/google/obf/n;->g:Lcom/google/obf/t;

    invoke-virtual {v0}, Lcom/google/obf/t;->c()Z

    move-result v0

    if-nez v0, :cond_13

    .line 241
    iget-object v0, p0, Lcom/google/obf/n;->g:Lcom/google/obf/t;

    invoke-virtual {v0}, Lcom/google/obf/t;->d()V

    .line 242
    iget v0, p0, Lcom/google/obf/n;->G:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_12

    .line 243
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/obf/n;->G:I

    .line 244
    :cond_12
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 245
    :cond_13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/n;->O:Z

    .line 246
    :cond_14
    iget-object v0, p0, Lcom/google/obf/n;->g:Lcom/google/obf/t;

    invoke-virtual {v0}, Lcom/google/obf/t;->a()Z

    move-result v6

    .line 247
    invoke-direct {p0, v6}, Lcom/google/obf/n;->a(Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/obf/n;->N:Z

    .line 248
    iget-boolean v0, p0, Lcom/google/obf/n;->N:Z

    if-eqz v0, :cond_15

    .line 249
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 250
    :cond_15
    iget-boolean v0, p0, Lcom/google/obf/n;->q:Z

    if-eqz v0, :cond_17

    if-nez v6, :cond_17

    .line 251
    iget-object v0, p0, Lcom/google/obf/n;->g:Lcom/google/obf/t;

    iget-object v0, v0, Lcom/google/obf/t;->b:Ljava/nio/ByteBuffer;

    invoke-static {v0}, Lcom/google/obf/du;->a(Ljava/nio/ByteBuffer;)V

    .line 252
    iget-object v0, p0, Lcom/google/obf/n;->g:Lcom/google/obf/t;

    iget-object v0, v0, Lcom/google/obf/t;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    if-nez v0, :cond_16

    .line 253
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 254
    :cond_16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/n;->q:Z

    .line 255
    :cond_17
    :try_start_1
    iget-object v0, p0, Lcom/google/obf/n;->g:Lcom/google/obf/t;

    iget-object v0, v0, Lcom/google/obf/t;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v5

    .line 256
    iget-object v0, p0, Lcom/google/obf/n;->g:Lcom/google/obf/t;

    iget v0, v0, Lcom/google/obf/t;->c:I

    sub-int v0, v5, v0

    .line 257
    iget-object v1, p0, Lcom/google/obf/n;->g:Lcom/google/obf/t;

    iget-wide v2, v1, Lcom/google/obf/t;->e:J

    .line 258
    iget-object v1, p0, Lcom/google/obf/n;->g:Lcom/google/obf/t;

    invoke-virtual {v1}, Lcom/google/obf/t;->b()Z

    move-result v1

    if-eqz v1, :cond_18

    .line 259
    iget-object v1, p0, Lcom/google/obf/n;->i:Ljava/util/List;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 260
    :cond_18
    iget-object v1, p0, Lcom/google/obf/n;->g:Lcom/google/obf/t;

    iget-object v4, v1, Lcom/google/obf/t;->b:Ljava/nio/ByteBuffer;

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Lcom/google/obf/n;->a(JLjava/nio/ByteBuffer;IZ)V

    .line 261
    if-eqz v6, :cond_19

    .line 262
    iget-object v1, p0, Lcom/google/obf/n;->g:Lcom/google/obf/t;

    invoke-static {v1, v0}, Lcom/google/obf/n;->a(Lcom/google/obf/t;I)Landroid/media/MediaCodec$CryptoInfo;

    move-result-object v7

    .line 263
    iget-object v4, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    iget v5, p0, Lcom/google/obf/n;->C:I

    const/4 v6, 0x0

    const/4 v10, 0x0

    move-wide v8, v2

    invoke-virtual/range {v4 .. v10}, Landroid/media/MediaCodec;->queueSecureInputBuffer(IILandroid/media/MediaCodec$CryptoInfo;JI)V

    .line 266
    :goto_5
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/obf/n;->C:I

    .line 267
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/n;->I:Z

    .line 268
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/n;->G:I

    .line 269
    iget-object v0, p0, Lcom/google/obf/n;->a:Lcom/google/obf/c;

    iget v1, v0, Lcom/google/obf/c;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/obf/c;->c:I

    .line 274
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 265
    :cond_19
    iget-object v6, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    iget v7, p0, Lcom/google/obf/n;->C:I

    const/4 v8, 0x0

    const/4 v12, 0x0

    move v9, v5

    move-wide v10, v2

    invoke-virtual/range {v6 .. v12}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V
    :try_end_1
    .catch Landroid/media/MediaCodec$CryptoException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_5

    .line 271
    :catch_1
    move-exception v0

    .line 272
    invoke-direct {p0, v0}, Lcom/google/obf/n;->a(Landroid/media/MediaCodec$CryptoException;)V

    .line 273
    new-instance v1, Lcom/google/obf/g;

    invoke-direct {v1, v0}, Lcom/google/obf/g;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/16 v1, 0x12

    .line 407
    sget v0, Lcom/google/obf/ea;->a:I

    if-lt v0, v1, :cond_1

    sget v0, Lcom/google/obf/ea;->a:I

    if-ne v0, v1, :cond_0

    const-string v0, "OMX.SEC.avc.dec"

    .line 408
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "OMX.SEC.avc.dec.secure"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget v0, Lcom/google/obf/ea;->a:I

    const/16 v1, 0x13

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/google/obf/ea;->d:Ljava/lang/String;

    const-string v1, "SM-G800"

    .line 409
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "OMX.Exynos.avc.dec"

    .line 410
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "OMX.Exynos.avc.dec.secure"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 411
    :goto_0
    return v0

    .line 410
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Lcom/google/obf/q;)Z
    .locals 2

    .prologue
    .line 417
    sget v0, Lcom/google/obf/ea;->a:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    iget-object v0, p1, Lcom/google/obf/q;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "OMX.MTK.VIDEO.DECODER.AVC"

    .line 418
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 419
    :goto_0
    return v0

    .line 418
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Z)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 286
    iget-boolean v1, p0, Lcom/google/obf/n;->E:Z

    if-nez v1, :cond_1

    .line 293
    :cond_0
    :goto_0
    return v0

    .line 288
    :cond_1
    iget-object v1, p0, Lcom/google/obf/n;->e:Lcom/google/obf/ac;

    invoke-interface {v1}, Lcom/google/obf/ac;->b()I

    move-result v1

    .line 289
    if-nez v1, :cond_2

    .line 290
    new-instance v0, Lcom/google/obf/g;

    iget-object v1, p0, Lcom/google/obf/n;->e:Lcom/google/obf/ac;

    invoke-interface {v1}, Lcom/google/obf/ac;->d()Ljava/lang/Exception;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/g;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 291
    :cond_2
    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    if-nez p1, :cond_3

    iget-boolean v1, p0, Lcom/google/obf/n;->f:Z

    if-nez v1, :cond_0

    .line 292
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Lcom/google/obf/q;)Landroid/media/MediaFormat;
    .locals 3

    .prologue
    .line 282
    invoke-virtual {p1}, Lcom/google/obf/q;->b()Landroid/media/MediaFormat;

    move-result-object v0

    .line 283
    iget-boolean v1, p0, Lcom/google/obf/n;->l:Z

    if-eqz v1, :cond_0

    .line 284
    const-string v1, "auto-frc"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 285
    :cond_0
    return-object v0
.end method

.method private b(Lcom/google/obf/n$a;)V
    .locals 2

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/obf/n;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/n;->k:Lcom/google/obf/n$b;

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Lcom/google/obf/n;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/obf/n$1;

    invoke-direct {v1, p0, p1}, Lcom/google/obf/n$1;-><init>(Lcom/google/obf/n;Lcom/google/obf/n$a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 394
    :cond_0
    return-void
.end method

.method private static b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 412
    sget v0, Lcom/google/obf/ea;->a:I

    const/16 v1, 0x18

    if-ge v0, v1, :cond_2

    const-string v0, "OMX.Nvidia.h264.decode"

    .line 413
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "OMX.Nvidia.h264.decode.secure"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    sget-object v0, Lcom/google/obf/ea;->b:Ljava/lang/String;

    const-string v1, "flounder"

    .line 414
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/obf/ea;->b:Ljava/lang/String;

    const-string v1, "flounder_lte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/obf/ea;->b:Ljava/lang/String;

    const-string v1, "grouper"

    .line 415
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/obf/ea;->b:Ljava/lang/String;

    const-string v1, "tilapia"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 416
    :goto_0
    return v0

    .line 415
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;Lcom/google/obf/q;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 429
    sget v1, Lcom/google/obf/ea;->a:I

    const/16 v2, 0x12

    if-gt v1, v2, :cond_0

    iget v1, p1, Lcom/google/obf/q;->q:I

    if-ne v1, v0, :cond_0

    const-string v1, "OMX.MTK.AUDIO.DECODER.MP3"

    .line 430
    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 431
    :goto_0
    return v0

    .line 430
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 420
    sget v0, Lcom/google/obf/ea;->a:I

    const/16 v1, 0x11

    if-gt v0, v1, :cond_1

    const-string v0, "OMX.rk.video_decoder.avc"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "OMX.allwinner.video.decoder.avc"

    .line 421
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 422
    :goto_0
    return v0

    .line 421
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 423
    sget v0, Lcom/google/obf/ea;->a:I

    const/16 v1, 0x17

    if-gt v0, v1, :cond_0

    const-string v0, "OMX.google.vorbis.decoder"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget v0, Lcom/google/obf/ea;->a:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_2

    const-string v0, "hb2000"

    sget-object v1, Lcom/google/obf/ea;->b:Ljava/lang/String;

    .line 424
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "OMX.amlogic.avc.decoder.awesome"

    .line 425
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "OMX.amlogic.avc.decoder.awesome.secure"

    .line 426
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 427
    :goto_0
    return v0

    .line 426
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static e(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 428
    sget v0, Lcom/google/obf/ea;->a:I

    const/16 v1, 0x15

    if-ne v0, v1, :cond_0

    const-string v0, "OMX.google.aac.decoder"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static f(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 432
    sget v0, Lcom/google/obf/ea;->a:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_1

    sget-object v0, Lcom/google/obf/ea;->d:Ljava/lang/String;

    const-string v1, "ODROID-XU3"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "OMX.Exynos.AVC.Decoder"

    .line 433
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "OMX.Exynos.AVC.Decoder.secure"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 434
    :goto_0
    return v0

    .line 433
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g(J)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/obf/n;->h:Lcom/google/obf/r;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/obf/n;->a(JLcom/google/obf/r;Lcom/google/obf/t;)I

    move-result v0

    .line 159
    const/4 v1, -0x4

    if-ne v0, v1, :cond_0

    .line 160
    iget-object v0, p0, Lcom/google/obf/n;->h:Lcom/google/obf/r;

    invoke-virtual {p0, v0}, Lcom/google/obf/n;->a(Lcom/google/obf/r;)V

    .line 161
    :cond_0
    return-void
.end method

.method private h(J)I
    .locals 7

    .prologue
    .line 401
    iget-object v0, p0, Lcom/google/obf/n;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 402
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 403
    iget-object v0, p0, Lcom/google/obf/n;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v4, p1

    if-nez v0, :cond_0

    move v0, v1

    .line 406
    :goto_1
    return v0

    .line 405
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 406
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private i()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    const/16 v3, 0x20

    const/4 v2, 0x1

    .line 375
    iget-object v0, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v0

    .line 376
    iget-boolean v1, p0, Lcom/google/obf/n;->s:Z

    if-eqz v1, :cond_0

    const-string v1, "width"

    .line 377
    invoke-virtual {v0, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v3, :cond_0

    const-string v1, "height"

    .line 378
    invoke-virtual {v0, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 379
    iput-boolean v2, p0, Lcom/google/obf/n;->y:Z

    .line 385
    :goto_0
    return-void

    .line 381
    :cond_0
    iget-boolean v1, p0, Lcom/google/obf/n;->w:Z

    if-eqz v1, :cond_1

    .line 382
    const-string v1, "channel-count"

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 383
    :cond_1
    iget-object v1, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    invoke-virtual {p0, v1, v0}, Lcom/google/obf/n;->a(Landroid/media/MediaCodec;Landroid/media/MediaFormat;)V

    .line 384
    iget-object v0, p0, Lcom/google/obf/n;->a:Lcom/google/obf/c;

    iget v1, v0, Lcom/google/obf/c;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/obf/c;->d:I

    goto :goto_0
.end method


# virtual methods
.method protected a(Lcom/google/obf/m;Ljava/lang/String;Z)Lcom/google/obf/f;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/o$b;
        }
    .end annotation

    .prologue
    .line 20
    invoke-interface {p1, p2, p3}, Lcom/google/obf/m;->a(Ljava/lang/String;Z)Lcom/google/obf/f;

    move-result-object v0

    return-object v0
.end method

.method protected a(J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 136
    iput v0, p0, Lcom/google/obf/n;->K:I

    .line 137
    iput-boolean v0, p0, Lcom/google/obf/n;->L:Z

    .line 138
    iput-boolean v0, p0, Lcom/google/obf/n;->M:Z

    .line 139
    iget-object v0, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {p0}, Lcom/google/obf/n;->n()V

    .line 141
    :cond_0
    return-void
.end method

.method protected a(JJZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 144
    if-eqz p5, :cond_6

    .line 145
    iget v0, p0, Lcom/google/obf/n;->K:I

    if-nez v0, :cond_5

    move v0, v1

    .line 146
    :goto_0
    iput v0, p0, Lcom/google/obf/n;->K:I

    .line 147
    iget-object v0, p0, Lcom/google/obf/n;->m:Lcom/google/obf/q;

    if-nez v0, :cond_0

    .line 148
    invoke-direct {p0, p1, p2}, Lcom/google/obf/n;->g(J)V

    .line 149
    :cond_0
    invoke-virtual {p0}, Lcom/google/obf/n;->j()V

    .line 150
    iget-object v0, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    if-eqz v0, :cond_4

    .line 151
    const-string v0, "drainAndFeed"

    invoke-static {v0}, Lcom/google/obf/dz;->a(Ljava/lang/String;)V

    .line 152
    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/obf/n;->a(JJ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 153
    invoke-direct {p0, p1, p2, v1}, Lcom/google/obf/n;->a(JZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 154
    :cond_2
    invoke-direct {p0, p1, p2, v2}, Lcom/google/obf/n;->a(JZ)Z

    move-result v0

    if-nez v0, :cond_2

    .line 155
    :cond_3
    invoke-static {}, Lcom/google/obf/dz;->a()V

    .line 156
    :cond_4
    iget-object v0, p0, Lcom/google/obf/n;->a:Lcom/google/obf/c;

    invoke-virtual {v0}, Lcom/google/obf/c;->a()V

    .line 157
    return-void

    .line 145
    :cond_5
    iget v0, p0, Lcom/google/obf/n;->K:I

    goto :goto_0

    :cond_6
    move v0, v2

    .line 146
    goto :goto_0
.end method

.method protected a(JLjava/nio/ByteBuffer;IZ)V
    .locals 0

    .prologue
    .line 312
    return-void
.end method

.method protected a(Landroid/media/MediaCodec;Landroid/media/MediaFormat;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 310
    return-void
.end method

.method protected abstract a(Landroid/media/MediaCodec;ZLandroid/media/MediaFormat;Landroid/media/MediaCrypto;)V
.end method

.method protected a(Lcom/google/obf/r;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 294
    iget-object v3, p0, Lcom/google/obf/n;->m:Lcom/google/obf/q;

    .line 295
    iget-object v0, p1, Lcom/google/obf/r;->a:Lcom/google/obf/q;

    iput-object v0, p0, Lcom/google/obf/n;->m:Lcom/google/obf/q;

    .line 296
    iget-object v0, p1, Lcom/google/obf/r;->b:Lcom/google/obf/ab;

    iput-object v0, p0, Lcom/google/obf/n;->n:Lcom/google/obf/ab;

    .line 297
    iget-object v0, p0, Lcom/google/obf/n;->n:Lcom/google/obf/ab;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/obf/n;->E:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 298
    :goto_0
    iget-object v4, p0, Lcom/google/obf/n;->m:Lcom/google/obf/q;

    invoke-static {v4, v3}, Lcom/google/obf/ea;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    if-nez v0, :cond_1

    .line 309
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 297
    goto :goto_0

    .line 300
    :cond_1
    iget-object v4, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    if-eqz v4, :cond_3

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    iget-boolean v4, p0, Lcom/google/obf/n;->p:Z

    iget-object v5, p0, Lcom/google/obf/n;->m:Lcom/google/obf/q;

    .line 301
    invoke-virtual {p0, v0, v4, v3, v5}, Lcom/google/obf/n;->a(Landroid/media/MediaCodec;ZLcom/google/obf/q;Lcom/google/obf/q;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 302
    iput-boolean v1, p0, Lcom/google/obf/n;->F:Z

    .line 303
    iput v1, p0, Lcom/google/obf/n;->G:I

    .line 304
    iget-boolean v0, p0, Lcom/google/obf/n;->s:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/obf/n;->m:Lcom/google/obf/q;

    iget v0, v0, Lcom/google/obf/q;->h:I

    iget v4, v3, Lcom/google/obf/q;->h:I

    if-ne v0, v4, :cond_2

    iget-object v0, p0, Lcom/google/obf/n;->m:Lcom/google/obf/q;

    iget v0, v0, Lcom/google/obf/q;->i:I

    iget v3, v3, Lcom/google/obf/q;->i:I

    if-ne v0, v3, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/google/obf/n;->x:Z

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2

    .line 305
    :cond_3
    iget-boolean v0, p0, Lcom/google/obf/n;->I:Z

    if-eqz v0, :cond_4

    .line 306
    iput v1, p0, Lcom/google/obf/n;->H:I

    goto :goto_1

    .line 307
    :cond_4
    invoke-virtual {p0}, Lcom/google/obf/n;->m()V

    .line 308
    invoke-virtual {p0}, Lcom/google/obf/n;->j()V

    goto :goto_1
.end method

.method protected abstract a(JJLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;IZ)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation
.end method

.method protected a(Landroid/media/MediaCodec;ZLcom/google/obf/q;Lcom/google/obf/q;)Z
    .locals 1

    .prologue
    .line 314
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract a(Lcom/google/obf/m;Lcom/google/obf/q;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/o$b;
        }
    .end annotation
.end method

.method protected final a(Lcom/google/obf/q;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/o$b;
        }
    .end annotation

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/obf/n;->d:Lcom/google/obf/m;

    invoke-virtual {p0, v0, p1}, Lcom/google/obf/n;->a(Lcom/google/obf/m;Lcom/google/obf/q;)Z

    move-result v0

    return v0
.end method

.method protected b(J)V
    .locals 0

    .prologue
    .line 313
    return-void
.end method

.method protected c()V
    .locals 0

    .prologue
    .line 142
    return-void
.end method

.method protected d()V
    .locals 0

    .prologue
    .line 143
    return-void
.end method

.method protected e()Z
    .locals 1

    .prologue
    .line 315
    iget-boolean v0, p0, Lcom/google/obf/n;->M:Z

    return v0
.end method

.method protected f()Z
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/obf/n;->m:Lcom/google/obf/q;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/obf/n;->N:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/obf/n;->K:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/obf/n;->D:I

    if-gez v0, :cond_0

    .line 317
    invoke-direct {p0}, Lcom/google/obf/n;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 318
    :goto_0
    return v0

    .line 317
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected g()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 83
    iput-object v0, p0, Lcom/google/obf/n;->m:Lcom/google/obf/q;

    .line 84
    iput-object v0, p0, Lcom/google/obf/n;->n:Lcom/google/obf/ab;

    .line 85
    :try_start_0
    invoke-virtual {p0}, Lcom/google/obf/n;->m()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 86
    :try_start_1
    iget-boolean v0, p0, Lcom/google/obf/n;->E:Z

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/obf/n;->e:Lcom/google/obf/ac;

    invoke-interface {v0}, Lcom/google/obf/ac;->a()V

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/n;->E:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89
    :cond_0
    invoke-super {p0}, Lcom/google/obf/v;->g()V

    .line 100
    return-void

    .line 91
    :catchall_0
    move-exception v0

    invoke-super {p0}, Lcom/google/obf/v;->g()V

    throw v0

    .line 93
    :catchall_1
    move-exception v0

    .line 94
    :try_start_2
    iget-boolean v1, p0, Lcom/google/obf/n;->E:Z

    if-eqz v1, :cond_1

    .line 95
    iget-object v1, p0, Lcom/google/obf/n;->e:Lcom/google/obf/ac;

    invoke-interface {v1}, Lcom/google/obf/ac;->a()V

    .line 96
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/obf/n;->E:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 97
    :cond_1
    invoke-super {p0}, Lcom/google/obf/v;->g()V

    .line 99
    throw v0

    :catchall_2
    move-exception v0

    invoke-super {p0}, Lcom/google/obf/v;->g()V

    throw v0
.end method

.method protected h()V
    .locals 0

    .prologue
    .line 311
    return-void
.end method

.method protected final j()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    const/4 v11, 0x3

    const/4 v2, 0x0

    const/4 v10, -0x1

    const/4 v3, 0x0

    const/4 v7, 0x1

    .line 21
    invoke-virtual {p0}, Lcom/google/obf/n;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 23
    :cond_1
    iget-object v0, p0, Lcom/google/obf/n;->m:Lcom/google/obf/q;

    iget-object v4, v0, Lcom/google/obf/q;->b:Ljava/lang/String;

    .line 26
    iget-object v0, p0, Lcom/google/obf/n;->n:Lcom/google/obf/ab;

    if-eqz v0, :cond_9

    .line 27
    iget-object v0, p0, Lcom/google/obf/n;->e:Lcom/google/obf/ac;

    if-nez v0, :cond_2

    .line 28
    new-instance v0, Lcom/google/obf/g;

    const-string v1, "Media requires a DrmSessionManager"

    invoke-direct {v0, v1}, Lcom/google/obf/g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_2
    iget-boolean v0, p0, Lcom/google/obf/n;->E:Z

    if-nez v0, :cond_3

    .line 30
    iget-object v0, p0, Lcom/google/obf/n;->e:Lcom/google/obf/ac;

    iget-object v1, p0, Lcom/google/obf/n;->n:Lcom/google/obf/ab;

    invoke-interface {v0, v1}, Lcom/google/obf/ac;->a(Lcom/google/obf/ab;)V

    .line 31
    iput-boolean v7, p0, Lcom/google/obf/n;->E:Z

    .line 32
    :cond_3
    iget-object v0, p0, Lcom/google/obf/n;->e:Lcom/google/obf/ac;

    invoke-interface {v0}, Lcom/google/obf/ac;->b()I

    move-result v0

    .line 33
    if-nez v0, :cond_4

    .line 34
    new-instance v0, Lcom/google/obf/g;

    iget-object v1, p0, Lcom/google/obf/n;->e:Lcom/google/obf/ac;

    invoke-interface {v1}, Lcom/google/obf/ac;->d()Ljava/lang/Exception;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/g;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 35
    :cond_4
    if-eq v0, v11, :cond_5

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 36
    :cond_5
    iget-object v0, p0, Lcom/google/obf/n;->e:Lcom/google/obf/ac;

    invoke-interface {v0}, Lcom/google/obf/ac;->c()Lcom/google/obf/ad;

    move-result-object v0

    check-cast v0, Lcom/google/obf/ae;

    invoke-virtual {v0}, Lcom/google/obf/ae;->a()Landroid/media/MediaCrypto;

    move-result-object v1

    .line 37
    iget-object v0, p0, Lcom/google/obf/n;->e:Lcom/google/obf/ac;

    invoke-interface {v0, v4}, Lcom/google/obf/ac;->a(Ljava/lang/String;)Z

    move-result v0

    move v6, v0

    move-object v0, v1

    .line 40
    :goto_1
    :try_start_0
    iget-object v1, p0, Lcom/google/obf/n;->d:Lcom/google/obf/m;

    invoke-virtual {p0, v1, v4, v6}, Lcom/google/obf/n;->a(Lcom/google/obf/m;Ljava/lang/String;Z)Lcom/google/obf/f;
    :try_end_0
    .catch Lcom/google/obf/o$b; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    move-object v4, v1

    .line 44
    :goto_2
    if-nez v4, :cond_6

    .line 45
    new-instance v1, Lcom/google/obf/n$a;

    iget-object v5, p0, Lcom/google/obf/n;->m:Lcom/google/obf/q;

    const v8, -0xc34f

    invoke-direct {v1, v5, v3, v6, v8}, Lcom/google/obf/n$a;-><init>(Lcom/google/obf/q;Ljava/lang/Throwable;ZI)V

    invoke-direct {p0, v1}, Lcom/google/obf/n;->a(Lcom/google/obf/n$a;)V

    .line 46
    :cond_6
    iget-object v1, v4, Lcom/google/obf/f;->a:Ljava/lang/String;

    .line 47
    iget-boolean v3, v4, Lcom/google/obf/f;->c:Z

    if-eqz v3, :cond_7

    invoke-static {v1}, Lcom/google/obf/n;->f(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    move v2, v7

    :cond_7
    iput-boolean v2, p0, Lcom/google/obf/n;->p:Z

    .line 48
    iget-object v2, p0, Lcom/google/obf/n;->m:Lcom/google/obf/q;

    invoke-static {v1, v2}, Lcom/google/obf/n;->a(Ljava/lang/String;Lcom/google/obf/q;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/obf/n;->q:Z

    .line 49
    invoke-static {v1}, Lcom/google/obf/n;->a(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/obf/n;->r:Z

    .line 50
    invoke-static {v1}, Lcom/google/obf/n;->b(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/obf/n;->s:Z

    .line 51
    invoke-static {v1}, Lcom/google/obf/n;->c(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/obf/n;->t:Z

    .line 52
    invoke-static {v1}, Lcom/google/obf/n;->d(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/obf/n;->u:Z

    .line 53
    invoke-static {v1}, Lcom/google/obf/n;->e(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/obf/n;->v:Z

    .line 54
    iget-object v2, p0, Lcom/google/obf/n;->m:Lcom/google/obf/q;

    invoke-static {v1, v2}, Lcom/google/obf/n;->b(Ljava/lang/String;Lcom/google/obf/q;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/obf/n;->w:Z

    .line 55
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 56
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x13

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "createByCodecName("

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/obf/dz;->a(Ljava/lang/String;)V

    .line 57
    invoke-static {v1}, Landroid/media/MediaCodec;->createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v2

    iput-object v2, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    .line 58
    invoke-static {}, Lcom/google/obf/dz;->a()V

    .line 59
    const-string v2, "configureCodec"

    invoke-static {v2}, Lcom/google/obf/dz;->a(Ljava/lang/String;)V

    .line 60
    iget-object v2, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    iget-boolean v3, v4, Lcom/google/obf/f;->c:Z

    iget-object v4, p0, Lcom/google/obf/n;->m:Lcom/google/obf/q;

    invoke-direct {p0, v4}, Lcom/google/obf/n;->b(Lcom/google/obf/q;)Landroid/media/MediaFormat;

    move-result-object v4

    invoke-virtual {p0, v2, v3, v4, v0}, Lcom/google/obf/n;->a(Landroid/media/MediaCodec;ZLandroid/media/MediaFormat;Landroid/media/MediaCrypto;)V

    .line 61
    invoke-static {}, Lcom/google/obf/dz;->a()V

    .line 62
    const-string v0, "codec.start()"

    invoke-static {v0}, Lcom/google/obf/dz;->a(Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->start()V

    .line 64
    invoke-static {}, Lcom/google/obf/dz;->a()V

    .line 65
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 66
    sub-long v4, v2, v8

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/obf/n;->a(Ljava/lang/String;JJ)V

    .line 67
    iget-object v0, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/n;->z:[Ljava/nio/ByteBuffer;

    .line 68
    iget-object v0, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/n;->A:[Ljava/nio/ByteBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 72
    :goto_3
    invoke-virtual {p0}, Lcom/google/obf/n;->v()I

    move-result v0

    if-ne v0, v11, :cond_8

    .line 73
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    :goto_4
    iput-wide v0, p0, Lcom/google/obf/n;->B:J

    .line 74
    iput v10, p0, Lcom/google/obf/n;->C:I

    .line 75
    iput v10, p0, Lcom/google/obf/n;->D:I

    .line 76
    iput-boolean v7, p0, Lcom/google/obf/n;->O:Z

    .line 77
    iget-object v0, p0, Lcom/google/obf/n;->a:Lcom/google/obf/c;

    iget v1, v0, Lcom/google/obf/c;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/obf/c;->a:I

    goto/16 :goto_0

    .line 42
    :catch_0
    move-exception v1

    .line 43
    new-instance v4, Lcom/google/obf/n$a;

    iget-object v5, p0, Lcom/google/obf/n;->m:Lcom/google/obf/q;

    const v8, -0xc34e

    invoke-direct {v4, v5, v1, v6, v8}, Lcom/google/obf/n$a;-><init>(Lcom/google/obf/q;Ljava/lang/Throwable;ZI)V

    invoke-direct {p0, v4}, Lcom/google/obf/n;->a(Lcom/google/obf/n$a;)V

    move-object v4, v3

    goto/16 :goto_2

    .line 70
    :catch_1
    move-exception v0

    .line 71
    new-instance v2, Lcom/google/obf/n$a;

    iget-object v3, p0, Lcom/google/obf/n;->m:Lcom/google/obf/q;

    invoke-direct {v2, v3, v0, v6, v1}, Lcom/google/obf/n$a;-><init>(Lcom/google/obf/q;Ljava/lang/Throwable;ZLjava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/google/obf/n;->a(Lcom/google/obf/n$a;)V

    goto :goto_3

    .line 73
    :cond_8
    const-wide/16 v0, -0x1

    goto :goto_4

    :cond_9
    move v6, v2

    move-object v0, v3

    goto/16 :goto_1
.end method

.method protected k()Z
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/n;->m:Lcom/google/obf/q;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final l()Z
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected m()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 101
    iget-object v0, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    if-eqz v0, :cond_0

    .line 102
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/obf/n;->B:J

    .line 103
    iput v4, p0, Lcom/google/obf/n;->C:I

    .line 104
    iput v4, p0, Lcom/google/obf/n;->D:I

    .line 105
    iput-boolean v2, p0, Lcom/google/obf/n;->N:Z

    .line 106
    iget-object v0, p0, Lcom/google/obf/n;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 107
    iput-object v3, p0, Lcom/google/obf/n;->z:[Ljava/nio/ByteBuffer;

    .line 108
    iput-object v3, p0, Lcom/google/obf/n;->A:[Ljava/nio/ByteBuffer;

    .line 109
    iput-boolean v2, p0, Lcom/google/obf/n;->F:Z

    .line 110
    iput-boolean v2, p0, Lcom/google/obf/n;->I:Z

    .line 111
    iput-boolean v2, p0, Lcom/google/obf/n;->p:Z

    .line 112
    iput-boolean v2, p0, Lcom/google/obf/n;->q:Z

    .line 113
    iput-boolean v2, p0, Lcom/google/obf/n;->r:Z

    .line 114
    iput-boolean v2, p0, Lcom/google/obf/n;->s:Z

    .line 115
    iput-boolean v2, p0, Lcom/google/obf/n;->t:Z

    .line 116
    iput-boolean v2, p0, Lcom/google/obf/n;->u:Z

    .line 117
    iput-boolean v2, p0, Lcom/google/obf/n;->w:Z

    .line 118
    iput-boolean v2, p0, Lcom/google/obf/n;->x:Z

    .line 119
    iput-boolean v2, p0, Lcom/google/obf/n;->y:Z

    .line 120
    iput-boolean v2, p0, Lcom/google/obf/n;->J:Z

    .line 121
    iput v2, p0, Lcom/google/obf/n;->G:I

    .line 122
    iput v2, p0, Lcom/google/obf/n;->H:I

    .line 123
    iget-object v0, p0, Lcom/google/obf/n;->a:Lcom/google/obf/c;

    iget v1, v0, Lcom/google/obf/c;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/obf/c;->b:I

    .line 124
    :try_start_0
    iget-object v0, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 125
    :try_start_1
    iget-object v0, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 126
    iput-object v3, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    .line 135
    :cond_0
    return-void

    .line 128
    :catchall_0
    move-exception v0

    iput-object v3, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    throw v0

    .line 130
    :catchall_1
    move-exception v0

    .line 131
    :try_start_2
    iget-object v1, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->release()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 132
    iput-object v3, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    .line 134
    throw v0

    :catchall_2
    move-exception v0

    iput-object v3, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    throw v0
.end method

.method protected n()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 162
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/obf/n;->B:J

    .line 163
    iput v3, p0, Lcom/google/obf/n;->C:I

    .line 164
    iput v3, p0, Lcom/google/obf/n;->D:I

    .line 165
    iput-boolean v4, p0, Lcom/google/obf/n;->O:Z

    .line 166
    iput-boolean v2, p0, Lcom/google/obf/n;->N:Z

    .line 167
    iget-object v0, p0, Lcom/google/obf/n;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 168
    iput-boolean v2, p0, Lcom/google/obf/n;->x:Z

    .line 169
    iput-boolean v2, p0, Lcom/google/obf/n;->y:Z

    .line 170
    iget-boolean v0, p0, Lcom/google/obf/n;->r:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/obf/n;->u:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/obf/n;->J:Z

    if-eqz v0, :cond_2

    .line 171
    :cond_0
    invoke-virtual {p0}, Lcom/google/obf/n;->m()V

    .line 172
    invoke-virtual {p0}, Lcom/google/obf/n;->j()V

    .line 178
    :goto_0
    iget-boolean v0, p0, Lcom/google/obf/n;->F:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/obf/n;->m:Lcom/google/obf/q;

    if-eqz v0, :cond_1

    .line 179
    iput v4, p0, Lcom/google/obf/n;->G:I

    .line 180
    :cond_1
    return-void

    .line 173
    :cond_2
    iget v0, p0, Lcom/google/obf/n;->H:I

    if-eqz v0, :cond_3

    .line 174
    invoke-virtual {p0}, Lcom/google/obf/n;->m()V

    .line 175
    invoke-virtual {p0}, Lcom/google/obf/n;->j()V

    goto :goto_0

    .line 176
    :cond_3
    iget-object v0, p0, Lcom/google/obf/n;->o:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->flush()V

    .line 177
    iput-boolean v2, p0, Lcom/google/obf/n;->I:Z

    goto :goto_0
.end method

.method protected final o()I
    .locals 1

    .prologue
    .line 319
    iget v0, p0, Lcom/google/obf/n;->K:I

    return v0
.end method

.method protected p()J
    .locals 2

    .prologue
    .line 321
    const-wide/16 v0, 0x0

    return-wide v0
.end method
