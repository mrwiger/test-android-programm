.class public Lru/cn/utils/PermissionUtils;
.super Ljava/lang/Object;
.source "PermissionUtils.java"


# static fields
.field private static final PERMISSIONS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 14
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.ACCESS_COARSE_LOCATION"

    aput-object v2, v0, v1

    sput-object v0, Lru/cn/utils/PermissionUtils;->PERMISSIONS:[Ljava/lang/String;

    return-void
.end method

.method public static isGranted(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 17
    const/4 v0, 0x1

    .line 19
    .local v0, "granted":Z
    sget-object v3, Lru/cn/utils/PermissionUtils;->PERMISSIONS:[Ljava/lang/String;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v1, v3, v2

    .line 20
    .local v1, "s":Ljava/lang/String;
    invoke-static {p0, v1}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_0

    const/4 v0, 0x0

    .line 19
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 23
    .end local v1    # "s":Ljava/lang/String;
    :cond_1
    return v0
.end method

.method public static requestPermissions(Landroid/app/Activity;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 27
    sget-object v0, Lru/cn/utils/PermissionUtils;->PERMISSIONS:[Ljava/lang/String;

    const/16 v1, 0x1674

    invoke-static {p0, v0, v1}, Landroid/support/v4/app/ActivityCompat;->requestPermissions(Landroid/app/Activity;[Ljava/lang/String;I)V

    .line 28
    return-void
.end method
