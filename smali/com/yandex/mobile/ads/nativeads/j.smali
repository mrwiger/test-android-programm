.class public abstract Lcom/yandex/mobile/ads/nativeads/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/yandex/mobile/ads/nativeads/NativeGenericAd;
.implements Lcom/yandex/mobile/ads/nativeads/m;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/mobile/ads/nativeads/j$a;
    }
.end annotation


# instance fields
.field protected a:Lcom/yandex/mobile/ads/nativeads/d;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/yandex/mobile/ads/nativeads/l;

.field private final d:Lcom/yandex/mobile/ads/nativeads/p;

.field private final e:Lcom/yandex/mobile/ads/as;

.field private final f:Lcom/yandex/mobile/ads/nativeads/h;

.field private final g:Lcom/yandex/mobile/ads/nativeads/r;

.field private final h:Lcom/yandex/mobile/ads/PhoneStateTracker;

.field private i:Lcom/yandex/mobile/ads/nativeads/j$a;

.field private final j:Lcom/yandex/mobile/ads/PhoneStateTracker$b;

.field private final k:Lcom/yandex/mobile/ads/as$c;

.field private final l:Lcom/yandex/mobile/ads/report/b$a;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/yandex/mobile/ads/nativeads/o;Lcom/yandex/mobile/ads/nativeads/d;)V
    .locals 6

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    sget-object v0, Lcom/yandex/mobile/ads/nativeads/j$a;->a:Lcom/yandex/mobile/ads/nativeads/j$a;

    iput-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->i:Lcom/yandex/mobile/ads/nativeads/j$a;

    .line 239
    new-instance v0, Lcom/yandex/mobile/ads/nativeads/j$1;

    invoke-direct {v0, p0}, Lcom/yandex/mobile/ads/nativeads/j$1;-><init>(Lcom/yandex/mobile/ads/nativeads/j;)V

    iput-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->j:Lcom/yandex/mobile/ads/PhoneStateTracker$b;

    .line 250
    new-instance v0, Lcom/yandex/mobile/ads/nativeads/j$2;

    invoke-direct {v0, p0}, Lcom/yandex/mobile/ads/nativeads/j$2;-><init>(Lcom/yandex/mobile/ads/nativeads/j;)V

    iput-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->k:Lcom/yandex/mobile/ads/as$c;

    .line 259
    new-instance v0, Lcom/yandex/mobile/ads/nativeads/j$3;

    invoke-direct {v0}, Lcom/yandex/mobile/ads/nativeads/j$3;-><init>()V

    .line 271
    new-instance v0, Lcom/yandex/mobile/ads/nativeads/j$4;

    invoke-direct {v0, p0}, Lcom/yandex/mobile/ads/nativeads/j$4;-><init>(Lcom/yandex/mobile/ads/nativeads/j;)V

    iput-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->l:Lcom/yandex/mobile/ads/report/b$a;

    .line 54
    iput-object p1, p0, Lcom/yandex/mobile/ads/nativeads/j;->b:Landroid/content/Context;

    .line 56
    iput-object p3, p0, Lcom/yandex/mobile/ads/nativeads/j;->a:Lcom/yandex/mobile/ads/nativeads/d;

    .line 57
    invoke-virtual {p2}, Lcom/yandex/mobile/ads/nativeads/o;->d()Lcom/yandex/mobile/ads/nativeads/h;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->f:Lcom/yandex/mobile/ads/nativeads/h;

    .line 58
    new-instance v0, Lcom/yandex/mobile/ads/nativeads/l;

    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/j;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/j;->l:Lcom/yandex/mobile/ads/report/b$a;

    invoke-direct {v0, v1, p2, p3, v2}, Lcom/yandex/mobile/ads/nativeads/l;-><init>(Landroid/content/Context;Lcom/yandex/mobile/ads/nativeads/o;Lcom/yandex/mobile/ads/nativeads/d;Lcom/yandex/mobile/ads/report/b$a;)V

    iput-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->c:Lcom/yandex/mobile/ads/nativeads/l;

    .line 60
    new-instance v0, Lcom/yandex/mobile/ads/nativeads/i;

    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/j;->f:Lcom/yandex/mobile/ads/nativeads/h;

    invoke-static {}, Lcom/yandex/mobile/ads/nativeads/s;->a()Lcom/yandex/mobile/ads/nativeads/s;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/mobile/ads/nativeads/i;-><init>(Lcom/yandex/mobile/ads/nativeads/h;Lcom/yandex/mobile/ads/nativeads/s;)V

    .line 61
    new-instance v1, Lcom/yandex/mobile/ads/nativeads/p;

    invoke-direct {v1, v0}, Lcom/yandex/mobile/ads/nativeads/p;-><init>(Lcom/yandex/mobile/ads/nativeads/i;)V

    iput-object v1, p0, Lcom/yandex/mobile/ads/nativeads/j;->d:Lcom/yandex/mobile/ads/nativeads/p;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 64
    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/j;->f:Lcom/yandex/mobile/ads/nativeads/h;

    invoke-virtual {v1}, Lcom/yandex/mobile/ads/nativeads/h;->e()Lcom/yandex/mobile/ads/nativeads/ab;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    invoke-virtual {p2}, Lcom/yandex/mobile/ads/nativeads/o;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 67
    invoke-virtual {p2}, Lcom/yandex/mobile/ads/nativeads/o;->b()Lcom/yandex/mobile/ads/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/mobile/ads/e;->f()Ljava/lang/String;

    move-result-object v1

    .line 68
    new-instance v2, Lcom/yandex/mobile/ads/as;

    iget-object v3, p0, Lcom/yandex/mobile/ads/nativeads/j;->b:Landroid/content/Context;

    iget-object v4, p0, Lcom/yandex/mobile/ads/nativeads/j;->k:Lcom/yandex/mobile/ads/as$c;

    iget-object v5, p0, Lcom/yandex/mobile/ads/nativeads/j;->c:Lcom/yandex/mobile/ads/nativeads/l;

    invoke-static {v5}, Lcom/yandex/mobile/ads/s;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/yandex/mobile/ads/as;-><init>(Landroid/content/Context;Lcom/yandex/mobile/ads/as$c;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/yandex/mobile/ads/nativeads/j;->e:Lcom/yandex/mobile/ads/as;

    .line 69
    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/j;->e:Lcom/yandex/mobile/ads/as;

    iget-object v3, p0, Lcom/yandex/mobile/ads/nativeads/j;->l:Lcom/yandex/mobile/ads/report/b$a;

    invoke-virtual {v2, v3}, Lcom/yandex/mobile/ads/as;->a(Lcom/yandex/mobile/ads/report/b$a;)V

    .line 70
    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/j;->e:Lcom/yandex/mobile/ads/as;

    invoke-virtual {v2, v1, v0}, Lcom/yandex/mobile/ads/as;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 72
    new-instance v0, Lcom/yandex/mobile/ads/nativeads/r;

    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/j;->f:Lcom/yandex/mobile/ads/nativeads/h;

    iget-object v3, p0, Lcom/yandex/mobile/ads/nativeads/j;->c:Lcom/yandex/mobile/ads/nativeads/l;

    iget-object v4, p0, Lcom/yandex/mobile/ads/nativeads/j;->e:Lcom/yandex/mobile/ads/as;

    invoke-direct {v0, v2, v3, v4}, Lcom/yandex/mobile/ads/nativeads/r;-><init>(Lcom/yandex/mobile/ads/nativeads/h;Lcom/yandex/mobile/ads/nativeads/l;Lcom/yandex/mobile/ads/as;)V

    iput-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->g:Lcom/yandex/mobile/ads/nativeads/r;

    .line 73
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->c:Lcom/yandex/mobile/ads/nativeads/l;

    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/j;->g:Lcom/yandex/mobile/ads/nativeads/r;

    invoke-virtual {v0, v2}, Lcom/yandex/mobile/ads/nativeads/l;->a(Lcom/yandex/mobile/ads/nativeads/r;)V

    .line 74
    invoke-static {}, Lcom/yandex/mobile/ads/PhoneStateTracker;->a()Lcom/yandex/mobile/ads/PhoneStateTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->h:Lcom/yandex/mobile/ads/PhoneStateTracker;

    .line 76
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->f:Lcom/yandex/mobile/ads/nativeads/h;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/h;->c()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/yandex/mobile/ads/nativeads/j;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lcom/yandex/mobile/ads/nativeads/j;->a()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/yandex/mobile/ads/nativeads/j;->a(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0, v1, v0}, Lcom/yandex/mobile/ads/nativeads/j;->a(Ljava/lang/String;Ljava/util/List;)Lcom/yandex/mobile/ads/report/b;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/j;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/yandex/mobile/ads/report/a;->a(Landroid/content/Context;)Lcom/yandex/mobile/ads/report/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/yandex/mobile/ads/report/a;->a(Lcom/yandex/mobile/ads/report/b;)V

    .line 77
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/yandex/mobile/ads/nativeads/j;)Lcom/yandex/mobile/ads/nativeads/p;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->d:Lcom/yandex/mobile/ads/nativeads/p;

    return-object v0
.end method

.method static synthetic b(Lcom/yandex/mobile/ads/nativeads/j;)Lcom/yandex/mobile/ads/as;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->e:Lcom/yandex/mobile/ads/as;

    return-object v0
.end method

.method static synthetic c(Lcom/yandex/mobile/ads/nativeads/j;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lcom/yandex/mobile/ads/nativeads/j;)Lcom/yandex/mobile/ads/PhoneStateTracker;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->h:Lcom/yandex/mobile/ads/PhoneStateTracker;

    return-object v0
.end method

.method static synthetic e(Lcom/yandex/mobile/ads/nativeads/j;)Lcom/yandex/mobile/ads/nativeads/j$a;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->i:Lcom/yandex/mobile/ads/nativeads/j$a;

    return-object v0
.end method

.method static synthetic f(Lcom/yandex/mobile/ads/nativeads/j;)Lcom/yandex/mobile/ads/nativeads/h;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->f:Lcom/yandex/mobile/ads/nativeads/h;

    return-object v0
.end method


# virtual methods
.method a(Ljava/lang/String;Ljava/util/List;)Lcom/yandex/mobile/ads/report/b;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/yandex/mobile/ads/report/b;"
        }
    .end annotation

    .prologue
    .line 134
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 135
    const-string v1, "block_id"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    const-string v1, "assets"

    invoke-interface {p2}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    new-instance v1, Lcom/yandex/mobile/ads/report/b;

    sget-object v2, Lcom/yandex/mobile/ads/report/b$b;->j:Lcom/yandex/mobile/ads/report/b$b;

    invoke-direct {v1, v2, v0}, Lcom/yandex/mobile/ads/report/b;-><init>(Lcom/yandex/mobile/ads/report/b$b;Ljava/util/Map;)V

    return-object v1
.end method

.method protected abstract a()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method a(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/yandex/mobile/ads/nativeads/b;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 113
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/b;

    .line 114
    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/b;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 117
    :cond_0
    return-object v1
.end method

.method a(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    invoke-interface {p2, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 124
    return-object p2
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onVisibilityChanged(), visibility = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clazz = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/j;->c:Lcom/yandex/mobile/ads/nativeads/l;

    invoke-static {v1}, Lcom/yandex/mobile/ads/s;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    if-nez p1, :cond_0

    .line 159
    invoke-virtual {p0}, Lcom/yandex/mobile/ads/nativeads/j;->b()V

    .line 163
    :goto_0
    return-void

    .line 161
    :cond_0
    invoke-virtual {p0}, Lcom/yandex/mobile/ads/nativeads/j;->c()V

    goto :goto_0
.end method

.method a(Lcom/yandex/mobile/ads/nativeads/q;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/yandex/mobile/ads/nativeads/NativeAdException;
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->d:Lcom/yandex/mobile/ads/nativeads/p;

    invoke-virtual {v0, p1}, Lcom/yandex/mobile/ads/nativeads/p;->a(Lcom/yandex/mobile/ads/nativeads/q;)V

    .line 81
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->d:Lcom/yandex/mobile/ads/nativeads/p;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/p;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->g:Lcom/yandex/mobile/ads/nativeads/r;

    invoke-virtual {v0, p1}, Lcom/yandex/mobile/ads/nativeads/r;->a(Lcom/yandex/mobile/ads/nativeads/q;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "renderAdView(), BIND, clazz = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/j;->c:Lcom/yandex/mobile/ads/nativeads/l;

    invoke-static {v1}, Lcom/yandex/mobile/ads/s;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/yandex/mobile/ads/nativeads/j;->b()V

    return-void

    .line 84
    :cond_0
    new-instance v0, Lcom/yandex/mobile/ads/nativeads/NativeAdException;

    const-string v1, "Resource for required view is not present"

    invoke-direct {v0, v1}, Lcom/yandex/mobile/ads/nativeads/NativeAdException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lcom/yandex/mobile/ads/nativeads/template/NativeBannerView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/yandex/mobile/ads/nativeads/NativeAdException;
        }
    .end annotation

    .prologue
    .line 213
    sget-object v0, Lcom/yandex/mobile/ads/nativeads/j$a;->b:Lcom/yandex/mobile/ads/nativeads/j$a;

    iput-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->i:Lcom/yandex/mobile/ads/nativeads/j$a;

    .line 215
    new-instance v0, Lcom/yandex/mobile/ads/nativeads/template/b;

    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/j;->a:Lcom/yandex/mobile/ads/nativeads/d;

    invoke-direct {v0, p1, v1}, Lcom/yandex/mobile/ads/nativeads/template/b;-><init>(Lcom/yandex/mobile/ads/nativeads/template/NativeBannerView;Lcom/yandex/mobile/ads/nativeads/d;)V

    invoke-virtual {p0, v0}, Lcom/yandex/mobile/ads/nativeads/j;->a(Lcom/yandex/mobile/ads/nativeads/q;)V

    .line 216
    invoke-virtual {p1, p0}, Lcom/yandex/mobile/ads/nativeads/template/NativeBannerView;->a(Lcom/yandex/mobile/ads/nativeads/j;)V

    .line 217
    return-void
.end method

.method public addImageLoadingListener(Lcom/yandex/mobile/ads/nativeads/NativeAdImageLoadingListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/yandex/mobile/ads/nativeads/NativeAdImageLoadingListener;

    .prologue
    .line 231
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->c:Lcom/yandex/mobile/ads/nativeads/l;

    invoke-virtual {v0, p1}, Lcom/yandex/mobile/ads/nativeads/l;->addImageLoadingListener(Lcom/yandex/mobile/ads/nativeads/NativeAdImageLoadingListener;)V

    .line 232
    return-void
.end method

.method b()V
    .locals 3

    .prologue
    .line 142
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->d:Lcom/yandex/mobile/ads/nativeads/p;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/p;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 143
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "registerTrackers(), isNativeAdViewShown = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clazz = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/j;->c:Lcom/yandex/mobile/ads/nativeads/l;

    invoke-static {v1}, Lcom/yandex/mobile/ads/s;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->e:Lcom/yandex/mobile/ads/as;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/as;->a()V

    .line 145
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->h:Lcom/yandex/mobile/ads/PhoneStateTracker;

    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/j;->j:Lcom/yandex/mobile/ads/PhoneStateTracker$b;

    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/j;->b:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, Lcom/yandex/mobile/ads/PhoneStateTracker;->a(Lcom/yandex/mobile/ads/PhoneStateTracker$b;Landroid/content/Context;)V

    .line 146
    return-void

    .line 142
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c()V
    .locals 3

    .prologue
    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "unregisterTrackers(), clazz = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/j;->c:Lcom/yandex/mobile/ads/nativeads/l;

    invoke-static {v1}, Lcom/yandex/mobile/ads/s;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->e:Lcom/yandex/mobile/ads/as;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/as;->b()V

    .line 151
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->h:Lcom/yandex/mobile/ads/PhoneStateTracker;

    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/j;->j:Lcom/yandex/mobile/ads/PhoneStateTracker$b;

    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/j;->b:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, Lcom/yandex/mobile/ads/PhoneStateTracker;->b(Lcom/yandex/mobile/ads/PhoneStateTracker$b;Landroid/content/Context;)V

    .line 152
    return-void
.end method

.method d()V
    .locals 1

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/yandex/mobile/ads/nativeads/j;->c()V

    .line 194
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->c:Lcom/yandex/mobile/ads/nativeads/l;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/l;->a()V

    .line 195
    return-void
.end method

.method public getAdAssets()Lcom/yandex/mobile/ads/nativeads/NativeAdAssets;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->c:Lcom/yandex/mobile/ads/nativeads/l;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/l;->getAdAssets()Lcom/yandex/mobile/ads/nativeads/NativeAdAssets;

    move-result-object v0

    return-object v0
.end method

.method public getAdType()Lcom/yandex/mobile/ads/nativeads/NativeAdType;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->c:Lcom/yandex/mobile/ads/nativeads/l;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/l;->getAdType()Lcom/yandex/mobile/ads/nativeads/NativeAdType;

    move-result-object v0

    return-object v0
.end method

.method public loadImages()V
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->c:Lcom/yandex/mobile/ads/nativeads/l;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/nativeads/l;->loadImages()V

    .line 227
    return-void
.end method

.method public removeImageLoadingListener(Lcom/yandex/mobile/ads/nativeads/NativeAdImageLoadingListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/yandex/mobile/ads/nativeads/NativeAdImageLoadingListener;

    .prologue
    .line 236
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->c:Lcom/yandex/mobile/ads/nativeads/l;

    invoke-virtual {v0, p1}, Lcom/yandex/mobile/ads/nativeads/l;->removeImageLoadingListener(Lcom/yandex/mobile/ads/nativeads/NativeAdImageLoadingListener;)V

    .line 237
    return-void
.end method

.method public setAdEventListener(Lcom/yandex/mobile/ads/nativeads/NativeAdEventListener;)V
    .locals 1
    .param p1, "eventListener"    # Lcom/yandex/mobile/ads/nativeads/NativeAdEventListener;

    .prologue
    .line 172
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->c:Lcom/yandex/mobile/ads/nativeads/l;

    invoke-virtual {v0, p1}, Lcom/yandex/mobile/ads/nativeads/l;->setAdEventListener(Lcom/yandex/mobile/ads/nativeads/NativeAdEventListener;)V

    .line 173
    return-void
.end method

.method public shouldOpenLinksInApp(Z)V
    .locals 1
    .param p1, "shouldOpenLinksInApp"    # Z

    .prologue
    .line 184
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/j;->c:Lcom/yandex/mobile/ads/nativeads/l;

    invoke-virtual {v0, p1}, Lcom/yandex/mobile/ads/nativeads/l;->shouldOpenLinksInApp(Z)V

    .line 185
    return-void
.end method
