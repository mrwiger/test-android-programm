.class public Lcom/yandex/metrica/impl/ob/bs;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ob/bs$a;
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/yandex/metrica/impl/utils/h;

.field private c:Lcom/yandex/metrica/impl/ob/bs$a;

.field private d:Lcom/yandex/metrica/impl/ob/br;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 130
    new-instance v0, Lcom/yandex/metrica/impl/utils/h;

    const-string v1, "com.yandex.metrica.impl.configservice.client.ConfigurationServiceLauncher.lock"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/utils/h;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/yandex/metrica/impl/ob/br;

    invoke-direct {v1, p1}, Lcom/yandex/metrica/impl/ob/br;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/yandex/metrica/impl/ob/bs;-><init>(Landroid/content/Context;Lcom/yandex/metrica/impl/utils/h;Lcom/yandex/metrica/impl/ob/br;)V

    .line 131
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/yandex/metrica/impl/utils/h;Lcom/yandex/metrica/impl/ob/br;)V
    .locals 0

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/bs;->a:Landroid/content/Context;

    .line 137
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/bs;->b:Lcom/yandex/metrica/impl/utils/h;

    .line 138
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/bs;->d:Lcom/yandex/metrica/impl/ob/br;

    .line 139
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 289
    const-wide v2, 0x7fffffffffffffffL

    .line 290
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 291
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v1, v0

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 292
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v5, v5, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/yandex/metrica/impl/bf;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)J

    move-result-wide v4

    .line 294
    cmp-long v7, v4, v2

    if-gez v7, :cond_1

    .line 296
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 297
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-wide v2, v4

    goto :goto_0

    .line 298
    :cond_1
    cmp-long v4, v4, v2

    if-nez v4, :cond_0

    .line 299
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 302
    :cond_2
    return-object v1
.end method

.method private static a(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 271
    const/high16 v1, -0x80000000

    .line 272
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 273
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    move-object v1, v0

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 274
    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v3, v3, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    const-string v5, "metrica:configuration:api:level"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 275
    if-le v3, v2, :cond_1

    .line 277
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 278
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v2, v3

    goto :goto_0

    .line 279
    :cond_1
    if-ne v3, v2, :cond_0

    .line 280
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 283
    :cond_2
    return-object v1
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/ob/bs;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 39
    .line 2206
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bs;->d:Lcom/yandex/metrica/impl/ob/br;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/br;->a()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2208
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bs;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2211
    :goto_0
    return-void

    .line 39
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static b()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 221
    :try_start_0
    new-instance v2, Landroid/net/LocalSocket;

    invoke-direct {v2}, Landroid/net/LocalSocket;-><init>()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 223
    :try_start_1
    new-instance v1, Landroid/net/LocalSocketAddress;

    const-string v3, "com.yandex.metrica.configuration.MetricaConfigurationService"

    invoke-direct {v1, v3}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    .line 225
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    invoke-virtual {v2}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 226
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v0

    .line 234
    :try_start_3
    invoke-virtual {v2}, Landroid/net/LocalSocket;->close()V

    .line 237
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5

    .line 243
    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    move-object v1, v0

    move-object v2, v0

    .line 233
    :goto_1
    if-eqz v2, :cond_1

    .line 234
    :try_start_4
    invoke-virtual {v2}, Landroid/net/LocalSocket;->close()V

    .line 236
    :cond_1
    if-eqz v1, :cond_0

    .line 237
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 242
    :catch_1
    move-exception v1

    goto :goto_0

    .line 232
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    .line 233
    :goto_2
    if-eqz v2, :cond_2

    .line 234
    :try_start_5
    invoke-virtual {v2}, Landroid/net/LocalSocket;->close()V

    .line 236
    :cond_2
    if-eqz v1, :cond_3

    .line 237
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 242
    :cond_3
    :goto_3
    throw v0

    :catch_2
    move-exception v1

    goto :goto_3

    .line 232
    :catchall_1
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_2

    :catchall_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v1

    move-object v1, v0

    goto :goto_1

    :catch_4
    move-exception v3

    goto :goto_1

    :catch_5
    move-exception v1

    goto :goto_0
.end method

.method private static b(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 307
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 308
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 309
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 311
    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 312
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 144
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bs;->b:Lcom/yandex/metrica/impl/utils/h;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/utils/h;->c()V

    .line 146
    invoke-static {}, Lcom/yandex/metrica/impl/ob/bs;->b()Ljava/lang/String;

    move-result-object v0

    .line 1162
    new-instance v3, Ljava/util/concurrent/CountDownLatch;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 1172
    if-nez v0, :cond_0

    .line 1249
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bs;->a:Landroid/content/Context;

    .line 1317
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const-string v5, "com.yandex.metrica.configuration.ACTION_START"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 1318
    invoke-static {v4}, Lcom/yandex/metrica/impl/bf;->a(Landroid/content/Intent;)V

    .line 1320
    invoke-static {v0, v4}, Lcom/yandex/metrica/impl/bf;->a(Landroid/content/Context;Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    .line 1251
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    .line 1252
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v1, :cond_2

    .line 1253
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    .line 1173
    :cond_0
    :goto_0
    invoke-static {v0}, Lcom/yandex/metrica/impl/bj;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 2201
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v4, "com.yandex.metrica.configuration.ACTION_START"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1176
    new-instance v2, Lcom/yandex/metrica/impl/ob/bs$a;

    iget-object v4, p0, Lcom/yandex/metrica/impl/ob/bs;->a:Landroid/content/Context;

    new-instance v5, Lcom/yandex/metrica/impl/ob/bs$1;

    invoke-direct {v5, p0, v0, v3}, Lcom/yandex/metrica/impl/ob/bs$1;-><init>(Lcom/yandex/metrica/impl/ob/bs;Landroid/content/Intent;Ljava/util/concurrent/CountDownLatch;)V

    invoke-direct {v2, v4, v5}, Lcom/yandex/metrica/impl/ob/bs$a;-><init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/bs$a$a;)V

    iput-object v2, p0, Lcom/yandex/metrica/impl/ob/bs;->c:Lcom/yandex/metrica/impl/ob/bs$a;

    .line 1191
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/bs;->c:Lcom/yandex/metrica/impl/ob/bs$a;

    invoke-virtual {v2, v0}, Lcom/yandex/metrica/impl/ob/bs$a;->a(Landroid/content/Intent;)V

    move v0, v1

    .line 1164
    :goto_1
    if-eqz v0, :cond_1

    .line 1166
    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Lcom/yandex/metrica/impl/utils/h$a; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bs;->b:Lcom/yandex/metrica/impl/utils/h;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/utils/h;->b()V

    .line 156
    :goto_2
    return-void

    .line 1255
    :cond_2
    :try_start_1
    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/bs;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 1257
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v1, :cond_3

    .line 1258
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    goto :goto_0

    .line 1260
    :cond_3
    iget-object v4, p0, Lcom/yandex/metrica/impl/ob/bs;->a:Landroid/content/Context;

    invoke-static {v4, v0}, Lcom/yandex/metrica/impl/ob/bs;->a(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 1262
    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/bs;->b(Ljava/util/List;)Ljava/lang/String;
    :try_end_1
    .catch Lcom/yandex/metrica/impl/utils/h$a; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 1266
    :cond_4
    const/4 v0, 0x0

    .line 1172
    goto :goto_0

    :cond_5
    move v0, v2

    .line 1196
    goto :goto_1

    .line 155
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bs;->b:Lcom/yandex/metrica/impl/utils/h;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/utils/h;->b()V

    goto :goto_2

    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bs;->b:Lcom/yandex/metrica/impl/utils/h;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/utils/h;->b()V

    goto :goto_2

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/bs;->b:Lcom/yandex/metrica/impl/utils/h;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/utils/h;->b()V

    .line 156
    throw v0
.end method
