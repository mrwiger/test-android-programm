.class public Lru/cn/ad/interstitial/adapters/YandexInterstitial;
.super Lru/cn/ad/AdAdapter;
.source "YandexInterstitial.java"


# instance fields
.field private final blockId:Ljava/lang/String;

.field private interstitialAd:Lcom/yandex/mobile/ads/InterstitialAd;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "adSystem"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lru/cn/ad/AdAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    .line 25
    const-string v0, "block_id"

    invoke-virtual {p2, v0}, Lru/cn/api/money_miner/replies/AdSystem;->getParamOrThrow(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->blockId:Ljava/lang/String;

    .line 26
    return-void
.end method

.method static synthetic access$000(Lru/cn/ad/interstitial/adapters/YandexInterstitial;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/interstitial/adapters/YandexInterstitial;

    .prologue
    .line 16
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/ad/interstitial/adapters/YandexInterstitial;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/interstitial/adapters/YandexInterstitial;

    .prologue
    .line 16
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/ad/interstitial/adapters/YandexInterstitial;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/interstitial/adapters/YandexInterstitial;

    .prologue
    .line 16
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/ad/interstitial/adapters/YandexInterstitial;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/interstitial/adapters/YandexInterstitial;

    .prologue
    .line 16
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$400(Lru/cn/ad/interstitial/adapters/YandexInterstitial;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/interstitial/adapters/YandexInterstitial;

    .prologue
    .line 16
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$500(Lru/cn/ad/interstitial/adapters/YandexInterstitial;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/interstitial/adapters/YandexInterstitial;

    .prologue
    .line 16
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 90
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->interstitialAd:Lcom/yandex/mobile/ads/InterstitialAd;

    invoke-virtual {v0, v1}, Lcom/yandex/mobile/ads/InterstitialAd;->setInterstitialEventListener(Lcom/yandex/mobile/ads/InterstitialEventListener;)V

    .line 91
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->interstitialAd:Lcom/yandex/mobile/ads/InterstitialAd;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/InterstitialAd;->destroy()V

    .line 93
    iput-object v1, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->interstitialAd:Lcom/yandex/mobile/ads/InterstitialAd;

    .line 94
    return-void
.end method

.method protected onLoad()V
    .locals 3

    .prologue
    .line 30
    new-instance v1, Lcom/yandex/mobile/ads/InterstitialAd;

    iget-object v2, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/yandex/mobile/ads/InterstitialAd;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->interstitialAd:Lcom/yandex/mobile/ads/InterstitialAd;

    .line 31
    iget-object v1, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->interstitialAd:Lcom/yandex/mobile/ads/InterstitialAd;

    iget-object v2, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->blockId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/yandex/mobile/ads/InterstitialAd;->setBlockId(Ljava/lang/String;)V

    .line 32
    iget-object v1, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->interstitialAd:Lcom/yandex/mobile/ads/InterstitialAd;

    new-instance v2, Lru/cn/ad/interstitial/adapters/YandexInterstitial$1;

    invoke-direct {v2, p0}, Lru/cn/ad/interstitial/adapters/YandexInterstitial$1;-><init>(Lru/cn/ad/interstitial/adapters/YandexInterstitial;)V

    invoke-virtual {v1, v2}, Lcom/yandex/mobile/ads/InterstitialAd;->setInterstitialEventListener(Lcom/yandex/mobile/ads/InterstitialEventListener;)V

    .line 77
    invoke-static {}, Lcom/yandex/mobile/ads/AdRequest;->builder()Lcom/yandex/mobile/ads/AdRequest$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/mobile/ads/AdRequest$Builder;->build()Lcom/yandex/mobile/ads/AdRequest;

    move-result-object v0

    .line 78
    .local v0, "adRequest":Lcom/yandex/mobile/ads/AdRequest;
    iget-object v1, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->interstitialAd:Lcom/yandex/mobile/ads/InterstitialAd;

    invoke-virtual {v1, v0}, Lcom/yandex/mobile/ads/InterstitialAd;->loadAd(Lcom/yandex/mobile/ads/AdRequest;)V

    .line 79
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->interstitialAd:Lcom/yandex/mobile/ads/InterstitialAd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->interstitialAd:Lcom/yandex/mobile/ads/InterstitialAd;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/InterstitialAd;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->interstitialAd:Lcom/yandex/mobile/ads/InterstitialAd;

    invoke-virtual {v0}, Lcom/yandex/mobile/ads/InterstitialAd;->show()V

    .line 86
    :cond_0
    return-void
.end method
